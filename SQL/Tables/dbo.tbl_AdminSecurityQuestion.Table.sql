USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_AdminSecurityQuestion]    Script Date: 8/31/2020 6:37:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_AdminSecurityQuestion](
	[SecurityQuestionCode] [int] NOT NULL,
	[Question] [nvarchar](250) NOT NULL
) ON [PRIMARY]
GO
