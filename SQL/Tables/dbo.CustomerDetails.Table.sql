USE [Tegans]
GO
/****** Object:  Table [dbo].[CustomerDetails]    Script Date: 8/31/2020 6:37:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerDetails](
	[S NO] [float] NULL,
	[CUSTOMER'S NAME] [nvarchar](255) NULL,
	[CONTACT PERSON] [nvarchar](255) NULL,
	[Tin Number] [nvarchar](255) NULL,
	[PLACE] [nvarchar](255) NULL,
	[MOBILE 1] [float] NULL,
	[ADDRESS 2] [nvarchar](255) NULL,
	[Sales Executive] [nvarchar](255) NULL,
	[commission] [nvarchar](255) NULL,
	[commission type] [nvarchar](255) NULL,
	[DISTRICT] [nvarchar](255) NULL,
	[STATE NAME] [nvarchar](255) NULL,
	[PINCODE] [float] NULL,
	[FAX NO#] [nvarchar](255) NULL,
	[E_Mail ID] [nvarchar](255) NULL,
	[F16] [nvarchar](255) NULL
) ON [PRIMARY]
GO
