USE [Tegans]
GO
/****** Object:  Table [dbo].[Tbl_EmpDepot]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_EmpDepot](
	[EDID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Form_Id] [int] NULL,
	[Depotexpenes] [nvarchar](max) NULL,
	[Labour] [nvarchar](50) NULL,
	[Work_Hours] [int] NULL,
 CONSTRAINT [PK_Tbl_EmpDepot] PRIMARY KEY CLUSTERED 
(
	[EDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
