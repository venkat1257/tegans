USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_CompanyMaster]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CompanyMaster](
	[CompanyId] [int] IDENTITY(112,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[CompanyDesc] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_CompanyMaster] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
