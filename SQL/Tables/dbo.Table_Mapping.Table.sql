USE [Tegans]
GO
/****** Object:  Table [dbo].[Table_Mapping]    Script Date: 8/31/2020 6:37:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Mapping](
	[Table_ID] [int] IDENTITY(110000000,1) NOT NULL,
	[Module_ID] [int] NULL,
	[Table_Name] [varchar](50) NULL,
	[Alias_Table_Name] [varchar](50) NULL,
	[Company_ID] [int] NULL,
	[Operating_Unit_ID] [int] NULL,
	[Account_Owner_ID] [int] NULL,
	[Classification_ID] [int] NULL,
	[Department_ID] [int] NULL,
	[LCreated_Date] [datetime] NULL,
	[Lmodified_Date] [datetime] NULL,
	[Created_By] [int] NULL,
	[GCreated_Date] [datetime] NULL,
	[Gmodified_Date] [datetime] NULL,
	[Modified_By] [int] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Table_Mapping] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
