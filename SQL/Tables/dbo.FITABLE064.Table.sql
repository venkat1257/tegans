USE [Tegans]
GO
/****** Object:  Table [dbo].[FITABLE064]    Script Date: 8/31/2020 6:37:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FITABLE064](
	[COLUMN01] [bigint] IDENTITY(100000000,1) NOT NULL,
	[COLUMN02] [bigint] NOT NULL,
	[COLUMN03] [nvarchar](250) NULL,
	[COLUMN04] [nvarchar](250) NULL,
	[COLUMN05] [bigint] NULL,
	[COLUMN06] [bigint] NULL,
	[COLUMN07] [datetime] NULL,
	[COLUMN08] [nvarchar](250) NULL,
	[COLUMN09] [bigint] NULL,
	[COLUMN10] [bigint] NULL,
	[COLUMN11] [bigint] NULL,
	[COLUMN12] [nvarchar](1000) NULL,
	[COLUMN13] [bigint] NULL,
	[COLUMN14] [decimal](18, 4) NULL,
	[COLUMN15] [decimal](18, 4) NULL,
	[COLUMN16] [nvarchar](250) NULL,
	[COLUMN17] [nvarchar](250) NULL,
	[COLUMN18] [nvarchar](250) NULL,
	[COLUMN19] [nvarchar](250) NULL,
	[COLUMN20] [nvarchar](250) NULL,
	[COLUMNA01] [int] NULL,
	[COLUMNA02] [int] NULL,
	[COLUMNA03] [int] NULL,
	[COLUMNA04] [int] NULL,
	[COLUMNA05] [int] NULL,
	[COLUMNA06] [datetime] NULL,
	[COLUMNA07] [datetime] NULL,
	[COLUMNA08] [int] NULL,
	[COLUMNA09] [datetime] NULL,
	[COLUMNA10] [datetime] NULL,
	[COLUMNA11] [int] NULL,
	[COLUMNA12] [bit] NULL,
	[COLUMNA13] [bit] NULL,
	[COLUMNB01] [nvarchar](250) NULL,
	[COLUMNB02] [nvarchar](250) NULL,
	[COLUMNB03] [nvarchar](250) NULL,
	[COLUMNB04] [nvarchar](250) NULL,
	[COLUMNB05] [nvarchar](250) NULL,
	[COLUMNB06] [nvarchar](250) NULL,
	[COLUMNB07] [datetime] NULL,
	[COLUMNB08] [datetime] NULL,
	[COLUMNB09] [decimal](18, 4) NULL,
	[COLUMNB10] [decimal](18, 4) NULL,
	[COLUMNB11] [int] NULL,
	[COLUMNB12] [int] NULL,
	[COLUMND01] [nvarchar](250) NULL,
	[COLUMND02] [nvarchar](250) NULL,
	[COLUMND03] [nvarchar](250) NULL,
	[COLUMND04] [nvarchar](250) NULL,
	[COLUMND05] [nvarchar](250) NULL,
	[COLUMND06] [nvarchar](250) NULL,
	[COLUMND07] [datetime] NULL,
	[COLUMND08] [datetime] NULL,
	[COLUMND09] [decimal](18, 4) NULL,
	[COLUMND10] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[COLUMN02] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FITABLE064] ADD  DEFAULT (NEXT VALUE FOR [DBO].[FITABLE064_Sequence]) FOR [COLUMN02]
GO
