USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_EmployeeMaster]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_EmployeeMaster](
	[EmployeeID] [int] IDENTITY(203,1) NOT NULL,
	[EmployeeName] [nvarchar](50) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[TypeOfBusines] [nvarchar](50) NOT NULL,
	[IsActive] [nvarchar](50) NULL,
	[Email] [nvarchar](250) NOT NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_EmployeeMaster] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
