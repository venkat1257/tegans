USE [Tegans]
GO
/****** Object:  Table [dbo].[PUTABLE005]    Script Date: 8/31/2020 6:37:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PUTABLE005](
	[COLUMN01] [int] IDENTITY(1,1) NOT NULL,
	[COLUMN02] [nvarchar](250) NOT NULL,
	[COLUMN03] [int] NULL,
	[COLUMN04] [nvarchar](250) NULL,
	[COLUMN05] [nvarchar](250) NULL,
	[COLUMN06] [nvarchar](250) NULL,
	[COLUMN07] [nvarchar](250) NULL,
	[COLUMN08] [datetime] NULL,
	[COLUMN09] [nvarchar](250) NULL,
	[COLUMN10] [nvarchar](250) NULL,
	[COLUMN11] [datetime] NULL,
	[COLUMN12] [nvarchar](250) NULL,
	[COLUMN13] [nvarchar](250) NULL,
	[COLUMN14] [decimal](18, 2) NULL,
	[COLUMN15] [nvarchar](250) NULL,
	[COLUMN16] [nvarchar](250) NULL,
	[COLUMN17] [nvarchar](250) NULL,
	[COLUMN18] [nvarchar](250) NULL,
	[COLUMN19] [nvarchar](250) NULL,
	[COLUMN20] [nvarchar](250) NULL,
	[COLUMN21] [nvarchar](250) NULL,
	[COLUMN22] [decimal](18, 2) NULL,
	[COLUMN23] [nvarchar](250) NULL,
	[COLUMN24] [decimal](18, 2) NULL,
	[COLUMN25] [nvarchar](250) NULL,
	[COLUMNA01] [int] NULL,
	[COLUMNA02] [int] NULL,
	[COLUMNA03] [int] NULL,
	[COLUMNA04] [int] NULL,
	[COLUMNA05] [int] NULL,
	[COLUMNA06] [datetime] NULL,
	[COLUMNA07] [datetime] NULL,
	[COLUMNA08] [int] NULL,
	[COLUMNA09] [datetime] NULL,
	[COLUMNA10] [datetime] NULL,
	[COLUMNA11] [int] NULL,
	[COLUMNA12] [bit] NULL,
	[COLUMNA13] [bit] NULL,
	[COLUMNB01] [nvarchar](250) NULL,
	[COLUMNB02] [nvarchar](250) NULL,
	[COLUMNB03] [nvarchar](250) NULL,
	[COLUMNB04] [nvarchar](250) NULL,
	[COLUMNB05] [nvarchar](250) NULL,
	[COLUMNB06] [nvarchar](250) NULL,
	[COLUMNB07] [datetime] NULL,
	[COLUMNB08] [datetime] NULL,
	[COLUMNB09] [decimal](18, 0) NULL,
	[COLUMNB10] [decimal](18, 0) NULL,
	[COLUMNB11] [int] NULL,
	[COLUMNB12] [int] NULL,
	[COLUMND01] [nvarchar](250) NULL,
	[COLUMND02] [nvarchar](250) NULL,
	[COLUMND03] [nvarchar](250) NULL,
	[COLUMND04] [nvarchar](250) NULL,
	[COLUMND05] [nvarchar](250) NULL,
	[COLUMND06] [nvarchar](250) NULL,
	[COLUMND07] [datetime] NULL,
	[COLUMND08] [datetime] NULL,
	[COLUMND09] [decimal](18, 0) NULL,
	[COLUMND10] [int] NULL,
	[COLUMN26] [nvarchar](250) NULL,
	[COLUMN27] [nvarchar](250) NULL,
	[COLUMN28] [decimal](18, 2) NULL,
	[COLUMN29] [int] NULL,
	[column30] [nvarchar](250) NULL,
	[COLUMN31] [nvarchar](250) NULL,
	[COLUMN32] [nvarchar](250) NULL,
	[COLUMN33] [nvarchar](250) NULL,
	[COLUMN34] [decimal](18, 2) NULL,
	[COLUMN35] [decimal](18, 2) NULL,
	[COLUMN36] [nvarchar](250) NULL,
	[COLUMN37] [nvarchar](250) NULL,
	[COLUMN38] [bigint] NULL,
	[COLUMN39] [nvarchar](250) NULL,
	[COLUMN40] [decimal](18, 2) NULL,
	[COLUMN41] [decimal](18, 2) NULL,
	[COLUMN42] [decimal](18, 2) NULL,
	[COLUMN43] [int] NULL,
	[COLUMN44] [decimal](18, 2) NULL,
	[COLUMN45] [decimal](18, 2) NULL,
	[COLUMN46] [decimal](18, 2) NULL,
	[COLUMN55] [nvarchar](250) NULL,
	[COLUMN56] [int] NULL,
	[COLUMN57] [int] NULL,
	[COLUMN58] [bit] NULL,
	[COLUMN59] [decimal](18, 2) NULL,
	[COLUMN74] [decimal](18, 2) NULL,
	[COLUMN60] [nvarchar](250) NULL,
	[COLUMN61] [nvarchar](250) NULL,
	[COLUMN62] [decimal](18, 2) NULL,
	[COLUMN63] [decimal](18, 2) NULL,
	[COLUMN64] [nvarchar](250) NULL,
	[COLUMN66] [bit] NULL,
 CONSTRAINT [PK__PUTABLE0__8723CF0FB85AAE91] PRIMARY KEY CLUSTERED 
(
	[COLUMN02] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
