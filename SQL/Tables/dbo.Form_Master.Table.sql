USE [Tegans]
GO
/****** Object:  Table [dbo].[Form_Master]    Script Date: 8/31/2020 6:37:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Form_Master](
	[F_Id] [int] IDENTITY(1010,10) NOT NULL,
	[Form_Name] [nvarchar](50) NULL,
	[Form_Type] [nvarchar](50) NULL,
	[Parent] [int] NULL,
	[Module_Id] [int] NULL,
	[Status] [nchar](1) NULL,
 CONSTRAINT [PK_Form_Master] PRIMARY KEY CLUSTERED 
(
	[F_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
