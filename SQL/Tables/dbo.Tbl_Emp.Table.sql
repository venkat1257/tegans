USE [Tegans]
GO
/****** Object:  Table [dbo].[Tbl_Emp]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Emp](
	[EmployeeID] [int] IDENTITY(508,1) NOT NULL,
	[Form_Id] [int] NULL,
	[Employee_No] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[Phone_No] [bigint] NULL,
	[Mobile_No] [bigint] NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Location] [nvarchar](50) NULL,
	[Subsidary] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[DepLocation] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tbl_Emp] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
