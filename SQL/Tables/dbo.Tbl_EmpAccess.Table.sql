USE [Tegans]
GO
/****** Object:  Table [dbo].[Tbl_EmpAccess]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_EmpAccess](
	[EAID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Form_Id] [int] NULL,
	[Give_Access] [nvarchar](max) NULL,
	[Send_notification_email] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Confirm_password] [nvarchar](50) NULL,
	[Required_password_change_on_Next_Login] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tbl_EmpAccess] PRIMARY KEY CLUSTERED 
(
	[EAID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
