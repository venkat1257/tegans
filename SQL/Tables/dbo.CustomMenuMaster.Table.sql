USE [Tegans]
GO
/****** Object:  Table [dbo].[CustomMenuMaster]    Script Date: 8/31/2020 6:37:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomMenuMaster](
	[MenuId] [int] NOT NULL,
	[CenterId] [int] NULL,
	[ModuleName] [nvarchar](max) NULL,
	[Priority] [int] NULL,
	[IsEnable] [nvarchar](max) NULL,
	[IsDelete] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
