USE [Tegans]
GO
/****** Object:  Table [dbo].[POTABLE003]    Script Date: 8/31/2020 6:37:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POTABLE003](
	[COLUMN001] [nvarchar](250) NULL,
	[COLUMN002] [nvarchar](250) NULL,
	[COLUMN003] [nvarchar](250) NULL,
	[COLUMN004] [date] NULL,
	[COLUMN005] [nvarchar](250) NULL,
	[COLUMN006] [nvarchar](250) NULL,
	[COLUMN007] [int] NULL,
	[COLUMN008] [decimal](18, 0) NULL,
	[COLUMN009] [decimal](18, 0) NULL,
	[COLUMN010] [nvarchar](250) NULL,
	[COLUMNB001] [nvarchar](250) NULL,
	[COLUMNB002] [nvarchar](250) NULL,
	[COLUMNB003] [nvarchar](250) NULL,
	[COLUMNB004] [nvarchar](250) NULL,
	[COLUMNB005] [nvarchar](250) NULL,
	[COLUMNB006] [nvarchar](250) NULL,
	[COLUMNB007] [nvarchar](250) NULL,
	[COLUMNB008] [nvarchar](250) NULL,
	[COLUMNB009] [nvarchar](250) NULL,
	[COLUMNB010] [nvarchar](250) NULL,
	[COLUMND001] [nvarchar](250) NULL,
	[COLUMND002] [nvarchar](250) NULL,
	[COLUMND003] [nvarchar](250) NULL,
	[COLUMND004] [nvarchar](250) NULL,
	[COLUMND005] [nvarchar](250) NULL,
	[COLUMND006] [nvarchar](250) NULL,
	[COLUMND007] [nvarchar](250) NULL,
	[COLUMND008] [nvarchar](250) NULL,
	[COLUMND009] [nvarchar](250) NULL,
	[COLUMND010] [nvarchar](250) NULL
) ON [PRIMARY]
GO
