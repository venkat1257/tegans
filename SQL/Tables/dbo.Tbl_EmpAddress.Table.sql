USE [Tegans]
GO
/****** Object:  Table [dbo].[Tbl_EmpAddress]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_EmpAddress](
	[ADID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Form_Id] [int] NULL,
	[Primary_point_of_contact] [bigint] NULL,
	[Addressee] [nvarchar](max) NULL,
	[Phone_No] [bigint] NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Zip] [int] NULL,
	[Country] [nvarchar](50) NULL,
	[Default_shipping] [nvarchar](max) NULL,
	[Home] [nvarchar](50) NULL,
	[Office] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tbl_EmpAddress] PRIMARY KEY CLUSTERED 
(
	[ADID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
