USE [Tegans]
GO
/****** Object:  Table [dbo].[Tbl_CenterMaster]    Script Date: 8/31/2020 6:37:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_CenterMaster](
	[Center ID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Operating unit] [nvarchar](50) NULL,
	[Subsidary] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tbl_CenterMaster] PRIMARY KEY CLUSTERED 
(
	[Center ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
