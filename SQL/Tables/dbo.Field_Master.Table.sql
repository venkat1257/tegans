USE [Tegans]
GO
/****** Object:  Table [dbo].[Field_Master]    Script Date: 8/31/2020 6:37:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Field_Master](
	[Field_Id] [int] IDENTITY(100,1) NOT NULL,
	[Field_Name] [nvarchar](50) NULL,
	[Label_Name] [nvarchar](50) NULL,
	[Action] [nvarchar](50) NULL,
	[Mandatory] [nvarchar](50) NULL,
	[Default_Value] [nvarchar](50) NULL,
	[Control_Type] [nvarchar](50) NULL,
	[Section_Type] [nvarchar](50) NULL,
	[Section_Name] [nvarchar](50) NULL,
	[Priority] [nvarchar](50) NULL,
	[Form_Id] [int] NULL,
	[Table_Id] [int] NULL,
	[Company_ID] [int] NULL,
	[Operating_Unit_ID] [int] NULL,
	[Account_Owner_ID] [int] NULL,
	[Classification_ID] [int] NULL,
	[Department_ID] [int] NULL,
	[LCreated_Date] [datetime] NULL,
	[Lmodified_Date] [datetime] NULL,
	[Created_By] [int] NULL,
	[GCreated_Date] [datetime] NULL,
	[Gmodified_Date] [datetime] NULL,
	[Modified_By] [int] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Field_Master] PRIMARY KEY CLUSTERED 
(
	[Field_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Field_Master]  WITH CHECK ADD  CONSTRAINT [FK_Field_Master_Form_Master] FOREIGN KEY([Form_Id])
REFERENCES [dbo].[Form_Master] ([F_Id])
GO
ALTER TABLE [dbo].[Field_Master] CHECK CONSTRAINT [FK_Field_Master_Form_Master]
GO
ALTER TABLE [dbo].[Field_Master]  WITH CHECK ADD  CONSTRAINT [FK_Field_Master_Table_Mapping] FOREIGN KEY([Table_Id])
REFERENCES [dbo].[Table_Mapping] ([Table_ID])
GO
ALTER TABLE [dbo].[Field_Master] CHECK CONSTRAINT [FK_Field_Master_Table_Mapping]
GO
