USE [Tegans]
GO
/****** Object:  Table [dbo].[Menu_Master]    Script Date: 8/31/2020 6:37:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu_Master](
	[MenuId] [int] IDENTITY(2368,1) NOT NULL,
	[CenterId] [int] NULL,
	[MenuName] [nvarchar](max) NULL,
	[ModuleId] [int] NULL,
	[Parent] [int] NULL,
	[PriorityLevel] [int] NULL,
	[IsEnable] [nvarchar](2) NULL,
	[IsFormApplicable] [nvarchar](2) NULL,
	[ScreenName] [nvarchar](max) NULL,
	[Order] [int] NULL,
	[Operatingunit] [nvarchar](50) NULL,
	[Subsidary] [nvarchar](50) NULL,
	[AppOwnerId] [int] NULL,
 CONSTRAINT [PK_Menu_Master] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
