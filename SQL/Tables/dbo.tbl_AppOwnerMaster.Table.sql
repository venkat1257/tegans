USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_AppOwnerMaster]    Script Date: 8/31/2020 6:37:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_AppOwnerMaster](
	[ApplicationOwnerId] [int] IDENTITY(3,1) NOT NULL,
	[AppOwnerUserName] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](50) NULL,
	[TypeOfBusines] [nvarchar](50) NULL,
	[DetailsNeedFor] [nvarchar](250) NULL,
	[EmailId] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [int] NULL,
	[Country] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tbl_AppOwnerMaster] PRIMARY KEY CLUSTERED 
(
	[ApplicationOwnerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
