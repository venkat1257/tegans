USE [Tegans]
GO
/****** Object:  Table [dbo].[CUSTOMER_RECEIVABLE_IMPORT]    Script Date: 8/31/2020 6:37:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER_RECEIVABLE_IMPORT](
	[CUSTOMER ID] [int] NULL,
	[CUSTOMER NAME] [nvarchar](250) NULL,
	[AMOUNT] [decimal](18, 2) NULL,
	[OPERATING UNIT] [int] NULL,
	[ACCOUNT OWNER] [int] NULL
) ON [PRIMARY]
GO
