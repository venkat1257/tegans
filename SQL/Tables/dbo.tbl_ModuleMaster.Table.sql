USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_ModuleMaster]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ModuleMaster](
	[Module_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Subsidary] [nvarchar](50) NULL,
	[Operating_unit] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_ModuleMaster] PRIMARY KEY CLUSTERED 
(
	[Module_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
