USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_BranchMaster]    Script Date: 8/31/2020 6:37:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_BranchMaster](
	[BranchId] [int] IDENTITY(3,1) NOT NULL,
	[BranchName] [nvarchar](50) NOT NULL,
	[BranchDesc] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_BranchMaster] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
