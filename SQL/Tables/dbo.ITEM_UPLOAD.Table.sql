USE [Tegans]
GO
/****** Object:  Table [dbo].[ITEM_UPLOAD]    Script Date: 8/31/2020 6:37:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITEM_UPLOAD](
	[ITEM_INTERNAL_ID] [int] NULL,
	[UPC] [nvarchar](250) NULL,
	[ITEM_NAME] [nvarchar](250) NULL,
	[UOM] [nvarchar](250) NULL,
	[LOT] [nvarchar](250) NULL,
	[QTY] [decimal](18, 2) NULL,
	[PRICE] [decimal](18, 2) NULL,
	[AMOUNT] [decimal](18, 2) NULL,
	[OPERATING_UNIT] [int] NULL,
	[ACCOUNT_OWNER] [int] NULL,
	[LOCATION] [nvarchar](250) NULL
) ON [PRIMARY]
GO
