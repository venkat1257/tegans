USE [Tegans]
GO
/****** Object:  Table [dbo].[Column_Mapping]    Script Date: 8/31/2020 6:37:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Column_Mapping](
	[Column_ID] [int] IDENTITY(200000000,1) NOT NULL,
	[Column_Name] [nvarchar](50) NULL,
	[Alias_Column_Name] [nvarchar](50) NULL,
	[Data_Type] [nvarchar](50) NULL,
	[IsNullable] [bit] NULL,
	[Size] [nvarchar](50) NULL,
	[Table_ID] [int] NULL,
	[Company_ID] [int] NULL,
	[Operating_Unit_ID] [int] NULL,
	[Account_Owner_ID] [int] NULL,
	[Classification_ID] [int] NULL,
	[Department_ID] [int] NULL,
	[LCreated_Date] [datetime] NULL,
	[Lmodified_Date] [datetime] NULL,
	[Created_By] [int] NULL,
	[GCreated_Date] [datetime] NULL,
	[Gmodified_Date] [datetime] NULL,
	[Modified_By] [int] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Column_Mapping] PRIMARY KEY CLUSTERED 
(
	[Column_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Column_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Column_Mapping_Table_Mapping] FOREIGN KEY([Table_ID])
REFERENCES [dbo].[Table_Mapping] ([Table_ID])
GO
ALTER TABLE [dbo].[Column_Mapping] CHECK CONSTRAINT [FK_Column_Mapping_Table_Mapping]
GO
