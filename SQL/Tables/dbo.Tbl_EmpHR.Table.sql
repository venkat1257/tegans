USE [Tegans]
GO
/****** Object:  Table [dbo].[Tbl_EmpHR]    Script Date: 8/31/2020 6:37:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_EmpHR](
	[EHID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[Form_Id] [int] NULL,
	[Social_Security_Number] [nvarchar](50) NULL,
	[Number_for_any_Government_authorized_document] [nvarchar](50) NULL,
	[Type_of_Authorized_Document] [nvarchar](50) NULL,
	[Supervisior] [nvarchar](50) NULL,
	[Expense_Limit] [nvarchar](50) NULL,
	[Expense_Approver] [nvarchar](50) NULL,
	[Expense_Approval_Limit] [nvarchar](50) NULL,
	[Purchase_limit] [nvarchar](50) NULL,
	[Purchase_Approver] [nvarchar](50) NULL,
	[Purchase_approval_limit] [nvarchar](50) NULL,
	[Time_approver] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[Birth_Date] [datetime] NULL,
	[Hire_date] [datetime] NULL,
	[Release_Date] [datetime] NULL,
	[Last_Review_Date] [datetime] NULL,
	[Next_Review_Date] [datetime] NULL,
	[Job_Title] [nvarchar](max) NULL,
	[Employee_Status] [nvarchar](50) NULL,
	[Job_Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Tbl_EmpHR] PRIMARY KEY CLUSTERED 
(
	[EHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
