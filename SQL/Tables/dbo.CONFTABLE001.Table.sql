USE [Tegans]
GO
/****** Object:  Table [dbo].[CONFTABLE001]    Script Date: 8/31/2020 6:37:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONFTABLE001](
	[COLUMN01] [int] NOT NULL,
	[COLUMN02] [nvarchar](max) NULL,
	[COLUMN03] [nvarchar](250) NULL,
	[COLUMN04] [nvarchar](250) NULL,
	[COLUMN05] [nvarchar](250) NULL,
	[COLUMNA01] [nvarchar](250) NULL,
	[COLUMNA02] [nvarchar](250) NULL,
	[COLUMNA03] [nvarchar](250) NULL,
	[COLUMNA04] [nvarchar](250) NULL,
	[COLUMNA05] [nvarchar](250) NULL,
	[COLUMNA06] [nvarchar](250) NULL,
	[COLUMNA07] [nvarchar](250) NULL,
	[COLUMNA08] [nvarchar](250) NULL,
	[COLUMNA09] [nvarchar](250) NULL,
	[COLUMNA10] [nvarchar](250) NULL,
	[COLUMNA11] [nvarchar](250) NULL,
	[COLUMNA12] [nvarchar](250) NULL,
	[COLUMNA13] [nvarchar](250) NULL,
	[COLUMNB01] [nvarchar](250) NULL,
	[COLUMNB02] [nvarchar](250) NULL,
	[COLUMNB03] [nvarchar](250) NULL,
	[COLUMNB04] [nvarchar](250) NULL,
	[COLUMNB05] [nvarchar](250) NULL,
	[COLUMNB06] [nvarchar](250) NULL,
	[COLUMNB07] [nvarchar](250) NULL,
	[COLUMNB08] [nvarchar](250) NULL,
	[COLUMNB09] [nvarchar](250) NULL,
	[COLUMNB10] [nvarchar](250) NULL,
	[COLUMNB11] [nvarchar](250) NULL,
	[COLUMNB12] [nvarchar](250) NULL,
	[COLUMND01] [nvarchar](250) NULL,
	[COLUMND02] [nvarchar](250) NULL,
	[COLUMND03] [nvarchar](250) NULL,
	[COLUMND04] [nvarchar](250) NULL,
	[COLUMND05] [nvarchar](250) NULL,
	[COLUMND06] [nvarchar](250) NULL,
	[COLUMND07] [nvarchar](250) NULL,
	[COLUMND08] [nvarchar](250) NULL,
	[COLUMND09] [nvarchar](250) NULL,
	[COLUMND10] [nvarchar](250) NULL,
 CONSTRAINT [PK_CONFTABLE001] PRIMARY KEY CLUSTERED 
(
	[COLUMN01] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [INDEXES]
) ON [INDEXES] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CONFTABLE001] ADD  CONSTRAINT [DF_CONFTABLE001_COLUMNA06]  DEFAULT (getdate()) FOR [COLUMNA06]
GO
