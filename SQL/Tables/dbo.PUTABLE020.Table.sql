USE [Tegans]
GO
/****** Object:  Table [dbo].[PUTABLE020]    Script Date: 8/31/2020 6:37:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PUTABLE020](
	[COLUMN01] [int] NULL,
	[COLUMN02] [nvarchar](250) NULL,
	[COLUMN03] [int] NULL,
	[COLUMN04] [nvarchar](250) NULL,
	[COLUMN05] [nvarchar](250) NULL,
	[COLUMN06] [datetime] NULL,
	[COLUMN07] [nvarchar](250) NULL,
	[COLUMN08] [nvarchar](250) NULL,
	[COLUMN09] [nvarchar](250) NULL,
	[COLUMN10] [nvarchar](250) NULL,
	[COLUMN11] [nvarchar](250) NULL,
	[COLUMN12] [nvarchar](250) NULL,
	[COLUMN13] [nvarchar](250) NULL,
	[COLUMNA01] [int] NULL,
	[COLUMNA02] [int] NULL,
	[COLUMNA03] [int] NULL,
	[COLUMNA04] [int] NULL,
	[COLUMNA05] [int] NULL,
	[COLUMNA06] [datetime] NULL,
	[COLUMNA07] [datetime] NULL,
	[COLUMNA08] [int] NULL,
	[COLUMNA09] [datetime] NULL,
	[COLUMNA10] [datetime] NULL,
	[COLUMNA11] [int] NULL,
	[COLUMNA12] [bit] NULL,
	[COLUMNA13] [bit] NULL,
	[COLUMNB01] [nvarchar](250) NULL,
	[COLUMNB02] [nvarchar](250) NULL,
	[COLUMNB03] [nvarchar](250) NULL,
	[COLUMNB04] [nvarchar](250) NULL,
	[COLUMNB05] [nvarchar](250) NULL,
	[COLUMNB06] [nvarchar](250) NULL,
	[COLUMNB07] [datetime] NULL,
	[COLUMNB08] [datetime] NULL,
	[COLUMNB09] [decimal](18, 0) NULL,
	[COLUMNB10] [decimal](18, 0) NULL,
	[COLUMNB11] [int] NULL,
	[COLUMNB12] [int] NULL,
	[COLUMND01] [nvarchar](250) NULL,
	[COLUMND02] [nvarchar](250) NULL,
	[COLUMND03] [nvarchar](250) NULL,
	[COLUMND04] [nvarchar](250) NULL,
	[COLUMND05] [nvarchar](250) NULL,
	[COLUMND06] [nvarchar](250) NULL,
	[COLUMND07] [datetime] NULL,
	[COLUMND08] [datetime] NULL,
	[COLUMND09] [decimal](18, 0) NULL,
	[COLUMND10] [int] NULL,
	[COLUMN20] [nvarchar](250) NULL
) ON [PRIMARY]
GO
