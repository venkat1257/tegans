USE [Tegans]
GO
/****** Object:  Table [dbo].[INTABLE002]    Script Date: 8/31/2020 6:37:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INTABLE002](
	[COLUMN001] [int] NULL,
	[COLUMN002] [nvarchar](250) NULL,
	[COLUMN003] [nvarchar](250) NULL,
	[COLUMN004] [nvarchar](250) NULL,
	[COLUMN005] [nvarchar](250) NULL,
	[COLUMN006] [nvarchar](250) NULL,
	[COLUMN007] [decimal](18, 0) NULL,
	[COLUMN008] [decimal](18, 0) NULL,
	[COLUMN009] [nvarchar](250) NULL,
	[COLUMN010] [nvarchar](250) NULL,
	[COLUMNB001] [date] NULL,
	[COLUMNB002] [date] NULL,
	[COLUMNB003] [money] NULL,
	[COLUMNB004] [bit] NULL,
	[COLUMNB005] [int] NULL,
	[COLUMNB006] [int] NULL,
	[COLUMNB007] [int] NULL,
	[COLUMNB008] [int] NULL,
	[COLUMNB009] [int] NULL,
	[COLUMNB010] [int] NULL,
	[COLUMND001] [int] NULL,
	[COLUMND002] [int] NULL,
	[COLUMND003] [int] NULL,
	[COLUMND004] [int] NULL,
	[COLUMND005] [int] NULL,
	[COLUMND006] [int] NULL,
	[COLUMND007] [int] NULL,
	[COLUMND008] [int] NULL,
	[COLUMND009] [int] NULL,
	[COLUMND010] [int] NULL
) ON [PRIMARY]
GO
