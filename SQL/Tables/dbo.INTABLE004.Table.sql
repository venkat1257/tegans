USE [Tegans]
GO
/****** Object:  Table [dbo].[INTABLE004]    Script Date: 8/31/2020 6:37:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INTABLE004](
	[COLUMN001] [int] NULL,
	[COLUMN002] [nvarchar](250) NULL,
	[COLUMN003] [nvarchar](250) NULL,
	[COLUMN004] [money] NULL,
	[COLUMN005] [int] NULL,
	[COLUMN006] [int] NULL,
	[COLUMN007] [int] NULL,
	[COLUMN008] [int] NULL,
	[COLUMN009] [int] NULL,
	[COLUMN010] [int] NULL,
	[COLUMNB001] [nvarchar](250) NULL,
	[COLUMNB002] [nvarchar](250) NULL,
	[COLUMNB003] [nvarchar](250) NULL,
	[COLUMNB004] [nvarchar](250) NULL,
	[COLUMNB005] [nvarchar](250) NULL,
	[COLUMNB006] [nvarchar](250) NULL,
	[COLUMNB007] [nvarchar](250) NULL,
	[COLUMNB008] [nvarchar](250) NULL,
	[COLUMNB009] [nvarchar](250) NULL,
	[COLUMNB010] [nvarchar](250) NULL,
	[COLUMND001] [nvarchar](250) NULL,
	[COLUMND002] [nvarchar](250) NULL,
	[COLUMND003] [nvarchar](250) NULL,
	[COLUMND004] [nvarchar](250) NULL,
	[COLUMND005] [nvarchar](250) NULL,
	[COLUMND006] [nvarchar](250) NULL,
	[COLUMND007] [nvarchar](250) NULL,
	[COLUMND008] [nvarchar](250) NULL,
	[COLUMND009] [nvarchar](250) NULL,
	[COLUMND010] [nvarchar](250) NULL
) ON [PRIMARY]
GO
