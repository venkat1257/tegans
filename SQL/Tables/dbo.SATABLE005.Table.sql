USE [Tegans]
GO
/****** Object:  Table [dbo].[SATABLE005]    Script Date: 8/31/2020 6:37:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SATABLE005](
	[COLUMN01] [int] IDENTITY(1,1) NOT NULL,
	[COLUMN02] [nvarchar](250) NOT NULL,
	[COLUMN03] [nvarchar](250) NULL,
	[COLUMN04] [nvarchar](250) NULL,
	[COLUMN05] [nvarchar](250) NULL,
	[COLUMN06] [date] NULL,
	[COLUMN07] [bit] NULL,
	[COLUMN08] [date] NULL,
	[COLUMN09] [nvarchar](250) NULL,
	[COLUMN10] [nvarchar](250) NULL,
	[COLUMN11] [nvarchar](250) NULL,
	[COLUMN12] [decimal](18, 2) NULL,
	[COLUMN13] [decimal](18, 0) NULL,
	[COLUMN14] [decimal](18, 2) NULL,
	[COLUMN15] [decimal](18, 2) NULL,
	[COLUMN16] [nvarchar](250) NULL,
	[COLUMN17] [bit] NULL,
	[COLUMN18] [bit] NULL,
	[COLUMN19] [bit] NULL,
	[COLUMN20] [nvarchar](250) NULL,
	[COLUMN21] [nvarchar](250) NULL,
	[COLUMN22] [nvarchar](250) NULL,
	[COLUMN23] [nvarchar](250) NULL,
	[COLUMN24] [nvarchar](250) NULL,
	[COLUMN25] [nvarchar](250) NULL,
	[COLUMN26] [nvarchar](250) NULL,
	[COLUMN27] [nvarchar](250) NULL,
	[COLUMN28] [nvarchar](250) NULL,
	[COLUMN29] [nvarchar](250) NULL,
	[COLUMN30] [nvarchar](250) NULL,
	[COLUMN31] [nvarchar](250) NULL,
	[COLUMN32] [decimal](18, 2) NULL,
	[COLUMN33] [nvarchar](250) NULL,
	[COLUMN34] [nvarchar](250) NULL,
	[COLUMNA01] [int] NULL,
	[COLUMNA02] [int] NULL,
	[COLUMNA03] [int] NULL,
	[COLUMNA04] [int] NULL,
	[COLUMNA05] [int] NULL,
	[COLUMNA06] [datetime] NULL,
	[COLUMNA07] [datetime] NULL,
	[COLUMNA08] [int] NULL,
	[COLUMNA09] [datetime] NULL,
	[COLUMNA10] [datetime] NULL,
	[COLUMNA11] [int] NULL,
	[COLUMNA12] [bit] NULL,
	[COLUMNA13] [bit] NULL,
	[COLUMNB01] [nvarchar](250) NULL,
	[COLUMNB02] [nvarchar](250) NULL,
	[COLUMNB03] [bigint] NULL,
	[COLUMNB04] [nvarchar](250) NULL,
	[COLUMNB05] [decimal](18, 0) NULL,
	[COLUMNB06] [decimal](18, 0) NULL,
	[COLUMNB07] [int] NULL,
	[COLUMNB08] [nvarchar](250) NULL,
	[COLUMNB09] [decimal](18, 0) NULL,
	[COLUMNB10] [decimal](18, 0) NULL,
	[COLUMNB11] [int] NULL,
	[COLUMNB12] [int] NULL,
	[COLUMND01] [nvarchar](250) NULL,
	[COLUMND02] [nvarchar](250) NULL,
	[COLUMND03] [nvarchar](250) NULL,
	[COLUMND04] [nvarchar](250) NULL,
	[COLUMND05] [nvarchar](250) NULL,
	[COLUMND06] [nvarchar](250) NULL,
	[COLUMND07] [datetime] NULL,
	[COLUMND08] [datetime] NULL,
	[COLUMND09] [decimal](18, 2) NULL,
	[COLUMND10] [int] NULL,
	[COLUMN35] [nvarchar](250) NULL,
	[COLUMN36] [nvarchar](250) NULL,
	[COLUMN37] [nvarchar](250) NULL,
	[COLUMN38] [nvarchar](250) NULL,
	[COLUMN39] [nvarchar](250) NULL,
	[COLUMN40] [nvarchar](250) NULL,
	[COLUMN41] [decimal](18, 2) NULL,
	[COLUMN42] [decimal](18, 2) NULL,
	[COLUMN43] [nvarchar](250) NULL,
	[COLUMN44] [nvarchar](250) NULL,
	[COLUMN45] [bigint] NULL,
	[COLUMN46] [nvarchar](250) NULL,
	[COLUMN47] [decimal](18, 2) NULL,
	[COLUMN48] [nvarchar](250) NULL,
	[COLUMN49] [nvarchar](250) NULL,
	[COLUMN50] [int] NULL,
	[COLUMN51] [nvarchar](250) NULL,
	[COLUMN52] [bit] NULL,
	[COLUMN53] [int] NULL,
	[COLUMN54] [nvarchar](250) NULL,
	[COLUMN59] [int] NULL,
	[COLUMN60] [decimal](18, 2) NULL,
	[COLUMN67] [nvarchar](250) NULL,
	[COLUMN68] [int] NULL,
	[COLUMN69] [int] NULL,
	[COLUMN70] [bit] NULL,
	[COLUMN71] [decimal](18, 2) NULL,
 CONSTRAINT [PK__SATABLE0__8723CF0FAEC02868] PRIMARY KEY CLUSTERED 
(
	[COLUMN02] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
