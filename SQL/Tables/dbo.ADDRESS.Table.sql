USE [Tegans]
GO
/****** Object:  Table [dbo].[ADDRESS]    Script Date: 8/31/2020 6:37:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ADDRESS](
	[ADDRESS 2] [varchar](50) NULL,
	[ADDR3] [varchar](50) NULL,
	[ADDR] [varchar](50) NULL,
	[4] [varchar](50) NULL,
	[ADDR5] [varchar](50) NULL,
	[Column 0] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
