USE [Tegans]
GO
/****** Object:  Table [dbo].[tbl_AdminSecurityAnswer]    Script Date: 8/31/2020 6:37:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_AdminSecurityAnswer](
	[AID] [int] IDENTITY(7,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[SeqQue1] [nvarchar](max) NOT NULL,
	[SecAns1] [nvarchar](100) NOT NULL,
	[SeqQue2] [nvarchar](max) NOT NULL,
	[SecAns2] [nvarchar](100) NOT NULL,
	[SeqQue3] [nvarchar](max) NOT NULL,
	[SecAns3] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_tbl_AdminSecurityAnswer] PRIMARY KEY CLUSTERED 
(
	[AID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
