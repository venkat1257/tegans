USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetOutStandingAmount]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetOutStandingAmount]
(
@PayeeType  nvarchar(250)= null)
as 
begin
if(@PayeeType='Vendor')
begin
if ((select isnull(COLUMN13,0)  from PUTABLE016 where COLUMN06='JOURNAL ENTRY' and COLUMN08=4008 and COLUMN02=(select max(COLUMN02) from PUTABLE016 where COLUMN06='JOURNAL ENTRY'))=0)
begin
select  COLUMN14 totamt,COLUMN14 dueamt  from PUTABLE016  where COLUMN06='JOURNAL ENTRY' and COLUMN08=4008 and COLUMN02=(select max(COLUMN02) from PUTABLE016 where COLUMN06='JOURNAL ENTRY')
end
else
begin
select  COLUMN12 totamt,COLUMN14 dueamt  from PUTABLE016  where COLUMN06='JOURNAL ENTRY' and COLUMN02=(select max(COLUMN02) from PUTABLE016 where COLUMN06='JOURNAL ENTRY')
end
end
else
begin
if ((select  isnull(COLUMN11,0)  from PUTABLE018 where COLUMN06='JOURNAL ENTRY' and COLUMN16=4008 and COLUMN02=(select max(COLUMN02) from PUTABLE018 where COLUMN06='JOURNAL ENTRY'))=0)
begin
select  COLUMN12 totamt,COLUMN12 dueamt  from PUTABLE018  where COLUMN06='JOURNAL ENTRY' and COLUMN16=4008 and COLUMN02=(select max(COLUMN02) from PUTABLE018 where COLUMN06='JOURNAL ENTRY')
end
else
begin
select  COLUMN09 totamt,COLUMN12  dueamt  from PUTABLE018  where COLUMN06='JOURNAL ENTRY' and COLUMN02=(select max(COLUMN02) from PUTABLE018 where COLUMN06='JOURNAL ENTRY')
end
end
end














GO
