USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Proc_RedisCacheStatus]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_Proc_RedisCacheStatus]
(
@Formid  nvarchar(250) = null,@TblID  nvarchar(250) = null,@OPUnit nvarchar(250) = null,@AcOwner nvarchar(250) = null,
@Eid nvarchar(250) = null,@Type nvarchar(250) = null
)
AS
BEGIN
	if(@Type='Update')
	begin
    if exists(SELECT COLUMN02 from CONTABLE033 where COLUMN03=@Formid and COLUMN04=@TblID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
	begin
	UPDATE CONTABLE033 set COLUMN06='1',COLUMN07=NULL,COLUMNA07=GETDATE(),COLUMNA08=@Eid where COLUMN03=@Formid and COLUMN04=@TblID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
	end
	else
	begin
	declare @id int 
	set @id=(SELECT MAX(isnull(COLUMN02,0)) FROM CONTABLE033)
	set @id=((iif(isnull(cast(@id as nvarchar(250)),'')='',999,@id))+1)
	INSERT INTO CONTABLE033(COLUMN02,COLUMN03,COLUMN04,COLUMN06,COLUMN07,COLUMNA02,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA13) 
	VALUES(@id,@Formid,@TblID,'1',NULL,@OPUnit,@AcOwner,GETDATE(),GETDATE(),@Eid,0) 
	end
	end
	else if(@Type='Select')
	begin	
	if exists(select c.COLUMN02 from CONTABLE006 f inner join CONTABLE033 c on 
	c.COLUMN04=f.COLUMN15 and f.COLUMN03 not in(((SELECT ListValue FROM dbo.FN_ListToTable(',',c.COLUMN07) s))) and c.COLUMN06=1 and c.COLUMNA03=@AcOwner and c.COLUMNA13=0
	where f.COLUMN03=@Formid and f.COLUMN07 != 'N')
	begin
	UPDATE C33 set COLUMN07=(CONCAT(COLUMN07,iif(isnull(COLUMN07,'')='','',','),@Formid)),COLUMNA07=GETDATE(),COLUMNA08=@Eid From CONTABLE033 C33
	inner join (select c.COLUMN02,c.COLUMNA03 from CONTABLE006 f inner join CONTABLE033 c on 
	c.COLUMN04=f.COLUMN15 and f.COLUMN03 not in(((SELECT ListValue FROM dbo.FN_ListToTable(',',c.COLUMN07) s))) and c.COLUMN06=1 and c.COLUMNA03=@AcOwner and c.COLUMNA13=0
	where f.COLUMN03=@Formid and f.COLUMN07 != 'N') a on a.COLUMN02=C33.COLUMN02 and a.COLUMNA03=C33.COLUMNA03
	where C33.COLUMNA03=@AcOwner and isnull(C33.COLUMNA13,0)=0
	end
	END
END 













GO
