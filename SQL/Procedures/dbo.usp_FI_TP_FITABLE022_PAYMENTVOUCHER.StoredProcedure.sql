USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE022_PAYMENTVOUCHER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE022_PAYMENTVOUCHER]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,   @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)
AS
BEGIN
 begin try
 DECLARE @dt date
 DECLARE @AC int
 DECLARE @PM int
 DECLARE @ID varchar(250)
 declare @ltaxamt decimal(18,2)
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE022_SequenceNo
 --set @COLUMN09= (select max(column01) from FITABLE020)
 set @dt= (select column05 from FITABLE020 where COLUMN01=@COLUMN09)
 set @ID= (select column04 from FITABLE020 where COLUMN01=@COLUMN09)
 --//EMPHCS1505	Adding Bank Fiels in Payment voucher and receipt voucher at header level by RAJ.Jr 
 --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
 declare @HBank nvarchar(250)
 set @HBank= (select column06 from FITABLE020 where COLUMN01=@COLUMN09)
 set @COLUMN11= (@HBank)
 set @COLUMN10= (select COLUMN22 from FITABLE020 where COLUMN01=@COLUMN09)
 set @COLUMN12= (select COLUMN23 from FITABLE020 where COLUMN01=@COLUMN09)
insert into FITABLE022
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13, 
   COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, @COLUMN10,@COLUMN11,@COLUMN12, @COLUMN13,
   @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
 update FITABLE020 set  COLUMN16=(select column04 from contable025 where column02=64) where column02=@COLUMN02
  --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
 declare @balance decimal(18,2), @TAXRATE decimal(18,2), @pid nvarchar(250)=null, @eid nvarchar(250)=null, @internalid nvarchar(250)=null,@Payee nvarchar(250)
 declare @liabid nvarchar(250)=null, @Tax nvarchar(250)=null, @TaxAG nvarchar(250)=null, @TaxName nvarchar(250)=null
 set @Tax=( @COLUMN14)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 set @TAXRATE= (select isnull(COLUMN17,0) from MATABLE013 where COLUMN02=@Tax)
 set @internalid= (select column01 from FITABLE022 where COLUMN02=@COLUMN02)
 --set @eid=(select top 1 isnull(column02,0) from FITABLE030  order by column02 desc)
 --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
declare @PU19 nvarchar(250), @TotAmount decimal(18,2),@Bankbal decimal(18,2) ,@AccType nvarchar(250)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
declare @Hinternalid nvarchar(250)=null,@Voucher# nvarchar(250),@PayeeType nvarchar(250)
set @pid=((select isnull(max(column02),999) from PUTABLE016)+1)
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @Hinternalid= (select max(column01) from FITABLE020 where COLUMN01=@COLUMN09)
set @TotAmount=(select ISNULL(COLUMN10,0) from FITABLE020 where COLUMN01=@COLUMN09)
set @Bankbal=(select ISNULL(COLUMN10,0) from FITABLE001 where COLUMN02=@COLUMN11 and COLUMNA03=@COLUMNA03)
set @TotAmount=(select ISNULL(COLUMN10,0) from FITABLE020 where COLUMN01=@COLUMN09)
 set @Payee=(select COLUMN07 from FITABLE020 where COLUMN01=@COLUMN09)
 set @PayeeType=(select COLUMN11 from FITABLE020 where COLUMN01=@COLUMN09)
set @AccType=(select COLUMN04 from FITABLE001 where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03)
if(@Tax!='' and (@Tax!=1000 or @Tax!='1000'))
begin
set @TaxName=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Tax and COLUMNA03=@COLUMNA03)
end
set @Voucher#=(select column04 from FITABLE020 where COLUMN01=@COLUMN09)
update FITABLE044 set  COLUMN18=(select column02 from FITABLE020 where COLUMN01=@COLUMN09),COLUMN19=(@COLUMN02) where column02=@COLUMN12 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
if((@COLUMN12!='' or @COLUMN12!=null) and @COLUMN10=22273)
begin
update FITABLE044 set COLUMN15=22629 where COLUMN02=@COLUMN12 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
 --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
DECLARE @TYPE INT,@NUMBER INT, @TYPEID NVARCHAR(250), @DATE date, @ACCNAME NVARCHAR(250),@Project NVARCHAR(250)
DECLARE @ACCOUNTTYPE NVARCHAR(250),@Increased NVARCHAR(250),@Decreased NVARCHAR(250),@Remainbal decimal(18,2),@Totbal decimal(18,2)
set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
set @DATE=(@dt)
set @TYPEID=(@Voucher#)
set @NUMBER=(@Hinternalid)
set @Project=(@COLUMN08)
set @Totbal=(cast(isnull(@COLUMN05,0) as decimal(18,2)))--+cast(isnull(@COLUMN06,0) as decimal(18,2)))
set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=@TYPE)
declare @Projectactualcost decimal(18,2)
if(cast(@Totbal as nvarchar)!='' and cast(isnull(@Totbal,0) as decimal(18,2))!=0)
begin
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@Totbal as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
end
 --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
 
IF(@TYPE NOT IN(0))
BEGIN
	set @Increased=(0)
	set @Decreased=(@COLUMN05)
	IF(@TYPE=22383)
		BEGIN
			if(isnull(@ACCNAME,'') like 'INPUT%')
				BEGIN
					set @Increased=(@COLUMN05)
					set @Decreased=(0)
				END
		END
	if(@TYPE = '22406')
		begin 
			set @Increased=(@COLUMN05)
			set @Decreased=(0)
		end
END
ELSE
BEGIN
	set @Increased=(@COLUMN05)
	set @Decreased=(0)
END
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TYPE, @COLUMN11 = @COLUMN03,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @Increased,       @COLUMN15 = @Decreased,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

--Accounts payable
if(@COLUMN03=1117)
begin
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE016)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN13,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
	values(@ID,2000,@dt,@internalid,'PAYMENT VOUCHER',@Payee,@COLUMN11,@TYPEID,'OPEN',(cast(@COLUMN05 as decimal(18,2))),(cast(@COLUMN05 as decimal(18,2))),@COLUMN04,@COLUMNA02, @COLUMNA03,@COLUMN08)
end
--Accounts payable
else if(@TYPE=22263)
begin
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE016)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN13,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
	values(@ID,@COLUMN03,@dt,@internalid,'PAYMENT VOUCHER',@Payee,@COLUMN11,@TYPEID,'OPEN',(cast(@COLUMN05 as decimal(18,2))),(cast(@COLUMN05 as decimal(18,2))),@COLUMN04,@COLUMNA02, @COLUMNA03,@COLUMN08)
end

--Bank
ELSE IF(@TYPE=22266)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE019)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into PUTABLE019 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,  COLUMN20,  COLUMNA02, COLUMNA03)
	values(
	ISNULL(@ID,10001),  @COLUMN03,  @DATE,  @TYPEID,  'PAYMENT VOUCHER',  @Payee,  @NUMBER,  @COLUMN04, 0,@Totbal, 
	CAST(@Remainbal AS DECIMAL(18,2)),@Project,  @COLUMNA02, @COLUMNA03) 
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@COLUMN03 and COLUMNA03=@COLUMNA03
END

--Cash
ELSE IF(@TYPE=22409)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@ID,1000),@COLUMN03,  @DATE,  @TYPEID,  'PAYMENT VOUCHER',@PayeeType,  @Payee,  @NUMBER,  @COLUMN04,0,@Totbal,
	  CAST(@Remainbal AS DECIMAL(18,2)),@Project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@COLUMN03 and COLUMNA03=@COLUMNA03
END

--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE017)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into PUTABLE017 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	COLUMN12,  COLUMN13,  COLUMN14, COLUMNA02, COLUMNA03)
	values( 
	--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @COLUMN03,   @DATE,  @NUMBER,  'PAYMENT VOUCHER',  @Payee,  @COLUMN03,  @COLUMN04, @Totbal,0,  
	@Remainbal,  @COLUMNA02, @Project, @COLUMNA02, @COLUMNA03)
END

--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM PUTABLE018)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	insert into PUTABLE018 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,    COLUMN09, COLUMN11,   
	COLUMN12,   COLUMN18,COLUMNA02, COLUMNA03 )
	values( 
	ISNULL(@ID,10001),  @COLUMN03,  @DATE,  @TYPEID,  'PAYMENT VOUCHER',  @Payee,  @COLUMN04,  @Totbal,0,
	@Remainbal, @Project,   @COLUMNA02, @COLUMNA03)
END

--Equity
ELSE IF(@TYPE=22330)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE029)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE029 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN07,    COLUMN06,    COLUMN08,   COLUMN09,  COLUMN10, 
	COLUMN11,COLUMN12,COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN03,  @Payee,  @COLUMN04, 0, @Totbal,  
	@TYPEID, @Project, @COLUMNA02, @COLUMNA03)
END

--Expense
ELSE IF(@TYPE=22344)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE036)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	Insert into FITABLE036 
	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,
	COLUMN10,COLUMN11,COLUMNA02, COLUMNA03) 
	values(	
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	isnull(@ID,10001),'PAYMENT VOUCHER',@DATE,@NUMBER,@Payee,@Totbal,0, @TYPEID,
	@COLUMN03,@Project,@COLUMNA02, @COLUMNA03 )
END

--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE026)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	declare @chk nvarchar(250)
	set @chk=(select 1 from FITABLE001 where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMN04 like 'INPUT%')
	if(@chk=1)
	begin
	insert into FITABLE026 
	(COLUMN02,  COLUMN03, COLUMN04,   COLUMN05,  COLUMN06, COLUMN08,COLUMN07,  COLUMN11,  COLUMN12,  COLUMN09,COLUMN17,COLUMN19,
	COLUMNA02, COLUMNA03 )
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	values( ISNULL(@ID,10001), @DATE,'PAYMENT VOUCHER',@NUMBER,  @Payee,  @COLUMN03, @COLUMN04,@Totbal,0,  @TYPEID,@AccType,@Project,
	@COLUMNA02, @COLUMNA03)
	end
	else
	begin
	insert into FITABLE026 
	(COLUMN02,  COLUMN03, COLUMN04,   COLUMN05,  COLUMN06, COLUMN08,COLUMN07,  COLUMN11,  COLUMN12,  COLUMN09,COLUMN17,COLUMN19,
	COLUMNA02, COLUMNA03 )
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	values( ISNULL(@ID,10001), @DATE,'PAYMENT VOUCHER',@NUMBER,  @Payee,  @COLUMN03, @COLUMN04,0,@Totbal,  @TYPEID,@AccType,@Project,
	@COLUMNA02, @COLUMNA03)
	end
END

--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE034)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee, @Totbal, 0,@TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE028)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE028 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,
	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @COLUMN03,  @DATE,  'PAYMENT VOUCHER',  @TYPEID,  @Payee,  @COLUMN04, @Totbal, 0,  @Project,
	@COLUMNA02, @COLUMNA03)
END

--Income
ELSE IF(@TYPE=22405)
BEGIN
	set @PM=@COLUMN10
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE025)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	SET @COLUMN10=(SELECT MAX(ISNULL(COLUMN10,0)) FROM FITABLE025 WHERE COLUMN08=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @COLUMN10=(CAST(ISNULL(@COLUMN10,0) AS DECIMAL(18,2))-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE025 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN11,  
	 COLUMN10, COLUMN12, COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @DATE,  'PAYMENT VOUCHER',  @NUMBER,  @Payee,  @COLUMN04,  @COLUMN03,0,  @Totbal,
	 @COLUMN10,@Project,@COLUMNA02, @COLUMNA03)
END

--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE025)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	SET @COLUMN10=(SELECT MAX(ISNULL(COLUMN10,0)) FROM FITABLE025 WHERE COLUMN08=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @COLUMN10=(CAST(ISNULL(@COLUMN10,0) AS DECIMAL(18,2))-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE025 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN11,  
	 COLUMN10, COLUMN12,  	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @DATE,  'PAYMENT VOUCHER',  @NUMBER,  @Payee,  @COLUMN04,  @COLUMN03,@Totbal, 0,
	@COLUMN10,@Project,	@COLUMNA02, @COLUMNA03)
END

--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE035)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE035 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,
	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee, @Totbal,0,  @TYPEID, @COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE036)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE036 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10,  COLUMN11,  
	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @NUMBER,  @Payee,@Totbal,0,  @TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Non Current assets
ELSE IF(@TYPE=22410)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE060)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE060 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee, @Totbal, 0,@TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Non Current Liabilities
ELSE IF(@TYPE=22411)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE063)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE063 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee,  0,@Totbal,@TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END
if(@PM=22627)
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PDC ISSUED',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22383', @COLUMN11 = '1117',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
	insert into FITABLE026 
	(COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN11,    COLUMN09,COLUMN17,COLUMN07,COLUMN08,
	COLUMN14,COLUMN19,COLUMNA02, COLUMNA03 	)
	values( 
	@liabid,  'PDC ISSUED',  @dt,  @internalid,  @Payee,  @Totbal,    @Voucher#,@AccType,@COLUMN04,'1117',
	@Totbal,@COLUMN08,@COLUMNA02, @COLUMNA03)
end
else
begin
 set @PU19=((select isnull(max(column02),999) from PUTABLE019)+1)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN11 AND COLUMNA03=@COLUMNA03)
--Cash
IF(@AccountType=22409)
BEGIN
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN11,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN17,       @COLUMN15 = 0,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @PU19=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN11 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@PU19,1000),@COLUMN11,@dt,@Voucher#,  'PAYMENT VOUCHER',@PayeeType,  @Payee,  @internalid,  @COLUMN04,(cast(@COLUMN17 as decimal(18,2))),
	 0,(cast(@Bankbal as decimal(18,2))-(cast(@COLUMN17 as decimal(18,2)))),@Project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@COLUMN11 and COLUMNA03=@COLUMNA03
END
else
begin
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN11,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN17,       @COLUMN15 = 0,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMNA02, COLUMNA03,COLUMN20)
 values(@PU19,@COLUMN11,@dt,@Voucher#,'PAYMENT VOUCHER',@Payee,@internalid,(cast(@COLUMN17 as decimal(18,2))),(cast(@Bankbal as decimal(18,2))-(cast(@COLUMN17 as decimal(18,2)))),@COLUMN12,@COLUMNA02, @COLUMNA03,@COLUMN08)
 update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))-cast(@COLUMN17 as decimal(18,2))) where column02=@COLUMN11 and COLUMNA03=@COLUMNA03
 end
end

--set @balance=(select top 1 isnull(column12,0) from FITABLE030  order by column02 desc)
--set @eid=(cast((select max(isnull(column02,1000)) from FITABLE030) as int)+1)
--EMPHCS1574	Tax changes in PaymentVoucher And ReceiptVoucher By RAJ.Jr
declare @TaxString nvarchar(250)=NULL,@TAXRATEM DECIMAL(18,2)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN06,@TTYPE INT = 0,@HTTYPE INT = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=(@COLUMN14)
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
if(isnull(cast(@COLUMN06 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG!=22400 AND @TaxAG!=22401)
		BEGIN
		set @ltaxamt=(cast(@COLUMN05 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
		set @TaxName=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SET @ID = (SELECT TOP 1 COLUMN02 FROM FITABLE001 WHERE COLUMN04 = @TaxName AND COLUMNA03 = @COLUMNA03)	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = @ID,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN05,    @COLUMN17 = @TAXRATE,    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN09,COLUMN17,  COLUMNA02, COLUMNA03,COLUMN19)
			values(@liabid,@dt,'PAYMENT VOUCHER',@internalid,@Payee,@COLUMN04,@Tax,@TaxAG,cast(ISNULL(@ltaxamt,0) as decimal(18,2)),(cast(ISNULL(@COLUMN05,0) as decimal(18,2))),@TAXRATE,@Voucher#,@TaxName,@COLUMNA02, @COLUMNA03, @COLUMN08)
		END
--if((SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)=22344)
--begin
-- Insert into FITABLE030(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,  COLUMN13,COLUMNA02, COLUMNA03)
-- values(@eid,@COLUMN03,@dt,@internalid,'ADVANCE PAYMENT',@COLUMN07,@COLUMN11,@COLUMN04,(cast(@COLUMN05 as decimal(18,2))+cast(@COLUMN06 as decimal(18,2))),cast((cast(isnull(@balance,0) as decimal(18,2)))+(cast(@COLUMN05 as decimal(18,2))+cast(@COLUMN06 as decimal(18,2))) as decimal(18,2)),@COLUMN04,@COLUMNA02, @COLUMNA03)
--end
--EMPHCS1574	Tax changes in PaymentVoucher And ReceiptVoucher By RAJ.Jr
		if(isnull(cast(@COLUMN06 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22400)
		begin
		set @ltaxamt=(cast(@COLUMN05 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		
			set @TaxName=(select max(column02) from fitable001 where column04 like('INPUT CST'+cast(@TAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			SET @ID = (SELECT TOP 1 COLUMN02 FROM FITABLE001 WHERE COLUMN04 = @TaxName AND COLUMNA03 = @COLUMNA03)	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = @ID,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN05,    @COLUMN17 = @TAXRATE,    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @liabid=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@liabid,'PAYMENT VOUCHER',@dt,@internalid,@Payee,@ltaxamt,NULL,@Voucher#,@TaxName,@COLUMNA02 , @COLUMNA03,0,@COLUMN08)
	    end
	
		if(isnull(cast(@COLUMN06 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		declare @kksb decimal(18,2)=0
		set @kksb=@column06
		set @ltaxamt=(cast(@COLUMN05 as decimal(18,2))*(cast((0.5) as decimal(18,2))*0.01))
		set @Payee=iif(@Payee!='' or @Payee is not null,@Payee,0)
		if(@dt>'11/14/2015')
		begin
			set @liabid=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @kksb=@kksb-@ltaxamt
			SELECT TOP 1 @ID = COLUMN07,@TaxName=COLUMN04 FROM FITABLE001 WHERE COLUMN02 = 1122 AND COLUMNA03 = @COLUMNA03	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ID, @COLUMN11 = '1122',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05,    @COLUMN17 = '0.5',    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@liabid,'PAYMENT VOUCHER',@dt,@internalid,@Payee,@ltaxamt,NULL,@Voucher#,1122,@COLUMNA02 , @COLUMNA03,0,@COLUMN08)
		end
		if(@dt>'05/31/2016')
		begin
			set @liabid=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @kksb=@kksb-@ltaxamt
			SELECT TOP 1 @ID = COLUMN07,@TaxName=COLUMN04 FROM FITABLE001 WHERE COLUMN02 = 1131 AND COLUMNA03 = @COLUMNA03	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ID, @COLUMN11 = '1131',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05,    @COLUMN17 = '0.5',    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN09,COLUMN17,  COLUMNA02, COLUMNA03,COLUMN19)
		values(@liabid,@dt,'PAYMENT VOUCHER',@internalid,@Payee,@COLUMN04,1131,NULL,@ltaxamt,@COLUMN05,0.5,@Voucher#,'Input KKC@0.50%',@COLUMNA02, @COLUMNA03,@COLUMN08)
	end
		set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
		set @TaxName=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SELECT TOP 1 @ID = COLUMN02,@TTYPE=COLUMN07 FROM FITABLE001 WHERE COLUMN04 = @TaxName AND COLUMNA03 = @COLUMNA03	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = @ID,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05,    @COLUMN17 = @TAXRATE,    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN09,COLUMN17,  COLUMNA02, COLUMNA03,COLUMN19)
			values(@liabid,@dt,'PAYMENT VOUCHER',@internalid,@Payee,@COLUMN04,@Tax,@TaxAG,cast(ISNULL(@kksb,0) as decimal(18,2)),(cast(ISNULL(@COLUMN05,0) as decimal(18,2))),@TAXRATE,@Voucher#,@TaxName,@COLUMNA02, @COLUMNA03, @COLUMN08)
		end
end
if(isnull(cast(@COLUMN16 as decimal(18,2)),0)>0)
		BEGIN
		SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE034)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	SET @TTYPE = (SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02 = 1120 AND COLUMNA03 = @COLUMNA03)	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @DATE,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = '1120',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN16,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee, 0, @COLUMN16,@TYPEID,'1120',@Project,
	@COLUMNA02, @COLUMNA03)
		END

		DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1 ,@cretAmnt decimal(18,2)
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@Payee AND FITABLE020.COLUMN16='OPEN' AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN18) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id!='' and @id is not null)
		     BEGIN 
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363'  AND FITABLE020.COLUMN07=@Payee AND FITABLE020.COLUMN16='OPEN' and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN18) s)
			   AND ISNULL(FITABLE020.COLUMNA13,0)=0 )
			   WHILE @Initialrow <= @MaxRownum
			   BEGIN
		        if(@COLUMN16!='0' and cast(@COLUMN16 as decimal(18,2))>0)
			  begin
				set @cretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 --		 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 --'Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 --'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN16 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(0), COLUMN16='CLOSE'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					set @COLUMN16=(cast(@COLUMN16 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN16 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
					update FITABLE020 set column18=(cast(isnull(column18,0) as DECIMAL(18,2))-cast(isnull(@COLUMN16,0) as DECIMAL(18,2))), 
					COLUMN16='OPEN'  where column02= @id and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 set @COLUMN16='0'
				 end
				end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1 
				END
				CLOSE curA 
				deallocate curA
			END
		--END

set @ReturnValue = 1
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE022
END 

ELSE IF @Direction = 'Update'
BEGIN
 set @COLUMN09= (select column01 from FITABLE020 WHERE COLUMN01=@COLUMN09)
 set @dt= (select column05 from FITABLE020 where COLUMN01=@COLUMN09)
 set @ID= (select column04 from FITABLE020 where COLUMN01=@COLUMN09)
 --//EMPHCS1505	Adding Bank Fiels in Payment voucher and receipt voucher at header level by RAJ.Jr 
 --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
 set @HBank= (select column06 from FITABLE020 where COLUMN01=@COLUMN09)
 set @COLUMN11= (@HBank)
 set @COLUMN10= (select COLUMN22 from FITABLE020 where COLUMN01=@COLUMN09)
 set @COLUMN12= (select COLUMN23 from FITABLE020 where COLUMN01=@COLUMN09)
 if not exists( SELECT 1 FROM FITABLE022 WHERE COLUMN02=@COLUMN02 and COLUMN09=@COLUMN09)
begin 
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE022_SequenceNo
insert into FITABLE022
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13, COLUMN14,COLUMN15,COLUMN16,COLUMN17,COLUMN18,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, @COLUMN10,@COLUMN11,@COLUMN12, @COLUMN13,@COLUMN14,@COLUMN15,@COLUMN16,@COLUMN17,
   @COLUMN18,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
end
else
begin
UPDATE FITABLE022 SET
--EMPHCS1067	when we enter two lines advance amount is not considering BY RAJ.Jr 11/9/2015
   COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,   COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14, COLUMN15=@COLUMN15, COLUMN16=@COLUMN16, COLUMN17=@COLUMN17, 
   COLUMN18=@COLUMN18,  COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMND04=@COLUMND04,                         COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02=@COLUMN02 and COLUMN09=@COLUMN09
end
 declare @Pay decimal(18,2)
 set @Tax= (@COLUMN14)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 set @TAXRATE= (select isnull(COLUMN17,0) from MATABLE013 where COLUMN02=@Tax)
set @internalid= (select column01 from FITABLE022 where COLUMN02=@COLUMN02)
set @Pay= (select column17 from FITABLE022 where COLUMN02=@COLUMN02)

set @pid=((select isnull(max(column02),999) from PUTABLE016)+1)
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @Hinternalid= (select max(column01) from FITABLE020 where COLUMN01=@COLUMN09)
set @TotAmount=(select ISNULL(COLUMN10,0) from FITABLE020 where COLUMN01=@COLUMN09)
set @Payee=(select COLUMN07 from FITABLE020 where COLUMN01=@COLUMN09)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
 set @PayeeType=(select COLUMN11 from FITABLE020 where COLUMN01=@COLUMN09)
set @Bankbal=(select ISNULL(COLUMN10,0) from FITABLE001 where COLUMN02=@COLUMN11 and COLUMNA03=@COLUMNA03)
if(@Tax!='' and (@Tax!=1000 or @Tax!='1000'))
begin
set @TaxName=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Tax and COLUMNA03=@COLUMNA03)
end
set @Voucher#=(select column04 from FITABLE020 where COLUMN01=@COLUMN09)
set @AccType=(select COLUMN04 from FITABLE001 where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03)
update FITABLE044 set  COLUMN18=(select column02 from FITABLE020 where COLUMN01=@COLUMN09),COLUMN19=(@COLUMN02) where column02=@COLUMN12 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
if((@COLUMN12!='' or @COLUMN12!=null) and @COLUMN10=22273)
begin
update FITABLE044 set COLUMN15=22629 where COLUMN02=@COLUMN12 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
 --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
set @DATE=(@dt)
set @TYPEID=(@Voucher#)
set @NUMBER=(@Hinternalid)
set @Project=(@COLUMN08)
set @Totbal=(cast(isnull(@COLUMN05,0) as decimal(18,2)))--+cast(isnull(@COLUMN06,0) as decimal(18,2)))
set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=@TYPE)
if(cast(@Totbal as nvarchar)!='' and cast(isnull(@Totbal,0) as decimal(18,2))!=0)
begin
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@Totbal as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
end

IF(@TYPE NOT IN(0))
BEGIN
	set @Increased=(0)
	set @Decreased=(@COLUMN05)
	IF(@TYPE=22383)
		BEGIN
			if(isnull(@ACCNAME,'') like 'INPUT%')
				BEGIN
					set @Increased=(@COLUMN05)
					set @Decreased=(0)
				END
		END
	if(@TYPE = '22406')
		begin 
			set @Increased=(@COLUMN05)
			set @Decreased=(0)
		end
END
ELSE
BEGIN
	set @Increased=(@COLUMN05)
	set @Decreased=(0)
END
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TYPE, @COLUMN11 = @COLUMN03,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @Increased,       @COLUMN15 = @Decreased,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

--Accounts payable
if(@COLUMN03=1117)
begin
 --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE016)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN13,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
	values(@ID,2000,@dt,@internalid,'PAYMENT VOUCHER',@Payee,@COLUMN11,@TYPEID,'OPEN',(cast(@COLUMN05 as decimal(18,2))),(cast(@COLUMN05 as decimal(18,2))),@COLUMN04,@COLUMNA02, @COLUMNA03,@COLUMN08)
end
--Accounts payable
else if(@TYPE=22263)
begin
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE016)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN13,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
	values(@ID,@COLUMN03,@dt,@internalid,'PAYMENT VOUCHER',@Payee,@COLUMN11,@TYPEID,'OPEN',(cast(@COLUMN05 as decimal(18,2))),(cast(@COLUMN05 as decimal(18,2))),@COLUMN04,@COLUMNA02, @COLUMNA03,@COLUMN08)
end

--Bank
ELSE IF(@TYPE=22266)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE019)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into PUTABLE019 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,  COLUMN20,  COLUMNA02, COLUMNA03)
	values(
	ISNULL(@ID,10001),  @COLUMN03,  @DATE,  @TYPEID,  'PAYMENT VOUCHER',  @Payee,  @NUMBER,  @COLUMN04, 0,@Totbal, 
	CAST(@Remainbal AS DECIMAL(18,2)),@Project,  @COLUMNA02, @COLUMNA03)
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@COLUMN03 and COLUMNA03=@COLUMNA03
END

--Cash
ELSE IF(@TYPE=22409)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@ID,1000),@COLUMN03,  @DATE,  @TYPEID,  'PAYMENT VOUCHER',@PayeeType,  @Payee,  @NUMBER,  @COLUMN04,0,@Totbal,
	  CAST(@Remainbal AS DECIMAL(18,2)),@Project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@COLUMN03 and COLUMNA03=@COLUMNA03
END

--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM PUTABLE017)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into PUTABLE017 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	COLUMN12,  COLUMN13,  COLUMN14, COLUMNA02, COLUMNA03)
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @COLUMN03,   @DATE,  @NUMBER,  'PAYMENT VOUCHER',  @Payee,  @COLUMN03,  @COLUMN04, @Totbal, 0, 
	@Remainbal,  @COLUMNA02, @Project, @COLUMNA02, @COLUMNA03)
END

--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM PUTABLE018)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	insert into PUTABLE018 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,    COLUMN09, COLUMN11,    
	COLUMN12,   COLUMN18,COLUMNA02, COLUMNA03 )
	values( 
	ISNULL(@ID,10001),  @COLUMN03,  @DATE,  @TYPEID,  'PAYMENT VOUCHER',  @Payee,  @COLUMN04,  @Totbal,0,
	@Remainbal, @Project,   @COLUMNA02, @COLUMNA03)
END

--Equity
ELSE IF(@TYPE=22330)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE029)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE029 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN07,    COLUMN06,    COLUMN08,   COLUMN09,  COLUMN10, 
	COLUMN11,COLUMN12,COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN03,  @Payee,  @COLUMN04,  0, @Totbal, 
	@TYPEID, @Project, @COLUMNA02, @COLUMNA03)
END

--Expense
ELSE IF(@TYPE=22344)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE036)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	Insert into FITABLE036 
	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,
	COLUMN10,COLUMN11,COLUMNA02, COLUMNA03) 
	values(	
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	isnull(@ID,10001),'PAYMENT VOUCHER',@DATE,@NUMBER,@Payee,@Totbal,0, @TYPEID,
	@COLUMN03,@Project,@COLUMNA02, @COLUMNA03 )
END

--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE026)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	set @chk=(select 1 from FITABLE001 where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMN04 like 'INPUT%')
	if(@chk=1)
	begin
	insert into FITABLE026 
	(COLUMN02,  COLUMN03, COLUMN04,   COLUMN05,  COLUMN06,COLUMN08,COLUMN07,  COLUMN11,  COLUMN12,  COLUMN09,COLUMN17,COLUMN19,
	COLUMNA02, COLUMNA03 )
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	values( ISNULL(@ID,10001), @DATE,'PAYMENT VOUCHER',@NUMBER,  @Payee,  @COLUMN03, @COLUMN04,@Totbal,0,  @TYPEID,@AccType,@Project,
	@COLUMNA02, @COLUMNA03)
	end
	else
	begin
		insert into FITABLE026 
	(COLUMN02,  COLUMN03, COLUMN04,   COLUMN05,  COLUMN06,COLUMN08,COLUMN07,  COLUMN11,  COLUMN12,  COLUMN09,COLUMN17,COLUMN19,
	COLUMNA02, COLUMNA03 )
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	values( ISNULL(@ID,10001), @DATE,'PAYMENT VOUCHER',@NUMBER,  @Payee,  @COLUMN03, @COLUMN04,0,@Totbal,  @TYPEID,@AccType,@Project,
	@COLUMNA02, @COLUMNA03)
	end
END

--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE034)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee,@Totbal,  0,@TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE028)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE028 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,
	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @COLUMN03,  @DATE,  'PAYMENT VOUCHER',  @TYPEID,  @Payee,  @COLUMN04, @Totbal, 0,  @Project,
	@COLUMNA02, @COLUMNA03)
END

--Income
ELSE IF(@TYPE=22405)
BEGIN
	set @PM=@COLUMN10
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE025)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	SET @COLUMN10=(SELECT MAX(ISNULL(COLUMN10,0)) FROM FITABLE025 WHERE COLUMN08=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @COLUMN10=(CAST(ISNULL(@COLUMN10,0) AS DECIMAL(18,2))-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE025 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN11,  
	 COLUMN10, COLUMN12, COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @DATE,  'PAYMENT VOUCHER',  @NUMBER,  @Payee,  @COLUMN04,  @COLUMN03, 0, @Totbal,
	 @COLUMN10,@Project,@COLUMNA02, @COLUMNA03)
END

--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE025)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	SET @COLUMN10=(SELECT MAX(ISNULL(COLUMN10,0)) FROM FITABLE025 WHERE COLUMN08=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @COLUMN10=(CAST(ISNULL(@COLUMN10,0) AS DECIMAL(18,2))-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE025 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN11,  
	 COLUMN10, COLUMN12,  	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  @DATE,  'PAYMENT VOUCHER',  @NUMBER,  @Payee,  @COLUMN04,  @COLUMN03, @Totbal,0,
	@COLUMN10,@Project,	@COLUMNA02, @COLUMNA03)
END

--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE035)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE035 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,
	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee,@Totbal, 0,  @TYPEID, @COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE036)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE036 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10,  COLUMN11,  
	COLUMNA02, COLUMNA03 )
	values( 
		--EMPHCS1413 rajasekhar reddy patakota 9/12/2015 Vochers latest changes according to accounts
	ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @NUMBER,  @Payee,@Totbal,0,  @TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END
--Non Current assets
ELSE IF(@TYPE=22410)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE060)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE060 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee, @Totbal, 0,@TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

--Non Current Liabilities
ELSE IF(@TYPE=22411)
BEGIN
	SET @ID=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE063)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE063 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@ID,10001),  'PAYMENT VOUCHER',  @DATE,  @COLUMN04,  @Payee,  0,@Totbal,@TYPEID,@COLUMN03,@Project,
	@COLUMNA02, @COLUMNA03)
END

if(@PM=22627)
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PDC ISSUED',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22383', @COLUMN11 = '1117',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
	insert into FITABLE026 
	(COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN11,    COLUMN09,COLUMN17,COLUMN07,COLUMN08,
	COLUMN14,COLUMN19,COLUMNA02, COLUMNA03 	)
	values( 
	@liabid,  'PDC ISSUED',  @dt,  @internalid,  @Payee,  @Totbal,    @Voucher#,@AccType,@COLUMN04,'1117',
	@Totbal,@COLUMN08,@COLUMNA02, @COLUMNA03)
end
else
begin
set @PU19=((select isnull(max(column02),999) from PUTABLE019)+1)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN11 AND COLUMNA03=@COLUMNA03)
--Cash
IF(@AccountType=22409)
BEGIN
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN11,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN17,       @COLUMN15 = 0,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @PU19=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@COLUMN11 AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@PU19,1000),@COLUMN11,@dt,@Voucher#,  'PAYMENT VOUCHER',@PayeeType,  @Payee,  @internalid,  @COLUMN04,(cast(@COLUMN17 as decimal(18,2))),
	 0,(cast(@Bankbal as decimal(18,2))-(cast(@COLUMN17 as decimal(18,2)))),@Project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@COLUMN11 and COLUMNA03=@COLUMNA03
END
else
begin
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN11,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @Pay,       @COLUMN15 = 0,     @COLUMN19 = @ACCNAME, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMNA02, COLUMNA03,COLUMN20)
 values(@PU19,@COLUMN11,@dt,@Voucher#,'PAYMENT VOUCHER',@Payee,@internalid,cast(@Pay as decimal(18,2)),(cast(@Bankbal as decimal(18,2))-(cast(@Pay as decimal(18,2)))),@COLUMN12,@COLUMNA02, @COLUMNA03,@COLUMN08)
 update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))-cast(@COLUMN17 as decimal(18,2))) where column02=@COLUMN11 and COLUMNA03=@COLUMNA03
 end
end

--set @balance=(select top 1 isnull(column12,0) from FITABLE030  order by column02 desc)
--set @eid=(cast((select max(isnull(column02,1000)) from FITABLE030) as int)+1)
--EMPHCS1574	Tax changes in PaymentVoucher And ReceiptVoucher By RAJ.Jr
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
set @cnt  = 0 
set @vochercnt  = 1
set @AMOUNTM=@COLUMN06 set @TTYPE  = 0 set @HTTYPE  = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=(@COLUMN14)
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
 if(isnull(cast(@COLUMN06 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG!=22400 AND @TaxAG!=22401)
		BEGIN
		set @ltaxamt=(cast(@COLUMN05 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
		set @TaxName=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		SET @ID = (SELECT TOP 1 COLUMN02 FROM FITABLE001 WHERE COLUMN04 = @TaxName AND COLUMNA03 = @COLUMNA03)	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = @ID,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN05,    @COLUMN17 = @TAXRATE,    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN09,COLUMN17,  COLUMNA02, COLUMNA03,COLUMN19)
			values(@liabid,@dt,'PAYMENT VOUCHER',@internalid,@Payee,@COLUMN04,@Tax,@TaxAG,cast(ISNULL(@ltaxamt,0) as decimal(18,2)),(cast(ISNULL(@COLUMN05,0) as decimal(18,2))),@TAXRATE,@Voucher#,@TaxName,@COLUMNA02, @COLUMNA03,@COLUMN08)
		END
--EMPHCS1574	Tax changes in PaymentVoucher And ReceiptVoucher By RAJ.Jr
		if(isnull(cast(@COLUMN06 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22400)
		begin
		set @ltaxamt=(cast(@COLUMN05 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		
			set @TaxName=(select max(column02) from fitable001 where column04 like('INPUT CST'+cast(@TAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			SET @ID = (SELECT TOP 1 COLUMN02 FROM FITABLE001 WHERE COLUMN04 = @TaxName AND COLUMNA03 = @COLUMNA03)	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = @ID,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN05,    @COLUMN17 = @TAXRATE,    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @liabid=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@liabid,'PAYMENT VOUCHER',@dt,@internalid,@Payee,@ltaxamt,NULL,@Voucher#,@TaxName,@COLUMNA02 , @COLUMNA03,0,@Project)
	    end
	
		if(isnull(cast(@COLUMN06 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		set @kksb=@column06
		set @ltaxamt=(cast(@COLUMN05 as decimal(18,2))*(cast((0.5) as decimal(18,2))*0.01))
		set @Payee=iif(@Payee!='' or @Payee is not null,@Payee,0)
		if(@dt>'11/14/2015')
		begin
			set @liabid=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @kksb=@kksb-@ltaxamt
			SELECT TOP 1 @ID = COLUMN07,@TaxName=COLUMN04 FROM FITABLE001 WHERE COLUMN02 = 1122 AND COLUMNA03 = @COLUMNA03	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ID, @COLUMN11 = '1122',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05,    @COLUMN17 = '0.5',    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@liabid,'PAYMENT VOUCHER',@dt,@internalid,@Payee,@ltaxamt,NULL,@Voucher#,1122,@COLUMNA02 , @COLUMNA03,0,@COLUMN08)
		end
		if(@dt>'05/31/2016')
		begin
			set @liabid=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @kksb=@kksb-@ltaxamt
			SELECT TOP 1 @ID = COLUMN07,@TaxName=COLUMN04 FROM FITABLE001 WHERE COLUMN02 = 1131 AND COLUMNA03 = @COLUMNA03	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ID, @COLUMN11 = '1131',
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05,    @COLUMN17 = '0.5',    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN09,COLUMN17,  COLUMNA02, COLUMNA03,COLUMN19)
		values(@liabid,@dt,'PAYMENT VOUCHER',@internalid,@Payee,@COLUMN04,1131,NULL,@ltaxamt,@COLUMN05,0.5,@Voucher#,'Input KKC@0.50%',@COLUMNA02, @COLUMNA03,@COLUMN08)
	end
		set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
		set @TaxName=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SELECT TOP 1 @ID = COLUMN02,@TTYPE=COLUMN07 FROM FITABLE001 WHERE COLUMN04 = @TaxName AND COLUMNA03 = @COLUMNA03	
exec  [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT VOUCHER',  @COLUMN04 = @TYPEID, @COLUMN05 = @Hinternalid,  @COLUMN06 = @internalid,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @TTYPE, @COLUMN11 = @ID,
		@COLUMN12 = @COLUMN04,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05,    @COLUMN17 = @TAXRATE,    @COLUMN18 = @TaxAG,@COLUMN19 = @TaxName,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN09,COLUMN17,  COLUMNA02, COLUMNA03,COLUMN19)
			values(@liabid,@dt,'PAYMENT VOUCHER',@internalid,@Payee,@COLUMN04,@Tax,@TaxAG,cast(ISNULL(@kksb,0) as decimal(18,2)),(cast(ISNULL(@COLUMN05,0) as decimal(18,2))),@TAXRATE,@Voucher#,@TaxName,@COLUMNA02, @COLUMNA03, @COLUMN08)
		
		end
		end
--if((SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03)=22344)
--begin
-- Insert into FITABLE030(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,  COLUMN13,COLUMNA02, COLUMNA03)
-- values(@eid,@COLUMN03,@dt,@internalid,'PAYMENT VOUCHER',@COLUMN07,@COLUMN11,@COLUMN04,(cast(@COLUMN05 as decimal(18,2))+cast(@COLUMN06 as decimal(18,2))),cast((cast(isnull(@balance,0) as decimal(18,2)))+(cast(@COLUMN05 as decimal(18,2))+cast(@COLUMN06 as decimal(18,2))) as decimal(18,2)),@COLUMN04,@COLUMNA02, @COLUMNA03)
--end
--DECLARE @MaxRownum INT
      --DECLARE @Initialrow INT=1 ,@cretAmnt decimal(18,2)
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@Payee AND FITABLE020.COLUMN16='OPEN' AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN18) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id!='' and @id is not null)
		     BEGIN 
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363'  AND FITABLE020.COLUMN07=@Payee AND FITABLE020.COLUMN16='OPEN' and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN18) s)
			   AND ISNULL(FITABLE020.COLUMNA13,0)=0 )
			   WHILE @Initialrow <= @MaxRownum
			   BEGIN
		        if(@COLUMN16!='0' and cast(@COLUMN16 as decimal(18,2))>0)
			  begin
				set @cretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 --		 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 --'Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 --'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN16 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(0), COLUMN16='CLOSE'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					set @COLUMN16=(cast(@COLUMN16 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN16 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
					update FITABLE020 set column18=(cast(isnull(column18,0) as DECIMAL(18,2))-cast(isnull(@COLUMN16,0) as DECIMAL(18,2))), 
					COLUMN16='OPEN'  where column02= @id and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 set @COLUMN16='0'
				 end
				end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1 
				END
				CLOSE curA 
				deallocate curA
			END

set @ReturnValue = 1
END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE022 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END

end try
begin catch
--EMPHCS1410 LOGS CREATION BY SRINIVAS	
declare @tempSTR nvarchar(max)	
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_FITABLE022_PAYMENTVOUCHER.txt',0
return 0
end catch
end








GO
