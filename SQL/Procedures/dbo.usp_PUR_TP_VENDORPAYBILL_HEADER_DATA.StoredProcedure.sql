USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_VENDORPAYBILL_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_VENDORPAYBILL_HEADER_DATA]
(
	@PurchaseOrderID nvarchar(250)
)

AS
BEGIN
   SELECT   COLUMN05 ,COLUMN02,column06, COLUMN10 ,(null)  COLUMN09,COLUMN12 COLUMN13,
   --EMPHCS1127	 payment mode , payment terms not automatically updating in payment BY RAJ.Jr 12/9/2015
   isnull((select top 1(COLUMN11) from PUTABLE014  where COLUMN05=@PurchaseOrderID order by COLUMN02 DESC),
   (select COLUMN04 from CONTABLE025 where COLUMN02=19)) COLUMN12,COLUMN21,COLUMN15, COLUMN16,COLUMN17,COLUMN18,COLUMN20 
   FROM PUTABLE005 WHERE  COLUMN05=@PurchaseOrderID --and COLUMN13!=(select COLUMN04 from CONTABLE025 where COLUMN02=18) 
END












GO
