USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTDELIVERYCHALLAN]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_GSTDELIVERYCHALLAN](@SONO nvarchar(250)=null,@TYPE nvarchar(250)=null,@frDT nvarchar(250)=null)
AS
BEGIN
if(@TYPE='HEADER')
begin
select S7.COLUMN04,s.COLUMN05,S7.COLUMN08,S7.COLUMN01,So.COLUMN04,So.COLUMN15,m.COLUMN04,pm.COLUMN04  mode ,
So.COLUMN08, So.COLUMN32 COLUMN32,CONVERT(varchar,CAST(So.COLUMN12 AS money), 1),So.COLUMN32,S7.COLUMN05 cust,ht.COLUMN07 as HTamnt,
c.COLUMN04 as company  , c.COLUMN28 as logoName,S7.COLUMN15 as oppunit ,So.COLUMN40 as TrackingNo,isnull(So.COLUMN14,0)+isnull(So.COLUMN60,0) as Discount,
So.COLUMN41 as PackingCharges,So.COLUMN42 as ShippingCharges,f.COLUMN04 as form, S7.COLUMN05 as cId,c.COLUMN27 as VATNO,
c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO,c.COLUMN09 as compADD,cc.COLUMN03 as compCT,cs.COLUMN03 as compST, 
c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, 
CONVERT(varchar,CAST(So.COLUMN15 AS money), 1) totalamt1,s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,
c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,
So.COLUMN15 amt ,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,
opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,
CONVERT(varchar,CAST(So.COLUMN14 AS money), 1) discount,0 TDSAmount,0 BalanceDue , 
S7.COLUMNA03 ACOUNTOWNER ,(So.COLUMN32+So.COLUMN12) TotalnonTDS,s.COLUMN42 CustGSTIN,
(M18.COLUMN06+'-'+M18.COLUMN05) StateCode,'' RevCharge,'' PlaceofSupply,S7.COLUMN05 as vId,s1.COLUMN05 Vendor,c.COLUMND02 CGSTIN,(M19.COLUMN06+'-'+M19.COLUMN05) compCtry,s1.COLUMN34 Jober,c.COLUMN07 Custmail,so.COLUMN44 TruckNo,so.COLUMN11 reference,so.COLUMN09 memo,so.COLUMN50 from SATABLE007 S7 
left join SATABLE005 so on so.column02=S7.COLUMN06 and so.COLUMNA03=S7.COLUMNA03 
left join SATABLE002 s on s.column02=S7.COLUMN05 left join matable002 mot on mot.column02=So.COLUMN39 
left join matable002 ft on ft.column02=So.COLUMN36 left join CONTABLE008 c on c.COLUMNA03=S7.COLUMNA03 AND (c.COLUMNA02=S7.COLUMNA02 or c.COLUMNA02 is null) AND ISNULL(c.COLUMNA13,0)=0 AND ISNULL(c.COLUMN05,0)=0 
left join MATABLE016 cc on cc.COLUMN02=c.COLUMN15 and cc.COLUMNA03=c.COLUMNA03 and isnull(cc.COLUMNA13,0)=0 
left join MATABLE017 cs on cs.COLUMN02=c.COLUMN13 and cs.COLUMNA03=c.COLUMNA03 and isnull(cs.COLUMNA13,0)=0 
left join CONTABLE007 opu on opu.COLUMN02=S7.COLUMN15 left join matable002 f on f.column02=So.COLUMN37    
left join MATABLE022 m on m.column02=So.COLUMN10  left join matable002 pm on pm.column02=So.COLUMN30 
left JOIN MATABLE017 M18 ON M18.COLUMN02 = s.COLUMN43  and isnull(M18.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=So.COLUMN31 
left join SATABLE001 s1 on s1.column02=S7.COLUMN05 and s1.COLUMNA03=S7.COLUMNA03
left JOIN MATABLE017 M19 ON M19.COLUMN02 = s1.COLUMN35  and isnull(M19.COLUMNA13,0)=0 
where S7.column02=@SONO
end
ELSE if(@TYPE='LINE')
begin
select * from(
select it.column04 item,it.column06 upc,isnull(a.COLUMN09,0) quantity,a.COLUMN19 units,'' serial,
a.COLUMN05 'desc',CONVERT(varchar,CAST(isnull(a.COLUMN12,0) AS money), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN09,0)*isnull(a.COLUMN12,0) AS money), 1) lineamt,
isnull(sl.COLUMN10,0) Discount,sum(isnull(t.COLUMN07,0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0)) as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))+cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate from  SATABLE008 a 
inner join SATABLE007 b on b.column01=a.COLUMN14 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN04 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join SATABLE005 so on so.column02=b.COLUMN06 and so.COLUMNA02=b.COLUMNA02 and so.COLUMNA03=b.COLUMNA03 and isnull(so.COLUMNA13,0)=0 
left join SATABLE006 sl on sl.column19=so.COLUMN01 and sl.column02=a.COLUMN26 and sl.COLUMNA02=so.COLUMNA02 and sl.COLUMNA03=so.COLUMNA03 and isnull(sl.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=so.COLUMN31 and ht.COLUMNA03=so.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN19 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or t.COLUMN02=sl.COLUMN26) and t.COLUMNA03=sl.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
--left join MATABLE013 vt on  (vt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or vt.COLUMN02=sl.COLUMN26) AND vt.COLUMN16=22399 and isnull(vt.COLUMNA13,0)=0
--left join MATABLE013 cst on (cst.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or cst.COLUMN02=sl.COLUMN26) AND cst.COLUMN16=22400 and isnull(cst.COLUMNA13,0)=0
--left join MATABLE013 cgt on (cgt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or cgt.COLUMN02=sl.COLUMN26) AND cgt.COLUMN16=23582 and isnull(cgt.COLUMNA13,0)=0
--left join MATABLE013 sgt on (sgt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or sgt.COLUMN02=sl.COLUMN26) AND sgt.COLUMN16=23583 and isnull(sgt.COLUMNA13,0)=0
--left join MATABLE013 igt on (igt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or igt.COLUMN02=sl.COLUMN26) AND igt.COLUMN16=23584 and isnull(igt.COLUMNA13,0)=0
--left join MATABLE013 ugt on (ugt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or ugt.COLUMN02=sl.COLUMN26) AND ugt.COLUMN16=23585 and isnull(ugt.COLUMNA13,0)=0
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN19,a.COLUMN05,a.COLUMN12,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN09,a.COLUMN25,sl.COLUMN10,ht.COLUMN07,a.COLUMN02) f order by f.COLUMN02
END
ELSE if(@TYPE='TOTAL')
begin
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt,sum(f.subtotamt1)subtotamt1 from(
select (isnull(a.COLUMN09,0)) qty,cast((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN12 ,0)) as decimal(18,2)) subtotamt,
cast((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0)) as decimal(18,2)) subtotamt1,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))+cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2))) as decimal(18,2)) amt
from  SATABLE008 a 
inner join SATABLE007 b on b.column01=a.COLUMN14 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
left join SATABLE005 so on so.column02=b.COLUMN06 and so.COLUMNA02=b.COLUMNA02 and so.COLUMNA03=b.COLUMNA03 and isnull(so.COLUMNA13,0)=0 
left join SATABLE006 sl on sl.column19=so.COLUMN01 and sl.column02=a.COLUMN26 and sl.COLUMNA02=so.COLUMNA02 and sl.COLUMNA03=so.COLUMNA03 and isnull(sl.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or t.COLUMN02=sl.COLUMN26) and t.COLUMNA03=sl.COLUMNA03 and isnull(t.COLUMNA13,0)=0
--left join MATABLE013 vt on  (vt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s))) AND vt.COLUMN16=22399 and vt.COLUMNA03=sl.COLUMNA03 and isnull(vt.COLUMNA13,0)=0
--left join MATABLE013 cst on (cst.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s))) AND cst.COLUMN16=22400 and cst.COLUMNA03=sl.COLUMNA03 and isnull(cst.COLUMNA13,0)=0
--left join MATABLE013 cgt on (cgt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s))) AND cgt.COLUMN16=23582 and cgt.COLUMNA03=sl.COLUMNA03 and isnull(cgt.COLUMNA13,0)=0
--left join MATABLE013 sgt on (sgt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s))) AND sgt.COLUMN16=23583 and sgt.COLUMNA03=sl.COLUMNA03 and isnull(sgt.COLUMNA13,0)=0
--left join MATABLE013 igt on (igt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s))) AND igt.COLUMN16=23584 and igt.COLUMNA03=sl.COLUMNA03 and isnull(igt.COLUMNA13,0)=0
--left join MATABLE013 ugt on (ugt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=sl.COLUMNA03 and COLUMN02=replace(sl.COLUMN26,'-',''))) s))) AND ugt.COLUMN16=23585 and ugt.COLUMNA03=sl.COLUMNA03 and isnull(ugt.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN09,a.COLUMN12,a.COLUMN25,a.COLUMN02) f
END
ELSE if(@TYPE='HSNCODE')
begin
select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(CAST(f.lineamt as decimal(18,2))) lineamt,f.SerItem as SerItem from (select CONVERT(varchar,CAST(isnull(a.COLUMN25,0) AS money), 1) rate,(CAST(isnull(a.COLUMN09,0)*isnull(a.COLUMN25,0) as decimal(18,2))) lineamt,sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN25 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt ,M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,a.COLUMN02 from  SATABLE008 a inner join SATABLE007 b on b.column01=a.COLUMN14 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN04 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join SATABLE005 so on so.column02=b.COLUMN06 and so.COLUMNA02=b.COLUMNA02 and so.COLUMNA03=b.COLUMNA03 and isnull(so.COLUMNA13,0)=0 left join SATABLE006 sl on sl.column19=so.COLUMN01 and sl.column02=a.COLUMN26 and sl.COLUMNA02=so.COLUMNA02 and sl.COLUMNA03=so.COLUMNA03 and isnull(sl.COLUMNA13,0)=0 left join MATABLE013 ht on ht.column02=so.COLUMN31 and ht.COLUMNA03=so.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN19 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(sl.COLUMN26,'-',''))) s)) or t.COLUMN02=sl.COLUMN26) and t.COLUMNA03=sl.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by it.column06,a.COLUMN19,a.COLUMN05,a.COLUMN12,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,a.COLUMN09,a.COLUMN25,sl.COLUMN10,ht.COLUMN07,a.COLUMN02) f group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST
END
END


GO
