USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE045]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE045]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
    @COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,   
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT,    @internalid nvarchar(250)=null, 
	@pid nvarchar(250)=null
)

AS
BEGIN
begin try  
set @COLUMNA02=( CASE WHEN (@COLUMN16!= '' and @COLUMN16 is not null and @COLUMN16!=0 ) THEN @COLUMN16 else @COLUMNA02  END )
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE045_SequenceNo
declare @tempSTR nvarchar(max)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Payment Header Values are Intiated for FITABLE045 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into FITABLE045 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11, 
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMNA01, 
   COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
   COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, 
   COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19,   @COLUMN20,  @COLUMNA01, 
   @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
   @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
   @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
   @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Payment Header Intiated Values are Created in FITABLE045 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
DECLARE @Projectactualcost DECIMAL(18,2), @Totbal DECIMAL(18,2),@balance DECIMAL(18,2),@Project  nvarchar(250),@AccType  nvarchar(250),@paymenttype  nvarchar(250)
set @internalid= (select COLUMN01 from FITABLE045 where COLUMN02=@COLUMN02)
set @Project=(@COLUMN09)
set @Totbal=(isnull(@COLUMN19,0))
--if(@Totbal>0 and @Totbal is not null)
--begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN13,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

set @pid=((select max(isnull(column02,999)) from PUTABLE016)+1)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Accounts Payable Values are Intiated for PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@pid,999)as nvarchar(250)) +','+'2000'+','+ cast(isnull(@COLUMN05,'') as nvarchar(250))+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+ cast('EXPENSE' as nvarchar(250))+','+ cast(isnull(@COLUMN06,'') as nvarchar(250))+','+  cast(isnull(@COLUMN12,'') as nvarchar(250))+','+cast(isnull(@COLUMN04,'') as nvarchar(250))+','+'CLOSE'+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@Totbal,'')) as nvarchar(250))+','+cast(isnull(@COLUMN13,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +','+cast(isnull(@Project,'') as nvarchar(250)) +  '  '))
Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN13,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
values(@pid,2000,@COLUMN05,@internalid,'EXPENSE',@COLUMN06,@COLUMN12,@COLUMN04,'Paid',@Totbal,@Totbal,@COLUMN13,@COLUMNA02, @COLUMNA03,@Project)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Accounts Payable Intiated Values are Created in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @AccType=(select COLUMN04 from FITABLE001 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03)
if(@COLUMN15=22627 or @COLUMN15=22273)
begin
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Write Cheque Updation for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Expense Payment Header ID'+
   cast(isnull(@COLUMN02,'') as nvarchar(250)) +','+  '2.Expense Payment Line ID'+','+  cast(isnull(@COLUMN02,'')  as nvarchar(250))+','+ '3.Cheque'+','+ cast(isnull(@COLUMN20,'')   as nvarchar(250))+','+'4.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +  '  '))
update FITABLE044 set  COLUMN18=(@column02),COLUMN19=(@internalid) where column02=@COLUMN20 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Write Cheque Updated in FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
if(@COLUMN15=22627)
begin
	set @pid=((select isnull(max(column02),999) from FITABLE026)+1)
	set @AccType=(select COLUMN04 from FITABLE001 where COLUMN02=1117 and COLUMNA03=@COLUMNA03)
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PDC ISSUED',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = @AccType, @COLUMN11 = '1117',
		@COLUMN12 = @COLUMN13,   @COLUMN13 = 0,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Current Liabilities Values are Intiated for PDC in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are '+
	   cast(isnull(@pid,999)as nvarchar(250)) +','+cast(isnull(@COLUMN05,'') as nvarchar(250))+','+'PDC ISSUED'+','+ cast(isnull(@internalid,'') as nvarchar(250))+','+  cast(isnull(@COLUMN06,'') as nvarchar(250))+','+ cast(isnull(@Totbal,'') as nvarchar(250))+','+ cast(isnull(@COLUMN04,'') as nvarchar(250))+','+  cast(isnull(@AccType,'') as nvarchar(250))+','+cast(isnull(@COLUMN13,'') as nvarchar(250))+','+'1117'+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@COLUMN09,'')) as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	insert into FITABLE026 
		(COLUMN02,  COLUMN03, COLUMN04,   COLUMN05,  COLUMN06,  COLUMN11,    COLUMN09,COLUMN17,COLUMN07,COLUMN08,
		COLUMN14,COLUMN19,COLUMNA02, COLUMNA03 	)
		values( 
		isnull(@pid,999),  @COLUMN05,'PDC ISSUED',    @internalid,  @COLUMN06,  @Totbal,    @COLUMN04,@AccType,@COLUMN13,'1117',
		@Totbal,@COLUMN09,@COLUMNA02, @COLUMNA03)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Current Liabilities Intiated Values are Created in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
else
begin	
	if(@COLUMN19!='' and (@Project!='' and @Project!=0))
	begin
	set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
	 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Project Actualcost Updation for PRTABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are 1.Project Actualcost'+
	   cast(isnull(@Projectactualcost,'') as nvarchar(250)) +','+  '2.Expense Amount'+','+  cast(isnull(@COLUMN19,'')  as nvarchar(250))+','+ '3.Project'+','+ cast(isnull(@Project,'')   as nvarchar(250))+','+'4.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +  '  '))
	update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN19 as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Project Actualcost Updated in PRTABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
	set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03)
	set @pid=((select max(isnull(column02,999)) from PUTABLE019)+1)
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	if(cast(@COLUMN19 as decimal(18,2))>0)
	begin
	declare @AccountType nvarchar(250)
	 set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN12 AND COLUMNA03=@COLUMNA03)
	  IF(@AccountType=22409)
	  BEGIN
	  	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN12,
		@COLUMN12 = @COLUMN13,   @COLUMN13 = 0,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

SET @pid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	  	insert into FITABLE052 
	  	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	  	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	  	values(
	  	ISNULL(@pid,1000),  @COLUMN12,@COLUMN05,@COLUMN04,'EXPENSE',  @COLUMN06,@internalid,@COLUMN13,@Totbal,  
	  	0,(@balance -@Totbal),@Project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
	  END
	  else
	  begin
	 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are '+
	   cast(isnull(@pid,999)as nvarchar(250)) +','+ cast(isnull(@COLUMN12,'') as nvarchar(250))+','+  cast(isnull(@COLUMN05,'') as nvarchar(250))+','+ cast(isnull(@COLUMN04,'') as nvarchar(250))+','+  'EXPENSE'+','+  cast(isnull(@COLUMN06,'') as nvarchar(250))+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@balance,'') -isnull(@Totbal,'')) as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +','+cast(isnull(@Project,'') as nvarchar(250)) +  '  '))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN12,
		@COLUMN12 = @COLUMN13,   @COLUMN13 = 0,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMNA02, COLUMNA03, COLUMN20)
	values(isnull(@pid,999),@COLUMN12,@COLUMN05,@COLUMN04,'EXPENSE',@COLUMN06,@internalid,@Totbal,(@balance -@Totbal),@COLUMNA02, @COLUMNA03,@Project)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Bank Register Intiated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are 1.Balance Amount'+ cast(isnull(@balance,'') as nvarchar(250)) +','+  '2.Expense Amount'+','+  cast(isnull(@Totbal,'')  as nvarchar(250))+','+ '3.Date'+','+ cast(isnull(@COLUMN05,'')   as nvarchar(250))+','+ '4.Account'+','+ cast( isnull(@COLUMN12,'')  as nvarchar(250))+','+'4.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +  '  '))
	update FITABLE001 set COLUMN10=(@balance -@Totbal),COLUMN09=@COLUMN05 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
	end
	--end
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue = (SELECT COLUMN01 FROM FITABLE045 WHERE COLUMN02=@COLUMN02)
END

ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE045
END 
 
ELSE IF @Direction = 'Update'
BEGIN
declare @pbalance decimal(18,2)
set @pbalance= (select COLUMN19 from FITABLE045 where COLUMN02=@COLUMN02)
  set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Payment Header Values are Intiated for FITABLE045 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE FITABLE045 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05, 
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01, 
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Payment Header Intiated Values are Updated in FITABLE045 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )  
set @internalid= (select COLUMN01 from FITABLE045 where COLUMN02=@COLUMN02)
set @Project=(@COLUMN09)
set @Totbal=( isnull(@COLUMN19,0))
delete from fitable064 where column03 in( 'EXPENSE','PDC ISSUED') and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
delete from  PUTABLE016  where COLUMN03=2000 and COLUMN05 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN09=@COLUMN04 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN13,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

set @pid=((select max(isnull(column02,999)) from PUTABLE016)+1)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Accounts Payable Values are Intiated for PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@pid,999)as nvarchar(250)) +','+'2000'+','+ cast(isnull(@COLUMN05,'') as nvarchar(250))+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+ cast('EXPENSE' as nvarchar(250))+','+ cast(isnull(@COLUMN06,'') as nvarchar(250))+','+  cast(isnull(@COLUMN12,'') as nvarchar(250))+','+cast(isnull(@COLUMN04,'') as nvarchar(250))+','+'CLOSE'+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@Totbal,'')) as nvarchar(250))+','+cast(isnull(@COLUMN13,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +','+cast(isnull(@Project,'') as nvarchar(250)) +  '  '))
Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN13,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
values(isnull(@pid,999),2000,@COLUMN05,@internalid,'EXPENSE',@COLUMN06,@COLUMN12,@COLUMN04,'Paid',@Totbal,@Totbal,@COLUMN13,@COLUMNA02, @COLUMNA03,@Project)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Header Update:Accounts Payable Intiated Values are Created in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''))

set @AccType=(select COLUMN04 from FITABLE001 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03)
if(@COLUMN15=22627 or @COLUMN15=22273)
begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Write Cheque Updation for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Expense Payment Header ID'+
   cast(isnull(@COLUMN02,'') as nvarchar(250)) +','+  '2.Expense Payment Line ID'+','+  cast(isnull(@COLUMN02,'')  as nvarchar(250))+','+ '3.Cheque'+','+ cast( isnull(@COLUMN20,'')  as nvarchar(250))+','+'4.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +  '  '))
update FITABLE044 set  COLUMN18=(@column02),COLUMN19=(@internalid) where column02=@COLUMN20 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Header Update:Write Cheque Updated in FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
if(@COLUMN15=22627)
begin
	delete from  FITABLE026  where COLUMN04='PDC ISSUED' and  COLUMN05 =@internalid and  COLUMN09 =@COLUMN04 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
			 
	set @pid=((select isnull(max(column02),999) from FITABLE026)+1)
	set @AccType=(select COLUMN04 from FITABLE001 where COLUMN02=1117 and COLUMNA03=@COLUMNA03)
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PDC ISSUED',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = @AccType, @COLUMN11 = '1117',
		@COLUMN12 = @COLUMN13,   @COLUMN13 = 0,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Update:Current Liabilities Values are Intiated for PDC in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are '+
	   cast(isnull(@pid,1000)as nvarchar(250)) +','+cast(isnull(@COLUMN05,'') as nvarchar(250))+','+'PDC ISSUED'+','+ cast(isnull(@internalid,'') as nvarchar(250))+','+  cast(isnull(@COLUMN06,'') as nvarchar(250))+','+ cast(isnull(@Totbal,'') as nvarchar(250))+','+ cast(isnull(@COLUMN04,'') as nvarchar(250))+','+  cast(isnull(@AccType,'') as nvarchar(250))+','+cast(isnull(@COLUMN13,'') as nvarchar(250))+','+'1117'+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@COLUMN09,'')) as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
		insert into FITABLE026 
		(COLUMN02,  COLUMN03, COLUMN04,   COLUMN05,  COLUMN06,  COLUMN11,    COLUMN09,COLUMN17,COLUMN07,COLUMN08,
		COLUMN14,COLUMN19,COLUMNA02, COLUMNA03 	)
		values( 
		isnull(@pid,1000),  @COLUMN05,'PDC ISSUED',    @internalid,  @COLUMN06,  @Totbal,    @COLUMN04,@AccType,@COLUMN13,'1117',
		@Totbal,@COLUMN09,@COLUMNA02, @COLUMNA03)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Header Update:Current Liabilities Intiated Values are Created in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
else
begin
	if(@COLUMN19!='' and (@Project!='' and @Project!=0))
	begin
	set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Update:Project Actualcost Updation for PRTABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are 1.Project Actualcost'+
	   cast(isnull(@Projectactualcost,'') as nvarchar(250)) +','+  '2.Expense Amount'+','+  cast(isnull(@COLUMN19,'')  as nvarchar(250))+','+ '3.Project'+','+ cast(isnull(@Project,'')   as nvarchar(250))+','+'4.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +','+'5.Previous Amount'+','+ISNULL(cast(isnull(@pbalance,'') as nvarchar(250)),'')+  '  '))
	update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN19 as decimal(18,2))-cast(@pbalance as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Header Update:Project Actualcost Updated in PRTABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end 
	delete from  PUTABLE019  where COLUMN03=@COLUMN12 and COLUMN08 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN05=@COLUMN04 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	delete from  FITABLE052  where COLUMN03=@COLUMN12 and COLUMN09 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN05=@COLUMN04 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03)
	set @pid=((select max(isnull(column02,999)) from PUTABLE019)+1)
	if(cast(@COLUMN19 as decimal(18,2))>0)
	begin
	set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@COLUMN12 AND COLUMNA03=@COLUMNA03)
	  IF(@AccountType=22409)
	  BEGIN
	  	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN12,
		@COLUMN12 = @COLUMN13,   @COLUMN13 = 0,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @pid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	  	insert into FITABLE052 
	  	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	  	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	  	values(
	  	ISNULL(@pid,1000),  @COLUMN12,@COLUMN05,@COLUMN04,'EXPENSE',  @COLUMN06,@internalid,@COLUMN13,@Totbal,  
	  	0,(@balance -@Totbal),@Project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
	  END
	  else
	  begin
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Update:Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are '+
	   cast(isnull(@pid,999)as nvarchar(250)) +','+ cast(isnull(@COLUMN12,'') as nvarchar(250))+','+  cast(isnull(@COLUMN05,'') as nvarchar(250))+','+ cast(isnull(@COLUMN04,'') as nvarchar(250))+','+  'EXPENSE'+','+  cast(isnull(@COLUMN06,'') as nvarchar(250))+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@balance,'') -isnull(@Totbal,'')) as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +','+cast(isnull(@Project,'') as nvarchar(250)) +  '  '))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN05,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN06,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN12,
		@COLUMN12 = @COLUMN13,   @COLUMN13 = 0,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMNA02, COLUMNA03, COLUMN20)
	values(isnull(@pid,999),@COLUMN12,@COLUMN05,@COLUMN04,'EXPENSE',@COLUMN06,@internalid,@Totbal,(@balance -@Totbal),@COLUMNA02, @COLUMNA03,@Project)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Header Update:Bank Register Intiated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Update:Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are 1.Balance Amount'+ cast(isnull(@balance,'') as nvarchar(250)) +','+  '2.Expense Amount'+','+  cast(isnull(@Totbal,'')  as nvarchar(250))+','+ '3.Date'+','+ cast(isnull(@COLUMN05,'')   as nvarchar(250))+','+ '4.Account'+','+ cast(isnull(@COLUMN12,'') as nvarchar(250))+','+'5.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +','+'6.Previous Amount'+','+ISNULL(cast(isnull(@pbalance,'') as nvarchar(250)),'')+  '  '))
	update FITABLE001 set COLUMN10=(cast(isnull(@balance,0) as decimal(18,2)) -cast(isnull(@Totbal,0) as decimal(18,2))+cast(isnull(@pbalance,0) as decimal(18,2))),COLUMN09=@COLUMN05 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Header Update:Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
end

DECLARE @id nvarchar(250),@hid nvarchar(250),@hMaxRownum nvarchar(250),@hInitialrow nvarchar(250), @DATE nvarchar(250), @vendor nvarchar(250), @memo nvarchar(250),@cretAmnt decimal(18,2),
 @TYPE nvarchar(250),@NUMBER NVARCHAR(250), @Remainbal decimal(18,2), @TYPEID nvarchar(250), @Acc nvarchar(250),@ACCNAME nvarchar(250)
set @TYPEID= (select COLUMN04 from FITABLE045 where column02=@column02)
set @Project= (select COLUMN09 from FITABLE045 where column02=@column02)
set @NUMBER= (select COLUMN01 from FITABLE045 where column02=@column02)
set @vendor= (select column06 from FITABLE045 where column02=@column02)
set @DATE=(select column05 from FITABLE045 where column02=@column02)
  set @hInitialrow=(1)
  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE046 where COLUMN09 in(@internalid) and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @hid
      SET @hMaxRownum = (SELECT COUNT(*) FROM FITABLE046 where COLUMN09 in(@internalid) and isnull(COLUMNA13,'False')='False')
         WHILE @hInitialrow <= @hMaxRownum
         BEGIN  
		 set @COLUMN07=(SELECT sum(isnull(COLUMN07,0)) from FITABLE046 where COLUMN09 in(@internalid))    
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET8***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Payment Updated Data in FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+'1.Header Internal ID' +','+cast(isnull(@internalid,'') as nvarchar(250))+','+'2.Payment Amount'+','+ cast(isnull(@COLUMN07,'') as nvarchar(250))+','+  '3.Employee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(@COLUMN07!= '0' and @COLUMN07!= '')
	begin
	declare @AdvIDs nvarchar(250)
	--set @AdvIDs=(select COLUMN11 from FITABLE046 where COLUMN02=@hid)
	DECLARE @MaxRownum INT
     DECLARE @Initialrow INT=1
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id!='' and @id is not null)
		     BEGIN 
		        if(@COLUMN07!='0' and cast(@COLUMN07 as decimal(18,2))>0)
			  begin
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0)
		         WHILE @Initialrow <= @MaxRownum
			  begin
			  if(cast(@COLUMN07 as DECIMAL(18,2))!=0 and @MaxRownum>0)
			  begin
				set @cretAmnt=(select column10 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET9***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 'Header Update:Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN07 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(@cretAmnt), COLUMN16='OPEN'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				set @COLUMN07=(@COLUMN07-@cretAmnt)
				enD
				 else if(cast(@COLUMN07 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
					update FITABLE020 set column18=(cast(isnull(@COLUMN07,0) as DECIMAL(18,2))+cast(isnull(column18,0) as DECIMAL(18,2))), COLUMN16='OPEN'  where column02= @id
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				set @COLUMN07=(0)
				 end
				 end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1
				end
			END
			end
				CLOSE curA 
				deallocate curA
		end	   
		declare @padv decimal(18,2), @pamt decimal(18,2), @dueamt decimal(18,2)
set @padv=(select COLUMN07 from FITABLE046 where COLUMN02=@hid);
set @pamt=(select COLUMN08 from FITABLE046 where COLUMN02=@hid);
set @dueamt=(select COLUMN06 from FITABLE046 where COLUMN02=@hid);
set @dueamt=(cast(isnull(@dueamt,0) as DECIMAL(18,2))+cast(isnull(@padv,0) as DECIMAL(18,2))+cast(isnull(@pamt,0 )as DECIMAL(18,2)));
--update FITABLE046 set COLUMN06=@dueamt where COLUMN02=@hid
if(isnull(@padv,0)> 0)
begin
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
--delete from FITABLE026 where column05=@NUMBER and column04='Expense' and column09=@TYPEID and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where COLUMN09=@TYPEID and COLUMN03='Expense' and COLUMN10=1120 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
		FETCH NEXT FROM cur1 INTO @hid
             SET @hInitialrow = @hInitialrow + 1 
			 END
        CLOSE cur1 
UPDATE FITABLE046 SET COLUMNA13=1 WHERE COLUMN09 in(@NUMBER)
set @ReturnValue = 1
END
 

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE045 SET COLUMNA13=1 WHERE COLUMN02=@COLUMN02
      set @COLUMNA02=(select COLUMNA02 from FITABLE045 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA03=(select COLUMNA03 from FITABLE045 WHERE COLUMN02 = @COLUMN02)
	  set @COLUMN09=(select COLUMN01 from FITABLE045 WHERE COLUMN02 = @COLUMN02)
set @TYPEID= (select COLUMN04 from FITABLE045 where COLUMN02 = @COLUMN02)
set @Project= (select COLUMN09 from FITABLE045 where COLUMN02 = @COLUMN02)
set @NUMBER= (select COLUMN01 from FITABLE045 where  COLUMN02 = @COLUMN02)
set @vendor= (select column06 from FITABLE045 where COLUMN02 = @COLUMN02)
set @DATE=(select column05 from FITABLE045 where COLUMN02 = @COLUMN02)
set @internalid= (select COLUMN01 from FITABLE045 where COLUMN02 = @COLUMN02)
set @Totbal=(select COLUMN19 from FITABLE045 where COLUMN02 = @COLUMN02)
set @COLUMN19=(select COLUMN19 from FITABLE045 where COLUMN02 = @COLUMN02)
set @Acc=(select COLUMN12 from FITABLE045 where COLUMN02 = @COLUMN02)
set @paymenttype=(select COLUMN15 from FITABLE045 where COLUMN02 = @COLUMN02)
set @COLUMN20=(select COLUMN20 from FITABLE045 where COLUMN02 = @COLUMN02)
set @pbalance= (select COLUMN19 from FITABLE045 where COLUMN02=@COLUMN02)
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN12 and COLUMNA03=@COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Delete:Accounts Payable Deleted Records in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   'Accounts Payable ID' +','+'2000'+','+ 'Header Internal ID'+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+'Type'+','+ cast('EXPENSE' as nvarchar(250))+','+'Project'+','+ cast(isnull(@Project,'') as nvarchar(250))+','+  'Transaction NO'+','+cast(isnull(@TYPEID,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
delete from fitable064 where column03 in( 'EXPENSE','PDC ISSUED') and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
delete from  PUTABLE016  where COLUMN03=2000 and COLUMN05 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Delete:Accounts Payable Records are Deleted In PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''))
update FITABLE044 set  COLUMN18=(@column02),COLUMN19=(@internalid) where column02=@COLUMN20 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
if(@paymenttype=22627)
begin
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	  'Header Delete:Current Liabilities Deleted Records for PDC in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
	  'The Values are '+
	  '1.Type' +','+'PDC ISSUED'+','+'2.Internal Id' +','+ cast(isnull(@internalid,'') as nvarchar(250))+','+'3.Transaction No' +','+  cast(isnull(@TYPEID,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
		delete from  FITABLE026  where COLUMN04='PDC ISSUED' and  COLUMN05 =@internalid and  COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	  'Header Delete:Current Liabilities Records are Deleted for PDC in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
  end
  else
  begin  
	if(@COLUMN19!='' and (@Project!='' and @Project!=0))
	begin
	set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Delete:Project Actualcost Updation for PRTABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are 1.Project Actualcost'+
	   cast(isnull(@Projectactualcost,'') as nvarchar(250)) +','+  '2.Expense Amount'+','+  cast(isnull(@Totbal,'')  as nvarchar(250))+','+ '3.Project'+','+ cast( isnull(@Project,'')  as nvarchar(250))+','+'4.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') + '  '))
	update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))-cast(@Totbal as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Header Delete:Project Actualcost Updated in PRTABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
  end
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Delete:Bank Register Deleted Records in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   'Account ID' +','+cast(isnull(@Acc,'') as nvarchar(250))+','+ 'Header Internal ID'+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+'Type'+','+ cast('EXPENSE' as nvarchar(250))+','+'Project'+','+ cast(isnull(@Project,'') as nvarchar(250))+','+  'Transaction NO'+','+cast(isnull(@TYPEID,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	delete from  PUTABLE019  where COLUMN03=@Acc and COLUMN08 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN05=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	delete from  FITABLE052  where COLUMN03=@Acc and COLUMN09 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN05=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Delete:Bank Register Records are Deleted In PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
	  set @balance=(select column10 from FITABLE001 where COLUMN02=@Acc and COLUMNA03=@COLUMNA03)
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	   'Header Delete:Chart Of Accounts Balance Updation While Deletion for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
	   'The Values are 1.Balance Amount'+ cast(isnull(@balance,'') as nvarchar(250)) +','+  '2.Expense Amount'+','+  cast(isnull(@Totbal,'')  as nvarchar(250))+','+ '3.Date'+','+ cast(isnull(@DATE,'')   as nvarchar(250))+','+ '4.Account'+','+ cast(isnull(@Acc,'')   as nvarchar(250))+','+'5.Account Owner'+','+ISNULL(cast(isnull(@COLUMNA03,'') as nvarchar(250)),'') +  '  '))
	update FITABLE001 set COLUMN10=(cast(isnull(@balance,0) as decimal(18,2)) +cast(isnull(@Totbal,0) as decimal(18,2))) where COLUMN02=@Acc and COLUMNA03=@COLUMNA03
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Header Delete:Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end

set @hInitialrow=(1)
  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE046 where COLUMN09 in(@internalid) and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @hid
      SET @hMaxRownum = (SELECT COUNT(*) FROM FITABLE046 where COLUMN09 in(@internalid) and isnull(COLUMNA13,'False')='False')
         WHILE @hInitialrow <= @hMaxRownum
         BEGIN  
		 set @AdvIDs=(select COLUMN11 from FITABLE046 where COLUMN02=@hid)
		 set @COLUMN07=(SELECT sum(isnull(COLUMN07,0)) from FITABLE046 where COLUMN09 in(@internalid))  
     set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Delete:Expense Payment Updated Data in FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+'1.Header Internal ID' +','+cast(isnull(@internalid,'') as nvarchar(250))+','+'2.Payment Amount'+','+ cast(isnull(@COLUMN07,'') as nvarchar(250))+','+  '3.Employee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(@COLUMN07!= '0' and @COLUMN07!= '')
	begin
	
      set @Initialrow =(1)
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id!='' and @id is not null)
		     BEGIN 
		        if(@COLUMN07!='0' and cast(@COLUMN07 as decimal(18,2))>0)
			  begin
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0)
		         WHILE @Initialrow <= @MaxRownum
			  begin
			  if(cast(@COLUMN07 as DECIMAL(18,2))!=0 and @MaxRownum>0)
			  begin
				set @cretAmnt=(select column10 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 'Header Delete:Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN07 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(@cretAmnt), COLUMN16='OPEN'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				set @COLUMN07=(@COLUMN07-@cretAmnt)
				enD
				 else if(cast(@COLUMN07 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
				 declare @BALcretAmnt DECIMAL(18,2)
				 set @BALcretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 if(@BALcretAmnt!=@cretAmnt)
				 begin
					update FITABLE020 set column18=(cast(isnull(@COLUMN07,0) as DECIMAL(18,2))+cast(isnull(@BALcretAmnt,0) as DECIMAL(18,2))), COLUMN16='OPEN'  where column02= @id
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				set @COLUMN07=(0)
				 end
				 end
				 end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1
				end
			END
			end
				CLOSE curA 
				deallocate curA
		end		
set @padv=(select COLUMN07 from FITABLE046 where COLUMN02=@hid);
set @pamt=(select COLUMN08 from FITABLE046 where COLUMN02=@hid);
set @dueamt=(select COLUMN06 from FITABLE046 where COLUMN02=@hid);
set @dueamt=(cast(isnull(@dueamt,0) as DECIMAL(18,2))+cast(isnull(@padv,0) as DECIMAL(18,2))+cast(isnull(@pamt,0 )as DECIMAL(18,2)));
update FITABLE046 set COLUMN06=@dueamt where COLUMN02=@hid
if(isnull(@padv,0)> 0)
begin
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
--delete from FITABLE026 where column05=@NUMBER and column04='Expense' and column09=@TYPEID and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where COLUMN09=@TYPEID and COLUMN03='Expense' and COLUMN10=1120 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
 UPDATE FITABLE046 SET COLUMNA13=1 WHERE COLUMN02=@hid
		FETCH NEXT FROM cur1 INTO @hid
             SET @hInitialrow = @hInitialrow + 1 
			 END
        CLOSE cur1 
		deallocate cur1	 
END
 exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE045.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE045.txt',0
end catch
end




GO
