USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TR_GetItem1]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TR_GetItem1]
(
	
	@PurchaseOrderID int
)

AS
BEGIN
	
		SELECT a.COLUMN03
	FROM dbo.PUTABLE004 a
	LEFT JOIN dbo.PUTABLE003 po on po.COLUMN01 = a.COLUMN12
	WHERE a.COLUMN12 = (select COLUMN01 FROM dbo.PUTABLE003 WHERE COLUMN02= @PurchaseOrderID)
END













GO
