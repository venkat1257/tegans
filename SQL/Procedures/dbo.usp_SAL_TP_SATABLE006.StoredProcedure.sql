USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE006]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_SATABLE006] 
(@COLUMN02      NVARCHAR(250),        @COLUMN03      NVARCHAR(250)=NULL,     @COLUMN04      NVARCHAR(250)= NULL, 
 @COLUMN05      NVARCHAR(max)=NULL,   @COLUMN06      NVARCHAR(250)= NULL,    @COLUMN07      NVARCHAR(250)=NULL , 
 @COLUMN08      NVARCHAR(250)= NULL,  @COLUMN09      NVARCHAR(250)=NULL,     @COLUMN10      NVARCHAR(250)= NULL, 
 @COLUMN11      NVARCHAR(250)=NULL ,  @COLUMN12      NVARCHAR(250)= NULL,    @COLUMN13      NVARCHAR(250)=NULL , 
 @COLUMN14      NVARCHAR(250)= NULL,  @COLUMN15      NVARCHAR(250)=NULL ,    @COLUMN16      NVARCHAR(250)= NULL, 
 @COLUMN17      NVARCHAR(250)=NULL,   @COLUMN18      NVARCHAR(250)= NULL,    @COLUMN19      NVARCHAR(250)=NULL, 
 @COLUMN20      NVARCHAR(250)= NULL,  @COLUMN21      NVARCHAR(250)=NULL,     @COLUMN22      NVARCHAR(250)= NULL, 
 @COLUMN23      NVARCHAR(250)=NULL,   @COLUMN24      NVARCHAR(250)= NULL,    @COLUMN25      NVARCHAR(250)=NULL , 
 @COLUMN26      NVARCHAR(250)=NULL,   @COLUMN27      NVARCHAR(250)=NULL,     @COLUMN28      NVARCHAR(250)=NULL, 
 @COLUMN29      NVARCHAR(250)=NULL,   @COLUMN30      NVARCHAR(250)=NULL,     @COLUMN31      NVARCHAR(250)=NULL,
 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
 @COLUMN32      NVARCHAR(250)=null,   @COLUMN33      NVARCHAR(250)=null,     @COLUMN34      NVARCHAR(250)=null,
 @COLUMN35      NVARCHAR(250)=null,   @COLUMN36      NVARCHAR(250)=null,     @COLUMN37      NVARCHAR(250)=null, 
 @COLUMN38      NVARCHAR(250)=null, 
 @COLUMNA01     VARCHAR(100)=NULL,    @COLUMNA02     VARCHAR(100)=NULL,      @COLUMNA03     VARCHAR(100)=NULL, 
 @COLUMNA04     VARCHAR(100)=NULL,    @COLUMNA05     VARCHAR(100)=NULL,      @COLUMNA06     NVARCHAR(250)=NULL, 
 @COLUMNA07     NVARCHAR(250)=NULL,   @COLUMNA08     VARCHAR(100)=NULL,      @COLUMNA09     NVARCHAR(250)=NULL, 
 @COLUMNA10     NVARCHAR(250)= NULL,  @COLUMNA11     VARCHAR(100)=NULL,      @COLUMNA12     NVARCHAR(250)=NULL , 
 @COLUMNA13     NVARCHAR(250)= NULL,  @COLUMNB01     NVARCHAR(250)=NULL,     @COLUMNB02     NVARCHAR(250)= NULL, 
 @COLUMNB03     NVARCHAR(250)=NULL ,  @COLUMNB04     NVARCHAR(250)= NULL,    @COLUMNB05     NVARCHAR(250)=NULL, 
 @COLUMNB06     NVARCHAR(250)= NULL,  @COLUMNB07     NVARCHAR(250)=NULL ,    @COLUMNB08     NVARCHAR(250)= NULL, 
 @COLUMNB09     NVARCHAR(250)=NULL,   @COLUMNB10     NVARCHAR(250)= NULL,    @COLUMNB11     VARCHAR(100)=NULL, 
 @COLUMNB12     VARCHAR(100)=NULL,    @COLUMND01     NVARCHAR(250)=NULL ,    @COLUMND02     NVARCHAR(250)=NULL, 
 @COLUMND03     NVARCHAR(250)=NULL ,  @COLUMND04     NVARCHAR(250)= NULL,    @COLUMND05     NVARCHAR(250)=NULL , 
 @COLUMND06     NVARCHAR(250)= NULL,  @COLUMND07     NVARCHAR(250)=NULL ,    @COLUMND08     NVARCHAR(250)=NULL, 
 @COLUMND09     NVARCHAR(250)=NULL ,  @COLUMND10     VARCHAR(100)=NULL,      @Direction     NVARCHAR(250), 
 @TabelName     NVARCHAR(250)=NULL ,  @ReturnValue   NVARCHAR(250)= NULL,    @TComitData    decimal(18,2)=NULL, 
 @ComitData     decimal(18,2) =NULL,  @WIP           decimal(18,2) =NULL,    @QOH           decimal(18,2) =NULL, 
 @Qty_Avl       decimal(18,2)=NULL,   @BackOrderData decimal(18,2)=NULL) 
AS 
  BEGIN 
      BEGIN try 
      --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
	  set @COLUMN27=(select iif((@COLUMN27!='' and @COLUMN27>0),@COLUMN27,(select max(column02) from fitable037 where column07=1 and column08=1)))
          IF @Direction = 'Insert' 
            BEGIN
	    --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @FRMID int, @location nvarchar(250),@uom nvarchar(250),@lot nvarchar(250), @Project nvarchar(250)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE006_SequenceNo
set @FRMID= (SELECT COLUMN03 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN20= (SELECT COLUMN24 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN21= (SELECT COLUMN25 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN22= (SELECT COLUMN26 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN23= (SELECT COLUMN27 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @location=(select COLUMN48 from SATABLE005 WHERE  COLUMN01=@COLUMN19)
set @Location = (case when (cast(isnull(@COLUMN38,'') as nvarchar(250))!='' and @COLUMN38!=0) then @COLUMN38 else @Location end)
set @location=(iif(@location='',0,isnull(@location,0)))
set @lot=(iif(@COLUMN17='',0,isnull(@COLUMN17,0)))
set @uom=(iif(@COLUMN27='',10000,isnull(@COLUMN27,10000)))    
set @Project = (SELECT COLUMN35 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
         
--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
--set @COLUMN06= (select sum(isnull(COLUMN08,0)) from FITABLE010 where COLUMN03=@COLUMN03 and COLUMNA02=@COLUMN20 and COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0))
  set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0 ) THEN (select COLUMN24 from SATABLE005
  where COLUMN01=@COLUMN19) else @COLUMNA02  END )
insert into SATABLE006 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
   COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN27,  @COLUMN07,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10,
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08,
   @COLUMND09, @COLUMND10
) 
if exists(select column01 from SATABLE006 where column19=@column19)
					begin 
declare @Type nvarchar(250)
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
declare @TrackQty bit
 set @COLUMN15=(SELECT CAST(GETDATE() AS DATE));
 set @COLUMN02=(select COLUMN01 from SATABLE005 where COLUMN01=@COLUMN19);
 --set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@COLUMN19);
        --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	set @ComitData=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04) 
	set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04) 
	set @QOH=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04)
	set @BackOrderData=(select isnull(COLUMN06,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04)
	--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
if not exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04)
begin 
declare @tmpnewID1 int
declare @newID int
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
insert into FITABLE010 
(
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
(
  @newID,@COLUMN03,'0',@COLUMN07, '0','0', @COLUMN20,@uom,@location,@lot,@COLUMNA02,@COLUMNA03,@Project,@COLUMN04 
)
end
end
--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin
if(@ComitData>=0)
	begin
		if(@QOH<cast(@COLUMN07  as decimal(18,2)))
			begin
				set @WIP=@WIP+cast(@COLUMN07  as decimal(18,2))
				set @TComitData=@ComitData+cast(@COLUMN07  as decimal(18,2))
				set @BackOrderData=(cast(@COLUMN07  as decimal(18,2))-cast(@QOH  as decimal(18,2)))+cast(@BackOrderData  as decimal(18,2))
                set @Qty_Avl=0
				if(@FRMID=1283)
					begin
					        --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
								--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
							set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
						update  FITABLE010 set
						COLUMN18=@WIP where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) and COLUMN24=@COLUMN04
						end
					end
				else
					begin
						--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
							--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
						set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
						update  FITABLE010 set
						COLUMN05=@TComitData,COLUMN06=@BackOrderData  where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
						end
					end
			end
		else
			begin
				set @WIP=cast(@WIP  as decimal(18,2))+cast(@COLUMN07  as decimal(18,2))
		set @TComitData=cast(@ComitData  as decimal(18,2))+cast(@COLUMN07  as decimal(18,2))
		set @Qty_Avl=(@QOH)
		if(@Qty_Avl<0)
		begin
		set @Qty_Avl=0
		set @BackOrderData=(cast(@COLUMN07  as decimal(18,2))-cast(@QOH  as decimal(18,2)))+cast(@BackOrderData  as decimal(18,2))
		end
				if(@FRMID=1283)
					begin
					        --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
							--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
						set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
						update  FITABLE010 set
						COLUMN18=@WIP where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
						end
					end
				else
					begin
						--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
							--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
						set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
						update  FITABLE010 set
						COLUMN05=@TComitData,COLUMN06= @BackOrderData where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
						end
					end
	end
end
else
	begin set @TComitData=cast(@COLUMN07  as decimal(18,2))
		set @Qty_Avl=(@QOH)
		--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
			--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
		set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
		if(@TrackQty=1)
		begin
		update  FITABLE010 set
			COLUMN05=@TComitData,COLUMN06=@BackOrderData,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
		end
end
end
insert into PUTABLE013 
( 
   COLUMN02,   COLUMN04,  COLUMN05,  COLUMN08,  COLUMN13,  COLUMN14,  COLUMN15,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
   COLUMND09, COLUMND10
)
values
(  
   @COLUMN02,    @COLUMN04,  @COLUMN06,   @COLUMN07,    @COLUMN14,    @COLUMN03,  @COLUMN15,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, 
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, 
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05,
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
update  PUTABLE013 set
COLUMN03='Sales Order'  WHERE COLUMN04 LIKE 'SO%' AND COLUMN02=@COLUMN02
update  PUTABLE013 set
COLUMN03='Job Order'  WHERE COLUMN04 LIKE 'JO%' AND COLUMN02=@COLUMN02
set @ReturnValue = 1
end
else
begin
return 0
end
END
          ELSE IF @Direction = 'Select' 
            BEGIN 
                SELECT * 
                FROM   satable006 
            END 
          ELSE IF @Direction = 'Update' 
            BEGIN 
                DECLARE @IQTY decimal(18,2)
                DECLARE @OredrType nvarchar(250)
		--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @location=(select COLUMN48 from SATABLE005 WHERE  COLUMN01=(@COLUMN19))
set @location=(iif(@location='',0,isnull(@location,0)))
set @lot=(iif(@COLUMN17='',0,isnull(@COLUMN17,0)))
set @uom=(iif(@COLUMN27='',10000,isnull(@COLUMN27,10000)))
set @Project = (select COLUMN35 from SATABLE005 where COLUMN01=@COLUMN19)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)

   if  exists( SELECT 1 FROM SATABLE006 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
   begin
   set @COLUMN19 =(select COLUMN19 from  SATABLE006 Where COLUMN02=@COLUMN02 );
   end
   SET @IQTY=(select isnull(COLUMN07,0) from  SATABLE006 Where COLUMN02=@COLUMN02)
   set @COLUMN20 =(select COLUMN24 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMN21 =(select COLUMN25 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMN22 =(select COLUMN26 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMN23 =(select COLUMN27 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @OredrType =(select COLUMN29 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0) THEN 
   (select COLUMN24 from SATABLE005 where COLUMN01=@COLUMN19) else @COLUMNA02  END ) 
if not exists( SELECT 1 FROM SATABLE006 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
begin
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE006_SequenceNo
                      INSERT INTO SATABLE006
                         (COLUMN02, COLUMN03, COLUMN04, COLUMN05, COLUMN06, COLUMN07, COLUMN08, COLUMN09, COLUMN10, COLUMN11, COLUMN12, COLUMN13, 
                         COLUMN14, COLUMN15, COLUMN16, COLUMN17, COLUMN18, COLUMN19, COLUMN20, COLUMN21, COLUMN22, COLUMN23, COLUMN24, COLUMN25, 
                         --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
			 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		         COLUMN26,COLUMN27,COLUMN28,  COLUMN29, COLUMN30, COLUMN31,COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36,  COLUMN37, COLUMN38, COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, 
                         COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
                         COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, 
                         COLUMND08, COLUMND09, COLUMND10)
			 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
VALUES        (@COLUMN02,@COLUMN03,@COLUMN04,@COLUMN05,@COLUMN06,@COLUMN07,@COLUMN08,@COLUMN09,@COLUMN10,@COLUMN11,@COLUMN12,@COLUMN13,@COLUMN14,@COLUMN15,@COLUMN16,@COLUMN17,@COLUMN18,@COLUMN19,@COLUMN20,@COLUMN21,@COLUMN22,@COLUMN23,@COLUMN24,@COLUMN25,@COLUMN26,@COLUMN27,@COLUMN27,  @COLUMN07, @COLUMN30, @COLUMN31,@COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMNA01,@COLUMNA02,@COLUMNA03,@COLUMNA04,@COLUMNA05,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMNA09,@COLUMNA10,@COLUMNA11,@COLUMNA12,@COLUMNA13,@COLUMNB01,@COLUMNB02,@COLUMNB03,@COLUMNB04,@COLUMNB05,@COLUMNB06,@COLUMNB07,@COLUMNB08,@COLUMNB09,@COLUMNB10,@COLUMNB11,@COLUMNB12,@COLUMND01,@COLUMND02,@COLUMND03,@COLUMND04,@COLUMND05,@COLUMND06,@COLUMND07,@COLUMND08,@COLUMND09,@COLUMND10)
                      --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
                      SET @ComitData=CAST(ISNULL((SELECT Isnull(column05, 0) 
                                      FROM   fitable010 
                                      WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04),-1) as decimal(18,2))  
                      SET @WIP=CAST(ISNULL((SELECT Isnull(column18, 0) 
                                FROM   fitable010 
                                WHERE  column03 = @COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04),-1) as decimal(18,2))
                      SET @QOH=CAST(ISNULL((SELECT Isnull(column04, 0) 
                                FROM   fitable010 
                                WHERE  column03 = @COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04),-1) as decimal(18,2)) 
                      SET @BackOrderData=CAST(ISNULL((SELECT Isnull(column06, 0) 
                                          FROM   fitable010 
                                          WHERE  column03 = @COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04),-1) as decimal(18,2)) 
 --EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
if not exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04)
begin 
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
insert into FITABLE010 
(
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
(
  @newID,@COLUMN03,'0',@COLUMN07, '0','0', @COLUMN20,@uom,@location,@lot,@COLUMNA02,@COLUMNA03 ,@Project,@COLUMN04
)
end
end
--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin
                      IF( @ComitData >= 0 ) 
                        BEGIN 
                            IF( @QOH < Cast(@COLUMN07 as decimal(18,2)) ) 
                              BEGIN 
                                  SET @WIP=@WIP + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @TComitData=@ComitData + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @BackOrderData=( Cast(@COLUMN07 as decimal(18,2)) 
                                                       - @QOH ) 
                                                     + 
                                                     @BackOrderData 
                                  SET @Qty_Avl=0 
				  --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
                                  IF( @FRMID = 1283 ) 
                                    BEGIN 
										--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
									set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
									if(@TrackQty=1)
									begin
                                        UPDATE fitable010 
                                        SET    column18 = @WIP 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
										end
                                    END 
                                  ELSE 
                                    BEGIN 
										--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
									set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
									if(@TrackQty=1)
									begin
                                        UPDATE fitable010 
                                        SET    column05 = @TComitData, 
                                               column06 = @BackOrderData 
                                               --,column08 = @Qty_Avl 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
										end
                                    END 
                              END 
                            ELSE 
                              BEGIN 
                                  SET @WIP=@WIP + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @TComitData=@ComitData + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @Qty_Avl=@QOH

                                  IF( @Qty_Avl < 0 ) 
                                    BEGIN 
                                        SET @Qty_Avl=0 
                                        SET @BackOrderData=( Cast( 
                                        @COLUMN07 as decimal(18,2) 
                                                             ) 
                                                             - @QOH 
                                                           ) 
                                                           + 
                                                           @BackOrderData 
                                    END 
				  --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
                                  IF( @FRMID = 1283 ) 
                                    BEGIN 
										--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
									set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
									if(@TrackQty=1)
									begin
                                        UPDATE fitable010 
                                        SET    column18 = @WIP 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
										end
                                    END 
                                  ELSE 
                                    BEGIN 
										--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
									set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
									if(@TrackQty=1)
									begin
                                        UPDATE fitable010 
                                        SET    column05 = @TComitData, 
                                               column06 = @BackOrderData 
                                               --,column08 = @Qty_Avl 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
										end
                                    END 
                              END 
                        END 
                      ELSE 
                        BEGIN 
                            SET @TComitData=Cast(@COLUMN07 as decimal(18,2)) 
                            SET @Qty_Avl=@QOH  
			    --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
                            set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
							UPDATE fitable010 
                            SET    column05 = @TComitData, 
                                   column06 = @BackOrderData 
                                   --,column08 = @Qty_Avl 
                            WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
							end
                        END 
                  END 
end
                ELSE 
                  BEGIN 
                      UPDATE       SATABLE006
SET                COLUMN02 = @COLUMN02, COLUMN03 = @COLUMN03, COLUMN04 = @COLUMN04, COLUMN05 = @COLUMN05, COLUMN07 = @COLUMN07, 
                         COLUMN08 = @COLUMN08, COLUMN09 = @COLUMN09, COLUMN10 = @COLUMN10, COLUMN11 = @COLUMN11, COLUMN14 = @COLUMN14, 
                         COLUMN15 = @COLUMN15, COLUMN16 = @COLUMN16, COLUMN17 = @COLUMN17, COLUMN18 = @COLUMN18, COLUMN19 = @COLUMN19, 
                         COLUMN20 = @COLUMN20, COLUMN21 = @COLUMN21, COLUMN22 = @COLUMN22, COLUMN23 = @COLUMN23, COLUMN24 = @COLUMN24, 
                         --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
			             COLUMN25 =  @COLUMN25,  COLUMN26 =  @COLUMN26,  COLUMN27 =  @COLUMN27,  COLUMN28  = @COLUMN27,  COLUMN29 =@COLUMN07, 
				     --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
						 COLUMN30 =  @COLUMN30,  COLUMN31=   @COLUMN31,  COLUMN32=   @COLUMN32,  COLUMN33  = @COLUMN33,  COLUMN34=   @COLUMN34,  
						 COLUMN35=   @COLUMN35,  COLUMN36=   @COLUMN36,  COLUMN37=   @COLUMN37,  COLUMNA01 = @COLUMNA01, COLUMNA02 = @COLUMNA02, COLUMNA03 = @COLUMNA03, 
                         COLUMNA04 = @COLUMNA04, COLUMNA05 = @COLUMNA05, COLUMNA07 = @COLUMNA07, COLUMNA08 = @COLUMNA08, 
                         COLUMNA09 = @COLUMNA09, COLUMNA10 = @COLUMNA10, COLUMNA11 = @COLUMNA11, COLUMNA12 = @COLUMNA12, COLUMNA13 = @COLUMNA13, 
                         COLUMNB01 = @COLUMNB01, COLUMNB02 = @COLUMNB02, COLUMNB03 = @COLUMNB03, COLUMNB04 = @COLUMNB04, COLUMNB05 = @COLUMNB05, 
                         COLUMNB06 = @COLUMNB06, COLUMNB07 = @COLUMNB07, COLUMNB08 = @COLUMNB08, COLUMNB09 = @COLUMNB09, COLUMNB10 = @COLUMNB10, 
                         COLUMNB11 = @COLUMNB11, COLUMNB12 = @COLUMNB12, COLUMND01 = @COLUMND01, COLUMND02 = @COLUMND02, COLUMND03 = @COLUMND03, 
                         COLUMND04 = @COLUMND04, COLUMND05 = @COLUMND05, COLUMND06 = @COLUMND06, COLUMND07 = @COLUMND07, COLUMND08 = @COLUMND08, 
                         COLUMND09 = @COLUMND09, COLUMND10 = @COLUMND10
WHERE        (COLUMN02 = @COLUMN02)
--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
 SET @COLUMN05=((SELECT isnull(COLUMN05,0) FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20  AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04)+CAST(@COLUMN07 AS DECIMAL))
			--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
		set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
		if(@TrackQty=1)
		begin
		UPDATE FITABLE010 SET COLUMN05 =@COLUMN05 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@COLUMN04
		end
END

 set @COLUMN15=(SELECT COLUMN06 from  SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN02=(select COLUMN01 from  SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from  SATABLE005 where COLUMN01=@COLUMN19);
 --set @COLUMN04=(select COLUMN04 from  SATABLE005 where COLUMN01=@COLUMN19);
 if( @COLUMN04 not like 'SQ%') 
 begin  
IF (@OredrType !='1001')
 begin
 set @Type=('Purchase Return Order')
 end
 else
 begin
 set @Type=('Sales Order')
 end
insert into PUTABLE013 
( 
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN08,  COLUMN13,  COLUMN14,  COLUMN15,
   --COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,   COLUMN07,  COLUMN11,  COLUMN12,    COLUMN14,  COLUMN15, COLUMN16, 
   --COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,   COLUMN21,  COLUMN22,  COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09,
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08,
   COLUMND09, COLUMND10
)
values
(  
   @COLUMN02,  @Type,  @COLUMN04,  @COLUMN06,   @COLUMN07,    @COLUMN14,    @COLUMN03,  @COLUMN15,
   --  @COLUMN03,  @COLUMN04,  @COLUMN05,   @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN15,
   --@COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, 
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, 
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05,
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  

                      SET @ReturnValue = 1 

                  END 
            END 
          ELSE IF @Direction = 'Delete' 
            BEGIN 
                SET @IQTY=(select COLUMN07 from  SATABLE006 Where COLUMN02=@COLUMN02 )
   set @COLUMN19 =(select COLUMN19 from  SATABLE006 Where COLUMN02=@COLUMN02 );
   --set @COLUMN04=(select COLUMN04 from  SATABLE005 where COLUMN01=@COLUMN19);
   set @COLUMN20 =(select COLUMN24 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
   SET @COLUMN07=((SELECT COLUMN05 FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND  COLUMN24=@COLUMN04 and COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 and COLUMN24=@COLUMN04)-@IQTY)
   	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
   set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
	if(@TrackQty=1)
	begin
   UPDATE FITABLE010 SET COLUMN05 =@COLUMN07 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 and COLUMN24=@COLUMN04
   end
update PUTABLE013 set COLUMNA13=@COLUMNA13 where COLUMN04=@COLUMN04 and COLUMN02=@COLUMN19
UPDATE SATABLE006 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
            END 
      END try 

      BEGIN catch 
	declare @tempSTR nvarchar(max)	
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_SATABLE006.txt',0
          IF NOT EXISTS(SELECT column01 
                        FROM   satable006 
                        WHERE  column19 = @column19) 
            BEGIN 
                DELETE satable005 
                WHERE  column01 = @COLUMN19 

                RETURN 0 
            END 

          RETURN 0 

          RETURN 0 
      END catch 
  END 





GO
