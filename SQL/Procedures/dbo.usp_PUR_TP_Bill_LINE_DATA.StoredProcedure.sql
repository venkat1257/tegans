USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_Bill_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_Bill_LINE_DATA]
(
	@PurchaseOrderID int= null,
	@Form varchar(50)
)
AS
BEGIN
   declare @tmpVal int
   declare @tmpVal1 int
   declare @tmpVal2 int
   if(@Form='0')
   BEGIN
   --EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation Bill
   set @tmpVal=(select isnull(sum(COLUMN12),0) from PUTABLE002 WHERE COLUMN19 = (SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and COLUMNA13=0)
   set @tmpVal2=(select isnull(sum(COLUMN07),0) from PUTABLE002 WHERE COLUMN19 = (SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and COLUMNA13=0)
   
   set @tmpVal1=(select isnull(sum(COLUMN13),0) from PUTABLE002 WHERE COLUMN19 = (SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and COLUMNA13=0)

   if(@tmpVal=0 and @tmpVal1=0)

   begin

   SELECT  a.COLUMN01 as COLUMN03, a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05,a.COLUMN15 as COLUMN06 , 

	 a.COLUMN07 as COLUMN07, 0 as COLUMN08,a.COLUMN07 as COLUMN09,

	a.COLUMN07 as COLUMN10,a.COLUMN09 as COLUMN11,a.COLUMN07*a.COLUMN09 as COLUMN12,a.COLUMN25 AS COLUMN18,a.COLUMN26 as COLUMN19
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(a.COLUMN07*a.COLUMN09 as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN17 as COLUMN27,a.COLUMN02 LineID,a.COLUMN32 COLUMN33,a.COLUMN37 COLUMN35,hs.COLUMN04 hsncode
	FROM dbo.PUTABLE002 a 
	 left join MATABLE013 m on  m.column02=a.COLUMN25
	inner join MATABLE007 m7 on a.COLUMN03=m7.COLUMN02 and a.COLUMNA03=m7.COLUMNA03
	left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	WHERE a.COLUMN19 = (SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and a.COLUMN07>0   and a.COLUMNA13='False'

   end

  else if(@tmpVal>0 and @tmpVal1=0)

   begin

  SELECT distinct b.COLUMN02 as COLUMN03,c.COLUMN03 as COLUMN04,c.COLUMN04 as COLUMN05,c.COLUMN05 as COLUMN06 , 

	 c.COLUMN06 as COLUMN07, 0  as COLUMN08,c.COLUMN08 as COLUMN09,
	c.COLUMN08 as COLUMN10,a.COLUMN09 as COLUMN11,cast((c.COLUMN08*c.COLUMN10) as decimal(18,2)) as COLUMN12,a.COLUMN25 AS COLUMN18,c.COLUMN17 as COLUMN19
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(c.COLUMN08*c.COLUMN10 as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,c.COLUMN24 as COLUMN27,c.COLUMN26 LineID,c.COLUMN10 COLUMN33,c.COLUMN28 COLUMN35 ,hs.COLUMN04 hsncode
	FROM PUTABLE004 c inner join PUTABLE003 b on b.COLUMN01=c.COLUMN12 and b.COLUMN06=@PurchaseOrderID and c.COLUMN08>0
	  and c.COLUMNA13='False'
	  --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	inner join dbo.PUTABLE002 a  on iif(isnull(c.COLUMN26,0)=0,0,isnull(a.COLUMN02,0))=isnull(c.COLUMN26,0) and c.COLUMN26=a.COLUMN02 or a.COLUMN19=(SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and a.COLUMN03=c.COLUMN03 and a.COLUMN26=c.COLUMN17 and iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))=iif(c.COLUMN24='',0,isnull(c.COLUMN24,0))
	left join MATABLE013 m on  m.column02=a.COLUMN25
	inner join MATABLE007 m7 on a.COLUMN03=m7.COLUMN02 and a.COLUMNA03=m7.COLUMNA03
	left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
   end

   else if(@tmpVal=0 and @tmpVal1>0)

   begin

   SELECT   0 as COLUMN03,a.COLUMN04 as COLUMN04,max(a.COLUMN05) as COLUMN05,max(a.COLUMN06) as COLUMN06 , 

	 max(a.COLUMN07) as COLUMN07, max(a.COLUMN08) as COLUMN08,min(a.COLUMN07-a.COLUMN08) as COLUMN09,

	min(a.COLUMN07-a.COLUMN08) as COLUMN10,a.COLUMN11 as COLUMN11,cast((min((a.COLUMN07-a.COLUMN08)*a.COLUMN11)) as decimal(18,2)) as COLUMN12,a.COLUMN18 AS COLUMN18,a.COLUMN19
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(min((a.COLUMN07-a.COLUMN08)*a.COLUMN11) as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN27,a.COLUMN34 LineID,a.COLUMN33,a.COLUMN35,hs.COLUMN04 hsncode
	FROM dbo.PUTABLE006 a inner join PUTABLE005 b on b.COLUMN06=@PurchaseOrderID   and a.COLUMNA13='False'
	left join MATABLE013 m on  m.column02=a.COLUMN18
	inner join MATABLE007 m7 on a.COLUMN04=m7.COLUMN02 and a.COLUMNA03=m7.COLUMNA03
	left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	WHERE a.COLUMN13 in (SELECT COLUMN01 FROM dbo.PUTABLE005 WHERE COLUMN06= @PurchaseOrderID) group by a.COLUMN04,a.COLUMN11,a.COLUMN19,a.COLUMN18,m.column17,a.COLUMN27,a.COLUMN34,a.COLUMN33,a.COLUMN35,hs.COLUMN04
	having min(a.COLUMN07-a.COLUMN08)>0
   end

   else if(@tmpVal>0 and @tmpVal1>0  and @tmpVal2!=@tmpVal1)

   begin

 --  SELECT   a.column03 as COLUMN03,a.COLUMN04 as COLUMN04,max(a.COLUMN05) as COLUMN05,max(a.COLUMN06) as COLUMN06 , 
	-- max(a.COLUMN07) as COLUMN07, max(a.COLUMN08) as COLUMN08,max(a.COLUMN07)-sum(a.COLUMN09) as COLUMN09,
	--max(a.COLUMN07)-sum(a.COLUMN09) as COLUMN10,max(a.COLUMN11) as COLUMN11,((max(a.COLUMN07)-sum(a.COLUMN09))*min(a.COLUMN11)) as COLUMN12,a.COLUMN18 AS COLUMN18,a.COLUMN19
	--,((cast(m.column17 as decimal)*(0.01))*(cast((max(a.COLUMN07)-sum(a.COLUMN09))*min(a.COLUMN11) as decimal))) COLUMN20
	--FROM dbo.PUTABLE006 a inner join PUTABLE005 b on b.COLUMN01=a.COLUMN13 and  b.COLUMN06=@PurchaseOrderID
	----left outer join PUTABLE003 c on c.COLUMN06=b.COLUMN06 inner join PUTABLE004 d on d.COLUMN12=c.COLUMN01
	--inner join MATABLE013 m on  m.column02=a.COLUMN18
	--WHERE a.COLUMN13 in (SELECT COLUMN01 FROM dbo.PUTABLE005 WHERE COLUMN06= @PurchaseOrderID)  
	--group by a.COLUMN04,a.COLUMN03,a.COLUMN19,a.COLUMN18,m.column17
	--having (max(a.COLUMN07)-sum(a.COLUMN09))>0

	SELECT  b.COLUMN02 as COLUMN03,c.COLUMN03 as COLUMN04,max(c.COLUMN04) as COLUMN05,max(c.COLUMN05) as COLUMN06 ,
	--EMPHCS855 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in bill 
	 max(c.COLUMN06) as COLUMN07, max(isnull(a.COLUMN08,0)) as COLUMN08,((c.COLUMN08) -isnull(sum( d.COLUMN09),0)) as COLUMN09,
	((c.COLUMN08) -isnull(sum( d.COLUMN09),0)) as COLUMN10,a.COLUMN09 as COLUMN11,cast((((c.COLUMN08) -isnull(sum( d.COLUMN09),0))*max(c.COLUMN10)) as decimal(18,2)) as COLUMN12,a.COLUMN25 AS COLUMN18,c.COLUMN17 as COLUMN19
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(((c.COLUMN08) -isnull(sum( d.COLUMN09),0))*max(c.COLUMN10) as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,c.COLUMN24 as COLUMN27,c.COLUMN26 LineID,c.COLUMN10 COLUMN33,c.COLUMN28 COLUMN35,hs.COLUMN04 hsncode
	FROM PUTABLE004 c inner join PUTABLE003 b on b.COLUMN01=c.COLUMN12 and b.COLUMN06=@PurchaseOrderID and c.COLUMN08>0  and c.COLUMNA13='False'
	--EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation Bill
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
        --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	inner join dbo.PUTABLE002 a  on  iif(isnull(c.COLUMN26,0)=0,0,isnull(a.COLUMN02,0))=isnull(c.COLUMN26,0) and a.COLUMN19=(SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and a.COLUMN03=c.COLUMN03 and a.COLUMN26=c.COLUMN17 and iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))=iif(c.COLUMN24='',0,isnull(c.COLUMN24,0))
	left outer join PUTABLE006 d  on  iif(isnull(d.COLUMN34,0)=0,0,isnull(a.COLUMN02,0))=isnull(d.COLUMN34,0) and d.COLUMN03=b.COLUMN02 and  c.COLUMN03=d.COLUMN04 and d.COLUMN19=c.COLUMN17 and d.COLUMN27=c.COLUMN24  and d.COLUMNA13='False' and d.COLUMNA13=0
	--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	 left join MATABLE013 m on  m.column02=a.COLUMN25
	inner join MATABLE007 m7 on c.COLUMN03=m7.COLUMN02 and c.COLUMNA03=m7.COLUMNA03
	left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	 --EMPHCS855 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in bill
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
    group by b.COLUMN02,c.COLUMN03,m.column17,a.COLUMN25,c.COLUMN17,c.COLUMN08,c.COLUMNA13,d.COLUMNA13,c.COLUMN24,c.COLUMN26,c.COLUMN25,a.COLUMN09,c.COLUMN28,hs.COLUMN04,c.COLUMN10
			having ((c.COLUMN08) -isnull(sum( d.COLUMN09),0))>0  and c.COLUMNA13=0 
	--union all
	--select 0 as COLUMN03,c.COLUMN03 as COLUMN04,c.COLUMN05 as COLUMN05,0 as COLUMN06 , 

	-- c.COLUMN07 as COLUMN07, 0 COLUMN08,(c.COLUMN07-c.COLUMN13) as COLUMN09,

	--(c.COLUMN07-c.COLUMN13) as COLUMN10,(c.COLUMN09) as COLUMN11,((c.COLUMN07-c.COLUMN13)*c.COLUMN09) as COLUMN12,c.COLUMN25 AS COLUMN18,(c.COLUMN26)
	--,((cast(m.column17 as decimal)*(0.01))*(cast(((c.COLUMN07-c.COLUMN13)*c.COLUMN09) as decimal))) COLUMN20
	--FROM dbo.PUTABLE002 c inner join MATABLE013 m on  m.column02=c.COLUMN25
	--inner join PUTABLE001 a on a.COLUMN01=c.COLUMN19 inner join PUTABLE003 d on d.COLUMN06!=a.COLUMN02
	--WHERE c.COLUMN19 = (select column01 from putable001 where column02=@PurchaseOrderID) and (c.COLUMN07-c.COLUMN13)>0 group by c.COLUMN04,c.COLUMN03,c.COLUMN05,c.COLUMN07,c.COLUMN13,c.COLUMN09,c.COLUMN26,m.column17,c.COLUMN25
	--having (c.COLUMN07-c.COLUMN13)>0

   end
   else if( @tmpVal!=@tmpVal1)

   begin

 --  SELECT distinct d.COLUMN02 as COLUMN03, a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05,a.COLUMN15 as COLUMN06 , 

	-- a.COLUMN07 as COLUMN07, a.COLUMN13 as COLUMN08,(a.COLUMN12-a.COLUMN13) as COLUMN09,

	--0 as COLUMN10,a.COLUMN09 as COLUMN11,((a.COLUMN12-a.COLUMN13))*a.COLUMN09 as COLUMN12

	--FROM dbo.PUTABLE002 a left outer join PUTABLE003 d on d.COLUMN01 in(SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE COLUMN06= 1015)  

	--WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= 1015) 

   SELECT   d.COLUMN02 as COLUMN03, a.COLUMN03 as COLUMN04, a.COLUMN04 as COLUMN05,cast(avg(cast(a.COLUMN05 as int))as int) as COLUMN06  ,

	 cast(avg(a.COLUMN06)as int) as COLUMN07,isnull(max(b.COLUMN08),0) as COLUMN08,min(a.COLUMN08-isnull(b.COLUMN08,0)) as COLUMN09,

	min(a.COLUMN08-isnull(b.COLUMN08,0)) as COLUMN10,cast(avg(a.COLUMN10) as int) as COLUMN11,cast(min((a.COLUMN08-isnull(b.COLUMN08,0))*a.COLUMN10) as decimal(18,2)) as COLUMN12,b.COLUMN18 AS COLUMN18,a.COLUMN17 as COLUMN19
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(min((a.COLUMN08-isnull(b.COLUMN08,0))*a.COLUMN10) as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN24 as COLUMN27,a.COLUMN26 LineID,a.COLUMN10 COLUMN33,a.COLUMN28 COLUMN35,hs.COLUMN04 hsncode
	FROM  dbo.PUTABLE004 a 
	inner join PUTABLE003 d on d.COLUMN06=@PurchaseOrderID  and d.COLUMN01=a.COLUMN12  and a.COLUMNA13='False'
	 left outer join PUTABLE005 c on  c.COLUMN06=@PurchaseOrderID 
	 --EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation Bill
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	 left outer join PUTABLE006 b on iif(isnull(b.COLUMN34,0)=0,0,isnull(a.COLUMN26,0))=isnull(b.COLUMN34,0) and b.COLUMN13 =c.COLUMN01 and d.COLUMN02=b.COLUMN03 and b.COLUMN04=a.COLUMN03  and b.COLUMN19=a.COLUMN17 and b.COLUMN27=a.COLUMN24  and d.COLUMNA13='False'
	 --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	 left join MATABLE013 m on  m.column02=b.COLUMN18
	 inner join MATABLE007 m7 on a.COLUMN03=m7.COLUMN02 and a.COLUMNA03=m7.COLUMNA03
	 left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
     --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	  where d.COLUMN17=1001 and b.COLUMNA13=0
	  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	  group by d.COLUMN02,a.COLUMN03,a.COLUMN04,a.COLUMN17,b.COLUMN18,m.column17,a.COLUMN24,a.COLUMN26,a.COLUMN25,a.COLUMN28,hs.COLUMN04,a.COLUMN10
	  having min(a.COLUMN08-isnull(b.COLUMN08,0))>0
	--SELECT distinct b.COLUMN02 as COLUMN03,a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05,a.COLUMN15 as COLUMN06 , 

	-- a.COLUMN07 as COLUMN07, a.COLUMN13 as COLUMN08,(a.COLUMN12-a.COLUMN13) as COLUMN09,

	--0 as COLUMN10,a.COLUMN09 as COLUMN11,((a.COLUMN12-a.COLUMN13))*a.COLUMN09 as COLUMN12

	--FROM PUTABLE004 c inner join PUTABLE003 b on b.COLUMN01=c.COLUMN12 and b.COLUMN06=@PurchaseOrderID

	--left outer join dbo.PUTABLE002 a  on a.COLUMN19=(SELECT COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID)

	end

END

else

Begin



if exists( SELECT  COLUMN03 FROM PUTABLE006 WHERE COLUMN03 =(SELECT COLUMN02 FROM dbo.PUTABLE003 WHERE COLUMN04= @Form or COLUMN02= @Form))

begin

   SELECT  distinct d.COLUMN02 as COLUMN03, a.COLUMN03 as COLUMN04, a.COLUMN04 as COLUMN05,a.COLUMN05 as COLUMN06 , 
	--EMPHCS855 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in bill
	 max(a.COLUMN06) as COLUMN07,max(b.COLUMN08) as COLUMN08,(a.COLUMN08) -isnull(sum( b.COLUMN09),0) as COLUMN09,
	--EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation Bill
	(a.COLUMN08) -isnull(sum( b.COLUMN09),0) as COLUMN10,f.COLUMN09 as COLUMN11,cast((((a.COLUMN08) -isnull(sum( b.COLUMN09),0))*max(a.COLUMN10)) as decimal(18,2)) as COLUMN12,f.COLUMN25 AS COLUMN18,a.COLUMN17 as COLUMN19
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(((a.COLUMN08) -isnull(sum( b.COLUMN09),0))*max(a.COLUMN10) as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN24 as COLUMN27,c.COLUMN26 LineID,a.COLUMN10 COLUMN33,a.COLUMN28 COLUMN35,hs.COLUMN04 hsncode
	FROM dbo.PUTABLE004 a inner join PUTABLE003 d on d.COLUMN01=a.COLUMN12 and  d.COLUMN06=@PurchaseOrderID  
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
     inner join PUTABLE001 e on e.COLUMN02=d.COLUMN06 inner join PUTABLE002 f on f.COLUMN19=e.COLUMN01 and f.COLUMN03=a.COLUMN03 and f.COLUMN26=a.COLUMN17 and iif(f.COLUMN17='',0,isnull(f.COLUMN17,0))=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	 left  join PUTABLE006 b on iif(isnull(a.COLUMN26,0)=0,0,isnull(b.COLUMN34,0))=isnull(a.COLUMN26,0) and b.COLUMN03 =d.COLUMN02 and  b.COLUMN04 =a.COLUMN03  and b.COLUMN19=a.COLUMN17 and b.COLUMN27=a.COLUMN24  and b.COLUMNA13='False'
	 left  join PUTABLE005 c on  b.COLUMN13 =c.COLUMN01 and  c.COLUMN06=d.COLUMN06    and b.COLUMNA13='False'
	 --and	b.COLUMN13=(select max(COLUMN13) from PUTABLE006 where COLUMN03=(SELECT COLUMN02 FROM dbo.PUTABLE003 WHERE COLUMN04= @Form or COLUMN02= @Form)) and
     --b.COLUMN04 =a.COLUMN03  and b.COLUMN03=d.COLUMN02 and b.COLUMN03=(select max(COLUMN03) from PUTABLE006 where COLUMN03=(SELECT COLUMN02 FROM dbo.PUTABLE003 WHERE COLUMN04= @Form or COLUMN02= @Form)) 
	 left join MATABLE013 m on  m.column02=f.COLUMN25
	 inner join MATABLE007 m7 on a.COLUMN03=m7.COLUMN02 and a.COLUMNA03=m7.COLUMNA03
	 left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
     --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	WHERE a.COLUMN12 in(SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE COLUMN04= @Form or COLUMN02= @Form)  and a.COLUMNA13=0
	--EMPHCS855 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in bill
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	group by d.COLUMN02,a.COLUMN03, a.COLUMN04,a.COLUMN05,m.column17, a.COLUMN04,b.COLUMN18,a.COLUMN17,f.COLUMN25,f.COLUMN09,a.COLUMN08,a.COLUMNA13,b.COLUMNA13,a.COLUMN24,c.COLUMN26,a.COLUMN25,a.COLUMN28,hs.COLUMN04,a.COLUMN10
	 having ((a.COLUMN08) -isnull(sum( b.COLUMN09),0))>0   and a.COLUMNA13=0  

   end

   else

   begin

   SELECT  b.COLUMN02 as COLUMN03,a.COLUMN03 as COLUMN04,a.COLUMN04 as COLUMN05,a.COLUMN05 as COLUMN06 , 

	 a.COLUMN06 as COLUMN07, 0  as COLUMN08,a.COLUMN08 as COLUMN09,

	a.COLUMN08 as COLUMN10,e.COLUMN09 as COLUMN11,a.COLUMN11 as COLUMN12,e.column25 AS COLUMN18,a.COLUMN17 as COLUMN19
	,cast((cast(isnull(m.column17,0) as decimal(18,2))*(0.01))*(cast(e.COLUMN09 as decimal(18,2))) as decimal(18,2)) COLUMN20
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN24 as COLUMN27,a.COLUMN26 LineID,a.COLUMN10 COLUMN33,a.COLUMN28 COLUMN35,hs.COLUMN04 hsncode
	FROM dbo.PUTABLE004 a inner join PUTABLE003 b on b.COLUMN01=a.COLUMN12 
	--EMPHCS855 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in bill
	---EMPHCS968	Duplication of Bill - Verify IR to Bill - IR00023  BY RAJ.Jr
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	inner join PUTABLE001 c on c.COLUMN02=b.COLUMN06 inner join PUTABLE002 e on iif(isnull(a.COLUMN26,0)=0,0,e.COLUMN02)=isnull(a.COLUMN26,0) and e.COLUMN19=c.COLUMN01 and e.COLUMN03=a.COLUMN03 and e.COLUMN26=a.COLUMN17 and iif(e.COLUMN17='',0,isnull(e.COLUMN17,0))=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0)) and e.COLUMNA13=0 and a.COLUMN17=e.COLUMN26
	 --inner join SATABLE006 e on e.COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02=@SalesOrderID)
	 left join MATABLE013 m on  m.column02=e.column25
	 inner join MATABLE007 m7 on a.COLUMN03=m7.COLUMN02 and a.COLUMNA03=m7.COLUMNA03
	 left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
     --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	WHERE a.COLUMN12 in (SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE (COLUMN04= @Form or COLUMN02= @Form) and COLUMN06 in(cast(@PurchaseOrderID as nvarchar)) )
	and a.COLUMN08>0  and a.COLUMNA13='False' and a.COLUMNA13=0
   end

end

END























GO
