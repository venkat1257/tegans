USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PQ_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_PQ_HEADER_DATA]
(
	@PurchaseOrderID int,
	@Status varchar(250) = null,
	@MonthStartDate varchar(250) = null,
	@MonthEndDate varchar(250) = null
)

AS
BEGIN
set @Status=(select column04 from CONTABLE025 where COLUMN02=65)
set @MonthStartDate=( SELECT DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0))
set @MonthEndDate=( SELECT DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0)))
	SELECT  a.COLUMN05 vendor,b.COLUMN03 item,a.COLUMN24 location,a.COLUMN04 quotation,a.COLUMN36 project
	,(select column05 from SATABLE001 where COLUMN02=a.COLUMN05) as cName
	FROM PUTABLE001 a inner join satable001 v on a.COLUMN05=v.column02 inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 
	where a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and  a.COLUMN08>=@MonthStartDate 
	 and a.COLUMN16=@Status  and  a.COLUMN02=@PurchaseOrderID 
END






GO
