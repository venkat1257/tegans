USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE018]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_FI_TP_FITABLE018]

(
    @COLUMN02   nvarchar(250)=null,  @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN21   nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN

declare @newIID int
declare @balance decimal
declare @balance1 decimal(18,2)
declare @balance2 decimal(18,2)
declare @Increase decimal
declare @decrease decimal
declare @Increase1 decimal(18,2)
declare @decrease1 decimal(18,2)
declare @Type INT
declare @tmpnewIID int
set @Type=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
set @COLUMN04=(select COLUMN05 from FITABLE049 where COLUMN01=@COLUMN21  AND COLUMNA03=@COLUMNA03  )
set @Increase= cast(isnull(@COLUMN09,0) as decimal(18,2))
set @decrease=cast(isnull(@COLUMN10,0) as decimal(18,2))
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE018_SequenceNo
set @balance=(( (select ISNULL(MAX(ISNULL(COLUMN11,0)),0) from FITABLE018 where COLUMN08=@COLUMN08  AND COLUMNA03=@COLUMNA03 )+ @Increase)-@decrease)
insert into FITABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07, COLUMN08, COLUMN09,COLUMN10, COLUMN11, COLUMN12,COLUMN21, COLUMNA02,  COLUMNA03, COLUMNA13, COLUMNA06, COLUMNA07, COLUMNA08)
Values(@COLUMN02,@COLUMN03,@COLUMN04,'OB'+@COLUMN02,'Opening Balance',@COLUMN07,@COLUMN08,@Increase, @decrease,@balance ,@COLUMN12,@COLUMN21,@COLUMNA02,@COLUMNA03,0, @COLUMNA06, @COLUMNA07,@COLUMNA08)

--DECLARE @ACCOUNTTYPE NVARCHAR(250)
--set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  ))
--if(@ACCOUNTTYPE='Personal')
--	begin
--	set @Increase=cast(isnull(@COLUMN09,0) as decimal)
--	set @decrease=cast(isnull(@COLUMN10,0) as decimal)
--	end
--	else if(@ACCOUNTTYPE='Real')
--	begin
--	set @Increase=cast(isnull(@COLUMN09,0) as decimal)
--	set @decrease=cast(isnull(@COLUMN10,0) as decimal)
--	end
--	else if(@ACCOUNTTYPE='Nominal')
--	begin
--	set @Increase=cast(isnull(@COLUMN10,0) as decimal)
--	set @decrease=cast(isnull(@COLUMN09,0) as decimal)
--	end
--	else
--	begin
--	set @Increase=cast(isnull(@COLUMN09,0) as decimal)
--	set @decrease=cast(isnull(@COLUMN10,0) as decimal)
--	end
	     --Bank
		 if(@Type=22266)
		BEGIN
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2))  from PUTABLE019 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from PUTABLE019 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update PUTABLE019 set COLUMN11=((@Increase+@Increase1)-@decrease),COLUMN12=(@Increase+(@decrease1-@decrease)) where COLUMN05=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		end
        --Inventory Asset
		 if(@Type=22262)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2))  from PUTABLE017 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from PUTABLE017 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update PUTABLE017 set COLUMN12=((@Increase+@Increase1)-@decrease),COLUMN10=(@Increase+(@decrease1-@decrease)) where COLUMN05=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		
		END
		--Accounts Payable
		if(@Type=22263)
		BEGIN
		set @tmpnewIID=(select MAX(COLUMN02) from PUTABLE016)
if(@tmpnewIID>0)
		BEGIN
		set @newIID=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		end
else
		begin
		set @newIID=10000
		end
		set @COLUMN04 = (select column05 from FITABLE050 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
		set @COLUMN05 = (select column04 from FITABLE050 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
			insert into PUTABLE016 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,COLUMN13,COLUMN18, COLUMNA02,COLUMNA03, COLUMNA13, COLUMNA06, COLUMNA07)
Values(@newIID,@COLUMN08,@COLUMN04,@COLUMN02,'Opening Balance',@COLUMN07,@COLUMN08,@COLUMN05,@COLUMN09,@COLUMN10,@COLUMN12,@COLUMNA02,@COLUMNA03,0, @COLUMNA06, @COLUMNA07)
		 --SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 --SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 --set @Increase1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2))  from PUTABLE016 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN14,0)AS DECIMAL(18,2)) from PUTABLE016 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 --update PUTABLE016 set COLUMN12=((@Increase+@Increase1)-@decrease),COLUMN14=(@Increase+(@decrease1-@decrease)) where COLUMN05=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 --UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		
		END
       --Accounts Receivable
		if(@Type=22264)
		begin
set @tmpnewIID=(select MAX(COLUMN02) from PUTABLE018)
if(@tmpnewIID>0)
		BEGIN
		set @newIID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1
		end
else
		begin
		set @newIID=10000
		end
		set @COLUMN04 = (select column05 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
         set @COLUMN05 = (select column04 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
--		insert into PUTABLE018(
--				COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMNA02, COLUMNA03,COLUMNA13,COLUMNA06, COLUMNA07 )
--values(@newIID, 3000,getdate(), @COLUMN05, @COLUMN07 ,@COLUMN12, @COLUMN09, @COLUMN11, @COLUMN12, @COLUMNA02, @COLUMNA03,0,@COLUMNA06, @COLUMNA07)
--insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04, COLUMN05,COLUMN06, COLUMN07, COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMNA02, COLUMNA03,COLUMNA13,COLUMNA06, COLUMNA07 )
--values               (@newIID, 3000,   getdate(), @COLUMN05,'Opening Balance', @COLUMN07 ,@COLUMN12, @COLUMN09, @COLUMN10, @COLUMN09, @COLUMNA02, @COLUMNA03,0,@COLUMNA06, @COLUMNA07)
insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04, COLUMN05,COLUMN06, COLUMN07, COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN16,COLUMNA02, COLUMNA03,COLUMNA13,COLUMNA06, COLUMNA07 )
values      (@newIID, 3000,  @COLUMN04, @COLUMN05,'Opening Balance', @COLUMN07 ,@COLUMN12, @COLUMN09, @COLUMN10, @COLUMN09,@COLUMN02, @COLUMNA02, @COLUMNA03,0,@COLUMNA06, @COLUMNA07)
		--SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		--SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		--set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from PUTABLE018 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		--set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from PUTABLE018 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))    and COLUMNA03=@COLUMNA03)
		--update PUTABLE018 set COLUMN09=((@Increase+@Increase1)-@decrease),COLUMN12=(@Increase+(@decrease1-@decrease)) where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03
		--UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA0
		END
		--Equity
		if(@Type=22330)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from FITABLE029 where  COLUMN11=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from FITABLE029 where COLUMN11=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE029 set COLUMN09=(@Increase+(@Increase1-@decrease)) where COLUMN11=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Expense
		if(@Type=22344)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE036 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2)) from FITABLE036 where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03)
		 update FITABLE036 set COLUMN07=((@Increase+@Increase1)-@decrease) where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Current liabilities
		if(@Type=22383)
		BEGIN
		

		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2))  from FITABLE026 where  COLUMN08=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from FITABLE026 where COLUMN08=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE026 set COLUMN11=((@Increase+@Increase1)-@decrease) where COLUMN08=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		
		END
		 --Current assets
		if(@Type=22402)
		BEGIN
		
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2))  from FITABLE034 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2)) from FITABLE034 where COLUMN09=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE034 set COLUMN07=(@Increase+(@decrease1-@decrease)) where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Fixed assets
		if(@Type=22403)
		BEGIN
		
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from FITABLE028 where  COLUMN06=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2)) from FITABLE028 where COLUMN06=@COLUMN08    and COLUMNA03=@COLUMNA03)
		 update FITABLE028 set COLUMN09=((@Increase+@Increase1)-@decrease) where COLUMN06=@COLUMN08 and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Income
		if(@Type=22405)
		BEGIN
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 --set @Increase1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2))  from FITABLE035 where  COLUMN09=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2)) from FITABLE035 where COLUMN09=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE035 set COLUMN07=(@Increase+(@decrease1-@decrease)) where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Cost of Goods Sold
		if(@Type=22406)
		BEGIN

		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2)) from FITABLE025 where COLUMN05=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from FITABLE025 where COLUMN05=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 update FITABLE025 set COLUMN09=((@Increase+@Increase1)-@decrease),COLUMN10=(@Increase+(@decrease1-@decrease)) where COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Other Income
		if(@Type=22407)
		BEGIN

		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE035 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		-- set @decrease1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2)) from FITABLE035 where COLUMN09=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 update FITABLE035 set COLUMN07=((@Increase+@Increase1)-@decrease) where COLUMN09=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		 --Other Expense
		if(@Type=22408)
		BEGIN
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE036 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2)) from FITABLE036 where COLUMN09=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 update FITABLE036 set COLUMN07=((@Increase+@Increase1)-@decrease) where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease),COLUMN08=((@balance2+@Increase)-@decrease) WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
END
 

IF @Direction = 'Select'
BEGIN
select * from FITABLE018
END 
 

IF @Direction = 'Update'
BEGIN
set @Type=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
set @COLUMN04=(select COLUMN05 from FITABLE049 where COLUMN01=@COLUMN21  AND COLUMNA03=@COLUMNA03  )
set @Increase= cast(isnull(@COLUMN09,0) as decimal(18,2))
set @decrease=cast(isnull(@COLUMN10,0) as decimal(18,2))
declare @ChangeAmnt decimal(18,2)
set @ChangeAmnt= (select (ISNULL(COLUMN09,0)) from FITABLE018 where COLUMN02=@COLUMN02  AND COLUMN08=@COLUMN08 and  COLUMN02 = @COLUMN02 AND COLUMNA03=@COLUMNA03 )



UPDATE FITABLE018 SET
COLUMN03=@COLUMN03, COLUMN04=@COLUMN04, COLUMN07=@COLUMN07,COLUMN08=@COLUMN08, COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10 ,COLUMN12=@COLUMN12,COLUMNA13=0
WHERE COLUMN02 = @COLUMN02 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03


   --Bank
		 if(@Type=22266)
		BEGIN
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2))  from PUTABLE019 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from PUTABLE019 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update PUTABLE019 set COLUMN11=((@Increase+@Increase1)-@decrease)-@ChangeAmnt,COLUMN12=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN05=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt,COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		end
        --Inventory Asset
		 if(@Type=22262)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )

		 set @Increase1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2))  from PUTABLE017 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from PUTABLE017 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update PUTABLE017 set COLUMN12=((@Increase+@Increase1)-@decrease)-@ChangeAmnt, COLUMN10=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN05=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		
		END
		--Accounts Payable
		if(@Type=22263)
		BEGIN
			
		 --SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 --SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 --set @Increase1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2))  from PUTABLE016 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN14,0)AS DECIMAL(18,2)) from PUTABLE016 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 --update PUTABLE016 set COLUMN12=((@Increase+@Increase1)-@decrease)-@ChangeAmnt,COLUMN14=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN05=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 --UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		
		set @COLUMN04 = (select column05 from FITABLE050 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
		set @COLUMN05 = (select column04 from FITABLE050 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
		IF @Direction = 'Update'
BEGIN
UPDATE  PUTABLE016 SET COLUMN03=@COLUMN08,COLUMN04=@COLUMN04,COLUMN05=@COLUMN02,COLUMN06='Opening Balance',COLUMN07=@COLUMN07,COLUMN08=@COLUMN08,COLUMN09=@COLUMN05,COLUMN12=@COLUMN09,COLUMN13=@COLUMN10,COLUMN18=@COLUMN12,COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03,COLUMNA13=0
where  COLUMN03=@COLUMN08 and COLUMN05=@COLUMN02 and COLUMN06='Opening Balance' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 
		END
		END
       --Accounts Receivable
		if(@Type=22264)
		BEGIN
		
		 --SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 --SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 --set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from PUTABLE018 where  COLUMN05=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from PUTABLE018 where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))    and COLUMNA03=@COLUMNA03)
		 --update PUTABLE018 set COLUMN09=((@Increase+@Increase1)-@decrease)-@ChangeAmnt,COLUMN12=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN05=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03
		 --UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		--select * from fitable049 order by column02 desc
		--select * from fitable018 order by column02 desc
		--select * from putable018  where  COLUMN04 ='2016-10-04 00:00:00.000' and COLUMN05='OBC00061' and COLUMN06= 'Opening Balance' and COLUMN07=2003 and COLUMN16=1232 and COLUMNA02=56569 and COLUMNA03=56577

		set @COLUMN04 = (select column05 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
         set @COLUMN05 = (select column04 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
		IF @Direction = 'Update'
BEGIN
UPDATE PUTABLE018 SET COLUMN04=@COLUMN04, COLUMN05=@COLUMN05,COLUMN06= 'Opening Balance', COLUMN07=@COLUMN07, COLUMN08=@COLUMN12,COLUMN09=@COLUMN09,COLUMN11=@COLUMN10,COLUMN12=@COLUMN09,COLUMN16=@COLUMN02,COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03 ,COLUMNA13 = 0
 where  COLUMN05=@COLUMN05 and COLUMN06= 'Opening Balance'  and COLUMN16=@COLUMN02 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END
END

		--Equity
		if(@Type=22330)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from FITABLE029 where  COLUMN11=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from FITABLE029 where COLUMN11=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE029 set COLUMN09=(@Increase+(@Increase1-@decrease))-@ChangeAmnt where COLUMN11=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Expense
		if(@Type=22344)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE036 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2)) from FITABLE036 where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03)
		 update FITABLE036 set COLUMN07=((@Increase+@Increase1)-@decrease)-@ChangeAmnt where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Current liabilities
		if(@Type=22383)
		BEGIN
		

		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2))  from FITABLE026 where  COLUMN08=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from FITABLE026 where COLUMN08=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE026 set COLUMN11=((@Increase+@Increase1)-@decrease)-@ChangeAmnt where COLUMN08=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		
		END
		 --Current assets
		if(@Type=22402)
		BEGIN
		
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		 set @Increase1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2))  from FITABLE034 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2)) from FITABLE034 where COLUMN09=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE034 set COLUMN07=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Fixed assets
		if(@Type=22403)
		BEGIN
		
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from FITABLE028 where  COLUMN06=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2)) from FITABLE028 where COLUMN06=@COLUMN08    and COLUMNA03=@COLUMNA03)
		 update FITABLE028 set COLUMN09=((@Increase+@Increase1)-@decrease)-@ChangeAmnt where COLUMN06=@COLUMN08 and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Income
		if(@Type=22405)
		BEGIN
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 --set @Increase1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2))  from FITABLE035 where  COLUMN09=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2)) from FITABLE035 where COLUMN09=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		 update FITABLE035 set COLUMN07=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Cost of Goods Sold
		if(@Type=22406)
		BEGIN

		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2)) from FITABLE025 where COLUMN05=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from FITABLE025 where COLUMN05=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 update FITABLE025 set COLUMN09=((@Increase+@Increase1)-@decrease)-@ChangeAmnt,COLUMN10=(@Increase+(@decrease1-@decrease))-@ChangeAmnt where COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		--Other Income
		if(@Type=22407)
		BEGIN

		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE035 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50))   and COLUMNA03=@COLUMNA03)
		-- set @decrease1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2)) from FITABLE035 where COLUMN09=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 update FITABLE035 set COLUMN07=((@Increase+@Increase1)-@decrease)-@ChangeAmnt where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END
		 --Other Expense
		if(@Type=22408)
		BEGIN
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03 )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE036 where  COLUMN09=CAST(@COLUMN08 AS nvarchar(50))  and COLUMNA03=@COLUMNA03)
		 --set @decrease1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2)) from FITABLE036 where COLUMN09=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 update FITABLE036 set COLUMN07=((@Increase+@Increase1)-@decrease)-@ChangeAmnt where COLUMN09=CAST(@COLUMN08 AS nvarchar(50)) and COLUMNA03=@COLUMNA03
		 UPDATE FITABLE001 SET  COLUMN10=((@balance1+@Increase)-@decrease)-@ChangeAmnt, COLUMN08=((@balance2+@Increase)-@decrease)-@ChangeAmnt  WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03
		END


END
 

else IF @Direction = 'Delete'
BEGIN


set @Type=(select COLUMN07 from FITABLE001 where COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )    )

set @Increase= (select COLUMN09 from FITABLE018 where COLUMN02=@COLUMN02)
set @decrease=(select COLUMN10 from FITABLE018 where COLUMN02=@COLUMN02)

        --Bank
		 if(@Type=22266)
		BEGIN
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ))
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 set @Increase1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2))  from PUTABLE019 where  COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from PUTABLE019 where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02  ) AS nvarchar(50))  )
		 update PUTABLE019 set COLUMN11=((@Increase1-@Increase)+@decrease),COLUMN12=((@decrease1-@Increase)+@decrease) where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02  ) AS nvarchar(50)) 
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease) WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		end
        --Inventory Asset
		 if(@Type=22262)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )   )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02  )   )
		 set @Increase1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2))  from PUTABLE017 where  COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from PUTABLE017 where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02  ) AS nvarchar(50)) )
		 update PUTABLE017 set COLUMN12=((@Increase1-@Increase)+@decrease),COLUMN10=((@decrease1-@Increase)+@decrease) where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02  ) AS nvarchar(50)) 
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )
		
		END
		--Accounts Payable
		if(@Type=22263)
		BEGIN
			
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )     )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 set @Increase1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2))  from PUTABLE016 where  COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  AS nvarchar(50))  )
		 set @decrease1=(select cast(isnull(COLUMN14,0)AS DECIMAL(18,2)) from PUTABLE016 where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  AS nvarchar(50)) )
		 update PUTABLE016 set COLUMN12=((@Increase1-@Increase)+@decrease),COLUMN14=((@decrease1-@Increase)+@decrease) where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  AS nvarchar(50))
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  
		
		END
       --Accounts Receivable
		if(@Type=22264)
		BEGIN
		
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )   )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from PUTABLE018 where  COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))  )
		 set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from PUTABLE018 where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))    )
		 update PUTABLE018 set COLUMN09=((@Increase1-@Increase)+@decrease),COLUMN12=((@decrease1-@Increase)+@decrease) where COLUMN05=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) 
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		

		END
		--Equity
		if(@Type=22330)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )   )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from FITABLE029 where  COLUMN11=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 --set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from FITABLE029 where COLUMN11=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))   )
		 update FITABLE029 set COLUMN09=((@Increase1-@Increase)+@decrease)where COLUMN11=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) 
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )
		END
		--Expense
		if(@Type=22344)
		BEGIN
	
		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )    )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE036 where  COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 set @decrease1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2)) from FITABLE036 where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 update FITABLE036 set COLUMN07=((@Increase1-@Increase)+@decrease) where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )
		END
		--Current liabilities
		if(@Type=22383)
		BEGIN
		

		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 set @Increase1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2))  from FITABLE026 where  COLUMN08=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)))
		 set @decrease1=(select cast(isnull(COLUMN12,0)AS DECIMAL(18,2)) from FITABLE026 where COLUMN08=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 update FITABLE026 set COLUMN11=((@Increase1-@Increase)+@decrease) where COLUMN08=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )
		
		END
		 --Current assets
		if(@Type=22402)
		BEGIN
		
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )   )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 set @Increase1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2))  from FITABLE034 where  COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 set @decrease1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2)) from FITABLE034 where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 update FITABLE034 set COLUMN07=((@decrease1-@Increase)+@decrease) where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		END
		--Fixed assets
		if(@Type=22403)
		BEGIN
		
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2))  from FITABLE028 where  COLUMN06=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )  )
		 --set @decrease1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2)) from FITABLE028 where COLUMN06=@COLUMN08  )
		 update FITABLE028 set COLUMN09=((@Increase1-@Increase)+@decrease) where COLUMN06=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )
		END
		--Income
		if(@Type=22405)
		BEGIN
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )   )
		 --set @Increase1=(select cast(isnull(COLUMN08,0)AS DECIMAL(18,2))  from FITABLE035 where  COLUMN09=@COLUMN08  and COLUMNA03=@COLUMNA03)
		 set @decrease1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2)) from FITABLE035 where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))  )
		 update FITABLE035 set COLUMN07=((@Increase1-@Increase)+@decrease) where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) 
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		END
		--Cost of Goods Sold
		if(@Type=22406)
		BEGIN

		 SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 set @Increase1=(select cast(isnull(COLUMN09,0)AS DECIMAL(18,2)) from FITABLE025 where COLUMN05=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ))
		 set @decrease1=(select cast(isnull(COLUMN10,0)AS DECIMAL(18,2)) from FITABLE025 where COLUMN05=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ))
		 update FITABLE025 set COLUMN09=((@Increase1-@Increase)+@decrease),COLUMN10=((@decrease1-@Increase)+@decrease) where COLUMN05=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 )
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		END
		--Other Income
		if(@Type=22407)
		BEGIN

		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE035 where  COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))  )
		-- set @decrease1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2)) from FITABLE035 where COLUMN09=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 update FITABLE035 set COLUMN07=((@Increase1-@Increase)+@decrease) where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))  
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease) WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		END
		 --Other Expense
		if(@Type=22408)
		BEGIN
		SET @balance1=(SELECT cast(ISNULL(COLUMN10,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 SET @balance2=(SELECT cast(ISNULL(COLUMN08,0)AS DECIMAL(18,2)) FROM FITABLE001 WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) )
		 set @Increase1=(select cast(isnull(COLUMN07,0)AS DECIMAL(18,2))  from FITABLE036 where  COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50)) )
		 --set @decrease1=(select cast(isnull(COLUMN11,0)AS DECIMAL(18,2)) from FITABLE036 where COLUMN09=@COLUMN08   and COLUMNA03=@COLUMNA03)
		 update FITABLE036 set COLUMN07=((@Increase1-@Increase)+@decrease) where COLUMN09=CAST((select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) AS nvarchar(50))
		 UPDATE FITABLE001 SET COLUMN10=((@balance1-@Increase)+@decrease),COLUMN08=((@balance2-@Increase)+@decrease)  WHERE COLUMN02=(select COLUMN08 from FITABLE018 where COLUMN02=@COLUMN02 ) 
		END

		 

		UPDATE FITABLE018 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02

END
end try
			begin catch
			declare @tempSTR nvarchar(max)
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_FITABLE018.txt',0
return 0
end catch
end

















GO
