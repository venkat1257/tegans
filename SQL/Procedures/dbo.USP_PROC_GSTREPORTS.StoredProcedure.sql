USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTREPORTS]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_GSTREPORTS](@FromDate NVARCHAR(250),
	@ToDate NVARCHAR(250),
	@GSTIN NVARCHAR(250) = NULL,
	@GSTCATEGORY INT = NULL,
	@AcOwner INT,
	@OPUnit NVARCHAR(250) =NULL,
	@OperatingUnit INT = NULL,
	@DateF NVARCHAR(250) = NULL,@whereStr nvarchar(1000)=null,@crwhereStr nvarchar(1000)=null,@advwhereStr nvarchar(1000)=null)
AS
BEGIN
IF @FromDate!='' and  @ToDate!=''
begin
  		set @whereStr=' and S9.COLUMN08 between '''+@FromDate+''' AND '''+@ToDate+''''
  		set @crwhereStr=' and S9.COLUMN06 between '''+@FromDate+''' AND '''+@ToDate+''''
  		set @advwhereStr=' and S9.COLUMN05 between '''+@FromDate+''' AND '''+@ToDate+''''
END
if(@OPUnit!='')
begin
	set @whereStr=@whereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
	set @crwhereStr=@crwhereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
	set @advwhereStr=@advwhereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
end
if(@whereStr='' or @whereStr is null)
begin
	set @whereStr=' and 1=1'
end
if(@crwhereStr='' or @crwhereStr is null)
begin
	set @crwhereStr=' and 1=1'
end
if(@advwhereStr='' or @advwhereStr is null)
begin
	set @advwhereStr=' and 1=1'
end
exec('SELECT (Q.CNT)CNT,Q.TableNo,Q.Particulars,Q.FRMID,Q.TRANS,Q.Date,Q.Party,(Q.SUBTOTAL)SUBTOTAL,(Q.TTAXRATE)TTAXRATE,(Q.TTAX)TTAX,(Q.IGSTAMT)IGSTAMT,(Q.CGSTAMT)CGSTAMT,(Q.SGSTAMT)SGSTAMT,(Q.CESSTAMT)CESSTAMT FROM(
(SELECT 0 CNT,5 TableNo,''B2B Invoices'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,6 TableNo,''B2C(Large Invoices)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,7 TableNo,''B2C(Small Invoices)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,8 TableNo,''Credit/Debit Notes'' Particulars,1330 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,9 TableNo,''Nil Rated Invoices'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,10 TableNo,''Exports Invoices'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,11 TableNo,''Tax Liability On Advances'' Particulars,1386 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
UNION ALL
SELECT 0 CNT,12 TableNo,''Setoff Tax Paid against Advances'' Particulars,1386 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
)UNION ALL(
SELECT COUNT(distinct P.TRANS)CNT,P.TableNo,P.Particulars,P.FRMID,P.TRANS,P.Date,P.Party,SUM(P.SUBTOTAL)SUBTOTAL,SUM(P.TTAXRATE)TTAXRATE,SUM(P.TTAX)TTAX,SUM(P.IGSTAMT)IGSTAMT,SUM(P.CGSTAMT)CGSTAMT,SUM(P.SGSTAMT)SGSTAMT,SUM(P.CESSTAMT)CESSTAMT FROM(
SELECT COUNT(*)CNT,5 TableNo,''B2B Invoices'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.COLUMN65=23601 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT COUNT(*)CNT,6 TableNo,''B2C(Large Invoices)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.COLUMN65=23602 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT COUNT(*)CNT,7 TableNo,''B2C(Small Invoices)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.COLUMN65=236022 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT COUNT(*)CNT,8 TableNo,''Credit/Debit Notes'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN06,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.columna03='+@AcOwner+' '+@crwhereStr+'
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,S9.COLUMN04,S9.COLUMN03,S9.COLUMN06,S.COLUMN05
UNION ALL
SELECT COUNT(*)CNT,9 TableNo,''Nil Rated Invoices'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
inner join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMN07=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT COUNT(*)CNT,11 TableNo,''Tax Liability On Advances'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN05,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN05,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN05,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM FITABLE023 S9  
inner join FITABLE024 a on S9.COLUMN01=a.COLUMN09 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN08=s.COLUMN02 and (isnull(S9.COLUMN20,22335)=22335 or cast(S9.COLUMN20 as nvarchar(250))='''') and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(S9.COLUMN12,''-'',''''))) s)) or t.COLUMN02=S9.COLUMN12) and t.COLUMNA03=S9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1386 and s9.columna03='+@AcOwner+' '+@advwhereStr+'
group by a.COLUMN02,a.COLUMN05,S9.COLUMN04,S9.COLUMN03,S9.COLUMN05,S.COLUMN05
) P GROUP BY P.FRMID,p.TRANS,P.Date,P.Party,P.TableNo,P.Particulars
))Q --GROUP BY Q.TableNo,Q.Particulars,Q.FRMID
')
END



GO
