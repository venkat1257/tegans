USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_InvoiceItemDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAL_TP_InvoiceItemDetails]
(
	@ItemID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL,@uom nvarchar(250)=NULL,@location nvarchar(250)=NULL,
	@StockItem nvarchar(250)=NULL,@lotno nvarchar(250)=NULL,@upc nvarchar(250)=NULL,@customer nvarchar(250)=NULL,
	@Project nvarchar(250)=NULL
)
AS
BEGIN
declare @whereStr nvarchar(max)=null,@Query1 nvarchar(max)=null,@itempricing nvarchar(250)=NULL
set @location=(iif(@location is null or @location='',0,@location ))
declare @uomsetup nvarchar(250)=NULL
set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
set @uomsetup=(@ItemID)
if(@OPUnit='' or @OPUnit=null) set @OPUnit=0;
if(@customer='' or @customer=null) set @customer=0;
if(@upc!='' and @upc is not null) set @ItemID=@upc;
else set @upc=0;
set @Project=(iif(@Project is null or @Project='',0,@Project ))
begin
	IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@upc and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@upc and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@upc as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@upc as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@upc as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@upc as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
end
if(@ItemID='' or @ItemID=null) set @ItemID=0;
	IF NOT EXISTS(SELECT * FROM FITABLE039 MA07 inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 WHERE MA07.COLUMN05=@ItemID and MA07.COLUMNA03=@AcOwner and (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and MA07.COLUMNA13=0 )
	set @itempricing=(0)
	else
	set @itempricing=(1)
--select * into #InvoiceItemDetails from (
	    select MA07.COLUMN04,MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN09,
		(case when isnull(lt.COLUMN11,0)>0 then isnull(lt.COLUMN11,0) when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end)COLUMN17,
		MA07.COLUMN45,MA07.COLUMN29,
		(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(MA07.COLUMN51,0) end)COLUMN51,
		--(case when isnull(cp.COLUMN08,0)>0 then cp.COLUMN08 when isnull(sp.COLUMN04,0)>0 then 
		--cast(iif(ip.COLUMN05='22778',isnull(sp.COLUMN04,0)-(isnull(sp.COLUMN04,0)*(cast(isnull(ip.COLUMN07,0) as decimal(18,2))*0.01)),isnull(sp.COLUMN04,0)+(isnull(sp.COLUMN04,0)*(cast(isnull(ip.COLUMN07,0) as decimal(18,2))*0.01))) as decimal(18,2))
		--when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(MA07.COLUMN51,0) end) Rate,
		(case when isnull(cp.COLUMN05,0)>0 then isnull(cp.COLUMN08,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(MA07.COLUMN51,0) end)Rate,
		MA07.COLUMN50 sdesc, MA07.COLUMN69 sQdesc,iif(cast(FI10.COLUMN08 as nvarchar(250))='',0,convert(double precision,isnull(FI10.COLUMN08,0)))COLUMN08,MA07.COLUMN57 pdesc, MA07.COLUMN68 pQdesc,MA07.COLUMN63,MA07.COLUMN54,MA07.COLUMN61,MA07.COLUMN64,MA07.COLUMN48,MA07.COLUMN66,MA07.COLUMN65,
		(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty,MA07.COLUMN10 as BRAND,(case when isnull(cp.COLUMN05,0)>0 then cp.COLUMN06 else 1000 end) pricelevel,iif(isnull(pl.COLUMN05,0)>0,pl.COLUMN05,ip.COLUMN05) pricetype,0 itempricing,
		(case when isnull(lt.COLUMN14,0)>0 then isnull(lt.COLUMN14,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(sp.COLUMN04,0) end)MRP,
		iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT,FI10.COLUMN22 as ALot
		 from MATABLE007 MA07 
		left join MATABLE021 ia on (cast(MA07.COLUMN02 as nvarchar(250))=@ItemID or cast(MA07.COLUMN06 as nvarchar(250))=@ItemID) AND ia.COLUMN05=MA07.COLUMN02 and ia.COLUMN04=cast(@upc as nvarchar(250)) and   MA07.COLUMNA03=@AcOwner and (MA07.COLUMNA02=@OPUnit or MA07.COLUMNA02 is null) and 
		 ia.COLUMNA03=@AcOwner and ia.COLUMN05=MA07.COLUMN02 and ia.COLUMN04=cast(@upc as nvarchar(250)) and (ia.COLUMNA02=@OPUnit  or ia.COLUMNA02 is null) and ia.COLUMNA13=0 
		left join MATABLE026 cp on   cp.COLUMN05=MA07.COLUMN02 and cp.COLUMNA03=@AcOwner and cp.COLUMN03=@customer and(isnull(cp.COLUMN04,0)=@OPUnit  or isnull(cp.COLUMN04,0)=0) and cp.COLUMNA03=@AcOwner and cp.COLUMNA13=0 
		left join MATABLE025 ip on   ip.COLUMN03=MA07.COLUMN02 and ip.COLUMNA03=@AcOwner and(ip.COLUMNA02=@OPUnit  or ip.COLUMNA02 is null) and ip.COLUMNA03=@AcOwner and ip.COLUMNA13=0  and ip.COLUMN08=1 
		left join MATABLE024 sp on  sp.COLUMN07=MA07.COLUMN02 and sp.COLUMNA03=@AcOwner and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0 
		left join MATABLE024 pp on pp.COLUMN07=MA07.COLUMN02 and pp.COLUMNA03=@AcOwner and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=@AcOwner and pp.COLUMNA13=0 
		left join FITABLE043 lt on lt.COLUMN09=MA07.COLUMN02 and lt.COLUMNA03=@AcOwner and lt.COLUMN02=@lotno and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=MA07.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
		left join MATABLE023 pl on  pl.COLUMNA03=@AcOwner and pl.COLUMN02=cp.COLUMN06 and pl.COLUMNA13=0 
		left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02 and FI10.COLUMNa03=@AcOwner and (FI10.COLUMN13=@OPUnit or FI10.COLUMN13 is null) and FI10.COLUMN19=iif(cast(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end as nvarchar(250)) is null,10000,(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end)) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@lotno and isnull(FI10.COLUMN23,0)=@Project
		left join MATABLE032 hs on hs.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
		left join MATABLE032 sc on sc.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
		--inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
		WHERE   isnull(MA07.COLUMN47,'False')='False' and MA07.COLUMNA13=0 and (cast(MA07.COLUMN02 as nvarchar(250))=@ItemID or cast(MA07.COLUMN06 as nvarchar(250))=@ItemID) and (MA07.COLUMNA02=@OPUnit or MA07.COLUMNA02 is null) and  MA07.COLUMNA03=@AcOwner
   --     union all	
			--select MA07.COLUMN05 COLUMN02,m10.COLUMN06,m10.COLUMN09,MA07.COLUMN09 COLUMN17,m10.COLUMN45,m10.COLUMN29,MA07.COLUMN10 COLUMN51,MA07.COLUMN10 Rate,m10.COLUMN50 sdesc, m10.COLUMN69 sQdesc,
			--(isnull(FI10.COLUMN08,0)) COLUMN08,m10.COLUMN57 pdesc, m10.COLUMN68 pQdesc,m10.COLUMN63,m10.COLUMN54,m10.COLUMN61,m10.COLUMN64,m10.COLUMN48,m10.COLUMN66,m10.COLUMN65,
			--(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty,FI10.COLUMN22 as Lot,MA07.COLUMN10 as BRAND,0 pricelevel,1 itempricing  from FITABLE039 MA07
			--inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05  and  m10.COLUMNA13=0 and  isnull(m10.COLUMN47,'False')='False' and  m10.COLUMNA03=@AcOwner
			--left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN05 and ia.COLUMN04=@upc and (ia.COLUMNA02=@OPUnit or ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and isnull(ia.COLUMNA13,0)=0 
			--left join MATABLE026 cp on cp.COLUMN03=@customer and cp.COLUMN05=MA07.COLUMN02 and (isnull(cp.COLUMN04,0)=@OPUnit  or cp.COLUMN04 is null) and cp.COLUMNA03=@AcOwner and cp.COLUMNA13=0 
			--left join MATABLE025 ip on ip.COLUMN03=MA07.COLUMN02 and (ip.COLUMNA02=@OPUnit  or ip.COLUMNA02 is null) and ip.COLUMNA03=@AcOwner and ip.COLUMNA13=0  and ip.COLUMN08=1 
			--left join MATABLE024 sp on sp.COLUMN07=MA07.COLUMN02 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0)=@OPUnit  or sp.COLUMN03 is null) and sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0 
			--left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN05 and (FI10.COLUMN13=@OPUnit or FI10.COLUMN13 is null) and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@lotno
			--WHERE   isnull(MA07.COLUMNA13,'False')='False' and  (MA07.COLUMN05=@ItemID OR m10.COLUMN06=@ItemID) AND (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and  MA07.COLUMNA03=@AcOwner
		--) Query
--if @itempricing=(0)
--begin
--set @whereStr=' where itempricing=0'
--end
--else if @itempricing=(1)
--begin
--set @whereStr=' where itempricing=1'
--end
--if @Operating!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
--end
--end
--if(@whereStr='' or @whereStr is null)
--begin
-- set @whereStr=' where 1=1  '
--end
--set @Query1='select * from #InvoiceItemDetails'+@whereStr
--exec (@Query1) 
END








GO
