USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PROFIT_R_LOSS_BY_INVOICEREFERENCE]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PROFIT_R_LOSS_BY_INVOICEREFERENCE]
(
	@REF NVARCHAR(25),
	@OPUNIT NVARCHAR(250) = NULL,
	@OPERATING NVARCHAR(250) = NULL,
	@ACOWNER INT,
	@DTF NVARCHAR(25)
)
AS
BEGIN
declare @WHERESTR nvarchar(1000)=null
IF @OPUNIT!=''
	BEGIN
		SET @WHERESTR=' S9.COLUMNA02 IN (SELECT LISTVALUE FROM DBO.FN_LISTTOTABLE('','','''+@OPUNIT+''') S)'		
	END
ELSE
	BEGIN
		set @WHERESTR=' 1=1'
	END
SELECT [REFERENCE],[CUSTOMER NAME],MOBILE,[EMAIL],DETAILS,REMARKS,PARTY,PARTICULARS,[DATE],[TRANSACTION TYPE],[TRANSACTION],BANK,DEBIT,CREDIT FROM
	(
		SELECT @REF [REFERENCE],S2.COLUMN05 [CUSTOMER NAME],S2.COLUMN11 MOBILE,S2.COLUMN10 [EMAIL],
		STUFF((SELECT  CHAR(10) +(m7.COLUMN04)  FROM SATABLE009 S
		INNER JOIN SATABLE010 S10 ON S10.COLUMN15 = S.COLUMN01 AND S10.COLUMNA03 = S.COLUMNA03
		INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = S10.COLUMN05 AND M7.COLUMNA03 = S.COLUMNA03
		WHERE S.COLUMN04 = @REF AND S.COLUMNA03 = @ACOWNER 
		FOR XML PATH ('')), 1, 1, '') DETAILS,
		(S9.COLUMN12) REMARKS,'' PARTY,'Amount Invoiced'PARTICULARS,FORMAT(S9.COLUMN08,@DTF) [DATE],
		'Invoice' [TRANSACTION TYPE],S9.COLUMN04 [TRANSACTION],''BANK,S9.COLUMN20 DEBIT,0 CREDIT
		FROM SATABLE009 S9
		INNER JOIN SATABLE002 S2 ON S2.COLUMN02 = S9.COLUMN05 AND S2.COLUMNA03 = S9.COLUMNA03
		WHERE S9.COLUMN04 = @REF AND S9.COLUMN04 != '' AND S9.COLUMNA03 = @ACOWNER  AND ISNULL(S9.COLUMNA13,0) = 0 

		UNION ALL

		SELECT @REF [REFERENCE],'' [CUSTOMER NAME],'' MOBILE,'' [EMAIL],''DETAILS,''REMARKS,'' PARTY,'Payment  Received'PARTICULARS,FORMAT(S1.COLUMN19,@DTF) [DATE],
		'Payment' [TRANSACTION TYPE],S1.COLUMN04 [TRANSACTION],F1.COLUMN04 BANK,0 DEBIT,s9.COLUMN06 CREDIT
		FROM SATABLE012 S9
		INNER JOIN SATABLE009 S2 ON S2.COLUMN02 = S9.COLUMN03 AND S2.COLUMNA03 = S9.COLUMNA03
		INNER JOIN SATABLE011 S1 ON S1.COLUMN01 = S9.COLUMN08 AND S1.COLUMNA03 = S9.COLUMNA03
		INNER JOIN FITABLE001 F1 ON F1.COLUMN02 = S1.COLUMN08 AND F1.COLUMNA03 = S9.COLUMNA03
		WHERE S2.COLUMN04 = @REF AND S2.COLUMN04 != '' AND S9.COLUMNA03 = @ACOWNER  AND ISNULL(S9.COLUMNA13,0) = 0 

		UNION ALL

		SELECT @REF [REFERENCE],'' [CUSTOMER NAME],'' MOBILE,'' [EMAIL],''DETAILS,''REMARKS,'' PARTY,'Advance  Received'PARTICULARS,FORMAT(S9.COLUMN05,@DTF) [DATE],
		'Advance' [TRANSACTION TYPE],S9.COLUMN04 [TRANSACTION],F1.COLUMN04 BANK,0 DEBIT,s9.COLUMN10 CREDIT
		FROM FITABLE023 S9
		INNER JOIN SATABLE002 S2 ON S2.COLUMN02 = S9.COLUMN08 AND S2.COLUMNA03 = S9.COLUMNA03
		INNER JOIN FITABLE001 F1 ON F1.COLUMN02 = S9.COLUMN09 AND F1.COLUMNA03 = S9.COLUMNA03
		WHERE S9.COLUMN11 = @REF AND S9.COLUMN11 != '' AND S9.COLUMNA03 = @ACOWNER  AND ISNULL(S9.COLUMNA13,0) = 0 

		UNION ALL
		
		SELECT @REF [REFERENCE],'' [CUSTOMER NAME],NULL MOBILE,S2.COLUMN11 [EMAIL],''DETAILS,''REMARKS,S2.COLUMN05 PARTY,'Transportation'PARTICULARS,FORMAT( S9.COLUMN08,@DTF) [DATE],
		'Bill' [TRANSACTION TYPE],S9.COLUMN04 [TRANSACTION],''BANK,0 DEBIT,S9.COLUMN14 CREDIT
		 FROM PUTABLE005 S9
		INNER JOIN SATABLE001 S2 ON S2.COLUMN02 = S9.COLUMN05 AND S2.COLUMNA03 = S9.COLUMNA03
		WHERE S9.COLUMN09 = @REF AND S9.COLUMN09 != '' AND S9.COLUMNA03 = @ACOWNER  AND ISNULL(S9.COLUMNA13,0) = 0 

		UNION ALL

		SELECT @REF [REFERENCE],'' [CUSTOMER NAME],'' MOBILE,'' [EMAIL],''DETAILS,''REMARKS,S3.COLUMN05 PARTY,'Payment  Send'PARTICULARS,FORMAT(S1.COLUMN18,@DTF) [DATE],
		'Payment' [TRANSACTION TYPE],S1.COLUMN04 [TRANSACTION],F1.COLUMN04 BANK,S9.COLUMN06 DEBIT,0 CREDIT
		FROM PUTABLE015 S9
		INNER JOIN PUTABLE005 S2 ON S2.COLUMN02 = S9.COLUMN03 AND S2.COLUMNA03 = S9.COLUMNA03
		INNER JOIN PUTABLE014 S1 ON S1.COLUMN01 = S9.COLUMN08 AND S1.COLUMNA03 = S9.COLUMNA03
		INNER JOIN FITABLE001 F1 ON F1.COLUMN02 = S1.COLUMN08 AND F1.COLUMNA03 = S9.COLUMNA03
		INNER JOIN SATABLE001 S3 ON S3.COLUMN02 = S1.COLUMN05 AND S3.COLUMNA03 = S9.COLUMNA03
		WHERE S9.COLUMNA03 = @ACOWNER AND S2.COLUMN09 = @REF AND S9.COLUMN09 != ''  AND ISNULL(S9.COLUMNA13,0) = 0 
		
		UNION ALL

		SELECT @REF [REFERENCE],'' [CUSTOMER NAME],'' MOBILE,'' [EMAIL],''DETAILS,''REMARKS,S2.COLUMN05 PARTY,'Advance  Send'PARTICULARS,FORMAT(S9.COLUMN05,@DTF) [DATE],
		'Advance' [TRANSACTION TYPE],S9.COLUMN04 [TRANSACTION],F1.COLUMN04 BANK,S9.COLUMN10 DEBIT,0 CREDIT
		FROM FITABLE020 S9
		INNER JOIN SATABLE001 S2 ON S2.COLUMN02 = S9.COLUMN07 AND S2.COLUMNA03 = S9.COLUMNA03
		INNER JOIN FITABLE001 F1 ON F1.COLUMN02 = S9.COLUMN06 AND F1.COLUMNA03 = S9.COLUMNA03
		WHERE S9.COLUMNA03 = @ACOWNER AND S9.COLUMN08 = @REF AND S9.COLUMN08 != ''  AND ISNULL(S9.COLUMNA13,0) = 0 
	)A

END
GO
