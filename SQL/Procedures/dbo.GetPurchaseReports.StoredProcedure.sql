USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetPurchaseReports]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetPurchaseReports]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Location    nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@DateF nvarchar(250)=null,
@HSN   nvarchar(250)= null)
as 
begin

 DECLARE @PERECENT DECIMAL(18,2)

 if @ReportName='DebitMemoBySuppliersDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and p.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and p.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and p.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and p.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and p.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end

set @Query1='
select o.COLUMN03 as OPUnit,(case when p.COLUMN50=''22335'' then s2.COLUMN05 else s.COLUMN05 end) Vendor,FORMAT(p.COLUMN06,'''+ @DateF+''') Date
,p.COLUMN06 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN09 memo,f.COLUMN07 Qty,f.COLUMN09 rate,f.COLUMN24 Amount
,p.COLUMN05 VID,p.COLUMN05 CID,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand,c1.COLUMN04 Location,c1.COLUMN02 LocationID
--EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
				,p.COLUMN03 fid from PUTABLE001 p 
left outer join PUTABLE002 f on f.COLUMN19=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN24 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and  isnull(s2.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03 
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355 and p.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' 
and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND P.COLUMNA03='+@AcOwner+ ' '+ @whereStr + '  order by Convert(DateTime,p.COLUMN06,103) desc'

exec (@Query1) 
END

Else iF @ReportName='DebitMemoBySuppliersSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and p.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
set @Query1=
'select o.COLUMN03 as OPUnit,(case when p.COLUMN50=''22335'' then s2.COLUMN05 else s.COLUMN05 end) Vendor,FORMAT(p.COLUMN06,'''+ @DateF+''') Date,p.COLUMN06 Dtt,sum(f.COLUMN07) Qty
,sum(f.COLUMN24) Amount,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand ,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,p.COLUMN04 Trans# ,p.COLUMN03 fid  , p.COLUMN05 v , '+ @FromDate +' fd , '+ @ToDate +' td  from PUTABLE001 p 
left outer join PUTABLE002 f on f.COLUMN19=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN24 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and  isnull(s2.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03 
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355  and p.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' and 
p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND P.COLUMNA03='+@AcOwner+ ' '+ @whereStr +'
group by m5.COLUMN02,m5.COLUMN04,c1.COLUMN04,c1.COLUMN02,p.COLUMN05,o.COLUMN03,s.COLUMN05,p.COLUMN06,p.COLUMN12,p.COLUMN24,p.COLUMN04  ,p.COLUMN03,p.COLUMN50,s2.COLUMN05
 order by Convert(DateTime,p.COLUMN06,103) desc '

exec (@Query1) 
end

IF  @ReportName='DebitMemoByProductDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and p.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
set @Query1='
select o.COLUMN03 as OPUnit,(case when p.COLUMN50=''22335'' then s2.COLUMN05 else s.COLUMN05 end) Vendor,FORMAT(p.COLUMN06,'''+ @DateF+''') Date
,p.COLUMN06 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN09 memo,f.COLUMN07 Qty,f.COLUMN09 rate,
f.COLUMN24 Amount,p.COLUMN05 VID,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand
 ,c1.COLUMN04 Location,c1.COLUMN02 LocationID,p.COLUMN03 fid  from PUTABLE001 p 
left outer join PUTABLE002 f on f.COLUMN19=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN24 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and  isnull(s2.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355  and p.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' and 
 p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND p.COLUMNA03='+@AcOwner+ ' '+ @whereStr +'
  order by Convert(DateTime,p.COLUMN06,103) desc'


exec (@Query1) 
end


Else if @ReportName='DebitMemoByProductSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where p.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

SET @PERECENT=(SELECT SUM(f.COLUMN24) FROM PUTABLE002 f
INNER JOIN MATABLE007 M7 ON M7.COLUMN02=f.COLUMN03 
INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=f.COLUMNA02
inner join PUTABLE001 p on p.COLUMN01=f.COLUMN19 
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355 and p.COLUMN06 between
 @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner )


set @Query1='
select o.COLUMN03 as OPUnit,m.COLUMN04 itm,L.COLUMN04 Lot,sum(f.COLUMN07) Qty,sum(f.COLUMN24) amont,CAST((100/'+ cast ( @PERECENT as nvarchar(250)) +')*(SUM(f.COLUMN24)) AS DECIMAL(18,4)) [% OF SALE],
CAST(SUM(f.COLUMN24)/SUM(f.COLUMN07)AS DECIMAL(18,2)) [AVG PRICE],'+ cast ( @PERECENT as nvarchar(250)) +' TOTAL,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand,c1.COLUMN04 Location,c1.COLUMN02 LocationID 
,p.COLUMN04 Trans# ,p.COLUMN03 fid  from PUTABLE002 f
inner join PUTABLE001 p on p.COLUMN01=f.COLUMN19 and p.COLUMNA03=f.COLUMNA03
inner join CONTABLE007 o on o.COLUMN02=f.COLUMNA02 and o.COLUMNA03=f.COLUMNA03
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 and l.COLUMNA03=f.COLUMNA03
inner join MATABLE007 m on m.COLUMN02=f.COLUMN03  and m.COLUMNA03=f.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0 and m5.COLUMNA03=f.COLUMNA03
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0 and c1.COLUMNA03=f.COLUMNA03
where isnull(p.COLUMNA13,0)=0 and isnull(f.COLUMNA13,0)=0 and isnull(c1.COLUMNA13,0)=0 and p.COLUMN03=1355 and p.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +'''
 and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND p.COLUMNA03='+@AcOwner+ ' '+ @whereStr +'  
 group by o.COLUMN03,m.COLUMN04,L.COLUMN04,p.COLUMN24,m5.COLUMN02,m5.COLUMN04 ,c1.COLUMN04 ,c1.COLUMN02 ,p.COLUMN04  ,p.COLUMN03   
 HAVING SUM(ISNULL(f.COLUMN24,0))>0 '

exec (@Query1) 
END



ELSE IF @ReportName='PurchaseByProductSummary'
BEGIN
		 if @OperatingUnit!=''
begin
 set @whereStr= ' and S10.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end 
         if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end

			
				DECLARE @PERECENT1 DECIMAL(18,2)
				DECLARE @PERECENT2 DECIMAL(18,2)

				SET @PERECENT1=(SELECT isnull((cast(SUM(S10.COLUMN08 * S10.COLUMN10) as decimal(18,2)) - cast(SUM(S10.COLUMN16 * S10.COLUMN10) as decimal(18,2))),0) 
				FROM FITABLE015 S10
				INNER JOIN FITABLE014 S9 ON S9.COLUMN01=S10.COLUMN11   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN03   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
				AND ISNULL(S9.COLUMNA13,0)=0 INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S9.COLUMN10 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				WHERE  S9.COLUMN05 BETWEEN @FromDate and @ToDate AND (S10.COLUMNA02=@OperatingUnit or S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) )AND S10.COLUMNA03=@AcOwner AND ISNULL(S10.COLUMNA13,0)=0)
				

				SET @PERECENT=(SELECT SUM(S10.COLUMN12) FROM PUTABLE006 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN04 AND S10.COLUMNA03=M7.COLUMNA03 AND S10.COLUMNA02=M7.COLUMNA02 AND ISNULL(M7.COLUMNA13,0)=0	
				INNER JOIN PUTABLE005 S9 ON S9.COLUMN01=S10.COLUMN13   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				--)
				
				WHERE S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND S9.COLUMN08 BETWEEN @FromDate and @ToDate AND S10.COLUMNA03=@AcOwner AND ISNULL(S10.COLUMNA13,0)=0)
				SET @PERECENT2= sum(isnull(@PERECENT,0))+sum(isnull(@PERECENT1,0))
				
				if(@PERECENT2!=0)
				begin

				set @Query1='

				SELECT M7.COLUMN04 ITEM,C7.COLUMN04 [OPERATING UNIT],SUM(S10.COLUMN09) QTY,SUM(S10.COLUMN12) AMOUNT,CAST((100/'+ cast ( @PERECENT2 as nvarchar(250)) +')*(SUM(S10.COLUMN12)) AS DECIMAL(18,4)) [% OF SALE],
				CAST(SUM(S10.COLUMN12)/SUM(S10.COLUMN09)AS DECIMAL(18,2)) [AVG PRICE],'+ cast ( @PERECENT as nvarchar(250)) +' TOTAL,
				 M7.COLUMN10 BrandID,m5.COLUMN04 as Brand ,S10.COLUMNA02 OPID,S9.COLUMN04 Trans#,S9.COLUMN03 fid  FROM PUTABLE006 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN04 AND S10.COLUMNA03=M7.COLUMNA03 AND S10.COLUMNA02=M7.COLUMNA02 AND ISNULL(M7.COLUMNA13,0)=0	
				INNER JOIN PUTABLE005 S9 ON S9.COLUMN01=S10.COLUMN13   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+ '
				WHERE   S9.COLUMN08 BETWEEN '''+ @FromDate +''' and '''+ @ToDate +''' AND S10.COLUMNA03='+@AcOwner+ ' '+ @whereStr +'  and 
				S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND ISNULL(S10.COLUMNA13,0)=0
								GROUP BY M7.COLUMN04,S9.COLUMN04,S9.COLUMN03,C7.COLUMN04,m5.COLUMN04,M7.COLUMN10,S10.COLUMNA02,S9.COLUMN04,S9.COLUMN03 HAVING SUM(ISNULL(S10.COLUMN12,0))>0
				UNION ALL

				SELECT M7.COLUMN04 ITEM,C7.COLUMN04 [OPERATING UNIT],(CASE  WHEN max(S10.COLUMN08)=0 then S10.COLUMN16 else S10.COLUMN08 end) QTY,
				isnull((cast(SUM(S10.COLUMN08 * S10.COLUMN10) as decimal(18,2)) - cast(SUM(S10.COLUMN16 * S10.COLUMN10) as decimal(18,2))),0)  AMOUNT,
				CAST((100/'+ cast ( @PERECENT2 as nvarchar(250)) +')*(isnull((cast(SUM(S10.COLUMN08 * S10.COLUMN10) as decimal(18,2)) - cast(SUM(S10.COLUMN16 * S10.COLUMN10) as decimal(18,2))),0)) AS DECIMAL(18,4)) [% OF SALE],
				CAST(SUM('+ cast ( @PERECENT2 as nvarchar(250)) +')/SUM(CASE  WHEN S10.COLUMN08=0 then S10.COLUMN16 else S10.COLUMN08 end) AS DECIMAL(18,2)) [AVG PRICE],'+ cast ( @PERECENT1 as nvarchar(250)) +' TOTAL ,
				M7.COLUMN10 BrandID, m5.COLUMN04 as Brand ,S10.COLUMNA02 OPID ,S9.COLUMN04 Trans#,S9.COLUMN03 fid
				FROM FITABLE015 S10
				INNER JOIN FITABLE014 S9 ON S9.COLUMN01=S10.COLUMN11   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN03   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
				AND ISNULL(S9.COLUMNA13,0)=0 INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S9.COLUMN10 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+ '
				WHERE  S9.COLUMN05 BETWEEN '''+ @FromDate +''' and '''+ @ToDate +''' AND S10.COLUMNA03='+@AcOwner+ ' '+ @whereStr +' 
				 and S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND ISNULL(S10.COLUMNA13,0)=0
				GROUP BY M7.COLUMN04,S9.COLUMN04,S9.COLUMN03,C7.COLUMN04,S10.COLUMN08,S10.COLUMN16,m5.COLUMN04,M7.COLUMN10,S10.COLUMNA02 HAVING SUM(ISNULL('+ cast ( @PERECENT1 as nvarchar(250)) +',0))>0
				
				'

exec (@Query1) 
end




			END


ELSE IF @ReportName='PurchaseByProductDetails'
	BEGIN
--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
if @OperatingUnit!=''
begin
--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end 
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Item!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
else
begin
set @whereStr=@whereStr+' and ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
end
if @HSN!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where HSNID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@HSN+''') s)'
end
else
begin
set @whereStr=@whereStr+' and HSNID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@HSN+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

exec(';With MyTable AS
(
				SELECT M7.COLUMN04 ITEM,C7.COLUMN03 [OPERATING UNIT],FORMAT((S9.COLUMN08),'''+ @DateF+''') [DATE],S9.COLUMN08 Dtt,''Bill''
				[TRANSACTION TYPE],(S9.COLUMN04) [NO],(S2.COLUMN05) [CLIENT],
				(S9.COLUMN12) [MEMO/DESCREPTION],(S10.COLUMN09) [QTY],(S10.COLUMN11) [RATE],(S10.COLUMN12) AMOUNT ,(F.COLUMN04) LOT
				,M7.COLUMN10 BrandID, m5.COLUMN04 as Brand,S9.COLUMN15 OPID  ,S9.COLUMN03 fid,MAX(M32.COLUMN04) hsncode,M2.COLUMN04 units, 
				sum(isnull(iif(t.COLUMN16 IN (23582,23583,23584),CAST(t.COLUMN17 AS DECIMAL(18,2)),0),0)) taxrate,
				cast(((isnull(S10.COLUMN09 ,0)*isnull(S10.COLUMN33 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN17 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
                cast(((isnull(S10.COLUMN09 ,0)*isnull(S10.COLUMN33 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN17 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
                cast(((isnull(S10.COLUMN09 ,0)*isnull(S10.COLUMN33 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN17 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT,
				M7.COLUMN02 ItemID,M32.COLUMN02 HSNID
				FROM PUTABLE006 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN04   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
				INNER JOIN PUTABLE005 S9 ON S9.COLUMN01=S10.COLUMN13   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN SATABLE001 S2 ON S2.COLUMN02=S9.COLUMN05    AND S10.COLUMNA03=S2.COLUMNA03 AND ISNULL(S2.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0

				left outer JOIN FITABLE043 F ON F.COLUMN02=S10.COLUMN27 AND S10.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0
				left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+ '
				left JOIN MATABLE032 M32 ON M32.COLUMN02=M7.COLUMN75 AND (M32.COLUMNA03=M7.COLUMNA03 OR M32.COLUMNA03 IS NULL) AND ISNULL(M32.COLUMNA13,0)=0
				LEFT JOIN MATABLE002 M2 ON M2.COLUMN03 =11119 AND M2.COLUMN02 = S10.COLUMN19 AND ISNULL(M2.COLUMNA13,0)=0
				left join MATABLE013 t on t.COLUMNA03='''+@AcOwner+''' and (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMNA03=S10.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(S10.COLUMN18,''-'',''''))) s)) or t.COLUMN02=S10.COLUMN18) and t.COLUMNA03=S10.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
				WHERE  S9.COLUMN08 BETWEEN '''+ @FromDate +''' and '''+ @ToDate +'''  AND S10.COLUMNA03='+@AcOwner+ ' and S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)
				 AND ISNULL(S10.COLUMNA13,0)=0
				 GROUP BY M7.COLUMN04,C7.COLUMN03,S9.COLUMN08,S9.COLUMN08 ,S9.COLUMN04,S2.COLUMN05,
				S9.COLUMN12,S10.COLUMN09,S10.COLUMN11,S10.COLUMN12,F.COLUMN04
				,M7.COLUMN10, m5.COLUMN04,S9.COLUMN15,S9.COLUMN03,M2.COLUMN04,S10.COLUMN33,M7.COLUMN02,M32.COLUMN02
			--UNION ALL
			--SELECT 
		 --M7.COLUMN04 ITEM,
		 --C7.COLUMN03 [OPERATING UNIT],
		 --FORMAT((S9.COLUMN05),'''+ @DateF+''') [DATE],S9.COLUMN05 Dtt,''Inventory Adjustments''[TRANSACTION TYPE],(S9.COLUMN04) [NO],NULL [CLIENT],
			--	(S9.COLUMN15) [MEMO/DESCREPTION],(CASE  WHEN S10.COLUMN08=null then S10.COLUMN16 else S10.COLUMN08 end) [QTY],(S10.COLUMN10) [RATE],
			--	(CASE  WHEN S10.COLUMN08=null then cast(-(S10.COLUMN16*S10.COLUMN10) as decimal(18,2)) else cast((S10.COLUMN08 * S10.COLUMN10) as decimal(18,2)) end) AMOUNT
			--	 ,(F.COLUMN04) LOT 
			--	,M7.COLUMN10 BrandID,m5.COLUMN04 as Brand,S10.COLUMNA02 OPID  ,S9.COLUMN03 fid    FROM FITABLE015 S10
			--	INNER JOIN FITABLE014 S9 ON S9.COLUMN01=S10.COLUMN11   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02
			--	INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN03   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
				
			--	 AND ISNULL(S9.COLUMNA13,0)=0
				
			--	INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S9.COLUMN10 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
			--	left outer JOIN FITABLE043 F ON F.COLUMN02=S10.COLUMN23 AND S10.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0
			--	 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+ '
			--	WHERE S9.COLUMN05 BETWEEN '''+ @FromDate +''' and '''+ @ToDate +'''  AND S10.COLUMNA03='+@AcOwner+ ' and S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)
			--	 AND ISNULL(S10.COLUMNA13,0)=0
           ) 


		 select * from MyTable '+@whereStr +' order by Dtt desc
		 '
)


		END



ELSE IF  @ReportName='PurchasesBySuppliersSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and p.COLUMN15 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and p.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and p.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
set @Query1='
select o.COLUMN03 as OPUnit,s.COLUMN05 Vendor,p.COLUMN08 Date,f.COLUMN12 Amount,p.COLUMN05 VID,p.COLUMN15 OPID,isnull(m.COLUMN10,0) BrandID, m5.COLUMN04 as Brand

,p.COLUMN04 Trans# ,p.COLUMN03 fid ,p.COLUMN05 v ,'+ @FromDate +' fd,'+ @ToDate +' td from PUTABLE005 p 
inner  join PUTABLE006 f on f.COLUMN13=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
inner join CONTABLE007 o on o.COLUMN02=p.COLUMN15 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
inner join SATABLE001 s on s.COLUMN02=p.COLUMN05  and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
inner join MATABLE007 m on m.COLUMN02=f.COLUMN04  and m.COLUMNA03=p.COLUMNA03 and  isnull(m.COLUMNA13,0)=0
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m5.COLUMNA03=p.COLUMNA03 and isnull(m5.COLUMNA13,0)=0   
  left outer join PUTABLE005 f1 on f.COLUMN13=f1.COLUMN01 and f1.COLUMNA03=f.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
 and m5.COLUMNA03='+@AcOwner+ '
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN08 between '''+ @FromDate +''' and '''+ @ToDate +''' and 
 p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND P.COLUMNA03='+@AcOwner+ ' '+ @whereStr +'

union all

select o.COLUMN03 as OPUnit,'''' Vendor,p.COLUMN05 Date,cast((f.COLUMN07+(f.COLUMN10*f.COLUMN08)+(f.COLUMN10*f.COLUMN16)) as decimal(18,2)) Amount
,'''' VID,p.COLUMN10 OPID,isnull(m.COLUMN10,0) BrandID, m5.COLUMN04 as Brand
 ,p.COLUMN04 Trans# ,1251 fid ,''''v  ,'+ @FromDate +' fd,'+ @ToDate +' td from FITABLE014 p left outer join FITABLE015 f on f.COLUMN11=p.COLUMN01 
 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN10  and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN23 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03 left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0   
 and m5.COLUMNA03='+@AcOwner+ ' where isnull(p.COLUMNA13,0)=0  and 
p.COLUMN05 between '''+ @FromDate +''' and '''+ @ToDate +''' and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND 
P.COLUMNA03='+@AcOwner+ ' '+ @whereStr +'
'

exec (@Query1) 
end

ELSE 
if @ReportName='PurchasesBySuppliersDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
exec(';With MyTable AS
(
select ''Bill'' as AA,o.COLUMN03 as OPUnit,s.COLUMN05 Vendor,FORMAT(p.COLUMN08,'''+ @DateF+''') Date,p.COLUMN08 Dtt,c.COLUMN04 TransType,
p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN12 memo,
convert(float,convert(double precision ,f.COLUMN09)) Qty,f.COLUMN11 rate,f.COLUMN12 Amount,p.COLUMN05 VID,p.COLUMN15 OPID 
,m.COLUMN10 BrandID, m5.COLUMN04 as Brand ,p.COLUMN03 fid from PUTABLE005 p 
left outer join PUTABLE006 f on f.COLUMN13=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN15 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN27 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN04 
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN04 and isnull(m5.COLUMNA13,0)=0   
 and m5.COLUMNA03='+@AcOwner+ '
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN08 between '''+ @FromDate +''' and '''+ @ToDate +'''
  and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND P.COLUMNA03='+@AcOwner+ '
 
union all

select ''Inventory Adjustments'' as AA,o.COLUMN03 as OPUnit,'''' Vendor,FORMAT(p.COLUMN05,'''+ @DateF+''') Date,p.COLUMN05 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,
m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN15 memo,
convert(float,convert(double precision ,f.COLUMN06)) Qty,f.COLUMN10 rate,cast((f.COLUMN07+(f.COLUMN10*f.COLUMN08)+(f.COLUMN10*f.COLUMN16)) as decimal(18,2)) Amount,'''' VID,p.COLUMN10 OPID 
,m.COLUMN10 BrandID, m5.COLUMN04 as Brand ,1273 fid from FITABLE014 p left outer join FITABLE015 f on f.COLUMN11=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN10  and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN23 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0  
and m5.COLUMNA03='+@AcOwner+ '
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN05 between '''+ @FromDate +''' and '''+ @ToDate +''' and p.COLUMNA02 in(SELECT ListValue 
 FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND P.COLUMNA03='+@AcOwner+ '
 )
 
 Select AA,OPUnit,Vendor,Date,Dtt,TransType,Trans#,Lot,memo,sum(rate)rate,sum(Qty)Qty,
sum(Amount)Amount,Brand,BrandID,fid from MyTable ' + @whereStr + ' group by AA,OPUnit,Vendor,Date,Dtt,TransType,Trans#,Lot,memo,Brand,BrandID,fid order by Dtt desc
 '
 )



END


 IF  @ReportName='VendorPaymentDues'
BEGIN
if @OperatingUnit!=''
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1) '
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1) '
end
end

if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end

exec(';With MyTable AS
(
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07) vendor,FORMAT(a.COLUMN04,'''+ @DateF+''') pdt,a.COLUMN04 Dtt,
a.COLUMN09 pbno, a.COLUMN12 total,a.COLUMN13 payment,
--a.COLUMN14
 (isnull(a.COLUMN12,0) - isnull(a.COLUMN13,0)) due,
FORMAT(a.COLUMN11,'''+ @DateF+''') dt,a.COLUMNA02 OPID,a.COLUMN07 VID
,a.COLUMN06 fname

,(case
 when a.COLUMN06=''BILL'' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and COLUMNA03='+@AcOwner+ ')))
 when  a.COLUMN06=''BILL PAYMENT'' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03='+@AcOwner+ '))
 when  a.COLUMN06=''Debit Memo'' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and COLUMNA03='+@AcOwner+ ')))

end) Brand 
,(case
 when a.COLUMN06=''BILL'' then (select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and  COLUMNA03='+@AcOwner+ '))
 when  a.COLUMN06=''BILL PAYMENT'' then (select (COLUMN10) from MATABLE007 where COLUMN02 = 
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03='+@AcOwner+ '))
  when a.COLUMN06=''Debit Memo'' then (select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and  COLUMNA03='+@AcOwner+ '))
  when a.COLUMN06=''JOURNAL ENTRY'' then (1)
  when a.COLUMN06=''PAYMENT VOUCHER'' then (1)
 end) BrandID 

FROM PUTABLE016 a   where  isnull((a.COLUMNA13),0)=0  AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and  a.COLUMNA03='+@AcOwner+ '
and  a.COLUMN04 between '''+ @FromDate +''' and '''+ @ToDate +''' 

union all

select (select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as ou,
(case when FITABLE020.COLUMN11=22305  then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) end) vendor,FORMAT(fitable020.COLUMN05,'''+ @DateF+''')
 pdt,fitable020.COLUMN05 Dtt,fitable020.COLUMN04 pbno,0.00 total,(fitable020.COLUMN18) payment,-isnull(fitable020.COLUMN18,0) due, null dt,
 fitable020.COLUMNA02 OPID,fitable020.COLUMN07 VID 

,cast(fitable020.COLUMN03 as varchar) fname, '''' Brand
,1 BrandID 
 from fitable020 fitable020
where COLUMN03=1363 and FITABLE020.COLUMN11=22305 and isnull((COLUMNA13),0)=0 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''')
 s) AND  COLUMNA03='+@AcOwner+ ' 
and  fitable020.COLUMN05 between '''+ @FromDate +''' and '''+ @ToDate +''' 

union all
 select ou,vendor,pdt,Dtt,pbno, 
    iif( sum(isnull(total,0))>sum(isnull(payment,0)),sum(isnull(total,0))-sum(isnull(payment,0)),0) total,
 
    iif( sum(isnull(payment,0))>sum(isnull(total,0)),sum(isnull(payment,0))-sum(isnull(total,0)),0) payment,
 
 

 sum(isnull(due,0)),dt,OPID,VID,fname,Brand,BrandID from (
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07) vendor,NULL pdt,NULL Dtt,
''PREVIOUS BALANCE'' pbno, SUM(ISNULL(a.COLUMN12,0)) total,SUM(ISNULL(a.COLUMN13,0)) payment,

SUM(ISNULL(a.COLUMN14,0))
  due,
NULL dt,a.COLUMNA02 OPID,a.COLUMN07 VID
,null fname,

iif('''+@Brand+'''='''',null

,
(case
 when a.COLUMN06=''BILL'' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and COLUMNA03='+@AcOwner+ ')))
 when  a.COLUMN06=''BILL PAYMENT'' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03='+@AcOwner+ '))
 when  a.COLUMN06=''Debit Memo'' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and COLUMNA03='+@AcOwner+ ')))

end)

) 
 Brand 
,
iif('''+@Brand+'''='''',
(case
 when a.COLUMN06=''BILL'' then (1)
  when  a.COLUMN06=''BILL PAYMENT'' then  (1)
  when a.COLUMN06=''Debit Memo'' then  (1)
  when a.COLUMN06=''JOURNAL ENTRY'' then (1)
  when a.COLUMN06=''PAYMENT VOUCHER'' then (1)
 end),

(case
 when a.COLUMN06=''BILL'' then isnull((select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and  COLUMNA03='+@AcOwner+ ')),1)
 when  a.COLUMN06=''BILL PAYMENT'' then  isnull((select (COLUMN10) from MATABLE007 where COLUMN02 = 
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03='+@AcOwner+ ')),1)
  when a.COLUMN06=''Debit Memo'' then  isnull((select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and  COLUMNA03='+@AcOwner+ ')),1)
  when a.COLUMN06=''JOURNAL ENTRY'' then (1)
  when a.COLUMN06=''PAYMENT VOUCHER'' then (1)
 end))
  BrandID   

FROM PUTABLE016 a   where  isnull((a.COLUMNA13),0)=0  AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and  a.COLUMNA03='+@AcOwner+ '
and  a.COLUMN04 < '''+ @FromDate +''' 
group by  a.COLUMNA02,a.COLUMN07
,a.COLUMN05,a.COLUMN06

union all

select (select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as ou,
(case when FITABLE020.COLUMN11=22305  then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) end) vendor,NULL pdt,NULL Dtt,
''PREVIOUS BALANCE'' pbno,0 total,(SUM(isnull(fitable020.COLUMN18,0))) payment,(SUM(isnull(fitable020.COLUMN18,0))) due, null dt,fitable020.COLUMNA02 OPID,fitable020.COLUMN07 VID 

,null fname, null Brand
,1 BrandID 
 from fitable020 fitable020
where COLUMN03=1363 and FITABLE020.COLUMN11=22305 and isnull((COLUMNA13),0)=0 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  COLUMNA03='+@AcOwner+ '  
and  fitable020.COLUMN05 < '''+ @FromDate +''' 
 group by  fitable020.COLUMNA02,fitable020.COLUMN07,FITABLE020.COLUMN11,fitable020.COLUMN03
  ) a
group by 
 a.ou,a.vendor,a.pdt,a.pbno,a.dt,a.Dtt,a.OPID,a.VID,a.fname,a.Brand,a.BrandID

)

select * from MyTable '+@whereStr+' order by Dtt desc

'
 )

end


end
GO
