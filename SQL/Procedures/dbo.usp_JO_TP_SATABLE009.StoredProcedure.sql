USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE009]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE009]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
    @COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  
    @COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,  
	@COLUMN29   nvarchar(250)=null, 
	@COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),  
	@TabelName  nvarchar(250)=null,  @ReturnValue  int=null OUTPUT,   @Totamt nvarchar(50)=null
)

AS
BEGIN
begin try 
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE009_SequenceNo
insert into SATABLE009 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20  ,COLUMN21,
  COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29, 
  COLUMN30,  COLUMN31,

   COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36, 
  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21 , 
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  
   @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36, 
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  


declare @SalesRep int 
declare @Commission DECIMAL(18,2) 
declare @number int 
declare @Type int declare @AID int
declare @BAL DECIMAL(18,2)
--declare @CBAL DECIMAL(18,2)
--declare @SBAL DECIMAL(18,2)
declare @DBAL DECIMAL(18,2)
 declare @Vid nvarchar(250)=null
 DECLARE @Tax int
 DECLARE @TaxAG int
 --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @number = (select MAX(COLUMN01) from  SATABLE009 where COLUMN02=@COLUMN02);
 set @Tax= (select COLUMN23 from SATABLE009 where COLUMN01=@number)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
--EMPHCS730 Account Owner Condition for Income Register by srinivas 7/21/2015
set @BAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMNA13=0) ;
--set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056)+ ;
--set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1053)+ISNULL(@COLUMN22,0) ;
set @DBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMNA13=0);
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
--BEGIN
--if(@AID>0)
--begin
--set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
--end
--else
--begin
--set @AID=1000;
--end
--			if(cast(isnull(@COLUMN25,0) as DECIMAL(18,2))>0)
--				begin
--					insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
--					values(@AID,@COLUMN08,'JOBBER BILL',@number,@COLUMN05,@COLUMN12,1049,isnull(@COLUMN25,0),@DBAL+cast(isnull(@COLUMN25,0) as DECIMAL(18,2)),@COLUMN14 , @COLUMNA03,0)  
--				end
--			set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
--		--Sales of Product Income creating at line level  by srinivas 7/21/2015
--			--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03)
--			--values(@AID,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,1052,NULL,@BAL+cast(isnull(@COLUMN22,0) as DECIMAL(18,2)),@COLUMN14 , @COLUMNA03)
			
--END
set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)

if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22303)
	BEGIN
		declare @PNewID int
		set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
		insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02,COLUMNA03)
		--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
		values(@PNewID,@COLUMN08,'JOBBER BILL',@number,@COLUMN05,@COLUMN12,cast(round((cast(@COLUMN22 as DECIMAL(18,2))*@Commission/100),0)as DECIMAL(18,2)),@COLUMN11,@COLUMN22,@COLUMN22,@SalesRep,@Commission,'OPEN',@COLUMNA02,@COLUMNA03) 
	END

declare @newID int
declare @tmpnewID int 
		--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
		if not exists(select COLUMN05  from PUTABLE016 where COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
		BEGIN
		set @tmpnewID=(select MAX(COLUMN02) from PUTABLE016)
		if(@tmpnewID>0)
		begin
		set @newID=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		end
		else
		begin
		set @newID=100000
		end
		insert into PUTABLE016	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14, COLUMNA02, COLUMNA03)
					values(@newID,2000,GETDATE(),@number,'JOBER BILL',@COLUMN05,@COLUMN04,null,@COLUMN10,@COLUMN20,null,@COLUMN20,@COLUMNA02,@COLUMNA03) 
		END

--IF (@COLUMN19 ='1002')
--begin
--						if not exists(select COLUMN05  from PUTABLE018 where  COLUMN05=@COLUMN04 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
--							BEGIN 
--								set @tmpnewID=(select MAX(COLUMN02) from PUTABLE018)
--								if(@tmpnewID>0)
--									begin
--										set @newID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1
--									end
--								else
--									begin
--										set @newID=100000
--									end
--				--EMPHCS891 rajasekhar reddy patakota 10/8/2015 Account receivable amount should be (Total amount - Discount amount) and excluding taxes
--insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02, COLUMNA03)
--				values(@newID,3000,GETDATE(),@COLUMN04,'JOBBER BILL',@COLUMN05,@COLUMN12,cast((cast(isnull(@COLUMN22,0) as decimal(18,2))-cast(isnull(@COLUMN25,0) as decimal(18,2)))as decimal(18,2)),@COLUMN10,cast((cast(isnull(@COLUMN22,0) as decimal(18,2))-cast(isnull(@COLUMN25,0) as decimal(18,2)))as decimal(18,2)),'Open',@COLUMN14,@COLUMNA03)
--							END
--						else
--							BEGIN
--								set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@COLUMN04 )
--								update PUTABLE018 set COLUMN04=GETDATE(),COLUMN09=(cast(isnull(@Totamt,0) as DECIMAL(18,2))+cast((cast(isnull(@COLUMN22,0) as decimal(18,2))-cast(isnull(@COLUMN25,0) as decimal(18,2)))as decimal(18,2))),COLUMN12=(cast(isnull(@Totamt,0) as DECIMAL(18,2))+cast((cast(isnull(@COLUMN22,0) as decimal(18,2))-cast(isnull(@COLUMN25,0) as decimal(18,2)))as decimal(18,2))) where COLUMN05=@COLUMN04
--							END
--end


--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0)
--		BEGIN
--			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMNA02, COLUMNA03)
--			values(@Vid,@COLUMN08,'Invoice',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@COLUMN24,@COLUMNA02, @COLUMNA03)
--		END
--EMPHCS794 Packing and Shipping Charges Calculation in Sales Order and Invoice Done by Srinivas 7/30/2015 
SET @BAL=(isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN36 as decimal(18,2)),0))
if(@BAL>0)
		BEGIN
		set @newID=(select MAX(COLUMN02) from FITABLE036)+1
			insert into FITABLE036 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,    COLUMN08,  COLUMN09, COLUMN10,  
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
	   ISNULL(@newID,10001),  'JOBBER BILL',  @COLUMN08,  @COLUMN12,  @COLUMN05,  @BAL,  @COLUMN04,1075,
	   @COLUMNA02, @COLUMNA03
	)
		END
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application		
set @ReturnValue =(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
END
 

IF @Direction = 'Select'
BEGIN
select * from SATABLE009
END 

  
IF @Direction = 'Update'
BEGIN
UPDATE SATABLE009 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,     
   COLUMN19=@COLUMN19,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,     COLUMN22=@COLUMN22,     COLUMN23=@COLUMN23,
   COLUMN24=@COLUMN24,     COLUMN25=@COLUMN25,     COLUMN26=@COLUMN26,     COLUMN27=@COLUMN27,     COLUMN28=@COLUMN28,
   COLUMN29=@COLUMN29,	   COLUMN30=@COLUMN30,	   COLUMN31=@COLUMN31,     COLUMN32=@COLUMN32,     COLUMN33=@COLUMN33,
   COLUMN34=@COLUMN34,     COLUMN35=@COLUMN35,     COLUMN36=@COLUMN36,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
      COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   SET @number=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
	set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 )
	--EMPHCS891 rajasekhar reddy patakota 10/8/2015 Account receivable amount should be (Total amount - Discount amount) and excluding taxes
	--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
	--update PUTABLE018 set COLUMN04=@COLUMN08,COLUMN09=cast((cast(isnull(@COLUMN22,0) as decimal(18,2))-cast(isnull(@COLUMN25,0) as decimal(18,2)))as decimal(18,2)),COLUMN12=cast((cast(isnull(@COLUMN22,0) as decimal(18,2))-cast(isnull(@COLUMN25,0) as decimal(18,2)))as decimal(18,2)) where COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	update PUTABLE016 set COLUMN04=@COLUMN08,COLUMN12=@COLUMN20,COLUMN14=@COLUMN20 where COLUMN05=@number and COLUMN06='JOBER BILL' and COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03

	declare @SOID nvarchar(250),
	@QtyH nvarchar(250),
	@QtyA nvarchar(250),
	@QtyC nvarchar(250),
	@Item nvarchar(250),
	@AMOUNT decimal(18,2),
	@AMOUNT1 decimal(18,2),
	@ItemQty nvarchar(250),
	@Price nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250),
	--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
	@id nvarchar(250),@uom nvarchar(250)
	SET @COLUMN06=(SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)
	if(@COLUMN06=0)
	BEGIN
		DECLARE @MaxRownum INT
		  DECLARE @Initialrow INT=1
		  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
		  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15=@SOID
		  OPEN cur1
		  FETCH NEXT FROM cur1 INTO @id
		  SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15=@SOID)
		     WHILE @Initialrow <= @MaxRownum
		     BEGIN 
		         set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
		         --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
			 set @uom=(select COLUMN22 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
		         set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
		         set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
		         set @QtyH=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)
		         set @QtyA=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)
		         set @QtyC=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)
			 --EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
				 UPDATE SATABLE010 SET COLUMN09=(cast((select Max(column09) from satable010 WHERE COLUMN02=@id)as decimal(18,2))-cast(@ItemQty as decimal(18,2)))  WHERE COLUMN02=@id
				 --EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Inventory Quantity Calculation
				 if((SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN01=@SOID)=0)				 
				 begin
				 UPDATE FITABLE010 SET COLUMN04=cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   --COLUMN05=cast(@QtyC as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   COLUMN08=cast((cast(@QtyH as decimal(18,2))-cast(@QtyC as decimal(18,2))) as decimal(18,2))
				 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				UPDATE FITABLE010 SET COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@Price as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
                UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)as decimal(18,2))/cast((cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2))) as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				
				 end
				 FETCH NEXT FROM cur1 INTO @id
		         SET @Initialrow = @Initialrow + 1 
			END
		CLOSE cur1 
  END
  
	SET @COLUMN06=(SELECT (COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)
  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur4 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0
      OPEN cur4
	  FETCH NEXT FROM cur4 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
			 set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMNA13=0)
             set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
             set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
			 
             --BILLED QTY Updation
			    set @COLUMN13=(select isnull(COLUMN13,0) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item and COLUMNA13=0)
				--UPDATE SATABLE010 SET COLUMN12=(select COLUMN09 from SATABLE010 where COLUMN02=@id) WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
				UPDATE SATABLE006 SET   COLUMN13=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMNA13=0 and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06))
				UPDATE PUTABLE013 SET COLUMN11=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
				delete from PUTABLE017 where COLUMN05=@number and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN14 and COLUMNA03=@COLUMNA03
			 FETCH NEXT FROM cur4 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur4 

  UPDATE SATABLE010 SET COLUMNA13=1 WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
 -- if(cast(isnull(@COLUMN25,0) as decimal(18,2))>0)
	--BEGIN
	--SET @AMOUNT=(SELECT isnull(COLUMN09,0) FROM FITABLE025 WHERE COLUMN05=@number AND COLUMN08=1049  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--SET @AMOUNT1=(SELECT isnull(COLUMN10,0) FROM FITABLE025 WHERE COLUMN05=@number AND COLUMN08=1049  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--IF(cast(@COLUMN25 as decimal(18,2))>@AMOUNT)
	--BEGIN
	--SET @BAL=(cast(@COLUMN25 as decimal(18,2))-@AMOUNT)
	--	UPDATE FITABLE025 SET COLUMN09=isnull(@COLUMN25,0),COLUMN10=@AMOUNT1+cast(@BAL as decimal(18,2)), COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--END
	--ELSE IF(cast(@COLUMN25 as decimal(18,2))<@AMOUNT)
	--BEGIN
	--SET @BAL=(-(@AMOUNT-cast(@COLUMN25 as decimal(18,2))))
	--	UPDATE FITABLE025 SET COLUMN09=isnull(@COLUMN25,0),COLUMN10=@AMOUNT1+cast(@BAL as decimal(18,2)), COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--END
	--ELSE IF(@AMOUNT=0)
	--BEGIN
	--SET @BAL=(@AMOUNT+cast(@COLUMN25 as decimal(18,2)))
	--	set @DBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 );
	--	set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
	--	insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
	--	values(@AID,@COLUMN08,'JOBBER BILL',@number,@COLUMN05,@COLUMN12,1049,isnull(@COLUMN25,0),@DBAL+cast(isnull(@COLUMN25,0) as DECIMAL(18,2)),@COLUMN14 , @COLUMNA03,0)  
	--END
	--else
	--begin
	--UPDATE FITABLE025 SET  COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--end
	
	--	  DECLARE cur2 CURSOR FOR SELECT COLUMN02 from FITABLE025 where cast(COLUMN05 as int)>@number AND  COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0
	--	  OPEN cur2
	--	  set @Initialrow =1
	--	  FETCH NEXT FROM cur2 INTO @id
	--	  SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE025 where cast(COLUMN05 as int)>@number AND  COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--	     WHILE @Initialrow <= @MaxRownum
	--	     BEGIN 
	--	         set @Item=(select COLUMN10 from FITABLE025 where COLUMN02=@id)
	--		     UPDATE FITABLE025 SET COLUMN10=cast(@Item as decimal(18,2))+cast(@BAL as decimal(18,2))
	--				WHERE COLUMN02=@id AND  COLUMN08=1049
	--			 FETCH NEXT FROM cur1 INTO @id
	--	         SET @Initialrow = @Initialrow + 1 
	--		END
	--	CLOSE cur2 
	--END
	set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)  
		  
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		  
	set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
	set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
	set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)
	
	if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22303)
		BEGIN
			UPDATE FITABLE027 SET COLUMN10=@COLUMN22,COLUMN13=CAST(ISNULL(@COLUMN22,0) AS decimal(18,2))-CAST(ISNULL(COLUMN11,0) AS decimal(18,2))
			--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
				WHERE COLUMN04='JOBBER BILL' AND COLUMN05=@number AND COLUMN06=@COLUMN05 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		END
--EMPHCS794 Packing and Shipping Charges Calculation in Sales Order and Invoice Done by Srinivas 7/30/2015 
		SET @BAL=(isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN36 as decimal(18,2)),0))
		if(@BAL>0)
		BEGIN
		UPDATE FITABLE036 SET COLUMN08=@BAL
		--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
		WHERE COLUMN09=@COLUMN04 AND COLUMN10=1075 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='JOBBER BILL'
		
		END
	delete from FITABLE036 where COLUMN05 in(select COLUMN01 from SATABLE010 where COLUMN15 in(@number)) and COLUMN03='JOBBER BILL' and  COLUMNA03=@COLUMNA03 AND COLUMN10!=1075
	delete from FITABLE025 where COLUMN05 in(select COLUMN01 from SATABLE010 where COLUMN15 in(@number)) and COLUMN04='JOBBER BILL' and  COLUMNA03=@COLUMNA03 AND COLUMN10!=1075

END


else IF @Direction = 'Delete'
BEGIN
UPDATE SATABLE009 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
SET @number=(SELECT COLUMN01 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN25=(SELECT COLUMN25 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN14=(SELECT COLUMN14 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN06=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA02=(SELECT COLUMNA02 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA03=(SELECT COLUMNA03 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
UPDATE FITABLE027 SET COLUMNA13=@COLUMNA13 WHERE COLUMN04='JOBBER BILL' AND COLUMN05=@number
--EMPHCS892 rajasekhar reddy patakota 8/9/2015 After deletion of invoice account receivable records should delete and status also should change based on the scenario
--update PUTABLE018 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
delete from PUTABLE018  where COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03	
if((SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)=0)
	BEGIN
	SET @Initialrow = 1
		  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
		  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15= @SOID
		  OPEN cur1
		  FETCH NEXT FROM cur1 INTO @id
		  SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15=@SOID)
		     WHILE @Initialrow <= @MaxRownum
		     BEGIN 
		     --EMPHCS907 rajasekhar reddy patakota 11/08/2015 After deleting invoice - Taxes are not cleared
		         UPDATE FITABLE026 SET COLUMNA13=@COLUMNA13 WHERE COLUMN05=(select COLUMN01 from SATABLE010 where COLUMN02=@id)  AND COLUMN04='Invoice' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
				 set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
		         set @uom=(select COLUMN22 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
		         set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
		         set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
		         set @QtyH=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)
		         set @QtyA=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)
		         set @QtyC=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14)
				 --EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Inventory Quantity Calculation
				 if((SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN01=@SOID)=0)				 
				 begin
				 UPDATE FITABLE010 SET COLUMN04=cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   --COLUMN05=cast(@QtyC as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   COLUMN08=cast((cast(@QtyH as decimal(18,2))-cast(@QtyC as decimal(18,2))) as decimal(18,2))
				 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				UPDATE FITABLE010 SET COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@Price as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
                UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)as decimal(18,2))/cast((cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2))) as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				
				 end
				 FETCH NEXT FROM cur1 INTO @id
		         SET @Initialrow = @Initialrow + 1 
			END
		CLOSE cur1 
  END
  --EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
	SET @COLUMN06=(SELECT (COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)
  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur4 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0
      OPEN cur4
	  FETCH NEXT FROM cur4 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
	                 --EMPHCS907 rajasekhar reddy patakota 11/08/2015 After deleting invoice - Taxes are not cleared
			 UPDATE FITABLE026 SET COLUMNA13=@COLUMNA13 WHERE COLUMN05=(select COLUMN01 from SATABLE010 where COLUMN02=@id)  AND COLUMN04='Invoice' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
			 set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMNA13=0)
             set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
             set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
			 
             --BILLED QTY Updation
			    set @COLUMN13=(select isnull(COLUMN13,0) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item and COLUMNA13=0)
				--UPDATE SATABLE010 SET COLUMN12=(select COLUMN09 from SATABLE010 where COLUMN02=@id) WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
				UPDATE SATABLE006 SET   COLUMN13=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMNA13=0 and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06))
				UPDATE PUTABLE013 SET COLUMN11=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
				delete from PUTABLE017 where COLUMN05=@number and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN14 and COLUMNA03=@COLUMNA03
			 FETCH NEXT FROM cur4 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur4 
  --EMPHCS892 rajasekhar reddy patakota 8/9/2015 After deletion of invoice account receivable records should delete and status also should change based on the scenario
  if((SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)>0)
	BEGIN
     declare @BillQty DECIMAL(18,2),@PBillQty DECIMAL(18,2),@IQty DECIMAL(18,2),@IRQty DECIMAL(18,2), @PBQty DECIMAL(18,2),@Result nvarchar(250),@RectNO nvarchar(250)
     SET @BillQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMNA13=0)
     SET @IRQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMNA13=0)
     SET @IQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMNA13=0 )
     SET @RectNO=(SELECT max(COLUMN04) FROM SATABLE010 WHERE COLUMN15 in (@number))
     IF(@IRQty=(@IQty))
     begin
     if(@IRQty=@BillQty)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
     end
     else if(@BillQty=0)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
     end
     else
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
     end
     end
     ELSE
     BEGIN
     if(@IQty=@IRQty)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
     end
     else if(@BillQty=0)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
     end
     else
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
     end
     end
     UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE009 where COLUMN01= @number))
     declare @invoiceqty decimal(18,2),@issueqty decimal(18,2)
     set @invoiceqty=(select sum(isnull(COLUMN10,0)) from SATABLE010 where  COLUMN04=@RectNO  and COLUMNA13=0)
     set @issueqty=(select sum(isnull(COLUMN09,0)) from SATABLE008 where    columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN02 in(@RectNO)))
     if(@invoiceqty=0.00)
     begin
     UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=27) where COLUMN02=@RectNO
     end
     else if(@invoiceqty=@issueqty)
     begin
     UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=81) where COLUMN02=@RectNO
     end
     else if(@invoiceqty<@issueqty)
     begin
     UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=29) where COLUMN02=@RectNO
     end
	 end
  UPDATE SATABLE010 SET COLUMNA13=1 WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
  --EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
 -- if(cast(isnull(@COLUMN25,0) as decimal(18,2))>0)
	--BEGIN
	--SET @AMOUNT=(SELECT isnull(COLUMN09,0) FROM FITABLE025 WHERE COLUMN05=@number AND COLUMN08=1049  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--SET @AMOUNT1=(SELECT isnull(COLUMN10,0) FROM FITABLE025 WHERE COLUMN05=@number AND COLUMN08=1049  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--IF(cast(@COLUMN25 as decimal(18,2))>@AMOUNT)
	--BEGIN
	--SET @BAL=(cast(@COLUMN25 as decimal(18,2))-@AMOUNT)
	--	UPDATE FITABLE025 SET COLUMN09=isnull(@COLUMN25,0),COLUMN10=@AMOUNT1+cast(@BAL as decimal(18,2)), COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--END
	--ELSE IF(cast(@COLUMN25 as decimal(18,2))<@AMOUNT)
	--BEGIN
	--SET @BAL=(-(@AMOUNT-cast(@COLUMN25 as decimal(18,2))))
	--	UPDATE FITABLE025 SET COLUMN09=isnull(@COLUMN25,0),COLUMN10=@AMOUNT1+cast(@BAL as decimal(18,2)), COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--END
	--ELSE IF(@AMOUNT=0)
	--BEGIN
	--SET @BAL=(@AMOUNT+cast(@COLUMN25 as decimal(18,2)))
	--	set @DBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 );
	--	set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
	--	insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
	--	values(@AID,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,1049,isnull(@COLUMN25,0),@DBAL+cast(isnull(@COLUMN25,0) as DECIMAL(18,2)),@COLUMN14 , @COLUMNA03,0)  
	--END
	--else
	--begin
	--UPDATE FITABLE025 SET  COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--end
	
	--	  DECLARE cur2 CURSOR FOR SELECT COLUMN02 from FITABLE025 where cast(COLUMN05 as int)>@number AND  COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0
	--	  OPEN cur2
	--	  set @Initialrow =1
	--	  FETCH NEXT FROM cur2 INTO @id
	--	  SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE025 where cast(COLUMN05 as int)>@number AND  COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--	     WHILE @Initialrow <= @MaxRownum
	--	     BEGIN 
	--	         set @Item=(select COLUMN10 from FITABLE025 where COLUMN02=@id)
	--		     UPDATE FITABLE025 SET COLUMN10=cast(@Item as decimal(18,2))+cast(@BAL as decimal(18,2))
	--				WHERE COLUMN02=@id AND  COLUMN08=1049
	--			 FETCH NEXT FROM cur1 INTO @id
	--	         SET @Initialrow = @Initialrow + 1 
	--		END
	--	CLOSE cur2 
	--END
	set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)  
	UPDATE FITABLE025 SET  COLUMNA13=1 WHERE COLUMN05=@number AND COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		  
	set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
	set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
	set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)
	
	if(@SalesRep!=0 and cast(@Commission as DECIMAL(18,2))>0 and @Type!=0 and @Type=22303)
		BEGIN
			UPDATE FITABLE027 SET COLUMN10=@COLUMN22,COLUMN13=CAST(ISNULL(@COLUMN22,0) AS decimal(18,2))-CAST(ISNULL(COLUMN11,0) AS decimal(18,2))
				WHERE COLUMN04='INVOICE' AND COLUMN05=@number AND COLUMN06=@COLUMN05 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		END
--EMPHCS794 Packing and Shipping Charges Calculation in Sales Order and Invoice Done by Srinivas 7/30/2015 
		SET @BAL=(isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN36 as decimal(18,2)),0))
		if(@BAL>0)
		BEGIN
		UPDATE FITABLE036 SET COLUMN08=@BAL,columna13=@columna13
		--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
		WHERE COLUMN09=@COLUMN04 AND COLUMN10=1075 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='JOBBER BILL'
		
		END
	delete from FITABLE036 where COLUMN05 in(select COLUMN01 from SATABLE010 where COLUMN15 in(@number)) and COLUMN03='JOBBER BILL' and  COLUMNA03=@COLUMNA03 AND COLUMN10!=1075
	delete from FITABLE025 where COLUMN05 in(select COLUMN01 from SATABLE010 where COLUMN15 in(@number)) and COLUMN04='JOBBER BILL' and  COLUMNA03=@COLUMNA03 AND COLUMN10!=1075

END
end try
begin catch

DECLARE @tempSTR NVARCHAR(MAX)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(250))))
    exec [CheckDirectory] @tempSTR,'Exception_JO_SATABLE009.txt',0

return 0
end catch

end








GO
