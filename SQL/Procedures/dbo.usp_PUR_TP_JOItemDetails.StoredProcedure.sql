USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_JOItemDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_JOItemDetails]
(
	@ItemID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL,
	@StockItem nvarchar(250)=NULL,@JO nvarchar(250)=NULL
)
AS
BEGIN
    if(@ItemID !='')
	begin
    if(@OPUnit !='' or @OPUnit !=null)
	begin
	select MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN09,MA07.COLUMN42,MA07.COLUMN43,MA07.COLUMN44,MA07.COLUMN10,(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end)COLUMN17 ,MA07.COLUMN45 ,MA07.COLUMN63 ,iif(isnull(f.COLUMN07,0)>0,isnull(f.COLUMN07,0),isnull(jl.COLUMN09,0)) as FG from MATABLE007 MA07 
	--left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02
	--inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
	--EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
	left join SATABLE005 s on s.column02=@JO
	left join SATABLE007 ji on s.column02=ji.COLUMN06 and s.columnA03=ji.columnA03 and isnull(ji.columnA13,0)=0
	--left join (select sum(isnull(COLUMN09,0)) COLUMN09,column14,COLUMNA03,COLUMNA13 from SATABLE008  group by column14,COLUMNA03,COLUMNA13) jl on jl.column14=ji.COLUMN01 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join SATABLE008 jl on jl.column14=ji.COLUMN01 and jl.column04=MA07.COLUMN02 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join MATABLE024 pp on pp.COLUMN07=MA07.COLUMN02 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=@AcOwner and pp.COLUMNA13=0 
	left outer join FITABLE038 f on f.COLUMN04=s.COLUMN04 and f.COLUMN06=@ItemID and f.COLUMN08=MA07.COLUMN63 and f.COLUMN06=MA07.COLUMN67 and f.COLUMNA03=@AcOwner and f.COLUMNA02=@OPUnit 
	 WHERE   (isnull(MA07.COLUMN47,'False')='False' or MA07.COLUMN47 is null) and (MA07.COLUMN02=@ItemID or MA07.COLUMN06= CONVERT(varchar(50), @ItemID) ) and ( MA07.COLUMNA02 = @OPUnit or MA07.COLUMNA02 is null) and  MA07.COLUMNA03=@AcOwner
        end
		else if(@AcOwner !='' or @AcOwner !=null)
	begin
	select MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN09,MA07.COLUMN42,MA07.COLUMN43,MA07.COLUMN44,MA07.COLUMN10,(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end)COLUMN17,MA07.COLUMN45 ,MA07.COLUMN63,iif(isnull(f.COLUMN07,0)>0,isnull(f.COLUMN07,0),isnull(jl.COLUMN09,0)) as FG from MATABLE007 MA07 
	--left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02
	--inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
	--EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
	left outer join SATABLE005 s on s.column02=@JO
	left join SATABLE007 ji on s.column02=ji.COLUMN06 and s.columnA03=ji.columnA03 and isnull(ji.columnA13,0)=0
	--left join (select sum(isnull(COLUMN09,0)) COLUMN09,column14,COLUMNA03,COLUMNA13 from SATABLE008  group by column14,COLUMNA03,COLUMNA13) jl on jl.column14=ji.COLUMN01 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join SATABLE008 jl on jl.column14=ji.COLUMN01 and jl.column04=MA07.COLUMN02 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join MATABLE024 pp on pp.COLUMN07=MA07.COLUMN02 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=@AcOwner and pp.COLUMNA13=0 
	left outer join FITABLE038 f on f.COLUMN04=s.COLUMN04 and f.COLUMN06=@ItemID and f.COLUMN08=MA07.COLUMN63 and f.COLUMN06=MA07.COLUMN67 and f.COLUMNA03=@AcOwner and f.COLUMNA02=@OPUnit 
	 WHERE   isnull(MA07.COLUMN47,'False')='False' and  (MA07.COLUMN02=@ItemID or MA07.COLUMN06=@ItemID) and  MA07.COLUMNA03=@AcOwner
        end
		else
	begin
	select MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN09,MA07.COLUMN42,MA07.COLUMN43,MA07.COLUMN44,MA07.COLUMN10,(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end)COLUMN17 ,MA07.COLUMN45 ,MA07.COLUMN63,iif(isnull(f.COLUMN07,0)>0,isnull(f.COLUMN07,0),isnull(jl.COLUMN09,0)) as FG from MATABLE007 MA07
	 --left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02
	--inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
	--EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
	left outer join SATABLE005 s on s.column02=@JO
	left join SATABLE007 ji on s.column02=ji.COLUMN06 and s.columnA03=ji.columnA03 and isnull(ji.columnA13,0)=0
	--left join (select sum(isnull(COLUMN09,0)) COLUMN09,column14,COLUMNA03,COLUMNA13 from SATABLE008  group by column14,COLUMNA03,COLUMNA13) jl on jl.column14=ji.COLUMN01 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join SATABLE008 jl on jl.column14=ji.COLUMN01 and jl.column04=MA07.COLUMN02 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join MATABLE024 pp on pp.COLUMN07=MA07.COLUMN02 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=@AcOwner and pp.COLUMNA13=0 
	left outer join FITABLE038 f on f.COLUMN04=s.COLUMN04 and f.COLUMN06=@ItemID and f.COLUMN08=MA07.COLUMN63 and f.COLUMN06=MA07.COLUMN67 and f.COLUMNA03=@AcOwner and f.COLUMNA02=@OPUnit 
	 WHERE   isnull(MA07.COLUMN47,'False')='False' and  (MA07.COLUMN02=@ItemID or MA07.COLUMN06=@ItemID) 
        end
		end
	 else
	 begin
         select MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN09,MA07.COLUMN42,MA07.COLUMN43,MA07.COLUMN44,MA07.COLUMN10,(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end)COLUMN17  ,MA07.COLUMN45 ,MA07.COLUMN63,iif(isnull(f.COLUMN07,0)>0,isnull(f.COLUMN07,0),isnull(jl.COLUMN09,0)) as FG from MATABLE007 MA07
	 --left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02  
	left outer join SATABLE005 s on s.column02=@JO
	left join SATABLE007 ji on s.column02=ji.COLUMN06 and s.columnA03=ji.columnA03 and isnull(ji.columnA13,0)=0
	--left join (select sum(isnull(COLUMN09,0)) COLUMN09,column14,COLUMNA03,COLUMNA13 from SATABLE008  group by column14,COLUMNA03,COLUMNA13) jl on jl.column14=ji.COLUMN01 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join SATABLE008 jl on jl.column14=ji.COLUMN01 and jl.column04=MA07.COLUMN02 and jl.columnA03=ji.columnA03 and isnull(jl.columnA13,0)=0
	left join MATABLE024 pp on pp.COLUMN07=MA07.COLUMN02 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=@AcOwner and pp.COLUMNA13=0 
	left outer join FITABLE038 f on f.COLUMN04=s.COLUMN04 and f.COLUMN06=@ItemID and f.COLUMN08=MA07.COLUMN63 and f.COLUMN06=MA07.COLUMN67 and f.COLUMNA03=@AcOwner and f.COLUMNA02=@OPUnit 
	 WHERE    MA07.COLUMN47='False' and  ( MA07.COLUMN02=@StockItem or MA07.COLUMN06=@StockItem)
	 end
END



GO
