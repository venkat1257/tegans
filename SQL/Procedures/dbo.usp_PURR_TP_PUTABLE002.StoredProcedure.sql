USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PURR_TP_PUTABLE002]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PURR_TP_PUTABLE002]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null, 
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null, 
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null, 
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null, 
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,  
	@COLUMN37   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null, 
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  nvarchar(100)=null,  @Direction  nvarchar(250),
	@TabelName  nvarchar(250)=null,	 @ReceiptType  nvarchar(250)=null,@Type  nvarchar(250)=null,
	@ReturnValue nvarchar(250)=null, @Qty_On_Hand decimal(18,2)=null, @Qty_On_Hand1 decimal(18,2)=null, 
	@Qty_On_Avl  decimal(18,2)=null, @newID int=null,                 @tmpnewID1 int=null
)
AS
BEGIN
begin try
 declare @tempSTR nvarchar(max)
 declare @partytyp nvarchar(250)
 set @partytyp=(select isnull(COLUMN50,'22305') from PUTABLE001 where COLUMN01=@COLUMN19)
set @COLUMN26=(select iif((@COLUMN26!='' and @COLUMN26>0 ),@COLUMN26,(select max(column02) from fitable037 where column07=1 and column08=1 )))
IF @Direction = 'Insert'
BEGIN
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
  declare @FRMID int, @date nvarchar(250)=null
  --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
   set @FRMID= (SELECT COLUMN03 FROM PUTABLE001 WHERE COLUMN01=@COLUMN19)
   --set @COLUMN19 =(select MAX(COLUMN01) from  PUTABLE001);
   select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE002_SequenceNo
     set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= '0' ) THEN (select COLUMN24 from PUTABLE001 
   where COLUMN01=@COLUMN19) else @COLUMNA02  END ) 
   set @ReceiptType =(select COLUMN29 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN20 =(select COLUMN24 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN21 =(select COLUMN25 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN22 =(select COLUMN26 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN23 =(select COLUMN27 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @date =(select COLUMN06 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
insert into PUTABLE002 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12, 
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN24,  COLUMN25, COLUMN26,   COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
   COLUMN37,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,  
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
   COLUMND09, COLUMND10 
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11, 
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN26,  @COLUMN07,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN37,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08,  
   @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,  
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03,  
   @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10 
)  
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
declare @RefundQty decimal(18,2),@RefID int,@Refreceipt int,@TQty decimal(18,2),@RRQty decimal(18,2),@RIQty decimal(18,2),@PIQty decimal(18,2),@PID int,@Project nvarchar(250)
declare @Result nvarchar(250),@POID nvarchar(250),@PONum nvarchar(250),@BillNo nvarchar(250),@BillId nvarchar(250),@CharAcc nvarchar(250)
set @Refreceipt=(select COLUMN33 from PUTABLE001 where COLUMN01=@COLUMN19);
set @BillNo=(select COLUMN48 from PUTABLE001 where COLUMN01=@COLUMN19);
set @BillId=(select COLUMN01 from PUTABLE005 where COLUMN02=@BillNo);
set @RefID=(select COLUMN06 from SATABLE007 where COLUMN02=@Refreceipt);
set @PID=(select COLUMN01 from SATABLE005 where COLUMN02=@RefID);
set @PONum=(select COLUMN04 from PUTABLE001 where COLUMN04=(select COLUMN11 from SATABLE005 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @POID=(select COLUMN02 from PUTABLE001 where COLUMN04=(select COLUMN11 from SATABLE005 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @Project = (select COLUMN36 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin	
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(ISNULL(@COLUMN34,0)>0)
begin
set @RefundQty=(select sum(isnull(COLUMN13,0)) from SATABLE006 where COLUMN02=@COLUMN34 and isnull(COLUMNA13,0)=0)
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN02=@COLUMN34 and isnull(COLUMNA13,0)=0
end
else
begin
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @RefundQty=isnull((select sum(COLUMN13) from SATABLE006 where COLUMN19 in (@PID) and COLUMN03=@COLUMN03 and isnull(COLUMN27,0)=isnull(@COLUMN26,0) and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and isnull(COLUMNA13,0)=0),0);
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN03=@COLUMN03 and isnull(COLUMN27,0)=isnull(@COLUMN26,0) and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and isnull(COLUMNA13,0)=0 and COLUMN19 in (@PID)
end
SET @TQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN33=CAST(@Refreceipt AS NVARCHAR(250))) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RIQty=(SELECT sum(isnull(COLUMN12,0)) FROM SATABLE006 WHERE COLUMN19 in(@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @PIQty=(SELECT sum(isnull(COLUMN09,0)) FROM SATABLE008 WHERE COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN02=@Refreceipt) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
end
else
begin
set @TQty=(SELECT sum(isnull(COLUMN09,0)) FROM PUTABLE006 WHERE COLUMN13 in(@BillId) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN48=@BillNo) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @PIQty=(@TQty)
end
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN19=@COLUMN19 and COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=isnull(@COLUMN26,0) and isnull(COLUMN17,0)=isnull(@COLUMN17,0)
if(@PIQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
end
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@COLUMN19)

if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin
UPDATE SATABLE007 set COLUMN14=@Result where COLUMN02=(@Refreceipt)
IF(@TQty=@RIQty)
begin
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=98)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=99)
end
end
ELSE 
BEGIN
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=96)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=97)
end
end
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@PID)
END

if((@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0') and (@BillNo='' or isnull(@BillNo,0)=0 or isnull(@BillNo,0)='0'))
begin
UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=61) where COLUMN01=(@COLUMN19)
end
if exists(select column01 from putable002 where column19=@column19)
begin 
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
Declare @CustID int, @memo nvarchar(250),@Num nvarchar(250),@dueDT date,@tmpnewID int,@Totamt nvarchar(250),
@Num1 nvarchar(250) , @Creditamt nvarchar(50),@Tax nvarchar(250), @TaxAG nvarchar(250), @TAXRATE nvarchar(250),@TaxColumn17 nvarchar(250),@TTYPE nvarchar(250),
@HeaderTax nvarchar(250), @HeaderTaxAG nvarchar(250), @HeaderTAXRATE nvarchar(250),@HeaderTaxColumn17 nvarchar(250), @ltaxamt decimal(18,2)
set @CustID = (select COLUMN05 from PUTABLE001 where COLUMN01=@COLUMN19)
set @memo = (select COLUMN09 from PUTABLE001 where COLUMN01=@COLUMN19)
	declare @lIID nvarchar(250)
	set @lIID=(select COLUMN01 from PUTABLE002 WHERE COLUMN02=@COLUMN02)
	set @Num = (select COLUMN01 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Num1 = (select COLUMN04 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project = (select COLUMN36 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
--set @Tax= (select COLUMN25 from PUTABLE002 where COLUMN02=@COLUMN02)
--   set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
--  set @TAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
--set @HeaderTax= (select COLUMN31 from PUTABLE001 where COLUMN01=@COLUMN19)
--   set @HeaderTaxAG= (select column16 from MATABLE013 where COLUMN02=@HeaderTax)
--  set @HeaderTAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@HeaderTax)
declare @mode nvarchar(250),@TaxString nvarchar(250)=NULL,@TAXRATEM decimal(18,2)

set @Tax= (select COLUMN25 from PUTABLE002 where COLUMN02=@COLUMN02)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN24;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN25,0) from PUTABLE002 where COLUMN02=@COLUMN02
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end

	set @headerTax= (select COLUMN31 from PUTABLE001 where COLUMN01=@COLUMN19)
	select @TaxAG=column16, @TAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@Tax
	select @headerTaxAG=column16,@headerTAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@headerTax
		if(@partytyp=22335)
begin
set @mode='OUTPUT '
set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
set @HeaderTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@HeaderTax)
end
else
begin
set @mode='INPUT '
end
		if(@PONum=(select COLUMN11 from SATABLE005 where COLUMN02=@RefID))
BEGIN
if exists(select 1 from PUTABLE005 where COLUMN06=@POID and COLUMNA03=@COLUMNA03)
begin
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0  AND @TaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
  if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0  AND @HeaderTaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @HeaderTaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
end
END
else
BEGIN
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0  AND @TaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0 AND @HeaderTaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @HeaderTaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
END
        declare @ReverseCharged bit,@ReverseItemid int,@CharAcc1 nvarchar(250),@chk bit
		set @ReverseItemid = (select COLUMN03 from PUTABLE002 where COLUMN02=@COLUMN02)
		set @ReverseCharged = (select isnull(COLUMN73,0) from PUTABLE001 where COLUMN01=@COLUMN19)
        if((@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		begin
		if(@TaxAG=23582) set @CharAcc1=(1136)
		else if(@TaxAG=23584) set @CharAcc1=(1137)
		else if(@TaxAG=23583) set @CharAcc1=(1138)
		else if(@TaxAG=23585) set @CharAcc1=(1139)
		else if(@TaxAG=23586) set @CharAcc1=(1140)
		if(@CharAcc1 is not null )
		begin
		   set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)	
		   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
           set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		   --set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
  insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	        values(ISNULL(@tmpnewID,1000),'Debit Memo',@date,@memo,@CustID,0, @ltaxamt, @Num1,@CharAcc1,@project,@COLUMNA02, @COLUMNA03)
			--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
				--values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@CharAcc1,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num1,@Project)
		end
		end
		else if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		BEGIN
	    set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1141 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	    set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)
        set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))	
	    --set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	    --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = '22402', @COLUMN11 = '1141',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
 insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	    values(ISNULL(@tmpnewID,1000),'Debit Memo',@date,@memo,@CustID,0, @ltaxamt, @Num1,1141,@project,@COLUMNA02, @COLUMNA03)
	    set @tmpnewID=((select isnull(max(column02),1000) from FITABLE026)+1)	
	    set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1135 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = '22383', @COLUMN11 = '1135',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
	    values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,1135,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num1,@Project)
        END
		--EMPHCS1564	CST not inserting while Debit memo BY RAJ.Jr
		declare @AID nvarchar(250)
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22400)
		begin
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like(@mode+'CST'+cast(@TAXRATE as nvarchar(20))+'%') and columna03=@columna03)
	select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = @TaxColumn17 and columna03 = @COLUMNA03
			set @ltaxamt = CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2)),@Num1,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0)
		end
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0 AND @HeaderTaxAG=22400)
		begin
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like(@mode+'CST'+cast(@HeaderTAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = @TaxColumn17 and columna03 = @COLUMNA03
			set @ltaxamt = CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2))
			
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2)),@Num1,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0)
			end

	if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		begin
		set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast((0.5) as decimal(18,2))*0.01))
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = 1122 and columna03 = @COLUMNA03
			
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = 1122,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,@ltaxamt,@Num1,1122,@COLUMNA02 , @COLUMNA03,0)
		end
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast((0.5) as decimal(18,2))*0.01))	
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = 1122 and columna03 = @COLUMNA03
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = 1122,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,@ltaxamt,@Num1,1122,@COLUMNA02 , @COLUMNA03,0)
		end
END		
		
		set @chk=(select column48 from matable007 where column02=@COLUMN03)
		if(@chk=0)
		begin
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = 1078 and columna03 = @COLUMNA03
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = 1078,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN24,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,isnull(@COLUMN24,0),@Num1,1078,@COLUMNA02 , @COLUMNA03,0)
		end
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
if(@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0')
			 begin	
			 declare @Price	decimal(18,2), @Qty	decimal(18,2),@lotno nvarchar(250),@Location nvarchar(250)
			 set @chk=(select column48 from matable007 where column02=@COLUMN03)
			 if(@chk=1)
			 begin
  set @lotno=(select COLUMN17 from PUTABLE002 where COLUMN02=@COLUMN02)
  set @Location=(select COLUMN47 from PUTABLE001 where column01=@Num)
  set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
  set @Location = (case when (cast(isnull(@COLUMN37,'') as nvarchar(250))!='' and @COLUMN37!=0) then @COLUMN37 else @Location end)
  set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   if exists(select COLUMN03 from FITABLE010  where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
  begin   
  set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@COLUMN04);
  set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN26,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@Project,0)  and COLUMN24=@COLUMN04);
  set @Qty=cast(@COLUMN07 as decimal(18,2));
  set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@COLUMN04)as decimal(18,2))-(cast(@COLUMN07 as decimal(18,2))*cast(@Price as decimal(18,2))))	 
  WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@COLUMN04)as decimal(18,2)));
  if(cast(@Qty_On_Hand as decimal(18,2))=0)
  begin
  UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@COLUMN04
  end
  else
  begin
  UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@COLUMN04)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
  end
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  end
else
begin
declare @newIID int,@tmpnewIID1 int,@ASSET decimal(18,2),@AvgPrice decimal(18,2) 
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
--SET @AvgPrice=(ISNULL(@COLUMN09,0))
  set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN26,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=@Project  and COLUMN24=@COLUMN04);
SET @ASSET=(-CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03,column23,COLUMN24
)
values
( 
  @newIID,@COLUMN03,-CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2)),-CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2)), @ASSET,@AvgPrice,@COLUMN26,  @lotno, @location,@COLUMN20,@COLUMNA02,@COLUMNA03,@Project,@COLUMN04 )
END

declare @VendorID nvarchar(250),@IAmt decimal(18,2)
SET @VendorID=(select COLUMN05 from PUTABLE001 where column01=@Num)
SET @Memo=(select COLUMN09 from PUTABLE001 where column01=@Num)
SET @date=(select COLUMN06 from PUTABLE001 where column01=@Num)
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN26,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04);
SET @IAmt=(cast(@COLUMN07 as decimal(18,2))*cast(@Price as decimal(18,2)))
--EMPHCS1785 rajasekhar reddy patakota 21/07/2016 Rate Calculation with inclusive of tax in Purchase
--SET @IAmt=(isnull(@COLUMN24,0))
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE017) as int)+1
  insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   @newID,'1000',@date ,@Num,'Debit Memo', @VendorID,'Inventory Received',@Memo,@IAmt,@IAmt,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lIID, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
declare @ACID int,@ACTYPE int
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHASE  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',Debit Memo,'+  cast(@date as nvarchar(250))+','+  cast(@lIID as nvarchar(250))+','+  cast(@VendorID as nvarchar(250))+','+  cast(@COLUMN24 as nvarchar(250))+','+  cast(@Num1 as nvarchar(250))+',1133'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@Num1,'')+','+  cast(@COLUMN24 as nvarchar(250))+','+  cast(@COLUMN24 as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN24,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'Debit Memo',@date,@lIID,@VendorID,NULL,@COLUMN24,@Num1,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN24,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12)
			values(@AID,@date,'Debit Memo',@lIID,@VendorID,@MEMO,@ACID,CAST(isnull(@COLUMN24,0) AS decimal(18,2)),CAST(isnull(@COLUMN24,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project)
		    END
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHAGE  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
end
end

set @ReturnValue = 1
end
else
begin
return 0
end
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from PUTABLE001
END 

ELSE IF @Direction = 'Update'
BEGIN

   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= '0' ) THEN (select COLUMN24 from PUTABLE001 
   where COLUMN01=@COLUMN19) else @COLUMNA02  END ) 
   set @FRMID= (SELECT COLUMN03 FROM PUTABLE001 WHERE COLUMN01=(@COLUMN19))
   set @ReceiptType =(select COLUMN29 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @date =(select COLUMN06 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN20= (SELECT COLUMN24 FROM PUTABLE001 WHERE COLUMN01=@COLUMN19)
   set @COLUMN21= (SELECT COLUMN25 FROM PUTABLE001 WHERE COLUMN01=@COLUMN19)
   set @COLUMN22= (SELECT COLUMN26 FROM PUTABLE001 WHERE COLUMN01=@COLUMN19)
   set @COLUMN23= (SELECT COLUMN27 FROM PUTABLE001 WHERE COLUMN01=@COLUMN19)
   if not exists( SELECT 1 FROM PUTABLE002 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
	begin
	select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE002_SequenceNo
	insert into PUTABLE002 
	(
	   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12, 
	   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
	   COLUMN24,  COLUMN25, COLUMN26,   COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
	   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,  
	   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
	   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
	   COLUMND09, COLUMND10 
	)
	values
	( 
	   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11, 
	   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
	   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN26,  @COLUMN07,  @COLUMN29,  @COLUMN30,  @COLUMN31,
	   @COLUMN32,  @COLUMN33,  @COLUMN34,
	   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08,  
	   @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,  
	   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03,  
	   @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10 
	)  
	end
	ELSE
	BEGIN
   UPDATE PUTABLE002 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16, 
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21, 
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,    
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31, 
   COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    COLUMN34=@COLUMN34,    COLUMN37=@COLUMN37,
   COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05, 
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08, 
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06, 
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
END
set @Refreceipt=(select COLUMN33 from PUTABLE001 where COLUMN01=@COLUMN19);
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
set @BillNo=(select COLUMN48 from PUTABLE001 where COLUMN01=@COLUMN19);
set @BillId=(select COLUMN01 from PUTABLE005 where COLUMN02=@BillNo);
set @RefID=(select COLUMN06 from SATABLE007 where COLUMN02=@Refreceipt);
set @PID=(select COLUMN01 from SATABLE005 where COLUMN02=@RefID);
set @PONum=(select COLUMN04 from PUTABLE001 where COLUMN04=(select COLUMN11 from SATABLE005 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @POID=(select COLUMN02 from PUTABLE001 where COLUMN04=(select COLUMN11 from SATABLE005 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin	
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(ISNULL(@COLUMN34,0)>0)
begin
set @RefundQty=(select sum(isnull(COLUMN13,0)) from SATABLE006 where COLUMN02=@COLUMN34 and isnull(COLUMNA13,0)=0)
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN02=@COLUMN34 and isnull(COLUMNA13,0)=0
end
else
begin
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @RefundQty=isnull((select sum(COLUMN13) from SATABLE006 where COLUMN19 in (@PID) and COLUMN03=@COLUMN03 and isnull(COLUMN27,0)=isnull(@COLUMN26,0) and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and isnull(COLUMNA13,0)=0),0);
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN03=@COLUMN03 and isnull(COLUMN27,0)=isnull(@COLUMN26,0) and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and COLUMN19 in (@PID)
end
SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM SATABLE006 WHERE COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN33=CAST(@Refreceipt AS NVARCHAR(250))) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RIQty=(SELECT sum(isnull(COLUMN12,0)) FROM SATABLE006 WHERE COLUMN19 in(@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @PIQty=(SELECT sum(isnull(COLUMN09,0)) FROM SATABLE008 WHERE COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN02=@Refreceipt) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
end
else
begin
set @TQty=(SELECT sum(isnull(COLUMN09,0)) FROM PUTABLE006 WHERE COLUMN13 in(@BillId) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN48=@BillNo) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @PIQty=(@TQty)
end
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN19=@COLUMN19 and COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=isnull(@COLUMN26,0) and isnull(COLUMN17,0)=isnull(@COLUMN17,0)

if(@PIQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
end
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@COLUMN19)

if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin
UPDATE SATABLE007 set COLUMN14=@Result where COLUMN02=(@Refreceipt)
IF(@TQty=@RIQty)
begin
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=98)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=99)
end
end
ELSE 
BEGIN
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=96)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=97)
end
end
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@PID)
END

if((@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0') and (@BillNo='' or isnull(@BillNo,0)=0 or isnull(@BillNo,0)='0'))
begin
UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=61) where COLUMN01=(@COLUMN19)
end
set @CustID = (select COLUMN05 from PUTABLE001 where COLUMN01=@COLUMN19)
set @memo = (select COLUMN09 from PUTABLE001 where COLUMN01=@COLUMN19)
	set @lIID=(select COLUMN01 from PUTABLE002 WHERE COLUMN02=@COLUMN02)
set @Num = (select COLUMN01 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Num1 = (select COLUMN04 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project = (select COLUMN36 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
--set @Tax= (select COLUMN25 from PUTABLE002 where COLUMN02=@COLUMN02)
--   set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
--  set @TAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
--set @HeaderTax= (select COLUMN31 from PUTABLE001 where COLUMN01=@COLUMN19)
--   set @HeaderTaxAG= (select column16 from MATABLE013 where COLUMN02=@HeaderTax)
--  set @HeaderTAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@HeaderTax)
set @partytyp = (select COLUMN50 from PUTABLE001 where COLUMN01=@COLUMN19)--,@TaxString nvarchar(250)=NULL,@TAXRATEM decimal(18,2)

set @Tax= (select COLUMN25 from PUTABLE002 where COLUMN02=@COLUMN02)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
Set @cnt = 0 set @vochercnt = 1 set @AMOUNTM =@COLUMN24;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN25,0) from PUTABLE002 where COLUMN02=@COLUMN02
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end

	set @headerTax= (select COLUMN31 from PUTABLE001 where COLUMN01=@COLUMN19)
	select @TaxAG=column16, @TAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@Tax
	select @headerTaxAG=column16,@headerTAXRATE=COLUMN17 from MATABLE013 where COLUMN02=@headerTax
		if(@partytyp=22335)
begin
set @mode='OUTPUT '
set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
set @HeaderTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@HeaderTax)
end
else
begin
set @mode='INPUT '
end
if(@PONum=(select COLUMN11 from SATABLE005 where COLUMN02=@RefID))
BEGIN
if exists(select 1 from PUTABLE005 where COLUMN06=@POID and COLUMNA03=@COLUMNA03)
begin
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0  AND @TaxAG!=22400 AND @TaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0  AND @HeaderTaxAG!=22400 AND @TaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @HeaderTaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
end
END
else
BEGIN
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0  AND @TaxAG!=22400 AND @TaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0  AND @HeaderTaxAG!=22400 AND @TaxAG!=22400 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)
		BEGIN
    set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @HeaderTaxColumn17 and columna03 = @COLUMNA03
if(@partytyp=22335)
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
else
begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num1,@Project)
end
		end
END        
		set @ReverseItemid = (select COLUMN03 from PUTABLE002 where COLUMN02=@COLUMN02)
		set @ReverseCharged = (select isnull(COLUMN73,0) from PUTABLE001 where COLUMN01=@COLUMN19)
        if((@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		begin
		if(@TaxAG=23582) set @CharAcc1=(1136)
		else if(@TaxAG=23584) set @CharAcc1=(1137)
		else if(@TaxAG=23583) set @CharAcc1=(1138)
		else if(@TaxAG=23585) set @CharAcc1=(1139)
		else if(@TaxAG=23586) set @CharAcc1=(1140)
		if(@CharAcc1 is not null )
		begin
		   set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)	
		   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
           set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		   --set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
 insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	        values(ISNULL(@tmpnewID,1000),'Debit Memo',@date,@memo,@CustID,0, @ltaxamt, @Num1,@CharAcc1,@project,@COLUMNA02, @COLUMNA03)
				--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
				--values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,@CharAcc1,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num1,@Project)
		end
		end
		else if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		BEGIN
	    set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1141 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	    set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)
        set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))	
	    --set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	    --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = '22402', @COLUMN11 = '1141',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	    values(ISNULL(@tmpnewID,1000),'Debit Memo',@date,@memo,@CustID,0, @ltaxamt, @Num1,1141,@project,@COLUMNA02, @COLUMNA03)
	    set @tmpnewID=((select isnull(max(column02),1000) from FITABLE026)+1)	
	    set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1135 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = '22383', @COLUMN11 = '1135',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
	    values(@tmpnewID,@date,'Debit Memo',@Num,@CustID,@memo,1135,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN24,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num1,@Project)
        END
		--EMPHCS1564	CST not inserting while Debit memo BY RAJ.Jr
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22400)
		begin
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like(@mode+'CST'+cast(@TAXRATE as nvarchar(20))+'%') and columna03=@columna03)
	select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = @TaxColumn17 and columna03 = @COLUMNA03
			set @ltaxamt = CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2)),@Num1,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0)
		end
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0 AND @HeaderTaxAG=22400)
		begin
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like(@mode+'CST'+cast(@HeaderTAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = @TaxColumn17 and columna03 = @COLUMNA03
			set @ltaxamt = CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2))
			
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,CAST(isnull(@COLUMN11,0) as decimal(18,2))-CAST(isnull(@COLUMN24,0) as decimal(18,2)),@Num1,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0)
			end
			
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		begin
		set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast((0.5) as decimal(18,2))*0.01))	
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = 1122 and columna03 = @COLUMNA03
			
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = '1122',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,@ltaxamt,@Num1,1122,@COLUMNA02 , @COLUMNA03,0)
		end
		if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		set @ltaxamt=(cast(@COLUMN24 as decimal(18,2))*(cast((0.5) as decimal(18,2))*0.01))	
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = 1122 and columna03 = @COLUMNA03
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = 1122,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,@ltaxamt,@Num1,1122,@COLUMNA02 , @COLUMNA03,0)
		end
END		
		set @chk=(select column48 from matable007 where column02=@COLUMN03)
		if(@chk=0)
		begin
			select top 1 @CharAcc = column04,@TTYPE=COLUMN07 from fitable001 where column02 = 1078 and columna03 = @COLUMNA03
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @TTYPE, @COLUMN11 = 1078,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN24,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'Debit Memo',@date,@Num,@CustID,NULL,isnull(@COLUMN24,0),@Num1,1078,@COLUMNA02 , @COLUMNA03,0)
		end
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
if(@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0')
			 begin	
			 set @chk=(select column48 from matable007 where column02=@COLUMN03)
			 if(@chk=1)
			 begin
  set @lotno=(select COLUMN17 from PUTABLE002 where COLUMN02=@COLUMN02)
  set @Location=(select COLUMN47 from PUTABLE001 where column01=@Num)
  set @Location = (case when (cast(isnull(@COLUMN37,'') as nvarchar(250))!='' and @COLUMN37!=0) then @COLUMN37 else @Location end)
  set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
  set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   if exists(select COLUMN03 from FITABLE010  where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
  begin   
  set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04);
  set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN26,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04);
  set @Qty=cast(@COLUMN07 as decimal(18,2));
  set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))-(cast(@COLUMN07 as decimal(18,2))*cast(@Price as decimal(18,2))))	 
  WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2)));
  if(cast(@Qty_On_Hand as decimal(18,2))=0)
  begin
  UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
  end
  else
  begin
  UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04) as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
  end
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
 end
else
begin
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
--SET @AvgPrice=(ISNULL(@COLUMN09,0))
  set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN26,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04);
SET @ASSET=(-CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
( 
  @newIID,@COLUMN03,-CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2)),-CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2)), @ASSET,@AvgPrice,@COLUMN26,  @lotno, @location,@COLUMN20,@COLUMNA02,@COLUMNA03,@Project,@COLUMN04 )
END

SET @VendorID=(select COLUMN05 from PUTABLE001 where column01=@Num)
SET @Memo=(select COLUMN09 from PUTABLE001 where column01=@Num)
SET @date=(select COLUMN06 from PUTABLE001 where column01=@Num)
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN26,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@Project,0)  and COLUMN24=@COLUMN04);
SET @IAmt=(cast(@COLUMN07 as decimal(18,2))*cast(@Price as decimal(18,2)))
--EMPHCS1785 rajasekhar reddy patakota 21/07/2016 Rate Calculation with inclusive of tax in Purchase
--SET @IAmt=(isnull(@COLUMN24,0))
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE017) as int)+1
	set @lIID=(select COLUMN01 from PUTABLE002 WHERE COLUMN02=@COLUMN02)
  insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   @newID,'1000',@date ,@Num,'Debit Memo', @VendorID,'Inventory Received',@Memo,@IAmt,@IAmt,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lIID, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHASE  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',Debit Memo,'+  cast(@date as nvarchar(250))+','+  cast(@lIID as nvarchar(250))+','+  cast(@VendorID as nvarchar(250))+','+  cast(@COLUMN24 as nvarchar(250))+','+  cast(@Num1 as nvarchar(250))+',1133'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@Num1,'')+','+  cast(@COLUMN24 as nvarchar(250))+','+  cast(@COLUMN24 as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN24,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'Debit Memo',@date,@lIID,@VendorID,NULL,@COLUMN24,@Num1,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @Num1, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lIID,
		@COLUMN07 = @date,   @COLUMN08 = @partytyp,  @COLUMN09 = @CustID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN24,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12)
			values(@AID,@date,'Debit Memo',@lIID,@VendorID,@MEMO,@ACID,CAST(isnull(@COLUMN24,0) AS decimal(18,2)),CAST(isnull(@COLUMN24,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project)
		    END
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHAGE  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
end
end

set @ReturnValue = 1
END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE002 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END
end try
begin catch
if not exists(select column01 from putable002 where column19=@column19)
		begin
			delete from putable001 where column01=@column19
		end
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PURR_TP_PUTABLE002.txt',0
return 0
end catch
end















GO
