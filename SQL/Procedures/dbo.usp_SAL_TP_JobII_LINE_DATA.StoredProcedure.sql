USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_JobII_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_JobII_LINE_DATA]
(
	@SalesOrderID int
)

AS
BEGIN
	--select  a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05,a.COLUMN15 as COLUMN06 , a.COLUMN07 as COLUMN07,
	-- isnull(a.COLUMN12,0) as COLUMN08 ,(isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) as COLUMN09,
	
	-- (case when (b.COLUMN65=1 or b.COLUMN65='True') then
	-- isnull((select sum(COLUMN08) from FITABLE010 where COLUMN03 in (a.COLUMN03)  AND COLUMN13=A.COLUMN20 AND isnull(COLUMN19,0)=isnull(a.COLUMN27,0)),0) else 
	-- isnull((select COLUMN04 from FITABLE010 where COLUMN03 in(a.COLUMN03)  AND COLUMN13=A.COLUMN20  AND isnull(COLUMN19,0)=isnull(a.COLUMN27,0) and isnull(COLUMN22,0)=isnull(a.COLUMN17,0) and isnull(COLUMN21,0)=isnull(s.COLUMN48,0)),0)end) as COLUMN10,
	--  (isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) as COLUMN11,  a.COLUMN09 as COLUMN12,
	
	--  a.COLUMN27 COLUMN19,a.COLUMN17 COLUMN24,b.COLUMN48 COLUMN48,b.COLUMN65,a.COLUMN09 COLUMN25,a.COLUMN25 as COLUMN13
	--FROM dbo.SATABLE006 a 
	--inner join SATABLE005 s on s.COLUMN02=@SalesOrderID
	--inner join MATABLE007 b on a.COLUMN03=b.COLUMN02
	--WHERE a.COLUMN19 in (select COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02=@SalesOrderID) and ((isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)))>0 and a.COLUMNA13=0
	select  a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05,a.COLUMN04 as COLUMN06 , a.COLUMN07 as COLUMN07,
	 isnull(a.COLUMN12,0) as COLUMN08 ,(isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) as COLUMN09,
	(case when (b.COLUMN65=1 or b.COLUMN65='True') then sum(ISNULL(f10.COLUMN08,0)) else SUM(ISNULL(f10.COLUMN04,0)) end) as COLUMN10,
	(isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) as COLUMN11,  a.COLUMN09 as COLUMN12,
	  a.COLUMN27 COLUMN19,a.COLUMN17 COLUMN24,b.COLUMN48 COLUMN48,b.COLUMN65,a.COLUMN09 COLUMN25,a.COLUMN25 as COLUMN13
	FROM dbo.SATABLE006 a 
	inner join SATABLE005 s on s.COLUMN02=@SalesOrderID
	inner join MATABLE007 b on a.COLUMN03=b.COLUMN02
	left join FITABLE010 f10 on f10.COLUMN03 in (a.COLUMN03) and f10.COLUMNA03 in (a.COLUMNA03)  AND f10.COLUMN13=A.COLUMN20 AND isnull(f10.COLUMN19,0)=isnull(a.COLUMN27,0) AND isnull(f10.COLUMN22,0)=isnull(a.COLUMN17,0) AND isnull(f10.COLUMN21,0)=0 and isnull(f10.COLUMNA13,0)=0
	WHERE a.COLUMN19 in (select COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02=@SalesOrderID) and ((isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)))>0 and a.COLUMNA13=0
	group by a.COLUMN03,a.COLUMN05,a.COLUMN04,a.COLUMN07,a.COLUMN12,b.COLUMN65,a.COLUMN09,a.COLUMN27,a.COLUMN17,b.COLUMN48,a.COLUMN25
END




GO
