USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_DirectDebit_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_POR_TP_DirectDebit_HEADER_DATA]
(
	@BillID int
)

AS
BEGIN
	SELECT a.COLUMN05 Vendor,a.COLUMN10 Terms,a.COLUMN21 Mode,a.COLUMN15 ou,a.COLUMN18 dept, a.COLUMN04 Ref, 
	a.COLUMN23 taxtype,isnull(m.column17,0) htaxamt,a.COLUMN12 notes, a.COLUMN26 project, a.COLUMN29 location,a.COLUMN22 subtotal,a.COLUMN24 taxamt,a.COLUMN14 totamt,
	ca.COLUMN06 addresse,ca.COLUMN07 add1,ca.COLUMN08 add2,ca.COLUMN16 country,ca.COLUMN11 'state',ca.COLUMN10 city,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
    ca.COLUMN12 zip,ca.COLUMN17 bill,ca.COLUMN18 shipp,a.COLUMN43 Amtin,a.COLUMN56 CGRY,a.COLUMN55 GSTIN,a.COLUMN57 STCODE,a.COLUMN59 RoundOffAmt
	FROM PUTABLE005 a inner join PUTABLE006 b on a.COLUMN01=b.COLUMN13  and isnull(b.COLUMNA13,0)=0
	left join MATABLE013 m on  m.column02=a.COLUMN23 and m.COLUMNA03=a.COLUMNA03 and isnull(m.COLUMNA13,0)=0
	left join SATABLE001 c on  c.COLUMN02=a.COLUMN05 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
	left join SATABLE003 ca on c.COLUMN01=ca.COLUMN20 and ca.COLUMN19='Vendor' and ca.COLUMNA03=a.COLUMNA03 and isnull(ca.COLUMNA13,0)=0
	 WHERE  a.COLUMN02= @BillID  and isnull(a.COLUMNA13,0)=0
END



GO
