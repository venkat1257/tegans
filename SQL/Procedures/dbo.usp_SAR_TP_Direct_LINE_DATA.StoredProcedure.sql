USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_Direct_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_Direct_LINE_DATA]
( @InvoiceID int)
AS
BEGIN
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
declare @Query1 nvarchar(max)					
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions					
select p.COLUMN03,p.COLUMN05,p.COLUMN07,p.COLUMN27,p.COLUMN26,p.COLUMN09,p.COLUMN10,cast((isnull(p.column07,0)*isnull(p.COLUMN09,0)) as decimal(18,2))COLUMN25,cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))COLUMN24,(cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))+((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01)))COLUMN11,p.COLUMN17,cast((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01) as decimal(18,2) ) aCOLUMN25,p.aCOLUMN26,p.COLUMN37,p.Item,p.COLUMN38,p.UPC into #tempdirectcredit from(					
SELECT    
	iif(im.COLUMN08!='',im.column06,b.COLUMN05) COLUMN03, b.COLUMN06 COLUMN05,iif(im.COLUMN08!='',(sum(isnull(im.COLUMN07,0))-sum(isnull(sl.column07,0))),(sum(isnull(b.column10,0))-sum(isnull(sl.column07,0)))) COLUMN07,iif(im.COLUMN08!='',im.COLUMN08,b.COLUMN22) COLUMN27,b.COLUMN21 COLUMN26,b.COLUMN13 COLUMN09,cast(isnull(b.COLUMN19,0) as decimal(18,2)) COLUMN10,
	cast(((sum(isnull(b.column10,0))-sum(isnull(sl.column07,0)))*isnull(b.COLUMN13,0)) as decimal(18,2)) COLUMN25,
	cast((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*((iif(b.COLUMN30=22556,(cast(b.COLUMN14 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))), cast(b.COLUMN14 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))))) +(cast(b.COLUMN14 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))) as decimal(18,2)) COLUMN11,
cast((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*((iif(b.COLUMN30=22556,(cast(b.COLUMN14 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))), cast(b.COLUMN14 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))))) as decimal(18,2)) aCOLUMN25,
	isnull(m.column07,0) aCOLUMN26,b.COLUMN31 COLUMN17,isnull(b.COLUMN19,0) disc,m.COLUMN07 taxper,iif((i.COLUMN66=1 or i.COLUMN66='True'),'Y','N') lottrack,i.COLUMN04 Item
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
    ,b.COLUMN37,b.COLUMN38,b.COLUMNB01 'UPC'
	FROM SATABLE009 a inner join SATABLE010 b on a.COLUMN01=b.COLUMN15 and a.COLUMNa03=b.COLUMNa03 and isnull(b.COLUMNA13,0)=0
	left join SATABLE005 sh on  sh.column49=a.COLUMN02 and sh.COLUMNa03=a.COLUMNa03 and isnull(sh.COLUMNa13,0)=0  
	left join SATABLE006 sl on  sl.column19=sh.COLUMN01 and sl.column09=b.column13 and sl.column03=b.column05 and isnull(sl.column27,0)=isnull(b.column22,0) and isnull(sl.column17,0)=isnull(b.column31,0) and isnull(sl.COLUMNA13,0)=0  and sl.columnA03=sh.columnA03  and sl.columnA02=sh.columnA02
	left join MATABLE007 i on  i.column02=b.COLUMN05 and i.columnA03=b.columnA03 and isnull(i.columnA13,0)=0
	left join MATABLE013 m on  m.column02=b.COLUMN21
	left join FITABLE038 im on  im.column06=b.COLUMN05 and im.column05=b.column01 and im.columnA03=b.columnA03 and im.columnA02=b.columnA02 and isnull(im.COLUMNA13,0)=0
	left join MATABLE007 m7 on m7.column02=iif(im.COLUMN08!='',im.column06,b.COLUMN05)
	WHERE  a.COLUMN02= @InvoiceID and isnull(a.COLUMNA13,0)=0
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 group by b.COLUMN05,b.COLUMN06,im.COLUMN08,im.column06,b.COLUMN10,b.COLUMN22,m.column07,b.COLUMN21,b.COLUMN13,b.column14,b.column19,b.column23,b.COLUMN31,b.COLUMN30,i.COLUMN66,b.COLUMN37,i.COLUMN04,b.COLUMN38,b.COLUMNB01
	having (sum(isnull(b.column10,0))-sum(isnull(sl.column07,0)))>0) p
set @Query1='select * from #tempdirectcredit'
execute(@Query1)
END



GO
