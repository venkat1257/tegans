USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_II_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_II_LINE_DATA]
(
	@SalesOrderID int
)

AS
BEGIN
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	declare @Query1 nvarchar(max)	
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions				
select p.COLUMN04,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN08,iif(p.COLUMN09<0,0,p.COLUMN09)COLUMN09,p.COLUMN10,p.COLUMN11,p.COLUMN12,cast((isnull(CAST(iif(p.COLUMN09<0,0,p.COLUMN09) as decimal(18,2)),0))*(isnull(CAST(p.COLUMN12 as decimal(18,2)),0)) as decimal(18,2)) as COLUMN13,p.COLUMN19,p.COLUMN24,p.COLUMN48,p.COLUMN65,p.COLUMN25,p.lottrack,p.LineID,p.COLUMN28,p.hsncode into #tempii from(					
select  a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05, a.COLUMN07 as COLUMN07,
	  a.Column04 as COLUMN08 ,
	 (case when exists(select top 1column04 from contable031 where columna03=a.columna03 and column03='Allow BackOrder' and isnull(column04,0)=1 and isnull(columna13,0)=0)then
	 (isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) 
	 when (b.COLUMN65=0 or b.COLUMN65='False' ) then (isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) 
	 when(isnull((f.COLUMN04),0)>=(isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)))
	 then (isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0))
	  when(isnull((f.COLUMN04),0)<(isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)))
	 then f.COLUMN04 end) as COLUMN09,
	 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	 cast((case when (b.COLUMN65=1 or b.COLUMN65='True') then
	 isnull(((f.COLUMN08)),0) else 
	 isnull((f.COLUMN04),0)end) as decimal(18,2)) as COLUMN10,
	 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	  (isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)) as COLUMN11, iif((isnull(a.COLUMN32,0)>0 and s.COLUMN03=1353),a.COLUMN32, a.COLUMN09) as COLUMN12, iif(isnull(a.COLUMN32,0)>0,a.COLUMN32,a.COLUMN09)COLUMN25,	  
	  --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	  a.COLUMN27 COLUMN19,a.COLUMN17 COLUMN24,b.COLUMN48 COLUMN48,b.COLUMN65,iif((b.COLUMN66=1 or b.COLUMN66='True'),'Y','N') lottrack,a.COLUMN02 LineID,a.COLUMN38 COLUMN28,hs.COLUMN04 hsncode,a.column04 COLUMN06
	FROM dbo.SATABLE006 a 
	inner join SATABLE005 s on s.COLUMN02=@SalesOrderID
	inner join MATABLE007 b on a.COLUMN03=b.COLUMN02
	left join FITABLE010 f on f.COLUMN03=a.COLUMN03  and f.COLUMN24=a.COLUMN04 AND f.COLUMN13=a.COLUMN20   AND isnull(f.COLUMN19,0)=isnull(a.COLUMN27,0)  AND isnull(f.COLUMN21,0)=(case when (isnull(a.COLUMN38,0)!=0 and cast(a.COLUMN38 as nvarchar(250))!='') then a.COLUMN38 else iif(s.COLUMN48='',0,isnull(s.COLUMN48,0)) end) AND isnull(f.COLUMN22,0)=isnull(a.COLUMN17,0) 
	AND isnull(f.COLUMN23,0)=(case when (isnull(s.COLUMN35,0)!=0 and cast(s.COLUMN35 as nvarchar(250))!='') then s.COLUMN35 else iif(s.COLUMN35='',0,isnull(s.COLUMN35,0)) end)
	left join MATABLE032 hs on hs.COLUMN02=b.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=b.COLUMN75 and b.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	WHERE a.COLUMN19 in (select COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02=@SalesOrderID) and ((isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0)))>0 and a.COLUMNA13=0) p
set @Query1='select * from #tempii'
execute(@Query1)
END









GO
