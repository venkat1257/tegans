USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAS_BL_ItemMaster]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAS_BL_ItemMaster]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
    @COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null, 
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,  
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
	@COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,  
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,  
	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN43   nvarchar(250)=null,
	@COLUMN44   nvarchar(250)=null,  @COLUMN45   nvarchar(250)=null,  @COLUMN46   nvarchar(250)=null,
	@COLUMN47   nvarchar(250)=null,
	@COLUMN48   nvarchar(250)=null,  @COLUMN49   nvarchar(250)=null,  @COLUMN50   nvarchar(max)=null,  
	@COLUMN51   nvarchar(250)=null,  @COLUMN52   nvarchar(250)=null,  @COLUMN53   nvarchar(250)=null, 
	@COLUMN54   nvarchar(250)=null,  @COLUMN55   nvarchar(250)=null,  @COLUMN56   nvarchar(250)=null,
	@COLUMN57   nvarchar(max)=null,  @COLUMN58   nvarchar(250)=null,  @COLUMN59   nvarchar(250)=null,
	@COLUMN60   nvarchar(250)=null,  @COLUMN61   nvarchar(250)=null,  @COLUMN62   nvarchar(250)=null, 
	@COLUMN63 nvarchar(250)=null,    @COLUMN64 nvarchar(250)=null,    @COLUMN65 nvarchar(250)=null,
	--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
	@COLUMN66 nvarchar(250)=null,    @COLUMN67 nvarchar(250)=null,    @COLUMN68 nvarchar(max)=null,
	@COLUMN69 nvarchar(max)=null,    @COLUMN70 nvarchar(250)=null,    @COLUMN71 nvarchar(250)=null,
	@COLUMN72 nvarchar(250)=null,    @COLUMN73 nvarchar(250)=null,    @COLUMN74 nvarchar(250)=null, 
	  @COLUMN75 nvarchar(250)=null,    @COLUMN76 nvarchar(250)=null,  @COLUMN77 nvarchar(250)=null,
	  @COLUMN78 nvarchar(250)=null,    @COLUMN79 nvarchar(250)=null,  @COLUMN80 nvarchar(250)=null,
	  @COLUMN81 nvarchar(250)=null,    @COLUMN82 nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null, 
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null, 
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null, 
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null, 
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
        --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT

)

AS

BEGIN
IF  @TabelName='MATABLE007'
BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN37!= '' and @COLUMN37 is not null and @COLUMN37!=0 ) THEN @COLUMN37 else @COLUMNA02  END )
set @COLUMN37=( CASE WHEN (@COLUMN37!= '' and @COLUMN37 is not null and @COLUMN37!=0 ) THEN @COLUMN37 else @COLUMNA02  END )

EXEC usp_MAS_TP_MATABLE007 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31, 
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN47,  @COLUMN48,  @COLUMN49,  @COLUMN50,  @COLUMN51, 
   @COLUMN52,  @COLUMN53,  @COLUMN54,  @COLUMN55,  @COLUMN56,  @COLUMN57,  @COLUMN58,  @COLUMN59,  @COLUMN60,  @COLUMN61,  
   --EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
   @COLUMN62,  @COLUMN63,  @COLUMN64,  @COLUMN65,  @COLUMN66,  @COLUMN67,  @COLUMN68,  @COLUMN69,  @COLUMN70,  @COLUMN71,
    @COLUMN72, @COLUMN73,  @COLUMN74, @COLUMN75,  @COLUMN76,    @COLUMN77, @COLUMN78, @COLUMN79,  @COLUMN80,    @COLUMN81, 
	@COLUMN82 ,@COLUMNA01, 
   @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
   @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
   @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
   --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
   @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue OUTPUT
select @ReturnValue
END
 
ELSE IF  @TabelName='MATABLE012'
BEGIN

EXEC usp_MAS_TP_MATABLE012 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,  
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   @COLUMNA01, 
   @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
   @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
   @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
   @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue

END

ELSE IF @TabelName = 'FITABLE013'
--Handing Type of prices deletion and updates
--BEGIN
--if(@Direction='Update')
--begin
--update FITABLE013 set COLUMNA13=1 where COLUMn02=@COLUMN02 and (COLUMNA02 in (select COLUMN37 from MATABLE007 

--where COLUMN02=@COLUMN02) or COLUMNA02 is null) and COLUMNA03=@COLUMNA03 
--end
EXEC usp_MAS_TP_FITABLE013  

    @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07,
    @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
	@COLUMNB04,	@COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12,
	@COLUMND01, @COLUMND02,	@COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09,
    @COLUMND10, @Direction, @TabelName, @ReturnValue

END 

--end





GO
