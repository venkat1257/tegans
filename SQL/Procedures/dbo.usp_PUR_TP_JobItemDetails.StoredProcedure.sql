USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_JobItemDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_JobItemDetails]
(
	@ItemID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL,@uom nvarchar(250)=NULL,@JO nvarchar(250)=NULL,
	
	@StockItem nvarchar(250)=NULL,@lotno nvarchar(250)=NULL
)
AS
BEGIN
if(@ItemID !='')
BEGIN
IF(@OPUnit!='' OR @OPUnit != NULL)
BEGIN
	begin
	select isnull(f.COLUMN07,0) as FG from FITABLE038 f
	left outer join SATABLE005 s on s.column02=@JO and s.COLUMNA03=@AcOwner and s.COLUMNA02=@OPUnit
	left outer join MATABLE007 MA07 on MA07.COLUMN02=@ItemID and MA07.COLUMNA03=@AcOwner and (MA07.COLUMNA02=@OPUnit or MA07.COLUMNA02 is null)
	where f.COLUMN04=s.COLUMN04  and f.COLUMN08=@uom and f.COLUMN06=MA07.COLUMN67 and f.COLUMNA03=@AcOwner and f.COLUMNA02=@OPUnit        
	END
	END
	--ELSE
	--BEGIN
	--begin
	----select isnull(f.COLUMN07,0) as FG from FITABLE038 f
	----left outer join SATABLE005 s on s.column02=@JO and s.COLUMNA03=@AcOwner and s.COLUMNA02=@OPUnit
	----left outer join MATABLE007 MA07 on MA07.COLUMN02=@ItemID and MA07.COLUMNA03=@AcOwner and MA07.COLUMNA02=@OPUnit
	----where f.COLUMN04=s.COLUMN04  and f.COLUMN08=@uom and f.COLUMN06=MA07.COLUMN67 and f.COLUMNA03=@AcOwner and f.COLUMNA02=@OPUnit    
	--END
	END


END

GO
