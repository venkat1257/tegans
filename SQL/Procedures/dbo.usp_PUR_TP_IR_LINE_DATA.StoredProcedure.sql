USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_IR_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_IR_LINE_DATA]
(
	@PurchaseOrderID int
)
AS
BEGIN
	select  a.COLUMN03 as COLUMN03,a.COLUMN05 as COLUMN04, a.COLUMN07 as COLUMN06, isnull(a.COLUMN12,0) as COLUMN07 ,
	(CAST(a.COLUMN07 as decimal(18,2))-CAST(isnull(a.COLUMN12,0) as decimal(18,2))) as COLUMN08, (CAST(a.COLUMN07 as decimal(18,2))-CAST(isnull(a.COLUMN12,0) as decimal(18,2))) as COLUMN09,
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	 iif((isnull(a.COLUMN32,0)>0),a.COLUMN32, a.COLUMN09) as COLUMN10	, cast((CAST(a.COLUMN07 as decimal(18,2))-CAST(isnull(a.COLUMN12,0) as decimal(18,2)))*iif(CAST(isnull(a.COLUMN32,0) as decimal(18,2))>0,CAST(a.COLUMN32 as decimal(18,2)),CAST(a.COLUMN09 as decimal(18,2))) as decimal(18,2)) as COLUMN11,
	a.COLUMN26 as COLUMN17,a.COLUMN17 as COLUMN24,null COLUMN65,iif((b.COLUMN66=1 or b.COLUMN66='True'),'Y','N')
	--EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
	 lottrack,a.COLUMN02 LineID,a.COLUMN32 COLUMN25,a.COLUMN27 unit,a.COLUMN02 COLUMN26,a.COLUMN37 COLUMN28,hs.COLUMN04 hsncode,a.column04 COLUMN05
	 FROM dbo.PUTABLE002 a inner join MATABLE007 b on a.COLUMN03=b.COLUMN02 and a.COLUMNA03=b.COLUMNA03
	 --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	 inner join PUTABLE001 h on h.COLUMN01=a.COLUMN19 and a.COLUMNA03=h.COLUMNA03 and isnull(h.COLUMNA13,0)=0
	 left join MATABLE032 hs on hs.COLUMN02=b.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=b.COLUMN75 and b.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	WHERE a.COLUMN19 = (select COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID) and (CAST(a.COLUMN07 as decimal(18,2))-CAST(isnull(a.COLUMN12,0) as decimal(18,2)))>0
	 and a.COLUMNA13='False'  order by a.Column02 Asc 
END







GO
