USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_JOBBERPAYMENT_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USE [MySmartSCM_SB_2016]
--GO
--/****** Object:  StoredProcedure [dbo].[usp_JO_TP_JOBBERPAYMENT_HEADER_DATA]    Script Date: 04/05/2016 1:52:36 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE  PROCEDURE [dbo].[usp_JO_TP_JOBBERPAYMENT_HEADER_DATA]
(
	@SalesOrderID nvarchar(250)
)

AS
BEGIN
SELECT  COLUMN02 , COLUMN05,COLUMN06 ,COLUMN09,  COLUMN11 ,COLUMN12, 
   COLUMN13,COLUMN14, COLUMN15,COLUMN16,COLUMN17,COLUMN19,COLUMN21 
   FROM SATABLE009  where  COLUMN05=@SalesOrderID and isnull(COLUMNA13,0)=0  
end

GO
