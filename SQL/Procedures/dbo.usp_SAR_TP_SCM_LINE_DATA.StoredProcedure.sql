USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_SCM_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_SCM_LINE_DATA]
( @SalesOrderID int=null,@ReceiptID int=null)
AS
BEGIN
	SELECT  
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	b.COLUMN03, b.COLUMN04,b.COLUMN05,((cast(d.COLUMN08 as decimal(18,2)))-sum(cast(isnull(f.COLUMN07,0) as decimal(18,2))) )COLUMN07,max(b.COLUMN08)COLUMN08,max(b.COLUMN09)COLUMN09,
	max(b.COLUMN10)COLUMN10,cast(((cast(d.COLUMN08 as decimal(18,2)))-sum(cast(isnull(f.COLUMN07,0) as decimal(18,2))) )* max(cast(d.COLUMN10 as decimal(18,2))) as decimal(18,2))COLUMN25,max(b.COLUMN12) COLUMN12,max(b.COLUMN13)COLUMN13,
	d.COLUMN17 COLUMN27,b.COLUMN25 COLUMN26
	,cast((((cast((isnull(m.COLUMN07,0)) as decimal(18,2))*(0.01)))*((((cast(d.COLUMN08 as decimal(18,2)))-sum(cast(isnull(f.COLUMN07,0) as decimal(18,2))))))
	 * (max(cast(d.COLUMN10 as decimal(18,2))))) as decimal(18,2))
	+cast((((cast(d.COLUMN08 as decimal(18,2)))-sum(cast(isnull(f.COLUMN07,0) as decimal(18,2))) )* max(cast(d.COLUMN10 as decimal(18,2))) )as decimal(18,2)) COLUMN11,m.column07 tax ,b.column17
	,b.COLUMN02 LineID,b.COLUMN35 COLUMN37,b.COLUMN37 COLUMN38
	FROM PUTABLE001 a inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19  and isnull(b.COLUMNA13,0)=0
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	inner join  PUTABLE003 c on c.COLUMN02=@ReceiptID inner join PUTABLE004 d on iif(isnull(d.COLUMN26,0)=0,0,isnull(b.COLUMN02,0))=isnull(d.COLUMN26,0) and d.COLUMN12=c.COLUMN01 and d.COLUMN03=b.COLUMN03 and d.COLUMN17=b.COLUMN26 and iif(d.COLUMN24='',0,isnull(d.COLUMN24,0))=iif(b.COLUMN17='',0,isnull(b.COLUMN17,0))  and isnull(d.COLUMNA13,0)=0
	left outer join SATABLE005 e on e.COLUMN33=c.COLUMN02 left outer join  SATABLE006 f on iif(isnull(d.COLUMN26,0)=0,0,isnull(f.COLUMN36,0))=isnull(d.COLUMN26,0) and  f.COLUMN19=e.COLUMN01 and f.COLUMN03=d.COLUMN03 and d.COLUMN17=f.COLUMN27 and iif(d.COLUMN24='',0,isnull(d.COLUMN24,0))=iif(f.COLUMN17='',0,isnull(f.COLUMN17,0))   and isnull(f.COLUMNA13,0)=0
	left join MATABLE013 m on  m.column02=b.COLUMN25
	WHERE  a.COLUMN02= @SalesOrderID and isnull(a.COLUMNA13,0)=0 group by b.COLUMN03,b.COLUMN04,b.COLUMN05,d.COLUMN17,b.COLUMN25,m.column07,b.column17,d.COLUMN08,b.COLUMN02,b.COLUMN35,b.COLUMN37
	having ((cast(d.COLUMN08 as decimal(18,2)))-sum(cast(isnull(f.COLUMN07,0) as decimal(18,2))))>0
END



GO
