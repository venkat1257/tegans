USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE011]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE011]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  @COLUMN05   nvarchar(250)=null,  
	@COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  @COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  
	@COLUMN10   nvarchar(250)=null,  @COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,  @COLUMN17   nvarchar(250)=null,
	@COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  @COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null, @COLUMN22   nvarchar(250)=null, @COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null, 
    @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250),
	@ReturnValue  int=null OUTPUT
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
 if((@COLUMN08='' or @COLUMN08=null))
  begin
  set @COLUMN08=0
  end
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE011_SequenceNo
insert into SATABLE011 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,  
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMNA01, COLUMNA02, COLUMNA03, 
   COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, 
   COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 

  @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  
  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMNA01, 
  @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
  @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
  @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
  @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
declare @newID int, @HeaderID nvarchar(250),@tempSTR nvarchar(MAX)
set @HeaderID =(select COLUMN01 from  SATABLE011 where column02=@column02);
if not exists(select COLUMN05  from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
BEGIN
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN13 as decimal(18,2))>0)
begin

insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03 )
			values(@newID,2000,@COLUMN19,@HeaderID,'JOBBER PAYMENT',@COLUMN05,@COLUMN08,@COLUMN04,'Paid',@COLUMN13,@COLUMN13,@COLUMN10,@COLUMN14, @COLUMNA03)		
end
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE019) as int)+1
DECLARE @BAL decimal(18,2),@Totamt decimal(18,2),@dueamt decimal(18,2),@Project nvarchar(250),@paidamt decimal(18,2),@Projectactualcost decimal(18,2)
SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08)as decimal(18,2)) - cast(@COLUMN13 as decimal(18,2)))	
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN13 as decimal(18,2))>0)
begin
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03) 
values(@newID,@COLUMN08,@COLUMN19,@COLUMN04,'JOBBER PAYMENT',@COLUMN05,@HeaderID,'',@COLUMN13,@BAL, @COLUMNA02, @COLUMNA03)
UPDATE FITABLE001 SET COLUMN10=cast( @BAL as decimal(18,2)) WHERE COLUMN02=@COLUMN08  and COLUMNA03=@COLUMNA03
end
end
else
	begin
			set @Totamt=(select COLUMN12 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02) 
            update PUTABLE016 set COLUMN04=@COLUMN19,
			--COLUMN12=(cast(isnull(@Totamt,0) as int)+cast(isnull(@COLUMN04,0) as int)),
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@COLUMN13,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@COLUMN13,0) as decimal(18,2))) where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08)as decimal(18,2)) - cast(@COLUMN13 as decimal(18,2)))		
   --EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN13 as decimal(18,2))>0)
begin
			update PUTABLE019 set COLUMN04=@COLUMN19,COLUMN10=@COLUMN13 where COLUMN05=@COLUMN04 and COLUMN08=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			--EMPHCS1410	Logs Creation by srinivas		
			UPDATE FITABLE001 SET COLUMN10=(cast((select column10 from FITABLE001 where column02=@COLUMN08) as decimal(18,2)) -cast( @BAL as decimal(18,2))) WHERE COLUMN02=@COLUMN08  and COLUMNA03=@COLUMNA03
end
end
--declare @number int 

--declare @SalesRep int 
--declare @Commission decimal 
--set @number = (select MAX(COLUMN01) from  SATABLE011);
--declare @Type int 
--set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
--set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
--set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)

--if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22304)
--	BEGIN
--		declare @PNewID int
--		set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
--		insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02, COLUMNA03)
--		values(@PNewID,@COLUMN19,'PAYMENT',@number,@COLUMN05,@COLUMN10,0,@COLUMN11,@COLUMN12,@COLUMN13,@SalesRep,@Commission,'OPEN',@COLUMNA02, @COLUMNA03) 
--	END 
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue =(Select COLUMN01 from SATABLE011 where COLUMN02=@COLUMN02)

END

 

IF @Direction = 'Select'

BEGIN

select * from SATABLE011

END 

 

IF @Direction = 'Update'

BEGIN
if((@COLUMN08='' or @COLUMN08=null))
begin
set @COLUMN08=0
end
UPDATE SATABLE011 SET

    COLUMN02=@COLUMN02,   COLUMN03=@COLUMN03,   COLUMN04=@COLUMN04,  COLUMN05=@COLUMN05,  COLUMN06=@COLUMN06,  COLUMN07=@COLUMN07,					
	COLUMN08=@COLUMN08,   COLUMN09=@COLUMN09,   COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11,  COLUMN12=@COLUMN12,  COLUMN13=@COLUMN13,					
	COLUMN14=@COLUMN14,   COLUMN15=@COLUMN15,   COLUMN16=@COLUMN16,  COLUMN17=@COLUMN17,  COLUMN18=@COLUMN18,  COLUMN19=@COLUMN19,					
	COLUMN20=@COLUMN20,   COLUMN21=@COLUMN21,   COLUMN22=@COLUMN22,   COLUMNA01=@COLUMNA01, COLUMNA02=@COLUMNA02,COLUMNA03=@COLUMNA03,
	COLUMNA04=@COLUMNA04,COLUMNA05=@COLUMNA05,				
	  COLUMNA07=@COLUMNA07, COLUMNA08=@COLUMNA08,COLUMNA09=@COLUMNA09,COLUMNA10=@COLUMNA10,COLUMNA11=@COLUMNA11,          
    COLUMNA12=@COLUMNA12, COLUMNA13=@COLUMNA13, COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03=@COLUMNB03,COLUMNB04=@COLUMNB04,				
	COLUMNB05=@COLUMNB05, COLUMNB06=@COLUMNB06,COLUMNB07=@COLUMNB07, COLUMNB08=@COLUMNB08,COLUMNB09=@COLUMNB09,COLUMNB10=@COLUMNB10,
	COLUMNB11=@COLUMNB11, COLUMNB12=@COLUMNB12, COLUMND01=@COLUMND01,COLUMND02=@COLUMND02,COLUMND03=@COLUMND03,COLUMND04=@COLUMND04,
	COLUMND05=@COLUMND05, COLUMND06=@COLUMND06, COLUMND07=@COLUMND07,COLUMND08=@COLUMND08,COLUMND09=@COLUMND09,COLUMND10=@COLUMND10
	WHERE COLUMN02 = @COLUMN02


DECLARE @INTERNAL nvarchar(250),@AMOUNT DECIMAL(18,2),@BillNo nvarchar(250)
	SET @INTERNAL=(SELECT COLUMN01 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL))

	set @HeaderID =(select COLUMN01 from  SATABLE011 where column02=@column02);
	delete from PUTABLE016  WHERE  COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	delete from PUTABLE019  WHERE COLUMN05=@COLUMN04 AND COLUMN08=@HeaderID AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	
if not exists(select COLUMN05  from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
BEGIN
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN13 as decimal(18,2))>0)
begin
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1
insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03)
			values(@newID,2000,@COLUMN19,@HeaderID,'JOBBER PAYMENT',@COLUMN05,@COLUMN08,@COLUMN04,'OPEN',@COLUMN13,@COLUMN13,@COLUMN10,@COLUMN14, @COLUMNA03)
end
			--update PUTABLE016 set column10=@APStatus  where COLUMN09=@BNO  and COLUMNA03=@COLUMNA03 and COLUMN20=@COLUMN23
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE019) as int)+1
SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08)as decimal(18,2)) - cast(@COLUMN13 as decimal(18,2)))	
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN13 as decimal(18,2))>0)
begin
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03) 
values(@newID,@COLUMN08,@COLUMN19,@COLUMN04,'JOBBER PAYMENT',@COLUMN05,@HeaderID,'',@COLUMN13,@BAL, @COLUMNA02, @COLUMNA03)
end
UPDATE FITABLE001 SET COLUMN10=cast( @BAL as decimal(18,2)) WHERE COLUMN02=@COLUMN08  and COLUMNA03=@COLUMNA03
end
else
	begin
			set @Totamt=(select COLUMN12 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02) 
            update PUTABLE016 set COLUMN04=@COLUMN19,
			--COLUMN12=(cast(isnull(@Totamt,0) as int)+cast(isnull(@COLUMN04,0) as int)),
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@COLUMN13,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@COLUMN13,0) as decimal(18,2))) where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			--update PUTABLE016 set column10=@APStatus  where COLUMN09=@BNO and COLUMNA03=@COLUMNA03 and COLUMN20=@COLUMN23
			SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08)as decimal(18,2)) - cast(@COLUMN13 as decimal(18,2)))	
			if(cast(@COLUMN13 as decimal(18,2))>0)
			begin
			update PUTABLE019 set COLUMN04=@COLUMN19,COLUMN10=@COLUMN13 where COLUMN05=@COLUMN04 and COLUMN08=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			UPDATE FITABLE001 SET COLUMN10=(cast((select column10 from FITABLE001 where column02=@COLUMN08) as decimal(18,2)) -cast( @BAL as decimal(18,2))) WHERE COLUMN02=@COLUMN08  and COLUMNA03=@COLUMNA03
			end
end
--delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='JOBBER PAYMENT' and COLUMN10=1120 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END
 

else IF @Direction = 'Delete'

BEGIN
UPDATE SATABLE011 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
SET @COLUMN08=(SELECT COLUMN08 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMNA02=(SELECT COLUMNA02 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMNA03=(SELECT COLUMNA03 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @INTERNAL=(SELECT COLUMN01 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL))
	set @COLUMN09=(SELECT SUM(COLUMN09) FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL))
	DELETE from PUTABLE016 WHERE COLUMN05=@INTERNAL AND COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	
	DELETE from PUTABLE019 WHERE COLUMN05=@COLUMN04 AND COLUMN08=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03

if((@COLUMN09!= '0' and @COLUMN09!= '0.00'  ) and @COLUMN09!= '')
	begin
	DECLARE @MaxRowA INT,@MaxRownum int,@d1 int,@id int,@cretAmnt decimal(18,2)
	DECLARE @FirstRow INT=1,@Initialrow int
	 DECLARE curP CURSOR FOR SELECT COLUMN02 from SATABLE012 where COLUMN08 in(@INTERNAL) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN curP
	  FETCH NEXT FROM curP INTO @d1
      SET @MaxRowA = (SELECT COUNT(*) FROM SATABLE012 where COLUMN08 in(@INTERNAL) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @FirstRow <= @MaxRowA
         BEGIN
		 declare @AdvIDs nvarchar(250)
		 set @AdvIDs=(select COLUMN15 from SATABLE012 where COLUMN02=@d1 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
      set @Initialrow =1
			   DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@COLUMN05 AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id='' or @id is not null)
		     BEGIN 
		        if((@COLUMN09!='0'  and @COLUMN09!= '0.00' and @COLUMN09!= '') and cast(@COLUMN09 as decimal(18,2))>0)
			  begin
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@COLUMN05 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0)
		         WHILE @Initialrow <= @MaxRownum
			  begin
			  if(cast(@COLUMN09 as DECIMAL(18,2))!=0 and @MaxRownum>0)
			  begin
				set @cretAmnt=(select column10 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				if(cast(@COLUMN09 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(@cretAmnt), COLUMN16='OPEN'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					--EMPHCS1410	Logs Creation by srinivas		
						set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Advance Amount Reset in FITABLE020 Table by '+cast(@cretAmnt as nvarchar(250))+' for '+cast(@id as nvarchar(250))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						)

					set @COLUMN09=(cast(@COLUMN09 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN09 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
				 declare @BALcretAmnt DECIMAL(18,2)
				 set @BALcretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				  if(@BALcretAmnt!=@cretAmnt)
				 begin
					update FITABLE020 set column18=(cast(isnull(@COLUMN09,0) as DECIMAL(18,2))+cast(isnull(@BALcretAmnt,0) as DECIMAL(18,2))), COLUMN16='OPEN'  where column02= @id
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 --EMPHCS1410	Logs Creation by srinivas		
						set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Advance Amount Reset in FITABLE020 Table by '+cast(@COLUMN14 as nvarchar(250))+' for '+cast(@id as nvarchar(250))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						)

					set @COLUMN09='0'
					end
				 end
				 end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1
				end
				CLOSE curA 
				deallocate curA
			END
			end
	
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET8***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Payment Line Data Deleted in SATABLE012 for '+cast(@d1 as nvarchar(250))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)
			 FETCH NEXT FROM curP INTO @d1
             SET @FirstRow = @FirstRow + 1 
			 end
        CLOSE curP
        deallocate curP
		end

		DECLARE @Initialrowd INT=1,@PIQty decimal(18,2),@PBQty decimal(18,2)
		set @Initialrowd=(1)
	SET @COLUMN08=(SELECT COLUMN01 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=19) where COLUMN02 in(select isnull(column03,0)  from  SATABLE012 where COLUMN08 in(@COLUMN08) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0)

		DECLARE curd CURSOR FOR SELECT COLUMN02 from SATABLE012 where COLUMN08 in(@COLUMN08) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0
		OPEN curd
		FETCH NEXT FROM curd INTO @id
		SET @MaxRownum = (SELECT COUNT(*) from SATABLE012 where COLUMN08 in(@COLUMN08) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0)
		WHILE @Initialrowd <= @MaxRownum
        BEGIN   
		set @COLUMN03=(select isnull(column03,0)  from  SATABLE012 where COLUMN02=@id)
		SET @PIQty=isnull((SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0),0)
		SET @PBQty=isnull((SELECT (@PIQty-(sum(COLUMN06)+sum(COLUMN09))) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0),0)
		IF(@PBQty>0)
		begin
		UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=17) where COLUMN02=@COLUMN03
		end
		else 
		begin
		UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=19) where COLUMN02=@COLUMN03
		end	
		 FETCH NEXT FROM curd INTO @id
             SET @Initialrowd = @Initialrowd + 1 
			 end
			 deallocate curd
			UPDATE SATABLE012 SET COLUMNA13=1 WHERE COLUMN08 in(@COLUMN08) 

delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='JOBBER PAYMENT' and COLUMN10=1120 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END
exec [CheckDirectory] @tempSTR,'usp_JO_TP_SATABLE011.txt',0
end try
begin catch		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_JO_SATABLE011.txt',0
return 0
end catch

end







GO
