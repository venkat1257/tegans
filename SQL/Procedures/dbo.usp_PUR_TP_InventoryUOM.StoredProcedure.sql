USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_InventoryUOM]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_InventoryUOM]
@Billno nvarchar(250)=null,@Billlineref nvarchar(250)=null,
@Item nvarchar(250)=null,@Quantity nvarchar(250)=null,@OperatingUnit nvarchar(250)=null,
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
@UOM nvarchar(250)=null,@AccountOwner nvarchar(250)=null,@Direction nvarchar(250),@Location nvarchar(250)=null,@lotno nvarchar(250)=null
AS
BEGIN
declare @Qty_On_Hand decimal(18,2),@Qty_On_Hand1 decimal(18,2),@Qty_Order decimal(18,2),@Qty_Avl decimal(18,2),@Qty_Cmtd decimal(18,2),
@COLUMN04 nvarchar(250)=null,@COLUMN14 nvarchar(250)=null,@COLUMN19 nvarchar(250)=null,@id nvarchar(250)=null,
@COLUMN09 nvarchar(250)=null,@COLUMN11 nvarchar(250)=null,@COLUMNA03 nvarchar(250)=null,@Type nvarchar(250)=null,@Price nvarchar(250)=null,
--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
@Transno nvarchar(250)=null,@Transid nvarchar(250)=null,@POrderno nvarchar(250)=null,@PTransno nvarchar(250)=null,@PTransid nvarchar(250)=null,
@COLUMNA02 nvarchar(250)=null,@PTranQty DECIMAL(18,2)=null,@COLUMN08 DECIMAL(18,2)=null,@COLUMN06 DECIMAL(18,2)=null,@COLUMN13 DECIMAL(18,2)=null,
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
@Qty_Commit DECIMAL(18,2),@Qty_BACK DECIMAL(18,2),@Qty DECIMAL(18,2),@chk bit
set @chk=(select column48 from matable007 where column02=@Item)
set @Location=(isnull(@Location,0))
if(@chk=1)
begin
set @COLUMN04=(@Item)
set @COLUMN14=(@OperatingUnit)

	  DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
      --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	  DECLARE curiv CURSOR FOR SELECT COLUMN01 from FITABLE038 where COLUMN05=@Billlineref and COLUMN04=@Billno and COLUMN06=@Item and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner and COLUMNA13=0
      OPEN curiv
	  FETCH NEXT FROM curiv INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE038 where COLUMN05=@Billlineref and COLUMN04=@Billno and COLUMN06=@Item and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @COLUMNA03=(@AccountOwner)
	     --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
             set @COLUMNA02=(@OperatingUnit)
             set @Type=(SELECT COLUMN03 FROM FITABLE038 WHERE COLUMN01=@id)
             set @UOM=(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Transno=(SELECT COLUMN04 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Transid=(SELECT COLUMN05 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Price=(SELECT COLUMN09 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Quantity=(SELECT COLUMN07 FROM FITABLE038 WHERE COLUMN01=@id)
             set @COLUMN19=(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN01=@id)
             set @COLUMN11=(SELECT COLUMN09 FROM FITABLE038 WHERE COLUMN01=@id)
             set @COLUMN09=(SELECT COLUMN07 FROM FITABLE038 WHERE COLUMN01=@id)
		     --UPDATE FITABLE038 SET COLUMN02=@Item WHERE COLUMN01=@id
			 if(@Type='Bill')
			 begin
			 if(@Direction='Insert')
			 begin
	   set @COLUMN14=(@OperatingUnit)
	   	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	   set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
	   if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
	   begin
	   	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+ cast(@COLUMN09 as decimal(18,2)));
	  set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
	  set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)))
	  --EMPHCS1117 rajasekhar reddy 09/09/2015 Multiple Uom Condition in bill and Invoice
	  if(@Qty_Order>0 or @Qty_Order>cast(@COLUMN09 as decimal(18,2)))
	  Begin
	  	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	  set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@COLUMN09 as decimal(18,2)));
	  end
	  else
	  begin
	  set @Qty_Order = 0;
	  end
	  set @Qty_Avl =  cast(@Qty_Cmtd as decimal(18,2));
	  	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+((cast(@COLUMN09 as decimal(18,2)))*(cast(@COLUMN11 as decimal(18,2)
	  )))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno

	  --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
	  --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end
	  end
	  else
	  begin
	  --EMPHCS1117 rajasekhar reddy 09/09/2015 Multiple Uom Condition in bill and Invoice
	  delete from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
	  declare @newIID int
	  declare @tmpnewIID1 int 
	  			set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
	  			if(@tmpnewIID1>0)
	  					begin
	  					set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
	  					end
	  					else
	  							begin
	  									set @newIID=1000
	  									end
											--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	  insert into FITABLE010 
	  (
	  --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	    COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22
	  )
	  values
	  (
	    @newIID,@COLUMN04,@COLUMN09,@COLUMN09,cast( @COLUMN09 as decimal(18,2))*cast( @COLUMN11 as decimal(18,2)),@COLUMN11 , @COLUMN14,@COLUMN19,@COLUMN14,@COLUMNA03,@Location,@lotno)
	  END
	  end
	  else
	  begin
	  	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	   set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
            if(@Qty_On_Hand1>=0)
            begin
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@Quantity as decimal(18,2)));
           set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
           set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)))
           set @Qty_Avl =  cast(isnull(@Qty_Cmtd,0) as decimal(18,2));
           --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	   UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))-((cast(@Quantity as decimal(18,2)))*(cast(@Price as decimal(18,2)
           )))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
           
		   --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
		   	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE   COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end

           end
	  end
	  end
	  --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
			 else if(@Type='SALESORDER')
			 begin
			 declare @COLUMN03 nvarchar(250), @COLUMN20 nvarchar(250), @COLUMN27 nvarchar(250), @COLUMN07 decimal(18,2),
			 @newID int,@TComitData decimal(18,2),@TrackQty decimal(18,2)
			 set @COLUMN03=(@Item)
			 set @COLUMN20=(@OperatingUnit)
			 set @COLUMN27=(@UOM)
			 set @COLUMN07=(@Quantity)
			 if(@Direction='Insert')
			 begin
			 declare @ComitData decimal(18,2), @WIP decimal(18,2), @QOH decimal(18,2), @BackOrderData decimal(18,2)
			 set @ComitData=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
	set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
	set @QOH=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
	set @BackOrderData=(select isnull(COLUMN06,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
if not exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
begin 
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMNA02,COLUMNA03
)
values
(
  @newID,@COLUMN03,'0',@COLUMN07, '0','0', @COLUMN20,@COLUMN27,@COLUMN20,@COLUMNA03 
)
end
else
begin
if(@ComitData>=0)
	begin
		if(@QOH<cast(@COLUMN07  as decimal(18,2)))
			begin
				set @WIP=@WIP+cast(@COLUMN07  as decimal(18,2))
				set @TComitData=@ComitData+cast(@COLUMN07  as decimal(18,2))
				set @BackOrderData=(cast(@COLUMN07  as decimal(18,2))-cast(@QOH  as decimal(18,2)))+cast(@BackOrderData  as decimal(18,2))
                set @Qty_Avl=0
						set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
						update  FITABLE010 set
						COLUMN05=@TComitData,COLUMN06=@BackOrderData ,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0
						end
			end
		else
			begin
				set @WIP=cast(@WIP  as decimal(18,2))+cast(@COLUMN07  as decimal(18,2))
		set @TComitData=cast(@ComitData  as decimal(18,2))+cast(@COLUMN07  as decimal(18,2))
		set @Qty_Avl=(@QOH)
		if(@Qty_Avl<0)
		begin
		set @Qty_Avl=0
		set @BackOrderData=(cast(@COLUMN07  as decimal(18,2))-cast(@QOH  as decimal(18,2)))+cast(@BackOrderData  as decimal(18,2))
		end
						set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
						if(@TrackQty=1)
						begin
						update  FITABLE010 set
						COLUMN05=@TComitData,COLUMN06= @BackOrderData,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0
						end
	end
end
else
	begin set @TComitData=cast(@COLUMN07  as decimal(18,2))
		set @Qty_Avl=(@QOH)
		set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
		if(@TrackQty=1)
		begin
		update  FITABLE010 set
			COLUMN05=@TComitData,COLUMN06=@BackOrderData,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0
		end
end
end
			 end
			 else
				begin
				 set @Qty=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
				 set @BackOrderData=(select isnull(COLUMN06,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0)
				 UPDATE FITABLE010 SET COLUMN05=cast(@Qty as decimal(18,2))-cast(@Quantity as decimal(18,2)),COLUMN06=cast(@BackOrderData as decimal(18,2))-cast(@Quantity as decimal(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0
				end
				end
			 else if(@Type='ITEMISSUE')
			 begin
			 declare @COLUMN15 nvarchar(250),@ItemQty decimal(18,2),@COLUMN12 decimal(18,2)
			 set @COLUMN04=(@Item)
			 set @COLUMN15=(@OperatingUnit)
			 set @COLUMN19=(@UOM)
			 set @uom=(@UOM)
			 set @COLUMN09=(@Quantity)
			 set @ItemQty=(@Quantity)
			 set @COLUMN12=(@COLUMN11)

             set @POrderno=(SELECT COLUMN06 FROM SATABLE007 WHERE COLUMN01=@Transid)
             set @PTransno=(SELECT COLUMN04 FROM SATABLE005 WHERE COLUMN02=@POrderno)
             set @PTransid=(SELECT COLUMN01 FROM SATABLE005 WHERE COLUMN02=@POrderno)
             set @PTranQty=(SELECT COLUMN07 FROM FITABLE038 WHERE COLUMN03='SALESORDER' AND COLUMN04=@PTransno AND COLUMN05=@PTransid AND COLUMN06=@Item AND COLUMN08=@UOM and @COLUMNA02=@COLUMNA02 and @COLUMNA03=@COLUMNA03)
			 if(@Direction='Insert')
			 begin
			set @Qty_BACK =0
 set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)- cast(@COLUMN09 as decimal(18,2));
 set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)
 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19);
 set @Qty_AVL=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19);
 set @Qty=cast(@COLUMN09 as decimal(18,2));
 set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
 set @Qty_AVL=( @Qty_AVL - @Qty);
 if(@Qty_AVL<0)
 begin
 set @Qty_AVL=0
 set @Qty_BACK =@Qty_Commit
 set @Qty_Commit=0
 end
 --EMPHCS1596 rajasekhar reddy patakota 10/03/2016 Average Price Calculation Changes in Bill and Invoice Transactions
  SET @COLUMN12=(SELECT ISNULL(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05 =@Qty_Commit, COLUMN07 =@Qty_BACK, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)as decimal(18,2))-(cast(@COLUMN09 as decimal(18,2))*cast(@COLUMN12 as decimal(18,2))))	 
  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19
				end
 update FITABLE038 set COLUMN07=(@PTranQty-@ItemQty) WHERE COLUMN03='SALESORDER' AND COLUMN04=@PTransno AND COLUMN05=@PTransid AND COLUMN06=@Item AND COLUMN08=@UOM and @COLUMNA02=@COLUMNA02 and @COLUMNA03=@COLUMNA03
 end

			 else
				begin
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)
				begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2)))+ cast(@ItemQty as decimal(18,2));
				set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as decimal(18,2));
				--EMPHCS1596 rajasekhar reddy patakota 10/03/2016 Average Price Calculation Changes in Bill and Invoice Transactions
				SET @Price=(SELECT ISNULL(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19)
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))-((cast(@ItemQty as decimal(18,2)))*(cast(@Price as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				end
				update FITABLE038 set COLUMN07=(@PTranQty+@ItemQty) WHERE COLUMN03='SALESORDER' AND COLUMN04=@PTransno AND COLUMN05=@PTransid AND COLUMN06=@Item AND COLUMN08=@UOM and @COLUMNA02=@COLUMNA02 and @COLUMNA03=@COLUMNA03
				end

				end
				end
else if(@Type='StockTransfer')
begin
--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
declare @STRef nvarchar(250)=null,@STQty decimal(18,2)=null,@STUnits nvarchar(250)=null,@STItem nvarchar(250)=null
             set @STRef=(SELECT COLUMN05 FROM FITABLE038 WHERE COLUMN01=@id)
             set @STItem=(SELECT COLUMN06 FROM FITABLE038 WHERE COLUMN01=@id)
             set @STQty=(SELECT isnull(COLUMN07,0) FROM FITABLE038 WHERE COLUMN01=@id)
             set @ItemQty=(SELECT isnull(COLUMN07,0) FROM FITABLE038 WHERE COLUMN01=@id)
             set @Price=(SELECT isnull(COLUMN09,0) FROM FITABLE038 WHERE COLUMN01=@id)
             set @STUnits=(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN01=@id)
declare @Sourceou nvarchar(250)=null,@Destinationou nvarchar(250)=null,@SAvgCost decimal(18,2)=null,@DAvgCost decimal(18,2)=null,
@SQOH nvarchar(250)=null,@SAvlQty nvarchar(250)=null,@DQOH nvarchar(250)=null,@DAvlQty nvarchar(250)=null
declare @InewID int,@ItmpnewID1 int ,@uomselection nvarchar(250)=null ,@stno nvarchar(250)=null
declare @totprice decimal(18,2)
set @Sourceou= (SELECT COLUMN05 FROM PRTABLE003 WHERE COLUMN01=(@STRef))
set @Destinationou= (SELECT COLUMN06 FROM PRTABLE003 WHERE COLUMN01=(@STRef))
set @SQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Sourceou and COLUMN19=@STUnits)
set @SAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Sourceou and COLUMN19=@STUnits)
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Destinationou and COLUMN19=@STUnits)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Destinationou and COLUMN19=@STUnits)
set @SAvgCost= (SELECT isnull(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Sourceou and COLUMN19=@STUnits)
set @SAvgCost= (cast(@SAvgCost as decimal(18,2))*cast(@STQty as decimal(18,2)))
set @totprice=(SELECT isnull(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Sourceou and COLUMN19=@STUnits)*(cast( @STQty as decimal(18,2)))
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
declare @tempSTR nvarchar(max)
			 if(@Direction='Insert')
			 begin
			 --EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
			 set @tempSTR=(null)
   set @tempSTR=('Target1:Line Insert For MultiUOM:Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@SQOH as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@SAvlQty as nvarchar(250)),'')+',3.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',4.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@STQty as nvarchar(250)),'')+',7.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+',8.Item '+isnull(cast(@STItem as nvarchar(250)),'')+',9.Source Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+',10.Destination Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
update FITABLE010 set COLUMN04=(cast( @SQOH as decimal(18,2))-cast( @STQty as decimal(18,2))), COLUMN08=(cast( @SAvlQty as decimal(18,2))-cast( @STQty as decimal(18,2)))
,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2))-((cast(@STQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2)))))
where COLUMN03=(@STItem) and COLUMN13=@Sourceou and COLUMN19=@STUnits
set @SQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Sourceou and COLUMN19=@STUnits)
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
			 set @tempSTR=(null)
   set @tempSTR=('Target2:Line Insert For MultiUOM:After Updation Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@SQOH as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@SQOH as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
if(cast(@SQOH as decimal(18,2))=0)
	begin
		UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits
	end
else
	begin
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2))/(cast( @SQOH as decimal(18,2))))as decimal(18,2))  WHERE COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits
	end
if exists(select column13  FROM FITABLE010 WHERE COLUMN13=@Destinationou and  COLUMN03=@STItem and COLUMN19=@STUnits)
begin
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Destinationou and COLUMN19=@STUnits)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Destinationou and COLUMN19=@STUnits)
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
			 set @tempSTR=(null)
   set @tempSTR=('Target3:Line Insert For MultiUOM:Stock Transfer Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+',3.Item '+isnull(cast(@STItem as nvarchar(250)),'')+',4.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
update FITABLE010 set COLUMN04=(cast( @DQOH as decimal(18,2))+cast( @STQty as decimal(18,2))), COLUMN08=(cast( @DQOH as decimal(18,2))+cast( @STQty as decimal(18,2)))
,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2))+((cast(@STQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2)))))
where COLUMN03=(@STItem) and COLUMN13=@Destinationou and COLUMN19=@STUnits
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@STItem) and COLUMN13=@Destinationou and COLUMN19=@STUnits)
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 set @tempSTR=(null)
   set @tempSTR=('Target4:Line Insert For MultiUOM:After Updation Stock Transfer Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@DQOH as nvarchar(250)),'')+',3.Item '+isnull(cast(@STItem as nvarchar(250)),'')+',4.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
if(cast(@DQOH as decimal(18,2))=0)
	begin
		UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits
	end
else
	begin
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2))/(cast( @DQOH as decimal(18,2))))as decimal(18,2)) WHERE COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits
	end
end
else
begin
set @newID=((select MAX(isnull(COLUMN02,999)) from FITABLE010)+1)
declare @Avgprice decimal(18,2)
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @Avgprice=(@totprice)/(cast( @STQty as decimal(18,2)))
set @tempSTR=(null)
   set @tempSTR=('Target5:Line Insert For MultiUOM:Destination New Inventory Values are 1.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@STQty as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+',4.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+',5.Total Price '+isnull(cast(@totprice as nvarchar(250)),'')+',5.Average Price '+isnull(cast(@Avgprice as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04 , COLUMN08, COLUMN13,COLUMN19,COLUMN12,COLUMN17,COLUMNA02,COLUMNA03
)
values
(
  @newID,@STItem,@STQty ,@STQty, @Destinationou,@STUnits,@totprice,@Avgprice,@Destinationou,@COLUMNA03 
)
end
end
			 else
				 begin
				 --EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2)));
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2)));
				set @tempSTR=(null)
				set @tempSTR=('Target1:Line Update For MultiUOM:Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',3.Destination Quantity Onhand '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',4.Destination Available Quantity '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',5.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',6.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+',7.Item '+isnull(cast(@STItem as nvarchar(250)),'')+',8.Source Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+',9.Destination Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits) as decimal(18,2))))) WHERE COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand1, COLUMN08=@Qty_On_Hand1 ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2))-((cast(@ItemQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits) as decimal(18,2))))) WHERE COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits
				
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2)));
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @tempSTR=(null)
				set @tempSTR=('Target2:Line Update For MultiUOM:After Update Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',3.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',4.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+',5.Item '+isnull(cast(@STItem as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@STItem AND COLUMN13=@Sourceou and COLUMN19=@STUnits
				end
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2)));
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @tempSTR=(null)
				set @tempSTR=('Target3:Line Update For MultiUOM:After Update Stock Transfer Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',3.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',4.Units '+isnull(cast(@STUnits as nvarchar(250)),'')+',5.Item '+isnull(cast(@STItem as nvarchar(250)),'')+' of  '+isnull('PRTABLE004','')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				if(cast(@Qty_On_Hand1 as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits)as decimal(18,2))/cast(@Qty_On_Hand1 as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@STItem AND COLUMN13=@Destinationou and COLUMN19=@STUnits
				end
				end
end

	  else
	  begin
	  --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	         set @POrderno=(SELECT COLUMN04 FROM SATABLE010 WHERE COLUMN15 in(@Transid))
             set @PTransno=(SELECT COLUMN04 FROM SATABLE007 WHERE COLUMN02=@POrderno)
             set @PTransid=(SELECT COLUMN01 FROM SATABLE007 WHERE COLUMN02=@POrderno)
             set @PTranQty=(SELECT COLUMN07 FROM FITABLE038 WHERE COLUMN03='ITEMISSUE' AND COLUMN04=@PTransno AND COLUMN05=@PTransid AND COLUMN06=@Item AND COLUMN08=@UOM and @COLUMNA02=@COLUMNA02 and @COLUMNA03=@COLUMNA03)
			 if(@Direction='Insert')
			 begin
			set @Qty_BACK =0
				--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno);
			set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno);
			set @Qty=cast(@Quantity as DECIMAL(18,2));
			set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
			set @Qty_Commit=( @Qty_Commit - @Qty)
			set @Qty_AVL=(@Qty_On_Hand)-@Qty_Commit
		if(@Qty_AVL<0)
			begin
				set @Qty_AVL=0
				set @Qty_BACK =@Qty_Commit
				set @Qty_Commit=0
			end
			--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
				--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
				--EMPHCS1596 rajasekhar reddy patakota 10/03/2016 Average Price Calculation Changes in Bill and Invoice Transactions
			SET @Price=(SELECT ISNULL(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
		    UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN07 =@Qty_BACK, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-(cast(@Quantity as DECIMAL(18,2))*cast(@Price as DECIMAL(18,2)))) WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
			
		   --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end

		end
		else
		begin
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
		         set @Qty_AVL=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
		         set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
				 UPDATE FITABLE010 SET COLUMN04=cast(@Qty_On_Hand as decimal(18,2))+cast(@Quantity as decimal(18,2)),
									   --COLUMN05=cast(@QtyC as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   COLUMN08=cast((cast(@Qty_On_Hand as decimal(18,2))+cast(@Quantity as decimal(18,2))) as decimal(18,2))
				 WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				 --EMPHCS1596 rajasekhar reddy patakota 10/03/2016 Average Price Calculation Changes in Bill and Invoice Transactions
				SET @Price=(SELECT ISNULL(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)
				UPDATE FITABLE010 SET COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+((cast(@Quantity as decimal(18,2)))*(cast(@Price as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
		   
		   --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
		   	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@OperatingUnit AND COLUMN19=@UOM AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno
				end

		end
		end
			 FETCH NEXT FROM curiv INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
        CLOSE curiv 
        deallocate curiv 
END
END

GO
