USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_ItemUPCCheck]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_ItemUPCCheck]
(
	@ItemID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL
)
AS
BEGIN
if exists(select column02 from MATABLE021 where COLUMN04=@ItemID and isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null) and  COLUMNA03=@AcOwner)
begin
select 'true' COLUMN06,COLUMN04 upc from MATABLE021 where COLUMN04=@ItemID and isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null) and  COLUMNA03=@AcOwner
end
else if exists(select column02 from MATABLE007 WHERE COLUMN06=@ItemID and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null) and COLUMNA03=@AcOwner)
begin
 select 'true' COLUMN06,COLUMN06 upc from MATABLE007 WHERE COLUMN06=@ItemID and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null) and COLUMNA03=@AcOwner
 end
 else
 begin
 select max('false') COLUMN06,null upc  from MATABLE007
 end
END


GO
