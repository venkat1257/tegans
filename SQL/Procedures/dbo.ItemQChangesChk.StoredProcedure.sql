USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ItemQChangesChk]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[ItemQChangesChk](@ItemID nvarchar(250)=null,@SName nvarchar(250)=null,@OPUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null)
as
begin
select s10.COLUMN10 AS AQty FROM SATABLE009 S9 INNER JOIN SATABLE010 S10
LEFT JOIN MATABLE007 M7 ON M7.COLUMN02= s10.COLUMN05 
ON S10.COLUMN15=S9.COLUMN01 
WHERE S9.COLUMN02=@SName AND M7.COLUMN04 = @ItemID  and  S9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
and s9.COLUMNA03=@AcOwner

end



GO
