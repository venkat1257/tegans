USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[MISDuplicateCheck]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[MISDuplicateCheck]
(@AcOwner int,@Opunit nvarchar(250)=null,@MIS nvarchar(250)=null)
as
Begin
select COLUMN15 from  MATABLE019 where COLUMN15 =@MIS and COLUMN15 !='' AND   (COLUMNA02=@Opunit or COLUMNA02 is null)  AND COLUMNA03=@AcOwner and isnull(columna13,0)=0
end
GO
