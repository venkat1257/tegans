USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE012]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAL_TP_SATABLE012]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  @COLUMN05   nvarchar(250)=null,  
	@COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  @COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  
	@COLUMN10   nvarchar(250)=null,  @COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	 ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN for ADVID)
	 --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,  @COLUMN17   nvarchar(250)=null, 
	@COLUMN18   nvarchar(250)=null,  @COLUMN20   nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null, 
    @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),  @TabelName  nvarchar(250), 
	@ReturnValue nvarchar(250)=null out
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
DECLARE @BNO nvarchar(250),@Project nvarchar(250),@dt nvarchar(250),@CUID INT
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
--set @COLUMN08 =(select MAX(COLUMN01) from  SATABLE011);
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE012_SequenceNo
set @COLUMN18=(iif(isnull(@COLUMN18,0)='',0,isnull(@COLUMN18,0)))
set @BNO =(select COLUMN04 from  SATABLE009 where COLUMN02=@COLUMN03);
set @dt=(select COLUMN19 from SATABLE011 where COLUMN01=@COLUMN08)
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
set @Project =(select COLUMN22 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN10 =(select COLUMN15 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN11 =(select COLUMN16 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN12 =(select COLUMN17 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @CUID =(select COLUMN05 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN20=(iif((cast(@COLUMN20 as nvarchar(250))='' or isnull(@COLUMN20,0)='0'),@CUID,@COLUMN20))
set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null ) THEN (select COLUMN14 from SATABLE011 
   where COLUMN01=@COLUMN08) else @COLUMNA02  END )
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level   
if(@COLUMN05!= '' and @COLUMN05 is not null)
begin
set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))-((cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))+cast(isnull(@COLUMN09,0 )as DECIMAL(18,2))));
end
--set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))+cast(isnull(@COLUMN09,0 )as DECIMAL(18,2)));
--EMPHCS1410	Logs Creation by srinivas
  declare @tempSTR nvarchar(max)
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Line Values are Intiated for SATABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMNA01,'')+','+isnull(@COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 											  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+'')
   )

insert into SATABLE012 

( ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN for ADVID)
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12, COLUMN13,  COLUMN14,COLUMN15,
   --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMN16,  COLUMN17,  COLUMN18,   COLUMN20,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, 
   COLUMND08, COLUMND09, COLUMND10
)

values

( @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  
 ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN for ADVID)
  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15, 
  --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
  --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
  @COLUMN16,  @COLUMN17,  @COLUMN18,   @COLUMN20,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, 
  @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, 
  @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, 
  @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 

   --EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Line Values are Created in SATABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
    )

--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
if(@COLUMN05= '' or @COLUMN05 is null)
begin
declare @cdueamt decimal(18,2)
set @cdueamt=(select min(isnull(COLUMN05,0)) from  SATABLE012 Where COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0)
set @COLUMN05=((SELECT CASE WHEN isnull(@COLUMN05,0)=0 THEN ( cast(@COLUMN04 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))+cast(isnull(@COLUMN09,0 )as DECIMAL(18,2)))
 ELSE (cast(@cdueamt as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))+cast(isnull(@COLUMN09,0 )as DECIMAL(18,2))) END));
update SATABLE012 set COLUMN05=@COLUMN05 Where COLUMN02=@COLUMN02
end
declare @cust int 
declare @Payment# nvarchar(250), @Commission DECIMAL(18,2), @number int, @number1 int, @date date  
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @number = (select MAX(COLUMN01) from  SATABLE011 where COLUMN01=@COLUMN08);
set @number1 = (select MAX(COLUMN01) from  SATABLE012 where COLUMN02=@COLUMN02);
set @cust = (select COLUMN05 from  SATABLE011 where COLUMN01=@number);
set @date = (select COLUMN19 from  SATABLE011 where COLUMN01=@number);;
set @Payment# = (select COLUMN04 from  SATABLE011 where COLUMN01=@number);
if exists(select column01 from SATABLE012 where column08=@COLUMN08 and @COLUMN18=0)
					begin
declare @col6 DECIMAL(18,2)
declare @col5 DECIMAL(18,2)
DECLARE @RefSOID nvarchar(250)=null
DECLARE @RefSO nvarchar(250)=null
DECLARE @Result nvarchar(250)=null
DECLARE @RefBillID nvarchar(250)=null
DECLARE @RefPaymentID nvarchar(250)=null
DECLARE @AMT DECIMAL(18,2)
DECLARE @PAMT DECIMAL(18,2)
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
DECLARE @PDUEAMT decimal(18,2)
DECLARE @PCRAMT  decimal(18,2)
DECLARE @PADVAMT decimal(18,2)
DECLARE @PDISAMT decimal(18,2)
declare @Tamt DECIMAL(18,2)
declare @DAMT DECIMAL(18,2)
declare @PIQty DECIMAL(18,2)
declare @PBQty DECIMAL(18,2)
DECLARE @deposit DECIMAL(18,2)
--EMPHCS1593	payment calculation regarding bank selection in Bill Payment and Customer payment By RAj.Jr
set @deposit=  (SELECT isnull(COLUMN08,0) FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
set @col6=@COLUMN06
set @col5=@COLUMN05

set @DATE =(select COLUMN19 from  SATABLE011 Where COLUMN01=@COLUMN08 );
SET @RefSOID=(SELECT COLUMN06 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
SET @RefSO=(SELECT COLUMN04 FROM SATABLE005 WHERE COLUMN02=@RefSOID);
SET @RefBillID=(SELECT COLUMN07 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
SET @RefPaymentID=(SELECT COLUMN02 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
SET @AMT=(SELECT COLUMN15 FROM SATABLE005 WHERE COLUMN02=@RefSOID);
SET @Tamt=(SELECT COLUMN15  from SATABLE005 where COLUMN02=@RefSOID)
SET @PAMT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN08 in(select COLUMN01 from SATABLE011 where COLUMN06=@RefSOID))
SET @DAMT=(SELECT isnull(SUM(COLUMN13),0) FROM SATABLE012 WHERE COLUMN08 in(select COLUMN01 from SATABLE011 where COLUMN06=@RefSOID))
SET @PAMT=(cast(@PAMT as DECIMAL(18,2))+cast(isnull(@DAMT,0) as DECIMAL(18,2))+cast(isnull(@COLUMN14,0) as DECIMAL(18,2))+cast(isnull(@COLUMN09,0) as DECIMAL(18,2)))

if(@RefSOID='')
begin
SET @Tamt=(SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
SET @PAMT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
SET @DAMT=(SELECT isnull(SUM(COLUMN13),0) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
SET @PAMT=(cast(@PAMT as DECIMAL(18,2))+cast(isnull(@DAMT,0) as DECIMAL(18,2))+cast(isnull(@COLUMN14,0) as DECIMAL(18,2))+cast(isnull(@COLUMN09,0) as DECIMAL(18,2)))
end
--if(@Tamt=@PAMT)
--begin
----update SATABLE005 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=38) where COLUMN02=@RefSOID
----update  PUTABLE018 set COLUMN13='Paid' where COLUMN05=@BNO
--update  PUTABLE018 set COLUMN13='Paid' where COLUMN05 in 
----EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--( select a.COLUMN04 from  SATABLE011 a  inner join SATABLE012 b on b.COLUMN08=a.COLUMN01  where b.COLUMN03=@COLUMN03 and b.COLUMNA02=@COLUMNA02 and b.COLUMNA03=@COLUMNA03)
--set @Result='Paid'
--end
--else
--begin
----update SATABLE005 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=39) where COLUMN02=@RefSOID
----EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--update  PUTABLE018 set COLUMN13='Partially Paid' where COLUMN05=@BNO and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--set @Result='Partially Paid'
--end
SET @DAMT=isnull((SELECT sum(COLUMN13) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
--SET @PBQty=(cast(@PBQty as DECIMAL(18,2))+cast(isnull(@DAMT,0) as DECIMAL(18,2))+cast(isnull(@COLUMN14,0) as DECIMAL(18,2))+cast(isnull(@COLUMN09,0) as DECIMAL(18,2)))

SET @PIQty=isnull((SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
SET @PBQty=isnull((SELECT @PIQty-((sum(cast(COLUMN06 as DECIMAL(18,2)))+sum(cast(isnull(COLUMN13,0 )as DECIMAL(18,2))))+sum(cast(isnull(COLUMN14,0 )as DECIMAL(18,2)))+sum(cast(isnull(COLUMN09,0 )as DECIMAL(18,2)))) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
IF(@PBQty>0)
begin
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=32) where COLUMN02=@COLUMN03
end
else IF(@PBQty=0.00)
begin
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=34) where COLUMN02=@COLUMN03
end

SET @PIQty=isnull((SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03),0)
SET @PBQty=isnull((SELECT sum(COLUMN06) FROM SATABLE012 WHERE COLUMN03=@COLUMN03),0)
--IF(@PIQty=(@PBQty))
--begin
--update SATABLE011 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=40) 
--where COLUMN01 in (select COLUMN08 from SATABLE012 where COLUMN03=@COLUMN03)
--UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=40) where COLUMN02=@COLUMN03
--end
--else
--begin
--update SATABLE011 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=39) 
--where COLUMN01 in (select COLUMN08 from SATABLE012 where COLUMN03=@COLUMN03)
--UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=39) where COLUMN02=@COLUMN03
--end
declare @iID int
declare @inID nvarchar(250)
Declare @CustID int
declare @Oredr nvarchar(250)
declare @acID varchar(250)
declare @acID1 int
Declare @memo  nvarchar(250)=null
Declare @Num  nvarchar(250)=null
begin
set @CustID = (select COLUMN05 from SATABLE011 where COLUMN02=@RefPaymentID);
set @memo = (select COLUMN10 from SATABLE011 where COLUMN02=@RefPaymentID);
set @Num = (select COLUMN04 from SATABLE011 where COLUMN02=@RefPaymentID);
BEGIN
declare @newID int,@Totamt DECIMAL(18,2),@dueamt DECIMAL(18,2),@paidamt DECIMAL(18,2),@tmpnewID int ,@FormID int,@Creditcashsale bit
set @Creditcashsale = (select isnull(COLUMN04,0) from CONTABLE031 where COLUMN03='Allow credit for cash sale' and isnull(COLUMN04,0)=1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @FormID=(select isnull(max(COLUMN03),'0') from SATABLE009 where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03)
if(cast(isnull(@COLUMN06,0) as decimal(18,2))>0 and ((@FormID!='1532' and @FormID!='1604')  or (@Creditcashsale=1 or @Creditcashsale='True' or @Creditcashsale='1')))
BEGIN
if not exists(select COLUMN05  from PUTABLE018 where COLUMN05=@Num AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and COLUMN07=@COLUMN20 and COLUMN17=@COLUMN08)
BEGIN
set @tmpnewID=(select MAX(COLUMN02) from PUTABLE018);
if(@tmpnewID>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1;
end
else
begin
set @newID=1000;
end
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Receivable Values are Intiated for PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are ') 			
   )
set @COLUMN20=(iif((cast(@COLUMN20 as nvarchar(250))='' or isnull(@COLUMN20,0)='0'),@CustID,@COLUMN20))

   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @dt,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN20,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN06,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
	insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,column11,column12,COLUMN13,COLUMNA02,COLUMNA03,COLUMN18,COLUMN16,COLUMN17)
			    values(@newID,3000,@dt,@Num,'Payment',@COLUMN20,@memo,@COLUMN06,NULL,'Paid',@COLUMNA02,@COLUMNA03,@Project,@COLUMN03,@COLUMN08) 
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Receivable Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
			
	set @COLUMN20=(iif((cast(@COLUMN20 as nvarchar(250))='' or isnull(@COLUMN20,0)='0'),@CustID,@COLUMN20))

   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @dt,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN20,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN06,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0', @COLUMNA07  = @COLUMNA06
	
			set @paidamt=(select cast(isnull(COLUMN11,0) as decimal(18,2)) from PUTABLE018 where COLUMN05=@Num and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN07=@COLUMN20 and COLUMN17=@COLUMN08)
            update PUTABLE018 set --COLUMN04=@COLUMN19,
			COLUMN11=(cast(isnull(@paidamt,0) as DECIMAL(18,2))+cast(isnull(@COLUMN06,0) as DECIMAL(18,2))), COLUMN07=@COLUMN20--, COLUMN18=@COLUMN22
			 where COLUMN05=@Num and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN17=@COLUMN08
end
END
END

declare @SalesRep int 
declare @Type int 
set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@cust)
set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@cust)
set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@cust and COLUMN22=@SalesRep)

if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22304)
	BEGIN
	--EMPHCS1028	COA commission report - COMMISSION PAYMENT (for all commission transactions) 24/8/2015 BY RAJ.Jr
		declare @PNewID int
		set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
		--EMPHCS1151	Agent Commission payments(amount pay-vat=tottal ) BY RAJ.Jr 20/9/2015
		if  exists(select COLUMN03 from SATABLE012 where COLUMN03=@COLUMN03)
		begin
		declare @Count int
		declare @tax decimal(18,2)
		set @tax=(select COLUMN24 from SATABLE009 where COLUMN02=@COLUMN03)
		set @Count=(select count(COLUMN03) from SATABLE012 where COLUMN03=@COLUMN03)
		if(@Count=1)
		begin
--EMPHCS1410	Logs Creation by srinivas
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission  Values are Intiated for FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@PNewID as nvarchar(250)) +','+cast(@date as nvarchar(250)) +',COMMISSION PAYMENT'+  
						 cast(@number1 as nvarchar(250))+','+  cast(@cust as nvarchar(250))+','+  cast(@Payment# as nvarchar(250))+','+  cast(@COLUMN04 as nvarchar(250))+','+  
						 cast(((cast(@COLUMN06 as DECIMAL(18,2))-@tax-cast(@COLUMN13 as decimal(18,2)))*(@Commission)/100)as nvarchar(250))+','+ cast(@COLUMN06 as nvarchar(250))+','+ cast( @SalesRep as nvarchar(250))+','+cast(@Commission as nvarchar(250))+',OPEN,'+isnull(@COLUMNA02,'')+ ','+isnull(@COLUMNA03,'')+ ' ')
					)
					declare @amnt decimal(18,2)
					set @amnt = ((cast(@COLUMN06 as decimal(18,2))-@tax-cast(@COLUMN13 as decimal(18,2)))*(@Commission)/100)
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @dt,   @COLUMN08 = '22492',  @COLUMN09 = @SalesRep,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @amnt,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
		insert into FITABLE027
(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02, COLUMNA03)
 values(@PNewID,@date,'COMMISSION PAYMENT',@number1,@cust,@Payment#,@COLUMN04,(cast(@COLUMN06 as decimal(18,2))-@tax-cast(@COLUMN13 as decimal(18,2)))*(@Commission)/100,
 @COLUMN06,@SalesRep,@Commission,'OPEN',@COLUMNA02, @COLUMNA03) 
 --EMPHCS1410	Logs Creation by srinivas
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission Intiated Values are Created in FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + '')
					)

 end
 else 
 begin
 --EMPHCS1410	Logs Creation by srinivas
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission  Values are Intiated for FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@PNewID as nvarchar(250)) +','+cast(@date as nvarchar(250)) +',COMMISSION PAYMENT'+  
						 cast(@number1 as nvarchar(250))+','+  cast(@cust as nvarchar(250))+','+  cast(@Payment# as nvarchar(250))+','+  cast(@COLUMN04 as nvarchar(250))+','+  
						 cast(((cast(@COLUMN06 as DECIMAL(18,2)))*(@Commission)/100)as nvarchar(250))+','+  cast(@COLUMN06 as nvarchar(250))+','+ cast( @SalesRep as nvarchar(250))+','+cast(@Commission as nvarchar(250))+',OPEN,'+isnull(@COLUMNA02,'')+ ','+isnull(@COLUMNA03,'')+ ' ')
					)
					set @amnt = (cast(@COLUMN06 as decimal(18,2))*(@Commission)/100)
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @dt,   @COLUMN08 = '22492',  @COLUMN09 = @SalesRep,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @amnt,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
 insert into FITABLE027
(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02, COLUMNA03)
 values(@PNewID,@date,'COMMISSION PAYMENT',@number1,@cust,@Payment#,@COLUMN04,cast(@COLUMN06 as decimal(18,2))*(@Commission)/100,@COLUMN06,@SalesRep,@Commission,'OPEN',@COLUMNA02, @COLUMNA03) 
 --EMPHCS1410	Logs Creation by srinivas
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission Intiated Values are Created in FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + '')
					)

 end
 end
	END 
END
set @ReturnValue = 1
end

--EMPHCS994	 Add Discount Field in Customer Payment by srinivas 20/8/2015
if(cast(isnull(@COLUMN13,0) as DECIMAL(18,2))>0)
	begin
		set @Totamt=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMNA13=0);
		set @PNewID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
		--EMPHCS1410	Logs Creation by srinivas		
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@PNewID as nvarchar(250)) +','+  cast(@date as nvarchar(250))+','+  'PAYMENT'+','+ cast(@number1 as nvarchar(250))+','+ cast( @cust as nvarchar(250))+',NULL,1049,'+ 
						 cast(isnull(@COLUMN13,0) as nvarchar(250))+', '+   cast(CAST((CAST(isnull(@COLUMN13,0) AS decimal(18,2))+CAST(isnull(@Totamt,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+ isnull( @COLUMNA02,'')+','+isnull(@COLUMNA03,'') + '0 ')
					)

		--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMNA02, COLUMNA03, COLUMNA13)
		EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @date,   @COLUMN08 = '22335',  @COLUMN09 = @cust,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN13,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
		values(@PNewID,@date,'PAYMENT',@Payment#,@cust,@number1,1049,isnull(@COLUMN13,0),@COLUMNA02 , @COLUMNA03,0,@Project)  
	--EMPHCS1410	Logs Creation by srinivas
			set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
					)

	end
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
		declare @IDAR nvarchar(250),@refID nvarchar(250),@Totbal1 nvarchar(250)
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		set @Totbal1= (select column13 from SATABLE011 where COLUMN01=@COLUMN08)
		set @memo=(select COLUMN10 from SATABLE011 where column01=@COLUMN08)
		set @refID=(select COLUMN04 from SATABLE011 where column01=@COLUMN08)
		set @COLUMN09=(select COLUMN09 from SATABLE012 where column02=@COLUMN02)
		set @CustID = (select COLUMN05 from SATABLE011 where column01=@COLUMN08);
		set @memo = (select COLUMN10 from SATABLE011 where column01=@COLUMN08);
		set @Num = (select COLUMN04 from SATABLE011 where column01=@COLUMN08);

declare @cretAmnt decimal(18,2),@retAmnt decimal(18,2),@VTransno nvarchar(250),@TransAmt decimal(18,2),@delimiterstr nvarchar(250),
@Vocherid nvarchar(250),@Vochertype nvarchar(250),@MainString nvarchar(250),@VochrAmt decimal(18,2),@PrevAmt decimal(18,2),
@Ivoiceid nvarchar(250),@Ivoiceno nvarchar(250),@id1 nvarchar(250)
		if(@COLUMN14!='0' and @COLUMN14!='0.00' and @COLUMN14!='' )
	    begin

		set @VochrAmt=(@COLUMN14)
		set @MainString=(@COLUMN16)
		DECLARE @cnt INT = 0,@vochercnt INT = 1;
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='C')
		begin
		set @VTransno=(select column04 from satable005 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN15,0)-isnull(COLUMND05,0))COLUMN15 from satable005 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from satable005 where column02=@Vocherid)
		update satable005 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from satable005 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update satable005 set COLUMND05=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update satable005 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN15,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN15 from satable005 where column02=@Vocherid)=0)
		begin
		update satable005 set COLUMN16='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		end
		else if(@Vochertype='R')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN16,0)-isnull(COLUMND05,0))COLUMN16 from FITABLE024 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE024 where column02=@Vocherid)
		update FITABLE023 set COLUMN13='OPEN' where column01 in(select COLUMN09 from FITABLE024 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE024 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE024 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN16,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN16 from FITABLE024 where column02=@Vocherid)=0)
		begin
		update FITABLE023 set COLUMN13='CLOSE' where column01 in(select COLUMN09 from FITABLE024 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE023 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMN19,0) from FITABLE023 where column02=@Vocherid)
		update FITABLE023 set COLUMN13='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE023 set COLUMN19=(@PrevAmt-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		exec usp_SAL_TP_AdvanceCalculations  @refID,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@VochrAmt
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE023 set COLUMN19=0 where column02=@Vocherid and columna03=@columna03
		exec usp_SAL_TP_AdvanceCalculations  @refID,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@PrevAmt
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		if((select CAST(isnull(COLUMN19,0) AS DECIMAL(18,2)) from FITABLE023 where column02=@Vocherid)=0)
		begin
		update FITABLE023 set COLUMN13='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		declare @TAXRATE decimal(18,2)=null, @TaxAG nvarchar(250)=null, @Taxregid nvarchar(250)=null,@Vid int,@COLUMN24 DECIMAL(18,2)
		set @Tax= ISNULL((SELECT COLUMN12 FROM FITABLE023 WHERE COLUMN02=@Vocherid),1000)
		set @COLUMN24= ISNULL((SELECT CAST(COLUMN24 AS DECIMAL(18,2)) FROM FITABLE023 WHERE COLUMN02=@Vocherid),0)
		set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
		set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
		set @Taxregid= (SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE026)+1
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TaxAG in(23585,23586,23584,23583,23582))
		BEGIN
			set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
			declare @TaxColumn17 nvarchar(250)
			set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
			EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @date,   @COLUMN08 = '22335',  @COLUMN09 = @CustID,    @COLUMN10 = '22383', @COLUMN11 = '1143',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN24,       @COLUMN15 = 0,         @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,  COLUMN12,  COLUMN14,  COLUMN16,COLUMN17,  COLUMNA02, COLUMNA03)
			values(@Vid,@date,'PAYMENT',@number1,@CustID,@memo,1143,@Num,@COLUMN24,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03)
		
			--set @Vid=((select isnull(max(column02),999) from FITABLE034)+1)
			--set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
			--Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,  COLUMN10,  COLUMNA02, COLUMNA03)
			--				values(@Vid,'PAYMENT',@date,@memo,@CustID,@COLUMN24,@Num,1143,@COLUMNA02, @COLUMNA03)
		END
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN05,0)-isnull(COLUMND05,0))COLUMN05 from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END
		
		if(@COLUMN14='' or @COLUMN14 is null) set @COLUMN14=0;
		set @COLUMN06=(select COLUMN06 from SATABLE012 where column02=@COLUMN02)
			--if(@COLUMN17='Journal' or @COLUMN17='DebitMemo' or @COLUMN17='PaymentVoucher' or @COLUMN17='AdvancePayment')
			--	begin
			--	if exists(select column02 from putable018 where COLUMN07=@custid and COLUMN05=@Num  and COLUMN06='Payment'
			--		 and COLUMN03=3000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable018 set COLUMN11=(COLUMN11-@COLUMN06) where COLUMN07=@custid and COLUMN05=@Num  and COLUMN06='Payment'
			--		 and COLUMN03=3000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	if exists(select column02 from putable019 where COLUMN07=@custid and COLUMN05=@Num and COLUMN08=@COLUMN08  
			--	and COLUMN06='PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable019 set COLUMN11=(COLUMN11-@COLUMN06) where COLUMN07=@custid and COLUMN05=@Num and COLUMN08=@COLUMN08  
			--	and COLUMN06='PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	end
				if(@COLUMN17='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='PaymentVoucher')
				begin
				update FITABLE022 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='AdvancePayment')
				begin
				update FITABLE020 set COLUMN18=(cast(isnull(COLUMN18,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='DebitMemo')
				begin
				update PUTABLE001 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
END

 

IF @Direction = 'Select'

BEGIN

select * from SATABLE012

END 

 

IF @Direction = 'Update'

BEGIN
--EMPHCS1208	CUSTOMER PAYMENT EDIT NOT WORKING FOR ACCOUNT TABLES BY SRINIVAS 25/09/2015
DECLARE @ACC INT,@PRAMT DECIMAL(18,2)
set @PRAMT =(select COLUMN06 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @COLUMN08 =(select COLUMN08 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @COLUMN18 =(select iif(isnull(COLUMN18,0)='',0,isnull(COLUMN18,0))COLUMN18 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @PAMT =(select isnull(COLUMN06,0) from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @PDUEAMT =(select isnull(COLUMN05,0) from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @PCRAMT  =(select isnull(COLUMN14,0) from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @PDISAMT =(select isnull(COLUMN13,0) from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @PADVAMT =(select isnull(COLUMN09,0) from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @COLUMN15 =(select COLUMN15 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @DAMT =(select COLUMN09 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @TAMT =(select COLUMN13 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @Totamt =(select COLUMN14 from  SATABLE012 Where COLUMN02=@COLUMN02 );
set @ACC =(select COLUMN08 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @DATE =(select COLUMN19 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @Num =(select COLUMN04 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @refID =(select COLUMN04 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @Payment# =(select COLUMN04 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN10 =(select COLUMN15 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN11 =(select COLUMN16 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN12 =(select COLUMN17 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @Project =(select COLUMN22 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @CustID = (select COLUMN05 from SATABLE011 where column01=@COLUMN08);
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null ) THEN (select COLUMN14 from SATABLE011 
   where COLUMN01=@COLUMN08) else @COLUMNA02  END )
if(@COLUMN05!= '' and @COLUMN05 is not null)
begin
--set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))+((@PAMT-cast(@COLUMN06 as DECIMAL(18,2)))+(@PCRAMT-cast(isnull(@COLUMN14,0 )as DECIMAL(18,2)))+(@PDISAMT-cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+(@PADVAMT-cast(isnull(@COLUMN09,0 )as DECIMAL(18,2)))));
declare @Pay decimal(18,2)
set @pay=(select (cast(sum(isnull(COLUMN06,0)) as DECIMAL(18,2))+cast(sum(isnull(COLUMN13,0 ))as DECIMAL(18,2))+cast(sum(isnull(COLUMN14,0 ))as DECIMAL(18,2))+cast(sum(isnull(COLUMN09,0 ))as DECIMAL(18,2))) from SATABLE012 where COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0)
if(@COLUMN04=@COLUMN05)
begin
set @COLUMN05=0.00
end
end
--set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))+(@PRAMT-cast(@COLUMN06 as DECIMAL(18,2)))+(@TAMT-cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))
--			  +(@Totamt-cast(isnull(@COLUMN14,0 )as DECIMAL(18,2)))+(@DAMT-cast(isnull(@COLUMN09,0 )as DECIMAL(18,2))));

--EMPHCS1410	Logs Creation by srinivas
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Line Values are Intiated for SATABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMNA01,'')+','+isnull(@COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+ 											  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+'')
   )

UPDATE SATABLE012 SET
	COLUMN02=@COLUMN02,   COLUMN03=@COLUMN03,   COLUMN04=@COLUMN04,  COLUMN05=@COLUMN05,  COLUMN06=@COLUMN06,  COLUMN07=@COLUMN07,					
	COLUMN08=@COLUMN08,   COLUMN09=@COLUMN09,   COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11,  COLUMN12=@COLUMN12,  COLUMN13=@COLUMN13,
	 ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN for ADVID)
	COLUMN14=@COLUMN14,   COLUMN15=@COLUMN15,  COLUMN20=@COLUMN20, COLUMNA01=@COLUMNA01,				
	COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03, COLUMNA04=@COLUMNA04,COLUMNA05=@COLUMNA05,COLUMNA07=@COLUMNA07,				
	COLUMNA08=@COLUMNA08, COLUMNA09=@COLUMNA09, COLUMNA10=@COLUMNA10,COLUMNA11=@COLUMNA11,COLUMNA12=@COLUMNA12,COLUMNA13=@COLUMNA13,				
	COLUMNB01=@COLUMNB01, COLUMNB02=@COLUMNB02, COLUMNB03=@COLUMNB03,COLUMNB04=@COLUMNB04,COLUMNB05=@COLUMNB05,COLUMNB06=@COLUMNB06,				
	COLUMNB07=@COLUMNB07, COLUMNB08=@COLUMNB08,COLUMNB09=@COLUMNB09,COLUMNB10=@COLUMNB10,
	COLUMNB11=@COLUMNB11, COLUMNB12=@COLUMNB12, COLUMND01=@COLUMND01,COLUMND02=@COLUMND02,COLUMND03=@COLUMND03,COLUMND04=@COLUMND04,
	COLUMND05=@COLUMND05, COLUMND06=@COLUMND06, COLUMND07=@COLUMND07,COLUMND08=@COLUMND08,COLUMND09=@COLUMND09,COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   --EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Line Values are Created in SATABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
    )
	
set @FormID=(select isnull(max(COLUMN03),'0') from SATABLE009 where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03)
set @number1 = (select MAX(COLUMN01) from  SATABLE012 where COLUMN02=@COLUMN02);
set @Creditcashsale = (select isnull(COLUMN04,0) from CONTABLE031 where COLUMN03='Allow credit for cash sale' and isnull(COLUMN04,0)=1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
if(cast(isnull(@COLUMN06,0) as decimal(18,2))>0 and ((@FormID!='1532' and @FormID!='1604') or (@Creditcashsale=1 or @Creditcashsale='True' or @Creditcashsale='1')))
BEGIN
if not exists(select COLUMN05  from PUTABLE018 where COLUMN05=@Num AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and COLUMN07=@COLUMN20 and COLUMN17=@COLUMN08)
BEGIN
set @tmpnewID=(select MAX(COLUMN02) from PUTABLE018);
set @memo = (select COLUMN10 from SATABLE011 where COLUMN01=@COLUMN08);
if(@tmpnewID>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1;
end
else
begin
set @newID=1000;
end
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Receivable Values are Intiated for PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are ') 			
   )
   
set @COLUMN20=(iif((cast(@COLUMN20 as nvarchar(250))='' or isnull(@COLUMN20,0)='0'),@CustID,@COLUMN20))

   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @DATE,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN20,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN06,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,column11,column12,COLUMN13,COLUMNA02,COLUMNA03,COLUMN18,COLUMN16,COLUMN17)
			    values(@newID,3000,@DATE,@Num,'Payment',@COLUMN20,@memo,@COLUMN06,NULL,'Paid',@COLUMNA02,@COLUMNA03,@Project,@COLUMN03,@COLUMN08) 
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Receivable Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
			
	set @COLUMN20=(iif((cast(@COLUMN20 as nvarchar(250))='' or isnull(@COLUMN20,0)='0'),@CustID,@COLUMN20))

  
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @DATE,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN20,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN06,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0', @COLUMNA07  = @COLUMNA06
	
			
			set @paidamt=(select cast(isnull(COLUMN11,0) as decimal(18,2)) from PUTABLE018 where COLUMN05=@Num and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN07=@COLUMN20 and COLUMN17=@COLUMN08)
            update PUTABLE018 set --COLUMN04=@COLUMN19,
			COLUMN11=(cast(isnull(@paidamt,0) as DECIMAL(18,2))+cast(isnull(@COLUMN06,0) as DECIMAL(18,2))), COLUMN07=@COLUMN20--, COLUMN18=@COLUMN22
			 where COLUMN05=@Num and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN17=@COLUMN08
end
END
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
if(@COLUMN05= '' or @COLUMN05 is null)
begin
set @cdueamt=(select min(isnull(COLUMN05,0)) from  SATABLE012 Where COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0)
set @COLUMN05=((SELECT CASE WHEN isnull(@COLUMN05,0)=0 THEN ( cast(@COLUMN04 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))+cast(isnull(@COLUMN09,0 )as DECIMAL(18,2)))
 ELSE (cast(@PDUEAMT as DECIMAL(18,2))-(@PAMT-cast(@COLUMN06 as DECIMAL(18,2))+(@PCRAMT-cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))))+(@PDISAMT-cast(isnull(@COLUMN13,0 )as DECIMAL(18,2)))+(@PADVAMT-cast(isnull(@COLUMN09,0 )as DECIMAL(18,2)))) END));
update SATABLE012 set COLUMN05=@COLUMN05 Where COLUMN02=@COLUMN02
end
   --EMPHCS1208	CUSTOMER PAYMENT EDIT NOT WORKING FOR ACCOUNT TABLES BY SRINIVAS 25/09/2015
SET @number=(SELECT COLUMN01 FROM SATABLE012 WHERE COLUMN02=@COLUMN02)
if(@COLUMN18=0)
begin
SET @Commission=(SELECT ISNULL(COLUMN15,0) FROM FITABLE027 WHERE COLUMN05=@number AND COLUMN04='COMMISSION PAYMENT' AND COLUMN09=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
SET @Tamt=(SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
SET @PAMT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
SET @DAMT=(SELECT isnull(SUM(COLUMN13),0) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
SET @PAMT=(cast(@PAMT as DECIMAL(18,2))+cast(isnull(@DAMT,0) as DECIMAL(18,2))+cast(isnull(@COLUMN14,0) as DECIMAL(18,2))+cast(isnull(@COLUMN09,0) as DECIMAL(18,2)))
set @Result='Partially Paid'
if(@Tamt=@PAMT)
begin
set @Result='Paid'
end
	--UPDATE FITABLE001 SET COLUMN10=(SELECT COLUMN10 FROM FITABLE001 WHERE COLUMN02=@ACC)+cast(@COLUMN06 as decimal(18,2))WHERE COLUMN02=@ACC
	--update PUTABLE018 set COLUMNA13=0, COLUMN04=@DATE,COLUMN11=@COLUMN06,COLUMN12=@COLUMN05,COLUMN05=@NUM  WHERE COLUMN05=@NUM AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  
	--SET @Tamt=(SELECT COLUMN12 FROM PUTABLE019 where COLUMN08=@COLUMN08 AND COLUMN05=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
	--update PUTABLE019 set COLUMNA13=0, COLUMN04=@DATE,COLUMN11=@COLUMN06,COLUMN12=@Tamt-(@PRAMT-cast(@COLUMN06 as DECIMAL(18,2))) where COLUMN08=@COLUMN08 AND COLUMN05=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	IF(ISNULL(@Commission,0)>0)
	BEGIN
		set @tax=(select ISNULL(COLUMN24,0) from SATABLE009 where COLUMN02=@COLUMN03)
		set @Count=(select count(COLUMN03) from SATABLE012 where COLUMN03=@COLUMN03)
		IF(@Count=1)
		BEGIN
		SET @TAMT=(cast(@COLUMN06 as decimal(18,2))-@tax-cast(@COLUMN13 as decimal(18,2)))
		SET @Totamt=(cast(@COLUMN06 as decimal(18,2))-@tax-cast(@COLUMN13 as decimal(18,2)))*(@Commission)/100
		END
		ELSE
		BEGIN
		SET @TAMT=(cast(@COLUMN06 as decimal(18,2))-cast(@COLUMN13 as decimal(18,2)))
		SET @Totamt=(cast(@COLUMN06 as decimal(18,2))-cast(@COLUMN13 as decimal(18,2)))*(@Commission)/100
		END
	UPDATE FITABLE027 SET COLUMNA13=0,COLUMN10=@TAMT,COLUMN13=@TAMT,COLUMN12=@Totamt,COLUMN03=@DATE WHERE  COLUMN05=@number AND COLUMN04='COMMISSION PAYMENT' AND COLUMN09=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	END
	

SET @DAMT=isnull((SELECT sum(COLUMN13) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
SET @PIQty=isnull((SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
SET @PBQty=isnull((SELECT @PIQty-((sum(cast(COLUMN06 as DECIMAL(18,2)))+sum(cast(isnull(COLUMN13,0 )as DECIMAL(18,2))))+sum(cast(isnull(COLUMN14,0 )as DECIMAL(18,2)))+sum(cast(isnull(COLUMN09,0 )as DECIMAL(18,2))))  FROM SATABLE012 WHERE COLUMN02=@COLUMN02 and isnull(COLUMNA13,0)=0),0)
IF(@PBQty>0)
begin
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=32) where COLUMN02=@COLUMN03
end
else IF(@PBQty=0.00)
begin
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=34) where COLUMN02=@COLUMN03
end
end

	set @CustID = (select COLUMN05 from SATABLE011 where COLUMN01=@COLUMN08);
    set @memo = (select COLUMN10 from SATABLE011 where COLUMN01=@COLUMN08);
	IF(CAST(ISNULL(@COLUMN13,0) AS DECIMAL(18,2))>0)
	BEGIN
	IF EXISTS (select column02 from FITABLE036 where COLUMN05=CAST(@number AS NVARCHAR(250) ) AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='PAYMENT')
	BEGIN
	UPDATE FITABLE036 SET COLUMNA13=0,COLUMN07=@COLUMN13,COLUMN04=@DATE WHERE COLUMN05=CAST(@number AS NVARCHAR(250) ) AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='PAYMENT'
	END
	ELSE
	BEGIN
	EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @date,   @COLUMN08 = '22335',  @COLUMN09 = @CustID,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN13,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @PNewID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
		insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
		values(@PNewID,@date,'PAYMENT',@Payment#,@CustID,@number,1049,isnull(@COLUMN13,0),@COLUMNA02 , @COLUMNA03,0,@Project)  
	END
	END

	IF EXISTS (select column02 from FITABLE036 where COLUMN05=CAST(@number AS NVARCHAR(250) ) AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='PAYMENT')
	BEGIN
	UPDATE FITABLE036 SET COLUMNA13=0,COLUMN07=@COLUMN13,COLUMN04=@DATE WHERE COLUMN05=CAST(@number AS NVARCHAR(250) ) AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='PAYMENT'
	END
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
		SET @COLUMN17=(SELECT COLUMN17 FROM SATABLE012 WHERE COLUMN02 =@COLUMN02)
		SET @COLUMN16=(SELECT COLUMN16 FROM SATABLE012 WHERE COLUMN02 =@COLUMN02)
		SET @COLUMN18=(SELECT COLUMN18 FROM SATABLE012 WHERE COLUMN02 =@COLUMN02)
		SET @COLUMN14=(SELECT COLUMN14 FROM SATABLE012 WHERE COLUMN02 =@COLUMN02)
		if(@COLUMN14!='0' and @COLUMN14!='0.00' and @COLUMN14!='' )
	    begin

		set @VochrAmt=(@COLUMN14)
		set @MainString=(@COLUMN16)
		set @cnt= 0;set @vochercnt= 1;
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='C')
		begin
		set @VTransno=(select column04 from satable005 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN15,0)-isnull(COLUMND05,0))COLUMN15 from satable005 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from satable005 where column02=@Vocherid)
		update satable005 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from satable005 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update satable005 set COLUMND05=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update satable005 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN15,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN15 from satable005 where column02=@Vocherid)=0)
		begin
		update satable005 set COLUMN16='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		end
		else if(@Vochertype='R')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN16,0)-isnull(COLUMND05,0))COLUMN16 from FITABLE024 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE024 where column02=@Vocherid)
		update FITABLE023 set COLUMN13='OPEN' where column01 in(select COLUMN09 from FITABLE024 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE024 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE024 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN16,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN16 from FITABLE024 where column02=@Vocherid)=0)
		begin
		update FITABLE023 set COLUMN13='CLOSE' where column01 in (select COLUMN09 from FITABLE024 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE023 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMN19,0) from FITABLE023 where column02=@Vocherid)
		update FITABLE023 set COLUMN13='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE023 set COLUMN19=(@PrevAmt-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		exec usp_SAL_TP_AdvanceCalculations  @refID,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@VochrAmt
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE023 set COLUMN19=0 where column02=@Vocherid and columna03=@columna03
		exec usp_SAL_TP_AdvanceCalculations  @refID,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@PrevAmt
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		if((select CAST(isnull(COLUMN19,0) AS DECIMAL(18,2)) from FITABLE023 where column02=@Vocherid)=0)
		begin
		update FITABLE023 set COLUMN13='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		--declare @TAXRATE decimal(18,2)=null, @TaxAG nvarchar(250)=null, @Taxregid nvarchar(250)=null,@Vid int,@COLUMN24 DECIMAL(18,2)
		set @Tax= ISNULL((SELECT COLUMN12 FROM FITABLE023 WHERE COLUMN02=@Vocherid),1000)
		set @COLUMN24= ISNULL((SELECT CAST(COLUMN24 AS DECIMAL(18,2)) FROM FITABLE023 WHERE COLUMN02=@Vocherid),0)
		set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
		set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
		set @Taxregid= (SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE026)+1
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TaxAG in(23585,23586,23584,23583,23582))
		BEGIN
			set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
			--declare @TaxColumn17 nvarchar(250)
			set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
			EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN08,  @COLUMN06 = @number1,
		@COLUMN07 = @date,   @COLUMN08 = '22335',  @COLUMN09 = @CustID,    @COLUMN10 = '22383', @COLUMN11 = '1143',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN24,       @COLUMN15 = 0,         @COLUMN16 = @COLUMN24, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,  COLUMN12,  COLUMN14,  COLUMN16,COLUMN17,  COLUMNA02, COLUMNA03)
			values(@Vid,@date,'PAYMENT',@number1,@CustID,@memo,1143,@Num,@COLUMN24,@COLUMN24,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03)
		
			--set @Vid=((select isnull(max(column02),999) from FITABLE034)+1)
			--set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
			--Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,  COLUMN10,  COLUMNA02, COLUMNA03)
			--				values(@Vid,'PAYMENT',@date,@memo,@CustID,@COLUMN24,@Num,1143,@COLUMNA02, @COLUMNA03)
		END
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN05,0)-isnull(COLUMND05,0))COLUMN05 from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END

		if(@COLUMN14='' or @COLUMN14 is null) set @COLUMN14=0;
		SET @COLUMN06=(SELECT COLUMN06 FROM SATABLE012 WHERE COLUMN02 =@COLUMN02)
			--if(@COLUMN17='Journal' or @COLUMN17='DebitMemo' or @COLUMN17='PaymentVoucher' or @COLUMN17='AdvancePayment')
			--	begin
			--	if exists(select column02 from putable018 where COLUMN07=@custid and COLUMN05=@Num  and COLUMN06='Payment'
			--		 and COLUMN03=3000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable018 set COLUMN11=(COLUMN11-@COLUMN06) where COLUMN07=@custid and COLUMN05=@Num  and COLUMN06='Payment'
			--		 and COLUMN03=3000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	if exists(select column02 from putable019 where COLUMN07=@custid and COLUMN05=@Num and COLUMN08=@COLUMN08  
			--	and COLUMN06='PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable019 set COLUMN11=(COLUMN11-@COLUMN06) where COLUMN07=@custid and COLUMN05=@Num and COLUMN08=@COLUMN08  
			--	and COLUMN06='PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	end
				if(@COLUMN17='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='PaymentVoucher')
				begin
				update FITABLE022 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='AdvancePayment')
				begin
				update FITABLE020 set COLUMN18=(cast(isnull(COLUMN18,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='DebitMemo')
				begin
				update PUTABLE001 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
	
END

 

else IF @Direction = 'Delete'

BEGIN

UPDATE SATABLE012 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02

END
  --EMPHCS1410	Logs Creation by srinivas
  exec [CheckDirectory] @tempSTR,'usp_SAL_TP_SATABLE012.txt',0
end try
begin catch

--EMPHCS1410	Logs Creation by srinivas
	 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_SATABLE012.txt',0


if not exists(select column01 from SATABLE012 where column08=@COLUMN08)
					begin
					delete SATABLE011 where column01=@COLUMN08
					return 0
					end
return 0
end catch

end









GO
