USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_FormBuildInfo]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_FormBuildInfo](@FormId nvarchar(250)= null,@TableName nvarchar(250)= null,
@OPUnit nvarchar(250)= null,@AcOwner nvarchar(250)= null,@SearchData nvarchar(max)= null,@FilterByStatus nvarchar(250)= '',
@whereStr nvarchar(max)='',@iwhereStr nvarchar(max)='',@swhereStr nvarchar(max)='',@dateF nvarchar(250)= null,@SortBy nvarchar(250)='',@Type nvarchar(250)='')
AS
BEGIN
DECLARE @FRDATE NVARCHAR(25),@TODATE NVARCHAR(25)
SELECT @FRDATE=ISNULL(MAX(COLUMN04),'1/1/2010'),@TODATE=ISNULL(MAX(COLUMN05),'1/1/2100') FROM FITABLE048 WHERE COLUMNA03 = @AcOwner AND COLUMN09 =1
set @OPUnit=(isnull(@OPUnit,0))
set @FilterByStatus=(isnull(@FilterByStatus,''))
set @SearchData=(replace (@SearchData, '''', ''''''))
if (@SortBy='Newly Modified')
begin
set @swhereStr= ' ORDER BY p.COLUMNA07 desc'
end
else if (@SortBy='Newly Created')
begin
set @swhereStr= ' ORDER BY p.COLUMNA06 desc'
end
else
begin
set @SortBy=''
set @swhereStr= ' ORDER BY p.COLUMNA06 desc'
end
IF(@FormId='1261')
BEGIN
if @FilterByStatus!=''
begin
set @whereStr= ' and P.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',p.COLUMN50,'' '',g.COLUMN04,'' '',b.COLUMN04,'' '',p.COLUMN45,'' '',p.COLUMN17,'' '',M32.COLUMN04) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Name],'''' AS [Description],'''' AS [Group],'''' AS [Brand],'''' AS [Purchase Price],'''' AS [Sales Price],'''' AS [Sales MRP],'''' AS [HSN/SAC] ,'''' AS [Status]')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,replace(ltrim(rtrim(replace(REPLACE(P.COLUMN04, ''"'', ''''), char(9), ''    ''))),''    '', char(9)) AS [Name],REPLACE(LTRIM(RTRIM(REPLACE(P.COLUMN50, ''"'', ''''))), CHAR(13) + CHAR(10), '''') AS [Description],g.[COLUMN04] AS [Group],
b.[COLUMN04] AS [Brand],iif(isnull(r.column04,0)>0,r.column04,
p.[COLUMN17 ]) AS [Purchase Price],ISNULL(rs.column04,0) AS [Sales Price],ISNULL(rs.column05,0) AS [Sales MRP],
M32.COLUMN04 [HSN/SAC],p.COLUMN47 as [Status] From MATABLE007 p 
left join  MATABLE024 r on p.COLUMNA03='''+@AcOwner+''' and r.COLUMN07=p.COLUMN02 and r.column06=''Purchase'' and  (r.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or isnull(r.COLUMN03,0)=0) and p.columna03=r.columna03 and isnull(r.columna13,0)=0 
left join  MATABLE024 rs on rs.COLUMN07=p.COLUMN02 and rs.column06=''Sales'' and  (rs.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or isnull(rs.COLUMN03,0)=0) and p.columna03=rs.columna03 and isnull(rs.columna13,0)=0 
--left join  MATABLE008 d on d.COLUMN02=p.COLUMN05 and d.COLUMNA03=p.COLUMNA03 and isnull(d.COLUMNA13,0)=0
left join  MATABLE004 g on g.COLUMN02=p.COLUMN11 and g.COLUMNA03=p.COLUMNA03 and isnull(g.COLUMNA13,0)=0 
left join  MATABLE005 b on b.COLUMN02=p.COLUMN10  and (b.COLUMNA03=p.COLUMNA03 or isnull(b.COLUMNA03,0)=0) and isnull(b.COLUMNA13,0)=0
left join MATABLE032 M32 ON M32.COLUMN02 = p.COLUMN75 and (M32.COLUMNA03=p.COLUMNA03 or isnull(M32.COLUMNA03,0)=0) and isnull(M32.COLUMNA13,0)=0
where p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 '+@whereStr+'
'+@swhereStr+'')
--select column75 from matable007 where IsNumeric(column75) != 1 and  column75 is not null and  column75! = ''

END
END
ELSE IF(@FormId='1277')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN13 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN13 not in (select COLUMN04 from CONTABLE025 where column02=34)'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN08],''' + @dateF + '''),'' '',s.COLUMN05,'' '',f.COLUMN04,'' '',p.COLUMN09,'' '',p.COLUMN12,'' '',p.COLUMN20,'' '',p.COLUMN13) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN08 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN08, 101) desc, p.COLUMN04 desc,convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS Invoice#,'''' AS Date,'''' AS Customer,'''' AS [Reference Order#],'''' AS Reference#,'''' AS Memo,
'''' [Total Amount],'''' as [Returns/Credit],'''' [Amount Received],'''' [Due Amount],'''' as [Days Overdue],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Invoice#,FORMAT(p.[COLUMN08],''' + @dateF + ''') AS Date,
REPLACE(s.COLUMN05, ''"'', '''') AS Customer,replace(ltrim(rtrim(replace(REPLACE(f.COLUMN04, ''"'', ''''), char(9), ''    ''))),''    '', char(9)) AS [Reference Order#],replace(ltrim(rtrim(replace(REPLACE(P.COLUMN09, ''"'', ''''), char(9), ''    ''))),''    '', char(9)) AS Reference#,replace(ltrim(rtrim(replace(REPLACE(P.COLUMN12, ''"'', ''''), char(9), ''    ''))),''    '', char(9)) AS Memo,
p.COLUMN20 [Total Amount],sum(isnull(f1.COLUMN12,0))+sum(isnull(f1.COLUMN32,0)) as [Returns/Credit],
(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0))) [Amount Received],
(p.COLUMN20-(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0)))) [Due Amount],
iif((p.COLUMN20-(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0))))=0,0,DATEDIFF(day,p.COLUMN10,GETDATE())) as [Days Overdue],p.COLUMN13 Status,ma.[COLUMN09] AS [Created By] From SATABLE009 p 
left join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
--left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 
left join  SATABLE005 f on f.COLUMN02=p.COLUMN06 and f.COLUMNA02=p.COLUMNA02 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0 
left join  SATABLE005 f1 on f1.COLUMN11=p.COLUMN04 and f1.COLUMNa03=p.COLUMNa03 and f1.COLUMNa02=p.COLUMNa02  and isnull(f1.COLUMNA13,0)=0 and f1.COLUMN15>0 
left join  SATABLE012 s12 on s12.COLUMN03=p.column02 and s12.COLUMNA03=p.COLUMNA03 and isnull(s12.COLUMNA13,0)=0 
left join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (ma.COLUMNA02=p.COLUMNA02 or ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0  
where p.COLUMN03 = 1277 and p.COLUMN19 = 1002 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,p.COLUMN13,p.COLUMN20,ma.COLUMN09,p.COLUMN10,p.COLUMNA06,p.COLUMNA07 '+@swhereStr+' ')
END
END
ELSE IF(@FormId='1273')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN13 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN13 not in (select COLUMN04 from CONTABLE025 where column02=12)'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN08],''' + @dateF + '''),'' '',s.COLUMN05,'' '',f.COLUMN04,'' '',p.COLUMN09,'' '',p.COLUMN12,'' '',p.COLUMN14,'' '',p.COLUMN11,'' '',p.COLUMN13,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN08 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN08, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS Bill#,'''' AS Date,'''' AS Vendor,'''' AS [Reference PO#],'''' AS Reference#,'''' AS Memo,'''' [Total Amount],
'''' [Returns/Debit],'''' [Amount Paid],'''' [Due Amount],'''' as [Days Overdue],'''' Status,'''' [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Bill#,(FORMAT(p.[COLUMN08 ],''' + @dateF + ''')) AS Date,
REPLACE(LTRIM(RTRIM(s.COLUMN05)), ''"'', '''') AS Vendor,f.[COLUMN04 ] AS [Reference PO#],REPLACE(LTRIM(RTRIM(p.COLUMN09)), ''"'', '''') AS Reference#,REPLACE(LTRIM(RTRIM(p.COLUMN12)), ''"'', '''') AS Memo,
p.COLUMN14 [Total Amount],sum(isnull(f1.COLUMN15,0)) [Returns/Debit],
(sum(isnull(p15.COLUMN16,0))+sum(isnull(p15.COLUMN14,0))+sum(isnull(p15.COLUMN06,0))) [Amount Paid],
(p.COLUMN14-(sum(isnull(p15.COLUMN16,0))+sum(isnull(p15.COLUMN14,0))+sum(isnull(p15.COLUMN06,0)))) [Due Amount],
DATEDIFF(day,p.COLUMN11,GETDATE()) as [Days Overdue],p.COLUMN13 Status,REPLACE(LTRIM(RTRIM(ma.COLUMN09)), ''"'', '''') [Created By] From PUTABLE005 p 
left join  SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
left join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0 
left join  PUTABLE001 f1 on f1.COLUMN11=p.COLUMN04 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and f1.COLUMN15>0.00 and isnull(f1.COLUMNA13,0)=0 
left join PUTABLE015 p15 on p15.COLUMN03=p.COLUMN02 and isnull(p15.COLUMNA13,0)=0 and p15.COLUMNA03=p.COLUMNA03 
left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0 
where p.COLUMN20 = 1001 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,p.COLUMN14,p.COLUMN11,p.COLUMN13,ma.COLUMN09,p.COLUMNA06,p.COLUMNA07 '+@swhereStr+' ')
END
END
ELSE IF(@FormId='1330')
BEGIN
if (@FilterByStatus='FULLY REFUNDED')
set @FilterByStatus=('FULLY CREDITED')
else if (@FilterByStatus='PARTIALLY REFUNDED')
set @FilterByStatus=('PARTIALLY CREDITED')
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN16 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN06 ],''' + @dateF + '''),'' '',s1.COLUMN05,'' '',s.COLUMN05,'' '',sr.COLUMN04,'' '',s9.COLUMN04,'' '',p.COLUMN11,'' '',p.COLUMN09,'' '',p.COLUMN15,'' '',p.COLUMN16,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN06 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Credit Memo#],'''' AS [Date],'''' AS Customer,'''' AS [Reference Order#],'''' [Reference Invoice#],
'''' Reference#,'''' Memo,'''' AS [Total Amount],'''' AS [Status],'''' [Created By]')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Credit Memo#],FORMAT(p.[COLUMN06 ],''' + @dateF + ''') AS [Date],
(case when p.COLUMN51=''22305'' then s1.COLUMN05 else REPLACE(s.COLUMN05, ''"'', '''') end) AS Customer,sr.COLUMN04 AS [Reference Order#],
s9.COLUMN04 [Reference Invoice#],p.COLUMN11 Reference#,p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount],
 p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From SATABLE005 p 
 left join  PUTABLE003 rr on rr.COLUMN02=p.COLUMN33 and rr.COLUMNA03=p.COLUMNA03 and isnull(rr.COLUMNA13,0)=0 
 left join  PUTABLE001 sr on sr.COLUMN02=rr.COLUMN06 and sr.COLUMNA03=rr.COLUMNA03 and isnull(sr.COLUMNA13,0)=0 
 left join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
 left join  SATABLE001 s1 on s1.COLUMN02=p.COLUMN05 and s1.COLUMNA03=p.COLUMNA03 and isnull(s1.COLUMNA13,0)=0 
 --left join PUTABLE003 o on o.COLUMN02=p.COLUMN33 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 left join  MATABLE009 e on e.COLUMN02=p.COLUMN29 
 left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) 
 left join SATABLE009 s9 on s9.COLUMN02=p.COLUMN49 and s9.COLUMNA02=p.COLUMNA02 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0
where p.COLUMN29 = 1003 and p.COLUMN03=1330 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+''+@swhereStr+'')
END
END
ELSE IF(@FormId='1355')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN16 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN06 ],''' + @dateF + '''),'' '',s2.COLUMN05,'' '',s.COLUMN05,'' '',p14.COLUMN04,'' '',p.COLUMN11,'' '',p.COLUMN09,'' '',p.COLUMN15,'' '',p.COLUMN16,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN06 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN06, 101) desc, cast(p.COLUMN02 as int) desc'

if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Debit Memo#],'''' AS [Date],'''' AS Vendor,'''' [Reference PO#],'''' AS [Reference Bill#],'''' AS [Reference#],
'''' Memo,'''' AS [Total Amount],'''' AS [Status],'''' [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Debit Memo#],FORMAT(p.[COLUMN06 ],''' + @dateF + ''') AS [Date],
(case when p.COLUMN50=''22335'' then REPLACE(s2.COLUMN05, ''"'', '''') else REPLACE(s.COLUMN05, ''"'', '''') end) AS Vendor,
(select COLUMN11 from SATABLE005 where COLUMN02=(select COLUMN06 from SATABLE007 where COLUMN02=p.COLUMN33)) [Reference PO#],
p14.[COLUMN04] AS [Reference Bill#],p.[COLUMN11] AS [Reference#],p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount],
p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p 
left join  SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
left join  SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0 
--left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 
left join PUTABLE014 p14 on p14.COLUMN02=p.COLUMN48 and p14.COLUMNA02=p.COLUMNA02 and p14.COLUMNA03=p.COLUMNA03 and isnull(p14.COLUMNA13,0)=0 
left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0 
where p.COLUMN29 = 1003 and p.COLUMN03=1355 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+''+@swhereStr+'')
END
END
ELSE IF(@FormId='1357')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05 ],''' + @dateF + '''),'' '',p.COLUMN14,'' '',p.COLUMN15,'' '',p.COLUMN07,'' '',e.COLUMN04,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN05 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Adjustment#],'''' AS [Date],'''' Reference#,'''' Memo,'''' AS [Total Value],'''' AS [Account], '''' AS [Status],'''' AS [Created By]')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Adjustment#],FORMAT(p.[COLUMN05 ],''' + @dateF + ''') AS [Date],
p.COLUMN14 Reference#,p.COLUMN15 Memo,p.[COLUMN07 ] AS [Total Value],e.[COLUMN04] AS [Account], '''' AS [Status],
ma.[COLUMN09 ] AS [Created By] From FITABLE014 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 
left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN12 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN06 and e.COLUMNA03=p.COLUMNA03 
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)
where p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+''+@swhereStr+'')
END
END
ELSE IF(@FormId='1515')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05 ],''' + @dateF + '''),'' '',l.COLUMN04,'' '',m.COLUMN04,'' '',o.COLUMN03) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Lot No],'''' AS [Expiry Date], '''' AS Location,''''  AS [Item],''''  AS [Operating Unit]')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Lot No],FORMAT(p.[COLUMN05 ],''' + @dateF + ''') AS [Expiry Date],
l.[COLUMN04] AS [Location],m.[COLUMN04] AS [Item],o.[COLUMN03] AS ''Operating Unit''  From FITABLE043 p 
left join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left join  CONTABLE030 l on l.COLUMN02=p.COLUMN08 and 
l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0 
left outer join  MATABLE007 m on m.COLUMN02=p.COLUMN09 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
where p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+''+@swhereStr+'')
END
END
ELSE IF(@FormId='1623')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(q.Item,'' '',q.[Desc],'' '',q.[UPC/BarCode],'' '',q.[Units],'' '',q.Price) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (q.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or q.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [AliasID],'''' AS [Item], '''' AS [Desc],''''  AS [UPC/BarCode],''''  AS [Units],''''  AS [Price]')
end
else
begin
exec ('SELECT q.ID,q.[AliasID],q.Item,q.[Desc],q.[UPC/BarCode],q.[Units],q.Price FROM((
Select p.COLUMN02 ID,0 AliasID,p.COLUMN04 Item,p.COLUMN50 [Desc],p.COLUMN06 [UPC/BarCode],m.COLUMN04 Units,s.COLUMN04 Price,p.COLUMNA06,p.COLUMNA02,p.COLUMNA03,p.COLUMNA13 From MATABLE007 p 
left join MATABLE002 m on m.COLUMN02=p.COLUMN63 and m.COLUMNA03=p.COLUMNA03 
left join MATABLE024 s on s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 and (isnull(s.COLUMN03,0)=p.COLUMNA02  or isnull(s.COLUMN03,0)=0) 
and s.COLUMN07=p.COLUMN02 and s.COLUMN06=''sales'' where p.COLUMN06!='''') 
union all (Select p.COLUMN02 ID,ia.COLUMN02 AliasID,p.COLUMN04 Item,p.COLUMN50 [Desc],ia.COLUMN04 [UPC/BarCode],im.COLUMN04 Units,ia.COLUMN09 Price,ia.COLUMNA06,ia.COLUMNA02,p.COLUMNA03,p.COLUMNA13 From MATABLE007 p 
inner join MATABLE021 ia on ia.COLUMN05=p.COLUMN02 and  ia.COLUMNA03=p.COLUMNA03 and isnull(ia.COLUMNA13,0)=0 
left join MATABLE002 im on im.COLUMN02=ia.COLUMN06 and im.COLUMNA03=ia.COLUMNA03 
where ia.COLUMN04!=''''))q where q.COLUMNA03='''+@AcOwner+''' and isnull(q.COLUMNA13,0)=0 '+@whereStr+' ORDER BY q.COLUMNA06 desc')
END
END
ELSE IF(@FormId='1252')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',p.COLUMN05,'' '',sc.COLUMN04,'' '',c.COLUMN04,'' '',b.COLUMN04,'' '',p.COLUMN11,'' '',p.COLUMN12,'' '',p.COLUMN23) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Vendor#],'''' AS Name, '''' AS Type,''''  AS [Category],''''  AS [Sub Category], '''' as [Email], ''''  as Phone,'''' [TIN#]')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID, p.[COLUMN04 ] AS ''Vendor#'',p.[COLUMN05] AS Name,sc.[COLUMN04 ] AS Type,c.[COLUMN04 ] AS Category,
b.[COLUMN04 ] AS ''Sub Category'',p.[COLUMN11 ] as Email,p.[COLUMN12 ] as Phone, p.[COLUMN23 ] as ''TIN#'' From SATABLE001 p 
left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN07  left outer join  MATABLE002 sc on sc.COLUMN02=p.COLUMN22 
left outer join  MATABLE002 b on sc.COLUMN02=p.COLUMN08 
where p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+''+@swhereStr+'')
END
END
ELSE IF(@FormId='1265')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',p.COLUMN05,'' '',c.COLUMN04,'' '',sc.COLUMN04,'' '',p.COLUMN10,'' '',p.COLUMN11,'' '',p.COLUMN21,'' '',e.COLUMN06,'' '',p.COLUMN23,'' '',st.COLUMN04,'' '',p.COLUMN25) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Customer#],'''' AS Name, '''' AS Category,''''  AS [Sub Category], '''' as [Email], ''''  as Phone,
'''' [TIN#],'''' AS [Sales Rep],'''' AS [Commission%],''''  AS [Type],''''  AS [Status]')
end
else
begin
exec ('
SELECT p.COLUMN02  AS ID,REPLACE(P.COLUMN04, ''"'', '''')  AS [Customer#],REPLACE(P.COLUMN05, ''"'', '''') AS Name, c.COLUMN04 AS Category,
sc.COLUMN04  AS [Sub Category], p.COLUMN10  as [Email], p.COLUMN11  as Phone,p.COLUMN21 [TIN#],e.COLUMN06 AS [Sales Rep],
p.COLUMN23 AS [Commission%],st.COLUMN04  AS [Type],p.COLUMN25  as [Status] From SATABLE002 p 
left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN07   left outer join  MATABLE002 sc on sc.COLUMN02=p.COLUMN08 
left outer join  MATABLE010 e on e.COLUMN02=p.COLUMN22 and e.COLUMN30=''True''  
left outer join  MATABLE002 st on sc.COLUMN02=p.COLUMN24
where p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+''+@swhereStr+'')
END
END
ELSE IF(@FormId='1532')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',s.COLUMN05,'' '',FORMAT(p.[COLUMN08 ],''' + @dateF + '''),'' '',p.COLUMN12,'' '',p.COLUMN25,'' '',p.COLUMN24,'' '',p.COLUMN20,'' '',p.COLUMN13) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN08 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN08, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Sales Receipt#],'''' AS Customer,'''' AS Date,'''' AS [Total Qty Sold],
'''' AS [No Of Items Sold],'''' Memo,'''' AS Discount,'''' Tax,'''' AS Total,'''' AS [Status] ')
end
else
begin
exec ('
SELECT p.[COLUMN02] AS ID,p.[COLUMN04 ] AS [Sales Receipt#],REPLACE(s.COLUMN05, ''"'', '''') AS Customer,
FORMAT(p.[COLUMN08 ],''' + @dateF + ''') AS Date,sum(isnull(f.COLUMN10,0)) AS [Total Qty Sold], count(*) AS [No Of Items Sold],
p.[COLUMN12] Memo,(isnull(p.COLUMN25,0)+isnull(p.COLUMN56,0)) AS Discount,p.COLUMN24 Tax,p.[COLUMN20] AS Total,p.[COLUMN13] AS [Status] From SATABLE009 p 
left outer join  SATABLE010 f on f.COLUMN15=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0
left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 
where p.COLUMNA03='''+@AcOwner+''' AND  p.COLUMN03=1532 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
group by p.COLUMN02,p.COLUMN04,s.COLUMN05,p.COLUMN08,p.COLUMN01,p.COLUMN12 ,p.COLUMN25,p.COLUMN56,p.COLUMN24,p.COLUMN20,p.COLUMNA06,p.COLUMNA07,p.[COLUMN13] '+@swhereStr+'')
END
END
ELSE IF(@FormId='1603')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN06],''' + @dateF + '''),'' '',s.COLUMN05,'' '',o.COLUMN04,'' '',p.COLUMN32,'' '',p.COLUMN15) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN06 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Credit Memo#],'''' AS [Date],'''' AS Customer,'''' AS Receipt#,
'''' AS [Tax Amount],'''' AS [Total Amount] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Credit Memo#],FORMAT(p.[COLUMN06],''' + @dateF + ''') AS [Date],
REPLACE(s.COLUMN05, ''"'', '''') AS Customer, o.[COLUMN04 ] AS Receipt#, p.[COLUMN32] AS [Tax Amount],p.[COLUMN15] AS [Total Amount]
From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  
left outer join SATABLE009 o on o.COLUMN02=p.COLUMN33  
where p.COLUMNA03='''+@AcOwner+''' AND  p.COLUMN03=1603 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+'')
END
END
ELSE IF(@FormId='1604')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',s.COLUMN05,'' '',FORMAT(p.[COLUMN08],''' + @dateF + '''),'' '',p.COLUMN25,'' '',p.COLUMN24,'' '',p.COLUMN20,'' '',p.COLUMN13) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN08 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN08, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Sales Receipt#],'''' AS Customer,'''' AS Date,'''' AS [Total Qty Sold],
'''' AS [No Of Items Sold],'''' AS Discount,'''' Tax,'''' AS Total,'''' AS [Status] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Sales Receipt#],REPLACE(s.COLUMN05, ''"'', '''') AS Customer,FORMAT(p.[COLUMN08],''' + @dateF + ''') AS Date,
sum(isnull(f.COLUMN10,0)) AS [Total Qty Sold],count(*) AS [No Of Items Sold],p.[COLUMN25 ] AS Discount,p.COLUMN24 Tax,
p.[COLUMN20] AS Total,p.[COLUMN13] AS [Status] From SATABLE009 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 
left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 left outer join  SATABLE010 f on f.COLUMN15=p.COLUMN01 
where p.COLUMNA03='''+@AcOwner+''' AND  p.COLUMN03=1604 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
group by p.COLUMN02,p.COLUMN04,s.COLUMN05,p.COLUMN08,p.COLUMN01,p.COLUMN25 ,p.COLUMN24,p.COLUMN20,p.COLUMNA06,p.COLUMNA07,p.[COLUMN13]
'+@swhereStr+'')
END
END
ELSE IF(@FormId='1530')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',i.COLUMN04,'' '',d.COLUMN04,'' '',p.COLUMN07,'' '',v.COLUMN05,'' '',p.COLUMN09,'' '',d1.COLUMN04,'' '',o.COLUMN03) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
--if (@SortBy='')
--set @swhereStr=' ORDER BY convert(datetime, p.COLUMN08, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [UPC#],'''' AS Item,'''' AS Units,'''' AS [Quantity],'''' AS [Vendor],'''' AS Price,'''' Type,'''' AS [Operating Unit] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS ''UPC#'',i.[COLUMN04] AS ''Item'',d.COLUMN04 Units,p.COLUMN07 as Quantity,v.[COLUMN05 ] AS ''Vendor'', p.[COLUMN09 ] AS Price,
d1.COLUMN04 as Type, o.[COLUMN03] AS ''Operating Unit'' From MATABLE021 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN11   
left outer join    MATABLE002 d on d.COLUMN02=p.COLUMN06  left outer join  SATABLE001 v on v.COLUMN02=p.COLUMN08 left outer join MATABLE007 i on i.COLUMN02=p.COLUMN05 
left outer join    MATABLE002 d1 on d1.COLUMN02=p.COLUMN10  
where p.COLUMNA03='''+@AcOwner+''' AND  isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
'+@swhereStr+'')
END
END
ELSE IF(@FormId='1274')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN18],''' + @dateF + '''),'' '',r.COLUMN09,'' '',s.COLUMN05,'' '',b.COLUMN04,'' '',p.COLUMN10,'' '',f1.COLUMN04,'' '',fb.COLUMN04,'' '',b.COLUMN14,'' '',ma.COLUMN09,'' '',isnull((isnull(fb.COLUMN06,0)+isnull(fb.COLUMN16,0)+isnull(fb.COLUMN14,0)),0)) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN18 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN18, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Payment#],'''' AS Date,'''' AS Vendor,'''' AS [Reference Bill#],
'''' AS [Memo],'''' AS [Bank Account],'''' [Total Amount],'''' AS [Amount Paid],'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Payment#,FORMAT(p.[COLUMN18 ],''' + @dateF + ''') as Date,
(case when p.COLUMN20=22306 then r.COLUMN09 else s.[COLUMN05 ] end) AS Vendor,b.[COLUMN04 ] AS [Reference Bill#],
p.COLUMN10 Memo,f1.[COLUMN04 ] AS [Bank Account],(case when p.COLUMN20=22306 then fb.COLUMN04 else b.COLUMN14 end) 
[Total Amount],isnull((isnull(fb.COLUMN06,0)+isnull(fb.COLUMN16,0)+isnull(fb.COLUMN14,0)),0) as [Amount Paid],
ma.COLUMN09 [Created By] From PUTABLE014 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
left outer join  MATABLE010 r on r.COLUMN02=p.COLUMN05 and isnull(r.COLUMN30,0)=1 and r.COLUMNA03=p.COLUMNA03 and isnull(r.COLUMNA13,0)=0
left outer join  PUTABLE015 fb on fb.COLUMN08=p.COLUMN01 and fb.COLUMNA03=p.COLUMNA03 and fb.COLUMNA02=p.COLUMNA02 and isnull(fb.COLUMNA13,0)=0 
left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 
left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06  left outer join  PUTABLE005 b on b.COLUMN02=fb.COLUMN03  
left outer join FITABLE001 f1 on f1.COLUMN02=p.COLUMN08 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and 
ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN17 = 1001 and p.COLUMN03=1274 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
'+@swhereStr+'')
END
END
ELSE IF(@FormId='1278')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN19],''' + @dateF + '''),'' '',s.COLUMN05,'' '',f.COLUMN04,'' '',d.COLUMN04,'' '',p.COLUMN10,'' '',c.COLUMN04,'' '',d.COLUMN20,'' '',ma.COLUMN09,'' '',(isnull(fb.COLUMN06,0)+isnull(fb.COLUMN14,0)+isnull(fb.COLUMN13,0)+isnull(fb.COLUMN09,0))) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN19 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN19, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Payment#],'''' AS Date,'''' AS Customer,'''' AS [Reference Order#],
'''' AS [Reference Invoice#],'''' Memo,'''' AS [Bank Account],'''' [Total Amount],'''' AS [Amount Paid],'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Payment#,FORMAT(p.[COLUMN19 ],''' + @dateF + ''') Date,s.[COLUMN05 ] AS Customer,
f.[COLUMN04 ] AS [Reference Order#],d.[COLUMN04 ] AS [Reference Invoice#],p.COLUMN10 Memo,c.[COLUMN04 ] AS [Bank Account],
d.COLUMN20 [Total Amount],(isnull(fb.COLUMN06,0)+isnull(fb.COLUMN14,0)+isnull(fb.COLUMN13,0)+isnull(fb.COLUMN09,0)) [Amount Paid],
ma.COLUMN09 [Created By] From  SATABLE011  p  left outer join  SATABLE012  fb on   fb.COLUMN08 =p.COLUMN01 and fb.COLUMNA03=p.COLUMNA03 and isnull(fb.COLUMNA13,0)=0
left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14  and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join SATABLE009 d on d.COLUMN02=fb.COLUMN03  
and d.COLUMNA03=fb.COLUMNA03 and isnull(d.COLUMNA13,0)=0 left outer join  FITABLE001  c  on  c.COLUMN02=p.COLUMN08 
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN18 = 1002 and p.COLUMN03=1278 and isnull(p.COLUMNA13,0)=0 and 
(d.COLUMN03 = 1277 or isnull(cast(d.COLUMN03 as nvarchar(250)),'''')='''') and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+'')
END
END
ELSE IF(@FormId='1525')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05],''' + @dateF + '''),'' '',s1.COLUMN05,'' '',m1.COLUMN09,'' '',s1.COLUMN05,'' '',s2.COLUMN05,'' '',m1.COLUMN09,'' '',f1.COLUMN04,'' '',fi.COLUMN04,'' '',fi.COLUMN16,'' '',fi.COLUMN17,'' '',o.COLUMN04,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN05 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Voucher#],'''' AS Date,'''' AS Party,'''' AS [Account],
'''' Memo,'''' AS [Advance Paid],'''' [Amount],'''' AS [Bank],'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],''' + @dateF + ''') AS [Date],
(case when p.COLUMN11=''22305'' or p.COLUMN11=''22700'' then s1.COLUMN05 when p.COLUMN11=''22306'' then m1.COLUMN09 
when p.COLUMN11=''22334'' then s1.COLUMN05  when p.COLUMN11=''22335'' then s2.COLUMN05  when p.COLUMN11=''22492'' then 
m1.COLUMN09  else '''' end) as Party,f1.[COLUMN04 ] AS  Account,fi.COLUMN04 Memo,fi.COLUMN16 [Advance Paid],
fi.COLUMN17 [Amount],o.[COLUMN04 ] AS [Bank],ma.COLUMN09 [Created By] From FITABLE020 p  
left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN12 left outer join FITABLE022 fi on p.COLUMN01=fi.COLUMN09 and fi.COLUMNA03=p.COLUMNA03 and isnull(fi.COLUMNA13,0)=0 
left outer join FITABLE001 o on o.COLUMN02=p.COLUMN06  and o.COLUMNA03=p.COLUMNA03 
left outer join FITABLE001 f1 on f1.COLUMN02=fi.COLUMN03 and f1.COLUMNA03=p.COLUMNA03 
left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN07 and s1.COLUMNA03=p.COLUMNA03 and isnull(s1.COLUMNA13,0)=0 
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0 
left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0  
left outer join FITABLE044 f4 on f4.COLUMN02=fi.COLUMN12 and f4.COLUMNA03=fi.COLUMNA03 and isnull(f4.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) 
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN03=1525 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+'')
END
END
ELSE IF(@FormId='1526')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05],''' + @dateF + '''),'' '',s1.COLUMN05,'' '',m1.COLUMN09,'' '',s1.COLUMN05,'' '',s2.COLUMN05,'' '',m1.COLUMN09,'' '',f1.COLUMN04,'' '',f.COLUMN04,'' '',f.COLUMN17,'' '',f.COLUMN18,'' '',o.COLUMN04,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN05 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Voucher#],'''' AS Date,'''' AS Party,'''' AS [Account],
'''' Memo,'''' AS [Advance Received],'''' [Amount],'''' AS [Bank],'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],''' + @dateF + ''') AS [Date],
(case when p.COLUMN20=''22305'' then s1.COLUMN05 when p.COLUMN20=''22306'' then m1.COLUMN09 when p.COLUMN20=''22334'' 
then s1.COLUMN05  when p.COLUMN20=''22335'' then s2.COLUMN05  when p.COLUMN20=''22492'' then m1.COLUMN09  else '''' end) as Party,
f1.[COLUMN04 ] AS  Account,f.COLUMN04 Memo,f.COLUMN17 [Advance Received],f.COLUMN18 [Amount],o.[COLUMN04 ] AS [Bank],
ma.COLUMN09 [Created By] From FITABLE023 p 
left outer join FITABLE024 f on f.COLUMN09=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0
left outer join FITABLE001 o on o.COLUMN02=f.COLUMN12  and o.COLUMNA03=f.COLUMNA03 and isnull(o.COLUMNA13,0)=0 
left outer join FITABLE001 f1 on f1.COLUMN02=f.COLUMN03  and f1.COLUMNA03=p.COLUMNA03 and isnull(f1.COLUMNA13,0)=0 
left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 
left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16  and q.COLUMNA03=p.COLUMNA03 and isnull(q.COLUMNA13,0)=0
left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN08  and s1.COLUMNA03=p.COLUMNA03 and isnull(s1.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08  and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0
left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN08  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
left outer join FITABLE044 f4 on f4.COLUMN02=f.COLUMN13 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and 
ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN03=1526 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+'')
END
END
ELSE IF(@FormId='1363')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05],''' + @dateF + '''),'' '',s1.COLUMN05,'' '',m1.COLUMN09,'' '',s1.COLUMN05,'' '',s2.COLUMN05,'' '',m1.COLUMN09,'' '',p.COLUMN08,'' '',p.COLUMN18,'' '',o.COLUMN04,'' '',f44.COLUMN20,'' '',p.COLUMN16,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN05 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Voucher#],'''' AS Date,'''' AS Party,'''' AS [Reference#],
'''' Memo,'''' AS [Total Amount],'''' AS [Bank],'''' [Cheque#],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],''' + @dateF + ''') AS [Date],
(case when p.COLUMN11=''22305'' then s1.COLUMN05 when p.COLUMN11=''22306'' then m1.COLUMN09 when p.COLUMN11=''22334'' then 
s1.COLUMN05  when p.COLUMN11=''22335'' then s2.COLUMN05  when p.COLUMN11=''22492'' then m1.COLUMN09  else '''' end) as Party,
'''' Reference#,p.COLUMN08 Memo,p.[COLUMN18 ]AS [Total Amount] ,o.[COLUMN04 ] AS [Bank],f44.[COLUMN20 ] AS [Cheque#], 
p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From FITABLE020 p left outer join FITABLE001 o on o.COLUMN02=p.COLUMN06 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0  
left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN11  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 
left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN12 and q.COLUMNA03=p.COLUMNA03 and isnull(q.COLUMNA13,0)=0 
left outer join FITABLE022 fi on p.COLUMN01=fi.COLUMN09 and fi.COLUMNA03=p.COLUMNA03 and isnull(fi.COLUMNA13,0)=0
left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN07 and s1.COLUMNA03=p.COLUMNA03 and isnull(s1.COLUMNA13,0)=0 
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07  and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0
left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
left join FITABLE044 f44 on f44.COLUMN02=p.COLUMN09 and f44.COLUMNA03=p.COLUMNA03 and isnull(f44.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN03=1363 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+'')
END
END
ELSE IF(@FormId='1386')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05],''' + @dateF + '''),s2.COLUMN05,'' '',p.COLUMN11,'' '',p.COLUMN19,'' '',o.COLUMN04,'' '',cq.COLUMN20,'' '',p.COLUMN13,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN05 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Voucher#],'''' AS Date,'''' AS Party,'''' AS [Reference#],
'''' Memo,'''' AS [Total Amount],'''' AS [Bank],'''' [Cheque#],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],''' + @dateF + ''') AS [Date],
s2.[COLUMN05] as [Party],'''' Reference#,p.[COLUMN11 ] AS [Memo],p.[COLUMN19 ] AS [Total Amount],o.[COLUMN04 ] AS [Bank],
cq.[COLUMN20 ] AS [Cheque#],p.COLUMN13 Status,ma.COLUMN09 [Created By] From FITABLE023 p 
left outer join FITABLE001 o on o.COLUMN02=p.COLUMN09   left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN07  
left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16 left outer join FITABLE024 fi on p.COLUMN01=fi.COLUMN09 and fi.COLUMNA03=p.COLUMNA03 and isnull(fi.COLUMNA13,0)=0 
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08  and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0
left outer join FITABLE044 cq on cq.COLUMN02=fi.COLUMN13 and cq.COLUMNA03=fi.COLUMNA03 and isnull(cq.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) 
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN03=1386 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+'')
END
END
ELSE IF(@FormId='1414')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN09],''' + @dateF + '''),'' '',s1.COLUMN05,'' '',m1.COLUMN09,'' '',s2.COLUMN05,'' '',fj.COLUMN06,'' '',f.COLUMN04,'' '',fj.COLUMN04,'' '',fj.COLUMN05,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN09 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN09, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Journal Entry#],'''' AS Date,'''' AS Party,'''' Memo,'''' AS [Account],
'''' AS Debit,'''' Credit,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02] AS ID,(p.[COLUMN04]) AS [Journal Entry#],FORMAT(p.[COLUMN09 ],''' + @dateF + ''') AS [Date],
(case when p.column07=''22305'' or fj.column14=''22305'' then s1.cOLUMN05
when p.column07=''22334'' or fj.column14=''22334'' then s1.cOLUMN05 
when p.column07=''22335'' or fj.column14=''22335'' then s2.cOLUMN05	
when p.column07=''22306'' or fj.column14=''22306'' then m1.COLUMN09 
when p.column07=''22492'' or fj.column14=''22492'' then m1.COLUMN09 
else NULL end) Party,fj.COLUMN06 as Memo,f.COLUMN04 as Account,fj.COLUMN04 as Debit,fj.COLUMN05 as Credit,
ma.[COLUMN09 ] AS [Created By] From FITABLE031 p  left  join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 
left outer join FITABLE032 fj on fj.COLUMN12=p.COLUMN01 and isnull(fj.COLUMNA13,0)=0 
left outer join FITABLE001 f on f.COLUMN02=fj.COLUMN03 and f.COLUMNA03=p.columna03 
left outer join SATABLE001 S1 on s1.COLUMN02=fj.COLUMN07  and s1.COLUMNA03=fj.COLUMNA03 and isnull(s1.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=fj.COLUMN07  and s2.COLUMNA03=fj.COLUMNA03 and isnull(s2.COLUMNA13,0)=0
left outer join MATABLE010 m1 on m1.COLUMN02=fj.COLUMN07  and m1.COLUMNA03=fj.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN03=1414 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
group by p.[COLUMN02],p.[COLUMN04],p.[COLUMN09] ,p.[COLUMN07],o.[COLUMN04],p.COLUMNA06,p.COLUMNA07,f.COLUMN04,fj.COLUMN04,fj.COLUMN05,fj.COLUMN06,s1.COLUMN05,s2.COLUMN05,m1.COLUMN09,ma.COLUMN09,fj.COLUMN14
 '+@swhereStr+'')
END
END
ELSE IF(@FormId='1275')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN16 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN16 not in (select COLUMN04 from CONTABLE025 where column02 in(26,30,43))'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN06],''' + @dateF + '''),'' '',s.COLUMN05,'' '',FORMAT(p.[COLUMN08],''' + @dateF + '''),'' '',p.COLUMN11,'' '',p.COLUMN09,'' '',p.COLUMN15,'' '',p.COLUMN16,'' '',ma.column09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN06 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN06, 101) desc,convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS Order#,'''' AS Date,'''' AS Customer,'''' AS [Ship Date],'''' AS Reference#,'''' AS Memo,
'''' [Total Amount],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],FORMAT(p.[COLUMN06 ],''' + @dateF + ''')  [Date],s.[COLUMN05 ]  Customer,
FORMAT(p.[COLUMN08],''' + @dateF + ''') [Ship Date],p.[COLUMN11] as Reference#,p.[COLUMN09] as Memo,p.[COLUMN15] AS [Total Amount],
p.[COLUMN16 ] AS [Status],ma.column09 [Created By] From SATABLE005 p 
left join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and  s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
--left join  MATABLE009 e on e.COLUMN02=p.COLUMN29 and  e.COLUMNA03=p.COLUMNA03 and  isnull(e.COLUMNA13,0)=0
left join MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (ma.COLUMNA02=p.COLUMNA02 or ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0 
where p.COLUMN03 = 1275 and p.COLUMN29 = 1002 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+' ')
END
END
ELSE IF(@FormId='1276')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN14 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN14 not in (select COLUMN04 from CONTABLE025 where column02 in(81))'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN08],''' + @dateF + '''),'' '',s.COLUMN05,'' '',f.COLUMN04,'' '',p.COLUMN09,'' '',p.COLUMN13,'' '',p.COLUMN14,'' '',ma.column09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN08 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN08, 101) desc,convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Item Issue#],'''' AS Date,'''' AS Customer,'''' AS [ReferenceOrder#],'''' AS Reference#,'''' AS Memo,
'''' [Total Amount],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Issue#],FORMAT(p.[COLUMN08 ],''' + @dateF + ''') AS Date,s.[COLUMN05 ] AS Customer,
f.[COLUMN04 ] AS [ReferenceOrder#],p.[COLUMN09 ] AS Reference#,p.[COLUMN13 ] AS Memo,sum(isnull(i.COLUMN13,0)) [Total Amount],p.COLUMN14 Status,
ma.column09 [Created By] From SATABLE007 p 
left join SATABLE008 i on i.COLUMN14=p.COLUMN01 and i.COLUMNA02=p.COLUMNA02 and i.COLUMNA03=p.COLUMNA03 and isnull(i.COLUMNA13,0)=0
left join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and  s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
--left join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left join  SATABLE005 f on f.COLUMN02=p.COLUMN06 and f.COLUMNA02=p.COLUMNA02 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0
left join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (ma.COLUMNA02=p.COLUMNA02 or ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0
where p.COLUMN03 = 1276 and p.COLUMN20 = 1002 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' 
group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA06,p.COLUMNA07 '+@swhereStr+' ')
END
END
ELSE IF(@FormId='1656')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN16 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN16 not in (select COLUMN04 from CONTABLE025 where column02 in(09,41,10))'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN06],''' + @dateF + '''),'' '',s.COLUMN05,'' '',FORMAT(p.[COLUMN08],''' + @dateF + '''),'' '',p.COLUMN11,'' '',p.COLUMN09,'' '',p.COLUMN15,'' '',p.COLUMN16,'' '',ma.column09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN06 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN06, 101) desc,convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Service Order#],'''' AS Date,'''' AS Customer,'''' AS [Ship Date],'''' AS Reference#,'''' AS Memo,
'''' [Total Amount],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Service Order#],FORMAT(p.[COLUMN06 ],''' + @dateF + ''') AS [Date],s.[COLUMN05 ] AS [Customer],
FORMAT(p.[COLUMN08 ],''' + @dateF + ''') AS [Ship Date],p.COLUMN11 Reference#,p.COLUMN09 Memo,p.[COLUMN15 ] AS [Total Amount],
p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p 
left join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0
--inner join  MATABLE011 m on m.COLUMN02=p.COLUMN29 
left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0 
where p.COLUMN03 = 1656 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+' ')
-- and p.COLUMN29 = 1001  and p.COLUMN17 = 1001
END
END
ELSE IF(@FormId='1657')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN18 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN18 not in (select COLUMN04 from CONTABLE025 where column02 in(83))'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN09],''' + @dateF + '''),'' '',s.COLUMN05,'' '',f.COLUMN04,'' '',p.COLUMN09,'' '',p.COLUMN13,'' '',p.COLUMN14,'' '',ma.column09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN09 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN09, 101) desc,convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Service Receipt#],'''' AS Date,'''' AS Customer,'''' AS [Reference SO#],'''' AS Reference#,'''' AS Memo,
'''' [Total Amount],'''' Status,'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Service Receipt#],FORMAT(p.[COLUMN09 ],''' + @dateF + ''') AS Date,s.[COLUMN05 ] AS Vendor,
f.[COLUMN04 ] AS [Reference SO#],p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,
sum(cast(iif(p4.COLUMN11='''',0,cast(isnull(p4.COLUMN11,0) as decimal(18,2))) as decimal(18,2))) [Total Amount],p.COLUMN18 Status,
ma.COLUMN09 [Created By] From PUTABLE003 p 
left join PUTABLE004 p4 on p4.COLUMN12=p.COLUMN01 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03 and isnull(p4.COLUMNA13,0)=0 
left join SATABLE002 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
--left join CONTABLE007 o on o.COLUMN02=p.COLUMN13 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 left join  MATABLE002 d on d.COLUMN02=p.COLUMN16 
left join PUTABLE001 f on f.COLUMN02=p.COLUMN06 and f.COLUMNA02=p.COLUMNA02 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0 
left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0 
where p.COLUMN03 = 1657 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' 
group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA06,p.COLUMNA07 '+@swhereStr+' ')
END
END
ELSE IF(@FormId='1659')
BEGIN
if (@FilterByStatus!='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN18 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@FilterByStatus+''') s)'
end
else if (@FilterByStatus='' and @FilterByStatus!='ALL')
begin
set @whereStr= ' and P.COLUMN18 not in (select COLUMN04 from CONTABLE025 where column02 in(83))'
end
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN09],''' + @dateF + '''),'' '',pr.COLUMN05,'' '',m.COLUMN09,'' '',p.COLUMN10,'' '',p.COLUMN12) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN09 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN09, 101) desc,convert(datetime, p.COLUMNA06, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [Receipt#],'''' AS Date,'''' AS [Project#],'''' AS [Handed Over By],'''' AS Reference#,'''' AS Memo')
end
else
begin
exec ('
SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Receipt#],FORMAT(p.[COLUMN09 ],''' + @dateF + ''') AS Date,pr.[COLUMN05 ] AS Project#,m.[COLUMN09 ] AS [Handed Over By],p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo
From PUTABLE003 p 
--left join PUTABLE004 p4 on p4.COLUMN12=p.COLUMN01 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03 and isnull(p4.COLUMNA13,0)=0 
left join MATABLE010 m on m.COLUMN02=p.COLUMN14 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 
left join PRTABLE001 pr on pr.COLUMN02=p.COLUMN21 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0 
--left join CONTABLE007 o on o.COLUMN02=p.COLUMN13 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 left join  MATABLE002 d on d.COLUMN02=p.COLUMN16 
--p.COLUMN17 = 1001 and  left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) and isnull(ma.COLUMNA13,0)=0 
where p.COLUMN03 = 1659 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+' '+@swhereStr+' ')
END
END
ELSE IF(@FormId='1669')
BEGIN
if @SearchData!=''
begin
set @whereStr= @whereStr+ ' and CONCAT(p.COLUMN04,'' '',FORMAT(p.[COLUMN05],''' + @dateF + '''),'' '',S5.COLUMN04,'' '',S1.COLUMN05,'' '',S7.COLUMN04,'' '',p.COLUMN10,'' '',p.COLUMN13,'' '',p.COLUMN14,'' '',p.COLUMN16,'' '',ma.COLUMN09) like ''%'+@SearchData+'%'''
end
if @OPUnit!=''
begin
set @whereStr= @whereStr+ ' and (P.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or P.COLUMNA02 is null)'
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end
set @whereStr= @whereStr + ' and P.COLUMN05 BETWEEN '''+ @FRDATE +''' AND '''+ @TODATE +''' '
if (@SortBy='')
set @swhereStr=' ORDER BY convert(datetime, p.COLUMN05, 101) desc, cast(p.COLUMN02 as int) desc'
if(@Type='Info')
begin
exec('SELECT '''' AS ID,'''' AS [MRP Gen#],'''' AS Date,'''' AS [Job Order],'''' [Jobber Name],'''' AS [Job Order Issue],
'''' AS [Cost of Material],'''' [Total Cost],'''' [Qty Received],'''' [MRP],'''' AS [Created By] ')
end
else
begin
exec ('
SELECT p.[COLUMN02] AS ID,
(p.[COLUMN04]) AS [MRP Gen#],
FORMAT(p.[COLUMN05 ],''' + @dateF + ''') AS [Date],
S5.COLUMN04 AS [Job Order],S1.COLUMN05 AS [Jobber Name],
S7.COLUMN04 AS [Job Order Issue],ISNULL(p.COLUMN10,0) AS [Cost of Material],ISNULL(p.COLUMN13,0) AS [Total Cost],
ISNULL(p.COLUMN16,0) AS [Qty Received],ISNULL(p.COLUMN18,0) AS [MRP],
ma.[COLUMN09 ] AS [Created By] From SATABLE022 p  
left  join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 AND o.COLUMNA03=p.COLUMNA03 AND ISNULL(o.COLUMNA13,0)=0
LEFT JOIN SATABLE005 S5 ON S5.COLUMN02 = p.COLUMN06 AND S5.COLUMNA03 = p.COLUMNA03 AND ISNULL(S5.COLUMNA13,0)=0 AND S5.COLUMN03 = 1283
LEFT JOIN SATABLE001 S1 ON S1.COLUMN02 = p.COLUMN07 AND S1.COLUMNA03 = p.COLUMNA03 AND ISNULL(S1.COLUMNA13,0)=0 AND S1.COLUMN22 = 22286
LEFT JOIN SATABLE007 S7 ON S7.COLUMN02 = p.COLUMN08 AND S7.COLUMNA03 = p.COLUMNA03 AND ISNULL(S7.COLUMNA13,0)=0 AND S7.COLUMN03 = 1284
left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 AND ISNULL(ma.COLUMNA13,0)=0 
where p.COLUMNA03='''+@AcOwner+''' AND p.COLUMN03=1669 and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='''' '+@whereStr+'
group by p.[COLUMN02],p.[COLUMN04],p.[COLUMN05] ,S5.COLUMN04,S1.COLUMN05,S7.COLUMN04,
p.COLUMN10,p.COLUMN13,p.COLUMN14,p.COLUMN16,p.COLUMN18,ma.[COLUMN09]
 '+@swhereStr+'')
END
END
END








GO
