USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE014]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_FI_TP_FITABLE014]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,@COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   
	@COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   
	@COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   
	@COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  
	@COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  
	@COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  
	@COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  
	@COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  
	@COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@Direction  nvarchar(250),       @TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT,    
	@Totamt nvarchar(250)=null
)

AS

BEGIN
begin try
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08= '1' order by COLUMN01 desc
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE014_SequenceNo
--EMPHCS1410 LOGS CREATION BY SRINIVAS
Declare @tempSTR nvarchar(max)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment Header Values are Intiated for FITABLE014 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 				  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
    )
set @COLUMN16=(iif(@COLUMN16='',0,isnull(@COLUMN16,0)))
insert into FITABLE014 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN25,  COLUMN26,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, 
   COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, 
   COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, 
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, 
   COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN25,  @COLUMN26,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, 
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, 
   @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, 
   @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, 
   @COLUMND10
)  

--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment Header Values are Created in FITABLE014 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue =(select COLUMN01 from FITABLE014 where COLUMN02=@COLUMN02)
END
 

IF @Direction = 'Select'
BEGIN
select * from FITABLE014
END 

 

IF @Direction = 'Update'
BEGIN
--EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
declare @Transno nvarchar(250), @lIID nvarchar(250),@Project nvarchar(250)
set @Transno= (SELECT COLUMN04 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
set @COLUMN16=(iif(@COLUMN16='',0,isnull(@COLUMN16,0)))
set @Project = (select COLUMN26 from FITABLE014 Where COLUMN02=@COLUMN02)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
UPDATE FITABLE014 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11, 
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr  
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
   COLUMN26=@COLUMN26,
   COLUMNA01=@COLUMNA01,
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  
   COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  
   COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  
   COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  
   COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  
   COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  
   COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  
   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment Header Values are Updated in FITABLE014 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
   set @COLUMN11= (SELECT COLUMN01 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
	delete from PUTABLE017 where COLUMN05 in(@COLUMN11) and COLUMN13=@COLUMN10 and isnull(COLUMN15,0)=@COLUMN16 and COLUMNA03=@COLUMNA03 and column06='Inventory Adjustment'
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Deleted in PUTABLE017 Table for '+Cast(isnull(@COLUMN11,'')as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
    )
 declare @ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@uom int,@Lot nvarchar(250),@Location nvarchar(250),@upcno nvarchar(250) = null
set @Location=(isnull((select COLUMN16 from FITABLE014 where COLUMN01=@COLUMN11),0))
set @Location=(iif(@Location='',0,isnull(@Location,0)))
 declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@TrackQty  bit,@Price  decimal(18,2),
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
 @Item  int,@MaxRownum  int,@Initialrow int,@id int,@uomselection nvarchar(250)=null ,@stno nvarchar(250)=null 
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE015 where COLUMN11 in(@COLUMN11) and isnull(COLUMNA13,0)=0
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE015 where COLUMN11 in(@COLUMN11) and isnull(COLUMNA13,0)=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from FITABLE015 where COLUMN02=@id)
	     --EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
             set @lIID=(select COLUMN01 from FITABLE015 where COLUMN02=@id)
			 set @Lot=(select COLUMN23 from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
			 set @Lot=(iif(@Lot='',0,isnull(@Lot,0)))
             set @ItemQty=(select isnull(COLUMN08,0)-isnull(COLUMN16,0) from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
	     --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
             set @Price=(select isnull(COLUMN10,0) from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
			 set @uom=(select isnull(COLUMN22,0) from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
			 set @upcno=(select COLUMN04 from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
				--Inventory Updation
			declare @AvgPrice decimal(18,7),@Qty_Order decimal(18,7),@Qty_Cmtd decimal(18,7),@TotalBillQty decimal(18,7),@TotalBillAmt decimal(18,2),@BillAvgPrice decimal(18,2)
			set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06 =@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN26,0)=CAST(@Project AS INT)
			  union all
			  select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
			  union all
			  select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN04 =@upcno AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=isnull(@Lot,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location and isnull(h.COLUMN26,0)=CAST(@Project AS INT)))T)
			set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06 =@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN26,0)=CAST(@Project AS INT)
			  union all
			  select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
			  union all
			  select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN04 =@upcno AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=isnull(@Lot,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location and isnull(h.COLUMN26,0)=CAST(@Project AS INT)))T)
			  if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN05 =@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))
			  begin
			  set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN05 =@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,7)),4))as decimal(18,7)),4)
			  set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN05 =@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,4)))as decimal(18,4))
			  end
			            if(isnull(@TotalBillQty,0)=0)
			            begin
						set @BillAvgPrice=(0)
						end
						else
						begin
						set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),4))
						end
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@Lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@upcno)
			declare  @FinalAvgPrice decimal(18,2)
			set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				set @tempSTR=(null)
				--set @tempSTR=('Target2:Header Update:At Header Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',3.Item '+isnull(cast(@Item as nvarchar(250)),'')+',4.UOM '+isnull(cast(@uom as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',7.Source Operating Unit '+isnull(cast(@COLUMN05 as nvarchar(250)),'')+',8.Destination Operating Unit '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				if(@TrackQty=1)
				begin
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),2)*cast(@BillAvgPrice as decimal(18,2)) as decimal(18,2)) 
				WHERE COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				--EMPHCS1410 LOGS CREATION BY SRINIVAS
				   --set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   --'Inventory Values are Reset in FITABLE010 Table BY '+Cast(isnull(@Qty_On_Hand,'')as nvarchar(20))+' For ITEM '+cast(@Item as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				   -- )
					--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
					--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				end
				--EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
				delete from FITABLE029 where COLUMND06 in(@lIID) and COLUMN03='Inventory Adjustment' and COLUMN11=@Transno and COLUMNA03=@COLUMNA03
				delete from FITABLE025 where COLUMN05 in(@lIID) and COLUMN04='Inventory Adjustment' and COLUMNA03=@COLUMNA03
				end
			if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
			begin
			EXEC usp_TP_InventoryAssetUpdate @COLUMN11,@COLUMN04,null,@Item,@uom,@Location,@Lot,@BillAvgPrice,@COLUMN10,@COLUMNA03
			end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
	update FITABLE015 set columna13=1 where column11 in(@COLUMN11)
END
 

else IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE014 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS1410 LOGS CREATION BY SRINIVAS
				   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   'Inventory Adjustment Values are Deleted in FITABLE010 Table  For  '+cast(@COLUMN02 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				    )
   set @COLUMN11= (SELECT COLUMN01 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
   --EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
   set @Transno= (SELECT COLUMN04 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
   set @COLUMN10= (SELECT COLUMN10 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
   set @COLUMNA02= (SELECT COLUMNA02 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
   set @COLUMNA03= (SELECT COLUMNA03 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   set @COLUMN16=(select COLUMN16 from FITABLE014 where COLUMN02=@COLUMN02)
   set @COLUMN16=(iif(@COLUMN16='',0,isnull(@COLUMN16,0)))
   set @Location = @COLUMN16
   set @Project = (select COLUMN26 from FITABLE014 Where COLUMN01=@COLUMN11)
  set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
   set @COLUMNA08= (SELECT COLUMNA08 FROM FITABLE014 WHERE COLUMN02=@COLUMN02) 
   set @COLUMNA07= (SELECT GETDATE())
   set @COLUMNB01= (SELECT COLUMN04 FROM FITABLE014 WHERE COLUMN02=@COLUMN02)
   set @COLUMNB02= (SELECT COLUMN03 FROM FITABLE014 WHERE COLUMN02=@COLUMN02)
	select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08= '1' order by COLUMN01 desc
	delete from PUTABLE017 where COLUMN05 in(@COLUMN11) and COLUMN13=@COLUMN10 and isnull(COLUMN15,0)=@COLUMN16 and COLUMNA03=@COLUMNA03 and column06='Inventory Adjustment'
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE015 where COLUMN11 in(@COLUMN11) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE015 where COLUMN11 in(@COLUMN11) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from FITABLE015 where COLUMN02=@id)
	     --EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
             set @lIID=(select COLUMN01 from FITABLE015 where COLUMN02=@id)
			 --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
			 set @Lot=(select COLUMN23 from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
			 set @Lot=(iif(@Lot='',0,isnull(@Lot,0)))
             set @ItemQty=(select isnull(COLUMN08,0)-isnull(COLUMN16,0) from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
	     --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
             set @Price=(select isnull(COLUMN10,0) from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
			 set @uom=(select isnull(COLUMN22,0) from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
			 set @upcno=(select COLUMN04 from FITABLE015 where COLUMN02=@id and COLUMN03=@Item)
				--Inventory Updation
			  set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06=@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN26,0)=CAST(@Project AS INT)
			  union all
			  select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
			  union all
			  select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN04=@upcno AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=isnull(@Lot,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND ISNULL(h.COLUMN26,0)=CAST(@Project AS INT)))T)
			set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06=@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN26,0)=CAST(@Project AS INT)
			  union all
			  select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
			  union all
			  select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN04=@upcno AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=isnull(@Lot,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND ISNULL(h.COLUMN26,0)=CAST(@Project AS INT) ))T)
			  if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN05 =@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN21,0)=CAST(@Project AS INT))
			  begin
			  set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN05 =@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,7)),4))as decimal(18,7)),4)
			  set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND l.COLUMN05 =@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMNA02,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=isnull(@Lot,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,4)))as decimal(18,4))
			  end
			            if(isnull(@TotalBillQty,0)=0)
			            begin
						set @BillAvgPrice=(0)
						end
						else
						begin
						set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),2))
						end
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@Lot,0) AND isnull(COLUMN23,0)=ISNULL(@Project,0) and COLUMN24=@upcno)
			set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				set @tempSTR=(null)
				--set @tempSTR=('Target2:Header Update:At Header Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',3.Item '+isnull(cast(@Item as nvarchar(250)),'')+',4.UOM '+isnull(cast(@uom as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',7.Source Operating Unit '+isnull(cast(@COLUMN05 as nvarchar(250)),'')+',8.Destination Operating Unit '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))-cast(@ItemQty as decimal(18,2)));
				if(@TrackQty=1)
				begin
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),2)*cast(@BillAvgPrice as decimal(18,2)) as decimal(18,2))
				,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' 
				WHERE COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				--EMPHCS1410 LOGS CREATION BY SRINIVAS
				   --set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   --'Inventory Values are Reset in FITABLE010 Table BY '+Cast(isnull(@Qty_On_Hand,'')as nvarchar(20))+' For ITEM '+cast(@Item as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				   -- )
					--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
					--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				end 
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN10 and isnull(COLUMN21,0)=@COLUMN16 and isnull(COLUMN22,0)=@Lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				end
				--EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
				delete from FITABLE029 where COLUMND06 in(@lIID) and COLUMN03='Inventory Adjustment' and COLUMN11=@Transno and COLUMNA03=@COLUMNA03
				delete from FITABLE025 where COLUMN05 in(@lIID) and COLUMN04='Inventory Adjustment' and COLUMNA03=@COLUMNA03
				end
			if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
			begin
			EXEC usp_TP_InventoryAssetUpdate @COLUMN11,@Transno,null,@Item,@uom,@COLUMN16,@Lot,@BillAvgPrice,@COLUMN10,@COLUMNA03
			end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
	update FITABLE015 set columna13=1 where column11 in(@COLUMN11)
	
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment Line Values are Deleted in FITABLE015 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
END

   --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE014.txt',0
end try
begin catch
--EMPHCS1410	Logs Creation by srinivas		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_Exception_FITABLE014.txt',0
return 0
end catch
end


































GO
