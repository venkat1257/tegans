USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_PAYMENT_LINE_DebitDATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_PAYMENT_LINE_DebitDATA]
(
   @SalesOrderID nvarchar(250)=null,
	@AcOwner int=null,
	@OPUNIT int=null,
	@FRDATE date = '1/1/2012',
	@TODATE date = '1/1/2200'
)

AS
BEGIN
select top 1 @FRDATE=column04,@TODATE = COLUMN05 from FITABLE048 where COLUMNA03=@AcOwner AND COLUMN09=1 order by COLUMN01 desc
select CU.COLUMN02 as COLUMN20,cast(h.COLUMN02 as int) COLUMN03,sum(isnull(l.COLUMN04,0)) COLUMN04,
sum(isnull(l.COLUMN04,0)-isnull(l.COLUMND05,0))COLUMN05,0.00 COLUMN14,0.00 directReturn,
cast(sum(isnull(l.COLUMN04,0)-isnull(l.COLUMND05,0)) as nvarchar) COLUMN06,cast(h.COLUMN09 as date) dates,'' COLUMN16,'' 
directReturnCol,'Journal' COLUMN17,l.COLUMN02 COLUMN18
 from FITABLE032 l inner join FITABLE031 h on h.COLUMN01=l.COLUMN12 and 
h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02 and (h.COLUMN07=22335  or l.COLUMN14=22335 ) and
 (isnull(l.COLUMN04,0)-isnull(l.COLUMND05,0))!=0 and  (isnull(l.COLUMN04,0))!=0  and isnull(h.COLUMNA13,0)=0  
  inner JOIN FITABLE001 Acc ON Acc.COLUMN02=l.COLUMN03 and Acc.COLUMNA03=l.COLUMNA03 and isnull(Acc.COLUMNA13,0)=0  AND ACC.COLUMN07=22264 
  inner JOIN SATABLE002 CU ON (CU.COLUMN02=@SalesOrderID OR CU.COLUMN38=@SalesOrderID) and CU.COLUMNA03=l.COLUMNA03 and isnull(CU.COLUMNA13,0)=0 
 where l.COLUMN07=CU.COLUMN02 and l.COLUMNA03=@AcOwner AND l.COLUMNA02 in(@OPUNIT)  and isnull(l.COLUMNA13,0)=0 AND H.COLUMN09 <= @TODATE  group by h.COLUMN02,h.COLUMN09,l.COLUMN02,CU.COLUMN02
union all
select  CU.COLUMN02 as COLUMN20,cast(h.COLUMN02 as int) COLUMN03,h.COLUMN15 COLUMN04,(isnull(h.COLUMN15,0)-isnull(h.COLUMND05,0)) COLUMN05,
0.00 COLUMN14,0.00 directReturn,cast((isnull(h.COLUMN15,0)-isnull(l.COLUMND05,0)) as nvarchar) COLUMN06,
cast(h.COLUMN06 as date) dates,'' COLUMN16,'' directReturnCol,'DebitMemo' COLUMN17,h.COLUMN02 COLUMN18
 from PUTABLE002 l INNER JOIN 
PUTABLE001 h ON h.COLUMN01=l.COLUMN19  and h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02  and isnull(l.columna13,0)=0 
  inner JOIN SATABLE002 CU ON (CU.COLUMN02=@SalesOrderID OR CU.COLUMN38=@SalesOrderID) and CU.COLUMNA03=l.COLUMNA03 and isnull(CU.COLUMNA13,0)=0 
 WHERE   h.COLUMNA03=@AcOwner AND h.COLUMN24 in(@OPUNIT)
 AND h.COLUMN03='1355' AND h.COLUMN50=22335  AND h.COLUMN05=CU.COLUMN02 AND (isnull(h.COLUMN16,'OPEN')!='CLOSE' and 
 cast(isnull(h.COLUMN15,0)-isnull(h.COLUMND05,0) as decimal(18,2))!=0) and isnull(h.columna13,0)=0  AND H.COLUMN06 <= @TODATE  
union all
select  CU.COLUMN02 as COLUMN20,cast(h.COLUMN02 as int) COLUMN03,h.COLUMN18 COLUMN04,h.COLUMN18 COLUMN05,0.00 COLUMN14,0.00 directReturn, 
cast(h.COLUMN18 as nvarchar) COLUMN06,cast(h.COLUMN05 as date) dates,'' COLUMN16,'' directReturnCol,'AdvancePayment' COLUMN17,h.COLUMN02 COLUMN18
 from FITABLE022 l INNER JOIN 
FITABLE020 h ON h.COLUMN01=l.COLUMN09  and h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02  and isnull(l.columna13,0)=0 
  inner JOIN SATABLE002 CU ON (CU.COLUMN02=@SalesOrderID OR CU.COLUMN38=@SalesOrderID) and CU.COLUMNA03=l.COLUMNA03 and isnull(CU.COLUMNA13,0)=0 
 WHERE   h.COLUMNA03=@AcOwner AND h.COLUMN12 in(@OPUNIT)
 AND h.COLUMN03='1363' AND h.COLUMN11=22335  AND h.COLUMN07=CU.COLUMN02 AND (isnull(h.COLUMN16,'OPEN')!='CLOSE' and 
 cast(isnull(h.COLUMN18,0) as decimal(18,2))!=0) and isnull(h.columna13,0)=0  AND H.COLUMN05 <= @TODATE  
union all
select  CU.COLUMN02 as COLUMN20,cast(h.COLUMN02 as int) COLUMN03,sum(isnull(l.COLUMN15,0)) COLUMN04,sum(isnull(l.COLUMN15,0)-isnull(l.COLUMND05,0)) COLUMN05,
0.00 COLUMN14,0.00 directReturn,cast(sum(isnull(l.COLUMN15,0)-isnull(l.COLUMND05,0)) as nvarchar) COLUMN06,
cast(h.COLUMN05 as date) dates,'' COLUMN16,'' directReturnCol,'PaymentVoucher' COLUMN17,l.COLUMN02 COLUMN18
 from FITABLE022 l INNER JOIN 
FITABLE020 h ON h.COLUMN01=l.COLUMN09  and h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02  and isnull(l.columna13,0)=0 
 inner JOIN FITABLE001 Acc ON Acc.COLUMN02=l.COLUMN03 and Acc.COLUMNA03=l.COLUMNA03 and isnull(Acc.COLUMNA13,0)=0  AND ACC.COLUMN07=22264 
  inner JOIN SATABLE002 CU ON (CU.COLUMN02=@SalesOrderID OR CU.COLUMN38=@SalesOrderID) and CU.COLUMNA03=l.COLUMNA03 and isnull(CU.COLUMNA13,0)=0 
 WHERE   h.COLUMNA03=@AcOwner AND h.COLUMN12 in(@OPUNIT)
 AND h.COLUMN03='1525' AND h.COLUMN11=22335  AND h.COLUMN07=CU.COLUMN02 AND (isnull(h.COLUMN16,'OPEN')!='CLOSE' and 
 cast(isnull(l.COLUMN15,0) as decimal(18,2))!=0) and isnull(h.columna13,0)=0 AND H.COLUMN05 <= @TODATE   group by h.COLUMN02,h.COLUMN05,l.COLUMN02,CU.COLUMN02
END















GO
