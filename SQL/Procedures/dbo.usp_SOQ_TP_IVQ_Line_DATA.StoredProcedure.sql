USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SOQ_TP_IVQ_Line_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SOQ_TP_IVQ_Line_DATA]
(
	@SalesOrderID int
)

AS
BEGIN
	SELECT  b.COLUMN03 as COLUMN05, b.COLUMN05 as COLUMN06 ,b.COLUMN06 as COLUMN10,b.COLUMN07 as COLUMN13,b.COLUMN08 as COLUMN14,b.COLUMN12 as COLUMN22,sum(isnull(f.COLUMN08,0)) as COLUMN11,b.COLUMN09 as COLUMN19,null COLUMN20
	,b.COLUMN07 COLUMN35,b.COLUMN13 COLUMN21,b.COLUMN14 COLUMN23,hs.COLUMN04 hsncode,b.COLUMN04 COLUMN03
	FROM SATABLE016 b  left outer join FITABLE010 f on f.COLUMN03=b.COLUMN03 and f.COLUMN19=b.COLUMN12 and f.COLUMNA03=b.COLUMNA03 
	and f.COLUMNA02=b.COLUMNA02  
	LEFT JOIN MATABLE007 m7 ON m7.COLUMN02 = b.COLUMN03 AND m7.COLUMNA03 = b.COLUMNA03
	left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
   -- left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0

	WHERE  b.COLUMN11=(select COLUMN01 from SATABLE015 where COLUMN02=@SalesOrderID) group by b.COLUMN03, b.COLUMN05,b.COLUMN06,b.COLUMN07,b.COLUMN08,b.COLUMN12,b.COLUMN09,b.COLUMN13,b.COLUMN14,hs.COLUMN04,b.COLUMN02,b.COLUMN04 order by b.COLUMN02 asc
END







GO
