USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GETFINANCEREPORTS]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GETFINANCEREPORTS]
(
	@ReportName  nvarchar(250)= null,
	@FrDate    nvarchar(250)= null,
	@ToDate      nvarchar(250)= null,
	@Type        nvarchar(250)= null,
	@Vendor      nvarchar(250)= null,
	@Customer      nvarchar(250)= null,
	@BillNO      nvarchar(250)= null,
	@Jobber      nvarchar(250)= null,
	@SONO      nvarchar(250)= null,
	@PONO      nvarchar(250)= null,
	@Status      nvarchar(250)= null,
	@Item      nvarchar(250)= null,
	@UPC      nvarchar(250)= null,
	@OPUnit nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@SalesRep nvarchar(250)= null,
	@Operating      nvarchar(250)= null,
	@DateF nvarchar(250)=null,
	--EMPHCS1252 gnaneshwar on 7/10/2015  Inserting Data Into Cost Of Sales When Resourse Consumption Created in Project
	@ProjectName      nvarchar(250)= null,
@Brand nvarchar(250)= null
)
AS
	BEGIN
	DECLARE @PFRDATE  nvarchar(250)= '01/01/1900', @PTODATE DATE = '1/1/2010'
		if(@FrDate!='' and @ToDate!='')
		BEGIN
			--SET @PFRDATE=((SELECT DateAdd(yy, -1, @FrDate)))
			--SET @PTODATE=((SELECT DateAdd(yy, -1, @ToDate)))
			--select top 1 @PFRDATE=column04 from MATABLE020 where COLUMNA03=@AcOwner and isnull(columna13,0)=0 order by COLUMN01 desc
			select top 1 @PFRDATE=column04 from FITABLE048 where COLUMNA03=@AcOwner and column09 = '1' and isnull(columna13,0)=0 --order by COLUMN01 desc
			IF((SELECT MAX(COLUMN04) FROM MATABLE020 WHERE COLUMNA03=@AcOwner AND COLUMN04 = @PFRDATE AND ISNULL(COLUMNA13,0)=0) IS NULL)
			BEGIN
				SET @PTODATE = @PFRDATE
			END
			ELSE IF((SELECT DATEADD(YEAR, -1, (SELECT MIN(COLUMN04) FROM MATABLE020 WHERE COLUMNA03=@AcOwner))) IS NOT NULL)
			BEGIN
				SET @PTODATE = (SELECT DATEADD(YEAR, -1, (SELECT MIN(COLUMN04) FROM MATABLE020 WHERE COLUMNA03=@AcOwner)))
			END
		END

declare @whereStr nvarchar(max)
if @ProjectName!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@ProjectName+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@ProjectName+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end
IF @ReportName='VATComputation'
BEGIN
declare @INPUTBForwd decimal(18,2),@OUTPUTBForwd decimal(18,2)
if(@FrDate!='' and @ToDate!='' and @Operating!='')
begin
declare @paid decimal(18,2), @brought decimal(18,2), @Tbrought decimal(18,2), @paidtax decimal(18,2),@TINPUTBForwd decimal(18,2),@TOUTPUTBForwd decimal(18,2)
set @paid=(select cast(isnull(COLUMN04,0) as decimal(18,2)) as Paid from FITABLE021 where COLUMN07 between (SELECT CAST(DATEADD(DAY,-DAY(GETDATE())+1, CAST(GETDATE() AS DATE)) AS DATETIME)) and GETDATE() and COLUMNA03=@AcOwner and COLUMN03=1108 and COLUMNA02=@Operating and isnull(COLUMNA13,0)=0)
set @OUTPUTBForwd=(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY')and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03<@FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')
set @INPUTBForwd= (select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03<@FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')
set @paidtax= (select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02=@Operating) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMNA13,0)=0)
set @TOUTPUTBForwd=(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY')and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03<=@ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')
set @TINPUTBForwd= (select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03<=@ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')
--if(@OUTPUTBForwd=0.00 and @INPUTBForwd=0.00) begin set @OUTPUTBForwd=(@TOUTPUTBForwd) set @INPUTBForwd=(@TINPUTBForwd) end
if(@OUTPUTBForwd>@INPUTBForwd)
begin
set @brought= (@OUTPUTBForwd-@INPUTBForwd-@paidtax)
set @Tbrought= (@TOUTPUTBForwd-@TINPUTBForwd-@paidtax)
if(@brought>=0)begin set @type=('OUTPUT')end else begin set @type=('INPUT')end 
end
else
begin
set @brought= (@INPUTBForwd-@OUTPUTBForwd-@paidtax)
set @Tbrought= (@TINPUTBForwd-@TOUTPUTBForwd-@paidtax)
if(@brought>=0)begin set @type=('INPUT')end else begin set @type=('OUTPUT')end 
end
if(@TOUTPUTBForwd>@TINPUTBForwd)
begin
if exists(select column02 from FITABLE026 where column03 between @FrDate and @ToDate)
begin
select 'OUTPUT' CI,'INPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'INPUT VAT from Purchases in the month' as A,(case when (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then('INPUT '+(MATABLE002.COLUMN04)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end)TAX,iif((FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY'),-(FITABLE026.COLUMN14), FITABLE026.COLUMN14) as TotalAmount,isnull(FITABLE026.COLUMN11,0)-isnull(FITABLE026.COLUMN12,0) as TaxAmount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as totTax,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02=@Operating) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN10=22399 AND FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02=@Operating  and FITABLE026.COLUMN17 like 'INPUT%'
union all
Select 'OUTPUT' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT VAT from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo'  or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(MATABLE002.COLUMN04)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))),(sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02=@Operating) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and  isnull(FITABLE026.COLUMN10,0) in(22399) and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02=@Operating and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03,MATABLE002.COLUMN04
union all
--cst
Select 'CST' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT CST from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(MATABLE002.COLUMN04)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))),(sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%') ) as totTax,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo'or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Brought,@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10  where isnull(FITABLE026.COLUMNA13,0)=0 and isnull(FITABLE026.COLUMN10,0) in(22400) and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02=@Operating and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03,MATABLE002.COLUMN04
end
else
begin
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
--cst
select 'CST' CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 

end
end
else
begin
if exists(select column02 from FITABLE026 where column03 between @FrDate and @ToDate)
begin
select 'INPUT' CI,'INPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'INPUT VAT from Purchases in the month' as A,(case when (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then('INPUT '+(select COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=FITABLE026.COLUMN10)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end)TAX,iif((FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY'),-(FITABLE026.COLUMN14), FITABLE026.COLUMN14) as TotalAmount,isnull(FITABLE026.COLUMN11,0)-isnull(FITABLE026.COLUMN12,0) as TaxAmount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0)) as totTax,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02=@Operating) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN10=22399 AND FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02=@Operating  and FITABLE026.COLUMN17 like 'INPUT%'
union all
Select 'INPUT' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT VAT from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(select COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=FITABLE026.COLUMN10)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))),(sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02=@Operating) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02=@Operating and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03
union all
--cst
Select 'CST' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT CST from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(select COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=FITABLE026.COLUMN10)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))), (sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02=@Operating) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMN10,0) in(22400) and isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02=@Operating and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03
end
else
begin
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
--cst
select 'CST' CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 

end
end
end
else if(@FrDate!='' and @ToDate!='')
begin
set @paid=(select cast(isnull(COLUMN04,0) as decimal(18,2)) as Paid from FITABLE021 where COLUMN07 between (SELECT CAST(DATEADD(DAY,-DAY(GETDATE())+1, CAST(GETDATE() AS DATE)) AS DATETIME)) and GETDATE() and COLUMNA03=@AcOwner and COLUMN03=1108 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(COLUMNA13,0)=0)
set @OUTPUTBForwd=(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY')and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03<@FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')
set @INPUTBForwd= (select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03<@FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')
set @paidtax= (select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMNA13,0)=0)
set @TOUTPUTBForwd=(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY')and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03<=@ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')
set @TINPUTBForwd= (select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03<=@ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')
--if(@OUTPUTBForwd=0.00 and @INPUTBForwd=0.00) begin set @OUTPUTBForwd=(@TOUTPUTBForwd) set @INPUTBForwd=(@TINPUTBForwd) end
if(@OUTPUTBForwd>@INPUTBForwd)
begin
set @brought= (@OUTPUTBForwd-@INPUTBForwd-@paidtax)
set @Tbrought= (@TOUTPUTBForwd-@TINPUTBForwd-@paidtax)
if(@brought>=0)begin set @type=('OUTPUT')end else begin set @type=('INPUT')end 
end
else
begin
set @brought= (@INPUTBForwd-@OUTPUTBForwd-@paidtax)
set @Tbrought= (@TINPUTBForwd-@TOUTPUTBForwd-@paidtax)
if(@brought>=0)begin set @type=('INPUT')end else begin set @type=('OUTPUT')end 
end
if(@TOUTPUTBForwd>@TINPUTBForwd)
begin
if exists(select column02 from FITABLE026 where column03 between @FrDate and @ToDate)
begin
select 'OUTPUT' CI,'INPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'INPUT VAT from Purchases in the month' as A,
(case when (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  
then('INPUT '+(MATABLE002.COLUMN04)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end)TAX,
iif((FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY'),-(FITABLE026.COLUMN14), 
FITABLE026.COLUMN14) as TotalAmount,isnull(FITABLE026.COLUMN11,0)-isnull(FITABLE026.COLUMN12,0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 
where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03 between @FrDate and @ToDate and 
isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as totTax,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 
where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')-
(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or 
COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 
between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 
left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and 
(FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  
FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  
FITABLE026.COLUMN10=22399 AND FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMN17 like 'INPUT%' 
union all
Select 'OUTPUT' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT VAT from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(MATABLE002.COLUMN04)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))), (sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and  isnull(FITABLE026.COLUMN10,0) in(22399) and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03,MATABLE002.COLUMN04
union all
--cst
Select 'CST' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT CST from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(MATABLE002.COLUMN04)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))), (sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Brought,@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10  where isnull(FITABLE026.COLUMNA13,0)=0 and isnull(FITABLE026.COLUMN10,0) in(22400) and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03,MATABLE002.COLUMN04
end
else
begin
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
--cst
select 'CST' CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 

end
end
else
begin
if exists(select column02 from FITABLE026 where column03 between @FrDate and @ToDate)
begin
select 'INPUT' CI,'INPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'INPUT VAT from Purchases in the month' as A,(case when (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then('INPUT '+(select COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=FITABLE026.COLUMN10)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end)TAX,iif((FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY'),-(FITABLE026.COLUMN14), FITABLE026.COLUMN14) as TotalAmount,isnull(FITABLE026.COLUMN11,0)-isnull(FITABLE026.COLUMN12,0) as TaxAmount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')) as totTax,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='BILL' or FITABLE026.COLUMN04='Refund' or FITABLE026.COLUMN04='Debit Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN10=22399 AND FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  and FITABLE026.COLUMN17 like 'INPUT%'
union all
Select 'INPUT' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT VAT from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(select COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=FITABLE026.COLUMN10)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))),(sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and COLUMN10=22399 and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'INPUT%')-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMN10,0) in(22399) and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,
--((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='BILL' or COLUMN04='Refund' or COLUMN04='Debit Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN10=22399 and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0)-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,
@brought Brought,
@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  isnull(FITABLE026.COLUMN10,0) in(22399) and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03
union all
--cst
Select 'CST' CI,'OUTPUT' type,FITABLE026.COLUMN08 taxid,FITABLE026.COLUMNA02 opunit,FITABLE026.COLUMNA03 acowner,'OUTPUT CST from Sales in the month' as A,(case when (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY')  then ('OUTPUT '+(select COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=FITABLE026.COLUMN10)+CAST(FITABLE026.COLUMN16 AS NVARCHAR(250))+'%')end )TAX,iif((FITABLE026.COLUMN04='CREDIT Memo'),-(sum(isnull(FITABLE026.COLUMN14,0))),(sum(isnull(FITABLE026.COLUMN14,0)))) as TotalAmount,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) as TaxAmount,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as totTax,
((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 between @FrDate and @ToDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')) as Amount,((select isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0) from FITABLE026 where (COLUMN04='INVOICE' or COLUMN04='CREDIT Memo' or COLUMN04='JOURNAL ENTRY') and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMN10,0) in(22400) and  FITABLE026.COLUMN03 < @FrDate and isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN17 like 'OUTPUT%')-(select (isnull(sum(COLUMN11),0)-isnull(sum(COLUMN12),0)) from FITABLE026 where COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=1109 and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and COLUMN03<@FrDate and COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE026.COLUMNA13,0)=0)) as Brought,@type as B,@paid as Paid from FITABLE026 left outer join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10 where isnull(FITABLE026.COLUMN10,0) in(22400) and isnull(FITABLE026.COLUMNA13,0)=0 and (FITABLE026.COLUMN04='INVOICE' or FITABLE026.COLUMN04='CREDIT Memo' or FITABLE026.COLUMN04='JOURNAL ENTRY') and  FITABLE026.COLUMN03 between @FrDate and @ToDate and  FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMNA03=@AcOwner and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FITABLE026.COLUMN17 like 'OUTPUT%' GROUP BY FITABLE026.COLUMN10,FITABLE026.COLUMN04,FITABLE026.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMNA02,FITABLE026.COLUMNA03
end
else
begin
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
select @type CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 
union all
--cst
select 'CST' CI,'' type,'' taxid,'' opunit,'' acowner,'' as A,'' TAX,0 as TotalAmount,0 as TaxAmount,0 as totTax,0 as Amount,@brought as Brought,@type as B,0 as Paid from FITABLE026 

end
end
end
END
		ELSE IF  @ReportName='APAgeing'
			BEGIN
			if(@Operating='')
			begin
			set @Operating=@OPUnit
			end
--EMPHCS797		A/P Aging  , A/P report  balances are getting summed. (Amout paid - Billed ) Done by rinivas 26/7/2015
				SELECT  --EMPHCS940 A/R and A/P Aging Report Changes by srinivas 13/8/2015
					(SELECT COLUMN05 FROM SATABLE001 WHERE COLUMN02=PUTABLE016.COLUMN07 AND COLUMNA03=@AcOwner) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,COLUMN04 Dtt,COLUMN09,
					(CASE
						WHEN 
							CONVERT(VARCHAR,PUTABLE016.COLUMN04,105)=convert(varchar,GETDATE(),105) 
							--EMPHCS1199	A/p AGING REPORT and A/R AGINGING reports not concidering paid amount BY RAJ.Jr 22/9/2015
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [CURRENT],COLUMN12 as AMOUNT,COLUMN13 as PAID, 
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-30 AND  GETDATE()-1
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [1-30],
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-60  AND GETDATE()-30
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [31-60],
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-90 AND  GETDATE()-60
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [61-90],
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-180 AND  GETDATE()-90
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [91-180],
						--EMPHCS940 A/R and A/P Aging Report Changes by srinivas 13/8/2015
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-360 AND  GETDATE()-180
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [181-360],
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-520 AND  GETDATE()-360
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [361-520],
					(CASE
						WHEN 
							PUTABLE016.COLUMN04 BETWEEN GETDATE()-5200 AND  GETDATE()-520
						then isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0) end)  [520 and Over]
						--EMPHCS797		A/P Aging  , A/P report  balances are getting summed. (Amout paid - Billed ) Done by rinivas 26/7/2015
			FROM PUTABLE016 WHERE isnull(PUTABLE016.COLUMNA13,0)=0 and PUTABLE016.COLUMN04 <=getdate()  AND PUTABLE016.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE016.COLUMNA03=@AcOwner AND PUTABLE016.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
--EMPHCS1199	A/p AGING REPORT and A/R AGINGING reports not concidering paid amount BY RAJ.Jr 22/9/2015
			group by PUTABLE016.COLUMN07,PUTABLE016.COLUMN04,PUTABLE016.COLUMN09,PUTABLE016.COLUMN04,PUTABLE016.COLUMN12,PUTABLE016.COLUMN13
			union all
			select (select COLUMN05 from SATABLE001 where COLUMN02=fitable020.COLUMN07 ) as COLUMN07,FORMAT(fitable020.COLUMN05,@DateF) COLUMN04,fitable020.COLUMN05 Dtt,fitable020.COLUMN04 COLUMN09,(CASE
						WHEN 
							CONVERT(VARCHAR,fitable020.COLUMN05,105)=convert(varchar,GETDATE(),105) 
						then -isnull(sum(fitable020.COLUMN18),0) end) [CURRENT],0.00 as AMOUNT,(fitable020.COLUMN18) as PAID, 
(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-30 AND  GETDATE()-1
						then -isnull(sum(fitable020.COLUMN18),0) end)  [1-30],
					(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-60  AND GETDATE()-29
						then -isnull(sum(fitable020.COLUMN18),0) end)  [31-60],
					(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-90 AND  GETDATE()-60
						then -isnull(sum(fitable020.COLUMN18),0) end)  [61-90],
					(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-180 AND  GETDATE()-90
						then -isnull(sum(fitable020.COLUMN18),0) end)  [91-180],
						--EMPHCS940 A/R and A/P Aging Report Changes by srinivas 13/8/2015
					(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-360 AND  GETDATE()-180
						then -isnull(sum(fitable020.COLUMN18),0) end)  [181-360],
					(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-520 AND  GETDATE()-360
						then -isnull(sum(fitable020.COLUMN18),0) end)  [361-520],
					(CASE
						WHEN 
							fitable020.COLUMN05 BETWEEN GETDATE()-5200 AND  GETDATE()-520
						then -isnull(sum(fitable020.COLUMN18),0) end)  [520 and Over]
from fitable020 fitable020
where COLUMN03=1363 and FITABLE020.COLUMN11=22305 and isnull((COLUMNA13),0)=0 and fitable020.COLUMN05 <=getdate()  AND fitable020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND fitable020.COLUMNA03=@AcOwner AND fitable020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  group by fitable020.COLUMN07,fitable020.COLUMN05,fitable020.COLUMN04,fitable020.COLUMN18 order by Dtt desc
		END
		

		 ELSE IF  @ReportName='ARAgeing'
		BEGIN
		if( @Operating!='')
		BEGIN
					SELECT  COLUMN07,(CASE WHEN  ((cast(isnull([days],0) as int)) =0 and cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)[CURRENT],
					(CASE WHEN ((cast(isnull([days],0) as int) <= 30 and (cast(isnull([days],0) as int))>=1) and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end) [1-30],
					(CASE WHEN  ((cast(isnull([days],0) as int)) <= 60 and cast(isnull([days],0) as int) >=31and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [31-60],
					(CASE WHEN ((cast(isnull([days],0) as int))<= 90 and cast(isnull([days],0) as int)>=61 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [61-90],
					(CASE WHEN ((cast(isnull([days],0) as int))<= 180 and(cast(isnull([days],0) as int))>= 91 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [91-180],
					(CASE WHEN  ((cast(isnull([days],0) as int))<= 360 and(cast(isnull([days],0) as int))>= 181 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [181-360],
					(CASE WHEN  ((cast(isnull([days],0) as int))<= 520 and (cast(isnull([days],0) as int))>=361 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [361-520],
					(CASE WHEN  ((cast([days] as int)) >=521 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [520 and Over],
					(CASE WHEN  ((cast([days] as int)) =0 and  cast([type] as varchar(250))='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [Credit/Advance]
					FROM(SELECT (SATABLE002.COLUMN05) COLUMN07,DATEDIFF(DAY,(iif(min(SALES.column08) is null,max(SALES1.column08),min(SALES.column08))),GETDATE()) [days],
						 ( (cast(sum(isnull(PUTABLE018.COLUMN09,0))as decimal(18,2))-cast(sum(isnull(PUTABLE018.COLUMN11,0))as decimal(18,2))-sum(isnull(SATABLE012.COLUMN06,0))-sum(isnull(SATABLE012.COLUMN13,0))))Amount,''[type] FROM PUTABLE018 
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner AND SATABLE009.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE009.COLUMNA13,0)=0 AND SATABLE009.COLUMNA02=PUTABLE018.COLUMNA02 AND SATABLE009.COLUMN05=PUTABLE018.COLUMN07
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					left join (select sum(isnull(SATABLE012.COLUMN06,0)) COLUMN06,COLUMN03,COLUMNA03,COLUMNA02,COLUMNA13,sum(isnull(COLUMN13,0))COLUMN13,sum(isnull(SATABLE012.COLUMN14,0))COLUMN14 ,sum(isnull(COLUMN09,0))COLUMN09 from SATABLE012  where  COLUMNA03=@AcOwner and COLUMNA02=@Operating and isnull(SATABLE012.COLUMNA13,0)=0 group by COLUMN03,COLUMNA03,COLUMNA02,COLUMNA13) SATABLE012 
					on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner AND SATABLE012.COLUMNA02=PUTABLE018.COLUMNA02 and isnull(SATABLE012.COLUMNA13,0)=0
					left join (select column08,COLUMN04,COLUMN05,COLUMNA02 from SATABLE009 where COLUMNA03=@AcOwner and isnull(SATABLE009.COLUMNA13,0)=0 and COLUMN13 not in('AMOUNT FULLY RECEIVED','FULLY RECEIVED')
					group by column08,COLUMN04,COLUMN05,COLUMNA02) SALES
					on SALES.COLUMN04=PUTABLE018.COLUMN05  AND SALES.COLUMNA02 =PUTABLE018.COLUMNA02  AND SALES.COLUMN05=PUTABLE018.COLUMN07
					left join (select column08,COLUMN04,COLUMN05,COLUMNA02 from SATABLE009 where COLUMNA03=@AcOwner and isnull(SATABLE009.COLUMNA13,0)=0 and COLUMN13  in('AMOUNT FULLY RECEIVED','FULLY RECEIVED')
					group by column08,COLUMN04,COLUMN05,COLUMNA02) SALES1
					on SALES1.COLUMN04=PUTABLE018.COLUMN05  AND SALES1.COLUMNA02 =PUTABLE018.COLUMNA02  AND SALES1.COLUMN05=PUTABLE018.COLUMN07
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  and isnull(SATABLE009.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','RECEIVE PAYMENT')
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner and PUTABLE018.COLUMNA02=@Operating
					and isnull(PUTABLE018.COLUMNA13,0)=0 and isnull(SATABLE012.COLUMNA13,0)=0
					group by SATABLE002.COLUMN05, SATABLE009.COLUMN04
					union all
					SELECT (SATABLE002.COLUMN05) COLUMN07,0 [days],
						 -max(isnull(CreditMemo.column15,0))-max(isnull(Advance.column19,0)) Amount,'Credit'[type] FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					left join (select sum(isnull(column12,0))+sum(isnull(column32,0)) column15,COLUMN03,COLUMNA03,COLUMN05 from SATABLE005 where COLUMN03='1330' and COLUMNA03=@AcOwner and isnull(SATABLE005.COLUMNA13,0)=0
					group by COLUMN03,COLUMNA03,COLUMN05) CreditMemo
					on CreditMemo.COLUMN05=PUTABLE018.COLUMN07  and CreditMemo.COLUMNA03=PUTABLE018.COLUMNA03
					  left join (select sum(cast(isnull(COLUMN10,0)as decimal(18,2))) COLUMN19,COLUMN03,COLUMNA03,COLUMN08 from FITABLE023 where COLUMN03='1386' and COLUMNA03=@AcOwner and isnull(FITABLE023.COLUMNA13,0)=0
					group by COLUMN03,COLUMNA03,COLUMN08) Advance
					on Advance.COLUMN08=PUTABLE018.COLUMN07  and Advance.COLUMNA03=PUTABLE018.COLUMNA03
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','RECEIVE PAYMENT')
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner and PUTABLE018.COLUMNA02=@Operating
					and isnull(PUTABLE018.COLUMNA13,0)=0 
					group by SATABLE002.COLUMN05 )a group by column07,[days],[type]
					having ( (cast(sum(isnull(a.Amount,0))as decimal(18,2))))!=0
END
		ELSE
		BEGIN
					SELECT  COLUMN07,(CASE WHEN  ((cast(isnull([days],0) as int)) =0 and cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)[CURRENT],
					(CASE WHEN ((cast(isnull([days],0) as int) <= 30 and (cast(isnull([days],0) as int))>=1) and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end) [1-30],
					(CASE WHEN  ((cast(isnull([days],0) as int)) <= 60 and cast(isnull([days],0) as int) >=31and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [31-60],
					(CASE WHEN ((cast(isnull([days],0) as int))<= 90 and cast(isnull([days],0) as int)>=61 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [61-90],
					(CASE WHEN ((cast(isnull([days],0) as int))<= 180 and(cast(isnull([days],0) as int))>= 91 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [91-180],
					(CASE WHEN  ((cast(isnull([days],0) as int))<= 360 and(cast(isnull([days],0) as int))>= 181 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [181-360],
					(CASE WHEN  ((cast(isnull([days],0) as int))<= 520 and (cast(isnull([days],0) as int))>=361 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [361-520],
					(CASE WHEN  ((cast([days] as int)) >=521 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [520 and Over],
					(CASE WHEN  ((cast([days] as int)) =0 and  cast([type] as varchar(250))='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [Credit/Advance]
					FROM(SELECT (SATABLE002.COLUMN05) COLUMN07,DATEDIFF(DAY,(iif(min(SALES.column08) is null,max(SALES1.column08),min(SALES.column08))),GETDATE()) [days],
						 ( (cast(sum(isnull(PUTABLE018.COLUMN09,0))as decimal(18,2))-cast(sum(isnull(PUTABLE018.COLUMN11,0))as decimal(18,2))-sum(isnull(SATABLE012.COLUMN06,0))-sum(isnull(SATABLE012.COLUMN13,0))))Amount,''[type] FROM PUTABLE018 
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner AND SATABLE009.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE009.COLUMNA13,0)=0 AND SATABLE009.COLUMNA02=PUTABLE018.COLUMNA02 AND SATABLE009.COLUMN05=PUTABLE018.COLUMN07
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					left join (select sum(isnull(SATABLE012.COLUMN06,0)) COLUMN06,COLUMN03,COLUMNA03,COLUMNA02,COLUMNA13,sum(isnull(COLUMN13,0))COLUMN13,sum(isnull(SATABLE012.COLUMN14,0))COLUMN14 ,sum(isnull(COLUMN09,0))COLUMN09 from SATABLE012  where  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE012.COLUMNA13,0)=0 group by COLUMN03,COLUMNA03,COLUMNA02,COLUMNA13) SATABLE012 
					on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner AND SATABLE012.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE012.COLUMNA13,0)=0
					left join (select column08,COLUMN04,COLUMN05,COLUMNA02 from SATABLE009 where COLUMNA03=@AcOwner and isnull(SATABLE009.COLUMNA13,0)=0 and COLUMN13 not in('AMOUNT FULLY RECEIVED','FULLY RECEIVED')
					group by column08,COLUMN04,COLUMN05,COLUMNA02) SALES
					on SALES.COLUMN04=PUTABLE018.COLUMN05  AND SALES.COLUMNA02 =PUTABLE018.COLUMNA02  AND SALES.COLUMN05=PUTABLE018.COLUMN07
					left join (select column08,COLUMN04,COLUMN05,COLUMNA02 from SATABLE009 where COLUMNA03=@AcOwner and isnull(SATABLE009.COLUMNA13,0)=0 and COLUMN13  in('AMOUNT FULLY RECEIVED','FULLY RECEIVED')
					group by column08,COLUMN04,COLUMN05,COLUMNA02) SALES1
					on SALES1.COLUMN04=PUTABLE018.COLUMN05  AND SALES1.COLUMNA02 =PUTABLE018.COLUMNA02  AND SALES1.COLUMN05=PUTABLE018.COLUMN07
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  and isnull(SATABLE009.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','RECEIVE PAYMENT')
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner and PUTABLE018.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					and isnull(PUTABLE018.COLUMNA13,0)=0 and isnull(SATABLE012.COLUMNA13,0)=0
					group by SATABLE002.COLUMN05, SATABLE009.COLUMN04
					union all
					SELECT (SATABLE002.COLUMN05) COLUMN07,0 [days],
						 -max(isnull(CreditMemo.column15,0))-max(isnull(Advance.column19,0)) Amount,'Credit'[type] FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					left join (select sum(isnull(column12,0))+sum(isnull(column32,0)) column15,COLUMN03,COLUMNA03,COLUMN05 from SATABLE005 where COLUMN03='1330' and COLUMNA03=@AcOwner and isnull(SATABLE005.COLUMNA13,0)=0
					group by COLUMN03,COLUMNA03,COLUMN05) CreditMemo
					on CreditMemo.COLUMN05=PUTABLE018.COLUMN07  and CreditMemo.COLUMNA03=PUTABLE018.COLUMNA03
					  left join (select sum(cast(isnull(COLUMN10,0)as decimal(18,2))) COLUMN19,COLUMN03,COLUMNA03,COLUMN08 from FITABLE023 where COLUMN03='1386' and COLUMNA03=@AcOwner and isnull(FITABLE023.COLUMNA13,0)=0
					group by COLUMN03,COLUMNA03,COLUMN08) Advance
					on Advance.COLUMN08=PUTABLE018.COLUMN07  and Advance.COLUMNA03=PUTABLE018.COLUMNA03
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','RECEIVE PAYMENT')
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner
					group by SATABLE002.COLUMN05 )a group by column07,[days],[type]
					having ( (cast(sum(isnull(a.Amount,0))as decimal(18,2))))!=0
END
		END
		 

		ELSE IF  @ReportName='ARAgeingDetailes'
			begin
		if( @SalesRep!='' AND @Operating!='')
BEGIN

				SELECT  
					 SATABLE002.COLUMN05 COLUMN07, 
					 SATABLE002.COLUMN11  AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= SATABLE002.COLUMN16) AS CITY, 
					 PUTABLE018.COLUMN04, PUTABLE018.COLUMN05, PUTABLE018.COLUMN09 as TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],

					  (CASE WHEN  CONVERT(VARCHAR,PUTABLE018.COLUMN04,105)=convert(varchar,GETDATE(),105) 
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end) as DUEAMOUNT,	 NULL RETUNNS,
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-30 AND  GETDATE()-1
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [1-30],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-60  AND GETDATE()-30
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [31-60],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-90 AND  GETDATE()-60
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [61-90],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-180 AND  GETDATE()-90
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [91-180],
					
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-360 AND  GETDATE()-180
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [181-360],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-520 AND  GETDATE()-360
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [361-520],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-5200 AND  GETDATE()-520
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [520 and Over]
						

					FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND SATABLE002.COLUMN22= @SalesRep  
					--AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					and isnull(SATABLE002.COLUMNA13,0)=0
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner and  SATABLE009.COLUMNA02=PUTABLE018.COLUMNA02
					--AND SATABLE009.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					and isnull(SATABLE009.COLUMNA13,0)=0
					left join SATABLE012 on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner 
					--AND SATABLE012.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					and isnull(SATABLE012.COLUMNA13,0)=0
					left join SATABLE005 on SATABLE005.COLUMN05=PUTABLE018.COLUMN07 and  SATABLE005.COLUMN03='' 
					--and SATABLE005.COLUMNA03=@AcOwner AND SATABLE005.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					and isnull(SATABLE005.COLUMNA13,0)=0
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','Payment')
					--and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					AND PUTABLE018.COLUMNA03=@AcOwner and PUTABLE018.COLUMNA02=@Operating
					
					group by PUTABLE018.COLUMN07,SATABLE002.COLUMN05,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN09,SATABLE005.COLUMN15,SATABLE002.COLUMN11,SATABLE002.COLUMN16
					
					having ((isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)))>0

					union all

                     SELECT 
					  (SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02=PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=@Operating) COLUMN07, 
					 (SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=@Operating) AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= (SELECT COLUMN16 FROM SATABLE002 WHERE 
					  (COLUMN02 = PUTABLE018.COLUMN07)   and COLUMNA03=@AcOwner AND COLUMNA02=@Operating)) AS CITY,
					   PUTABLE018.COLUMN04 Date,
					   PUTABLE018.COLUMN05 [COLUMN05], NULL TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],NULL DUEAMOUNT,cast(-isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))+cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) RETUNNS,
					   NULL [1-30],
					   NULL [31-60],
					   NULL [61-90],
					
					   NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
					   FROM PUTABLE018 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Credit Memo','RECEIVE PAYMENT')
					 --AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					 AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02=@Operating

					group by PUTABLE018.COLUMN07,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN14,PUTABLE018.COLUMN09

					ORDER BY PUTABLE018.COLUMN04   , CITY 
		END
			ELSE if( @SalesRep!='' )
BEGIN

				SELECT  
					 SATABLE002.COLUMN05 COLUMN07, 
					 SATABLE002.COLUMN11  AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= SATABLE002.COLUMN16) AS CITY, 
					 PUTABLE018.COLUMN04, PUTABLE018.COLUMN05, PUTABLE018.COLUMN09 as TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],

					  (CASE WHEN  CONVERT(VARCHAR,PUTABLE018.COLUMN04,105)=convert(varchar,GETDATE(),105) 
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end) as DUEAMOUNT,	 NULL RETUNNS,
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-30 AND  GETDATE()-1
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [1-30],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-60  AND GETDATE()-30
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [31-60],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-90 AND  GETDATE()-60
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [61-90],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-180 AND  GETDATE()-90
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [91-180],
					
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-360 AND  GETDATE()-180
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [181-360],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-520 AND  GETDATE()-360
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [361-520],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-5200 AND  GETDATE()-520
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [520 and Over]
						

					FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND SATABLE002.COLUMN22= @SalesRep  AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE002.COLUMNA13,0)=0
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner AND SATABLE009.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE009.COLUMNA13,0)=0
					left join SATABLE012 on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner AND SATABLE012.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE012.COLUMNA13,0)=0
					left join SATABLE005 on SATABLE005.COLUMN05=PUTABLE018.COLUMN07 and  SATABLE005.COLUMN03='' and SATABLE005.COLUMNA03=@AcOwner AND SATABLE005.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE005.COLUMNA13,0)=0
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','Payment')
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner or PUTABLE018.COLUMNA02=@Operating
					
					group by PUTABLE018.COLUMN07,SATABLE002.COLUMN05,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN09,SATABLE005.COLUMN15,SATABLE002.COLUMN11,SATABLE002.COLUMN16
					
					having ((isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)))>0

					union all

                     SELECT 
					  (SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02=PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=@Operating) COLUMN07, 
					 (SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=@Operating) AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= (SELECT COLUMN16 FROM SATABLE002 WHERE 
					  (COLUMN02 = PUTABLE018.COLUMN07)   and COLUMNA03=@AcOwner AND COLUMNA02=@Operating)) AS CITY,
					   PUTABLE018.COLUMN04 Date,
					   PUTABLE018.COLUMN05 [COLUMN05], NULL TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],NULL DUEAMOUNT,cast(-isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))+cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) 'RETUNNS/ADVANCES',
					   NULL [1-30],
					   NULL [31-60],
					   NULL [61-90],
					
					   NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
					   FROM PUTABLE018 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Credit Memo','RECEIVE PAYMENT')
					 AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02=@Operating
					
					group by PUTABLE018.COLUMN07,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN14,PUTABLE018.COLUMN09
					
					ORDER BY PUTABLE018.COLUMN04   , CITY 
		
		END
			
		ELSE if( @Operating!='')
			BEGIN

				SELECT  
					 SATABLE002.COLUMN05 COLUMN07, 
					 SATABLE002.COLUMN11  AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= SATABLE002.COLUMN16) AS CITY, 
					 PUTABLE018.COLUMN04, PUTABLE018.COLUMN05, PUTABLE018.COLUMN09 as TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],

					  (CASE WHEN  CONVERT(VARCHAR,PUTABLE018.COLUMN04,105)=convert(varchar,GETDATE(),105) 
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end) as DUEAMOUNT,	 NULL RETUNNS,
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-30 AND  GETDATE()-1
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [1-30],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-60  AND GETDATE()-30
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [31-60],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-90 AND  GETDATE()-60
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [61-90],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-180 AND  GETDATE()-90
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [91-180],
					
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-360 AND  GETDATE()-180
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [181-360],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-520 AND  GETDATE()-360
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [361-520],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-5200 AND  GETDATE()-520
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [520 and Over]
						

					FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner --AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					and isnull(SATABLE002.COLUMNA13,0)=0
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner and  SATABLE009.COLUMNA02=PUTABLE018.COLUMNA02 AND --SATABLE009.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and 
					isnull(SATABLE009.COLUMNA13,0)=0
					left join SATABLE012 on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner --AND SATABLE012.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					and isnull(SATABLE012.COLUMNA13,0)=0
					left join SATABLE005 on SATABLE005.COLUMN05=PUTABLE018.COLUMN07 and  SATABLE005.COLUMN03='' and SATABLE005.COLUMNA03=@AcOwner --AND 
					--SATABLE005.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE005.COLUMNA13,0)=0
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06  in ('Invoice','Payment')
					--and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					AND PUTABLE018.COLUMNA03=@AcOwner and PUTABLE018.COLUMNA02=@Operating
					
					group by PUTABLE018.COLUMN07,SATABLE002.COLUMN05,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN09,SATABLE005.COLUMN15,SATABLE002.COLUMN11,SATABLE002.COLUMN16
					
					having ((isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)))>0

					union all

                    SELECT 
					  (SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02=PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=@Operating) COLUMN07, 
					 (SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=@Operating) AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= (SELECT COLUMN16 FROM SATABLE002 WHERE 
					  (COLUMN02 = PUTABLE018.COLUMN07)   and COLUMNA03=@AcOwner AND COLUMNA02=@Operating)) AS CITY,
					   PUTABLE018.COLUMN04 Date,
					   PUTABLE018.COLUMN05 [COLUMN05], NULL TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],NULL DUEAMOUNT,cast(-isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))+cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) 'RETUNNS/ADVANCES',
					   NULL [1-30],
					   NULL [31-60],
					   NULL [61-90],
					
					   NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
					   FROM PUTABLE018 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Credit Memo','RECEIVE PAYMENT')
					 --AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					 AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02=@Operating
					
					group by PUTABLE018.COLUMN07,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN14,PUTABLE018.COLUMN09
					
					ORDER BY PUTABLE018.COLUMN04   , CITY 
		END
		
		else

		BEGIN
		SELECT  
					 SATABLE002.COLUMN05 COLUMN07, 
					 SATABLE002.COLUMN11  AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= SATABLE002.COLUMN16) AS CITY, 
					 PUTABLE018.COLUMN04, PUTABLE018.COLUMN05, PUTABLE018.COLUMN09 as TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],

					  (CASE WHEN  CONVERT(VARCHAR,PUTABLE018.COLUMN04,105)=convert(varchar,GETDATE(),105) 
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)
					-		isnull((SATABLE005.COLUMN15),0)
						  )) end) as DUEAMOUNT,	 NULL RETUNNS,
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-30 AND  GETDATE()-1
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [1-30],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-60  AND GETDATE()-30
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [31-60],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-90 AND  GETDATE()-60
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [61-90],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-180 AND  GETDATE()-90
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [91-180],
					
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-360 AND  GETDATE()-180
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [181-360],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-520 AND  GETDATE()-360
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [361-520],
					(CASE
						WHEN 
							PUTABLE018.COLUMN04 BETWEEN GETDATE()-5200 AND  GETDATE()-520
						then ( (isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)  )) end)  [520 and Over]
						

					FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02 and isnull(SATABLE002.COLUMNA13,0)=0
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner AND SATABLE009.COLUMNA02 =PUTABLE018.COLUMNA02 and isnull(SATABLE009.COLUMNA13,0)=0
					left join SATABLE012 on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner AND SATABLE012.COLUMNA02 =PUTABLE018.COLUMNA02 and isnull(SATABLE012.COLUMNA13,0)=0
					left join SATABLE005 on SATABLE005.COLUMN05=PUTABLE018.COLUMN07 and  SATABLE005.COLUMN03='' and SATABLE005.COLUMNA03=@AcOwner AND SATABLE005.COLUMNA02 =PUTABLE018.COLUMNA02 and isnull(SATABLE005.COLUMNA13,0)=0
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and isnull(SATABLE012.COLUMNA13,0)=0and isnull(SATABLE009.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','Payment')
					--and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					AND PUTABLE018.COLUMNA03=@AcOwner --OR PUTABLE018.COLUMNA02=@Operating
					
					group by PUTABLE018.COLUMN07,SATABLE002.COLUMN05,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN09,SATABLE005.COLUMN15,SATABLE002.COLUMN11,SATABLE002.COLUMN16
					
					having ((isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
					isnull((SATABLE005.COLUMN15),0)))>0

					union all

                    SELECT 
					  (SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02=PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=PUTABLE018.COLUMNA02) COLUMN07, 
					 (SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND COLUMNA02=PUTABLE018.COLUMNA02) AS PNNO, 
					 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= (SELECT COLUMN16 FROM SATABLE002 WHERE 
					  (COLUMN02 = PUTABLE018.COLUMN07)   and COLUMNA03=@AcOwner AND COLUMNA02=PUTABLE018.COLUMNA02)) AS CITY,
					   PUTABLE018.COLUMN04 Date,
					   PUTABLE018.COLUMN05 [COLUMN05], NULL TOTALAMOUNT,DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],NULL DUEAMOUNT,cast(-isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))+cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) 'RETUNNS/ADVANCES',
					   NULL [1-30],
					   NULL [31-60],
					   NULL [61-90],
					
					   NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
					   FROM PUTABLE018 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Credit Memo','RECEIVE PAYMENT')
					 --AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					 AND PUTABLE018.COLUMNA03=@AcOwner --AND PUTABLE018.COLUMNA02=@Operating

					group by PUTABLE018.COLUMN07,PUTABLE018.COLUMN04,PUTABLE018.COLUMN05,PUTABLE018.COLUMN11,PUTABLE018.COLUMN14,PUTABLE018.COLUMN09,PUTABLE018.COLUMNA02
					--HAVING PUTABLE018.COLUMN14>0
					ORDER BY PUTABLE018.COLUMN04   , CITY 
				

		END
		end


		ELSE IF  @ReportName='TrialBalance1'
		begin
		if(@Operating='')
		begin
		set @Operating=@OPUnit
		end
		else
		begin
		set @OPUnit=@Operating
		end
			declare @DSC decimal(18,2)
			set @DSC=(SELECT(iif(EXISTS(select sum(column07) from fitable036 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner and COLUMN04 <=@ToDate  AND COLUMN03='PAYMENT' AND COLUMN10=1049 AND ISNULL(COLUMNA13,0)=0),(select ISNULL(SUM(COLUMN07),0) from fitable036 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner and COLUMN04 <=@ToDate AND COLUMN03='PAYMENT' AND COLUMN10=1049 AND ISNULL(COLUMNA13,0)=0),0)))
		if(@ProjectName!='')
		BEGIN 
			select '' , FITABLE001.COLUMN04 Type,ISNULL(sum(MATABLE020.COLUMN07),0) Debit,ISNULL(sum(MATABLE020.COLUMN09),0) Credit from MATABLE020
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE MATABLE020.COLUMNA03=@AcOwner and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)--AND FITABLE036.COLUMN10=1078
			AND MATABLE020.COLUMN04 = @PFRDATE   
			group by FITABLE001.COLUMN04
			union ALL
			select '' , FITABLE001.COLUMN04 Type,-sum(FITABLE028.COLUMN10) Debit,(NULL) Credit from FITABLE028
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE028.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE028.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE028.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE028.COLUMN04 BETWEEN @FrDate AND @ToDate   AND FITABLE028.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  AND ISNULL(FITABLE028.COLUMNA13,0)=0 and isnull(FITABLE028.COLUMN10,0)>0
			group by FITABLE001.COLUMN04
			union ALL
			select '' , FITABLE001.COLUMN04 Type,sum(FITABLE028.COLUMN09) Debit,(NULL) Credit from FITABLE028
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE028.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE028.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE028.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE028.COLUMN04 BETWEEN @FrDate AND @ToDate AND FITABLE028.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)     AND ISNULL(FITABLE028.COLUMNA13,0)=0 and isnull(FITABLE028.COLUMN09,0)>0
			group by FITABLE001.COLUMN04
			union ALL
			select '' , FITABLE001.COLUMN04 Type,sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)) Debit,(NULL) Credit from FITABLE036
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE036.COLUMN04 BETWEEN @FrDate AND @ToDate   AND FITABLE036.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)   AND ISNULL(FITABLE036.COLUMNA13,0)=0 
			group by FITABLE001.COLUMN04
			union ALL
			--EMPHCS1390 rajasekhar reddy patakota 26/11/2015 P&l,balance sheet and trail balance report changes
			select  ' ' , FITABLE001.COLUMN04 Type,sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0)) Debit,(NULL) Credit from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1056) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate  AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  
			group by FITABLE001.COLUMN04 
			union ALL
			--EMPHCS1390 rajasekhar reddy patakota 26/11/2015 P&l,balance sheet and trail balance report changes
			select  ' ' , FITABLE001.COLUMN04 Type,(0) Debit,
			--(case when (sum(isnull(FITABLE025.COLUMN11,0))-sum(isnull(FITABLE025.COLUMN09,0)))>0 
			--then (sum(isnull(FITABLE025.COLUMN11,0))-sum(isnull(FITABLE025.COLUMN09,0)))   when (sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0)))>0
			--then (sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0))) end)
			sum(isnull(FITABLE025.COLUMN09,0))- sum(isnull(FITABLE025.COLUMN11,0)) Credit from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053,1050,1118,1129) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate  AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  
			group by FITABLE001.COLUMN04 
			union ALL
			select  ' ' , FITABLE001.COLUMN04 Type,(0) Debit,
			sum(isnull(FITABLE025.COLUMN09,0))- sum(isnull(FITABLE025.COLUMN11,0)) Credit   from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1052,1053,1050,1118,1129,1056,4663,4183) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select  ' ' , FITABLE001.COLUMN04 Type,(0) Debit,sum(isnull(FITABLE025.COLUMN11,0))-sum(isnull(FITABLE025.COLUMN09,0)) Credit from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08>4000  and FITABLE025.COLUMN04!='Inventory Adjustment'
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate  AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  
			group by FITABLE001.COLUMN04 
			union ALL
			select '' ,FITABLE001.COLUMN04  Type,(0) Debit,isnull(sum(FITABLE029.COLUMN09),0)-isnull(sum(FITABLE029.COLUMN10),0) Credit from FITABLE029 
					inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE029.COLUMN07  AND FITABLE001.COLUMNA03=@AcOwner where FITABLE029.COLUMN03!='22330' and   FITABLE029.COLUMN04 <= @ToDate AND FITABLE029.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   AND FITABLE029.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) and isnull(fitable029.columna13,0)=0  group by FITABLE001.COLUMN04 
					having  (isnull(sum(FITABLE029.COLUMN10),0)-isnull(sum(FITABLE029.COLUMN09),0))!=0 
				union 
			select '' ,COLUMN04 Type,(0) Debit,sum(Column08) Credit from fitable001 where columna03=@AcOwner and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null) and column07=22330  and Column08>0  group by COLUMN04
		
			
			--select '' ,COLUMN04 Type,(0) Debit,sum(Column10) Credit from fitable001 where columna03=@AcOwner and column07=22330  and Column10>0  group by COLUMN04
			union ALL
			select  distinct '',a.COLUMN04 Type,(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Debit,
				(0) Credit
				from  FITABLE034 b 
				inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
				--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
				where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND b.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) and b.COLUMN04 between @FrDate AND @ToDate
				group by a.COLUMN04
			union ALL
			select  distinct '',a.COLUMN04 Type,0 Debit,
				(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Credit
				from  FITABLE035 b 
				inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
				--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
				where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  and b.COLUMN04 between @FrDate AND @ToDate
				group by a.COLUMN04
			union ALL
			select  distinct '',b.COLUMN17 Type,(0) Debit,
			(sum(cast(isnull(b.COLUMN11,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))) Credit
			from  FItable026 b 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(b.COLUMN17,'') NOT LIKE('INPUT%') and b.COLUMN03 between @FrDate AND @ToDate   AND b.COLUMN19 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  group by b.COLUMN17 having sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0))!=0
			union ALL
			--EMPHCS1492 - Input VAT is not showing in Blance sheet , General Ledger , Trail Balance. BY GNANESHWAR ON 2/1/2016
			select  distinct '',b.COLUMN17 Type,
			(sum(cast(isnull(b.COLUMN11,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))) Debit,(0) Credit
			from  FItable026 b 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(b.COLUMN17,'')  LIKE('INPUT%') and b.COLUMN03 between @FrDate AND @ToDate   AND b.COLUMN19 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  group by b.COLUMN17 having sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0))!=0
			union ALL
			--select  distinct '',b.COLUMN17 Type,sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0)) Debit,
			--(0) Credit
			--from  FItable026 b  
			----EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			--     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02=@Operating and b.COLUMN17 LIKE('INPUT%') and b.COLUMN03 between @FrDate AND @ToDate and (column10 not in (22399,22400,22401) or column10 is null)  group by B.COLUMN17
			--union ALL
			--select  distinct 'Advance Receipt','Advance Received' Type,(null) Debit,
			--(sum(cast(b.COLUMN11 as decimal(18,2)))) Credit
			--from  putable018 b  
			----EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			--     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02=@Operating and b.COLUMN06='RECEIVE PAYMENT' and b.COLUMN04 between @FrDate AND @ToDate 
			--union ALL
			--select  distinct 'Advance Paid','Advance Amount' Type,(sum(cast(b.COLUMN13 as decimal(18,2)))) Debit,
			--(0) Credit
			--from  putable016 b  
			----EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			--     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02=@Operating and b.COLUMN06='ADVANCE' and b.COLUMN04 between @FrDate AND @ToDate 
			--union ALL
			select  distinct a.COLUMN04,max(m.COLUMN04) Type,
			 (case when (sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))))<0 
			then (case when sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))=0 then sum(cast(isnull(b.COLUMN13,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2))) else sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))) end)  
			else 0 end)Debit,
			(case when (sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2)))   
			else 0 end)Credit
			from fitable001 a left outer join putable016 b on  a.COLUMN02=b.COLUMN03 and a.COLUMN07=22263 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  and b.COLUMN04 <=@ToDate   AND b.COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  group by a.COLUMN04
			union ALL
			select  distinct a.COLUMN04,max(m.COLUMN04) Type, -(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end)Debit,
			 
			(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end)Credit
			from fitable001 a left outer join putable019 b on a.COLUMN02=b.COLUMN03 and a.COLUMN07=22266 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and isnull(a.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner   AND b.COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)   and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND b.COLUMN04<=@ToDate group by a.COLUMN04
				 --where isnull(a.COLUMNA13,0)=0 and a.COLUMN07=22266  and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner   AND b.COLUMN04<=@ToDate group by a.COLUMN04
			union ALL
			--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
			select  distinct a.COLUMN04,a.COLUMN04 Type, -(case when (sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2)))   
			else 0 end)Debit,
			(case when (sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2)))   
			else 0 end)Credit
			from fitable001 a left outer join FITABLE052 b on a.COLUMN02=b.COLUMN03 and a.COLUMN07=22409 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			where isnull(b.COLUMNA13,0)=0 and isnull(a.COLUMNA13,0)=0 and  (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.COLUMNA02 is null) AND b.COLUMNA03=@AcOwner   AND b.COLUMN16 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)   and (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) or b.COLUMNA02 is null)  AND b.COLUMN04<=@ToDate group by a.COLUMN04
			union ALL
			select  distinct a.COLUMN04,max(m.COLUMN04) Type,(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end) Debit,
			-(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end) Credit
			from fitable001 a left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND b.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)   AND b.COLUMN04 between @FrDate AND @ToDate group by a.COLUMN04
			union ALL
			select  distinct a.COLUMN04,max(m.COLUMN04) Type,(case when (sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))-@DSC)   
			else 0 end) Debit,
			-(case when (sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))+@DSC)   
			else 0 end) Credit
			from fitable001 a left outer join PUTABLE018 b on a.COLUMN02=b.COLUMN03 and a.COLUMN07=22264 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner  AND b.COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)   and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND b.COLUMN04 <=@ToDate group by a.COLUMN04
		END
		else
		BEGIN 
			--;With MyTable AS
			select a,Type,sum(Debit)Debit,sum(Credit)Credit,max(id)id,max(typ)typ from(
			select iif(isnull(f1.column15,0)=0,max(a),m2.column04)a,f1.column04 Type,sum(Debit)Debit,sum(Credit)Credit,max(f1.column02) id,f1.column07 typ from
			(select M2.COLUMN04 a, MATABLE020.COLUMN08 Type,ISNULL(sum(MATABLE020.COLUMN07),0) Debit,ISNULL(sum(MATABLE020.COLUMN09),0) Credit ,
			MATABLE020.COLUMN08 id ,MATABLE020.COLUMN08 typ 
			--null id ,null typ  
			from MATABLE020
			left join fiTABLE001 f1 on f1.column02=MATABLE020.column08 and f1.columna03=MATABLE020.COLUMNA03
			left join MATABLE002 m2 on m2.column02=isnull(f1.column15,MATABLE020.COLUMN10)
			--inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE MATABLE020.COLUMNA03=@AcOwner  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)--AND FITABLE036.COLUMN10=1078
			AND MATABLE020.COLUMN04 = @PFRDATE   
			group by MATABLE020.COLUMN08,M2.COLUMN04-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07 
			union ALL
			select 'Fixed Assets' a, FITABLE028.COLUMN03 Type,-sum(FITABLE028.COLUMN10) Debit,(NULL) Credit ,FITABLE028.COLUMN03 id ,FITABLE028.COLUMN03 typ  from FITABLE028
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE028.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE028.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE028.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE028.COLUMN04 BETWEEN @PFRDATE AND @ToDate   AND ISNULL(FITABLE028.COLUMNA13,0)=0 and isnull(FITABLE028.COLUMN10,0)!=0
			group by FITABLE028.COLUMN03-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select 'Fixed Assets' a, FITABLE028.COLUMN03 Type,sum(FITABLE028.COLUMN09) Debit,(NULL) Credit ,FITABLE028.COLUMN03 id ,FITABLE028.COLUMN03 typ  from FITABLE028
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE028.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE028.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE028.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE028.COLUMN04 BETWEEN @PFRDATE AND @ToDate   AND ISNULL(FITABLE028.COLUMNA13,0)=0 and isnull(FITABLE028.COLUMN09,0)!=0
			group by FITABLE028.COLUMN03-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select 'Direct Expenses' a, FITABLE036.COLUMN10 Type,sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)) Debit,(NULL) Credit ,FITABLE036.COLUMN10 id ,FITABLE036.COLUMN10 typ  from FITABLE036
			---LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE036.COLUMN04 BETWEEN @PFRDATE AND @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0 
			and isnull(FITABLE036.COLUMN10,0) in(1133,1118,1049,1078) group by FITABLE036.COLUMN10 --,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select 'Indirect Expenses' a, FITABLE036.COLUMN10 Type,sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)) Debit,(NULL) Credit ,FITABLE036.COLUMN10 id ,FITABLE036.COLUMN10 typ  from FITABLE036
			---LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner --AND FITABLE036.COLUMN10=1078
			AND FITABLE036.COLUMN04 BETWEEN @PFRDATE AND @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0 
			and isnull(FITABLE036.COLUMN10,0) not in(1133,1118,1049,1078) group by FITABLE036.COLUMN10 --,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			--select  ' Indirect Income ' a, FITABLE025.COLUMN08 Type,sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0)) Debit,(NULL) Credit ,FITABLE025.COLUMN08 id ,FITABLE025.COLUMN08 typ  from FITABLE025 
			----LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			--where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN04 != 'Inventory Adjustment'
			--AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			--AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate and FITABLE025.COLUMN08 in(4663,4183,5436,1078)
			--group by FITABLE025.COLUMN08--  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			--union ALL
			select  'Direct Income  ' a, FITABLE025.COLUMN08 Type,(0) Debit,
			--(case when (sum(isnull(FITABLE025.COLUMN11,0))-sum(isnull(FITABLE025.COLUMN09,0)))>0 
			--then (sum(isnull(FITABLE025.COLUMN11,0))-sum(isnull(FITABLE025.COLUMN09,0)))   when (sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0)))>0
			--then (sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0))) end) 
			sum(isnull(FITABLE025.COLUMN09,0))- sum(isnull(FITABLE025.COLUMN11,0)) Credit ,FITABLE025.COLUMN08 id ,FITABLE025.COLUMN08 typ  from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053,1050,1118,1129) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @PFRDATE AND @ToDate --and FITABLE025.column04  in('Bill','Invoice')
			group by FITABLE025.COLUMN08--  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select  'Indirect Income  ' a, FITABLE025.COLUMN08 Type,
			(0)  Debit,sum(isnull(FITABLE025.COLUMN11,0))- sum(isnull(FITABLE025.COLUMN09,0)) Credit,FITABLE025.COLUMN08 id ,FITABLE025.COLUMN08 typ  from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN04='Inventory Adjustment' and FITABLE025.COLUMN08 not in(1052,1053,1050,1118,1129,1056,4663,4183,1132) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @PFRDATE AND @ToDate --and FITABLE025.column04 not in('Bill','Invoice')
			group by FITABLE025.COLUMN08--  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select  'Indirect Income  ' a, FITABLE025.COLUMN08 Type,(0) Debit,sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0)) Credit ,FITABLE025.COLUMN08 id ,FITABLE025.COLUMN08 typ  from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08>4000 and FITABLE025.COLUMN04!='Inventory Adjustment'  and FITABLE001.COLUMN07 not in (22406) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @PFRDATE AND @ToDate
			group by FITABLE025.COLUMN08--  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
			union ALL
			select  'Indirect Income ' a, FITABLE025.COLUMN08 Type,sum(isnull(FITABLE025.COLUMN09,0))-sum(isnull(FITABLE025.COLUMN11,0)) Debit, 0 Credit ,FITABLE025.COLUMN08 id ,FITABLE025.COLUMN08 typ  from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE001.COLUMN07 in(22406) and FITABLE001.COLUMN02 not in(1056,1132)
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 BETWEEN @PFRDATE AND @ToDate 
			group by FITABLE025.COLUMN08
			union ALL
			--SATYA ISSUES
			select 'Capital Accounts' a,FITABLE029.COLUMN07  Type,(0) Debit,isnull(sum(FITABLE029.COLUMN09),0)-isnull(sum(FITABLE029.COLUMN10),0) Credit ,FITABLE029.COLUMN07 id ,FITABLE029.COLUMN07 typ  from FITABLE029 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE029.COLUMN07  AND FITABLE001.COLUMNA03=@AcOwner 
					where FITABLE029.COLUMN03!='22330' and  FITABLE029.COLUMN04 <= @ToDate AND FITABLE029.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(fitable029.columna13,0)=0 
					group by FITABLE029.COLUMN07--  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
					having  (isnull(sum(FITABLE029.COLUMN10),0)-isnull(sum(FITABLE029.COLUMN09),0))!=0 
				union all
			--select 'Capital Accounts' a,COLUMN02 Type,(0) Debit,sum(Column08) Credit ,FITABLE001.COLUMN02 id ,FITABLE001.COLUMN07 typ  from fitable001 where columna03=@AcOwner and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null) and column07=22330  and Column08>0  group by COLUMN04  ,COLUMN02 ,COLUMN07
			----select '' ,COLUMN04 Type,(0) Debit,sum(Column10) Credit from fitable001 where columna03=@AcOwner and column07=22330  and Column10>0  group by COLUMN04
			--union ALL
			select  distinct 'Current Assets' a,b.column10 Type,(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Debit,
				(0) Credit ,b.column10 id ,b.column10 typ 
				from  FITABLE034 b 
				--inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
				where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and b.COLUMN04 between @PFRDATE AND @ToDate
				group by b.column10-- ,a.COLUMN02 ,a.COLUMN07
			union ALL
			--select  distinct 'Non Current Assets' a,b.column10 Type,(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Debit,
			--	(0) Credit ,b.column10 id ,b.column10 typ 
			--	from  FITABLE060 b 
			--	--inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
			--	where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and b.COLUMN04 between @FrDate AND @ToDate
			--	group by b.column10-- ,a.COLUMN02 ,a.COLUMN07
			--union ALL
			select  'Current Liabilities'a,b.COLUMN03  Type,
			 -(case when (sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))))<0 
			then (case when sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))=0 then sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))) else sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))) end)  
			else 0 end)Debit,
			(case when (sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2)))   
			else 0 end)Credit  ,b.COLUMN03  id ,b.COLUMN03  typ 
			from putable016 b-- left outer join putable016 b on  a.COLUMN02=b.COLUMN03 and a.COLUMN07=22263 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMN04 <=@ToDate  group by b.COLUMN03
			union ALL
			select  'Non Current Assets' a,b.column10 Type,(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Debit,
				(0) Credit ,b.column10 id ,b.column10 typ 
				from  FITABLE060 b 
				--inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
				where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and b.COLUMN04 between @PFRDATE AND @ToDate
				group by b.column10 --,a.COLUMN04-- ,a.COLUMN07
			union ALL
			select  distinct 'Non Current Liabilities' a,b.column10 Type,0 Debit,
				(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Credit ,b.column10 id ,b.column10 typ 
				from  FITABLE063 b 
				--inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
				where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and b.COLUMN04 between @PFRDATE AND @ToDate
				group by b.column10-- ,a.COLUMN02 ,a.COLUMN07
			union ALL
			select  distinct 'Other Incomes' a,b.column10 Type,0 Debit,
				(sum(cast(isnull(b.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN08,0) as decimal(18,2)))) Credit ,b.column10 id ,b.column10 typ 
				from  FITABLE035 b 
				--inner join fitable001 a  on a.column02=b.column10 and a.columna03=@AcOwner
				where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and b.COLUMN04 between @PFRDATE AND @ToDate
				group by b.column10-- ,a.COLUMN02 ,a.COLUMN07
		--		 select a,f1.column04 Type,Debit,Credit,f1.column02 id,f1.column07 typ from Mytable 
		--LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=type AND F1.COLUMNA03=@AcOwner
			union ALL
		select   'Bank Accounts'a,max(b.COLUMN03) Type, -(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end)Debit,
			 
			(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end)Credit
		,b.COLUMN03 id ,b.COLUMN03 typ 

			from putable019 b-- a left outer join putable019 b on a.COLUMN02=b.COLUMN03 and a.COLUMN07=22266 and a.COLUMNA03=@AcOwner 
			--inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner  and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND b.COLUMN04<=@ToDate 
				 group by b.COLUMN03
				 --where isnull(a.COLUMNA13,0)=0 and a.COLUMN07=22266  and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner   AND b.COLUMN04<=@ToDate group by a.COLUMN04
			union ALL
			--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
			select   'Cash Accounts'a,b.COLUMN03 Type, -(case when (sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2)))   
			else 0 end)Debit,
			(case when (sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN11,0) as decimal(18,2))-cast(isnull(b.COLUMN12,0) as decimal(18,2)))   
			else 0 end)Credit
			,b.COLUMN03 id ,b.COLUMN03 typ 
			from FITABLE052 b-- left outer join FITABLE052 b on a.COLUMN02=b.COLUMN03 and a.COLUMN07=22409 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			where isnull(b.COLUMNA13,0)=0 and  (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.COLUMNA02 is null) AND b.COLUMNA03=@AcOwner and (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) or b.COLUMNA02 is null)  AND b.COLUMN04<=@ToDate 
			group by b.COLUMN03
			union ALL
			select  'Current Assets'a,max(b.COLUMN03) Type,(case when (sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))-@DSC   
			else 0 end) Debit,
			-(case when (sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN09,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))+@DSC   
			else 0 end) Credit
		,b.COLUMN03 id ,b.COLUMN03 typ 
			from PUTABLE018 b-- left outer join PUTABLE018 b on a.COLUMN02=b.COLUMN03 and a.COLUMN07=22264 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMN04 <=@ToDate  group by b.COLUMN03
				 
			union ALL
			select distinct 'Inventory Assets'a,b.column03 Type,(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end) Debit,
			-(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			else 0 end) Credit
			,b.COLUMN03 id ,b.COLUMN03 typ 
			from fitable001 a left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262 inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND b.COLUMN04<= @ToDate and b.column06 in('Inventory Adjustment','Opening Balance') group by b.COLUMN03
		 ,a.COLUMN02 ,a.COLUMN07)Q
				 --select iif(isnull(f1.column15,0)=0,max(a),m2.column04)a,f1.column04 Type,sum(Debit)Debit,sum(Credit)Credit,max(f1.column02) id,f1.column07 typ from Mytable 
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=q.type AND F1.COLUMNA03=@AcOwner
		LEFT OUTER join MATABLE002 m2 on m2.COLUMN02=f1.column15 --AND F1.COLUMNA03=56590
		group by f1.column15,m2.column04,f1.column04,f1.column07
			union ALL
			--select distinct 'Stock'a,'Closing Stock' Type,(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))<0 
			--then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			--else 0 end) Debit,
			--(case when (sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))))>0 
			--then sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2)))   
			--else 0 end) Credit
			--,a.COLUMN02 id ,a.COLUMN07 typ 
			--from fitable001 a left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262 inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			--     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND b.COLUMN04<= @ToDate group by a.COLUMN04
		 --,a.COLUMN02 ,a.COLUMN07
			--union ALL
			select a, Type,sum(Debit),sum(Credit),max(id),typ from(select  'Current Liabilities'a,b.COLUMN17 Type,(0) Debit,
			(sum(cast(isnull(b.COLUMN11,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))) Credit
			--,null id ,null typ 
			 ,(  b.column08) id  
			-- ,(select max(COLUMN07) from FITABLE001 where COLUMN04 in  (b.column17) AND FITABLE001.COLUMNA03=@AcOwner) typ 
			,'22383' typ
			from  FItable026 b  
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(b.COLUMN17,'') NOT LIKE('INPUT%')  and isnull(b.COLUMN17,'') NOT LIKE('OUTPUT%') and isnull(b.COLUMN17,'') NOT IN ('CGST RCM Payable','SGST RCM Payable','IGST RCM Payable','UTGST RCM Payable','Cess RCM Payable') and b.COLUMN03 between @PFRDATE AND @ToDate   group by b.COLUMN17,b.column08 having sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0))!=0
			union ALL
			select  'Current Liabilities'a,b.COLUMN17 Type,(0) Debit,
			(sum(cast(isnull(b.COLUMN11,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))) Credit
			--,null id ,null typ 
			 ,(  b.column08) id  
			-- ,(select max(COLUMN07) from FITABLE001 where COLUMN04 in  (b.column17) AND FITABLE001.COLUMNA03=@AcOwner) typ 
			,'223832' typ
			from  FItable026 b  
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(b.COLUMN17,'')  LIKE('OUTPUT%') and b.COLUMN03 between @PFRDATE AND @ToDate   group by b.COLUMN17,b.column08 having sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0))!=0
			union ALL
			select  'Current Liabilities'a,b.COLUMN17 Type,
			(sum(cast(isnull(b.COLUMN11,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))) Debit,(0) Credit
,(  b.column08) id   ,'223831' typ  
			from  FItable026 b  
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(b.COLUMN17,'')  LIKE('INPUT%') and b.COLUMN03 between @PFRDATE AND @ToDate   group by b.COLUMN17,b.column08 having sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0))!=0
				 union ALL
			select  'Current Liabilities'a,b.COLUMN17 Type,
			(0) Debit,(sum(cast(isnull(b.COLUMN11,0) as decimal(18,2)))-sum(cast(isnull(b.COLUMN12,0) as decimal(18,2)))) Credit
,(  b.column08) id   ,22383 typ  
			from  FItable026 b  
			     where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(b.COLUMN17,'')  IN ('CGST RCM Payable','SGST RCM Payable','IGST RCM Payable','UTGST RCM Payable','Cess RCM Payable') and b.COLUMN03 between @PFRDATE AND @ToDate   group by b.COLUMN17,b.column08 having sum(isnull(b.COLUMN11,0))-sum(isnull(b.COLUMN12,0))!=0
				 )a
		group by a,typ, Type
		UNION ALL
		
		SELECT ''A,'Difference in Openning Balance' TYPE,IIF((SUM(DR)-SUM(CR))>0,(SUM(DR)-SUM(CR)),0) Debit, IIF((SUM(DR)-SUM(CR))<0,(SUM(CR)-SUM(DR)),0) Credit ,NULL id,NULL typ FROM
		(SELECT SUM(IIF(COLUMN07 IN(22266,22409,22264,22402,22403,22344,22410,22408,22262),(COLUMN08),0))CR,
		SUM(IIF(COLUMN07 IN(22330,22263,22383,22411,22405,22407),(COLUMN08),0))DR,''A FROM FITABLE001 
		WHERE COLUMNA03 = @AcOwner AND COLUMN10!=0  AND COLUMN09<=@ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN07
		UNION ALL
		SELECT SUM(IIF(COLUMN08 = 3000,(COLUMN09-column10),0))CR,SUM(IIF(COLUMN08 = 2000 ,(COLUMN09-column10),0))DR,''A FROM FITABLE018 
		WHERE COLUMN04 <= @ToDate and COLUMNA03 = @AcOwner and COLUMN02 > 3256 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN08
		UNION ALL
					SELECT SUM(COLUMN10)CR,SUM(COLUMN11)DR,''A FROM PUTABLE017 
					WHERE COLUMNA03 = @AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					AND PUTABLE017.COLUMN04 <= @ToDate    AND PUTABLE017.COLUMN06 in ('Inventory Adjustment' )  AND ISNULL(PUTABLE017.COLUMNA13,0)=0
					
		)T GROUP BY T.A HAVING  SUM(T.DR)-SUM(T.CR)!=0)R
		group by a,Type
		--union ALL
		--	select  'Vendors (Creditors)'a,max(m.COLUMN04) Type,
		--	 0 Debit,
		--	 sum(cast(isnull(b.COLUMN12,0) as decimal(18,2))-cast(isnull(b.COLUMN13,0) as decimal(18,2)))   
		--	Credit 
		--,a.COLUMN02 id ,a.COLUMN07 typ 
		--	from fitable001 a left outer join putable016 b on  a.COLUMN02=b.COLUMN03 and a.COLUMN07=22263 and a.COLUMNA03=@AcOwner inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
		-- where isnull(b.COLUMNA13,0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  and b.COLUMN04 <=@ToDate group by a.COLUMN04 ,a.COLUMN02 ,a.COLUMN07
		END
		end

		ELSE IF  @ReportName='TrialBalance'
		begin
		if(@Operating='')
		begin
		set @Operating=@OPUnit
		end
		else
		begin
		set @OPUnit=@Operating
		end
			BEGIN 
			--select top 1 @PFRDATE=column04 from FITABLE048 where COLUMNA03=@AcOwner and column09 ='1'  order by COLUMN01 desc
		;With MyTable AS(
			select MATABLE020.COLUMN10 a, MATABLE020.COLUMN08 column08,ISNULL((MATABLE020.COLUMN07),0) Debit,ISNULL((MATABLE020.COLUMN09),0) Credit ,
			MATABLE020.COLUMN08 id
			--,NULL Trans,MATABLE020.COLUMN04 Date,NULL type 
			from MATABLE020
			left join fiTABLE001 f1 on f1.column02=MATABLE020.column08 and f1.columna03=MATABLE020.COLUMNA03 and isnull(f1.columna13,0) = 0
			left join MATABLE002 m2 on m2.column02=isnull(f1.column15,MATABLE020.COLUMN10)
			WHERE MATABLE020.COLUMNA03=@AcOwner  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)--AND FITABLE036.COLUMN10=1078
			AND MATABLE020.COLUMN04 = @PFRDATE and MATABLE020.COLUMN10 not in(22405)
			---group by MATABLE020.COLUMN08,M2.COLUMN04-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07 
			union ALL
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,sum((ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0)))Debit, 0 Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22403,22410,22402) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @PFRDATE AND @ToDate
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,sum((ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0)))Debit, 0 Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22264) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,sum((ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0)))Debit,0  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22344,22408) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate --and column11 != 1078
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			--union ALL
			--select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))Debit,0  Credit
		 --,FITABLE025.COLUMN11 id 
		 ----,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 --from FITABLE064  FITABLE025
			--where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22344,22408) AND FITABLE025.COLUMNA03=@AcOwner
			--AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			--AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate and column11 = 1078
			--	 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL
			
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,0 Debit,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22405) AND FITABLE025.COLUMN11 in(1052,1053,1050,1118,1129,1154)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL
			
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,0 Debit,sum((ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0)))  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22405,22406,22407) AND FITABLE025.COLUMN11 not in(1052,1053,1050,1118,1129,1056,4663,4183,1132,8358,1154)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate  and FITABLE025.COLUMN03='Inventory Adjustment'
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,0 Debit,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type 
		  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22405,22407) AND FITABLE025.COLUMN11  not in(1052,1053,1050,1118,1129,1154)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate  and FITABLE025.COLUMN03!='Inventory Adjustment'
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL 

			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))) Debit,0  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22406) AND FITABLE025.COLUMN11  not in(1056,1132)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate 
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL 
			select '22405' Income, '1154' column08,0 Debit,sum(Debit)  Credit
		 ,'1154' id from(
			select '22405' Income, '1154' column08,sum(ISNULL(FITABLE025.column14,0))-sum(ISNULL(FITABLE025.column15,0)) Debit,0  Credit
		 ,'1154' id 
		 --,NULL Trans, NULL Date,iif((sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)))>0,'Profit','Loss') Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22405,22406) and FITABLE025.COLUMN11 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078,1132,6158,8358,1154)  and FITABLE025.COLUMN03!='Inventory Adjustment'   AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and dateadd(day,-1,@FrDate)  group by FITABLE025.COLUMNA03 having sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)) != 0
			
			union ALL 
			select '22405' Income, '1154' column08,ISNULL(-ISNULL(SUM(FITABLE025.column14),0)+ISNULL(SUM(FITABLE025.column15),0),0) Debit,0  Credit
		 ,'1154' id 
		 --,NULL Trans, NULL Date,iif((sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)))>0,'Profit','Loss') Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22405,22406) and FITABLE025.COLUMN11 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078,1132,6158,8358,1154) and FITABLE025.COLUMN03='Inventory Adjustment'    AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and dateadd(day,-1,@FrDate)  group by FITABLE025.COLUMNA03 having sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)) != 0
			
			union ALL 
			select '22405' Income, '1154' column08,(ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0)) Debit,0  Credit
		 ,'1154' id 
		 --,NULL Trans, NULL Date,iif((sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)))>0,'Profit','Loss') Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22405)  and FITABLE025.COLUMN11 in(1050,1052,1053,1154)    AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and dateadd(day,-1,@FrDate)  group by FITABLE025.COLUMNA03 having sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)) != 0
			union ALL 
			select '22405' Income, '1154' column08,-(ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0)) Debit,0  Credit
		 ,'1154' id 
		 --,NULL Trans, NULL Date,iif((sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)))>0,'Profit','Loss') Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22406)  and FITABLE025.COLUMN11 in(4663,1078,5436,6158,8358)    AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and dateadd(day,-1,@FrDate)  group by FITABLE025.COLUMNA03 having sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)) != 0
			
			union ALL 
			select '22405' Income, '1154' column08,(ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0)) Debit,0  Credit
		 ,'1154' id 
		 --,NULL Trans, NULL Date,iif((sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)))>0,'Profit','Loss') Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and   FITABLE025.COLUMN10 in(22407,22344,22408)     AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and dateadd(day,-1,@FrDate)  group by FITABLE025.COLUMNA03 having sum(ISNULL(FITABLE025.column15,0))-sum(ISNULL(FITABLE025.column14,0)) != 0
			union ALL
				select '22405' Income, '1154' column08, (ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) Debit,0  Credit ,'1154' id  from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND PUTABLE017.COLUMN04  between @PTODATE and dateadd(day,-1,@FrDate)     AND (PUTABLE017.COLUMN06 != 'Inventory Adjustment' AND PUTABLE017.COLUMN06 != 'Opening Balance' AND PUTABLE017.COLUMN06 != 'OP' AND PUTABLE017.COLUMN06 != 'JOURNAL ENTRY')   AND ISNULL(PUTABLE017.COLUMNA13,0)=0 
			union ALL
				select '22405' Income, '1154' column08, (ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) Debit,0  Credit ,'1154' id  from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND PUTABLE017.COLUMN04  between @PTODATE and dateadd(day,-1,@FrDate)     AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' or  PUTABLE017.COLUMN06 = 'Opening Balance' or PUTABLE017.COLUMN06 = 'OP')   AND ISNULL(PUTABLE017.COLUMNA13,0)=0 
			union ALL
				select '22405' Income, '1154' column08, -(ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) Debit,0  Credit ,'1154' id  from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND PUTABLE017.COLUMN04  between @PFRDATE and dateadd(day,-1,@FrDate)     AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' or  PUTABLE017.COLUMN06 = 'Opening Balance' or PUTABLE017.COLUMN06 = 'OP')   AND ISNULL(PUTABLE017.COLUMNA13,0)=0 
			
			)a
			union ALL 
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,0 Debit,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22330)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate  
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL 

			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,0 Debit,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22411,22383)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @PFRDATE AND @ToDate  
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL 
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,0 Debit,sum((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  
		 from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22263)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL 
			
			select FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,sum((ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0))) Debit,0  Credit
		 ,FITABLE025.COLUMN11 id 
		 --,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type 
		  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22266,22409)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate  
				 group by FITABLE025.COLUMN10,FITABLE025.COLUMN11
			union ALL 

			select '22262' Income,a.column08 column08, sum(cast(isnull(a.Debit,0) as decimal(18,2))) Debit,
			0 Credit
			,a.id id from(
			select '22262' Income,b.column03 column08, sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))) Debit,
			0 Credit
			,b.COLUMN03 id
			--,cast(b.COLUMN05 as nvarchar(25)) Trans,b.COLUMN04 Date ,b.COLUMN06 Type 
			from fitable001 a 
			left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262 
			inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMN04 between @PTODATE and dateadd(day,-1,@FrDate)  and b.column06 not in('Opening Balance','Inventory Adjustment','op','JOURNAL ENTRY') 
				 group by b.column03
			union ALL 

			select '22262' Income,b.column03 column08, sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))) Debit,
			0 Credit
			,b.COLUMN03 id
			--,cast(b.COLUMN05 as nvarchar(25)) Trans,b.COLUMN04 Date ,b.COLUMN06 Type 
			from fitable001 a 
			left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262 
			inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMN04 between @PTODATE and dateadd(day,-1,@FrDate) and b.column06 in('Opening Balance','Inventory Adjustment','op','JOURNAL ENTRY') 
				 group by b.column03
				 union ALL 

			select '22262' Income,b.column03 column08, sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))) Debit,
			0 Credit
			,b.COLUMN03 id
			--,cast(b.COLUMN05 as nvarchar(25)) Trans,b.COLUMN04 Date ,b.COLUMN06 Type 
			from fitable001 a 
			left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262 
			inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMN04 = @PTODATE  and b.column06 in('Opening Balance','Inventory Adjustment','op') 
				 group by b.column03
				 union ALL 

			select '22262' Income,b.column03 column08, sum(cast(isnull(b.COLUMN10,0) as decimal(18,2))-cast(isnull(b.COLUMN11,0) as decimal(18,2))) Debit,
			0 Credit
			,b.COLUMN03 id
			--,cast(b.COLUMN05 as nvarchar(25)) Trans,b.COLUMN04 Date ,b.COLUMN06 Type 
			from fitable001 a 
			left outer join PUTABLE017 b on a.COLUMN02=b.COLUMN03  and a.COLUMNA03=@AcOwner and a.COLUMN07=22262 
			inner join MATABLE002 m on m.COLUMN02=a.COLUMN07 
			     where isnull(b.COLUMNA13,0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMN04 = @PFRDATE and @PFRDATE >  dateadd(day,-1,@FrDate) and b.column06 in('Opening Balance','Inventory Adjustment','op') 
				 group by b.column03
				 )a group by a.column08,a.id
		)
		 select m2.COLUMN04 a,f1.column04 Type,--Mytable.Trans Trans,FORMAT(Mytable.Date,@DateF)Date,Mytable.Type Typ,
		 case when (sum(Mytable.Debit)-sum(Mytable.Credit))>0 then sum(Mytable.Debit)-sum(Mytable.Credit) else 0 end  Debit,
		 -case when (sum(Mytable.Debit)-sum(Mytable.Credit))<0 then sum(Mytable.Debit)-sum(Mytable.Credit) else 0 end  Credit
		 --,DATENAME(month, Mytable.Date) AS 'Month' 
		 ,id,a typ from Mytable
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=id AND F1.COLUMNA03=@AcOwner and isnull(f1.columna13,0) = 0
		LEFT OUTER join MATABLE002 m2 on m2.COLUMN02= iif(f1.column15 is null or f1.column15 = '' ,f1.column07,f1.column15) --AND F1.COLUMNA03=@AcOwner
		group by id,MyTable.a,f1.column04,f1.column07,m2.column04  HAVING  (sum(Mytable.Debit)-sum(Mytable.Credit))!=0
		UNION ALL
		
		SELECT ''a,'Difference in Openning Balance' Type,--NULL Trans,NULL Date,'Difference in Openning Balance'Typ,
		IIF((SUM(DR)-SUM(CR))>0,(SUM(DR)-SUM(CR)),0) Debit, IIF((SUM(DR)-SUM(CR))<0,(SUM(CR)-SUM(DR)),0) Credit ,
		NULL id,NULL typ FROM
		(SELECT SUM(IIF(COLUMN07 IN(22266,22409,22264,22402,22403,22344,22410,22408,22262),(COLUMN08),0))CR,
		SUM(IIF(COLUMN07 IN(22330,22263,22383,22411,22405,22407),(COLUMN08),0))DR,''A FROM FITABLE001 
		WHERE COLUMNA03 = @AcOwner AND COLUMN10!=0  AND COLUMN09 between @PTODATE and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN07
		UNION ALL
		SELECT SUM(IIF(COLUMN08 = 3000,(COLUMN09-column10),0))CR,SUM(IIF(COLUMN08 = 2000 ,(COLUMN09-column10),0))DR,''A FROM FITABLE018 
		WHERE COLUMN04  between @PTODATE and  @ToDate and COLUMNA03 = @AcOwner and COLUMN02 > 3256 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN08
		UNION ALL
		SELECT SUM(COLUMN10)CR,SUM(COLUMN11)DR,''A FROM PUTABLE017 
		WHERE COLUMNA03 = @AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
		AND PUTABLE017.COLUMN04  = @PTODATE AND PUTABLE017.COLUMN06 in ('Inventory Adjustment' )  AND ISNULL(PUTABLE017.COLUMNA13,0)=0
		UNION ALL
		SELECT SUM(COLUMN10)CR,SUM(COLUMN11)DR,''A FROM PUTABLE017 
		WHERE COLUMNA03 = @AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
		AND PUTABLE017.COLUMN04  = @PFRDATE AND PUTABLE017.COLUMN06 in ('Inventory Adjustment' )  AND ISNULL(PUTABLE017.COLUMNA13,0)=0
					
		)T GROUP BY T.A HAVING  SUM(T.DR)-SUM(T.CR)!=0
		 order by a

		END
		END
		ELSE IF  @ReportName='GeneralLedger'
			BEGIN
				SELECT p.[COLUMN02] AS BankRegisterID,s.[COLUMN04] AS Bank,cast(p.[COLUMN04] as date) AS Date,s.[COLUMN08] AS OppeningBalance,
					p.[COLUMN05] AS BillInvoice,p.[COLUMN06] AS Type,p.[COLUMN09] AS Account,p.COLUMN14 AS Cheque,p.[COLUMN10] AS PaymentAmount,
					p.[COLUMN11] AS DepositAmount,p.[COLUMN12] AS BalanceAmount,
					(CASE  
							WHEN 
								p.COLUMN06='BILL PAYMENT' 
								then (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN07)
							when	
								p.COLUMN06='PAYMENT'
								then null
								else (select COLUMN05 from SATABLE002 where COLUMN02=p.COLUMN07) 
						end) Name From	PUTABLE019 p 
					inner join  FITABLE001 s on s.COLUMN02=p.COLUMN03
					--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			                WHERE isnull(p.COLUMNA13,0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
		
		END
		ELSE IF  @ReportName='ProfitAndLoss1'
		begin
			declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
			select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner AND COLUMN08= '1' order by COLUMN01 desc
			if(@Operating='')
		    begin
		    set @Operating=@OPUnit
		    end
		    else
		    begin
		    set @OPUnit=@Operating
		    end
			if(@ProjectName!='')
			BEGIN
			--EMPHCS1232 rajasekhar reddy patakota 10/7/2015 Balance sheet not working
			select 1 a, ' Indirect Income' Income, FITABLE001.COLUMN04 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0)) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1052,1053,1049,1056,4231,1050,1118,1128,1132) and FITABLE025.COLUMN04!='Inventory Adjustment'
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			union all
			select 1 a, ' Cost of Sale' Income, FITABLE001.COLUMN04 column08,SUM(ISNULL(FITABLE025.column11,0))-SUM(ISNULL(FITABLE025.column09,0)) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0  and FITABLE025.COLUMN04='Inventory Adjustment'
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			select 1 a, 'Stock in hand' Income, FITABLE001.COLUMN04 column08,SUM(ISNULL(PUTABLE017.column10,0))-SUM(ISNULL(PUTABLE017.column11,0)) column10,FITABLE001.COLUMN08 Opening from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND PUTABLE017.COLUMN04 BETWEEN @FrDate AND @ToDate   AND PUTABLE017.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			union all
			--EMPHCS1232 rajasekhar reddy patakota 10/7/2015 Balance sheet not working
			select 1 a, ' Direct Income' Income, FITABLE001.COLUMN04 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0)) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1118,1128)
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			union all
			--EMPHCS1232 rajasekhar reddy patakota 10/7/2015 Balance sheet not working
			select 1 a, ' Direct Income' Income, FITABLE001.COLUMN04 column08,ISNULL(SUM(FITABLE025.column09),0)-SUM(ISNULL(FITABLE025.column11,0)) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			union all
			--EMPHCS1299 BY GNANESHWAR ON 13/10/2015 Journal Entry Chages on balance sheet,profitandloss
			select 1 a, ' Direct Income' Income, FITABLE001.COLUMN04 column08,(SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0))) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
		
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1050) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner    AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			union all

		----EMPHCS1252 gnaneshwar on 7/10/2015  Inserting Data Into Cost Of Sales When Resourse Consumption Created in Project
		--	select 1 a, ' Income' Income, FITABLE001.COLUMN04 column08,ISNULL(SUM(FITABLE025.column09),0) column10 from FITABLE025 
		--	LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053) 
		--	AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
		--	AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
		--	group by FITABLE001.COLUMN04 
			--union all
			
			select 1 a, ' Indirect Income' Income, FITABLE001.COLUMN04 column08,-(ISNULL(SUM(FITABLE025.column09),0)) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
			--EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1049) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
			union all
			select 1 a, ' Indirect Income' Income, FITABLE001.COLUMN04 column08,(sum(cast(isnull(FITABLE035.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(FITABLE035.COLUMN08,0) as decimal(18,2)))) column10
		    ,FITABLE001.COLUMN08 Opening  from FITABLE035 
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE035.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE035.COLUMNA13,0)=0  AND 
			FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE035.COLUMNA03=@AcOwner AND FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE035.COLUMN04 BETWEEN @FrDate AND @ToDate
			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN08 
		 ,FITABLE001.COLUMN02  ,FITABLE001.COLUMN07 
			union all
			    --EMPHCS900 rajasekhar reddy patakota 10/08/2015 After deleting bill , the taxes information is not getting deleted.
			--        select 1 a, 'Cost of Sale' Income, FITABLE001.COLUMN04 column08,-(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0)) column10,FITABLE001.COLUMN08 Opening from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1056,4663) AND 
			--FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			--AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			--group by FITABLE001.COLUMN04,FITABLE001.COLUMN08 
			--union all 
				select 2 a, ' Direct Expenses' Income, FITABLE001.COLUMN04 column08,-(ISNULL(sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)),0)) column10,FITABLE001.COLUMN08 Opening from FITABLE036
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner AND FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE036.COLUMN04 BETWEEN @FrDate AND @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0    AND FITABLE036.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) and isnull(FITABLE036.COLUMN10,0) in(1133,1118)
			group by FITABLE001.COLUMN04,FITABLE001.COLUMN08  having (sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)))>0 
			 union all
				select 2 a, ' Indirect Expenses' Income, FITABLE001.COLUMN04 column08,-(ISNULL(sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)),0)) column10,FITABLE001.COLUMN08 Opening from FITABLE036
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner AND FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE036.COLUMN04 BETWEEN @FrDate AND @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0    AND FITABLE036.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) and isnull(FITABLE036.COLUMN10,0) not in(1133,1118)
			group by FITABLE001.COLUMN04,FITABLE001.COLUMN08  having (sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)))>0 order by a
		END
			else
			BEGIN
			
		;With MyTable AS
(	
			--EMPHCS1232 rajasekhar reddy patakota 10/7/2015 Balance sheet not working
					  select 1 a, ' Indirect Income' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0))  column10
		 ,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1052,1053,1049,1056,4231,1050,1118,4663,1128,4183,5436,1078,1132,6158) and FITABLE025.COLUMN04!='Inventory Adjustment' AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE025.COLUMN08  
			union all
			        select 1 a, 'Cost of Sale' Income, FITABLE025.COLUMN08 column08,-(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0)) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(4663,4183,5436,1078,6158) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE025.COLUMN08 
			union all 
			--select 1 a, ' Cost of Sale' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column11,0))-SUM(ISNULL(FITABLE025.column09,0)) column10
		 --,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			----LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			--where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN04='Inventory Adjustment'
			--AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			--AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			--group by FITABLE025.COLUMN08  
			--union all
			select 1 a, 'Stock in hand' Income, 'Stock in hand' column08,SUM(ISNULL(PUTABLE017.column10,0))-SUM(ISNULL(PUTABLE017.column11,0)) column10,PUTABLE017.COLUMN03 id,1 typ,0 Opening from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND PUTABLE017.COLUMN04 <= @ToDate 
			group by FITABLE001.COLUMN04 ,PUTABLE017.COLUMN03 
			union all
			select 1 a, ' Direct Income' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0)) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1118,1128) --and FITABLE025.COLUMNa03!=56655
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE025.COLUMN08  
			union all
			select 1 a, ' Direct Income' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0)) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			--LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053)  --and FITABLE025.COLUMNa03!=56655
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE025.COLUMN08  
			union all
			select 1 a, ' Direct Income' Income, FITABLE025.COLUMN08 column08,(SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0))) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			        --inner join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
					where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1050)   AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE025.COLUMN08
			union all
			select 1 a, ' Indirect Income' Income, FITABLE025.COLUMN08 column08,-(ISNULL(SUM(FITABLE025.column09),0)) column10
		 ,FITABLE025.COLUMN08 id ,1 typ,0 Opening  from FITABLE025 
			        --inner join FITABLE001 on FITABLE025.COLUMN08=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner 
					where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1049) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE025.COLUMN03 BETWEEN @FrDate AND @ToDate
			group by FITABLE025.COLUMN08  
			union all
			select 1 a, ' Indirect Income' Income, FITABLE035.COLUMN10 column08,(sum(cast(isnull(FITABLE035.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(FITABLE035.COLUMN08,0) as decimal(18,2)))) column10
		 ,FITABLE035.COLUMN10 id ,1 typ,0 Opening  from FITABLE035 
			        --inner join FITABLE001 on FITABLE025.COLUMN08=FITABLE035.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner 
					where isnull(FITABLE035.COLUMNA13,0)=0  AND 
			FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE035.COLUMNA03=@AcOwner AND FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND FITABLE035.COLUMN04 BETWEEN @FrDate AND @ToDate
			group by FITABLE035.COLUMN10
			union all
			--select 2 a, ' Opening Stock' Income, ' Opening Stock' column08,-(SUM(ISNULL(PUTABLE017.column10,0))-SUM(ISNULL(PUTABLE017.column11,0))) column10,PUTABLE017.COLUMN03  id,1 typ,0 Opening from PUTABLE017 
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			--AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner-- AND PUTABLE017.COLUMNA03!=56655 --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			--AND PUTABLE017.COLUMN04<= @FrDate    AND PUTABLE017.COLUMN06 != 'Inventory Adjustment'
			--group by FITABLE001.COLUMN04 ,PUTABLE017.COLUMN03 
			
			--union all
			select 2 a, ' Opening Stock' Income, ' Opening Stock' column08,-(SUM(ISNULL(PUTABLE017.column10,0))-SUM(ISNULL(PUTABLE017.column11,0))) column10,PUTABLE017.COLUMN03  id,1 typ,0 Opening from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA03!=56655 --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
			AND PUTABLE017.COLUMN04 <= @ToDate   AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' OR PUTABLE017.COLUMN06 = 'Opening Balance')    --AND PUTABLE017.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			group by FITABLE001.COLUMN04 ,PUTABLE017.COLUMN03 
			union all
				select 2 a, 'Direct Expenses' Income, FITABLE036.COLUMN10 column08,-(ISNULL(sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)),0)) column10 
		,FITABLE036.COLUMN10 id ,1 typ,0 Opening  from FITABLE036
			--LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner 
			AND FITABLE036.COLUMN04 BETWEEN @FrDate AND @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0 AND FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(FITABLE036.COLUMN10,0) in(1133,1118,1049) --and FITABLE036.COLUMNa03!=56655
			group by FITABLE036.COLUMN10    having (sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)))!=0 
			union all
				select 2 a, 'Indirect Expenses' Income, FITABLE036.COLUMN10 column08,-(ISNULL(sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)),0)) column10 
		,FITABLE036.COLUMN10 id ,1 typ,0 Opening  from FITABLE036
			--LEFT OUTER join FITABLE001 on FITABLE025.COLUMN08=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner 
			AND FITABLE036.COLUMN04 BETWEEN @FrDate AND @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0 AND FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and isnull(FITABLE036.COLUMN10,0) not in(1133,1118,1049)
			group by FITABLE036.COLUMN10    having (sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)))!=0 
		) select a,Income,f1.column04 COLUMN08,sum(Mytable.column10) column10,id,f1.column07 typ,
		 f1.column08 Opening from Mytable 
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=id AND F1.COLUMNA03=@AcOwner group by id,MyTable.a,MyTable.Income,f1.column04,f1.column07,f1.column08
		--inner join FITABLE001 f2 on F2.COLUMN08=FITABLE035.COLUMN10 AND F2.COLUMNA03=@AcOwner
		 order by a
		END
		end
		ELSE IF  @ReportName='ProfitAndLoss'
		begin
			--declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
			select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner and column09= '1' order by COLUMN01 desc
			if(@Operating='')
		    begin
		    set @Operating=@OPUnit
		    end
		    else
		    begin
		    set @OPUnit=@Operating
		    end
			BEGIN
			
		;With MyTable AS
(	
			select 1 a, m2.COLUMN04 Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))  column10
		 ,FITABLE025.COLUMN11 id ,1 typ,0 Opening,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=FITABLE025.COLUMN11 AND F1.COLUMNA03=@AcOwner and isnull(f1.columna13,0) = 0
			LEFT OUTER join MATABLE002 m2 on m2.COLUMN02= iif(f1.column15 is null ,f1.column07,f1.column15) --AND F1.COLUMNA03=@AcOwner
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22405,22407) and FITABLE025.COLUMN03!='Inventory Adjustment' AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate AND FITABLE025.COLUMN11 != 1154
			union all
	 		select 1 a, ' Cost of Sale' Income, FITABLE025.COLUMN11 column08,((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  column10
		 ,FITABLE025.COLUMN11 id ,1 typ,0 Opening,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN11 in(4183) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate
			union all
			select 1 a, ' Cost of Sale' Income, FITABLE025.COLUMN11 column08,-((ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0)))  column10
		 ,FITABLE025.COLUMN11 id ,1 typ,0 Opening,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN11 in(4663,1078,5436,6158) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate
			union all
			select 1 a, 'Stock in hand' Income, '1000' column08,(ISNULL(PUTABLE017.column10,0))-(ISNULL(PUTABLE017.column11,0)) column10,PUTABLE017.COLUMN03 id,1 typ,0 Opening
			,cast(PUTABLE017.COLUMN05 as nvarchar(25)) Trans,PUTABLE017.COLUMN04 Date,PUTABLE017.COLUMN06 from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND PUTABLE017.COLUMN04 between @PTODATE and @ToDate

			union all
			--select 1 a, ' Direct Income' Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))  column10
		 --,FITABLE025.COLUMN11 id ,1 typ,0 Opening,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			--where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in(22407) AND FITABLE025.COLUMNA03=@AcOwner
			--AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			--AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate
			--union all
			select 2 a, ' Opening Stock' Income, '1000' column08,-((ISNULL(PUTABLE017.column10,0))-(ISNULL(PUTABLE017.column11,0))) column10,PUTABLE017.COLUMN03  id,1 typ,0 Opening 
			,cast(PUTABLE017.COLUMN05 as nvarchar(25)) Trans,PUTABLE017.COLUMN04 Date,PUTABLE017.COLUMN06 from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA03!=56655 --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND PUTABLE017.COLUMN04 between @PTODATE and dateadd(day,-1,@FrDate)   AND (PUTABLE017.COLUMN06 != 'Inventory Adjustment' and PUTABLE017.COLUMN06 != 'Opening Balance' and PUTABLE017.COLUMN06 != 'OP')    --AND PUTABLE017.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			union all
			select 2 a, ' Opening Stock' Income, '1000' column08,-((ISNULL(PUTABLE017.column10,0))-(ISNULL(PUTABLE017.column11,0))) column10,PUTABLE017.COLUMN03  id,1 typ,0 Opening 
			,cast(PUTABLE017.COLUMN05 as nvarchar(25)) Trans,PUTABLE017.COLUMN04 Date,PUTABLE017.COLUMN06 from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA03!=56655 --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND PUTABLE017.COLUMN04 between @PTODATE and @FrDate   AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' OR PUTABLE017.COLUMN06 = 'Opening Balance' OR PUTABLE017.COLUMN06 = 'OP' OR PUTABLE017.COLUMN06 = 'JOURNAL ENTRY')    --AND PUTABLE017.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			union all
			--select 2 a, ' Opening Stock' Income, '1000' column08,-((ISNULL(PUTABLE017.column10,0))-(ISNULL(PUTABLE017.column11,0))) column10,PUTABLE017.COLUMN03  id,1 typ,0 Opening 
			--,cast(PUTABLE017.COLUMN05 as nvarchar(25)) Trans,PUTABLE017.COLUMN04 Date,PUTABLE017.COLUMN06 from PUTABLE017 
			--LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			--AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA03!=56655 --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			--AND PUTABLE017.COLUMN04 <= @ToDate   AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' OR PUTABLE017.COLUMN06 = 'Opening Balance')    --AND PUTABLE017.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			--union all
			select 2 a, 'Direct Expenses' Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))  column10
		 ,FITABLE025.COLUMN11 id ,1 typ,0 Opening,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in(22344,22408) and FITABLE025.COLUMN11 in(1133,1118,1049,1133) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate
			union all
			select 2 a, 'Indirect Expenses' Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))  column10
		 ,FITABLE025.COLUMN11 id ,1 typ,0 Opening,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in(22344,22408) and FITABLE025.COLUMN11 not in(1133,1118,1049,1078) AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @FrDate AND @ToDate
			) select a,Income,f1.column04 COLUMN08,cast(sum(Mytable.column10) as decimal(18,2)) column10,id,f1.column07 typ,
		 sum(f1.column08) Opening
		 --,Mytable.Trans,FORMAT(Mytable.Date,@DateF)Date,Mytable.Type,DATENAME(month, Mytable.Date) AS 'Month' 
		 from Mytable 
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=id AND F1.COLUMNA03=@AcOwner 
		group by id,MyTable.a,MyTable.Income,f1.column04,f1.column07,f1.column08
		 order by a


		END
		end

		ELSE IF  @ReportName='ProfitAndLossByProject'
			BEGIN
			
		exec(';With MyTable AS
(	
					  select 1 a, ''Income'' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0))  column10
		 ,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project  from FITABLE025 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1052,1053,1049,1056,4231,1050,1118,4663,1128,4183,5436,1078) and FITABLE025.COLUMN04!=''Inventory Adjustment''
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12  
			union all
			select 1 a, ''Cost of Sale'' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column11,0))-SUM(ISNULL(FITABLE025.column09,0)) column10
		 ,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project from FITABLE025 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN04=''Inventory Adjustment''
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12  
			union all
			select 1 a, ''Income'' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0)) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project  from FITABLE025 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1118,1128)
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12  
			union all
			select 1 a, ''Income'' Income, FITABLE025.COLUMN08 column08,SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0)) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project  from FITABLE025 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12  
			union all
			select 1 a, ''Income'' Income, FITABLE025.COLUMN08 column08,(SUM(ISNULL(FITABLE025.column09,0))-SUM(ISNULL(FITABLE025.column11,0))) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project  from FITABLE025 
					where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1050) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12
			union all
			select 1 a, ''Income'' Income, FITABLE025.COLUMN08 column08,-(ISNULL(SUM(FITABLE025.column09),0)) column10
		 ,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project  from FITABLE025 
					where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1049) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12  
			union all
			select 1 a, ''Income'' Income, FITABLE035.COLUMN10 column08,(sum(cast(isnull(FITABLE035.COLUMN07,0) as decimal(18,2)))-sum(cast(isnull(FITABLE035.COLUMN08,0) as decimal(18,2)))) column10
		 ,FITABLE035.COLUMN10 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE035.COLUMN11,'''') Project  from FITABLE035 
					where isnull(FITABLE035.COLUMNA13,0)=0  AND 
			FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE035.COLUMNA03='''+@AcOwner+''' AND FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE035.COLUMN04 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE035.COLUMN10,FITABLE035.COLUMN11
			union all
			        select 1 a, ''Cost of Sale'' Income, FITABLE025.COLUMN08 column08,-(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0)) column10 
		,FITABLE025.COLUMN08 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE025.COLUMN12,'''') Project  from FITABLE025 
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1056,4663,4183,5436,1078) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE025.COLUMNA03='''+@AcOwner+''' AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			AND FITABLE025.COLUMN03 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''
			group by FITABLE025.COLUMN08,FITABLE025.COLUMN12 
			union all 
				select 2 a, ''Expenses'' Income, FITABLE036.COLUMN10 column08,-(ISNULL(sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)),0)) column10 
		,FITABLE036.COLUMN10 id ,1 typ,0 Opening,iif('''+@ProjectName+'''!='''',FITABLE036.COLUMN11,'''') Project  from FITABLE036
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND FITABLE036.COLUMNA03='''+@AcOwner+''' 
			AND FITABLE036.COLUMN04 BETWEEN '''+@FrDate+''' AND '''+@ToDate+'''   AND ISNULL(FITABLE036.COLUMNA13,0)=0 AND FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)
			group by FITABLE036.COLUMN10,FITABLE036.COLUMN11    having (sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)))!=0 
		) select a,Income,f1.column04 COLUMN08,Mytable.column10 column10,id,f1.column07 typ,
		 f1.column08 Opening,Project from Mytable
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=id AND F1.COLUMNA03='''+@AcOwner+''' '+@whereStr+'
		 order by a ') 
		END


		ELSE IF  @ReportName='BalanceSheet1'
		begin
		if(@Operating='')
		begin
		set @Operating=@OPUnit
		end
		else
		begin
		set @OPUnit=@Operating
			set @DSC=(SELECT(iif(EXISTS(select sum(column07) from fitable036 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner and COLUMN04 <=@ToDate  AND COLUMN03='PAYMENT' AND COLUMN10=1049 AND ISNULL(COLUMNA13,0)=0),(select ISNULL(SUM(COLUMN07),0) from fitable036 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner and COLUMN04 <=@ToDate AND COLUMN03='PAYMENT' AND COLUMN10=1049 AND ISNULL(COLUMNA13,0)=0),0)))
		end
		DECLARE @PROFIT decimal(18,2)
			if(@ProjectName!='')
			BEGIN
set @profit=(select(select ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0) column10 from FITABLE025 
			--EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078)  and FITABLE025.COLUMN04!='Inventory Adjustment' 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner  AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(select ISNULL(-ISNULL(SUM(FITABLE025.column09),0)+ISNULL(SUM(FITABLE025.column11),0),0) column10 from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078) and FITABLE025.COLUMN04='Inventory Adjustment' 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner  AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1050) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate  AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) 
			AND FITABLE025.COLUMN03 <=cast( @ToDate as date)
			)+(
			select -(ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1049) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner    AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			        select -(ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1056,4663,4183,5436,1078) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner   AND FITABLE025.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE035.column07),0)-ISNULL(SUM(FITABLE035.column08),0),0)) column10 from FITABLE035 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE035.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE035.COLUMNA13,0)=0 --and FITABLE025.COLUMN08 in(1052,1053) 
			AND FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE035.COLUMNA03=@AcOwner 
			AND FITABLE035.COLUMN04 <= @ToDate
			)+(
				select -(ISNULL(ISNULL(sum(FITABLE036.COLUMN07),0)-ISNULL(sum(FITABLE036.COLUMN08),0),0)) column10 from FITABLE036
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner 
			AND FITABLE036.COLUMN04 <= @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0   AND FITABLE036.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
			 --having (ISNULL(sum(ISNULL(FITABLE036.COLUMN07,0))-sum(ISNULL(FITABLE036.COLUMN08,0)),0))!=0
			  ))
			 select 'Assets' a,'Current Assets' B,'Fixed Assets' Income, FITABLE001.COLUMN04 as column08,sum(isnull(FITABLE028.column09,0))-sum(isnull(FITABLE028.column10,0)) column10 from FITABLE028 
					inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE028.COLUMN03  AND FITABLE001.COLUMNA03=@AcOwner where    FITABLE028.COLUMN04 BETWEEN @FrDate AND @ToDate AND FITABLE028.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   AND FITABLE028.COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) group by FITABLE001.COLUMN04 
						union all
				select 'Assets' a,'Current Assets' B,'Fixed Assets' Income, FITABLE001.COLUMN04 as column08,sum(isnull(MATABLE020.column07,0)) column10 from MATABLE020
					inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner where   MATABLE020.COLUMN04 = @PFRDATE   AND MATABLE020.COLUMNA03=@AcOwner AND FITABLE001.COLUMN07=22403 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) group by FITABLE001.COLUMN04 
					union all
					select 'Assets' a,'Current Assets' B, 'Current Assets' Income, FITABLE001.COLUMN04 column08,sum(isnull(FITABLE034.column07,0))-sum(isnull(FITABLE034.column08,0)) column10 from FITABLE034 
					inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE034.COLUMN10  AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE034.COLUMNA13,0)=0 AND  FITABLE034.COLUMN04 BETWEEN @FrDate AND @ToDate AND FITABLE034.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   AND FITABLE034.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) group by FITABLE001.COLUMN04 
						union all 
					select 'Assets' a,'Current Assets' B, 'Current Assets' Income, FITABLE001.COLUMN04 column08,sum(isnull(MATABLE020.column07,0)) column10 from MATABLE020
					inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22402 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) group by FITABLE001.COLUMN04 
					union all 
					select 'Assets' a,'Current Assets' B, 'Account Rceivable' Income, FITABLE001.COLUMN04 column08,sum(isnull(PUTABLE018.COLUMN09,0))-sum(isnull(PUTABLE018.COLUMN11,0))+isnull((select sum(isnull(MATABLE020.column07,0))+sum(isnull(MATABLE020.column09,0))-@DSC column10 from MATABLE020
					 where MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=3000 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)),0) column10 from PUTABLE018
			                inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE018.COLUMN03  AND  FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE018.COLUMNA13,0)=0  AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMN04 <= @ToDate AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   AND PUTABLE018.COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)  group by FITABLE001.COLUMN04 
					union all
					--select 'Assets' a,'Current Assets' B, 'Account Rceivable' Income, FITABLE001.COLUMN04 column08,sum(isnull(MATABLE020.column07,0))+sum(isnull(MATABLE020.column09,0)) column10 from MATABLE020
					--inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=3000 group by FITABLE001.COLUMN04 
					--union all 
					select 'Assets' a,'Current Assets' B, 'Cash and Cash Equivalents' Income, FITABLE001.COLUMN04 column08,sum(cast(isnull(PUTABLE019.column11,0)-isnull(PUTABLE019.column10,0)as decimal(18,2))) column10 from PUTABLE019 
					inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE019.COLUMN03  and  (FITABLE001.COLUMN07 ='22266') AND  FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE019.COLUMNA13,0)=0  AND PUTABLE019.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE019.COLUMNA03=@AcOwner AND PUTABLE019.COLUMN04<=@ToDate
			            AND PUTABLE019.COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) 
					  group by FITABLE001.COLUMN04		having (isnull(sum(PUTABLE019.column11),0)-isnull(sum(PUTABLE019.column10),0))!=0  				union all
					  --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
				   select 'Assets' a,'Current Assets' B, 'Cash and Cash Equivalents' Income, FITABLE001.COLUMN04 column08,sum(cast(isnull(FITABLE052.column12,0)-isnull(FITABLE052.column11,0)as decimal(18,2))) column10 from FITABLE052 
					inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE052.COLUMN03  and  (FITABLE001.COLUMN07 ='22409') AND  FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE052.COLUMNA13,0)=0  AND (FITABLE052.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or FITABLE052.COLUMNA02 is null) AND FITABLE052.COLUMNA03=@AcOwner AND FITABLE052.COLUMN04<=@ToDate
			            AND FITABLE052.COLUMN16 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) 
					  group by FITABLE001.COLUMN04		having (isnull(sum(FITABLE052.column12),0)-isnull(sum(FITABLE052.column11),0))!=0  				union all
				   select 'Assets' a,'Current Assets' B, 'Inventory Assets' Income, FITABLE001.COLUMN04 column08,sum(Isnull(PUTABLE017.column10,0))-sum(Isnull(PUTABLE017.column11,0)) column10 from PUTABLE017 
			                left join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.column03  AND FITABLE001.COLUMNA03=@AcOwner WHERE isnull(PUTABLE017.COLUMNA13,0)=0 and  PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner AND PUTABLE017.COLUMN04 BETWEEN @FrDate AND @ToDate AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND PUTABLE017.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
					group by FITABLE001.COLUMN04


					union all 
					select 'Assets' a,'Capital Assets' B, 'Capital' Income, FITABLE001.COLUMN04 column08,Isnull(sum(FITABLE018.column09),0)-Isnull(sum(FITABLE018.column10),0) column10 from FITABLE018 
			                left join FITABLE001 on FITABLE001.COLUMN02=FITABLE018.column08  AND FITABLE001.COLUMNA03=@AcOwner WHERE isnull(FITABLE018.COLUMNA13,0)=0  AND FITABLE018.COLUMNA03=@AcOwner AND FITABLE018.COLUMN04 BETWEEN @FrDate AND @ToDate
					group by FITABLE001.COLUMN04

					union all 
					select 'Assets' a,'Current Assets' B, 'Advance Amount' Income, 'Advance Paid' column08,sum(isnull(FITABLE022.COLUMN05,0)) column10 from FITABLE022
 
					inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE022.COLUMN03  AND FITABLE001.COLUMNA03=@AcOwner 
					inner join FITABLE020 on FITABLE020.COLUMN01=FITABLE022.COLUMN09  
					where FITABLE020.COLUMN12!=0 AND FITABLE020.COLUMNA02 in						(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					AND FITABLE020.COLUMNA03=@AcOwner    AND FITABLE020.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) 
					--AND FITABLE020.COLUMN04 BETWEEN @FrDate AND @ToDate
					group by FITABLE001.COLUMN04 	
					union all 
					select p.a,p.B,p.Income,P.column08,sum(isnull(p.column10,0))column10 from (
					select 'Liabilities And Equity' a,'Current Equity' B, 'Equity' Income,FITABLE001.COLUMN04  column08,sum(isnull(FITABLE029.COLUMN09,0))-sum(isnull(FITABLE029.COLUMN10,0)) column10 from FITABLE029 
					inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE029.COLUMN07  AND FITABLE001.COLUMNA03=@AcOwner where FITABLE029.COLUMN03!='22330' and  FITABLE029.COLUMN04 <= @ToDate AND FITABLE029.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND FITABLE029.COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)and isnull(fitable029.columna13,0)=0  group by FITABLE001.COLUMN04
					having(sum(isnull(FITABLE029.COLUMN09,0))-sum(isnull(FITABLE029.COLUMN10,0)))!=0
					union 
					select 'Liabilities And Equity' a,'Current Equity' B, 'Equity' Income,FITABLE001.COLUMN04  column08,sum(FITABLE001.COLUMN08) column10 from FITABLE001 
					where FITABLE001.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) and FITABLE001.COLUMNA03=@AcOwner and FITABLE001.COLUMN07=22330 group by FITABLE001.COLUMN04) p group by p.a,p.B,p.Income,P.column08 
					union all
					select 'Liabilities And Equity' a,'Current Equity' B, 'Account Payable' Income, FITABLE001.COLUMN04 column08,sum(isnull(PUTABLE016.COLUMN12,0))-sum(isnull(PUTABLE016.COLUMN13,0))+isnull((
					select sum(isnull(MATABLE020.column07,0))+sum(isnull(MATABLE020.column09,0))-@DSC column10 from MATABLE020
					where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=2000  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					),0) column10 from PUTABLE016
			                inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE016.COLUMN03  AND FITABLE001.COLUMNA03=@AcOwner where    PUTABLE016.COLUMN04<= @ToDate AND PUTABLE016.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND PUTABLE016.COLUMNA03=@AcOwner  and isnull(PUTABLE016.COLUMNa13,0)=0  AND PUTABLE016.COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
					group by FITABLE001.COLUMN04 having (sum(isnull(PUTABLE016.COLUMN12,0))-sum(isnull(PUTABLE016.COLUMN13,0)))!=0
					union all
					select 'Liabilities And Equity' a,'Current Equity' B, 'Current Liabilities' Income,FITABLE026.COLUMN17  column08,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) column10 from FITABLE026 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE026.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
					where    FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   AND isnull(FITABLE026.COLUMN16,0)=0 
					--SATYA ISSUES
					AND ISNULL(FITABLE026.COLUMN17,'') not like ('INPUT%') --AND ISNULL(FITABLE026.COLUMN17,'') not like ('OUTPUT%') 
					AND FITABLE026.COLUMN19 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) group by FITABLE026.COLUMN17 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					union all
					select 'Liabilities And Equity' a,'Current Equity' B, 'Current Liabilities' Income,FITABLE026.COLUMN17  column08,-(isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0)) column10 from FITABLE026 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE026.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
					where    FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   AND isnull(FITABLE026.COLUMN16,0)=0 
					AND ISNULL(FITABLE026.COLUMN17,'')  like ('INPUT%') --AND ISNULL(FITABLE026.COLUMN17,'') not like ('OUTPUT%') 
					AND FITABLE026.COLUMN19 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s) group by FITABLE026.COLUMN17 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					union all
					select 'Liabilities And Equity' a,'Current Equity' B, 'Current Liabilities' Income, FITABLE001.COLUMN04 column08,sum(isnull(MATABLE020.column07,0))+sum(isnull(MATABLE020.column09,0)) column10 from MATABLE020
					inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22383 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) group by FITABLE001.COLUMN04 
					union all 
					 select 'Liabilities And Equity' a,'Current Liabilities' B, 'Tax Payable' Income, --(CASE  
						FITABLE026.COLUMN17 column08,-(isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0)) column10 from FITABLE026
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate  AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND FITABLE026.COLUMN19 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
					 AND (FITABLE026.COLUMN17 like ('INPUT%')) --OR FITABLE026.COLUMN17 
					 --like ('OUTPUT%'))
					group by FITABLE026.COLUMN17--,FITABLE026.COLUMN04,FITABLE026.COLUMN10,FITABLE026.COLUMN16
					 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					union all
					 select 'Liabilities And Equity' a,'Current Liabilities' B, 'Tax Payable' Income, --(CASE  
						FITABLE026.COLUMN17 column08,(isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0)) column10 from FITABLE026
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate  AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  AND FITABLE026.COLUMN19 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@ProjectName) s)
					 AND (FITABLE026.COLUMN17 like ('OUTPUT%')) --OR FITABLE026.COLUMN17 
					 --like ('OUTPUT%'))
					group by FITABLE026.COLUMN17--,FITABLE026.COLUMN04,FITABLE026.COLUMN10,FITABLE026.COLUMN16
					 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					union all
					select iif(cast(@PROFIT as decimal(18,2))>0,'Liabilities And Equity','Assets') a,iif(cast(@PROFIT as decimal(18,2))>0,'Profit','Loss') B, iif(cast(@PROFIT as decimal(18,2))>0,'Profit','Loss') Income, iif(cast(@PROFIT as decimal(18,2))>0,'Profit','Loss') column08,iif(cast(@PROFIT as decimal(18,2))>0,@PROFIT,-(@PROFIT)) column10 ORDER BY a 				
			end
			else
			BEGIN
set @profit=(select(select ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0) column10 from FITABLE025 
			--EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078,1132,6158)  and FITABLE025.COLUMN04!='Inventory Adjustment' 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(select ISNULL(-ISNULL(SUM(FITABLE025.column09),0)+ISNULL(SUM(FITABLE025.column11),0),0) column10 from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078,1132,6158) and FITABLE025.COLUMN04='Inventory Adjustment' 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1050) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1052,1053) 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE035.column07),0)-ISNULL(SUM(FITABLE035.column08),0),0)) column10 from FITABLE035 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE035.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE035.COLUMNA13,0)=0 --and FITABLE025.COLUMN08 in(1052,1053) 
			AND FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE035.COLUMNA03=@AcOwner 
			AND FITABLE035.COLUMN04 <= @ToDate
			)+(
			select -(ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(1049) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
			        select -(ISNULL(ISNULL(SUM(FITABLE025.column09),0)-ISNULL(SUM(FITABLE025.column11),0),0)) column10 from FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN08 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN08 in(4663,4183,5436,1078,6158) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN03 <= @ToDate
			)+(
				select -(ISNULL(ISNULL(sum(FITABLE036.COLUMN07),0)-ISNULL(sum(FITABLE036.COLUMN08),0),0)) column10 from FITABLE036
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE036.COLUMN10 AND FITABLE001.COLUMNA03=@AcOwner AND FITABLE001.COLUMNA03=@AcOwner 
			WHERE FITABLE036.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE036.COLUMNA03=@AcOwner 
			AND FITABLE036.COLUMN04 <= @ToDate   AND ISNULL(FITABLE036.COLUMNA13,0)=0 
			--having (isnull(ISNULL(sum(FITABLE036.COLUMN07),0)-ISNULL(sum(FITABLE036.COLUMN08),0),0))!=0 
			 )+(
				select (ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) column10 from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND PUTABLE017.COLUMN04 <= @ToDate    AND (PUTABLE017.COLUMN06 != 'Inventory Adjustment' AND PUTABLE017.COLUMN06 != 'Opening Balance' AND PUTABLE017.COLUMN06 != 'OP')   AND ISNULL(PUTABLE017.COLUMNA13,0)=0 
			--having (isnull(ISNULL(sum(FITABLE036.COLUMN07),0)-ISNULL(sum(FITABLE036.COLUMN08),0),0))!=0 
			 ))
			 ;with MyTable as(select 'Assets' a,'Current Assets' B,'Fixed Assets' Income, FITABLE028.COLUMN03 as column08,isnull(sum(FITABLE028.column09),0)-isnull(sum(FITABLE028.column10),0) column10
		,FITABLE028.COLUMN03 id ,FITABLE028.COLUMN03 typ from FITABLE028 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE028.COLUMN03  AND FITABLE001.COLUMNA03=@AcOwner 
					where    FITABLE028.COLUMN04 BETWEEN @PFRDATE AND @ToDate AND FITABLE028.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) 
					group by FITABLE028.COLUMN03-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07      
						union all
				select 'Assets' a,'Current Assets' B,'Fixed Assets' Income, MATABLE020.COLUMN08 as column08,isnull(sum(MATABLE020.column07),0) column10 
				--,FITABLE001.COLUMN02 id,FITABLE001.COLUMN07 typ
					--EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET,General ledger COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
		,column08 id,column08 typ
				 from MATABLE020
					--inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner
					 where   MATABLE020.COLUMN04 = @PFRDATE   AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22403 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) group by MATABLE020.COLUMN08 
					--,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
					union all
					select 'Assets' a,'Current Assets' B, 'Current Assets' Income, FITABLE034.COLUMN10 column08,isnull(sum(FITABLE034.column07),0)-isnull(sum(FITABLE034.column08),0) column10 
		,FITABLE034.COLUMN10 id,FITABLE034.COLUMN10 typ from FITABLE034 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE034.COLUMN10  AND FITABLE001.COLUMNA03=@AcOwner 
					where   isnull(FITABLE034.COLUMNA13,0)=0 AND FITABLE034.COLUMN04 BETWEEN @PFRDATE AND @ToDate AND FITABLE034.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND ISNULL(FITABLE034.COLUMNA13,0)=0 
					group by FITABLE034.COLUMN10-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
					having (isnull(sum(FITABLE034.column07),0)-isnull(sum(FITABLE034.column08),0))!=0
						union all 
					select 'Assets' a,'Non Current Assets' B, 'Non Current Assets' Income, FITABLE060.COLUMN10 column08,isnull(sum(FITABLE060.column07),0)-isnull(sum(FITABLE060.column08),0) column10 
		,FITABLE060.COLUMN10 id,FITABLE060.COLUMN10 typ from FITABLE060 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE034.COLUMN10  AND FITABLE001.COLUMNA03=@AcOwner 
					where    FITABLE060.COLUMN04 BETWEEN @PFRDATE AND @ToDate AND FITABLE060.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND ISNULL(FITABLE060.COLUMNA13,0)=0 
					group by FITABLE060.COLUMN10-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
					having (isnull(sum(FITABLE060.column07),0)-isnull(sum(FITABLE060.column08),0))!=0
						union all 
					select 'Assets' a,'Current Assets' B, 'Current Assets' Income, MATABLE020.COLUMN08 column08,isnull(sum(MATABLE020.column07),0) column10
		 ,MATABLE020.COLUMN08 id,MATABLE020.COLUMN08 typ from MATABLE020
					--inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
					where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22402 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					group by MATABLE020.COLUMN08-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
					union all 
					select 'Assets' a,'Current Assets' B, 'Account Rceivable' Income, PUTABLE018.COLUMN03 column08,	 isnull(sum(PUTABLE018.COLUMN09),0)-isnull(sum(PUTABLE018.COLUMN11),0)+isnull((select isnull(sum(MATABLE020.column07),0)+isnull(sum(MATABLE020.column09),0)-@DSC column10   from MATABLE020
					 where MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22264 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)),0) column10 
		,PUTABLE018.COLUMN03 id,PUTABLE018.COLUMN03 typ from PUTABLE018
			                --inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE018.COLUMN03  AND  FITABLE001.COLUMNA03=@AcOwner 
							where isnull(PUTABLE018.COLUMNA13,0)=0  AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMN04 <= @ToDate AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) 
					 group by  PUTABLE018.COLUMN03-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
						union all
						--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
				select 'Assets' a,'Current Assets' B, 'Cash and Cash Equivalents' Income, FITABLE052.COLUMN03 column08,sum(cast(isnull(FITABLE052.column12,0)-isnull(FITABLE052.column11,0)as decimal(18,2))) column10,FITABLE052.COLUMN03 id  ,FITABLE052.COLUMN03 typ from FITABLE052 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE052.COLUMN03  and  (FITABLE001.COLUMN07 ='22409') AND  FITABLE001.COLUMNA03=@AcOwner 
					where isnull(FITABLE052.COLUMNA13,0)=0  AND (FITABLE052.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or FITABLE052.COLUMNA02 is null) AND FITABLE052.COLUMNA03=@AcOwner AND FITABLE052.COLUMN04<=@ToDate
			        group by FITABLE052.COLUMN03 having (isnull(sum(FITABLE052.column12),0)-isnull(sum(FITABLE052.column11),0))!=0
					union all
				select 'Assets' a,'Current Assets' B, 'Inventory Assets' Income, PUTABLE017.column03 column08,Isnull(sum(PUTABLE017.column10),0)-Isnull(sum(PUTABLE017.column11),0) column10 
		,PUTABLE017.column03 id  ,PUTABLE017.column03 typ from PUTABLE017 
			                --left join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.column03  AND FITABLE001.COLUMNA03=@AcOwner 
							WHERE isnull(PUTABLE017.COLUMNA13,0)=0 and  PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner AND PUTABLE017.COLUMN04 <= @ToDate AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)
					group by PUTABLE017.column03-- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07 
					union all
					select 'Liabilities And Equity' a,'Current Equity' B, 'Account Payable' Income, PUTABLE016.COLUMN03 column08,isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0)+isnull((
					select isnull(sum(MATABLE020.column07),0)+isnull(sum(MATABLE020.column09),0) column10 from MATABLE020
					where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22263  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					),0) column10  
		,PUTABLE016.COLUMN03 id  ,PUTABLE016.COLUMN03 typ from PUTABLE016
			                --inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE016.COLUMN03  AND FITABLE001.COLUMNA03=@AcOwner
							 where    PUTABLE016.COLUMN04<= @ToDate AND PUTABLE016.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND PUTABLE016.COLUMNA03=@AcOwner  and isnull(PUTABLE016.COLUMNa13,0)=0
					group by PUTABLE016.COLUMN03 -- ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07 
					having (isnull(sum(PUTABLE016.COLUMN12),0)-isnull(sum(PUTABLE016.COLUMN13),0))!=0
					union all
		--		select 'Assets' a,'Current Assets' B,'Fixed Assets' Income, MATABLE020.COLUMN08 as column08,isnull(sum(MATABLE020.column07),0) column10 
		--		--,FITABLE001.COLUMN02 id,FITABLE001.COLUMN07 typ
		--,null id,null typ
		--		 from MATABLE020
		--			--inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
		--			where   MATABLE020.COLUMN04 = @PFRDATE   AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22403  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
		--			group by MATABLE020.COLUMN08
		--			union all
					select 'Assets' a,'Current Assets' B, 'Cash and Cash Equivalents' Income, PUTABLE019.COLUMN03 column08,sum(cast(isnull(PUTABLE019.column11,0)-isnull(PUTABLE019.column10,0)as decimal(18,2))) column10 
		,PUTABLE019.COLUMN03 id  ,PUTABLE019.COLUMN03 typ from PUTABLE019 
					--inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE019.COLUMN03  and  (FITABLE001.COLUMN07 ='22266') AND  FITABLE001.COLUMNA03=@AcOwner 
					where isnull(PUTABLE019.COLUMNA13,0)=0  AND PUTABLE019.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE019.COLUMNA03=@AcOwner AND PUTABLE019.COLUMN04<=@ToDate
					group by PUTABLE019.COLUMN03 having (isnull(sum(PUTABLE019.column11),0)-isnull(sum(PUTABLE019.column10),0))!=0  			
						union all
					--select p.a,p.B,p.Income,P.column08,sum(isnull(p.column10,0))column10,p.id,p.typ from (
					select 'Liabilities And Equity' a,'Current Equity' B, 'Equity' Income,FITABLE029.COLUMN07  column08,isnull(sum(FITABLE029.COLUMN09),0)-isnull(sum(FITABLE029.COLUMN10),0) column10 
		,FITABLE029.COLUMN07 id  ,FITABLE029.COLUMN07 typ from FITABLE029 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE029.COLUMN07  AND FITABLE001.COLUMNA03=@AcOwner
					 where FITABLE029.COLUMN03!='22330' and  FITABLE029.COLUMN04 <= @ToDate AND FITABLE029.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)and isnull(fitable029.columna13,0)=0  group by FITABLE029.COLUMN07
					having(isnull(sum(FITABLE029.COLUMN09),0)-isnull(sum(FITABLE029.COLUMN10),0)) !=0
					union all
		--			select 'Liabilities And Equity' a,'Current Equity' B, 'Equity' Income,FITABLE001.COLUMN02  column08,sum(FITABLE001.COLUMN08) column10 
		--			--,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ
		--,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN02 typ from FITABLE001 
		--			where  FITABLE001.COLUMNA03=@AcOwner and FITABLE001.COLUMN07=22330 and FITABLE001.COLUMN08!=0  group by  FITABLE001.COLUMN02 ,FITABLE001.COLUMN07--) p group by p.a,p.B,p.Income,P.column08,p.id,p.typ
		--			union all
					select 'Liabilities And Equity' a,'Current Equity' B, 'Current Liabilities' Income, MATABLE020.COLUMN08 column08,isnull(sum(MATABLE020.column09),0)-isnull(sum(MATABLE020.column07),0) column10 
					 --,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ
					  ,null id  ,null typ from MATABLE020
					--inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
					where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22383 and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
					 group by MATABLE020.COLUMN08
					 union all 
					select 'Liabilities And Equity' a,'Non Current Liabilities' B, 'Non Current Liabilities' Income, FITABLE063.COLUMN10 column08,isnull(sum(FITABLE063.column07),0)-isnull(sum(FITABLE063.column08),0) column10 
		,FITABLE063.COLUMN10 id,FITABLE063.COLUMN10 typ from FITABLE063 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE063.COLUMN10  AND FITABLE001.COLUMNA03=@AcOwner 
					where    FITABLE063.COLUMN04 BETWEEN @PFRDATE AND @ToDate AND FITABLE063.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) AND ISNULL(FITABLE063.COLUMNA13,0)=0 
					group by FITABLE063.COLUMN10--,FITABLE001.COLUMN04 -- ,FITABLE001.COLUMN07
					having (isnull(sum(FITABLE063.column07),0)-isnull(sum(FITABLE063.column08),0))!=0
					)
					select  a,B,iif(isnull(f1.column15,0) =0,Income,m2.column04)Income,f1.column04 COLUMN08,Mytable.column10 column10,f1.column02 id,f1.column07 typ from Mytable 
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=Mytable.COLUMN08 AND F1.COLUMNA03=@AcOwner 
		LEFT OUTER join MATABLE002 m2 on m2.COLUMN02=f1.column15 --AND F1.COLUMNA03=@AcOwner
					
					union all

					 select 'Liabilities And Equity' a,'Current Liabilities' B, 'Tax Payable' Income, --(CASE  
						FITABLE026.COLUMN17 column08,-(sum(isnull(FITABLE026.COLUMN11,0))-sum(isnull(FITABLE026.COLUMN12,0))) column10  ,
						(FITABLE026.COLUMN08) id  
						,'223831' typ  from FITABLE026
						--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE026.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03  BETWEEN @PFRDATE AND @ToDate  AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) 
					 AND (FITABLE026.COLUMN17 like ('INPUT%'))
					group by FITABLE026.COLUMN17,FITABLE026.COLUMN08 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					
						union all  
					 select 'Liabilities And Equity' a,'Current Liabilities' B, 'Tax Payable' Income, --(CASE  
						FITABLE026.COLUMN17 column08,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) column10  ,
						(FITABLE026.COLUMN08) id  
						,'223832' typ   from FITABLE026
						--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE026.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03  BETWEEN @PFRDATE AND @ToDate  AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) 
					 AND (FITABLE026.COLUMN17 like ('OUTPUT%'))
					group by FITABLE026.COLUMN17,FITABLE026.COLUMN08 --,FITABLE026.COLUMN04,FITABLE026.COLUMN10,FITABLE026.COLUMN16
					 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					 union all 
					 select 'Liabilities And Equity' a,'Current Liabilities' B, 'Current Liabilities' Income, --(CASE  
						FITABLE026.COLUMN17 column08,(sum(isnull(FITABLE026.COLUMN11,0))-sum(isnull(FITABLE026.COLUMN12,0))) column10  ,
						(FITABLE026.COLUMN08) id  
						,'22383' typ  from FITABLE026
						--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE026.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03  BETWEEN @PFRDATE AND @ToDate  AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) 
					 AND (FITABLE026.COLUMN17 IN ('CGST RCM Payable','SGST RCM Payable','IGST RCM Payable','UTGST RCM Payable','Cess RCM Payable'))
					group by FITABLE026.COLUMN17,FITABLE026.COLUMN08 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0

		--			union all
		--		select 'Assets' a,'Current Assets' B,'Fixed Assets' Income, FITABLE001.COLUMN04 as column08,isnull(sum(MATABLE020.column07),0) column10 
		--		--,FITABLE001.COLUMN02 id,FITABLE001.COLUMN07 typ
		--,null id,null typ
		--		 from MATABLE020
		--			inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
		--			where   MATABLE020.COLUMN04 = @PFRDATE   AND MATABLE020.COLUMNA03=@AcOwner AND FITABLE001.COLUMN07=22403 
		--			group by FITABLE001.COLUMN04
		--			union all
		--			select 'Assets' a,'Current Assets' B, 'Cash and Cash Equivalents' Income, FITABLE001.COLUMN04 column08,sum(cast(isnull(PUTABLE019.column11,0)-isnull(PUTABLE019.column10,0)as decimal(18,2))) column10 
		--,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ from PUTABLE019 
		--			inner join FITABLE001 on FITABLE001.COLUMN02=PUTABLE019.COLUMN03  and  (FITABLE001.COLUMN07 ='22266') AND  FITABLE001.COLUMNA03=@AcOwner 
		--			where isnull(PUTABLE019.COLUMNA13,0)=0  AND PUTABLE019.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE019.COLUMNA03=@AcOwner AND PUTABLE019.COLUMN04<=@ToDate
		--			group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07 having (isnull(sum(PUTABLE019.column11),0)-isnull(sum(PUTABLE019.column10),0))!=0  			
		--				union all
		--			select p.a,p.B,p.Income,P.column08,sum(isnull(p.column10,0))column10,p.id,p.typ from (
		--			select 'Liabilities And Equity' a,'Current Equity' B, 'Equity' Income,FITABLE001.COLUMN04  column08,isnull(sum(FITABLE029.COLUMN09),0)-isnull(sum(FITABLE029.COLUMN10),0) column10 
		--,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ from FITABLE029 
		--			inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE029.COLUMN07  AND FITABLE001.COLUMNA03=@AcOwner where FITABLE029.COLUMN03!='22330' and  FITABLE029.COLUMN04 <= @ToDate AND FITABLE029.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s) group by FITABLE001.COLUMN04 ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
		--			having(isnull(sum(FITABLE029.COLUMN09),0)-isnull(sum(FITABLE029.COLUMN10),0)) !=0
		--			union 
		--			select 'Liabilities And Equity' a,'Current Equity' B, 'Equity' Income,FITABLE001.COLUMN04  column08,sum(FITABLE001.COLUMN08) column10 
		--			--,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ
		--,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ from FITABLE001 
		--			where  FITABLE001.COLUMNA03=@AcOwner and FITABLE001.COLUMN07=22330 and FITABLE001.COLUMN08!=0 group by  FITABLE001.COLUMN04  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07) p group by p.a,p.B,p.Income,P.column08,p.id,p.typ
		--			union all
		--			select 'Liabilities And Equity' a,'Current Equity' B, 'Current Liabilities' Income, FITABLE001.COLUMN04 column08,isnull(sum(MATABLE020.column07),0)+isnull(sum(MATABLE020.column09),0) column10 
		--			 --,FITABLE001.COLUMN02 id  ,FITABLE001.COLUMN07 typ
		--			  ,null id  ,null typ from MATABLE020
		--			inner join FITABLE001 on FITABLE001.COLUMN02=MATABLE020.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner 
		--			where    MATABLE020.COLUMN04 = @PFRDATE  AND MATABLE020.COLUMNA03=@AcOwner AND MATABLE020.COLUMN10=22383
		--			 group by FITABLE001.COLUMN04  ,FITABLE001.COLUMN02 ,FITABLE001.COLUMN07
					union all 

					select 'Liabilities And Equity' a,'Current Equity' B, 'Current Liabilities' Income,FITABLE026.COLUMN17  column08,isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0) column10
		,(select max(COLUMN02) from FITABLE001 where COLUMN02 in  (FITABLE026.column08) AND FITABLE001.COLUMNA03=@AcOwner) id  ,
		22383 typ    from FITABLE026 
					--inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE026.COLUMN08  AND FITABLE001.COLUMNA03=@AcOwner
					 where    FITABLE026.COLUMN03  BETWEEN @PFRDATE AND @ToDate AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   --and isnull(FITABLE026.COLUMN16,0)=0
					 AND ISNULL(FITABLE026.COLUMN17,'') not like ('OUTPUT%')  AND ISNULL(FITABLE026.COLUMN17,'') not like ('INPUT%')  AND ISNULL(FITABLE026.COLUMN17,'') NOT IN ('CGST RCM Payable','SGST RCM Payable','IGST RCM Payable','UTGST RCM Payable','Cess RCM Payable') AND ISNULL(FITABLE026.COLUMNA13,0)=0
					group by FITABLE026.COLUMN17 ,FITABLE026.column08 having (isnull(sum(FITABLE026.COLUMN11),0)-isnull(sum(FITABLE026.COLUMN12),0))!=0
					union all
					select iif(cast(@PROFIT as decimal(18,2))>0,'Liabilities And Equity','Assets') a,iif(cast(@PROFIT as decimal(18,2))>0,'Profit and Loss Account','Profit and Loss Account') B, iif(cast(@PROFIT as decimal(18,2))>0,'Profit and Loss Account','Profit and Loss Account') Income, iif(cast(@PROFIT as decimal(18,2))>0,'Profit','Loss') column08,iif(cast(@PROFIT as decimal(18,2))>0,@PROFIT,-(@PROFIT)) column10 
					,null id  ,null typ 
					UNION ALL
					SELECT iif((SUM(CR)-SUM(DR))>0,'Liabilities And Equity','Assets')A,''B,'' Income,'Difference in Openning Balance'column08, IIF((SUM(DR)-SUM(CR))>0,(SUM(DR)-SUM(CR)),-(SUM(DR)-SUM(CR))) column10 ,
					null id,null typ FROM
					(SELECT SUM(IIF(COLUMN07 IN(22266,22409,22264,22402,22403,22344,22410,22408,22262),(COLUMN08),0))CR,
					SUM(IIF(COLUMN07 IN(22330,22263,22383,22411,22405,22407),(COLUMN08),0))DR FROM FITABLE001 
					WHERE COLUMNA03 = @AcOwner AND COLUMN10!=0  AND COLUMN09<=@ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN07
					UNION ALL
					SELECT SUM(IIF(COLUMN08 = 3000,(COLUMN09-column10),0))CR,SUM(IIF(COLUMN08 = 2000 ,(COLUMN09-column10),0))DR FROM FITABLE018 
					WHERE COLUMN04 between @PFRDATE AND @ToDate and COLUMNA03 = @AcOwner and COLUMN02 > 3256 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN08
					UNION ALL
					SELECT SUM(COLUMN10)CR,SUM(COLUMN11)DR FROM PUTABLE017 
					WHERE COLUMNA03 = @AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					AND PUTABLE017.COLUMN04 between @PFRDATE AND @ToDate    AND PUTABLE017.COLUMN06 = 'Inventory Adjustment'   AND ISNULL(PUTABLE017.COLUMNA13,0)=0
					)T  HAVING  SUM(T.DR)-SUM(T.CR)!=0
					ORDER BY a 

		END
		end
		
		ELSE IF  @ReportName='BalanceSheet'
		begin
		if(@Operating='')
		begin
		set @Operating=@OPUnit
		end
		else
		begin
		set @OPUnit=@Operating
			set @DSC=(SELECT(iif(EXISTS(select sum(column07) from fitable036 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner and COLUMN04  between @PTODATE and @ToDate  AND COLUMN03='PAYMENT' AND COLUMN10=1049 AND ISNULL(COLUMNA13,0)=0),(select ISNULL(SUM(COLUMN07),0) from fitable036 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner and COLUMN04  between @PTODATE and @ToDate AND COLUMN03='PAYMENT' AND COLUMN10=1049 AND ISNULL(COLUMNA13,0)=0),0)))
		end

			BEGIN
---DECLARE @PFRDATE  nvarchar(250)= '01/01/1900'--, @PTODATE DATE
		
		--BEGIN
		--	--SET @PFRDATE=((SELECT DateAdd(yy, -1, @FrDate)))
		--	SET @PTODATE=((SELECT DateAdd(yy, -1, '2/28/2018')))
		--	select top 1 @PFRDATE=column04 from MATABLE020 where COLUMNA03=56578 and isnull(columna13,0)=0 order by COLUMN01 desc
		--END
declare @profitB decimal(18,2) = 0
set @profitB=(select(
			select ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0) column10 from FITABLE064 FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22405,22406) and FITABLE025.COLUMN11 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078,1132,6158,1154)  and FITABLE025.COLUMN03!='Inventory Adjustment' 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate
			)+(select ISNULL(-ISNULL(SUM(FITABLE025.column14),0)+ISNULL(SUM(FITABLE025.column15),0),0) column10 from  FITABLE064 FITABLE025 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22405,22406) and FITABLE025.COLUMN11 not in(1050,1052,1053,1049,1056,4231,4663,4183,5436,1078,1132,6158,1154) and FITABLE025.COLUMN03='Inventory Adjustment' 
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0)) column10 from FITABLE064 FITABLE025  
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22405)  and FITABLE025.COLUMN11 in(1050,1052,1053,1154) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate
			)+(
			
			select -(ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0)) column10 from FITABLE064 FITABLE025  
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in (22406)  and FITABLE025.COLUMN11 in(4663,1078,5436,6158) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate
			)+(
			
			select (ISNULL(ISNULL(SUM(FITABLE035.column14),0)-ISNULL(SUM(FITABLE035.column15),0),0)) column10 from FITABLE064 FITABLE035 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=FITABLE035.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE035.COLUMNA13,0)=0 and FITABLE035.COLUMN10 in(22407) 
			AND FITABLE035.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE035.COLUMNA03=@AcOwner 
			AND FITABLE035.COLUMN07  between @PTODATE and  @ToDate
			)+(
			select (ISNULL(ISNULL(SUM(FITABLE025.column14),0)-ISNULL(SUM(FITABLE025.column15),0),0)) column10 from FITABLE064 FITABLE025 
			        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in(22344,22408) AND 
			FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate --and FITABLE025.COLUMN11 != 1078
			--)+(
			--select (ISNULL(ISNULL(SUM(FITABLE025.column15),0)-ISNULL(SUM(FITABLE025.column14),0),0)) column10 from FITABLE064 FITABLE025 
			--        inner join FITABLE001 on FITABLE001.COLUMN02=FITABLE025.COLUMN11 AND FITABLE001.COLUMNA03=@AcOwner where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10 in(22344,22408) AND 
			--FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner 
			--AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate and FITABLE025.COLUMN11 = 1078
			)+(
				select (ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) column10 from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND PUTABLE017.COLUMN04  between @PTODATE and  @ToDate  
			AND (PUTABLE017.COLUMN06 != 'Inventory Adjustment' AND PUTABLE017.COLUMN06 != 'Opening Balance' AND PUTABLE017.COLUMN06 != 'OP' AND PUTABLE017.COLUMN06 != 'JOURNAL ENTRY')   
			AND ISNULL(PUTABLE017.COLUMNA13,0)=0 
			 )
			
			 +(
				select (ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) column10 from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' or PUTABLE017.COLUMN06 = 'Opening Balance' or  PUTABLE017.COLUMN06 = 'OP')  
			 AND ISNULL(PUTABLE017.COLUMNA13,0)=0 and PUTABLE017.COLUMN04 between @PTODATE and @ToDate
			 )
			  +(
				select -(ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) column10 from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' or PUTABLE017.COLUMN06 = 'Opening Balance' or  PUTABLE017.COLUMN06 = 'OP')  
			 AND ISNULL(PUTABLE017.COLUMNA13,0)=0 and PUTABLE017.COLUMN04 = @PTODATE
			 )
			  +(
				select -(ISNULL(ISNULL(sum(PUTABLE017.COLUMN10),0)-ISNULL(sum(PUTABLE017.COLUMN11),0),0)) column10 from PUTABLE017
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner  
			WHERE PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE017.COLUMNA03=@AcOwner 
			AND (PUTABLE017.COLUMN06 = 'Inventory Adjustment' or PUTABLE017.COLUMN06 = 'Opening Balance' or  PUTABLE017.COLUMN06 = 'OP')  
			 AND ISNULL(PUTABLE017.COLUMNA13,0)=0 and PUTABLE017.COLUMN04 = @PFRDATE
			 )
			 )
			 ;with MyTable as(
			 
			select 'Asset' a,FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0))  Credit
		 ,FITABLE025.COLUMN11 id ,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22403,22402,22410)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 BETWEEN @PFRDATE AND @ToDate 
			union all
				select 'Asset' a,MATABLE020.COLUMN10 Income, MATABLE020.COLUMN08 column08,ISNULL((MATABLE020.COLUMN07),0)-ISNULL((MATABLE020.COLUMN09),0) Credit ,
			MATABLE020.COLUMN08 id,NULL Trans,MATABLE020.COLUMN04 Date,NULL type 

			from MATABLE020
			left join fiTABLE001 f1 on f1.column02=MATABLE020.column08 and f1.columna03=MATABLE020.COLUMNA03
			left join MATABLE002 m2 on m2.column02=isnull(f1.column15,MATABLE020.COLUMN10)
			WHERE MATABLE020.COLUMNA03=@AcOwner  and MATABLE020.COLUMN10 in(22403,22402,22410,22264,22266,22409)  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)--AND FITABLE036.COLUMN10=1078
			AND MATABLE020.COLUMN04 = @PFRDATE   
			union all 
					 
			select 'Asset' a,FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column15,0))-(ISNULL(FITABLE025.column14,0))  Credit
		 ,FITABLE025.COLUMN11 id ,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22264,22266,22409)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate  
			union all
			select 'Asset' a,'22262' Income, PUTABLE017.COLUMN03 column08,(ISNULL(PUTABLE017.column10,0))-(ISNULL(PUTABLE017.column11,0)) Credit,PUTABLE017.COLUMN03 id
			,cast(PUTABLE017.COLUMN05 as nvarchar(25)) Trans,PUTABLE017.COLUMN04 Date,PUTABLE017.COLUMN06 from PUTABLE017 
			LEFT OUTER join FITABLE001 on FITABLE001.COLUMN02=PUTABLE017.COLUMN03 AND FITABLE001.COLUMNA03=@AcOwner where isnull(PUTABLE017.COLUMNA13,0)=0  --and PUTABLE017.COLUMN04='Inventory Adjustment'
			AND (PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND PUTABLE017.COLUMNA03=@AcOwner --AND PUTABLE017.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND PUTABLE017.COLUMN04  between @PTODATE and  @ToDate 
			union all
				 
			select 'Liabilities And Equity' a,FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))  Credit
		 ,FITABLE025.COLUMN11 id ,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22263,22330)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07  between @PTODATE and  @ToDate  
			
					union all
				 
			select 'Liabilities And Equity' a,FITABLE025.COLUMN10 Income, FITABLE025.COLUMN11 column08,(ISNULL(FITABLE025.column14,0))-(ISNULL(FITABLE025.column15,0))  Credit
		 ,FITABLE025.COLUMN11 id ,FITABLE025.COLUMN04 Trans,FITABLE025.COLUMN07 Date,FITABLE025.COLUMN03 Type  from FITABLE064  FITABLE025
			where isnull(FITABLE025.COLUMNA13,0)=0 and FITABLE025.COLUMN10  in(22383,22411)  AND FITABLE025.COLUMNA03=@AcOwner
			AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE025.COLUMNA03=@AcOwner AND FITABLE025.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
			AND FITABLE025.COLUMN07 between @PFRDATE AND @ToDate  
			
					union all
				select 'Liabilities And Equity' a,MATABLE020.COLUMN10 Income, MATABLE020.COLUMN08 column08,ISNULL((MATABLE020.COLUMN09),0)-ISNULL((MATABLE020.COLUMN07),0) Credit ,
			MATABLE020.COLUMN08 id,NULL Trans,MATABLE020.COLUMN04 Date,NULL type 

			from MATABLE020
			left join fiTABLE001 f1 on f1.column02=MATABLE020.column08 and f1.columna03=MATABLE020.COLUMNA03 and isnull(f1.columna13,0) = 0
			left join MATABLE002 m2 on m2.column02=isnull(f1.column15,MATABLE020.COLUMN10)
			WHERE MATABLE020.COLUMNA03=@AcOwner  and MATABLE020.COLUMN10 not in(22403,22402,22410,22264,22266,22409,22405)  and MATABLE020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)--AND FITABLE036.COLUMN10=1078
			AND MATABLE020.COLUMN04 = @PFRDATE   
			 )
					select  a,m2.column04 b,m2.column04 Income,F1.COLUMN04 column08,sum(Mytable.Credit) column10,
					--Mytable.Trans Trans,FORMAT(Mytable.Date,@DateF)Date,Mytable.Type,DATENAME(month, Mytable.Date) AS 'Month'
					Mytable.column08 id,f1.column07 typ from Mytable 
		LEFT OUTER join FITABLE001 f1 on F1.COLUMN02=Mytable.COLUMN08 AND F1.COLUMNA03=@AcOwner and isnull(F1.COLUMNA13,0) = 0
		LEFT OUTER join MATABLE002 m2 on m2.COLUMN02 = iif(f1.column15 is null or f1.column15 = '' ,f1.column07,f1.column15) --AND F1.COLUMNA03=@AcOwner
					group by a,m2.column04,f1.column04,Mytable.column08,f1.column07,f1.column04 having sum(Mytable.Credit)!=0
					union all
					select iif(cast(@profitB as decimal(18,2))>0,'Liabilities And Equity','Asset') a,iif(cast(@profitB as decimal(18,2))>0,'Profit and Loss Account','Profit and Loss Account') B, 
					iif(cast(@profitB as decimal(18,2))>0,'Profit and Loss Account','Profit and Loss Account') Income, iif(cast(@profitB as decimal(18,2))>0,'Profit','Loss') 
					 column08,iif(cast(@profitB as decimal(18,2))>0,@profitB,-(@profitB)) column10 
					--,null Trans  , NULL Date,iif(cast(@PROFIT as decimal(18,2))>0,'Profit','Loss') Type ,null 'Month'
					,NULL id,null typ where @profitB != 0
					UNION ALL
					SELECT iif((SUM(CR)-SUM(DR))>0,'Liabilities And Equity','Asset')A,''B,'Difference in Openning Balance' Income,''column08, IIF((SUM(DR)-SUM(CR))>0,(SUM(DR)-SUM(CR)),-(SUM(DR)-SUM(CR))) column10 ,
					--null Trans,null Date,NULL 'Type',NULL 'Month'
					NULL id,null typ FROM
					(SELECT SUM(IIF(COLUMN07 IN(22266,22409,22264,22402,22403,22344,22410,22408,22262),(COLUMN08),0))CR,
					SUM(IIF(COLUMN07 IN(22330,22263,22383,22411,22405,22407),(COLUMN08),0))DR FROM FITABLE001 
					WHERE COLUMNA03 = @AcOwner AND COLUMN10!=0  AND COLUMN09 between @PTODATE and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN07
					UNION ALL
					SELECT SUM(IIF(COLUMN08 = 3000,(COLUMN09-column10),0))CR,SUM(IIF(COLUMN08 = 2000 ,(COLUMN09-column10),0))DR FROM FITABLE018 
					WHERE COLUMN04  between @PTODATE and  @ToDate and COLUMNA03 = @AcOwner and COLUMN02 > 3256 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND ISNULL(COLUMNA13,0)=0 GROUP BY COLUMN08
					--UNION ALL
					--SELECT SUM(COLUMN10)CR,SUM(COLUMN11)DR FROM PUTABLE017 
					--WHERE COLUMNA03 = @AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					--AND PUTABLE017.COLUMN04  between @PTODATE and  @ToDate    AND PUTABLE017.COLUMN06 = 'Inventory Adjustment'   AND ISNULL(PUTABLE017.COLUMNA13,0)=0
					UNION ALL
					SELECT SUM(COLUMN10)CR,SUM(COLUMN11)DR FROM PUTABLE017 
					WHERE COLUMNA03 = @AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
					AND PUTABLE017.COLUMN04 IN  (SELECT  COLUMN04 FROM FITABLE048 WHERE COLUMNA03 = @AcOwner and column04 = @PFRDATE and isnull(columna13,0)=0 )   AND PUTABLE017.COLUMN06 = 'Inventory Adjustment'   AND ISNULL(PUTABLE017.COLUMNA13,0)=0
					)T  HAVING  SUM(T.DR)-SUM(T.CR)!=0
					ORDER BY a 

		END
		end

		ELSE IF  @ReportName='VatInfo'
		BEGIN
			if(@Operating!='' and @Brand!='')
			begin
					select FITABLE026.COLUMN17 Vat, FITABLE026.COLUMN04 [Type], 
(CASE  WHEN (FITABLE026.COLUMN04='ADVANCE PAYMENT' OR FITABLE026.COLUMN04='Bill'  OR FITABLE026.COLUMN04='BILL PAYMENT' OR FITABLE026.COLUMN04='PAYMENT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'INPUT%' then (select COLUMN05 from SATABLE001 where isnull(SATABLE001.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Invoice' OR FITABLE026.COLUMN04='RECEIVE PAYMENT'  OR FITABLE026.COLUMN04='RECEIPT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'OUTPUT%' then (select COLUMN05 from SATABLE002 where isnull(SATABLE002.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50=22335)  then s2.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51=22305) then s1.COLUMN05 
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50!=22335)  then s1.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51!=22305) then s2.COLUMN05 
else  NULL end) Name,
					(  isnull(sum(isnull(FITABLE026.COLUMN11,0)),0)-(isnull(sum(isnull(FITABLE026.COLUMN12,0)),0))) Amount,

			                (SELECT MATABLE007.COLUMN04 FROM MATABLE007 WHERE isnull(MATABLE007.COLUMNA13,0)=0 and MATABLE007.COLUMN02= FITABLE026.COLUMN13) ITEM ,

					iif(FITABLE026.COLUMN04='JOURNAL ENTRY' AND isnull(FITABLE026.COLUMN16,0)>0, 0,sum(isnull(FITABLE026.COLUMN14,0)))COLUMN14,FITABLE026.COLUMN16 , FITABLE026.COLUMN09 TASNSACTION  , MATABLE005.COLUMN04  BrandIDN
					from FITABLE026
					LEFT OUTER join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10
					LEFT OUTER join MATABLE007 on MATABLE007.COLUMN02=FITABLE026.COLUMN13
					
					left join MATABLE005  on MATABLE005.column02=MATABLE007.column10 
					left join SATABLE001 s1  on s1.column02=FITABLE026.column06 
					left join SATABLE002 s2  on s2.column02=FITABLE026.column06 
					left join SATABLE005 s5  on s5.column01=FITABLE026.column05 
					left join PUTABLE001 p1  on p1.column01=FITABLE026.column05
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN08 not in(1134,1135,1136,1137,1138,1139,1140,1141,1142) and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)  
							 and  MATABLE007.COLUMN10  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Brand) s)
					group by MATABLE002.COLUMN04  , MATABLE005.COLUMN04,FITABLE026.COLUMN04,FITABLE026.COLUMN06,FITABLE026.COLUMN10,FITABLE026.COLUMN16,MATABLE007.COLUMN04,FITABLE026.COLUMN13,FITABLE026.COLUMN14,FITABLE026.COLUMN16,FITABLE026.COLUMN17, FITABLE026.COLUMN09,s1.COLUMN05,s2.COLUMN05,s5.COLUMN51,p1.COLUMN50 having isnull(FITABLE026.COLUMN16,0)>0  and isnull(FITABLE026.COLUMN10,0)!=0 
end

else if(@Operating!='')
begin
					select FITABLE026.COLUMN17 Vat, FITABLE026.COLUMN04 [Type], 
(CASE  WHEN (FITABLE026.COLUMN04='ADVANCE PAYMENT' OR FITABLE026.COLUMN04='Bill'  OR FITABLE026.COLUMN04='BILL PAYMENT' OR FITABLE026.COLUMN04='PAYMENT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'INPUT%' then (select COLUMN05 from SATABLE001 where isnull(SATABLE001.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Invoice' OR FITABLE026.COLUMN04='RECEIVE PAYMENT'  OR FITABLE026.COLUMN04='RECEIPT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'OUTPUT%' then (select COLUMN05 from SATABLE002 where isnull(SATABLE002.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50=22335)  then s2.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51=22305) then s1.COLUMN05 
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50!=22335)  then s1.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51!=22305) then s2.COLUMN05 
else  NULL end) Name,
					(  isnull(sum(isnull(FITABLE026.COLUMN11,0)),0)-(isnull(sum(isnull(FITABLE026.COLUMN12,0)),0))) Amount,

			                (SELECT MATABLE007.COLUMN04 FROM MATABLE007 WHERE isnull(MATABLE007.COLUMNA13,0)=0 and MATABLE007.COLUMN02= FITABLE026.COLUMN13) ITEM ,

					iif(FITABLE026.COLUMN04='JOURNAL ENTRY' AND isnull(FITABLE026.COLUMN16,0)>0, 0,sum(isnull(FITABLE026.COLUMN14,0)))COLUMN14,FITABLE026.COLUMN16 , FITABLE026.COLUMN09 TASNSACTION, MATABLE005.COLUMN04  BrandIDN
					from FITABLE026
					LEFT OUTER join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10
					LEFT OUTER join MATABLE007 on MATABLE007.COLUMN02=FITABLE026.COLUMN13
					
					left join MATABLE005  on MATABLE005.column02=MATABLE007.column10 
					left join SATABLE001 s1  on s1.column02=FITABLE026.column06 
					left join SATABLE002 s2  on s2.column02=FITABLE026.column06 
					left join SATABLE005 s5  on s5.column01=FITABLE026.column05 
					left join PUTABLE001 p1  on p1.column01=FITABLE026.column05
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN08 not in(1134,1135,1136,1137,1138,1139,1140,1141,1142) and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate AND FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Operating) s)   
					group by MATABLE002.COLUMN04, MATABLE005.COLUMN04 ,FITABLE026.COLUMN04,FITABLE026.COLUMN06,FITABLE026.COLUMN10,FITABLE026.COLUMN16,MATABLE007.COLUMN04,FITABLE026.COLUMN13,FITABLE026.COLUMN14,FITABLE026.COLUMN16,FITABLE026.COLUMN17, FITABLE026.COLUMN09,s1.COLUMN05,s2.COLUMN05,s5.COLUMN51,p1.COLUMN50 having isnull(FITABLE026.COLUMN16,0)>0  and isnull(FITABLE026.COLUMN10,0)!=0 
end


else if(@Brand!='')

begin
					select FITABLE026.COLUMN17 Vat, FITABLE026.COLUMN04 [Type], 
(CASE  WHEN (FITABLE026.COLUMN04='ADVANCE PAYMENT' OR FITABLE026.COLUMN04='Bill'  OR FITABLE026.COLUMN04='BILL PAYMENT' OR FITABLE026.COLUMN04='PAYMENT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'INPUT%' then (select COLUMN05 from SATABLE001 where isnull(SATABLE001.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Invoice' OR FITABLE026.COLUMN04='RECEIVE PAYMENT'  OR FITABLE026.COLUMN04='RECEIPT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'OUTPUT%' then (select COLUMN05 from SATABLE002 where isnull(SATABLE002.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50=22335)  then s2.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51=22305) then s1.COLUMN05 
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50!=22335)  then s1.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51!=22305) then s2.COLUMN05 
else  NULL end) Name,
					(  isnull(sum(isnull(FITABLE026.COLUMN11,0)),0)-(isnull(sum(isnull(FITABLE026.COLUMN12,0)),0))) Amount,

			                (SELECT MATABLE007.COLUMN04 FROM MATABLE007 WHERE isnull(MATABLE007.COLUMNA13,0)=0 and MATABLE007.COLUMN02= FITABLE026.COLUMN13) ITEM ,

					iif(FITABLE026.COLUMN04='JOURNAL ENTRY' AND isnull(FITABLE026.COLUMN16,0)>0, 0,sum(isnull(FITABLE026.COLUMN14,0)))COLUMN14,FITABLE026.COLUMN16 , FITABLE026.COLUMN09 TASNSACTION, MATABLE005.COLUMN04  BrandIDN
					from FITABLE026
					LEFT OUTER join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10
					LEFT OUTER join MATABLE007 on MATABLE007.COLUMN02=FITABLE026.COLUMN13
					left join MATABLE005  on MATABLE005.column02=MATABLE007.column10 
					left join SATABLE001 s1  on s1.column02=FITABLE026.column06 
					left join SATABLE002 s2  on s2.column02=FITABLE026.column06 
					left join SATABLE005 s5  on s5.column01=FITABLE026.column05 
					left join PUTABLE001 p1  on p1.column01=FITABLE026.column05
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN08 not in(1134,1135,1136,1137,1138,1139,1140,1141,1142) and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate   
							 and  MATABLE007.COLUMN10  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Brand) s)
					group by MATABLE002.COLUMN04, MATABLE005.COLUMN04 ,FITABLE026.COLUMN04,FITABLE026.COLUMN06,FITABLE026.COLUMN10,FITABLE026.COLUMN16,MATABLE007.COLUMN04,FITABLE026.COLUMN13,FITABLE026.COLUMN14,FITABLE026.COLUMN16,FITABLE026.COLUMN17, FITABLE026.COLUMN09,s1.COLUMN05,s2.COLUMN05,s5.COLUMN51,p1.COLUMN50 having isnull(FITABLE026.COLUMN16,0)>0  and isnull(FITABLE026.COLUMN10,0)!=0 
end

else
begin
					select FITABLE026.COLUMN17 Vat, FITABLE026.COLUMN04 [Type], 
(CASE  WHEN (FITABLE026.COLUMN04='ADVANCE PAYMENT' OR FITABLE026.COLUMN04='Bill'  OR FITABLE026.COLUMN04='BILL PAYMENT' OR FITABLE026.COLUMN04='PAYMENT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'INPUT%' then (select COLUMN05 from SATABLE001 where isnull(SATABLE001.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Invoice' OR FITABLE026.COLUMN04='RECEIVE PAYMENT'  OR FITABLE026.COLUMN04='RECEIPT VOUCHER'  OR FITABLE026.COLUMN04='JOURNAL ENTRY')AND FITABLE026.COLUMN17 like'OUTPUT%' then (select COLUMN05 from SATABLE002 where isnull(SATABLE002.COLUMNA13,0)=0 and COLUMN02=FITABLE026.COLUMN06)
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50=22335)  then s2.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51=22305) then s1.COLUMN05 
when (FITABLE026.COLUMN04='Debit Memo'  and p1.COLUMN50!=22335)  then s1.COLUMN05  
when (FITABLE026.COLUMN04='Credit Memo' and s5.COLUMN51!=22305) then s2.COLUMN05 
else  NULL end) Name,
					(  isnull(sum(isnull(FITABLE026.COLUMN11,0)),0)-(isnull(sum(isnull(FITABLE026.COLUMN12,0)),0))) Amount,

			                (SELECT MATABLE007.COLUMN04 FROM MATABLE007 WHERE isnull(MATABLE007.COLUMNA13,0)=0 and MATABLE007.COLUMN02= FITABLE026.COLUMN13) ITEM ,

					iif(FITABLE026.COLUMN04='JOURNAL ENTRY' AND isnull(FITABLE026.COLUMN16,0)>0, 0,sum(isnull(FITABLE026.COLUMN14,0)))COLUMN14,FITABLE026.COLUMN16 , FITABLE026.COLUMN09 TASNSACTION, MATABLE005.COLUMN04  BrandIDN
					from FITABLE026
					LEFT OUTER join MATABLE002 on MATABLE002.COLUMN02=FITABLE026.COLUMN10
					LEFT OUTER join MATABLE007 on MATABLE007.COLUMN02=FITABLE026.COLUMN13
					left join MATABLE005  on MATABLE005.column02=MATABLE007.column10 
					left join SATABLE001 s1  on s1.column02=FITABLE026.column06 
					left join SATABLE002 s2  on s2.column02=FITABLE026.column06 
					left join SATABLE005 s5  on s5.column01=FITABLE026.column05 
					left join PUTABLE001 p1  on p1.column01=FITABLE026.column05
					--left join FITABLE031 jh  on jh.column01=FITABLE026.column05 and jh.columnA03=FITABLE026.columnA03
					--left join FITABLE032 jv  on jv.column12=jh.column01 and jh.columnA03=jv.columnA03
			                WHERE isnull(FITABLE026.COLUMNA13,0)=0 and FITABLE026.COLUMN08 not in(1134,1135,1136,1137,1138,1139,1140,1141,1142) and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner AND FITABLE026.COLUMN03 BETWEEN @FrDate AND @ToDate   
					group by MATABLE002.COLUMN04, MATABLE005.COLUMN04 ,FITABLE026.COLUMN04,FITABLE026.COLUMN06,FITABLE026.COLUMN10,FITABLE026.COLUMN16,MATABLE007.COLUMN04,FITABLE026.COLUMN13,FITABLE026.COLUMN14,FITABLE026.COLUMN16,FITABLE026.COLUMN17, FITABLE026.COLUMN09,s1.COLUMN05,s2.COLUMN05,s5.COLUMN51,p1.COLUMN50 having isnull(FITABLE026.COLUMN16,0)>0  and isnull(FITABLE026.COLUMN10,0)!=0 
end
		END
END


































GO
