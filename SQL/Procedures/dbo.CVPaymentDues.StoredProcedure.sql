USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CVPaymentDues]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CVPaymentDues]
(
@ReportName  nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@DateF nvarchar(250)=null)
as 
begin

declare @whereStr nvarchar(1000)=null
declare	@Query1 nvarchar(max)=null

 IF  @ReportName='VendorPaymentDues'
BEGIN
if @OperatingUnit!=''
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1) '
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1) '
end
end

if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end
select * into #VendorPaymentDues from (

SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07  and COLUMNA03=@AcOwner) vendor,(CASE WHEN a.COLUMN06 ='JOURNAL ENTRY' THEN fj.column06 ELSE  a.COLUMN18 END) Memo,FORMAT(a.COLUMN04,@DateF) pdt,a.COLUMN04 Dtt,
a.COLUMN09 pbno, a.COLUMN12 total,a.COLUMN13 payment,
--a.COLUMN14
 (isnull(a.COLUMN12,0) - isnull(a.COLUMN13,0)) due,
FORMAT(a.COLUMN11,@DateF) dt,a.COLUMNA02 OPID,a.COLUMN07 VID
,a.COLUMN06 fname

,(case
 when a.COLUMN06='BILL' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and COLUMNA03=@AcOwner)))
 when  a.COLUMN06='BILL PAYMENT' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03=@AcOwner))
 when  a.COLUMN06='Debit Memo' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and COLUMNA03=@AcOwner)))

end) Brand 
,(case
 when a.COLUMN06='BILL' then (select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and  COLUMNA03=@AcOwner))
 when  a.COLUMN06='BILL PAYMENT' then (select (COLUMN10) from MATABLE007 where COLUMN02 = 
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03=@AcOwner))
  when a.COLUMN06='Debit Memo' then (select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and  COLUMNA03=@AcOwner))
  when a.COLUMN06='JOURNAL ENTRY' then (1)
  when a.COLUMN06='PAYMENT VOUCHER' then (1)
 end) BrandID 

FROM PUTABLE016 a 
left outer join FITABLE031 j on j.COLUMN01=a.COLUMN05 and j.COLUMNA03=a.COLUMNA03 and (j.COLUMNA02=a.COLUMNA02 or j.COLUMNA02 is null) and isnull(j.COLUMNA13,0)=0 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and (fj.COLUMNA02=j.COLUMNA02  or fj.COLUMNA02 is null) and isnull(fj.COLUMNA13,0)=0 and (((a.COLUMN12) in (fj.COLUMN05)) or ((a.COLUMN13) in (fj.COLUMN04)))
 and ISNULL(a.column07,0) = ISNULL(fj.column07,0) 
where  isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  a.COLUMNA03=@AcOwner
and  a.COLUMN04 between @FromDate and @ToDate 
union all
select (select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as ou,
((select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07  and COLUMNA03=@AcOwner) ) vendor,fitable020.COLUMN08 Memo,FORMAT(fitable020.COLUMN05,@DateF) pdt,fitable020.COLUMN05 Dtt,fitable020.COLUMN04 pbno,0.00 total,(fitable020.COLUMN18) payment,-isnull(fitable020.COLUMN18,0) due, null dt,fitable020.COLUMNA02 OPID,fitable020.COLUMN07 VID 
--,'5' fname
,cast(fitable020.COLUMN03 as varchar) fname, '' Brand
,1 BrandID 
 from fitable020 fitable020
where COLUMN03=1363  and isnull((COLUMNA13),0)=0 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  
and  fitable020.COLUMN05 between @FromDate and @ToDate 

union all
--PREVIOUS BLANCE

 select ou,vendor,Memo,pdt,Dtt,pbno, 
    iif( sum(isnull(total,0))>sum(isnull(payment,0)),sum(isnull(total,0))-sum(isnull(payment,0)),0) total,
 
    iif( sum(isnull(payment,0))>sum(isnull(total,0)),sum(isnull(payment,0))-sum(isnull(total,0)),0) payment,
 
 

 sum(isnull(due,0)),dt,OPID,VID,fname,Brand,BrandID from (

SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07  and COLUMNA03=@AcOwner) vendor,(CASE WHEN a.COLUMN06 ='JOURNAL ENTRY' THEN fj.column06 ELSE  a.COLUMN18 END) Memo,NULL pdt,NULL Dtt,
'PREVIOUS BALANCE' pbno, SUM(ISNULL(a.COLUMN12,0)) total,SUM(ISNULL(a.COLUMN13,0)) payment,

SUM(ISNULL(a.COLUMN14,0))
  due,
NULL dt,a.COLUMNA02 OPID,a.COLUMN07 VID
,null fname,

iif(@Brand='',
null
,
(case
 when a.COLUMN06='BILL' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and COLUMNA03=@AcOwner)))
 when  a.COLUMN06='BILL PAYMENT' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03=@AcOwner))
 when  a.COLUMN06='Debit Memo' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and COLUMNA03=@AcOwner)))

end)

) 
 Brand 
,
iif(@Brand='',
(case
 when a.COLUMN06='BILL' then (1)
  when  a.COLUMN06='BILL PAYMENT' then  (1)
  when a.COLUMN06='Debit Memo' then  (1)
  when a.COLUMN06='JOURNAL ENTRY' then (1)
  when a.COLUMN06='PAYMENT VOUCHER' then (1)
 end),

(case
 when a.COLUMN06='BILL' then isnull((select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN04)  from PUTABLE006 where COLUMN13 = a.COLUMN05 and  COLUMNA03=@AcOwner)),1)
 when  a.COLUMN06='BILL PAYMENT' then  isnull((select (COLUMN10) from MATABLE007 where COLUMN02 = 
(select max(p6.COLUMN04) from PUTABLE005 p5 inner join PUTABLE015 p15 on p15.COLUMN03=p5.COLUMN02 inner join PUTABLE006 p6  on p6.COLUMN13=p5.COLUMN01
 where p15.COLUMN08= a.COLUMN05 and p15.COLUMNA03=@AcOwner)),1)
  when a.COLUMN06='Debit Memo' then  isnull((select (COLUMN10) from MATABLE007 where COLUMN02 = ( select max(COLUMN03)  from PUTABLE002 where COLUMN19 = a.COLUMN05 and  COLUMNA03=@AcOwner)),1)
  when a.COLUMN06='JOURNAL ENTRY' then (1)
  when a.COLUMN06='PAYMENT VOUCHER' then (1)
 end))
  BrandID   

FROM PUTABLE016 a  
left outer join FITABLE031 j on j.COLUMN04=a.COLUMN05 and j.COLUMNA03=a.COLUMNA03 and (j.COLUMNA02=a.COLUMNA02 or j.COLUMNA02 is null) and isnull(j.COLUMNA13,0)=0 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and (fj.COLUMNA02=j.COLUMNA02  or fj.COLUMNA02 is null) and isnull(fj.COLUMNA13,0)=0 and (((a.COLUMN12) in (fj.COLUMN05)) or ((a.COLUMN13) in (fj.COLUMN04)))
 --and a.column08 = fj.column07  
where  isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  a.COLUMNA03=@AcOwner
and  a.COLUMN04 < @FromDate  
group by  a.COLUMNA02,a.COLUMN07,a.COLUMN18
,a.COLUMN05,a.COLUMN06,fj.COLUMN06

union all

select (select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as ou,
((select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07  and COLUMNA03=@AcOwner) ) vendor,fitable020.COLUMN08 Memo,NULL pdt,NULL Dtt,
'PREVIOUS BALANCE' pbno,0 total,(SUM(isnull(fitable020.COLUMN18,0))) payment,(SUM(isnull(fitable020.COLUMN18,0))) due, null dt,fitable020.COLUMNA02 OPID,fitable020.COLUMN07 VID 
--,'5' fname
,null fname, null Brand
,1 BrandID 
 from fitable020 fitable020
where COLUMN03=1363  and isnull((COLUMNA13),0)=0 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  
and  fitable020.COLUMN05 < @FromDate 
 group by  fitable020.COLUMNA02,fitable020.COLUMN07,FITABLE020.COLUMN11,fitable020.COLUMN03,fitable020.COLUMN08

 ) a
group by 
 a.ou,a.vendor,a.Memo,a.pdt,a.pbno,a.dt,a.Dtt,a.OPID,a.VID,a.fname,a.Brand,a.BrandID


)Query
set @Query1='select * from #VendorPaymentDues'+@whereStr+' order by Dtt desc'
exec (@Query1) 
end

ELSE IF  @ReportName='CustomerPaymentDues'
BEGIN

if @OperatingUnit!=''
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1)'
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1)'
end
end

if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end
select * into #CustomerPaymentDues from (

SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,c.COLUMN11 As Mobile,
(select COLUMN10 from    SATABLE003 where COLUMN02=c.COLUMN16) as City,
c.COLUMN05 Party,FORMAT(a.COLUMN04,@DateF) Date,a.COLUMN04 Dtt,a.COLUMN05 [Bill No.],a.COLUMN09 Amount,a.COLUMN11 Paid,
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [0-30],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [31-45],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [46-60],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [61-90],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [>90],a.COLUMN12 Balance,a.COLUMN06 fname
,a.COLUMN07 CID,a.COLUMNA02 OPID 
--,(case
-- when a.COLUMN06='Invoice' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner))

-- when a.COLUMN06='Credit Memo' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))

--end) Brand ,
--(case
-- when a.COLUMN06='Invoice' then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select (COLUMN10) from MATABLE007 where COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner)) 

-- when a.COLUMN06='Credit Memo'  then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))
-- when a.COLUMN06='JOURNAL ENTRY' then (1)
--  when a.COLUMN06='RECEIPT VOUCHER' then (1)
--end) BrandID
 ,m5.COLUMN04 Brand,c.COLUMN37 BrandID
 FROM PUTABLE018 a 
 left join SATABLE002 c on c.COLUMN02=a.COLUMN07 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER' or a.COLUMN06 like 'PAYMENT VOUCHER')  and
 a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)   AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0
  and  a.COLUMN04 between @FromDate and @ToDate   
  group by a.COLUMNA02,c.COLUMN11,c.COLUMN16,c.COLUMN05,a.COLUMN04,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN12,a.COLUMN07 ,a.COLUMN06,m5.COLUMN04,c.COLUMN37

  union all
select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02 and  COLUMNA03=a.COLUMNA03) as OperatingUnit,c.COLUMN11 As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=c.COLUMN16) as City,c.column05 Party,FORMAT(a.COLUMN05,@DateF) Date,a.COLUMN05 Dtt, a.COLUMN04 [Bill No.],0.00 Amount,cast(isnull(a.COLUMN19,0)as decimal(18,2)) Paid,
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2))  END) AS [0-30],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2)) END) AS [31-45],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2))  END) AS [46-60],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2)) END) AS [61-90],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2))  END) AS [>90],
0.00 Balance   ,cast(a.COLUMN03 as varchar) fname 
,a.COLUMN08 CID,a.COLUMNA02 OPID ,m5.COLUMN04 Brand,c.COLUMN37 BrandID  from FITABLE023 a
 left join SATABLE002 c on c.COLUMN02=a.COLUMN08 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 where a.COLUMN03=1386 and isnull((a.COLUMNA13),0)=0    AND a.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  a.COLUMNA03=@AcOwner
 and  a.COLUMN05 between @FromDate and @ToDate 

 union all
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=c.COLUMN16) as City,
c.COLUMN05 Party,FORMAT(b.COLUMN04,@DateF) Date,b.COLUMN04 Dtt,
b.COLUMN09 [Bill No.], NULL Amount,b.COLUMN07 Paid,
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2))  END) AS [0-30],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2)) END) AS [31-45],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2))  END) AS [46-60],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2)) END) AS [61-90],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2))  END) AS [>90],
NULL Balance  ,b.COLUMN03 fname
,b.COLUMN06 CID,b.COLUMNA02 OPID,m5.COLUMN04 Brand,c.COLUMN37 BrandID 
 FROM FITABLE036 b  
 left join SATABLE002 c on c.COLUMN02=b.COLUMN06 and c.COLUMNA03=b.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
  where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049  AND b.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  b.COLUMNA03=@AcOwner  
  and  b.COLUMN04 between @FromDate and @ToDate 
  
  --PREVIOUS BALANCE


  union all

  select OperatingUnit,Mobile,City,Party,Date,Dtt,[Bill No.],
      iif( sum(isnull(Amount,0))>sum(isnull(Paid,0)),sum(isnull(Amount,0))-sum(isnull(Paid,0)),0) Amount,
 
    iif( sum(isnull(Paid,0))>sum(isnull(Amount,0)),sum(isnull(Paid,0))-sum(isnull(Amount,0)),0) Paid,
 
  [0-30],[31-45],[46-60],[61-90],[>90],Balance,fname,CID,OPID,Brand,BrandID from (


SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,null As Mobile,
(select COLUMN10 from    SATABLE003 where COLUMN02=c.COLUMN16) as City,
c.COLUMN05 Party,null Date,null Dtt, 'PREVIOUS BALANCE'  [Bill No.],a.COLUMN09 Amount,a.COLUMN11 Paid,
null  [0-30],
null  [31-45],
null  [46-60],
null  [61-90],
null  [>90],0 Balance,null fname
,a.COLUMN07 CID,a.COLUMNA02 OPID 
--,
--iif(@Brand='',
--null,
-- (case
-- when a.COLUMN06='Invoice' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner))

-- when a.COLUMN06='Credit Memo' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))

--end))
--  Brand ,
--iif(@Brand='',
--(case
-- when a.COLUMN06='Invoice' then (1)
--  when  a.COLUMN06='Payment' then  (1)
--  when a.COLUMN06='Credit Memo' then  (1)
--  when a.COLUMN06='JOURNAL ENTRY' then (1)
--  when a.COLUMN06='RECEIPT VOUCHER' then (1)
-- end),
--(case
-- when a.COLUMN06='Invoice' then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select (COLUMN10) from MATABLE007 where COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner)) 

-- when a.COLUMN06='Credit Memo'  then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))
-- when a.COLUMN06='JOURNAL ENTRY' then (1)
--  when a.COLUMN06='RECEIPT VOUCHER' then (1)
--end)) BrandID
 ,m5.COLUMN04 Brand,c.COLUMN37 BrandID 
 FROM PUTABLE018 a 
 left join SATABLE002 c on c.COLUMN02=a.COLUMN07 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER' or a.COLUMN06 like 'PAYMENT VOUCHER')  and
 a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)   AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0
  and  a.COLUMN04 < @FromDate   
  --group by a.COLUMN07,a.COLUMNA02,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN04,a.COLUMN12 ,a.COLUMN06

  
  union all

select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,null As Mobile,
(select COLUMN10 from SATABLE003 where COLUMN02=c.COLUMN16) as City,c.column05 Party,
null Date,null Dtt, 'PREVIOUS BALANCE' [Bill No.],0 Amount,cast(isnull(a.COLUMN19,0)as decimal(18,2)) Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0 Balance   ,null fname 
,a.COLUMN08 CID,a.COLUMNA02 OPID ,m5.COLUMN04 Brand,c.COLUMN37 BrandID   from FITABLE023 a
 left join SATABLE002 c on c.COLUMN02=a.COLUMN08 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
where a.COLUMN03=1386 and isnull((a.COLUMNA13),0)=0    AND a.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  a.COLUMNA03=@AcOwner
 and  a.COLUMN05 < @FromDate  
 union all

SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where 
COLUMN02=c.COLUMN16) as City,c.COLUMN05 Party,
null Date,null Dtt,'PREVIOUS BALANCE' [Bill No.], 0 Amount,b.COLUMN07 Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0 Balance  ,null fname
,b.COLUMN06 CID,b.COLUMNA02 OPID,m5.COLUMN04 Brand,c.COLUMN37 BrandID
 FROM FITABLE036 b 
 left join SATABLE002 c on c.COLUMN02=b.COLUMN06 and c.COLUMNA03=b.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
  where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049  AND b.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  b.COLUMNA03=@AcOwner  
  and  b.COLUMN04 < @FromDate 
 
 ) a group by

 a.OperatingUnit,a.Mobile,a.City,a.Party,a.Date,a.Dtt,a.[Bill No.],a.[0-30],a.[31-45],
 a.[46-60],a.[61-90],a.[>90],a.Balance,a.fname,a.CID,a.OPID,a.Brand,a.BrandID
 

)Query
set @Query1='select * from #CustomerPaymentDues'+@whereStr +' order by City,Dtt desc'
exec (@Query1) 
END

end


GO
