USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PRTABLE007_RCDirect]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PRO_TP_PRTABLE007_RCDirect]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE007_SequenceNo
insert into PRTABLE007 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMNA01, 
   COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21, 
   @COLUMN22,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
set @ReturnValue = (select COLUMN01 from PRTABLE007 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from PRTABLE007

END 

 

ELSE IF @Direction = 'Update'

BEGIN
declare @Project nvarchar(250),@Transno nvarchar(250)
set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN02=(@COLUMN02))
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @Transno= (SELECT COLUMN04 FROM PRTABLE007 WHERE COLUMN02=(@COLUMN02))

UPDATE PRTABLE007 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,  
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,        
   COLUMN22=@COLUMN22,   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
	  declare @internalid nvarchar(250)=null,@chk bit
      DECLARE @MaxRownum INT,@PID nvarchar(250),@id nvarchar(250),@ItemQty nvarchar(250),@Item nvarchar(250),@uom nvarchar(250)
	  declare @Qty_On_Hand decimal(18,2),@SAvgCost  decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
      @Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2),@Amount  decimal(18,2),@Projectactualcost  decimal(18,2)
      DECLARE @Initialrow INT=1, @DecimalPositions int
	  if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
	  begin
	  set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
	  end
	  else
	  begin set @DecimalPositions=(2) end
	  set @PID=(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
	  set @COLUMN13=(select COLUMN11 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN  

		  set @Item=(select COLUMN03 from PRTABLE008 where COLUMN02=@id)
          set @ItemQty=(select isnull(COLUMN06,0) from PRTABLE008 where COLUMN02=@id)
		  set @ItemQty=(round(cast(@ItemQty as decimal(18,7)),@DecimalPositions))
          set @Price=(select isnull(COLUMN08,0) from PRTABLE008 where COLUMN02=@id)
		  set @uom=(select COLUMN11 from PRTABLE008 where COLUMN02=@id)
		  set @chk=(select column48 from matable007 where column02=@Item)
		  set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@id)
		  set @Amount=(cast(@ItemQty as DECIMAL(18,2))*cast(@Price as DECIMAL(18,2)))
		  set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
		  update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))-cast(@Amount as DECIMAL(18,2))) where column02=@Project
		  if(@chk=1)
		  begin
		  set @Qty_On_Hand1=( round(cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions)+ round(cast(@ItemQty as decimal(18,7)),@DecimalPositions))
				 if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( round(cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions)+ round(cast(@ItemQty as decimal(18,7)),@DecimalPositions));
				set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))+((round(cast(@ItemQty as decimal(18,7)),@DecimalPositions))*(cast(@SAvgCost as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
				set @Qty_On_Hand=( round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions));
				if(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))/round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
				end
				end
		 end
		 delete from FITABLE036 WHERE COLUMN05=cast(@internalid as nvarchar(250)) and COLUMN03='Resource Consumption Direct' AND COLUMN09=cast(@Transno as nvarchar(250)) AND COLUMN10=1153 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
         
        CLOSE cur1
        deallocate cur1

delete from PUTABLE017  WHERE COLUMN05 in(@PID) and COLUMN06='Resource Consumption Direct' and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
UPDATE PRTABLE008 SET COLUMNA13=1 WHERE COLUMN10 in(@PID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
   
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE PRTABLE007 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02
set @COLUMNA02=(select COLUMNA02 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
set @COLUMNA03=(select COLUMNA03 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
set @Transno= (SELECT COLUMN04 FROM PRTABLE007 WHERE COLUMN02=(@COLUMN02))
set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN02=(@COLUMN02))
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @Initialrow =1
	  set @PID=(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
      
	  set @COLUMN13=((select COLUMN11 from PRTABLE007 WHERE COLUMN02 = @COLUMN02))
	  if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
	  begin
	  set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
	  end
	  else
	  begin set @DecimalPositions=(2) end
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN  
		  set @COLUMN04=(select COLUMN04 from PRTABLE008 where COLUMN02=@id)
		  set @Item=(select COLUMN03 from PRTABLE008 where COLUMN02=@id)
          set @ItemQty=(select isnull(COLUMN06,0) from PRTABLE008 where COLUMN02=@id)
          set @ItemQty=(round(cast(@ItemQty as decimal(18,7)),@DecimalPositions))
          set @Price=(select isnull(COLUMN08,0) from PRTABLE008 where COLUMN02=@id)
		  set @uom=(select COLUMN11 from PRTABLE008 where COLUMN02=@id)
		  set @chk=(select column48 from matable007 where column02=@Item)
		  set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@id)
		set @Amount=(cast(@ItemQty as DECIMAL(18,2))*cast(@Price as DECIMAL(18,2)))
		set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
		update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))-cast(@Amount as DECIMAL(18,2))) where column02=@Project
		 if(@chk=1)
		 begin
		 set @Qty_On_Hand1=(round(cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions)+ round(cast(@ItemQty as decimal(18,7)),@DecimalPositions))
				 if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( round(cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions)+ round(cast(@ItemQty as decimal(18,7)),@DecimalPositions));
				set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))+((round(cast(@ItemQty as decimal(18,7)),@DecimalPositions))*(cast(@SAvgCost as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
				set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions));
				if(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
				end 
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))/round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
				end
				end
			 end
		 delete from FITABLE036 WHERE COLUMN05=cast(@internalid as nvarchar(250)) and COLUMN03='Resource Consumption Direct' AND COLUMN09=cast(@Transno as nvarchar(250)) AND COLUMN10=1153 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
         
        CLOSE cur1
        deallocate cur1

delete from PUTABLE017  WHERE COLUMN05 in(@PID) and COLUMN06='Resource Consumption Direct' and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
UPDATE PRTABLE008 SET COLUMNA13=1 WHERE COLUMN10 in(@PID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03

END

end try
begin catch
return 0
end catch
end


















GO
