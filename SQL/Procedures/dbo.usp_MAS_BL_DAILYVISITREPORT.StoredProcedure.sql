USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAS_BL_DAILYVISITREPORT]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAS_BL_DAILYVISITREPORT]
(
     @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16 nvarchar(250)=null,    
	@COLUMN17 nvarchar(250)=null,    @COLUMN18 nvarchar(250)=null,    @COLUMN19 nvarchar(250)=null, 
	@COLUMN20 nvarchar(250)=null,    @COLUMN21 nvarchar(250)=null,    @COLUMN22 nvarchar(250)=null, 
	@COLUMN23 nvarchar(250)=null,    @COLUMN24 nvarchar(250)=null,    @COLUMN25 nvarchar(250)=null,
	--EMPHCS1811 rajasekhar reddy patakota 10/09/2016 Line Grid Details filling in sales & Purchase module
	@COLUMN26 nvarchar(250)=null,    @COLUMN27 nvarchar(250)=null,    @COLUMN28 nvarchar(250)=null,   
	@COLUMN29 nvarchar(250)=null,    @COLUMN30 nvarchar(250)=null,    @COLUMN31 nvarchar(250)=null,
	@COLUMN32 nvarchar(250)=null,    @COLUMN33 nvarchar(250)=null,    @COLUMN34 nvarchar(250)=null,
	@COLUMN35 nvarchar(250)=null,	
	@COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null, 
    @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,
    @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250),       
	@ReturnValue int=null output 
)
AS
BEGIN
IF  @TabelName='MATABLE019'
BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN35!= '' and @COLUMN35 is not null and @COLUMN35!= '0' ) THEN @COLUMN35 else @COLUMNA02  END )
EXEC usp_MAS_TP_MATABLE019 
   @COLUMN02,  @COLUMN03, @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,   @COLUMN08,  @COLUMN09, @COLUMN10,   @COLUMN11,
   @COLUMN12,  @COLUMN13, @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,   @COLUMN18,  @COLUMN19, @COLUMN20,   @COLUMN21,
   @COLUMN22,  @COLUMN23, @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,   @COLUMN28,  @COLUMN29, @COLUMN30,   @COLUMN31,  
   @COLUMN32,  @COLUMN33, @COLUMN34,  @COLUMN35,
   @COLUMNA01, @COLUMNA02, 
   @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, 
   @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, 
   @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, 
   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue output
select @ReturnValue
END

IF  @TabelName='MATABLE029'
BEGIN
EXEC usp_MAS_TP_MATABLE029 
   @COLUMN02,  @COLUMN03, @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,   @COLUMN08,  @COLUMN09, @COLUMN10,   @COLUMN11,
   @COLUMN12,  @COLUMN13, @COLUMN14,  @COLUMN15,  
   @COLUMNA01, @COLUMNA02, 
   @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, 
   @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, 
   @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, 
   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue
END
IF  @TabelName='SETABLE015'
BEGIN
EXEC usp_MAS_TP_SETABLE015
@COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, @COLUMN10,   @COLUMN11,
@COLUMN12,
@COLUMNA01, @COLUMNA02, 
   @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, 
   @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, 
   @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, 
   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue
END

end















GO
