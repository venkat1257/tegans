USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[OVERDUES_CREDITLIMIT]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[OVERDUES_CREDITLIMIT]
(
 @CustomerId  nvarchar(250)= null,
@AcOwner      nvarchar(250)= null,
@Date nvarchar(250)=null)
AS
BEGIN

select isnull(min(DueDays),0) as DueDays,isnull(sum(Balance),0) as Balance  from(

SELECT  DATEDIFF(day,a.COLUMN04,@Date) 'DueDays',a.COLUMN04 dates,(isnull(cast(COLUMN11 as decimal(18,2)),0))-(isnull(cast(COLUMN09 as decimal(18,2)),0)) Balance
FROM PUTABLE018 a   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'PAYMENT VOUCHER' or a.COLUMN06 like 'RECEIPT VOUCHER')  and
a.COLUMN07=  @CustomerId   AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0 and (isnull(cast(COLUMN09 as decimal(18,2)),0))-(isnull(cast(COLUMN11 as decimal(18,2)),0))!=0 group by a.COLUMN04,a.COLUMN09,a.COLUMN11

union all
select DATEDIFF(day,COLUMN05,@Date) 'DueDays',COLUMN05 dates,sum(isnull(cast(COLUMN19 as decimal(18,2)),0)) Balance  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN08=  @CustomerId  AND  COLUMNA03=@AcOwner  and isnull(cast(COLUMN19 as decimal(18,2)),0)!=0 group by FITABLE023.COLUMN05

union all
SELECT  DATEDIFF(day,b.COLUMN04,@Date) 'DueDays',b.COLUMN04 dates,-(sum(isnull(cast(b.COLUMN07 as decimal(18,2)),0))-sum(isnull(cast(b.COLUMN08 as decimal(18,2)),0))) Balance
FROM FITABLE036 b   where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049 AND b.COLUMN06=  @CustomerId  AND  b.COLUMNA03=@AcOwner 
group by b.COLUMN04 having  (sum(isnull(cast(b.COLUMN07 as decimal(18,2)),0))-sum(isnull(cast(b.COLUMN08 as decimal(18,2)),0)))!=0)D 

 --select isnull(max(DueDays),0) as DueDays,isnull(sum(Balance),0) as Balance  from(
 -- select(p.COLUMN20-(sum(isnull(s12.COLUMN06,0))+sum(isnull
 --(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0)))) Balance,DATEDIFF(day,p.COLUMN08,@Date) DueDays From SATABLE009
 --p left join  SATABLE002 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 left join SATABLE012 s12 on s12.COLUMN03=p.column02 and isnull
 --(s12.COLUMNA13,0)=0 and s12.COLUMNA03=p.COLUMNA03 where p.COLUMN05=@CustomerId  AND  p.COLUMNA03=@AcOwner and isnull((p.COLUMNA13),0)=0
 --group by p.COLUMN20,p.COLUMN08
 --having (p.COLUMN20-(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0))))!=0 )p

END




GO
