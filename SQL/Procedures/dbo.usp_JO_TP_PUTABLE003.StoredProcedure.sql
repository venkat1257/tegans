USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_PUTABLE003]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_PUTABLE003]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
        @COLUMN23   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250),       
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08='1'  order by COLUMN01 desc
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE003_SequenceNo
insert into PUTABLE003 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,
  COLUMN22,  COLUMN23,
  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
--if(@COLUMN20='true' or @COLUMN20='1')
--begin
 --if(@COLUMN03=1286)
	--Begin
    declare @WIP decimal(18,2) 
  declare @Qty decimal(18,2) 
	--  --EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
  declare @uom nvarchar(250) 
 
	  declare @id nvarchar(250)
    declare @JOID nvarchar(250)
     declare @Item nvarchar(250)
     declare @JOItems nvarchar(250)
     DECLARE @MaxRownum INT
    DECLARE @Initialrow INT=1
 --     set @JOID=(select COLUMN01 from SATABLE007 where COLUMN06=@COLUMN06 and COLUMNA13=0 and COLUMN04=@COLUMN10 and COLUMNA03=@COLUMNA03)
	--  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14=@JOID and COLUMNA13=0
 --     OPEN cur1
	--  FETCH NEXT FROM cur1 INTO @id
 --     SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE008 where  COLUMN14=@JOID and COLUMNA13=0)
 --        WHILE @Initialrow <= @MaxRownum
 --        BEGIN 
 --            set @Item=(SELECT COLUMN04 FROM SATABLE008 WHERE COLUMN02=@id  and COLUMN14=@JOID)
	--     set @uom=(SELECT COLUMN19 FROM SATABLE008 WHERE COLUMN02=@id  and COLUMN14=@JOID)
 --            set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
 --            set @Qty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN02=@id  and COLUMN04=@Item and COLUMN14=@JOID)
	--	     UPDATE FITABLE010 SET COLUMN18=@WIP-@Qty WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
	--		 FETCH NEXT FROM cur1 INTO @id
 --            SET @Initialrow = @Initialrow + 1 
	--		 END
 --       CLOSE cur1 
	--End
	--end
	declare @closechk bit,@Result1 nvarchar(250)
set @closechk=(SELECT isnull(COLUMN20,0) FROM PUTABLE003 WHERE COLUMN02=@COLUMN02)
set @closechk=(iif(cast(@closechk as nvarchar(250))='','0',isnull(@closechk,0)))
set @Result1=(select COLUMN04 from CONTABLE025 where COLUMN02=27)
if(@COLUMN20=1 or @COLUMN20='True' OR @COLUMN20='true' or @COLUMN20='1')
begin
set @Result1=(select COLUMN04 from CONTABLE025 where COLUMN02=142)
end
ELSE if(@COLUMN20=0 OR @COLUMN20='0' or @COLUMN20='FALSE' or @COLUMN20='False')
begin
set @Result1=(select COLUMN04 from CONTABLE025 where COLUMN02=143)
end
UPDATE SATABLE007 set COLUMN14=@Result1 where COLUMN06=(select ISNULL(COLUMN06,0) from PUTABLE003 where COLUMN02= @COLUMN02)
UPDATE F1 SET COLUMN18 = F1.COLUMN18 - S8.COLUMN09 FROM PUTABLE003 P3
	INNER JOIN SATABLE007 S7 ON S7.COLUMN06 = P3.COLUMN06 AND S7.COLUMNA02 = P3.COLUMNA02 AND S7.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S7.COLUMNA13,0)=0
	INNER JOIN SATABLE008 S8 ON S8.COLUMN14 = S7.COLUMN01 AND S8.COLUMNA02 = P3.COLUMNA02 AND S8.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S8.COLUMNA13,0)=0
	INNER JOIN FITABLE010 F1 ON F1.COLUMN03 = S8.COLUMN04 AND F1.COLUMNA02 = P3.COLUMNA02 AND F1.COLUMNA03 = P3.COLUMNA03 AND ISNULL(F1.COLUMNA13,0)=0
	AND F1.COLUMN19 = S8.COLUMN19 AND F1.COLUMN21 = 0 AND F1.COLUMN22 = 0 
	WHERE P3.COLUMN03 = 1286 AND S7.COLUMN03 = 1284 AND ISNULL(P3.COLUMNA13,0)=0 and p3.column02 = @COLUMN02

	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue =(Select COLUMN01 from PUTABLE003 where COLUMN02=@COLUMN02)
END
 

IF @Direction = 'Select'
BEGIN
select * from PUTABLE003
END 

 
IF @Direction = 'Update'
BEGIN

	declare @location nvarchar(250),@linelocation nvarchar(250),@lotno nvarchar(250),@upcno nvarchar(250) = null
	set @location =(select COLUMN22 from  PUTABLE003 Where COLUMN02=@COLUMN02 );
	set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
	UPDATE F1 SET COLUMN18 = F1.COLUMN18 + S8.COLUMN09 FROM PUTABLE003 P3
	INNER JOIN SATABLE007 S7 ON S7.COLUMN06 = P3.COLUMN06 AND S7.COLUMNA02 = P3.COLUMNA02 AND S7.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S7.COLUMNA13,0)=0
	INNER JOIN SATABLE008 S8 ON S8.COLUMN14 = S7.COLUMN01 AND S8.COLUMNA02 = P3.COLUMNA02 AND S8.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S8.COLUMNA13,0)=0
	INNER JOIN FITABLE010 F1 ON F1.COLUMN03 = S8.COLUMN04 AND F1.COLUMNA02 = P3.COLUMNA02 AND F1.COLUMNA03 = P3.COLUMNA03 AND ISNULL(F1.COLUMNA13,0)=0
	AND F1.COLUMN19 = S8.COLUMN19 AND F1.COLUMN21 = 0 AND F1.COLUMN22 = 0 
	WHERE P3.COLUMN03 = 1286 AND S7.COLUMN03 = 1284 AND ISNULL(P3.COLUMNA13,0)=0 and p3.column02 = @COLUMN02
	UPDATE PUTABLE003 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,   
   COLUMN17=@COLUMN17,     COLUMN18=@COLUMN18,     COLUMN19=@COLUMN19,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,
   COLUMN22=@COLUMN22,     COLUMN23=@COLUMN23,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
   COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02

if(@COLUMN20='true' or @COLUMN20='1')
begin
 if(@COLUMN03=1286)
	Begin
	  declare @id1 nvarchar(250)
     DECLARE @MaxRownum1 INT
     DECLARE @Initialrow1 INT=1
   --   set @JOID=(select COLUMN01 from SATABLE007 where COLUMN06=@COLUMN06 and COLUMN04=@COLUMN10 and COLUMNA13=0 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
	  --DECLARE cur11 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14=@JOID and COLUMNA13=0
   --   OPEN cur11
	  --FETCH NEXT FROM cur11 INTO @id1
   --   SET @MaxRownum1 = (SELECT COUNT(*) FROM SATABLE008 where COLUMN14=@JOID and COLUMNA13=0)
   --      WHILE @Initialrow1 <= @MaxRownum1
   --      BEGIN 
   --          set @Item=(SELECT COLUMN04 FROM SATABLE008 WHERE COLUMN02=@id1  and COLUMN14=@JOID)
	  --   set @uom=(SELECT COLUMN19 FROM SATABLE008 WHERE COLUMN02=@id1  and COLUMN14=@JOID)
   --          set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
   --          set @Qty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN04=@Item and COLUMN14=@JOID)
		 --    UPDATE FITABLE010 SET COLUMN18=@WIP-@Qty WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
			-- FETCH NEXT FROM cur11 INTO @id1
   --          SET @Initialrow1 = @Initialrow1 + 1 
			-- END
   --     CLOSE cur11 
	End
	end
	else
	begin
	 if(@COLUMN03=1286)
	Begin 
	  declare @id2 nvarchar(250)
      DECLARE @MaxRownum2 INT
      DECLARE @Initialrow2 INT=1
   --   set @JOID=(select COLUMN01 from SATABLE007 where COLUMN06=@COLUMN06 and COLUMN04=@COLUMN10 and COLUMNA13=0 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
	  --DECLARE cur12 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14=@JOID and COLUMNA13=0
   --   OPEN cur12
	  --FETCH NEXT FROM cur12 INTO @id2
   --   SET @MaxRownum2 = (SELECT COUNT(*) FROM SATABLE008 where  COLUMN14=@JOID)
   --      WHILE @Initialrow2 <= @MaxRownum2
   --      BEGIN 
   --          set @Item=(SELECT COLUMN04 FROM SATABLE008 WHERE COLUMN02=@id2  and COLUMN14=@JOID)
	  --   set @uom=(SELECT COLUMN19 FROM SATABLE008 WHERE COLUMN02=@id2  and COLUMN14=@JOID)
   --          set @WIP=(select sum(isnull(COLUMN18,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
   --          set @Qty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN04=@Item and COLUMN02=@id2  and COLUMN14=@JOID)
		 --    UPDATE FITABLE010 SET COLUMN18=@Qty WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
			-- FETCH NEXT FROM cur12 INTO @id2
   --          SET @Initialrow2 = @Initialrow2 + 1 
			-- END
   --     CLOSE cur12 
	End
	end
 declare @POID nvarchar(250),@ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250)
 declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
 @Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2)
      set @POID=(select COLUMN01 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN08,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @Price=(select isnull(COLUMN10,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             --EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
			set @uom=(select isnull(COLUMN17,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			set @linelocation=(select cast(COLUMN28 as nvarchar(250)) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
			 set @lotno=(select isnull(COLUMN24,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @uom =(case when @uom='' then 0 when isnull(@uom,0)=0 then 0  else @uom end)
			 set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 set @upcno=(select COLUMN05 from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 --Received Qty Updation
			     set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @COLUMN06) and COLUMN03=@Item)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@COLUMN06))
				UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from PUTABLE001 where COLUMN02 in (@COLUMN06))
			   	--EMPHCS806 rajasekhar reddy patakota 28/7/2015 partial qty edit is IR not updating remaining qty in IR
				--EMPHCS854 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in item receipt
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  columna13=0 and COLUMN03=@Item and COLUMN02=@id) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN09,0)) from PUTABLE004 where   columna13=0 and  COLUMN02=@id and COLUMN03=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				update PUTABLE004 set COLUMN07=@TotalItemQty, COLUMN08=0, COLUMN09=@RemainItemQty where   COLUMN02=@id and COLUMN03=@Item
				
				declare @TotalBillQty decimal(18,2),@TotalBillAmt decimal(18,2),@BillAvgPrice decimal(18,2)
				set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM 
				((select sum(isnull(l.COLUMN08,0))Total from PUTABLE004 l 
				inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item  AND l.COLUMN05=@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMN13,0) AND 
				isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND 
				(case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 
				else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location and isnull(h.COLUMNA13,0)=0
				union all
				select isnull(sum(isnull(l.COLUMN04,0)),0)Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
				union all
				select isnull(sum(isnull(l.COLUMN08,0)-isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  
				inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
				where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND 
				isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND 
				isnull(l.COLUMN23,0)=@lotno  AND l.COLUMN04=@upcno and isnull(l.COLUMNA13,0)=0  AND 
				(case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') 
				then h.COLUMN16 else 0 end)=@Location )T)
				set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM 
				((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2)))Total from PUTABLE004 l 
				inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(h.COLUMN13,0)=isnull(@COLUMN13,0) AND 
				isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND  l.COLUMN05=@upcno AND
				(case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 
				else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location and isnull(h.COLUMNA13,0)=0
				union all
				select isnull(sum(isnull(l.COLUMN12,0)),0)Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
				union all
				select isnull((isnull(l.COLUMN08,0)-isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   
				inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
				where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND 
				isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND 
				isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND  l.COLUMN04=@upcno AND
				(case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') 
				then h.COLUMN16 else 0 end)=@Location )T)
				if exists(select l.COLUMN09 from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06=@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND 
				isnull(h.COLUMN15,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND 
				isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and 
				isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and 
				isnull(h.COLUMNA13,0)=0)
				begin
				set @TotalBillQty=cast((@TotalBillQty+cast((select sum(isnull(l.COLUMN09,0)) from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06=@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0) as decimal(18,2)))as decimal(18,2))
				set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(isnull(l.COLUMN12,0)) from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06=@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0) as decimal(18,2)))as decimal(18,2))
				end
				if(isnull(@TotalBillQty,0)=0)
				            begin
				set @BillAvgPrice=(0)
				end
				else
				begin
				set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/cast(isnull(@TotalBillQty,0) as decimal(18,2)))
				end
				declare @AvgPrice decimal(18,2), @FinalAvgPrice decimal(18,2)
				--if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='ITEM RECEIPT' and COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
				--begin
				--set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000) and isnull(COLUMN21,0)=@Location and COLUMN22 in(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='ITEM RECEIPT' and COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03))
				--end
				--else
				--begin
				set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000)  AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMN24 =@upcno)
				--end
				set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)

				--Inventory Updation
				--EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
			    set @Qty_On_Hand1= cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))- cast(@ItemQty as decimal(18,2))
				if exists(select COLUMN03 from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN24 =@upcno AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
				 begin
				set @Qty_On_Hand=( cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select sum(isnull(COLUMN05,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2)));
				set @Qty_Order = (cast((select sum(isnull(COLUMN07,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))+cast(@ItemQty as decimal(18,2)))
		declare @PrevQty1 decimal(18,2)
						declare @Qtywip decimal(18,2)
				set @PrevQty1=(select COLUMN08 from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
				set @Qtywip=(select sum(isnull(COLUMN18,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
if(@Qtywip>0)
begin
set @Qtywip=(@Qtywip-@PrevQty1)
end
else
begin
set @Qtywip=0.00
end
				if(@Qty_Order>0 or @Qty_Order>cast(@ItemQty as decimal(18,2)))
				Begin
				set @Qty_Order = (cast((select sum(isnull(COLUMN07,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				end
				else
				begin
				set @Qty_Order = 0;
				end
				set @Qty_Avl = cast(@Qty_Cmtd as decimal(18,2));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN07=@Qty_Order, COLUMN08=@Qty_On_Hand ,
				COLUMN12=@Qty_On_Hand*@BillAvgPrice
				 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN24 =@upcno AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
				--EMPHCS884 rajasekhar reddy patakota 8/8/2015 to deleete existing inventory asset records for  IR
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN17=0 WHERE COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select sum(isnull(COLUMN12,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0 AND COLUMN24 =@upcno
				end
				end
				
				--Inventory Asset table Updations
				delete from PUTABLE011 where COLUMN02=@POID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN13 and COLUMNA03=@COLUMNA03
				delete from PUTABLE017 where COLUMN05=@POID and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN13 and COLUMNA03=@COLUMNA03 and column06 = 'JobOrder Receipt'
			 --set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
		     --EMPHCS854 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in item receipt
			 UPDATE PUTABLE004 SET COLUMNA13=1 WHERE  COLUMN02 = @id and COLUMN03=@Item
			 if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
				begin
				EXEC usp_TP_InventoryAssetUpdate @POID,@COLUMN04,null,@Item,@Uom,null,null,@BillAvgPrice,@COLUMN13,@COLUMNA03
				--EXEC usp_TP_ProdIncomeUpdate @COLUMN12,@Transno,null,@COLUMN03,@COLUMN17,null,null,@BillAvgPrice,@COLUMN13,@COLUMNA03
				end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 

	UPDATE F1 SET COLUMN18 = F1.COLUMN18 - S8.COLUMN09 FROM PUTABLE003 P3
	INNER JOIN SATABLE007 S7 ON S7.COLUMN06 = P3.COLUMN06 AND S7.COLUMNA02 = P3.COLUMNA02 AND S7.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S7.COLUMNA13,0)=0
	INNER JOIN SATABLE008 S8 ON S8.COLUMN14 = S7.COLUMN01 AND S8.COLUMNA02 = P3.COLUMNA02 AND S8.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S8.COLUMNA13,0)=0
	INNER JOIN FITABLE010 F1 ON F1.COLUMN03 = S8.COLUMN04 AND F1.COLUMNA02 = P3.COLUMNA02 AND F1.COLUMNA03 = P3.COLUMNA03 AND ISNULL(F1.COLUMNA13,0)=0
	AND F1.COLUMN19 = S8.COLUMN19 AND F1.COLUMN21 = 0 AND F1.COLUMN22 = 0 
	WHERE P3.COLUMN03 = 1286 AND S7.COLUMN03 = 1284 AND ISNULL(P3.COLUMNA13,0)=0 and p3.column02 = @COLUMN02
--if  exists( SELECT 1 FROM PUTABLE013 WHERE COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from PUTABLE001 WHERE COLUMN02 = @COLUMN02))
--begin
--delete from PUTABLE013  where COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
--end
set @closechk=(SELECT isnull(COLUMN20,0) FROM PUTABLE003 WHERE COLUMN02=@COLUMN02)
set @closechk=(iif(cast(@closechk as nvarchar(250))='','0',isnull(@closechk,0)))
set @Result1=(select COLUMN04 from CONTABLE025 where COLUMN02=27)
if(@COLUMN20=1 or @COLUMN20='True' OR @COLUMN20='true' or @COLUMN20='1')
begin
set @Result1=(select COLUMN04 from CONTABLE025 where COLUMN02=142)
end
ELSE if(@COLUMN20=0 OR @COLUMN20='0' or @COLUMN20='FALSE' or @COLUMN20='False')
begin
set @Result1=(select COLUMN04 from CONTABLE025 where COLUMN02=143)
end
UPDATE SATABLE007 set COLUMN14=@Result1 where COLUMN06=(select ISNULL(COLUMN06,0) from PUTABLE003 where COLUMN02= @COLUMN02)
END


else IF @Direction = 'Delete'
BEGIN
declare @JO int,@FullRecpt nvarchar(250),@Ref nvarchar(250),@Acowner int
 select @JO=COLUMN06,@COLUMN04=COLUMN04 from PUTABLE003 where COLUMN02 = @COLUMN02
set @Ref=(select COLUMN10 from PUTABLE003 where COLUMN02 = @COLUMN02)
set @Acowner=(select COLUMNA03 from PUTABLE003 where COLUMN02 = @COLUMN02)
set @FullRecpt=(select COLUMN20 from PUTABLE003 where COLUMN02 = @COLUMN02)
set @COLUMN22=(select COLUMN22 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
set @location=(case when @COLUMN22='' then 0 when isnull(@COLUMN22,0)=0 then 0  else @COLUMN22 end)
set @COLUMNA08=(select COLUMNA11 from PUTABLE003 where COLUMN02 = @COLUMN02)
set @COLUMNA07=(select GETDATE())
set @COLUMNB01=(select COLUMN04 from PUTABLE003 where COLUMN02 = @COLUMN02)
set @COLUMNB02=(select COLUMN03 from PUTABLE003 where COLUMN02 = @COLUMN02)
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08='1' order by COLUMN01 desc
UPDATE PUTABLE003 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
if(@FullRecpt='true' or @FullRecpt='1')
begin
 --if(@COLUMN03=1286)
	--Begin
	  declare @idd nvarchar(250)
   --   DECLARE @MaxRownumd INT
   --   DECLARE @Initialrowd INT=1
   --   set @JOID=(select COLUMN01 from SATABLE007 where COLUMN06=@JO and COLUMNA13=0 and COLUMN04=@Ref and COLUMNA03=@Acowner)
	  --DECLARE cur1d CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14=@JOID and COLUMNA13=0
   --   OPEN cur1d
	  --FETCH NEXT FROM cur1d INTO @idd
   --   SET @MaxRownumd= (SELECT COUNT(*) FROM SATABLE008 where  COLUMN14=@JOID and COLUMNA13=0)
   --      WHILE @Initialrowd <= @MaxRownumd
   --      BEGIN 
   --          set @Item=(SELECT COLUMN04 FROM SATABLE008 WHERE COLUMN02=@idd  and COLUMN14=@JOID)
	  --   set @uom=(SELECT COLUMN19 FROM SATABLE008 WHERE COLUMN02=@idd  and COLUMN14=@JOID)
   --          set @WIP=(select sum(isnull(COLUMN18,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
   --          set @Qty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN02=@idd  and COLUMN04=@Item and COLUMN14=@JOID)
		 --    UPDATE FITABLE010 SET COLUMN18=@Qty WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
			-- FETCH NEXT FROM cur1d INTO @idd
   --          SET @Initialrowd = @Initialrowd + 1 
			-- END
   --     CLOSE cur1d 
	--End
	end
	--else
	--begin
	--declare @idd1 nvarchar(250)
 --     DECLARE @MaxRownumd1 INT
 --     DECLARE @Initialrowd1 INT=1
 --     set @JOID=(select COLUMN01 from SATABLE007 where COLUMN06=@JO)
	--  DECLARE cur1d1 CURSOR FOR SELECT COLUMN04 from SATABLE008 where COLUMN14=@JOID
 --     OPEN cur1d1
	--  FETCH NEXT FROM cur1d1 INTO @idd1
 --     SET @MaxRownumd1= (SELECT COUNT(*) FROM SATABLE008 where COLUMN14=@JOID)
 --        WHILE @Initialrowd1 <= @MaxRownumd1
 --        BEGIN 
 --            set @Item=(SELECT COLUMN04 FROM SATABLE008 WHERE COLUMN04=@id  and COLUMN14=@JOID)
	--     set @uom=(SELECT COLUMN19 FROM SATABLE008 WHERE COLUMN04=@id  and COLUMN14=@JOID)
 --            set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
 --            set @Qty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN04=@Item and COLUMN14=@JOID)
	--	     UPDATE FITABLE010 SET COLUMN18=@WIP-@Qty WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
	--		 FETCH NEXT FROM cur1d1 INTO @idd1
 --            SET @Initialrowd1 = @Initialrowd1 + 1 
	--		 END
 --       CLOSE cur1d1 
	--end
--EMPHCS854 rajasekhar reddy patakota 8/8/2015Deleted row condition checking in item receipt
declare @PID nvarchar(250),@TQty decimal(18,2),@Result nvarchar(250),@BQty decimal(18,2),@RecQty decimal(18,2)
      set @COLUMN06=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN13=(select COLUMN13 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @POID=(select COLUMN01 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @PID=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE004 where COLUMN02=@id)
             set @COLUMNA03=(select COLUMNA03 from PUTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN08,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @Price=(select isnull(COLUMN10,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @uom=(select COLUMN17 from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @linelocation=(select cast(COLUMN28 as nvarchar(250)) from PUTABLE004 where COLUMN02=@id)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
			 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
			 set @lotno=(select isnull(COLUMN24,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @upcno=(select COLUMN05 from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @uom =(case when @uom='' then 0 when isnull(@uom,0)=0 then 0  else @uom end)
	         set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 --Received Qty Updation
			    set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @COLUMN06) and COLUMN03=@Item)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@COLUMN06))
				UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from PUTABLE001 where COLUMN02 in (@COLUMN06))
			   
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  columna13=0 and COLUMN03=@Item and COLUMN02=@id) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN09,0)) from PUTABLE004 where   columna13=0 and  COLUMN02=@id and COLUMN03=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				update PUTABLE004 set COLUMN07=@TotalItemQty, COLUMN08=0, COLUMN09=@RemainItemQty where   COLUMN02=@id and COLUMN03=@Item
				--EMPHCS854 rajasekhar reddy patakota 8/8/2015Deleted row condition checking in item receipt
				SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @RecQty=(SELECT sum(COLUMN12) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @BQty=(SELECT sum(COLUMN13) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				--EMPHCS886 rajasekhar reddy patakota 8/8/2015 PO status is not changing back to Approved after delete
				if(cast(@RecQty as decimal(18,2))=0.00)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=04)
				end
				else IF(@BQty=(0.00))
				begin
				if(@TQty=@RecQty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=09)
				end
				else 
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=07)
				end
				end
				
				ELSE 
				BEGIN
				if(@TQty=@BQty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
				end
				else
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=06)
				end
				end
				
				UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @POID))
				
				set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM 
				((select sum(isnull(l.COLUMN08,0))Total from PUTABLE004 l 
				inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(h.COLUMN13,0)=isnull(@COLUMN13,0) AND 
				isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND
				 (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 
				 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND l.COLUMN05 =@upcno
				union all
				select isnull(sum(isnull(l.COLUMN04,0)),0)Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
				union all
				select isnull(sum(isnull(l.COLUMN08,0)-isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  
				inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
				where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND 
				isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=@lotno  AND l.COLUMN04 =@upcno
				and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') 
				then h.COLUMN16 else 0 end)=@Location )T)
				set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM 
				((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2)))Total from PUTABLE004 l 
				inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(h.COLUMN13,0)=isnull(@COLUMN13,0) AND 
				isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND 
				(case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 
				else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND l.COLUMN05 =@upcno
				union all
				select isnull(sum(isnull(l.COLUMN12,0)),0)Total from FITABLE047 l 
				where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@Item AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND 
				isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
				union all
				select isnull((isnull(l.COLUMN08,0)-isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   
				inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
				where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@Item AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND 
				isnull(l.COLUMNA02,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND  
				isnull(l.COLUMN23,0)=@lotno AND l.COLUMN04 =@upcno and isnull(l.COLUMNA13,0)=0  AND 
				(case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') 
				then h.COLUMN16 else 0 end)=@Location )T)
				if exists(select l.COLUMN09 from PUTABLE006 l 
				inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND 
				isnull(h.COLUMN15,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND 
				isnull(l.COLUMN27,0)=@lotno AND l.COLUMN06 =@upcno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and 
				isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and 
				isnull(h.COLUMNA13,0)=0) 
				begin
				set @TotalBillQty=cast((@TotalBillQty+cast((select sum(isnull(l.COLUMN09,0)) from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item AND l.COLUMN06 =@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0) as decimal(18,2)))as decimal(18,2))
				set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(isnull(l.COLUMN12,0)) from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
				where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@Item  AND l.COLUMN06 =@upcno and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN13,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0) as decimal(18,2)))as decimal(18,2))
				end
				if(isnull(@TotalBillQty,0)=0)
				            begin
				set @BillAvgPrice=(0)
				end
				else
				begin
				set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/cast(isnull(@TotalBillQty,0) as decimal(18,2)))
				end
				--if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='ITEM RECEIPT' and COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
				--begin
				--set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000) and isnull(COLUMN21,0)=@Location and COLUMN22 in(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='ITEM RECEIPT' and COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03))
				--end
				--else
				--begin
				set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000)  AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMN24 =@upcno)
				--end
				set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
				declare @PrevQty decimal(18,2)
             --Inventory Updation
	                    --EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
			    set @Qty_On_Hand1= cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))
				--- cast(@ItemQty as decimal(18,2))
				 if exists(select COLUMN03 from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
				 begin
				set @Qty_On_Hand=( cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select sum(isnull(COLUMN05,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2)));
				set @Qty_Order = (cast((select sum(isnull(COLUMN07,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))+cast(@ItemQty as decimal(18,2)))
		
				set @PrevQty=(SELECT cast(COLUMN06 as decimal(18,2)) from PUTABLE004  where COLUMN02=@id and COLUMN03=@Item)
				set @Qtywip=(select sum(isnull(COLUMN18,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
if(@Qtywip>0)
begin
set @Qtywip=(@Qtywip-(@PrevQty1-@ItemQty))
end
else
begin
set @Qtywip=0.00
end
				if(@Qty_Order>0 or @Qty_Order>cast(@ItemQty as decimal(18,2)))
				Begin
				set @Qty_Order = (cast((select sum(isnull(COLUMN07,0)) from FITABLE010 where COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				end
				else
				begin
				set @Qty_Order = 0;
				end
				set @Qty_Avl = cast(@Qty_Cmtd as decimal(18,2));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN07=@Qty_Order, COLUMN08=@Qty_On_Hand ,COLUMN12=@Qty_On_Hand*@BillAvgPrice,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE'
				 WHERE COLUMN03=@Item AND COLUMN24 =@upcno AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
				--EMPHCS884 rajasekhar reddy patakota 8/8/2015 to deleete existing inventory asset records for  IR
				--UPDATE FITABLE010 SET COLUMN17=round(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2)),1) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN17=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0 AND COLUMN24 =@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND COLUMN24 =@upcno AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom AND iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0 AND COLUMN24 =@upcno
				end
				end
				set @COLUMN04=(select COLUMN04 from PUTABLE001 where COLUMN02=@COLUMN06)
				set @COLUMN05=(select COLUMN05 from PUTABLE003 where COLUMN01=@POID)
				--Inventory Asset table Updations
				--update PUTABLE011 set  COLUMNA13=@COLUMNA13 where COLUMN02=@POID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN13 and COLUMNA03=@COLUMNA03
				--update PUTABLE017  set  COLUMNA13=@COLUMNA13 where COLUMN05=@POID and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN13 and COLUMNA03=@COLUMNA03
			    delete from PUTABLE011  where COLUMN02=@POID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN13 and COLUMNA03=@COLUMNA03
				delete from  PUTABLE017  where COLUMN05=@POID and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN13 and COLUMNA03=@COLUMNA03 and column06 = 'JobOrder Receipt'
			 --set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
			   UPDATE PUTABLE004 SET COLUMNA13=1 WHERE COLUMN02=@id
			   if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
				begin
				EXEC usp_TP_InventoryAssetUpdate @POID,@COLUMN04,null,@Item,@Uom,null,null,@BillAvgPrice,@COLUMN13,@COLUMNA03
				--EXEC usp_TP_ProdIncomeUpdate @COLUMN12,@Transno,null,@COLUMN03,@COLUMN17,null,null,@BillAvgPrice,@COLUMN13,@COLUMNA03
				end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
  UPDATE SATABLE007 set COLUMN14 = (select COLUMN04 from CONTABLE025 where COLUMN02=141) where COLUMN06=(select COLUMN06 from PUTABLE003 where COLUMN02= @COLUMN02)
	UPDATE F1 SET COLUMN18 = F1.COLUMN18 + S8.COLUMN09 FROM PUTABLE003 P3
	INNER JOIN SATABLE007 S7 ON S7.COLUMN06 = P3.COLUMN06 AND S7.COLUMNA02 = P3.COLUMNA02 AND S7.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S7.COLUMNA13,0)=0
	INNER JOIN SATABLE008 S8 ON S8.COLUMN14 = S7.COLUMN01 AND S8.COLUMNA02 = P3.COLUMNA02 AND S8.COLUMNA03 = P3.COLUMNA03 AND ISNULL(S8.COLUMNA13,0)=0
	INNER JOIN FITABLE010 F1 ON F1.COLUMN03 = S8.COLUMN04 AND F1.COLUMNA02 = P3.COLUMNA02 AND F1.COLUMNA03 = P3.COLUMNA03 AND ISNULL(F1.COLUMNA13,0)=0
	AND F1.COLUMN19 = S8.COLUMN19 AND F1.COLUMN21 = 0 AND F1.COLUMN22 = 0 
	WHERE P3.COLUMN03 = 1286 AND S7.COLUMN03 = 1284 AND ISNULL(P3.COLUMNA13,0)=0 and p3.column02 = @COLUMN02
END

end try
begin catch
--EMPHCS1410 LOGS CREATION BY SRINIVAS
declare @tempSTR nvarchar(max)		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(250))))
    exec [CheckDirectory] @tempSTR,'Exception_JO_PUTABLE003.txt',0
return 0
end catch
end







GO
