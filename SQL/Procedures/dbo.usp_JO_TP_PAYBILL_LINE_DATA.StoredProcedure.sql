USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_PAYBILL_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_PAYBILL_LINE_DATA]
(
	@SalesOrderID int,@Form varchar(50)
)

AS
BEGIN
declare @DueAmnt INT
declare @DueAmnt1 int
declare @PayAmnt int
declare @PayAmnt1 int
declare @IVID int
if(@Form='0')
begin
set @DueAmnt=(select (sum(COLUMN06)+sum(COLUMN09)) from SATABLE012 where isnull(COLUMNA13,0)=0 and COLUMN08 in (select COLUMN01 from SATABLE011 where isnull(COLUMNA13,0)=0 and COLUMN06=cast(@SalesOrderID as nvarchar)))
if(@DueAmnt>0)
begin  
set @DueAmnt=( select isnull(max(COLUMN20),0) from SATABLE009 where isnull(COLUMNA13,0)=0 and COLUMN06= cast(@SalesOrderID as nvarchar))
set @DueAmnt1=@DueAmnt-(select (sum(COLUMN06)+sum(COLUMN09)) from SATABLE012 where isnull(COLUMNA13,0)=0 and COLUMN08 in (select  COLUMN01 from SATABLE011 where isnull(COLUMNA13,0)=0 and COLUMN06=cast(@SalesOrderID as nvarchar)))

 select distinct b.COLUMN02 as COLUMN03,(isnull(max (d.COLUMN04),max(b.COLUMN20))) as COLUMN04,
( max (b.COLUMN20) -((isnull(sum(d.COLUMN06),0))+(isnull(sum(d.COLUMN09),0)))) as COLUMN05,null as COLUMN06
	from  SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 
	 WHERE  b.COLUMN06 =@SalesOrderID and  isnull(b.COLUMNA13,0)=0 and isnull(d.COLUMNA13,0)=0 group by b.COLUMN02
end
else
begin
 select distinct b.COLUMN02 as COLUMN03,max(b.COLUMN20) as COLUMN04,( max (b.COLUMN20)) as COLUMN05,    null as COLUMN06
	from SATABLE009 b 
	 WHERE   b.COLUMN06= cast(@SalesOrderID as nvarchar) and  isnull(b.COLUMNA13,0)=0  group by b.COLUMN02
end
end
else
begin
set @IVID=(select column02 from SATABLE009 where COLUMN04=@Form and  isnull(COLUMNA13,0)=0);
set @DueAmnt=(select isnull(sum(COLUMN06),0)+isnull(sum(COLUMN09),0) from SATABLE012 where COLUMN08 in (select COLUMN01 from SATABLE011 where COLUMN06=cast(@SalesOrderID as nvarchar) and  isnull(COLUMNA13,0)=0)  and COLUMN03=@IVID and  isnull(COLUMNA13,0)=0)
if(@DueAmnt>0)
begin  
set @DueAmnt=(select isnull(max(COLUMN20),0) from SATABLE009 where COLUMN06= cast(@SalesOrderID as nvarchar)  and COLUMN04=@Form and  isnull(COLUMNA13,0)=0)
set @DueAmnt1=@DueAmnt-(select isnull(sum(COLUMN06),0)+isnull(sum(COLUMN09),0) from SATABLE012 where COLUMN08 in (select  COLUMN01 from SATABLE011 where COLUMN06=cast(@SalesOrderID as nvarchar) and  isnull(COLUMNA13,0)=0  ) and COLUMN03=@IVID and  isnull(COLUMNA13,0)=0)

select distinct b.COLUMN02 as COLUMN03,(isnull(max (d.COLUMN04),max(b.COLUMN20))) as COLUMN04,
( max (b.COLUMN20) -((isnull(sum(d.COLUMN06),0))+(isnull(sum(d.COLUMN09),0)))) as COLUMN05,null as COLUMN06
	from  SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 
	 WHERE  b.COLUMN06 =@SalesOrderID and  isnull(b.COLUMNA13,0)=0 and isnull(d.COLUMNA13,0)=0 group by b.COLUMN02
end
else
begin

set @DueAmnt1=(select SUM(COLUMN14) from SATABLE010 WHERE  COLUMN15 = (select COLUMN01 from SATABLE009 where COLUMN06= cast(@SalesOrderID as nvarchar) and COLUMN04=@Form and  isnull(COLUMNA13,0)=0) and  isnull(COLUMNA13,0)=0)
end

 select b.COLUMN02 as COLUMN03, max(b.COLUMN20) as COLUMN04,( max (b.COLUMN20)) as COLUMN05,     null as COLUMN06
	from SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 
	 WHERE   b.COLUMN06= cast(@SalesOrderID as nvarchar)  and  isnull(b.COLUMNA13,0)=0 and isnull(d.COLUMNA13,0)=0 group by b.COLUMN02
end

   
END

--exec [usp_SAL_TP_PAYMENT_LINE_DATA] 0,'IV1024'













GO
