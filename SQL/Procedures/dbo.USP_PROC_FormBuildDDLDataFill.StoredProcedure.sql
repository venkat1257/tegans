USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_FormBuildDDLDataFill]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_FormBuildDDLDataFill](@FormId nvarchar(250)= null,@TableName nvarchar(250)= null,
@OPUnit nvarchar(250)= null,@AcOwner nvarchar(250)= null,@ResultQry nvarchar(max)='',@whereStr nvarchar(max)='',
@whereStrac nvarchar(max)='',@whereStracnull nvarchar(max)='',@ColumnName nvarchar(250)='',@Type nvarchar(250)='',
@itemID nvarchar(max)='',@issueID nvarchar(max)='',@Col2id nvarchar(250)= '')
AS
BEGIN
DECLARE @FRDATE NVARCHAR(25),@TODATE NVARCHAR(25) 
SELECT @FRDATE=ISNULL(MAX(COLUMN04),'1/1/2010'),@TODATE=ISNULL(MAX(COLUMN05),'1/1/2100') FROM FITABLE048 WHERE COLUMNA03 = @AcOwner AND COLUMN09 ='1'

declare @whereStrop nvarchar(max)=''
set @OPUnit=(isnull(@OPUnit,0))
if @Col2id!=''
begin
set @whereStr= ' and COLUMN02='''+@Col2id+''''
end
if @OPUnit!=''
begin
set @whereStr= ' and (COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or COLUMNA02 is null)'
set @whereStrop= ' and (COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s))'
end
--if @Col2id!=''
--begin
--if(@whereStr!='')
--set @whereStr= @whereStr + ' and COLUMN02='''+@Col2id+''''
--else
--set @whereStr= ' and COLUMN02='''+@Col2id+''''
--end
if @AcOwner!=''
begin
set @whereStrac= ' and COLUMNA03='+@AcOwner+''
set @whereStracnull= ' and (COLUMNA03='+@AcOwner+' or COLUMNA03 is null)'
end
if (@FormId = 1275 or @FormId = 1375 or @FormId = 1376 or @FormId = 1276 or @FormId = 1278 or @FormId = 1277 or @FormId = 1265 or @FormId = 1252 or @FormId = 1501 or @FormId =1380 or @FormId =1381)
begin
if (@TableName = 'MATABLE013')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' 	where (COLUMN06 =''true'' or COLUMN06 =''1'') '+@whereStr+' '+@whereStracnull+' and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select ''-''+COLUMN02,''[''+COLUMN04+'']'' from matable014 	where  isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'')
else if (@TableName = 'PRTABLE001')
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + '  where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'SATABLE005' and @FormId = 1277 and @Type='New')
set @ResultQry =('select SATABLE005.COLUMN02,SATABLE005.COLUMN04 from ' + @TableName + '  left outer join SATABLE006  on SATABLE005.COLUMN01=SATABLE006.COLUMN19 where SATABLE005.COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Sales Order'' )  AND  SATABLE006.COLUMN12>0  AND (SATABLE005.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or SATABLE005.COLUMNA02 is null) AND SATABLE005.COLUMNA03=' + @AcOwner + ' group by SATABLE005.COLUMN02,SATABLE005.COLUMN04 order by SATABLE005.Column02 desc')
else if (@TableName = 'SATABLE005')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Sales Order'' )  AND ISNULL(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' order by Column02 desc');
else if (@TableName = 'SATABLE009')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN19 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Sales Order'' )  AND ISNULL(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' AND COLUMN04!=''''   and column08 <= '''+@TODATE+''' AND isnull(COLUMNA13,0)=0 order by Column02 desc');
else if (@TableName = 'SATABLE002')
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + '  where  isnull(COLUMN25,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'MATABLE016')
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + '')
else if (@TableName = 'MATABLE017' and ((@FormId = 1376 and @ColumnName = 'COLUMN37') or (@FormId = 1275 and @ColumnName = 'COLUMN69') or
(@FormId = 1276 and @ColumnName = 'COLUMN27') or (@FormId = 1265 and @ColumnName = 'COLUMN43') or (@FormId = 1252 and @ColumnName = 'COLUMN35') or (@FormId = 1501 and @ColumnName = 'COLUMN39') or @FormId = 1277))
set @ResultQry =('select COLUMN02,concat(COLUMN06,iif(COLUMN06!='''',''-'',''''),COLUMN03) as COLUMN04 from ' + @TableName + ' where COLUMN04=1007');
else if (@TableName = 'MATABLE017')
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + '');
else if (@TableName = 'MATABLE009')
set @ResultQry =('select COLUMN02,COLUMN04 as COLUMN04 from ' + @TableName + ' where isnull(COLUMN11,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'');
else if (@TableName = 'MATABLE022')
set @ResultQry =('select COLUMN02,COLUMN04 as COLUMN04 from ' + @TableName + ' where isnull(COLUMN11,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'');
else if (@TableName = 'FITABLE001' and @FormId = 1278)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where (COLUMN07=22266 OR COLUMN07=22329 OR COLUMN07=22409) '+@whereStr+' '+@whereStracnull+' and isnull(COLUMNA13,0)=0');
else if (@TableName = 'CONTABLE007'  and @FormId = 1501 )
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + ' where COLUMNA03= '+@AcOwner+' and isnull(COLUMNA13,0)=0');
else if (@TableName = 'CONTABLE007')
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + ' where COLUMN03!='''' '+@whereStrop+' '+@whereStrac+' and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0');
else if (@TableName = 'CONTABLE030')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where isnull(COLUMN07,0)=0 '+@whereStr+' '+@whereStrac+' and isnull(COLUMNA13,0)=0');
else if (@TableName = 'MATABLE007' and @FormId= 1277 and @Type='Edit')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where  isnull(COLUMN47,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' and COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@itemID+''') s)');
else if (@TableName = 'MATABLE007')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where  isnull(COLUMN47,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' and COLUMN49=''True''');
else if (@TableName = 'MATABLE010' and @ColumnName = 'COLUMN15' and @FormId = 1375)
set @ResultQry =('select COLUMN02,COLUMN09 COLUMN04 from ' + @TableName + '  Where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' and COLUMN30=''True''');
else if (@TableName = 'MATABLE010' and (@FormId = 1375 or @FormId =1380 or @FormId =1381))								
set @ResultQry =('select COLUMN02,COLUMN09 COLUMN04 from ' + @TableName + '  Where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'');
else if (@TableName = 'MATABLE010')													
set @ResultQry =('select COLUMN02,COLUMN09 COLUMN04 from ' + @TableName + '  Where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' and COLUMN30=''True''');
else if (@TableName = 'MATABLE023' and ((@ColumnName = 'COLUMN55' and @FormId = 1277) or (@ColumnName = 'COLUMN59' and (@FormId = 1275 or @FormId = 1330))))
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where  COLUMN07=''Invoice'' '+@whereStr+' '+@whereStracnull+' and isnull(COLUMNA13,0)=0 order by COLUMN04 ');
else if (@TableName = 'MATABLE023' and ((@ColumnName = 'COLUMN37' and @FormId = 1277) or (@ColumnName = 'COLUMN37' and (@FormId = 1275 or @FormId = 1330))))
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where (COLUMN07=''Item'' or isnull(COLUMN07,'''')='''') '+@whereStr+' '+@whereStracnull+' and isnull(COLUMNA13,0)=0 order by COLUMN04 ');
else if (@TableName = 'MATABLE023' or @TableName = 'MATABLE032')		 
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+' and COLUMN04!='''' order by COLUMN04 ');
else if (@TableName = 'SATABLE007' and @FormId= 1277 and @Type='Edit')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where  isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+' and COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@issueID+''') s) order by COLUMN04');
else																	 
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+' order by COLUMN02 desc');
END
ELSE IF (@FormId = 1251 or @FormId = 1656 or @FormId = 1657  or @FormId = 1261 or @FormId = 1669 or @FormId = 1379 or @FormId = 1380 or @FormId = 1381 or @FormId = 1602 or @FormId = 1374 or @FormId = 1377 or @FormId = 1272 or @FormId = 1273 or @FormId = 1274 or @FormId = 1349 or @FormId = 1350 or @FormId = 1351 or @FormId = 1659)
BEGIN
if (@TableName = 'MATABLE013')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' 	where (COLUMN06 =''true'' or COLUMN06 =''1'') '+@whereStr+' '+@whereStracnull+' AND isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select ''-''+COLUMN02,''[''+COLUMN04+'']'' from matable014 	where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'')
else if (@TableName = 'SATABLE002')
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + '  where  isnull(COLUMN25,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'PRTABLE001')
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + '  where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PRTABLE005')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE001' and @FormId = 1273 and @ColumnName = 'COLUMN25' and @Type='New')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and column04  like ''PQ%'' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE001' and @FormId = 1273 and @Type='New')
set @ResultQry =('select PUTABLE001.COLUMN02,PUTABLE001.COLUMN04 from ' + @TableName + '  inner join PUTABLE002  on PUTABLE001.COLUMN01=PUTABLE002.COLUMN19 where PUTABLE001.COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and PUTABLE001.column04 not like ''PQ%''  and PUTABLE002.COLUMN12>0  AND (PUTABLE001.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or PUTABLE001.COLUMNA02 is null) AND PUTABLE001.COLUMNA03=' + @AcOwner + ' group by PUTABLE001.COLUMN02,PUTABLE001.COLUMN04 order by PUTABLE001.COLUMN02 desc')
else if (@TableName = 'PUTABLE001' and @FormId = 1273)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc');
else if (@TableName = 'PUTABLE001' and @FormId = 1272)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and column04 not like ''PQ%'' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE001' and @FormId = 1274 and @Type='New')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN02 in(select distinct(COLUMN06) from PUTABLE005 where COLUMN13 !=(select COLUMN04 from conTABLE025 where column02=12) '+@whereStr+' '+@whereStrac+') and COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and column04 not like ''PQ%'' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE001' and @FormId = 1274)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc');
else if (@TableName = 'PUTABLE001' and @FormId = 1251)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and column04 like ''PQ%'' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE001' and @FormId = 1377)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and column04 like ''PQ%'' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'FITABLE001' and (@FormId = 1274 or @FormId = 1349 or @FormId = 1350 or @FormId = 1351))
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where (COLUMN07=22266 OR COLUMN07=22329 OR COLUMN07=22409) '+@whereStr+' '+@whereStracnull+' and isnull(COLUMNA13,0)=0')
else if (@TableName = 'PUTABLE005' and @FormId = 1274 and @Type='New')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN13!=(select COLUMN04 from conTABLE025 where column02=12) and COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and COLUMN04!=''''  and column08 <= '''+@TODATE+''' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE005' and @FormId = 1274)
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) and COLUMN04!='''' '+@whereStr+' '+@whereStrac+' order by COLUMN02 desc')
else if (@TableName = 'PUTABLE001')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04=''Purchase Order'' ) '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'SATABLE001' and @FormId = 1669 and @ColumnName = 'COLUMN07')
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + ' where (COLUMN22=22286) '+@whereStr+' '+@whereStrac+' and isnull(COLUMN24,0)=0 and isnull(COLUMNA13,0)=0')
else if (@TableName = 'SATABLE001')
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + ' where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='''') '+@whereStr+' '+@whereStrac+' and isnull(COLUMN24,0)=0 and isnull(COLUMNA13,0)=0')
else if (@TableName = 'MATABLE016')
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + '')
else if (@TableName = 'MATABLE017' and (@FormId = 1273 or (@FormId = 1265 and @ColumnName = 'COLUMN43') or (@FormId = 1252 and @ColumnName = 'COLUMN35') or (@FormId = 1501 and @ColumnName = 'COLUMN39') or (@FormId = 1251 and @ColumnName = 'COLUMN72') or (@FormId = 1272 and @ColumnName = 'COLUMN27')))
set @ResultQry =('select COLUMN02,concat(COLUMN06,iif(COLUMN06!='''',''-'',''''),COLUMN03) as COLUMN04 from ' + @TableName + ' where COLUMN04 =1007')
else if (@TableName = 'MATABLE017')
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + '')
else if (@TableName = 'MATABLE011')
set @ResultQry =('select COLUMN02,COLUMN04 as COLUMN04 from ' + @TableName + ' where isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+' ')
else if (@TableName = 'SATABLE002' and (@FormId = 1380 or @FormId = 1381 or @FormId = 1602))
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + '  where  isnull(COLUMN25,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'SATABLE002' and (@FormId = 1615))
set @ResultQry =('select COLUMN02,COLUMN05 COLUMN04 from ' + @TableName + '  where  isnull(COLUMN25,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'MATABLE009' and (@FormId = 1380 or @FormId = 1381 or @FormId = 1602))
set @ResultQry =('select COLUMN02,COLUMN04 as COLUMN04 from ' + @TableName + ' where isnull(COLUMN11,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'')
else if (@TableName = 'MATABLE009')
set @ResultQry =('select COLUMN02,COLUMN04 as COLUMN04 from ' + @TableName + ' where isnull(COLUMN11,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'')
else if (@TableName = 'MATABLE022')
set @ResultQry =('select COLUMN02,COLUMN04 as COLUMN04 from ' + @TableName + ' where isnull(COLUMN11,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'')
else if (@TableName = 'CONTABLE007')
set @ResultQry =('select COLUMN02,COLUMN03 as COLUMN04 from ' + @TableName + ' where COLUMN03!='''' AND isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStrop+' '+@whereStrac+'')
else if (@TableName = 'CONTABLE030')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'MATABLE007' and @FormId = 1261 and @ColumnName = 'COLUMN80'  )
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where isnull(COLUMN47,0)=0 and isnull(COLUMNA13,0)=0 and isnull(COLUMN78,0)=1 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'SATABLE005' and @FormId = 1261 and @ColumnName = 'COLUMN81' and (@Type='New')  )
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1283 AND COLUMN16! = ''RECEIVED'' and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')
else if (@TableName = 'SATABLE005' and @FormId = 1261 and @ColumnName = 'COLUMN81'  )
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1283 and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')
else if (@TableName = 'SATABLE007' and @FormId = 1261 and @ColumnName = 'COLUMN82' and (@Type='New') )
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1284 AND COLUMN14 != ''Issue(Out)'' and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')
else if (@TableName = 'SATABLE007' and @FormId = 1261 and @ColumnName = 'COLUMN82')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1284 and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')
else if (@TableName = 'SATABLE005' and @FormId = 1669 and @ColumnName = 'COLUMN06' and (@Type='New') )
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1283 AND COLUMN16! = ''RECEIVED'' and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')
else if (@TableName = 'SATABLE005' and @FormId = 1669 and @ColumnName = 'COLUMN06')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1283 and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')
else if (@TableName = 'SATABLE007' and @FormId = 1669 and @ColumnName = 'COLUMN08'  )
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where COLUMN03 = 1284 and isnull(COLUMNA13,0)=0'+@whereStr+' '+@whereStrac+' ORDER BY COLUMN02 DESC')

else if (@TableName = 'MATABLE007')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + '  Where isnull(COLUMN47,0)=0 and isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'')
else if (@TableName = 'SATABLE015')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+' order by column02 desc ')
else if (@TableName = 'MATABLE023' or @TableName = 'MATABLE032')
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+' and COLUMN04!='''' order by COLUMN04 ')
else if (@TableName = 'MATABLE010')													
set @ResultQry =('select COLUMN02,COLUMN09 COLUMN04 from ' + @TableName + '  Where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStrac+'');
else
set @ResultQry =('select COLUMN02,COLUMN04 from ' + @TableName + ' where isnull(COLUMNA13,0)=0 '+@whereStr+' '+@whereStracnull+'')
END                                
exec( @ResultQry)

END


GO
