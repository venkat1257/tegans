USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[getdetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[getdetails]
as
select m7.column04,m7.column57 ,'',m4.column04,m5.column04,m3.column04,0,0,isnull(pt.column04,ptg.column04),isnull(st.column04,ptg.column04),0,m2.column04,hs.column04,'Inventory Item'
from matable007 m7 
left join MATABLE004 m4 on  m4.column02 = m7.column11
left join MATABLE005 m5 on  m5.column02 = m7.column10
left join MATABLE003 m3 on  m3.column02 = m7.column12
left join MATABLE002 m2 on  m2.column02 = m7.column63
left join MATABLE013 pt on  pt.column02 = m7.column61
left join MATABLE013 st on  st.column02 = m7.column54
left join MATABLE014 ptg on  '-'+ptg.column02 = m7.column61
left join MATABLE014 stg on  '-'+stg.column02 = m7.column54
left join MATABLE024 pp on  pp.column07 = m7.column02 and pp.column06 = 'Purchase' and isnull(pp.columna13,0)=0
left join MATABLE024 sp on  sp.column07 = m7.column02 and sp.column06 = 'Sales'    and isnull(sp.columna13,0)=0
left join MATABLE032 hs on  hs.column02 = m7.column75 --and sp.column06 = 'Sales' 
where m7.columna03 = '56672' and m7.columna13 = 0
--column04,column57

select s2.column05,'','',s2.column10,s2.column11,'','',s3.column06,s3.column07,s3.column08,s3.column10,m7.column03,m6.column03,s3.column12,m2.column04,s2.column42,m17.column03
 from satable002 s2 
 inner join satable003 s3 on s3.column02 = s2.column16
 left join matable002 m2 on m2.column02 = s2.column41
 left join matable017 m7 on m7.column02 = s3.column11
 left join matable017 m17 on m17.column02 = s2.column43
 left join matable016 m6 on m6.column02 = s3.column16
where s2.columna03 = 56672 and s2.columna13 =0


select s2.column05,s2.column11,s2.column12,'','',s3.column06,s3.column07,s3.column08,s3.column10,m7.column03,m6.column03,s3.column12,m2.column04,s2.column34,m17.column03,'Vendor'
 from satable001 s2 
 inner join satable003 s3 on s3.column02 = s2.column17
 left join matable002 m2 on m2.column02 = s2.column33
 left join matable017 m7 on m7.column02 = s3.column11
 left join matable017 m17 on m17.column02 = s2.column35
 left join matable016 m6 on m6.column02 = s3.column16
where s2.columna03 = 56672 and s2.columna13 =0
GO
