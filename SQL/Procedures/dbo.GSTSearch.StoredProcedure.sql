USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GSTSearch]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GSTSearch]
(
@Bill  nvarchar(250)= null,
@Vendor  nvarchar(250)= null,
@OperatingUnit  nvarchar(250)= null,
@FromDate  nvarchar(250)= null,
@Date  nvarchar(250)= null,
@TaxType nvarchar(250)= null,
@DateF nvarchar(250)=null,
@AcOwner nvarchar(250)= null
)
as 
begin
declare @whereStr nvarchar(1000)=null
if @Bill!=''

begin
 set @whereStr= 'P5.COLUMN02 = '''+@Bill+''''
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='P5.COLUMN15 = '''+@OperatingUnit+''''
end
else
begin
set @whereStr=@whereStr+' and P5.COLUMN15 = '''+@OperatingUnit+''''
end
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' P5.COLUMN05 = '''+@Vendor+''''
end
else
begin
set @whereStr=@whereStr+' and P5.COLUMN05 = '''+@Vendor+''''
end
end
IF @FromDate!='' and  @Date!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' P5.COLUMN08 between '''+@FromDate+''' AND '''+@Date+''''
end
else
begin
set @whereStr=@whereStr+' and P5.COLUMN08 between '''+@FromDate+''' AND '''+@Date+''''
end
end
IF @TaxType!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' M13.COLUMN16 = '''+@TaxType+''''
end
else
begin
set @whereStr=@whereStr+' and M13.COLUMN16 = '''+@TaxType+''''
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' 1=1'
 end
 set @DateF=(iif(cast(isnull(@DateF,'') as nvarchar(250))='','dd/MM/yyyy',@DateF))
 exec ('
SELECT P5.COLUMN04 AS Bill,S1.COLUMN05 AS Vendor,FORMAT(P5.COLUMN08,'''+@DateF+''') AS [Date],CAST(ISNULL(P6.COLUMN12,0)-ISNULL(dl.COLUMN24,0)AS DECIMAL(18,2)) AS SubTotal,M2.COLUMN04 Groupwise,M13.COLUMN17 Rate,
CAST((M13.COLUMN17*(ISNULL(P6.COLUMN12,0)-ISNULL(dl.COLUMN24,0))/100)AS DECIMAL(18,2)) AS TaxAmount,'''' AS [ItcTaken#],''''AS [ItcRef#],'''' AS Remarks,'''' AS CheckBox,P6.COLUMN02 id,M13.COLUMN02 TAX,P6.COLUMN04 NAME,P5.COLUMN05 VendorID FROM 
PUTABLE005 P5 
inner join PUTABLE006 P6 ON P6.COLUMN13= P5.COLUMN01 and P6.COLUMNA02=P5.COLUMNA02 and P6.COLUMNA03=P5.COLUMNA03 and isnull(P6.COLUMNA13,0)=0
left join PUTABLE001 dh on dh.COLUMN48=P5.COLUMN02 and dh.COLUMNA02=P5.COLUMNA02 and dh.COLUMNA03=P5.COLUMNA03 and isnull(dh.COLUMNA13,0)=0
left join PUTABLE002 dl on dl.COLUMN25=P6.COLUMN18 and dl.COLUMN19=dh.COLUMN01 and dl.COLUMN03=P6.COLUMN04 and dl.COLUMN26=P6.COLUMN19 and iif(cast(dl.COLUMN17 as nvarchar(250))='''',''0'',isnull(dl.COLUMN17,0))=iif(cast(P6.COLUMN27 as nvarchar(250))='''',''0'',isnull(P6.COLUMN27,0)) and dl.COLUMNA02=dh.COLUMNA02 and dl.COLUMNA03=dh.COLUMNA03 and isnull(dl.COLUMNA13,0)=0
LEFT JOIN SATABLE001 S1 ON S1.COLUMN02 = P5.COLUMN05 AND S1.COLUMNA03 = P5.COLUMNA03 AND ISNULL(S1.COLUMNA13,0)=0
LEFT JOIN MATABLE014 M14 ON ''-''+(M14.COLUMN02) = (P6.COLUMN18) AND ISNULL(M14.COLUMNA13,0)=0
LEFT JOIN MATABLE013 M131 ON M131.COLUMN02 IN (SELECT ListValue FROM dbo.FN_ListToTable('','',M14.COLUMN05) s) AND M131.COLUMN16 IN(23585,23586,23584,23583,23582)
INNER JOIN MATABLE013 M13 ON M13.COLUMN02 IN (IIF(P6.COLUMN18<0,
M131.COLUMN02,P6.COLUMN18)) AND M13.COLUMN16 IN(23585,23586,23584,23583,23582)
LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = M13.COLUMN16 AND ISNULL(M2.COLUMNA13,0)=0  
LEFT JOIN MATABLE007 M7 ON M7.COLUMN02=P6.COLUMN04 AND M7.COLUMNA03=P6.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0 
LEFT JOIN PUTABLE024 P24 ON P24.COLUMN03=P5.COLUMN04  and ISNULL(P24.COLUMN15,0)=P6.COLUMN02 AND ISNULL(P24.COLUMN16,0)=M13.COLUMN02 AND P24.COLUMNA03=P6.COLUMNA03 AND ISNULL(p24.COLUMNA13,0)=0
WHERE  P5.COLUMNA03 = '+ @AcOwner +' and isnull (P5.COLUMNA13,0) = 0 AND P5.COLUMN58=0 and P24.COLUMN02 is null and (isnull(P6.COLUMN09,0)-isnull(dl.COLUMN07,0))!=0 and '+@whereStr +' GROUP BY P5.COLUMN04,S1.COLUMN05,P5.COLUMN08,M2.COLUMN04,P6.COLUMN12,P6.COLUMN20,P24.COLUMN05,M13.COLUMN17,P6.COLUMN02,M13.COLUMN02,P6.COLUMN04,P5.COLUMN05,dl.COLUMN24
') 
END





GO
