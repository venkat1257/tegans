USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[BACKUP]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[BACKUP]
(@FileName varchar(500))
AS
BEGIN
--'E:\Accnu_DB_BKP\Accnu_PRD_'
DECLARE @Filepath varchar(500)=  @FileName
SET @FileName=@FileName+'\Accnu_PRD_'+convert(varchar(20),getdate(),112)+'_'+replace(Convert (varchar(8),GetDate(), 108),':','')+'.Bak'
BACKUP DATABASE [MySmartSCM_PRD_SWAMY] 
TO DISK = @FileName
WITH NOFORMAT, NOINIT, 
NAME = N'MySmartSCM_PRD-Full Database Backup', SKIP, NOREWIND, NOUNLOAD, 
STATS = 10

exec  Backupfilepath @Filepath,'insert'

IF(@@ERROR <> 0)
select 'Backup Failed....with Error Code ' +@@ERROR [Result]
else
select 'Backup Success........' [Result]

END
GO
