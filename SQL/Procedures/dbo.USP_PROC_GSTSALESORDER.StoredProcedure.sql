USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTSALESORDER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_GSTSALESORDER](@SONO nvarchar(250)=null,@TYPE nvarchar(250)=null,@frDT nvarchar(250)=null)
AS
BEGIN
if(@TYPE='HEADER')
begin
select S5.COLUMN04,s.COLUMN05 COLUMN05,S5.COLUMN06,S5.COLUMN01,S5.COLUMN02,S5.COLUMN15,S5.COLUMN05 cust,S5.COLUMN24 oper ,
isnull(S5.COLUMN14,0)+isnull(S5.COLUMN60,0) as Discount,S5.COLUMN05 as cId,ht.COLUMN07 as HTamnt,c.COLUMN32 as StateLincense,
c.COLUMN33 as CntralLincese,c.COLUMN03 as company,	 c.COLUMN28 as logoName, S5.COLUMN24 as oppunit, 
S5.COLUMN41 as PackingCharges,S5.COLUMN40 as TrackingNo, S5.COLUMN42 as ShippingCharges, S5.COLUMN11 as ref,
f.COLUMN04 as form, m.COLUMN04 as PaymentTerms,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,
cast(S5.COLUMN08 as date) duedate,c.COLUMN27 vatno,c.COLUMN29 cstno,  c.COLUMN31 panno,S5.COLUMN15 totalamt,
CONVERT(varchar,CAST(S5.COLUMN15 AS money), 1) totalamt1,pm.COLUMN04 Paymentmode  ,s.COLUMN26 cpanno ,s.COLUMN21 custTin,
s.COLUMN27 cvatno,s.COLUMN28 ccstno,opu.COLUMN03 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,
opu.COLUMN33 as OPUPANNO, opu.COLUMN34 as OPUPhNo,c.COLUMN35 as tin, c.COLUMN10 as compA1, c.COLUMN11 as compA2, 
c.COLUMN12 as compCity,c.COLUMN14 as compZip,s.COLUMN11 custPhNo,c.COLUMN34 as footerText,s.COLUMN29 ctaxno,
S5.COLUMN50 Amtin,s.COLUMN10 mailid,s.COLUMN42 CustGSTIN,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,
c.COLUMND02 CGSTIN,MC.COLUMN03 compCtry,MS.COLUMN03 compState,m10.COLUMN33 as [signature],c.COLUMN02 CompanyID  from SATABLE005 S5 
left join matable002 pm on pm.column02=s5.COLUMN30  
left join matable002 m on m.column02=s5.column10 left join matable002 f on f.column02=s5.column37   
left join matable002 mot on mot.column02=s5.column39 left join matable002 ft on ft.column02=s5.column36 
left join CONTABLE007 opu on opu.COLUMN02=S5.COLUMN24  left join SATABLE002 s on s.column02=S5.COLUMN05 and s.COLUMNA03=S5.COLUMNA03 and isnull(s.COLUMNA13,0)=0	 
left join MATABLE013 ht on ht.column02=S5.COLUMN31 and ht.COLUMNA03=S5.COLUMNA03 and isnull(ht.COLUMNA13,0)=0
left join CONTABLE008 c on c.COLUMNA03=s5.COLUMNA03 AND (c.COLUMNA02=s5.COLUMNA02 or c.COLUMNA02 is null) AND ISNULL(c.COLUMNA13,0)=0 AND ISNULL(c.COLUMN05,0)=0 
left outer join MATABLE010 m10 on m10.COLUMN02=S5.COLUMNA08
left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 
left JOIN MATABLE016 MC  ON MC.COLUMN02 = c.COLUMN15 
left JOIN MATABLE017 MS  ON MS.COLUMN02 = c.COLUMN13
where (S5.column04=@SONO OR S5.column02=@SONO) AND  ISNULL(S5.COLUMNA13,0)=0
end
ELSE if(@TYPE='LINE')
begin
select it.column04 item,it.column06 upc,a.COLUMN07 quantity,a.COLUMN16 serial, a.COLUMN05 'desc',
CONVERT(varchar, CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN32,isnull(a.COLUMN09,0)),isnull(a.COLUMN09,0)) AS money), 1) rate,
sum(isnull(t.COLUMN07,0)) val,u.COLUMN04 UOM,(select COLUMN07 from MATABLE013  WHERE COLUMN02= a.COLUMN26 AND COLUMN16=22399) cVAT, 
(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN26 AND COLUMN16=22400) cCST,ht.COLUMN07 as HTamnt,
cast((a.COLUMN25  * (ht.COLUMN07/100)) as decimal(18,2)) as lTamnt ,
sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt,  
(a.COLUMN25+cast((a.COLUMN25 * sum(isnull(t.COLUMN07,0))/100) as decimal(18,2))+cast((a.COLUMN25 * sum(isnull(t.COLUMN07,0))/100) as decimal(18,2) )) as LHtaxAmnt,
a.COLUMN11 amt,CONVERT(varchar, (CAST(iif(pl.COLUMN05=22831,a.COLUMN25,cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0) as decimal(18,2))) AS money)), 1) lineamt,
M32.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN09,0) Price
--, ta.column04 names,ta.column04 name 
from  SATABLE006 a 
inner join SATABLE005 b on b.COLUMN01=a.COLUMN19 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN03 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,'-',''))) s)) or t.COLUMN02=a.COLUMN26) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN31 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0  
left join MATABLE002 u on u.column02=a.COLUMN27 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 ta on ta.COLUMN02=t.COLUMN16 and isnull(pl.COLUMNA13,0)=0
where b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN07,a.COLUMN16,a.COLUMN05,pl.COLUMN05,a.COLUMN32,a.COLUMN09,u.COLUMN04,a.COLUMN26,
a.COLUMN25,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,ht.COLUMN07,a.COLUMN11
END
ELSE if(@TYPE='TOTAL')
begin
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt from(
select isnull(a.COLUMN07,0) qty,cast((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN09 ,0)) as decimal(18,2)) subtotamt,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(isnull(a.COLUMN11,0) as decimal(18,2)) amt
from  SATABLE006 a 
inner join SATABLE005 b on b.column01=a.COLUMN19 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and COLUMN02=replace(a.COLUMN26,'-',''))) s)) or t.COLUMN02=a.COLUMN26) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN11,a.COLUMN02,pl.COLUMN05)f
END
ELSE if(@TYPE='LineTax')
begin
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
sum(cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
lt.COLUMN16 TaxType from   SATABLE006 a  inner join SATABLE005 b on  b.COLUMN01=a.COLUMN19 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,'-',''))) s)) or lt.COLUMN02=a.COLUMN26) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0  
where b.column02=@SONO and isnull(a.columna13,0)=0   group by lt.column04,lt.column07,lt.COLUMN16   
union all      
select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, 	
sum(cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST((isnull(ht.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
ht.COLUMN16 TaxType from   SATABLE006 a  inner join SATABLE005 b on  b.COLUMN01=a.COLUMN19 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0  
inner join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN31,'-',''))) s)) or ht.COLUMN02=b.COLUMN31) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0  
where b.column02=@SONO and cast(ht.column07 as decimal(18,2))>0	
group by ht.column04,ht.column07,ht.COLUMN16
END
END



GO
