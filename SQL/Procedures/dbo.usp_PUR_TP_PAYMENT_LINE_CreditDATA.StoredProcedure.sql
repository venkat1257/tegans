USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PAYMENT_LINE_CreditDATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PAYMENT_LINE_CreditDATA]
(
   @SalesOrderID nvarchar(250)=null,
	@AcOwner int=null,
	@OPUNIT int=null,
	@FRDATE date = '1/1/2012',
	@TODATE date = '1/1/2200'
)

AS
BEGIN
select top 1 @FRDATE=column04,@TODATE = COLUMN05 from FITABLE048 where COLUMNA03=@AcOwner AND COLUMN09=1 order by COLUMN01 desc

select cast(h.COLUMN02 as nvarchar) COLUMN03,sum(isnull(l.COLUMN05,0)) COLUMN04,
sum(isnull(l.COLUMN05,0)-isnull(l.COLUMND05,0))COLUMN05,0.00 COLUMN16,0.00 directReturn,
cast(sum(isnull(l.COLUMN05,0)-isnull(l.COLUMND05,0)) as nvarchar) COLUMN06,'' COLUMN18,
'' directReturnCol,'Journal' COLUMN19,l.COLUMN02 COLUMN20
 from FITABLE032 l inner join FITABLE031 h on h.COLUMN01=l.COLUMN12 and 
h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02 and (h.COLUMN07=22305  or l.COLUMN14=22305 ) and
 (isnull(l.COLUMN05,0)-isnull(l.COLUMND05,0))!=0 and  (isnull(l.COLUMN05,0))!=0  and isnull(h.COLUMNA13,0)=0  
  inner JOIN FITABLE001 Acc ON Acc.COLUMN02=l.COLUMN03 and Acc.COLUMNA03=l.COLUMNA03 and isnull(Acc.COLUMNA13,0)=0  AND ACC.COLUMN07=22263 
 where l.COLUMN07=@SalesOrderID and l.COLUMNA03=@AcOwner AND l.COLUMNA02 in(@OPUNIT) and isnull(l.COLUMNA13,0)=0 AND H.COLUMN09 <= @TODATE group by h.COLUMN02,l.COLUMN02
union all
select  cast(h.COLUMN02 as nvarchar) COLUMN03,h.COLUMN15 COLUMN04,(isnull(h.COLUMN15,0)-isnull(h.COLUMND05,0)) COLUMN05,
0.00 COLUMN16,0.00 directReturn,cast((isnull(h.COLUMN15,0)-isnull(l.COLUMND05,0)) as nvarchar) COLUMN06,
'' COLUMN18,'' directReturnCol,'CreditMemo' COLUMN19,h.COLUMN02 COLUMN20
 from SATABLE006 l INNER JOIN 
SATABLE005 h ON h.COLUMN01=l.COLUMN19  and h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02  and isnull(l.columna13,0)=0 
 WHERE   h.COLUMNA03=@AcOwner AND h.COLUMN24 in(@OPUNIT)
 AND h.COLUMN03='1330' AND h.COLUMN51=22305  AND h.COLUMN05=@SalesOrderID AND (isnull(h.COLUMN16,'OPEN')!='CLOSE' and 
 cast(isnull(h.COLUMN15,0)-isnull(h.COLUMND05,0) as decimal(18,2))!=0) and isnull(h.columna13,0)=0  AND H.COLUMN06 <= @TODATE
union all
select  cast(h.COLUMN02 as nvarchar) COLUMN03,cast(isnull(h.COLUMN10,0) as decimal(18,2)) COLUMN04,
cast(isnull(h.COLUMN19,0) as decimal(18,2)) COLUMN05,
0.00 COLUMN16,0.00 directReturn, cast(h.COLUMN19 as nvarchar) COLUMN06,'' COLUMN18,'' directReturnCol,
'AdvanceReceipt' COLUMN19,h.COLUMN02 COLUMN20
 from FITABLE024 l INNER JOIN 
FITABLE023 h ON h.COLUMN01=l.COLUMN09  and h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02  and isnull(l.columna13,0)=0 
 WHERE   h.COLUMNA03=@AcOwner AND h.COLUMN16 in(@OPUNIT)
 AND h.COLUMN03='1386' AND h.COLUMN20=22305  AND h.COLUMN08=@SalesOrderID AND (isnull(h.COLUMN13,'OPEN')!='CLOSE' and 
 cast(isnull(h.COLUMN19,0) as decimal(18,2))!=0) and isnull(h.columna13,0)=0  AND H.COLUMN05 <= @TODATE
union all
select  cast(h.COLUMN02 as nvarchar) COLUMN03,sum(isnull(l.COLUMN16,0)) COLUMN04,sum(isnull(l.COLUMN16,0)-isnull(l.COLUMND05,0)) COLUMN05,
0.00 COLUMN16,0.00  directReturn,cast(sum(isnull(l.COLUMN16,0)-isnull(l.COLUMND05,0)) as nvarchar) COLUMN06,
'' COLUMN18,'' directReturnCol,'ReceiptVoucher' COLUMN19,l.COLUMN02 COLUMN20
 from FITABLE024 l INNER JOIN 
FITABLE023 h ON h.COLUMN01=l.COLUMN09  and h.COLUMNA03=l.COLUMNA03 and h.COLUMNA02=l.COLUMNA02  and isnull(l.columna13,0)=0 
 inner JOIN FITABLE001 Acc ON Acc.COLUMN02=l.COLUMN03 and Acc.COLUMNA03=l.COLUMNA03 and isnull(Acc.COLUMNA13,0)=0  AND ACC.COLUMN07=22263 
 WHERE   h.COLUMNA03=@AcOwner AND h.COLUMN16 in(@OPUNIT)
 AND h.COLUMN03='1526' AND h.COLUMN20=22305  AND h.COLUMN08=@SalesOrderID AND (isnull(h.COLUMN13,'OPEN')!='CLOSE' and 
 cast(isnull(l.COLUMN16,0) as decimal(18,2))!=0) and isnull(h.columna13,0)=0 AND H.COLUMN05 <= @TODATE group by h.COLUMN02,l.COLUMN02
END














GO
