USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CustomerSearch]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[CustomerSearch]
(
@Customer  nvarchar(250)= null,
@Phoneno nvarchar(250)= null,
@Address nvarchar(250)= null,
@AcOwner nvarchar(250)= null
)
as 
begin
if(@Customer!='')
begin
;with MyTable as (
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  0 [Advance],
  0 as [Credit],
 0 [DueAmount],
   MAX(S.COLUMN35) [OverDueDays],max(format(ss.COLUMN08,'dd/MM/yyyy')) [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
--left join FITABLE023 f on f.COLUMN08 = s.column02 and f.column03 = 1386 and f.COLUMNA03=s.COLUMNA03   
--and isnull (f.COLUMNA13,0) = 0
left join MATABLE022 m on m.COLUMN02 = s.COLUMN30 and m.COLUMNA03=s.COLUMNA03   
and isnull (m.COLUMNA13,0) = 0
left join SATABLE009 ss on ss.COLUMN05 = s.column02 and ss.COLUMNA03=s.COLUMNA03   
and isnull (ss.COLUMNA13,0) = 0
where s.column02 =@Customer and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  sum(cast(isnull(f.COLUMN19,0) as decimal(18,2))) [Advance],  
  0 as [Credit],
 0 [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
left join FITABLE023 f on f.COLUMN08 = s.column02 and f.column03 = 1386 and f.COLUMNA03=s.COLUMNA03   
and isnull (f.COLUMNA13,0) = 0
  where s.column02 =@Customer and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
 0 [Advance],  
  0 as [Credit],
 sum(cast(isnull(p.COLUMN09,0) as decimal(18,2))) - sum(cast(isnull(p.COLUMN11,0) as decimal(18,2))) [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
 left join putable018 p on  p.column07 = s.column02 and p.COLUMNA03=s.COLUMNA03   
and isnull (p.COLUMNA13,0) = 0 
  where p.column06!='Credit Memo' and s.column02 =@Customer and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
 0 [Advance],  
  0 as [Credit],
 (- sum(cast(isnull(ff.COLUMN07,0) as decimal(18,2)))) [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
left join FITABLE036 ff on  ff.column06 = s.column02 and ff.COLUMNA03=s.COLUMNA03   and ff.COLUMN03='PAYMENT' and isnull(ff.COLUMN10,0)=1049 and isnull (ff.COLUMNA13,0) = 0 
  where  s.column02 =@Customer and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  0 [Advance],  
  sum(cast(isnull(p.COLUMN14,0) as decimal(18,2))) [Credit],
 0 [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
 left join putable018 p on  p.column07 = s.column02 and p.COLUMNA03=s.COLUMNA03   
and isnull (p.COLUMNA13,0) = 0 
  where p.column06='Credit Memo' and s.column02 =@Customer and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  )
  select [Customer Name],[Contact],[Phone#],[City],sum(Advance) Advance,sum(Credit) Credit,sum([DueAmount]) [DueAmount],sum([OverDueDays]) [OverDueDays],max([Date]) from MyTable
  group by [Customer Name],[Contact],[Phone#],[City]--,[Date]
  end
else if(@Phoneno!='')
begin
;with MyTable as (
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  0 [Advance],
  0 as [Credit],
 0 [DueAmount],
   MAX(S.COLUMN35) [OverDueDays],max(ss.COLUMN08) [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
--left join FITABLE023 f on f.COLUMN08 = s.column02 and f.column03 = 1386 and f.COLUMNA03=s.COLUMNA03   
--and isnull (f.COLUMNA13,0) = 0
left join MATABLE022 m on m.COLUMN02 = s.COLUMN30 and m.COLUMNA03=s.COLUMNA03   
and isnull (m.COLUMNA13,0) = 0
left join SATABLE009 ss on ss.COLUMN05 = s.column02 and ss.COLUMNA03=s.COLUMNA03   
and isnull (ss.COLUMNA13,0) = 0
where s.column11 =@Phoneno and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  sum(cast(isnull(f.COLUMN19,0) as decimal(18,2))) [Advance],  
  0 as [Credit],
 0 [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
left join FITABLE023 f on f.COLUMN08 = s.column02 and f.column03 = 1386 and f.COLUMNA03=s.COLUMNA03   
and isnull (f.COLUMNA13,0) = 0
  where s.column11 =@Phoneno and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
 0 [Advance],  
  0 as [Credit],
 sum(cast(isnull(p.COLUMN09,0) as decimal(18,2))) - sum(cast(isnull(p.COLUMN11,0) as decimal(18,2))) [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
 left join putable018 p on  p.column07 = s.column02 and p.COLUMNA03=s.COLUMNA03   
and isnull (p.COLUMNA13,0) = 0 
  where p.column06!='Credit Memo' and s.column11 =@Phoneno and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
 0 [Advance],  
  0 as [Credit],
 (- sum(cast(isnull(ff.COLUMN07,0) as decimal(18,2)))) [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
left join FITABLE036 ff on  ff.column06 = s.column02 and ff.COLUMNA03=s.COLUMNA03   and ff.COLUMN03='PAYMENT' and isnull(ff.COLUMN10,0)=1049 and isnull (ff.COLUMNA13,0) = 0 
  where  s.column11 =@Phoneno and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  0 [Advance],  
  sum(cast(isnull(p.COLUMN14,0) as decimal(18,2))) [Credit],
 0 [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
 left join putable018 p on  p.column07 = s.column02 and p.COLUMNA03=s.COLUMNA03   
and isnull (p.COLUMNA13,0) = 0 
  where p.column06='Credit Memo' and s.column11 =@Phoneno and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  )
  select [Customer Name],[Contact],[Phone#],[City],sum(Advance) Advance,sum(Credit) Credit,sum([DueAmount]) [DueAmount],sum([OverDueDays]) [OverDueDays],max([Date]) from MyTable
  group by [Customer Name],[Contact],[Phone#],[City]--,[Date]
  end
  else if(@Address!='')
begin
;with MyTable as (
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  0 [Advance],
  0 as [Credit],
 0 [DueAmount],
   MAX(S.COLUMN35) [OverDueDays],max(ss.COLUMN08) [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0

left join MATABLE022 m on m.COLUMN02 = s.COLUMN30 and m.COLUMNA03=s.COLUMNA03   
and isnull (m.COLUMNA13,0) = 0
left join SATABLE009 ss on ss.COLUMN05 = s.column02 and ss.COLUMNA03=s.COLUMNA03   
and isnull (ss.COLUMNA13,0) = 0
where s1.column06 =@Address and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  sum(cast(isnull(f.COLUMN19,0) as decimal(18,2))) [Advance],  
  0 as [Credit],
 0 [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
left join FITABLE023 f on f.COLUMN08 = s.column02 and f.column03 = 1386 and f.COLUMNA03=s.COLUMNA03   
and isnull (f.COLUMNA13,0) = 0
  where s1.column06 =@Address and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
 0 [Advance],  
  0 as [Credit],
 sum(cast(isnull(p.COLUMN09,0) as decimal(18,2))) - sum(cast(isnull(p.COLUMN11,0) as decimal(18,2))) [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
 left join putable018 p on  p.column07 = s.column02 and p.COLUMNA03=s.COLUMNA03   
and isnull (p.COLUMNA13,0) = 0 
  where p.column06!='Credit Memo' and s1.column06 =@Address and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
 0 [Advance],  
  0 as [Credit],
 (- sum(cast(isnull(ff.COLUMN07,0) as decimal(18,2)))) [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
left join FITABLE036 ff on  ff.column06 = s.column02 and ff.COLUMNA03=s.COLUMNA03   and ff.COLUMN03='PAYMENT' and isnull(ff.COLUMN10,0)=1049 and isnull (ff.COLUMNA13,0) = 0 
  where s1.column06 =@Address  and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  union all
  select s.column05 [Customer Name],s1.column06 [Contact],s.column11 [Phone#],s1.column10 [City],
  0 [Advance],  
  sum(cast(isnull(p.COLUMN14,0) as decimal(18,2))) [Credit],
 0 [DueAmount],
  0 [OverDueDays],NULL [Date]
from SATABLE002 s
left join SATABLE003 s1 on s1.column20 = s.column01 and s1.column19 = 'Customer' and s1.COLUMNA03=s.COLUMNA03   
and isnull (s1.COLUMNA13,0) = 0
 left join putable018 p on  p.column07 = s.column02 and p.COLUMNA03=s.COLUMNA03   
and isnull (p.COLUMNA13,0) = 0 
  where p.column06='Credit Memo' and s1.column06 =@Address and s.COLUMNA03=@AcOwner
  Group by s.column05,s1.column06,s.column11,s1.column10
  )
  select [Customer Name],[Contact],[Phone#],[City],sum(Advance) Advance,sum(Credit) Credit,sum([DueAmount]) [DueAmount],sum([OverDueDays]) [OverDueDays],max([Date]) from MyTable
  group by [Customer Name],[Contact],[Phone#],[City]--,[Date]
  end
 end
 

GO
