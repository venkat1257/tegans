USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PUTABLE015]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PUTABLE015]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null, 
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(50)=null,   @COLUMN16   nvarchar(50)=null, 
	 ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)
	@COLUMN17   nvarchar(50)=null, 
	--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
	--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
	@COLUMN18   nvarchar(50)=null,   @COLUMN19   nvarchar(50)=null,   @COLUMN20   nvarchar(50)=null,   
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@ReturnValue nvarchar(250)=null out,@Totamt nvarchar(250)=null,   @dueamt nvarchar(250)=null,  
	@paidamt nvarchar(250)=null,     @acID nvarchar(250)=null
)

AS
BEGIN
begin try
DECLARE @PID INT
DECLARE @PBNO nvarchar(250)
DECLARE @BNO nvarchar(250)
DECLARE @Memo nvarchar(250)
DECLARE @AMT decimal(18,2)
DECLARE @PAMT decimal(18,2)
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
DECLARE @ACC INT
DECLARE @PDUEAMT decimal(18,2)
DECLARE @PCRAMT  decimal(18,2)
DECLARE @PADVAMT decimal(18,2)
declare @date date
DECLARE @Projectactualcost decimal(18,2)
DECLARE @Project  nvarchar(250)
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
DECLARE @COLUMN23  nvarchar(250)
declare @deposit decimal(18,2)
DECLARE @Billdate date
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
--set @COLUMN08 =(select MAX(COLUMN01) from  PUTABLE014);
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE015_SequenceNo
set @PBNO =(select COLUMN04 from  PUTABLE014 where COLUMN01=@COLUMN08);
set @COLUMN20=(iif(isnull(@COLUMN20,0)='',0,isnull(@COLUMN20,0)))
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
set @COLUMN23 =(select COLUMN23 from  PUTABLE014 where COLUMN01=@COLUMN08);
set @BNO =(select COLUMN04 from  PUTABLE005 where COLUMN02=@COLUMN03);
SET @PID=(SELECT COLUMN06 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
SET @date=(SELECT COLUMN18 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
SET @AMT=(SELECT COLUMN15 FROM PUTABLE001 WHERE COLUMN02=@PID)
SET @Memo=(SELECT COLUMN10 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
set @COLUMN09 =(select COLUMN13 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN10 =(select COLUMN14 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN11 =(select COLUMN15 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN12 =(select COLUMN16 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @Billdate =(select COLUMN08 from  PUTABLE005 where COLUMN02=@COLUMN03);
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
if(@COLUMN05!= '' and @COLUMN05 is not null)
begin
set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN16,0 )as DECIMAL(18,2))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))));
end
set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null and @COLUMN08!= '0' ) THEN (select COLUMN13 from PUTABLE014 
   where COLUMN01=@COLUMN08) else @COLUMNA02  END )
   --EMPHCS1410	Logs Creation by srinivas
  declare @tempSTR nvarchar(max)
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Vendor Paytment Line Values are Intiated for PUTABLE015 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMNA01,'')+','+isnull(@COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 											  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+'')
   )

insert into PUTABLE015 
(
  --EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
    ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)  
   COLUMN13,  COLUMN14,  column15,  column16,  COLUMN17, 
   --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMN18,  COLUMN19,  COLUMN20,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  isnull(@COLUMN05,0),  @COLUMN06,	 @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  
    ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,@COLUMN17,
   --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)

   --EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Vendor Paytment Line Values are Created in PUTABLE015 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
    )
	declare @internalid1 nvarchar(250)=null
set @internalid1 = (select column01 from PUTABLE015 where column02=@COLUMN02)
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
if(@COLUMN05= '' or @COLUMN05 is null)
begin
declare @cdueamt decimal(18,2)
set @cdueamt=(select min(isnull(COLUMN05,0)) from  PUTABLE015 Where COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0)
set @COLUMN05=((SELECT CASE WHEN isnull(@COLUMN05,0)=0 THEN ( cast(@COLUMN04 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN16,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2)))
 ELSE (cast(@cdueamt as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN16,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))) END));
update PUTABLE015 set COLUMN05=@COLUMN05 Where COLUMN02=@COLUMN02
end
				if exists(select column01 from putable015 where column08=@column08 and @COLUMN20=0)
					begin
set @Project=(select column23 from  PUTABLE014 Where COLUMN01=@COLUMN08)
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
--update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN06 as decimal(18,2))) where column02=@Project
set @deposit=  (SELECT COLUMN08 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
declare @Tamt decimal(18,2)
declare @PIQty decimal(18,2)
declare @PBQty decimal(18,2)
declare @APStatus nvarchar(250)
set @APStatus=('PARTIALLY PAID')
if(cast(@COLUMN05 as decimal(18,2))=0)
begin 
set @APStatus=('PAID')
end
SET @Tamt=(select COLUMN15 from PUTABLE001 where COLUMN02=@PID)
SET @PAMT=(SELECT SUM(COLUMN06) FROM PUTABLE015 WHERE COLUMN08 in(select COLUMN01 from PUTABLE014 where COLUMN06=@PID))
if(@PID='')
begin
SET @Tamt=(SELECT max(COLUMN04) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03)
SET @PAMT=(SELECT SUM(COLUMN06) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03)
end

----if(@Tamt=@PAMT)
----begin
----update PUTABLE001 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=20) where COLUMN02=@PID
--update  PUTABLE016 set COLUMN13='Paid' where COLUMN05=@PBNO
--update  PUTABLE016 set COLUMN13='Paid' where COLUMN05 in 
--( select a.COLUMN04 from  PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01  where b.COLUMN03=@COLUMN03)
----end
----else
----begin
----update PUTABLE001 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=17) where COLUMN02=@PID
--update  PUTABLE016 set COLUMN13='PARTIALLY PAID' where COLUMN05=@PBNO
----end
SET @PIQty=isnull((SELECT max(COLUMN04) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
SET @PBQty=isnull((SELECT @PIQty-(sum(isnull(COLUMN06,0))+sum(isnull(COLUMN14,0))+sum(isnull(COLUMN16,0))) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
IF(@PBQty>0)
begin
----update PUTABLE014 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=18)  
----where COLUMN01 in (select COLUMN08 from PUTABLE015 where COLUMN03=@COLUMN03)
UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=11) where COLUMN02=@COLUMN03
end
else IF(@PBQty=0.00)
begin
----update PUTABLE014 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=17) 
----where COLUMN01 in (select COLUMN08 from PUTABLE015 where COLUMN03=@COLUMN03)
UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=12) where COLUMN02=@COLUMN03
end
Declare @venID int
set @venID = (select cast(COLUMN05 as int)  from PUTABLE014 where COLUMN01=cast(@COLUMN08 as int))
declare @paid decimal(18,2)
set @paid=CAST(@COLUMN04 as decimal(18,2))
set @COLUMN04=( SELECT TOP 1 COLUMN06 FROM PUTABLE014 ORDER BY COLUMN01 DESC);
--set @COLUMN08=( SELECT COLUMN01 FROM PUTABLE001 where COLUMN02= @COLUMN04);
--update  PUTABLE013 set COLUMN11=@COLUMN14  WHERE COLUMN02 = @COLUMN08
declare @iID int,@newID int,@BAL decimal(18,2),@HAmount decimal(18,2)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @COLUMN08 =(select MAX(COLUMN01) from  PUTABLE014 where COLUMN01= @COLUMN08);
if(@COLUMN03='0' or @COLUMN03='' or @COLUMN03=null)
BEGIN
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE027) as int)+1
set @acID = (select COLUMN08  from PUTABLE014 where COLUMN01=@COLUMN08)
declare @cID int
declare @sID int
declare @comn decimal(18,2)
set @sID = (select COLUMN05  from PUTABLE014 where COLUMN01=@COLUMN08)
--set @cID = (select COLUMN02  from SATABLE002 where COLUMN22=@sID)
--set @comn = (select COLUMN23  from SATABLE002 where COLUMN22=@sID)
----EMPHCS606		Reports - Commission Report is not working as expected values srinivas 12/8/2015
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
declare @SalesRep nvarchar(250)
set @SalesRep=(select COLUMN20  from PUTABLE014 where COLUMN01=@COLUMN08)
if(@SalesRep=22306)
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @COLUMN08,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN06,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN14,COLUMN16,COLUMN18, COLUMNA02, COLUMNA03)
values(@newID,@date,'COMMISSION PAYMENT',@COLUMN08,@venID,@Memo,@acID,@PBNO,@paid,@COLUMN06,@sID,@APStatus,@COLUMN14, @COLUMNA02, @COLUMNA03)
end
		--update PUTABLE016 set column10=@APStatus  where COLUMN09=@BNO and COLUMNA03=@COLUMNA03		
set @iID=(select COLUMN01  from FITABLE027 where COLUMN02=@newID)
SET @BAL=(cast((select column10 from FITABLE001 where column02= @deposit) as decimal(18,2)) - cast(@COLUMN06 as decimal(18,2)))
set @HAmount = (select COLUMN25  from PUTABLE014 where COLUMN01=@COLUMN08)
--EMPHCS1410	Logs Creation by srinivas
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(@HAmount>0)
begin
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
declare @AccountType nvarchar(250),@PayeeType nvarchar(250)
set @PayeeType = (select COLUMN20  from PUTABLE014 where COLUMN01=@COLUMN08 and isnull(COLUMNA13,0)=0)
SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@acID and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
if(@AccountType=22409)
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Intiated for FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  cast(isnull(@acID,'')as nvarchar(10))+','+  cast(isnull(@date,'')as nvarchar(10))+','+ cast(isnull(@PBNO,'')as nvarchar(10))+',COMMISSION PAYMENT'+  isnull(@COLUMN08,'')+','+  isnull(cast(@venID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+ cast( isnull(@Project,'')as nvarchar(20))+'') 			
   )
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE052) as int)+1
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @COLUMN08,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @AccountType, @COLUMN11 = @acID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN06,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08) 
values(@newID,@acID,@date,@PBNO,'COMMISSION PAYMENT',@PayeeType,@venID,@COLUMN08,@Memo,@COLUMN06,@BAL, @COLUMNA02, @COLUMNA03,@Project, @COLUMNA08)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Created in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  cast(isnull(@acID,'')as nvarchar(10))+','+  cast(isnull(@date,'')as nvarchar(10))+','+ cast(isnull(@PBNO,'')as nvarchar(10))+',COMMISSION PAYMENT'+  isnull(@COLUMN08,'')+','+  isnull(cast(@venID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+ cast( isnull(@Project,'')as nvarchar(20))+'') 			
   )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @COLUMN08,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @AccountType, @COLUMN11 = @acID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN06,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE019) as int)+1
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20) 
values(@newID,@acID,@date,@PBNO,'COMMISSION PAYMENT',@venID,@COLUMN08,@Memo,@COLUMN06,@BAL, @COLUMNA02, @COLUMNA03,@Project)
--EMPHCS1410	Logs Creation by srinivas		
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@deposit and COLUMNA03=@COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Decreased By '+cast(@BAL as nvarchar(15))+' in FITABLE001 Table For '+cast(@COLUMN08 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
set @ReturnValue = 1
end
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
declare @cretAmnt decimal(18,2),@retAmnt decimal(18,2),@VTransno nvarchar(250),@TransAmt decimal(18,2),@delimiterstr nvarchar(250),
@Vocherid nvarchar(250),@Vochertype nvarchar(250),@MainString nvarchar(250),@VochrAmt decimal(18,2),@PrevAmt decimal(18,2),
@Ivoiceid nvarchar(250),@Ivoiceno nvarchar(250),@id1 nvarchar(250),@id nvarchar(250)
		if(@COLUMN16!='0' and @COLUMN16!='0.00' and @COLUMN16!='' )
	    begin
		set @VochrAmt=(@COLUMN16)
		set @MainString=(@COLUMN18)
		DECLARE @cnt INT = 0,@vochercnt INT = 1;
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='D')
		begin
		set @VTransno=(select column04 from PUTABLE001 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN15,0)-isnull(COLUMND05,0))COLUMN15 from PUTABLE001 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from PUTABLE001 where column02=@Vocherid)
		update PUTABLE001 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from PUTABLE001 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update PUTABLE001 set COLUMND05=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update PUTABLE001 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN15,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN15 from PUTABLE001 where column02=@Vocherid)=0)
		begin
		update PUTABLE001 set COLUMN16='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		end
		else if(@Vochertype='P')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN15,0)-isnull(COLUMND05,0))COLUMN16 from FITABLE022 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE022 where column02=@Vocherid)
		update FITABLE020 set COLUMN16='OPEN' where column01 in(select COLUMN09 from FITABLE022 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE022 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE022 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN05,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN16 from FITABLE022 where column02=@Vocherid)=0)
		begin
		update FITABLE020 set COLUMN16='CLOSE' where column01 in(select COLUMN09 from FITABLE022 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE020 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMN18,0) from FITABLE020 where column02=@Vocherid)
		update FITABLE020 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE020 set COLUMN18=(@PrevAmt-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		exec usp_PUR_TP_VendorAdvanceCalculations  @PBNO,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@VochrAmt
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE020 set COLUMN18=0 where column02=@Vocherid and columna03=@columna03
		exec usp_PUR_TP_VendorAdvanceCalculations  @PBNO,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@PrevAmt
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		if((select isnull(COLUMN18,0) from FITABLE020 where column02=@Vocherid)=0)
		begin
		update FITABLE020 set COLUMN16='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN04,0)-isnull(COLUMND05,0))COLUMN05 from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END
		
		if(@COLUMN16='' or @COLUMN16 is null) set @COLUMN16=0;
		set @COLUMN06=(select COLUMN06 from PUTABLE015 where column02=@COLUMN02)
		set @COLUMN19=(select COLUMN19 from PUTABLE015 where column02=@COLUMN02)
		set @COLUMN20=(select COLUMN20 from PUTABLE015 where column02=@COLUMN02)
			--if(@COLUMN19='Journal' or @COLUMN19='CreditMemo' or @COLUMN19='ReceiptVoucher' or @COLUMN19='AdvanceReceipt')
			--	begin
			--	if exists(select column02 from putable016 where COLUMN07=@venID and COLUMN09=@PBNO  and COLUMN06='BILL PAYMENT'
			--		 and COLUMN03=2000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable016 set COLUMN13=(COLUMN13-@COLUMN06) where COLUMN07=@venID and COLUMN09=@PBNO  and COLUMN06='BILL PAYMENT'
			--		 and COLUMN03=2000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	if exists(select column02 from putable019 where COLUMN07=@venID and COLUMN05=@PBNO and COLUMN08=@COLUMN08  
			--	and COLUMN06='BILL PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable019 set COLUMN10=(COLUMN10-@COLUMN06) where COLUMN07=@venID and COLUMN05=@PBNO and COLUMN08=@COLUMN08  
			--	and COLUMN06='BILL PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	end
				if(@COLUMN19='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='ReceiptVoucher')
				begin
				update FITABLE024 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='AdvanceReceipt')
				begin
				update FITABLE023 set COLUMN19=(cast(isnull(COLUMN19,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='CreditMemo')
				begin
				update SATABLE005 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
declare @BILLID nvarchar(250)=null ,@ltaxamt decimal(18,2),@CharAcc nvarchar(250),@CharAcc1 nvarchar(250),@PY nvarchar(250),@chk bit,@PItem nvarchar(250),@SubTotal decimal(18,2),@TaxAmnt decimal(18,2),@TaxAvg  decimal(18,2)=0,@internalid nvarchar(250)=null, @ReverseCharged bit,@ReverseItemid int,@BID int,@BID1 int,@BUnits nvarchar(250),
@BQty decimal(18,2),@BTaxType nvarchar(250),@BPrice decimal(18,2),@TTAX decimal(18,2),@BILLLINEID nvarchar(250)=null,@BILLItem nvarchar(250)=null,@TaxID nvarchar(250)=null,@Vid nvarchar(250)=null,
@TaxAG nvarchar(250)=null,@TAXRATE nvarchar(250)=null,@TaxNo nvarchar(250)=null,@TTYPE nvarchar(250)=null,@Billno nvarchar(250)=null
set @BILLID = (select COLUMN03 from PUTABLE015 where COLUMN02=@COLUMN02)
set @Billno = (select column04 from PUTABLE005 where column02=@BILLID)
set @ReverseCharged = (select isnull(COLUMN58,0) from PUTABLE005 where column02=@BILLID)
set @internalid= (select max(column01) from PUTABLE014 where COLUMN01=@COLUMN08)
set @VENID=(select COLUMN05 from PUTABLE014 where COLUMN01=@COLUMN08)
set @memo=(select COLUMN10 from PUTABLE014 where COLUMN01=@COLUMN08)

set @cnt = 1;
select @TTAX=(cast(sum(isnull(M13.COLUMN17,0))AS decimal(18,2))) from PUTABLE006 P INNER join PUTABLE005 P5 on P5.COLUMN01= P.COLUMN13 AND P5.COLUMNA03= P.COLUMNA03 AND P5.COLUMNA02= P.COLUMNA02 and isnull(P.COLUMNA13,0)=0 INNER JOIN MATABLE007 M07 ON M07.COLUMN05 ='ITTY009' AND M07.COLUMN02 = P.COLUMN04 AND M07.COLUMNA03 = P.COLUMNA03 and isnull(M07.COLUMNA13,0)=0
INNER JOIN MATABLE013 M13 ON (M13.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(P.COLUMN18,'-',''))) s)) or M13.COLUMN02=P.COLUMN18)  AND M13.COLUMN16 in(23582,23584,23583,23585,23586) AND isnull(M13.COLUMNA13,0)=0 WHERE P5.COLUMN02 = @BILLID and P5.COLUMNA03=@COLUMNA03 and isnull(P5.COLUMNA13,0)=0 and (cast(@Billdate as date)>'06/30/2017' or cast(@Billdate as date)>'06/30/17')
select @vochercnt=ROW_NUMBER() OVER (ORDER BY P.COLUMN02) from PUTABLE006 P INNER join PUTABLE005 P5 on P5.COLUMN01= P.COLUMN13 AND P5.COLUMNA03= P.COLUMNA03 AND P5.COLUMNA02= P.COLUMNA02 and isnull(P.COLUMNA13,0)=0 INNER JOIN MATABLE007 M07 ON M07.COLUMN05 ='ITTY009' AND M07.COLUMN02 = P.COLUMN04 AND M07.COLUMNA03 = P.COLUMNA03 and isnull(M07.COLUMNA13,0)=0
INNER JOIN MATABLE013 M13 ON (M13.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(P.COLUMN18,'-',''))) s)) or M13.COLUMN02=P.COLUMN18)  AND M13.COLUMN16 in(23582,23584,23583,23585,23586) AND isnull(M13.COLUMNA13,0)=0 WHERE P5.COLUMN02 = @BILLID and P5.COLUMNA03=@COLUMNA03 and isnull(P5.COLUMNA13,0)=0 AND P.COLUMN18 <0 and (cast(@Billdate as date)>'06/30/2017' or cast(@Billdate as date)>'06/30/17')
WHILE @cnt <= @vochercnt
BEGIN
(select @SubTotal =q.SubTotal,@TaxAvg=q.TaxAvg,@TaxID=q.TaxID,@BILLLINEID=q.BILLLINEID from(select ROW_NUMBER() OVER (ORDER BY P.COLUMN02) cnt,(CAST((isnull(P.COLUMN12,0)) AS decimal(18,2)))SubTotal,(CAST(isnull(P.COLUMN12,0)AS decimal(18,2))*cast(isnull(M13.COLUMN17,0)AS decimal(18,2))*0.01)TaxAvg,M13.COLUMN02 TaxID,P.COLUMN01 BILLLINEID from PUTABLE006 P INNER join PUTABLE005 P5 on P5.COLUMN01= P.COLUMN13 AND P5.COLUMNA03= P.COLUMNA03 AND P5.COLUMNA02= P.COLUMNA02 and isnull(P.COLUMNA13,0)=0 INNER JOIN MATABLE007 M07 ON M07.COLUMN05 ='ITTY009' AND M07.COLUMN02 = P.COLUMN04 AND M07.COLUMNA03 = P.COLUMNA03 and isnull(M07.COLUMNA13,0)=0
INNER JOIN MATABLE013 M13 ON (M13.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(P.COLUMN18,'-',''))) s)) or M13.COLUMN02=P.COLUMN18)  AND M13.COLUMN16 in(23582,23584,23583,23585,23586) AND isnull(M13.COLUMNA13,0)=0 WHERE P5.COLUMN02 = @BILLID and P5.COLUMNA03=@COLUMNA03 and isnull(P5.COLUMNA13,0)=0 AND P.COLUMN18 <0 and (cast(@Billdate as date)>'06/30/2017' or cast(@Billdate as date)>'06/30/17')) q where q.cnt=@cnt) 
select @TaxAG=column16, @TAXRATE=isnull(COLUMN17,0),@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@TaxID
set @COLUMN12 = (select isnull(SUM(isnull(COLUMN06,0)),0) from PUTABLE015 where COLUMN02=@COLUMN02)
set @COLUMN12= (((CAST(@COLUMN12 AS decimal(18,2)))*(100 / (100 + cast(@TTAX AS decimal(18,2))))));
set @BILLItem = (select COLUMN04 from PUTABLE006 where COLUMN01=@BILLLINEID)
set @BQty = (select isnull(COLUMN09,0) from PUTABLE006 where COLUMN01=@BILLLINEID)
set @BPrice = (CAST(iif(cast(@BQty as nvarchar(250))='0',0,CAST(@COLUMN12 AS decimal(18,2))) AS decimal(18,2))/(CAST(@BQty AS decimal(18,2))))
set @COLUMN12= ((CAST(isnull(@BQty,0) AS decimal(18,2)))*(CAST(isnull(@BPrice,0) AS decimal(18,2))));
set @TaxAmnt = (CAST(iif(@SubTotal=0,0,@TaxAvg) AS decimal(18,2))/(CAST(@SubTotal AS decimal(18,2))))
set @ltaxamt= ((CAST(isnull(@TAXRATE,0) AS decimal(18,2))*0.01)*(CAST(isnull(@COLUMN12,0) AS decimal(18,2))));
if(@ltaxamt>0 and (@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0'))
begin
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)

if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   select @CharAcc =max(COLUMN04),@BTaxType = max(column07) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
   --set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @internalid,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @BTaxType, @COLUMN11 = @CharAcc1,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL PAYMENT',@date,@Memo,@venID,0,@ltaxamt,  @PBNO,@CharAcc1,@Project,@COLUMNA02, @COLUMNA03)
end
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE026) as int)+1
set @CharAcc1=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')		
		set @CharAcc=(select top(1)COLUMN02 from fitable001 where COLUMN04=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @internalid,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc1,   @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11,COLUMN13,COLUMN14,COLUMN16, COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
values(@newID,@date,'BILL PAYMENT',@internalid,@venID,@Memo,@TaxID,@TaxAG,@ltaxamt,@BILLItem,@COLUMN12,@TAXRATE,@CharAcc1,@COLUMNA02,@COLUMNA03,@PBNO,@Project)
end
SET @cnt = @cnt + 1
end
		
END

IF @Direction = 'Select'
BEGIN
select * from PUTABLE015
END 

IF @Direction = 'Update'
BEGIN
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
if(@COLUMN03='' or @COLUMN03 is null)
begin
set @COLUMN03=0
end
set @PAMT =(select COLUMN06 from  PUTABLE015 Where COLUMN02=@COLUMN02 );
set @PDUEAMT =(select isnull(COLUMN05,0) from  PUTABLE015 Where COLUMN02=@COLUMN02 );
set @PCRAMT  =(select isnull(COLUMN16,0) from  PUTABLE015 Where COLUMN02=@COLUMN02 );
set @PADVAMT =(select isnull(COLUMN14,0) from  PUTABLE015 Where COLUMN02=@COLUMN02 );
set @COLUMN20 =(select iif(isnull(COLUMN20,0)='',0,isnull(COLUMN20,0)) COLUMN20 from  PUTABLE015 Where COLUMN02=@COLUMN02 );
set @COLUMN17 =(select COLUMN17 from  PUTABLE015 Where COLUMN02=@COLUMN02 );
set @PBNO =(select COLUMN04 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN09 =(select COLUMN13 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN10 =(select COLUMN14 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN11 =(select COLUMN15 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @COLUMN12 =(select COLUMN16 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
--EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
set @date =(select COLUMN18 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @ACC =(select COLUMN08 from  PUTABLE014 Where COLUMN01=@COLUMN08 );
set @venID = (select COLUMN05 from PUTABLE014 where COLUMN01= @COLUMN08)
SET @PID=(SELECT COLUMN06 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
SET @Memo=(SELECT COLUMN10 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
set @Billdate =(select COLUMN08 from  PUTABLE005 where COLUMN02=@COLUMN03);
if(@COLUMN05!= '' and @COLUMN05 is not null)
begin
--EMPHCS1317	Bill Payment screen showing due amount wrong while selecting vendor BY RAJ.Jr 05/11/2015
--set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))+(@PAMT-cast(@COLUMN06 as DECIMAL(18,2))+(@PCRAMT-cast(isnull(@COLUMN16,0 )as DECIMAL(18,2))))+(@PADVAMT-cast(isnull(@COLUMN14,0 )as DECIMAL(18,2))));
declare @pay decimal(18,2)
set @pay=(select (cast(sum(isnull(COLUMN06,0)) as DECIMAL(18,2))+cast(sum(isnull(COLUMN16,0 ))as DECIMAL(18,2))+cast(sum(isnull(COLUMN14,0 ))as DECIMAL(18,2))) from PUTABLE015 where COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0)
if(@COLUMN04=@COLUMN05)
begin
set @COLUMN05=0.00
end
end
--set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))+(@PAMT-cast(@COLUMN06 as DECIMAL(18,2))))
  set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null and @COLUMN08!= '0' ) THEN (select COLUMN13 from PUTABLE014 
   where COLUMN01=@COLUMN08) else @COLUMNA02  END )
   UPDATE PUTABLE015 SET
   COLUMN02=@COLUMN02,	  COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=isnull(@COLUMN05,0),    COLUMN06=@COLUMN06,  
   COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,	  COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,  COLUMN17=@COLUMN17,  COLUMNA01=@COLUMNA01,
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01, 
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   
--EMPHCS1410	Logs Creation by srinivas
set @internalid1 = (select column01 from PUTABLE015 where column02=@COLUMN02)
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Vendor Patment Line Values are Updated in PUTABLE015 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
   --EMPHCS1310 rajasekhar reddy patakota 30/10/2015 Customization of forms at header level
if(@COLUMN05= '' or @COLUMN05 is null)
begin
set @cdueamt=(select min(isnull(COLUMN05,0)) from  PUTABLE015 Where COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0)
set @COLUMN05=((SELECT CASE WHEN isnull(@COLUMN05,0)=0 THEN ( cast(@COLUMN04 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN16,0 )as DECIMAL(18,2)))+cast(isnull(@COLUMN14,0 )as DECIMAL(18,2)))
 ELSE (cast(@PDUEAMT as DECIMAL(18,2))-(@PAMT-cast(@COLUMN06 as DECIMAL(18,2))+(@PCRAMT-cast(isnull(@COLUMN16,0 )as DECIMAL(18,2))))+(@PADVAMT-cast(isnull(@COLUMN14,0 )as DECIMAL(18,2)))) END));
update PUTABLE015 set COLUMN05=@COLUMN05 Where COLUMN02=@COLUMN02
end

set @venID = (select cast(COLUMN05 as int)  from PUTABLE014 where COLUMN01=cast(@COLUMN08 as int))
 if( @COLUMN20=0)
 begin
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
set @APStatus=('PARTIALLY PAID')
if(cast(@COLUMN05 as decimal(18,2))=0)
begin 
set @APStatus=('PAID')
end
if(@COLUMN03='0' or @COLUMN03='' or @COLUMN03=null)
BEGIN
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE027) as int)+1
set @acID = (select COLUMN08  from PUTABLE014 where COLUMN01=@COLUMN08)
set @paid=CAST(@COLUMN04 as decimal(18,2))
set @sID = (select COLUMN05  from PUTABLE014 where COLUMN01=@COLUMN08)
set @SalesRep=(select COLUMN20  from PUTABLE014 where COLUMN01=@COLUMN08)
if(@SalesRep=22306)
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @COLUMN08,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN06,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN14,COLUMN16,COLUMN18, COLUMNA02, COLUMNA03)
values(@newID,@date,'COMMISSION PAYMENT',@COLUMN08,@venID,@Memo,@acID,@PBNO,@paid,@COLUMN06,@sID,@APStatus,@COLUMN14, @COLUMNA02, @COLUMNA03)
end		
set @iID=(select COLUMN01  from FITABLE027 where COLUMN02=@newID)
SET @BAL=(cast((select column10 from FITABLE001 where column02= @deposit) as decimal(18,2)) - cast(@COLUMN06 as decimal(18,2)))
set @HAmount = (select COLUMN25  from PUTABLE014 where COLUMN01=@COLUMN08)
if(@HAmount>0)
begin
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
set @PayeeType = (select COLUMN20  from PUTABLE014 where COLUMN01=@COLUMN08 and isnull(COLUMNA13,0)=0)
SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@acID and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
if(@AccountType=22409)
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Intiated for FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  cast(isnull(@acID,'')as nvarchar(10))+','+  cast(isnull(@date,'')as nvarchar(10))+','+ cast(isnull(@PBNO,'')as nvarchar(10))+',COMMISSION PAYMENT'+  isnull(@COLUMN08,'')+','+  isnull(cast(@venID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+ cast( isnull(@Project,'')as nvarchar(20))+'') 			
   )
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE052) as int)+1
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @COLUMN08,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @AccountType, @COLUMN11 = @acID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN06,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08) 
values(@newID,@acID,@date,@PBNO,'COMMISSION PAYMENT',@PayeeType,@venID,@COLUMN08,@Memo,@COLUMN06,@BAL, @COLUMNA02, @COLUMNA03,@Project, @COLUMNA08)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Created in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  cast(isnull(@acID,'')as nvarchar(10))+','+  cast(isnull(@date,'')as nvarchar(10))+','+ cast(isnull(@PBNO,'')as nvarchar(10))+',COMMISSION PAYMENT'+  isnull(@COLUMN08,'')+','+  isnull(cast(@venID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+ cast( isnull(@Project,'')as nvarchar(20))+'') 			
   )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'COMMISSION PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @COLUMN08,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @AccountType, @COLUMN11 = @acID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN06,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE019) as int)+1
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20) 
values(@newID,@acID,@date,@PBNO,'COMMISSION PAYMENT',@venID,@COLUMN08,@Memo,@COLUMN06,@BAL, @COLUMNA02, @COLUMNA03,@Project)
--EMPHCS1410	Logs Creation by srinivas		
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@deposit and COLUMNA03=@COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Decreased By '+cast(@BAL as nvarchar(15))+' in FITABLE001 Table For '+cast(@COLUMN08 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
SET @PID=(SELECT COLUMN06 FROM PUTABLE014 WHERE COLUMN01=@COLUMN08)
SET @AMT=(SELECT COLUMN15 FROM PUTABLE001 WHERE COLUMN02=@PID)

declare @acIIDU int
	
	SET @PAMT=(SELECT SUM(COLUMN06) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03)
	SET @PIQty=isnull((SELECT max(COLUMN04) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
	--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
	SET @PBQty=isnull((SELECT  @PIQty-(sum(isnull(COLUMN06,0))+sum(isnull(COLUMN14,0))+sum(isnull(COLUMN16,0))) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
	IF(@PBQty>0)
    begin
	UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=11) where COLUMN02=@COLUMN03
	end
	else IF(@PBQty=0.00)
	begin
	UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=12) where COLUMN02=@COLUMN03
	end
end
	--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
set @COLUMN16=(select COLUMN16 from PUTABLE015 where column02=@COLUMN02)
set @COLUMN18=(select COLUMN18 from PUTABLE015 where column02=@COLUMN02)
if(@COLUMN16!='0' and @COLUMN16!='0.00' and @COLUMN16!='' )
	    begin
		set @VochrAmt=(@COLUMN16)
		set @MainString=(@COLUMN18)
		set @cnt =(0);set @vochercnt = (1);
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='D')
		begin
		set @VTransno=(select column04 from PUTABLE001 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN15,0)-isnull(COLUMND05,0))COLUMN15 from PUTABLE001 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from PUTABLE001 where column02=@Vocherid)
		update PUTABLE001 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from PUTABLE001 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update PUTABLE001 set COLUMND05=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update PUTABLE001 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN15,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN15 from PUTABLE001 where column02=@Vocherid)=0)
		begin
		update PUTABLE001 set COLUMN16='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		end
		else if(@Vochertype='P')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN15,0)-isnull(COLUMND05,0))COLUMN16 from FITABLE022 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE022 where column02=@Vocherid)
		update FITABLE020 set COLUMN16='OPEN' where column01 in(select COLUMN09 from FITABLE022 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE022 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE022 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		if((select (isnull(COLUMN05,0)-CAST(isnull(COLUMND05,0) AS DECIMAL(18,2)))COLUMN16 from FITABLE022 where column02=@Vocherid)=0)
		begin
		update FITABLE020 set COLUMN16='CLOSE' where column01 in(select COLUMN09 from FITABLE022 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE020 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMN18,0) from FITABLE020 where column02=@Vocherid)
		update FITABLE020 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE020 set COLUMN18=(@PrevAmt-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		exec usp_PUR_TP_VendorAdvanceCalculations  @PBNO,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@VochrAmt
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE020 set COLUMN18=0 where column02=@Vocherid and columna03=@columna03
		exec usp_PUR_TP_VendorAdvanceCalculations  @PBNO,@COLUMNA02,@COLUMNA03,@COLUMN02,@COLUMN08,@PrevAmt
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		if((select isnull(COLUMN18,0) from FITABLE020 where column02=@Vocherid)=0)
		begin
		update FITABLE020 set COLUMN16='CLOSE' where column02=@Vocherid and columna03=@columna03
		end
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN04,0)-isnull(COLUMND05,0))COLUMN05 from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@TransAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(@PrevAmt+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)+@TransAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@TransAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END

set @COLUMN14=(select COLUMN14 from PUTABLE015 where column02=@COLUMN02)
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
		if(@COLUMN16='' or @COLUMN16 is null) set @COLUMN16=0;
		set @COLUMN06=(select COLUMN06 from PUTABLE015 where column02=@COLUMN02)
		set @COLUMN19=(select COLUMN19 from PUTABLE015 where column02=@COLUMN02)
		set @COLUMN20=(select COLUMN20 from PUTABLE015 where column02=@COLUMN02)
			--if(@COLUMN19='Journal' or @COLUMN19='CreditMemo' or @COLUMN19='ReceiptVoucher' or @COLUMN19='AdvanceReceipt')
			--	begin
			--	if exists(select column02 from putable016 where COLUMN07=@venID and COLUMN09=@PBNO  and COLUMN06='BILL PAYMENT'
			--		 and COLUMN03=2000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable016 set COLUMN13=(COLUMN13-@COLUMN06) where COLUMN07=@venID and COLUMN09=@PBNO  and COLUMN06='BILL PAYMENT'
			--		 and COLUMN03=2000 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	if exists(select column02 from putable019 where COLUMN07=@venID and COLUMN05=@PBNO and COLUMN08=@COLUMN08  
			--	and COLUMN06='BILL PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
   --                 Begin
			--		update putable019 set COLUMN10=(COLUMN10-@COLUMN06) where COLUMN07=@venID and COLUMN05=@PBNO and COLUMN08=@COLUMN08  
			--	and COLUMN06='BILL PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			--		end
			--	end
				if(@COLUMN19='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='ReceiptVoucher')
				begin
				update FITABLE024 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='AdvanceReceipt')
				begin
				update FITABLE023 set COLUMN19=(cast(isnull(COLUMN19,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='CreditMemo')
				begin
				update SATABLE005 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end

set @TaxAvg =0;
set @BILLID = (select COLUMN03 from PUTABLE015 where COLUMN02=@COLUMN02)
--set @internalid1 = (select column01 from PUTABLE005 where column02=@BILLID)
set @Billno = (select column04 from PUTABLE005 where column02=@BILLID)
set @ReverseCharged = (select isnull(COLUMN58,0) from PUTABLE005 where column02=@BILLID)
set @internalid= (select max(column01) from PUTABLE014 where COLUMN01=@COLUMN08)
set @VENID=(select COLUMN05 from PUTABLE014 where COLUMN01=@COLUMN08)
set @memo=(select COLUMN10 from PUTABLE014 where COLUMN01=@COLUMN08)

set @cnt = 1;
select @TTAX=(cast(sum(isnull(M13.COLUMN17,0))AS decimal(18,2))) from PUTABLE006 P INNER join PUTABLE005 P5 on P5.COLUMN01= P.COLUMN13 AND P5.COLUMNA03= P.COLUMNA03 AND P5.COLUMNA02= P.COLUMNA02 and isnull(P.COLUMNA13,0)=0 INNER JOIN MATABLE007 M07 ON M07.COLUMN05 ='ITTY009' AND M07.COLUMN02 = P.COLUMN04 AND M07.COLUMNA03 = P.COLUMNA03 and isnull(M07.COLUMNA13,0)=0
INNER JOIN MATABLE013 M13 ON (M13.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(P.COLUMN18,'-',''))) s)) or M13.COLUMN02=P.COLUMN18)  AND M13.COLUMN16 in(23582,23584,23583,23585,23586) AND isnull(M13.COLUMNA13,0)=0 WHERE P5.COLUMN02 = @BILLID and P5.COLUMNA03=@COLUMNA03 and isnull(P5.COLUMNA13,0)=0 and (cast(@Billdate as date)>'06/30/2017' or cast(@Billdate as date)>'06/30/17')
select @vochercnt=ROW_NUMBER() OVER (ORDER BY P.COLUMN02) from PUTABLE006 P INNER join PUTABLE005 P5 on P5.COLUMN01= P.COLUMN13 AND P5.COLUMNA03= P.COLUMNA03 AND P5.COLUMNA02= P.COLUMNA02 and isnull(P.COLUMNA13,0)=0 INNER JOIN MATABLE007 M07 ON M07.COLUMN05 ='ITTY009' AND M07.COLUMN02 = P.COLUMN04 AND M07.COLUMNA03 = P.COLUMNA03 and isnull(M07.COLUMNA13,0)=0
INNER JOIN MATABLE013 M13 ON (M13.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(P.COLUMN18,'-',''))) s)) or M13.COLUMN02=P.COLUMN18)  AND M13.COLUMN16 in(23582,23584,23583,23585,23586) AND isnull(M13.COLUMNA13,0)=0 WHERE P5.COLUMN02 = @BILLID and P5.COLUMNA03=@COLUMNA03 and isnull(P5.COLUMNA13,0)=0 AND P.COLUMN18 <0 and (cast(@Billdate as date)>'06/30/2017' or cast(@Billdate as date)>'06/30/17')
WHILE @cnt <= @vochercnt
BEGIN
(select @SubTotal =q.SubTotal,@TaxAvg=q.TaxAvg,@TaxID=q.TaxID,@BILLLINEID=q.BILLLINEID from(select ROW_NUMBER() OVER (ORDER BY P.COLUMN02) cnt,(CAST((isnull(P.COLUMN12,0)) AS decimal(18,2)))SubTotal,(CAST(isnull(P.COLUMN12,0)AS decimal(18,2))*cast(isnull(M13.COLUMN17,0)AS decimal(18,2))*0.01)TaxAvg,M13.COLUMN02 TaxID,P.COLUMN01 BILLLINEID from PUTABLE006 P INNER join PUTABLE005 P5 on P5.COLUMN01= P.COLUMN13 AND P5.COLUMNA03= P.COLUMNA03 AND P5.COLUMNA02= P.COLUMNA02 and isnull(P.COLUMNA13,0)=0 INNER JOIN MATABLE007 M07 ON M07.COLUMN05 ='ITTY009' AND M07.COLUMN02 = P.COLUMN04 AND M07.COLUMNA03 = P.COLUMNA03 and isnull(M07.COLUMNA13,0)=0
INNER JOIN MATABLE013 M13 ON (M13.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(P.COLUMN18,'-',''))) s)) or M13.COLUMN02=P.COLUMN18)  AND M13.COLUMN16 in(23582,23584,23583,23585,23586) AND isnull(M13.COLUMNA13,0)=0 WHERE P5.COLUMN02 = @BILLID and P5.COLUMNA03=@COLUMNA03 and isnull(P5.COLUMNA13,0)=0 AND P.COLUMN18 <0 and (cast(@Billdate as date)>'06/30/2017' or cast(@Billdate as date)>'06/30/17')) q where q.cnt=@cnt) 
select @TaxAG=column16, @TAXRATE=isnull(COLUMN17,0),@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@TaxID
set @COLUMN12 = (select isnull(SUM(isnull(COLUMN06,0)),0) from PUTABLE015 where COLUMN02=@COLUMN02)
set @COLUMN12= (((CAST(@COLUMN12 AS decimal(18,2)))*(100 / (100 + cast(@TTAX AS decimal(18,2))))));
set @BILLItem = (select COLUMN04 from PUTABLE006 where COLUMN01=@BILLLINEID)
set @BQty = (select isnull(COLUMN09,0) from PUTABLE006 where COLUMN01=@BILLLINEID)
set @BPrice = (CAST(iif(cast(@BQty as nvarchar(250))='0',0,CAST(@COLUMN12 AS decimal(18,2))) AS decimal(18,2))/(CAST(@BQty AS decimal(18,2))))
set @COLUMN12= ((CAST(isnull(@BQty,0) AS decimal(18,2)))*(CAST(isnull(@BPrice,0) AS decimal(18,2))));
set @TaxAmnt = (CAST(iif(@SubTotal=0,0,@TaxAvg) AS decimal(18,2))/(CAST(@SubTotal AS decimal(18,2))))
set @ltaxamt= ((CAST(isnull(@TAXRATE,0) AS decimal(18,2))*0.01)*(CAST(isnull(@COLUMN12,0) AS decimal(18,2))));
if(@ltaxamt>0 and (@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0'))
begin
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)

if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   select @CharAcc =max(COLUMN04),@BTaxType = max(column07) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
   --set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @internalid,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @BTaxType, @COLUMN11 = @CharAcc1,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL PAYMENT',@date,@Memo,@venID,0,@ltaxamt,  @PBNO,@CharAcc1,@Project,@COLUMNA02, @COLUMNA03)
end
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE026) as int)+1
set @CharAcc1=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')		
		set @CharAcc=(select top(1)COLUMN02 from fitable001 where COLUMN04=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @PBNO, @COLUMN05 = @internalid,  @COLUMN06 = @internalid1,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc1,   @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11,COLUMN13,COLUMN14,COLUMN16, COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
values(@newID,@date,'BILL PAYMENT',@internalid,@venID,@Memo,@TaxID,@TaxAG,@ltaxamt,@BILLItem,@COLUMN12,@TAXRATE,@CharAcc1,@COLUMNA02,@COLUMNA03,@PBNO,@Project)
end
SET @cnt = @cnt + 1
end
		
END

else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE015 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--SET @PBQty=isnull((SELECT min(COLUMN05) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03),0)
--	IF(@PBQty>0)
--    begin
--	UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=11) where COLUMN02=@COLUMN03
--	end
--	else IF(@PBQty=0.00)
--	begin
--	UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=12) where COLUMN02=@COLUMN03
--	end
END

    --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_PUR_TP_PUTABLE015.txt',0
end try

begin catch

		--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PUTABLE015.txt',0
				if not exists(select column01 from putable015 where column08=@column08)
					begin
						delete from putable014 where column01=@column08
						return 0
			end
return 0
end catch
end











GO
