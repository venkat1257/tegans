USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GeneralLedger]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROC [dbo].[GeneralLedger]
(
	@ReportName  nvarchar(250)= null,
	@FrDate    nvarchar(250)= null,
	@ToDate      nvarchar(250)= null,
	@Type        nvarchar(250)= null,
	@Vendor      nvarchar(250)= null,
	@Customer      nvarchar(250)= null,
	@BillNO      nvarchar(250)= null,
	@Jobber      nvarchar(250)= null,
	@SONO      nvarchar(250)= null,
	@PONO      nvarchar(250)= null,
	@Status      nvarchar(250)= null,
	@Item      nvarchar(250)= null,
	@UPC      nvarchar(250)= null,
	@OPUnit nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@SalesRep nvarchar(250)= null,
	@Operating      nvarchar(250)= null,
	--EMPHCS1252 gnaneshwar on 7/10/2015  Inserting Data Into Cost Of Sales When Resourse Consumption Created in Project
	@Project      nvarchar(250)= null,
	@Account nvarchar(250)=null,
	@whereStr nvarchar(1000)=null,
	@Query1 nvarchar(max)=null,
	@Query2 nvarchar(max)=null,
	@DateF nvarchar(250)=null
)
AS
	BEGIN
if @Account!=''
begin
 --set @Account=(select COLUMN04 from FITABLE001 where COLUMN02=@Account and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
 set @whereStr= ' where ID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Account+''') s)'
end
if @Type!=''
begin
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
--set @Type=(select COLUMN04 from MATABLE002 where COLUMN02=@Type and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Type2 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Type2 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s)'
end
end
if @Operating!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
end
if @Project!=''
begin
set @Project=(select COLUMN05 from PRTABLE001 where COLUMN02=@Project)
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @Vendor!=''
begin
set @Vendor=(select COLUMN05 from SATABLE001 where COLUMN02=@Vendor)
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Name in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Name in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if @Customer!=''
begin
set @Customer=(select COLUMN05 from SATABLE002 where COLUMN02=@Customer)
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Name in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Name in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end

select * into #GeneralLedger from (
--Accounts Payable
--select p.COLUMN03 as ID, f.COLUMN04 as Account,FORMAT(p.COLUMN04,@DateF) as [Date] ,p.COLUMN04 Dtt,p.COLUMN06 as Type,p.COLUMN09 as Number,
--(CASE  WHEN p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='PAYMENT VOUCHER' or p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='ADVANCE') then (case when pv.column11=22305 or pv.column11=22334 or pv.column11=22700 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when rv.column20=22305 
-- or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
--WHEN (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Debit Memo' or p.COLUMN06='Credit Memo') then a.COLUMN05
--WHEN (p.COLUMN06='EXPENSE') then (select COLUMN06 from matable010 where column02=p.COLUMN07)
--else '' end) Name,''  Accounts,
--(case when p.COLUMN06='JOURNAL ENTRY' then fj.COLUMN06 when p.COLUMN06='Opening Balance' then '' ELSE p.COLUMN18 END) as Memo,

--(case when p.COLUMN06='Debit Memo'  then ABS(p.COLUMN12) else p.COLUMN13 end) as [Debit],(case when p.COLUMN06='Debit Memo'  then 0 else p.COLUMN12 end) as [Credit],(case when p.COLUMN06='Debit Memo' or p.COLUMN06='Credit Memo' then (isnull(p.COLUMN13,0)-isnull(p.COLUMN12,0)) else (isnull(p.COLUMN12,0)-isnull(p.COLUMN13,0)) end) as [Balance],
--null OpeningBal,'Accounts Payable' [Type1],'22263' [Type2],p.COLUMNA02 OP ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from putable016 p 
----left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0
----left outer join FITABLE032 fj on fj.COLUMN12=p.COLUMN05 and fj.COLUMNA03=p.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22263
--left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=p.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22263 AND (fj.COLUMN04 = p.COLUMN13 OR fj.COLUMN05 = p.COLUMN12) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN09 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN07  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
--left outer join FITABLE001 f on f.COLUMN02=p.COLUMN03  and f.COLUMNA03=p.COLUMNA03
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN20  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
-- where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
-- select p.COLUMN03 as ID, f.COLUMN04 as Account,null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],
---( sum(isnull(p.COLUMN12,0)-isnull(p.COLUMN13,0)) ) OpeningBal,'Accounts Payable' [Type1],'22263' [Type2],p.COLUMNA02 OP ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project  from putable016 p 
--left outer join FITABLE001 f on f.COLUMN02=p.COLUMN03  and f.COLUMNA03=p.COLUMNA03
--where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by f.column04,p.COLUMNA02,p.COLUMN03  
-- union all
 --Accounts Receiable
--select p.COLUMN03 as ID,f.column04 Account,FORMAT(p.COLUMN04,@DateF) as Date,p.COLUMN04 Dtt,p.COLUMN06 as Type, (case when p.COLUMN06='JOURNAL ENTRY' then j.COLUMN04 when p.COLUMN06='Opening Balance' then '' else p.COLUMN05 end) Number,
--(CASE  WHEN p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05  when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='PAYMENT VOUCHER' or p.COLUMN06='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.COLUMN20=22700  or rv.COLUMN20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
--WHEN (p.COLUMN06='Invoice' or p.COLUMN06='Payment' or p.COLUMN06='Credit Memo' or p.COLUMN06='Debit Memo') then b.COLUMN05
--when (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then m.COLUMN06
--else '' end) Name,'' Accounts,
--(case when p.COLUMN06='Opening Balance' then '' else p.COLUMN08 end) as Memo,(case when p.COLUMN06='Credit Memo'  then 0 else p.COLUMN09 end)  [Debit],(case when p.COLUMN06='Credit Memo'  then ABS(p.COLUMN09) else p.COLUMN11 end)  Credit,(case when p.COLUMN06='Credit Memo' or p.COLUMN06='Debit Memo' then(isnull(p.COLUMN11,0)-isnull(p.COLUMN09,0)) else isnull(p.COLUMN09,0)-isnull(p.COLUMN11,0) end)  [Balance],
--null OpeningBal,'Accounts Receivable' [Type1],'22264' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from putable018 p
-- left outer join FITABLE001 f on f.COLUMN02=p.COLUMN03   and f.COLUMNA03=p.COLUMNA03
----left outer join FITABLE031 j on j.COLUMN01=(case WHEN p.COLUMN06='JOURNAL ENTRY' then p.COLUMN05 end)  and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=p.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22264
--left outer join FITABLE031 j on j.COLUMN01=(case WHEN p.COLUMN06='JOURNAL ENTRY' then p.COLUMN05 end)  and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0 
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=p.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22264 and (fj.COLUMN04=p.COLUMN09 or fj.COLUMN05=p.COLUMN11) and ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN05 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN05 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN07  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN18  and pr.COLUMNA03=p.COLUMNA03  and isnull(pr.COLUMNA13,0)=0
--where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--select p.COLUMN03 as ID, f.column04 Account,null as [Date] ,NULL Dtt,'Opening Bal'  Type, null  Number,
--null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],
--( sum(isnull(p.COLUMN09,0)-isnull(p.COLUMN11,0)) ) OpeningBal,'Accounts Receivable' [Type1],'22264' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from putable018 p
--  left outer join FITABLE001 f on f.COLUMN02=p.COLUMN03   and f.COLUMNA03=p.COLUMNA03
--where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by f.column04,p.COLUMN03,p.COLUMNA02
-- union all
 --Expenses
-- select p.COLUMN10 as ID,f.column04 Account, FORMAT(p.COLUMN04,@DateF) as Date,p.COLUMN04 Dtt,p.COLUMN03 Type,
-- (case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN09,'') end)
-- --isnull(p.COLUMN09,'') 
-- 'Number',
--(CASE  WHEN p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='BILL') then a.COLUMN05
-- WHEN (p.COLUMN03='Debit Memo') THEN a.COLUMN05
-- WHEN (p.COLUMN03='INVOICE') then b.COLUMN05
-- WHEN (p.COLUMN03='EXPENSE' or p.COLUMN03='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
--else '' end) Name,'' Accounts,
--(CASE WHEN p.COLUMN03='EXPENSE' THEN f21.COLUMN05
--WHEN p.COLUMN03='PAYMENT VOUCHER' THEN f22.COLUMN04 
--WHEN p.COLUMN03='RECEIPT VOUCHER' THEN f24.COLUMN04
--WHEN p.COLUMN03='JOURNAL ENTRY' THEN fj.COLUMN06
--WHEN p.COLUMN03='BILL' THEN p5.COLUMN12
--WHEN p.COLUMN03='Debit Memo' THEN P1.COLUMN09
--WHEN p.COLUMN03='INVOICE' THEN s9.COLUMN12
--WHEN p.COLUMN03='Opening Balance' THEN f.COLUMN04
--ELSE p.COLUMN05 END) 'Memo',
--p.COLUMN07 'Debit',p.COLUMN08 'Credit',(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0)) as Balance,
--null OpeningBal,'Expenses' [Type1],'22344' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE036 p
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN09 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11 left outer join CONTABLE007 o on o.column02=p.columna02 
--left outer join CONTABLE002 cn on cn.COLUMN02=p.COLUMNA03 left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 
--left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 
--left outer join FITABLE031 j on  j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 
--left outer join SATABLE009 s9 on cast(s9.COLUMN01 as nvarchar(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 and p.COLUMN03='Invoice' 
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03))  and p.COLUMN03='JOURNAL ENTRY' 
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) and (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) and p.COLUMN03='JOURNAL ENTRY' 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
--left outer join FITABLE012 f1 on f1.COLUMN17=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0
--left outer join FITABLE021 f21 on f21.COLUMN06=f1.column01 and f21.COLUMNA02=f1.COLUMNA02 and f21.COLUMNA03=f1.COLUMNA03 and isnull(f21.COLUMNA13,0)=0  AND p.COLUMN10= f21.column03  AND p.COLUMN07= f21.column04
--left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
--left outer join FITABLE031 f3 on f3.COLUMN04=p.column05 and f3.COLUMNA03=p.COLUMNA03 and isnull(f3.COLUMNA13,0)=0 and f3.COLUMNA02=p.COLUMNA02 and p.COLUMN03='JOURNAL ENTRY' 
--left outer join FITABLE020 f20 on f20.COLUMN04=p.column09 and f20.COLUMNA03=p.COLUMNA03 and f20.COLUMNA02=p.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
--left outer join FITABLE022 f22 on f20.COLUMN01=f22.column09 and f22.COLUMNA02=f20.COLUMNA02 and f20.COLUMNA03=f22.COLUMNA03 and isnull(f22.COLUMNA13,0)=0  AND p.COLUMN10= f22.column03  --AND isnull(p.COLUMN08,0)= isnull(f22.column05,0)
--left outer join FITABLE023 F23 ON CAST(F23.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) and F23.COLUMNA02=p.COLUMNA02 and F23.COLUMNA03=p.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
--left outer join FITABLE024 F24 ON F24.COLUMN09=F23.COLUMN01 and F24.COLUMNA02=F23.COLUMNA02 and F24.COLUMNA03=F23.COLUMNA03 and isnull(F24.COLUMNA13,0)=0 AND p.COLUMN10= f24.column03  AND p.COLUMN08= f24.column05 
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
-- inner join FITABLE001 f on f.COLUMN02=p.COLUMN10 and f.COLUMNA03=p.COLUMNA03 and f.COLUMN07 in ('22344')
-- LEFT JOIN PUTABLE001 P1 ON P1.COLUMN04 = p.COLUMN09 and P1.COLUMNA03 = p.COLUMNA03 and P1.COLUMNA02 = p.COLUMNA02 and isnull(P1.COLUMNA13,0)=0
----left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11  and pr.COLUMNA03=p.COLUMNA03  and isnull(pr.COLUMNA13,0)=0
-- where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
-- select p.COLUMN10 as ID,f.column04 Account, null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null Name,null Accounts, null  Memo,null  [Debit],null [Credit],null [Balance],
--( sum(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0)) ) OpeningBal,'Expenses' [Type1],'22344' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE036 p
--left outer join FITABLE001 f on f.COLUMN02=p.COLUMN10 and f.COLUMNA03=p.COLUMNA03 and f.COLUMN07 in ('22344')
--where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by p.COLUMNA02,f.column04,p.COLUMN10

-- select 'COMMISSION PAYMENT' Account, convert(date,p.COLUMN03,103) as Date,p.COLUMN04 Type,isnull(p.COLUMN09,'') 'Number',a.COLUMN05 as Name,p.COLUMN07 'Memo',
--p.COLUMN12 'Debit',p.COLUMN11 'Credit',(isnull(p.COLUMN12,0)-isnull(p.COLUMN11,0)) as Balance,
--null OpeningBal,'Expense' [Type1],p.COLUMNA02 OP from FITABLE027 p
-- left outer join SATABLE002 a on a.COLUMN02=p.COLUMN06
-- left outer join FITABLE001 f on f.COLUMN02=p.COLUMN08 and f.COLUMN07=22344 
-- where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 BETWEEN @FrDate AND @ToDate
-- union all
-- select 'COMMISSION PAYMENT' Account, null as [Date] ,null  Type,null  Number,null Name,null  Memo,null  [Debit],null [Credit],null [Balance],
--( sum(isnull(p.COLUMN12,0)-isnull(p.COLUMN11,0)) ) OpeningBal,'Expense' [Type1],p.COLUMNA02 OP from FITABLE027 p
-- left outer join SATABLE002 a on a.COLUMN02=p.COLUMN06
-- left outer join FITABLE001 f on f.COLUMN02=p.COLUMN08 and f.COLUMN07=22344 
-- where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 < @FrDate group by p.COLUMNA02
 
-- union all
----Equity
--select p.COLUMN07 as ID,m1.COLUMN04 'Account', FORMAT(p.COLUMN04,@DateF) as Date,row_number() over(order by p.COLUMN04) Dtt,p.COLUMN03 Type,(case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN11,'') end) 'Number',(CASE WHEN p.COLUMN03='JOURNAL ENTRY' then (case when j.column07='22305' or fj.column14='22305' then a.COLUMN05
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='ISSUE CHEQUE') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT' or p.COLUMN03='Service' or p.COLUMN03='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN03='INVOICE' or p.COLUMN03='PAYMENT' or p.COLUMN03='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN03='EXPENSE' or p.COLUMN03='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
--end) Name,'' Accounts,p.COLUMN08 Memo,
--p.COLUMN10 'Debit',p.COLUMN09 'Credit',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as Balance,
--null OpeningBal,'Equity' [Type1],'22330' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE029 p 
--inner join FITABLE001 m1 on m1.COLUMN02=p.COLUMN07 and m1.COLUMNA03=p.COLUMNA03 and m1.COLUMN07='22330' 
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN11 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN11 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02  and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
--left outer join FITABLE031 j on  j.COLUMN04=p.COLUMN11 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02 
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=p.COLUMN07 --AND (fj.COLUMN04=p.COLUMN09 or fj.COLUMN05=p.COLUMN10) 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12  and pr.COLUMNA03=p.COLUMNA03  and isnull(pr.COLUMNA13,0)=0
--where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--select p.COLUMN07 as ID,m.COLUMN04 'Account',null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],
--( sum(isnull(p.COLUMN09,0)-isnull(p.COLUMN10,0))) OpeningBal,'Equity' [Type1],'22330' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE029 p 
--inner join FITABLE001 m on m.COLUMN02=p.COLUMN07 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22330' 
--where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by p.COLUMNA02,p.COLUMN07,m.COLUMN04
-- union all
----Current liabilities
--select p.COLUMN08 as ID,(iif ((p.COLUMN17!='' )  , p.COLUMN17,  f.COLUMN04)) 'Account',FORMAT(p.COLUMN03,@DateF) as Date,p.COLUMN03 Dtt, p.COLUMN04 Type,isnull(p.COLUMN09,'') 'Number',
--(CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05
-- when  j.column07='22700' or fj.column14='22700' then a.COLUMN05
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='ADVANCE PAYMENT' or p.COLUMN04='PDC ISSUED') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='RECEIPT VOUCHER' or p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='BILL' or p.COLUMN04='Debit Memo' or p.COLUMN04='BILL PAYMENT') then a.COLUMN05
-- WHEN (p.COLUMN04='INVOICE' or p.COLUMN04='Credit Memo' or p.COLUMN04='PAYMENT') then b.COLUMN05
 
-- WHEN (p.COLUMN04='EXPENSE' or p.COLUMN04='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
--else '' end) Name,'' Accounts,
--(case when p.COLUMN04='JOURNAL ENTRY' then fj.COLUMN06 
--WHEN (p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT')  then rv.COLUMN11
--when p.COLUMN04='ADVANCE PAYMENT' then pv.COLUMN08

--else p.COLUMN07 end) 'Memo',
--(CASE  WHEN p.COLUMN17 like'INPUT%' AND isnull(p.COLUMN11,0)>0  then isnull(p.COLUMN11,0)
--	   WHEN p.COLUMN17 not like'INPUT%' AND isnull(p.COLUMN12,0)>0  then isnull(p.COLUMN12,0)
--end )'Debit',
--(CASE  WHEN (p.COLUMN17 like'INPUT%' AND isnull(p.COLUMN12,0)>0)  then isnull(p.COLUMN12,0)  
--	   WHEN (p.COLUMN17 not like'INPUT%' AND isnull(p.COLUMN11,0)>0)  then isnull(p.COLUMN11,0)
--end )'Credit',
--(CASE  WHEN p.COLUMN04='BILL' then isnull(p.COLUMN11,0) 
--else p.COLUMN12
--end )'Debit',
--(CASE  WHEN p.COLUMN04!='BILL'then isnull(p.COLUMN11,0) 
 
----else p.COLUMN11
--end )'Credit',
--p.COLUMN12 'Debit',p.COLUMN11 'Credit',
--isnull(p.COLUMN11,0)'Debit',
--isnull(p.COLUMN12,0) 'Credit',
--(isnull(p.COLUMN11,0)-isnull(p.COLUMN12,0)) as Balance,
--null OpeningBal,'Current liabilities' [Type1],'22383' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE026 p 
--left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03)) and (((p.COLUMN11) in (fj.COLUMN05)) or ((p.COLUMN12) in (fj.COLUMN04))) 
--and isnull(p.column06,0) = isnull(fj.column07,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN09 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
--left join FITABLE001 f on f.COLUMN02=p.COLUMN08 and f.COLUMNA03=p.COLUMNA03  and f.COLUMN07='22383' 
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN19  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--where isnull(p.COLUMNA13,0)=0   and 
--P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 BETWEEN @FrDate AND @ToDate
-- union all
--select p.COLUMN08 as ID,(iif ((p.COLUMN17!='' or p.COLUMN17 is not null) and (f.COLUMN04='' or f.COLUMN04 is null) , p.COLUMN17,  f.COLUMN04)) 'Account',null as Date ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],

--(-isnull((CASE  WHEN p.COLUMN17 like'INPUT%' AND sum(isnull(p.COLUMN11,0))>0  then -sum(isnull(p.COLUMN11,0))
--	   WHEN p.COLUMN17 not like'INPUT%' AND sum(isnull(p.COLUMN12,0))>0  then -sum(isnull(p.COLUMN12,0))
--end ),0)-
--isnull(((CASE  WHEN (p.COLUMN17 like'INPUT%' AND sum(isnull(p.COLUMN12,0))>0)  then -sum(isnull(p.COLUMN12,0))  
--	   WHEN (p.COLUMN17 not like'INPUT%' AND sum(isnull(p.COLUMN11,0))>0)  then sum(isnull(p.COLUMN11,0))
--end )),0)) OpeningBal,'Current liabilities' [Type1],'22383' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE026 p 
--left join FITABLE001 f on f.COLUMN02=p.COLUMN08 and f.COLUMNA03=p.COLUMNA03  and f.COLUMN07='22383' 
--where isnull(p.COLUMNA13,0)=0  and 
--P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 < @FrDate group by f.column04,p.column17,p.COLUMNA02,p.COLUMN08
-- union all
----current assets
--select p.COLUMN10 as ID,m.COLUMN04 'Account',FORMAT(p.COLUMN04,@DateF) as Date,p.COLUMN04 Dtt, p.COLUMN03 Type,(case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN09,'') end) 'Number',
--(CASE  WHEN p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m1.COLUMN09 when j.column07='22492' or fj.column14='22492' then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.column11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT' or p.COLUMN03='Debit Memo' or p.COLUMN03='Credit Memo') then a.COLUMN05
-- WHEN (p.COLUMN03='INVOICE' or p.COLUMN03='PAYMENT') then b.COLUMN05
-- WHEN (p.COLUMN03='EXPENSE' or p.COLUMN03='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
--else '' end) Name,'' Accounts,
--p.COLUMN05 'Memo',
--p.COLUMN07 'Debit',p.COLUMN08 'Credit',(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0)) as Balance,null OpeningBal,'current assets' [Type1],'22402' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE034 p  left join FITABLE001 m on m.COLUMN02=p.COLUMN10  and m.COLUMNA03=p.COLUMNA03   left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11  
--left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03))  and ((p.COLUMN06) in (fj.COLUMN07)) and ((p.COLUMN07) in (fj.COLUMN04)) and ((p.COLUMN08) in (fj.COLUMN05)) 
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN09 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
-- left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--where  isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate and m.COLUMN07='22402'
-- union all
--select  p.COLUMN10 as ID,m.COLUMN04 'Account',NULL [Date] ,NULL Dtt, 'Openinig Bal'  Type,null  Number,
--null Name,null Accounts,NULL Memo,null  [Debit],null [Credit],null [Balance],( sum(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0)) ) OpeningBal,'current assets' [Type1],'22402' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE034 p  left join FITABLE001 m on m.COLUMN02=p.COLUMN10  and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22402'  left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11  
--where   isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate 
-- group by m.column04,p.COLUMNA02,p.COLUMN10,p.COLUMN04
-- union all

--fixed assets
--EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET,General ledger COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
--select p.COLUMN03 as ID,m.COLUMN04 'Account',FORMAT(p.COLUMN04,@DateF) as Date,p.COLUMN04 Dtt, p.COLUMN05 Type,
--(case when p.COLUMN05='Opening Balance' then '' else isnull(p.COLUMN06,'') end) 'Number',
--(CASE  WHEN p.COLUMN05='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m1.COLUMN09 when j.column07='22492' or fj.column14='22492' then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN05='PAYMENT VOUCHER' or p.COLUMN05='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN05='RECEIPT VOUCHER' or p.COLUMN05='ADVANCE RECEIPT' or p.COLUMN05='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN05='BILL' or p.COLUMN05='BILL PAYMENT') then a.COLUMN05
-- WHEN (p.COLUMN05='INVOICE' or p.COLUMN05='PAYMENT') then b.COLUMN05
-- WHEN (p.COLUMN05='EXPENSE' or p.COLUMN05='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN07)
--else '' end) Name,'' Accounts,
--p.COLUMN08 'Memo',
--p.COLUMN09 'Debit',p.COLUMN10 'Credit',(isnull(p.COLUMN09,0)-isnull(p.COLUMN10,0)) as Balance,null OpeningBal,'fixed assets' [Type1],'22403' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE028 p  left join  FITABLE001 m on m.COLUMN02=p.COLUMN03 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22403' 
--left outer join FITABLE031 j on j.COLUMN04=p.COLUMN06 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (fj.COLUMN03)) and (((p.COLUMN09) in (fj.COLUMN04)) or ((p.COLUMN10) in (fj.COLUMN05))) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN06 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN06 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN07  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
-- left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--select p.COLUMN03 as ID,m.COLUMN04 'Account',null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],( sum(isnull(p.COLUMN09,0)-isnull(p.COLUMN10,0))) OpeningBal,'fixed assets' [Type1],'22403' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE028 p  left join  FITABLE001 m on m.COLUMN02=p.COLUMN03 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22403' 
--where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by m.column04,p.COLUMNA02,p.COLUMN03
-- union all
--income register
--select p.COLUMN08 as ID,
--m.COLUMN04 'Account',
--FORMAT(p.COLUMN03,@DateF) as Date,p.COLUMN03 Dtt,p.COLUMN04 Type,
--(CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then isnull((select column04 from FITABLE031 where COLUMN01=p.COLUMN05),'') 
--WHEN p.COLUMN04='PAYMENT VOUCHER' then isnull((pv.column04),'')
--WHEN p.COLUMN04='RECEIPT VOUCHER' then isnull((rv.column04),'')
--when p.COLUMN04='Credit Memo' then isnull((select column04 from SATABLE005 where COLUMN01=(select max(column19) from SATABLE006 where column01=p.COLUMN05)),'')
--when p.COLUMN04='INVOICE' then isnull((select column04 from SATABLE009 where COLUMN01=(select max(column15) from satable010 where column01=p.COLUMN05)),'')
-- when p.COLUMN04='PAYMENT' then isnull((select column04 from SATABLE011 where COLUMN01=(select max(column08) from satable012 where column01=p.COLUMN05)),'')
--when p.COLUMN04='BILL' then isnull((select column04 from PUTABLE005 where COLUMN01=p.COLUMN05),'') when p.COLUMN04='EXPENSE' or p.COLUMN04='EXPENCE' then isnull(f12.COLUMN17,'') end )'Number',
--(CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m1.COLUMN09 when j.column07='22492' or fj.column14='22492' then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='RECEIPT VOUCHER' or p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='BILL' or p.COLUMN04='BILL PAYMENT' or p.COLUMN04='Service' or p.COLUMN04='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN04='INVOICE' or p.COLUMN04='PAYMENT' or p.COLUMN04='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN04='EXPENSE' or p.COLUMN04='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
-- end ) 'Name','' Accounts,
--p.COLUMN07 'Memo',p.COLUMN11 as 'Debit',p.COLUMN09 as 'Credit',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
---SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as 'Balance',null OpeningBal,'income register' [Type1],'22405' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE025 p 
--inner join FITABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22405' 
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and fj.COLUMNA02=j.COLUMNA02 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03)) 
----AND (((p.COLUMN09) in (fj.COLUMN04)) or ((p.COLUMN11) in (fj.COLUMN05))) 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join FITABLE020 pv on pv.COLUMN01=p.COLUMN05 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN01=p.COLUMN05 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join FITABLE012 f12 on f12.COLUMN01=p.COLUMN05 and f12.COLUMNA03=p.COLUMNA03 and f12.COLUMNA02=p.COLUMNA02 and isnull(f12.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
--  where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 BETWEEN @FrDate AND @ToDate
-- GROUP BY m.COLUMN04,p.COLUMN04,p.COLUMN03,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN08,p.COLUMN09,p.COLUMN11,p.COLUMNA02,p.COLUMN09,pr.COLUMN05 ,j.column07, pv.column11, rv.column20, a.COLUMN05, b.COLUMN05,fj.COLUMN14,m1.COLUMN09,pv.column04,rv.column04,f12.COLUMN17
-- union all
--select p.COLUMN08 as ID,
--m.COLUMN04 'Account',
--null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null 'Name',null Accounts,null Memo,null  [Debit],null [Credit],null [Balance],-( sum(isnull(p.COLUMN09,0)-isnull(p.COLUMN11,0))) OpeningBal,'income register' [Type1],'22405' [Type2],p.COLUMNA02 OP ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE025 p 
--inner join FITABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22405' 
--where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 < @FrDate group by m.column04,p.COLUMNA02,p.COLUMN08
--UNION ALL
--select F35.column10 ID,  
--F35.COLUMN03 'Account',FORMAT(F35.[COLUMN04],@dateF) as Date,  NULL Dtt,
--'Opening Bal'  Type,null  Number,--'' as Project,'' Reference#,'' 'Memo',
----F35.COLUMN07 as 'Increased Amount',F35.COLUMN08 as 'Decreased Amount',(SUM(isnull(F35.COLUMN07,0)) OVER(ORDER BY F35.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(F35.COLUMN08,0)) OVER(ORDER BY F35.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as 'Balance Amount',
--null 'Name',null Accounts,null  Memo,null  [Debit],null [Credit],(SUM(isnull(F35.COLUMN07,0)) OVER(ORDER BY F35.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(F35.COLUMN08,0)) OVER(ORDER BY F35.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance],( sum(isnull(F35.COLUMN07,0)-isnull(F35.COLUMN08,0))) OpeningBal,'income register' [Type1],'22405' [Type2],F35.COLUMNA02 OP , cast(C7.COLUMN03 as nvarchar(250)) as OU,null Project
----row_number() over(order by F35.COLUMN04) RNO,
----'' 'Created By' 
--from FITABLE035 F35
--left outer join FITABLE001 m ON F35.COLUMN10=m.COLUMN02 AND F35.COLUMNA03=m.COLUMNA03
--LEFT OUTER JOIN CONTABLE007 C7 ON C7.COLUMN02 = F35.COLUMNA02 AND ISNULL(C7.COLUMNA13,0)=0
--where --F35.COLUMN10=@ide 
-- F35.COLUMN04 between @FrDate AND @ToDate AND F35.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND F35.COLUMNA03=@AcOwner --order by RNO desc
--group by F35.COLUMN10,F35.COLUMN03,F35.COLUMN04,F35.COLUMN07,F35.COLUMN08,F35.COLUMNA02,C7.COLUMN03
-- union all
----other income
--select p.COLUMN10 as ID,m.COLUMN04 'Account',FORMAT(p.COLUMN04,@DateF) as Date,p.COLUMN04 Dtt, p.COLUMN03 Type,(case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN09,'') end) 'Number',
--(CASE  WHEN p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m1.COLUMN09 when j.column07='22492' or fj.column14='22492' then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT' or p.COLUMN03='Service' or p.COLUMN03='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN03='INVOICE' or p.COLUMN03='PAYMENT' or p.COLUMN03='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN03='EXPENSE' or p.COLUMN03='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
-- else '' end) Name,'' Accounts,
--(CASE WHEN p.COLUMN03='INVOICE' THEN S9.COLUMN12 
--WHEN p.COLUMN03='Credit Memo' THEN S5.COLUMN09 WHEN p.COLUMN03='Opening Balance' THEN '' ELSE p.COLUMN05 END) 'Memo',
--p.COLUMN08 'Debit',p.COLUMN07 'Credit',(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0)) as Balance,null OpeningBal,'other income' [Type1],'22407' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE035 p  left join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22407'
--left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22407
----left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (fj.COLUMN03)=p.COLUMN10 
----AND (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN09 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
--left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
--left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
-- left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
-- where isnull((p.COLUMNA13),0)=0 and 
--P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--select p.COLUMN10 as ID,m.COLUMN04 'Account',null as [Date] ,NULL Dtt,'Opening Bal'   Type,null   Number,
--null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],( sum(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0))) OpeningBal,'other income' [Type1],'22407' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE035 p  left join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22407' 
--where isnull((p.COLUMNA13),0)=0 and 
--P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by m.column04,p.COLUMNA02,p.COLUMN10
-- union all
----other expenses
--select p.COLUMN10 as ID,m.COLUMN04 'Account',FORMAT(p.COLUMN04,@DateF) as Date,p.COLUMN04 Dtt, p.COLUMN03 Type,(case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN09,'') end) 'Number',
--(CASE  WHEN p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m1.COLUMN09 when j.column07='22492' or fj.column14='22492' then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT' or p.COLUMN03='Service' or p.COLUMN03='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN03='INVOICE' or p.COLUMN03='PAYMENT' or p.COLUMN03='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN03='EXPENSE' or p.COLUMN03='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
-- else '' end) Name,'' Accounts,
--(CASE WHEN p.COLUMN03 = 'PAYMENT VOUCHER' THEN F22.COLUMN04 
--WHEN p.COLUMN03 = 'RECEIPT VOUCHER' THEN F24.COLUMN04
--WHEN p.COLUMN03 = 'JOURNAL ENTRY' THEN fj.COLUMN06
--WHEN p.COLUMN03 = 'EXPENSE' THEN f21.COLUMN05
--WHEN p.COLUMN03 = 'BILL' THEN P5.COLUMN12
--WHEN p.COLUMN03 = 'Debit Memo' THEN p1.COLUMN09
--WHEN p.COLUMN03 = 'EXPENSE' OR p.COLUMN03 = 'EXPENCE' THEN f21.COLUMN05
--WHEN p.COLUMN03 = 'Opening Balance' THEN ''
--ELSE p.COLUMN05 END) 'Memo',
--p.COLUMN07 'Credit',p.COLUMN08 'Debit',(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0)) as Balance,null OpeningBal,'other expenses' [Type1],'22408' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE036 p 
--left outer join FITABLE031 j on  j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN09 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
-- left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
-- inner join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07 in ('22408') 
-- left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) and (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) and p.COLUMN03='JOURNAL ENTRY' 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join FITABLE020 f20 on f20.COLUMN04=p.column09 AND CAST(f20.COLUMN01 AS NVARCHAR(250))= CAST(p.column05 AS NVARCHAR(250)) and f20.COLUMNA03=p.COLUMNA03 and f20.COLUMNA02=p.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
--left outer join FITABLE022 f22 on f20.COLUMN01=f22.column09 and f20.COLUMNA03=f22.COLUMNA03 and f20.COLUMNA02=f22.COLUMNA02 and isnull(f22.COLUMNA13,0)=0 AND F22.COLUMN05=P.COLUMN07 AND F22.COLUMN03=P.COLUMN10
--left outer join FITABLE023 F23 ON CAST(F23.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) AND f23.COLUMN04=p.column09 and F23.COLUMNA02=p.COLUMNA02 and F23.COLUMNA03=p.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
--left outer join FITABLE024 F24 ON F24.COLUMN09=F23.COLUMN01 and F24.COLUMNA03=F23.COLUMNA03 and F24.COLUMNA02=F23.COLUMNA02 and isnull(F24.COLUMNA13,0)=0 AND F24.COLUMN05=P.COLUMN08 AND F24.COLUMN03=P.COLUMN10
--left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMNA02=p.COLUMNA02  and isnull(p5.COLUMNA13,0)=0
--left outer join  PUTABLE001 p1 on p1.COLUMN04=p.COLUMN09 and p1.COLUMNA03=p.COLUMNA03 and p1.COLUMNA02=p.COLUMNA02 and isnull(p1.COLUMNA13,0)=0
--left outer join FITABLE012 F12 ON CAST(F12.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) AND F12.COLUMN17=p.column09 and F12.COLUMNA02=p.COLUMNA02 and F12.COLUMNA03=p.COLUMNA03 and isnull(F12.COLUMNA13,0)=0
--left outer join FITABLE021 f21 on f21.COLUMN06=F12.column01 and f21.COLUMNA03=F12.COLUMNA03 and isnull(f21.COLUMNA13,0)=0 and  f21.COLUMNA02=F12.COLUMNA02 AND f21.COLUMN04=P.COLUMN07 AND f21.COLUMN03=P.COLUMN10
-- where isnull((p.COLUMNA13),0)=0 and 
--P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--select p.COLUMN10 as ID,m.COLUMN04 'Account',null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null Name,null Accounts,null   Memo,null  [Debit],null [Credit],null [Balance],( sum(isnull(p.COLUMN07,0)-isnull(p.COLUMN08,0))) OpeningBal,'other expenses' [Type1],'22408' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE036 p 
--inner join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07 in ('22408') where isnull((p.COLUMNA13),0)=0 and 
--P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by m.column04,p.COLUMNA02,p.COLUMN10
-- union all
----Cost of Goods Sold
--select p.COLUMN08 as ID,
--m.COLUMN04 'Account',
--FORMAT(p.COLUMN03,@DateF) as Date,p.COLUMN03 Dtt,p.COLUMN04 Type,
--(CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then isnull((select column04 from FITABLE031 where COLUMN01=p.COLUMN05),'') 
--WHEN p.COLUMN04='PAYMENT VOUCHER' then isnull((pv.column04),'')
-- WHEN p.COLUMN04='RECEIPT VOUCHER' then isnull((rv.column04),'')
--when p.COLUMN04='Credit Memo' then isnull((select column04 from SATABLE005 where COLUMN01=(select max(column19) from SATABLE006 where column01=p.COLUMN05)),'')
--when p.COLUMN04='INVOICE' then isnull((select column04 from SATABLE009 where COLUMN01=(select max(column15) from satable010 where column01=p.COLUMN05)),'')
-- when p.COLUMN04='PAYMENT' then isnull((select column04 from SATABLE011 where COLUMN01=(select max(column08) from satable012 where column01=p.COLUMN05)),'')
--when p.COLUMN04='BILL' then isnull((select column04 from PUTABLE005 where COLUMN01=p.COLUMN05),'')
--when p.COLUMN04='EXPENSE' then isnull((select COLUMN17 from FITABLE012 where COLUMN01=p.COLUMN05),'')end )'Number',
--(CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m1.COLUMN09 when j.column07='22492' or fj.column14='22492' then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='RECEIPT VOUCHER' or p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m1.COLUMN09 else '' end)
-- WHEN (p.COLUMN04='BILL' or p.COLUMN04='BILL PAYMENT' or p.COLUMN04='Service' or p.COLUMN04='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN04='INVOICE' or p.COLUMN04='PAYMENT' or p.COLUMN04='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN04='EXPENSE' or p.COLUMN04='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN06)
-- end ) 'Name','' Accounts,
--p.COLUMN07 'Memo',p.COLUMN09 as 'Debit',p.COLUMN11 as 'Credit',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
---SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as 'Balance',null OpeningBal,'Cost of Goods Sold' [Type1],'22406' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project from FITABLE025 p inner join FITABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22406' 
--left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03)) and (fj.COLUMN04=p.COLUMN09 or fj.COLUMN05=p.COLUMN11) and ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join FITABLE020 pv on pv.COLUMN01=p.COLUMN05 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN01=p.COLUMN05 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN06 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN06  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06  and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.COLUMNA13,0)=0
-- left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--  where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 BETWEEN @FrDate AND @ToDate
-- GROUP BY m.COLUMN04,p.COLUMN04,p.COLUMN03,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN08,p.COLUMN09,p.COLUMN11,p.COLUMNA02,p.COLUMN09,pr.COLUMN05 , j.column07, pv.column11, rv.column20, a.COLUMN05, b.COLUMN05,fj.COLUMN14,m1.COLUMN09,pv.column04,rv.column04
-- union all
--select p.COLUMN08 as ID,
--m.COLUMN04 'Account',null as [Date] ,NULL Dtt,'Opening Bal'  Type,null  Number,
--null 'Name',null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],-( sum(isnull(p.COLUMN09,0)-isnull(p.COLUMN11,0))) OpeningBal,'Cost of Goods Sold' [Type1],'22406' [Type2],p.COLUMNA02 OP  ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,null Project from FITABLE025 p inner join FITABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07='22406' 
-- where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN03 < @FrDate group by m.column04,p.COLUMNA02,p.COLUMN08
-- union all
-- --Bank
--SELECT  p.COLUMN03 as ID,s.[COLUMN04] AS Account,FORMAT(p.COLUMN04,@DateF) AS Date,p.COLUMN04 Dtt,
--					p.[COLUMN06] AS Type,(case when p.COLUMN06='Opening Balance' then '' else p.COLUMN05 end) AS Number,
--(CASE WHEN p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='PAYMENT VOUCHER' or p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='ISSUE CHEQUE') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Service' or p.COLUMN06='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN06='INVOICE' or p.COLUMN06='PAYMENT' or p.COLUMN06='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN07)
--end) Name,(case when p.COLUMN06='PAYMENT VOUCHER' then pv1.[COLUMN04] when p.COLUMN06='JOURNAL ENTRY' then jv1.[COLUMN04] 
--when p.COLUMN06='RECEIPT VOUCHER' then rv1.[COLUMN04] when p.COLUMN06='EXPENSE' then ex1.[COLUMN04] end) Accounts,
--(case when p.COLUMN06='PAYMENT VOUCHER' then f22.COLUMN04 
--when p.COLUMN06='RECEIPT VOUCHER' then f24.COLUMN04
--when p.COLUMN06='ADVANCE RECEIPT' then rv.COLUMN11
--when p.COLUMN06='ADVANCE PAYMENT' then pv.COLUMN08
--when p.COLUMN06='JOURNAL ENTRY' then fj.COLUMN06
--when p.COLUMN06='WITHDRAW' then F62.COLUMN19
--when p.COLUMN06='DEPOSIT' then F62.COLUMN19
--when p.COLUMN06='TRANSFER' then F62.COLUMN19
--when p.COLUMN06='BILL PAYMENT' then P14.COLUMN10
--when p.COLUMN06='EXPENSE' then F45.COLUMN13
--else p.COLUMN09 end) Memo,p.[COLUMN11] AS Debit,
--					p.[COLUMN10] AS Credit,(isnull(p.COLUMN11,0)-isnull(p.COLUMN10,0)) AS Balance,null OpeningBal,'Bank' [Type1],'22266' [Type2],p.COLUMNA02 OP ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project 
--					 From	PUTABLE019 p 
--left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 and s.COLUMNA03=p.COLUMNA03  and s.COLUMN07='22266' 
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN20  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--left outer join FITABLE031 j on j.COLUMN01=p.COLUMN08 and j.COLUMN04=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0
-- left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)) 
-- AND (fj.COLUMN04 = p.COLUMN11 OR fj.COLUMN05 = p.COLUMN10) 
-- AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN05 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN05 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN07  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join FITABLE023 v1 on v1.COLUMN04=p.COLUMN05 and v1.COLUMNA03=p.COLUMNA03 and v1.COLUMNA02=p.COLUMNA02 and isnull(v1.COLUMNA13,0)=0 
--left outer join FITABLE024 F24 on F24.COLUMN09 = v1.COLUMN01 and F24.COLUMNA03 = v1.COLUMNA03 and F24.COLUMNA02 = v1.COLUMNA02 and isnull(F24.COLUMNA13,0) = 0 and F24.column01 = p.COLUMN08
--left join FITABLE022 f22 on pv.COLUMN01=f22.column09 and pv.COLUMNA03=f22.COLUMNA03 and f22.column01 = p.COLUMN08  and isnull(f22.COLUMNA13,0)=0  and pv.COLUMNA02=f22.COLUMNA02
--LEFT OUTER JOIN FITABLE062 F62 ON F62.COLUMN05 = P.COLUMN05 AND F62.COLUMNA03 = P.COLUMNA03 AND F62.COLUMNA02 = P.COLUMNA02 AND ISNULL(F62.COLUMNA13,0)=0
--LEFT OUTER JOIN putable014 P14 ON P14.COLUMN04 = p.COLUMN05 and P14.COLUMNA03 = p.COLUMNA03 and P14.COLUMNA02 = p.COLUMNA02 AND ISNULL(P14.COLUMNA13,0)=0
--LEFT OUTER JOIN FITABLE045 F45 ON F45.COLUMN04 = P.COLUMN05 AND F45.COLUMNA03 = P.COLUMNA03 AND F45.COLUMNA02 = P.COLUMNA02 AND ISNULL(F45.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
----left outer join FITABLE022 f22 on f22.COLUMN09=pv.COLUMN01 and f22.COLUMNA03=pv.COLUMNA03 and f22.COLUMNA02=pv.COLUMNA02 and isnull(f22.COLUMNA13,0)=0
----left outer join FITABLE024 f24 on f24.COLUMN09=rv.COLUMN01 and f24.COLUMNA03=rv.COLUMNA03  and f24.COLUMNA02=rv.COLUMNA02 and isnull(f24.COLUMNA13,0)=0
--left outer join FITABLE012 f12 on f12.COLUMN17=p.COLUMN05 and f12.COLUMNA03=p.COLUMNA03 and f12.COLUMNA02=p.COLUMNA02 and isnull(f12.COLUMNA13,0)=0
--left outer join FITABLE021 f21 on f21.COLUMN06=f12.COLUMN01 and f21.COLUMNA03=f12.COLUMNA03 and f21.COLUMNA02=f12.COLUMNA02 and isnull(f21.COLUMNA13,0)=0
--left outer join FITABLE001 pv1 on pv1.COLUMN02=f22.COLUMN03 and pv1.COLUMNA03=f22.COLUMNA03 and isnull(pv1.columna13,0)=0
--left outer join FITABLE001 jv1 on jv1.COLUMN02=fj.COLUMN03 and jv1.COLUMNA03=fj.COLUMNA03 and isnull(jv1.columna13,0)=0
--left outer join FITABLE001 rv1 on rv1.COLUMN02=f24.COLUMN03 and rv1.COLUMNA03=f24.COLUMNA03 and isnull(rv1.columna13,0)=0
--left outer join FITABLE001 ex1 on ex1.COLUMN02=f21.COLUMN03 and ex1.COLUMNA03=f21.COLUMNA03 and isnull(ex1.columna13,0)=0
--where isnull((p.COLUMNA13),0)=0 and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)   AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--					SELECT p.COLUMN03 as ID,s.[COLUMN04] AS Account,null as [Date] ,NULL Dtt,'Opening Bal' Type,
--	null  Number,null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],
--	( sum(isnull(p.COLUMN11,0)-isnull(p.COLUMN10,0))) OpeningBal,'Bank' [Type1],'22266' [Type2],p.COLUMNA02 OP ,
--	(select column03 from contable007 where column02=p.COLUMNA02) as OU ,null Project
--					 From	PUTABLE019 p 
--					left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 and s.COLUMNA03=p.COLUMNA03 and s.COLUMN07='22266' 
--					where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by s.column04,p.COLUMNA02,p.COLUMN03
-- union all
-- --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
-- --Cash
--SELECT  p.COLUMN03 as ID,s.[COLUMN04] AS Account,FORMAT(p.COLUMN04,@DateF) AS Date,p.COLUMN04 Dtt,
--					p.[COLUMN06] AS Type,(case when p.COLUMN06='Opening Balance' then '' else p.[COLUMN05] end) AS Number,
--(CASE WHEN p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
--when  j.column07='22700' or fj.column14='22700' then a.COLUMN05
--when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='PAYMENT VOUCHER' or p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='ISSUE CHEQUE') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
-- WHEN (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Service' or p.COLUMN06='Debit Memo') then a.COLUMN05
-- WHEN (p.COLUMN06='INVOICE' or p.COLUMN06='PAYMENT' or p.COLUMN06='Credit Memo') then b.COLUMN05
-- WHEN (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN08)
--end) Name,(case when p.COLUMN06='PAYMENT VOUCHER' then pv1.[COLUMN04] when p.COLUMN06='JOURNAL ENTRY' then jv1.[COLUMN04] 
--when p.COLUMN06='RECEIPT VOUCHER' then rv1.[COLUMN04] when p.COLUMN06='EXPENSE' then ex1.[COLUMN04] end) Accounts,p.COLUMN10 Memo,p.[COLUMN12] AS Debit,
--					p.[COLUMN11] AS Credit,(isnull(p.COLUMN12,0)-isnull(p.COLUMN11,0)) AS Balance,null OpeningBal,'Cash' [Type1],'22409' [Type2],p.COLUMNA02 OP ,(select column03 from contable007 where column02=p.COLUMNA02) as OU,pr.COLUMN05 Project 
--					 From	FITABLE052 p 
--left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 and s.COLUMNA03=p.COLUMNA03  and s.COLUMN07='22409' 
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN16  and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
--left outer join FITABLE031 j on j.COLUMN01=p.COLUMN09 and j.COLUMN04=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0
----left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in(fj.COLUMN03)) --AND (fj.COLUMN04 = p.COLUMN11 OR fj.COLUMN05 = p.COLUMN12) 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN08,0)
--left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN05 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
--left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN05 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
--left outer join SATABLE001 a on a.COLUMN02=p.COLUMN08 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
--left outer join SATABLE002 b on b.COLUMN02=p.COLUMN08  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
--left outer join MATABLE010 m on m.COLUMN02=p.COLUMN08  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
--left outer join FITABLE022 f22 on f22.COLUMN09=pv.COLUMN01 and f22.COLUMNA03=pv.COLUMNA03 and f22.COLUMNA02=pv.COLUMNA02 and isnull(f22.COLUMNA13,0)=0 and isnull(f22.COLUMN05,0)=isnull(p.COLUMN11,0)
--left outer join FITABLE024 f24 on f24.COLUMN09=rv.COLUMN01 and f24.COLUMNA03=rv.COLUMNA03  and f24.COLUMNA02=rv.COLUMNA02 and isnull(f24.COLUMNA13,0)=0 and isnull(f24.COLUMN05,0)=isnull(p.COLUMN12,0)
--left outer join FITABLE012 f12 on f12.COLUMN17=p.COLUMN05 and f12.COLUMNA03=p.COLUMNA03 and f12.COLUMNA02=p.COLUMNA02 and isnull(f12.COLUMNA13,0)=0
--left outer join FITABLE021 f21 on f21.COLUMN06=f12.COLUMN01 and f21.COLUMNA03=f12.COLUMNA03 and f21.COLUMNA02=f12.COLUMNA02 and isnull(f21.COLUMNA13,0)=0
--left outer join FITABLE001 pv1 on pv1.COLUMN02=f22.COLUMN03 and pv1.COLUMNA03=f22.COLUMNA03 and isnull(pv1.columna13,0)=0
--left outer join FITABLE001 jv1 on jv1.COLUMN02=fj.COLUMN03 and jv1.COLUMNA03=fj.COLUMNA03 and isnull(jv1.columna13,0)=0
--left outer join FITABLE001 rv1 on rv1.COLUMN02=f24.COLUMN03 and rv1.COLUMNA03=f24.COLUMNA03 and isnull(rv1.columna13,0)=0
--left outer join FITABLE001 ex1 on ex1.COLUMN02=f21.COLUMN03 and ex1.COLUMNA03=f21.COLUMNA03 and isnull(ex1.columna13,0)=0
--where isnull((p.COLUMNA13),0)=0 and (P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or P.COLUMNA02 is null) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
-- union all
--					SELECT p.COLUMN03 as ID,s.[COLUMN04] AS Account,null as [Date] ,NULL Dtt,'Opening Bal' Type,
--	null  Number,null Name,null Accounts,null  Memo,null  [Debit],null [Credit],null [Balance],
--	( sum(isnull(p.COLUMN12,0)-isnull(p.COLUMN11,0))) OpeningBal,'Cash' [Type1],'22409' [Type2],p.COLUMNA02 OP ,
--	(select column03 from contable007 where column02=p.COLUMNA02) as OU ,null Project
--					 From	FITABLE052 p 
--					left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 and s.COLUMNA03=p.COLUMNA03 and s.COLUMN07='22409' 
--					where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by s.column04,p.COLUMNA02,p.COLUMN03
-- union all
--Inventory Asset
SELECT p.COLUMN03 as ID,'Inventory Asset' AS Account,FORMAT(p.COLUMN04,@DateF) AS 'Date',p.COLUMN04 Dtt,p.COLUMN06 Type,
     (CASE  WHEN p.COLUMN06='Item Issue' or p.COLUMN06='JobOrder Issue' then isnull((select column04 from SATABLE007 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='Resource Consumption' then  isnull((select column04 from PRTABLE007 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='INVOICE' then  isnull((select column04 from SATABLE009 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='Inventory Adjustment' then  isnull((select column04 from FITABLE014 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='Item Receipt' then  isnull((select column04 from PUTABLE003 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='Work Order' then  isnull((select column04 from PRTABLE005 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='BILL' then  isnull((select column04 from PUTABLE005 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='BILL' then  isnull((select column04 from PUTABLE005 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='Return Receipt' then  isnull((select column04 from PUTABLE003 where COLUMN01=p.COLUMN05),'')
      when p.COLUMN06='JobOrder Receipt' then  isnull((select column04 from PUTABLE003 where COLUMN01=p.COLUMN05),'')
	  when p.COLUMN06='Credit Memo' then isnull((select column04 from SATABLE005 where COLUMN01=(p.COLUMN05)),'')
	  when p.COLUMN06='Debit Memo' then isnull((select column04 from PUTABLE001 where COLUMN01=(p.COLUMN05)),'')
	  when p.COLUMN06='JOURNAL ENTRY' then isnull((select column04 from FITABLE031 where COLUMN01=(p.COLUMN05)),'')
	  when p.COLUMN06='RECEIPT VOUCHER' then isnull((rv.column04),'')
      when p.COLUMN06='PAYMENT VOUCHER' then  isnull((pv.column04),'')
      when p.COLUMN06='Return Issue' then  isnull((select column04 from SATABLE007 where COLUMN01=p.COLUMN05),'') when p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE' then isnull(f12.COLUMN17,'') else '' end) Number,
(CASE  WHEN p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then m.COLUMN09 when j.column07='22492' or fj.column14='22492' then m.COLUMN09 else '' end)
 WHEN (p.COLUMN06='PAYMENT VOUCHER' or p.COLUMN06='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.COLUMN11=22700 or pv.COLUMN11=22334 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then m.COLUMN09 else '' end)
 WHEN (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 or rv.column20=22334 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then m.COLUMN09 else '' end)
 WHEN (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Service' or p.COLUMN06='Debit Memo' or p.COLUMN06='Item Receipt' or p.COLUMN06='Return Issue' or p.COLUMN06='JobOrder Issue' or p.COLUMN06='JobOrder Receipt') then a.COLUMN05
 WHEN (p.COLUMN06='INVOICE' or p.COLUMN06='PAYMENT' or p.COLUMN06='Credit Memo' or p.COLUMN06='Item Issue' or p.COLUMN06='Return Receipt') then b.COLUMN05
 WHEN (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then (select COLUMN06 from matable010 where column02=p.COLUMN07)
else '' end) Name,'' Accounts,
p.COLUMN09 AS Memo,p.COLUMN10 AS [Debit],p.COLUMN11 AS [Credit],
((isnull(p.COLUMN10,0))  
-(isnull(p.COLUMN11,0))) AS [Balance],0 OpeningBal,'Inventory Asset' [Type1],'22262' [Type2],p.COLUMNA02 OP ,CON07.COLUMN03 as OU ,pr.COLUMN05 Project
From PUTABLE017 p 
left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02 and e.COLUMNA03=@AcOwner and e.COLUMN07='22262'
--left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMNA03=@AcOwner and isnull(j.COLUMNA13,0)=0
left outer join FITABLE031 j on  j.COLUMN01=p.COLUMN05 and j.COLUMNA03=@AcOwner and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0 
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22262 AND  (fj.COLUMN04 = p.COLUMN10 OR fj.COLUMN05 = p.COLUMN11) 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN08,0)
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22262 
--AND  (fj.COLUMN04 = p.COLUMN11 OR fj.COLUMN05 = p.COLUMN10)
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
left outer join FITABLE020 pv on CAST(pv.COLUMN01 AS NVARCHAR(250))= CAST(p.COLUMN05 AS NVARCHAR(250))  and pv.COLUMNA03=@AcOwner and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
left outer join FITABLE023 rv on CAST(rv.COLUMN01 AS NVARCHAR(250)) = CAST(p.COLUMN05 AS NVARCHAR(250)) and rv.COLUMNA03=@AcOwner and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
left outer join FITABLE012 F12 on CAST(F12.COLUMN01 AS NVARCHAR(250)) = CAST(p.COLUMN05 AS NVARCHAR(250)) and F12.COLUMNA03=@AcOwner and F12.COLUMNA02=p.COLUMNA02 and isnull(F12.COLUMNA13,0)=0
left outer join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=@AcOwner and isnull(a.COLUMNA13,0)=0
left outer join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and  b.COLUMNA03=@AcOwner and isnull(b.COLUMNA13,0)=0
left outer join MATABLE010 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=@AcOwner and isnull(m.COLUMNA13,0)=0
 left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN14  and pr.COLUMNA03=@AcOwner and isnull(pr.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 BETWEEN @FrDate AND @ToDate
 union all
SELECT p.COLUMN03 as ID,'Inventory Asset' AS Account,null as [Date] ,NULL Dtt,'Opening Bal' Type,null  Number,
null Name,null Accounts,null  Memo,0  [Debit],0 [Credit],( sum(isnull(p.COLUMN10,0)-isnull(p.COLUMN11,0))) [Balance],( sum(isnull(p.COLUMN10,0)-isnull(p.COLUMN11,0))) OpeningBal,'Inventory Asset' [Type1],'22262' [Type2],p.COLUMNA02 OP ,(CON07.column03) as OU,null Project 
From PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02 and e.COLUMNA03=@AcOwner and e.COLUMN07='22262'
where isnull((p.COLUMNA13),0)=0  and P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner AND p.COLUMN04 < @FrDate group by p.COLUMNA02,p.COLUMN03,(CON07.column03)
--non current asset
--union all
--select p.COLUMN10 as ID,
--m.COLUMN04 'Account',
--FORMAT(p.[COLUMN04],@DateF) as Date,
--p.COLUMN04 Dtt,
--p.COLUMN03 Type,
--(case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN09,'') end) 'Number',
--(case when (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT') then sa.column05  when p.COLUMN03='PAYMENT' then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 or v.COLUMN11=22334 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when (p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='PDC RECEIVED' or p.COLUMN03='Sales Claim Register') then s2.COLUMN05 
--when p.COLUMN03='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.column20=22700 or v1.column20=22334 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end)
--when p.COLUMN03='EXPENSE' then ma.COLUMN09 else (sa.column05)  end)  Name,'' Accounts,
--(case when p.COLUMN03='Opening Balance' then '' else p.COLUMN05 end) 'Memo',
--p.COLUMN07 'Credit',p.COLUMN08 'Debit',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as Balance,null OpeningBal,'Non Current Assets' [Type1],'22410' [Type2],p.COLUMNA02 OP  ,o.COLUMN03 as OU,pr.COLUMN05 Project
----o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,p.COLUMN09 Reference#,p.COLUMN07 'Increased Amount',
----p.COLUMN08 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,(case when isnull(p.COLUMNA08,0)>0  then cast(isnull(ma2.COLUMN09,0)as nvarchar(250)) else ma1.COLUMN09 end) 'Created By' 
--from FITABLE060 p 
--left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11 
--left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 and pr.COLUMNA03=p.COLUMNA03 AND ISNULL(pr.COLUMNA13,0)=0
--left outer join CONTABLE007 o on o.column02=p.columna02 AND ISNULL(o.COLUMNA13,0)=0
-- left outer join conTABLe002 cn on cn.column02=p.columna03 
--left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and (v.COLUMNA02=p.COLUMNA02 OR v.COLUMNA02 is null) and isnull(v.COLUMNA13,0)=0  
--left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
--left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
--left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
--left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02  
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03))  
----AND (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) 
--AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)  
--left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
--left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
--left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
--left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
--left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
--left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0  
--left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
--left outer join PUTABLE014 p4 on p4.COLUMN04=p.column09 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03  and isnull(p4.COLUMNA13,0)=0  
-- inner join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07 in ('22410')
--left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08,p4.columna08)  
--left outer join  MATABLE010 ma2 on ma2.COLUMN02 = p.COLUMNA08 and ma2.COLUMNA03 = p.COLUMNA03
--where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FrDate and @ToDate --AND p.COLUMN10=@ide 
--AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner  
----AND  isnull(m2.COLUMN02,'')=iif(isnull(@Tds,'')='','',@Tds) order by RNO desc 
--UNION ALL
--select p.COLUMN10 as ID,
--m.COLUMN04 'Account',
--FORMAT(p.[COLUMN04],@DateF) as Date,
--p.COLUMN04 Dtt,
--p.COLUMN03 Type,
--(case when p.COLUMN03='Opening Balance' then '' else isnull(p.COLUMN09,'') end) 'Number',
--(case when (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT') then sa.column05  when p.COLUMN03='PAYMENT' then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 or v.COLUMN11=22334 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when (p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='PDC RECEIVED' or p.COLUMN03='Sales Claim Register') then s2.COLUMN05 
--when p.COLUMN03='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.column20=22700 or v1.column20=22334 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end)
--when p.COLUMN03='EXPENSE' then ma.COLUMN09 else (sa.column05) end)  Name, '' Accounts,
--(case when p.COLUMN03='Opening Balance' then '' else p.COLUMN05 end) 'Memo',
--p.COLUMN07 'Credit',
--p.COLUMN08 'Debit',
--(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) Balance,
--null OpeningBal,
--'Non Current Liabilities' [Type1],
--'22411' [Type2],
--p.COLUMNA02 AS  OP  ,
--o.COLUMN03 AS OU,
--pr.COLUMN05 as Project 
--from FITABLE063 p 
--left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11 left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 
--left outer join CONTABLE007 o on o.column02=p.columna02 left outer join conTABLe002 cn on cn.column02=p.columna03 
--left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and (v.COLUMNA02=p.COLUMNA02 OR v.COLUMNA02 is null) and isnull(v.COLUMNA13,0)=0  
--left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
--left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
--left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
--left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02  
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) AND (((p.COLUMN07) in (fj.COLUMN05)) or ((p.COLUMN08) in (fj.COLUMN04))) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
--left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
--left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
--left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
--left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
--left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
--left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0  
--left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
--left outer join PUTABLE014 p4 on p4.COLUMN04=p.column09 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03  and isnull(p4.COLUMNA13,0)=0 
--inner join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and m.COLUMN07 in ('22411') 
--left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08,p4.columna08)
--left outer join  MATABLE010 ma2 on ma2.COLUMN02 = p.COLUMNA08 and ma2.COLUMNA03 = p.COLUMNA03  
--where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FrDate and @ToDate --AND p.COLUMN10=@ide 
--AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner  
--AND  isnull(m2.COLUMN02,'')=iif(isnull(@Tds,'')='','',@Tds) order by RNO desc
union all

select f64.column11 ID,f1.column04 Account,FORMAT(f64.COLUMN07,@DateF) Date,f64.COLUMN07 Dtt,
f64.column03 Type,f64.column04 Number,
(case 
	when f64.column08 in('22492','22306') then m10.column09
	when f64.column08 in('22334','22654','22700','22305') then s01.column05
	when f64.column08 in('22335') then s02.column05 else '' end )Name,''Accounts,
f64.column12 Memo,cast(ISNULL(f64.column15,0) as decimal(18,2)) Debit,cast(ISNULL(f64.column14,0) as decimal(18,2)) Credit,
IIF(f64.column10 IN(22264,22266,22409,22344,22408,22410,22403,22402) and f64.column11 != 1078,
cast((ISNULL(f64.column15,0)-ISNULL(f64.column14,0))as decimal(18,2)),
cast((ISNULL(f64.column14,0)-ISNULL(f64.column15,0))as decimal(18,2)))Balance,
0 OpeningBal,m2.column04 Type1,f64.COLUMN10 Type2,f64.columna02 OP,c7.column03 OU,pr.COLUMN05 Project
  from fitable064 f64
inner join fitable001 f1 on f1.column02 = f64.column11 and f1.columna03 = @AcOwner and f1.columna13 = '0'
left join matable010 m10 on m10.column02 = f64.column09 and m10.columna03 = @AcOwner and f64.column08 in('22492','22306')
left join satable001 s01 on s01.column02 = f64.column09 and s01.columna03 = @AcOwner and f64.column08 in('22334','22654','22700','22305')
left join satable002 s02 on s02.column02 = f64.column09 and s02.columna03 = @AcOwner and f64.column08 in('22335')
left join contable007 c7 on c7.column02 = f64.columnA02 and c7.columna03 = @AcOwner 
left join matable002 m2 on m2.column02 = f64.column10 
left join PRTABLE001 pr on pr.COLUMN02= f64.COLUMN13  and pr.COLUMNA03=@AcOwner and isnull(pr.COLUMNA13,0)=0
where f64.columna03 = @AcOwner and f64.columna02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and
f64.column07 between @FrDate and @ToDate and  isnull(f64.columna13,0) = 0
union all 

select f64.column11 ID,f1.column04 Account,NULL Date,NULL Dtt,'Opening Bal' Type,NULL Number,
NULL Name,NULL Accounts,NULL Memo,0 Debit,0 Credit,
IIF(f64.column10 IN(22264,22266,22409,22344,22408,22410,22403,22402) and f64.column11 != 1078,
cast(SUM((ISNULL(f64.column15,0)-ISNULL(f64.column14,0)))as decimal(18,2)),
cast(SUM(ISNULL(f64.column14,0)-ISNULL(f64.column15,0))as decimal(18,2)))Balance,
IIF(f64.column10 IN(22264,22266,22409,22344,22408,22410,22403,22402) and f64.column11 != 1078,
cast(SUM((ISNULL(f64.column15,0)-ISNULL(f64.column14,0)))as decimal(18,2)),
cast(SUM(ISNULL(f64.column14,0)-ISNULL(f64.column15,0))as decimal(18,2))) OpeningBal,m2.column04 Type1,f64.COLUMN10 Type2,f64.columna02 OP,c7.column03 OU,NULL Project
  from fitable064 f64
inner join fitable001 f1 on f1.column02 = f64.column11 and f1.columna03 = @AcOwner and f1.columna13 = '0'
--left join matable010 m10 on m10.column02 = f64.column08 and m10.columna03 = @AcOwner and f64.column08 in('22492','22306')
--left join satable001 s01 on s01.column02 = f64.column08 and s01.columna03 = @AcOwner and f64.column08 in('22334','22654','22700','22305')
--left join satable002 s02 on s02.column02 = f64.column08 and s02.columna03 = @AcOwner and f64.column08 in('22335')
left join contable007 c7 on c7.column02 = f64.columnA02 and c7.columna03 = @AcOwner 
left join matable002 m2 on m2.column02 = f64.column10 
--left join PRTABLE001 pr on pr.COLUMN02= f64.COLUMN13  and pr.COLUMNA03=@AcOwner and isnull(pr.COLUMNA13,0)=0
where f64.columna03 = @AcOwner and f64.columna02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and
f64.column07 < @FrDate  and  isnull(f64.columna13,0) = 0-- and '2018-02-07'
group by f64.column11,f1.column04,m2.column04,f64.COLUMN10,f64.columna02,c7.column03
) Query
--select * from #GeneralLedger
set @Query1='select ID, Account, Date, Dtt,  Type,  Number,
  Name,  Accounts,  Memo,  Debit,  Credit, 
  (SUM(isnull(Balance,0)) OVER(PARTITION BY ID,OP ORDER BY Dtt   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	) Balance,
 OpeningBal,Type1,Type2,OP,OU,Project from #GeneralLedger'+@whereStr+' order by Dtt desc'
exec (@Query1) 
END















GO
