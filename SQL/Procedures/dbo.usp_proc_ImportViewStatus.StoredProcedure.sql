USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_proc_ImportViewStatus]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_proc_ImportViewStatus]
(
	@FromDate nvarchar(250)= null,
	@ToDate nvarchar(250)= null,
	@DateF nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@TYPE nvarchar(250)= null,
	@OPUnit nvarchar(250)= null
	)
as 
begin
			
--IF (@TYPE='UPDATE')
--BEGIN

--select m46.[Column16] AS 'Price Engine#',s.COLUMN04 AS [Brand],a.COLUMN04 AS [Item],i.COLUMN04 AS [Velocity],m46.COLUMN07 AS [Purchase Price],m46.COLUMN08 AS [MRP],p.COLUMN04 AS [Price Tag],m46.COLUMN10 AS [Sale Price],m46.COLUMN11 AS [CLP],m46.COLUMN12 AS [PLP],O.COLUMN03 [Operating Unit],FORMAT(m46.[COLUMN14],@DateFormat) AS [Effective Start Date],FORMAT(m46.[COLUMN15],@DateFormat) AS [Effective End Date]  from MATABLE046 m46
--left join MATABLE005 s ON m46.COLUMN04=s.COLUMN02 and m46.COLUMNA03=s.COLUMNA03 and isnull (s.COLUMNA13,0)=0
--LEFT JOIN CONTABLE007 O on m46.COLUMN13=O.COLUMN02 and m46.COLUMNA03=O.COLUMNA03 and isnull (O.COLUMNA13,0)=0
--LEFT JOIN MATABLE007 a on m46.COLUMN05=a.COLUMN02 and m46.COLUMNA03=a.COLUMNA03 and isnull (a.COLUMNA13,0)=0
--LEFT JOIN MATABLE045 p on m46.COLUMN09=p.COLUMN02 and m46.COLUMNA03=p.COLUMNA03 and isnull (p.COLUMNA13,0)=0
--left join MATABLE002 i on m46.COLUMN06=i.COLUMN02 and i.COLUMN03=11234 and (m46.COLUMNA03=i.COLUMNA03 or isnull (i.COLUMNA03,0)=0) and isnull (i.COLUMNA13,0)=0
--where m46.column03 = 1684 and m46.columna03 =@AcOwner and isnull(m46.COLUMNA13,0)=0 and m46.COLUMN04!=''
--END
--ELSE
BEGIN
select format(m.COLUMNA06,'') 'Date',c.COLUMN04+ '_ '+m.COLUMN06 'JobName',iif(count(p.COLUMN02)=s.totcnt,'Complete','Inprogress') Status,
cast(cast(iif((s.totcnt)!=0,(80/cast(s.totcnt as decimal(18,2))*cast(s.succcnt as decimal(18,2)))+20,0) as decimal(18,2)) as nvarchar(250))+'%' 'PresentComplete',
s.recsuccmsg	'Message',''	'CsvResponse',''	'Queue',''	'Cancel', m.COLUMN02 'id',s.totcnt-s.succcnt 'failcnt'  from MATABLE034 m
	inner join INTFMGR_ITEMMASTER p on p.COLUMN18=m.COLUMN02 and p.ColumnA03=m.ColumnA03  and isnull(p.ColumnA13,0)=0
	left join CONTABLE0010 c on m.COLUMN05=c.COLUMN02
	
	left join (select cast(isnull(e.Status,'0') as nvarchar(25)) +' of '+cast(count(1) as nvarchar(25)) +' records sucessfully completed' 'recsuccmsg',isnull(e.Status,'0') 'succcnt',(count(1)) 'totcnt',p.COLUMN18,p.ColumnA03 from INTFMGR_ITEMMASTER p
left join(select count(Status)Status,COLUMN18,columnA03 from INTFMGR_ITEMMASTER where Status = 'y' and isnull(columna13,0)=0 group by COLUMN18,columnA03) e on p.column18 = e.column18 and e.ColumnA03=p.ColumnA03 group by e.Status,p.COLUMN18,p.ColumnA03) as s on s.COLUMN18 =m.COLUMN02 and s.ColumnA03=m.ColumnA03
	where  m.ColumnA03=@AcOwner  and isnull(m.ColumnA13,0)=0 --and cast(m.COLUMNA06 as date) between @FromDate and @ToDate
	and cast(cast(iif((s.totcnt)!=0,(80/cast(s.totcnt as decimal(18,2))*cast(s.succcnt as decimal(18,2)))+20,0) as decimal(18,2)) as nvarchar(250)) is not null
	group by m.COLUMN02,m.COLUMN02,m.COLUMNA06,c.COLUMN04,s.recsuccmsg,s.totcnt,s.succcnt,m.COLUMN06 order by m.COLUMNA06 desc
	--iif(count(p.COLUMN02)=e.cnt,'Complete','Inprogress')
	END
	End




GO
