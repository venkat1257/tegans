USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ARAgingDetailsREPORTS]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROC [dbo].[ARAgingDetailsREPORTS]
(
	@ReportName  nvarchar(250)= null,
	@FrDate    nvarchar(250)= null,
	@ToDate      nvarchar(250)= null,
	@Customer      nvarchar(250)= null,
	@Brand      nvarchar(250)= null,
	@OPUnit nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@SalesRep nvarchar(250)= null,
	@Operating      nvarchar(250)= null,
	@ProjectName      nvarchar(250)= null,
	@DateF nvarchar(250)=null
)
AS
	BEGIN
declare @whereStr nvarchar(max)=null, @Query1 nvarchar(max)=null

if @SalesRep!=''
begin
set @whereStr=' where SalesRepc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SalesRep+''') s)'
end
if(@Operating!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OPUID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OPUID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)) '
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)) '
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1  '
end
Create Table #ARAgingTable (
COLUMN07 varchar(500),
[CURRENT] decimal(18,2),
[1-30] decimal(18,2),
[31-60] decimal(18,2),
[61-90] decimal(18,2),
[91-180] decimal(18,2),
[181-360] decimal(18,2),
[361-520] decimal(18,2),
[520 and Over] decimal(18,2),
RETUNNS decimal(18,2),RowIds int Identity(1,1),
CITY nvarchar(250),
PNNO nvarchar(250),
COLUMN04 nvarchar(250),
COLUMN05 nvarchar(250),
TOTALAMOUNT decimal(18,2),
[DAY] decimal(18,2),
SALESREP nvarchar(250),
SALESREPC nvarchar(250),
Dtt nvarchar(250),
OPUID nvarchar(250),
Brand nvarchar(250),
BrandID nvarchar(250)
)
declare @cntARAging int
declare @flgARAging int
declare @CustomerName varchar(500),
@CURRENT decimal(18,2),
@1_30 decimal(18,2),
@31_60 decimal(18,2),
@61_90 decimal(18,2),
@91_180 decimal(18,2),
@181_360 decimal(18,2),
@361_520 decimal(18,2),
@520 decimal(18,2),
@RETUNNS decimal(18,2),
@CITY nvarchar(250),
@PNNO  nvarchar(250),@CreditBal decimal(18,2),
@COLUMN04 nvarchar(250),
@Dtt nvarchar(250),
@COLUMN05 nvarchar(250),
@TOTALAMOUNT decimal(18,2),
@DAY decimal(18,2),
@CSALESREP nvarchar(250),
@CSALESREPC nvarchar(250),
@COPUID nvarchar(250),
@BrandName nvarchar(250),
@BrandID nvarchar(250)
select * into #AGINGSUMMARY from (
select a.COLUMN07,sum(a.[CURRENT]) [CURRENT],sum(a.[1-30]) [1-30],sum(a.[31-60]) [31-60],sum(a.[61-90]) [61-90],
sum(a.[91-180]) [91-180],sum(a.[181-360]) [181-360],sum(a.[361-520]) [361-520],sum(a.[520 and Over]) [520 and Over],sum(a.[RETUNNS]) [RETUNNS] ,
a.CITY,a.PNNO,a.COLUMN04,a.COLUMN05,sum(a.TOTALAMOUNT)TOTALAMOUNT,sum(a.[DAY])[DAY],a.SalesRep,a.SalesRepc,format(a.dtt,@DateF) dtt,a.opuid,a.Brand,a.BrandID
from (
SELECT  COLUMN07,CITY,PNNO,COLUMN04,COLUMN05,sum(isnull(TOTALAMOUNT,0))TOTALAMOUNT,sum(a.[DAY])[DAY],SalesRep,SalesRepc,(CASE WHEN  ((cast(isnull([DAY],0) as int)) =0 and cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)[CURRENT],
					(CASE WHEN ((cast(isnull([DAY],0) as int) <= 30 and (cast(isnull([DAY],0) as int))>=1) and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end) [1-30],
					(CASE WHEN  ((cast(isnull([DAY],0) as int)) <= 60 and cast(isnull([DAY],0) as int) >=31and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)  [31-60],
					(CASE WHEN ((cast(isnull([DAY],0) as int))<= 90 and cast(isnull([DAY],0) as int)>=61 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)  [61-90],
					(CASE WHEN ((cast(isnull([DAY],0) as int))<= 180 and(cast(isnull([DAY],0) as int))>= 91 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)  [91-180],
					(CASE WHEN  ((cast(isnull([DAY],0) as int))<= 360 and(cast(isnull([DAY],0) as int))>= 181 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)  [181-360],
					(CASE WHEN  ((cast(isnull([DAY],0) as int))<= 520 and (cast(isnull([DAY],0) as int))>=361 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)  [361-520],
					(CASE WHEN  ((cast([DAY] as int)) >=521 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(DUEAMOUNT,0)) ))end)  [520 and Over],
					(CASE WHEN  ((cast([DAY] as int)) =0 and  cast([type] as varchar(250))='Credit' )
						then ( (sum(isnull(RETUNNS,0)) ))end)  [RETUNNS],dtt,opuid,Brand,BrandID
					FROM(
SELECT SATABLE002.COLUMN05 COLUMN07,SATABLE002.COLUMN11  AS PNNO, 
(s3.COLUMN10) AS CITY,'' [type], 
DATEDIFF(DAY,SATABLE009.COLUMN08,GETDATE()) [DAY],SATABLE009.COLUMN08 COLUMN04, SATABLE009.COLUMN04 COLUMN05, PUTABLE018.COLUMN09 as TOTALAMOUNT,
(isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-isnull(sum(SATABLE012.COLUMN09),0)-
isnull((SATABLE005.COLUMN15),0)) as DUEAMOUNT,NULL RETUNNS,MATABLE010.COLUMN06 SalesRep, SATABLE002.COLUMN22 SalesRepc,SATABLE009.COLUMN08 dtt,PUTABLE018.COLUMNA02 opuid,m5.COLUMN04 Brand,SATABLE002.COLUMN37 BrandID FROM PUTABLE018 
inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner 
--AND SATABLE002.COLUMN22= @SalesRep  
and isnull(SATABLE002.COLUMNA13,0)=0
left join MATABLE005 m5   on m5.COLUMN02=SATABLE002.COLUMN37 and SATABLE002.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 left join MATABLE010 on MATABLE010.COLUMN02=SATABLE002.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1 AND ISNULL(MATABLE010.COLUMNA13,0)=0
 --AND MATABLE010.COLUMNA02 =SATABLE002.COLUMNA02
left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and SATABLE009.COLUMN05=PUTABLE018.COLUMN07 and  SATABLE009.COLUMNA03=@AcOwner and  SATABLE009.COLUMNA02=PUTABLE018.COLUMNA02
and isnull(SATABLE009.COLUMNA13,0)=0
left join SATABLE012 on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=PUTABLE018.COLUMNA03  and  SATABLE012.COLUMNA02=PUTABLE018.COLUMNA02
and isnull(SATABLE012.COLUMNA13,0)=0 and isnull(SATABLE012.COLUMN17,'')=''--and SATABLE012.COLUMN17 not in('DebitMemo','Journal','PaymentVoucher','AdvancePayment')
left join SATABLE005 on SATABLE005.COLUMN05=PUTABLE018.COLUMN07 and  SATABLE005.COLUMN03=''  and  SATABLE005.COLUMNA03=@AcOwner  and  SATABLE005.COLUMNA02=PUTABLE018.COLUMNA02
and isnull(SATABLE005.COLUMNA13,0)=0
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = SATABLE002.COLUMN16 and isnull(s3.COLUMNA13,0)=0and  s3.COLUMNA03=@AcOwner
WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Invoice','Payment')
AND PUTABLE018.COLUMNA03=@AcOwner and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
group by PUTABLE018.COLUMN09,SATABLE005.COLUMN15,SATABLE002.COLUMN05,SATABLE002.COLUMN11,SATABLE002.COLUMN16,SATABLE009.COLUMN08,SATABLE009.COLUMN04,MATABLE010.COLUMN06,SATABLE002.COLUMN22,PUTABLE018.COLUMNA02,m5.COLUMN04,SATABLE002.COLUMN37,SATABLE002.COLUMN05,s3.column10--PUTABLE018.COLUMN07,PUTABLE018.COLUMN11
having ((isnull((PUTABLE018.COLUMN09),0)-isnull(sum(SATABLE012.COLUMN06),0)-isnull(sum(SATABLE012.COLUMN13),0)-
isnull((SATABLE005.COLUMN15),0)))!=0
union all
SELECT 
--(SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02=PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)) COLUMN07, 
--(SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)) AS PNNO, 
c.COLUMN05 COLUMN07,c.COLUMN11 PNNO, 
(S3.COLUMN10) AS CITY,
'Credit' [type],0 [DAY],null COLUMN04,null COLUMN05, NULL TOTALAMOUNT, NULL DUEAMOUNT,
cast(-isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))+cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) RETUNNS
,null SalesRep , C.COLUMN22 SalesRepc,NULL dtt,PUTABLE018.columna02 opuid,m5.COLUMN04 Brand,c.COLUMN37 BrandID

--,NULL [1-30],NULL [31-60],NULL [61-90],NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
FROM PUTABLE018 PUTABLE018
left join SATABLE002 c on c.COLUMN02=PUTABLE018.COLUMN07 and c.COLUMNA03=PUTABLE018.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = c.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Credit Memo','Debit Memo','RECEIVE PAYMENT','PAYMENT VOUCHER')
--AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',56700) s) 
AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) --and isnull(PUTABLE018.COLUMN09,0) !>'0'
--group by PUTABLE018.COLUMN07,PUTABLE018.COLUMN04,PUTABLE018.COLUMN11,PUTABLE018.COLUMN14,PUTABLE018.COLUMN09,PUTABLE018.COLUMN02,PUTABLE018.COLUMNA02,m5.COLUMN04,c.COLUMN37
union all
SELECT 
--(SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02=PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)) COLUMN07, 
--(SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = PUTABLE018.COLUMN07  and COLUMNA03=@AcOwner AND (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)) AS PNNO, 
c.COLUMN05 COLUMN07,c.COLUMN11 PNNO, 
(S3.COLUMN10) AS CITY,
'Credit' [type],0 [DAY],null COLUMN04,null COLUMN05, NULL TOTALAMOUNT, NULL DUEAMOUNT,
cast(-isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))+cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) RETUNNS
,null SalesRep , C.COLUMN22 SalesRepc,NULL dtt,PUTABLE018.columna02 opuid,m5.COLUMN04 Brand,c.COLUMN37 BrandID

--,NULL [1-30],NULL [31-60],NULL [61-90],NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
FROM PUTABLE018 PUTABLE018
left join SATABLE002 c on c.COLUMN02=PUTABLE018.COLUMN07 and c.COLUMNA03=PUTABLE018.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = c.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('JOURNAL ENTRY')
--AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',56700) s) 
AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(PUTABLE018.COLUMN09,0) !>'0'

-- opening Balance
union all
SELECT 
c.COLUMN05 COLUMN07,c.COLUMN11 PNNO,
(S3.COLUMN10) AS CITY,
'' [type],DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],PUTABLE018.COLUMN04 COLUMN04,FITABLE049.COLUMN04 COLUMN05,ISNULL(PUTABLE018.COLUMN09,0)-ISNULL(PUTABLE018.COLUMN11,0) as TOTALAMOUNT, ISNULL(PUTABLE018.COLUMN09,0)-ISNULL(PUTABLE018.COLUMN11,0) DUEAMOUNT,
0 RETUNNS
,null SalesRep , C.COLUMN22 SalesRepc,PUTABLE018.COLUMN04 dtt,PUTABLE018.columna02 opuid,m5.COLUMN04 Brand,c.COLUMN37 BrandID
FROM PUTABLE018 PUTABLE018
left join SATABLE002 c on c.COLUMN02=PUTABLE018.COLUMN07 and c.COLUMNA03=PUTABLE018.COLUMNA03 and isnull(c.COLUMNA13,0)=0
left join MATABLE005 m5 on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
left join FITABLE049 FITABLE049 on FITABLE049.COLUMN04=PUTABLE018.COLUMN05 and FITABLE049.COLUMNA03=PUTABLE018.COLUMNA03 and isnull(FITABLE049.COLUMNA13,0)=0
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = c.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
WHERE isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('Opening Balance') AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
--advance
union all
SELECT A.COLUMN07,A.PNNO,A.CITY,A.[type] ,A.[DAY],A.COLUMN04,A.COLUMN05,A.TOTALAMOUNT,A.DUEAMOUNT,SUM(A.RETUNNS) RETUNNS,A.SalesRep,A.SalesRepc,A.dtt,A.opuid,A.Brand,A.BrandID FROM(
SELECT (SATABLE002.COLUMN05) COLUMN07,(SATABLE002.COLUMN11) PNNO,(S3.COLUMN10) AS CITY,
'Credit' [type],0 [DAY],null COLUMN04,null COLUMN05, NULL TOTALAMOUNT, NULL DUEAMOUNT,
cast(isnull(FITABLE026.COLUMN19,0) as decimal(18,2)) RETUNNS,null SalesRep , SATABLE002.COLUMN22 SalesRepc,NULL dtt,FITABLE026.columna02 opuid,m5.COLUMN04 Brand,SATABLE002.COLUMN37 BrandID
,FITABLE026.COLUMN02 TRANS
  FROM FITABLE023 FITABLE026 
inner join fiTABLE024 on fiTABLE024.COLUMN09=FITABLE026.COLUMN01 and  fiTABLE024.COLUMNA03=@AcOwner and isnull(fiTABLE024.COLUMNA13,0)=0 and (fiTABLE024.COLUMN03 in(3000,5248) or fiTABLE024.COLUMN03 is null)
inner join SATABLE002 on SATABLE002.COLUMN02=FITABLE026.COLUMN08 and  SATABLE002.COLUMNA03=FITABLE026.COLUMNA03 and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
left join MATABLE005 m5   on m5.COLUMN02=SATABLE002.COLUMN37 and SATABLE002.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 --left join MATABLE010 on MATABLE010.COLUMN02=SATABLE002.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1
--left join (select sum(isnull(p.column09,0)) column09,p.COLUMNA03,r.COLUMN08 COLUMN05,r.COLUMN02 from SATABLE012 p inner join FITABLE023 r on p.COLUMN15=r.COLUMN02 and 
-- p.COLUMNA02=r.COLUMNA02 and  p.COLUMNA03=r.COLUMNA03 and isnull(r.COLUMNA13,0)=0
-- where p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0
--group by p.COLUMNA03,r.COLUMN08,r.COLUMN02) CreditMemo
--on CreditMemo.COLUMN05=FITABLE026.COLUMN06 and CreditMemo.COLUMNA03=FITABLE026.COLUMNA03
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = SATABLE002.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
WHERE  isnull(FITABLE026.COLUMNA13,0)=0  and FITABLE026.COLUMN05 <=getdate()
and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner
and isnull(FITABLE026.COLUMNA13,0)=0 
group by SATABLE002.COLUMN05,SATABLE002.COLUMN11,SATABLE002.COLUMN16,FITABLE026.COLUMNA02,m5.COLUMN04,SATABLE002.COLUMN37,SATABLE002.COLUMN22,s3.column10,FITABLE026.COLUMN19,FITABLE026.COLUMN02
) A
GROUP BY A.COLUMN07,A.PNNO,A.CITY,A.[type] ,A.[DAY],A.COLUMN04,A.COLUMN05,A.TOTALAMOUNT,A.DUEAMOUNT,A.SalesRep,A.SalesRepc,A.dtt,A.opuid,A.Brand,A.BrandID 
--Debit Payment
union all
SELECT (SATABLE002.COLUMN05) COLUMN07,(SATABLE002.COLUMN11) PNNO,(S3.COLUMN10) AS CITY,
'Credit' [type],0 [DAY],null COLUMN04,null COLUMN05, NULL TOTALAMOUNT, NULL DUEAMOUNT,
(isnull(sum(SATABLE012.COLUMN14),0)) RETUNNS,
null SalesRep , SATABLE002.COLUMN22 SalesRepc,NULL dtt,SATABLE012.columna02 opuid,m5.COLUMN04 Brand,SATABLE002.COLUMN37 BrandID
FROM SATABLE012 inner join SATABLE011 on SATABLE011.COLUMN01=SATABLE012.COLUMN08 and isnull(SATABLE011.COLUMNA13,0)=0 and SATABLE012.COLUMNA03=SATABLE011.COLUMNA03
inner join SATABLE002 on SATABLE002.COLUMN02=SATABLE011.COLUMN05 and  SATABLE002.COLUMNA03=SATABLE011.COLUMNA03
and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
left join MATABLE005 m5   on m5.COLUMN02=SATABLE002.COLUMN37 and SATABLE002.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = SATABLE002.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
WHERE  isnull(SATABLE012.COLUMNA13,0)=0  and SATABLE011.COLUMN19 <=getdate() and isnull(SATABLE012.COLUMN16,'')  like('%a%') --and SATABLE012.COLUMN17 in('DebitMemo','Journal','PaymentVoucher','AdvancePayment')
and SATABLE012.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND SATABLE012.COLUMNA03=@AcOwner
and isnull(SATABLE012.COLUMNA13,0)=0 
group by SATABLE002.COLUMN05,SATABLE002.COLUMN11,SATABLE002.COLUMN16,SATABLE012.COLUMNA02,m5.COLUMN04,SATABLE002.COLUMN37,SATABLE002.COLUMN22,s3.column10
UNION ALL

SELECT c.COLUMN05 COLUMN07,
c.COLUMN11 PNNO, 
(S3.COLUMN10) AS CITY,

'' [type],
DATEDIFF(DAY,F31.COLUMN09,GETDATE()) [DAY],
F31.COLUMN09 COLUMN04,
F31.COLUMN04 COLUMN05, 

PUTABLE018.COLUMN09 as TOTALAMOUNT,
 cast(isnull(PUTABLE018.COLUMN09,0)as decimal(18,2))-cast(isnull(PUTABLE018.COLUMN11,0)as decimal(18,2)) DUEAMOUNT,
0 RETUNNS
,MATABLE010.COLUMN06 SalesRep ,
 C.COLUMN22 SalesRepc,
 F31.COLUMN09 dtt,
 PUTABLE018.columna02 opuid,
 m5.COLUMN04 Brand,
 c.COLUMN37 BrandID

--,NULL [1-30],NULL [31-60],NULL [61-90],NULL [91-180],NULL [181-360],NULL [361-520],NULL [>520]
FROM PUTABLE018 PUTABLE018
left join SATABLE002 c on c.COLUMN02=PUTABLE018.COLUMN07 and c.COLUMNA03=PUTABLE018.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = c.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
left join FITABLE031 F31 ON  CAST(F31.COLUMN01 AS NVARCHAR(250)) = CAST(PUTABLE018.COLUMN05 AS NVARCHAR(250)) AND F31.COLUMNA03 = PUTABLE018.COLUMNA03 AND isnull(F31.COLUMNA13,0)=0
left join MATABLE010 on MATABLE010.COLUMN02=c.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1 AND ISNULL(MATABLE010.COLUMNA13,0)=0
 WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('JOURNAL ENTRY')
--AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',56700) s) 
AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(PUTABLE018.COLUMN09,0)>'0'

--UNION ALL
--SELECT 
--c.COLUMN05 COLUMN07,c.COLUMN11 PNNO, 
--(S3.COLUMN10) AS CITY,
--'Credit' [type],0 [DAY],null COLUMN04,null COLUMN05, NULL TOTALAMOUNT, NULL DUEAMOUNT,
--cast(isnull(F32.COLUMN05,0)as decimal(18,2))-cast(isnull(F32.COLUMND05,0)as decimal(18,2)) RETUNNS
--,null SalesRep , C.COLUMN22 SalesRepc,NULL dtt,PUTABLE018.columna02 opuid,m5.COLUMN04 Brand,c.COLUMN37 BrandID
--FROM PUTABLE018 PUTABLE018
--left join SATABLE002 c on c.COLUMN02=PUTABLE018.COLUMN07 and c.COLUMNA03=PUTABLE018.COLUMNA03 and isnull(c.COLUMNA13,0)=0
--left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
--left join satable003 s3 on s3.column19 = 'Customer' and s3.column02 = c.COLUMN16 and isnull(s3.COLUMNA13,0)=0 and  s3.COLUMNA03=@AcOwner
--left JOIN FITABLE031 F31 ON CAST(F31.COLUMN01 AS NVARCHAR(250)) = CAST(PUTABLE018.COLUMN05 AS NVARCHAR(250)) AND F31.COLUMNA03 = PUTABLE018.COLUMNA03 AND F31.COLUMNA02 = PUTABLE018.COLUMNA02 and isnull(F31.COLUMNA13,0)=0
--LEFT JOIN FITABLE032 F32 ON F32.COLUMN12 = F31.COLUMN01 AND F32.COLUMNA03 = F31.COLUMNA03 AND F32.COLUMNA02 = F31.COLUMNA02 and isnull(F32.COLUMNA13,0)=0 --AND F32.COLUMN03 ='3000'
--LEFT JOIN FITABLE001 F1 ON F1.COLUMN02 = F32.COLUMN03 AND F1.COLUMNA03 = F32.COLUMNA03 AND isnull(F1.COLUMNA13,0)=0 
-- WHERE  isnull(PUTABLE018.COLUMNA13,0)=0 and PUTABLE018.COLUMN04 <=getdate() AND PUTABLE018.COLUMN06 in ('JOURNAL ENTRY')
----AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',56700) s) 
--AND PUTABLE018.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) --AND CAST(ISNULL(F32.COLUMN05,0) AS NVARCHAR(250))>'0' 
--AND F1.COLUMN07 = '22264'
)a group by column07,[DAY],[type],CITY,PNNO,COLUMN04,COLUMN05,SalesRep,SalesRepc,opuid,dtt,Brand,BrandID
--having isnull(a.COLUMN05,0)!='0' or a.COLUMN05!=''
--ORDER BY PUTABLE018.COLUMN04   , CITY 
					--
					) a group by a.COLUMN07,CITY,PNNO,COLUMN04,COLUMN05,a.SalesRep,a.SalesRepc,opuid,dtt,a.Brand,a.BrandID ) Query
--set @Query1='select * from #AGINGSUMMARY '+@whereStr
--exec (@Query1) 
Insert into #ARAgingTable select * FROM #AGINGSUMMARY 
set @flgARAging=1
declare @custo nvarchar(250)=1
select @cntARAging=COUNT(1) from #ARAgingTable
while (@flgARAging <=@cntARAging)
begin
select @CustomerName=COLUMN07,@CURRENT=isnull([CURRENT],0),@1_30=isnull([1-30],0),@31_60=isnull([31-60],0),@61_90=isnull([61-90],0),
@91_180=isnull([91-180],0),@181_360=isnull([181-360],0),@361_520=isnull([361-520],0),@520=isnull([520 and Over],0),@RETUNNS=isnull(RETUNNS,0),
@CITY=CITY,@PNNO=PNNO,@COLUMN04=COLUMN04,@COLUMN05=COLUMN05,@TOTALAMOUNT=TOTALAMOUNT,@DAY=[DAY],@CSALESREP=SALESREP,@CSALESREPC=SALESREPC,@dtt=dtt,@COPUID=OPUID,@BrandName=Brand,@BrandID=BrandID
from #ARAgingTable where RowIds=@flgARAging
if(cast(@custo as nvarchar(250))!=cast(@CustomerName as nvarchar(250)))
begin
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=(@flgARAging-1)
set @CreditBal=(@RETUNNS)
end
set @custo=(@CustomerName)
--seventh column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@520)<0
begin
set @CreditBal=@520-@CreditBal
update #ARAgingTable set [520 and Over]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [520 and Over]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@520
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--six column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@361_520)<0
begin
set @CreditBal=@361_520-@CreditBal
update #ARAgingTable set [361-520]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [361-520]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@361_520
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--fifth column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@181_360)<0
begin
set @CreditBal=@181_360-@CreditBal
update #ARAgingTable set [181-360]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [181-360]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@181_360
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--fourth column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@91_180)<0
begin
set @CreditBal=@91_180-@CreditBal
update #ARAgingTable set [91-180]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [91-180]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@91_180
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--third column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@61_90)<0
begin
set @CreditBal=@61_90-@CreditBal
update #ARAgingTable set [61-90]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [61-90]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@61_90
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--second column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@31_60)<0
begin
set @CreditBal=@31_60-@CreditBal
update #ARAgingTable set [31-60]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [31-60]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@31_60
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--first column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if(@CreditBal-@1_30)<0
begin
set @CreditBal=@1_30-@CreditBal
update #ARAgingTable set [1-30]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [1-30]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@1_30
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
end
--current column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if(@CreditBal-@CURRENT)<0
begin
set @CreditBal=@CURRENT-@CreditBal
update #ARAgingTable set [CURRENT]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [CURRENT]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@CURRENT
--if(@CreditBal>0)begin set @CreditBal=-(@CreditBal) end
--update #ARAgingTable set RETUNNS=@CreditBal where RowIds=@flgARAging
update #ARAgingTable set RETUNNS=0 where RowIds=@flgARAging
end
end
set @flgARAging=@flgARAging+1
end
--select * from #ARAgingTable
truncate table #AGINGSUMMARY
insert into #AGINGSUMMARY(COLUMN07,[CURRENT],[1-30],[31-60],[61-90],[91-180],[181-360],[361-520],[520 and Over],[RETUNNS],CITY,PNNO, COLUMN04,COLUMN05,TOTALAMOUNT,[DAY],SALESREP,SALESREPC,dtt,opuid,Brand,BrandID) select * from (
select a.COLUMN07,sum(a.[CURRENT]) [CURRENT],sum(a.[1-30]) [1-30],sum(a.[31-60]) [31-60],sum(a.[61-90]) [61-90],sum(a.[91-180]) [91-180],
sum(a.[181-360]) [181-360],sum(a.[361-520]) [361-520],sum(a.[520 and Over]) [520 and Over],sum(a.[RETUNNS]) [RETUNNS] ,LTRIM(RTRIM(a.CITY))CITY,a.PNNO,a.COLUMN04,a.COLUMN05,sum(isnull(a.TOTALAMOUNT,0))TOTALAMOUNT,sum(isnull(a.[DAY],0))[DAY],SALESREP,SALESREPC,Dtt,OPUID,a.Brand,a.BrandID
from #ARAgingTable a  group by a.COLUMN07,CITY,PNNO,COLUMN04,COLUMN05,SALESREP,SALESREPC,Dtt,OPUID,a.Brand,a.BrandID having ((sum(isnull(a.RETUNNS,0))+sum(isnull(a.[CURRENT],0))+sum(isnull(a.[1-30],0))+sum(isnull(a.[31-60],0))+sum(isnull(a.[61-90],0))+sum(isnull(a.[91-180],0))+sum(isnull(a.[181-360],0))+sum(isnull(a.[361-520],0))+sum(isnull(a.[520 and Over],0)))!=0) ) b
--select * from #AGINGSUMMARY
set @Query1='select COLUMN07,[CURRENT],[1-30],[31-60],[61-90],[91-180],[181-360],[361-520],[520 and Over],[RETUNNS],CITY,PNNO,COLUMN04,COLUMN05,TOTALAMOUNT,[DAY],SALESREP,SALESREPC,dtt,opuid,Brand,BrandID from #AGINGSUMMARY '+@whereStr +' order by COLUMN04 desc'
exec (@Query1) 
END






GO
