USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_JOBBERPAYMENT_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USE [MySmartSCM_SB_2016]
--GO
--/****** Object:  StoredProcedure [dbo].[usp_JO_TP_JOBBERPAYMENT_LINE_DATA]    Script Date: 04/05/2016 1:52:36 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


CREATE PROCEDURE [dbo].[usp_JO_TP_JOBBERPAYMENT_LINE_DATA]
(
	@SalesOrderID  nvarchar(50)
)

AS
BEGIN
declare @DueAmnt INT
declare @DueAmnt1 int
declare @PayAmnt int
declare @PayAmnt1 int
declare @IVID int
set @DueAmnt=(select isnull(SUM(COLUMN06),0)+isnull(SUM(COLUMN09),0) from SATABLE012 where COLUMN08 in (select isnull((COLUMN01),0) 
from SATABLE011 where COLUMN05=@SalesOrderID and isnull(COLUMNA13,0)=0) and isnull(COLUMNA13,0)=0)

if(@DueAmnt=0)
begin  
 select distinct b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,(isnull(max (b.COLUMN20),0)) as COLUMN05 ,
 null as COLUMN14, null as directReturn,'' AS COLUMN06,b.COLUMN08 dates,null as COLUMN16,null as directReturnCol
 from SATABLE010 a inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01 and isnull(b.COLUMNA13,0)=0   and isnull(a.COLUMNA13,0)=0 
 WHERE  a.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN05=@SalesOrderID) AND ( b.COLUMN20 IS NOT NULL AND b.COLUMN20!=0) 
 and b.COLUMN13!='AMOUNT FULLY RECEIVED' group by b.COLUMN02,b.COLUMN04,b.COLUMN05,b.COLUMN04,b.COLUMNA03,b.COLUMN08
 order by b.COLUMN08 asc
end
else if(@DueAmnt!=0 )
begin
DECLARE @CON INT
 -- select distinct b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),sum(a.COLUMN14))) as COLUMN04,  
 --   (SELECT CASE WHEN max(d.COLUMN04)>0 THEN min( d.COLUMN05) ELSE (max(isnull(b.COLUMN20,0))) END)  as COLUMN05,null as COLUMN14,null as directReturn,'' AS COLUMN06,
 --null as COLUMN16, null as directReturnCol,b.COLUMN08 dates
 --from SATABLE010 a  inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01  and isnull(b.COLUMNA13,0)=0  and isnull(a.COLUMNA13,0)=0  
 --left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03  and isnull(d.COLUMNA13,0)=0
 --WHERE  a.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN05=@SalesOrderID) and b.COLUMN13!='AMOUNT FULLY RECEIVED'  group by b.COLUMN02,b.COLUMN05,b.COLUMN04,b.COLUMNA03,b.COLUMN08
 --having ( SELECT CASE WHEN max(d.COLUMN04)>0 THEN min( d.COLUMN05) ELSE sum(a.COLUMN14) END)!=0
 --order by b.COLUMN08 asc
 select distinct b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,  
     ((max(isnull(b.COLUMN20,0)))-(isnull(sum(d.COLUMN06),0)+isnull(sum(d.COLUMN09),0)))  as COLUMN05,null as COLUMN14,null as directReturn,'' AS COLUMN06,
 null as COLUMN16, null as directReturnCol,b.COLUMN08 dates
 from  SATABLE009 b   
 left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03  and isnull(d.COLUMNA13,0)=0
 WHERE  b.COLUMN05=@SalesOrderID and b.COLUMN13!='AMOUNT FULLY RECEIVED' and isnull(b.COLUMNA13,0)=0  group by b.COLUMN02,b.COLUMN05,b.COLUMN04,b.COLUMNA03,b.COLUMN08
 order by b.COLUMN08 asc

	 END
else
begin
select distinct b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,0 as COLUMN05,null as COLUMN14
from SATABLE010 a  inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01  and isnull(b.COLUMNA13,0)=0  and isnull(a.COLUMNA13,0)=0  
WHERE  a.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN05=@SalesOrderID) AND ( b.COLUMN20 IS NOT NULL AND b.COLUMN20!=0) and b.COLUMN13!='AMOUNT FULLY RECEIVED'  group by b.COLUMN02,b.COLUMN05
end
END







GO
