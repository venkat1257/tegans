USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_CustomerPAYMENT_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_CustomerPAYMENT_LINE_DATA]
(
	@SalesOrderID  nvarchar(50),@AcOwner int=null,
	@OPUNIT int=null
)

AS
BEGIN
declare @DueAmnt INT
declare @DueAmnt1 int
declare @PayAmnt int
declare @PayAmnt1 int
declare @IVID int,@FrDate date = '1/1/2012',@ToDate date = '1/1/2200'
select top 1 @FrDate=f48.column04,@ToDate=f48.column05 from FITABLE048 f48 
inner join SATABLE002 s2 on s2.COLUMN02 = @SalesOrderID
where f48.COLUMNA03= s2.COLUMNA03 and f48.column09 = '1' and isnull(f48.columna13,0)=0 

set @DueAmnt=(
select isnull(SUM(s12.COLUMN06),0)+isnull(SUM(s12.COLUMN14),0)+isnull(SUM(s12.COLUMN09),0) from SATABLE012  s12
inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 and s11.COLUMN19 <= @ToDate
inner join SATABLE002 s2 on (s2.column02= s12.column20 OR s2.column38 = s12.column20) and s2.columna03=s12.columna03
where s12.COLUMN08=s11.COLUMN01 and isnull(s12.COLUMNA13,0)=0 and isnull(s11.COLUMNA13,0)=0 and (s2.column02=@SalesOrderID or s2.column38 =@SalesOrderID) and s12.COLUMNA03=@AcOwner and s12.COLUMNA02 in(@OPUNIT))

if(@DueAmnt=0)
begin  
 select distinct b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,(isnull(max (b.COLUMN20),0)) as COLUMN05 ,
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=@SalesOrderID and (column11=b.COLUMN04 or column11=cast(b.COLUMN02 as nvarchar(250))) and COLUMNA03=b.COLUMNA03 and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as COLUMN14,
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=@SalesOrderID and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06
 ,b.COLUMN08 dates,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 '' as COLUMN16,'' directReturnCol,b.COLUMN05 as COLUMN20
  --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=@SalesOrderID and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
 from SATABLE010 a inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01 and isnull(b.COLUMNA13,0)=0   and isnull(a.COLUMNA13,0)=0  and b.COLUMN08 <= @ToDate
 left outer join satable002 s2 on (s2.column02= @SalesOrderID OR s2.column38 = @SalesOrderID) and isnull(s2.COLUMNA13,0)=0  and s2.columna03=a.columna03
 WHERE  a.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN05=s2.COLUMN02) AND ( b.COLUMN20 IS NOT NULL AND b.COLUMN20!=0) 
 and (b.COLUMN13!='AMOUNT FULLY RECEIVED' or b.COLUMN13!='FULLY RECEIVED') and a.COLUMNA03=@AcOwner and a.COLUMNA02 in(@OPUNIT) group by b.COLUMN02,b.COLUMN04,b.COLUMN05,b.COLUMN04,b.COLUMNA03,b.COLUMN08
--EMPHCS1591 Direct return Credit will be applied to oldest to newest invoice/bill in customer/vendor payment by gnaneshwar on 7/3/2016
 order by b.COLUMN08 asc
end
else if(@DueAmnt!=0 )
begin
DECLARE @CON INT
  select distinct b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03,max (b.COLUMN20) as COLUMN04,  
    
 (SELECT CASE WHEN max(d.COLUMN04)>0 THEN (max (b.COLUMN20)-cast(Sum(isnull(d.COLUMN06,0))+Sum(isnull(d.COLUMN14,0))+Sum(isnull(d.COLUMN13,0))+Sum(isnull(d.COLUMN09,0))
 as decimal(18,2))) ELSE (max(isnull(b.COLUMN20,0))) END)  as COLUMN05,
 
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=@SalesOrderID and (column11=b.COLUMN04 or column11=cast(b.COLUMN02 as nvarchar(250))) and COLUMNA03=b.COLUMNA03 and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as COLUMN14,
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=@SalesOrderID and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06
 ,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 '' COLUMN16,'' directReturnCol
  --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=@SalesOrderID and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol

  ,b.COLUMN08 dates,b.COLUMN05 as COLUMN20
 from SATABLE009 b  
 left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03  and isnull(d.COLUMNA13,0)=0 AND d.COLUMNA03=b.COLUMNA03 AND ISNULL(d.COLUMN17,'')!='JOURNAL' 
	 --inner join SATABLE011 e on (CHARINDEX(',' + e.COLUMN06 + ',', ',' + REPLACE(@SalesOrderID, ' ', '') + ',') > 0 )
 left outer join satable002 s2 on (s2.column02= @SalesOrderID OR s2.column38 = @SalesOrderID) and isnull(s2.COLUMNA13,0)=0  and s2.columna03=b.columna03
 WHERE  b.COLUMN05=s2.COLUMN02 and isnull(b.COLUMNA13,0)=0  and (b.COLUMN13!='AMOUNT FULLY RECEIVED' or b.COLUMN13!='FULLY RECEIVED') and b.COLUMN08 <= @ToDate and b.COLUMNA03=@AcOwner and b.COLUMNA02 in(@OPUNIT) group by b.COLUMN02,b.COLUMN05,b.COLUMN04,b.COLUMNA03,b.COLUMN08
 having ( SELECT CASE WHEN max(d.COLUMN04)>0 THEN (max (b.COLUMN20)-cast(Sum(isnull(d.COLUMN06,0))+Sum(isnull(d.COLUMN14,0))+Sum(isnull(d.COLUMN13,0))+Sum(isnull(d.COLUMN09,0))
 as decimal(18,2))) ELSE (max(isnull(b.COLUMN20,0))) END)!=0
 --EMPHCS1591 Direct return Credit will be applied to oldest to newest invoice/bill in customer/vendor payment by gnaneshwar on 7/3/2016
 order by b.COLUMN08 asc

	 END
else
begin
select distinct b.COLUMN05 as COLUMN20, b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,0 as COLUMN05,
(select COLUMN15 from SATABLE005 where COLUMN05=@SalesOrderID or column11=cast(b.COLUMN05 as nvarchar(250)) and COLUMN33=b.COLUMN02  and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as COLUMN14
from SATABLE010 a  inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01  and isnull(b.COLUMNA13,0)=0  and isnull(a.COLUMNA13,0)=0  and b.COLUMN08 <= @ToDate  
left outer join satable002 s2 on (s2.column02= @SalesOrderID OR s2.column38 = @SalesOrderID) and isnull(s2.COLUMNA13,0)=0  and s2.columna03=a.columna03
WHERE  a.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN05=s2.COLUMN02) AND ( b.COLUMN20 IS NOT NULL AND b.COLUMN20!=0) and (b.COLUMN13!='AMOUNT FULLY RECEIVED' or b.COLUMN13!='FULLY RECEIVED') and a.COLUMNA03=@AcOwner and a.COLUMNA02 in(@OPUNIT) group by b.COLUMN02,b.COLUMN05
end
END













GO
