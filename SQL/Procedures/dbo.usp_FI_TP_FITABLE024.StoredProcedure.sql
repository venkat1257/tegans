USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE024]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE024]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS

BEGIN
 begin try
 DECLARE @dt date
 DECLARE @AC int
 DECLARE @Cust int
 DECLARE @Tax int
 --EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
 declare @TAXRATE decimal(18,2) 
 DECLARE @TaxAG int
 DECLARE @ID varchar(25)
 declare @PDC nvarchar(250),@Paymode nvarchar(250),@Payee nvarchar(250)
IF @Direction = 'Insert'

BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
 SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE024_SequenceNo
 --set @COLUMN09= (select max(column01) from FITABLE023)
 set @dt= (select column05 from FITABLE023 where COLUMN01=@COLUMN09)
 set @COLUMNA02= (select COLUMN16 from FITABLE023 where COLUMN01=@COLUMN09)
 set @COLUMNA13= (0)
 set @AC= (select column09 from FITABLE023 where COLUMN01=@COLUMN09)
 set @ID= (select column04 from FITABLE023 where COLUMN01=@COLUMN09)
 set @PDC= (select column07 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Payee= (select column08 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Paymode= (select column07 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Cust= (select column06 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Tax= (select column12 from FITABLE023 where COLUMN01=@COLUMN09)
 --EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
 set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 declare @tempSTR nvarchar(max)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Line Values are Intiated for FITABLE024 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+isnull(  @COLUMN12,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into FITABLE024
(
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, @COLUMN10,@COLUMN11,@COLUMN12,@COLUMN13,@COLUMN14,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
declare @AccountType nvarchar(250),@PayeeType nvarchar(250),@Exclusive int, @IDAR nvarchar(250),@memo nvarchar(250),
@Totbal decimal(18,2),@AMNT decimal(18,2),@INTERNAL nvarchar(250),@CharAcc nvarchar(250),@ACTYPE nvarchar(250)
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@AC AND COLUMNA03=@COLUMNA03)
set @INTERNAL=(SELECT COLUMN01 FROM FITABLE024 WHERE COLUMN02=@COLUMN02 AND COLUMNA03=@COLUMNA03)
 (select @PayeeType=COLUMN20,@Exclusive=COLUMN27 from FITABLE023 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03)
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(@column05,0)-ISNULL(column24,0) from FITABLE023 where COLUMN01=@COLUMN09)
else
set @Totbal= (@column05)

update FITABLE023 set  COLUMN10=@COLUMN05,COLUMN19=@Totbal where column01=@COLUMN09 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Line Values are Intiated for FITABLE024 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
    isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+ isnull( cast(@COLUMN02 as nvarchar(250)),'')+','+ isnull(cast(@COLUMN13 as nvarchar(250)),'')+','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
update FITABLE044 set  COLUMN18=(select column02 from FITABLE023 where COLUMN01=@COLUMN09),COLUMN19=(@COLUMN02) where column02=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
 if((@COLUMN13!='' or @COLUMN13!=null) and @Paymode=22273)
 begin
 update FITABLE044 set COLUMN15=22629 where COLUMN02=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
 end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''))
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
 declare @balance decimal(18,2) 
 declare @pid nvarchar(250)=null
 declare @bid nvarchar(250)=null
 declare @Vid nvarchar(250)=null
 declare @internalid nvarchar(250)=null
 --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @internalid= (select max(column01) from FITABLE023 where COLUMN01=@COLUMN09)

--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
set @balance=(select column10 from FITABLE001 where COLUMN02=@AC and COLUMNA03=@COLUMNA03)
set @bid=((select max(isnull(column02,999)) from PUTABLE019)+1)

declare @TaxString nvarchar(250)=NULL,@TAXRATEM DECIMAL(18,2)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN05,@TTYPE INT = 0,@HTTYPE INT = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=(select column12 from FITABLE023 where COLUMN01=@COLUMN09)
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@COLUMN05 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
		BEGIN
			
			declare @TaxColumn17 nvarchar(250),@ltaxamt DECIMAL(11,2)
			--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations 
			set @ltaxamt=(cast(@Totbal as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   --EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
   cast(isnull(@Vid,999)as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ 'RECEIVE PAYMENT'+','+ isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast(@COLUMN04 as nvarchar(250)),'')+','+ isnull( cast(@Tax as nvarchar(250)),'')+','+  isnull( cast(@ID as nvarchar(250)),'')+','+isnull( cast(@TaxAG as nvarchar(250)),'')+','+ isnull( cast(@COLUMN06  as nvarchar(250)),'')+','+  isnull(cast(@COLUMN05 as nvarchar(250)),'')+','+isnull(cast(@TAXRATE as nvarchar(250)),'') +','+isnull(cast(@TaxColumn17 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 SELECT top 1 @ACTYPE=COLUMN07,@CharAcc=column02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03 
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @DT,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ACTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN17,  COLUMNA02, COLUMNA03)
			values(@Vid,@dt,'ADVANCE RECEIPT',@internalid,@Payee,@COLUMN04,@Tax,@ID,@TaxAG,@ltaxamt,@COLUMN05,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
		END
		end
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22383', @COLUMN11 = '1119',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @IDAR= (SELECT MAX(isnull(COLUMN02,999)) FROM FITABLE026)+1
		--set @Totbal= (select ISNULL(column10,0) from FITABLE023 where COLUMN01=@COLUMN09)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@IDAR,999)as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ 'ADVANCE RECEIPT'+','+ isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+isnull( cast(@Totbal as nvarchar(250)),'')+','+isnull( cast(@ID as nvarchar(250)),'')+','+ '1119'+','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
--insert into FITABLE034 
--	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
--	COLUMNA02, COLUMNA03)
--	values
--	( ISNULL(@IDAR,10001),  'ADVANCE RECEIPT',  @dt,  @memo,  @COLUMN07,  @Totbal,0,@ID,'1119',@COLUMN08,
--	@COLUMNA02, @COLUMNA03)	
		
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN11,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
		 values(@IDAR,@dt,'ADVANCE RECEIPT',@internalid,@Payee,'1119',@ID,@Totbal,@Totbal,'Advances Received',@COLUMN08,@COLUMNA02, @COLUMNA03)

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @memo= (select column11 from FITABLE023 where COLUMN01=@COLUMN09)
set @Totbal= (select column10 from FITABLE023 where COLUMN01=@COLUMN09)
if(@PDC=22627)
begin
set @AMNT = (CAST(ISNULL(@COLUMN05,0) AS DECIMAL(18,2))+CAST(ISNULL(@COLUMN06,0) AS DECIMAL(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22402', @COLUMN11 = '1116',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @AMNT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
declare @assetid nvarchar(250)
set @assetid= (SELECT MAX(COLUMN02) FROM FITABLE034)+1
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC RECEIVED Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@assetid,999)as nvarchar(250)) +','+ 'ADVANCE RECEIPT'+','+isnull(cast(@dt as nvarchar(250)),'')+','+  isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+'0'+','+ isnull(cast(@ID as nvarchar(250)),'')+','+ '1116' +','+'0' +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
insert into FITABLE034 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMNA13,COLUMN12,
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   @assetid,  'ADVANCE RECEIPT',  @dt,  @memo,  @Payee,    (CAST(ISNULL(@COLUMN05,0) AS DECIMAL(18,2))+CAST(ISNULL(@COLUMN06,0) AS DECIMAL(18,2))), 0,  @ID,'1116',0,@COLUMN08,
	   @COLUMNA02, @COLUMNA03
	)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'PDC RECEIVED Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
	else
	begin
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions	
--declare @AccountType nvarchar(250),@PayeeType nvarchar(250),@Exclusive int
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@AC AND COLUMNA03=@COLUMNA03)
 (select @PayeeType=COLUMN20,@Exclusive=COLUMN27 from FITABLE023 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03)
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(column10,0) from FITABLE023 where COLUMN01=@COLUMN09)
else
set @Totbal= (select ISNULL(column10,0)+ISNULL(column24,0) from FITABLE023 where COLUMN01=@COLUMN09)
--Cash
 IF(@AccountType=22409)
BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @AC,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @bid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06, COLUMN07, COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@bid,10001),  @AC,  @dt,  @ID,  'ADVANCE RECEIPT',@PayeeType,  @Payee,  @internalid,  @memo,0,  
	@Totbal,@Totbal,@COLUMN08,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
END
ELSE
begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@bid,999)as nvarchar(250)) +','+cast(isnull(@AC,'')as nvarchar(250)) +','+cast(isnull(@dt,'')as nvarchar(250)) +','+cast(isnull(@ID,'')as nvarchar(250)) +','+ 'ADVANCE RECEIPT'+','+isnull(cast(@Payee as nvarchar(250)),'')+','+  isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+isnull( cast((@balance+@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +','+isnull(cast(@COLUMN04 as nvarchar(250)),'') +  '  '))
	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @AC,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08, COLUMN11, COLUMN12,COLUMNA02, COLUMNA03,COLUMN19)
 values(@bid,@AC,@dt,@ID,'ADVANCE RECEIPT',@Payee,@internalid,@Totbal,@Totbal,@COLUMNA02, @COLUMNA03,@COLUMN04)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
 end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
  'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
 'The Values are '+ isnull(cast((@balance+@COLUMN05+@COLUMN06) as nvarchar(250)),'') +','+isnull(cast(@AC as nvarchar(250)),'') +','+ isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
 update FITABLE001 set  COLUMN10=(cast(isnull(@balance,0) as decimal(18,2))+cast(isnull(@COLUMN05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))) where column02=@AC and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
 end
set @ReturnValue = 1
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from FITABLE024

END 

 

ELSE IF @Direction = 'Update'

BEGIN

set @COLUMNA13=0

set @COLUMN09=(select column09 from FITABLE024 where COLUMN02=@COLUMN02)
set @COLUMNA02=(select COLUMNA02 from FITABLE023 where COLUMN01=@COLUMN09)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Advance Payment Line Values are Intiated for FITABLE024 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE FITABLE024 SET
   COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,COLUMN12=@COLUMN12,COLUMN13=@COLUMN13,COLUMN14=@COLUMN14,
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMND04=@COLUMND04,                         COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02 
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Advance Payment Line Intiated Values are Created in FITABLE024 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
   update FITABLE023 set  COLUMN10=@COLUMN05,COLUMN19=@COLUMN05 where column01=@COLUMN09 and COLUMNA03=@COLUMNA03
--end
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
 --set @COLUMN09= (select column01 from FITABLE023 where COLUMN02 = @COLUMN02)
 set @dt= (select column05 from FITABLE023 where COLUMN01=@COLUMN09)
 set @COLUMNA02= (select COLUMN16 from FITABLE023 where COLUMN01=@COLUMN09)
 set @COLUMNA13=0
 set @AC= (select column09 from FITABLE023 where COLUMN01=@COLUMN09)
  set @PDC= (select column07 from FITABLE023 where COLUMN01=@COLUMN09)
  set @Paymode= (select column07 from FITABLE023 where COLUMN01=@COLUMN09)
 set @ID= (select column04 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Cust= (select column06 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Payee= (select column08 from FITABLE023 where COLUMN01=@COLUMN09)
 set @Tax= (select column12 from FITABLE023 where COLUMN01=@COLUMN09)
 set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
--set @pid=((select max(isnull(column02,999)) from PUTABLE018)+1)
set @INTERNAL=(SELECT COLUMN01 FROM FITABLE024 WHERE COLUMN02=@COLUMN02 AND COLUMNA03=@COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
    isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+ isnull( cast(@COLUMN02 as nvarchar(250)),'')+','+ isnull(cast(@COLUMN13 as nvarchar(250)),'')+','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
update FITABLE044 set  COLUMN18=(select column02 from FITABLE023 where COLUMN01=@COLUMN09),COLUMN19=(@COLUMN02) where column02=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
if((@COLUMN13!='' or @COLUMN13!=null) and @Paymode=22273)
 begin
 update FITABLE044 set COLUMN15=22629 where COLUMN02=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
 end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
set @balance=(select column10 from FITABLE001 where COLUMN02=@AC  and COLUMNA03=@COLUMNA03)
set @bid=((select max(isnull(column02,999)) from PUTABLE019)+1)
 (select @PayeeType=COLUMN20,@Exclusive=COLUMN27 from FITABLE023 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03)
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(column10,0)-ISNULL(column24,0) from FITABLE023 where COLUMN01=@COLUMN09)
else
set @Totbal= (select ISNULL(column10,0) from FITABLE023 where COLUMN01=@COLUMN09)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
 set @cnt  = 0 set @vochercnt  = 1 set @AMOUNTM =@COLUMN05 set @TTYPE  = 0 set @HTTYPE  = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=(select column12 from FITABLE023 where COLUMN01=@COLUMN09)
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@COLUMN05 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
		BEGIN
		
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			set @ltaxamt=(cast(@Totbal as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@Vid,999)as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ 'RECEIVE PAYMENT'+','+ isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast(@COLUMN04 as nvarchar(250)),'')+','+ isnull( cast(@Tax as nvarchar(250)),'')+','+  isnull( cast(@ID as nvarchar(250)),'')+','+isnull( cast(@TaxAG as nvarchar(250)),'')+','+ isnull( cast(@COLUMN06  as nvarchar(250)),'')+','+  isnull(cast(@COLUMN05 as nvarchar(250)),'')+','+isnull(cast(@TAXRATE as nvarchar(250)),'') +','+isnull(cast(@TaxColumn17 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
			
		SELECT top 1 @ACTYPE=COLUMN07,@CharAcc=column02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03 
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @DT,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ACTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN05, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN17,  COLUMNA02, COLUMNA03)
			values(@Vid,@dt,'ADVANCE RECEIPT',@COLUMN09,@Payee,@COLUMN04,@Tax,@ID,@TaxAG,@ltaxamt,@COLUMN05,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
		END
		end
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22383', @COLUMN11 = '1119',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@AC AND COLUMNA03=@COLUMNA03)
			set @IDAR= (SELECT MAX(COLUMN02) FROM FITABLE026)+1
			--set @Totbal= (select ISNULL(column10,0) from FITABLE023 where COLUMN01=@COLUMN09)
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   --cast(isnull(@IDAR,10001)as nvarchar(250)) +','+  'ADVANCE RECEIPT'+','+isnull(cast(@dt as nvarchar(250)),'')+','+ isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@COLUMN07 as nvarchar(250)),'')+','+ isnull( cast(@Totbal as nvarchar(250)),'')+','+'0'+','+isnull( cast(@ID as nvarchar(250)),'')+','+ '1119' +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
   cast(isnull(@IDAR,999)as nvarchar(250))+','+ 'ADVANCE PAYMENT'+','+ cast(@COLUMN09 as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+isnull( cast(@Totbal as nvarchar(250)),'')+','+isnull( cast(@ID as nvarchar(250)),'')+','+ '1119'+','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
--insert into FITABLE034 
--	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
--	COLUMNA02, COLUMNA03)
--	values
--	( ISNULL(@IDAR,10001),  'ADVANCE RECEIPT',  @dt,  @memo,  @COLUMN07,  @Totbal,0,@ID,'1119',@COLUMN08,
--	@COLUMNA02, @COLUMNA03)	
		
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN11,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
		 values(@IDAR,@dt,'ADVANCE RECEIPT',@COLUMN09,@Payee,'1119',@ID,@Totbal,@Totbal,'Advances Received',@COLUMN08,@COLUMNA02, @COLUMNA03)

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
		set @IDAR= (SELECT MAX(COLUMN02) FROM FITABLE034)+1
set @memo= (select column11 from FITABLE023 where COLUMN01=@COLUMN09)
set @Totbal= (select column10 from FITABLE023 where COLUMN01=@COLUMN09)
if(@PDC=22627)
begin
set @AMNT = (CAST(ISNULL(@COLUMN05,0) AS DECIMAL(18,2))+CAST(ISNULL(@COLUMN06,0) AS DECIMAL(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22402', @COLUMN11 = '1116',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @AMNT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @assetid= (SELECT MAX(COLUMN02) FROM FITABLE034)+1
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC RECEIVED Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@assetid,999)as nvarchar(250)) +','+ 'ADVANCE RECEIPT'+','+isnull(cast(@dt as nvarchar(250)),'')+','+  isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+'0'+','+ isnull(cast(@ID as nvarchar(250)),'')+','+ '1116' +','+'0' +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
insert into FITABLE034 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMNA13,COLUMN12,
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   @assetid,  'ADVANCE RECEIPT',  @dt,  @memo,  @Payee,    (CAST(ISNULL(@COLUMN05,0) AS DECIMAL(18,2))+CAST(ISNULL(@COLUMN06,0) AS DECIMAL(18,2))), 0,  @ID,'1116',0,@COLUMN08,
	   @COLUMNA02, @COLUMNA03
	)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC RECEIVED Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
	end
	else
	begin
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions	
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@AC AND COLUMNA03=@COLUMNA03)
 (select @PayeeType=COLUMN20,@Exclusive=COLUMN27 from FITABLE023 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03)
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(column10,0) from FITABLE023 where COLUMN01=@COLUMN09)
else
set @Totbal= (select ISNULL(column10,0)+ISNULL(column24,0) from FITABLE023 where COLUMN01=@COLUMN09)
--Cash
 IF(@AccountType=22409)
BEGIN
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @AC,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @bid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@bid,1000),  @AC,  @dt,  @ID,  'ADVANCE RECEIPT',@PayeeType,  @Payee,  @COLUMN09,  @memo,0,  
	@Totbal,@Totbal,@COLUMN08,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
END
ELSE
begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@bid,999)as nvarchar(250)) +','+cast(isnull(@AC,'')as nvarchar(250)) +','+cast(isnull(@dt,'')as nvarchar(250)) +','+cast(isnull(@ID,'')as nvarchar(250)) +','+ 'ADVANCE RECEIPT'+','+isnull(cast(@Payee as nvarchar(250)),'')+','+  isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+isnull( cast((@balance+@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +','+isnull(cast(@COLUMN04 as nvarchar(250)),'') +  '  '))
	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE RECEIPT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @AC,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08, COLUMN11, COLUMN12,COLUMNA02, COLUMNA03,COLUMN19)
 values(@bid,@AC,@dt,@ID,'ADVANCE RECEIPT',@Payee,@COLUMN09,@Totbal,@Totbal,@COLUMNA02, @COLUMNA03,@COLUMN04)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
  'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
 'The Values are '+ isnull(cast((@balance+@COLUMN05+@COLUMN06) as nvarchar(250)),'') +','+isnull(cast(@AC as nvarchar(250)),'') +','+ isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
 update FITABLE001 set  COLUMN10=(cast(isnull(@balance,0) as decimal(18,2))+cast(isnull(@COLUMN05,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))) where column02=@AC and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
 end
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

UPDATE FITABLE024 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02

END
exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE024.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'exception_FITABLE024.txt',0
end catch
end







GO
