USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FIN_REPORT_SYSRECONCILATIONDATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FIN_REPORT_SYSRECONCILATIONDATA]
(
@Type nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@Id        nvarchar(250)= null,
@Reconcilid        nvarchar(250)= null,
@DateF nvarchar(250)=null
)
AS
BEGIN
declare @whereStr nvarchar(max)
if (@Reconcilid!='' and @Reconcilid not in(23132))
begin
set @whereStr=' where BankReconcilid in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Reconcilid+''') s)'
end
if @Type!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where  BankType='''+@Type+''''
end
else
begin
set @whereStr=@whereStr+' and  BankType='''+@Type+''''
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end
 
exec(';With MyTable AS
(
SELECT p.COLUMN03 [Date],p.[COLUMN06] AS [Memo],p.[COLUMN07] Ref,isnull(p.[COLUMN08],0) Debit,isnull(p.[COLUMN09],0) Credit,p.[COLUMN10] Balance,f.[COLUMN04] AS [Bank],
m.[COLUMN04] AS [Status],h.COLUMN08 BankId,p.COLUMN02 chkrow,p.[COLUMN12] AS [BankReconcilid],
(case when isnull(p.COLUMN09,0)>0 then ''Payable'' when isnull(p.COLUMN08,0)>0 then ''Receivable'' else '''' end) BankType,p.COLUMNA02 OperatingUnit from SETABLE020 p 
inner join SETABLE019 h on h.COLUMN01=p.COLUMN11 and h.COLUMNA03=p.COLUMNA03 and h.COLUMNA02=p.COLUMNA02 and isnull(h.COLUMNA13,0)=0 
left join FITABLE001 f on f.COLUMN02=h.COLUMN08 and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0  
left join MATABLE002 m on m.COLUMN02=p.COLUMN12 and (m.COLUMNA03=p.COLUMNA03 or m.COLUMNA03 is null) and isnull(m.COLUMNA13,0)=0 
where h.COLUMN02='''+ @Id + ''' and p.COLUMNA03='''+@AcOwner+''' and p.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and isnull(p.COLUMNA13,0)=0
--having (isnull(p.COLUMN10,0)-isnull(p.COLUMN09,0))!=0
)
Select [Date],Memo,Ref,Debit,Credit,Balance,Bank,Status,BankId,chkrow,BankReconcilid,BankType,OperatingUnit
From MyTable '+@whereStr+'')
end

GO
