USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_CON_TP_CONTABLE031]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[usp_CON_TP_CONTABLE031]
(@AcOwner int,@Opunit int=null,@Name nvarchar(250)=null)
as
Begin
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @Opunit=(iif(@Opunit='',0 ,isnull(@Opunit,0)))
if(@Name='Allow Redies Cache')
select isnull(column04,0)COLUMN04,COLUMN05 from CONTABLE031 where COLUMNA03=@AcOwner and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03=@Name and isnull(columna13,0)=0
else
select COLUMN04,COLUMN05 from CONTABLE031 where COLUMNA03=@AcOwner and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03=@Name and isnull(column04,0)=1 and isnull(columna13,0)=0
end

GO
