USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAS_DEPARTMENT]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAS_DEPARTMENT]
(
    @ID   nvarchar(250),       
	@Form_ID   nvarchar(250)=null,  
	@Name   nvarchar(250)=null, 
	@Description   nvarchar(250)=null,  
	@Operating   nvarchar(250)=null,  
	@Memo   nvarchar(250)=null,  
	@Inactive   nvarchar(250)=null, 
	@MISC1   nvarchar(250)=null,  
	@MISC2   nvarchar(250)=null,  
	@MISC3   nvarchar(250)=null,  
	@MISC4   nvarchar(250)=null,
    @COLUMNA02  varchar(100)=null,   
	@COLUMNA03  varchar(100)=null,
	@COLUMNA06  varchar(100)=null,
	@COLUMNA07  varchar(100)=null,
	@COLUMNA08  varchar(100)=null,
	@COLUMNA11  varchar(100)=null,
	@COLUMNA12  varchar(100)=null,
	@COLUMNA13  varchar(100)=null,
	@Direction  varchar(100) 
)
AS

BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
select @ID=MAX(CAST(ISNULL(ID,10000) AS INT))+1 from DEPARTMENT

insert into DEPARTMENT 
(
   
   ID   ,
   Form_ID   ,
   Name   ,
   Description,Operating,Memo,
   Inactive  ,
   MISC1   ,
   MISC2   ,
   MISC3   ,
   MISC4   ,
   COLUMNA02  ,
   COLUMNA03  ,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA11,
   COLUMNA12  ,
   COLUMNA13 
)
values
( 
   
   @ID   ,
   @Form_ID   ,
   @Name   ,
   @Description,@Operating,@Memo,
   @Inactive   ,  
   @MISC1   ,
   @MISC2   ,
   @MISC3   ,
   @MISC4   ,
   @COLUMNA02 ,
   @COLUMNA03  ,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMNA11,@COLUMNA12,@COLUMNA13
)  
END

ELSE IF @Direction = 'Select'
BEGIN
select * from LOCATIONS
END 

ELSE IF @Direction = 'Update'
BEGIN
UPDATE LOCATIONS SET
   ID  = @ID ,
   Form_ID = @Form_ID  ,
   Name = @Name  ,
   Description = @Description,
   Operating = @Operating  ,
   Mobile = @Memo, INActive  = @Inactive, 
   MISC1 = @MISC1  ,
   MISC2 = @MISC2   ,
   MISC3 = @MISC3   ,
   MISC4 = @MISC4   ,
   COLUMNA02 = @COLUMNA02 ,
   COLUMNA03 = @COLUMNA03 ,COLUMNA06 = @COLUMNA06,COLUMNA07 = @COLUMNA07,COLUMNA08 = @COLUMNA08,@COLUMNA11  = @COLUMNA11,
   COLUMNA12 = @COLUMNA12 ,
   COLUMNA13 = @COLUMNA13 

   WHERE ID = @ID
END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE DEPARTMENT SET COLUMNA13 = 1  WHERE ID = @ID
END

end try
begin catch
declare @tempSTR nvarchar(max)
   set @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
   exec [CheckDirectory] @tempSTR,'EXception_usp_MAS_LOCATIONS.txt',0
return 0
end catch
end
GO
