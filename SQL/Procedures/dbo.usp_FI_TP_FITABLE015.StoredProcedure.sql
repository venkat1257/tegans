USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE015]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE015]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null, 	
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,    
	--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
	@COLUMN23   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  
	@COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null
)

AS
BEGIN
begin try
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08= '1' order by COLUMN01 desc
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
set @COLUMN22=(select iif((@COLUMN22!='' and @COLUMN22>0),@COLUMN22,(select max(column02) from fitable037 where column07=1 and column08=1)))
SELECT @COLUMNB01= COLUMN04,@COLUMNB02= COLUMN03 FROM FITABLE014 where COLUMN01=@COLUMN11
IF @Direction = 'Insert'
BEGIN
declare @Memo nvarchar(250)
declare @TrackQty bit
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
declare @Act nvarchar(250),@date nvarchar(250)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
declare @Num nvarchar(250),@Location nvarchar(250),@Project nvarchar(250)
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
select @COLUMN02=NEXT VALUE FOR FITABLE015_SequenceNo
--set @COLUMN11 = (select MAX(COLUMN01) from  FITABLE014);
set @COLUMN12=(select COLUMN10 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMNA02=(@COLUMN12)
set @COLUMN13=(select COLUMN11 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMN14=(select COLUMN12 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMN15=(select COLUMN13 from FITABLE014 where COLUMN01=@COLUMN11)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @Memo=(select COLUMN15 from FITABLE014 where COLUMN01=@COLUMN11)
set @date=(select COLUMN05 from FITABLE014 where COLUMN01=@COLUMN11)
set @Act=(select COLUMN06 from FITABLE014 where COLUMN01=@COLUMN11)
set @Num=(select COLUMN04 from FITABLE014 where COLUMN01=@COLUMN11)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @Location=(isnull((select COLUMN16 from FITABLE014 where COLUMN01=@COLUMN11),0))
set @Location=(iif(@Location='',0,isnull(@Location,0)))
set @COLUMN23=(iif(@COLUMN23='',0,isnull(@COLUMN23,0)))
set @Project = (select COLUMN26 from FITABLE014 Where COLUMN01=@COLUMN11)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)

--EMPHCS1410 LOGS CREATION BY SRINIVAS
Declare @tempSTR nvarchar(max)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment Line Values are Intiated for FITABLE015 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 				  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
    )
insert into FITABLE015 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,    
   @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,   @COLUMN16, @COLUMN17,
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   @COLUMN22,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 

--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment line Values are Created in FITABLE015 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
set @COLUMN19=(SELECT iif( (cast(isnull(@COLUMN08,0) as decimal(18,2)) > (0.00)) , isnull(@COLUMN08,0), isnull(@COLUMN16,0) ))
update FITABLE015 set COLUMN19=@COLUMN19 where COLUMN02=@COLUMN02
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
Begin
declare @Amount nvarchar(250) =null
set @Amount=((cast(isnull(@COLUMN08,0) as decimal(18,2))-(cast(isnull(@COLUMN16,0) as decimal(18,2))))*(cast(isnull(@COLUMN10,0) as decimal(18,2))))

declare @Qty_On_Hand decimal(18,2)
declare @Qty_Avl decimal(18,2)
declare @AvgPrice decimal(18,2)
declare @AvgAmt decimal(18,2)
set @COLUMN23=isnull(@COLUMN23,0)

declare @Qty_On_Hand1 decimal(18,7),@Qty_Order decimal(18,7),@Qty_Cmtd decimal(18,7),@TotalBillQty decimal(18,7),@TotalBillAmt decimal(18,2),@BillAvgPrice decimal(18,2)
set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@COLUMN03  AND l.COLUMN06=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN27,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN26,0)=CAST(@Project AS INT)
  union all
  select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN03 AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
  union all
  select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN04=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=isnull(@COLUMN23,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location and isnull(h.COLUMN26,0)=CAST(@Project AS INT) ))T)
set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@COLUMN03 AND l.COLUMN06=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN27,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND ISNULL(h.COLUMN26,0)=CAST(@Project AS INT)
  union all
  select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN03 AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
  union all
  select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN04=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=isnull(@COLUMN23,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location and isnull(h.COLUMN26,0)=CAST(@Project AS INT) ))T)
  if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN05=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN24,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT) )
  begin
  set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt AND  l.COLUMN03=@COLUMN03 AND l.COLUMN05=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN24,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,7)),2))as decimal(18,7)),2)
  set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN05=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN24,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,2)))as decimal(18,2))
  end
            if(isnull(@TotalBillQty,0)=0)
            begin
			set @BillAvgPrice=(0)
			end
			else
			begin
			set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),4))
			end
declare  @FinalAvgPrice decimal(18,2)
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@COLUMN23,0) AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if exists(select COLUMN04 from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
begin
 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))+ cast(isnull(@COLUMN08,0) as decimal(18,2))-isnull(cast(@COLUMN16  as decimal(18,2)),0));
 set @Qty_Avl=( cast((select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))+ cast(isnull(@COLUMN08,0) as decimal(18,2))-isnull(cast(@COLUMN16  as decimal(18,2)),0));
 set @Amount=((cast(isnull(@COLUMN08,0) as decimal(18,2))-(cast(isnull(@COLUMN16,0) as decimal(18,2))))*(cast(isnull(@BillAvgPrice,0) as decimal(18,2))))
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,COLUMN08 =@Qty_On_Hand,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT'
--,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22)as decimal(18,2))+(cast(@Amount as decimal(18,2))))
 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@COLUMN23,0) AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,7)),4));
  
 UPDATE FITABLE010 SET COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),2)*cast(@BillAvgPrice as decimal(18,2)
) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@COLUMN23,0) AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
--EMPHCS1410 LOGS CREATION BY SRINIVAS
				   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   'Inventory Values are Updated in FITABLE010 Table BY '+Cast(isnull(@Qty_On_Hand,'')as nvarchar(20))+' For ITEM '+cast(@COLUMN03 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				    )
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2)));
if(cast(@Qty_On_Hand as decimal(18,2))=0)
	begin
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
		--UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22
		UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
	end
else
	begin
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))/(cast( @Qty_On_Hand as decimal(18,2))))as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT'  WHERE COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
	
	END
end
else
begin
declare @newIID int
set @newIID=(select MAX(isnull(COLUMN02,0)) from FITABLE010)
if(@newIID>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into FITABLE010 
( COLUMN02,COLUMN03,COLUMN04, COLUMN08, COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02, COLUMNA03, COLUMNA06, COLUMNA07,COLUMN23,COLUMNA11,COLUMNB01,COLUMNB02,COLUMNB03,COLUMN24 )
values
( @newIID,@COLUMN03,@COLUMN08,@COLUMN08, @Amount,@BillAvgPrice , @COLUMN12,@COLUMN22,@Location,@COLUMN23,@COLUMNA02, @COLUMNA03, @COLUMNA06, @COLUMNA07,@Project,@COLUMNA08,@COLUMNB01,@COLUMNB02,'INSERT',@COLUMN04)
--EMPHCS1410 LOGS CREATION BY SRINIVAS
				   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   'Inventory Values Added to FITABLE010 Table BY '+Cast(isnull(@COLUMN08,'')as nvarchar(20))+' For ITEM '+cast(@COLUMN03 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				    )
END
declare @balance decimal(18,2)
declare @Increase decimal(18,2)
declare @decrease decimal(18,2)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @avgprice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 and ISNULL(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
set @Increase= cast(isnull(@COLUMN08,0) as  decimal(18,2))*cast(isnull(@COLUMN10,0) as decimal(18,2))
set @decrease=cast(isnull(@COLUMN16,0)  as  decimal(18,2))*cast(isnull(@COLUMN10,0) as decimal(18,2))
set @balance=( cast(isnull((select max(COLUMN11) from FITABLE018 where COLUMN08='1000'),0) as decimal(18,2))+cast(( @Increase-@decrease) as decimal(18,2)))
		set @newIID=(select MAX(isnull(COLUMN02,0)) from PUTABLE017)
				if(@newIID>0)
					begin
						set @newIID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
					end
				else
					begin
						set @newIID=1000
					end
					--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
					--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
					--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
					declare @lIID nvarchar(250)
					set @lIID=(select COLUMN01 from FITABLE015 WHERE COLUMN02=@COLUMN02)
			insert into PUTABLE017(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN15,COLUMNA02,COLUMNA03, COLUMNA06, COLUMNA07, COLUMND05,COLUMND06)			
			values(@newIID,1000,@date,@COLUMN11,'Inventory Adjustment',@Act,@Memo, @Increase, @decrease, @Increase, @COLUMNA02,@Location, @COLUMNA02,@COLUMNA03, @COLUMNA06, @COLUMNA07,@COLUMN03,@lIID)
			--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Created in PUTABLE017 Table for '+Cast(isnull(@COLUMN11,'')as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
    )
    --EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
	declare @REF NVARCHAR(250),@Acc NVARCHAR(250)
	set @lIID=(select COLUMN01 from FITABLE015 WHERE COLUMN02=@COLUMN02)
	set @Acc=(select MAX(isnull(COLUMN07,0))COLUMN07 from FITABLE001 WHERE COLUMN02=@Act and  COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	set @REF=(select COLUMN04 from FITABLE014 WHERE COLUMN01=@COLUMN11)
	--if(@Acc!='22330')
	--BEGIN
	--	set @newIID=(select MAX(COLUMN02) from FITABLE025)
	--	if(@newIID>0)
	--	begin
	--	set @newIID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1
	--	end
	--	else
	--	begin
	--	set @newIID=1000
	--	end
	--	insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN07,COLUMN08,COLUMN09,COLUMN11, COLUMNA02, COLUMNA03, COLUMNA13,COLUMNA06,COLUMNA07,COLUMNA08,COLUMND05,COLUMND06)
	--	values(@newIID,@date,'Inventory Adjustment',@lIID,@Memo,@Act,@decrease, @Increase, @COLUMNA02, @COLUMNA03,0,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMN03,@lIID)
	--END
	--ELSE BEGIN
	--	set @newIID=(select MAX(COLUMN02) from FITABLE029)
	--	if(@newIID>0)
	--	begin
	--	set @newIID=cast((select MAX(COLUMN02) from FITABLE029) as int)+1
	--	end
	--	else
	--	begin
	--	set @newIID=1000
	--	end
	--	insert into FITABLE029(COLUMN02,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11, COLUMNA02, COLUMNA03, COLUMNA13,COLUMNA06,COLUMNA07,COLUMNA08,COLUMND05,COLUMND06)
	--	values(@newIID,'Inventory Adjustment',@date,@Act,@Memo,@Increase, @decrease,@REF, @COLUMNA02, @COLUMNA03,0,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMN03,@lIID)
	--END
if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
begin
EXEC usp_TP_InventoryAssetUpdate @COLUMN11,@Num,null,@COLUMN03,@COLUMN22,@Location,@COLUMN23,@BillAvgPrice,@COLUMN12,@COLUMNA03
end
end	 

END
 

IF @Direction = 'Select'
BEGIN
select * from FITABLE015
END 
 

IF @Direction = 'Update'
BEGIN
set @COLUMN12=(select COLUMN10 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMN13=(select COLUMN11 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMN14=(select COLUMN12 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMN15=(select COLUMN13 from FITABLE014 where COLUMN01=@COLUMN11)
set @COLUMNA02=(@COLUMN12)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @date=(select COLUMN05 from FITABLE014 where COLUMN01=@COLUMN11)
set @Memo=(select COLUMN15 from FITABLE014 where COLUMN01=@COLUMN11)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @Location=(isnull((select COLUMN16 from FITABLE014 where COLUMN01=@COLUMN11),0))
set @Location=(iif(@Location='',0,isnull(@Location,0)))
set @COLUMN23=(iif(@COLUMN23='',0,isnull(@COLUMN23,0)))
set @Act=(select COLUMN06 from FITABLE014 WHERE COLUMN01=@COLUMN11)
set @Project = (select COLUMN26 from FITABLE014 Where COLUMN01=@COLUMN11)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
if not exists( SELECT 1 FROM FITABLE015 WHERE COLUMN02=@COLUMN02 and COLUMN03=@COLUMN03 and COLUMN04=@COLUMN04 and column22=@COLUMN22 and  columnA02=@COLUMNA02 and columnA03=@COLUMNA03 )
begin
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE015_SequenceNo
insert into FITABLE015 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,    
   @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,   @COLUMN16, @COLUMN17,
   @COLUMN22,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
end 
else 
begin
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
--set @COLUMN19=(SELECT iif( (cast(@COLUMN08 as int) > 0 and @COLUMN08!='') , isnull(@COLUMN08,0), isnull(@COLUMN16,0) ))
UPDATE FITABLE015 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16, 
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN22,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,    COLUMN22=@COLUMN22,
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   COLUMN23=@COLUMN23,
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  
   COLUMNA07=@COLUMNA07,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  
   COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  
   COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07, 
   COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  
   COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05, 
   COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   end
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Adjustment line Values are Updated in FITABLE015 Table For '+cast(@COLUMN02 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
    )
set @COLUMN19=(SELECT iif( (cast(isnull(@COLUMN08,0) as decimal(18,2)) > (0.00)) , isnull(@COLUMN08,0), isnull(@COLUMN16,0) ))
update FITABLE015 set COLUMN19=@COLUMN19 where COLUMN02=@COLUMN02

set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
set @COLUMN23=isnull(@COLUMN23,0)
if(@TrackQty=1)
begin
set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN08>=@FiscalYearStartDt AND l.COLUMN04=@COLUMN03 AND l.COLUMN06=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN27,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN26,0)=CAST(@Project AS INT)
  union all
  select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN03 AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
  union all
  select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN04=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=isnull(@COLUMN23,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location and isnull(h.COLUMN26,0)=CAST(@Project AS INT)))T)
set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN08>=@FiscalYearStartDt and l.COLUMN04=@COLUMN03 AND l.COLUMN06=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN27,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN26,0)=CAST(@Project AS INT)
  union all
  select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN03 AND isnull(l.COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0
  union all
  select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN04=@COLUMN04  AND isnull(l.COLUMN22,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=isnull(@COLUMN23,0) and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location and isnull(h.COLUMN26,0)=CAST(@Project AS INT)))T)
  if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN05=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN24,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))
  begin
  set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN05=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN24,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,7)),2))as decimal(18,7)),2)
  set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN03 AND l.COLUMN05=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN12,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN22,0) AND isnull(l.COLUMN24,0)=isnull(@COLUMN23,0) AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN21,0)=CAST(@Project AS INT))as decimal(18,2)))as decimal(18,2))
  end
            if(isnull(@TotalBillQty,0)=0)
            begin
			set @BillAvgPrice=(0)
			end
			else
			begin
			set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),2))
			end
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@COLUMN23,0) AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if exists(select COLUMN04 from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
begin
 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))+ cast(isnull(@COLUMN08,0) as decimal(18,2))-isnull(cast(@COLUMN16  as decimal(18,2)),0));
 set @Qty_Avl=( cast((select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))+ cast(isnull(@COLUMN08,0) as decimal(18,2))-isnull(cast(@COLUMN16  as decimal(18,2)),0));
 set @Amount=((cast(isnull(@COLUMN08,0) as decimal(18,2))-(cast(isnull(@COLUMN16,0) as decimal(18,2))))*(cast(isnull(@BillAvgPrice,0) as decimal(18,2))))
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,COLUMN08 =@Qty_Avl,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE'
--,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22)as decimal(18,2))+(cast(@Amount as decimal(18,2))))
 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@COLUMN23,0) AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,7)),2));
 
 UPDATE FITABLE010 SET COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),2)*(round(cast(@BillAvgPrice as decimal(18,7)),2)
) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=isnull(@COLUMN23,0) AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
--EMPHCS1410 LOGS CREATION BY SRINIVAS
				   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   'Inventory Values are Updated in FITABLE010 Table BY '+Cast(isnull(@Qty_On_Hand,'')as nvarchar(20))+' For ITEM '+cast(@COLUMN03 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				    )
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 and COLUMN24=@COLUMN04)as decimal(18,2)));
if(cast(@Qty_On_Hand as decimal(18,2))=0)
	begin
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
		UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
	end
else
	begin
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
		--UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22)as decimal(18,2))/(cast( @Qty_On_Hand as decimal(18,2))))as decimal(18,2))  WHERE COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@COLUMN23 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)as decimal(18,2))/round(cast(@Qty_On_Hand as decimal(18,7)),2)) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN12 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@COLUMN23 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04
	
	end
end
else
begin
set @newIID=(select MAX(isnull(COLUMN02,0)) from FITABLE010)
if(@newIID>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
set @Amount=((cast(isnull(@COLUMN08,0) as decimal(18,2))-(cast(isnull(@COLUMN16,0) as decimal(18,2))))*(cast(isnull(@COLUMN10,0) as decimal(18,2))))
--EMPHCS928 rajasekhar reddy patakota 12/8/2015 uom condition checking in Stock Transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into FITABLE010 
( COLUMN02,COLUMN03,COLUMN04, COLUMN08, COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02, COLUMNA03, COLUMNA06, COLUMNA07,COLUMN23,COLUMNA11,COLUMNB01,COLUMNB02,COLUMNB03,COLUMN24 )
values
( @newIID,@COLUMN03,@COLUMN08,@COLUMN08, @Amount,@BillAvgPrice , @COLUMN12,@COLUMN22,@Location,@COLUMN23,@COLUMNA02, @COLUMNA03, @COLUMNA06, @COLUMNA07,@Project,@COLUMNA08,@COLUMNB01,@COLUMNB02,'UPDATE',@COLUMN04)
END
 --EMPHCS1410 LOGS CREATION BY SRINIVAS
				   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				   'Inventory Values Added to FITABLE010 Table BY '+Cast(isnull(@COLUMN08,'')as nvarchar(20))+' For ITEM '+cast(@COLUMN03 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
				    )
				    --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @avgprice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN12 and isnull(COLUMN21,0)=@Location and isnull(COLUMN22,0)=@COLUMN23 and COLUMN19=@COLUMN22 AND isnull(COLUMN23,0)=@Project and COLUMN24=@COLUMN04)
set @Increase= cast(isnull(@COLUMN08,0) as  decimal(18,2))*cast(isnull(@COLUMN10,0) as decimal(18,2))
set @decrease=cast(isnull(@COLUMN16,0)  as  decimal(18,2))*cast(isnull(@COLUMN10,0) as decimal(18,2))
set @balance=( cast(isnull((select max(COLUMN11) from FITABLE018 where COLUMN08='1000'),0) as decimal(18,2))+cast(( @Increase-@decrease) as decimal(18,2)))
  set @newIID=(select MAX(isnull(COLUMN02,0)) from PUTABLE017)
				if(@newIID>0)
					begin
						set @newIID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
					end
				else
					begin
						set @newIID=1000
					end
					--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
					--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
					--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
					set @lIID=(select COLUMN01 from FITABLE015 WHERE COLUMN02=@COLUMN02)
			insert into PUTABLE017(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN15,COLUMNA02,COLUMNA03, COLUMNA06, COLUMNA07, COLUMND05,COLUMND06)				
			values(@newIID,1000,@date,@COLUMN11,'Inventory Adjustment',@Act,@Memo, @Increase, @decrease, @Increase, @COLUMNA02,@Location, @COLUMNA02,@COLUMNA03, @COLUMNA06, @COLUMNA07,@COLUMN03,@lIID)
			--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Created in PUTABLE017 Table for '+Cast(isnull(@COLUMN11,'')as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
    )
    --EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
	set @lIID=(select COLUMN01 from FITABLE015 WHERE COLUMN02=@COLUMN02)
	set @Act=(select COLUMN06 from FITABLE014 WHERE COLUMN01=@COLUMN11)
	set @Acc=(select MAX(isnull(COLUMN07,0))COLUMN07 from FITABLE001 WHERE COLUMN02=@Act and  COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	set @REF=(select COLUMN04 from FITABLE014 WHERE COLUMN01=@COLUMN11)
	--if(@Acc!='22330')
	--BEGIN
	--	set @newIID=(select MAX(COLUMN02) from FITABLE025)
	--	if(@newIID>0)
	--	begin
	--	set @newIID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1
	--	end
	--	else
	--	begin
	--	set @newIID=1000
	--	end
	--	insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN07,COLUMN08,COLUMN09,COLUMN11, COLUMNA02, COLUMNA03, COLUMNA13,COLUMNA06,COLUMNA07,COLUMNA08,COLUMND05,COLUMND06)
	--	values(@newIID,@date,'Inventory Adjustment',@lIID,@Memo,@Act,@decrease, @Increase, @COLUMNA02, @COLUMNA03,0,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMN03,@lIID)
	--END
	--ELSE BEGIN
	--	set @newIID=(select MAX(COLUMN02) from FITABLE029)
	--	if(@newIID>0)
	--	begin
	--	set @newIID=cast((select MAX(COLUMN02) from FITABLE029) as int)+1
	--	end
	--	else
	--	begin
	--	set @newIID=1000
	--	end
	--	insert into FITABLE029(COLUMN02,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11, COLUMNA02, COLUMNA03, COLUMNA13,COLUMNA06,COLUMNA07,COLUMNA08,COLUMND05,COLUMND06)
	--	values(@newIID,'Inventory Adjustment',@date,@Act,@Memo,@Increase, @decrease,@REF, @COLUMNA02, @COLUMNA03,0,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMN03,@lIID)
	--END
if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
begin
EXEC usp_TP_InventoryAssetUpdate @COLUMN11,@Num,null,@COLUMN03,@COLUMN22,@Location,@COLUMN23,@BillAvgPrice,@COLUMN12,@COLUMNA03
end
End
END
 

else IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE015 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END

   --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE015.txt',0
end try
			begin catch
		--EMPHCS1410	Logs Creation by srinivas		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_FITABLE015.txt',0

				if not exists(select column01 from FITABLE015 where column11=@column11)
					begin
						delete from FITABLE014 where column01=@column11
						return 0
			end

return 0
end catch
end













GO
