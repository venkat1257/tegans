USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PRTABLE008_RCDirect]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PRO_TP_PRTABLE008_RCDirect]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS

BEGIN
begin try
set @COLUMN11=(select iif((@COLUMN11!='' and @COLUMN11>0),@COLUMN11,(select max(column02) from fitable037 where column07=1 and column08=1)))
IF @Direction = 'Insert'
BEGIN
declare @DecimalPositions int
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
set @COLUMN07=round(cast(@COLUMN07 as decimal(18,7)),@DecimalPositions)
set @COLUMN06=round(cast(@COLUMN06 as decimal(18,7)),@DecimalPositions)
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE008_SequenceNo
--set @COLUMN10=(select max(COLUMN01) from PRTABLE007)
set @COLUMNA02=(select COLUMN11 from PRTABLE007 where COLUMN01=@COLUMN10) 
set @COLUMN08= (SELECT isnull(COLUMN04,0) from MATABLE024 where COLUMN07=@COLUMN03 and COLUMN06='Purchase' and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 )
insert into PRTABLE008 
(
   COLUMN02,  COLUMN03,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN10,COLUMN11,  
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN10,@COLUMN11,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  

declare @Sourceou nvarchar(250)=null,@Memo nvarchar(250)=null,@SAvgCost nvarchar(250)=null,
@SQOH nvarchar(250)=null,@Project nvarchar(250)=null,@newID1 int=null,@Projectactualcost DECIMAL(18,2), @Amount DECIMAL(18,2), 
@InewID int,@ItmpnewID1 int ,@AID int,@internalid nvarchar(250)=null,@chk bit ,@Customer nvarchar(250)=null,@Cunsumptionno nvarchar(250)=null,@date nvarchar(250)=null,
@SBAL DECIMAL(18,2),@CBAL DECIMAL(18,2),@lineiid nvarchar(250)=null,@DT date,@number1 int,@CUST int,@PRICE DECIMAL(18,2)

set @chk=(select column48 from matable007 where column02=@COLUMN03 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @Cunsumptionno= (SELECT COLUMN04 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10) and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@COLUMN02)
set @date= (SELECT COLUMN09 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Customer= (SELECT COLUMN06 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Memo= (SELECT COLUMN13 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @Sourceou= (SELECT COLUMN11 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Amount=(round(cast(@COLUMN06 as DECIMAL(18,2)),@DecimalPositions)*cast(@COLUMN08 as DECIMAL(18,2)))
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))+cast(@Amount as DECIMAL(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0

if(round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)>0)
begin
if(@chk=1)
begin
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou AND COLUMN19=@COLUMN11 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0 AND isnull(COLUMN23,0)=@Project)
begin
set @SQOH= (SELECT sum(COLUMN04) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
 
update FITABLE010 set COLUMN04=(round(cast( @SQOH as DECIMAL(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)), COLUMN08=(round(cast( @SQOH as decimal(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)),
COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 AND isnull(COLUMN23,0)=@Project and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)as decimal(18,2))-((round(cast(@COLUMN06 as decimal(18,7)),@DecimalPositions))*(round(cast(@SAvgCost as decimal(18,7)),@DecimalPositions))))
where COLUMN03=@COLUMN03 and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
set @SQOH=(round(cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where  COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions));
if(round(cast(@SQOH as decimal(18,7)),@DecimalPositions)=0)
begin
UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
end
else
begin
UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))/round(cast(@SQOH as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
end

end
else
begin

set @newID1=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08,COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23
)
values
(
@newID1,@COLUMN03,-(cast(@COLUMN06 as decimal(18,2))),'0', '0',-(cast(@COLUMN06 as decimal(18,2))),0,0,@COLUMNA02,@COLUMN11,0,0,@COLUMNA02,@COLUMNA03,@Project 
)
end

set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end

set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
set @SAvgCost= (cast(@SAvgCost as DECIMAL(18,2))*round(cast(@COLUMN06 as DECIMAL(18,7)),@DecimalPositions))
insert into PUTABLE017 
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07, COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13)
values
( @InewID,'1000', @date ,@COLUMN10,'Resource Consumption Direct' ,@Customer,@Memo,@SAvgCost,@SAvgCost,@Sourceou,@Project,@COLUMNA02,@COLUMNA03,1,0)

set @AID=cast((select isnull(MAX(COLUMN02),1000) from FITABLE036) as int)+1;
insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
values(@AID,'Resource Consumption Direct',@date,@internalid,@Customer,CAST(@Amount AS decimal(18,2)),NULL,@Cunsumptionno,1153,@COLUMNA02 , @COLUMNA03,0,@Project)
end
end

set @ReturnValue = 1
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from PRTABLE008

END 

 

ELSE IF @Direction = 'Update'

BEGIN

if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
set @COLUMNA02=( CASE WHEN (@COLUMN10!= '' and @COLUMN10 is not null ) THEN (select COLUMN11 from PRTABLE007 where COLUMN01=@COLUMN10) else @COLUMNA02  END )
set @COLUMN08= (SELECT isnull(COLUMN04,0) from MATABLE024 where COLUMN07=@COLUMN03 and COLUMN06='Purchase' and COLUMNA03=@COLUMNA03 and isnull(columna13,0)=0)
if exists(SELECT COLUMN02 FROM PRTABLE008 where COLUMN02=@COLUMN02 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
begin
UPDATE PRTABLE008 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11,   
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
end
else
begin
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE008_SequenceNo
insert into PRTABLE008 
(
   COLUMN02,  COLUMN03,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN10,COLUMN11,  
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN10,@COLUMN11,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
end

set @chk=(select column48 from matable007 where column02=@COLUMN03 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @Cunsumptionno= (SELECT COLUMN04 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10) and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@COLUMN02)
set @date= (SELECT COLUMN09 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Customer= (SELECT COLUMN06 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Memo= (SELECT COLUMN13 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @Sourceou= (SELECT COLUMN11 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Amount=(round(cast(@COLUMN06 as DECIMAL(18,2)),@DecimalPositions)*cast(@COLUMN08 as DECIMAL(18,2)))
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))+cast(@Amount as DECIMAL(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0

if(round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)>0)
begin
if(@chk=1)
begin
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou AND COLUMN19=@COLUMN11 AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=0 AND isnull(COLUMN23,0)=@Project)
begin
set @SQOH= (SELECT sum(COLUMN04) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
 
update FITABLE010 set COLUMN04=(round(cast( @SQOH as DECIMAL(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)), COLUMN08=(round(cast( @SQOH as decimal(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)),
COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))-((round(cast(@COLUMN06 as decimal(18,7)),@DecimalPositions))*(round(cast(@SAvgCost as decimal(18,7)),@DecimalPositions))))
where COLUMN03=@COLUMN03 and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
set @SQOH=(round(cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where  COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,7)),@DecimalPositions));
if(round(cast(@SQOH as decimal(18,7)),@DecimalPositions)=0)
begin
UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
end
else
begin
UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)as decimal(18,2))/round(cast(@SQOH as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project
end

end
else
begin

set @newID1=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08,COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23
)
values
(
@newID1,@COLUMN03,-(cast(@COLUMN06 as decimal(18,2))),'0', '0',-(cast(@COLUMN06 as decimal(18,2))),0,0,@COLUMNA02,@COLUMN11,0,0,@COLUMNA02,@COLUMNA03,@Project
)
end

set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end

set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11  and isnull(COLUMN21,0)=0 and isnull(COLUMN22,0)=0 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 AND isnull(COLUMN23,0)=@Project)
set @SAvgCost= (cast(@SAvgCost as DECIMAL(18,2))*round(cast(@COLUMN06 as DECIMAL(18,7)),@DecimalPositions))
insert into PUTABLE017 
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07, COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13)
values
( @InewID,'1000', @date ,@COLUMN10,'Resource Consumption Direct' ,@Customer,@Memo, @SAvgCost,@SAvgCost,@Sourceou,@Project,@COLUMNA02,@COLUMNA03,1,0)

set @AID=cast((select isnull(MAX(COLUMN02),1000) from FITABLE036) as int)+1;
insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
values(@AID,'Resource Consumption Direct',@date,@internalid,@Customer,CAST(@Amount AS decimal(18,2)),NULL,@Cunsumptionno,1153,@COLUMNA02 , @COLUMNA03,0,@Project)
end
end

set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

UPDATE PRTABLE008 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02

END

end try
begin catch
declare @tempSTR nvarchar(max)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PRTABLE008_RCDirect.txt',0
return 0
end catch
end




GO
