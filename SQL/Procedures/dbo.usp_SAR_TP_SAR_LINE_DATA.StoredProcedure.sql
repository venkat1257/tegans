USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_SAR_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_SAR_LINE_DATA]
(
	@SalesOrderID int
)

AS
BEGIN
	SELECT  
	b.COLUMN03, b.COLUMN04,b.COLUMN05,b.COLUMN12 COLUMN07,b.COLUMN08,b.COLUMN09,b.COLUMN10
	--,b.COLUMN11
	,b.COLUMN12,
	b.COLUMN13
	--,b.COLUMN24
	,b.COLUMN25 COLUMN24,b.COLUMN27 COLUMN26,b.COLUMN26 COLUMN25
	--EMPHCS1145  rajasekhar reddy patakota 14/9/2015 getting tax caluclation wrong in sales order returns
	,cast((((cast(isnull(m.column07,0) as decimal(18,2))*(0.01)))*cast(b.COLUMN25 as decimal(18,2)))
	+cast(b.COLUMN25 as decimal(18,2)) as decimal(18,2)) COLUMN11,b.COLUMN17
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,b.COLUMN37 COLUMN35,b.COLUMN38 COLUMN37
	--EMPHCS876  rajasekhar reddy patakota 8/8/2015 After clicking on RETURN button from purchase order, system is not considerig deleted lines of that purchase order and showing all lines 
	FROM SATABLE005 a inner join SATABLE006 b on a.COLUMN01=b.COLUMN19 and b.COLUMNA13=0
	left join MATABLE013 m on  m.column02=b.COLUMN26
	WHERE  a.COLUMN02= @SalesOrderID
END


GO
