USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PUTABLE014]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PUTABLE014]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  @COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null, 
    @COLUMN07   nvarchar(250)=null,  @COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  @COLUMN11   nvarchar(250)=null, 
    @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  @COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  @COLUMN20   nvarchar(250)=null,  @COLUMN21  nvarchar(250)=null,
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
--EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
	@COLUMN22   nvarchar(250)=null,  @COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null, 
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null, 
    @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null, 
    @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null, 
    @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,
    @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
    --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN17=(1001)
 --EMPHCS1410	Logs Creation by srinivas
  declare @tempSTR nvarchar(max)
   --EMPHCS1593	payment calculation regarding bank selection in Bill Payment and Customer payment By RAj.Jr
    if((@COLUMN08='' or @COLUMN08=null))
  begin
  set @COLUMN08=0
  end 
  if((@COLUMN20='' or @COLUMN20=null))
  begin
  set @COLUMN20=22305
  end
  --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE014_SequenceNo
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Vendor Patment Header Values are Intiated for PUTABLE014 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+ isnull( @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull( @COLUMN24 ,'')+','+isnull( @COLUMN25,'')+','+isnull( @COLUMNA01,'')+','+isnull(@COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 											  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+'')
   )
insert into PUTABLE014 
(

   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,  COLUMN13,  COLUMN14, COLUMN15,COLUMN16,
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
  --EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
   COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,

   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,

   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10

)

values

( 

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,   @COLUMN15, @COLUMN16,
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
  --EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
   @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,

   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,

   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,

   @COLUMND08, @COLUMND09, @COLUMND10

)  

--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Vendor Patment Header Values are Created in PUTABLE014 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )

declare @newID int, @HeaderID nvarchar(250)
DECLARE @AccountType nvarchar(250),@BAL decimal(18,2),@Totamt decimal(18,2),@dueamt decimal(18,2),@Project nvarchar(250),@paidamt decimal(18,2),@Projectactualcost decimal(18,2)
set @HeaderID =(select COLUMN01 from  PUTABLE014 where column02=@column02);
if not exists(select COLUMN05  from PUTABLE016 where COLUMN09=@COLUMN04 and  COLUMN07=@COLUMN05 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0)
BEGIN
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN25 as decimal(18,2))>0)
begin
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
if(iif((@COLUMN20 is null)  ,0,@COLUMN20)!=22306)
begin
--EMPHCS1410	Logs Creation by srinivas
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Payable Values are Intiated for PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +',2000'+  isnull(@COLUMN18,'')+','+  isnull(@HeaderID,'')+',BILL PAYMENT'+ isnull(@COLUMN05,'')+','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN04,'')+',Paid,'+  isnull(@COLUMN25,'')+','+  isnull(@COLUMN25,'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN23,'')+'') 			
   )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HeaderID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN18,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN23,	@COLUMN14 = '0',       @COLUMN15 = @COLUMN25,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1

insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03 , COLUMN20,COLUMNA13)
			values(@newID,2000,@COLUMN18,@HeaderID,'BILL PAYMENT',@COLUMN05,@COLUMN08,@COLUMN04,'Paid',@COLUMN25,@COLUMN25,@COLUMN10,@COLUMN13, @COLUMNA03,@COLUMN23,@COLUMNA13)
			--EMPHCS1410	Logs Creation by srinivas	
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Payable Values are Created in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
end
else
	begin
			set @Totamt=(select COLUMN12 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02) 
            update PUTABLE016 set COLUMN04=@COLUMN18,
			--COLUMN12=(cast(isnull(@Totamt,0) as int)+cast(isnull(@COLUMN04,0) as int)),
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@COLUMN25,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@COLUMN25,0) as decimal(18,2))) where COLUMN09=@COLUMN04 and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			--update PUTABLE016 set column10=@APStatus  where COLUMN09=@BNO and COLUMNA03=@COLUMNA03 and COLUMN20=@COLUMN23
			SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)as decimal(18,2)) - cast(@COLUMN25 as decimal(18,2)))	
					--EMPHCS1410	Logs Creation by srinivas	
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Payable Values are Updated in PUTABLE016 Table By '+cast(isnull(@COLUMN25,0) as decimal(18,2))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end


SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)as decimal(18,2)) - cast(@COLUMN25 as decimal(18,2)))	
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08 and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
if(cast(@COLUMN25 as decimal(18,2))>0)
begin
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
if(iif((@COLUMN20 is null)  ,0,@COLUMN20)!=22306)
begin
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
if(@AccountType=22409)
begin
if exists(select COLUMN05  from FITABLE052 where COLUMN05=@COLUMN04 and  COLUMN08=@COLUMN05 and COLUMN09=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0)
begin
	update FITABLE052 set COLUMN04=@COLUMN18,COLUMN11=@COLUMN25,COLUMN16=@COLUMN23,COLUMN10=@COLUMN10 where COLUMN05=@COLUMN04 and  COLUMN08=@COLUMN05 and COLUMN09=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Updated in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Intiated for FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN18,'')+','+ isnull(@COLUMN04,'')+',BILL PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@HeaderID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMN25,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN23,'')+'') 			
   )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HeaderID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN18,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN23,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1

set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE052) as int)+1
if(@newID='' or isnull(@newID,0)=0) set @newID=(1000)
insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08) 
values(@newID,@COLUMN08,@COLUMN18,@COLUMN04,'BILL PAYMENT',@COLUMN20,@COLUMN05,@HeaderID,@COLUMN10,@COLUMN25,@BAL, @COLUMNA02, @COLUMNA03,@COLUMN23, @COLUMNA08)
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Created in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
else
begin
if exists(select COLUMN05  from PUTABLE019 where COLUMN05=@COLUMN04 and  COLUMN07=@COLUMN05 and COLUMN08=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0)
begin
	update PUTABLE019 set COLUMN04=@COLUMN18,COLUMN10=@COLUMN25,COLUMN20=@COLUMN23 where COLUMN05=@COLUMN04 and COLUMN08=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Updated in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN18,'')+','+ isnull(@COLUMN04,'')+',BILL PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@HeaderID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMN25,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN23,'')+'') 			
   )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HeaderID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN18,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN23,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE019) as int)+1
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20, COLUMNA08) 
values(@newID,@COLUMN08,@COLUMN18,@COLUMN04,'BILL PAYMENT',@COLUMN05,@HeaderID,'',@COLUMN25,@BAL, @COLUMNA02, @COLUMNA03,@COLUMN23, @COLUMNA08)
 --EMPHCS1410	Logs Creation by srinivas		
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
UPDATE FITABLE001 SET COLUMN10=cast( @BAL as decimal(18,2)) WHERE COLUMN02=@COLUMN08  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Increased By '+cast(@BAL as nvarchar(15))+' in FITABLE001 Table For '+cast(@COLUMN08 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
if(cast(@COLUMN25 as nvarchar)!='' and cast(isnull(@COLUMN25,0) as decimal(18,2))!=0)
begin
set @Projectactualcost=(select isnull(column09,0) from PRTABLE001 where column02=@COLUMN23 and COLUMNA03=@COLUMNA03)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN25 as decimal(18,2))) where column02=@COLUMN23 and COLUMNA03=@COLUMNA03
end
Declare @Transid nvarchar(250)
set @Transid=(cast((select MAX(isnull(COLUMN02,999)) from PUTABLE011) as int)+1);
insert into PUTABLE011 

( COLUMN02,  COLUMN03,    COLUMN05, COLUMN15,  COLUMN16,

   --COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,   COLUMN07,    COLUMN11,  COLUMN12,    COLUMN14,  COLUMN15, COLUMN16,  COLUMN17,

   --COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,

   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,

   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10

)

values
(  @Transid,  'PAY BILL',     @COLUMN18,'4','4',
   --  @COLUMN03,  @COLUMN04,  @COLUMN05,   @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN15,

   --@COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,
     @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,

   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,

   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,

   @COLUMND08, @COLUMND09, @COLUMND10

)  
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(Select COLUMN01 from PUTABLE014 where COLUMN02=@COLUMN02)

END

 

IF @Direction = 'Select'

BEGIN

select * from PUTABLE014

END 

 

IF @Direction = 'Update'

BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN17=(1001)
--EMPHCS1593	payment calculation regarding bank selection in Bill Payment and Customer payment By RAj.Jr
    if((@COLUMN08='' or @COLUMN08=null))
  begin
  set @COLUMN08=0
  end
  if((@COLUMN20='' or @COLUMN20=null))
  begin
  set @COLUMN20=22305
  end
  declare @PBillId NVARCHAR(250),@PreAmt decimal(18,2),@PAcc nvarchar(250)
set @PAcc=(SELECT COLUMN08 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
set @PBillId=(select COLUMN04 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
set @PreAmt=(select isnull(COLUMN25,0) FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
UPDATE PUTABLE014 SET

   COLUMN02=@COLUMN02,   COLUMN03=@COLUMN03,   COLUMN04=@COLUMN04, 
  --  COLUMN05=@COLUMN05,
	  COLUMN06=@COLUMN06,  COLUMN07=@COLUMN07,  COLUMN08=@COLUMN08,  COLUMN09=@COLUMN09,

   COLUMN10=@COLUMN10,   COLUMN11=@COLUMN11,   COLUMN12=@COLUMN12,  COLUMN13=@COLUMN13,  COLUMN14=@COLUMN14,  COLUMN15=@COLUMN15,  COLUMN16=@COLUMN16,  COLUMN17=@COLUMN17,
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   --EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
   COLUMN18=@COLUMN18,   COLUMN19=@COLUMN19,   COLUMN20=@COLUMN20,  COLUMN21=@COLUMN21,  COLUMN22=@COLUMN22,  COLUMN23=@COLUMN23,  COLUMN24=@COLUMN24,   COLUMN25=@COLUMN25,
   COLUMNA01=@COLUMNA01, COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03,COLUMNA04=@COLUMNA04,COLUMNA05=@COLUMNA05,COLUMNA07=@COLUMNA07,COLUMNA08=@COLUMNA08,            

   COLUMNA09=@COLUMNA09, COLUMNA10=@COLUMNA10, COLUMNA11=@COLUMNA11,COLUMNA12=@COLUMNA12,COLUMNA13=@COLUMNA13,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03=@COLUMNB03,

   COLUMNB04=@COLUMNB04, COLUMNB05=@COLUMNB05, COLUMNB06=@COLUMNB06,COLUMNB07=@COLUMNB07,COLUMNB08=@COLUMNB08,COLUMNB09=@COLUMNB09,COLUMNB10=@COLUMNB10,COLUMNB11=@COLUMNB11,

   COLUMNB12=@COLUMNB12, COLUMND01=@COLUMND01, COLUMND02=@COLUMND02,COLUMND03=@COLUMND03, COLUMND04=@COLUMND04,COLUMND05=@COLUMND05,COLUMND06=@COLUMND06,COLUMND07=@COLUMND07,

   COLUMND08=@COLUMND08, COLUMND09=@COLUMND09, COLUMND10=@COLUMND10

   WHERE COLUMN02 = @COLUMN02
   
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Vendor Patment Header Values are Updated in PUTABLE014 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
	--EMPHCS1208	CUSTOMER PAYMENT EDIT NOT WORKING FOR ACCOUNT TABLES BY SRINIVAS 25/09/2015
	DECLARE @INTERNAL nvarchar(250),@AMOUNT DECIMAL(18,2),@BillNo nvarchar(250)
	SET @INTERNAL=(SELECT COLUMN01 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN05=(SELECT COLUMN05 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT SUM(isnull(COLUMN06,0)) FROM PUTABLE015 WHERE COLUMN08 IN(@INTERNAL) and COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
	SET @BillNo=(SELECT max(COLUMN03) FROM PUTABLE015 WHERE COLUMN08 IN(@INTERNAL) and COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	DELETE FROM FITABLE064 where COLUMN05 = @INTERNAL AND COLUMN03 in('BILL PAYMENT','COMMISSION PAYMENT') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	if(@BillNo='0' or @BillNo='' or @BillNo=null)
	begin
	delete from FITABLE027 where COLUMN04='COMMISSION PAYMENT' and COLUMN09=@PBillId and COLUMN05 in(@INTERNAL) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	end
	UPDATE FITABLE001 SET COLUMN10=(SELECT COLUMN10 FROM FITABLE001 WHERE COLUMN02=@PAcc)+@AMOUNT WHERE COLUMN02=@PAcc and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
	--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Reset By '+cast(@AMOUNT as nvarchar(15))+' in FITABLE001 Table For '+cast(@PAcc as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
	--UPDATE PUTABLE016 SET COLUMNA13=1 WHERE COLUMN05=@INTERNAL AND COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--UPDATE PUTABLE019 SET COLUMNA13=1 WHERE COLUMN05=@COLUMN04 AND COLUMN08=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	set @HeaderID =(select COLUMN01 from  PUTABLE014 where column02=@column02);
	delete from PUTABLE016  WHERE  COLUMN09=@PBillId and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Payable Values Deleted in PUTABLE016 Table For '+cast(@COLUMN04 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
	delete from PUTABLE019  WHERE COLUMN05=@PBillId AND COLUMN08=@HeaderID AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	delete from FITABLE052  WHERE COLUMN05=@PBillId AND COLUMN09=@HeaderID AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values Deleted in PUTABLE019 Table For '+cast(@COLUMN04 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
if not exists(select COLUMN05  from PUTABLE016 where COLUMN09=@PBillId and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
BEGIN
--EMPHCS1569	when settingoff with credit amount if payment is 0.00 bank not mandatory and not display record in bankregister and account payable BY RAJ.Jr
if(cast(@COLUMN25 as decimal(18,2))>0)
begin
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
if(iif((@COLUMN20 is null)  ,0,@COLUMN20)!=22306)
begin

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HeaderID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN18,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN23,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN25,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1
insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03 , COLUMN20,COLUMNA13)
			values(@newID,2000,@COLUMN18,@HeaderID,'BILL PAYMENT',@COLUMN05,@COLUMN08,@COLUMN04,'OPEN',@COLUMN25,@COLUMN25,@COLUMN10,@COLUMN13, @COLUMNA03,@COLUMN23,@COLUMNA13)
			--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Payable Values are Created in PUTABLE016 Table For '+cast(@COLUMN04 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
end
else
	begin
			set @Totamt=(select COLUMN12 from PUTABLE016 where COLUMN09=@PBillId and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@PBillId and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@PBillId and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02) 
            update PUTABLE016 set COLUMN04=@COLUMN18,
			--COLUMN12=(cast(isnull(@Totamt,0) as int)+cast(isnull(@COLUMN04,0) as int)),
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@COLUMN25,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@COLUMN25,0) as decimal(18,2))) where COLUMN09=@PBillId and COLUMN05=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			--update PUTABLE016 set column10=@APStatus  where COLUMN09=@BNO and COLUMNA03=@COLUMNA03 and COLUMN20=@COLUMN23
end

SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08 and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
if(cast(@COLUMN25 as decimal(18,2))>0)
begin
--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
if(iif((@COLUMN20 is null)  ,0,@COLUMN20)!=22306)
begin
if(@AccountType=22409)
begin
if exists(select COLUMN05  from FITABLE052 where COLUMN05=@COLUMN04 and  COLUMN08=@COLUMN05 and COLUMN09=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0)
begin
	update FITABLE052 set COLUMN04=@COLUMN18,COLUMN11=@COLUMN25,COLUMN16=@COLUMN23,COLUMN10=@COLUMN10 where COLUMN05=@COLUMN04 and  COLUMN08=@COLUMN05 and COLUMN09=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Updated in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Intiated for FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN18,'')+','+ isnull(@COLUMN04,'')+',BILL PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@HeaderID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMN25,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN23,'')+'') 			
   )

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HeaderID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN18,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN23,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE052) as int)+1
if(@newID='' or isnull(@newID,0)=0) set @newID=(1000)
insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08) 
values(@newID,@COLUMN08,@COLUMN18,@COLUMN04,'BILL PAYMENT',@COLUMN20,@COLUMN05,@HeaderID,@COLUMN10,@COLUMN25,@BAL, @COLUMNA02, @COLUMNA03,@COLUMN23, @COLUMNA08)
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Created in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
else
begin
if exists(select COLUMN05  from PUTABLE019 where COLUMN05=@COLUMN04 and  COLUMN07=@COLUMN05 and COLUMN08=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 AND ISNULL(COLUMNA13,0)=0)
begin
	update PUTABLE019 set COLUMN04=@COLUMN18,COLUMN10=@COLUMN25,COLUMN20=@COLUMN23 where COLUMN05=@PBillId and COLUMN08=@HeaderID and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
			set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Updated in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(10)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN18,'')+','+ isnull(@COLUMN04,'')+',BILL PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@HeaderID as nvarchar(20)),'')+','+  cast(@BAL as nvarchar(20))+','+  isnull(@COLUMN25,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN23,'')+'') 			
   )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HeaderID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN18,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN23,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE019) as int)+1
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20, COLUMNA08) 
values(@newID,@COLUMN08,@COLUMN18,@COLUMN04,'BILL PAYMENT',@COLUMN05,@HeaderID,'',@COLUMN25,@BAL, @COLUMNA02, @COLUMNA03,@COLUMN23, @COLUMNA08)
 --EMPHCS1410	Logs Creation by srinivas		
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
SET @BAL=(cast((select column10 from FITABLE001 where column02= @COLUMN08 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)as decimal(18,2)) - cast(@COLUMN25 as decimal(18,2)))	
UPDATE FITABLE001 SET COLUMN10=cast( @BAL as decimal(18,2)) WHERE COLUMN02=@COLUMN08  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Increased By '+cast(@BAL as nvarchar(15))+' in FITABLE001 Table For '+cast(@COLUMN08 as nvarchar(20))+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
end
if(cast(@COLUMN25 as nvarchar)!='' and cast(isnull(@COLUMN25,0) as decimal(18,2))!=0)
begin
set @Projectactualcost=(select isnull(column09,0) from PRTABLE001 where column02=@COLUMN23 and COLUMNA03=@COLUMNA03)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN25 as decimal(18,2))-cast(iif(@PreAmt='',0,isnull(@PreAmt,0)) as decimal(18,2))) where column02=@COLUMN23 and COLUMNA03=@COLUMNA03
end
	declare @retAmnt decimal(18,2),@id int,@id1 int
	declare @cretAmnt decimal(18,2)
	SET @COLUMN16=(SELECT sum(isnull(COLUMN16,0)) FROM PUTABLE015 WHERE COLUMN08 =(@INTERNAL))
	SET @COLUMN14=(SELECT sum(isnull(COLUMN14,0)) FROM PUTABLE015 WHERE COLUMN08 =(@INTERNAL))
	--SET @COLUMN03=(SELECT COLUMN03 FROM PUTABLE015 WHERE COLUMN08 =(@INTERNAL))
	--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
	declare @VInitialrow int,@vid nvarchar(250),@vMaxRownum int,@VoucherCrdAmt nvarchar(250),@VochrAmt decimal(18,2),
	@Voucherids nvarchar(250),@MainString nvarchar(250),@Vochertype nvarchar(250),@delimiterstr nvarchar(250)
	,@Vocherid nvarchar(250),@VTransno nvarchar(250),@TransAmt decimal(18,2),@PrevAmt decimal(18,2)
SET @VInitialrow = 1
		  DECLARE curv CURSOR FOR SELECT COLUMN02 from PUTABLE015 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0
		  OPEN curv
		  FETCH NEXT FROM curv INTO @vid
		  SET @vMaxRownum = (SELECT COUNT(*) FROM PUTABLE015 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0)
		     WHILE @vInitialrow <= @vMaxRownum
		     BEGIN 
			 set @VoucherCrdAmt=(SELECT isnull(COLUMN16,0) from PUTABLE015 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
			 set @Voucherids=(SELECT isnull(COLUMN18,0) from PUTABLE015 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
	if(@VoucherCrdAmt!= '0' and @VoucherCrdAmt!= '0.00' and @VoucherCrdAmt!= '')
	begin
		set @VochrAmt=(@VoucherCrdAmt)
		set @MainString=(@Voucherids)
		DECLARE @cnt INT = 0,@vochercnt INT = 1;
		
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='D')
		begin
		set @VTransno=(select column04 from PUTABLE001 where column02=@Vocherid)
		set @TransAmt=(select isnull(column15,0) from PUTABLE001 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from PUTABLE001 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from satable005 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update PUTABLE001 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update PUTABLE001 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		update PUTABLE001 set column16='OPEN' where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE020 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE020 where column02=@Vocherid)
		set @PrevAmt=(select cast(isnull(COLUMN10,0) as decimal(18,2))-cast(isnull(COLUMN18,0) as decimal(18,2)) from FITABLE020 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE020 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE020 set COLUMN18=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE020 set COLUMN18=@TransAmt where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='P')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN15,0) from FITABLE022 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE022 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE022 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE022 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN04,0) from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END
		set @COLUMN06=(select COLUMN06 from PUTABLE015 where column02=@vid)
		set @COLUMN19=(select COLUMN19 from PUTABLE015 where column02=@vid)
		set @COLUMN20=(select COLUMN20 from PUTABLE015 where column02=@vid)
		set @COLUMN16=(select COLUMN16 from PUTABLE015 where column02=@vid)
		                if(@COLUMN19='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='ReceiptVoucher')
				begin
				update FITABLE024 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='AdvanceReceipt')
				begin
				update FITABLE023 set COLUMN19=(cast(isnull(COLUMN19,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='CreditMemo')
				begin
				update SATABLE005 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
    FETCH NEXT FROM curv INTO @vid
		         SET @vInitialrow = @vInitialrow + 1 
			END
		CLOSE curv 
		deallocate curv
		
	--	if(@COLUMN14!= '0.00' and @COLUMN14!= '')
	--begin
	-- ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)
	-- DECLARE @MaxRowA1 INT,@d2 int
	--DECLARE @FirstRow1 INT=1
	--	declare @AdvIDs nvarchar(250)
	-- DECLARE curUP CURSOR FOR SELECT COLUMN02 from PUTABLE015 where COLUMN08 in(@INTERNAL) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
 --     OPEN curUP
	--  FETCH NEXT FROM curUP INTO @d2
 --     SET @MaxRowA1 = (SELECT COUNT(*) FROM PUTABLE015 where COLUMN08 in(@INTERNAL) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
 --        WHILE @FirstRow1 <= @MaxRowA1
 --        BEGIN
	--	 set @AdvIDs=(select COLUMN17 from PUTABLE015 where COLUMN02=@d2 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
	--DECLARE @MaxRownum INT
 --     DECLARE @Initialrow INT=1
	--		  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
	--		  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
	--		  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@COLUMN05 AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
	--		  order by FITABLE020.column02 asc
	--	  OPEN curA
	--	  FETCH NEXT FROM curA INTO @id
	--	  if(@id='' or @id is not null)
	--	     BEGIN 
	--	        if(@COLUMN14!='0' and cast(@COLUMN14 as decimal(18,2))>0)
	--		  begin
	--		  set @MaxRownum=(select  COUNT(*) from FITABLE020 
	--		  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
	--		  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@COLUMN05 and FITABLE020.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0)
	--	         WHILE @Initialrow <= @MaxRownum
	--		  begin
	--		  if(cast(@COLUMN14 as DECIMAL(18,2))!=0 and @MaxRownum>0)
	--		  begin
	--			set @cretAmnt=(select column10 from FITABLE020 where column02= @id  
	--			 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
	--			if(cast(@COLUMN14 as DECIMAL(18,2))>=@cretAmnt)
	--			begin
	--				update FITABLE020 set column18=(@cretAmnt), COLUMN16='OPEN'  where column02= @id  
	--				and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
	--				--EMPHCS1410	Logs Creation by srinivas	
	--					set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	--					'Advance Amount Reset in FITABLE020 Table by '+cast(@cretAmnt as nvarchar(15))+' for '+cast(@id as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	--					)

	--				set @COLUMN14=(cast(@COLUMN14 as decimal(18,2))-@cretAmnt)
	--			enD
	--			 else if(cast(@COLUMN14 as DECIMAL(18,2))<@cretAmnt)				 
	--			 begin
	--				update FITABLE020 set column18=(cast(isnull(@COLUMN14,0) as DECIMAL(18,2))+cast(isnull(column18,0) as DECIMAL(18,2))), COLUMN16='OPEN'  where column02= @id
	--			 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
	--			 --EMPHCS1410	Logs Creation by srinivas	
	--					set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	--					'Advance Amount Reset in FITABLE020 Table by '+cast(@COLUMN14 as nvarchar(15))+' for '+cast(@id as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	--					)

	--				set @COLUMN14='0'
	--			 end
	--			 end
	--			FETCH NEXT FROM curA INTO @id
	--			SET @Initialrow = @Initialrow + 1
	--			end
	--			CLOSE curA 
	--			deallocate curA
	--		END
	--		end
	--		FETCH NEXT FROM curUP INTO @d2
 --            SET @FirstRow1 = @FirstRow1 + 1 
	--		 end
 --       CLOSE curUP
 --       deallocate curUP
	--	end
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
			UPDATE PUTABLE015 SET COLUMNA13=1 WHERE COLUMN08 in(@INTERNAL) 
delete from FITABLE026 where column05 in(@INTERNAL) and column04='BILL PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where COLUMN09=@PBillId and COLUMN03='BILL PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END
else IF @Direction = 'Delete'

BEGIN
declare @d1 int
UPDATE PUTABLE014 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Payment Header Data Deleted in PUTABLE014 for '+isnull(@COLUMN02,'')+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	--EMPHCS1208	CUSTOMER PAYMENT EDIT NOT WORKING FOR ACCOUNT TABLES BY SRINIVAS 25/09/2015
	SET @COLUMN08=(SELECT COLUMN08 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @COLUMNA02=(SELECT COLUMNA02 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @COLUMNA03=(SELECT COLUMNA03 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @INTERNAL=(SELECT COLUMN01 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN04=(SELECT COLUMN04 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT SUM(COLUMN06) FROM PUTABLE015 WHERE COLUMN08 IN(@INTERNAL))
	--EMPHCS1577	in Payment if paying to salesrep Record not insert into Accounts Payable rather insert into Commission table By RAJ.JR
	SET @BillNo=(SELECT max(COLUMN03) FROM PUTABLE015 WHERE COLUMN08 IN(@INTERNAL))
	DELETE FROM FITABLE064 where COLUMN05 = @INTERNAL AND COLUMN03 in('BILL PAYMENT','COMMISSION PAYMENT') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	if(@BillNo='0' or @BillNo='' or @BillNo=null)
	begin
	delete from FITABLE027 where COLUMN04='COMMISSION PAYMENT' and COLUMN09=@COLUMN04 and COLUMN05 in(@INTERNAL) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	end
	UPDATE FITABLE001 SET COLUMN10=(SELECT COLUMN10 FROM FITABLE001 WHERE COLUMN02=@COLUMN08)+@AMOUNT WHERE COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Bank Amount Increased By '+cast(@AMOUNT as nvarchar(20))+' in FITABLE001 for '+isnull(cast(@COLUMN08 as nvarchar(20)),'')+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	DELETE PUTABLE016 WHERE COLUMN05=@INTERNAL AND COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Account Payable Data Deleted in PUTABLE016 for '+isnull(@COLUMN04,'')+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	DELETE PUTABLE019 WHERE COLUMN05=@COLUMN04 AND COLUMN08=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	delete from FITABLE052  WHERE COLUMN05=@COLUMN04 AND COLUMN09=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Bank Register Data Deleted in PUTABLE019 for '+@COLUMN04+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)	
    SET @COLUMN23=(SELECT COLUMN23 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
    SET @COLUMN25=(SELECT COLUMN25 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
    if(cast(@COLUMN25 as nvarchar)!='' and cast(isnull(@COLUMN25,0) as decimal(18,2))!=0)
    begin
    set @Projectactualcost=(select isnull(column09,0) from PRTABLE001 where column02=@COLUMN23 and COLUMNA03=@COLUMNA03)
    update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))-cast(@COLUMN25 as decimal(18,2))) where column02=@COLUMN23 and COLUMNA03=@COLUMNA03
    end
	--UPDATE PUTABLE015 SET COLUMNA13=1 WHERE COLUMN08 = @INTERNAL
	
	----EMPHCS1410	Logs Creation by srinivas
	--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	--'Payment Line Data Deleted in PUTABLE015 for '+cast(@INTERNAL as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	--)
	declare @CreditID nvarchar(250),@IdCr int
	SET @COLUMN16=(SELECT sum(isnull(COLUMN16,0)) FROM PUTABLE015 WHERE COLUMN08 =(@INTERNAL))
	SET @COLUMN14=(SELECT sum(isnull(COLUMN14,0)) FROM PUTABLE015 WHERE COLUMN08 =(@INTERNAL))
	--SET @COLUMN03=(SELECT COLUMN03 FROM PUTABLE015 WHERE COLUMN08 =(@INTERNAL))
	SET @COLUMN04=(SELECT COLUMN04 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN05=(SELECT COLUMN05 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
          --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
          SET @VInitialrow = 1
		  DECLARE curv CURSOR FOR SELECT COLUMN02 from PUTABLE015 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0
		  OPEN curv
		  FETCH NEXT FROM curv INTO @vid
		  SET @vMaxRownum = (SELECT COUNT(*) FROM PUTABLE015 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0)
		     WHILE @vInitialrow <= @vMaxRownum
		     BEGIN 
			 set @VoucherCrdAmt=(SELECT isnull(COLUMN16,0) from PUTABLE015 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
			 set @Voucherids=(SELECT isnull(COLUMN18,0) from PUTABLE015 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
	if(@VoucherCrdAmt!= '0' and @VoucherCrdAmt!= '0.00' and @VoucherCrdAmt!= '')
	begin
		set @VochrAmt=(@VoucherCrdAmt)
		set @MainString=(@Voucherids)
		set @cnt =(0);set @vochercnt = (1);
		
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='D')
		begin
		set @VTransno=(select column04 from PUTABLE001 where column02=@Vocherid)
		set @TransAmt=(select isnull(column15,0) from PUTABLE001 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from PUTABLE001 where column02=@Vocherid)
		update PUTABLE001 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from satable005 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update PUTABLE001 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update PUTABLE001 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE020 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE020 where column02=@Vocherid)
		set @PrevAmt=(select cast(isnull(COLUMN10,0) as decimal(18,2))-cast(isnull(COLUMN18,0) as decimal(18,2)) from FITABLE020 where column02=@Vocherid)
		update FITABLE020 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE020 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE020 set COLUMN18=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE020 set COLUMN18=@TransAmt where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='P')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN15,0) from FITABLE022 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE022 where column02=@Vocherid)
		update FITABLE020 set COLUMN16='OPEN' where column01 in(select COLUMN09 from FITABLE022 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE022 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE022 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN04,0) from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END
		set @COLUMN06=(select COLUMN06 from PUTABLE015 where column02=@vid)
		set @COLUMN19=(select COLUMN19 from PUTABLE015 where column02=@vid)
		set @COLUMN20=(select COLUMN20 from PUTABLE015 where column02=@vid)
		set @COLUMN16=(select COLUMN16 from PUTABLE015 where column02=@vid)
		                if(@COLUMN19='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='ReceiptVoucher')
				begin
				update FITABLE024 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='AdvanceReceipt')
				begin
				update FITABLE023 set COLUMN19=(cast(isnull(COLUMN19,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN19='CreditMemo')
				begin
				update SATABLE005 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN16,0) as decimal(18,2))) where COLUMN02=@COLUMN20 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
    FETCH NEXT FROM curv INTO @vid
		         SET @vInitialrow = @vInitialrow + 1 
			END
		CLOSE curv 
		deallocate curv

		DECLARE @Initialrowd INT=1,@MaxRownum INT=1,@PIQty decimal(18,2),@PBQty decimal(18,2)
		set @Initialrowd=(1)
	SET @COLUMN08=(SELECT COLUMN01 FROM PUTABLE014 WHERE COLUMN02=@COLUMN02)
			--UPDATE PUTABLE015 SET COLUMNA13=1 WHERE COLUMN08 in(@COLUMN08) 
		DECLARE curd CURSOR FOR SELECT COLUMN02 from PUTABLE015 where COLUMN08 in(@COLUMN08) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0
		OPEN curd
		FETCH NEXT FROM curd INTO @id
		SET @MaxRownum = (SELECT COUNT(*) from PUTABLE015 where COLUMN08 in(@COLUMN08) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0)
		WHILE @Initialrowd <= @MaxRownum
        BEGIN   
		set @COLUMN03=(select isnull(column03,0)  from  PUTABLE015 where COLUMN02=@id)
		SET @PIQty=isnull((SELECT max(COLUMN04) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0),0)
		--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
		set @COLUMN20 =(select iif(isnull(COLUMN20,0)='',0,isnull(COLUMN20,0)) COLUMN20 from  PUTABLE015 Where COLUMN02=@id );
		if(@COLUMN20=0)
		begin
		SET @PBQty=isnull((SELECT  @PIQty-(sum(isnull(COLUMN06,0))+sum(isnull(COLUMN14,0))+sum(isnull(COLUMN16,0))) FROM PUTABLE015 WHERE COLUMN03=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0),0)
		IF(@PBQty>0)
		begin
		UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=17) where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0
		end
		else 
		begin
		UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=19) where COLUMN02=@COLUMN03 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(columna13,0)=0
	end	
		end
		 FETCH NEXT FROM curd INTO @id
             SET @Initialrowd = @Initialrowd + 1 
			 end
			 deallocate curd
			UPDATE PUTABLE015 SET COLUMNA13=1 WHERE COLUMN08 in(@COLUMN08) 
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
--delete from FITABLE026 where column05 in(@INTERNAL) and column04='BILL PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE026 where column05 in(@INTERNAL) and column04='BILL PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where COLUMN09=@PBillId and COLUMN03='BILL PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END

    --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_PUR_TP_PUTABLE014.txt',0
end try
begin catch

SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PUTABLE014.txt',0
return 0
end catch
end



























GO
