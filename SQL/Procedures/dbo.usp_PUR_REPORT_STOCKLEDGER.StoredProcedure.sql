USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_REPORT_STOCKLEDGER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_REPORT_STOCKLEDGER]
--(@COLUMN04 NVARCHAR)
AS
BEGIN
SELECT        f.COLUMN03,
                             (SELECT        COLUMN04
                               FROM            MATABLE007
                               WHERE        (COLUMN02 = f.COLUMN03)) AS item, f.COLUMN05, f.COLUMN06, f.COLUMN07, f.COLUMN08, f.COLUMN09, f.COLUMN12
FROM            FITABLE010 AS f CROSS JOIN
                         MATABLE007 AS m
GROUP BY f.COLUMN03, f.COLUMN05, f.COLUMN06, f.COLUMN07, f.COLUMN08, f.COLUMN09, f.COLUMN12
END



















GO
