USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[AdvanceReceiptGLImpact]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AdvanceReceiptGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
--select f1.COLUMN04 Account,F23.column11 Memo,0 Credit,sum(f26.COLUMN11) Debit  from FITABLE023 F23
--left outer join fitable026 f26 on f26.COLUMN09= F23.COLUMN04 and f26.COLUMN05= F23.COLUMN01 and f26.COLUMNA03= F23.COLUMNA03 and f26.COLUMNA02= F23.COLUMNA02 and f26.COLUMN04='ADVANCE RECEIPT' and isnull(f26.COLUMNA13,0)=0
--left outer join FITABLE001 f1 on f1.COLUMN02 = f26.COLUMN08
--where F23.COLUMN02=@ide and F23.COLUMNA03=@AcOwner and f1.COLUMNA03 = F23.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
--group by f1.COLUMN04,F23.column11
--union all
select f1.COLUMN04 Account,F23.column11 Memo,0 Credit,sum(p19.COLUMN11) Debit,1 id from FITABLE023 F23
left outer join putable019 p19 on p19.COLUMN05= F23.COLUMN04 and  p19.COLUMN08= F23.COLUMN01 and p19.COLUMNA03= F23.COLUMNA03 and p19.COLUMNA02= F23.COLUMNA02 and p19.COLUMN06='ADVANCE RECEIPT' and isnull(p19.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p19.COLUMN03
where F23.COLUMN02=@ide and F23.COLUMNA03=@AcOwner and f1.COLUMNA03 = F23.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
group by f1.COLUMN04,F23.column11
UNION ALL
select f1.COLUMN04 Account,F23.column11 Memo,0 Credit,sum(F52.COLUMN12) Debit,2 id from FITABLE023 F23
left outer join FITABLE052 F52 on F52.COLUMN05= F23.COLUMN04  and F52.COLUMNA03= F23.COLUMNA03 and F52.COLUMNA02= F23.COLUMNA02 and F52.COLUMN06='ADVANCE RECEIPT' and isnull(F52.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F52.COLUMN03
where F23.COLUMN02=@ide and F23.COLUMNA03=@AcOwner and f1.COLUMNA03 = F23.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
group by f1.COLUMN04,F23.column11
union all
select f26.COLUMN17 Account,F23.column11 Memo,sum(f26.COLUMN11) Credit,0 Debit,3 id from FITABLE023 F23
left outer join fitable026 f26 on f26.COLUMN09= F23.COLUMN04 and f26.COLUMN05= F23.COLUMN01 and f26.COLUMNA03= F23.COLUMNA03 and f26.COLUMNA02= F23.COLUMNA02 and f26.COLUMN04='ADVANCE RECEIPT' and isnull(f26.COLUMNA13,0)=0
left outer join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 and isnull(m13.COLUMNA13,0)=0
where F23.COLUMN02=@ide and F23.COLUMNA03=@AcOwner  and isnull(F23.COLUMNA13,0)=0 AND ISNULL(f26.COLUMN11,0)>0
group by f26.COLUMN17,F23.column11 
)
select [Account],[Memo],[Credit],[Debit],[id] from MyTable
group by [Account],[Memo],[Credit],[Debit],[id] ORDER BY ID ASC
end



GO
