USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[StockDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[StockDetails]
(@Item nvarchar(250)=null,
@UPC nvarchar(250)=null,
@OperatingUnit nvarchar(250)=null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project    nvarchar(250)= null )
as
begin
--EMPHCS1005		StockDetails and Stock ledger displaying deleted records  BY RAj.Jr 21/8/2015
if(@Item!='' and @UPC!='' and @OperatingUnit!='')
begin
	 select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom, b.COLUMN06 COLUMN10,o.COLUMN03 ou,
	 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where isnull(a.COLUMNA13,0)=0 and a.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN06=@upc and a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
end
else if(@Item!='' and @UPC!='')
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where isnull(a.COLUMNA13,0)=0 and  a.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN06=@upc AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
end
else if(@Item!=''  and @OperatingUnit!='')
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where isnull(a.COLUMNA13,0)=0 and  a.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  
and a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
end
else if( @UPC!='' and @OperatingUnit!='')
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where isnull(a.COLUMNA13,0)=0 and   b.COLUMN06=@upc and a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
end
else if(@Item!='' and @Item is not null )
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where isnull(a.COLUMNA13,0)=0 and  a.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
end
else if( @UPC!='')
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where isnull(a.COLUMNA13,0)=0 and   b.COLUMN06=@upc AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
end
else if( @OperatingUnit!='')
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and  isnull(b.COLUMNA13,0)=0
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
where  isnull(a.COLUMNA13,0)=0 and  a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND 
 a.COLUMNA03=@AcOwner
end
else 
begin
select a.COLUMN03 , b.column04  Item,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,b.COLUMN06 COLUMN10,o.COLUMN03 ou,
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
l.COLUMN04 Location,bm.COLUMN04 Lot,bm.COLUMN05 ExpiryDate,a.COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS929 16/8/2015 uom condition checking in Stock Ledger report by srinivas 
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
a.COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18 from FITABLE010 a inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0  and b.COLUMNA03= @AcOwner
inner  join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03
--EMPHCS1325 Rajasekhar reddy patakota 10/11/2015 Expiry date Should come in Stock ledger report
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMNA02=a.COLUMN13 and bm.COLUMNA03=a.COLUMNA03
 AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner order by COLUMN03 desc 
end
end











GO
