USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_VENDORPAYBILL_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_VENDORPAYBILL_LINE_DATA]
(
	@PurchaseOrderID nvarchar(250),@AcOwner int=null,
	@OPUNIT int=null
)

AS
BEGIN

declare @DueAmnt INT
declare @tmp INT
declare @DueAmnt1 int
declare @PayAmnt int
declare @PayAmnt1 int,@FrDate date = '1/1/2012',@ToDate date = '1/1/2200'
select top 1 @FrDate=f48.column04,@ToDate=f48.column05 from FITABLE048 f48
inner join SATABLE001 s1 on s1.column02 = @PurchaseOrderID
where f48.COLUMNA03= s1.COLUMNA03 and f48.column09 = '1' and isnull(f48.columna13,0)=0 
--EMPHCS910 rajasekhar reddy patakota 11/8/2015 Partial payment did then selected vendor in payment screen it is showing full due amounts againest bills 
set @DueAmnt1=(select (isnull(sum(COLUMN06),0)+isnull(sum(COLUMN16),0)+isnull(sum(COLUMN14),0)) AS COLUMN05 from PUTABLE015 WHERE  COLUMN08 in(select isnull((COLUMN01),0) 
from PUTABLE014 where COLUMN05=@PurchaseOrderID  and isnull(columna13,0)=0 and COLUMN18 <= @ToDate )  and isnull(columna13,0)=0)
--end
if(@DueAmnt1=0)
begin
select distinct b.COLUMN02 as COLUMN03,CONVERT(VARCHAR(10),max(b.column08),103)+' '+(max(b.column12)) COLUMN07,
--(case when b.COLUMN66=1 THEN (max(b.COLUMN14)-max(ISNULL(b.COLUMN63,0))) ELSE max(b.COLUMN14) END) 
(CASE 
WHEN (b.COLUMN66='1' AND b.COLUMN58='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
WHEN (b.COLUMN66='1' AND b.COLUMN58='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (b.COLUMN66='0' AND b.COLUMN58='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END)
as COLUMN04,
(CASE 
WHEN (b.COLUMN66='1' AND b.COLUMN58='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
WHEN (b.COLUMN66='1' AND b.COLUMN58='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (b.COLUMN66='0' AND b.COLUMN58='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END) as COLUMN05,
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=@PurchaseOrderID and (column11=b.COLUMN04 or column11=b.COLUMN02) and COLUMNA03=b.COLUMNA03 and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0) as COLUMN16,
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=@PurchaseOrderID and column11='' and  COLUMN03='1355'and column16!='CLOSE' and isnull(columna13,0)=0) as directReturn,'' AS COLUMN06
 ,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=@PurchaseOrderID and (column11=b.COLUMN04 or column11=b.COLUMN02) and COLUMNA03=b.COLUMNA03 and  COLUMN03='1355'and column16!='CLOSE' and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN18,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=@PurchaseOrderID and column11='' and  COLUMN03='1355'and column16!='CLOSE' and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol

	from PUTABLE006 a inner join PUTABLE005 b on b.COLUMN01=a.COLUMN13 and isnull(b.columna13,0)=0 and a.columna03=b.columna03 and b.COLUMN08 <= @ToDate 
	 WHERE  a.COLUMN13 in(select COLUMN01 from PUTABLE005 where COLUMN05=@PurchaseOrderID and isnull(COLUMNA13,0)=0)  and isnull(a.COLUMNA13,0)=0 and a.COLUMNA03=@AcOwner and a.COLUMNA02 in(@OPUNIT)
	  --EMPHCS910 rajasekhar reddy patakota 11/8/2015 Partial payment did then selected vendor in payment screen it is showing full due amounts againest bills 
	    group by b.COLUMN02,b.COLUMN03,b.COLUMN04,b.COLUMNA03,b.COLUMN66,b.COLUMN58
	 end
else if(@DueAmnt1!=0 )
begin
select distinct c.COLUMN02 as COLUMN03,CONVERT(VARCHAR(10),max(c.column08),103)+' '+(max(c.column12)) COLUMN07,
--(case when c.COLUMN66=1 THEN (max(c.COLUMN14)-max(ISNULL(c.COLUMN63,0))) ELSE (isnull(max (a.COLUMN04),max (c.COLUMN14))) END)
(CASE 
WHEN (c.COLUMN66='1' AND c.COLUMN58='1') THEN (max(c.COLUMN14)-max(c.COLUMN63)-max(c.COLUMN24)) 
WHEN (c.COLUMN66='1' AND c.COLUMN58='0') THEN (max(c.COLUMN14)-max(c.COLUMN63)) 
WHEN (c.COLUMN66='0' AND c.COLUMN58='1')  THEN (max(c.COLUMN14)-max(c.COLUMN24))
ELSE (isnull(max (a.COLUMN04),max (c.COLUMN14))) END)
 as COLUMN04,   
 (CASE 
WHEN (c.COLUMN66='1' AND c.COLUMN58='1') THEN (max(c.COLUMN14)-max(c.COLUMN63)-max(c.COLUMN24)) 
WHEN (c.COLUMN66='1' AND c.COLUMN58='0') THEN (max(c.COLUMN14)-max(c.COLUMN63)) 
WHEN (c.COLUMN66='0' AND c.COLUMN58='1')  THEN (max(c.COLUMN14)-max(c.COLUMN24))
ELSE (isnull(max (a.COLUMN04),max (c.COLUMN14))) END)-(cast(sum(isnull(a.COLUMN06,0)) as DECIMAL(18,2))+cast(sum(isnull(a.COLUMN16,0 ))as DECIMAL(18,2))+cast(sum(isnull(a.COLUMN14,0 ))as DECIMAL(18,2))) as COLUMN05,
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=@PurchaseOrderID and (column11=c.COLUMN04 or column11=c.COLUMN02) and COLUMNA03=c.COLUMNA03 and  COLUMN03='1355'and column16!='CLOSE' and isnull(columna13,0)=0) as COLUMN16,
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=@PurchaseOrderID and column11='' and  COLUMN03='1355'and column16!='CLOSE' and isnull(columna13,0)=0) as directReturn,'' AS COLUMN06
 ,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=@PurchaseOrderID and (column11=c.COLUMN04 or column11=c.COLUMN02) and COLUMNA03=a.COLUMNA03 and  COLUMN03='1355'and column16!='CLOSE' and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN18,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=@PurchaseOrderID and column11='' and  COLUMN03='1355'and column16!='CLOSE' and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol

	from  PUTABLE005 c 
	left outer join PUTABLE015 a on  a.COLUMN03=c.COLUMN02  and isnull(a.columna13,0)=0  and a.columna03=c.columna03
	--EMPHCS910 rajasekhar reddy patakota 11/8/2015 Partial payment did then selected vendor in payment screen it is showing full due amounts againest bills 
	where  c.COLUMN05=@PurchaseOrderID	and isnull(a.COLUMNA13,0)=0 	and isnull(c.COLUMNA13,0)=0 and c.COLUMN08 <= @ToDate
	and c.COLUMNA03=@AcOwner and c.COLUMNA02 in(@OPUNIT)
	 group by c.COLUMN02,c.COLUMN04,c.COLUMN03,c.COLUMNA03,a.COLUMNA03,c.COLUMN66,c.COLUMN58
	  having ( SELECT CASE WHEN max(a.COLUMN04)>0 THEN max (c.COLUMN14)-(cast(sum(isnull(a.COLUMN06,0)) as DECIMAL(18,2))+cast(sum(isnull(a.COLUMN16,0 ))as DECIMAL(18,2))+cast(sum(isnull(a.COLUMN14,0 ))as DECIMAL(18,2))) ELSE sum(c.COLUMN14) END)!=0
	  end
	 else
	 begin
	select distinct a.COLUMN03 as COLUMN03, max(cast(a.COLUMN04 as int)) as COLUMN04,    0 as COLUMN05
	from PUTABLE015 a inner join PUTABLE014 b on a.COLUMN08=b.COLUMN01   and isnull(b.columna13,0)=0 and b.COLUMN18 <= @ToDate
	WHERE a.COLUMN08 in(select COLUMN01 from PUTABLE014 where COLUMN05=@PurchaseOrderID )    and isnull(a.columna13,0)=0 
	and a.COLUMNA03=@AcOwner and a.COLUMNA02 in(@OPUNIT)
	 --EMPHCS910 rajasekhar reddy patakota 11/8/2015 Partial payment did then selected vendor in payment screen it is showing full due amounts againest bills 
	  group by a.COLUMN03 
	 end
END




















GO
