USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PRTABLE007]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PRO_TP_PRTABLE007]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  --@COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE007_SequenceNo
insert into PRTABLE007 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN23,  COLUMN24, 
   COLUMNA01, COLUMNA02, 
   COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  1005,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21, 
   @COLUMN23,   @COLUMN24,   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
) 
update PRTABLE005 set COLUMN21='Dispatched' where COLUMN02=@COLUMN05 and COLUMNA03=@COLUMNA03 and ISNULL(COLUMNA13,0)=0
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application 
set @ReturnValue = (select COLUMN01 from PRTABLE007 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from PRTABLE007

END 

 

ELSE IF @Direction = 'Update'

BEGIN

UPDATE PRTABLE007 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=1005,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,  
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,  
   COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
	  --EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
	  declare @internalid nvarchar(250)=null,@chk bit
      DECLARE @MaxRownum INT,@PID nvarchar(250),@id nvarchar(250),@ItemQty nvarchar(250),@Item nvarchar(250),@uom nvarchar(250)
	  declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
      @Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2),@Project nvarchar(250),@Amount  decimal(18,2),@Projectactualcost  decimal(18,2)
      DECLARE @Initialrow INT=1
	  set @PID=(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
      set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN02=(@COLUMN02))
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN  

		  set @Item=(select COLUMN03 from PRTABLE008 where COLUMN02=@id)
          set @ItemQty=(select isnull(COLUMN06,0) from PRTABLE008 where COLUMN02=@id)
          set @Price=(select isnull(COLUMN08,0) from PRTABLE008 where COLUMN02=@id)
		  set @uom=(select COLUMN11 from PRTABLE008 where COLUMN02=@id)
		  set @COLUMN13=(@COLUMN11)
		  --EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
		  set @chk=(select column48 from matable007 where column02=@Item)
		  set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@id)
		set @Amount=(cast(@ItemQty as DECIMAL(18,2))*cast(@Price as DECIMAL(18,2)))
		set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
		update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))-cast(@Amount as DECIMAL(18,2))) where column02=@Project
		--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
		 if(@chk=1)
		 begin
		 set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)))
				 if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@Price as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN17=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
				end
				end
				--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
		 end
		 else
		 begin
		 --EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
		  UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN05=cast(@internalid as nvarchar(250)) and COLUMN03='Resource Consumption' AND COLUMN09=cast(@COLUMN04 as nvarchar(250)) AND COLUMN10=1078 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
         
        CLOSE cur1
        deallocate cur1

delete from PUTABLE017  WHERE COLUMN05 in(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02) and COLUMN06='Resource Consumption'  and COLUMN14=@Project and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
UPDATE PRTABLE008 SET COLUMNA13=1 WHERE COLUMN10 in(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
 update PRTABLE005 set COLUMN21='Dispatched' where COLUMN02=@COLUMN05 and COLUMNA03=@COLUMNA03 and ISNULL(COLUMNA13,0)=0
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
BEGIN
UPDATE PRTABLE007 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02

set @Initialrow =1
	  set @PID=(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
      set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN02=(@COLUMN02))
      --EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
	  set @COLUMN13=((select COLUMN11 from PRTABLE007 WHERE COLUMN02 = @COLUMN02))
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PRTABLE008 where COLUMN10 in(@PID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN  
		--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion	
		  set @COLUMN04=(select COLUMN04 from PRTABLE008 where COLUMN02=@id)
		  set @Item=(select COLUMN03 from PRTABLE008 where COLUMN02=@id)
          set @ItemQty=(select isnull(COLUMN06,0) from PRTABLE008 where COLUMN02=@id)
          set @Price=(select isnull(COLUMN08,0) from PRTABLE008 where COLUMN02=@id)
		  set @uom=(select COLUMN11 from PRTABLE008 where COLUMN02=@id)
		  --EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
		  set @chk=(select column48 from matable007 where column02=@Item)
		  set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@id)
		set @Amount=(cast(@ItemQty as DECIMAL(18,2))*cast(@Price as DECIMAL(18,2)))
		set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
		update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))-cast(@Amount as DECIMAL(18,2))) where column02=@Project
		--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
		 if(@chk=1)
		 begin
		 set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)))
				 if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@Price as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN17=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom
				end
				end
				--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
			 end
		 else
		 begin
		 --EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
		  UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN05=cast(@internalid as nvarchar(250)) and COLUMN03='Resource Consumption' AND COLUMN09=cast(@COLUMN04 as nvarchar(250)) AND COLUMN10=1078 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
         
        CLOSE cur1
        deallocate cur1

delete from PUTABLE017  WHERE COLUMN05 in(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02) and COLUMN06='Resource Consumption'  and COLUMN14=@Project and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
UPDATE PRTABLE008 SET COLUMNA13=1 WHERE COLUMN10 in(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)
--EMPHCS1252 gnaneshwar on 7/10/2015  Inserting Data Into Cost Of Sales When Resourse Consumption Created in Project
UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from PRTABLE008 where COLUMN10 in(select COLUMN01 from PRTABLE007 WHERE COLUMN02 = @COLUMN02)) 

END

end try
begin catch
return 0
end catch
end


















GO
