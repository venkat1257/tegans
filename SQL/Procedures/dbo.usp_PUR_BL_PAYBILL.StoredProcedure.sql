USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_BL_PAYBILL]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_BL_PAYBILL]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  @COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null, 
	@COLUMN07   nvarchar(250)=null,  @COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  @COLUMN11   nvarchar(250)=null, 
	@COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  @COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  @COLUMN20   nvarchar(250)=null,  @COLUMN21  nvarchar(250)=null,
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
	@COLUMN22   nvarchar(250)=null,  @COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null, 
    @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null, 
    @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null, 
    @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,
    @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
    --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
    @TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try
   declare @tempSTR nvarchar(max)
   set @tempSTR=('the values '+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','
+isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+','+isnull(@COLUMN14,'')+','+isnull(@COLUMN15,'')+','+isnull(@COLUMN16,'')+','+isnull(@COLUMN17,'')+','+isnull(@COLUMN18,'')+','+isnull(@COLUMN19,'')+' of  '+isnull(@TabelName,'')+' succeeded at  ');

IF  @TabelName='PUTABLE014'
BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN13!= '' and @COLUMN13 is not null and @COLUMN13!= '0' ) THEN @COLUMN13 else @COLUMNA02  END )
EXEC usp_PUR_TP_PUTABLE014 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14, @COLUMN15,  @COLUMN16,
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   --EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
   @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
   --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue OUTPUT
select @ReturnValue

END 

ElSE IF  @TabelName='PUTABLE015'
BEGIN
EXEC usp_PUR_TP_PUTABLE015 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15, 
 ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)
 @COLUMN16, @COLUMN17,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   @COLUMN18,  @COLUMN19,  @COLUMN20,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @ReturnValue
END 
   exec [CheckDirectory] @tempSTR,'usp_PUR_BL_PAYBILL.txt',0
END TRY
			BEGIN CATCH
			begin
   --EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PAYBILL.txt',0
			return 0
			end
			END CATCH
end



GO
