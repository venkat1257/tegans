USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_Direct_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_Direct_HEADER_DATA]
(
	@InvoiceID int
)

AS
BEGIN
	SELECT a.COLUMN05 Customer,a.COLUMN11 Terms,a.COLUMN21 Mode,a.COLUMN14 ou,a.COLUMN17 dept, a.COLUMN04 Ref, 
	a.COLUMN23 taxtype,isnull(m.column07,0) htaxamt,a.COLUMN12 notes, a.COLUMN29 project, a.COLUMN39 location,a.COLUMN22 subtotal,a.COLUMN24 taxamt,a.COLUMN20 totamt,
	ca.COLUMN06 addresse,ca.COLUMN07 add1,ca.COLUMN08 add2,ca.COLUMN16 country,ca.COLUMN11 'state',ca.COLUMN10 city,
    ca.COLUMN12 zip,ca.COLUMN17 bill,ca.COLUMN18 shipp,a.COLUMN46 COLUMN50
	,c.COLUMN05 cName,a.COLUMN55 COLUMN59,a.COLUMN56 COLUMN60,a.COLUMN65 CGRY,a.COLUMN64 GSTIN,a.COLUMN66 STCODE,a.COLUMN68 RoundOffAmt
	FROM SATABLE009 a 
	left join MATABLE013 m on  m.column02=a.COLUMN23 and m.COLUMNA03=a.COLUMNA03 and isnull(m.COLUMNA13,0)=0
	left join SATABLE002 c on c.COLUMN02=a.COLUMN05  and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
	left join SATABLE003 ca on c.COLUMN01=ca.COLUMN20 and ca.COLUMN19='Customer' and ca.COLUMNA03=a.COLUMNA03 and isnull(ca.COLUMNA13,0)=0
	 WHERE  a.COLUMN02= @InvoiceID and isnull(a.columnA13,0)=0
END












GO
