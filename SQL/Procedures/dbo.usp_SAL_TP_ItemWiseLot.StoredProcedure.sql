USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_ItemWiseLot]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_ItemWiseLot]
(
	@ItemID nvarchar(250)=NULL,@FrmID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL
)
AS
BEGIN
if((@FrmID=1357) )
begin
if not exists(select COLUMN02 FROM FITABLE043 where COLUMN09=@ItemID  And COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0  AND (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or isnull(COLUMNA02,0)=0))
begin
select l.COLUMN02,l.COLUMN04 FROM FITABLE043 l
LEFT join FITABLE010 f on f.COLUMN22=l.COLUMN02  and f.COLUMN22=l.COLUMN02 and isnull(f.COLUMN21,0)=0 and  f.COLUMNA02=@OPUnit and  f.COLUMNA03=l.COLUMNA03
where f.COLUMN03=@ItemID  And 
(l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)
  AND l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False' 
end
else
begin
select l.COLUMN02,l.COLUMN04 FROM FITABLE043 l
LEFT join FITABLE010 f on f.COLUMN03=l.COLUMN09  and f.COLUMN22=l.COLUMN02 and isnull(f.COLUMN21,0)=0 and  f.COLUMNA02=@OPUnit and  f.COLUMNA03=l.COLUMNA03
where   l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False'  AND 
(l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)
end
end
else
begin
select l.COLUMN02,l.COLUMN04 FROM FITABLE043 l
left join FITABLE010 f on f.COLUMN22=l.COLUMN02 and f.COLUMN22=l.COLUMN02 and isnull(f.COLUMN21,0)=0 and  f.COLUMNA02=@OPUnit and  f.COLUMNA03=l.COLUMNA03
where f.COLUMN03=@ItemID And  
(l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)
  AND l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False' 

END
END
GO
