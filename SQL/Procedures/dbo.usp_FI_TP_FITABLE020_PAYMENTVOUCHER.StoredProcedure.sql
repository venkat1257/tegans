USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE020_PAYMENTVOUCHER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE020_PAYMENTVOUCHER]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	--EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)

AS

BEGIN
 begin try
IF @Direction = 'Insert'

BEGIN
if(@COLUMN18='False'or @COLUMN18='0')
begin
set @COLUMN19=(null)
end
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE020_SequenceNo
insert into FITABLE020
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN10,  @COLUMN19,  @COLUMN20,  @COLUMN21,
--EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   @COLUMN22,  @COLUMN23,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
) 
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue =(select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE020
END  

ELSE IF @Direction = 'Update'
BEGIN
UPDATE FITABLE020 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,        
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN10,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,
   --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,                         COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02

 declare @paymenttype nvarchar(250)=null,@bank nvarchar(250)=null,@Bankbal decimal(18,2)=null,@lineamt decimal(18,2)=null,
 @linetax decimal(18,2)=null, @internalid nvarchar(250)=null,@Voucher# nvarchar(250),@Acc nvarchar(250),@Hinternalid nvarchar(250),
@Project nvarchar(250),@DATE date, @TYPEID nvarchar(250)=null,@NUMBER nvarchar(250),@Totbal decimal(18,2)=null,@Remainbal decimal(18,2)=null,
@ACCNAME nvarchar(250),@TYPE nvarchar(250)=null,@ACCOUNTTYPE nvarchar(250)
set @Voucher#=(select column04 from FITABLE020 where COLUMN02=@COLUMN02)
set @internalid= (select column01 from FITABLE020 where COLUMN02=@COLUMN02)
set @Hinternalid= (select column01 from FITABLE020 where COLUMN02=@COLUMN02)
set @DATE=(@COLUMN05)
set @TYPEID=(@Voucher#)
set @NUMBER=(@Hinternalid)
DELETE FROM FITABLE064 WHERE COLUMN05 = @internalid AND COLUMN03 IN('PAYMENT VOUCHER',  'PDC ISSUED') AND COLUMNA02 = @COLUMNA02 AND COLUMNA03 = @COLUMNA03 
 DECLARE @MaxRownum INT,@id nvarchar(250)
      DECLARE @Initialrow INT=1
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE022 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE022 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN
		 set @paymenttype= (select column10 from FITABLE022 where COLUMN02=@id)
		 set @Acc= (select column03 from FITABLE022 where COLUMN02=@id)
		 set @Project= (select isnull(column08,0) from FITABLE022 where COLUMN02=@id)
		 set @internalid= (select column01 from FITABLE022 where COLUMN02=@id)
		 set @bank= (select column11 from FITABLE022 where COLUMN02=@id)
		 set @lineamt= (select column05 from FITABLE022 where COLUMN02=@id)
		 set @linetax= (select column06 from FITABLE022 where COLUMN02=@id)
		 set @Bankbal=(select ISNULL(COLUMN10,0) from FITABLE001 where COLUMN02=@bank and COLUMNA03=@COLUMNA03)
		 if(isnull(cast(@linetax as decimal(18,2)),0)>0)
		 BEGIN
		 delete from  FITABLE026  where COLUMN04='PAYMENT VOUCHER'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  FITABLE036  where COLUMN03='PAYMENT VOUCHER'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 
		 set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @Totbal=(cast(isnull(@lineamt,0) as decimal(18,2))+cast(isnull(@linetax,0) as decimal(18,2)))
		 set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=@TYPE)
		 declare @Projectactualcost decimal(18,2)
		 if(cast(@Totbal as nvarchar)!='' and cast(isnull(@Totbal,0) as decimal(18,2))!=0)
		 begin
		 set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
		 update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))-cast(@Totbal as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
		 end
IF(@TYPE=22266)
BEGIN
	delete from PUTABLE019  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='PAYMENT VOUCHER' and COLUMN08 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
    SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Cash
ELSE IF(@TYPE=22409)
BEGIN
	delete from FITABLE052  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='PAYMENT VOUCHER' and COLUMN09 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
    SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	delete from  PUTABLE017  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='PAYMENT VOUCHER'   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Payable
ELSE IF(@TYPE=22263 or @Acc=1117)
BEGIN
	delete from  PUTABLE016  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='PAYMENT VOUCHER' and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	delete from  PUTABLE018  where COLUMN03=@Acc and COLUMN05 in(@TYPEID) and COLUMN06='PAYMENT VOUCHER'  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Equity
ELSE IF(@TYPE=22330)
BEGIN
	delete from  FITABLE029  where COLUMN03='PAYMENT VOUCHER' and COLUMN07=@Acc and COLUMN11 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Expense
ELSE IF(@TYPE=22344)
BEGIN
	delete from  FITABLE036  where COLUMN03='PAYMENT VOUCHER' and COLUMN10=@Acc  and COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	delete from  FITABLE026  where COLUMN04='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	delete from  FITABLE034  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	delete from  FITABLE028  where COLUMN05='PAYMENT VOUCHER' and  COLUMN06 =@TYPEID and COLUMN03=@Acc  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Income
ELSE IF(@TYPE=22405)
BEGIN
	delete from  FITABLE025  where COLUMN04='PAYMENT VOUCHER'  and COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	delete from  FITABLE025  where COLUMN04='PAYMENT VOUCHER'  and  COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	delete from  FITABLE035  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09=@TYPEID   and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	delete from  FITABLE036  where COLUMN03='PAYMENT VOUCHER'  and COLUMN09=@TYPEID   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current assets
ELSE IF(@TYPE=22410)
BEGIN
	delete from  FITABLE060  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current Liabilities
ELSE IF(@TYPE=22411)
BEGIN
	delete from  FITABLE063  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END

		 update FITABLE044 set  COLUMN18=(null),COLUMN19=(null) where column02 in(select COLUMN12 from FITABLE022 where COLUMN02=@id) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		 if(@paymenttype=22627)
		 begin
		 delete from  FITABLE026  where COLUMN04='PDC ISSUED'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE016  where   COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 else
		 begin
		 delete from  PUTABLE016  where   COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE019  where   COLUMN05 =@Voucher# and  COLUMN08 =@internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		 delete from  FITABLE052  where   COLUMN05 =@Voucher# and  COLUMN09 =@internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))+cast(@lineamt as decimal(18,2))+cast(@linetax as decimal(18,2))) where column02=@bank and COLUMNA03=@COLUMNA03
		 end

		  set @COLUMN16= (select sum(isnull(COLUMN16,0)) from FITABLE022 where COLUMN09 in(@Hinternalid))
	--	 if(@COLUMN16!= '0' and @COLUMN16!= '')
	--begin
	--declare @id1 int
	DECLARE @cretAmnt decimal(18,2),@vendor nvarchar(250),@AdvIDs nvarchar(250),@Initialrow1 int=1,@MaxRownum1 int
	--set @AdvIDs=(select COLUMN18 from FITABLE022 where COLUMN02=@id)
 --   set @vendor=(select COLUMN07 from FITABLE020 where COLUMN02=@COLUMN02)
	--		  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
	--		  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
	--		  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
	--		  order by FITABLE020.column02 asc
	--	  OPEN curA
	--	  FETCH NEXT FROM curA INTO @id1
	--	  if(@id1!='' and @id1 is not null)
	--	     BEGIN 
	--	        if(@COLUMN16!='0' and cast(@COLUMN16 as decimal(18,2))>0)
	--		  begin
	--		  set @MaxRownum1=(select  COUNT(*) from FITABLE020 
	--		  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
	--		  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0)
	--	         WHILE @Initialrow1 <= @MaxRownum1
	--		  begin
	--		  if(cast(@COLUMN16 as DECIMAL(18,2))!=0 and @MaxRownum1>0)
	--		  begin
	--			set @cretAmnt=(select column10 from FITABLE020 where column02= @id1  
	--			 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
	--			 --set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET9***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	--			 --'Header Update:Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
	--			 --'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	--if(cast(@COLUMN16 as DECIMAL(18,2))>=@cretAmnt)
	--			begin
	--				update FITABLE020 set column18=(@cretAmnt), COLUMN16='OPEN'  where column02= @id1  
	--				and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
	--			set @COLUMN16=(cast(@COLUMN16 as decimal(18,2))-@cretAmnt)
	--			enD
	--			 else if(cast(@COLUMN16 as DECIMAL(18,2))<@cretAmnt)					 
	--			 begin
	--				update FITABLE020 set column18=(cast(isnull(@COLUMN16,0) as DECIMAL(18,2))+cast(isnull(column18,0) as DECIMAL(18,2))), COLUMN16='OPEN'  where column02= @id1
	--			 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
	--			set @COLUMN16=(0)
	--			 end
	--			 end
	--			FETCH NEXT FROM curA INTO @id1
	--			SET @Initialrow1 = @Initialrow1 + 1
	--			end
	--			CLOSE curA 
	--			deallocate curA
	--		END
	--		end
	--	end	
		 UPDATE FITABLE022 SET COLUMNA13=1 WHERE COLUMN02=@id
 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end
        CLOSE cur1
        deallocate cur1

set @ReturnValue = 1
END 

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE020 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
set @Voucher#=(select column04 from FITABLE020 where COLUMN02=@COLUMN02)
set @internalid= (select column01 from FITABLE020 where COLUMN02=@COLUMN02)
set @COLUMNA02=(select COLUMNA02 from FITABLE020 where COLUMN02=@COLUMN02)
set @COLUMNA03= (select COLUMNA03 from FITABLE020 where COLUMN02=@COLUMN02)
set @Hinternalid= (select column01 from FITABLE020 where COLUMN02=@COLUMN02)
set @TYPEID=(@Voucher#)
set @NUMBER=(@Hinternalid)
DELETE FROM FITABLE064 WHERE COLUMN05 = @internalid AND COLUMN03 IN('PAYMENT VOUCHER',  'PDC ISSUED') AND COLUMNA02 = @COLUMNA02 AND COLUMNA03 = @COLUMNA03 
declare @LInternal nvarchar(250)
      set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE022 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE022 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN
		 set @paymenttype= (select column10 from FITABLE022 where COLUMN02=@id)
		 set @internalid= (select column01 from FITABLE022 where COLUMN02=@id)
		  --EMPHCS1566	PaymentVoucher recorder are not deleteing in BankRegister BY RAJ.Jr
		 set @LInternal= (select column01 from FITABLE022 where COLUMN02=@id)
		 set @AdvIDs=(select COLUMN18 from FITABLE022 where COLUMN02=@id)
		 set @Acc= (select column03 from FITABLE022 where COLUMN02=@id)
		 set @Project= (select isnull(column08,0) from FITABLE022 where COLUMN02=@id)
		 set @bank= (select column11 from FITABLE022 where COLUMN02=@id)
		 set @lineamt= (select column05 from FITABLE022 where COLUMN02=@id)
		 set @linetax= (select column06 from FITABLE022 where COLUMN02=@id)
		 set @Bankbal=(select ISNULL(COLUMN10,0) from FITABLE001 where COLUMN02=@bank and COLUMNA03=@COLUMNA03)
		 if(isnull(cast(@linetax as decimal(18,2)),0)>0)
		 BEGIN
		 delete from  FITABLE026  where COLUMN04='PAYMENT VOUCHER'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  FITABLE036  where COLUMN03='PAYMENT VOUCHER'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end

		 set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @Totbal=(cast(isnull(@lineamt,0) as decimal(18,2))+cast(isnull(@linetax,0) as decimal(18,2)))
		 set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=@TYPE)
		 if(cast(@Totbal as nvarchar)!='' and cast(isnull(@Totbal,0) as decimal(18,2))!=0)
		 begin
		 set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
		 update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))-cast(@Totbal as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
		 end
IF(@TYPE=22266)
BEGIN
	delete from PUTABLE019  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='PAYMENT VOUCHER' and COLUMN08 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
    SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Cash
ELSE IF(@TYPE=22409)
BEGIN
	delete from FITABLE052  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='PAYMENT VOUCHER' and COLUMN09 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
    SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	delete from  PUTABLE017  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='PAYMENT VOUCHER'   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Payable
ELSE IF(@TYPE=22263 or @Acc=1117)
BEGIN
	delete from  PUTABLE016  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='PAYMENT VOUCHER'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	delete from  PUTABLE018  where COLUMN03=@Acc and COLUMN05=@Voucher# and COLUMN06='PAYMENT VOUCHER'  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Equity
ELSE IF(@TYPE=22330)
BEGIN
	delete from  FITABLE029  where COLUMN03='PAYMENT VOUCHER' and COLUMN07=@Acc and COLUMN11 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Expense
ELSE IF(@TYPE=22344)
BEGIN
	delete from  FITABLE036  where COLUMN03='PAYMENT VOUCHER' and COLUMN10=@Acc  and COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	delete from  FITABLE026  where COLUMN04='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	delete from  FITABLE034  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	delete from  FITABLE028  where COLUMN05='PAYMENT VOUCHER' and  COLUMN06 =@TYPEID and COLUMN03=@Acc  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Income
ELSE IF(@TYPE=22405)
BEGIN
	delete from  FITABLE025  where COLUMN04='PAYMENT VOUCHER'  and COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	delete from  FITABLE025  where COLUMN04='PAYMENT VOUCHER'  and  COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	delete from  FITABLE035  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09=@TYPEID   and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	delete from  FITABLE036  where COLUMN03='PAYMENT VOUCHER' and COLUMN09=@TYPEID   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current assets
ELSE IF(@TYPE=22410)
BEGIN
	delete from  FITABLE060  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current Liabilities
ELSE IF(@TYPE=22411)
BEGIN
	delete from  FITABLE063  where COLUMN03='PAYMENT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END


		 if(@paymenttype=22627)
		 begin
		 update FITABLE044 set  COLUMN18=(null),COLUMN19=(null) where column02 in(select COLUMN12 from FITABLE022 where COLUMN02=@id) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		 delete from  FITABLE026  where COLUMN04='PDC ISSUED'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE016  where   COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE019  where   COLUMN05 =@Voucher# and  COLUMN08 =@internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03		 
		 --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		 delete from  FITABLE052  where   COLUMN05 =@Voucher# and  COLUMN09 =@internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 else
		 begin
		 delete from  PUTABLE016  where   COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		  --EMPHCS1566	PaymentVoucher recorder are not deleteing in BankRegister BY RAJ.Jr
		 delete from  PUTABLE019  where   COLUMN05 =@Voucher# and  COLUMN08 =@LInternal and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03		 
		 delete from  FITABLE052  where   COLUMN05 =@Voucher# and  COLUMN09 =@LInternal and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))+cast(@lineamt as decimal(18,2))+cast(@linetax as decimal(18,2))) where column02=@bank and COLUMNA03=@COLUMNA03
		 end
		  set @COLUMN16= (select sum(isnull(COLUMN16,0)) from FITABLE022 where COLUMN09 in(@Hinternalid))
		  --EMPHCS1566	PaymentVoucher recorder are not deleteing in BankRegister BY RAJ.Jr
		 if(cast(@COLUMN16 as decimal(18,2))!=0 and cast(@COLUMN16 as decimal(18,2))>0 and @COLUMN16!='')
	begin
	declare @d1 int,@MaxRowA int,@FirstRow int=1
    set @vendor=(select COLUMN07 from FITABLE020 where COLUMN02=@COLUMN02)
			  DECLARE curA1 CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
			  order by FITABLE020.column02 asc
		  OPEN curA1
		  FETCH NEXT FROM curA1 INTO @d1
		  if(@d1!='' and @d1 is not null)
		     BEGIN 
		        if(@COLUMN16!='0' and cast(@COLUMN16 as decimal(18,2))>0)
			  begin
			  set @MaxRowA=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0)
		         WHILE @FirstRow <= @MaxRowA
			  begin
			  if(cast(@COLUMN16 as DECIMAL(18,2))!=0 and @MaxRowA>0)
			  begin
				set @cretAmnt=(select column10 from FITABLE020 where column02= @d1  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 --set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET9***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 --'Header Update:Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 --'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN16 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(@cretAmnt), COLUMN16='OPEN'  where column02= @d1  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				set @COLUMN16=(cast(@COLUMN16 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN16 as DECIMAL(18,2))<@cretAmnt)					 
				 begin
				 declare @BALcretAmnt DECIMAL(18,2)
				 set @BALcretAmnt=(select column18 from FITABLE020 where column02= @d1  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 if(@BALcretAmnt!=@cretAmnt)
				 begin
					update FITABLE020 set column18=(cast(isnull(@COLUMN16,0) as DECIMAL(18,2))+cast(isnull(@BALcretAmnt,0) as DECIMAL(18,2))), COLUMN16='OPEN'  where column02= @d1
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				set @COLUMN16=(0)
				end
				 end
				 end
				FETCH NEXT FROM curA1 INTO @d1
				SET @FirstRow = @FirstRow + 1
				end
				CLOSE curA1 
				deallocate curA1
			END
			end
		end	
		 UPDATE FITABLE022 SET COLUMNA13=1 WHERE COLUMN02=@id
 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end
        CLOSE cur1
        deallocate cur1

END

end try
begin catch
declare @tempSTR nvarchar(max)
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_FITABLE020.txt',0

return 0
end catch
end






















GO
