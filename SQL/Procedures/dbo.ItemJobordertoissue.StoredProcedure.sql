USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ItemJobordertoissue]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemJobordertoissue]
(
	@ItemID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL
)
AS
BEGIN
select S7.COLUMN02,S7.COLUMN04 FROM SATABLE007 S7
LEFT JOIN SATABLE005 S5 ON S5.COLUMN02 = S7.COLUMN06 AND S5.COLUMNA03 = S7.COLUMNA03 AND ISNULL(S5.COLUMNA13,0)=0 
where S7.COLUMN06=@ItemID  And  S7.COLUMN14! ='Issue(Out)' AND
(S7.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or S7.COLUMNA02 is null)
  AND S7.COLUMNA03=@AcOwner  and isnull(S7.COLUMNA13,'False')='False' 
END
GO
