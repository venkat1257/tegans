USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_Address]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[usp_SAL_TP_Address]
(
	@SalesOrderID int
)

AS
BEGIN
	SELECT a.COLUMN06,a.COLUMN07, a.COLUMN08,a.COLUMN12,
	--(select COLUMN04 from MATABLE002  where COLUMN02=a.COLUMN10) as
	 a.COLUMN10,(select COLUMN03 from MATABLE017  where COLUMN02=a.COLUMN11) COLUMN11,(select COLUMN03 from MATABLE016  where COLUMN02=a.COLUMN16) COLUMN16
	FROM dbo.SATABLE003 a
	LEFT JOIN dbo.SATABLE005 po on po.COLUMN01 = a.COLUMN20
	WHERE a.COLUMN20 = (select COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)
	and a.COLUMNA02 = (select COLUMNA02 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID) and a.COLUMN19='SALESORDER'
END









GO
