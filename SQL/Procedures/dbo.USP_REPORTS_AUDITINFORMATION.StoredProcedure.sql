USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_AUDITINFORMATION]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_REPORTS_AUDITINFORMATION]
(
@FromDate nvarchar(250)=null,
@ToDate nvarchar(250)=null,
@User nvarchar(250)=null,
@EmailID nvarchar(250)=null,
--@LoginDateTime nvarchar(250)=null,
--@LogoutDateTime nvarchar(250)= null,
@IPAddress nvarchar(250)= null,
@Browser nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null
)
as
begin
declare @whereStr nvarchar(1000)=null
declare @Query1 nvarchar(max)=null 
if @User!=''
begin
 set @whereStr= ' where UserID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@User+''') s)'
end
if @Browser!=''
begin
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr= ' where Browser in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Browser+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Browser in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Browser+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #AuditInformation from (
Select CAST(c.COLUMN04 AS DATETIME) date,m.COLUMN09,c.COLUMN03 as UserID,m.COLUMN13,c.COLUMN04,c.COLUMN05,c.COLUMN06, c.COLUMN07,c.COLUMN07 as Browser From CONTABLE020 c inner JOIN MATABLE010 m on m.COLUMN02 = c.COLUMN03
where c.COLUMNA03=@AcOwner and cast(c.COLUMN04 as date) between @FromDate and @ToDate 
) Query
set @Query1='select * from #AuditInformation'+@whereStr +'order by cast(COLUMN04 as date) desc'
exec (@Query1) 
end



GO
