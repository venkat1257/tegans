USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE010]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_SATABLE010]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(max)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22 nvarchar(250)=null,
	--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
	@COLUMN23 nvarchar(250)=null,    @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  
	--EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
	@COLUMN26 nvarchar(250)=null,    @COLUMN27 nvarchar(250)=null,    @COLUMN28   nvarchar(250)=null,
	--EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	@COLUMN31   nvarchar(250)=null,  @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	@COLUMN34   nvarchar(250)=null,  @COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  
	@COLUMN37   nvarchar(250)=null,  @COLUMN38   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),     
	@ReturnValue nvarchar(250)=null, @UOMData  XML=null,              @Soid nvarchar(250)=null,  
	@Result nvarchar(250)=null,      @Status nvarchar(250)=null,      @OrderType nvarchar(250)=null
)

AS
BEGIN
begin try
--EMPHCS1410	Logs Creation by srinivas
declare @tempSTR nvarchar(max)
declare @form int
--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
set @COLUMN22=(select iif((@COLUMN22!='' and @COLUMN22>0),@COLUMN22,(select max(column02) from fitable037 where column07=1 and column08=1)))
DECLARE @INVTRANSNO NVARCHAR(250)
SELECT @INVTRANSNO= COLUMN04,@COLUMNB02= COLUMN03 FROM SATABLE009 WHERE COLUMN01=@COLUMN15
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE010_SequenceNo
set @COLUMN16=(select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15)
set @form=(select COLUMN03 from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
declare @Location nvarchar(250)=null,@ProjectID nvarchar(250)
set @Location = (select isnull(COLUMN39,0) from  SATABLE009 where COLUMN01=@COLUMN15)
set @Location = (case when (cast(isnull(@COLUMN38,'') as nvarchar(250))!='' and @COLUMN38!=0) then @COLUMN38 else @Location end)
set @COLUMN35 = (case when (cast(isnull(@COLUMN35,'') as nvarchar(250))='') then @COLUMN13 else @COLUMN35 end)
--EMPHCS714	At the time of invoice edit and save , system is getting some ID in Discount field
--set @COLUMN17=(select COLUMN15 from SATABLE009 where COLUMN01=@COLUMN15)
--set @COLUMN18=(select COLUMN16 from SATABLE009 where COLUMN01=@COLUMN15)
--set @COLUMN19=(select COLUMN17 from SATABLE009 where COLUMN01=@COLUMN15)
set @OrderType=(select COLUMN19 from SATABLE009 where COLUMN01=@COLUMN15)
set @Soid=(select COLUMN06 from SATABLE009 where COLUMN01=@COLUMN15)
set @ProjectID = (select COLUMN29 from SATABLE009 Where COLUMN01=@COLUMN15)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
--IF (@Soid != '' ) 
--BEGIN
--set @COLUMN09=(select sum(isnull(COLUMN09,0)) from SATABLE010 where COLUMN15 in(select COLUMN01 from  SATABLE009 where COLUMN06=@Soid))
--end
set @COLUMNA02=( CASE WHEN (@COLUMN15!= '' and @COLUMN15 is not null and @COLUMN15!= '0'  ) THEN (select COLUMN14 from SATABLE009 
   where COLUMN01=@COLUMN15) else @COLUMNA02  END )
IF (@OrderType !='1000')
begin
IF (@Soid != '' ) 
BEGIN
set @COLUMN12 =(CAST(@COLUMN12 AS decimal(18,2))- CAST(@COLUMN10 AS decimal(18,2)))
end
end
 
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'insert into SATABLE010 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,   COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,   COLUMN19,  COLUMN20,  COLUMN21,
   COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31, 
   COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   '+
   isnull(@COLUMN02 ,'''NULL''')+','+isnull(  @COLUMN03,'''NULL''')+','+isnull(  @COLUMN04,'''NULL''')+','+isnull( @COLUMN05,'''NULL''')+','+isnull(  @COLUMN06,'''NULL''')+','+isnull(  @COLUMN07,'''NULL''')+','+isnull(  @COLUMN08,'''NULL''')+','+isnull(  @COLUMN10,'''NULL''')+','+isnull(  @COLUMN11,'''NULL''')+','+
   isnull(@COLUMN12 ,'''NULL''')+','+isnull(  @COLUMN13,'''NULL''')+','+isnull(  @COLUMN14,'''NULL''')+','+isnull( @COLUMN15,'''NULL''')+','+isnull(  @COLUMN16,'''NULL''')+','+isnull(  @COLUMN17,'''NULL''')+','+isnull(  @COLUMN18,'''NULL''')+','+isnull(  @COLUMN19,'''NULL''')+','+isnull(  @COLUMN20,'''NULL''')+','+isnull(  @COLUMN21,'''NULL''')+','+ 
   isnull(@COLUMN22 ,'''NULL''')+','+isnull(  @COLUMN23,'''NULL''')+','+isnull(  @COLUMN22,'''NULL''')+','+isnull( @COLUMN10,'''NULL''')+','+isnull(  @COLUMN26,'''NULL''')+','+isnull(  @COLUMN27,'''NULL''')+','+isnull(  @COLUMN28,'''NULL''')+','+isnull(  @COLUMN29,'''NULL''')+','+isnull(  @COLUMN30,'''NULL''')+','+isnull(  @COLUMN31,'''NULL''')+','+
   isnull(@COLUMN32 ,'''NULL''')+','+isnull(  @COLUMN33,'''NULL''')+','+isnull(  @COLUMN34,'''NULL''')+','+isnull( @COLUMN35,'''NULL''')+','+isnull(  @COLUMN36,'''NULL''')+','+isnull(  @COLUMN37,'''NULL''')+','+isnull(  @COLUMN38,'''NULL''')+','+isnull( @COLUMNA01,'''NULL''')+','+isnull( @COLUMNA02,'''NULL''')+','+isnull( @COLUMNA03,'''NULL''')+','+isnull( @COLUMNA04,'''NULL''')+','+isnull( @COLUMNA05,'''NULL''')+','+isnull( @COLUMNA06,'''NULL''')+','+ 				  
   isnull(@COLUMNA07,'''NULL''')+','+isnull( @COLUMNA08,'''NULL''')+','+isnull( @COLUMNA09,'''NULL''')+','+isnull(@COLUMNA10,'''NULL''')+','+isnull( @COLUMNA11,'''NULL''')+','+isnull( @COLUMNA12,'''NULL''')+','+isnull( @COLUMNA13,'''NULL''')+','+isnull( @COLUMNB01,'''NULL''')+','+isnull( @COLUMNB02,'''NULL''')+','+isnull( @COLUMNB03,'''NULL''')+','+ 
   isnull(@COLUMNB04,'''NULL''')+','+isnull( @COLUMNB05,'''NULL''')+','+isnull( @COLUMNB06,'''NULL''')+','+isnull(@COLUMNB07,'''NULL''')+','+isnull( @COLUMNB08,'''NULL''')+','+isnull( @COLUMNB09,'''NULL''')+','+isnull( @COLUMNB10,'''NULL''')+','+isnull( @COLUMNB11,'''NULL''')+','+isnull( @COLUMNB12,'''NULL''')+','+isnull( @COLUMND01,'''NULL''')+','+ 
   isnull(@COLUMND02,'''NULL''')+','+isnull( @COLUMND03,'''NULL''')+','+isnull( @COLUMND04,'''NULL''')+','+isnull(@COLUMND05,'''NULL''')+','+isnull( @COLUMND06,'''NULL''')+','+isnull( @COLUMND07,'''NULL''')+','+isnull( @COLUMND08,'''NULL''')+','+isnull( @COLUMND09,'''NULL''')+','+isnull( @COLUMND10,'''NULL''')+')'+ 'at ')
    )

insert into SATABLE010 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,   COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,   COLUMN19,  COLUMN20,  COLUMN21,
   --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   --EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  
   @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19, 
   --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,  @COLUMN22,  @COLUMN10,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,
   --EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN30,  @COLUMN31,  @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Line Values are Created in SATABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
declare @Invoiceno nvarchar(250)=null,@Invoicelineref nvarchar(250)=null,@uomselection nvarchar(250)=null,@lotno nvarchar(250)=null,@Taxappliedon nvarchar(250)=null
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN05)
set @Invoiceno=(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15)
set @Invoicelineref=(select COLUMN01 from SATABLE010 where COLUMN02=@COLUMN02)
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
set @lotno=(case when @column31='' then 0 when isnull(@column31,0)=0 then 0  else @column31 end)
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'Invoice',@Invoiceno,@Invoicelineref,@COLUMN05,@COLUMN16,@COLUMNA03
end
if(@Soid!='')
begin
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
set @COLUMN09=(select max(isnull(COLUMN09,0)) from SATABLE010 where  COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0)
end
else
begin
set @COLUMN09=(select max(isnull(COLUMN09,0)) from SATABLE010 where  COLUMN04=@COLUMN04 and COLUMN05=@COLUMN05 and COLUMNA13=0 and COLUMN15 in(select COLUMN01 from  SATABLE009 where COLUMN06 in(@Soid)))
end
set @COLUMN09=(CAST(@COLUMN09 AS decimal(18,2))+ CAST(@COLUMN10 AS decimal(18,2)))
update SATABLE010 set COLUMN09=@COLUMN09 where COLUMN02=@COLUMN02
end
if exists(select column01 from SATABLE010 where column15=@COLUMN15)
					begin
--set @COLUMN09=(select sum(isnull(COLUMN09,0)) from SATABLE010 where COLUMN15 in(select COLUMN01 from  SATABLE009 where COLUMN06=@Soid))
--update SATABLE010 set column09=(CAST(@COLUMN09 AS decimal(18,2))+ CAST(@COLUMN10 AS decimal(18,2))) where column02=@column02
declare @PID int
declare @SPID int
declare @number int
declare @number1 int
declare @BillQty DECIMAL(18,2)
declare @PBillQty DECIMAL(18,2)
declare @IQty DECIMAL(18,2)
declare @IRQty DECIMAL(18,2)
declare @PBQty DECIMAL(18,2)
declare @AID int
declare @AID1 int
declare @CUST int
declare @DT date
declare @chk bit
declare @SBAL DECIMAL(18,2)
declare @PRICE DECIMAL(18,2)
declare @CBAL DECIMAL(18,2)
declare @Vid nvarchar(250)=null
declare @MEMO nvarchar(250)=null
declare @idname nvarchar(250)=null
 DECLARE @Tax int
 DECLARE @Tax1 int
declare @TaxString nvarchar(250)=NULL
 DECLARE @headerTax int
 DECLARE @TaxAG int 
 DECLARE @headerTaxAG int 
 declare @TAXRATE DECIMAL(18,2) 
 declare @TAXRATEM DECIMAL(18,2) 
 declare @headerTAXRATE DECIMAL(18,2)
 declaRE @TaxNo DECIMAL(18,2) 
 declare @lineiid nvarchar(250)=null
 declare @Project nvarchar(250)=null,@disval DECIMAL(18,2),@DBAL DECIMAL(18,2),@ACTYPE nvarchar(250),@AMNT DECIMAL(18,2)

--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @number1=(select max(column01) from SATABLE009 where column01=@COLUMN15)
set @number = (select MAX(COLUMN01) from  SATABLE010 where column02=@COLUMN02);
-- set @Tax= (select COLUMN21 from SATABLE010 where COLUMN01=@number)
-- set @headerTax= (select COLUMN23 from SATABLE009 where COLUMN01=@number1)
-- set @Taxappliedon= (select COLUMN19 from MATABLE013 where COLUMN02=@headerTax)
-- set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
-- set @headerTaxAG= (select column16 from MATABLE013 where COLUMN02=@headerTax)
-- set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
-- set @TaxNo= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
-- set @headerTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@headerTax)
--set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @number=(select max(column01) from SATABLE010 where column02=@COLUMN02 )
set @chk=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from matable007 where column02=@COLUMN05)
declare @Itemtype nvarchar(250)
set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@COLUMN05)
set @PRICE=(select column17 from matable007 where column02=@COLUMN05)
set @DT=(select column08 from SATABLE009 where column01=@number1)
set @CUST=(select column05 from SATABLE009 where column01=@number1)
set @MEMO=(select column12 from SATABLE009 where column01=@number1)
set @Project=(select column29 from SATABLE009 where column01=@number1)
set @idname=(select column04 from SATABLE009 where column01=@number1)
--EMPHCS730	Account Owner Condition for Income Register by srinivas 7/21/2015
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
set @DBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE036 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
set @lineiid=(select max(column01) from SATABLE010 where column02=@COLUMN02)
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @COLUMN19=(select column19 from SATABLE010 where column02=@COLUMN02)
IF(ISNULL(@COLUMN30,0)=22556)
BEGIN SET @disval=((CAST(@COLUMN14 AS decimal(18,2))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
else
BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END

		--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
declare @salesclaim bit,@actualdiscount nvarchar(250),@expecteddiscount nvarchar(250),@claimdiscount nvarchar(250),@claimtype nvarchar(250),
@rate nvarchar(250),@claimamt nvarchar(250),@increaseamt nvarchar(250),@fixedamt decimal(18,2)
set @fixedamt=(select iif(exists(select COLUMN11 from MATABLE023 where column02=@COLUMN37 and column05='22831'),(select isnull(COLUMN11,0) COLUMN11 from MATABLE023 where column02=@COLUMN37 and column05='22831'),0));
set @expecteddiscount=(select iif(exists(select COLUMN07 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),(select COLUMN07 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),'0'));
set @claimtype=(select iif(exists(select COLUMN06 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),(select COLUMN06 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),'0'));
set @claimtype=(select iif(exists(select COLUMN05 from MATABLE023 where COLUMN02=@claimtype  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),(select COLUMN05 from MATABLE023 where COLUMN02=@claimtype and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),'0'));
set @actualdiscount=(select iif(exists(select COLUMN06 from MATABLE023 where column02=@COLUMN37 and column05='22831'),(select iif(isnull(COLUMN06,0)=0,isnull(COLUMN11,0),isnull(COLUMN06,0))COLUMN06 from MATABLE023 where column02=@COLUMN37 and column05='22831'),'0'));
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+'expected discount'+cast(@expecteddiscount as nvarchar(250)) +','+'actual discount'+cast(@actualdiscount as nvarchar(250)) +','+'claimtype'+cast(@claimtype as nvarchar(250)) +','+
   'Item'+cast(@COLUMN05 as nvarchar(250)) +','+'Customer'+cast(@CUST as nvarchar(250)) +','+'Price Level Id '+cast(@COLUMN37 as nvarchar(250)) +','+
   ' at ') )
if((cast(isnull(@actualdiscount,0) as decimal(18,2))>cast(isnull(iif(@expecteddiscount!='',@expecteddiscount,'0'),0) as decimal(18,2)))  and cast(isnull(@actualdiscount,0) as decimal(18,2))>0)
begin
set @salesclaim=(select COLUMN04 from CONTABLE031 where COLUMN03='Sales Claim Required' and COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=isnull(@COLUMNA02,0) or COLUMNA02 is null) and  isnull(column04,0)=1 and isnull(columna13,0)=0)
if(@salesclaim=1 or @salesclaim='True')
begin
set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))-cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--IF(@claimtype='22777')
--begin
--set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))+cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--end
set @rate=(isnull(@COLUMN35,0));
set @claimamt=((cast(isnull(@COLUMN13,0) as decimal(18,2))*(cast(isnull(@claimdiscount,0) as decimal(18,2))*0.01))*cast(isnull(@COLUMN10,0) as decimal(18,2)));
if(@fixedamt>0.00)
begin
set @claimamt=((cast(isnull(@claimdiscount,0) as decimal(18,2)))*cast(isnull(@COLUMN10,0) as decimal(18,2)));
end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@COLUMN05 as nvarchar(250))+','
   +  cast(@rate as nvarchar(250))+','+ isnull(@claimamt,'')+' at ')
    )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Markdown Register',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22405', @COLUMN11 = '1129',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @claimamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

Insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11,COLUMN13,COLUMNA02, COLUMNA03,COLUMN12)
values(@AID,@DT,'Markdown Register',@number,@CUST,@MEMO,'1129',@claimamt,@claimamt,0,@COLUMN05, @COLUMNA02, @COLUMNA03,@Project)

set @AID=cast((select MAX(COLUMN02) from FITABLE034) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Assets  Values are Intiated For FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@COLUMN05 as nvarchar(250))+','
   +  cast(@rate as nvarchar(250))+','+ isnull(@claimamt,'')+' at ')
    )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Markdown Register',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22402', @COLUMN11 = '1128',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @claimamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03,COLUMN12)
values(@AID,'Sales Claim Register',@DT,@MEMO,@CUST,@claimamt,0,@idname,'1128',@COLUMNA02, @COLUMNA03,@Project)

end	
end
IF(@disval>0)
BEGIN
set @AID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE025)+1
set @AID1=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
IF ((select COLUMN05 from MATABLE023 where COLUMN02= @COLUMN37 and COLUMNA03=@COLUMNA03) != '22831' or @form = 1532  or @form = 1604)
begin 
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @disval,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12,COLUMN13)
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
values(@AID1,@DT,'INVOICE',@idname,@CUST,@number1,1049,CAST(isnull(@disval,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN05) 
END
END
declare @ActAmt decimal(18,2)
--if(@form = 1604)
--begin
--SET @ActAmt=(cast(ISNULL(@COLUMN14,0) as decimal(18,2))) 
--end
--else
--begin
SET @ActAmt=(cast(ISNULL(@COLUMN14,0) as decimal(18,2))+cast(ISNULL(@COLUMN19,0) as decimal(18,2))) 
--end
if(@Itemtype='ITTY009')
	begin	
		if(@AID>0)
			begin
			--Sales of Product Income storing each row wise at line level by srinivas 7/21/2015
			--delete from FITABLE025 where column05=@number1	 and column08=1052
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
	--EMPHCS1410	Logs Creation by srinivas				
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  'INVOICE'+','+ cast(@lineiid as nvarchar(250))+','+ cast( @CUST as nvarchar(250))+',NULL,1053,'+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+', '+   cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  @COLUMNA02+','+@COLUMNA03 + ' at ')
					)
					--//EMPHCS1788 SEGREGATION OF SERVICE ITEM'S SALES AND PURCHASE ACCOUNTS
					DECLARE @ACID int
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1053))
					SET @ACTYPE=(SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ActAmt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1167	Service items calculation by srinivas22/09/2015
			--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
			values(@AID,@DT,'INVOICE',@lineiid,@CUST,@MEMO,@ACID,(CAST(isnull(@ActAmt,0) AS decimal(18,2))),(CAST(isnull(@ActAmt,0) AS decimal(18,2))),@COLUMNA02 , @COLUMNA03,0,@Project)
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + '')
					)

			--Changed by Raj Patakota on 07/17/2015 to handle decimal
		--	if(@AID>0)
		--	begin
		--		set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
		--	end
		--else
		--	begin
		--		set @AID=1000;
		--	end
		--	insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
		--	values(@AID,@DT,'INVOICE',@lineiid,@CUST,NULL,1056,cast(cast(cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))as DECIMAL(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2))AS decimal(18,2)),@CBAL+cast(cast((cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2)))as DECIMAL(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0)
	end
else
	begin	
		if(@AID>0)
			begin
			--Sales of Product Income storing each row wise at line level by srinivas 7/21/2015
			--delete from FITABLE025 where column05=@number1	 and column08=1052
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  'INVOICE'+','+ cast(@lineiid as nvarchar(250))+','+ cast( @CUST as nvarchar(250))+',NULL,1052,'+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+', '+   cast(@SBAL+CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  @COLUMNA02+','+@COLUMNA03 + ' at ')
					)
					--//EMPHCS1788 SEGREGATION OF SERVICE ITEM'S SALES AND PURCHASE ACCOUNTS
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1052))
			SET @ACTYPE=(SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ActAmt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
			values(@AID,@DT,'INVOICE',@lineiid,@CUST,@MEMO,@ACID,(CAST(isnull(@ActAmt,0) AS decimal(18,2))),@SBAL+(CAST(isnull(@ActAmt,0) AS decimal(18,2))),@COLUMNA02 , @COLUMNA03,0,@Project)
			--Changed by Raj Patakota on 07/17/2015 to handle decimal
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + '')
					)
			
			if(@AID>0)
			begin
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			declare @AvgPrice decimal(18,2)
			if(@uomselection=1 or @uomselection='1' or @uomselection='True')
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN05) and COLUMN24=@COLUMN03 and COLUMN13=@COLUMN16 and isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03 and COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Invoice' and COLUMN04=@idname and COLUMN05=@Invoicelineref and COLUMN06=@COLUMN05 and COLUMNA02=@COLUMN16 and COLUMNA03=@COLUMNA03))
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND isnull(COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@ProjectID,0)  and COLUMN24=@COLUMN03);
			end
		--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  'INVOICE'+','+ cast(@lineiid as nvarchar(250))+','+ cast( @CUST as nvarchar(250))+',NULL,1056,'+  cast(cast(cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))as DECIMAL(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2))AS decimal(18,2)) as nvarchar(250))+', '+   cast(@CBAL+cast(cast(cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))as DECIMAL(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2))AS decimal(18,2)) as nvarchar(250))+','+  @COLUMNA02+','+@COLUMNA03 + ' at ')
					)					
			IF(ISNULL(@COLUMN30,0)=22556)
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			BEGIN SET @disval=(((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2)))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
			else
			BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END
					--//EMPHCS1788 SEGREGATION OF SERVICE ITEM'S SALES AND PURCHASE ACCOUNTS
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1056))
SET @ACTYPE=(SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
SET @disval = ((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))))			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @disval,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE025)+1
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12,COLUMND05, COLUMND06)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			values(@AID,@DT,'INVOICE',@lineiid,@CUST,@MEMO,@ACID,((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2)))),@CBAL+((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2)))),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN05,@lineiid)
--EMPHCS1410	Logs Creation by srinivas
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
SET @PID=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
SET @SPID=(SELECT datalength(ISNULL(COLUMN06,0)) FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
SET @PBillQty=(SELECT isnull(COLUMN13,0) FROM SATABLE006 WHERE COLUMN02=@COLUMN36 and COLUMNA13=0)
SET @COLUMN08=(SELECT isnull(COLUMN07,0) FROM SATABLE006 WHERE  COLUMN02=@COLUMN36 and COLUMNA13=0)
UPDATE SATABLE006 SET   COLUMN13=((@PBillQty)+ CAST(@COLUMN10 AS DECIMAL(18,2))) where  COLUMN02=@COLUMN36 and COLUMNA13=0
end
else
begin
SET @PBillQty=isnull((SELECT (COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMN03=@COLUMN05 and isnull(COLUMN27,0)=isnull(@COLUMN22,0) and isnull(COLUMN17,0)=isnull(@COLUMN31,0) and COLUMNA13=0),0)
SET @COLUMN08=isnull((SELECT (COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMN03=@COLUMN05 and isnull(COLUMN27,0)=isnull(@COLUMN22,0) and isnull(COLUMN17,0)=isnull(@COLUMN31,0) and COLUMNA13=0),0)
UPDATE SATABLE006 SET   COLUMN13=((@PBillQty)+ CAST(@COLUMN10 AS DECIMAL(18,2))) where COLUMN03=@COLUMN05 and isnull(COLUMN27,0)=isnull(@COLUMN22,0) and isnull(COLUMN17,0)=isnull(@COLUMN31,0) and COLUMNA13=0 and COLUMN19=
(select COLUMN01 from SATABLE005 where COLUMN02= (@PID))
end
UPDATE SATABLE010 SET  COLUMN09=((@PBillQty)+ CAST(@COLUMN10 AS DECIMAL(18,2))),COLUMN08=@COLUMN08 where COLUMN02=@COLUMN02


SET @BillQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMNA13=0)
SET @IRQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMNA13=0)
SET @IQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMNA13=0 )

if(@SPID=0 AND @chk=1)
	begin
		declare @Qty_On_Hand DECIMAL(18,2)
		declare @Qty_Commit DECIMAL(18,2)
		declare @Qty_Order DECIMAL(18,2)
		declare @Qty_AVL DECIMAL(18,2)
		declare @Qty_BACK DECIMAL(18,2)
		declare @Qty DECIMAL(18,2)

		begin
		--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
		 --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
		if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False')
		begin
		--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
		--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)
begin
			set @Qty_BACK =0
			--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
			set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
			--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
			set @Qty=cast(@COLUMN10 as DECIMAL(18,2));
			set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
			set @Qty_Commit=( @Qty_Commit - @Qty)
			set @Qty_AVL=(@Qty_On_Hand)-@Qty_Commit
		if(@Qty_AVL<0)
			begin
				set @Qty_AVL=0
				set @Qty_BACK =@Qty_Commit
				set @Qty_Commit=0
			end
			--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN07 =@Qty_BACK, COLUMN08= @Qty_On_Hand ,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@INVTRANSNO,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT',COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03)as DECIMAL(18,2))-(cast(@COLUMN10 as DECIMAL(18,2))*cast(@AvgPrice as DECIMAL(18,2)))) WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03
			--EMPHCS1410	Logs Creation by srinivas
			 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Updated in FITABLE010 Table For Item '+cast(@COLUMN05 as nvarchar(250))+' Quantity '+cast(@Qty_On_Hand as nvarchar(250))+' Units'+cast(@COLUMN22 as nvarchar(250))+ '' + CHAR(13)+CHAR(10) + '')
    )
			--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@INVTRANSNO,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT' WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@INVTRANSNO,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT' WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03
				end
		   --UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22)as DECIMAL(18,2))/@Qty_On_Hand)as DECIMAL(18,2)) WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22
		   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
        set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
	set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
	set @Qty_AVL=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)
		end
--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin
declare @tmpnewID1 int
declare @newID1 int
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID1=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID1=1000
end
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
	declare @TrackQty bit
	--EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
set @TrackQty=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from MATABLE007 where COLUMN02=@COLUMN05)
set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@COLUMN05)
if(@TrackQty=1)
begin
--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Values are Intiated for FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID1 as nvarchar(250)) +','+  @COLUMN05+','+  @COLUMN10+',0,0,'+  @COLUMN10+','+  cast((select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15)as nvarchar(250))+','+ @COLUMN22+','+  cast(@Location  as nvarchar(250))+','+ cast( @lotno as nvarchar(250))+','+  @COLUMNA02+','+
   @COLUMNA03 +  ' at ')
    )

insert into FITABLE010 
(
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
--EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08,COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMNA08,COLUMNA06,COLUMNA07,COLUMNB01,COLUMNB02,COLUMNB03,COLUMN24
)
values
(
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  @newID1,@COLUMN05,-(cast(@COLUMN10 as decimal(18,2))),'0', '0',-(cast(@COLUMN10 as decimal(18,2))),-(cast(0 as decimal(18,2))),-(cast(0 as decimal(18,2))), (select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15),@COLUMN22,@Location,@lotno,@COLUMNA02,@COLUMNA03,@ProjectID ,@COLUMNA08,@COLUMNA06,@COLUMNA06,@INVTRANSNO,@COLUMNB02,'INSERT' ,@COLUMN03
)
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Intiated Values are Created in FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

end
		end
		end
		--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
		else
		begin
		--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		exec usp_PUR_TP_InventoryUOM @Invoiceno,@Invoicelineref,@COLUMN05,@COLUMN10,@COLUMN16,@COLUMN22,@COLUMNA03,'Insert',@Location,@lotno
		end
		end
	end

if(@PID!='')
begin

IF(@BillQty=(@IQty))
begin
if(@IRQty=@BillQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
end
else if(@IRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
end
end
ELSE
BEGIN
if(@IQty=@IRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
end
else if(@IRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
end
end

UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=31) where COLUMN01=@COLUMN15
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE009 where COLUMN01= @COLUMN15))
--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
declare @invoiceqty decimal(18,2),@issueqty decimal(18,2)
set @invoiceqty=(select sum(isnull(COLUMN10,0)) from SATABLE010 where  COLUMN04=@COLUMN04  and COLUMNA13=0)
set @issueqty=(select sum(isnull(COLUMN09,0)) from SATABLE008 where    columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN02 in(@COLUMN04)))
if(@invoiceqty=@issueqty)
begin
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=81) where COLUMN02=@COLUMN04
end
else if(@invoiceqty<@issueqty)
begin
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=29) where COLUMN02=@COLUMN04
end
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=31)
UPDATE SATABLE009 set COLUMN13=@Result where COLUMN01=@COLUMN15
DECLARE @newID INT, @VENID INT
if(@chk=1)
Begin
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
--EMPHCS1596 rajasekhar reddy patakota 10/03/2016 Average Price Calculation Changes in Bill and Invoice Transactions
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN05) and COLUMN13=@COLUMN16 and isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03 and COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Invoice' and COLUMN04=@idname and COLUMN05=@Invoicelineref and COLUMN06=@COLUMN05 and COLUMNA02=@COLUMN16 and COLUMNA03=@COLUMNA03));
end
else
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03);
end
set @newID=cast((select ISNULL(MAX(COLUMN02),1000) from PUTABLE017) as int)+1
set @VENID=(select COLUMN05 from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Intiated for PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +', 1000 '+  cast(GETDATE() as nvarchar(250))+','+ isnull(@COLUMN15,'')+', INVOICE ,'+  cast(@VENID as nvarchar(250))+', 1000 ,'+ isnull( @COLUMN14,'')+','+  isnull(@COLUMN16,'')+','+ isnull(@COLUMNA01,'')+','+ isnull(@COLUMNA02,'')+','+ 
   isnull(@COLUMNA03,'')+','+ isnull(@COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+ isnull(@COLUMNA06,'')+','+ 			
   isnull(@COLUMNA07,'')+','+isnull(@COLUMNA08,'')+','+ isnull(@COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+ isnull(@COLUMNA11,'')+','+ isnull(@COLUMNA12,'')+','+ isnull(@COLUMNA13,'')+','+ isnull(@COLUMNB01,'')+','+ isnull(@COLUMNB02,'')+','+ isnull(@COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull(@COLUMNB05,'')+','+ isnull(@COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+ isnull(@COLUMNB08,'')+','+ isnull(@COLUMNB09,'')+','+ isnull(@COLUMNB10,'')+','+ isnull(@COLUMNB11,'')+','+ isnull(@COLUMNB12,'')+','+ isnull(@COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull(@COLUMND03,'')+','+ isnull(@COLUMND04,'')+','+isnull(@COLUMND05,'')+','+ isnull(@COLUMND06,'')+','+ isnull(@COLUMND07,'')+','+ isnull(@COLUMND08,'')+','+ isnull(@COLUMND09,'')+','+ isnull(@COLUMND10,'')+''+ 'at ')
    )

insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   @newID,'1000', @DT ,@COLUMN15,'INVOICE', @VENID,'1000',@MEMO,(cast(@COLUMN10 as DECIMAL(18,2))*cast(@AvgPrice as DECIMAL(18,2))),(cast(@COLUMN10 as DECIMAL(18,2))*cast(@AvgPrice as DECIMAL(18,2))),@COLUMN16,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN05, @Invoicelineref, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
end
	--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Intiated Values are Created in PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
end
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN02=(select COLUMN06 from SATABLE009 where COLUMN01= @COLUMN15)
---EMPHCS717		Output Taxes are not getting stored done by srinivas 7/21/2015
UPDATE PUTABLE013 SET COLUMN10=(CAST(@COLUMN10 AS DECIMAL(18,2))+ CAST(@PBillQty AS DECIMAL(18,2)))   WHERE COLUMN14=@COLUMN04 and COLUMN02 = (select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN05 from SATABLE009 where COLUMN01= @number1))

select @Invoiceno=COLUMN04,@number1=(column01) from SATABLE009 where column01=@COLUMN15
set @number = (select MAX(COLUMN01) from  SATABLE010 where column02=@COLUMN02);
 select  @Tax=ISNULL(COLUMN21,0) from SATABLE010 where COLUMN01=@number
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN23,@TTYPE INT = 0,@HTTYPE INT = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN21,0) from SATABLE010 where COLUMN01=@number
	if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			select @COLUMN23= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			select @COLUMN23= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	
	select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	set @headerTax= (select COLUMN23 from SATABLE009 where COLUMN01=@number1)
	select @Taxappliedon=COLUMN19,@headerTaxAG=column16,@headerTAXRATE=COLUMN07,@HTTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@headerTax
IF(ISNULL(@COLUMN30,0)=22556)
BEGIN SET @disval=((CAST(ISNULL(@COLUMN14,0) AS decimal(18,2))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
else
BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END	
declare @Amtin nvarchar(250),@Taxsubtotal nvarchar(250),@TotTaxAmt decimal(18,2),@ltaxamt decimal(18,2),@ReverseCharged bit,@CharAcc nvarchar(250),@CharAcc1 nvarchar(250)
set @Taxsubtotal=(@COLUMN14)
set @Amtin=(select COLUMN46 from SATABLE009 where COLUMN01=@COLUMN15);
set @ReverseCharged=(select isnull(COLUMN67,0) from SATABLE009 where COLUMN01=@COLUMN15);
--if(@form = 1604)set @ltaxamt=(((cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))-cast(isnull(@COLUMN19,0) as decimal(18,2)))*(cast(@TAXRATE as decimal(18,2))*0.01))
--else 
set @ltaxamt=((cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))*(cast(@TAXRATE as decimal(18,2))*0.01))
if(@Amtin='22713') 
begin 
set @Taxsubtotal=(cast(@COLUMN14 as decimal(18,2)))	
end	
	    if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
	    BEGIN
	    	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1134 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
	    	set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22383', @COLUMN11 = '1134',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
	    		values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,1134,@TaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Invoiceno,@project)
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
	    	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1142 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	    	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22402', @COLUMN11 = '1142',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	    	values(ISNULL(@Vid,1000),'INVOICE',@DT,@MEMO,@CUST,@ltaxamt,0,  @Invoiceno,1142,@project,@COLUMNA02, @COLUMNA03)
	    	--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
	    	--values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,1142,@TaxAG,@COLUMN23,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Invoiceno,@project)
	    END
	    else if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TTYPE=0 OR @TTYPE=22383))
		BEGIN
			--EMPHCS991	COA - bill and invoice tax details are showing in both input and output account  BY RAJ.Jr 28/8/2015
		declare @TaxColumn17 nvarchar(250)
	--EMPHCS1148 Tax amount not displaying in chart of accounts for invoice by srinivas 15/09/2015
		set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	--EMPHCS1410	Logs Creation by srinivas			
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',INVOICE,'+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@Tax as nvarchar(250))+','+  cast(@TaxAG as nvarchar(250))+','
   +  cast((CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+ isnull(@COLUMN05,'')+','+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@TaxColumn17,'')+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),'') +  ' at ')
    )	
	SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))	
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,@Tax,@TaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Invoiceno,@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		END
		ELSE if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22344)
		BEGIN
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SET @CharAcc = @TaxColumn17
			set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
	SET @AMNT = (cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),1000) from FITABLE036)+1)
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@COLUMN23,0) AS decimal(18,2)))
			insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
			values(@Vid,@DT,'INVOICE',@Invoiceno,@CUST,@number,@TaxColumn17,@ltaxamt,@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN05) 
		END
		ELSE if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22402)
		BEGIN
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SET @CharAcc = @TaxColumn17
			set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
			SET @AMNT = (cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@COLUMN23,0) AS decimal(18,2)))
			Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03,COLUMN12)
			values(@Vid,'INVOICE',@DT,@MEMO,@CUST,@ltaxamt,0,@Invoiceno,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Project)
		END
		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
		DECLARE @DISCTYPE INT,@DISCAMNT DECIMAL(18,2),@ExciseTotTax decimal(18,2)=null
		--IF(ISNULL(@COLUMN30,0)=22556)
		--BEGIN SET @DISCAMNT=(CAST(ISNULL(@COLUMN14,0) AS decimal(18,2))-(CAST(@COLUMN14 AS decimal(18,2))*CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100)) END
		--ELSE 
		--BEGIN SET @DISCAMNT=(CAST(ISNULL(@COLUMN14,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))) END
		SET @DISCAMNT=(CAST(ISNULL(@COLUMN14,0) AS decimal(18,2)))
		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		declare @KKC bit,@SBC bit,@KKCRate decimal(18,2),@SBCRate decimal(18,2),@Servicename nvarchar(250)
		set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@Tax)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@Tax)
		if(cast(@DT as date)>'11/14/2015')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@SBCRate))
			set @TaxColumn17=('Output SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			SET @AMNT = ((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@SBCRate)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @AMNT,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN14, @COLUMN17 = @SBCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@SBCRate),@COLUMN05,(CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@SBCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
			end
		if(cast(@DT as date)>'5/31/2016')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate))
			set @TaxColumn17=('Output KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			SET @AMNT = ((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @AMNT,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN14, @COLUMN17 = @KKCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate),@COLUMN05,(CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end

		declare @EDCESS bit,@SHECESS bit,@EDCRate decimal(18,2),@SHECRate decimal(18,2),@Excisename nvarchar(250),@lexcisetaxamt decimal(18,2)
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=23513)
		begin
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@Tax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@Tax)
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
			set @TaxColumn17=('OUTPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		    set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	        set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		    set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @TaxColumn17=('OUTPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		SET @AMNT = ((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
	end

		if(@Taxappliedon='Tax')
	    begin
		set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end

		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0)
		BEGIN
		----EMPHCS851 Invoice - Header level tax is not updating in Tax Register,also check in bill BY Raj.Jr 5/8/2015
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
		--EMPHCS991	COA - bill and invoice tax details are showing in both input and output account  BY RAJ.Jr 28/8/2015
		--EMPHCS1148 Tax amount not displaying in chart of accounts for invoice by srinivas 15/09/2015
		--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
		set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+CAST(@headerTAXRATE AS NVARCHAR(250))+'%')
		--set @ltaxamt=((isnull(cast(@COLUMN14 as decimal(18,2)),0)-cast(@disval as decimal(18,2)))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
		--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',INVOICE,'+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@headerTax as nvarchar(250))+','+  cast(@headerTaxAG as nvarchar(250))+','
   +  cast((CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+ isnull(@COLUMN05,'')+','+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@TaxColumn17,'')+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),'') +  ' at ')
    )
			SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @headerTAXRATE, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,@headerTax,@headerTaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@headerTAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		END

		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		begin
		set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		if(cast(@DT as date)>'11/14/2015')
		begin
	    if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
			set @TaxColumn17=('OUTPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    
			SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @SBCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@SBCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
			end
		if(cast(@DT as date)>'5/31/2016')
		begin
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
			set @TaxColumn17=('OUTPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @KKCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
			--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end

		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=23513)
		begin
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@headerTax)
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
			set @TaxColumn17=('OUTPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    --set @ltaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@headerTAXRATE,0) as decimal(18,2))*0.01))
			set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		    set @ltaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@headerTaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @TaxColumn17=('OUTPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		--set @ltaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@headerTAXRATE,0) as decimal(18,2))*0.01))
		set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		set @ltaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@headerTaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
	end
END

set @ReturnValue = 1
end
else
begin
return 0
end
END
 

IF @Direction = 'Select'
BEGIN
select * from SATABLE010
END 
 
 
IF @Direction = 'Update'
BEGIN
set @COLUMN16=(select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS714	At the time of invoice edit and save , system is getting some ID in Discount field
--set @COLUMN17=(select COLUMN15 from SATABLE009 where COLUMN01=@COLUMN15)
--set @COLUMN18=(select COLUMN16 from SATABLE009 where COLUMN01=@COLUMN15)
--set @COLUMN19=(select COLUMN17 from SATABLE009 where COLUMN01=@COLUMN15)
set @COLUMN12=(select COLUMN12 from SATABLE010 WHERE COLUMN02=@COLUMN02)
set @COLUMN08=(select COLUMN08 from SATABLE010 WHERE COLUMN02=@COLUMN02)
set @COLUMN09=(select COLUMN09 from SATABLE010 WHERE COLUMN02=@COLUMN02)
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
set @Location = (select isnull(COLUMN39,0) from  SATABLE009 where COLUMN01=@COLUMN15)
set @Location = (case when (cast(isnull(@COLUMN38,'') as nvarchar(250))!='' and CAST(@COLUMN38 AS NVARCHAR(250))!='0') then @COLUMN38 else @Location end)
set @COLUMN35 = (case when (cast(isnull(@COLUMN35,'') as nvarchar(250))='') then @COLUMN13 else @COLUMN35 end)
set @form=(select COLUMN03 from SATABLE009 where COLUMN01=@COLUMN15)
set @chk=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from matable007 where column02=@COLUMN05)
set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@COLUMN05)
set @COLUMN12 =(CAST(@COLUMN12 AS decimal(18,2))-CAST(@COLUMN10 AS decimal(18,2)) )
set @COLUMNA02=(@COLUMN16)
--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
set @COLUMNA12=(1)
set @COLUMNA13=(0)
set @column04 = (iif(@column04='',0,@column04))
set @ProjectID = (select COLUMN29 from SATABLE009 Where COLUMN01=@COLUMN15)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
if not exists( SELECT 1 FROM SATABLE010 WHERE COLUMN02=@COLUMN02 and COLUMN15=@COLUMN15)
begin
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE010_SequenceNo
--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Line Values are Intiated for SATABLE010 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+ isnull( @COLUMN18,'')+','+ isnull( @COLUMN19,'')+','+ isnull( @COLUMN20,'')+','+ isnull( @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull(  @COLUMN33,'')+','+isnull(  @COLUMN34,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+				  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ 'at ')
    )

insert into SATABLE010 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,   COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,   COLUMN19,  COLUMN20,  COLUMN21,
   --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   --EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  
   @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19, 
   --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,  @COLUMN22,  @COLUMN10,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,
   --EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN30,  @COLUMN31,  @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
   --EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Line Values are Created in SATABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
--EMPHCS1118 rajasekhar reddy 13/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
--set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN05)
--set @Invoiceno=(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15)
--set @Invoicelineref=(select COLUMN01 from SATABLE010 where COLUMN02=@COLUMN02)
--if(@uomselection=1 or @uomselection='1' or @uomselection='True')
--begin
--if(@UOMData is null)
--begin
--update FITABLE038 set COLUMNA13=0 where COLUMN04=@Invoiceno and COLUMN06=@COLUMN05 and COLUMN05=@Invoicelineref and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--end
--else
--begin
--EXEC usp_PUR_TP_InsertUOM @UOMData,'Invoice',@Invoiceno,@Invoicelineref,@COLUMN05,@COLUMN16,@COLUMNA03
--end
--end
end
else
begin
set @COLUMN36=(select COLUMN36 from SATABLE010 WHERE COLUMN02=@COLUMN02)
--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Line Values are Intiated for SATABLE010 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+							
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull(  @COLUMN33,'')+','+isnull(  @COLUMN34,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+			
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ 'at ')
    )
UPDATE SATABLE010 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
   --EMPHCS984 rajasekhar reddy patakota 20/8/2015  edit and save tax went to negatives
   COLUMN19=isnull(@COLUMN19,0),    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,    COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,
   --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   COLUMN24=@COLUMN22,    COLUMN25=@COLUMN10,    COLUMN26=@COLUMN26,    COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,  
   --EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,    COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,    COLUMN36=@COLUMN36,    COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  
     COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  
   COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  
   COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07, 
   COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  
   COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05, 
   COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02

   --EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Line Values are Updated in SATABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
   end
set @number1=(select COLUMN15 from  SATABLE010 WHERE COLUMN02=@COLUMN02 )
set @number = (select COLUMN01 from  SATABLE010 WHERE COLUMN02=@COLUMN02)
 --set @Tax= (select COLUMN21 from SATABLE010 where COLUMN01=@number)
 --set @headerTax= (select COLUMN23 from SATABLE009 where COLUMN01=@number1)
 --set @Taxappliedon= (select COLUMN19 from MATABLE013 where COLUMN02=@headerTax)
 --set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 --set @headerTaxAG= (select column16 from MATABLE013 where COLUMN02=@headerTax)
 --set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
 --set @TaxNo= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
 --set @headerTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@headerTax)
set @DT=(select column08 from SATABLE009 where column01=@number1)
set @CUST=(select column05 from SATABLE009 where column01=@number1)
set @MEMO=(select column12 from SATABLE009 where column01=@number1)
set @Project=(select column29 from SATABLE009 where column01=@number1)
set @idname=(select column04 from SATABLE009 where column01=@number1)
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
set @DBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE036 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
IF(ISNULL(@COLUMN30,0)=22556)
BEGIN SET @disval=((CAST(@COLUMN14 AS decimal(18,2))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
else
BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END

--EMPHCS1167	Service items calculation by srinivas22/09/2015
--if(@form = 1604)
--begin
--SET @ActAmt=(cast(ISNULL(@COLUMN14,0) as decimal(18,2))) 
--end	
--else
--begin
SET @ActAmt=(cast(ISNULL(@COLUMN14,0) as decimal(18,2))+cast(ISNULL(@COLUMN19,0) as decimal(18,2))) 
--end	

--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @expecteddiscount=(select iif(exists(select COLUMN07 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),(select COLUMN07 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),'0'));
set @claimtype=(select iif(exists(select COLUMN06 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),(select COLUMN06 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN05 and COLUMN03=@CUST and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),'0'));
set @claimtype=(select iif(exists(select COLUMN05 from MATABLE023 where COLUMN02=@claimtype  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),(select COLUMN05 from MATABLE023 where COLUMN02=@claimtype and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0),'0'));
set @actualdiscount=(select iif(exists(select COLUMN06 from MATABLE023 where column02=@COLUMN37 and column05='22831'),(select iif(isnull(COLUMN06,0)=0,isnull(COLUMN11,0),isnull(COLUMN06,0))COLUMN06 from MATABLE023 where column02=@COLUMN37 and column05='22831'),'0'));
set @fixedamt=(select iif(exists(select COLUMN11 from MATABLE023 where column02=@COLUMN37 and column05='22831'),(select isnull(COLUMN11,0) COLUMN11 from MATABLE023 where column02=@COLUMN37 and column05='22831'),0));
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+'expected discount'+cast(@expecteddiscount as nvarchar(250)) +','+'actual discount'+cast(@actualdiscount as nvarchar(250)) +','+'claimtype'+cast(@claimtype as nvarchar(250)) +','+
   'Item'+cast(@COLUMN05 as nvarchar(250)) +','+'Customer'+cast(@CUST as nvarchar(250)) +','+'Price Level Id '+cast(@COLUMN37 as nvarchar(250)) +','+
   ' at ') )
if((cast(isnull(@actualdiscount,0) as decimal(18,2))>cast(isnull(iif(@expecteddiscount!='',@expecteddiscount,'0'),0) as decimal(18,2)))  and cast(isnull(@actualdiscount,0) as decimal(18,2))>0)
begin
set @salesclaim=(select COLUMN04 from CONTABLE031 where COLUMN03='Sales Claim Required' and COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=isnull(@COLUMNA02,0) or COLUMNA02 is null) and  isnull(column04,0)=1 and isnull(columna13,0)=0)
if(@salesclaim=1 or @salesclaim='True')
begin
set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))-cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--IF(@claimtype='22777')
--begin
--set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))+cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--end
set @rate=(isnull(@COLUMN35,0));
set @claimamt=((cast(isnull(@COLUMN13,0) as decimal(18,2))*(cast(isnull(@claimdiscount,0) as decimal(18,2))*0.01))*cast(isnull(@COLUMN10,0) as decimal(18,2)));
if(@fixedamt>0.00)
begin
set @claimamt=((cast(isnull(@claimdiscount,0) as decimal(18,2)))*cast(isnull(@COLUMN10,0) as decimal(18,2)));
end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@COLUMN05 as nvarchar(250))+','
   +  cast(@rate as nvarchar(250))+','+ isnull(@claimamt,'')+' at ')
    )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Markdown Register',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22405', @COLUMN11 = '1129',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @claimamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

Insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11,COLUMN13,COLUMNA02, COLUMNA03,COLUMN12)
values(@AID,@DT,'Markdown Register',@number,@CUST,@MEMO,'1129',@claimamt,@claimamt,0,@COLUMN05, @COLUMNA02, @COLUMNA03,@Project)

set @AID=cast((select MAX(COLUMN02) from FITABLE034) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Assets  Values are Intiated For FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@COLUMN05 as nvarchar(250))+','
   +  cast(@rate as nvarchar(250))+','+ isnull(@claimamt,'')+' at ')
    )
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Markdown Register',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22402', @COLUMN11 = '1128',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @claimamt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMN12)
values(@AID,'Sales Claim Register',@DT,@MEMO,@CUST,@claimamt,0,@idname,'1128',@COLUMNA02, @COLUMNA03,@Project)

end	
end

IF(@disval>0)
BEGIN
set @AID=(CAST((select MAX(ISNULL(COLUMN02,999)) from FITABLE025)AS INT)+1)
set @AID1=(CAST((select MAX(ISNULL(COLUMN02,999)) from FITABLE036)AS INT)+1)
IF ((select COLUMN05 from MATABLE023 where COLUMN02= @COLUMN37 and COLUMNA03=@COLUMNA03) != '22831' or @form = 1532  or @form = 1604)
begin 
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @disval,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

--IF EXISTS (select column02 from FITABLE036 where COLUMN05=CAST(@number1 AS NVARCHAR(250)) AND COLUMN10=1049  AND COLUMN12=@COLUMN05 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE')
--	BEGIN
--	UPDATE FITABLE036 SET COLUMNA13=0,COLUMN07=@disval,COLUMN04=@DT WHERE COLUMN05=CAST(@number1 AS NVARCHAR(250)) AND COLUMN10=1049   AND COLUMN12=@COLUMN05  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
--	END
--	ELSE
--	BEGIN
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
values(@AID1,@DT,'INVOICE',@idname,@CUST,@number1,1049,CAST(isnull(@disval,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN05) 
--END
END
END

IF(ISNULL(@COLUMN30,0)=22556)
BEGIN SET @disval=((CAST(@COLUMN14 AS decimal(18,2))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
else
BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END

set @Taxsubtotal=(@COLUMN14)
set @Amtin=(select COLUMN46 from SATABLE009 where COLUMN01=@COLUMN15);
if(@Amtin='22713') 
begin 
set @Taxsubtotal=(cast(@COLUMN14 as decimal(18,2)))	
end	
select @Invoiceno=COLUMN04, @number1=(column01) from SATABLE009 where column01=@COLUMN15
set @number = (select MAX(COLUMN01) from  SATABLE010 where column02=@COLUMN02);
 set @Tax= (select ISNULL(COLUMN21,0) from SATABLE010 where COLUMN01=@number)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
set @AMOUNTM=@COLUMN23;set @cnt=0 set @vochercnt=1
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN21,0) from SATABLE010 where COLUMN01=@number
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			select @COLUMN23= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			select @COLUMN23= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end

	select @Invoiceno=COLUMN04,@headerTax=COLUMN23,@ReverseCharged=isnull(COLUMN67,0) from SATABLE009 where COLUMN01=@number1
	select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	select @Taxappliedon=COLUMN19,@headerTaxAG=column16,@headerTAXRATE=COLUMN07,@HTTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@headerTax
	--if(@form = 1604)set @ltaxamt=(((cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))-cast(isnull(@COLUMN19,0) as decimal(18,2)))*(cast(@TAXRATE as decimal(18,2))*0.01))
	--else 
	set @ltaxamt=((cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))*(cast(@TAXRATE as decimal(18,2))*0.01))
	if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		BEGIN
			set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1134 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
				SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22383', @COLUMN11 = '1134',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
				values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,1134,@TaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Invoiceno,@project)
			set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
			set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1142 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = '22402', @COLUMN11 = '1142',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
			values(ISNULL(@Vid,1000),'INVOICE',@DT,@MEMO,@CUST,@ltaxamt,0,  @Invoiceno,1142,@project,@COLUMNA02, @COLUMNA03)
			--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,1142,@TaxAG,@COLUMN23,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Invoiceno,@project)
		END
		ELSE if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TTYPE=22383 OR @TTYPE=0))
		BEGIN
	set @Vid=(CAST((select MAX(ISNULL(COLUMN02,999)) from FITABLE026)AS INT)+1)
	--EMPHCS1148 Tax amount not displaying in chart of accounts for invoice by srinivas 15/09/2015
		set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	--EMPHCS1410	Logs Creation by srinivas			
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',INVOICE,'+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@Tax as nvarchar(250))+','+  cast(@TaxAG as nvarchar(250))+','
   +  cast((CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+ isnull(@COLUMN05,'')+','+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@TaxColumn17,'')+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),'') +  ' at ')
    )		
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+(CAST(isnull(@COLUMN23,0) as decimal(18,2))))
			SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))	
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,@Tax,@TaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Invoiceno,@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		END
	    ELSE if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22344)
		BEGIN
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SET @CharAcc = @TaxColumn17
			set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			SET @AMNT = (cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),1000) from FITABLE036)+1)
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@COLUMN23,0) AS decimal(18,2)))
			insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
			values(@Vid,@DT,'INVOICE',@Invoiceno,@CUST,@number,@TaxColumn17,@ltaxamt,@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN05) 
		END
		ELSE if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22402)
		BEGIN
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
			SET @CharAcc = @TaxColumn17
			set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
			SET @AMNT = (cast(isnull(@COLUMN10,0) as decimal(18,2))*cast(isnull(@COLUMN35,0) as decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@COLUMN23,0) AS decimal(18,2)))
			Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03,COLUMN12)
			values(@Vid,'INVOICE',@DT,@MEMO,@CUST,@ltaxamt,0,@Invoiceno,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Project)
		END
		--IF(ISNULL(@COLUMN30,0)=22556)
		--BEGIN SET @DISCAMNT=(CAST(ISNULL(@COLUMN14,0) AS decimal(18,2))-(CAST(@COLUMN14 AS decimal(18,2))*CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100)) END
		--ELSE 
		--BEGIN SET @DISCAMNT=(CAST(ISNULL(@COLUMN14,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))) END
		SET @DISCAMNT=(CAST(ISNULL(@COLUMN14,0) AS decimal(18,2)))
		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
	    set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@Tax)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@Tax)
		if(cast(@DT as date)>'11/14/2015')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@SBCRate))
			set @TaxColumn17=('OUTPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			SET @AMNT = ((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@SBCRate)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @AMNT,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN14, @COLUMN17 = @SBCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@SBCRate),@COLUMN05,(CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@SBCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
			end
		if(cast(@DT as date)>'5/31/2016')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate))
			set @TaxColumn17=('OUTPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			SET @AMNT = ((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @AMNT,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN14, @COLUMN17 = @KKCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate),@COLUMN05,(CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end

		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=23513)
		begin
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@Tax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@Tax)
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
			set @TaxColumn17=('OUTPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		    set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	        set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		 set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @TaxColumn17=('OUTPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		SET @AMNT = ((CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(ISNULL(@COLUMN19,0) AS decimal(18,2)))/100)*(@KKCRate)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @Invoiceno, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
	    end
		
		if(@Taxappliedon='Tax')
	    begin
		set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end

		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0)
		BEGIN
		----EMPHCS851 Invoice - Header level tax is not updating in Tax Register,also check in bill BY Raj.Jr 5/8/2015
		set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
		--EMPHCS991	COA - bill and invoice tax details are showing in both input and output account  BY RAJ.Jr 28/8/2015
		--EMPHCS1148 Tax amount not displaying in chart of accounts for invoice by srinivas 15/09/2015
		--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
		set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+CAST(@headerTAXRATE AS NVARCHAR(250))+'%')
		 --set @ltaxamt=((isnull(cast(@COLUMN14 as decimal(18,2)),0)-cast(@disval as decimal(18,2)))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
		--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',INVOICE,'+  cast(@number as nvarchar(250))+','+  cast(@CUST as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@headerTax as nvarchar(250))+','+  cast(@headerTaxAG as nvarchar(250))+','
   +  cast((CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+ isnull(@COLUMN05,'')+','+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@TaxColumn17,'')+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),'') +  ' at ')
    )
	SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @headerTAXRATE, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@Vid,@DT,'INVOICE',@number,@CUST,@MEMO,@headerTax,@headerTaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@headerTAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		END

		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		begin
	    set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		if(cast(@DT as date)>'11/14/2015')
		begin
	    if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
			set @TaxColumn17=('OUTPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)		    
			
			SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @SBCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@SBCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
			end
		if(cast(@DT as date)>'5/31/2016')
		begin
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
			set @TaxColumn17=('OUTPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)		    
			SET @AMNT = (CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @AMNT, @COLUMN17 = @KKCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
			--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Servicename,@headerTaxAG,@ltaxamt,@COLUMN05,(CAST(isnull(@ActAmt,0) AS decimal(18,2))-CAST(isnull(@disval,0) AS decimal(18,2))),@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end
		
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=23513)
		begin
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@headerTax)
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
			set @TaxColumn17=('OUTPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    --set @ltaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@headerTAXRATE,0) as decimal(18,2))*0.01))
		    set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		    set @ltaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	        exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
			values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@headerTaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @TaxColumn17=('OUTPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		--set @ltaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@headerTAXRATE,0) as decimal(18,2))*0.01))
		set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		set @ltaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN05,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@DT,'INVOICE',@number,@CUST,@MEMO,@Excisename,@headerTaxAG,@ltaxamt,@COLUMN05,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15),@Project)
		end
	    end
END
--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN05)
set @Invoiceno=(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15)
set @Invoicelineref=(select COLUMN01 from SATABLE010 where COLUMN02=@COLUMN02)
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
set @lotno=(case when @column31='' then 0 when isnull(@column31,0)=0 then 0  else @column31 end)
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
if(@UOMData is null)
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@Invoiceno and COLUMN06=@COLUMN05 and COLUMN05=@Invoicelineref and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
else
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'Invoice',@Invoiceno,@Invoicelineref,@COLUMN05,@COLUMN16,@COLUMNA03
end
end
SET @PID=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
SET @SPID=(SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
set @number = (select COLUMN01 from  SATABLE010 WHERE COLUMN02=@COLUMN02);
   if(@SPID=0)
	BEGIN
	DECLARE @QtyH nvarchar(250),@QtyA nvarchar(250)
	--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
    --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
     --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
     --EMPHCS1167	Service items calculation by srinivas22/09/2015
   if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False') and @chk=1)
      begin
	  --EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
	  --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)
begin
      --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
       set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
	set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
	set @Qty_AVL=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)
	set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
			if(@Qty_AVL<0)
			begin
				set @Qty_AVL=0
				set @Qty_BACK =@Qty_Commit
				set @Qty_Commit=0
			end
	set @Qty=cast(@COLUMN10 as DECIMAL(18,2));
	set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
	set @Qty_Commit=( @Qty_Commit - @Qty)
	set @Qty_AVL=(@Qty_On_Hand)-@Qty_Commit
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Values are Intiated for FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

			UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN07 =@Qty_BACK, COLUMN08= @Qty_On_Hand ,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@INVTRANSNO,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE',COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)as DECIMAL(18,2))-(cast(@COLUMN10 as DECIMAL(18,2))*cast(@AvgPrice as DECIMAL(18,2)))) WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03
			set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@INVTRANSNO,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@INVTRANSNO,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03
				end
	--EMPHCS1410	Logs Creation by srinivas			
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Intiated Values are Updated in FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

			--UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22)as DECIMAL(18,2))/@Qty_On_Hand)as DECIMAL(18,2)) WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22
			--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
		end
--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID1=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
	--EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
set @TrackQty=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from MATABLE007 where COLUMN02=@COLUMN05)
set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@COLUMN05)
if(@TrackQty=1)
begin
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Values are Intiated for FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID1 as nvarchar(250)) +','+  isnull(@COLUMN05,'')+','+  isnull(@COLUMN10,'')+',0,0,'+  isnull(@COLUMN10,'')+','+   isnull(cast((select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15)as nvarchar(250)),'')+','+ isnull(@COLUMN22,'')+','+  isnull(@Location,'')+','+  isnull(@lotno,'')+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +  ' at ')
    )
insert into FITABLE010 
(
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
--EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08,COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMNA08,COLUMNA06,COLUMNA07,COLUMNB01,COLUMNB02,COLUMNB03,COLUMN24
)
values
(
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @newID1,@COLUMN05,-(cast(@COLUMN10 as decimal(18,2))),'0', '0',-(cast(@COLUMN10 as decimal(18,2))),-(cast(0 as decimal(18,2))),-(cast(0 as decimal(18,2))), (select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15),@COLUMN22,@Location,@lotno,@COLUMNA02,@COLUMNA03,@ProjectID,@COLUMNA08,@COLUMNA06,@COLUMNA06,@INVTRANSNO,@COLUMNB02,'UPDATE',@COLUMN03 
)
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Intiated Values are Created in FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
end
		end
		end
		else
		begin
		--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		exec usp_PUR_TP_InventoryUOM @Invoiceno,@Invoicelineref,@COLUMN05,@COLUMN10,@COLUMN16,@COLUMN22,@COLUMNA03,'Insert',@Location,@lotno
		end
		if(@chk=1)
Begin
		--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Inventory Asset Calculation		 
		--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
		--EMPHCS1596 rajasekhar reddy patakota 10/03/2016 Average Price Calculation Changes in Bill and Invoice Transactions
		if(@uomselection=1 or @uomselection='1' or @uomselection='True')
		begin
		--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
		set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN05) and COLUMN13=@COLUMN16 and isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03 and COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Invoice' and COLUMN04=@idname and COLUMN05=@Invoicelineref and COLUMN06=@COLUMN05 and COLUMNA02=@COLUMN16 and COLUMNA03=@COLUMNA03))
		end
		else
		begin
		set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND COLUMN19=@COLUMN22 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and COLUMN24=@COLUMN03);
	    end
	    set @newID=cast((select ISNULL(MAX(COLUMN02),1000) from PUTABLE017) as int)+1
		set @VENID=(select COLUMN05 from SATABLE009 where COLUMN01=@COLUMN15)
	--EMPHCS1410	Logs Creation by srinivas	
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Intiated for PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +', 1000 '+  cast(GETDATE() as nvarchar(250))+','+ isnull(@COLUMN15,'')+', INVOICE ,'+  cast(@VENID as nvarchar(250))+', 1000 ,'+  isnull(@COLUMN14,'')+','+ isnull(@COLUMN16,'')+','+isnull(@COLUMNA01,'')+','+ 
   isnull(@COLUMNA02,'')+','+ isnull(@COLUMNA03,'')+','+ isnull(@COLUMNA04,'')+','+isnull(@COLUMNA05,'')+','+ isnull(@COLUMNA06,'')+','+ 			
   isnull(@COLUMNA07,'')+','+ isnull(@COLUMNA08,'')+','+ isnull(@COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+ isnull(@COLUMNA11,'')+','+ isnull(@COLUMNA12,'')+','+ isnull(@COLUMNA13,'')+','+ isnull(@COLUMNB01,'')+','+ isnull(@COLUMNB02,'')+','+ isnull(@COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+ isnull(@COLUMNB05,'')+','+ isnull(@COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+ isnull(@COLUMNB08,'')+','+ isnull(@COLUMNB09,'')+','+ isnull(@COLUMNB10,'')+','+ isnull(@COLUMNB11,'')+','+ isnull(@COLUMNB12,'')+','+ isnull(@COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+ isnull(@COLUMND03,'')+','+ isnull(@COLUMND04,'')+','+isnull(@COLUMND05,'')+','+ isnull(@COLUMND06,'')+','+ isnull(@COLUMND07,'')+','+ isnull(@COLUMND08,'')+','+ isnull(@COLUMND09,'')+','+ isnull(@COLUMND10,'')+''+ 'at ')
    )

		insert into PUTABLE017 
		( 
		--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
		   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
		   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
		   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
		   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
		)
		values
		(  
		--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
		   @newID,'1000', @DT ,@COLUMN15,'INVOICE', @VENID,'1000',@MEMO,(cast(@COLUMN10 as DECIMAL(18,2))*cast(@AvgPrice as DECIMAL(18,2))),(cast(@COLUMN10 as DECIMAL(18,2))*cast(@AvgPrice as DECIMAL(18,2))),@COLUMN16,@Project,
		   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
		   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
		   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
		   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN05, @Invoicelineref, @COLUMND07,
		   @COLUMND08, @COLUMND09, @COLUMND10
		)
		end
	--EMPHCS1410	Logs Creation by srinivas	
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Intiated Values are Created in PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	END
	
if(@PID!='')
begin
--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   SET @PID=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
if(isnull(@COLUMN36,0)>0)
begin
UPDATE SATABLE010 SET COLUMN09=(cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0 ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
UPDATE SATABLE010 SET COLUMN12=(cast((select sum(isnull(COLUMN09,0)) from SATABLE008 where COLUMN26=@COLUMN36 and isnull(COLUMNA13,0)=0) as decimal(18,2))-cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0  ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
SET @PBillQty=(SELECT isnull(COLUMN13,0) FROM SATABLE006 WHERE COLUMN02=@COLUMN36 and COLUMNA13=0)
UPDATE SATABLE006 SET   COLUMN13=((@PBillQty)+ CAST(@COLUMN10 AS DECIMAL(18,2))) where  COLUMN02=@COLUMN36 and COLUMNA13=0
end
else
begin
UPDATE SATABLE010 SET COLUMN09=(cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN04 in(select COLUMN02 from SATABLE007 where COLUMN06=@PID) and COLUMN05=@COLUMN05 and COLUMNA13=0  ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
UPDATE SATABLE010 SET COLUMN12=(cast((select sum(isnull(COLUMN09,0)) from SATABLE008 where COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN02=@COLUMN04) and COLUMN04=@COLUMN05 and COLUMNA13=0) as decimal(18,2))-cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN04=@COLUMN04 and COLUMN05=@COLUMN05 and COLUMNA13=0  ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
   UPDATE SATABLE006 SET   COLUMN13=((cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE  COLUMN05=@COLUMN05 and COLUMNA13=0  and isnull(COLUMN22,0)=isnull(@COLUMN22,0) and isnull(COLUMN31,0)=isnull(@COLUMN31,0) and COLUMN15 in(SELECT COLUMN01 FROM SATABLE009 WHERE COLUMN06 in(@PID)) ) as decimal(18,2)))) where COLUMN03=@COLUMN05  and isnull(COLUMN27,0)=isnull(@COLUMN22,0) and isnull(COLUMN17,0)=isnull(@COLUMN31,0) and COLUMN19=
   (select COLUMN01 from SATABLE005 where COLUMN02 in (@PID))
end
--SET @PBillQty=isnull((SELECT (COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@PID)) and COLUMN03=@COLUMN05),0)
--UPDATE SATABLE006 SET   COLUMN13=((@PBillQty)+ CAST(@COLUMN10 AS DECIMAL(18,2))) where COLUMN03=@COLUMN05 and COLUMN19=
--(select COLUMN01 from SATABLE005 where COLUMN02 in (@PID))
SET @BillQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMNA13=0)
SET @IRQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMNA13=0)
SET @IQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMNA13=0 )

IF(@BillQty=(@IQty))
begin
if(@IRQty=@BillQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
end
else if(@IRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
end
end
ELSE
BEGIN
if(@IQty=@IRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
end
else if(@IRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
end
end

UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=31) where COLUMN01=@COLUMN15
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE009 where COLUMN01= @COLUMN15))
--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
set @invoiceqty=(select sum(isnull(COLUMN10,0)) from SATABLE010 where  COLUMN04=@COLUMN04  and COLUMNA13=0)
set @issueqty=(select sum(isnull(COLUMN09,0)) from SATABLE008 where    columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN02 in(@COLUMN04)))
if(@invoiceqty=@issueqty)
begin
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=81) where COLUMN02=@COLUMN04
end
else if(@invoiceqty<@issueqty)
begin
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=29) where COLUMN02=@COLUMN04
end
end
else
begin
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
   UPDATE SATABLE010 SET COLUMN09=(cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN15=@COLUMN15 and COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0  ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
   UPDATE SATABLE010 SET COLUMN12=(cast((select max(isnull(COLUMN09,0)) from SATABLE010 where COLUMN15=@COLUMN15 and  COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0) as decimal(18,2))-cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN15=@COLUMN15 and  COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
end   
else
begin
--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
   UPDATE SATABLE010 SET COLUMN09=(cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN15=@COLUMN15 and COLUMNA13=0 and COLUMN05=@COLUMN05  ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
   UPDATE SATABLE010 SET COLUMN12=(cast((select max(isnull(COLUMN09,0)) from SATABLE010 where COLUMN15=@COLUMN15 and COLUMNA13=0 and COLUMN05=@COLUMN05 ) as decimal(18,2))-cast(( select sum(isnull(COLUMN10,0)) from SATABLE010 WHERE COLUMN15=@COLUMN15 and COLUMNA13=0 and COLUMN05=@COLUMN05  ) as decimal(18,2))) WHERE COLUMN02=@COLUMN02
end
   SET @PID=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=31)
--where using internal id of line level instead of header internal id
UPDATE SATABLE009 set COLUMN13=@Result where COLUMN01=@COLUMN15
end

Declare @IVID int,@IVAMT decimal(18,2),@PAMT decimal(18,2)
SET @IVID=(SELECT COLUMN02 FROM SATABLE009 WHERE COLUMN01=@COLUMN15 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
if exists(select COLUMN02 from SATABLE012 where COLUMN03=@IVID and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
begin
SET @IVAMT=(SELECT isnull(COLUMN20,0) FROM SATABLE009 WHERE COLUMN01=@COLUMN15 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
SET @PAMT=(SELECT SUM(cast(iif(isnull(cast(COLUMN14 as nvarchar(250)),'')='','0',COLUMN14)as decimal(18,2))+cast(iif(isnull(cast(COLUMN13 as nvarchar(250)),'')='','0',COLUMN13)as decimal(18,2))+cast(iif(isnull(cast(COLUMN06 as nvarchar(250)),'')='','0',COLUMN06)as decimal(18,2))) 
FROM SATABLE012 where COLUMN03=@IVID and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
if(@IVAMT=@PAMT)
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=34) where COLUMN01=@COLUMN15 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
else
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=32) where COLUMN01=@COLUMN15 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
end
else
begin
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=31) where COLUMN01=@COLUMN15 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
end

UPDATE SATABLE005 set COLUMN16=@Result where COLUMN02=(select COLUMN06 from SATABLE009 where COLUMN01= @COLUMN15)

UPDATE PUTABLE013 SET COLUMN11=(CAST(@COLUMN10 AS DECIMAL(18,2))+ CAST(@PBillQty AS DECIMAL(18,2)))   WHERE COLUMN14=@COLUMN05 and COLUMN02 = (select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN05 from SATABLE009 where COLUMN01= @COLUMN15))

set @chk=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from matable007 where column02=@COLUMN05)
set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@COLUMN05)
set @PRICE=(select column17 from matable007 where column02=@COLUMN05)
--EMPHCS730	Account Owner Condition for Income Register by srinivas 7/21/2015
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
--if(@chk=0)
--	begin
	DECLARE @id nvarchar(250)
	DECLARE @MaxRownum INT
	DECLARE @Item decimal(18,2),
	@AMOUNT decimal(18,2)=0,
	@BAL decimal(18,2)=0,
	@AMOUNT1 decimal(18,2)=0,@lineID nvarchar(250),@lineID1 nvarchar(250)
		  DECLARE @Initialrow INT=1
	set @lineID1=(select column01 from satable010 where column02=(cast(@COLUMN02 as int)) )
	SET @AMOUNT=(SELECT isnull(COLUMN09,0) FROM FITABLE025 WHERE COLUMN05=@lineID1 AND COLUMN08=1052  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
	IF(ISNULL(@COLUMN30,0)=22556)
	BEGIN SET @disval=((CAST(@COLUMN14 AS decimal(18,2))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
	else
	BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END
  if(@Itemtype='ITTY001')
  begin
	set @CUST=(select column05 from SATABLE009 where column01=@number1)
	set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
	set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
	set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
	set @lineiid=(select max(column01) from SATABLE010 where column02=@COLUMN02)
		if(@AID>0)
			begin
			--Sales of Product Income storing each row wise at line level by srinivas 7/21/2015
			--delete from FITABLE025 where column05=@number1	 and column08=1052
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
	--EMPHCS1410	Logs Creation by srinivas			
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  'INVOICE'+','+ cast(@lineiid as nvarchar(250))+','+ cast( @CUST as nvarchar(250))+',NULL,1052,'+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+', '+   cast(@SBAL+CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+isnull(@COLUMNA03,'') + ' at ')
					)
					--//EMPHCS1788 SEGREGATION OF SERVICE ITEM'S SALES AND PURCHASE ACCOUNTS
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03  AND COLUMN52!=0),(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03  AND COLUMN52!=0),1052))
			SET @ACTYPE=(SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ActAmt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
			values(@AID,@DT,'INVOICE',@lineiid,@CUST,@MEMO,@ACID,(CAST(isnull(@ActAmt,0) AS decimal(18,2))),@SBAL+(CAST(isnull(@ActAmt,0) AS decimal(18,2))),@COLUMNA02 , @COLUMNA03,0,@Project)
			--Changed by Raj Patakota on 07/17/2015 to handle decimal
			--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + '')
					)
			if(@AID>0)
			begin
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	if(@uomselection=1 or @uomselection='1' or @uomselection='True')
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN05) and COLUMN13=@COLUMN16 and isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN03 and COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Invoice' and COLUMN04=@idname and COLUMN05=@Invoicelineref and COLUMN06=@COLUMN05 and COLUMNA02=@COLUMN16 and COLUMNA03=@COLUMNA03))
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16 AND isnull(COLUMN19,0)=isnull(@COLUMN22,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@ProjectID,0)  and COLUMN24=@COLUMN03);
			end
	--EMPHCS1410	Logs Creation by srinivas		
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  'INVOICE'+','+ cast(@lineiid as nvarchar(250))+','+ cast( @CUST as nvarchar(250))+',NULL,1056,'+  cast(cast(cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))as DECIMAL(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2))AS decimal(18,2)) as nvarchar(250))+', '+   cast(@CBAL+cast(cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))as DECIMAL(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2))AS decimal(18,2)) as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+isnull(@COLUMNA03,'') + ' at ')
					)
			IF(ISNULL(@COLUMN30,0)=22556)
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			BEGIN SET @disval=(((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2)))*(CAST(ISNULL(@COLUMN19,0) AS decimal(18,2))/100))) END
			else
			BEGIN SET @disval=(cast(ISNULL(@COLUMN19,0) AS decimal(18,2))) END
					--//EMPHCS1788 SEGREGATION OF SERVICE ITEM'S SALES AND PURCHASE ACCOUNTS
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1056))
			SET @ACTYPE=(SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
SET @disval = ((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))))			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @disval,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMND05, COLUMND06,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			values(@AID,@DT,'INVOICE',@lineiid,@CUST,@MEMO,@ACID,((cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2)))),@CBAL+(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN10,0)as DECIMAL(18,2))),@COLUMNA02 , @COLUMNA03,0,@COLUMN05,@lineiid,@Project)
	--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + '')
					)
	end
	     --EMPHCS1167	Service items calculation by srinivas22/09/2015
	else
	begin
set @AID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE025)+1
	--EMPHCS1410	Logs Creation by srinivas
	set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@AID as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+','+  'INVOICE'+','+ cast(@lineID1 as nvarchar(250))+','+ cast( @CUST as nvarchar(250))+',NULL,1053,'+  cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+', '+   cast(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)) as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+isnull(@COLUMNA03,'') + ' at ')
					)
					--//EMPHCS1788 SEGREGATION OF SERVICE ITEM'S SALES AND PURCHASE ACCOUNTS
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1053))
SET @ACTYPE=(SELECT TOP 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @idname, @COLUMN05 = @COLUMN15,  @COLUMN06 = @number,
		@COLUMN07 = @DT,   @COLUMN08 = '22335',  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @ActAmt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1590 rajasekhar reddy patakota 07/03/2016 Latest issues fixes raised by naveen
			values(@AID,@DT,'INVOICE',@lineID1,@CUST,@MEMO,@ACID,(CAST(isnull(@ActAmt,0) AS decimal(18,2))),(CAST(isnull(@ActAmt,0) AS decimal(18,2))),@COLUMNA02 , @COLUMNA03,0,@Project)
	--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + '')
					)
	end
   end

else IF @Direction = 'Delete'
BEGIN
UPDATE SATABLE010 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
SET @number=(SELECT COLUMN01 FROM SATABLE010 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN14=(SELECT COLUMN14 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN06=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA02=(SELECT COLUMNA02 FROM SATABLE010 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA03=(SELECT COLUMNA03 FROM SATABLE010 WHERE COLUMN02 = @COLUMN02)
set @form=(select COLUMN03 from SATABLE009 where COLUMN02 = @COLUMN02)
--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--UPDATE FITABLE025 SET COLUMNA13=@COLUMNA13
			-- WHERE COLUMN08=1056 AND COLUMN05=@number AND COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03
	--DECLARE cur2 CURSOR FOR SELECT COLUMN02 from FITABLE025 where COLUMN05>@number AND  COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--	  OPEN cur2
	--	  FETCH NEXT FROM cur2 INTO @id
	--	  SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE025 where COLUMN05>@number AND  COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
	--	     WHILE @Initialrow <= @MaxRownum
	--	     BEGIN 
	--	         set @Item=(select COLUMN10 from FITABLE025 where COLUMN02=@id)
	--		     UPDATE FITABLE025 SET COLUMN10=cast(@Item as decimal(18,2))-cast(@COLUMN14 as decimal(18,2))
	--				WHERE COLUMN02=@id
	--			 FETCH NEXT FROM cur2 INTO @id
	--	         SET @Initialrow = @Initialrow + 1 
	--		END
	--	CLOSE cur2 
	--EMPHCS991	COA - bill and invoice tax details are showing in both input and output account  BY RAJ.Jr 28/8/2015
		UPDATE FITABLE026 SET COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN04='INVOICE' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03
		UPDATE FITABLE036 SET COLUMNA13=@COLUMNA13 WHERE COLUMN05=@number AND COLUMN03='INVOICE' AND COLUMN10=1049   AND COLUMN12=@COLUMN05 AND  COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03
		--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
END
   --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_SAL_TP_SATABLE010.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
--EMPHCS1410	Logs Creation by srinivas
	 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_SATABLE010.txt',0

if not exists(select column01 from SATABLE010 where column15=@COLUMN15)
					begin
					delete SATABLE009 where column01=@COLUMN15
					return 0
					end
return 0  
end catch
end










GO
