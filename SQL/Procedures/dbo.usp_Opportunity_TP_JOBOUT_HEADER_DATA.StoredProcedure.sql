USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Opportunity_TP_JOBOUT_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_Opportunity_TP_JOBOUT_HEADER_DATA]
(
	@Opportunity int
)

AS
BEGIN
	SELECT COLUMN05 as COLUMN53,COLUMN04  AS COLUMN11,COLUMN02,COLUMN11 as COLUMN24,COLUMN29 as COLUMN09,
	--EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
	COLUMN19 COLUMN35
	FROM SATABLE013 WHERE  COLUMN02= @Opportunity
END
GO
