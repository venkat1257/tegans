USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE023]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE023]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null, @COLUMN27   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)

AS

BEGIN
 begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE023_SequenceNo
declare @tempSTR nvarchar(max)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Header Values are Intiated for FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
	IF(@COLUMN27 = '22713')
	SET @COLUMN19= (cast(cast(ISNULL(@COLUMN10,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2))as nvarchar(250)))
	ELSE
	SET @COLUMN19= (cast((ISNULL(@COLUMN10,0))as nvarchar(250)))

insert into FITABLE023
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN24,  COLUMN25, COLUMN27,  
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN10,  @COLUMN20,  @COLUMN24, 
   @COLUMN25,
   @COLUMN27,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Header Intiated Values are Created in FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
   declare @status nvarchar(250)
   set @status=(select column04 from contable025 where column02=64)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received Status Updation for FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Status'+
   isnull(cast(@status as nvarchar(250)),'') +','+  '2.Advance Payment No'+','+  isnull(cast(@COLUMN02  as nvarchar(250)),'')+','+ '4.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 update FITABLE023 set  COLUMN13=@status where column02=@COLUMN02 and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received Updated in FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''))
 --EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
 declare @balance decimal(18,2)
 declare @pid nvarchar(250)=null
 declare @internalid nvarchar(250)=null
set @internalid= (select column01 from FITABLE023 where COLUMN02=@COLUMN02)
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN09)

	--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
	declare @Tax nvarchar(250)=null, @TAXRATE decimal(18,2)=null, @TaxAG nvarchar(250)=null, @Taxregid nvarchar(250)=null,@Vid int
  set @Tax= (@COLUMN12)
  set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
  set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
  set @Taxregid= (SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE026)+1
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application

		--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TaxAG in(23585,23586,23584,23583,23582))
		--BEGIN
		--	set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
		--	declare @TaxColumn17 nvarchar(250)
		--	set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
		--	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN17,  COLUMNA02, COLUMNA03)
		--	values(@Vid,@COLUMN05,'RECEIVE PAYMENT',@internalid,@COLUMN08,@COLUMN11,1143,@COLUMN04,@COLUMN24,@COLUMN10,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03)
		
		--	--set @Vid=((select isnull(max(column02),999) from FITABLE034)+1)
		--	--set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
		--	--Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,  COLUMN10,  COLUMNA02, COLUMNA03)
		--	--				values(@Vid,'RECEIVE PAYMENT',@COLUMN05,@COLUMN11,@COLUMN08,@COLUMN24,@COLUMN04,1143,@COLUMNA02, @COLUMNA03)
		--END
set @ReturnValue =(select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from FITABLE023

END 

 

ELSE IF @Direction = 'Update'

BEGIN
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
declare @Prebalance decimal(18,2)
set @Prebalance= (select COLUMN10 from FITABLE023 where COLUMN02=@COLUMN02)
set @internalid= (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN02)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Advance Received Header Values are Intiated for FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
	--IF(@COLUMN27 = 22713)
	--SET @COLUMN19= (cast(cast(ISNULL(@COLUMN10,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2))as nvarchar(250)))
	--ELSE
	--SET @COLUMN19= (cast((ISNULL(@COLUMN10,0))as nvarchar(250)))
UPDATE FITABLE023 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,							 COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,    
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,							COLUMN20=@COLUMN20,    COLUMN24=@COLUMN24,
   COLUMN25=@COLUMN25,
   COLUMN27=@COLUMN27,	  COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,   COLUMNA07=@COLUMNA07,--  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Advance Received Header Intiated Values are Updated in FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )  
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN09 and COLUMNA03=@COLUMNA03)
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
   'Advance Received Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+ 'ADVANCE RECEIPT'+',' +  isnull( cast(@COLUMNA02 as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
   --EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
--delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='ADVANCE RECEIPT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
delete from fitable064 where column03 = 'ADVANCE RECEIPT' and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
delete from FITABLE026 where COLUMN09=@COLUMN04 and COLUMN04='ADVANCE RECEIPT' and COLUMN05=@internalid and COLUMN08=1119 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'tax Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(cast(@internalid as nvarchar(250)),'') +','+ isnull( cast(@COLUMN04 as nvarchar(250)),'')+',' + 'ADVANCE RECEIPT'+',' +  isnull( cast(@COLUMNA02 as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
delete from FITABLE026  where COLUMN05=@internalid AND COLUMN09=@COLUMN04 AND COLUMN04='ADVANCE RECEIPT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'tax Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
if(@COLUMN07=22627)
begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC RECIVED Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
    isnull( cast(@COLUMN04 as nvarchar(250)),'')+',' + 'ADVANCE RECEIPT'+',' +  isnull( cast(@COLUMNA02 as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='ADVANCE RECEIPT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'PDC RECIVED Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
else
begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Updated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(cast(@internalid as nvarchar(250)),'') +','+ isnull(cast(@COLUMN04 as nvarchar(250)),'')+','+   isnull( cast(@COLUMNA02 as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
delete from PUTABLE019   WHERE COLUMN08=@internalid AND COLUMN05=@COLUMN04  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
delete from FITABLE052   WHERE COLUMN09=@internalid AND COLUMN05=@COLUMN04  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register Updated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Balance Amount'+ isnull(cast((@balance-@Prebalance) as nvarchar(250)),'') +','+  '2.Account'+','+ isnull(cast( @COLUMN09  as nvarchar(250)),'')+','+'3.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@Prebalance as decimal(18,2))) where column02=@COLUMN09 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))

end
  set @Tax= (@COLUMN12)
  set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
  set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
		--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TaxAG in(23585,23586,23584,23583,23582))
		--BEGIN
		--	set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
		--	set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
		--	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN17,  COLUMNA02, COLUMNA03)
		--	values(@Vid,@COLUMN05,'RECEIVE PAYMENT',@internalid,@COLUMN08,@COLUMN11,1143,@COLUMN04,@COLUMN24,@COLUMN10,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03)
		
		--	--set @Vid=((select isnull(max(column02),999) from FITABLE034)+1)
		--	--set @TaxColumn17=(select column04 from fitable001 where column02 = 1143   and COLUMNA03=@COLUMNA03)
		--	--Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,  COLUMN10,  COLUMNA02, COLUMNA03)
		--	--				values(@Vid,'RECEIVE PAYMENT',@COLUMN05,@COLUMN11,@COLUMN08,@COLUMN24,@COLUMN04,1143,@COLUMNA02, @COLUMNA03)
		--END
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
set @Prebalance= (select COLUMN10 from FITABLE023 where COLUMN02=@COLUMN02)
set @internalid= (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN02)
set @COLUMN04= (select COLUMN04 from FITABLE023 where COLUMN02=@COLUMN02)
set @COLUMN09= (select COLUMN09 from FITABLE023 where COLUMN02=@COLUMN02)
set @COLUMNA02= (select COLUMNA02 from FITABLE023 where COLUMN02=@COLUMN02)
set @COLUMNA03= (select COLUMNA03 from FITABLE023 where COLUMN02=@COLUMN02)
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN09 and COLUMNA03=@COLUMNA03)

delete from fitable064 where column03 = 'ADVANCE RECEIPT' and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
declare @PDC nvarchar(250)
set @PDC=(select COLUMN07 from FITABLE023 where COLUMN02=@COLUMN02)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received record Deletion  in FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@COLUMNA13 as nvarchar(250)),'') +','+ ISNULL(cast(@COLUMN02 as nvarchar(250)),'') +  '  '))
UPDATE FITABLE023 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received record Deletion  in FITABLE023 Table '+'' + CHAR(13)+CHAR(10) + ''))

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Received record Deletion  in FITABLE024 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@COLUMNA13 as nvarchar(250)),'') +','+ ISNULL(cast(@internalid as nvarchar(250)),'') +  '  '))
UPDATE FITABLE024 SET COLUMNA13=@COLUMNA13 WHERE COLUMN09 in( @internalid)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received record Deletion  in FITABLE024 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advace Received record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@internalid as nvarchar(250)),'') +','+isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+'RECEIVE PAYMENT' +','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
delete from FITABLE026  where COLUMN05=@internalid AND COLUMN09=@COLUMN04 AND COLUMN04='ADVANCE RECEIPT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advace Received record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advace Received record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+'ADVANCE RECEIPT' +','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
   --EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
--delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='ADVANCE RECEIPT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
delete from FITABLE026 where COLUMN09=@COLUMN04 and COLUMN04='ADVANCE RECEIPT' and COLUMN05=@internalid and COLUMN08=1119 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advace Received record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
if(@PDC=22627)
begin
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC Received record Deletion  in FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+'ADVANCE RECEIPT' +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+ ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='ADVANCE RECEIPT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'PDC Received record Deletion  in FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
--else
--begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register record Deletion  in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@internalid as nvarchar(250)),'') +','+isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
delete from PUTABLE019   WHERE COLUMN08=@internalid AND COLUMN05=@COLUMN04  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
delete from FITABLE052   WHERE COLUMN09=@internalid AND COLUMN05=@COLUMN04  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register record Deletion  in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
  'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
 'The Values are '+ isnull(cast((@balance-@Prebalance) as nvarchar(250)),'') +','+isnull(cast(@COLUMN09 as nvarchar(250)),'') +','+ isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@Prebalance as decimal(18,2))) where column02=@COLUMN09 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
--delete from FITABLE042  where COLUMN09=@internalid AND COLUMN10=@COLUMN04 AND COLUMN06='ADVANCE RECEIVE' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03

--end
--delete from PUTABLE018  where COLUMN05=@COLUMN04 AND  COLUMN06='RECEIVE PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END
exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE023.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exec_TP_FITABLE023.txt',0
end catch
end







GO
