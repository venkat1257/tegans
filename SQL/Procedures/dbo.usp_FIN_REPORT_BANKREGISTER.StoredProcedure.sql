USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FIN_REPORT_BANKREGISTER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_FIN_REPORT_BANKREGISTER]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnitStatus      int= null,
@OperatingUnit nvarchar(250)=null,
@Bank        nvarchar(250)= null,
@Project        nvarchar(250)= null,
@DateF nvarchar(250)=null
)
AS
BEGIN
declare @whereStr nvarchar(max)
if @OperatingUnit!=''
begin
set @whereStr=' and p.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Bank!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and  p.[COLUMN03 ] in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Bank+''') s)'
end
else
begin
set @whereStr=@whereStr+' and  p.[COLUMN03 ] in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Bank+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
 end
else
begin
 set @whereStr=@whereStr
end
exec(';With MyTable AS
(
	(SELECT NULL COLUMN04,NULL OperatingUnit, NULL   BankRegisterID,s.column04 Bank,NULL Date,NULL Dtt,
		NULL BillInvoice,NULL Type,NULL Account,NULL Cheque,''Previous Amount'' Memo,NULL PaymentAmount,
		NULL DepositAmount,isnull(sum(p.column11),0)-isnull(sum(p.column10),0) AS BalanceAmount,
		NULL Partytype,
		NULL name,0 ab,-1 RNO From	PUTABLE019 p 
		left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 AND S.COLUMNA03=P.COLUMNA03 where  p.COLUMN04 <'''+@FromDate+''' AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p.COLUMNA03='''+@AcOwner+''' AND ISNULL(P.COLUMNA13,0)=0 '+ @whereStr +' group by s.column04)
union all
SELECT p.COLUMN04 COLUMN04,(select COLUMN03 from CONTABLE007 where COLUMN02=p.COLUMNA02)OperatingUnit,p.[COLUMN02 ] AS BankRegisterID,s.[COLUMN04 ] AS Bank,FORMAT(p.COLUMN04,'''+@DateF+''') AS Date,p.COLUMN04 Dtt,
		p.[COLUMN05 ] AS BillInvoice,p.COLUMN06 AS Type,(case when p.COLUMN06=''PAYMENT VOUCHER'' then pv.[COLUMN04 ] when p.COLUMN06=''RECEIPT VOUCHER'' then (rv1.COLUMN04) end) AS Account,f44.COLUMN20 AS Cheque,(case when p.COLUMN06=''BILL PAYMENT'' then (select COLUMN10 from PUTABLE014 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''ISSUE CHEQUE'' or p.COLUMN06=''ADVANCE PAYMENT'' then (select COLUMN08 from FITABLE020 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03 )
when p.COLUMN06=''PAYMENT VOUCHER'' then (select COLUMN04 from FITABLE022 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''PAYMENT'' then (select COLUMN10 from SATABLE011 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''JOBBER PAYMENT'' then (select COLUMN10 from SATABLE011 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''RECEIVE PAYMENT'' or p.COLUMN06=''ADVANCE RECEIPT'' then (select COLUMN11 from FITABLE023 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''RECEIPT VOUCHER'' then (select COLUMN11 from FITABLE023 f where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''EXPENCE'' or p.COLUMN06=''EXPENSE'' then (select COLUMN13 from FITABLE045 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03) 
when p.COLUMN06=''COMMISSION PAYMENT'' then (select COLUMN10 from PUTABLE014 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''JOURNAL ENTRY'' then p.COLUMN09		
else p.COLUMN09 end) as Memo,p.[COLUMN10 ] AS PaymentAmount,
		p.[COLUMN11 ] AS DepositAmount,((isnull(p.COLUMN11,0))  
	-(isnull(p.COLUMN10,0)) ) AS BalanceAmount,
		(case when p.COLUMN06=''BILL PAYMENT'' or p.COLUMN06=''ISSUE CHEQUE''   then ''VENDOR'' when p.COLUMN06=''ADVANCE PAYMENT'' or p.COLUMN06=''PAYMENT VOUCHER'' then (case when pv1.COLUMN11=22305 then ''VENDOR'' when pv1.COLUMN11=22334 then ''JOBBER'' when pv1.COLUMN11=22335 then ''CUSTOMER'' when pv1.COLUMN11=22492 then ''EMPLOYEE'' when pv1.COLUMN11=22700 then ''OTHERS'' else '''' end) when p.COLUMN06=''PAYMENT'' or p.COLUMN06=''RECEIVE PAYMENT'' or p.COLUMN06=''ADVANCE RECEIPT'' or p.COLUMN06=''RECEIPT VOUCHER'' then ''CUSTOMER'' when p.COLUMN06=''EXPENCE'' or p.COLUMN06=''EXPENSE'' then ''EMPLOYEE'' when p.COLUMN06=''COMMISSION PAYMENT''  then ''SALES REP'' WHEN p.COLUMN06=''JOURNAL ENTRY'' then (case when  j.column07=''22305'' or fj.column14=''22305'' then ''VENDOR'' when  j.column07=''22335'' or fj.column14=''22335'' then ''CUSTOMER'' when  j.column07=''22334'' or fj.column14=''22334'' then ''JOBBER'' when j.column07=''22306'' or fj.column14=''22306'' then ''SALES REP'' when j.column07=''22492'' or fj.column14=''22492'' then ''EMPLOYEE'' else '''' end)	else NULL end) Partytype,
		(case when p.COLUMN06=''BILL PAYMENT'' or p.COLUMN06=''ISSUE CHEQUE'' or p.COLUMN06=''ADVANCE PAYMENT''  or p.COLUMN06=''JOBBER PAYMENT'' then (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN07) when p.COLUMN06=''PAYMENT VOUCHER'' or p.COLUMN06=''ADVANCE PAYMENT'' then (case when pv1.COLUMN11=22305 then a.COLUMN05 when pv1.COLUMN11=22334 then a.COLUMN05 when pv1.COLUMN11=22335 then b.COLUMN05 when pv1.COLUMN11=22492 then m.COLUMN09 when pv1.COLUMN11=22700 then a.column05 else '''' end) when p.COLUMN06=''PAYMENT'' or p.COLUMN06=''RECEIVE PAYMENT'' or p.COLUMN06=''ADVANCE RECEIPT'' or p.COLUMN06=''RECEIPT VOUCHER'' then (select COLUMN05 from SATABLE002 where COLUMN02=p.COLUMN07) when p.COLUMN06=''EXPENCE'' or p.COLUMN06=''EXPENSE'' then (select COLUMN09 from MATABLE010 where COLUMN02=p.COLUMN07) when p.COLUMN06=''COMMISSION PAYMENT'' then (select COLUMN09 from MATABLE010 where COLUMN02=p.COLUMN07 and COLUMN30=''True'') WHEN p.COLUMN06=''JOURNAL ENTRY'' then (case when  j.column07=''22305'' or fj.column14=''22305'' then a.COLUMN05 when  j.column07=''22335'' or fj.column14=''22335'' then b.COLUMN05 when  j.column07=''22334'' or fj.column14=''22334'' then a.COLUMN05 when j.column07=''22306'' or fj.column14=''22306'' then m.COLUMN09 when j.column07=''22492'' or fj.column14=''22492'' then m.COLUMN09 else '''' end)
 else NULL end) name,max(p.COLUMN02) ab,row_number() over(order by p.COLUMN04) RNO From	PUTABLE019 p 
		left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03  AND S.COLUMNA03=P.COLUMNA03  
		left outer join FITABLE044 f44 on f44.COLUMN02=p.COLUMN14  AND f44.COLUMNA03=p.COLUMNA03 and isnull(f44.COLUMNA13,0)=0
		left outer join FITABLE031 j on j.COLUMN01=p.COLUMN08 and j.COLUMN04=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=p.COLUMN03 
left outer join SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
left outer join SATABLE002 b on b.COLUMN02=p.COLUMN07  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
left outer join MATABLE010 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 
left outer join FITABLE022 f22 on f22.COLUMN01=p.COLUMN08 and f22.COLUMNA03=p.COLUMNA03 and f22.COLUMNA02=p.COLUMNA02 and isnull(f22.COLUMNA13,0)=0
		left outer join FITABLE001  pv on pv.COLUMN02=f22.COLUMN03 and pv.COLUMNA03=p.COLUMNA03 
		left outer join FITABLE024 f24 on f24.COLUMN01=p.COLUMN08 and f24.COLUMNA03=p.COLUMNA03  and f24.COLUMNA02=p.COLUMNA02 
		and isnull(f24.COLUMNA13,0)=0
		left outer join FITABLE001  rv1 on rv1.COLUMN02=f24.COLUMN03 and rv1.COLUMNA03=f24.COLUMNA03
		left outer join FITABLE020 pv1 on pv1.COLUMN01=(case when p.COLUMN06=''PAYMENT VOUCHER'' then f22.COLUMN09 else p.COLUMN08 end) and pv1.COLUMNA03=p.COLUMNA03 and isnull(pv1.COLUMNA13,0)=0
		left outer join FITABLE023 rv on rv.COLUMN01=f24.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and isnull(rv.COLUMNA13,0)=0
		where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 
		between '''+@FromDate+''' and '''+@ToDate+''' AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p.COLUMNA03='''+@AcOwner+''' '+@whereStr+'
		group by s.COLUMN04,p.COLUMN02 ,p.COLUMN04,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN09,f44.COLUMN20,p.COLUMN10,p.COLUMN11,p.COLUMN19,p.COLUMNA02,p.COLUMNA03,p.COLUMN08,a.COLUMN05,b.COLUMN05,m.COLUMN09,j.COLUMN07,fj.COLUMN14,rv1.COLUMN04,pv.COLUMN04,pv1.COLUMN11,pv.COLUMN11
	)
Select column04,OperatingUnit,BankRegisterID,Bank,[Date],Dtt,BillInvoice,[Type],Account,Cheque,Memo,PaymentAmount,DepositAmount,
(SUM(isnull(BalanceAmount,0)) OVER(PARTITION BY Bank ORDER BY Dtt   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	) AS BalanceAmount,Partytype,Name,ab
From MyTable  order by RNO desc')
END




























GO
