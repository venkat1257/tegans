USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[Backupfilepath]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[Backupfilepath]
(@FilePath varchar(550)=NULL,@Direction varchar(250))
AS
BEGIN
if(@Direction = 'insert')
Begin
Declare @Count int = (select COUNT( COLUMN02 )from backup01)
if(isnull(@Count,0) > 0)
update backup01 set COLUMN02=@FilePath
else
insert into backup01 (COLUMN02) values (@FilePath)
End
Else
Begin
select COLUMN02 from backup01
End

END
GO
