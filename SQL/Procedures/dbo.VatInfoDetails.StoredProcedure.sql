USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[VatInfoDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[VatInfoDetails](@id nvarchar(250)=null,@type nvarchar(250)=null,@frmdate nvarchar(250)=null,@todate nvarchar(250)=null,@opunit nvarchar(250)=null,@acowner nvarchar(250)=null,@DateF nvarchar(250)=null)
as
begin
if(@type='INPUT VAT from Purchases in the month')
begin
select *into #VatInfoDetails from(
select FORMAT( cast(a.COLUMN08 as date), @DateF) date,a.COLUMN08 Dtt,a.column04 billno,v.column05 vendor,b.column12 subtotal,b.column20 tax,cast(isnull(b.column12,0) as decimal(18,2))+cast(isnull(b.column20,0) as decimal(18,2)) total  ,a.COLUMN03 fid from putable006 b
inner join  putable005 a on b.column13=a.column01
left join SATABLE001 v on v.column02=a.column05
left join MATABLE013 t on t.column02=b.column18 and t.COLUMN16=22399
 where b.column18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column08 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column12,0)!=0 and  isnull(b.column20,0)!=0
union all
select FORMAT( cast(a.COLUMN08 as date), @DateF) date,a.COLUMN08 Dtt,a.column04 billno,v.column05 vendor,b.column12 subtotal,
cast((isnull(b.column12,0)-(iif(ISNULL(b.COLUMN31,0)=22556,((isnull(b.column12,0)*(isnull(b.column32,0)/100))),isnull(b.column32,0) )))*(isnull(t.column17,0)*(0.01)) as decimal(18,2)) tax,
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
cast(isnull(b.column12,0) as decimal(18,2))+cast((isnull(b.column12,0)-(iif(ISNULL(b.COLUMN31,0)=22556,((isnull(b.column12,0)*(isnull(b.column32,0)/100))),isnull(b.column32,0) )))*(isnull(t.column17,0)*(0.01)) as decimal(18,2)) total  ,a.COLUMN03 fid from putable006 b
inner join  putable005 a on b.column13=a.column01 and isnull(a.COLUMN23,1000)!=1000
left join SATABLE001 v on v.column02=a.column05
left join MATABLE013 t on t.column02=a.column23 and t.COLUMN16=22399
 where a.column23 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column08 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column12,0)!=0
union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN50='22335' then v2.column05 else v.column05 end) vendor,-(b.column24) subtotal,-(cast(isnull(b.column11,0) as decimal(18,2))-cast(isnull(b.column24,0) as decimal(18,2))) tax,-(b.column11) total  ,a.COLUMN03 fid from putable002 b
inner join  putable001 a on b.column19=a.column01 and a.column03 in(1355)
left join SATABLE001 v on v.column02=a.column05
left join SATABLE002 v2 on v2.column02=a.column05
left join MATABLE013 t on t.column02=b.column25 and t.COLUMN16=22399
 where b.column25 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column24,0)!=0 and  isnull(b.column11,0)!=0 and isnull(a.COLUMN50,0)!='22335'
union all
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN50='22335' then v2.column05 else v.column05 end) vendor,-(b.column24) subtotal,
-(cast((isnull(b.column24,0))*(isnull(t.column17,0)*(0.01)) as decimal(18,2))) tax,
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
-(cast(isnull(b.column24,0) as decimal(18,2))+cast((isnull(b.column24,0))*(isnull(t.column17,0)*(0.01)) as decimal(18,2))) total  ,a.COLUMN03 fid from putable002 b
inner join  putable001 a on b.column19=a.column01 and a.column03 in(1355) and isnull(a.COLUMN31,1000)!=1000
left join SATABLE001 v on v.column02=a.column05
left join SATABLE002 v2 on v2.column02=a.column05
left join MATABLE013 t on t.column02=a.column31 and t.COLUMN16=22399
 where a.column31 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column24,0)!=0 and  isnull(b.column11,0)!=0 and isnull(a.COLUMN50,0)!='22335'

 union all
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN51='22305' then v1.column05 else v.column05 end) vendor,-(b.column25) subtotal,-(cast(isnull(b.column11,0) as decimal(18,2))-cast(isnull(b.column25,0) as decimal(18,2))) tax,-(b.column11) total  ,a.COLUMN03 fid  from SATABLE006 b
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
inner join  SATABLE005 a on b.column19=a.column01 and a.column03 in(1330)
left join SATABLE002 v on v.column02=a.column05
left join SATABLE001 v1 on v1.column02=a.column05
left join MATABLE013 t on t.column02=b.column26 and t.COLUMN16=22399
 where b.column26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column25,0)!=0 and  isnull(b.column11,0)!=0 and a.COLUMN51='22305'
union all
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN51='22305' then v1.column05 else v.column05 end) vendor,-(b.column25) subtotal,
-(cast(isnull(b.column25,0) as decimal(18,2))*cast((cast(isnull(t.column07,0) as decimal(18,2))*(0.01)) as decimal(18,2))) tax,
-(cast(isnull(b.column25,0) as decimal(18,2))+cast(isnull(b.column25,0) as decimal(18,2))*cast((isnull(t.column07,0)*(0.01)) as decimal(18,2))) total  ,a.COLUMN03 fid from SATABLE006 b
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
inner join  SATABLE005 a on b.column19=a.column01 and a.column03 in(1330) and isnull(a.COLUMN31,1000)!=1000 and isnull(a.COLUMNa13,0)=0
left join SATABLE002 v on v.column02=a.column05
left join SATABLE001 v1 on v1.column02=a.column05
left join MATABLE013 t on t.column02=a.column31 and t.COLUMN16=22399
 where a.column31 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column25,0)!=0 and  isnull(b.column11,0)!=0 and a.COLUMN51='22305'
 union all
select FORMAT( cast(a.COLUMN03 as date), @DateF) date,a.COLUMN03 Dtt,a.column09 billno,NULL vendor,0 subtotal,iif(isnull(column11,0)>0,column11,iif(isnull(column12,0)>0,column12,0)) tax,iif(isnull(column11,0)>0,column11,iif(isnull(column12,0)>0,column12,0)) total  ,'1414' fid from fitable026 a
 where a.column08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column03 between @frmdate and @todate  and a.columna02=@opunit and  a.columna03=@acowner and  isnull(a.columna13,0)=0 and  isnull(a.column16,0)!=0 and  isnull(a.column10,0)!=0 and a.COLUMN17 like'input%' and column04='JOURNAL ENTRY'
 )Query
declare @Q1 nvarchar(max)
set @Q1='select *from #VatInfoDetails order by Dtt desc,billno desc'
exec(@Q1)
end
else
begin
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
select *into #VatinfoDetails1 from(
select FORMAT( cast(a.COLUMN08 as date), @DateF) date,a.COLUMN08 Dtt,a.column04 billno,v.column05 vendor,b.column14 subtotal,b.column23 tax,cast(isnull(b.column14,0) as decimal(18,2))+cast(isnull(b.column23,0) as decimal(18,2)) total  ,a.COLUMN03 fid  from satable010 b
inner join  satable009 a on b.column15=a.column01 and b.COLUMNA03=a.COLUMNA03 and isnull(a.columna13,0)=0 
left join SATABLE002 v on v.column02=a.column05
left join MATABLE013 t on t.column02=b.column21
 where b.column21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column08 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column14,0)!=0 and  isnull(b.column23,0)!=0
union all
select FORMAT( cast(a.COLUMN08 as date), @DateF) date,a.COLUMN08 Dtt,a.column04 billno,v.column05 vendor,b.column14 subtotal,
cast(((isnull(b.column14,0)-(iif(ISNULL(b.COLUMN30,0)=22556,((isnull(b.column14,0)*(cast(isnull(b.column19,0) as decimal(18,2))/100))),isnull(b.column19,0) )))*(isnull(t.column07,0)*(0.01))) as decimal(18,2)) tax,
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
cast(isnull(b.column14,0) as decimal(18,2))+cast((isnull(b.column14,0)-(iif(ISNULL(b.COLUMN30,0)=22556,((isnull(b.column14,0)*(cast(isnull(b.column19,0) as decimal(18,2))/100))),isnull(b.column19,0) )))*(isnull(t.column07,0)*(0.01)) as decimal(18,2)) total  ,a.COLUMN03 fid  from satable010 b
inner join  satable009 a on b.column15=a.column01 and isnull(a.COLUMN23,1000)!=1000 and b.COLUMNA03=a.COLUMNA03 and isnull(a.columna13,0)=0 
left join SATABLE002 v on v.column02=a.column05
left join MATABLE013 t on t.column02=a.column23
 where a.column23 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column08 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column14,0)!=0
union all
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN51='22305' then v1.column05 else v.column05 end) vendor,-(b.column25) subtotal,-(cast(isnull(b.column11,0) as decimal(18,2))-cast(isnull(b.column25,0) as decimal(18,2))) tax,-(b.column11) total  ,a.COLUMN03 fid  from SATABLE006 b
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
inner join  SATABLE005 a on b.column19=a.column01 and a.column03 in(1330)
left join SATABLE002 v on v.column02=a.column05
left join SATABLE001 v1 on v1.column02=a.column05
left join MATABLE013 t on t.column02=b.column26 and t.COLUMN16=22399
 where b.column26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column25,0)!=0 and  isnull(b.column11,0)!=0 and a.COLUMN51!='22305'
union all
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN51='22305' then v1.column05 else v.column05 end) vendor,-(b.column25) subtotal,
-(cast(isnull(b.column25,0) as decimal(18,2))*cast((cast(isnull(t.column07,0) as decimal(18,2))*(0.01)) as decimal(18,2))) tax,
-(cast(isnull(b.column25,0) as decimal(18,2))+cast(isnull(b.column25,0) as decimal(18,2))*cast((isnull(t.column07,0)*(0.01)) as decimal(18,2))) total  ,a.COLUMN03 fid from SATABLE006 b
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
inner join  SATABLE005 a on b.column19=a.column01 and a.column03 in(1330) and isnull(a.COLUMN31,1000)!=1000 and isnull(a.COLUMNa13,0)=0
left join SATABLE002 v on v.column02=a.column05
left join SATABLE001 v1 on v1.column02=a.column05
left join MATABLE013 t on t.column02=a.column31 and t.COLUMN16=22399
 where a.column31 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column25,0)!=0 and  isnull(b.column11,0)!=0 and a.COLUMN51!='22305'

 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN50='22335' then v2.column05 else v.column05 end) vendor,-(b.column24) subtotal,-(cast(isnull(b.column11,0) as decimal(18,2))-cast(isnull(b.column24,0) as decimal(18,2))) tax,-(b.column11) total  ,a.COLUMN03 fid from putable002 b
inner join  putable001 a on b.column19=a.column01 and a.column03 in(1355)
left join SATABLE001 v on v.column02=a.column05
left join SATABLE002 v2 on v2.column02=a.column05
left join MATABLE013 t on t.column02=b.column25 and t.COLUMN16=22399
 where b.column25 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column24,0)!=0 and  isnull(b.column11,0)!=0 and a.COLUMN50='22335'
union all
select FORMAT( cast(a.COLUMN06 as date), @DateF) date,a.COLUMN06 Dtt,a.column04 billno,(case when a.COLUMN50='22335' then v2.column05 else v.column05 end) vendor,-(b.column24) subtotal,
-(cast((isnull(b.column24,0))*(isnull(t.column17,0)*(0.01)) as decimal(18,2))) tax,
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
-(cast(isnull(b.column24,0) as decimal(18,2))+cast((isnull(b.column24,0))*(isnull(t.column17,0)*(0.01)) as decimal(18,2))) total  ,a.COLUMN03 fid from putable002 b
inner join  putable001 a on b.column19=a.column01 and a.column03 in(1355) and isnull(a.COLUMN31,1000)!=1000
left join SATABLE001 v on v.column02=a.column05
left join SATABLE002 v2 on v2.column02=a.column05
left join MATABLE013 t on t.column02=a.column31 and t.COLUMN16=22399
 where a.column31 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column06 between @frmdate and @todate  and b.columna02=@opunit and  b.columna03=@acowner and  isnull(b.columna13,0)=0 and  isnull(b.column24,0)!=0 and  isnull(b.column11,0)!=0 and a.COLUMN50='22335'
 union all
select FORMAT( cast(a.COLUMN03 as date), @DateF) date,a.COLUMN03 Dtt,a.column09 billno,NULL vendor,0 subtotal,iif(isnull(column11,0)>0,column11,iif(isnull(column12,0)>0,column12,0)) tax,iif(isnull(column11,0)>0,column11,iif(isnull(column12,0)>0,column12,0)) total  ,'1414' fid from fitable026 a
 where a.column08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@id) s) and a.column03 between @frmdate and @todate  and a.columna02=@opunit and  a.columna03=@acowner and  isnull(a.columna13,0)=0 and  isnull(a.column16,0)!=0 and  isnull(a.column10,0)!=0 and a.COLUMN17 like'output%' and column04='JOURNAL ENTRY'
 )Query
 set @Q1='select *from #VatInfoDetails1 order by Dtt desc,billno desc'
exec(@Q1)
end
end



GO
