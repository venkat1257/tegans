USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_EX_TP_PAYMENT_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create  PROCEDURE [dbo].[usp_EX_TP_PAYMENT_HEADER_DATA]
(@ExpenseID nvarchar(250),@AcOwner int=null)
AS
BEGIN
   SELECT  COLUMN02 , COLUMN16,COLUMN08 ,COLUMN25,COLUMN18 ,  COLUMN19 ,COLUMN07, 
   COLUMN05,COLUMN13, COLUMN10
   FROM fitable012 WHERE  COLUMN02=  @ExpenseID  and COLUMNA03=@AcOwner  and isnull(columna13,0)=0
END
GO
