USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_InventoryLOT]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_InventoryLOT]
@Billno nvarchar(250)=null,@Billlineref nvarchar(250)=null,
@Item nvarchar(250)=null,@Quantity nvarchar(250)=null,@OperatingUnit nvarchar(250)=null,
@UOM nvarchar(250)=null,@AccountOwner nvarchar(250)=null,@Direction nvarchar(250),@Location nvarchar(250)=null,@lotno nvarchar(250)=null
AS
BEGIN
declare @Qty_On_Hand decimal(18,2),@Qty_On_Hand1 decimal(18,2),@Qty_Order decimal(18,2),@Qty_Avl decimal(18,2),@Qty_Cmtd decimal(18,2),
@COLUMN04 nvarchar(250)=null,@COLUMN14 nvarchar(250)=null,@COLUMN19 nvarchar(250)=null,@id nvarchar(250)=null,
@COLUMN09 nvarchar(250)=null,@COLUMN11 nvarchar(250)=null,@COLUMNA03 nvarchar(250)=null,@Type nvarchar(250)=null,@Price nvarchar(250)=null,
@Transno nvarchar(250)=null,@Transid nvarchar(250)=null,@POrderno nvarchar(250)=null,@PTransno nvarchar(250)=null,@PTransid nvarchar(250)=null,
@COLUMNA02 nvarchar(250)=null,@PTranQty DECIMAL(18,2)=null,@COLUMN08 DECIMAL(18,2)=null,@COLUMN06 DECIMAL(18,2)=null,@COLUMN13 DECIMAL(18,2)=null,
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
@Qty_Commit DECIMAL(18,2),@Qty_BACK DECIMAL(18,2),@Qty DECIMAL(18,2),@AvgPrice DECIMAL(18,2),@chk bit,@newIID nvarchar(250)=null
set @chk=(select column48 from matable007 where column02=@Item)
set @Location=(isnull(@Location,0))
if(@chk=1)
begin
set @COLUMN04=(@Item)
set @COLUMN14=(@OperatingUnit)

	  DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
      DECLARE curiv CURSOR FOR SELECT COLUMN01 from FITABLE038 where COLUMN05=@Billlineref and COLUMN04=@Billno and COLUMN06=@Item and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner and COLUMNA13=0
      OPEN curiv
	  FETCH NEXT FROM curiv INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE038 where COLUMN05=@Billlineref and COLUMN04=@Billno and COLUMN06=@Item and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @COLUMNA03=(@AccountOwner)
             set @COLUMNA02=(@OperatingUnit)
             set @Type=(SELECT COLUMN03 FROM FITABLE038 WHERE COLUMN01=@id)
             set @UOM=(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Transno=(SELECT COLUMN04 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Transid=(SELECT COLUMN05 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Price=(SELECT COLUMN09 FROM FITABLE038 WHERE COLUMN01=@id)
             set @Quantity=(SELECT COLUMN07 FROM FITABLE038 WHERE COLUMN01=@id)
             set @COLUMN19=(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN01=@id)
             set @COLUMN11=(SELECT COLUMN09 FROM FITABLE038 WHERE COLUMN01=@id)
             set @COLUMN09=(SELECT COLUMN07 FROM FITABLE038 WHERE COLUMN01=@id)
             set @lotno=(SELECT isnull(COLUMN12,0) FROM FITABLE038 WHERE COLUMN01=@id)
		     if(@Type='ITEMISSUE')
			 begin
			 declare @COLUMN15 nvarchar(250),@ItemQty decimal(18,2),@COLUMN12 decimal(18,2)
			 set @COLUMN04=(@Item)
			 set @COLUMN15=(@OperatingUnit)
			 set @COLUMN19=(@UOM)
			 set @uom=(iif(@uom='',10000,isnull(@uom,0)))
			 set @COLUMN09=(@Quantity)
			 set @ItemQty=(@Quantity)
			 set @COLUMN12=(@COLUMN11)
			 set @location=(@Location)
			 set @location=(iif(@location='',0,isnull(@location,0)))
			 set @lotno=(iif(@lotno='',0,isnull(@lotno,0)))
if(@Direction='Insert')
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))- cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
			else
				begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+ cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand+@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				end
				--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
		     else if(@Type='RETURN ISSUE')
			 begin
			 set @COLUMN04=(@Item)
			 set @COLUMN15=(@OperatingUnit)
			 set @COLUMN19=(@UOM)
			 set @uom=(iif(@uom='',10000,isnull(@uom,0)))
			 set @COLUMN09=(@Quantity)
			 set @ItemQty=(@Quantity)
			 set @COLUMN12=(@COLUMN11)
			 set @location=(@Location)
			 set @location=(iif(@location='',0,isnull(@location,0)))
			 set @lotno=(iif(@lotno='',0,isnull(@lotno,0)))
if(@Direction='Insert')
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))- cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
			else
				begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+ cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand+@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				end
		     else if(@Type='RETURN RECEIPT')
			 begin
			 set @COLUMN04=(@Item)
			 set @COLUMN15=(@OperatingUnit)
			 set @COLUMN19=(@UOM)
			 set @uom=(iif(@uom='',10000,isnull(@uom,0)))
			 set @COLUMN09=(@Quantity)
			 set @ItemQty=(@Quantity)
			 set @COLUMN12=(@COLUMN11)
			 set @location=(@Location)
			 set @location=(iif(@location='',0,isnull(@location,0)))
			 set @lotno=(iif(@lotno='',0,isnull(@lotno,0)))
if(@Direction='Insert')
begin
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
if(@AvgPrice=0)set @AvgPrice=(@Price)
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+ cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand+@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				else
				begin	
				--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				set @newIID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1			
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19, COLUMN13,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22
)
values
(
  @newIID,@COLUMN04,@COLUMN09,@COLUMN09, (cast(isnull(@COLUMN09,0) as decimal(18,2))*cast(isnull(@Price,0) as decimal(18,2))),@Price,@COLUMN19,@COLUMN15,@COLUMN15,@COLUMNA03 ,@location,@lotno
)
				end
				end
			else
				begin
if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
if(@AvgPrice=0)set @AvgPrice=(@Price)
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))- cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				else
				begin	
				set @newIID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1			
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19, COLUMN13,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22
)
values
(
  @newIID,@COLUMN04,@COLUMN09,@COLUMN09, (cast(isnull(@COLUMN09,0) as decimal(18,2))*cast(isnull(@Price,0) as decimal(18,2))),@Price,@COLUMN19,@COLUMN15,@COLUMN15,@COLUMNA03 ,@location,@lotno
)
				end
				end
				end
		     else if(@Type='ITEM RECEIPT')
			 begin
			 set @COLUMN04=(@Item)
			 set @COLUMN15=(@OperatingUnit)
			 set @COLUMN19=(@UOM)
			 set @uom=(iif(@uom='',10000,isnull(@uom,0)))
			 set @COLUMN09=(@Quantity)
			 set @ItemQty=(@Quantity)
			 set @COLUMN12=(@COLUMN11)
			 set @location=(@Location)
			 set @location=(iif(@location='',0,isnull(@location,0)))
			 set @lotno=(iif(@lotno='',0,isnull(@lotno,0)))
if(@Direction='Insert')
begin
if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
if(@AvgPrice=0)set @AvgPrice=(@Price)
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+ cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand+@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))+((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				else
				begin	
				set @newIID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1			
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19, COLUMN13,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22
)
values
(
  @newIID,@COLUMN04,@COLUMN09,@COLUMN09, (cast(isnull(@COLUMN09,0) as decimal(18,2))*cast(isnull(@Price,0) as decimal(18,2))),@Price,@COLUMN19,@COLUMN15,@COLUMN15,@COLUMNA03 ,@location,@lotno
)
				end
				end
			else
				begin
if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
if(@AvgPrice=0)set @AvgPrice=(@Price)
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))- cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)))
set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				else
				begin	
				set @newIID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1			
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19, COLUMN13,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22
)
values
(
  @newIID,@COLUMN04,@COLUMN09,@COLUMN09, (cast(isnull(@COLUMN09,0) as decimal(18,2))*cast(isnull(@Price,0) as decimal(18,2))),@Price,@COLUMN19,@COLUMN15,@COLUMN15,@COLUMNA03 ,@location,@lotno
)
				end
				end
				end
			 FETCH NEXT FROM curiv INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
        CLOSE curiv 
        deallocate curiv 
END
END


GO
