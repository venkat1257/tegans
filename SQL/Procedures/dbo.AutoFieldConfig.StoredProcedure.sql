USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[AutoFieldConfig]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AutoFieldConfig]
(
--EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
--EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
--EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
@chkval  nvarchar(250)= null,@UOM  nvarchar(250)= null,@Location  nvarchar(250)= null,@Weight  nvarchar(250)= null,@Discount  nvarchar(250)= null,@Lot  nvarchar(250)= null,@AcOwner  nvarchar(250)= null,@OPUnit  nvarchar(250)= null)
as 
begin
  DECLARE @i int,@tblidId int,@formId int,@RowId int,@colname nvarchar(250),@fieldname nvarchar(250),
  @RedisCache nvarchar(250)=''
  SELECT @RedisCache=COLUMN04 from CONTABLE031 where COLUMNA03=@AcOwner and  (isnull(COLUMNA02,0)=@OPUnit or COLUMNA02 is null) and  COLUMN03='Allow Redies Cache' and isnull(column04,0)=1 and isnull(columna13,0)=0
  --EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
  DECLARE @enable nvarchar(250)
  SET @i = 1000
  WHILE (@i <= (SELECT MAX(CAST(Column02 AS INT)) FROM SETABLE011))
    BEGIN
    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	 if exists(SELECT Column02 FROM SETABLE011 WHERE Column02 = @i)
	 begin
        SET @tblidId = (SELECT Column04 FROM SETABLE011 WHERE Column02 = @i)
        SET @formId = (SELECT Column06 FROM SETABLE011 WHERE Column02 = @i)
        SET @colname = (SELECT Column05 FROM SETABLE011 WHERE Column02 = @i)
		SET @RowId = (select Column02 from CONTABLE006 where COLUMN03=@formId and column04=@tblidId and column05=@colname and column07='Y')
		SET @fieldname = (select COLUMN07 from SETABLE011 where COLUMN02=@i)
		--EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
	    set @enable=('N')
		if( @fieldname='Project')
      begin
      if(@chkval is not null and @chkval!='')
      begin
	  set @enable=('Y')
      END
	  if(@RedisCache='1')
	  begin
	  EXEC usp_Proc_RedisCacheStatus '1378','110010870',@OPUnit,@AcOwner,'','Update'
	  end
	  end
	  --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	  else if( @fieldname='UOM')
	  begin
	  if( @UOM is not null and @UOM!='')
	  --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
      begin
	  set @enable=('Y')
      END
	  if(@RedisCache='1')
	  begin
	  EXEC usp_Proc_RedisCacheStatus '1290','11119',@OPUnit,@AcOwner,'','Update'
	  end
	  end
	  else if( @fieldname='Location')
	  begin
	  if( @Location is not null and @Location!='')
      begin
	  set @enable=('Y')
      END
	  if(@RedisCache='1')
	  begin
	  EXEC usp_Proc_RedisCacheStatus '1501','110014036',@OPUnit,@AcOwner,'','Update'
	  end
	  end
	  --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
	  else if( @fieldname='Weight')
	  begin
	  if( @Weight is not null and @Weight!='')
      begin
	  set @enable=('Y')
      END
	  if(@RedisCache='1')
	  begin
	  EXEC usp_Proc_RedisCacheStatus '1261','110008817',@OPUnit,@AcOwner,'','Update'
	  end
	  end
	  --EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
	  else if( @fieldname='Discount')
	  begin
	  if( @Discount is not null and @Discount!='')
      begin
	  set @enable=('Y')
      END
	  if(@RedisCache='1')
	  begin
	  EXEC usp_Proc_RedisCacheStatus '1261','110008817',@OPUnit,@AcOwner,'','Update'
	  end
	  end
	  --EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
	  else if( @fieldname='Lot')
	  begin
	  if( @Lot is not null and @Lot!='')
      begin
	  set @enable=('Y')
      END
	  if(@RedisCache='1')
	  begin
	  EXEC usp_Proc_RedisCacheStatus '1515','110014041',@OPUnit,@AcOwner,'','Update'
	  end
      END
	  if exists(SELECT Column04 FROM SETABLE012 WHERE COLUMN04 = @tblidId and COLUMN05 = @colname and COLUMN06 = @formId and COLUMNA03 = @AcOwner )
	  begin
	  update SETABLE012 set COLUMN07=@enable  WHERE COLUMN04 = @tblidId and COLUMN05 = @colname and COLUMN06 = @formId and COLUMNA03 = @AcOwner 
	  end
	  else
	  begin
	  delete FROM SETABLE012 WHERE COLUMN04 = @tblidId and COLUMN05 = @colname and COLUMN06 = @formId and COLUMNA03 = @AcOwner 
      insert into SETABLE012( COLUMN02 ,COLUMN04 ,COLUMN05,COLUMN06,COLUMN07,COLUMNA03) values ((SELECT isnull(MAX(CAST(Column02 AS INT)),999) FROM SETABLE012)+1, @tblidId,@colname, @formId,@enable, @AcOwner) 
	  end

 --   if( @fieldname='Project')
 --   begin
 --   if( @chkval is not null and @chkval!='')
 --   begin
	--   update CONTABLE006 set COLUMN07='Y' where COLUMN02=@RowId
 --   END
	--else
 --   begin
	--   update CONTABLE006 set COLUMN07='N' where COLUMN02=@RowId
 --   END
	--end
	--else
 --   begin
 --   if( @UOM is not null and @UOM!='')
 --   begin
	--   update CONTABLE006 set COLUMN07='Y' where COLUMN02=@RowId
 --   END
	--else
 --   begin
	--   update CONTABLE006 set COLUMN07='N' where COLUMN02=@RowId
 --   END
	--end
	end
        SET @i = @i + 1
	end
end







GO
