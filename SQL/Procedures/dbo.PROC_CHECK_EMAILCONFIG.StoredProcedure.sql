USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PROC_CHECK_EMAILCONFIG]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PROC_CHECK_EMAILCONFIG]
(@EMPID INT =NULL,@ACOWNER INT=NULL)
AS 
BEGIN
IF EXISTS(SELECT COLUMN04,COLUMN05,
	CASE WHEN EXISTS(SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
	THEN (SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
	ELSE '' END
 COMPANY 
FROM SETABLE013 WHERE COLUMN06=@EMPID AND COLUMNA03=@ACOWNER and isnull(COLUMNA13,0)=0)
	BEGIN
		SELECT COLUMN04,COLUMN05 ,
	CASE WHEN EXISTS(SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
	THEN (SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
	ELSE '' END
	COMPANY 
		FROM SETABLE013 WHERE COLUMN06=@EMPID AND COLUMNA03=@ACOWNER and isnull(COLUMNA13,0)=0
	END
ELSE
	BEGIN
		IF EXISTS(SELECT COLUMN04,COLUMN05,
	CASE WHEN EXISTS(SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
	THEN (SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
	ELSE '' END
	COMPANY   
		FROM SETABLE013 WHERE COLUMN07=(SELECT MAX(COLUMN02) FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0) AND COLUMNA03=@ACOWNER and isnull(COLUMNA13,0)=0)
			BEGIN
				SELECT COLUMN04,COLUMN05,
				CASE WHEN EXISTS(SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
				THEN (SELECT top(1) ISNULL(COLUMN03,'') FROM CONTABLE008 WHERE COLUMNA03=@ACOWNER and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0 order by column02 desc)
				ELSE '' END
				COMPANY  
				FROM SETABLE013 WHERE COLUMN06=@EMPID AND COLUMNA03=@ACOWNER and isnull(COLUMNA13,0)=0
			END
		ELSE
			BEGIN
				SELECT 0 COLUMN04,0 COLUMN05,'' COMPANY
			END
		END
END
GO
