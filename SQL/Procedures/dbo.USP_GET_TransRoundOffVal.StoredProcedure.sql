USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_TransRoundOffVal]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_GET_TransRoundOffVal](@TransId nvarchar(250)=null,@FormId nvarchar(250)=null,@AcOwner nvarchar(250)=null,@OPUNIT nvarchar(250)=null)
AS
BEGIN
if(@FormId=1277)
begin
SELECT COLUMN68 'RoundOff' FROM SATABLE009 WHERE COLUMN02=@TransId and COLUMN03 in(@FormId,1532,1604) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
end
ELSE if(@FormId=1273)
begin
SELECT COLUMN59 'RoundOff' FROM PUTABLE005 WHERE COLUMN02=@TransId and COLUMN03=@FormId and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
ELSE if(@FormId=1275)
begin
SELECT COLUMN71 'RoundOff' FROM SATABLE005 WHERE COLUMN02=@TransId and COLUMN03=@FormId and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
ELSE if(@FormId=1251)
begin
SELECT COLUMN74 'RoundOff' FROM PUTABLE001 WHERE COLUMN02=@TransId and COLUMN03=@FormId and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
ELSE if(@FormId=1330)
begin
SELECT COLUMN71 'RoundOff' FROM SATABLE005 WHERE COLUMN02=@TransId and COLUMN03 in(@FormId,1603) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
ELSE if(@FormId=1355)
begin
SELECT COLUMN74 'RoundOff' FROM PUTABLE001 WHERE COLUMN02=@TransId and COLUMN03=@FormId and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
END






GO
