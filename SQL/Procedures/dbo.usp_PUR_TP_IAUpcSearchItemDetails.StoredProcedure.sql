USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_IAUpcSearchItemDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_IAUpcSearchItemDetails]
(
	@ItemID nvarchar(250)=NULL,
	@uom nvarchar(250)=NULL,
	--EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
	@OPUnit nvarchar(250)=NULL,
	@AcOwner nvarchar(250)=NULL,@location nvarchar(250)=NULL,@lotno nvarchar(250)=NULL
)

AS
BEGIN    
set @location=(iif(@location is null or @location='',0,@location ))
set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
declare @StockItem nvarchar(250)=NULL,@uomsetup nvarchar(250)=NULL
    set @uomsetup=(@ItemID)
	IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		end
	select MA07.COLUMN02,(case  when (MA07.COLUMN06='' or MA07.COLUMN06 is null) then ia.COLUMN04 else MA07.COLUMN06 end) as COLUMN06,MA07.COLUMN50 COLUMN09,isnull((FI10.COLUMN17),(MATABLE024.COLUMN04)) COLUMN17,
	isnull(( FI10.COLUMN12),0) COLUMN12,
	isnull(( FI10.COLUMN08),0) COLUMN08,MA07.COLUMN63 , ia.COLUMN07 qty , (case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) uom 
	,MA07.COLUMN04 iName,@lotno LOT from
    MATABLE007 MA07	left join MATABLE021 ia on ia.COLUMN04=@uomsetup and ia.COLUMNA03=MA07.COLUMNA03 and ia.COLUMNA13=0 left outer join 
	FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02   and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location  and isnull(FI10.COLUMN22,0)=@lotno and FI10.COLUMN13=@OPUnit
	left join MATABLE024 MATABLE024 on  MATABLE024.COLUMNA02=@OPUnit and MATABLE024.COLUMNA03=@AcOwner and MATABLE024.COLUMN07=MA07.COLUMN02 and MATABLE024.COLUMN06='purchase'
	WHERE    isnull(MA07.COLUMN47,'False')='False' and  MA07.COLUMNA13=0 
	 and ( MA07.COLUMN02=@ItemID or MA07.COLUMN06=@ItemID) 
	  and  MA07.COLUMNA03=@AcOwner
	 --IF NOT EXISTS(SELECT * FROM FITABLE039 MA07 inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 WHERE (MA07.COLUMN05=@ItemID  OR m10.COLUMN06=@ItemID) and MA07.COLUMNA03=@AcOwner and (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and MA07.COLUMNA13=0 )
		--BEGIN
		-- end
	 -- else
	 -- begin
	 -- select MA07.COLUMN05 COLUMN02,m10.COLUMN06,m10.COLUMN09,MA07.COLUMN09 COLUMN17,m10.COLUMN45,m10.COLUMN29,MA07.COLUMN10 COLUMN51,m10.COLUMN50,
	 --(isnull(FI10.COLUMN08,0)) COLUMN08,m10.COLUMN57,m10.COLUMN63,m10.COLUMN54,m10.COLUMN61,m10.COLUMN64,m10.COLUMN48,m10.COLUMN66,m10.COLUMN65,
	 --(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty  from FITABLE039 MA07
		--	inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05  and  m10.COLUMNA13=0 and  isnull(m10.COLUMN47,'False')='False' and  m10.COLUMNA03=@AcOwner
		--	left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN05 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02=@OPUnit or ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and isnull(ia.COLUMNA13,0)=0 left outer join 
		--	FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN05 and FI10.COLUMN13=@OPUnit and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@lotno
		--	WHERE   isnull(MA07.COLUMNA13,'False')='False' and  (MA07.COLUMN05=@ItemID OR m10.COLUMN06=@ItemID) AND (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and  MA07.COLUMNA03=@AcOwner
		--end
END



GO
