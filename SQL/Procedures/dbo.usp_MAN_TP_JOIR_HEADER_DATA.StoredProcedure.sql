USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAN_TP_JOIR_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAN_TP_JOIR_HEADER_DATA]
(
	@SalesOrderID nvarchar(250),@JOI nvarchar(250)
)

AS
BEGIN
	SELECT a.COLUMN05 as COLUMN05,a.COLUMN11 AS COLUMN09,a.COLUMN10 AS COLUMN12,a.COLUMN09 AS COLUMN13,a.COLUMN16 as COLUMN14,
	--EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
	a.COLUMN24 AS COLUMN15,a.COLUMN25 as COLUMN16,a.COLUMN26 as COLUMN17,a.COLUMN27 as COLUMN18,a.COLUMN29 as COLUMN20,a.COLUMN30,
	b.COLUMN04 as JOI,a.COLUMN02 as JO,a.COLUMN35 COLUMN21 
	,(case when b.COLUMN03 = 1284 then (s1.COLUMN05)  
	when b.COLUMN03 = 1588 then (s1.COLUMN05) 
		   ELSE s2.COLUMN05
  end)  as cName FROM SATABLE005 a 
	left outer join SATABLE007 b on b.COLUMN06=a.COLUMN02 and b.COLUMN04=@JOI 
		left outer join SATABLE002 s2 on s2.COLUMN02=a.COLUMN05
	left outer join SATABLE001 S1 on s1.COLUMN02=a.COLUMN05  where  a.COLUMN02= @SalesOrderID
end













GO
