USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_InvoiceBillDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_REPORTS_InvoiceBillDetails](
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null,
@DCNO  nvarchar(250)= null
 )
as
begin
if @Item!=''
begin
 set @whereStr= ' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
end
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc

if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s) '
end
else
begin
set @whereStr=@whereStr+' and upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
end
if @DCNO!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where dcno in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@DCNO+''') s) '
end
else
begin
set @whereStr=@whereStr+' and dcno in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@DCNO+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end
 
declare @DecimalPositions int
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end

select Q.ou,f.COLUMN04 item,f.COLUMN06 upc,Q.uomid,Q.uom,Q.Location,Q.Lot,Q.tno,Q.tdate,Q.Dtt,Q.vendor,
convert(float,convert(double precision,Q.openbal)) openbal,
convert(float,convert(double precision,Q.qr)) qr,
convert(float,convert(double precision,Q.qi)) qi,
Q.openbill,
Q.price,Q.amt, Q.price1, Q.amt1,Q.transType,Q.ItemID,Q.OPID,Q.TransTypeID,Q.Project,Q.fid,(case when f.COLUMN10=0 then null else f.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,Q.LocID,Q.LotID,f.column50 descr,Q.Cqtyp,Q.Cqty,Q.discount,Q.Tax,Q.dcno into #InvoiceBillDetailsReport from (
--(
--select c7.COLUMN03 ou,c.COLUMN04 item,c.COLUMN04 upc,iif(isnull(mu.COLUMN08,0)!=0,mu.COLUMN08,c.COLUMN19) uomid,(case when (iif(mu.COLUMN08!='',mu.COLUMN08,c.COLUMN19))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08!=0,mu.COLUMN08,c.COLUMN19))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--b.COLUMN04 tno,FORMAT(b.COLUMN08,@DateF) tdate,b.COLUMN08 Dtt, (s1.COLUMN05) vendor ,
--0 openbal,iif(mu.COLUMN08!='',mu.COLUMN07,c.COLUMN09) qr,0 qi,0 openbill,c.COLUMN11 price,cast((c.COLUMN09)*(c.COLUMN11)as decimal(18,2)) amt, 0price1,0 amt1,'Bill' transType,c.COLUMN04 ItemID,b.COLUMN15 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN26 Project
--,b.COLUMN03  fid,c.COLUMN04 BrandID,c.COLUMN04 as Brand,b.COLUMN29 LocID,c.COLUMN27 LotID,c.column04 descr,c.COLUMN09 Cqtyp,0  Cqty
--from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
--left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN04 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN15  left join satable001 s1 on s1.column02=b.column05 and s1.columna03=b.columna03
-- left join MATABLE002 m on m.COLUMN02=c.COLUMN19 and m.COLUMNA03=c.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=b.COLUMN29 and l.COLUMNA02=b.COLUMN15 and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN27 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
-- where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null)  AND isnull((b.COLUMNA13),0)=0 and  
-- ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 
-- and b.COLUMN08 between @FromDate and @ToDate and b.COLUMN08 >=@FiscalYearStartDt
--)
--UNION ALL
(select c7.COLUMN03 ou,d.COLUMN05 item,d.COLUMN05 upc,d.COLUMN22 uomid,u.COLUMN04 uom,'' Location,'' Lot,
a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, s2.COLUMN05 vendor,
0 openbal, d.COLUMN10 qr,d.COLUMN10 qi,0 openbill ,iif(isnull(p.column11,0)=0,iif((isnull(i.COLUMN17,0))=0,isnull((m24.column04),0),(i.COLUMN17)),p.column11) price,iif(isnull(p.column11,0)=0,iif((isnull(i.COLUMN17,0))=0,isnull((m24.column04),0),(i.COLUMN17)),p.column11)*(d.COLUMN10) amt,d.COLUMN13 price1, -((d.COLUMN10))*(d.COLUMN13)  amt1,'Invoice' transType,d.COLUMN05 ItemID,a.COLUMN14 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN29 Project
,a.COLUMN03 fid,d.COLUMN05 BrandID,d.COLUMN05 as Brand,a.COLUMN39 LocID,d.COLUMN31 LotID,d.column05 descr,0 Cqtyp,d.COLUMN10 Cqty,iif(cast(isnull(d.COLUMN19,0) as nvarchar(250))='','0',d.COLUMN19) discount,
--iif((isnull(d.column23,0) = 0 and isnull(a.column22,0)!=0 and isnull(a.column22,0)-isnull(a.column25,0)!=0),cast((d.column14)*(((a.Column24)/((a.column22-a.column25)/100))/100)as decimal(10,2)),d.column23) Tax,
iif(( (isnull(d.column21,1000)=1000 ) and isnull(a.column22,0)!=0 and isnull(a.column22,0)-isnull(a.column25,0)!=0),cast((d.column14)*(((a.column24-isnull(l.column23,0))/((a.column22-a.column25-isnull(l.column14,0))/100))/100)as decimal(10,2)),d.column23) Tax,
a.COLUMN12 dcno
from SATABLE009 a 
--inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and d.columna03=a.columna03 and  isnull((d.COLUMNA13),0)=0-- and  isnull((d.COLUMN21),1000)!=1000
left join (
select d1.columna02,d1.columna03,d1.columna13,sum(d1.column23)column23,sum(d1.column14)column14,d1.COLUMN15 from SATABLE009 a 
inner join  SATABLE010 d1 on d1.COLUMN15=a.COLUMN01 and d1.columna03=a.columna03 and  isnull((d1.COLUMNA13),0)=0 and 
 isnull((d1.COLUMN21),1000)!=1000
where (d1.COLUMN04='0' or d1.COLUMN04='' or d1.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
 AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
 and a.COLUMN08 between @FromDate and @ToDate  group by d1.columna02,d1.columna03,d1.columna13,d1.COLUMN15) L on L.COLUMN15=a.COLUMN01 and l.columna03=a.columna03 and  isnull((l.COLUMNA13),0)=0

inner join MATABLE002 u on u.COLUMN02=d.COLUMN22 and  isnull((u.COLUMNA13),0)=0 --and u.columna03=a.columna03
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN05 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN14  left join satable002 s2 on s2.column02=a.column05 and s2.columna03=a.columna03
 --left join CONTABLE030 l on l.COLUMN02=a.COLUMN39 and l.COLUMNA02=a.COLUMN14 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN31 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN05 and i.COLUMN19=d.COLUMN22 and i.COLUMN13=a.COLUMN14 and isnull(i.COLUMN21,0)=isnull(a.COLUMN39,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
left outer join MATABLE024 M24 on m24.COLUMN07=d.COLUMN05 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03 and m24.column06='Purchase' 
 left join(select max(column02)column02,column11,column04,columna03 from PUTABLE006 where COLUMNA03=@AcOwner and  isnull((COLUMNA13),0)=0  group by column04,column11,columna03
 having max(column02)in(select max(column02) from PUTABLE006 where COLUMNA03=@AcOwner and  isnull((COLUMNA13),0)=0  group by column04))p on p.COLUMN04=d.COLUMN05 and p.columna03=a.columna03 
where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
 AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
 and a.COLUMN08 between @FromDate and @ToDate --and a.COLUMN08 >=@FiscalYearStartDt
)
-- UNION ALL
--  (select c7.COLUMN03  ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,dl.COLUMN04 Location,null Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, null vendor,
--0 openbal,case when a.COLUMN06!='' then (iif(mu.COLUMN08!='',mu.COLUMN07,d.COLUMN06)) else 0 end qr, 0  qi,
--0 openbill ,iif(i.COLUMN17=0,isnull(m24.column04,0),i.COLUMN17) price,(iif(i.COLUMN17=0,isnull(m24.column04,0),i.COLUMN17)*(d.COLUMN06)) amt,0 price1,0 amt1,'Transfer' transType,d.COLUMN03 ItemID,a.COLUMN06 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project
-- ,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN10 LocID,NULL LotID,d.column03 descr,0 Cqtyp,0 Cqty
-- from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
--left join CONTABLE007 c7 on (c7.column02=a.column06)  
-- left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
-- left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMN06 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--left outer join MATABLE024 M24 on m24.COLUMN07=d.COLUMN03 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03   and m24.column06='Purchase' 
-- left join CONTABLE030 dl on dl.COLUMN02=a.COLUMN10  and dl.COLUMNA03=a.COLUMNA03
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt)
--UNION ALL
--(select c7.COLUMN03  ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else ( select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,sl.COLUMN04 Location,null Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, null vendor,
--0 openbal,0  qr,case when a.COLUMN05!='' then  (iif(mu.COLUMN08!='',mu.COLUMN07,d.COLUMN06)) else 0 end qi,0 openbill,0 price,0 amt,iif(i.COLUMN17=0,isnull(m24.column04,0),i.COLUMN17)price1,-(iif(i.COLUMN17=0,isnull(m24.column04,0),i.COLUMN17)*(d.COLUMN06))amt1,'Transfer' transType,d.COLUMN03 ItemID,a.COLUMN05 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project 
--,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN09 LocID,NULL LotID,d.column03 descr,0 Cqtyp,0 Cqty
-- from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
--left join CONTABLE007 c7 on (c7.Column02=a.column05) 
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMN05 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--left outer join MATABLE024 M24 on m24.COLUMN07=d.COLUMN03 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03   and m24.column06='Purchase' 
-- left join CONTABLE030 sl on sl.COLUMN02=a.COLUMN09  and sl.COLUMNA03=a.COLUMNA03 
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt
--)UNION ALL
-- (select c7.COLUMN03 ou,d.COLUMN03 item,d.COLUMN03 upc,d.COLUMN22 uomid,(case when d.COLUMN22=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN05,@DateF) tdate,a.COLUMN05 Dtt, null vendor,
--0 openbal,isnull(d.COLUMN08,0)-isnull(d.COLUMN16,0) qr,0 qi ,0 openbill,iif(i.COLUMN17=0,isnull(m24.column04,0),i.COLUMN17) price,CAST((isnull(d.COLUMN08,0)-isnull(d.COLUMN16,0))*iif(i.COLUMN17=0,isnull(m24.column04,0),i.COLUMN17) AS DECIMAL(18,2)) amt,0price1,0amt1,'Adjustment' transType,d.COLUMN03 ItemID,a.COLUMN10 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,'' Project
--,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN16 LocID,d.COLUMN23 LotID,d.column03 descr,0 Cqtyp,0 Cqty
--from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and d.COLUMNa02=a.COLUMNa02 and d.COLUMNa03=a.COLUMNa03  and   isnull((d.COLUMNA13),0)=0
--left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN10 
--left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and m.COLUMNA03=d.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN16 and l.COLUMNA02=a.COLUMN10 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN23 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and isnull(bm.COLUMNA13,0)=0 and bm.COLUMNA03=d.COLUMNA03
--left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN22 and i.COLUMN13=a.COLUMN10 and isnull(i.COLUMN21,0)=isnull(a.COLUMN16,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--left outer join MATABLE024 M24 on m24.COLUMN07=d.COLUMN03 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03   and m24.column06='Purchase' 
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and a.COLUMN05 >=@FiscalYearStartDt
--)

-- UNION ALL
-- ----------------
--(select ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,isnull(sum(openbal),0) as openbal,qr,qi,
--isnull(sum(openbill),0) openbill, isnull(sum(price),0)price, isnull(sum(amt),0)amt, isnull(sum(price1),0)price, isnull(sum(amt1),0)amt,transType,ItemID,OPID,0 TransTypeID,null Project
--,fid,BrandID,Brand, LocID,LotID,max(descr) descr,null Cqtyp, null Cqty from (
----(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,c.COLUMN04 item,c.COLUMN04 upc,iif(isnull(mu.COLUMN08,0)!=0,mu.COLUMN08,c.COLUMN19) uomid,(case when (iif(mu.COLUMN08!='',mu.COLUMN08,c.COLUMN19))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08!=0,mu.COLUMN08,c.COLUMN19))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
----null tno,null tdate,null Dtt, null vendor ,
----0 openbal,0 qr,0 qi,sum(isnull(c.COLUMN09,0)) openbill,avg(isnull(c.COLUMN11,0)) price,cast((sum(isnull(c.COLUMN09,0))*(avg(isnull(c.COLUMN11,0))))as decimal(18,2)) amt,0 price1,0 amt1,'Opening' transType,c.COLUMN04 ItemID,b.COLUMN15 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN26 Project
----,null  fid,c.COLUMN04 BrandID,c.COLUMN04 as Brand,b.COLUMN29 LocID,c.COLUMN27 LotID,c.COLUMN04 descr,null Cqtyp, null Cqty
----from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
----inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
----left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN04 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
----left join MATABLE002 m on m.COLUMN02=c.COLUMN19 and m.COLUMNA03=c.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=b.COLUMN29 and l.COLUMNA02=b.COLUMN15 and l.COLUMNA03=b.COLUMNA03 
----left join FITABLE043 bm on bm.COLUMN02=c.COLUMN27 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
---- where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0  
---- AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
---- AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
---- group by f.COLUMN10,c.COLUMN27,bm.COLUMN04,f.COLUMN06,m5.COLUMN04,b.COLUMN15,f.COLUMN04,c.COLUMN04,b.COLUMN03,b.COLUMN26,c.COLUMN19,m.COLUMN04,l.COLUMN04,mu.COLUMN08,b.COLUMN29)
----  union all
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,d.COLUMN05 item,d.COLUMN05 upc,iif(mu.COLUMN08 !=0,mu.COLUMN08,d.COLUMN22) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,null tdate,null Dtt, null vendor,
--sum(isnull(d.COLUMN10,0)) openbal,0 qr,0 qi, sum(isnull(d.COLUMN10,0)) openbill,iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17)) price,iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17))*sum(isnull(d.COLUMN10,0)) amt,
-- avg(isnull(d.COLUMN13,0)) price1, -((iif(mu.COLUMN08!='',sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN10,0))))*(avg(isnull(d.COLUMN13,0))))  amt1,'Opening' transType,d.COLUMN05 ItemID,a.COLUMN14 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN29 Project
--,null fid,d.COLUMN05 BrandID,d.COLUMN05 as Brand,a.COLUMN39 LocID,d.COLUMN31 LotID,d.COLUMN05 descr,null Cqtyp, null Cqty
--from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01  and  isnull((d.COLUMNA13),0)=0
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN05 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE030 l on l.COLUMN02=a.COLUMN39 and l.COLUMNA02=a.COLUMN14 and l.COLUMNA03=a.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=d.COLUMN31 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
--left join FITABLE010 i on i.COLUMN03=d.COLUMN05 and i.COLUMN19=d.COLUMN22 and i.COLUMN13=a.COLUMN14 and isnull(i.COLUMN21,0)=isnull(a.COLUMN39,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--left outer join MATABLE024 M24 on m24.COLUMN07=d.COLUMN05 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03 and m24.column06='Purchase' 
--where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
--AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
--and a.COLUMN08<@FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
--group by g.COLUMN10,d.COLUMN31,bm.COLUMN04,g.COLUMN06,m5.COLUMN04,a.COLUMN14,g.COLUMN04,d.COLUMN05,a.COLUMN03,a.COLUMN29,d.COLUMN22,mu.COLUMN08,l.COLUMN04,a.COLUMN39)

----union all
---- (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN05) ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,sl.COLUMN04 Location,null Lot,
----null tno,null tdate,null Dtt, null vendor,
----(iif(mu.COLUMN08  !=0,sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN06,0)))) openbal,0 qr,0 qi ,0openbill,0 price,0 amt,iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17)) price1,(iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17))*(sum(isnull(d.column06,0)))) amt1,'Opening' transType,d.COLUMN03 ItemID,a.COLUMN05 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project
----,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN09 LocID,NULL LotID,d.COLUMN03 descr,0 Cqtyp,0 Cqty
----from PRTABLE003 a left join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0 and  a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
----left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and i.COLUMN13=a.COLUMN05 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
----left outer join MATABLE024 M24 on m24.COLUMN07=g.COLUMN02 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03   and m24.column06='Purchase' 
----left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
----left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
----left join CONTABLE030 sl on sl.COLUMN02=a.COLUMN09 and sl.COLUMNA02=a.COLUMN05 and sl.COLUMNA03=a.COLUMNA03
----where  isnull((a.COLUMNA13),0)=0 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))
----group by g.COLUMN10,g.COLUMN06,m5.COLUMN04,a.columna02,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN07,mu.COLUMN08,sl.COLUMN04,a.COLUMN05,a.COLUMN09,d.COLUMN13
----) union all
---- (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN06) ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,dl.COLUMN04 Location,null Lot,
----null tno,null tdate,null Dtt, null vendor,
----0 openbal,null qr,null qi ,(iif(mu.COLUMN08  !=0,sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN06,0))))openbill,iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17)) price,(iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17))*(sum(isnull(d.column06,0)))) amt,0 price1,0 amt1,'Opening' transType,d.COLUMN03 ItemID,a.COLUMN06 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project
----,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN10 LocID,NULL LotID,d.COLUMN03 descr,0 Cqtyp,0 Cqty
----from PRTABLE003 a left join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0 and  a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
----left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and i.COLUMN13=a.COLUMN06 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
----left outer join MATABLE024 M24 on m24.COLUMN07=g.COLUMN02 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03   and m24.column06='Purchase' 
----left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
----left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
----left join CONTABLE030 dl on dl.COLUMN02=a.COLUMN10 and dl.COLUMNA02=a.COLUMN06 and dl.COLUMNA03=a.COLUMNA03
----where  isnull((a.COLUMNA13),0)=0 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))
----group by g.COLUMN10,g.COLUMN06,m5.COLUMN04,a.columna02,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN07,d.COLUMN13,mu.COLUMN08,dl.COLUMN04,a.COLUMN06,a.COLUMN10
----) union all
---- (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,d.COLUMN03 item,d.COLUMN03 upc,d.COLUMN22 uomid,(case when d.COLUMN22=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
----null tno,null tdate,null Dtt, null vendor,
---- 0openbal,null qr,null qi ,sum(isnull(d.COLUMN08,0))-sum(isnull(d.COLUMN16,0))openbill,iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17)) price,CAST((sum(isnull(d.COLUMN08,0))-sum(isnull(d.COLUMN16,0)))*iif(avg(i.COLUMN17)=0,isnull(avg(m24.column04),0),avg(i.COLUMN17)) AS DECIMAL(18,2)) amt,0 price1,0 amt1,'Opening' transType,d.COLUMN03 ItemID,a.COLUMN10 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,'' Project
----,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN16 LocID,d.COLUMN23 LotID,d.COLUMN03 descr,0 Cqtyp,0 Cqty
----from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01  and d.COLUMNa02=a.COLUMNa02 and d.COLUMNa03=a.COLUMNa03 and   isnull((d.COLUMNA13),0)=0
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
----left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and m.COLUMNA03=d.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN16 and l.COLUMNA02=a.COLUMN10 and l.COLUMNA03=a.COLUMNA03 
----left join FITABLE043 bm on bm.COLUMN02=d.COLUMN23 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and isnull(bm.COLUMNA13,0)=0 and bm.COLUMNA03=d.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
----left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN22 and i.COLUMN13=a.COLUMN10 and isnull(i.COLUMN21,0)=isnull(a.COLUMN16,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
----left outer join MATABLE024 M24 on m24.COLUMN07=g.COLUMN02 and isnull(m24.COLUMNA13,0)=0  and m24.COLUMNA03=a.COLUMNA03 and m24.column06='Purchase' 
----where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
---- AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate and a.COLUMN05 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))  
----group by g.COLUMN10,d.COLUMN23,bm.COLUMN04,g.COLUMN06,m5.COLUMN04,a.column10,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN22,m.COLUMN04,l.COLUMN04,a.COLUMN16
----)
--) p where ((TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable(',',isnull(@Type,0)) s)) or TransTypeID=0) group by ou,item,upc,uomid,uom,Location,LocID,Lot,tno,tdate,Dtt,vendor,qr,qi,transType,ItemID,OPID,fid,BrandID,Brand, LocID,LotID 
--)
  ) Q  	 inner join MATABLE007 f on f.COLUMN02=q.item and isnull(f.COLUMNA13,0)=0 and f.COLUMN48=1 and f.COLUMNA03=@AcOwner 
			 left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner 
if(@Type=null)
begin
set @Query1='select ou,item, upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,openbalisnull(openbill,0)+isnull(qr,0)qr,isnull(qi,0)+isnull(openbal,0)qi,cast(openbill as decimal(18,2))openbill, cast(price as decimal(18,2))price,  cast(amt as decimal(18,2))amt, cast(price1 as decimal(18,2))price1, -cast(amt1 as decimal(18,2))amt1,transType,ItemID,OPID,0 TransTypeID,null Project,fid,BrandID,Brand, LocID,LotID,descr,price AvgPrice,isnull(openbill,0)+isnull(qr,0)-isnull(qi,0)-isnull(openbal,0) closing,cast(((-isnull(openbill,0)-isnull(qr,0))*isnull(price,0))+((isnull(qi,0)+isnull(openbal,0))*isnull(price1,0))-isnull(discount,0) as decimal(18,2)) asset,qr Cqtyp, qi Cqty,cast(discount as decimal(18,2)) discount,Tax,dcno from #InvoiceBillDetailsReport  '+@whereStr+'    order by case IsNumeric(item) when 1 then Replicate(''0'', 100 - Len(item)) + item else item end,Dtt desc' 
end
else
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
end
else
begin
set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
end
set @Query1='select ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,openbal,isnull(openbill,0)+isnull(qr,0)qr,isnull(qi,0)+isnull(openbal,0)qi,cast(openbill as decimal(18,2))openbill, cast(price as decimal(18,2))price, cast(amt as decimal(18,2))amt, cast(price1 as decimal(18,2))price1, -cast(amt1 as decimal(18,2))amt1,transType,ItemID,OPID,0 TransTypeID,null Project,fid,BrandID,Brand, LocID,LotID,descr,price AvgPrice,isnull(openbill,0)+isnull(qr,0)-isnull(qi,0)-isnull(openbal,0) closing,cast(((-isnull(openbill,0)-isnull(qr,0))*isnull(price,0))+((isnull(qi,0)+isnull(openbal,0))*isnull(price1,0))-isnull(discount,0) as decimal(18,2)) asset,qr Cqtyp,qi Cqty,cast(discount as decimal(18,2)) discount,Tax,dcno from #InvoiceBillDetailsReport
			    '+@whereStr+'    order by case IsNumeric(item) when 1 then Replicate(''0'', 100 - Len(item)) + item else item end,Dtt desc'  
 end
exec (@Query1) 
end

--select ''ou,''item,''upc,''uomid,''uom,''Location,''Lot,''tno,''tdate,''Dtt,''vendor,''openbal,''qr,''qi,''openbill, ''price, ''amt,'' price1, ''amt1,''transType,''ItemID,''OPID,0 TransTypeID,null Project,''fid,''BrandID,''Brand,'' LocID,''LotID,''descr,'' AvgPrice,'' closing,'' asset


--select * from putable005
GO
