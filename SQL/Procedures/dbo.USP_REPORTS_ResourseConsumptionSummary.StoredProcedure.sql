USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_ResourseConsumptionSummary]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   procedure [dbo].[USP_REPORTS_ResourseConsumptionSummary]
(

@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@IFamily nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
)
as 
begin
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where ProjectID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and ProjectID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end

select * into #ResourceConsumption from 
(
select 'Opening Balance' ob, isnull(sum(PrdAmnt),0) PrdAmnt,ou,OPID,ProjectID, Project,(sum(Color)) Color,(sum(Chemical)) Chemical,(sum(Color)) Opening1,(sum(Chemical)) Opening2,(sum(Billing)) Billing from(

select 'Opening Balance' ob, ( 0 ) PrdAmnt
,(c7.COLUMN03) ou,b.COLUMN11 OPID,b.COLUMN08 ProjectID,pr1.COLUMN05 Project ,((isnull(c.COLUMN06,0))*(isnull(c.COLUMN08,0))) 'Color',0 'Chemical',0 Opening1,0 Opening2,(0) Billing from 
PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=b.COLUMNA03 
inner join MATABLE003 m3 on m3.COLUMN02=f.COLUMN12 and  m3.COLUMNA03 =b.COLUMNA03
inner join PRTABLE001 pr1 on pr1.COLUMN02=b.COLUMN08  and  pr1.COLUMNA03 =b.COLUMNA03
left outer join PRTABLE009 PR9 ON    PR9.COLUMN05= b.COLUMN09 AND PR9.COLUMNA03=b.COLUMNA03 
left JOIN PRTABLE010 PR10 ON PR9.COLUMN01= PR10.COLUMN10 AND PR10.COLUMN03= b.COLUMN08 AND PR10.COLUMNA03=b.COLUMNA03 and PR10.COLUMN03= b.COLUMN08
inner join CONTABLE007 c7 on c7.column02=b.COLUMN11 and  c7.COLUMNA03 =b.COLUMNA03
where  isnull((b.COLUMNA13),0)=0  and (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND 
b.COLUMNA03=@AcOwner  and b.COLUMN09< @FromDate and m3.COLUMN04 in('Color','Colour','Colors','Colours')and PR9.COLUMN05< @FromDate

union all
select 'Opening Balance' ob, ( (PR10.COLUMN04) ) PrdAmnt
,(c7.COLUMN03) ou,C7.COLUMN02 OPID,pr1.COLUMN02 ProjectID,pr1.COLUMN05 Project ,0 'Color',0 'Chemical',0 Opening1,0 Opening2 ,
0 Billing from 
PRTABLE009 PR9 inner join PRTABLE010 PR10 on PR9.COLUMN01= PR10.COLUMN10 AND PR10.COLUMNA03= PR9.COLUMNA03  and isnull(PR10.COLUMNA13,0)=0
inner join PRTABLE001 pr1 on pr1.COLUMN02=PR10.COLUMN03  and  pr1.COLUMNA03 =PR9.COLUMNA03  and isnull(pr1.COLUMNA13,0)=0
inner join CONTABLE007 c7 on c7.column02=PR9.COLUMNA02 and  c7.COLUMNA03 =PR9.COLUMNA03
where  isnull((PR9.COLUMNA13),0)=0  and (PR9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or PR9.columna02 is null) AND 
PR9.COLUMNA03=@AcOwner  and  PR9.COLUMN05 <@FromDate

union all

select 'Opening Balance' ob, null PrdAmnt
,(c7.COLUMN03) ou,null OPID,null ProjectID,null Project ,null 'Color',null 'Chemical',null Opening1,null Opening2
,convert(float,convert(double precision ,(isnull(PR9.COLUMN08,0)))) Billing  from 
PRTABLE009 PR9  
inner join CONTABLE007 c7 on c7.column02=PR9.COLUMNA02 and  c7.COLUMNA03 =PR9.COLUMNA03
where isnull((PR9.COLUMNA13),0)=0  and (PR9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or PR9.columna02 is null) AND 
PR9.COLUMNA03=@AcOwner  and  PR9.COLUMN05 <@FromDate

union all
select 'Opening Balance' ob,( 0) PrdAmnt,
 (c7.COLUMN03) ou,b.COLUMN11 OPID,b.COLUMN08 ProjectID,pr1.COLUMN05 Project ,0 'Color',((isnull(c.COLUMN06,0))*(isnull(c.COLUMN08,0))) 'Chemical',0 Opening1,0 Opening2,(0) Billing from 
PRTABLE007 b 
inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0  and f.COLUMNA03=b.COLUMNA03 
inner join MATABLE003 m3 on m3.COLUMN02=f.COLUMN12 and  m3.COLUMNA03 =b.COLUMNA03
inner join PRTABLE001 pr1 on pr1.COLUMN02=b.COLUMN08 and  pr1.COLUMNA03 =b.COLUMNA03
inner join CONTABLE007 c7 on c7.column02=b.COLUMN11 and  c7.COLUMNA03 =b.COLUMNA03
left outer join PRTABLE009 PR9 ON    PR9.COLUMN05= b.COLUMN09 AND PR9.COLUMNA03=b.COLUMNA03 
left JOIN PRTABLE010 PR10 ON PR9.COLUMN01= PR10.COLUMN10 AND PR10.COLUMN03= b.COLUMN08 AND PR10.COLUMNA03=b.COLUMNA03 and PR10.COLUMN03= b.COLUMN08
where  isnull((b.COLUMNA13),0)=0    and (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner  and b.COLUMN09< @FromDate and m3.COLUMN04 in('Chemical','Chemicals') and PR9.COLUMN05< @FromDate
) a group by a.ou,a.ProjectID,a.Project,a.OPID

union all

select ob,sum(PrdAmnt),ou,OPID,ProjectID,Project,sum(Color)Color,sum(Chemical)Chemical,Sum(Opening1)Opening1,sum(Opening2)Opening2,isnull(sum(Billing),0) Billing from(

select NULL ob,( 0 ) PrdAmnt
,(c7.COLUMN03) ou,b.COLUMN11 OPID,b.COLUMN08 ProjectID,pr1.COLUMN05 Project ,((isnull(c.COLUMN06,0))*(isnull(c.COLUMN08,0))) 'Color',0 'Chemical',0 Opening1,0 Opening2,(0) Billing from 
PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=b.COLUMNA03 
inner join MATABLE003 m3 on m3.COLUMN02=f.COLUMN12 and  m3.COLUMNA03 =b.COLUMNA03
inner join PRTABLE001 pr1 on pr1.COLUMN02=b.COLUMN08  and  pr1.COLUMNA03 =b.COLUMNA03
left outer join PRTABLE009 PR9 ON    PR9.COLUMN05= b.COLUMN09 AND PR9.COLUMNA03=b.COLUMNA03 and PR9.COLUMN05  between @FromDate and @ToDate
left JOIN PRTABLE010 PR10 ON PR9.COLUMN01= PR10.COLUMN10 AND PR10.COLUMN03= b.COLUMN08 AND PR10.COLUMNA03=b.COLUMNA03 and PR10.COLUMN03= b.COLUMN08
inner join CONTABLE007 c7 on c7.column02=b.COLUMN11 and  c7.COLUMNA03 =b.COLUMNA03
where  isnull((b.COLUMNA13),0)=0  and (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND 
b.COLUMNA03=@AcOwner  and b.COLUMN09 between @FromDate and @ToDate and m3.COLUMN04 in('Color','Colour','Colors','Colours')

union all
select NULL ob, ( (PR10.COLUMN04) ) PrdAmnt
,(c7.COLUMN03) ou,C7.COLUMN02 OPID,pr1.COLUMN02 ProjectID,pr1.COLUMN05 Project ,0 'Color',0 'Chemical',0 Opening1,0 Opening2 ,
0 Billing from 
PRTABLE009 PR9 inner join PRTABLE010 PR10 on PR9.COLUMN01= PR10.COLUMN10 AND PR10.COLUMNA03= PR9.COLUMNA03  and isnull(PR10.COLUMNA13,0)=0
inner join PRTABLE001 pr1 on pr1.COLUMN02=PR10.COLUMN03  and  pr1.COLUMNA03 =PR9.COLUMNA03  and isnull(pr1.COLUMNA13,0)=0
inner join CONTABLE007 c7 on c7.column02=PR9.COLUMNA02 and  c7.COLUMNA03 =PR9.COLUMNA03
where  isnull((PR9.COLUMNA13),0)=0  and (PR9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or PR9.columna02 is null) AND 
PR9.COLUMNA03=@AcOwner  and  PR9.COLUMN05 between @FromDate and @ToDate

union all
select NULL ob, null PrdAmnt
,(c7.COLUMN03) ou,null OPID,null ProjectID,null Project ,null 'Color',null 'Chemical',0 Opening1,0 Opening2
,convert(float,convert(double precision ,isnull(PR9.COLUMN08,0))) Billing  from 
PRTABLE009 PR9 
inner join CONTABLE007 c7 on c7.column02=PR9.COLUMNA02 and  c7.COLUMNA03 =PR9.COLUMNA03
where   isnull((PR9.COLUMNA13),0)=0  and (PR9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or PR9.columna02 is null) AND 
PR9.COLUMNA03=@AcOwner  and  PR9.COLUMN05 between @FromDate and @ToDate
union all
select NULL ob,( 0) PrdAmnt,
 (c7.COLUMN03) ou,b.COLUMN11 OPID,b.COLUMN08 ProjectID,pr1.COLUMN05 Project ,0 'Color',((isnull(c.COLUMN06,0))*(isnull(c.COLUMN08,0))) 'Chemical',0 Opening1,0 Opening2,(0) Billing from 
PRTABLE007 b 
inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0  and f.COLUMNA03=b.COLUMNA03 
inner join MATABLE003 m3 on m3.COLUMN02=f.COLUMN12 and  m3.COLUMNA03 =b.COLUMNA03
inner join PRTABLE001 pr1 on pr1.COLUMN02=b.COLUMN08 and  pr1.COLUMNA03 =b.COLUMNA03
inner join CONTABLE007 c7 on c7.column02=b.COLUMN11 and  c7.COLUMNA03 =b.COLUMNA03
left outer join PRTABLE009 PR9 ON    PR9.COLUMN05= b.COLUMN09 AND PR9.COLUMNA03=b.COLUMNA03 and PR9.COLUMN05  between @FromDate and @ToDate
left JOIN PRTABLE010 PR10 ON PR9.COLUMN01= PR10.COLUMN10 AND PR10.COLUMN03= b.COLUMN08 AND PR10.COLUMNA03=b.COLUMNA03 and PR10.COLUMN03= b.COLUMN08
where  isnull((b.COLUMNA13),0)=0    and (b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner  and b.COLUMN09 between @FromDate and @ToDate and m3.COLUMN04 in('Chemical','Chemicals')
)D 
group by ou,OPID,ProjectID,Project,ob


) Query
set @Query1='select * from #ResourceConsumption '+@whereStr

set @Query1='select ob, sum(PrdAmnt) PrdAmnt,ou,OPID,ProjectID,Project,sum(Color) Color,sum(Chemical) Chemical,sum(Opening1) Opening1,sum(Opening2) Opening2,sum(Billing) Billing from #ResourceConsumption '+@whereStr +' group by ou,OPID,ProjectID,Project,ob order by Project desc '

exec (@Query1) 
end




GO
