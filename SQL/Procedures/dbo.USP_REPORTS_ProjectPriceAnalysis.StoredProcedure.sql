USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_ProjectPriceAnalysis]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_REPORTS_ProjectPriceAnalysis]
(
@FromDate nvarchar(250)=null,
@ToDate nvarchar(250)=null,
@Project nvarchar(250)=null,
@SubProject nvarchar(250)=null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@DateF nvarchar(250)=null
)
as
begin
declare @whereStr nvarchar(1000)=null
if @Project!=''
begin
 set @whereStr= 'p.COLUMN08 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
if @SubProject!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='p.COLUMN22 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SubProject+''') s)'
end
else
begin
set @whereStr=@whereStr+' and p.COLUMN22 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SubProject+''') s)'
end
end
if @OPUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' p.COLUMN11 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and p.COLUMN11 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' 1=1'
end

exec ('SELECT r.Date,r.Project,r.[Sub Project],r.OpUnit,SUM(r.Cost)Cost,r.Studcnt [No of Students],(case when r.Studcnt!=''0'' then cast(SUM(r.Cost)/r.Studcnt as decimal(18,2)) else 0 end)''Per Student Cost'' FROM(
SELECT FORMAT(p.COLUMN09,'''+@DateF+''') Date,m.COLUMN05 Project,s.COLUMN04 ''Sub Project'',o.COLUMN03 OpUnit,
cast((isnull(pp.COLUMN04,0)*isnull(pl.COLUMN06,0)) as decimal(18,2)) Cost,cast(iif(cast(p.COLUMN15 as nvarchar(250))='''',''0'',isnull(p.COLUMN15,0)) as decimal(18,2)) Studcnt,cast(p.COLUMN09 as date) cdate FROM PRTABLE007 p
inner join PRTABLE008 pl on pl.COLUMN10=p.COLUMN01 and pl.COLUMNA02=p.COLUMNA02 and pl.COLUMNA03=p.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE024 pp on pp.COLUMN07=pl.COLUMN03 and pp.COLUMN06=''Purchase'' and (isnull(pp.COLUMN03,0) in('''+@OPUnit+''')  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=pl.COLUMNA03 and pp.COLUMNA13=0 
inner join PRTABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
inner join CONTABLE007 o on o.COLUMN02=p.COLUMN11 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left join MATABLE002 s on s.COLUMN03=11171 and s.COLUMN02=p.COLUMN22 and (s.COLUMNA03=p.COLUMNA03 or s.COLUMNA03 is null) and isnull(s.COLUMNA13,0)=0
where p.COLUMN03=1602 AND  isnull(p.COLUMNA13,0)=0 AND  p.COLUMNA03='+@AcOwner+' and cast(p.COLUMN09 as date) between '''+@FromDate+''' and '''+@ToDate+''' and 
'+@whereStr +') r group by r.Date,r.Project,r.[Sub Project],r.OpUnit,r.Studcnt,r.cdate order by cast(r.cdate as date)')
end








GO
