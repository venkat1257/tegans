USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE044]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE044]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null, 
	@COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null, 
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null, 
	@COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null, 
	@COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null, 
	@COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  
	@COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null, 
	@COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  
	@COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  
	@COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   
	@Direction  nvarchar(250),       @TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null,
	@status nvarchar(250)=null
)

AS

BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE044_SequenceNo
insert into FITABLE044 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, 
   COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, 
   COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, 
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, 
   COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, 
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, 
   @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, 
   @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, 
   @COLUMND10
)  

set @ReturnValue = 1
END
 

IF @Direction = 'Select'
BEGIN
select * from FITABLE044
END 

 

IF @Direction = 'Update'
BEGIN
if(@COLUMN03!='')
begin
UPDATE FITABLE044 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
   COLUMN17=@COLUMN17,    COLUMN20=@COLUMN20,   COLUMN21=@COLUMN21,   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  
   COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  
   COLUMNA05=@COLUMNA05,   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  
   COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  
   COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  
   COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  
   COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  
   COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  
   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   end
set @COLUMN18= (select COLUMN18 from FITABLE044 where COLUMN02=@COLUMN02)
 --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
set @COLUMN06= (select cast(COLUMN06 as date) from FITABLE044 where COLUMN02=@COLUMN02)
set @COLUMN19= (select COLUMN19 from FITABLE044 where COLUMN02=@COLUMN02)
 declare @Amt decimal(18,2),@Tax decimal(18,2),@balance decimal(18,2) ,@Voucher# nvarchar(250),@Bank nvarchar(250) ,@internalid nvarchar(250),@bid nvarchar(250),@chqstatus nvarchar(250),
 @Project nvarchar(250),@Projectactualcost decimal(18,2),@chkamt decimal(18,2),@pid nvarchar(250)
set @chqstatus=(select COLUMN15 from  FITABLE044  where COLUMN02=@column02)
set @Project=(select COLUMN21 from  FITABLE044  where COLUMN02=@column02)
set @chkamt=(select COLUMN13 from  FITABLE044  where COLUMN02=@column02)
set @COLUMN10=(select COLUMN10 from  FITABLE044  where COLUMN02=@column02)
set @COLUMN05=(select COLUMN05 from  FITABLE044  where COLUMN02=@column02)
 if @status='Clear'
 BEGIN
if(@COLUMN10=22627 and @chqstatus!=22629)
begin
if(@COLUMN05=22625)
begin
set @Voucher#= (select COLUMN04 from FITABLE023 where COLUMN02=@COLUMN18)
set @Bank= (select COLUMN12 from FITABLE024 where COLUMN02=@COLUMN19)
if(@Bank='' or @Bank is null )
begin
set @Bank= (select COLUMN09 from FITABLE023 where COLUMN02=@COLUMN18)
end
set @Amt= (select isnull(COLUMN05,0) from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
set @Tax= (select isnull(COLUMN06,0) from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
update FITABLE044 set COLUMN15=22629 where COLUMN02=@column02
declare  @assetid nvarchar(250),@Memo nvarchar(250),@name nvarchar(250),@Account nvarchar(250),@FormName nvarchar(250)
SET @assetid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE034)+1
set @Memo= (select COLUMN04 from FITABLE024 where COLUMN02=@COLUMN19)
if(@Memo='' or @Memo is null )
begin
set @Memo= (select COLUMN11 from FITABLE023 where COLUMN02=@COLUMN18)
end
set @FormName=(select COLUMN04 from CONTABLE0010 where COLUMN02=(select COLUMN03 from FITABLE023 where COLUMN02=@COLUMN18))
set @name=(select COLUMN07 from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
set @Account=(select COLUMN03 from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
set @internalid=(select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18)
set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
set @project=(select COLUMN08 from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
set @bid=((select max(isnull(column02,999)) from PUTABLE019)+1)
insert into FITABLE034 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMNA13,COLUMN12,
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   @assetid,  UPPER(@FormName), @COLUMN06,  @Memo,  @name,  null,  (CAST(ISNULL(@Amt,0) AS DECIMAL(18,2))+CAST(ISNULL(@Tax,0) AS DECIMAL(18,2))),   @Voucher#,'1116',0,@project,
	   @COLUMNA02, @COLUMNA03
	)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		DECLARE @ACCOUNTTYPE NVARCHAR(250)
		set @ACCOUNTTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Bank AND COLUMNA03=@COLUMNA03)
--Cash
IF(@ACCOUNTTYPE=22409)
BEGIN
	SET @bid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@bid,1000),@Bank,  @COLUMN06,  @Voucher#,UPPER(@FormName),@COLUMN08,  @name,  @internalid,  '',0,  
	cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)),(cast(@balance as decimal(18,2))+(cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)))),@project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
END
else
begin
	Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08, COLUMN11, COLUMN12,COLUMN20,COLUMNA02, COLUMNA03)
 values(@bid,@Bank,@COLUMN06,@Voucher#,UPPER(@FormName),@name,@internalid,cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)),(cast(@balance as decimal(18,2))+(cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)))),@project,@COLUMNA02, @COLUMNA03)
end
--update  FITABLE034  set COLUMN08=(@Amt+@Tax) where COLUMN03='PDC RECEIVED'and    COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03

update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))+cast(@Amt as decimal(18,2))+cast(@Tax as decimal(18,2))) where column02=@Bank and COLUMNA03=@COLUMNA03
end
else if(@COLUMN05=22626)
begin
if exists (select COLUMN04 from FITABLE045 where COLUMN02=@COLUMN18 and COLUMN01=@COLUMN19)
begin
if((@Project!='' and @Project!=0))
	begin
	set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
	update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@chkamt as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
	end
declare @TransNo nvarchar(250),	@AccType nvarchar(250),@liabid nvarchar(250)	
set @TransNo= (select COLUMN04 from FITABLE045 where COLUMN02=@COLUMN18 and COLUMN01=@COLUMN19)
set @internalid= (select COLUMN01 from FITABLE045 where COLUMN02=@COLUMN18 and COLUMN01=@COLUMN19)
set @Bank= (select COLUMN11 from  FITABLE044  where COLUMN02=@column02)
set @name=(select COLUMN09 from  FITABLE044  where COLUMN02=@column02)
set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
set @AccType=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02='1117' and COLUMNA03=@COLUMNA03)
update FITABLE044 set COLUMN15=22629 where COLUMN02=@column02
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
set @ACCOUNTTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Bank AND COLUMNA03=@COLUMNA03)
--Cash
IF(@ACCOUNTTYPE=22409)
BEGIN
	SET @bid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@bid,1000),@Bank,  @COLUMN06,  @Voucher#,UPPER(@FormName),@COLUMN08,  @name,  @internalid,  '',0,  
	cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)),(cast(@balance as decimal(18,2))+(cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)))),@project,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
END
else
begin
	set @pid=((select max(isnull(column02,999)) from PUTABLE019)+1)
	Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMNA02, COLUMNA03, COLUMN20)
	values(isnull(@pid,999),@Bank,getdate(),@TransNo,'EXPENSE',@name,@internalid,@chkamt,(@balance -@chkamt),@COLUMNA02, @COLUMNA03,@Project)
end	
	update FITABLE001 set COLUMN10=(@balance -@chkamt),COLUMN09=getdate() where COLUMN02=@Bank and COLUMNA03=@COLUMNA03
set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
insert into FITABLE026 
	(COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN12,    COLUMN09,COLUMN17,COLUMN08,COLUMN14,COLUMN19,
	COLUMNA02, COLUMNA03 )
	values
	( @liabid,  'EXPENSE',  getdate(),  @internalid,  @name,  @chkamt,@TransNo,@AccType,'1117',@chkamt,@project,
	@COLUMNA02, @COLUMNA03)
end
else
begin
set @Voucher#= (select COLUMN04 from FITABLE020 where COLUMN02=@COLUMN18)
set @internalid= (select COLUMN01 from FITABLE022 where COLUMN02=@COLUMN19)
set @Bank= (select COLUMN11 from FITABLE022 where COLUMN02=@COLUMN19)
if(@Bank='' or @Bank is null )
begin
set @Bank= (select COLUMN06 from FITABLE020 where COLUMN02=@COLUMN18)
end
set @Amt= (select isnull(COLUMN05,0) from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
set @Tax= (select isnull(COLUMN06,0) from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
declare @TaxName nvarchar(250),@Taxtype nvarchar(250)
set @Taxtype= (select column17 from FITABLE020 where COLUMN02=@COLUMN18)
set @TaxName=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Taxtype and COLUMNA03=@COLUMNA03)
set @Memo= (select COLUMN04 from FITABLE022 where COLUMN02=@COLUMN19)
if(@Memo='' or @Memo is null )
begin
set @Memo= (select COLUMN08 from FITABLE020 where COLUMN02=@COLUMN18)
end
set @FormName=(select COLUMN04 from CONTABLE0010 where COLUMN02=(select COLUMN03 from FITABLE020 where COLUMN02=@COLUMN18))
set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
set @name=(select COLUMN07 from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
set @Account=(select COLUMN03 from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
set @bid=((select max(isnull(column02,999)) from PUTABLE019)+1)
set @AccType=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02='1117' and COLUMNA03=@COLUMNA03)
set @project=(select COLUMN08 from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
update FITABLE044 set COLUMN15=22629 where COLUMN02=@column02
--update  FITABLE026  set COLUMN12=(@Amt+@Tax) where COLUMN04='PDC ISSUED' and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
insert into FITABLE026 
	(
	    COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN12,    COLUMN09,COLUMN17,COLUMN07,COLUMN08,COLUMN14,COLUMN19,
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   @liabid,  UPPER(@FormName),  @COLUMN06,  @internalid,  @name,  (CAST(ISNULL(@Amt,0) AS DECIMAL(18,2))+CAST(ISNULL(@Tax,0) AS DECIMAL(18,2))),    @Voucher#,@AccType,@Memo,'1117',(CAST(ISNULL(@Amt,0) AS DECIMAL(18,2))+CAST(ISNULL(@Tax,0) AS DECIMAL(18,2))),@project,
	   @COLUMNA02, @COLUMNA03
	)
	Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMN20,COLUMNA02, COLUMNA03)
 values(@bid,@Bank,@COLUMN06,@Voucher#, UPPER(@FormName),@name,@internalid,(cast(@Amt as decimal(18,2))+cast(@Tax as decimal(18,2))),(cast(@balance as decimal(18,2))-(cast(@Amt as decimal(18,2))+cast(@Tax as decimal(18,2)))),@COLUMN12,@project,@COLUMNA02, @COLUMNA03)

update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@Amt as decimal(18,2))-cast(@Tax as decimal(18,2))) where column02=@Bank and COLUMNA03=@COLUMNA03
end
end
end
END
set @ReturnValue = 1
END
 

else IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE044 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--set @COLUMNA02= (select COLUMNA02 from FITABLE044 where COLUMN02=@COLUMN02)
--set @COLUMNA03= (select COLUMNA03 from FITABLE044 where COLUMN02=@COLUMN02)
--set @COLUMN18= (select COLUMN18 from FITABLE044 where COLUMN02=@COLUMN02)
--set @COLUMN19= (select COLUMN19 from FITABLE044 where COLUMN02=@COLUMN02)
--set @COLUMN10= (select COLUMN10 from FITABLE044 where COLUMN02=@COLUMN02)
--set @COLUMN05= (select COLUMN05 from FITABLE044 where COLUMN02=@COLUMN02)
--set @COLUMN06= (select COLUMN06 from FITABLE044 where COLUMN02=@COLUMN02)

----Payment Type
--if(@COLUMN10=22627)
--begin
----Type
--if(@COLUMN05=22625)
--begin
--set @Voucher#= (select COLUMN04 from FITABLE023 where COLUMN02=@COLUMN18)
--set @Bank= (select COLUMN12 from FITABLE024 where COLUMN02=@COLUMN19)
--set @Amt= (select isnull(COLUMN05,0) from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
--set @Tax= (select isnull(COLUMN06,0) from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
--update FITABLE044 set COLUMN15=22629 where COLUMN02=@column02
--SET @assetid=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE034)+1
--set @Memo= (select COLUMN04 from FITABLE024 where COLUMN02=@COLUMN19)
--set @name=(select COLUMN07 from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
--set @Account=(select COLUMN03 from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
--set @internalid=(select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18)
--set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
--set @project=(select COLUMN08 from FITABLE024 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN18))
--set @bid=((select max(isnull(column02,999)) from PUTABLE019)+1)
--if(@Amt>0)
--begin
--insert into FITABLE034 
--	(
--		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMNA13,
--		COLUMNA02, COLUMNA03 
--	)
--	values
--	( 
--	   @assetid,  'PDC RECEIVED', @COLUMN06,  @Memo,  @name,  null,  (CAST(ISNULL(@Amt,0) AS DECIMAL(18,2))+CAST(ISNULL(@Tax,0) AS DECIMAL(18,2))),   @Voucher#,'1116',0,
--	   @COLUMNA02, @COLUMNA03
--	)
--	Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08, COLUMN11, COLUMN12,COLUMN20,COLUMNA02, COLUMNA03)
-- values(@bid,@Bank,@COLUMN06,@Voucher#,'RECEIPT VOUCHER',@name,@internalid,cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)),(cast(@balance as decimal(18,2))+(cast(isnull(@Amt,0) as decimal(18,2))+cast(isnull(@Tax,0) as decimal(18,2)))),@project,@COLUMNA02, @COLUMNA03)

----update  FITABLE034  set COLUMN08=(@Amt+@Tax) where COLUMN03='PDC RECEIVED'and    COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03

--update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))+cast(@Amt as decimal(18,2))+cast(@Tax as decimal(18,2))) where column02=@Bank and COLUMNA03=@COLUMNA03
--end
--end
--else if(@COLUMN05=22626)
--begin
--set @Voucher#= (select COLUMN04 from FITABLE020 where COLUMN02=@COLUMN18)
--set @internalid= (select COLUMN01 from FITABLE022 where COLUMN02=@COLUMN19)
--set @Bank= (select COLUMN11 from FITABLE022 where COLUMN02=@COLUMN19)
--set @Amt= (select isnull(COLUMN05,0) from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
--set @Tax= (select isnull(COLUMN06,0) from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
--set @Taxtype= (select column17 from FITABLE020 where COLUMN02=@COLUMN18)
--set @TaxName=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Taxtype and COLUMNA03=@COLUMNA03)
--set @Memo= (select COLUMN04 from FITABLE022 where COLUMN02=@COLUMN19)
--set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
--set @name=(select COLUMN07 from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
--set @Account=(select COLUMN03 from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
--set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
--set @bid=((select max(isnull(column02,999)) from PUTABLE019)+1)
--set @AccType=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02='1117' and COLUMNA03=@COLUMNA03)
--set @project=(select COLUMN08 from FITABLE022 where COLUMN02=@COLUMN19 and COLUMN09 in (select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN18))
--update FITABLE044 set COLUMN15=22629 where COLUMN02=@column02
----update  FITABLE026  set COLUMN12=(@Amt+@Tax) where COLUMN04='PDC ISSUED' and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--if(@Amt>0)
--begin
--insert into FITABLE026 
--	(
--	    COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN12,    COLUMN09,COLUMN17,COLUMN07,COLUMN08,COLUMN14,
--		COLUMNA02, COLUMNA03 
--	)
--	values
--	( 
--	   @liabid,  'PDC ISSUED',  @COLUMN06,  @internalid,  @name,  (CAST(ISNULL(@Amt,0) AS DECIMAL(18,2))+CAST(ISNULL(@Tax,0) AS DECIMAL(18,2))),    @Voucher#,@AccType,@Memo,'1117',(CAST(ISNULL(@Amt,0) AS DECIMAL(18,2))+CAST(ISNULL(@Tax,0) AS DECIMAL(18,2))),
--	   @COLUMNA02, @COLUMNA03
--	)
--	Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMN20,COLUMNA02, COLUMNA03)
-- values(@bid,@Bank,@COLUMN06,@Voucher#,'ADVANCE PAYMENT',@name,@internalid,(cast(@Amt as decimal(18,2))+cast(@Tax as decimal(18,2))),(cast(@balance as decimal(18,2))-(cast(@Amt as decimal(18,2))+cast(@Tax as decimal(18,2)))),@COLUMN12,@project,@COLUMNA02, @COLUMNA03)

--update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@Amt as decimal(18,2))-cast(@Tax as decimal(18,2))) where column02=@Bank and COLUMNA03=@COLUMNA03
--end
--end
--end
END

end try
begin catch
return 0
end catch
end


GO
