USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PUTABLE005]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PUTABLE005]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20  nvarchar(250)=null,   @COLUMN21  nvarchar(250)=null,   @COLUMN22  nvarchar(250)=null,   
	@COLUMN23  nvarchar(250)=null,   @COLUMN24  nvarchar(250)=null,   @COLUMN25  nvarchar(250)=null,
	@COLUMN26  nvarchar(250)=null,   @COLUMN27  nvarchar(250)=null,   @COLUMN28  nvarchar(250)=null,
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	@COLUMN29  nvarchar(250)=null,   @COLUMN30  nvarchar(250)=null,   @COLUMN31  nvarchar(250)=null,
	@COLUMN32  nvarchar(250)=null,   @COLUMN33  nvarchar(250)=null,   @COLUMN34  nvarchar(250)=null,
	@COLUMN35  nvarchar(250)=null,   @COLUMN36  nvarchar(250)=null,   @COLUMN37  nvarchar(250)=null,
	@COLUMN38  nvarchar(250)=null,   @COLUMN39  nvarchar(250)=null,   @COLUMN40  nvarchar(250)=null,
	--EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	@COLUMN41  nvarchar(250)=null,   @COLUMN42  nvarchar(250)=null,   @COLUMN43  nvarchar(250)=null,
	@COLUMN44  nvarchar(250)=null,   @COLUMN45  nvarchar(250)=null,   @COLUMN46  nvarchar(250)=null,
	@COLUMN47  nvarchar(250)=null,   @COLUMN48  nvarchar(250)=null,   @COLUMN49  nvarchar(250)=null,   
	@COLUMN50  nvarchar(250)=null,   @COLUMN51  nvarchar(250)=null,   @COLUMN52  nvarchar(250)=null,
	@COLUMN53  nvarchar(250)=null,   @COLUMN54  nvarchar(250)=null,   @COLUMN55  nvarchar(250)=null,   
	@COLUMN56  nvarchar(250)=null,   @COLUMN57  nvarchar(250)=null,   @COLUMN58  nvarchar(250)=null,
	@COLUMN59  nvarchar(250)=null,   @COLUMN60  nvarchar(250)=null,   @COLUMN61  nvarchar(250)=null,   
	@COLUMN62  nvarchar(250)=null,   @COLUMN63  nvarchar(250)=null,   @COLUMN64  nvarchar(250)=null,   
	@COLUMN66  nvarchar(250)=null,   @COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   
	@COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   
	@COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   
	@COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  
	@COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  
	@COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  
	@COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  
	@COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  
	@COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@Direction  nvarchar(250),       @TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT,
	@Totamt nvarchar(250)=null
	
)
AS
BEGIN
begin try
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08='1' order by COLUMN01 desc
IF @Direction = 'Insert'
BEGIN 
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN20=(1001)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE005_SequenceNo
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   declare @tempSTR nvarchar(max)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Header Values are Intiated for PUTABLE005 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull(  @COLUMN33,'')+','+isnull(  @COLUMN34,'')+','+isnull( @COLUMN35,'')+','+isnull(  @COLUMN36,'')+','+isnull(  @COLUMN37,'')+','+isnull(  @COLUMN38,'')+','+isnull(  @COLUMN39,'')+','+isnull(  @COLUMN40,'')+','+isnull(  @COLUMN41,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into PUTABLE005 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21, 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,  COLUMN39,  COLUMN40,  COLUMN41,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN42,  COLUMN43,  COLUMN44,  COLUMN45,  COLUMN46,  COLUMN55,  COLUMN56,  COLUMN57,  COLUMN58,  
   COLUMN59,  COLUMN60,  COLUMN61,  COLUMN62,  COLUMN63,  COLUMN64,  COLUMN66,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, 
   COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, 
   COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, 
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, 
   COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,  
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN55,  @COLUMN56,  @COLUMN57,  @COLUMN58, 
   @COLUMN59,  @COLUMN60,  @COLUMN61,  @COLUMN62,  @COLUMN63,  @COLUMN64,  @COLUMN66,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, 
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, 
   @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, 
   @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, 
   @COLUMND10
)
--EMPHCS1410 LOGS CREATION BY SRINIVAS
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Header Intiated Values are Created in PUTABLE005 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
--EMPHCS1296 GNANESHWAR ON 10/12/2015 if TDS NOT there inserting record in TdsRecivable,TdsPayable in bill and invoice
if(@COLUMN28 !=''  and @COLUMN28 is not null and cast(isnull(@COLUMN28,0) as decimal(18,2))>0  )
BEGIN
DECLARE @IDCL INT
DECLARE @TYPEIDCL NVARCHAR(25)
DECLARE @NUMBERCL INT

SET @IDCl=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE026)+1
set @NUMBERCl=(SELECT (COLUMN01) FROM PUTABLE005 where column02 = @COLUMN02)
set @TYPEIDCl=(SELECT COLUMN04 FROM PUTABLE005 WHERE COLUMN01=@NUMBERCl)

  --EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liability Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(ISNULL(@IDCl,10001)as nvarchar(250)) +','+cast( ISNULL(@COLUMN08,'') as nvarchar(250))+','+  'TDS Payable'+','+  cast(isnull(@NUMBERCl,'') as nvarchar(20))+','+ cast(ISNULL(@COLUMN05,'')as nvarchar(250))+','+ cast( ISNULL(@COLUMN12,'')as nvarchar(250))+','+ cast( ISNULL(@COLUMN27,'')as nvarchar(250))+','+ cast( ISNULL(@TYPEIDCl,'')as nvarchar(250))+','+  CAST(ISNULL(@COLUMN28,0) AS nvarchar(20))+','+  ISNULL(@COLUMN27,'')+','+  'TDS Payable'+','+  ISNULL(@COLUMNA02,'')+','+
  cast( ISNULL(@COLUMNA03,'')as nvarchar(250)) +  '  ')
   )
   
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @NUMBERCl,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = '1101',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @COLUMN28,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE026 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07, COLUMN08,   COLUMN09,COLUMN11,COLUMN17,COLUMN18,COLUMN19,
		COLUMNA02, COLUMNA03 
	)
	values
	(  ISNULL(@IDCl,10001),@COLUMN08,'TDS Payable', @NUMBERCl,@COLUMN05,@COLUMN12,@COLUMN27,@TYPEIDCl,CAST(ISNULL(@COLUMN28,0) AS DECIMAL(18,2)),'TDS Payable',@COLUMN27,@COLUMN26,

	   @COLUMNA02, @COLUMNA03
	)
   --EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liability Intiated Values are Created in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
END
declare @number int
declare @AID int
declare @tmpnewID int 
declare @newID int 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
declare @SBAL DECIMAL(18,2),@PRATE DECIMAL(18,2),@Vid nvarchar(250)=null,@internalid nvarchar(250)=null,@Tax int,@TaxAG int,@ReverseCharged bit,@TOTALAMT DECIMAL(18,2)
set @internalid= (select max(column01) from PUTABLE005 where column02 = @COLUMN02)
set @ReverseCharged = (select isnull(COLUMN58,0) from PUTABLE005 where COLUMN01=@internalid)
if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') AND (@COLUMN66=0 or @COLUMN66='False' or @COLUMN66='0'))
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN14,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2)))
end
else if((@COLUMN66=1 or @COLUMN66='True' or @COLUMN66='1') AND (@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0'))
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN14,0)as decimal(18,2))-cast(ISNULL(@COLUMN63,0) as decimal(18,2)))
end
else if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') AND (@COLUMN66=1 or @COLUMN66='True' or @COLUMN66='1'))
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN14,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2))-cast(ISNULL(@COLUMN63,0) as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN14,0))
end
set @Tax= (select COLUMN23 from PUTABLE005 where COLUMN01=@internalid)
set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @number=(select max(column01) from PUTABLE005)
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
if not exists(select COLUMN05  from PUTABLE016 where  COLUMN09=@COLUMN04 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
BEGIN 
set @tmpnewID=(select MAX(COLUMN02) from PUTABLE016)
if(@tmpnewID>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
end
else
begin
set @newID=100000	
end
set @COLUMN13 = (select MAX(COLUMN01) from  PUTABLE005);
--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Payable  Values are Intiated for PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@newID as nvarchar(250)) +',2000,'+  ISNULL(@COLUMN08,'')+','+ cast(ISNULL(@COLUMN13,'')as nvarchar(250))+',Bill,'+ cast(ISNULL(@COLUMN05,'')as nvarchar(250))+','+ cast(ISNULL(@COLUMN04,'')as nvarchar(250))+','+  cast(ISNULL(@COLUMN11,'')as nvarchar(250))+','+ cast(ISNULL(@COLUMN14,'')as nvarchar(250))+','+  cast(ISNULL(@COLUMN14,'')as nvarchar(250))+','+cast(ISNULL(@COLUMNA02,'')as nvarchar(250))+','+  cast(ISNULL(@COLUMN12,'')as nvarchar(250))+ ','+cast(ISNULL(@COLUMNA03,'')as nvarchar(250))+ ' ')
					)
insert into PUTABLE016	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14, COLUMNA02,COLUMN18, COLUMNA03,COLUMN20)
			        values(@newID,2000,@COLUMN08,@COLUMN13,'BILL',@COLUMN05,@COLUMN04,(select COLUMN04 from CONTABLE025 where COLUMN02=16),@COLUMN11,@TOTALAMT,null,@TOTALAMT ,@COLUMNA02,@COLUMN12, @COLUMNA03,@COLUMN26) 
--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Payable Intiated Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''))
					
END
else
BEGIN
set @Totamt=(select COLUMN12 from PUTABLE016  where COLUMN09=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN20=@COLUMN26 )
update PUTABLE016 set COLUMN07=@COLUMN05, COLUMN04=@COLUMN08,COLUMN12=(cast(isnull(@Totamt,0) as decimal(18,2))+@TOTALAMT),COLUMN14=(cast(isnull(@Totamt,0) as decimal(18,2))+@TOTALAMT) where COLUMN09=@COLUMN04  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END
--EMPHCS1584	Shipping tab calculations in bill  BY RAJ.Jr
declare @Postage decimal(18,2),@HID nvarchar(250)
set @HID=( select COLUMN01 from PUTABLE005  WHERE COLUMN02 = @COLUMN02)
SET @Postage=(isnull(cast(@COLUMN34 as decimal(18,2)),0)+isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN40 as decimal(18,2)),0))
if(@Postage!=0)
		BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22344', @COLUMN11 = '1075',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @Postage,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @newID=(select MAX(COLUMN02) from FITABLE036)+1
		--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Expense  Values are Intiated for FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@newID as nvarchar(250)) +','+   'INVOICE'+','+ cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+  isnull(@COLUMN12,'')+','+  isnull(@COLUMN05,'')+','+  cast(isnull(@Postage,'') as nvarchar(250))+','+  isnull(@COLUMN04,'')+',1075,'+isnull(@COLUMNA02,'')+ ','+isnull(@COLUMNA03,'')+ ' ')
					)
			insert into FITABLE036 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,    COLUMN07,  COLUMN09, COLUMN10,  COLUMN11,  
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   ISNULL(@newID,10001),  'BILL',  @COLUMN08,  @HID,  @COLUMN05,  @Postage,  @COLUMN04,1075, @COLUMN26,
	   @COLUMNA02, @COLUMNA03
	)
		--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Expense Intiated Values are Created in FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''))					
		END
	IF(CAST(ISNULL(@COLUMN59,0)AS DECIMAL(18,2))!=0)
	BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22408', @COLUMN11 = '1146',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN59,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	SET @newID=((SELECT MAX(ISNULL(COLUMN02 ,1000)) FROM FITABLE036)+1)
	insert into FITABLE036(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10, COLUMN11,	COLUMNA02, COLUMNA03)
	values(@newID,'BILL',@COLUMN08,@HID,@COLUMN05,ISNULL(@COLUMN59,0),0,@COLUMN04,1146,@COLUMN26,@COLUMNA02, @COLUMNA03)
	END
	--PACKING,SHIPPING & OTHER TAXES BY VENKAT
DECLARE @PACKINGCHARGE decimal(18,2),@PACKINGAMT decimal(18,2),@TaxString nvarchar(250)=NULL,@TAXRATEM DECIMAL(18,2),
@cnt INT =0,@vochercnt INT = 1,@TAXRATE DECIMAL(18,2) ,@TTYPE INT = 0,@TaxNo DECIMAL(18,2),@AMOUNTM decimal(18,2),
@TaxColumn17 nvarchar(250),@TotTaxAmt decimal(18,2),@ActAmt decimal(18,2),@CharAcc nvarchar(250),@CharAcc1 nvarchar(250),
@ldisamt decimal(18,2),@ltaxamt decimal(18,2)
if((CAST(ISNULL(@COLUMN34,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN34,0)
SET  @Tax= ISNULL(@COLUMN64,0)
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt < @vochercnt
BEGIN
	SET @Tax = ISNULL(@COLUMN64,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt!=0)
			BEGIN
			set @vochercnt=(@vochercnt+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt=0
	end
	if(@COLUMN64>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@COLUMN66=0 OR @COLUMN66='False' or @COLUMN66='0'))
--	    BEGIN
--	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
--set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
--values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
--END
BEGIN
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN34,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
else if((@COLUMN66=1 OR @COLUMN66='True' or @COLUMN66='1'))
 BEGIN
if(@TaxAG=23582)      set @CharAcc1=(1148)
else if(@TaxAG=23583) set @CharAcc1=(1149)
else if(@TaxAG=23584) set @CharAcc1=(1150)
else if(@TaxAG=23585) set @CharAcc1=(1151)
else if(@TaxAG=23586) set @CharAcc1=(1152)
if(@CharAcc1 is not null)
BEGIN
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE,    @COLUMN17 = @TAXRATE,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@CharAcc1,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@COLUMN04)
END
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN34,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
END
END
DECLARE @cnt1 INT =0,@vochercnt1 INT = 1
if(CAST(ISNULL(@COLUMN35,0)AS DECIMAL(18,2))>0)
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN35,0)
SET  @Tax= ISNULL(@COLUMN60,0)
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt1 < @vochercnt1
BEGIN
	SET @Tax = ISNULL(@COLUMN60,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt1=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt1!=0)
			BEGIN
			set @vochercnt1=(@vochercnt1+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt1=0
	end
	if(@COLUMN60>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@COLUMN66=0 OR @COLUMN66='False' or @COLUMN66='0'))
		BEGIN
	    if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN40,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
end
	else if((@COLUMN66=1 OR @COLUMN66='True' or @COLUMN66='1'))
 BEGIN
if(@TaxAG=23582)      set @CharAcc1=(1148)
else if(@TaxAG=23583) set @CharAcc1=(1149)
else if(@TaxAG=23584) set @CharAcc1=(1150)
else if(@TaxAG=23585) set @CharAcc1=(1151)
else if(@TaxAG=23586) set @CharAcc1=(1152)
if(@CharAcc1 is not null)
BEGIN
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE,    @COLUMN17 = @TAXRATE,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11,COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@CharAcc1,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@COLUMN04)
END
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN35,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
END
END
DECLARE @cnt2 INT =0,@vochercnt2 INT = 1
if((CAST(ISNULL(@COLUMN40,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN40,0)
SET  @Tax= ISNULL(@COLUMN61,0)
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt2 < @vochercnt2
BEGIN
	SET @Tax = ISNULL(@COLUMN61,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt2=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt2!=0)
			BEGIN
			set @vochercnt2=(@vochercnt2+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt2=0
	end
	if(@COLUMN61>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@COLUMN66=0 OR @COLUMN66='False' or @COLUMN66='0'))
		BEGIN
	    if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN40,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
end
	else if((@COLUMN66=1 OR @COLUMN66='True' or @COLUMN66='1'))
 BEGIN
if(@TaxAG=23582)      set @CharAcc1=(1148)
else if(@TaxAG=23583) set @CharAcc1=(1149)
else if(@TaxAG=23584) set @CharAcc1=(1150)
else if(@TaxAG=23585) set @CharAcc1=(1151)
else if(@TaxAG=23586) set @CharAcc1=(1152)
if(@CharAcc1 is not null)
BEGIN
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE,    @COLUMN17 = @TAXRATE,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@CharAcc1,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@COLUMN04)
END
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN40,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
END
END
		--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(Select COLUMN01 from PUTABLE005 where COLUMN02=@COLUMN02)
END
 

IF @Direction = 'Select'
BEGIN
select * from PUTABLE005
END 

 

IF @Direction = 'Update'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN20=(1001)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
declare @PrevDiscount decimal(18,2), @PrevBILL NVARCHAR(250), @Location NVARCHAR(250), @linelocation NVARCHAR(250), @Project NVARCHAR(250),@DecimalPositions int=null,@DecimalQty decimal(18,7), @ProjectID NVARCHAR(250)
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
set @PrevBILL=(select COLUMN04 from PUTABLE005 where COLUMN02=@COLUMN02)
set @Location=(select COLUMN29 from PUTABLE005 where COLUMN02=@COLUMN02)
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
set @Project=(select COLUMN26 from PUTABLE005 where COLUMN02=@COLUMN02)
set @PrevDiscount=(select COLUMN41 from PUTABLE005 where COLUMN02=@COLUMN02)
set @ProjectID=(select COLUMN26 from PUTABLE005 where COLUMN02=@COLUMN02)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Header Values are Intiated for SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull(  @COLUMN33,'')+','+isnull(  @COLUMN34,'')+','+isnull( @COLUMN35,'')+','+isnull(  @COLUMN36,'')+','+isnull(  @COLUMN37,'')+','+isnull(  @COLUMN38,'')+','+isnull(  @COLUMN39,'')+','+isnull(  @COLUMN40,'')+','+isnull(  @COLUMN41,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE PUTABLE005 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,      
   COLUMN17=@COLUMN17,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,    COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,    COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28, 
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,    COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,
   COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,    COLUMN36=@COLUMN36,    COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN39=@COLUMN39,    COLUMN40=@COLUMN40,    COLUMN41=@COLUMN41,    COLUMN42=@COLUMN42,    COLUMN43=@COLUMN43,   
   COLUMN44=@COLUMN44,    COLUMN45=@COLUMN45,    COLUMN46=@COLUMN46,    COLUMN55=@COLUMN55,  
   COLUMN56=@COLUMN56,    COLUMN57=@COLUMN57,    COLUMN58=@COLUMN58,    COLUMN59=@COLUMN59,    COLUMN60=@COLUMN60,
   COLUMN61=@COLUMN61,    COLUMN62=@COLUMN62,    COLUMN63=@COLUMN63,    COLUMN64=@COLUMN64,     COLUMN66=@COLUMN66,
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  
   COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  
   COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  
   COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  
   COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  
   COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  
   COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  
   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   --EMPHCS1584	Shipping tab calculations in bill  BY RAJ.Jr
    delete from FITABLE036 WHERE COLUMN09=@PrevBILL AND COLUMN03='BILL' and COLUMN10=1146  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
    delete from FITABLE036 WHERE COLUMN09=@PrevBILL AND COLUMN03='BILL' and COLUMN10=1075  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	DELETE FROM FITABLE034 where COLUMN09=@PrevBILL AND COLUMN03='BILL' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	  
   declare @intID INT
  set @intID=( select COLUMN01 from PUTABLE005  WHERE COLUMN02 = @COLUMN02)
	DELETE FROM FITABLE064 where COLUMN05 = @intID AND (COLUMN03='BILL' or COLUMN03='TDS Payable') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	SET @Postage=(isnull(cast(@COLUMN34 as decimal(18,2)),0)+isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN40 as decimal(18,2)),0))
if(@Postage!=0)
		BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22344', @COLUMN11 = '1075',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @Postage,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @newID=(select MAX(COLUMN02) from FITABLE036)+1
		--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Expense  Values are Intiated for FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@newID as nvarchar(250)) +','+   'INVOICE'+','+ cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+  isnull(@COLUMN12,'')+','+  isnull(@COLUMN05,'')+','+  cast(isnull(@Postage,'') as nvarchar(250))+','+  isnull(@COLUMN04,'')+',1075,'+isnull(@COLUMNA02,'')+ ','+isnull(@COLUMNA03,'')+ ' ')
					)
			insert into FITABLE036 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,    COLUMN07,  COLUMN09, COLUMN10,   COLUMN11,  
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   ISNULL(@newID,10001),  'BILL',  @COLUMN08,  @intID,  @COLUMN05,  @Postage,  @COLUMN04,1075, @COLUMN26,
	   @COLUMNA02, @COLUMNA03
	)
		--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Expense Intiated Values are Created in FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''))					
		END
   --EMPHCS1410 LOGS CREATION BY SRINIVAS
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Header Intiated Values are Updated in SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   set @COLUMN29 =isnull(@Location,0)
   --declare @intID INT
  set @intID=( select COLUMN01 from PUTABLE005  WHERE COLUMN02 = @COLUMN02)

   DELETE FROM FITABLE026 where COLUMN04='TDS Payable' AND COLUMN09=@PrevBILL and COLUMN05=@intID  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
if(@COLUMN28 !=''  and @COLUMN28 is not null and cast(isnull(@COLUMN28,0) as decimal(18,2))>0  )
BEGIN
SET @IDCl=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE026)+1
set @TYPEIDCl=(SELECT COLUMN04 FROM PUTABLE005 WHERE COLUMN01=@intID)
  --EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liability Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(ISNULL(@IDCl,10001)as nvarchar(250)) +','+cast( ISNULL(@COLUMN08,'') as nvarchar(250))+','+  'TDS Payable'+','+  cast(isnull(@NUMBERCl,'') as nvarchar(20))+','+ cast(ISNULL(@COLUMN05,'')as nvarchar(250))+','+ cast( ISNULL(@COLUMN12,'')as nvarchar(250))+','+ cast( ISNULL(@COLUMN27,'')as nvarchar(250))+','+ cast( ISNULL(@TYPEIDCl,'')as nvarchar(250))+','+  CAST(ISNULL(@COLUMN28,0) AS nvarchar(20))+','+  ISNULL(@COLUMN27,'')+','+  'TDS Payable'+','+  ISNULL(@COLUMNA02,'')+','+
  cast( ISNULL(@COLUMNA03,'')as nvarchar(250)) +  '  ')
   )
   
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = '1101',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @COLUMN28,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE026 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07, COLUMN08,   COLUMN09,COLUMN11,COLUMN17,COLUMN18,COLUMN19,
		COLUMNA02, COLUMNA03 
	)
	values
	(  ISNULL(@IDCl,10001),@COLUMN08,'TDS Payable', @intID,@COLUMN05,@COLUMN12,@COLUMN27,@TYPEIDCl,CAST(ISNULL(@COLUMN28,0) AS DECIMAL(18,2)),'TDS Payable',@COLUMN27,@COLUMN26,
	   @COLUMNA02, @COLUMNA03
	)
   --EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liability Intiated Values are Created in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
END
DELETE FROM FITABLE026 where COLUMN09=@PrevBILL AND COLUMN04='BILL' AND COLUMNA03=@COLUMNA03
DELETE FROM FITABLE034 where COLUMN09=@PrevBILL AND COLUMN03='BILL' AND COLUMNA03=@COLUMNA03
if((CAST(ISNULL(@COLUMN34,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN34,0)
SET  @Tax= ISNULL(@COLUMN64,0)
set @cnt=0 set @vochercnt=1
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt < @vochercnt
BEGIN
	SET @Tax = ISNULL(@COLUMN64,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt!=0)
			BEGIN
			set @vochercnt=(@vochercnt+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt=0
	end
	if(@COLUMN64>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
if((@COLUMN66=0 OR @COLUMN66='False' or @COLUMN66='0'))
	--    BEGIN
	--    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
	--		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	    	
	--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
	--		values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	--END
	BEGIN
	 if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN34,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
else if((@COLUMN66=1 OR @COLUMN66='True' or @COLUMN66='1'))
 BEGIN
if(@TaxAG=23582)      set @CharAcc1=(1148)
else if(@TaxAG=23583) set @CharAcc1=(1149)
else if(@TaxAG=23584) set @CharAcc1=(1150)
else if(@TaxAG=23585) set @CharAcc1=(1151)
else if(@TaxAG=23586) set @CharAcc1=(1152)
if(@CharAcc1 is not null)
BEGIN
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE,    @COLUMN17 = @TAXRATE,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@CharAcc1,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@COLUMN04)
END
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN34,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
END
END
END
END
if((CAST(ISNULL(@COLUMN35,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN35,0)
SET  @Tax= ISNULL(@COLUMN60,0)
set @cnt1=0 set @vochercnt1=1
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt1 < @vochercnt1
BEGIN
	SET @Tax = ISNULL(@COLUMN60,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt1=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt1!=0)
			BEGIN
			set @vochercnt1=(@vochercnt1+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt1=0
	end
	if(@COLUMN60>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@COLUMN66=0 OR @COLUMN66='False' or @COLUMN66='0'))
		BEGIN
	   if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN35,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
else if((@COLUMN66=1 OR @COLUMN66='True' or @COLUMN66='1'))
 BEGIN
if(@TaxAG=23582)      set @CharAcc1=(1148)
else if(@TaxAG=23583) set @CharAcc1=(1149)
else if(@TaxAG=23584) set @CharAcc1=(1150)
else if(@TaxAG=23585) set @CharAcc1=(1151)
else if(@TaxAG=23586) set @CharAcc1=(1152)
if(@CharAcc1 is not null)
BEGIN
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE,    @COLUMN17 = @TAXRATE,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@CharAcc1,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@COLUMN04)
END
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN35,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
END	
	END

if((CAST(ISNULL(@COLUMN40,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN40,0)
SET  @Tax= ISNULL(@COLUMN61,0)
set @cnt2=0 set @vochercnt2=1
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt2 < @vochercnt2
BEGIN
	SET @Tax = ISNULL(@COLUMN61,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt2=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt2!=0)
			BEGIN
			set @vochercnt2=(@vochercnt2+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt2=0
	end
	if(@COLUMN61>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@COLUMN66=0 OR @COLUMN66='False' or @COLUMN66='0'))
		BEGIN
	   if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN40,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
end
else if((@COLUMN66=1 OR @COLUMN66='True' or @COLUMN66='1'))
 BEGIN
if(@TaxAG=23582)      set @CharAcc1=(1148)
else if(@TaxAG=23583) set @CharAcc1=(1149)
else if(@TaxAG=23584) set @CharAcc1=(1150)
else if(@TaxAG=23585) set @CharAcc1=(1151)
else if(@TaxAG=23586) set @CharAcc1=(1152)
if(@CharAcc1 is not null)
BEGIN
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE,    @COLUMN17 = @TAXRATE,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
values(@Vid,@COLUMN08,'BILL',@internalid,@COLUMN05,@COLUMN12,@CharAcc1,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@COLUMN04)
END
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ldisamt=(isnull(@COLUMN40,0))
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   --set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@COLUMN08,@COLUMN12,@COLUMN05,@ltaxamt,0,@COLUMN04,@CharAcc1,@COLUMNA02,@COLUMNA03)		
end
END
END
END
   --EMPHCS1410 LOGS CREATION BY SRINIVAS
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Values are Updated in FITABLE026 Table By '+isnull(@COLUMN28,'')+' for '+isnull(@COLUMN04,'')+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
	IF(CAST(ISNULL(@COLUMN59,0)AS DECIMAL(18,2))!=0)
	BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22408', @COLUMN11 = '1146',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN59,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	SET @newID=((SELECT MAX(ISNULL(COLUMN02 ,1000)) FROM FITABLE036)+1)
	insert into FITABLE036(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10, COLUMN11,	COLUMNA02, COLUMNA03)
	values(@newID,'BILL',@COLUMN08,@intID,@COLUMN05,ISNULL(@COLUMN59,0),0,@COLUMN04,1146,@COLUMN26,@COLUMNA02, @COLUMNA03)
	END
   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
declare @Billno nvarchar(250)=null,@Billlineref nvarchar(250)=null,@uomselection nvarchar(250)=null
	set @ReverseCharged = (select isnull(COLUMN58,0) from PUTABLE005 where COLUMN01=@intID)
if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') AND (@COLUMN66=0 or @COLUMN66='False' or @COLUMN66='0'))
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN14,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2)))
end
else if((@COLUMN66=1 or @COLUMN66='True' or @COLUMN66='1') AND (@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0'))
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN14,0)as decimal(18,2))-cast(ISNULL(@COLUMN63,0) as decimal(18,2)))
end
else if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') AND (@COLUMN66=1 or @COLUMN66='True' or @COLUMN66='1'))
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN14,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2))-cast(ISNULL(@COLUMN63,0) as decimal(18,2)))
end
	else
	begin
	set @TOTALAMT=(ISNULL(@COLUMN14,0))
	end
	--set @Totamt=(select COLUMN12 from PUTABLE016  where COLUMN09=@PrevBILL and COLUMNA02=@COLUMNA02)
	DELETE FROM PUTABLE016  where COLUMN05=@intID  and COLUMN06='BILL'  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @COLUMN04, @COLUMN05 = @intID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = @COLUMN30,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN26,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @newID=cast((select MAX(ISNULL(COLUMN02,999)) from PUTABLE016) as int)+1
	insert into PUTABLE016	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14, COLUMNA02,COLUMN18, COLUMNA03,COLUMN20)
			        values(@newID,2000,@COLUMN08,@intID,'BILL',@COLUMN05,@COLUMN04,(select COLUMN04 from CONTABLE025 where COLUMN02=16),@COLUMN11,@TOTALAMT,null,@TOTALAMT ,@COLUMNA02,@COLUMN12, @COLUMNA03,@COLUMN26) 
	--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Payable Updated in PUTABLE018 Table by '+cast(isnull(@COLUMN14,0) as nvarchar(20))+' for '+isnull(@COLUMN04,'')+' '+'' + CHAR(13)+CHAR(10) + ''))
	declare @AMOUNT nvarchar(250), @AMOUNT1 nvarchar(250), @BAL nvarchar(250)
	set @number=(select column01 from PUTABLE005 where column02=@COLUMN02)      	
 --EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
	 UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN09=@PrevBILL AND COLUMN03='BILL' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN10 not in(1075,1146)
	 --UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1122 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	 DECLARE @MaxRownum INT,@PID1 nvarchar(250),@id nvarchar(250),@ItemQty nvarchar(250),@Price nvarchar(250),@TranID nvarchar(250),@venID nvarchar(250)
      --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
      DECLARE @Initialrow INT=1,@newID1 INT,@PBillQty nvarchar(250),@PID nvarchar(250),@RemainQty nvarchar(250),@RefIR nvarchar(250),@uom nvarchar(250)
      --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	  declare	@chk bit,@lotno nvarchar(250),@RefId nvarchar(250),@upcno nvarchar(250) = null
	 set @TranID=(@COLUMN04)
	 set @venID=(@COLUMN05)
      set @COLUMN13=(SELECT COLUMN01 from PUTABLE005 where COLUMN02 in(@COLUMN02))
	  delete from PUTABLE017 where COLUMN05=@COLUMN13 AND COLUMN06='BILL' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	  delete from PUTABLE011 where COLUMN19=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	  set @PID1=(select DATALENGTH(COLUMN06)  from  PUTABLE005 where COLUMN02 in(@COLUMN02));
	  set @PID=(select COLUMN06  from  PUTABLE005 where COLUMN02 in(@COLUMN02));
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE006 where COLUMN13 in(@COLUMN13) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE006 where COLUMN13 in(@COLUMN13) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN  
		 set @COLUMN04=(select COLUMN04  from  PUTABLE006 where COLUMN02=@id)
		 if exists(SELECT COLUMN04 FROM MATABLE021 where COLUMN12 in(@id) and COLUMN05 in(@COLUMN04) and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		 begin
		 UPDATE MATABLE021 SET COLUMNA13=1 where COLUMN12 in(@id) and COLUMN05 in(@COLUMN04) and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
		 end
		 set @RefIR=(select COLUMN03  from  PUTABLE006 where COLUMN02=@id)
		 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
		 set @RefId=(select COLUMN34  from  PUTABLE006 where COLUMN02=@id)
		 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		 set @lotno=(select isnull(column27,0)  from  PUTABLE006 where COLUMN02=@id)
		 set @upcno=(select COLUMN06 from  PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
		 --EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
		 set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
         	 set @linelocation=(select cast(COLUMN35 as nvarchar(250)) from PUTABLE006 where COLUMN02=@id)
		 set @COLUMN29=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
		 set @ItemQty=(select isnull(COLUMN09,0) from PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
         set @ItemQty=round(cast(@ItemQty as decimal(18,7)),@DecimalPositions)
         set @Price=(select isnull(COLUMN11,0) from PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
         --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	 set @uom=(select isnull(COLUMN19,0) from PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
		  --Total Qty Updation
		  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			if(isnull(@RefId,0)>0)
			   begin
			   set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where COLUMNA13=0 and COLUMN02=@RefId)
			   end
			   else 
			   begin
			   set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@RefIR and COLUMN13 in(@COLUMN13))
			   end
			set @COLUMN08=(CAST(@COLUMN08 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))
			set @RemainQty=(select isnull(COLUMN10,0) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@RefIR and COLUMN02=@id)
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			declare @TotalBillQty decimal(18,2),@TotalBillAmt decimal(18,2),@BillAvgPrice decimal(18,2)
			set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			--EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
			where h.COLUMN08>=@FiscalYearStartDt and l.COLUMN06=@upcno AND l.COLUMN04=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)= CAST(@ProjectID AS INT)
			union all
			select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
			union all
			select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			where h.COLUMN05>=@FiscalYearStartDt and l.COLUMN04=@upcno AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)= CAST(@ProjectID AS INT) )T)
			set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN08>=@FiscalYearStartDt  and l.COLUMN06=@upcno AND l.COLUMN04=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)= CAST(@ProjectID AS INT)
			union all
			select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
			union all
			select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			where h.COLUMN05>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN04 AND l.COLUMN04=@upcno AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)= CAST(@ProjectID AS INT))T)
			if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt AND l.COLUMN03=@COLUMN04 and l.COLUMN05=@upcno AND isnull(h.COLUMN13,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)= CAST(@ProjectID AS INT))
			begin
			set @TotalBillQty=cast((@TotalBillQty+cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt  and l.COLUMN05=@upcno AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)= CAST(@ProjectID AS INT))as decimal(18,2)))as decimal(18,2))
			set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@upcno  AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)= CAST(@ProjectID AS INT))as decimal(18,2)))as decimal(18,2))
			end
			if(isnull(@TotalBillQty,0)=0)
                        begin
			set @BillAvgPrice=(0)
			end
			else
			begin
			set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/cast(isnull(@TotalBillQty,0) as decimal(18,2)))
			end
	   update PUTABLE006 set COLUMN08=@COLUMN08,COLUMN09=0,COLUMN10=(CAST(@RemainQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@id
	   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
           if(isnull(@PID1,0)=0)
           begin
	   --EMPHCS1167 Service items calculation by srinivas 22/09/2015
			set @chk=(select column48 from matable007 where column02=@COLUMN04)
			set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
			set @Billno=(select COLUMN04 from PUTABLE005 where COLUMN02=@COLUMN02)
			set @Billlineref=(select COLUMN01 from PUTABLE006 where COLUMN02=@id)
			declare @AvgPrice decimal(18,2), @FinalAvgPrice decimal(18,2)
			if(@uomselection=1 or @uomselection='1' or @uomselection='True')
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19 in (SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Bill' and COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN15 and COLUMNA03=@COLUMNA03) AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
			end
			set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
           declare @Qty_On_Hand decimal(18,2),@Qty_On_Hand1 decimal(18,2),@Qty_Order decimal(18,2),@Qty_Avl decimal(18,2),@Qty_Cmtd decimal(18,2)
	   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	   --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
          if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null  or @uomselection='False') and @chk=1)
			begin
		   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
		   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	    set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
		--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
        if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
			--if(@Qty_On_Hand1>=0)
            begin
	    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	    --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
           set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
           set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,2)));
           set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,2)))
	    --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
           set @Qty_Avl =  cast(isnull(@Qty_Cmtd,0) as decimal(18,2));
	   --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
		SET @PRATE=(ISNULL(@BillAvgPrice,0))
		IF(@PRATE=0)BEGIN SET @PRATE=(@Price)END
           --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	       UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN08=@Qty_On_Hand  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
		   --EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Inventory Updated in FITABLE010 Table by '+cast(isnull(@Qty_On_Hand,0) as nvarchar(20))+' for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
			set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,2)));
			 UPDATE FITABLE010 SET COLUMN12=cast(cast(@Qty_On_Hand as decimal(18,2))*cast(@BillAvgPrice as decimal(18,2)
) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
				--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Inventory Updated in FITABLE010 Table by 0 for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
				end
				else
				begin
				--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and Column24=@upcno
				--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Inventory Updated in FITABLE010 Table by '+cast(isnull(@Qty_On_Hand,0) as nvarchar(20))+' for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
				end
         --EMPHCS791 rajasekhar reddy patakota 28/7/2015 Inventory Asset Calculation In Bill With out po in edit
		 --   set @newID1=cast((select ISNULL(MAX(COLUMN02),1000) from PUTABLE017) as int)+1
			--insert into PUTABLE017 
			--( 
			--   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN12,COLUMN13,
			--   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
			--   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
			--   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
			--)
			--values
			--(  
			--   @newID1,'1000', GETDATE() ,@COLUMN13,'BILL', @COLUMN05,'1000',0,0,@COLUMN15,
			--   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
			--   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
			--   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
			--   @COLUMND08, @COLUMND09, @COLUMND10
			--)
			--EMPHCS791 rajasekhar reddy patakota 28/7/2015 Inventory Asset Calculation In Bill With out po in edit
			--insert into PUTABLE013 
			--(
			--COLUMN02,COLUMN03,COLUMN04,COLUMN06,COLUMN08,COLUMN09,COLUMN10,COLUMN14,COLUMN15,COLUMNA02,COLUMNA03
			--)
			--values
			--(@COLUMN13,  'BILL',  @TranID,  @venID,   0,0,0,      @COLUMN04,  GETDATE(),@COLUMNA02,@COLUMNA03)
		    end
		    --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
           end
		   else
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		   exec usp_PUR_TP_InventoryUOM  @Billno,@Billlineref,@COLUMN04,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@COLUMN29,@lotno
		   --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
		   update FITABLE038 set COLUMNA13=1 where COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		   end
		   else
		   begin
		    
			 --Billed Qty Updation
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			   if(isnull(@RefId,0)>0)
			   begin
			   SET @PBillQty=(SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN02=@RefId)
			   UPDATE PUTABLE002 SET   COLUMN13=(CAST(@PBillQty AS  decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMNA13=0 and COLUMN02=@RefId
			   end
			   else 
			   begin
			    SET @PBillQty=isnull((SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) and COLUMN03=@COLUMN04  and isnull(COLUMN26,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0)),0)
				UPDATE PUTABLE002 SET   COLUMN13=(CAST(@PBillQty AS  decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@COLUMN04 and isnull(COLUMN26,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0) and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02=  @PID)
			   end
				UPDATE PUTABLE013 SET COLUMN10=(CAST(@PBillQty AS  decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))   WHERE COLUMN14=@COLUMN04 and COLUMN02 = (select COLUMN01 from PUTABLE001 where COLUMN02= @PID)
				
		   end
		   --EMPHCS791 rajasekhar reddy patakota 28/7/2015 Inventory Asset Calculation In Bill With out po in edit
			--insert into PUTABLE011 
			--( 
			--   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05, COLUMN07,COLUMN08,COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,   COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,COLUMNA02,COLUMNA03
			--)
			--values
			--(  
			--   @COLUMN13,  'Bill',  @TranID,   GETDATE()  ,0 ,0 , '3','3', 0,  0,@COLUMN13,  @COLUMN15,  @COLUMN16,  @COLUMN17 ,@COLUMN18,@COLUMNA02,@COLUMNA03
			--)   
			 
		if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
		begin
		EXEC usp_TP_InventoryAssetUpdate @COLUMN13,@Billno,null,@COLUMN04,@Uom,@Location,@lotno,@BillAvgPrice,@COLUMN15,@COLUMNA03
		--EXEC usp_TP_ProdIncomeUpdate @COLUMN13,@Billno,null,@COLUMN04,@COLUMN19,@Location,@lotno,@BillAvgPrice,@COLUMN14,@COLUMNA03
		end
		 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
        CLOSE cur1
		update PUTABLE006 set COLUMNa13=1   where COLUMN13 in(@COLUMN13)
	--EMPHCS1147 Deleted tax amount in bill ,appearing in chart of accounts i.e., not deleting by srinivas 15/09/2015
	--EMPHCS1148 Tax amount not displaying in chart of accounts for invoice by srinivas 15/09/2015
     UPDATE FITABLE026 SET COLUMNA13=1 WHERE COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@number)) AND COLUMN04='Bill' AND COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03
	delete from FITABLE025 where COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@number)) and COLUMN04='BILL' and  COLUMNA03=@COLUMNA03
	delete from FITABLE025 where COLUMN05 in(@number) and COLUMN04='BILL' and COLUMN08=1118 and  COLUMNA03=@COLUMNA03
	delete from FITABLE026 where COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@number)) and COLUMN04='BILL' and  COLUMNA03=@COLUMNA03
	delete from FITABLE036 where COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@number)) and COLUMN03='BILL' and  COLUMNA03=@COLUMNA03 and COLUMN10 not in(1075,1146)
	 --EMPHCS1167 Service items calculation by srinivas 22/09/2015
	 UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN09=@PrevBILL AND COLUMN10=1078 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END

else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE005 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bill Header Deleted in PUTABLE005 Table for '+isnull(cast(@COLUMN02 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
--EMPHCS855 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in bill
    set @COLUMN13=(select column01 from PUTABLE005 where column02=@COLUMN02)
    set @COLUMN04 = (select COLUMN04 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02)
    set @COLUMN05 = (select COLUMN05 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02)
    set @COLUMNA03 = (select COLUMNA03 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02)
    set @COLUMNA02 = (select COLUMNA02 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02)
   set @COLUMN15= (SELECT COLUMN15 FROM PUTABLE005 WHERE COLUMN02=@COLUMN02) 
    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
    set @COLUMN29 = (select isnull(COLUMN29,0) from  PUTABLE005 WHERE COLUMN02 = @COLUMN02)
    --EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
    set @COLUMN29=(case when @COLUMN29='' then 0 when isnull(@COLUMN29,0)=0 then 0  else @COLUMN29 end)
    set @location=(case when @COLUMN29='' then 0 when isnull(@COLUMN29,0)=0 then 0  else @COLUMN29 end)
	set @ProjectID=(select COLUMN26 from PUTABLE005 where COLUMN02=@COLUMN02)
	set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
	select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08='1' order by COLUMN01 desc
    --EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
set @COLUMN26 = (select COLUMN26 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02)
	DELETE FROM FITABLE064 where COLUMN05 = @COLUMN13 AND (COLUMN03='BILL' or COLUMN03='TDS Payable') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
	 UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1078 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	 --EMPHCS1584	Shipping tab calculations in bill  BY RAJ.Jr
	 delete from FITABLE036 WHERE COLUMN09=@COLUMN04 AND COLUMN03='BILL' and COLUMN10=1075  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
     DELETE FROM FITABLE034 where COLUMN09=@COLUMN04 AND COLUMN03='BILL' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	 DELETE FROM FITABLE026 where COLUMN09=@COLUMN04 AND COLUMN04='BILL'  AND COLUMNA03=@COLUMNA03
  set @intID=( select COLUMN01 from PUTABLE005  WHERE COLUMN02 = @COLUMN02)

   update FITABLE026  SET COLUMNA13=@COLUMNA13 where COLUMN09=@COLUMN04 and COLUMN05=@intID
   --EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Current liabilities Deleted in FITABLE026 Table for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
	--update PUTABLE016 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN13 and COLUMN09=@COLUMN04  and COLUMNA03=@COLUMNA03
	DELETE FROM PUTABLE016  where COLUMN05=@COLUMN13  and COLUMN06='BILL'  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
   DELETE FROM FITABLE026 where COLUMN04='TDS Payable' AND COLUMN05=@COLUMN13  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	 --EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Payable Deleted in PUTABLE016 Table for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
	 --EMPHCS900 rajasekhar reddy patakota 10/8/2015  After deleting bill , the taxes information is not getting deleted.
     UPDATE FITABLE026 SET COLUMNA13=1 WHERE COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@COLUMN13)) AND COLUMN04='Bill' AND COLUMN09=@COLUMN04 AND  COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03
	 		
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Current liabilities Deleted in FITABLE026 Table for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
		 delete from FITABLE025 where COLUMN05=@COLUMN13 and COLUMN04='BILL' and COLUMN08=1118 and  COLUMNA03=@COLUMNA03
  set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Purchase Discount Income Deleted in FITABLE025 Table for '+isnull(cast(@COLUMN13 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))				
	 
		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
	UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN09=@COLUMN04 AND COLUMN03='BILL' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	 --UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1122 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03

		 set @TranID=(@COLUMN04)
		 set @venID=(@COLUMN05)
	  set @Initialrow =(1)
	  set @PID1=(select DATALENGTH(COLUMN06)  from  PUTABLE005 where COLUMN02 in(@COLUMN02));
	  set @PID=(select COLUMN06  from  PUTABLE005 where COLUMN02 in(@COLUMN02));
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE006 where COLUMN13 in(@COLUMN13) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE006 where COLUMN13 in(@COLUMN13) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN  
		 set @COLUMN04=(select COLUMN04  from  PUTABLE006 where COLUMN02=@id)
		 if exists(SELECT COLUMN04 FROM MATABLE021 where COLUMN12 in(@id) and COLUMN05 in(@COLUMN04) and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		 begin
		 UPDATE MATABLE021 SET COLUMNA13=1 where COLUMN12 in(@id) and COLUMN05 in(@COLUMN04) and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
		 end
		 set @RefIR=(select COLUMN03  from  PUTABLE006 where COLUMN02=@id)
		 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
		 set @RefId=(select COLUMN34  from  PUTABLE006 where COLUMN02=@id)
		 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		 set @lotno=(select isnull(column27,0)  from  PUTABLE006 where COLUMN02=@id)
		 --EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
	         set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
                 set @linelocation=(select cast(COLUMN35 as nvarchar(250)) from PUTABLE006 where COLUMN02=@id)
		 set @COLUMN29=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
		 set @ItemQty=(select isnull(COLUMN09,0) from PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
         set @ItemQty=round(cast(@ItemQty as decimal(18,7)),@DecimalPositions)
         set @Price=(select isnull(COLUMN11,0) from PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
		 set @upcno=(select COLUMN06 from  PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
         --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	 set @uom=(select isnull(COLUMN19,0) from PUTABLE006 where COLUMN02=@id and COLUMN04=@COLUMN04)
		  --Total Qty Updation
		  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			if(isnull(@RefId,0)>0)
			   begin
			   set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where COLUMNA13=0 and COLUMN02=@RefId)
			   end
			   else 
			   begin
			   set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@RefIR and COLUMN02 in(@id))
			   end
			set @COLUMN08=(round(CAST(@COLUMN08 AS decimal(18,7)),@DecimalPositions)- round(CAST(@ItemQty AS decimal(18,7)),@DecimalPositions))
			set @RemainQty=(select isnull(COLUMN10,0) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@RefIR and COLUMN02=@id)
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
		    set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
		        --EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
			where h.COLUMN08>=@FiscalYearStartDt and l.COLUMN06=@upcno AND l.COLUMN04=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT)
			union all
			select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
			union all
			select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			where h.COLUMN05>=@FiscalYearStartDt and l.COLUMN04=@upcno AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT))T)
			set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0			    
			where h.COLUMN08>=@FiscalYearStartDt and l.COLUMN06=@upcno AND l.COLUMN04=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT)
			union all
			select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
			union all
			select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			where h.COLUMN05>=@FiscalYearStartDt and l.COLUMN04=@upcno  AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@uom,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT))T)
			if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@upcno AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT)) 
			begin
			set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@upcno AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))as decimal(18,7)),@DecimalPositions))as decimal(18,7)),@DecimalPositions)
			set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt  and l.COLUMN05=@upcno AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN15,0) AND isnull(l.COLUMN17,0)=isnull(@uom,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))as decimal(18,2)))as decimal(18,2))
			end
			if(isnull(@TotalBillQty,0)=0)
            		begin
			set @BillAvgPrice=(0)
			end
			else
			begin
			set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),@DecimalPositions))
			end
	   update PUTABLE006 set COLUMN08=@COLUMN08,COLUMN09=0,COLUMN10=(round(CAST(@RemainQty AS decimal(18,7)),@DecimalPositions)+ round(CAST(@ItemQty AS decimal(18,7)),@DecimalPositions)) where COLUMN02=@id
			
			set @COLUMN15=(select column15 from PUTABLE005 where column02=@COLUMN02)
			--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
           if(isnull(@PID1,0)=0)
           begin
		   set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
			set @Billno=(select COLUMN04 from PUTABLE005 where COLUMN02=@COLUMN02)
			set @Billlineref=(select COLUMN01 from PUTABLE006 where COLUMN02=@id)
			--EMPHCS1167 Service items calculation by srinivas 22/09/2015
			set @chk=(select column48 from matable007 where column02=@COLUMN04)
			if(@uomselection=1 or @uomselection='1' or @uomselection='True')
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Bill' and COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN15 and COLUMNA03=@COLUMNA03 and Column24=@upcno))
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
			end
			set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
			 --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
			if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null  or @uomselection='False') and @chk=1)
			begin
            --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	    --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	    set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
		--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
      if exists(select COLUMN04 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)
			--if(@Qty_On_Hand1>=0)
            begin
	    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	    --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
           set @Qty_On_Hand=( round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,7)),@DecimalPositions)- round(cast(@ItemQty as decimal(18,7)),@DecimalPositions));
           set @Qty_Cmtd=(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)- round(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,7)),@DecimalPositions));
           set @Qty_Order = (round(cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno) as decimal(18,7)),@DecimalPositions))
           if(@Qty_Order>0 or @Qty_Order>round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions))
           Begin
	   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
           set @Qty_Order = (round(cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and Column24=@upcno)as decimal(18,7)),@DecimalPositions)- round(cast(@ItemQty as decimal(18,7)),@DecimalPositions));
           end
           else
           begin
           set @Qty_Order = 0;
           end
           set @Qty_Avl =  round(cast(isnull(@Qty_Cmtd,0) as decimal(18,7)),@DecimalPositions);
	   --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
		SET @PRATE=(ISNULL(@BillAvgPrice,0))
		IF(@PRATE=0)BEGIN SET @PRATE=(@Price)END
           --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	   UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN08=@Qty_On_Hand  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
	    --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
	    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	    --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		    set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,7)),@DecimalPositions));
			 UPDATE FITABLE010 SET COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)*cast(@BillAvgPrice as decimal(18,2)
) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
				if(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
				--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Inventory Deleted in FITABLE010 Table for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno)as decimal(18,2))/round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN29 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@upcno
				--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Inventory Deleted in FITABLE010 Table for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
				end
		   --UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
           --update PUTABLE017 set COLUMNA13=@COLUMNA13  where COLUMN05=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		   --update PUTABLE011 set COLUMNA13=@COLUMNA13  where COLUMN19=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		   delete from PUTABLE017 where COLUMN05=@COLUMN13 and COLUMN06='BILL' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		   delete from PUTABLE011 where COLUMN02=@COLUMN13 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	  
		   end
		   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
           end
		   else
		   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		   exec usp_PUR_TP_InventoryUOM  @Billno,@Billlineref,@COLUMN04,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@COLUMN29,@lotno
		   --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
		   update FITABLE038 set COLUMNA13=1 where COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		   end
		   else
		   begin
		    
			 --Billed Qty Updation
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefId,0)>0)
			   begin
			   SET @PBillQty=(SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN02=@RefId)
			   UPDATE PUTABLE002 SET   COLUMN13=(round(CAST(@PBillQty AS  decimal(18,7)),@DecimalPositions)- round(CAST(@ItemQty AS decimal(18,7)),@DecimalPositions)) where COLUMNA13=0 and COLUMN02=@RefId
			   end
			   else 
			   begin
			    SET @PBillQty=isnull((SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) and COLUMN03=@COLUMN04 and isnull(COLUMN26,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0)),0)
				UPDATE PUTABLE002 SET   COLUMN13=(round(CAST(@PBillQty AS  decimal(18,7)),@DecimalPositions)- round(CAST(@ItemQty AS decimal(18,7)),@DecimalPositions)) where COLUMN03=@COLUMN04 and isnull(COLUMN26,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0) and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02=  @PID)
			   end
				--EMPHCS855 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in bill
				UPDATE PUTABLE013 SET COLUMN10=(round(CAST(@PBillQty AS  decimal(18,7)),@DecimalPositions)- round(CAST(@ItemQty AS decimal(18,7)),@DecimalPositions))   WHERE COLUMN14=@COLUMN04 and COLUMN04 = (select COLUMN04 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				--delete from PUTABLE013  WHERE COLUMN14=@COLUMN04 and COLUMN04 = (select COLUMN04 from  PUTABLE005 WHERE COLUMN02 = @COLUMN02) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				--EMPHCS855 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in bill
				declare @billedqty decimal(18,2),@receiptqty decimal(18,2),@BillQty decimal(18,2),@IRQty decimal(18,2),@IQty decimal(18,2),@Result nvarchar(250)
				SET @BillQty=isnull((SELECT sum(COLUMN13) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID)),0)
				SET @IRQty=isnull((SELECT sum(COLUMN12) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID)),0)
				SET @IQty=isnull((SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) ),0)
				--EMPHCS855 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in bill
				IF(@IRQty=(@IQty))
				begin
				if(@IRQty=@BillQty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
				end
				else if(@BillQty=0)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=9)
				end
				else
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=41)
				end
				end
				ELSE
				BEGIN
				if(@IQty=@IRQty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=41)
				end
				else if(@BillQty=0)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=07)
				end
				else
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=06)
				end
				end
				UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=13) where COLUMN01=@COLUMN13
				UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE005 where COLUMN01= @COLUMN13))
				--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
				set @COLUMN03=(SELECT MAX(COLUMN03) from PUTABLE006 where COLUMN13 in(@COLUMN13) and  COLUMN04=@COLUMN04 and COLUMNA13=0)
				set @billedqty=(select sum(isnull(COLUMN09,0)) from PUTABLE006 where COLUMN03=@COLUMN03 and COLUMNA13=0)
				set @receiptqty=(select sum(isnull(COLUMN08,0)) from PUTABLE004 where columna13=0 and  COLUMN12 in(select COLUMN01 from  PUTABLE003 where COLUMN02 in(@COLUMN03)))
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				if(round(cast(@billedqty as decimal(18,7)),@DecimalPositions)=0.00)
				begin
				--EMPHCS890 rajasekhar reddy patakota 8/8/2015 After deletion of bill , IR status also should change to "OPEN" or "Partially billed" based on the scenario
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=05)
				UPDATE PUTABLE003 set COLUMN18=(@Result) where COLUMN02=@COLUMN03
				end
				else if(@billedqty=@receiptqty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
				UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=83) where COLUMN02=@COLUMN03
				end
				else if(@billedqty<@receiptqty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
				UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=82) where COLUMN02=@COLUMN03
				end
		   end
		   --EMPHCS855 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in bill
		   UPDATE PUTABLE006 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @id
		   delete from FITABLE025 where COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@COLUMN13)) and COLUMN04='BILL' and  COLUMNA03=@COLUMNA03
		   delete from FITABLE025 where COLUMN05 in(@COLUMN13) and COLUMN04='BILL' and COLUMN08=1118 and  COLUMNA03=@COLUMNA03
		   delete from FITABLE026 where COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@COLUMN13)) and COLUMN04='BILL' and  COLUMNA03=@COLUMNA03
		   delete from FITABLE036 where COLUMN05 in(select COLUMN01 from PUTABLE006 where COLUMN13 in(@COLUMN13)) and COLUMN03='BILL' and  COLUMNA03=@COLUMNA03
	
		if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
		begin
		EXEC usp_TP_InventoryAssetUpdate @COLUMN13,@Billno,null,@COLUMN04,@Uom,@Location,@lotno,@BillAvgPrice,@COLUMN15,@COLUMNA03
		--EXEC usp_TP_ProdIncomeUpdate @COLUMN13,@Billno,null,@COLUMN04,@COLUMN19,@Location,@lotno,@BillAvgPrice,@COLUMN14,@COLUMNA03
		end
		 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end

        CLOSE cur1
		SET @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+' CST DELETED '))
END
    --EMPHCS1410 LOGS CREATION BY SRINIVAS
    exec [CheckDirectory] @tempSTR,'usp_PUR_TP_PUTABLE005.txt',0
end try
begin catch 
			SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
	--EMPHCS1410 LOGS CREATION BY SRINIVAS		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
    exec [CheckDirectory] @tempSTR,'usp_PUR_Execption_TP_PUTABLE005.txt',0
return 0
end catch
end

















GO
