USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PRTABLE003]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PRO_TP_PRTABLE003]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE003_SequenceNo
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 declare @tempSTR nvarchar(max)
   set @tempSTR=('Target1:Stock Transfer Header Inserting Values are '+isnull(@COLUMN02,'')+','+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
insert into PRTABLE003 
(
--EMPHCS1381 rajasekhar reddy 18/11/2015 Adding Date column in Stock transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,   
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
--EMPHCS1381 rajasekhar reddy 18/11/2015 Adding Date column in Stock transfer
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,@COLUMN08, @COLUMN09, @COLUMN10,@COLUMN11,@COLUMN12,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
) 
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application 
set @ReturnValue =(select COLUMN01 from PRTABLE003 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from PRTABLE003

END 

 

ELSE IF @Direction = 'Update'

BEGIN
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 set @tempSTR=('Target1:Header Update:Stock Transfer Header Updating Values are '+isnull(@COLUMN02,'')+','+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+' of  '+isnull(@TabelName,'')+' at ');
    --exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
    --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	declare @HSL nvarchar(250),@HDL nvarchar(250),@HSP nvarchar(250),@HDP nvarchar(250)
	set @HSL=(SELECT isnull(COLUMN09,0) FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	set @HDL=(SELECT isnull(COLUMN10,0) FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	set @HSP=(SELECT isnull(COLUMN11,0) FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	set @HDP=(SELECT isnull(COLUMN12,0) FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	set @HSL=(iif(isnull(@HSL,'')='',0,@HSL))
	set @HDL=(iif(isnull(@HDL,'')='',0,@HDL))
	set @HSP=(iif(isnull(@HSP,'')='',0,@HSP))
	set @HDP=(iif(isnull(@HDP,'')='',0,@HDP))
UPDATE PRTABLE003 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   --EMPHCS1381 rajasekhar reddy 18/11/2015 Adding Date column in Stock transfer
   --EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,COLUMN09=@COLUMN09, COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11, 
        COLUMN12=@COLUMN12, 
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,    COLUMNA07=@COLUMNA07,  --COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
	set @COLUMN08= (SELECT COLUMN01 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
	--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	delete from PUTABLE017 where COLUMN05 in(SELECT COLUMN01 FROM PRTABLE004 where COLUMN08 in(@COLUMN08) and isnull(COLUMNA13,'False')='False') and COLUMN13=@COLUMN05 and isnull(COLUMN15,0)=isnull(@HSL,0) and COLUMNA03=@COLUMNA03 and column06='Stock Transfer'
		delete from PUTABLE017 where COLUMN05 in(SELECT COLUMN01 FROM PRTABLE004 where COLUMN08 in(@COLUMN08) and isnull(COLUMNA13,'False')='False') and COLUMN13=@COLUMN06 and isnull(COLUMN15,0)=isnull(@HDL,0) and COLUMNA03=@COLUMNA03 and column06='Stock Transfer'
 declare @ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@uom int,@lot nvarchar(250),@Project nvarchar(250),@upcno nvarchar(250) = null
 declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@TrackQty  bit,@Price  decimal(18,2),
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
 @Item  int,@MaxRownum  int,@Initialrow int,@id int,@uomselection nvarchar(250)=null ,@stno nvarchar(250)=null 
				set @stno= (SELECT COLUMN04 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PRTABLE004 where COLUMN08 in(@COLUMN08) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PRTABLE004 where COLUMN08 in(@COLUMN08) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PRTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN06,0) from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @Price=(select isnull(COLUMN17,0) from MATABLE007 where COLUMN02=@Item)
			 set @uom=(select isnull(COLUMN13,0) from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @lot=(select isnull(COLUMN14,0) from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @upcno=(select COLUMNB01 from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @lot=(iif(isnull(@lot,'')='','0',@lot))
				--Inventory Updation
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2)));
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno)as decimal(18,2)));
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@Item) 
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @tempSTR=(null)
				set @tempSTR=('Target2:Header Update:At Header Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',3.Item '+isnull(cast(@Item as nvarchar(250)),'')+',4.UOM '+isnull(cast(@uom as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',7.Source Operating Unit '+isnull(cast(@COLUMN05 as nvarchar(250)),'')+',8.Destination Operating Unit '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				--exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				set @tempSTR=(null)
				set @tempSTR=('Target3:Header Update:At Header Stock Transfer Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				--exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				if(@TrackQty=1)
				begin
				if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null  or @uomselection='False')
			    begin
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP AND COLUMN24 =@upcno ) as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL AND COLUMN19=@uom and isnull(COLUMN22,0)=@lot AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand1, COLUMN08=@Qty_On_Hand1 ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno)as decimal(18,2))-((cast(@ItemQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP AND COLUMN24 =@upcno ) as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno
				
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2)));
				
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				 set @tempSTR=(null)
				set @tempSTR=('Target4:Header Update:At Header After Updation Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				--exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@HSL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP AND COLUMN24 =@upcno
				end
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno)as decimal(18,2)));

				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @tempSTR=(null)
				set @tempSTR=('Target5:Header Update:At Header After Update Stock Transfer Destination Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				--exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				if(cast(@Qty_On_Hand1 as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP AND COLUMN24 =@upcno )as decimal(18,2))/cast(@Qty_On_Hand1 as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@HDL and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno
				end
				end
				else
				begin
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				 set @tempSTR=(null)
				set @tempSTR=('Target6:Header Update:At Header Updation of Inventory Values for Multiuom 1.Stock Transfer NO '+isnull(cast(@stno as nvarchar(250)),'')+',2.Item '+isnull(cast(@Item as nvarchar(250)),'')+',3.Item Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',4.UOM '+isnull(cast(@uom as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				--exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				exec usp_PUR_TP_InventoryUOM @stno,@COLUMN08,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',null,null
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@stno and COLUMN06=@Item and COLUMN05=@COLUMN08 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
	update PRTABLE004 set columna13=1 where column08 in(@column08)
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

UPDATE PRTABLE003 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
	set @COLUMN08= (SELECT COLUMN01 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	set @COLUMN05= (SELECT COLUMN05 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	set @COLUMN06= (SELECT COLUMN06 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
	set @COLUMN09= (SELECT COLUMN09 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	set @COLUMN10= (SELECT COLUMN10 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	set @COLUMNA02= (SELECT COLUMNA02 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	set @COLUMNA03= (SELECT COLUMNA03 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02) 
	set @stno= (SELECT COLUMN04 FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	set @COLUMN09=(iif(isnull(@COLUMN09,'')='',0,@COLUMN09))
	set @COLUMN10=(iif(isnull(@COLUMN10,'')='',0,@COLUMN10))
	set @HSP=(SELECT isnull(COLUMN11,0) FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
    set @HDP=(SELECT isnull(COLUMN12,0) FROM PRTABLE003 WHERE COLUMN02=@COLUMN02)
	set @HSP=(iif(isnull(@HSP,'')='',0,@HSP))
	set @HDP=(iif(isnull(@HDP,'')='',0,@HDP))
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
	--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
 	delete from PUTABLE017 where COLUMN05 in(SELECT COLUMN01 FROM PRTABLE004 where COLUMN08 in(@COLUMN08) and isnull(COLUMNA13,'False')='False') and COLUMN13=@COLUMN05 and isnull(COLUMN15,0)=isnull(@COLUMN09,0) and COLUMNA03=@COLUMNA03 and column06='Stock Transfer'
	delete from PUTABLE017 where COLUMN05 in(SELECT COLUMN01 FROM PRTABLE004 where COLUMN08 in(@COLUMN08) and isnull(COLUMNA13,'False')='False') and COLUMN13=@COLUMN06 and isnull(COLUMN15,0)=isnull(@COLUMN10,0) and  COLUMNA03=@COLUMNA03 and column06='Stock Transfer'


set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PRTABLE004 where COLUMN08 in(@COLUMN08) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PRTABLE004 where COLUMN08 in(@COLUMN08) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PRTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN06,0) from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @Price=(select isnull(COLUMN17,0) from MATABLE007 where COLUMN02=@Item)
			 set @uom=(select isnull(COLUMN13,0) from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @lot=(select isnull(COLUMN14,0) from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @upcno=(select COLUMNB01 from PRTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @lot=(iif(isnull(@lot,'')='','0',@lot))
				--Inventory Updation
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2)));
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno)as decimal(18,2)));
				
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@Item)
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @tempSTR=(null)
				set @tempSTR=('Target1:Header Delete:At Header Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',3.Item '+isnull(cast(@Item as nvarchar(250)),'')+',4.UOM '+isnull(cast(@uom as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@ItemQty as nvarchar(250)),',7.Source Operating Unit '+isnull(cast(@COLUMN05 as nvarchar(250)),'')+',8.Destination Operating Unit '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				set @tempSTR=(null)
				set @tempSTR=('Target2:Header Delete:At Header Stock Transfer Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno )as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				
				if(@TrackQty=1)
				begin
				if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null  or @uomselection='False')
			    begin
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP AND COLUMN24 =@upcno) as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP AND COLUMN24 =@upcno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand1, COLUMN08=@Qty_On_Hand1 ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND COLUMN24 =@upcno 
				AND isnull(COLUMN23,0)=@HDP)as decimal(18,2))-((cast(@ItemQty as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno) as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP AND COLUMN24 = @upcno)as decimal(18,2)));
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				 set @tempSTR=(null)
				set @tempSTR=('Target3:Header Delete:At Header After Updation Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@Qty_On_Hand as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot and isnull(COLUMN21,0)=@COLUMN09 AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN05 and isnull(COLUMN21,0)=@COLUMN09 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HSP and COLUMN24=@upcno
				end
				set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno)as decimal(18,2)));
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				set @tempSTR=(null)
				set @tempSTR=('Target4:Header Delete:At Header After Update Stock Transfer Destination Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@Qty_On_Hand1 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
				if(cast(@Qty_On_Hand1 as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP AND COLUMN24 =@upcno )as decimal(18,2))/cast(@Qty_On_Hand1 as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN06 and isnull(COLUMN21,0)=@COLUMN10 and isnull(COLUMN22,0)=@lot AND COLUMN19=@uom AND isnull(COLUMN23,0)=@HDP and COLUMN24=@upcno
				end
				end
				else
				begin
				--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
				 set @tempSTR=(null)
				set @tempSTR=('Target5:Header Delete:At Header Updation of Inventory Values for Multiuom 1.Stock Transfer NO '+isnull(cast(@stno as nvarchar(250)),'')+',2.Item '+isnull(cast(@Item as nvarchar(250)),'')+',3.Item Quantity '+isnull(cast(@ItemQty as nvarchar(250)),'')+',4.UOM '+isnull(cast(@uom as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				exec usp_PUR_TP_InventoryUOM @stno,@COLUMN08,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',null,null
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@stno and COLUMN06=@Item and COLUMN05=@COLUMN08 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				end
				--Inventory Asset table Updations
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
				end
  CLOSE cur1 
	update PRTABLE004 set columna13=1 where column08 in(@column08)
			 END

end try
begin catch
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
		set @tempSTR=(null)
		set @tempSTR=(cast((SELECT  ERROR_NUMBER())as nvarchar(250)) +'ErrorNumber'+ cast((SELECT  ERROR_LINE())as nvarchar(250)) +'ErrorLine' + cast((SELECT  ERROR_MESSAGE())as nvarchar(250)) +'ErrorMessage' +' at ');
		exec [CheckDirectory] @tempSTR,'usp_PRO_TP_Exception_PRTABLE003.txt',0
return 0
end catch
end


















GO
