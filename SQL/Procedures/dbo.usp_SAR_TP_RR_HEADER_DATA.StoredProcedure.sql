USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_RR_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_RR_HEADER_DATA]
(
	@ReceiptID int
)

AS
BEGIN
	SELECT a.COLUMN05 Customer,a.COLUMN11 Terms,a.COLUMN21 Mode,a.COLUMN14 ou,a.COLUMN17 dept, a.COLUMN04 Ref, 
	a.COLUMN23 taxtype,a.COLUMN12 notes, a.COLUMN29 project,a.COLUMN22 subtotal,a.COLUMN24 taxamt,a.COLUMN20 totamt,
	ca.COLUMN06 addresse,ca.COLUMN07 add1,ca.COLUMN08 add2,ca.COLUMN16 country,ca.COLUMN11 'state',ca.COLUMN10 city,
    ca.COLUMN12 zip,ca.COLUMN17 bill,ca.COLUMN18 shipp
	FROM SATABLE009 a inner join SATABLE010 b on a.COLUMN01=b.COLUMN15  and isnull(b.COLUMNA13,0)=0
	left join SATABLE002 c on c.COLUMN02=a.COLUMN05
	left join SATABLE003 ca on c.COLUMN16=ca.COLUMN02
	 WHERE  a.COLUMN02= @ReceiptID
END







GO
