USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE009]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_SATABLE009]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
    @COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  
    @COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,  
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null, 
		--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015 
	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN43   nvarchar(250)=null,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	@COLUMN44   nvarchar(250)=null,  @COLUMN45   nvarchar(250)=null,  @COLUMN46   nvarchar(250)=null,  
	@COLUMN47   nvarchar(250)=null,  @COLUMN48   nvarchar(250)=null,  @COLUMN49   nvarchar(250)=null,
	@COLUMN50   nvarchar(250)=null,  @COLUMN51   nvarchar(250)=null,  
	@COLUMN52   nvarchar(250)=null,  @COLUMN53   nvarchar(250)=null,  @COLUMN54   nvarchar(250)=null,  
	@COLUMN55   nvarchar(250)=null,  @COLUMN56   nvarchar(250)=null,  @COLUMN57   nvarchar(250)=null,  
	@COLUMN58   nvarchar(250)=null,  @COLUMN59   nvarchar(250)=null,  @COLUMN60   nvarchar(250)=null,  
	@COLUMN61   nvarchar(250)=null,  @COLUMN62   nvarchar(250)=null,  @COLUMN63   nvarchar(250)=null,  
	@COLUMN64   nvarchar(250)=null,  @COLUMN65   nvarchar(250)=null,  @COLUMN66   nvarchar(250)=null,  
	@COLUMN67   nvarchar(250)=null,  @COLUMN68   nvarchar(250)=null,  @COLUMN69   nvarchar(250)=null,
	@COLUMN70   nvarchar(250)=null,  @COLUMN71   nvarchar(250)=null,  @COLUMN72   nvarchar(250)=null,
	@COLUMN73   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @Totamt nvarchar(50)=null,  
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@TabelName  nvarchar(250)=null,  @ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try 
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE009_SequenceNo
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications 
set @COLUMN19=(1002)
--EMPHCS1410	Logs Creation by srinivas
   declare @tempSTR nvarchar(max)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Header Values are Intiated for SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull(  @COLUMN33,'')+','+isnull(  @COLUMN34,'')+','+isnull( @COLUMN35,'')+','+isnull(  @COLUMN36,'')+','+isnull(  @COLUMN37,'')+','+isnull(  @COLUMN38,'')+','+isnull(  @COLUMN39,'')+','+isnull(  @COLUMN40,'')+','+isnull(  @COLUMN41,'')+','+
   isnull(@COLUMN42 ,'')+','+isnull(  @COLUMN43,'')+','+isnull(  @COLUMN44,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into SATABLE009 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20  ,COLUMN21,
  --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
  COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,
  	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
  COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,  COLUMN39, COLUMN40,  COLUMN41,
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
  --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
  COLUMN42,  COLUMN43,  COLUMN44,  COLUMN45,  COLUMN46,  COLUMN47,  COLUMN48,  COLUMN49,  COLUMN50,
  COLUMN55,  COLUMN56,  COLUMN64,  COLUMN65,  COLUMN66,  COLUMN67,  
  COLUMN68,  COLUMN69,  COLUMN70,  COLUMN71,  COLUMN72,  COLUMN73,COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21 , 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN47,  @COLUMN48,  @COLUMN49,  @COLUMN50, 
   @COLUMN55,  @COLUMN56,  @COLUMN64,  @COLUMN65,  @COLUMN66,  @COLUMN67,  
   @COLUMN68,  @COLUMN69,  @COLUMN70,  @COLUMN71,  @COLUMN72,  @COLUMN73, @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, 
   @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
   --EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Header Intiated Values are Created in SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
--EMPHCS1296 GNANESHWAR ON 10/12/2015 if TDS NOT there inserting record in TdsRecivable,TdsPayable in bill and invoice
if(@COLUMN38 !=''  and @COLUMN38 is not null and cast(isnull(@COLUMN38,0) as decimal(18,2))>0  )
BEGIN
DECLARE @IDCL INT
DECLARE @TYPEIDCL NVARCHAR(25)
DECLARE @NUMBERCL INT

SET @IDCl=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE034)+1
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @NUMBERCl=(SELECT MAX(COLUMN01) FROM SATABLE009 where COLUMN02=@COLUMN02)
set @TYPEIDCl=(SELECT COLUMN04 FROM SATABLE009 WHERE COLUMN01=@NUMBERCl)
   --EMPHCS1410	Logs Creation by srinivas
    set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Asset Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(ISNULL(@IDCl,10001)as nvarchar(250)) +','+  'TDS Receivable'+','+ cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+ ISNULL(@COLUMN12,'')+','+  ISNULL(@COLUMN19,'')+','+  ISNULL(@COLUMN38,'')+','+  ISNULL(@TYPEIDCl,'')+','+  '1021'+','+  ISNULL(@COLUMN37,'')+','+  ISNULL(@COLUMNA02,'')+','+
   ISNULL(@COLUMNA03,'') +  '  ')
   )
   
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'TDS Receivable',  @COLUMN04 = @COLUMN04, @COLUMN05 = @NUMBERCl,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = '1021',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN38,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

insert into FITABLE034 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,    COLUMN09,COLUMN10,COLUMN11,COLUMN12,
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   ISNULL(@IDCl,10001),  'TDS Receivable',  @COLUMN08,  @COLUMN12,  @COLUMN19,  CAST(ISNULL(@COLUMN38,0) AS DECIMAL(18,2)),    @TYPEIDCl,'1021',@COLUMN37,@COLUMN29,
	   @COLUMNA02, @COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
	)	
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Asset Intiated Values are Created in FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
END
declare @SalesRep int 
declare @Commission DECIMAL(18,2) 
declare @number int 
declare @Type int declare @AID int
declare @BAL DECIMAL(18,2)
--declare @CBAL DECIMAL(18,2)
--declare @SBAL DECIMAL(18,2)
declare @DBAL DECIMAL(18,2)
 declare @Vid nvarchar(250)=null
 DECLARE @Tax int
 DECLARE @TaxAG int
 --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @number = (select MAX(COLUMN01) from  SATABLE009 where COLUMN02=@COLUMN02);
 set @Tax= (select COLUMN23 from SATABLE009 where COLUMN01=@number)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
--EMPHCS730 Account Owner Condition for Income Register by srinivas 7/21/2015
set @BAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMNA13=0) ;
declare @AID1 nvarchar(250)
--set @COLUMN56=(iif(cast(@COLUMN56 as nvarchar(250))='',0,isnull(@COLUMN56,0)))
IF(cast(@COLUMN56 as decimal(18,2))>0.00)
BEGIN

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN56,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

set @AID1=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
values(@AID1,@COLUMN08,'INVOICE',@COLUMN04,@COLUMN05,@number,1049,@COLUMN56,@COLUMNA02 , @COLUMNA03,0,@COLUMN29) 
END
--set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056)+ ;
--set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1053)+ISNULL(@COLUMN22,0) ;
--set @DBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMNA13=0);
			--if(cast(isnull(@COLUMN25,0) as DECIMAL(18,2))>0)
			--	begin
			--	--EMPHCS1410	Logs Creation by srinivas
			--		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
			--		'Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
			--			'The Values are '+
			--			cast(@AID as nvarchar(250)) +','+  cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+  'INVOICE'+','+ cast(@number as nvarchar(250))+','+  ISNULL(@COLUMN05,'')+','+  ISNULL(@COLUMN12,'')+',  1049,'+  isnull(@COLUMN25,'')+','+  ISNULL(cast(@DBAL as nvarchar(250)),'')+isnull(@COLUMN25,'')+','+  ISNULL(@COLUMN14,'')+','+ISNULL(@COLUMNA03,'') +',0,'+ISNULL(@COLUMN29,'')+ ' '))
			--		--EMPHCS1283 gnaneshwar on 7/10/2015  Adding Project Column in Fitable25(income Acount) 
			--		insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--		values(@AID,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,1049,isnull(@COLUMN25,0),@DBAL+cast(isnull(@COLUMN25,0) as DECIMAL(18,2)),@COLUMN14 , @COLUMNA03,0,@COLUMN29)  
			--	--EMPHCS1410	Logs Creation by srinivas
			--		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
			--		'Income Intiated Values are Created in FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''))					
			--	end
			--set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
		--Sales of Product Income creating at line level  by srinivas 7/21/2015
			--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03)
			--values(@AID,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,1052,NULL,@BAL+cast(isnull(@COLUMN22,0) as DECIMAL(18,2)),@COLUMN14 , @COLUMNA03)
--END
set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)
if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22303)
	BEGIN
		declare @PNewID int,@amnt nvarchar(250)
		set @amnt = ((cast(@COLUMN22 as DECIMAL(18,2))*@Commission/100))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22492',  @COLUMN09 = @SalesRep,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @amnt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
		--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission  Values are Intiated for FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@PNewID as nvarchar(250)) +','+  cast(ISNULL(@COLUMN08,'')as nvarchar(250))+','+  'INVOICE'+','+ cast(@number as nvarchar(250))+','+  ISNULL(@COLUMN05,'')+','+  ISNULL(@COLUMN12,'')+','+  cast((cast(@COLUMN20 as DECIMAL(18,2))-@COLUMN24)as nvarchar(250))+','+  cast(((cast(@COLUMN20 as DECIMAL(18,2))-@COLUMN24*@Commission/100))as nvarchar(250))+','+  cast((cast(@COLUMN20 as DECIMAL(18,2))-@COLUMN24)as nvarchar(250))+','+ cast( @SalesRep as nvarchar(250))+','+cast(@Commission as nvarchar(250))+',OPEN,'+ISNULL(@COLUMNA02,'')+ ','+ISNULL(@COLUMNA03,'')+ ' ')
					)
		--EMPHCS1151	Agent Commission payments(amount pay-vat=tottal ) BY RAJ.Jr 23/9/2015
		insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02,COLUMNA03)
		values(@PNewID,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@COLUMN04,(cast(@COLUMN22 as DECIMAL(18,2))),
		((cast(@COLUMN22 as DECIMAL(18,2))*@Commission/100)),(cast(@COLUMN22 as DECIMAL(18,2))),@SalesRep,@Commission,'OPEN',@COLUMNA02,@COLUMNA03) 
	--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission Intiated Values are Created in FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + ''))					
	END
declare @newID int,@tmpnewID int,@ReverseCharged bit,@TOTALAMT DECIMAL(18,2),@Creditcashsale bit
set @Creditcashsale = (select isnull(COLUMN04,0) from CONTABLE031 where COLUMN03='Allow credit for cash sale' and isnull(COLUMN04,0)=1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @ReverseCharged = (select isnull(COLUMN67,0) from SATABLE009 where COLUMN01=@number)
if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN20,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN20,0))
end
IF (@COLUMN19 ='1002' and ((@COLUMN03!='1532' and @COLUMN03!='1604') or (@Creditcashsale=1 or @Creditcashsale='True' or @Creditcashsale='1')))
begin
if not exists(select COLUMN05  from PUTABLE018 where  COLUMN05=@COLUMN04 and  COLUMN07=@COLUMN05 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and ISNULL(COLUMNA13,0)=0)
							BEGIN 
								
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @TOTALAMT,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

								set @tmpnewID=(select MAX(COLUMN02) from PUTABLE018)
								if(@tmpnewID>0)
									begin
										set @newID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1
									end
								else
									begin
										set @newID=100000
									end
				--EMPHCS891 rajasekhar reddy patakota 10/8/2015 Account receivable amount should be (Total amount - Discount amount) and excluding taxes
				--EMPHCS989		Account Receivables - amount is not considered tax values BY RAJ.Jr 18/8/2015

insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02, COLUMNA03,COLUMN18)
				values(@newID,3000,@COLUMN08,@COLUMN04,'Invoice',@COLUMN05,@COLUMN12,@TOTALAMT,@COLUMN10,@TOTALAMT,'Open',@COLUMN14,@COLUMNA03,@COLUMN29)
					--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Receivable Intiated Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''))
							END
						else
							BEGIN
						--EMPHCS1410	Logs Creation by srinivas	
								--set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@COLUMN04 and COLUMN18=@COLUMN29 )
								update PUTABLE018 set COLUMN07=@COLUMN05,COLUMN04=@COLUMN08,COLUMN09=@TOTALAMT,COLUMN12=@TOTALAMT,COLUMNA13=0 where COLUMN05=@COLUMN04  and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02							
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Receivable Intiated Values are Updated in PUTABLE018 Table By '+cast(@TOTALAMT as nvarchar(20))+' for '+cast(@COLUMN04 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + ''))					
							END
end
--if(isnull(cast(@COLUMN24 as decimal(18,2)),0)>0)
--		BEGIN
--			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,  COLUMNA02, COLUMNA03)
--			values(@Vid,@COLUMN08,'Invoice',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@COLUMN24,@COLUMNA02, @COLUMNA03)
--		END
--EMPHCS794 Packing and Shipping Charges Calculation in Sales Order and Invoice Done by Srinivas 7/30/2015 
declare @HID nvarchar(250)
set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
SET @BAL=(isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN36 as decimal(18,2)),0)+isnull(cast(@COLUMN44 as decimal(18,2)),0))
if(@BAL!=0)
		BEGIN
									
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22405', @COLUMN11 = '1052',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @BAL,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=((select isnull(max(column02),1000) from FITABLE025)+1)
         if(@newID is null or @newID='')set @newID=(1000)
	--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN08,COLUMN09 ,COLUMNA02, COLUMNA03, COLUMNA13)
--values ( @newID,@COLUMN08,'INVOICE',@HID,1052,@BAL, @COLUMNA02, @COLUMNA03,@COLUMNA13)
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
values ( @newID,@COLUMN08,'INVOICE',@HID,@COLUMN05,@COLUMN12,1052,@BAL,0, @COLUMNA02, @COLUMNA03,@COLUMNA13)
		END
		IF(CAST(ISNULL(@COLUMN47,0)AS DECIMAL(18,2))>0)
		BEGIN
									
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = '1124',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @COLUMN47,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @newID=((select isnull(max(column02),1000) from FITABLE026)+1)
		set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03, COLUMN19)
						values(@newID,  @COLUMN08,'INVOICE',@HID,  @COLUMN05,@COLUMN12,'1124',@COLUMN04,    '22399', @COLUMN47,@COLUMN24,'10','Output Surcharge10%',@COLUMNA02, @COLUMNA03,@COLUMN29)
		END
		IF(CAST(ISNULL(@COLUMN68,0)AS DECIMAL(18,2))!=0)
		BEGIN
									
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22407', @COLUMN11 = '1145',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @COLUMN68,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @newID=((SELECT MAX(ISNULL(COLUMN02,1000)) from FITABLE035)+1)
		insert into FITABLE035(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,COLUMNA02, COLUMNA03 )
		values(@newID,  'INVOICE',  @COLUMN08,  @HID,  @COLUMN05,ISNULL(@COLUMN68,0),0,@COLUMN04,1145,@COLUMN29,@COLUMNA02, @COLUMNA03)
		END
		--PACKING,SHIPPING & OTHER TAXES BY VENKAT
DECLARE @PACKINGCHARGE decimal(18,2),@PACKINGAMT decimal(18,2),@TaxString nvarchar(250)=NULL,@TAXRATEM DECIMAL(18,2),
@cnt INT =0,@vochercnt INT = 1,@TAXRATE DECIMAL(18,2) ,@TTYPE INT = 0,@TaxNo DECIMAL(18,2),@AMOUNTM decimal(18,2),
@TaxColumn17 nvarchar(250),@TotTaxAmt decimal(18,2),@ActAmt decimal(18,2),@CharAcc nvarchar(250)
if(CAST(ISNULL(@COLUMN35,0) AS DECIMAL(18,2))>0)
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN35,0)
SET  @Tax= ISNULL(@COLUMN73,0)
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt < @vochercnt
BEGIN
	SET @Tax = ISNULL(@COLUMN73,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt!=0)
			BEGIN
			set @vochercnt=(@vochercnt+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt=0
	end
	if(@COLUMN73>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if(@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586)
	    BEGIN
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	    	--set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	END
	END
	END
DECLARE @cnt1 INT =0,@vochercnt1 INT = 1
if((CAST(ISNULL(@COLUMN36,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN36,0)
SET  @Tax= ISNULL(@COLUMN69,0)
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt1 < @vochercnt1
BEGIN
	SET @Tax = ISNULL(@COLUMN69,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt1=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt1!=0)
			BEGIN
			set @vochercnt1=(@vochercnt1+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt1=0
	end
	if(@COLUMN69>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if(@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586)
	    BEGIN
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	    		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
	--set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	END
	END
	END
	DECLARE @cnt2 INT =0,@vochercnt2 INT = 1
if((CAST(ISNULL(@COLUMN44,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN44,0)
SET  @Tax= ISNULL(@COLUMN70,0)
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt2 < @vochercnt2
BEGIN
	SET @Tax = ISNULL(@COLUMN70,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt2=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt2!=0)
			BEGIN
			set @vochercnt2=(@vochercnt2+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt2=0
	end
	if(@COLUMN70>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if(@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586)
	    BEGIN
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	    	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	    		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
	--set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	END
	END
	END
set @ReturnValue = (@HID)
set @tempSTR=@tempSTR+('the return parameter is '+cast(@ReturnValue as nvarchar(max)))
END
 

IF @Direction = 'Select'
BEGIN
select * from SATABLE009
END 

  
IF @Direction = 'Update'
BEGIN
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
declare @PrevInv NVARCHAR(250), @Location NVARCHAR(250), @Project NVARCHAR(250)
set @PrevInv=(select COLUMN04 from SATABLE009 where COLUMN02=@COLUMN02)
set @Location=(select COLUMN39 from SATABLE009 where COLUMN02=@COLUMN02)
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
set @PrevInv=(select COLUMN04 from SATABLE009 where COLUMN02=@COLUMN02)
set @Project= (select COLUMN29 from SATABLE009 where COLUMN02=@COLUMN02)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications 
set @COLUMN19=(1002)
--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Header Values are Intiated for SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull(  @COLUMN33,'')+','+isnull(  @COLUMN34,'')+','+isnull( @COLUMN35,'')+','+isnull(  @COLUMN36,'')+','+isnull(  @COLUMN37,'')+','+isnull(  @COLUMN38,'')+','+isnull(  @COLUMN39,'')+','+isnull(  @COLUMN40,'')+','+isnull(  @COLUMN41,'')+','+
   isnull(@COLUMN42 ,'')+','+isnull(  @COLUMN43,'')+','+isnull(  @COLUMN44,'')+','+isnull(  @COLUMN45,'')+','+isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull(@COLUMNA07,'')+','+
   isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE SATABLE009 SET
   COLUMN02=@COLUMN02,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,     
   COLUMN19=@COLUMN19,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,     COLUMN22=@COLUMN22,     COLUMN23=@COLUMN23,
   COLUMN24=@COLUMN24,     COLUMN25=@COLUMN25,     COLUMN26=@COLUMN26,     COLUMN27=@COLUMN27,     COLUMN28=@COLUMN28,
   COLUMN29=@COLUMN29,	   COLUMN30=@COLUMN30,	   COLUMN31=@COLUMN31,     COLUMN32=@COLUMN32,     COLUMN33=@COLUMN33,
   COLUMN34=@COLUMN34,     COLUMN35=@COLUMN35,     COLUMN36=@COLUMN36,     COLUMN37=@COLUMN37,     COLUMN38=@COLUMN38,
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN39=@COLUMN39,     COLUMN40=@COLUMN40,     COLUMN41=@COLUMN41,     COLUMN42=@COLUMN42,     COLUMN43=@COLUMN43, 
   COLUMN44=@COLUMN44,     COLUMN45=@COLUMN45,     COLUMN46=@COLUMN46,     COLUMN47=@COLUMN47,     COLUMN48=@COLUMN48,
   COLUMN49=@COLUMN49,     COLUMN50=@COLUMN50,        
   COLUMN55=@COLUMN55,     COLUMN56=@COLUMN56,     COLUMN64=@COLUMN64,  
   COLUMN65=@COLUMN65,     COLUMN66=@COLUMN66,     COLUMN67=@COLUMN67,     COLUMN68=@COLUMN68,     COLUMN69=@COLUMN69,
   COLUMN70=@COLUMN70,     COLUMN71=@COLUMN71,     COLUMN72=@COLUMN72,     COLUMN73=@COLUMN73,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
   COLUMNA07=@COLUMNA07,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Intiated Values are Updated in SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + '')
   )

   if(@COLUMN03='1532' or @COLUMN03='1604')
   begin
   if exists(select COLUMN01 from SATABLE012 WHERE COLUMN03 in(@COLUMN02) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   begin
   if((SELECT top(1)CAST(P.COLUMN19 as date) FROM SATABLE011 p inner join SATABLE012 l on l.COLUMN08=p.COLUMN01 and l.COLUMNA02=p.COLUMNA02 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0
   WHERE l.COLUMN03 in(@COLUMN02) and l.COLUMNA02=@COLUMNA02 and l.COLUMNA03=@COLUMNA03 and isnull(p.COLUMNA13,0)=0)!=CAST(@COLUMN08 as date))
   begin
   UPDATE p set p.COLUMN19=@COLUMN08,p.COLUMN10=@COLUMN12 FROM SATABLE011 p inner join SATABLE012 l on l.COLUMN08=p.COLUMN01 and l.COLUMNA02=p.COLUMNA02 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0
   WHERE l.COLUMN03 in(@COLUMN02) and l.COLUMNA02=@COLUMNA02 and l.COLUMNA03=@COLUMNA03 and isnull(p.COLUMNA13,0)=0
   UPDATE r set r.COLUMN04=@COLUMN08,r.COLUMN08=@COLUMN12 FROM PUTABLE018 r 
   inner join SATABLE011 p on p.COLUMN04=r.COLUMN05 and p.COLUMN01=r.COLUMN17 and r.COLUMNA02=p.COLUMNA02 and r.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
   inner join SATABLE012 l on l.COLUMN03 in(@COLUMN02) and l.COLUMN08=p.COLUMN01 and l.COLUMNA02=p.COLUMNA02 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0
   WHERE r.COLUMN06='Payment' and r.COLUMNA02=@COLUMNA02 and r.COLUMNA03=@COLUMNA03 and isnull(r.COLUMNA13,0)=0
   UPDATE r set r.COLUMN04=@COLUMN08,r.COLUMN09=@COLUMN12 FROM PUTABLE019 r 
   inner join SATABLE011 p on p.COLUMN04=r.COLUMN05 and p.COLUMN01=r.COLUMN08 and r.COLUMNA02=p.COLUMNA02 and r.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
   inner join SATABLE012 l on l.COLUMN03 in(@COLUMN02) and l.COLUMN08=p.COLUMN01 and l.COLUMNA02=p.COLUMNA02 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0
   WHERE r.COLUMN06='PAYMENT' and r.COLUMNA02=@COLUMNA02 and r.COLUMNA03=@COLUMNA03 and isnull(r.COLUMNA13,0)=0
   UPDATE r set r.COLUMN04=@COLUMN08,r.COLUMN10=@COLUMN12 FROM FITABLE052 r 
   inner join SATABLE011 p on p.COLUMN04=r.COLUMN05 and p.COLUMN01=r.COLUMN09 and r.COLUMNA02=p.COLUMNA02 and r.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
   inner join SATABLE012 l on l.COLUMN03 in(@COLUMN02) and l.COLUMN08=p.COLUMN01 and l.COLUMNA02=p.COLUMNA02 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0
   WHERE r.COLUMN06='PAYMENT' and r.COLUMNA02=@COLUMNA02 and r.COLUMNA03=@COLUMNA03 and isnull(r.COLUMNA13,0)=0
   UPDATE r set r.COLUMN03=@COLUMN08,r.COLUMN07=@COLUMN12 FROM FITABLE027 r 
   inner join SATABLE011 p on p.COLUMN04=r.COLUMN09 and p.COLUMN01=r.COLUMN05 and r.COLUMNA02=p.COLUMNA02 and r.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
   inner join SATABLE012 l on l.COLUMN03 in(@COLUMN02) and l.COLUMN08=p.COLUMN01 and l.COLUMNA02=p.COLUMNA02 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0
   WHERE r.COLUMN04='PAYMENT' and r.COLUMNA02=@COLUMNA02 and r.COLUMNA03=@COLUMNA03 and isnull(r.COLUMNA13,0)=0
   end
   end
   end
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   set @COLUMN39=(isnull(@Location,0))
   SET @number=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
      DELETE FROM FITABLE034 where COLUMN09=@PrevInv AND COLUMN03='TDS Receivable' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	  DELETE FROM FITABLE034 where COLUMN09=@PrevInv AND COLUMN03='INVOICE' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	  DELETE FROM FITABLE064 where COLUMN05=@number AND COLUMN03 IN('INVOICE','TDS Receivable','Markdown Register','Sales Claim Register') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	  if(@COLUMN38 !=''  and @COLUMN38 is not null and cast(isnull(@COLUMN38,0) as decimal(18,2))>0  )
BEGIN
 
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'TDS Receivable',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22402', @COLUMN11 = '1021',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN38,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @IDCl=(SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM FITABLE034)+1
set @TYPEIDCl=(SELECT COLUMN04 FROM SATABLE009 WHERE COLUMN01=@number)
    set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Asset Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(ISNULL(@IDCl,10001)as nvarchar(250)) +','+  'TDS Receivable'+','+ cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+ ISNULL(@COLUMN12,'')+','+  ISNULL(@COLUMN19,'')+','+  ISNULL(@COLUMN38,'')+','+  ISNULL(@TYPEIDCl,'')+','+  '1021'+','+  ISNULL(@COLUMN37,'')+','+  ISNULL(@COLUMNA02,'')+','+
   ISNULL(@COLUMNA03,'') +  '  ')
   )
insert into FITABLE034 
	(
		COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,    COLUMN09,COLUMN10,COLUMN11,COLUMN12,
		COLUMNA02, COLUMNA03 
	)
	values
	( 
	   ISNULL(@IDCl,10001),  'TDS Receivable',  @COLUMN08,  @COLUMN12,  @COLUMN19,  CAST(ISNULL(@COLUMN38,0) AS DECIMAL(18,2)),    @TYPEIDCl,'1021',@COLUMN37,@COLUMN29,
	   @COLUMNA02, @COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
	)	
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Asset Intiated Values are Created in FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
END

   --EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Asset Intiated Values are Updated In FITABLE034 Table BY '+cast(@COLUMN38 as nvarchar(20))+' For '+isnull(@COLUMN04,'')+''+'' + CHAR(13)+CHAR(10) + '')
   )
   
set @ReverseCharged = (select isnull(COLUMN67,0) from SATABLE009 where COLUMN01=@number)
if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN20,0)as decimal(18,2))-cast(ISNULL(@COLUMN24,0)as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN20,0))
end
DELETE FROM PUTABLE018 WHERE  COLUMN05=@PrevInv AND COLUMN06='Invoice' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @Creditcashsale = (select isnull(COLUMN04,0) from CONTABLE031 where COLUMN03='Allow credit for cash sale' and isnull(COLUMN04,0)=1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
if((@COLUMN03!='1532' and @COLUMN03!='1604') or (@Creditcashsale=1 or @Creditcashsale='True' or @Creditcashsale='1'))
begin
   --EMPHCS1410	Logs Creation by srinivas
	--set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN18=@COLUMN29 )
							
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @TOTALAMT,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			set @newID=cast((select MAX(ISNULL(COLUMN02,999)) from PUTABLE018) as int)+1
				--EMPHCS1410	Logs Creation by srinivas					
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Receivable  Values are Intiated for PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@newID as nvarchar(250)) +',3000,'+  cast(ISNULL(@COLUMN08,'') as nvarchar(250))+','+ ISNULL(@COLUMN04,'')+',Invoice,'+  ISNULL(@COLUMN05,'')+','+ ISNULL(@COLUMN12,'')+','+  ISNULL(@COLUMN20,'')+',Open,'+  ISNULL(@COLUMN14,'')+','+ISNULL(@COLUMNA02,'')+ ','+ISNULL(@COLUMNA03,'')+ ' ')
					)
insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02, COLUMNA03,COLUMN18)
				values(@newID,3000,@COLUMN08,@COLUMN04,'Invoice',@COLUMN05,@COLUMN12,@TOTALAMT,@COLUMN10,@TOTALAMT,'Open',@COLUMN14,@COLUMNA03,@COLUMN29)
					--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Account Receivable Intiated Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''))
							

   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Receivable Intiated Values are Updated In PUTABLE018 Table  BY '+cast(@COLUMN20 as nvarchar(20))+' For '+isnull(@COLUMN04,'')+''+'' + CHAR(13)+CHAR(10) + '')
   )
end   
	DELETE FROM FITABLE036 WHERE COLUMN05 in(@number) AND COLUMN03='INVOICE' AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03		
	DELETE FROM FITABLE036 WHERE COLUMN03='INVOICE' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN09=@PrevInv		
		--set @COLUMN56=(iif(cast(@COLUMN56 as nvarchar(250))='',0,isnull(@COLUMN56,0)))
		IF(cast(@COLUMN56 as decimal(18,2))>0.00)
		BEGIN
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN56,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID1=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
		insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
		values(@AID1,@COLUMN08,'INVOICE',@COLUMN04,@COLUMN05,@number,1049,@COLUMN56,@COLUMNA02 , @COLUMNA03,0,@COLUMN29) 
		END
	set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
	set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
	set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)
	
	--EMPHCS1151	Agent Commission payments(amount pay-vat=tottal ) BY RAJ.Jr 23/9/2015
		DELETE FROM FITABLE027 WHERE COLUMN04='INVOICE' AND COLUMN05=@number  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22303)
		BEGIN
			set @amnt = ((cast(@COLUMN22 as DECIMAL(18,2))*@Commission/100))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22492',  @COLUMN09 = @SalesRep,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = 0,       @COLUMN15 = @amnt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
		--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission  Values are Intiated for FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(@PNewID as nvarchar(250)) +','+  cast(ISNULL(@COLUMN08,'')as nvarchar(250))+','+  'INVOICE'+','+ cast(@number as nvarchar(250))+','+  ISNULL(@COLUMN05,'')+','+  ISNULL(@COLUMN12,'')+','+  cast((cast(@COLUMN20 as DECIMAL(18,2))-@COLUMN24)as nvarchar(250))+','+  cast(((cast(@COLUMN20 as DECIMAL(18,2))-@COLUMN24*@Commission/100))as nvarchar(250))+','+  cast((cast(@COLUMN20 as DECIMAL(18,2))-@COLUMN24)as nvarchar(250))+','+ cast( @SalesRep as nvarchar(250))+','+cast(@Commission as nvarchar(250))+',OPEN,'+ISNULL(@COLUMNA02,'')+ ','+ISNULL(@COLUMNA03,'')+ ' ')
					)
		--EMPHCS1151	Agent Commission payments(amount pay-vat=tottal ) BY RAJ.Jr 23/9/2015
		insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02,COLUMNA03)
		values(@PNewID,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@COLUMN04,(cast(@COLUMN22 as DECIMAL(18,2))),
		((cast(@COLUMN22 as DECIMAL(18,2))*@Commission/100)),(cast(@COLUMN22 as DECIMAL(18,2))),@SalesRep,@Commission,'OPEN',@COLUMNA02,@COLUMNA03) 
	--EMPHCS1410	Logs Creation by srinivas
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Commission Intiated Values are Created in FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + ''))					
	END
--EMPHCS794 Packing and Shipping Charges Calculation in Sales Order and Invoice Done by Srinivas 7/30/2015
---EMPHCS987		Postage and Delivery account is not updated with shipping and delivery charges BY RAj.Jr 18/8/2015DELETE FROM FITABLE036 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1075 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
	   DELETE FROM FITABLE036 WHERE COLUMN09=@PrevInv AND COLUMN10=1075 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
	   DELETE FROM FITABLE035 WHERE COLUMN09=@PrevInv AND COLUMN10=1092 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
	   DELETE FROM FITABLE035 WHERE COLUMN09=@PrevInv AND COLUMN10=1145 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
	   DELETE FROM FITABLE025 WHERE COLUMN05 in (SELECT COLUMN01 from SATABLE009 where COLUMN02 IN (@COLUMN02))  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
	   set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
	   SET @BAL=(isnull(cast(@COLUMN35 as decimal(18,2)),0)+isnull(cast(@COLUMN36 as decimal(18,2)),0)+isnull(cast(@COLUMN44 as decimal(18,2)),0))
		if(@BAL!=0)
		BEGIN
								
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22405', @COLUMN11 = '1052',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @BAL,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=((select isnull(max(column02),1000) from FITABLE025)+1)
         if(@newID is null or @newID='')set @newID=(1000)
	--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN08,COLUMN09 ,COLUMNA02, COLUMNA03, COLUMNA13)
--values ( @newID,@COLUMN08,'INVOICE',@HID,1052,@BAL, @COLUMNA02, @COLUMNA03,@COLUMNA13)
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
values ( @newID,@COLUMN08,'INVOICE',@HID,@COLUMN05,@COLUMN12,1052,@BAL,0, @COLUMNA02, @COLUMNA03,@COLUMNA13)
		END

	declare @SOID nvarchar(250),@QtyH nvarchar(250),@QtyA nvarchar(250),@QtyC nvarchar(250),@Item nvarchar(250),
	@AMOUNT decimal(18,2),	@AMOUNT1 decimal(18,2),	@ItemQty nvarchar(250),
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
    @chk bit,
	@Price nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250),
	--EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	@id nvarchar(250),@uom nvarchar(250),@RefLineId nvarchar(250),@linelocation nvarchar(250)
	--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
    declare @Billno nvarchar(250)=null,@Billlineref nvarchar(250)=null,@uomselection nvarchar(250)=null,@lotno nvarchar(250),@upcno nvarchar(250) = null
	SET @COLUMN06=(SELECT  datalength(isnull(COLUMN06,'')) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)
	if(@COLUMN06=0)
	BEGIN
		DECLARE @MaxRownum INT
		  DECLARE @Initialrow INT=1
		  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
		   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15=@SOID and isnull(columna13,0)=0
		  OPEN cur1
		  FETCH NEXT FROM cur1 INTO @id
		   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
		  SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15=@SOID and isnull(columna13,0)=0)
		     WHILE @Initialrow <= @MaxRownum
		     BEGIN 
		         set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)				 
			 --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	        set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@Item)
			set @Billno=(select COLUMN04 from SATABLE009 where COLUMN02=@COLUMN02)
			set @Billlineref=(select COLUMN01 from SATABLE010 where COLUMN02=@id)
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		    set @lotno=(select isnull(column31,0)  from  SATABLE010 where COLUMN02=@id)
			 set @upcno=(select column03  from  SATABLE010 where COLUMN02=@id)
		    set @linelocation=(select cast(COLUMN38 as nvarchar(250)) from SATABLE010 where COLUMN02=@id)
			set @COLUMN39=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 --EMPHCS924 rajasekhar reddy patakota 11/8/2015 uom condition checking in Invoice
			 set @uom=(select COLUMN22 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
		         set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
		         set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
			 --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
			  --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
			  --EMPHCS1167 Service items calculation by srinivas 22/09/2015
			  set @chk=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from matable007 where column02=@Item)
		          declare @Itemtype nvarchar(250)
			  set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@Item)
		        if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null  or @uomselection='False') and @chk=1)
			  begin
				 --EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Inventory Quantity Calculation
				 if((SELECT datalength(isnull(COLUMN06,'')) FROM SATABLE009 WHERE COLUMN01=@SOID)=0)				 
				 begin
				 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
				  set @QtyH=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno )
		         set @QtyA=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)
		         set @QtyC=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)
			 --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			     declare @AvgPrice decimal(18,2)
				 set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno);
			     UPDATE FITABLE010 SET COLUMN04=cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   --COLUMN05=cast(@QtyC as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
									   COLUMN08=cast((cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2))) as decimal(18,2))
									   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
									   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
				 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno
				UPDATE FITABLE010 SET COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@AvgPrice as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno
				 --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
				 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
				 --EMPHCS1410	Logs Creation by srinivas
					 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					 'Inventory Values are Updated In FITABLE010 Table   BY '+cast(cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2)) as nvarchar(20))+' For '+cast(@Item as nvarchar(250))+','+cast(@uom as nvarchar(250))+''+'' + CHAR(13)+CHAR(10) + '')
					 )

				set @QtyH=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
				if(cast(@QtyH as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)as decimal(18,2))/cast(@QtyH as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
				end
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
                --UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)as decimal(18,2))/cast((cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2))) as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
		--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
				end
				end
				else
				begin
				--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
				exec usp_PUR_TP_InventoryUOM  @Billno,@Billlineref,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@COLUMN39,@lotno
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				 FETCH NEXT FROM cur1 INTO @id
		         SET @Initialrow = @Initialrow + 1 
			END
		CLOSE cur1 
  END
  
	SET @COLUMN06=(SELECT (COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)
  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur4 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0
      OPEN cur4
	  FETCH NEXT FROM cur4 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
			 set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMNA13=0)
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			 set @RefLineId=(select COLUMN36 from SATABLE010 where COLUMN02=@id and COLUMNA13=0)
             set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
             set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
			 set @uom=(select isnull(COLUMN22,0) from SATABLE010 where COLUMN02=@id)
			 set @lotno=(select isnull(column31,0)  from  SATABLE010 where COLUMN02=@id)
		     set @upcno=(select column03  from  SATABLE010 where COLUMN02=@id)
             --BILLED QTY Updation
	     --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefLineId,0)>0)
				begin
				set @COLUMN13=(select isnull(COLUMN13,0) from SATABLE006 where COLUMN02=@RefLineId and COLUMNA13=0)
				UPDATE SATABLE006 SET   COLUMN13=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@RefLineId and COLUMNA13=0
				end
				else
				begin
			    set @COLUMN13=(select isnull(COLUMN13,0) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item  and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0) and COLUMNA13=0)
				--UPDATE SATABLE010 SET COLUMN12=(select COLUMN09 from SATABLE010 where COLUMN02=@id) WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
				UPDATE SATABLE006 SET   COLUMN13=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item  and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0) and COLUMNA13=0 and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06))
				end
				--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
                delete from	FITABLE025 where COLUMN04='Markdown Register' and 	COLUMN05=(select COLUMN01 from SATABLE010 where COLUMN02=@id and COLUMNA13=0) and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03 --and COLUMN06 = @COLUMN05
                delete from	FITABLE034 where COLUMN03='Sales Claim Register' and 	COLUMN09=@PrevInv and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
				UPDATE PUTABLE013 SET COLUMN11=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
				delete from PUTABLE017 where COLUMN05 IN(@number) and COLUMN06='INVOICE' and COLUMN13=@COLUMNA02 and COLUMNA03=@COLUMNA03
			 FETCH NEXT FROM cur4 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur4 

  UPDATE SATABLE010 SET COLUMNA13=1 WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
	set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)  
	
	--UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN05 in(@SOID) AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03	  
	
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
	UPDATE FITABLE025 SET COLUMNA13=1 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) AND COLUMN08=1053 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1410	Logs Creation by srinivas	 
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Income Intiated Values are Updated In FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
	 
	--EMPHCS1148 Tax amount not displaying in chart of accounts for invoice by srinivas 15/09/2015
				UPDATE FITABLE026 SET COLUMNA13=1 WHERE COLUMN05 in (select COLUMN01 from SATABLE010 where COLUMN15=@number)  AND COLUMN04='Invoice' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Updated In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
	--DELETE FROM FITABLE036 WHERE COLUMN05 in(@SOID) AND COLUMN03='INVOICE' AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
			DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 IN (@SOID))  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
	--DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID))  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
	--DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID))  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
	--DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN15 in(@SOID))  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
	delete from FITABLE026 where COLUMN05 in(select COLUMN01 from SATABLE010 where COLUMN15 in(@SOID)) and COLUMN04='INVOICE' AND COLUMNA02=@COLUMNA02 and  COLUMNA03=@COLUMNA03
		set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
		delete from FITABLE026 where COLUMN05 in(@HID) and COLUMN04='INVOICE' AND COLUMNA02=@COLUMNA02 and  COLUMNA03=@COLUMNA03 AND COLUMN08='1124'
	
		IF(CAST(ISNULL(@COLUMN47,0)AS DECIMAL(18,2))>0)
		BEGIN
		set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
									
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22383', @COLUMN11 = '1124',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @COLUMN47,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @newID=((select isnull(max(column02),1000) from FITABLE026)+1)
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03, COLUMN19)
						values(@newID,  @COLUMN08,'INVOICE',@HID,  @COLUMN05,@COLUMN12,'1124',@COLUMN04,    '22399', @COLUMN47,@COLUMN24,'10','Output Surcharge10%',@COLUMNA02, @COLUMNA03,@COLUMN29)
		END
		IF(CAST(ISNULL(@COLUMN68,0)AS DECIMAL(18,2))!=0)
		BEGIN
										
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @HID,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22407', @COLUMN11 = '1145',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @COLUMN68,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @newID=((SELECT MAX(ISNULL(COLUMN02,1000)) from FITABLE035)+1)
		insert into FITABLE035(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,COLUMNA02, COLUMNA03 )
		values(@newID,  'INVOICE',  @COLUMN08,  @HID,  @COLUMN05,ISNULL(@COLUMN68,0),0,@COLUMN04,1145,@COLUMN29,@COLUMNA02, @COLUMNA03)
		END
--PACKING,SHIPPING & OTHER TAXES BY VENKAT
DELETE FROM FITABLE026 where COLUMN09=@PrevInv AND COLUMN04='INVOICE' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
if(CAST(ISNULL(@COLUMN35,0)AS DECIMAL(18,2))>0)
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN35,0)
SET  @Tax= ISNULL(@COLUMN73,0)
set @cnt=0 set @vochercnt=1
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt < @vochercnt
BEGIN
	SET @Tax = ISNULL(@COLUMN73,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			SET @vochercnt=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt!=0)
			BEGIN
			set @vochercnt=(@vochercnt+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt=0
	end
	if(@COLUMN73>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586 or @COLUMN73 != 0) and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    BEGIN
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	    	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	    	--set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	END
	END
	END

if((CAST(ISNULL(@COLUMN36,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN36,0)
SET  @Tax= ISNULL(@COLUMN69,0)
set @cnt1=0 set @vochercnt1=1
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt1 < @vochercnt1
BEGIN
	SET @Tax = ISNULL(@COLUMN69,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt1=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt1!=0)
			BEGIN
			set @vochercnt1=(@vochercnt1+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt1=0
	end
	if(@COLUMN69>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586 or @COLUMN69 != 0) and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    BEGIN
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	    	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	    		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	--set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	END
	END
	END
	
if((CAST(ISNULL(@COLUMN44,0)AS DECIMAL(18,2))>0))
BEGIN
SET @PACKINGCHARGE = ISNULL(@COLUMN44,0)
SET  @Tax= ISNULL(@COLUMN70,0)
set @cnt2=0 set @vochercnt2=1
if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 WHILE @cnt2 < @vochercnt2
BEGIN
	SET @Tax = ISNULL(@COLUMN70,0)
	if(@Tax<0) 
	BEGIN
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		BEGIN
			set @TaxString=(@TaxString)
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt2=0;
		END
		else
		BEGIN
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))
			if(@vochercnt2!=0)
			BEGIN
			set @vochercnt2=(@vochercnt2+1)
			END
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
			SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
			--SET @PACKINGAMT= (@AMOUNTM/@TAXRATEM)*@TAXRATE
		END
		END
		else
	   begin
		set @vochercnt2=0
	end
	if(@COLUMN70>0)
	    BEGIN
		select @TaxAG=column16, @TAXRATE=COLUMN07,@TaxNo=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
		if(isnull(cast(@PACKINGCHARGE as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    SET @AMOUNTM = ((CAST(@PACKINGCHARGE AS decimal(18,2))*(CAST(ISNULL(@TAXRATE,0) AS decimal(18,2))/100)))
	    END
		if((@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586 or @COLUMN69 != 0) and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
	    BEGIN
	    	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
			set @TaxColumn17=('OUTPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
	    	select top 1 @CharAcc = column02,@TTYPE=COLUMN07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	    		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'INVOICE',  @COLUMN04 = @COLUMN04, @COLUMN05 = @number,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN08,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @COLUMN12,   @COLUMN13 = @COLUMN29,	@COLUMN14 = @AMOUNTM,       @COLUMN15 = 0,      @COLUMN16 = @PACKINGCHARGE, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
	--set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@COLUMN08,'INVOICE',@number,@COLUMN05,@COLUMN12,@Tax,@TaxAG,@AMOUNTM,@PACKINGCHARGE,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@COLUMN04)
	END
	END
	END
		--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
		set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
set @ReturnValue = (@HID)
END


else IF @Direction = 'Delete'
BEGIN
UPDATE SATABLE009 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Invoice Header Values are Deleted In SATABLE009 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
SET @number=(SELECT COLUMN01 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN25=(SELECT COLUMN25 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN14=(SELECT COLUMN14 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN06=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMN29=(SELECT COLUMN29 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
SET @COLUMN39=(SELECT isnull(COLUMN39,0) FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA02=(SELECT COLUMNA02 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA03=(SELECT COLUMNA03 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
set @Project = (select COLUMN29 from SATABLE009 Where COLUMN02 = @COLUMN02)
set @Project = (case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
SET @COLUMNA06=(SELECT COLUMNA06 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA07= (SELECT GETDATE())
SET @COLUMNB02=(SELECT COLUMN03 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
SET @COLUMNA08=(SELECT COLUMNA08 FROM SATABLE009 WHERE COLUMN02 = @COLUMN02)
	  DELETE FROM FITABLE064 where COLUMN05=@number AND COLUMN03 IN('INVOICE','TDS Receivable','Markdown Register','Sales Claim Register') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	  DELETE FROM FITABLE034 where COLUMN09=@COLUMN04 AND COLUMN03='INVOICE' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
DELETE FROM FITABLE034 where COLUMN09=@COLUMN04 AND COLUMN03='TDS Receivable' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
DELETE FROM FITABLE027 WHERE COLUMN04='INVOICE' AND COLUMN05=@number  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
DELETE FROM FITABLE026 WHERE COLUMN04='INVOICE' AND COLUMN05=@number  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Commission Values are Deleted In FITABLE027 Table '+'' + CHAR(13)+CHAR(10) + '')
   )
--EMPHCS892 rajasekhar reddy patakota 8/9/2015 After deletion of invoice account receivable records should delete and status also should change based on the scenario
--update PUTABLE018 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
DELETE FROM PUTABLE018 WHERE  COLUMN05=@COLUMN04 AND COLUMN06='Invoice' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Account Receivable  Values are Deleted In PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + '')
   	)
DELETE FROM FITABLE036 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1075 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
DELETE FROM FITABLE035 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1092 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
DELETE FROM FITABLE035 WHERE COLUMN09=@COLUMN04 AND COLUMN10=1145 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN03='INVOICE'
DELETE FROM FITABLE036 WHERE COLUMN03='INVOICE' AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN09=@COLUMN04
UPDATE FITABLE025 SET COLUMNA13 = 1 WHERE COLUMN05 in (SELECT COLUMN01 from SATABLE009 where COLUMN02 IN (@COLUMN02))  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'		
--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
if((SELECT datalength(isnull(COLUMN06,'')) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)=0)
	BEGIN
	SET @Initialrow = 1
		  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
		  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15= @SOID AND isnull(COLUMNA13,0)=0
		  OPEN cur1
		  FETCH NEXT FROM cur1 INTO @id
		  SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15=@SOID AND isnull(COLUMNA13,0)=0)
		     WHILE @Initialrow <= @MaxRownum
		     BEGIN 
		     --EMPHCS907 rajasekhar reddy patakota 11/08/2015 After deleting invoice - Taxes are not cleared
		         UPDATE FITABLE026 SET COLUMNA13=@COLUMNA13 WHERE COLUMN05=(select COLUMN01 from SATABLE010 where COLUMN02=@id)  AND COLUMN04='Invoice' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
				 set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
		         set @uom=(select COLUMN22 from SATABLE010 where COLUMN02=@id and COLUMN15=@SOID)
			 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
		         set @lotno=(select isnull(column31,0)  from  SATABLE010 where COLUMN02=@id)
				  set @upcno=(select column03  from  SATABLE010 where COLUMN02=@id)
				 set @Location=(select COLUMN39 from SATABLE009 where COLUMN02=@COLUMN02)
				 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
		         set @linelocation=(select cast(COLUMN38 as nvarchar(250)) from SATABLE010 where COLUMN02=@id)
			     set @COLUMN39=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
				 set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
		         set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMN15=@SOID)		        
				--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
				 if((SELECT isnull(datalength(COLUMN06),'') FROM SATABLE009 WHERE COLUMN01=@SOID)=0)				 
				 begin
				 set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@Item)
			set @Billno=(select COLUMN04 from SATABLE009 where COLUMN02=@COLUMN02)
			set @Billlineref=(select COLUMN01 from SATABLE010 where COLUMN02=@id)
			--EMPHCS1167 Service items calculation by srinivas 22/09/2015
			set @chk=(select iif(COLUMN48='',0,ISNULL(COLUMN48,0))COLUMN48 from matable007 where column02=@Item)
			 --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
			set @Itemtype=(select iif(COLUMN05='','ITTY009',ISNULL(COLUMN05,'ITTY009'))COLUMN05 from matable007 where column02=@Item)
			if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null  or @uomselection='False') and @chk=1)
			begin 
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			     set @QtyH=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)
		         set @QtyA=(select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)
		         set @QtyC=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)
			 --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
				 set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno);
			     UPDATE FITABLE010 SET COLUMN04=cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   --COLUMN05=cast(@QtyC as decimal(18,2))+cast(@ItemQty as decimal(18,2)),
									   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
									   COLUMN08=cast((cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2))) as decimal(18,2))
									   ,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMN04,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE'
									   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
								--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill	   
				 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno
				UPDATE FITABLE010 SET COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@AvgPrice as decimal(18,2))))),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMN04,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24 =@upcno
				 --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
				 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				set @QtyH=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)as decimal(18,2)));
				if(cast(@QtyH as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMN04,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project  and COLUMN24=@upcno)as decimal(18,2))/cast(@QtyH as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMN04,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@COLUMN39 AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24 =@upcno
				end
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
                --UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom)as decimal(18,2))/cast((cast(@QtyH as decimal(18,2))+cast(@ItemQty as decimal(18,2))) as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN14 AND COLUMN19=@uom
				--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
				 end
				  else
				  begin
				  --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				  --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
				exec usp_PUR_TP_InventoryUOM  @Billno,@Billlineref,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@COLUMN39,@lotno
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				 end 
				 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
                delete from	FITABLE025 where COLUMN04='Markdown Register' and 	COLUMN05=(select COLUMN01 from SATABLE010 where COLUMN02=@id and COLUMNA13=0) and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03 --and COLUMN06 = @COLUMN05
                delete from	FITABLE034 where COLUMN03='Sales Claim Register' and 	COLUMN09=@Billno and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
				FETCH NEXT FROM cur1 INTO @id
		         SET @Initialrow = @Initialrow + 1 
			END
		CLOSE cur1 
  END
  --EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
	SET @COLUMN06=(SELECT (COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)
  set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur4 CURSOR FOR SELECT COLUMN02 from SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0
      OPEN cur4
	  FETCH NEXT FROM cur4 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE010 where COLUMN15 in(@SOID) and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
	                 --EMPHCS907 rajasekhar reddy patakota 11/08/2015 After deleting invoice - Taxes are not cleared
			 UPDATE FITABLE026 SET COLUMNA13=@COLUMNA13 WHERE COLUMN05=(select COLUMN01 from SATABLE010 where COLUMN02=@id)  AND COLUMN04='Invoice' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
			DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN02=@id)   AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
			--DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN02=@id) AND COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
			--DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN02=@id) AND COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
			--DELETE FROM FITABLE025 WHERE COLUMN05 in(SELECT COLUMN01 from SATABLE010 where COLUMN02=@id) AND COLUMN08=1053 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03  and COLUMN04='INVOICE'
			delete from FITABLE026 where COLUMN05 in(select COLUMN01 from SATABLE010 where COLUMN02=@id) and COLUMN04='INVOICE' AND COLUMNA02=@COLUMNA02 and  COLUMNA03=@COLUMNA03
			 set @Item=(select COLUMN05 from SATABLE010 where COLUMN02=@id and COLUMNA13=0)
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
             set @RefLineId=(select COLUMN36 from SATABLE010 where COLUMN02=@id and COLUMNA13=0)
 set @uom=(select isnull(COLUMN22,0) from SATABLE010 where COLUMN02=@id)
			 set @lotno=(select isnull(column31,0)  from  SATABLE010 where COLUMN02=@id)
			 set @upcno=(select column03  from  SATABLE010 where COLUMN02=@id)
		     set @ItemQty=(select isnull(COLUMN10,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
             set @Price=(select isnull(COLUMN13,0) from SATABLE010 where COLUMN02=@id and COLUMN05=@Item and COLUMNA13=0)
			 
             --BILLED QTY Updation
	     --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefLineId,0)>0)
				begin
				set @COLUMN13=(select isnull(COLUMN13,0) from SATABLE006 where COLUMN02=@RefLineId and COLUMNA13=0)
				UPDATE SATABLE006 SET   COLUMN13=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@RefLineId and COLUMNA13=0
				end
				else
				begin
			    set @COLUMN13=(select isnull(COLUMN13,0) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item  and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0) and COLUMNA13=0)
				--UPDATE SATABLE010 SET COLUMN12=(select COLUMN09 from SATABLE010 where COLUMN02=@id) WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
				UPDATE SATABLE006 SET   COLUMN13=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item  and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lotno,0) and COLUMNA13=0 and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06))
				end
				UPDATE PUTABLE013 SET COLUMN11=(CAST(@COLUMN13 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
				delete from PUTABLE017 where COLUMN05=@number  and COLUMN13=@COLUMNA02 and COLUMNA03=@COLUMNA03 AND COLUMN06='INVOICE'
			 FETCH NEXT FROM cur4 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur4 
  --EMPHCS892 rajasekhar reddy patakota 8/9/2015 After deletion of invoice account receivable records should delete and status also should change based on the scenario
  if((SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN02=@COLUMN02)>0)
	BEGIN
     declare @BillQty DECIMAL(18,2),@PBillQty DECIMAL(18,2),@IQty DECIMAL(18,2),@IRQty DECIMAL(18,2), @PBQty DECIMAL(18,2),@Result nvarchar(250),@RectNO nvarchar(250)
     SET @BillQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMNA13=0)
     SET @IRQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMNA13=0)
     SET @IQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMNA13=0 )
     SET @RectNO=(SELECT max(COLUMN04) FROM SATABLE010 WHERE COLUMN15 in (@number))
     IF(@IRQty=(@IQty))
     begin
     if(@IRQty=@BillQty)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
     end
     else if(@BillQty=0)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
     end
     else
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
     end
     end
     ELSE
     BEGIN
     if(@IQty=@IRQty)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
     end
     else if(@BillQty=0)
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
     end
     else
     begin
     set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
     end
     end
     UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE009 where COLUMN01= @number))
     declare @invoiceqty decimal(18,2),@issueqty decimal(18,2)
     UPDATE SATABLE010 SET COLUMNA13=1 WHERE COLUMN15 in( select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02 )
     set @invoiceqty=(select sum(isnull(COLUMN10,0)) from SATABLE010 where  COLUMN04=@RectNO  and COLUMNA13=0)
     set @issueqty=(select sum(isnull(COLUMN09,0)) from SATABLE008 where    columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN02 in(@RectNO)))
     if(isnull(@invoiceqty,0)=0.00)
     begin
     UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=27) where COLUMN02=@RectNO
     end
     else if(@invoiceqty=@issueqty)
     begin
     UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=81) where COLUMN02=@RectNO
     end
     else if(@invoiceqty<@issueqty)
     begin
     UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=29) where COLUMN02=@RectNO
     end
	 end
	set @SOID=(select COLUMN01 from SATABLE009 WHERE COLUMN02 = @COLUMN02)  
	DELETE FROM FITABLE036 WHERE COLUMN05 IN(@SOID) AND COLUMN03='INVOICE' AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	set @HID=(Select COLUMN01 from SATABLE009 where COLUMN02=@COLUMN02)
		delete from FITABLE026 where COLUMN05 in(@HID) and COLUMN04='INVOICE' AND COLUMNA02=@COLUMNA02 and  COLUMNA03=@COLUMNA03 AND COLUMN08='1124'
	
END
   --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_SAL_TP_SATABLE009.txt',0
end try
begin catch
--EMPHCS1410	Logs Creation by srinivas		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_SATABLE009.txt',0

return 0
end catch

end
































GO
