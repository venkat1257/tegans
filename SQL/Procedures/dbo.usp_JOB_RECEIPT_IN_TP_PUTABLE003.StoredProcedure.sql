USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JOB_RECEIPT_IN_TP_PUTABLE003]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_JOB_RECEIPT_IN_TP_PUTABLE003]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250),       
	@ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE003_SequenceNo
insert into PUTABLE003 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,
  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  	
set @ReturnValue =(Select COLUMN01 from PUTABLE003 where COLUMN02=@COLUMN02)
END
 

IF @Direction = 'Select'
BEGIN
select * from PUTABLE003
END 

 
IF @Direction = 'Update'
BEGIN
UPDATE PUTABLE003 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,   
   COLUMN17=@COLUMN17,     COLUMN18=@COLUMN18,     COLUMN19=@COLUMN19,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,  COLUMN22=@COLUMN22,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
   COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02

   declare @id nvarchar(250)
--set @id=(select COLUMN01 from PUTABLE003 where COLUMN02 = @COLUMN02)
 
declare @Item nvarchar(250)
DECLARE @MaxRownum INT
DECLARE @Initialrow INT=1
--declare @Qtywip decimal(18,2)
declare @uom nvarchar(250)
set @id=(select COLUMN01 from PUTABLE003 where COLUMN02 = @COLUMN02)
declare @PID nvarchar(250),@TQty decimal(18,2),@Result nvarchar(250),@BQty decimal(18,2),@RecQty decimal(18,2)
declare @POID nvarchar(250),@ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250)
 --declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
 --@Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2)
      set @COLUMN06=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN13=(select COLUMN13 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @POID=(select COLUMN01 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @PID=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE004 where COLUMN02=@id)
             set @COLUMNA03=(select COLUMNA03 from PUTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN08,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             --set @Price=(select isnull(COLUMN10,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @uom=(select COLUMN17 from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 --EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
			 declare @ItemLineID int
			 set @ItemLineID=(select COLUMN26 from PUTABLE004 where COLUMN02=@id)
			    set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @COLUMN06) and COLUMN02=@ItemLineID)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@ItemLineID and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@COLUMN06))
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  columna13=0 and COLUMN03=@Item and COLUMN02=@id) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN09,0)) from PUTABLE004 where   columna13=0 and  COLUMN02=@id and COLUMN03=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				update PUTABLE004 set COLUMN07=@TotalItemQty, COLUMN08=0, COLUMN09=@RemainItemQty where   COLUMN02=@id and COLUMN03=@Item
				SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @RecQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @BQty=(SELECT sum(isnull(COLUMN13,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
--if(@RecQty=@TQty)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
--end
--else 
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
--end
				
--				UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @POID))
				UPDATE PUTABLE004 SET COLUMNA13=1 WHERE COLUMN12=@id
--				declare @PrevQty decimal(18,2)
--				set @PrevQty=(SELECT cast(COLUMN08 as decimal(18,2)) from PUTABLE004  where COLUMN02=@id and COLUMN03=@Item)
--				set @Qtywip=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)
--if(@Qtywip>0)
--begin
--set @Qtywip=(@Qtywip-(@PrevQty1-@ItemQty))
--end
--else
--begin
--set @Qtywip=0.00
--end
--UPDATE FITABLE010 SET COLUMN18=(@Qtywip-@ItemQty)  WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom

			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 

END


else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE003 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
set @id=(select COLUMN01 from PUTABLE003 where COLUMN02 = @COLUMN02)
 --declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
 --@Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2)
      set @COLUMN06=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN13=(select COLUMN13 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @POID=(select COLUMN01 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @PID=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE004 where COLUMN02=@id)
             set @COLUMNA03=(select COLUMNA03 from PUTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN08,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             --set @Price=(select isnull(COLUMN10,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 set @uom=(select COLUMN17 from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
			 --EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
			 set @ItemLineID=(select COLUMN26 from PUTABLE004 where COLUMN02=@id)
			    set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @COLUMN06) and COLUMN02=@ItemLineID)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@ItemLineID and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@COLUMN06))
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  columna13=0 and COLUMN03=@Item and COLUMN02=@id) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN09,0)) from PUTABLE004 where   columna13=0 and  COLUMN02=@id and COLUMN03=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				update PUTABLE004 set COLUMN07=@TotalItemQty, COLUMN08=0, COLUMN09=@RemainItemQty where   COLUMN02=@id and COLUMN03=@Item
				SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @RecQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @BQty=(SELECT sum(isnull(COLUMN13,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
if(@RecQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=04)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end
				
				UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @POID))
				UPDATE PUTABLE004 SET COLUMNA13=1 WHERE COLUMN12=@id
--				declare @PrevQty decimal(18,2)
--				set @PrevQty=(SELECT cast(COLUMN08 as decimal(18,2)) from PUTABLE004  where COLUMN02=@id and COLUMN03=@Item)
--				set @Qtywip=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom)
--if(@Qtywip>0)
--begin
--set @Qtywip=(@Qtywip-(@PrevQty1-@ItemQty))
--end
--else
--begin
--set @Qtywip=0.00
--end
--UPDATE FITABLE010 SET COLUMN18=(@Qtywip-@ItemQty)  WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND COLUMN19=@uom

			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
END

end try
begin catch
BEGIN
DECLARE @tempSTR NVARCHAR(MAX)
	   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_PUTABLE003.txt',0

END
end catch
end







GO
