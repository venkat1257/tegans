USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PROC_TP_GETPayee]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_PROC_TP_GETPayee]

(
    @Type   nvarchar(250)=null, @Party   nvarchar(250)=null, @OPUnit   nvarchar(250)=null, @AcOwner   nvarchar(250)=null
)
	
as
Begin
if(@Party!='')
begin
     IF @Type = '22371'  
    begin                                                                                                                        
		select  COLUMN02,ltrim(rtrim(COLUMN06 +' '+ cast(isnull(COLUMN14,'') as nvarchar(250)))) 'COLUMN05' FROM MATABLE010 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Party) s) AND isnull(COLUMNA13,0)=0 AND  COLUMNA03=@AcOwner and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or COLUMNA02 is null) AND (COLUMN14!='' and COLUMN14!='0')	
	 end
	 IF @Type = '22368'  
    begin
       select  COLUMN02,ltrim(rtrim(COLUMN05 +' '+ cast(isnull(COLUMN11,'') as nvarchar(250)))) 'COLUMN05'FROM SATABLE002   Where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Party) s) and isnull(COLUMNA13,0)=0 AND  COLUMNA03=@AcOwner and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or COLUMNA02 is null) AND (COLUMN11!='' and COLUMN11!='0')
	 end
	  IF @Type = '22369'  
    begin        
		 select  COLUMN02,ltrim(rtrim(COLUMN05 +' '+ cast(isnull(COLUMN12,'') as nvarchar(250)))) COLUMN05 FROM SATABLE001 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Party) s) and  COLUMNA03=@AcOwner  AND (COLUMN12!='' and COLUMN12!='0') and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or COLUMNA02 is null) AND (COLUMN22='' or COLUMN22=22285)
	 end
	   IF @Type = '22370'  
    begin     
		 select  COLUMN02,ltrim(rtrim(COLUMN05 +' '+ cast(isnull(COLUMN12,'') as nvarchar(250)))) COLUMN05 FROM SATABLE001 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Party) s) and isnull(COLUMNA13,0)=0 AND  COLUMNA03=@AcOwner  and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or COLUMNA02 is null) AND (COLUMN12!='' and COLUMN12!='0')  AND COLUMN22=22286
	 end
	 end
	 else
	 begin
	 
     IF @Type = '22371'  
    begin
		select  COLUMN02,ltrim(rtrim(COLUMN06 +' '+ cast(isnull(COLUMN14,'') as nvarchar(250)))) 'COLUMN05' FROM MATABLE010 where  isnull(COLUMNA13,0)=0 AND  COLUMNA03=@AcOwner  AND (COLUMN14!='' and COLUMN14!='0')	
	 end
	 IF @Type = '22368'  
    begin
       select  COLUMN02,ltrim(rtrim(COLUMN05 +' '+ cast(isnull(COLUMN11,'') as nvarchar(250)))) 'COLUMN05'FROM SATABLE002   Where isnull(COLUMNA13,0)=0 AND  COLUMNA03=@AcOwner  AND (COLUMN11!='' and COLUMN11!='0')
	 end
	  IF @Type = '22369'  
    begin        
		 select  COLUMN02,ltrim(rtrim(COLUMN05 +' '+ cast(isnull(COLUMN12,'') as nvarchar(250)))) COLUMN05 FROM SATABLE001 where  COLUMNA03=@AcOwner  AND (COLUMN12!='' and COLUMN12!='0')  AND (COLUMN22='' or COLUMN22=22285)
	 end
	   IF @Type = '22370'  
    begin     
		 select  COLUMN02,ltrim(rtrim(COLUMN05 +' '+ cast(isnull(COLUMN12,'') as nvarchar(250)))) COLUMN05 FROM SATABLE001 where isnull(COLUMNA13,0)=0 AND  COLUMNA03=@AcOwner  AND (COLUMN12!='' and COLUMN12!='0')  AND COLUMN22=22286
	 end
	 end

	 end

GO
