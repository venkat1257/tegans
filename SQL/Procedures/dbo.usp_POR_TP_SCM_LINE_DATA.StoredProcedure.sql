USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_SCM_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_POR_TP_SCM_LINE_DATA]
( @PurchaseOrderID int=null,@IssueID int=null )
AS
BEGIN
	SELECT  
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	b.COLUMN03, b.COLUMN04,b.COLUMN05,((cast(d.COLUMN08 as DECIMAL(18,2)))-max(cast(isnull(b.COLUMN13,0) as DECIMAL(18,2))))COLUMN07,
	b.COLUMN27 COLUMN26,max(b.COLUMN08)COLUMN08,max(b.COLUMN09)COLUMN09,
	cast(((cast(d.COLUMN08 as DECIMAL(18,2)))-max(cast(isnull(b.COLUMN13,0) as DECIMAL(18,2))))* max(cast(d.COLUMN12 as DECIMAL(18,2))) as DECIMAL(18,2))COLUMN24,
	max(b.COLUMN10)COLUMN10,	
	--((max(cast(d.COLUMN08 as int))-max(cast(isnull(b.COLUMN13,0) as int)) )* max(cast(d.COLUMN12 as int))-max(b.COLUMN10))COLUMN11,
	cast((((cast(isnull(m.COLUMN17,0) as DECIMAL(18,2)))*(((cast(d.COLUMN08 as DECIMAL(18,2)))-max(cast(isnull(b.COLUMN13,0) as DECIMAL(18,2))) )* max(cast(d.COLUMN12 as DECIMAL(18,2)))))*0.01)+
	(((cast(d.COLUMN08 as DECIMAL(18,2)))-max(cast(isnull(b.COLUMN13,0) as DECIMAL(18,2))) )* max(cast(d.COLUMN12 as DECIMAL(18,2)))) as DECIMAL(18,2))COLUMN11
	,max(b.COLUMN12) COLUMN12,max(b.COLUMN13) as COLUMN13,b.COLUMN26 COLUMN25,b.COLUMN17
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
,b.COLUMN02 LineID,b.COLUMN38 COLUMN37
	FROM SATABLE005 a inner join SATABLE006 b on a.COLUMN01=b.COLUMN19
	inner join  SATABLE007 c on c.COLUMN06=a.COLUMN02 and isnull(c.COLUMNA13,0)=0 inner join SATABLE008 d on  iif(isnull(d.COLUMN26,0)=0,0,isnull(b.COLUMN02,0))=isnull(d.COLUMN26,0) and d.COLUMN14=c.COLUMN01 and d.COLUMN04=b.COLUMN03 and d.COLUMN19=b.COLUMN27 and iif(d.COLUMN24='',0,isnull(d.COLUMN24,0))=iif(b.COLUMN17='',0,isnull(b.COLUMN17,0)) and isnull(d.COLUMNA13,0)=0
	left join matable013 m on m.COLUMN02=b.COLUMN26  and isnull(m.COLUMNA13,0)=0
	WHERE  c.COLUMN02=@IssueID and isnull(c.COLUMNA13,0)=0 group by b.COLUMN03,b.COLUMN04,b.COLUMN05,b.COLUMN26,b.COLUMN17,b.COLUMN27,m.COLUMN17,d.COLUMN08,b.COLUMN02,b.COLUMN38
	having ((cast(d.COLUMN08 as DECIMAL(18,2)))-max(cast(isnull(b.COLUMN13,0) as DECIMAL(18,2))))>0
END

GO
