USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_proc_GETGSTINBasedTaxes]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_proc_GETGSTINBasedTaxes](@ItemID nvarchar(250)=null,@Customer nvarchar(250)=null,
@opunit nvarchar(250)=null,@OperatingUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null,@Type nvarchar(250)=null,@Price nvarchar(250)=null,@Date nvarchar(250)=null)
AS
BEGIN
set @opunit=(case when @opunit='' then @OperatingUnit else @opunit end)
declare @Tax  nvarchar(250)=null,@HSNTax  nvarchar(250)=null,@Taxids  nvarchar(250)=null
set @HSNTax=(SELECT isnull(COLUMN77,0) FROM MATABLE007 WHERE COLUMN02=@ItemID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
if(@HSNTax!='' and @HSNTax!='0' and @HSNTax!='1000')
SELECT @Tax=(SELECT replace(isnull(COLUMN77,1000),'-','') FROM MATABLE007 WHERE COLUMN02=@ItemID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
else
begin
if(@Type='Purchase')
SELECT @Tax=(SELECT isnull(COLUMN61,1000) FROM MATABLE007 WHERE COLUMN02=@ItemID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
else
SELECT @Tax=(SELECT isnull(COLUMN54,1000) FROM MATABLE007 WHERE COLUMN02=@ItemID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
end

if(cast(@Date as date)>'06/30/17')
begin
set @Price=(case when isnull(cast(@Price as nvarchar(250)),'')='' then '0' else cast(@Price as decimal(18,2)) end)
declare @CompGSTIN  nvarchar(250)=null,@CustGSTIN  nvarchar(250)=null,@ItemHSN  nvarchar(250)=null,
@HSNRATE  nvarchar(250)=null,@UGSTSTCODES  nvarchar(250)=null,@TAXAgency  nvarchar(250)=null,@HSNPricechk  nvarchar(250)=null
SELECT @CompGSTIN=(SELECT SUBSTRING(max(COLUMND02),1,2) FROM CONTABLE007 WHERE COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@opunit) s) and COLUMNA03=@AcOwner and COLUMND02!='')
if(@Type='Purchase')
SELECT @CustGSTIN=(SELECT SUBSTRING(max(COLUMN34),1,2) FROM SATABLE001 WHERE COLUMN02=@Customer and COLUMNA03=@AcOwner and COLUMN34!='')
else
SELECT @CustGSTIN=(SELECT SUBSTRING(max(COLUMN42),1,2) FROM SATABLE002 WHERE COLUMN02=@Customer and COLUMNA03=@AcOwner and COLUMN42!='')

SELECT @ItemHSN=(SELECT isnull(COLUMN75,0) FROM MATABLE007 WHERE COLUMN02=@ItemID and COLUMNA03=@AcOwner and COLUMN75!='')
if(@HSNTax!='' and @HSNTax!='0' and @HSNTax!='1000')
begin
SELECT @Taxids=COLUMN05 FROM MATABLE014 WHERE COLUMN02=@Tax and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
if(@Type='Purchase')
SELECT @HSNRATE=(SELECT sum(isnull(COLUMN17,0)) FROM MATABLE013 WHERE COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',@Taxids) s)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
else
SELECT @HSNRATE=(SELECT sum(isnull(COLUMN07,0)) FROM MATABLE013 WHERE COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',@Taxids) s)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
end
else
SELECT @HSNRATE=(SELECT (case when (isnull(COLUMN08,'')='' or COLUMN08='NIL' or COLUMN08='--' or COLUMN08='---') then 0 else Replace(COLUMN08,'%','') end)COLUMN08 FROM MATABLE032 WHERE COLUMN02=@ItemHSN and isnull(COLUMNA13,0)=0)
SELECT @HSNPricechk=(SELECT COLUMN09 FROM MATABLE032 WHERE COLUMN02=@ItemHSN and isnull(COLUMNA13,0)=0)
if(@HSNPricechk='1' and cast(@Price as decimal(18,2))>0)
begin
SELECT @HSNRATE=(SELECT (case when (isnull(COLUMN08,'')='' or COLUMN08='NIL' or COLUMN08='--' or COLUMN08='---') then 0 else Replace(COLUMN08,'%','') end)COLUMN08 FROM MATABLE033 WHERE COLUMN03=@ItemHSN and (cast(@Price as decimal(18,2))>COLUMN06 and cast(@Price as decimal(18,2))<=isnull(COLUMN07,cast(@Price as decimal(18,2)))) and isnull(COLUMNA13,0)=0)
end
SET @UGSTSTCODES=CONCAT('35',',','04',',','26',',','25',',','31')
if(@HSNRATE!='0' and (isnull(@CompGSTIN,'')!='' and isnull(@CustGSTIN,'')!=''))
begin
if((select count(*) from MATABLE017 where COLUMN06=@CompGSTIN and COLUMN06 in((SELECT ListValue FROM dbo.FN_ListToTable(',',@UGSTSTCODES) s)) )=0 and (select count(*) from MATABLE017 where COLUMN06=@CustGSTIN and COLUMN06 in((SELECT ListValue FROM dbo.FN_ListToTable(',',@UGSTSTCODES) s)) )=0)
begin
if(@CompGSTIN=@CustGSTIN)
begin
SET @TAXAgency=CONCAT('23583',',','23582')
end
else if(@CompGSTIN!=@CustGSTIN)
begin
SET @TAXAgency=('23584')
end
end
else
begin
SET @TAXAgency=CONCAT('23585',',','23582')
end

SELECT COLUMN02,COLUMN04,'' ID FROM MATABLE013 WHERE COLUMN02=1000
UNION ALL
SELECT COLUMN02,COLUMN04,'' ID FROM MATABLE013 WHERE ((case when @Type='Purchase' then ISNULL(COLUMN17,0) else ISNULL(COLUMN07,0) end)=0) and COLUMNA03=@AcOwner and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) and ISNULL(COLUMNA13,0)=0
UNION ALL
SELECT COLUMN02,COLUMN04,iif(COLUMN16='23584','1','') ID FROM MATABLE013 WHERE COLUMN16 in((SELECT ListValue FROM dbo.FN_ListToTable(',',@TAXAgency) s)) and ((case when @Type='Purchase' then ISNULL(COLUMN17,0) else ISNULL(COLUMN07,0) end)=@HSNRATE) and COLUMNA03=@AcOwner and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) and ISNULL(COLUMNA13,0)=0
UNION ALL
select q.COLUMN02,q.COLUMN04,q.ID from(
SELECT CONCAT('-',P.COLUMN02)COLUMN02,'['+P.COLUMN04+']' COLUMN04,'1' ID,sum(isnull(T.COLUMN07,0))COLUMN07,sum(isnull(T.COLUMN17,0))COLUMN17 FROM MATABLE014 P 
INNER JOIN MATABLE013 T on T.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',P.COLUMN05) s)) and (T.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or T.COLUMNA02 is null) AND T.COLUMNA03=P.COLUMNA03 and ISNULL(T.COLUMNA13,0)=0
and T.COLUMN16 in((SELECT ListValue FROM dbo.FN_ListToTable(',',@TAXAgency) s)) WHERE P.COLUMNA03=@AcOwner and (P.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or P.COLUMNA02 is null) and ISNULL(P.COLUMNA13,0)=0  group by P.COLUMN02,P.COLUMN04) q where (case when @Type='Purchase' then q.COLUMN17 else q.COLUMN07 end)=@HSNRATE
END
ELSE
BEGIN
select '-'+COLUMN02 COLUMN02,'['+COLUMN04+']'COLUMN04,(case when COLUMN02=@Tax then 1 else '' end) ID from matable014 	where  (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) AND (COLUMNA03=@AcOwner or COLUMNA03 is null) and isnull(COLUMNA13,0)=0
UNION ALL
SELECT COLUMN02,COLUMN04,(case when COLUMN02=@Tax then 1 else '' end) ID FROM MATABLE013 WHERE (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) and (COLUMNA03=@AcOwner or COLUMNA03 is null) and ISNULL(COLUMNA13,0)=0
END
END
ELSE if(@Date='')
BEGIN
select '-'+COLUMN02 COLUMN02,'['+COLUMN04+']'COLUMN04,(case when COLUMN02=@Tax then 1 else '' end) ID from matable014 	where  (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) AND (COLUMNA03=@AcOwner or COLUMNA03 is null) and isnull(COLUMNA13,0)=0
UNION ALL
SELECT COLUMN02,COLUMN04,(case when COLUMN02=@Tax then 1 else '' end) ID FROM MATABLE013 WHERE (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) and (COLUMNA03=@AcOwner or COLUMNA03 is null) and ISNULL(COLUMNA13,0)=0
END
ELSE
BEGIN
SELECT COLUMN02,COLUMN04,(case when COLUMN02=@Tax then 1 else '' end) ID FROM MATABLE013 WHERE (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) or COLUMNA02 is null) and (COLUMNA03=@AcOwner or COLUMNA03 is null) and ISNULL(COLUMNA13,0)=0 and (COLUMN16 not in(23582,23584,23583,23585,23586) or COLUMN02=1000)
END

END

GO
