USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_FI_BL_JOURNALLEDGER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_FI_BL_JOURNALLEDGER]

(

    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  

	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  

	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null, @COLUMN10   nvarchar(250)=null,

	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,   
	--EMPHCS1335 Adding Project Field in Journal Entry and also in show project column in GLView of Chart of Accounts BY RAJ.Jr
	@COLUMN14   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,  

    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   

	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  

	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  

	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  

	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null, 

    @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  

	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  

	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   

	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  

	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,

    @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  

	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250),       
        --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@ReturnValue int=null OUTPUT

)

AS

BEGIN
begin TRY 
BEGIN
IF  @TabelName='FITABLE031'

BEGIN

set @COLUMNA02=( CASE WHEN (@COLUMN10!= '' and @COLUMN10 is not null and @COLUMN10!= '0' ) THEN @COLUMN10 else @COLUMNA02  END )

EXEC [USP_FI_TP_FITABLE031] 

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, @COLUMN10, @COLUMN11, 
   --EMPHCS1335 Adding Project Field in Journal Entry and also in show project column in GLView of Chart of Accounts BY RAJ.Jr
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMNA01, @COLUMNA02, 

   @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, 

   @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, 

   @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, 
   --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue OUTPUT
SELECT @ReturnValue
END


ELSE IF  @TabelName='FITABLE032'

BEGIN

EXEC [USP_FI_TP_FITABLE032] 

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, @COLUMN10, @COLUMN11, 

   @COLUMN12,  @COLUMN13, @COLUMN14,  @COLUMNA01, @COLUMNA02, 

   @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, 

   @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, 

   @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, 

   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue

END
end
END try
begin CATCH
BEGIN


SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage


END
end catch
end

GO
