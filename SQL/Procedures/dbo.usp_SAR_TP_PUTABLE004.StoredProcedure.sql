USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_PUTABLE004]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAR_TP_PUTABLE004]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null, 
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
    @COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
    --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null, 
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null, @LotData  XML=null,
	@Result nvarchar(250)=null,      @VendorID nvarchar(250)=null,    @ReceiptType nvarchar(250)=null
)
AS
BEGIN
begin try
--EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
set @COLUMN17=(select iif((@COLUMN17!='' and @COLUMN17>0),@COLUMN17,(select max(column02) from fitable037 where column07=1 and column08=1)))
IF @Direction = 'Insert'
BEGIN
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
declare @poid nvarchar(250)=null, @date nvarchar(250)=null, @FRMID nvarchar(250)=null,@location nvarchar(250)=null,@Project nvarchar(250)=null
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
--set @COLUMN12 =(select MAX(COLUMN01) from  PUTABLE003);
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE004_SequenceNo
set @date= (SELECT COLUMN09 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
set @COLUMN13 =(select COLUMN13 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN14 =(select COLUMN14 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN15 =(select COLUMN15 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN16 =(select COLUMN16 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @ReceiptType =(select COLUMN17 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @Project =(select COLUMN21 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @poid =(select COLUMN06 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMNA02=( CASE WHEN (@COLUMN12!= '' and @COLUMN12 is not null and @COLUMN12!= '0' ) THEN
 (select COLUMN13 from PUTABLE003 where COLUMN01=@COLUMN12) else @COLUMNA02  END )
set @location =(select COLUMN22 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @Location = (case when (cast(isnull(@COLUMN28,'') as nvarchar(250))!='' and @COLUMN28!=0) then @COLUMN28 else @Location end)
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare  @lotno nvarchar(250)=null,@uom nvarchar(250)
set @uom =(case when @column17='' then 0 when isnull(@column17,0)=0 then 0  else @column17 end)
set @lotno=(case when @column24='' then 0 when isnull(@column24,0)=0 then 0  else @column24 end)
set @COLUMN09 =(CAST(@COLUMN09 AS DECIMAL(18,2))- CAST(@COLUMN08 AS DECIMAL(18,2)))
insert into PUTABLE004 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN28, 
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02,
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,    (CAST(@COLUMN08 AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))), @COLUMN08,
   @COLUMN09,  @COLUMN10,  @COLUMN11,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17, @COLUMN24,   @COLUMN25,  @COLUMN26,  @COLUMN28,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07,@COLUMNA08 , @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN26,0)>0)
begin
set @COLUMN07=(select sum(isnull(COLUMN08,0)) from PUTABLE004 where columna13=0 and COLUMN26=@COLUMN26)
end
else 
begin
set @COLUMN07=( (select sum(isnull(COLUMN08,0)) from PUTABLE004 where   COLUMN03=@COLUMN03 and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno and columna13=0 and  COLUMN12 in(select COLUMN01 from  PUTABLE003 where COLUMN06 in(@poid))))
end
--set @COLUMN07=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2)))
update PUTABLE004 set COLUMN07=@COLUMN07 where column02=@COLUMN02

if exists(select column01 from putable004 where column12=@column12)
		begin	
		--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @lIID nvarchar(250),@receiptno nvarchar(250)
set @lIID=(select COLUMN01 from PUTABLE004 WHERE COLUMN02=@COLUMN02)	
set @receiptno=(select COLUMN04 from  PUTABLE003 Where COLUMN01=@COLUMN12)	
if(@LotData is not null and datalength(@LotData)>0)
begin
EXEC usp_PUR_TP_InsertLOT @LotData,'RETURN RECEIPT',@receiptno,@lIID,@COLUMN03,@COLUMNA02,@COLUMNA03
end
	declare @PID int, @TQty decimal(18,2),@RecQty decimal(18,2),@BQty decimal(18,2),@RRQty decimal(18,2)
SET @PID=(SELECT COLUMN06 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
SET @RecQty=(SELECT sum(COLUMN12) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
SET @BQty=(SELECT sum(COLUMN13) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN26,0)>0)
begin
set @COLUMN07=(select sum(isnull(COLUMN12,0)) from PUTABLE002 where columna13=0 and COLUMN02=@COLUMN26)
UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where isnull(COLUMNA13,0)=0 and COLUMN02=@COLUMN26
end
else 
begin
set @COLUMN07=(select sum(isnull(COLUMN12,0)) from PUTABLE002 where columna13=0 and isnull(COLUMN26,0)=isnull(@COLUMN17,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and COLUMN19= (select COLUMN01 from PUTABLE001 where COLUMN02= @PID) and COLUMN03=@COLUMN03)
UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=isnull(@COLUMN17,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and isnull(COLUMNA13,0)=0 and COLUMN19=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))
end
UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) 
WHERE COLUMN14=@COLUMN03 and COLUMN02 in (select COLUMN01 from PUTABLE001 where COLUMN02 in (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))
IF(@TQty=(SELECT sum(COLUMN08) FROM PUTABLE004 WHERE  columna13=0 and COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN06=@PID) ))
begin
if(@TQty=@BQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@BQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=104)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=105)
end
end

ELSE 
BEGIN
if(@TQty=@BQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@BQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=102)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=103)
end
end
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))
UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=107) where COLUMN01=@COLUMN12	
set @COLUMN05= (@date);
declare @activetranid nvarchar(250)=null, @activetranno nvarchar(250)=null
set @activetranid=(select COLUMN01 from  PUTABLE003 where COLUMN01=@COLUMN12)
    set @activetranno=(select COLUMN06 from  PUTABLE003 where COLUMN01=@COLUMN12);
set @COLUMN20 =(select COLUMN13 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN21 =(select COLUMN14 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN22 =(select COLUMN15 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN23 =(select COLUMN16 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
insert into PUTABLE011 
( 
COLUMN02,COLUMN03,  COLUMN04, COLUMN05, COLUMN07,COLUMN08, COLUMN10, COLUMN15,  COLUMN16,COLUMN17,COLUMN18,COLUMN19,  COLUMN20 ,COLUMN21, COLUMN22 ,COLUMN23,
COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
@COLUMN12,   'RETURN RECEIPT',   @activetranno,  @COLUMN05,@COLUMN06,@COLUMN08,@COLUMN05, '2','2',@COLUMN10,@COLUMN11,@COLUMN02,  @COLUMN20 ,@COLUMN21, @COLUMN22 ,@COLUMN23,
@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
@COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
@COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
@COLUMND08, @COLUMND09, @COLUMND10
)  
declare @Qty_On_Hand decimal(18,2)
declare @Qty_On_Hand1 decimal(18,2)
declare @Qty_Order decimal(18,2)
declare @Qty_Avl decimal(18,2)
declare @Qty_Cmtd decimal(18,2),@AvgPrice decimal(18,2), @PRATE DECIMAL(18,2),@ASSET DECIMAL(18,2)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@COLUMN10,0))
set @column17=(select iif((@column17!='' and @column17>0),@column17,(select max(column02) from fitable037 where column07=1 and column08=1)))
declare @TrackQty bit
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
if(@LotData is not null  and datalength(@LotData)>0)
begin
exec usp_PUR_TP_InventoryLOT @receiptno,@lIID,@COLUMN03,@COLUMN08,@COLUMNA02,@uom,@COLUMNA03,'Insert',@location,@lotno
end
else
begin
 set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
 --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno)
begin
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as decimal(18,2))+ cast(@COLUMN08 as decimal(18,2)));
set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom   AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)))
if(@Qty_Order>0 and @Qty_Order>cast(@COLUMN08 as decimal(18,2)))
Begin
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@COLUMN08 as decimal(18,2)));
end
else
begin
set @Qty_Order = 0;
end
set @Qty_Avl = cast(@Qty_Cmtd as decimal(18,2));
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN07=@Qty_Order, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+((cast(@COLUMN08 as decimal(18,2)))*(cast(@AvgPrice as decimal(18,2))))) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
UPDATE FITABLE010 SET COLUMN17=cast(((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)))/@Qty_On_Hand)as decimal(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
end
end
else
begin
declare @newIID int
declare @tmpnewIID1 int 
			set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
SET @AvgPrice=(ISNULL(@COLUMN10,0))
--set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
SET @ASSET=(CAST(ISNULL(@COLUMN08,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03
)
values
(
  @newIID,@COLUMN03,@COLUMN08,@COLUMN08, @ASSET,@AvgPrice,@uom,  @lotno, @location,@COLUMN13,@COLUMNA02,@COLUMNA03 )
end
END
end
declare @memo nvarchar(250) 
SET @VendorID=(SELECT COLUMN05 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
SET @memo=(SELECT COLUMN12 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
declare @newID int
declare @tmpnewID1 int 
			set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @newID=10000
end
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@COLUMN10,0))
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN RECEIPT' and COLUMN04=@receiptno and COLUMN05=@lIID and COLUMN06=@COLUMN03 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
begin
set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000) and isnull(COLUMN21,0)=@Location and COLUMN22 in(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN RECEIPT' and COLUMN04=@receiptno and COLUMN05=@lIID and COLUMN06=@COLUMN03 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03))
end
else
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000)  AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0))
end
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
SET @ASSET=(CAST(ISNULL(@COLUMN08,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
	set @COLUMN06='Return Receipt';set @COLUMN08= 'Inventory Received';
	if(@TrackQty=1)
	begin
	--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system	
insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  --EMPHCS1578	Inventory Assert report price is taking purchase price insested price in Bill Price BY RAJ.JR
    @newID,'1000', @date ,@COLUMN12,@COLUMN06, @VendorID,@COLUMN08,@memo,@ASSET,@ASSET, @COLUMNA02,@Project,
	@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
    @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
    --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
    @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lIID, @COLUMND07,
    @COLUMND08, @COLUMND09, @COLUMND10
)
    END
--update  PUTABLE017 set
--COLUMN06='Item Recipet',COLUMN08= 'Inventory Received and Not Billed'   WHERE COLUMN05=(select COLUMN01 from PUTABLE003 where  COLUMN04 LIKE 'IR%' AND COLUMN01=@COLUMN12)
--update  PUTABLE017 set
--COLUMN06='JobOrder Recipet',COLUMN08= 'Inventory Received and Not Billed'   WHERE COLUMN05=(select COLUMN01 from PUTABLE003 where  COLUMN04 LIKE 'JR%' AND COLUMN01=@COLUMN12)
set @ReturnValue = 1
END
else
begin
return 0
end
END

IF @Direction = 'Select'
BEGIN
select * from PUTABLE004
END 

IF @Direction = 'Update'
BEGIN
declare @PORecQty nvarchar(250),@ItemRecQty nvarchar(250)
set @COLUMN13 =(select COLUMN13 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @date= (SELECT COLUMN09 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
set @COLUMN14 =(select COLUMN14 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN15 =(select COLUMN15 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN16 =(select COLUMN16 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @poid =(select COLUMN06 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @Project =(select COLUMN21 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMNA02=( CASE WHEN (@COLUMN12!= '' and @COLUMN12 is not null and @COLUMN12!= 0 ) THEN (select COLUMN13 from PUTABLE003 
  where COLUMN01=@COLUMN12) else @COLUMNA02  END )
UPDATE PUTABLE004 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06, 
   COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,   
   COLUMN17=@COLUMN17,     COLUMN18=@COLUMN17,     COLUMN19=@COLUMN08,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN22=@COLUMN22,     COLUMN23=@COLUMN23,     COLUMN24=@COLUMN24,     COLUMN25=@COLUMN25,     COLUMN26=@COLUMN26, 
   COLUMN28=@COLUMN28,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
    COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02

set @location =(select COLUMN22 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @Location = (case when (cast(isnull(@COLUMN28,'') as nvarchar(250))!='' and @COLUMN28!=0) then @COLUMN28 else @Location end)
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @uom =(case when @column17='' then 0 when isnull(@column17,0)=0 then 0  else @column17 end)
set @lotno=(case when @column24='' then 0 when isnull(@column24,0)=0 then 0  else @column24 end)
set @lIID=(select COLUMN01 from PUTABLE004 WHERE COLUMN02=@COLUMN02)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN26,0)>0)
begin
set @PORecQty=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN02=@COLUMN26 and columna13=0)
UPDATE PUTABLE002 SET   COLUMN12=( CAST(@COLUMN08 AS decimal(18,2))+CAST(@PORecQty AS decimal(18,2))) where COLUMN02=@COLUMN26 and columna13=0
set @ItemRecQty=(select max(isnull(COLUMN07,0)) from PUTABLE004 where  COLUMN26=@COLUMN26 and columna13=0)
set @COLUMN07=(select sum(isnull(COLUMN08,0)) from PUTABLE004 where COLUMN26=@COLUMN26 and columna13=0)
end
else 
begin
set @PORecQty=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @poid) and COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=isnull(@COLUMN17,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and columna13=0)
UPDATE PUTABLE002 SET   COLUMN12=( CAST(@COLUMN08 AS decimal(18,2))+CAST(@PORecQty AS decimal(18,2))) where columna13=0 and COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=isnull(@COLUMN17,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@poid))
set @ItemRecQty=( (select max(isnull(COLUMN07,0)) from PUTABLE004 where   COLUMN03=@COLUMN03 and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno and isnull(COLUMNA13,0)=0 and COLUMN12 in(select COLUMN01 from  PUTABLE003 where COLUMN06 in(@poid))))
set @COLUMN07=( (select sum(isnull(COLUMN08,0)) from PUTABLE004 where   COLUMN03=@COLUMN03 and isnull(COLUMN17,0)=isnull(@COLUMN17,0) and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno and columna13=0 and  COLUMN12 in(select COLUMN01 from  PUTABLE003 where COLUMN06 in(@poid))))
end

set @ItemRecQty=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@ItemRecQty AS decimal(18,2)))
set @COLUMN09 =(CAST(@COLUMN06 AS decimal(18,2))- (CAST(@PORecQty AS decimal(18,2))+ CAST(@COLUMN08 AS decimal(18,2))))
update PUTABLE004 set COLUMN07=@COLUMN07,COLUMN09=@COLUMN09 where column02=@COLUMN02
UPDATE PUTABLE013 SET COLUMN09=@ItemRecQty WHERE COLUMN14=@COLUMN03 and COLUMN02 in (select COLUMN01 from PUTABLE001 where COLUMN02 in (@poid))
SET @PID=(SELECT COLUMN06 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
--EMPHCS854 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in item receipt
SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
SET @RecQty=(SELECT sum(COLUMN12) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
SET @BQty=(SELECT sum(COLUMN13) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
--EMPHCS854 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in item receipt
IF(@TQty=(SELECT sum(COLUMN08) FROM PUTABLE004 WHERE  columna13=0 and  COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN06=@PID) ))
begin
if(@TQty=@BQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@BQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=104)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=105)
end
end

ELSE 
BEGIN
if(@TQty=@BQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@BQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=102)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=103)
end
end
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))
UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=107) where COLUMN01=@COLUMN12	
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @receiptno=(select COLUMN04 from  PUTABLE003 Where COLUMN01=@COLUMN12)	
if(@LotData is not null and datalength(@LotData)>0)
begin
EXEC usp_PUR_TP_InsertLOT @LotData,'RETURN RECEIPT',@receiptno,@lIID,@COLUMN03,@COLUMNA02,@COLUMNA03
end
else
begin 
update FITABLE038 set COLUMNA13=0 where COLUMN04=@receiptno and COLUMN06=@COLUMN03 and COLUMN05=@lIID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
set @column17=(select iif((@column17!='' and @column17>0),@column17,(select max(column02) from fitable037 where column07=1 and column08=1)))

set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN RECEIPT' and COLUMN04=@receiptno and COLUMN05=@lIID and COLUMN06=@COLUMN03 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
begin
exec usp_PUR_TP_InventoryLOT @receiptno,@lIID,@COLUMN03,@COLUMN08,@COLUMNA02,@column17,@COLUMNA03,'Insert',@location,@lotno
end
else
begin
--EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )
--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )
--if(@Qty_On_Hand1>=0)
begin
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as DECIMAL(18,2))+ cast(@COLUMN08 as DECIMAL(18,2)));
set @Qty_Cmtd=(cast(@Qty_On_Hand as DECIMAL(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as DECIMAL(18,2))-cast(@COLUMN08 as DECIMAL(18,2)))
if(@Qty_Order>0 or @Qty_Order>cast(@COLUMN08 as DECIMAL(18,2)))
Begin
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as DECIMAL(18,2))- cast(@COLUMN08 as DECIMAL(18,2)));
end
else
begin
set @Qty_Order = 0;
end
set @Qty_Avl = cast(@Qty_Cmtd as DECIMAL(18,2));
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@COLUMN10,0))
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
SET @ASSET=(CAST(ISNULL(@COLUMN08,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN07=@Qty_Order, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as DECIMAL(18,2))+((cast(@COLUMN08 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno 
--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno )as DECIMAL(18,2))/cast(@Qty_On_Hand as DECIMAL(18,2))) as DECIMAL(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location  AND isnull(COLUMN22,0)=@lotno 
end
end
else
begin
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
SET @PRATE=(ISNULL(@COLUMN10,0))
--set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
--IF(@AvgPrice<=0)BEGIN SET @PRATE=(@PRATE)END
SET @ASSET=(CAST(ISNULL(@COLUMN08,0)AS DECIMAL(18,2))*CAST(ISNULL(@PRATE,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03
)
values
(
  @newIID,@COLUMN03,@COLUMN08,@COLUMN08, @ASSET,@PRATE,@uom,  @lotno, @location,@COLUMN13,@COLUMNA02,@COLUMNA03 )
end
END
end
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
set @COLUMN05= (@date);
    set @activetranid=(select COLUMN01 from  PUTABLE003 where COLUMN01=@COLUMN12)
    set @activetranno=(select COLUMN06 from  PUTABLE003 where COLUMN01=@COLUMN12);
    set @COLUMN20 =(select COLUMN13 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
    set @COLUMN21 =(select COLUMN14 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
    set @COLUMN22 =(select COLUMN15 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
    set @COLUMN23 =(select COLUMN16 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
	
insert into PUTABLE011 
( 
   COLUMN02,COLUMN03,  COLUMN04, COLUMN05, COLUMN07,COLUMN08, COLUMN10, COLUMN15,  COLUMN16,  COLUMN20 ,COLUMN21, COLUMN22 ,COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
   @activetranid,  'Return Receipt',   @activetranno,  @COLUMN05,@COLUMN06,@COLUMN08,@COLUMN05, '2','2',  @COLUMN20 ,@COLUMN21, @COLUMN22 ,@COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)

SET @VendorID=(SELECT COLUMN05 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
SET @memo=(SELECT COLUMN12 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @newID=10000
end
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@COLUMN10,0))
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN RECEIPT' and COLUMN04=@receiptno and COLUMN05=@lIID and COLUMN06=@COLUMN03 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
begin
set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000) and isnull(COLUMN21,0)=@Location and COLUMN22 in(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN RECEIPT' and COLUMN04=@receiptno and COLUMN05=@lIID and COLUMN06=@COLUMN03 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03))
end
else
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=ISNULL(@uom,10000)  AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0))
end
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
SET @ASSET=(CAST(ISNULL(@COLUMN08,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
set @COLUMN06='Return Receipt';set @COLUMN08= 'Inventory Received';

	
--EMPHCS1480 - service item with track quantity on hand IS equal to false creating inventory and inventory asset BY GNANESHWAR ON 2/1/2016
	if(@TrackQty=1)
begin
insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  --EMPHCS1578	Inventory Assert report price is taking purchase price insested price in Bill Price BY RAJ.JR
    @newID,'1000', @date ,@COLUMN12,@COLUMN06, @VendorID,@COLUMN08,@memo,@ASSET,@ASSET,	@COLUMNA02,@Project,
	@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
    @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
    --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	@COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lIID, @COLUMND07,
	@COLUMND08, @COLUMND09, @COLUMND10
)
END
END

else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE004 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END

end try
begin catch
	if not exists(select column01 from putable004 where column12=@column12)
		begin
			delete from putable003 where column01=@column12
return 0
		end
return 0
DECLARE @tempSTR NVARCHAR(MAX)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_PUTABLE004.txt',0

end catch
end







GO
