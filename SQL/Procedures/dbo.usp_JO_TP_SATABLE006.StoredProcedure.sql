USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE006]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE006] 
(@COLUMN02      NVARCHAR(250),        @COLUMN03      NVARCHAR(250)=NULL,     @COLUMN04      NVARCHAR(250)= NULL, 
 @COLUMN05      NVARCHAR(250)=NULL,   @COLUMN06      NVARCHAR(250)= NULL,    @COLUMN07      NVARCHAR(250)=NULL , 
 @COLUMN08      NVARCHAR(250)= NULL,  @COLUMN09      NVARCHAR(250)=NULL,     @COLUMN10      NVARCHAR(250)= NULL, 
 @COLUMN11      NVARCHAR(250)=NULL ,  @COLUMN12      NVARCHAR(250)= NULL,    @COLUMN13      NVARCHAR(250)=NULL , 
 @COLUMN14      NVARCHAR(250)= NULL,  @COLUMN15      NVARCHAR(250)=NULL ,    @COLUMN16      NVARCHAR(250)= NULL, 
 @COLUMN17      NVARCHAR(250)=NULL,   @COLUMN18      NVARCHAR(250)= NULL,    @COLUMN19      NVARCHAR(250)=NULL, 
 @COLUMN20      NVARCHAR(250)= NULL,  @COLUMN21      NVARCHAR(250)=NULL,     @COLUMN22      NVARCHAR(250)= NULL, 
 @COLUMN23      NVARCHAR(250)=NULL,   @COLUMN24      NVARCHAR(250)= NULL,    @COLUMN25      NVARCHAR(250)=NULL , 
 @COLUMN26      NVARCHAR(250)=NULL,   @COLUMN27      NVARCHAR(250)=NULL,     @COLUMN28      NVARCHAR(250)=NULL, 
 @COLUMN29      NVARCHAR(250)=NULL,   @COLUMN30      NVARCHAR(250)=NULL,     @COLUMN31      NVARCHAR(250)=NULL, 
  @COLUMN32      NVARCHAR(250)=NULL,   @COLUMN33      NVARCHAR(250)=NULL,     @COLUMN34      NVARCHAR(250)=NULL, 
 @COLUMNA01     VARCHAR(100)=NULL,    @COLUMNA02     VARCHAR(100)=NULL,      @COLUMNA03     VARCHAR(100)=NULL, 
 @COLUMNA04     VARCHAR(100)=NULL,    @COLUMNA05     VARCHAR(100)=NULL,      @COLUMNA06     NVARCHAR(250)=NULL, 
 @COLUMNA07     NVARCHAR(250)=NULL,   @COLUMNA08     VARCHAR(100)=NULL,      @COLUMNA09     NVARCHAR(250)=NULL, 
 @COLUMNA10     NVARCHAR(250)= NULL,  @COLUMNA11     VARCHAR(100)=NULL,      @COLUMNA12     NVARCHAR(250)=NULL , 
 @COLUMNA13     NVARCHAR(250)= NULL,  @COLUMNB01     NVARCHAR(250)=NULL,     @COLUMNB02     NVARCHAR(250)= NULL, 
 @COLUMNB03     NVARCHAR(250)=NULL ,  @COLUMNB04     NVARCHAR(250)= NULL,    @COLUMNB05     NVARCHAR(250)=NULL, 
 @COLUMNB06     NVARCHAR(250)= NULL,  @COLUMNB07     NVARCHAR(250)=NULL ,    @COLUMNB08     NVARCHAR(250)= NULL, 
 @COLUMNB09     NVARCHAR(250)=NULL,   @COLUMNB10     NVARCHAR(250)= NULL,    @COLUMNB11     VARCHAR(100)=NULL, 
 @COLUMNB12     VARCHAR(100)=NULL,    @COLUMND01     NVARCHAR(250)=NULL ,    @COLUMND02     NVARCHAR(250)=NULL, 
 @COLUMND03     NVARCHAR(250)=NULL ,  @COLUMND04     NVARCHAR(250)= NULL,    @COLUMND05     NVARCHAR(250)=NULL , 
 @COLUMND06     NVARCHAR(250)= NULL,  @COLUMND07     NVARCHAR(250)=NULL ,    @COLUMND08     NVARCHAR(250)=NULL, 
 @COLUMND09     NVARCHAR(250)=NULL ,  @COLUMND10     VARCHAR(100)=NULL,      @Direction     NVARCHAR(250), 
 @TabelName     NVARCHAR(250)=NULL ,  @ReturnValue   NVARCHAR(250)= NULL,    @UOMData       XML=null 
 ,@TComitData    decimal(18,2)=NULL, 
 @ComitData     decimal(18,2) =NULL,  @WIP           decimal(18,2) =NULL,    @QOH           decimal(18,2) =NULL, 
 @Qty_Avl       decimal(18,2)=NULL,   @BackOrderData decimal(18,2)=NULL) 
AS 
  BEGIN 
      BEGIN try 
     
	  set @COLUMN27=(select iif((@COLUMN27!='' and @COLUMN27>0),@COLUMN27,(select max(column02) from fitable037 where column07=1 and column08=1)))
          IF @Direction = 'Insert' 
            BEGIN
declare @FRMID int,@JobOrder nvarchar(250)
set @JobOrder=@COLUMN04
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @FRMID= (SELECT COLUMN03 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
--set @COLUMN19= (SELECT COLUMN01 FROM SATABLE005 WHERE COLUMN01=(SELECT MAX(COLUMN01) FROM SATABLE005))
set @COLUMN20= (SELECT COLUMN24 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN21= (SELECT COLUMN25 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN22= (SELECT COLUMN26 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN23= (SELECT COLUMN27 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
 set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19);

   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0 ) THEN (select COLUMN24 from SATABLE005
  where COLUMN01=@COLUMN19) else @COLUMNA02  END )
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE006_SequenceNo
insert into SATABLE006 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   
   COLUMN24,   COLUMN25,COLUMN26, COLUMN27,COLUMN28,   COLUMN29,COLUMN30, COLUMN31,COLUMN33, COLUMN34, COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @JobOrder,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
  
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25, @COLUMN26, @COLUMN27, @COLUMN27,  @COLUMN07, @COLUMN30, @COLUMN31,@COLUMN33, @COLUMN34,@COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10,
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08,
   @COLUMND09, @COLUMND10
) 
if exists(select column19 from SATABLE006 where column19=@column19)
					begin 
 declare @uomselection nvarchar(250)
declare @TrackQty bit
declare @Type nvarchar(250)
 set @COLUMN15=(SELECT CAST(GETDATE() AS DATE));
 set @COLUMN02=(select COLUMN01 from SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@COLUMN19);
      
	set @ComitData=(select sum(isnull(COLUMN05,0)) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27)
	set @WIP=(select sum(isnull(COLUMN18,0)) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27)
	set @QOH=(select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27)
	set @BackOrderData=(select sum(isnull(COLUMN06,0)) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27)

	set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN03 )
		set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
		if(@uomselection=1 or @uomselection='1' or @uomselection='True')
	begin
	EXEC usp_PUR_TP_InsertUOM @UOMData,'JOBORDER',@COLUMN04,@COLUMN19,@COLUMN03,@COLUMNA02,@COLUMNA03
	end
	else
	begin
	declare @col2 int
	  set @col2=(isnull((select max(cast(COLUMN02 as int)) from FITABLE038),999))
	  set @col2=((@col2)+1)
	INSERT INTO FITABLE038(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13) 
	               values(@col2,'JOBORDER',@COLUMN04,@COLUMN19,@COLUMN03,@COLUMN34,@COLUMN33,@COLUMN09, @COLUMNA02, @COLUMNA03, @COLUMNA12, @COLUMNA13 )
	end

if(@ComitData>=0)
	begin
		if(@QOH<cast(@COLUMN07  as decimal(18,2)))
			begin
				set @WIP=@WIP+cast(@COLUMN07  as decimal(18,2))
				set @TComitData=@ComitData+cast(@COLUMN07  as decimal(18,2))
				set @BackOrderData=(cast(@COLUMN07  as decimal(18,2))-cast(@QOH  as decimal(18,2)))+cast(@BackOrderData  as decimal(18,2))
                set @Qty_Avl=0
				if(@FRMID=1283)
					begin
					      
						update  FITABLE010 set
						COLUMN18=0.00 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
					end
				else
					begin
					
						update  FITABLE010 set
						COLUMN05=@TComitData,COLUMN06=@BackOrderData ,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
					end
			end
		else
			begin
				set @WIP=cast(@WIP  as decimal(18,2))+cast(@COLUMN07  as decimal(18,2))
		set @TComitData=cast(@ComitData  as decimal(18,2))+cast(@COLUMN07  as decimal(18,2))
		set @Qty_Avl=(@QOH)
		if(@Qty_Avl<0)
		begin
		set @Qty_Avl=0
		set @BackOrderData=(cast(@COLUMN07  as decimal(18,2))-cast(@QOH  as decimal(18,2)))+cast(@BackOrderData  as decimal(18,2))
		end
				if(@FRMID=1283)
					begin
					      
						update  FITABLE010 set
						COLUMN18=0.00 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
					end
				else
					begin
						
						update  FITABLE010 set
						COLUMN05=@TComitData,COLUMN06= @BackOrderData,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
					end
	end
end
else
	begin set @TComitData=cast(@COLUMN07  as decimal(18,2))
		set @Qty_Avl=(@QOH)
		
		update  FITABLE010 set
			COLUMN05=@TComitData,COLUMN06=@BackOrderData,COLUMN08= @Qty_Avl where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
end
--EMPHCS1049 rajasekhar reddy patakota 29/08/2015 Job Oder Functionality Changes
insert into PUTABLE013 
( 
   COLUMN02,  COLUMN03,   COLUMN04,  COLUMN05,  COLUMN08,  COLUMN13,  COLUMN14,  COLUMN15,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
   COLUMND09, COLUMND10
)
values
(  
   @COLUMN19,  'Job Order',@COLUMN04,  @COLUMN06,   @COLUMN07,    @COLUMN14,    @COLUMN03,  @COLUMN15,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, 
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, 
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05,
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
set @ReturnValue = 1
end
else
begin
return 0
end
END
          ELSE IF @Direction = 'Select' 
            BEGIN 
                SELECT * 
                FROM   satable006 
            END 
          ELSE IF @Direction = 'Update' 
            BEGIN 
                DECLARE @IQTY decimal(18,2)
                DECLARE @OredrType nvarchar(250)
		--EMPHCS1049 rajasekhar reddy patakota 29/08/2015 Job Oder Functionality Changes
   set @FRMID= (SELECT COLUMN03 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
   if  exists( SELECT 1 FROM SATABLE006 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
   begin
   set @COLUMN19 =(select COLUMN19 from  SATABLE006 Where COLUMN02=@COLUMN02 );
   end
   SET @IQTY=(select isnull(COLUMN07,0) from  SATABLE006 Where COLUMN02=@COLUMN02)
   set @COLUMN20 =(select COLUMN24 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMN21 =(select COLUMN25 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMN22 =(select COLUMN26 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMN23 =(select COLUMN27 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @OredrType =(select COLUMN29 from  SATABLE005 Where COLUMN01=@COLUMN19 );
   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0) THEN 
   (select COLUMN24 from SATABLE005 where COLUMN01=@COLUMN19) else @COLUMNA02  END ) 
if not exists( SELECT 1 FROM SATABLE006 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
begin
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE006_SequenceNo

                      INSERT INTO SATABLE006
                         (COLUMN02, COLUMN03, COLUMN04, COLUMN05, COLUMN06, COLUMN07, COLUMN08, COLUMN09, COLUMN10, COLUMN11, COLUMN12, COLUMN13, 
                         COLUMN14, COLUMN15, COLUMN16, COLUMN17, COLUMN18, COLUMN19, COLUMN20, COLUMN21, COLUMN22, COLUMN23, COLUMN24, COLUMN25, 
                        
		         COLUMN26,COLUMN27,COLUMN28,  COLUMN29, COLUMN30, COLUMN31,COLUMN33, COLUMN34, COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, 
                         COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
                         COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, 
                         COLUMND08, COLUMND09, COLUMND10)
VALUES        (@COLUMN02,@COLUMN03,@COLUMN04,@COLUMN05,@COLUMN06,@COLUMN07,@COLUMN08,@COLUMN09,@COLUMN10,@COLUMN11,@COLUMN12,@COLUMN13,@COLUMN14,@COLUMN15,@COLUMN16,@COLUMN17,@COLUMN18,@COLUMN19,@COLUMN20,@COLUMN21,@COLUMN22,@COLUMN23,@COLUMN24,@COLUMN25,@COLUMN26,@COLUMN27,@COLUMN27,  @COLUMN07, @COLUMN30, @COLUMN31, @COLUMN33, @COLUMN34,@COLUMNA01,@COLUMNA02,@COLUMNA03,@COLUMNA04,@COLUMNA05,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMNA09,@COLUMNA10,@COLUMNA11,@COLUMNA12,@COLUMNA13,@COLUMNB01,@COLUMNB02,@COLUMNB03,@COLUMNB04,@COLUMNB05,@COLUMNB06,@COLUMNB07,@COLUMNB08,@COLUMNB09,@COLUMNB10,@COLUMNB11,@COLUMNB12,@COLUMND01,@COLUMND02,@COLUMND03,@COLUMND04,@COLUMND05,@COLUMND06,@COLUMND07,@COLUMND08,@COLUMND09,@COLUMND10)
                     
                      SET @ComitData=CAST(ISNULL((SELECT Isnull(column05, 0) 
                                      FROM   fitable010 
                                      WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27),-1) as decimal(18,2)) 
                      SET @WIP=CAST(ISNULL((SELECT Isnull(column18, 0) 
                                FROM   fitable010 
                                WHERE  column03 = @COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27),-1) as decimal(18,2))
                      SET @QOH=CAST(ISNULL((SELECT Isnull(column04, 0) 
                                FROM   fitable010 
                                WHERE  column03 = @COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27),-1) as decimal(18,2)) 
                      SET @BackOrderData=CAST(ISNULL((SELECT Isnull(column06, 0) 
                                          FROM   fitable010 
                                          WHERE  column03 = @COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27),-1) as decimal(18,2)) 

set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN03)
	set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
	declare @SALESNO nvarchar(250)
	set @SALESNO=(select COLUMN04 from  SATABLE005 Where COLUMN01=@COLUMN19)

	if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
if(@UOMData is null)
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@SALESNO and COLUMN06=@COLUMN03 and COLUMN05=@COLUMN19 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
else
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'JOBORDER',@SALESNO,@COLUMN19,@COLUMN03,@COLUMNA02,@COLUMNA03
end
end
else
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@SALESNO and COLUMN06=@COLUMN03 and COLUMN05=@COLUMN19 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end




                      IF( @ComitData >= 0 ) 
                        BEGIN 
                            IF( @QOH < Cast(@COLUMN07 as decimal(18,2)) ) 
                              BEGIN 
                                  SET @WIP=@WIP + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @TComitData=@ComitData + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @BackOrderData=( Cast(@COLUMN07 as decimal(18,2)) 
                                                       - @QOH ) 
                                                     + 
                                                     @BackOrderData 
                                  SET @Qty_Avl=0 
				
                                  IF( @FRMID = 1283 ) 
                                    BEGIN 
                                        UPDATE fitable010 
                                        SET    column18 = 0.00 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
                                    END 
                                  ELSE 
                                    BEGIN 
                                        UPDATE fitable010 
                                        SET    column05 = @TComitData, 
                                               column06 = @BackOrderData, 
                                               column08 = @Qty_Avl 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
                                    END 
                              END 
                            ELSE 
                              BEGIN 
                                  SET @WIP=@WIP + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @TComitData=@ComitData + Cast(@COLUMN07 as decimal(18,2)) 
                                  SET @Qty_Avl=@QOH

                                  IF( @Qty_Avl < 0 ) 
                                    BEGIN 
                                        SET @Qty_Avl=0 
                                        SET @BackOrderData=( Cast( 
                                        @COLUMN07 as decimal(18,2) 
                                                             ) 
                                                             - @QOH 
                                                           ) 
                                                           + 
                                                           @BackOrderData 
                                    END 
				  
                                  IF( @FRMID = 1283 ) 
                                    BEGIN 
                                        UPDATE fitable010 
                                        SET    column18 = 0.00 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
                                    END 
                                  ELSE 
                                    BEGIN 
                                        UPDATE fitable010 
                                        SET    column05 = @TComitData, 
                                               column06 = @BackOrderData, 
                                               column08 = @Qty_Avl 
                                        WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
                                    END 
                              END 
                        END 
                      ELSE 
                        BEGIN 
                            SET @TComitData=Cast(@COLUMN07 as decimal(18,2)) 
                            SET @Qty_Avl=@QOH  
			    
                            UPDATE fitable010 
                            SET    column05 = @TComitData, 
                                   column06 = @BackOrderData, 
                                   column08 = @Qty_Avl 
                            WHERE  column03 = @COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
                        END 
                  END 
                ELSE 
                  BEGIN 
                      UPDATE       SATABLE006
SET                COLUMN02 = @COLUMN02, COLUMN03 = @COLUMN03, COLUMN04 = @COLUMN04, COLUMN05 = @COLUMN05, COLUMN07 = @COLUMN07, 
                         COLUMN08 = @COLUMN08, COLUMN09 = @COLUMN09, COLUMN10 = @COLUMN10, COLUMN11 = @COLUMN11, COLUMN14 = @COLUMN14, 
                         COLUMN15 = @COLUMN15, COLUMN16 = @COLUMN16, COLUMN17 = @COLUMN17, COLUMN18 = @COLUMN18, COLUMN19 = @COLUMN19, 
                         COLUMN20 = @COLUMN20, COLUMN21 = @COLUMN21, COLUMN22 = @COLUMN22, COLUMN23 = @COLUMN23, COLUMN24 = @COLUMN24, 
                       
			 COLUMN25 = @COLUMN25,COLUMN26 = @COLUMN26, COLUMN27 = @COLUMN27, COLUMN28 =@COLUMN27,  COLUMN29 =@COLUMN07, 
						 COLUMN30 =@COLUMN30, COLUMN31=@COLUMN31,COLUMN33 =@COLUMN33, COLUMN34=@COLUMN34,COLUMNA01 = @COLUMNA01, COLUMNA02 = @COLUMNA02, COLUMNA03 = @COLUMNA03, 
                         COLUMNA04 = @COLUMNA04, COLUMNA05 = @COLUMNA05, COLUMNA07 = @COLUMNA07, COLUMNA08 = @COLUMNA08, 
                         COLUMNA09 = @COLUMNA09, COLUMNA10 = @COLUMNA10, COLUMNA11 = @COLUMNA11, COLUMNA12 = @COLUMNA12, COLUMNA13 = @COLUMNA13, 
                         COLUMNB01 = @COLUMNB01, COLUMNB02 = @COLUMNB02, COLUMNB03 = @COLUMNB03, COLUMNB04 = @COLUMNB04, COLUMNB05 = @COLUMNB05, 
                         COLUMNB06 = @COLUMNB06, COLUMNB07 = @COLUMNB07, COLUMNB08 = @COLUMNB08, COLUMNB09 = @COLUMNB09, COLUMNB10 = @COLUMNB10, 
                         COLUMNB11 = @COLUMNB11, COLUMNB12 = @COLUMNB12, COLUMND01 = @COLUMND01, COLUMND02 = @COLUMND02, COLUMND03 = @COLUMND03, 
                         COLUMND04 = @COLUMND04, COLUMND05 = @COLUMND05, COLUMND06 = @COLUMND06, COLUMND07 = @COLUMND07, COLUMND08 = @COLUMND08, 
                         COLUMND09 = @COLUMND09, COLUMND10 = @COLUMND10
WHERE        (COLUMN02 = @COLUMN02)


 set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN03)
		set @SALESNO=(select COLUMN04 from  SATABLE005 Where COLUMN01=@COLUMN19)
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
if(@UOMData is null)
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@SALESNO and COLUMN06=@COLUMN03 and COLUMN05=@COLUMN19 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
else
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'JOBORDER',@SALESNO,@COLUMN19,@COLUMN03,@COLUMNA02,@COLUMNA03
end
end
else
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@SALESNO and COLUMN06=@COLUMN03 and COLUMN05=@COLUMN19 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end


 SET @COLUMN18=(SELECT sum(isnull(COLUMN18,0)) FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20  AND COLUMN19=@COLUMN27)
 SET @COLUMN18=(CAST(@COLUMN18 as DECIMAL(18,2))+CAST(@COLUMN07 AS DECIMAL(18,2)))
		UPDATE FITABLE010 SET COLUMN18 =@COLUMN18 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
END

 set @COLUMN15=(SELECT COLUMN06 from  SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN02=(select COLUMN01 from  SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from  SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN04=(select COLUMN04 from  SATABLE005 where COLUMN01=@COLUMN19);
 --EMPHCS1049 rajasekhar reddy patakota 29/08/2015 Job Oder Functionality Changes
 
insert into PUTABLE013 
( 
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN08,  COLUMN13,  COLUMN14,  COLUMN15,
   --COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,   COLUMN07,  COLUMN11,  COLUMN12,    COLUMN14,  COLUMN15, COLUMN16, 
   --COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,   COLUMN21,  COLUMN22,  COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09,
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08,
   COLUMND09, COLUMND10
)
values
(  
   @COLUMN19,  'Job Order',  @COLUMN04,  @COLUMN06,   @COLUMN07,    @COLUMN14,    @COLUMN03,  @COLUMN15,
   --  @COLUMN03,  @COLUMN04,  @COLUMN05,   @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN15,
   --@COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, 
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, 
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05,
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  

                      SET @ReturnValue = 1 
            END 
          ELSE IF @Direction = 'Delete' 
            BEGIN 
                SET @IQTY=(select COLUMN07 from  SATABLE006 Where COLUMN02=@COLUMN02 )
   set @COLUMN19 =(select COLUMN19 from  SATABLE006 Where COLUMN02=@COLUMN02 );
   set @COLUMN04=(select COLUMN04 from  SATABLE005 where COLUMN01=@COLUMN19);
   set @COLUMN20 =(select COLUMN24 from  SATABLE005 Where COLUMN01=@COLUMN19 );
--EMPHCS1049 rajasekhar reddy patakota 29/08/2015 Job Oder Functionality Changes
   SET @COLUMN07=((SELECT sum(isnull(COLUMN18,0)) FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27)-@IQTY)
   --UPDATE FITABLE010 SET COLUMN18 =@COLUMN07 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27
UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02
update PUTABLE013 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN02=@COLUMN19 and COLUMN03='Job Order'
            END 
      END try 

      BEGIN catch 
      
DECLARE @tempSTR NVARCHAR(MAX)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(250))))
    exec [CheckDirectory] @tempSTR,'Exception_JO_SATABLE006.txt',0

          IF NOT EXISTS(SELECT column01 
                        FROM   satable006 
                        WHERE  column19 = @column19) 
            BEGIN 
                DELETE satable005 
                WHERE  column01 = @COLUMN19 

                RETURN 0 
            END 

          RETURN 0 

          RETURN 0 
      END catch 
  END 






GO
