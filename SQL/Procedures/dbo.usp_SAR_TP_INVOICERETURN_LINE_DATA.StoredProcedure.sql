USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_INVOICERETURN_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_INVOICERETURN_LINE_DATA]
(
	@SalesOrderID int
)

AS
BEGIN
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
declare @Query1 nvarchar(max)					
select p.COLUMN03,p.COLUMN04,p.COLUMN05,p.COLUMN07,p.COLUMN08,p.COLUMN26,p.COLUMN25,p.COLUMN33,p.COLUMN09,p.COLUMN10,p.COLUMN32,cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))COLUMN24,(cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))+((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01)))COLUMN11,p.COLUMN17,cast((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01) as decimal(18,2) ) tax,p.COLUMN35,p.COLUMN37 into #temp6 from(					
SELECT  
iif(im.column06!='',im.column06,b.COLUMN05) COLUMN03,'' COLUMN04, b.COLUMN06 COLUMN05,iif(im.column06!='',isnull(im.COLUMN07,0),isnull(b.COLUMN10,0)) COLUMN07,'' COLUMN08,iif(im.column06!='',im.COLUMN08,b.COLUMN22) COLUMN26,b.COLUMN21 COLUMN25,b.COLUMN33 COLUMN33,isnull(b.COLUMN13,0) COLUMN09,cast(isnull(b.COLUMN19,0) as decimal(18,2)) as COLUMN10,b.COLUMN35 COLUMN32,
(cast(isnull(b.COLUMN10,0)*isnull(b.COLUMN13,0) as decimal(18,2))) COLUMN24, (cast(b.COLUMN14 as decimal(18,2))+cast(b.COLUMN23 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))) COLUMN11,b.COLUMN31 COLUMN17
, (cast(b.COLUMN14 as decimal(18,2))+cast(b.COLUMN23 as decimal(18,2))-cast(b.COLUMN19 as decimal(18,2))) tax,isnull(b.COLUMN19,0) disc,m.COLUMN07 taxper
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,b.COLUMN37 COLUMN35,b.COLUMN38 COLUMN37
	FROM SATABLE010 b inner join SATABLE009 a on a.COLUMN01=b.COLUMN15
	left join MATABLE013 m on  m.column02=b.COLUMN21
	left join MATABLE007 mu on  mu.column02=b.COLUMN05 and mu.columnA03=b.columnA03 
	left join FITABLE038 im on  im.column06=b.COLUMN05 and im.column05=b.column01 and im.columnA03=b.columnA03 and im.columnA02=b.columnA02 and isnull(im.COLUMNA13,0)=0
	WHERE  a.COLUMN02=@SalesOrderID and b.COLUMNA13=0) p
set @Query1='select * from #temp6'
execute(@Query1)
END


GO
