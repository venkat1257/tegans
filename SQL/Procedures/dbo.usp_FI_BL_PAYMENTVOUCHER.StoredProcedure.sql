USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_BL_PAYMENTVOUCHER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_BL_PAYMENTVOUCHER]

(

    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  

	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  

	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  

	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  

	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,

	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	--EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,

	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   

	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 

    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,

	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,

	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  

	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  

	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,

    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   

	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  

	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  

	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  

	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
        --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT

)

AS

BEGIN

begin try

   declare @tempSTR nvarchar(max)

   set @tempSTR=('the values '+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','
+isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+','+isnull(@COLUMN14,'')+','+isnull(@COLUMN15,'')+' of  '+isnull(@TabelName,'')+' succeeded at  ');

IF  @TabelName='FITABLE020'

BEGIN

set @COLUMNA02=( CASE WHEN (@COLUMN12!= '' and @COLUMN12 is not null ) THEN @COLUMN12 else @COLUMNA02  END )

EXEC usp_FI_TP_FITABLE020_PAYMENTVOUCHER 

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,

   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   @COLUMN22,  @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, 

   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 

   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 

   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
   @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue OUTPUT
select @ReturnValue

END

 IF  @TabelName='FITABLE022'

BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN09!= '' and @COLUMN09 is not null and @COLUMN09!= 0 ) THEN (select COLUMN12 from FITABLE020 

where COLUMN01=@COLUMN09) else @COLUMNA02  END )

EXEC usp_FI_TP_FITABLE022_PAYMENTVOUCHER

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,@COLUMN10,@COLUMN11,@COLUMN12,@COLUMN13, @COLUMN14,@COLUMN15,@COLUMN16,@COLUMN17, @COLUMN18,@COLUMNA01, @COLUMNA02,

   @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, 
   @COLUMNA13,
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 

   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, 

   @COLUMND08, @COLUMND09, @COLUMND10, @Direction, @ReturnValue

END

   exec [CheckDirectory] @tempSTR,'usp_FI_BL_PAYMENTVOUCHER.txt',0

END TRY

			BEGIN CATCH

			begin

    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_FI_BL_PAYMENTVOUCHER.txt',0

			return 0

			end

			END CATCH

end

GO
