USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PVIMPORTECXEL]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PVIMPORTECXEL]
(
@Account nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null
)
AS
BEGIN
select f.column02 as AID,f.column04 AR,M13.COLUMN02 TID,M13.COLUMN04 TNAME,0 Project FROM FITABLE001 f 
left outer join MATABLE013 M13 ON M13.COLUMN02 = 1000 
WHERE f.COLUMNA03=@AcOwner AND f.column04=@Account
end

GO
