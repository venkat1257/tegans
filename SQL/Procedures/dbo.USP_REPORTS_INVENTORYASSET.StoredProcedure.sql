USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_INVENTORYASSET]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[USP_REPORTS_INVENTORYASSET](
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@DateF nvarchar(250)=null
 )
as
begin
if @OperatingUnit!=''
begin
set @whereStr=' and CON07.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and  p.COLUMN14 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and p.COLUMN14 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1 order by rno desc'
 end
else
begin
 set @whereStr=@whereStr+' order by rno desc'
end
declare @temp1 decimal(18,2)
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
--select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc
set @temp1=(select  isnull(sum(p.column10),0) -isnull(sum(p.column11),0)  from PUTABLE017 p where p.COLUMN04<@FromDate and p.COLUMN04>=@FiscalYearStartDt and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner  AND  (p.COLUMN14 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) or (1=1)) and (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and p.COLUMNA02 is not null or (1=1)) ) 

exec ('
(select accountno,OperatingUnit,ddate,ttype,payee,account,memo,sum(inamt)inamt,sum(decamt)decamt,sum(balcamt)balcamt,ab,trans1,ref,OPID,0 Project,'''' rno from (
select e.COLUMN04 accountno,CON07.COLUMN03 OperatingUnit,NULL ddate,NULL Dtt,NULL ttype,NULL payee,NULL account,''[Previous Amount]'' [memo],
sum(isnull(p.column10,0)) as inamt,sum(isnull(p.column11,0)) decamt, (sum(isnull(p.column10,0))-isnull(sum(p.column11),0)) balcamt,null ab,null trans1,null ref,p.COLUMNA02 OPID,(case when p.COLUMN14 is null then 0 else p.COLUMN14 end) Project,'''' rno  from PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 left join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA03=p.COLUMNA03
where p.COLUMN04<''' +@FromDate+''' and p.column04 >='''+@FiscalYearStartDt+''' and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and p.COLUMNA02 is not null  AND  p.COLUMNA03='''+@AcOwner+'''  and isnull(p.columna13,0)=0  group by CON07.COLUMN03,p.COLUMNA02,p.COLUMN14,e.COLUMN04) p group by accountno,OperatingUnit,ddate,ttype,payee,account,memo,ab,trans1,ref,OPID)
 union all
SELECT   e.COLUMN04 AS accountno,CON07.COLUMN03 as OperatingUnit,FORMAT(p.COLUMN04,'''+@DateF+''') AS ddate,p.COLUMN06 ttype, 
(CASE  WHEN a.COLUMN05!='''' then a.COLUMN05 else b.COLUMN05 end) payee,
p.COLUMN08 AS account, p.COLUMN09 AS memo, p.COLUMN10 AS inamt, p.COLUMN11  AS decamt,
(SUM(isnull(p.COLUMN10,0)) OVER(PARTITION BY CON07.COLUMN03 ORDER BY p.COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
-SUM(isnull(p.COLUMN11,0)) OVER(PARTITION BY CON07.COLUMN03 ORDER BY p.COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) + 
isnull('+@temp1+',0)  AS balcamt,p.COLUMN02 ab,
(case when p.COLUMN06=''BILL'' then bi.COLUMN04
when p.COLUMN06=''INVOICE'' then iv.COLUMN04
when p.COLUMN06=''Resource Consumption Direct'' then rc.COLUMN04
when (p.COLUMN06=''Item Issue'' or p.COLUMN06=''JobOrder Issue'') then ii.COLUMN04
when (p.COLUMN06=''Item Receipt'' or p.COLUMN06=''JobOrder Receipt'') then ir.COLUMN04
when p.COLUMN06=''Return Issue'' then ii.COLUMN04
when p.COLUMN06=''Return Receipt'' then ir.COLUMN04
when p.COLUMN06=''Credit Memo'' then cm.COLUMN04
when p.COLUMN06=''Debit Memo'' then dm.COLUMN04
when p.COLUMN06=''Stock Transfer'' then sh.COLUMN04
when p.COLUMN06=''Resource Consumption'' then rc.COLUMN04
when p.COLUMN06=''Resource Consumption Direct'' then rc.COLUMN04
when p.COLUMN06=''Inventory Adjustment'' then ia.COLUMN04 
when p.COLUMN06=''JOURNAL ENTRY'' then F31.COLUMN04 else '''' end) trans1,
(case when p.COLUMN06=''BILL'' then ''''
when p.COLUMN06=''INVOICE'' then iv.COLUMN09
when p.COLUMN06=''Resource Consumption Direct'' then ''''
when (p.COLUMN06=''Item Issue'' or p.COLUMN06=''JobOrder Issue'') then ii.COLUMN09
when (p.COLUMN06=''Item Receipt'' or p.COLUMN06=''JobOrder Receipt'') then ir.COLUMN10
when p.COLUMN06=''Return Issue'' then ii.COLUMN09
when p.COLUMN06=''Return Receipt'' then ir.COLUMN10
when p.COLUMN06=''Credit Memo'' then cm.COLUMN11
when p.COLUMN06=''Debit Memo'' then dm.COLUMN11
when p.COLUMN06=''Stock Transfer'' then ''''
when p.COLUMN06=''Inventory Adjustment'' then ia.COLUMN14 else '''' end) ref,
p.COLUMNA02 OPID,(case when p.COLUMN14='''' then 0 else p.COLUMN14 end) Project,row_number() over(order by p.COLUMN04) rno
 From PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
 left join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA03=p.COLUMNA03
 left join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and (p.COLUMN06=''BILL'' or p.COLUMN06= ''Resource Consumption Direct'' or p.COLUMN06=''Item Receipt'' or p.COLUMN06=''Debit Memo'' or p.COLUMN06=''JobOrder Receipt'' or p.COLUMN06=''Return Issue'' or p.COLUMN06=''JOURNAL ENTRY'') and a.COLUMNA03=p.COLUMNA03
 left join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and (p.COLUMN06=''INVOICE'' or p.COLUMN06= ''Resource Consumption Direct'' or p.COLUMN06=''Item Issue'' or p.COLUMN06=''Resource Consumption'' or p.COLUMN06=''Resource Consumption Direct'' or p.COLUMN06=''Credit Memo'' or p.COLUMN06=''JobOrder Issue'' or p.COLUMN06=''Return Receipt'' or p.COLUMN06=''JOURNAL ENTRY'') and b.COLUMNA03=p.COLUMNA03
 left join  PUTABLE005 bi on bi.COLUMN01=p.COLUMN05 and bi.COLUMNA02=p.COLUMNA02 and bi.COLUMNA03=p.COLUMNA03 and isnull((bi.COLUMNA13),0)=0
 left join  SATABLE009 iv on iv.COLUMN01=p.COLUMN05 and iv.COLUMNA02=p.COLUMNA02 and iv.COLUMNA03=p.COLUMNA03 and isnull((iv.COLUMNA13),0)=0
 
 left join  PUTABLE003 ir on ir.COLUMN01=p.COLUMN05 and ir.COLUMNA02=p.COLUMNA02 and ir.COLUMNA03=p.COLUMNA03 and isnull((ir.COLUMNA13),0)=0
 left join  SATABLE007 ii on ii.COLUMN01=p.COLUMN05 and ii.COLUMNA02=p.COLUMNA02 and ii.COLUMNA03=p.COLUMNA03 and isnull((ii.COLUMNA13),0)=0
 left join  SATABLE005 cm on cm.COLUMN01=p.COLUMN05 and cm.COLUMNA02=p.COLUMNA02 and cm.COLUMNA03=p.COLUMNA03 and isnull((cm.COLUMNA13),0)=0
 left join  PUTABLE001 dm on dm.COLUMN01=p.COLUMN05 and dm.COLUMNA02=p.COLUMNA02 and dm.COLUMNA03=p.COLUMNA03 and isnull((dm.COLUMNA13),0)=0
 left join  FITABLE014 ia on ia.COLUMN01=p.COLUMN05 and ia.COLUMNA02=p.COLUMNA02 and ia.COLUMNA03=p.COLUMNA03 and isnull((ia.COLUMNA13),0)=0
 left join  PRTABLE007 rc on rc.COLUMN01=p.COLUMN05 and rc.COLUMNA02=p.COLUMNA02 and rc.COLUMNA03=p.COLUMNA03 and isnull((rc.COLUMNA13),0)=0
 left join  PRTABLE004 sl on sl.COLUMN01=p.COLUMN05 and sl.COLUMNA03=p.COLUMNA03 and isnull((sl.COLUMNA13),0)=0
 left join  PRTABLE003 sh on sh.COLUMN01=sl.COLUMN08 and sh.COLUMNA03=sl.COLUMNA03 and isnull((sh.COLUMNA13),0)=0
 left join  FITABLE031 F31 ON F31.COLUMN01 = p.COLUMN05 and F31.COLUMNA03=p.COLUMNA03 and F31.COLUMNA02=p.COLUMNA02 
 and isnull((F31.COLUMNA13),0)=0
  where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 between '''+ @FromDate+''' and '''+ @ToDate+''' AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','', '''+@OPUnit+''') s) 
 AND  p.COLUMNA03= '''+@AcOwner+'''  and p.COLUMNA02 is not null and p.column04 >='''+@FiscalYearStartDt+''' '+@whereStr+' 
'   )
--exec (@Query1) 
end





GO
