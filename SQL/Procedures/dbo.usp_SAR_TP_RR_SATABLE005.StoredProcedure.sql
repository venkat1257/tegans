USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_RR_SATABLE005]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_RR_SATABLE005]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,
	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN43   nvarchar(250)=null,
	@COLUMN44   nvarchar(250)=null,  @COLUMN45   nvarchar(250)=null,  @COLUMN46   nvarchar(250)=null, 
	@COLUMN50   nvarchar(250)=null,  @COLUMN51  nvarchar(250)=null,   @COLUMN60   nvarchar(250)=null,   
	@COLUMN71   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250)=null,  @ReturnValue int=null OUTPUT 
)
AS

BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
 declare @tempSTR nvarchar(max)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE005_SequenceNo
insert into SATABLE005 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,  
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
   COLUMN35,  COLUMN36,  COLUMN37,  COLUMN38,  COLUMN39,  COLUMN40,  COLUMN41,  COLUMN42,  COLUMN43,  COLUMN44,  COLUMN45,
   COLUMN46,  COLUMN50,  COLUMN51,  COLUMN60,  COLUMN71,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN50,  @COLUMN51,  @COLUMN60,  @COLUMN71,  
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
declare @newID int,@Totamt decimal(18,2),@Creditamt decimal(18,2),@COLUMN01 int,@ReverseCharged bit,@TOTALAMT DECIMAL(18,2),@Creditcashsale bit
set @Creditcashsale = (select isnull(COLUMN04,0) from CONTABLE031 where COLUMN03='Allow credit for cash sale' and isnull(COLUMN04,0)=1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
set @COLUMN01 = (select COLUMN01  from SATABLE005 where COLUMN02=@COLUMN02)
set @ReverseCharged = (select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@COLUMN01)
if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN15,0)as decimal(18,2))-cast(ISNULL(@COLUMN32,0)as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN15,0))
end
if(@COLUMN03!='1603' or (@Creditcashsale=1 or @Creditcashsale='True' or @Creditcashsale='1'))
begin
						if not exists(select COLUMN05  from PUTABLE018 where  COLUMN05=@COLUMN04 and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
						    BEGIN 
							set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE018) as int)+1 
							insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02,COLUMN14,COLUMN15,COLUMNA03)
							values(@newID,3000,@COLUMN06,@COLUMN04,'Credit Memo',@COLUMN05,@COLUMN09,-cast(@TOTALAMT as decimal(18,2)),@COLUMN06,@TOTALAMT,'Open', @column24,@TOTALAMT,@COLUMN04,@COLUMNA03)
							END
						else
							BEGIN
								set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@COLUMN04     and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
								set @Creditamt=(select COLUMN14 from PUTABLE018  where COLUMN05=@COLUMN04  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
								update PUTABLE018 set COLUMN04=@COLUMN06,
								COLUMN09=(cast(@Totamt as DECIMAL(18,2))-cast(@TOTALAMT as DECIMAL(18,2))),
								COLUMN12=(-(cast(@Totamt as DECIMAL(18,2)))+cast(@TOTALAMT as DECIMAL(18,2))),
								COLUMN14=(cast(@Creditamt as DECIMAL(18,2))+cast(@TOTALAMT as DECIMAL(18,2))) 
								where COLUMN05=@COLUMN04  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
							END
end


declare @tmpnewID1 int,@AccountType nvarchar(250),@BAL decimal(18,2),@SA11COLUMN04 nvarchar(250)
SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN51 and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
SET @BAL=((select column10 from FITABLE001 where column02=@COLUMN51 and columnA03= @COLUMNA03) -cast(isnull( @COLUMN15,0) as decimal(18,2)))
set @COLUMN01 = (select COLUMN01  from SATABLE005 where COLUMN02=@COLUMN02)

if(cast(isnull(@COLUMN15,0) as decimal(18,2))>0)
begin
if(@AccountType=22409)
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE052)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE052) as int)+1
end
else
begin
set @newID=1000
end
insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08)
values(@newID,@COLUMN51,@COLUMN06,@COLUMN04,'PAYMENT','22335',@COLUMN05,@COLUMN01,@COLUMN09,@COLUMN15,@COLUMN15, @COLUMNA02, @COLUMNA03,@COLUMN35, @COLUMNA08)
end
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE019)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
end
else
begin
set @newID=100000
end
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08    ,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20)
values(@newID, @COLUMN51,@COLUMN06,@COLUMN04,'PAYMENT',@COLUMN05,@COLUMN01,@COLUMN09,@COLUMN15,@COLUMN15, @COLUMNA02, @COLUMNA03,@COLUMN35)
end
UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@COLUMN51 AND COLUMNA03=@COLUMNA03
IF(CAST(ISNULL(@COLUMN71,0)AS DECIMAL(18,2))!=0)
BEGIN
SET @newID=((SELECT MAX(ISNULL(COLUMN02,1000)) from FITABLE035)+1)
insert into FITABLE035(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,COLUMNA02, COLUMNA03 )
values(@newID,  'Credit Memo',  @COLUMN06,  @COLUMN01,  @COLUMN05,0,ISNULL(@COLUMN71,0),@COLUMN04,1145,@COLUMN35,@COLUMNA02, @COLUMNA03)
END
IF(cast(@COLUMN60 as decimal(18,2))>0.00)
BEGIN
set @newID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
values(@newID,@COLUMN06,'Credit Memo',@COLUMN04,@COLUMN05,@COLUMN01,1049,0,@COLUMN60,@COLUMNA02 , @COLUMNA03,0,0) 
END
end


set @ReturnValue =( select COLUMN01 from SATABLE005 WHERE COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from SATABLE005
END 
 
ELSE IF @Direction = 'Update'
BEGIN
declare @PAmnt  DECIMAL(18,2),@PrevNum nvarchar(250)
	SET @PrevNum=(SELECT COLUMN04 FROM SATABLE005 WHERE COLUMN02=@COLUMN02)
set @PAmnt =(select  COLUMN15 from SATABLE005 where  COLUMN02 = @COLUMN02 )
UPDATE SATABLE005 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,      COLUMN16=@COLUMN16,  
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,  
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,
   COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,
   COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,  
   COLUMN36=@COLUMN36,    COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,    COLUMN39=@COLUMN39,    COLUMN40=@COLUMN40, 
   COLUMN41=@COLUMN41,    COLUMN42=@COLUMN42,    COLUMN43=@COLUMN43,    COLUMN44=@COLUMN44,    COLUMN45=@COLUMN45,
   COLUMN46=@COLUMN46,    COLUMN50=@COLUMN50,    COLUMN51=@COLUMN51,    COLUMN60=@COLUMN60,    COLUMN71=@COLUMN71,    
   COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA06=@COLUMNA06,
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02   

if(@COLUMN03!='1603')
begin
    delete from PUTABLE018  where COLUMN05=@PrevNum  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
end	
	declare @Num nvarchar(250)
	DECLARE @INTERNAL INT,@AMOUNT DECIMAL(18,2)
	SET @INTERNAL=(SELECT COLUMN01 FROM SATABLE005 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT SUM(COLUMN09) FROM SATABLE006 WHERE COLUMN19 IN(@INTERNAL))
	set @ReverseCharged = (select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@INTERNAL)
	if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
	begin
	set @TOTALAMT=(cast(ISNULL(@COLUMN15,0)as decimal(18,2))-cast(ISNULL(@COLUMN32,0)as decimal(18,2)))
	end
	else
	begin
	set @TOTALAMT=(ISNULL(@COLUMN15,0))
	end

	set @Num=(select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN02);
	delete from	FITABLE035 where COLUMN03='Credit Memo' and COLUMN05=@Num and	COLUMN09=@PrevNum and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	delete from FITABLE026 where column05=@Num and column09=@PrevNum and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	delete from FITABLE036 where COLUMN10=1049  and column05=@Num and column09=@PrevNum and column03='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	delete from	FITABLE034 where COLUMN03='Credit Memo' and 	COLUMN09=@PrevNum and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	
IF(CAST(ISNULL(@COLUMN71,0)AS DECIMAL(18,2))!=0)
BEGIN
SET @newID=((SELECT MAX(ISNULL(COLUMN02,1000)) from FITABLE035)+1)
insert into FITABLE035(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,COLUMNA02, COLUMNA03 )
values(@newID,  'Credit Memo',  @COLUMN06,  @Num,  @COLUMN05,0,ISNULL(@COLUMN71,0),@COLUMN04,1145,@COLUMN35,@COLUMNA02, @COLUMNA03)
END	
IF(cast(@COLUMN60 as decimal(18,2))>0.00)
BEGIN
set @newID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
values(@newID,@COLUMN06,'Credit Memo',@COLUMN04,@COLUMN05,@Num,1049,0,@COLUMN60,@COLUMNA02 , @COLUMNA03,0,0) 
END
set @Creditcashsale = (select isnull(COLUMN04,0) from CONTABLE031 where COLUMN03='Allow credit for cash sale' and isnull(COLUMN04,0)=1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
if(@COLUMN03!='1603' or (@Creditcashsale=1 or @Creditcashsale='True' or @Creditcashsale='1'))
begin
	if not exists(select COLUMN05  from PUTABLE018 where  COLUMN05=@PrevNum and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
	    BEGIN 
		set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE018) as int)+1 
		insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02,COLUMN14,COLUMN15,COLUMNA03)
		values(@newID,3000,@COLUMN06,@COLUMN04,'Credit Memo',@COLUMN05,@COLUMN09,-cast(@TOTALAMT as decimal(18,2)),@COLUMN06,@TOTALAMT,'Open', @column24,@TOTALAMT,@COLUMN04,@COLUMNA03)
		END
	else
		BEGIN
			set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@PrevNum     and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			set @Creditamt=(select COLUMN14 from PUTABLE018  where COLUMN05=@PrevNum  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			update PUTABLE018 set COLUMN04=@COLUMN06,
			COLUMN09=(cast(@Totamt as DECIMAL(18,2))-cast(@TOTALAMT as DECIMAL(18,2))),
			COLUMN12=(-(cast(@Totamt as DECIMAL(18,2)))+cast(@TOTALAMT as DECIMAL(18,2))),
			COLUMN14=(cast(@Creditamt as DECIMAL(18,2))+cast(@TOTALAMT as DECIMAL(18,2))) 
			where COLUMN05=@COLUMN04  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
		END
end

   if(cast(isnull(@COLUMN15,0) as decimal(18,2))>0)
begin
UPDATE SATABLE005 SET COLUMN15= (CAST(0 AS DECIMAL(18,2)) + CAST(@COLUMN15 AS DECIMAL(18,2)))    where  COLUMN02 = @COLUMN02   

	SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE005 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT COLUMN10 FROM PUTABLE019 where COLUMN08=@INTERNAL AND COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)

	--update PUTABLE019 set COLUMNA13=0, COLUMN04=@COLUMN06,COLUMN12=@COLUMN13,COLUMN10=(CAST(@AMOUNT AS DECIMAL(18,2)) + CAST(@COLUMN15 AS DECIMAL(18,2)))  where  COLUMN05=@COLUMN04 AND  COLUMN08=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	DELETE FROM PUTABLE019 WHERE COLUMN05=@PrevNum AND COLUMN08=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	DELETE FROM FITABLE052 WHERE COLUMN05=@PrevNum AND COLUMN09=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN51 and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
SET @BAL=((select column10 from FITABLE001 where column02=@COLUMN51 and columnA03= @COLUMNA03)+cast(isnull( @PAmnt,0) as decimal(18,2)) -cast(isnull( @COLUMN15,0) as decimal(18,2)))
set @COLUMN01 = (select COLUMN01  from SATABLE005 where COLUMN02=@COLUMN02)
if(cast(isnull(@COLUMN15,0) as decimal(18,2))>0)
begin
if(@AccountType=22409)
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE052)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE052) as int)+1
end
else
begin
set @newID=1000
end
insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08)
values(@newID,@COLUMN51,@COLUMN06,@COLUMN04,'PAYMENT','22335',@COLUMN05,@COLUMN01,@COLUMN09,@COLUMN15,@COLUMN15, @COLUMNA02, @COLUMNA03,@COLUMN35, @COLUMNA08)
end
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE019)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
end
else
begin
set @newID=100000
end
insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08    ,COLUMN09,COLUMN10,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20)
values(@newID, @COLUMN51,@COLUMN06,@COLUMN04,'PAYMENT',@COLUMN05,@COLUMN01,@COLUMN09,@COLUMN15,@COLUMN15, @COLUMNA02, @COLUMNA03,@COLUMN35)
end
UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@COLUMN51 AND COLUMNA03=@COLUMNA03	 		

end
end

 declare @SOID nvarchar(250),@Qty nvarchar(250),@Item nvarchar(250),@ItemQty nvarchar(250),@id nvarchar(250),@uom nvarchar(250)
 DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
declare @RefundQty DECIMAL(18,2), @RefID int, @Refreceipt int, @TQty DECIMAL(18,2), @RRQty DECIMAL(18,2)
declare @PID int, @Result nvarchar(250), @date nvarchar(250), @chk bit, @AID int, @SBAL DECIMAL(18,2), @CBAL DECIMAL(18,2)
declare @number1 int, @DT date, @PRICE DECIMAL(18,2), @lineiid nvarchar(250)=null, @CUST int,@Type nvarchar(250),@lotno nvarchar(250)
Declare @CustID int, @memo nvarchar(250), @dueDT date,  @tmpnewID int
             set @date=(select COLUMN06 from SATABLE005 where COLUMN01=@SOID);
             set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@SOID);
             set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
             set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
      set @SOID=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
      DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE006 where COLUMN19=@SOID  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
	  SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE006 where COLUMN19=@SOID   and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             set @uom=(select COLUMN27 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
		     set @lotno=(select isnull(COLUMN17,0)  from  SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             set @ItemQty=(select isnull(COLUMN07,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
             set @Price=(select isnull(COLUMN09,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
			 --set @RefundQty=isnull((select COLUMN13 from PUTABLE002 where COLUMN19 in (@PID) and COLUMN03=@Item  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 ),0);
			 --UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN03=@Item and COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN19=@SOID and COLUMN03=@Item  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
             --SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMN19 in (@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 )
             if(@TQty=@RRQty)
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
             --UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=62) where COLUMN01=(@PID)
             end
             else 
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
             --UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@PID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             end
             UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@SOID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
              
			 if exists(select column01 from SATABLE006 where column19=@SOID  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
					begin 
			set @COLUMN02=(select COLUMN01 from SATABLE005 where COLUMN01=@SOID);
			set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@SOID);
			set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@SOID);
			set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@SOID)
			set @memo = (select COLUMN09 from SATABLE005 where COLUMN01=@SOID)
			set @Num = (select COLUMN04 from SATABLE005 where COLUMN01=@SOID)
			set @dueDT = (select COLUMN08  from SATABLE005 where COLUMN01=@SOID)
					
set @lineiid=(select max(column01) from SATABLE006 where column02=@id)
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
--set @PRICE=(select column17 from matable007 where column02=@Item)
set @PRICE=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMNA02=@COLUMNA02 AND isnull(COLUMN19,0)=isnull(@uom,0) AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03);
set @CUST=(select column05 from SATABLE005 where column01=@SOID)
set @chk=(select column48 from matable007 where column02=@Item)

if(@chk=0)
	begin	
	delete from FITABLE025 WHERE COLUMN08=1053  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	end
else
	begin	

  declare @Qty_On_Hand	decimal(18,2)
  set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03);
  set @Qty=cast(@ItemQty as decimal(18,2));
  set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03)as decimal(18,2))-(cast(@ItemQty as decimal(18,2))*cast(@Price as decimal(18,2))))	 
  WHERE COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where   COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03)as decimal(18,2)));
  if(cast(@Qty_On_Hand as decimal(18,2))=0)
  begin
  UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03
  end
  else
  begin
  UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03
  end
    if(@COLUMN03!='1603')
     begin
    delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06='Return Issue' and COLUMN07=@COLUMN05 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	end
	else
    begin
    delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06='Credit Memo'  and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	end
	
	delete from FITABLE025 WHERE COLUMN08=1052  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
    delete from FITABLE025 WHERE COLUMN08=1056  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	delete from FITABLE026 where column05=@SOID and column09=@PrevNum and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	end        
	end
		     UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN02= @id  
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
		END
  CLOSE cur1 

  update SATABLE006 set columna13=1 where  COLUMN19=@SOID  and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

 set @COLUMNA02=(select COLUMNA02 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @COLUMNA03=(select COLUMNA03 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @COLUMN24=(select COLUMN24 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @COLUMN04=(select COLUMN04 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @COLUMN15=(select iif(isnull(cast(COLUMN15 as nvarchar(250)),'')='','0',COLUMN15) from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @COLUMN51=(select COLUMN51 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @Num=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
	DELETE FROM PUTABLE019 WHERE COLUMN05=@COLUMN04 AND COLUMN08=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	DELETE FROM FITABLE052 WHERE COLUMN05=@COLUMN04 AND COLUMN09=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	SET @BAL=((select column10 from FITABLE001 where column02=@COLUMN51 and columnA03= @COLUMNA03)+cast(isnull( @COLUMN15,0) as decimal(18,2)))	
	UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@COLUMN51 AND COLUMNA03=@COLUMNA03	 
	delete from FITABLE026 where column05=@Num and COLUMN09=@COLUMN04 and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	delete from	FITABLE034 where COLUMN03='Credit Memo' and 	COLUMN09=@COLUMN04 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	delete from	FITABLE035 where COLUMN03='Credit Memo' and COLUMN05=@Num and	COLUMN09=@COLUMN04 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03	
	  set @Initialrow = 1
      set @SOID=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
	   set @date=(select COLUMN06 from SATABLE005 where COLUMN01=@SOID);
             set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@SOID);
             set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
             set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
      DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE006 where COLUMN19 in(@SOID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
	  SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE006 where COLUMN19 in(@SOID)   and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             set @uom=(select COLUMN27 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
		     set @lotno=(select isnull(COLUMN17,0)  from  SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             set @ItemQty=(select isnull(COLUMN07,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
			 set @Price=(select isnull(COLUMN09,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
			 			 
             set @RefundQty=isnull((select COLUMN13 from PUTABLE002 where COLUMN19 in (@PID) and COLUMN03=@Item  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 ),0);
             --UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN03=@Item and COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN02=@id and COLUMN03=@Item  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
             SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMN19 in (@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 )
             if(@TQty=@RRQty)
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
             --UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=62) where COLUMN01=(@PID)
             end
             else 
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
             --UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@PID)
             end
             UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@SOID)
              
			 if exists(select column01 from SATABLE006 where column19 in(@SOID)   and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
					begin 
			set @lineiid=(select COLUMN01 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID);
			set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@SOID);
			set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@SOID);
			set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@SOID)
			set @memo = (select COLUMN09 from SATABLE005 where COLUMN01=@SOID)
			set @Num = (select COLUMN04 from SATABLE005 where COLUMN01=@SOID)
			set @dueDT = (select COLUMN08  from SATABLE005 where COLUMN01=@SOID)
 delete from PUTABLE018  where COLUMN05=@Num  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
set @CUST=(select column05 from SATABLE005 where column01=@SOID)
set @chk=(select column48 from matable007 where column02=@Item)
if(@chk=0)
	begin	
	delete from FITABLE025 WHERE COLUMN08=1053  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	end
else
	begin	
  set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03);
  set @Qty=cast(@ItemQty as decimal(18,2));
  set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03)as decimal(18,2))-(cast(@ItemQty as decimal(18,2))*cast(@Price as decimal(18,2))))	 
  WHERE COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where   COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03)as decimal(18,2)));
  if(cast(@Qty_On_Hand as decimal(18,2))=0)
  begin
  UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03
  end
  else
  begin
  UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=0 AND isnull(COLUMN22,0)=isnull(@lotno,0) AND COLUMNA03=@COLUMNA03
  end

    delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06='Credit Memo' and COLUMN07=@CustID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	delete from FITABLE025 WHERE COLUMN08=1052  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 
    delete from FITABLE025 WHERE COLUMN08=1056  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 
	delete from FITABLE026 where column05=@SOID and column09=@COLUMN04 and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	delete from FITABLE036 where COLUMN10=1049  and column05=@SOID and column09=@COLUMN04 and column03='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	end        
	end
		     UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN02= @id  
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
		END
UPDATE SATABLE005 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02
 update SATABLE006 set columna13=1 where  COLUMN19=@SOID  and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
END

end try
begin catch

	 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_RECEIPT_SATABLE005.txt',0
return 0
end catch
end

GO
