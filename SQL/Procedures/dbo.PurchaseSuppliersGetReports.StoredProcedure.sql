USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PurchaseSuppliersGetReports]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PurchaseSuppliersGetReports]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@DateF nvarchar(250)=null)
as 
begin
IF  @ReportName='PurchasesBySuppliersSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #PurchasesBySuppliersSummary from (
select o.COLUMN03 as OPUnit,s.COLUMN05 Vendor,p.COLUMN08 Date,f.COLUMN12 Amount,p.COLUMN05 VID,p.COLUMN15 OPID,isnull(m.COLUMN10,0) BrandID, m5.COLUMN04 as Brand
--EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
,p.COLUMN04 Trans# ,p.COLUMN03 fid ,p.COLUMN05 v ,@FromDate fd,@ToDate td from PUTABLE005 p 
inner  join PUTABLE006 f on f.COLUMN13=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
inner join CONTABLE007 o on o.COLUMN02=p.COLUMN15 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
inner join SATABLE001 s on s.COLUMN02=p.COLUMN05  and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
inner join MATABLE007 m on m.COLUMN02=f.COLUMN04  and m.COLUMNA03=p.COLUMNA03 and  isnull(m.COLUMNA13,0)=0
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m5.COLUMNA03=p.COLUMNA03 and isnull(m5.COLUMNA13,0)=0   
  left outer join PUTABLE005 f1 on f.COLUMN13=f1.COLUMN01 and f1.COLUMNA03=f.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
 and m5.COLUMNA03=@AcOwner
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN08 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner
union all
select o.COLUMN03 as OPUnit,'' Vendor,p.COLUMN05 Date,cast((f.COLUMN07+(f.COLUMN10*f.COLUMN08)+(f.COLUMN10*f.COLUMN16)) as decimal(18,2)) Amount,'' VID,p.COLUMN10 OPID,isnull(m.COLUMN10,0) BrandID, m5.COLUMN04 as Brand
 ,p.COLUMN04 Trans# ,1251 fid ,''v  ,@FromDate fd,@ToDate td from FITABLE014 p left outer join FITABLE015 f on f.COLUMN11=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN10  and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN23 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03 left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0   
 and m5.COLUMNA03=@AcOwner where isnull(p.COLUMNA13,0)=0  and 
p.COLUMN05 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND 
P.COLUMNA03=@AcOwner) Query

set @Query1='select * from #PurchasesBySuppliersSummary'+@whereStr
exec (@Query1) 
end
if @ReportName='PurchasesBySuppliersDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
select * into #PurchasesBySuppliersDetails from (
select 'Bill' as AA,o.COLUMN03 as OPUnit,s.COLUMN05 Vendor,FORMAT(p.COLUMN08,@DateF) Date,p.COLUMN08 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN12 memo,f.COLUMN09 Qty,f.COLUMN11 rate,f.COLUMN12 Amount,p.COLUMN05 VID,p.COLUMN15 OPID 
,m.COLUMN10 BrandID, m5.COLUMN04 as Brand ,p.COLUMN03 fid from PUTABLE005 p 
left outer join PUTABLE006 f on f.COLUMN13=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN15 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN27 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN04 
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN04 and isnull(m5.COLUMNA13,0)=0   
 and m5.COLUMNA03=@AcOwner
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN08 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner
union all
select 'Inventory Adjustments' as AA,o.COLUMN03 as OPUnit,'' Vendor,FORMAT(p.COLUMN05,@DateF) Date,p.COLUMN05 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN15 memo,(f.COLUMN06) Qty,f.COLUMN10 rate,cast((f.COLUMN07+(f.COLUMN10*f.COLUMN08)+(f.COLUMN10*f.COLUMN16)) as decimal(18,2)) Amount,'' VID,p.COLUMN10 OPID 
,m.COLUMN10 BrandID, m5.COLUMN04 as Brand ,1273 fid from FITABLE014 p left outer join FITABLE015 f on f.COLUMN11=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN10  and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN23 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0  
and m5.COLUMNA03=@AcOwner
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN05 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue 
 FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner) Query

set @Query1='Select AA,OPUnit,Vendor,Date,Dtt,TransType,Trans#,Lot,memo,sum(rate)rate,sum(Qty)Qty,
sum(Amount)Amount,Brand,BrandID,fid from #PurchasesBySuppliersDetails ' + @whereStr + 'group by AA,OPUnit,Vendor,Date,Dtt,TransType,Trans#,Lot,memo,Brand,BrandID,fid order by Dtt desc'
exec (@Query1) 


END

end







GO
