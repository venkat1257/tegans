USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_AdvanceCalculations]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SAL_TP_AdvanceCalculations](@refID nvarchar(250)=null,@COLUMNA02 nvarchar(250)=null,
@COLUMNA03 nvarchar(250)=null,@COLUMN02 nvarchar(250)=null,@COLUMN08 nvarchar(250)=null,@VouchrAmt decimal(18,2)=null)
as 
begin

declare @newID1 int,@VendorID int,@Bank nvarchar(250)=null,@Memo1 nvarchar(250),@project1 int,@DATE nvarchar(250),
@IDAR nvarchar(250),@dueamt decimal(18,2),@paidamt decimal(18,2),@COLUMN05 decimal(18,2),@COLUMNA06 nvarchar(250),@COLUMNA08 nvarchar(250),@COLUMNA09 nvarchar(250),
@Duepayment nvarchar(250),@creditpayment nvarchar(250)
	set @newID1=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1;
	set @VendorID=(select COLUMN05 from SATABLE011 where column01=@COLUMN08)
	select @Bank=COLUMN08,@COLUMNA06 = @COLUMNA06,@COLUMNA08 = @COLUMNA08,@COLUMNA09 = @COLUMNA09 from SATABLE011 where column01=@COLUMN08
	set @Memo1=(select COLUMN10 from SATABLE011 where column01=@COLUMN08)
	set @DATE=(select COLUMN19 from SATABLE011 where column01=@COLUMN08)
	set @project1=(select isnull(COLUMN22,0) from SATABLE011 where column01=@COLUMN08)
	set @Duepayment=(select isnull(COLUMN03,0) from SATABLE012 where column02=@COLUMN02)
	set @creditpayment=(select isnull(COLUMN08,0) from SATABLE012 where column02=@COLUMN02)
	if(cast(isnull(@VouchrAmt,0) as decimal(18,2))> 0)
	begin
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @refID, @COLUMN05 = @COLUMN08,  @COLUMN06 = NULL,
		@COLUMN07 = @DATE,   @COLUMN08 = '22335',  @COLUMN09 = @VendorID,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @Memo1,   @COLUMN13 = @project1,	@COLUMN14 = @VouchrAmt,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	

	if exists(select COLUMN05  from PUTABLE018 where COLUMN05=@refID and COLUMN06='Payment' AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and COLUMN17=@COLUMN08)
	begin
			declare @pstatus nvarchar(250)
			set @dueamt=(select COLUMN12 from PUTABLE018 where COLUMN05=@refID and COLUMN06='Payment' AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and COLUMN17=@COLUMN08 ) --and COLUMN16=@Duepayment AND COLUMN17=@creditpayment
			set @paidamt=(select COLUMN11 from PUTABLE018 where COLUMN05=@refID and COLUMN06='Payment' AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02  and COLUMN17=@COLUMN08 )
			set @pstatus=('Paid')
			set @COLUMN05 =(select COLUMN05 from SATABLE012 where column02=@COLUMN02);
		   if(cast(@COLUMN05 as decimal(18,2))=0)
		   begin
		   set @pstatus=('CLOSE')
		   end
            update PUTABLE018 set COLUMN13=@pstatus, COLUMNA07=GETDATE(),
			COLUMN11=(cast(isnull(@paidamt,0) as DECIMAL(18,2))+cast(isnull(@VouchrAmt,0) as DECIMAL(18,2))), COLUMN12=(cast(isnull(@dueamt,0) as DECIMAL(18,2))+cast(isnull(@VouchrAmt,0) as DECIMAL(18,2)))
			where COLUMN05=@refID and COLUMN06='Payment' AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and COLUMN17=@COLUMN08 --and COLUMN16=@Duepayment AND COLUMN17=@creditpayment
	end
else
begin
	if(@Bank='' or @Bank=null)
	begin
	set @Bank=NULL
	end
insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08 ,column11,column12,COLUMN13,COLUMNA02, COLUMNA03, COLUMN17, COLUMN18)
			values(@newID1,3000,@DATE,@refID,'Payment',@VendorID,@Memo1,@VouchrAmt,NULL,'Paid',@COLUMNA02 , @COLUMNA03,@COLUMN08,@project1) 
end
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @refID, @COLUMN05 = @COLUMN08,  @COLUMN06 = NULL,
		@COLUMN07 = @DATE,   @COLUMN08 = '22335',  @COLUMN09 = @VendorID,    @COLUMN10 = '22383', @COLUMN11 = '1119',
		@COLUMN12 = @Memo1,   @COLUMN13 = @project1,	@COLUMN14 = 0,       @COLUMN15 = @VouchrAmt,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
set @IDAR= (SELECT MAX(COLUMN02) FROM FITABLE026)+1
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
	values(@IDAR,@DATE,'PAYMENT',@COLUMN08,@VendorID,@Memo1,'1119',@refID,cast(@VouchrAmt as decimal(18,2)),cast(@VouchrAmt as decimal(18,2)),'Advances Received',@Project1,@COLUMNA02, @COLUMNA03)
    end
	end




GO
