USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FIN_TP_PRICETYPE]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FIN_TP_PRICETYPE]
@UOMData XML,@ItemName nvarchar(250)=null,@OperatingUnit nvarchar(250)=null, @AccountOwner nvarchar(250)=null
AS
BEGIN
begin try
      SET NOCOUNT ON; 
	  declare @Quantity nvarchar(250), @Price nvarchar(250),@col2 int,@id int,@item int
	  DELETE FROM FITABLE013  where columnA02=@OperatingUnit and COLUMN02=@ItemName and COLUMNA03=@AccountOwner
	  if not exists( SELECT COLUMN01 from FITABLE013 where columnA02=@OperatingUnit and COLUMN02=@ItemName and COLUMNA03=@AccountOwner)
      begin
	  INSERT INTO FITABLE013(COLUMN02,COLUMN03,COLUMN04,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13) 
	  SELECT
      UOM.details.value('(item)[1]','varchar(100)') as COLUMN02,
      UOM.details.value('(TypeId)[1]','varchar(100)') as COLUMN03,
      UOM.details.value('(Price)[1]','varchar(100)') as COLUMN04,
	  @OperatingUnit,@AccountOwner,1,0
      from @UOMData.nodes('/ROOT/Column') as UOM(details)
	  
		end
		else 
		begin
		;WITH XmlData AS 
		(
		SELECT
      UOM.details.value('(item)[1]','varchar(100)') as COLUMN02,
      UOM.details.value('(TypeId)[1]','varchar(100)') as COLUMN03,
      UOM.details.value('(Price)[1]','varchar(100)') as COLUMN04
		FROM 
		    @UOMData.nodes('/ROOT/Column') AS UOM(details)
		)
	
        UPDATE FITABLE013 SET 
        COLUMN02 = x.COLUMN02,
        COLUMN03 = x.COLUMN03,
        COLUMN04 = x.COLUMN04,
        COLUMNA02=@OperatingUnit,COLUMNA03=@AccountOwner,COLUMNA12=1,COLUMNA13=0
	    from XmlData x
	  --update FITABLE038  set 
	  --COLUMN07=(SELECT UOM.details.value('(Quantity)[1]','varchar(100)') as Quantity from @UOMData.nodes('/ROOT/Column') as UOM(details)),
	  --COLUMN08=(SELECT UOM.details.value('(UnitIds)[1]','varchar(100)') as Units from @UOMData.nodes('/ROOT/Column') as UOM(details)),
	  --COLUMN09=(SELECT  UOM.details.value('(Price)[1]','varchar(100)') as Price from @UOMData.nodes('/ROOT/Column') as UOM(details)),
	    where FITABLE013.COLUMN02=x.COLUMN02 and FITABLE013.COLUMN03 = x.COLUMN03 and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner
		end
		end try
		begin catch
		
		SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		end catch

END
GO
