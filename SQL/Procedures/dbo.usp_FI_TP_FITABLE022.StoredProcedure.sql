USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE022]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE022]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null, 
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null, 
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)
AS
BEGIN
 begin try
 DECLARE @dt date
 DECLARE @AC int
 DECLARE @ID varchar(250)
 declare @cheque nvarchar(250),@Payee nvarchar(250),@PayeeType nvarchar(250),@INTERNAL nvarchar(250),@ACTYPE nvarchar(250),@CharAcc nvarchar(250)
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
 SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE022_SequenceNo
 --set @COLUMN09= (select max(column01) from FITABLE020)
 set @dt= (select column05 from FITABLE020 where COLUMN01=@COLUMN09)
 set @AC= (select column06 from FITABLE020 where COLUMN01=@COLUMN09)
 set @ID= (select column04 from FITABLE020 where COLUMN01=@COLUMN09)
  set @cheque= (select column09 from FITABLE020 where COLUMN01=@COLUMN09)
  select @Payee = column07, @PayeeType = column11 from FITABLE020 where COLUMN01=@COLUMN09 
  --EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
  declare @tempSTR nvarchar(max),@memo nvarchar(250),@PayMode nvarchar(250)
 set @memo= (select column08 from FITABLE020 where COLUMN01=@COLUMN09)
 set @PayMode= (select column20 from FITABLE020 where COLUMN01=@COLUMN09)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment Line Values are Intiated for FITABLE022 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into FITABLE022
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, 
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09, 
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13,
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
set @INTERNAL=(SELECT COLUMN01 FROM FITABLE022 WHERE COLUMN02=@COLUMN02 AND COLUMNA03=@COLUMNA03)

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment Line Intiated Values are Created in FITABLE022 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
   declare @status nvarchar(250)
   set @status=(select column04 from contable025 where column02=64)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment Status Updation for FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Status'+
   isnull(cast(@status as nvarchar(250)),'') +','+  '2.Advance Payment No'+','+  isnull(cast(@COLUMN02  as nvarchar(250)),'')+','+ '4.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 
	declare @Vid1 nvarchar(250),@Exclusive int,@Totbal decimal(18,2)
 select @Exclusive=COLUMN28 from FITABLE020 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(@COLUMN05,0)-ISNULL(column26,0) from FITABLE020 where COLUMN01=@COLUMN09)
else
set @Totbal= (@COLUMN05)

update FITABLE020 set  COLUMN10=@COLUMN05,COLUMN18=@Totbal,COLUMN16=@status where column01=@COLUMN09 and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Payment Updated in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
    isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+ isnull( cast(@COLUMN02 as nvarchar(250)),'')+','+ isnull(cast(@cheque as nvarchar(250)),'')+','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 update FITABLE044 set  COLUMN18=(select column02 from FITABLE020 where COLUMN01=@COLUMN09),COLUMN19=(@COLUMN02) where column02=@cheque and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
 if((@cheque!='' or @cheque!=null) and @PayMode=22273)
 begin
 update FITABLE044 set COLUMN15=22629 where COLUMN02=@cheque and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
 end
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''))
 declare @balance decimal(18,2) , @TAXRATE decimal(18,2),@pid nvarchar(250)=null, @Apid nvarchar(250)=null, @internalid nvarchar(250)=null
 declare @Vid nvarchar(250)=null, @Tax nvarchar(250)=null, @TaxAG nvarchar(250)=null
 set @Tax= (select column17 from FITABLE020 where COLUMN01=@COLUMN09)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 set @TAXRATE= (select isnull(COLUMN17,0) from MATABLE013 where COLUMN02=@Tax)
set @internalid= (select column01 from FITABLE022 where COLUMN02=@COLUMN02)
set @Apid= (select column04 from FITABLE020  where COLUMN01=@COLUMN09)
declare @TaxString nvarchar(250)=NULL,@TAXRATEM DECIMAL(18,2)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN05,@TTYPE INT = 0,@HTTYPE INT = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=isnull(COLUMN17,0) from FITABLE020 where COLUMN01=@COLUMN09
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	if(isnull(cast(@COLUMN05 as decimal(18,2)),0)>0)
		BEGIN
		declare @TaxColumn17 nvarchar(250),@ltaxamt DECIMAL(11,2)
		set @ltaxamt=(cast(@Totbal as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
		'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
		'The Values are '+
		cast(isnull(@Vid,999)as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ 'ADVANCE PAYMENT'+','+ isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast(@Apid as nvarchar(250)),'')+','+ isnull( cast(@Tax as nvarchar(250)),'')+','+ isnull( cast(@TaxAG as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+  isnull(cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+isnull(cast(@TAXRATE as nvarchar(250)),'') +','+isnull(cast(@TaxColumn17 as nvarchar(250)),'') +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
SELECT top 1 @ACTYPE=COLUMN07,@CharAcc=column02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03 
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @DT,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ACTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN05, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN09,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN17,COLUMN19,  COLUMNA02, COLUMNA03)
			values(@Vid,@dt,'ADVANCE PAYMENT',@COLUMN09,@Payee,@Apid,@Tax,@TaxAG,@ltaxamt,(cast(@COLUMN05 as decimal              (18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))),@TAXRATE,@TaxColumn17,@COLUMN08,@COLUMNA02, @COLUMNA03)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
		END
END
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22402', @COLUMN11 = '1120',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid1=((select isnull(max(column02),999) from FITABLE034)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@Vid1,999)as nvarchar(250))+','+ 'ADVANCE PAYMENT' +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+isnull( cast(@Apid as nvarchar(250)),'')+','+ '1120'+','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
		--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN11,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
		-- values(@Vid1,@dt,'ADVANCE PAYMENT',@internalid,@COLUMN07,'1120',@Apid,(cast(@COLUMN05 as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))),(cast(@COLUMN05 as decimal(18,2))           +cast(isnull(@COLUMN06,0) as decimal(18,2))),'Advances Paid',@COLUMN08,@COLUMNA02, @COLUMNA03)
	insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@Vid1,999),  'ADVANCE PAYMENT',  @dt,  @memo,  @Payee,  @Totbal,0,@Apid,'1120',@COLUMN08,
	@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''))
			declare @PDC nvarchar(250)
			set @PDC=(select COLUMN20 from FITABLE020 where COLUMN01=@COLUMN09)
			if(@PDC=22627)
			begin
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PDC Issued',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22383', @COLUMN11 = '1117',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @COLUMN05,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
declare @liabid nvarchar(250)
     set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC ISSUEd Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@liabid,999)as nvarchar(250)) +','+ 'ADVANCE PAYMENT'+','+isnull(cast(@dt as nvarchar(250)),'')+','+  isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+ isnull(cast(@Apid as nvarchar(250)),'')+','+  'PDC Issued'+','+'1117' +','+isnull(cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'') +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
	insert into FITABLE026 
	(COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN11,    COLUMN09,COLUMN17,COLUMN08,
	COLUMN14,COLUMN19,COLUMNA02, COLUMNA03 	)
	values( 
	@liabid,  'ADVANCE PAYMENT',  @dt,  @internalid,  @Payee,  (cast(@COLUMN05 as decimal(18,2))), @Apid,'PDC Issued','1117',
	(cast(@COLUMN05 as decimal(18,2))),@COLUMN08,@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'PDC ISSUEd Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
else
begin
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
declare @Bank nvarchar(250) 
 declare @pid1 nvarchar(250)=null,@ChqNo nvarchar(250)
 --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
declare @Hinternalid nvarchar(250)=null,@AccountType nvarchar(250)=null
set @Hinternalid= (select column01 from FITABLE020 where COLUMN01=@COLUMN09)
set @Bank= (select column06 from FITABLE020 where COLUMN01=@COLUMN09)
set @ChqNo= (select column09 from FITABLE020 where COLUMN01=@COLUMN09)
set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Bank AND COLUMNA03=@COLUMNA03)
 select @PayeeType= COLUMN11,@Exclusive=COLUMN28 from FITABLE020 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(column10,0) from FITABLE020 where COLUMN01=@COLUMN09)
else
set @Totbal= (select ISNULL(column10,0)+ISNULL(column26,0) from FITABLE020 where COLUMN01=@COLUMN09)
--Cash
 IF(@AccountType=22409)
BEGIN
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @Bank,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @pid1=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@pid1,1000),  @Bank,  @dt,  @ID,  'ADVANCE PAYMENT',@PayeeType,  @Payee,  @Hinternalid,  @memo,@Totbal,  
	0,@Totbal,@COLUMN08,  @COLUMNA02, @COLUMNA03,@COLUMNA08)
END
ELSE
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @Bank,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @pid1=((select max(column02) from PUTABLE019)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@pid1,999)as nvarchar(250)) +','+ isnull(cast(@Bank as nvarchar(250)),'')+','+ isnull( cast(@dt as nvarchar(250)),'')+','+ isnull(cast(@ID as nvarchar(250)),'')+','+  'ADVANCE PAYMENT'+','+ isnull( cast(@Payee as nvarchar(250)),'')+','+ isnull( cast(@Hinternalid as nvarchar(250)),'')+','+ isnull( cast(@COLUMN05 as nvarchar(250)),'')+','+ isnull( cast((@balance -@COLUMN05) as nvarchar(250)),'')+','+  isnull(cast(@ChqNo as nvarchar(250)),'')+','+isnull(cast(@COLUMN08 as nvarchar(250)),'')+','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMN20,COLUMNA02, COLUMNA03)
 values(@pid1,@Bank,@dt,@ID,'ADVANCE PAYMENT',@Payee,@Hinternalid,@Totbal,@Totbal,@ChqNo,@COLUMN08,@COLUMNA02, @COLUMNA03)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register Intiated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Balance Amount'+ isnull(cast(@balance as nvarchar(250)),'') +','+  '2.Payment Amount'+','+  isnull(cast(@COLUMN05  as nvarchar(250)),'')+','+ '4.Account'+','+ isnull(cast( @Bank  as nvarchar(250)),'')+','+'4.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@COLUMN05 as decimal(18,2))) where column02=@Bank and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
		--update PUTABLE019 set COLUMN20=@COLUMN08 where column08=@COLUMN09 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end

set @ReturnValue = 1
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE022
END 

ELSE IF @Direction = 'Update'
BEGIN
declare @taxtype nvarchar(250)
        set @internalid= (select column01 from FITABLE022 where COLUMN02=@COLUMN02)
		set @COLUMN09=(select column09 from FITABLE022 where COLUMN02=@COLUMN02)
		set @taxtype= (select COLUMN17 from FITABLE020 where COLUMN01=@COLUMN09)
		set @COLUMNA02=(select COLUMNA02 from FITABLE020 where COLUMN01=@COLUMN09)
		select @Payee = column07, @PayeeType = column11 from FITABLE020 where COLUMN01=@COLUMN09 
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		set @ID= (select column04 from FITABLE020 where COLUMN01=@COLUMN09)
		set @memo= (select column08 from FITABLE020 where COLUMN01=@COLUMN09)
		delete from FITABLE026 where COLUMN05=@internalid and  COLUMN08=@taxtype and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		delete from FITABLE034 where COLUMN09=@ID and COLUMN03='ADVANCE PAYMENT' and COLUMN10=1120 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		set @PDC=(select COLUMN20 from FITABLE020 where COLUMN01=@COLUMN09)
		set @PayMode= (select column20 from FITABLE020 where COLUMN01=@COLUMN09)
		if(@PDC=22627)
		begin
		delete from FITABLE026 where COLUMN05=@internalid and COLUMN17='PDC Issued' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		end
		

set @COLUMN09= (select column01 from FITABLE020 WHERE COLUMN01=@COLUMN09)
 set @dt= (select column05 from FITABLE020 where COLUMN01=@COLUMN09)
 set @ID= (select column04 from FITABLE020 where COLUMN01=@COLUMN09)
 set @AC= (select column06 from FITABLE020 where COLUMN01=@COLUMN09)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Advance Payment Line Values are Intiated for FITABLE022 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE FITABLE022 SET
--EMPHCS1067	when we enter two lines advance amount is not considering BY RAJ.Jr 11/9/2015
   COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMND04=@COLUMND04,                         COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02=@COLUMN02 
set @INTERNAL=(SELECT COLUMN01 FROM FITABLE022 WHERE COLUMN02=@COLUMN02 AND COLUMNA03=@COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Advance Payment Line Intiated Values are Created in FITABLE022 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
    update FITABLE020 set  COLUMN10=@COLUMN05 where column01=@COLUMN09 and COLUMNA03=@COLUMNA03
  set @cheque= (select column09 from FITABLE020 where COLUMN01=@COLUMN09)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
    isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+ isnull( cast(@COLUMN02 as nvarchar(250)),'')+','+ isnull(cast(@cheque as nvarchar(250)),'')+','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
update FITABLE044 set  COLUMN18=(select column02 from FITABLE020 where COLUMN01=@COLUMN09),COLUMN19=(@COLUMN02) where column02=@cheque and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
if((@cheque!='' or @cheque!=null) and @PayMode=22273)
 begin
 update FITABLE044 set COLUMN15=22629 where COLUMN02=@cheque and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
 end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'write cheque updation for reference in for FITABLE044 Table '+'' + CHAR(13)+CHAR(10) + ''+' '))
		set @Tax= (select column17 from FITABLE020 where COLUMN01=@COLUMN09)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 set @TAXRATE= (select isnull(COLUMN17,0) from MATABLE013 where COLUMN02=@Tax)
set @internalid= (select column01 from FITABLE022 where COLUMN02=@COLUMN02)
set @Apid= (select column04 from FITABLE020  where COLUMN01=@COLUMN09)
 select @Exclusive=COLUMN28 from FITABLE020 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(column10,0)-ISNULL(column26,0) from FITABLE020 where COLUMN01=@COLUMN09)
else
set @Totbal= (select ISNULL(column10,0) from FITABLE020 where COLUMN01=@COLUMN09)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 set @cnt  = 0 
 set @vochercnt = 1
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=isnull(COLUMN17,0) from FITABLE020 where COLUMN01=@COLUMN09
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	if(isnull(cast(@COLUMN05 as decimal(18,2)),0)>0)
		BEGIN
		set @ltaxamt=(cast(@Totbal as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		SELECT top 1 @ACTYPE=COLUMN07,@CharAcc=column02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03 
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @DT,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @ACTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN05, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = NULL,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
		'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
		'The Values are '+
		cast(isnull(@Vid,999)as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ 'ADVANCE PAYMENT'+','+ isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast(@Apid as nvarchar(250)),'')+','+ isnull( cast(@Tax as nvarchar(250)),'')+','+ isnull( cast(@TaxAG as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+  isnull(cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+isnull(cast(@TAXRATE as nvarchar(250)),'') +','+isnull(cast(@TaxColumn17 as nvarchar(250)),'') +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
set @Vid=((select isnull(max(column02),999) from FITABLE026)+1)
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN09,COLUMN08,COLUMN10,  COLUMN11,  COLUMN14,  COLUMN16,COLUMN17,COLUMN19,  COLUMNA02, COLUMNA03)
			values(@Vid,@dt,'ADVANCE PAYMENT',@COLUMN09,@Payee,@Apid,@Tax,@TaxAG,@ltaxamt,(cast(@COLUMN05 as decimal              (18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))),@TAXRATE,@TaxColumn17,@COLUMN08,@COLUMNA02, @COLUMNA03)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'tax  Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
		END
END
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22402', @COLUMN11 = '1120',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = 0,       @COLUMN15 = @Totbal,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid1=((select isnull(max(column02),999) from FITABLE034)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   --cast(isnull(@Vid1,999)as nvarchar(250)) +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ 'ADVANCE PAYMENT'+','+ isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@COLUMN07 as nvarchar(250)),'')+','+ '1120'+','+isnull( cast(@Apid as nvarchar(250)),'')+','+isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+  isnull(cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+'Advances Paid' +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
   cast(isnull(@Vid1,999)as nvarchar(250))+','+ 'ADVANCE PAYMENT' +','+ isnull(cast(@dt as nvarchar(250)),'')+','+ isnull(cast(@memo as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+isnull( cast(@Apid as nvarchar(250)),'')+','+ '1120'+','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
		--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN11,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
		--	values(@Vid1,@dt,'ADVANCE PAYMENT',@internalid,@COLUMN07,'1120',@Apid,(cast(@COLUMN05 as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))),(cast(@COLUMN05 as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))),'Advances Paid',@COLUMN08,@COLUMNA02, @COLUMNA03)
		insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@Vid1,999),  'ADVANCE PAYMENT',  @dt,  @memo,  @Payee, @Totbal,0,@Apid,'1120',@COLUMN08,
	@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+' '))
			set @PDC=(select COLUMN20 from FITABLE020 where COLUMN01=@COLUMN09)
			if(@PDC=22627)
			begin
     exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PDC Issued',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = '22383', @COLUMN11 = '1117',
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @COLUMN05,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @liabid=((select isnull(max(column02),999) from FITABLE026)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC ISSUEd Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@liabid,999)as nvarchar(250)) +','+ 'ADVANCE PAYMENT'+','+isnull(cast(@dt as nvarchar(250)),'')+','+  isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull(cast(@Payee as nvarchar(250)),'')+','+ isnull( cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'')+','+ isnull(cast(@Apid as nvarchar(250)),'')+','+  'PDC Issued'+','+'1117' +','+isnull(cast((@COLUMN05 +@COLUMN06) as nvarchar(250)),'') +','+isnull(cast(@COLUMN08 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
	insert into FITABLE026 
	(COLUMN02,  COLUMN04,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN11,    COLUMN09,COLUMN17,COLUMN08,
	COLUMN14,COLUMN19,COLUMNA02, COLUMNA03 	)
	values( 
	@liabid,  'ADVANCE PAYMENT',  @dt,  @internalid,  @Payee,  (cast(@COLUMN05 as decimal(18,2))), @Apid,'PDC Issued','1117',
	(cast(@COLUMN05 as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))),@COLUMN08,@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC ISSUEd Values are Intiated for FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+' '))
end
else
begin
set @Hinternalid= (select column01 from FITABLE020 where COLUMN01=@COLUMN09)
set @Bank= (select column06 from FITABLE020 where COLUMN01=@COLUMN09)
set @ID=(select column04 from FITABLE020 where COLUMN01=@COLUMN09)
set @ChqNo= (select column09 from FITABLE020 where COLUMN01=@COLUMN09)
set @balance=(select column10 from FITABLE001 where COLUMN02=@Bank and COLUMNA03=@COLUMNA03)
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Bank AND COLUMNA03=@COLUMNA03)
set @AccountType=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Bank AND COLUMNA03=@COLUMNA03)
 select @PayeeType= COLUMN11,@Exclusive=COLUMN28 from FITABLE020 where COLUMN01=@COLUMN09 AND COLUMNA03=@COLUMNA03
 if(@Exclusive = 22713)
set @Totbal= (select ISNULL(column10,0) from FITABLE020 where COLUMN01=@COLUMN09)
else
set @Totbal= (select ISNULL(column10,0)+ISNULL(column26,0) from FITABLE020 where COLUMN01=@COLUMN09)
--Cash
 IF(@AccountType=22409)
BEGIN
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @Bank,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @pid1=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
	insert into FITABLE052 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06, COLUMN07, COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
	 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
	values(
	ISNULL(@pid1,1000),  @Bank,  @dt,  @ID,  'ADVANCE PAYMENT',@PayeeType,  @Payee,  @Hinternalid,  @memo,@Totbal,  
	0,@Totbal,@COLUMN08,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
END
ELSE
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'ADVANCE PAYMENT',  @COLUMN04 = @ID, @COLUMN05 = @COLUMN09,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @dt,   @COLUMN08 = @PayeeType,  @COLUMN09 = @Payee,    @COLUMN10 = @AccountType, @COLUMN11 = @Bank,
		@COLUMN12 = @memo,   @COLUMN13 = @COLUMN08,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @pid1=((select max(column02) from PUTABLE019)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@pid1,999)as nvarchar(250)) +','+ isnull(cast(@Bank as nvarchar(250)),'')+','+ isnull( cast(@dt as nvarchar(250)),'')+','+ isnull(cast(@ID as nvarchar(250)),'')+','+  'ADVANCE PAYMENT'+','+ isnull( cast(@Payee as nvarchar(250)),'')+','+ isnull( cast(@Hinternalid as nvarchar(250)),'')+','+ isnull( cast(@COLUMN05 as nvarchar(250)),'')+','+ isnull( cast((@balance -@COLUMN05) as nvarchar(250)),'')+','+  isnull(cast(@ChqNo as nvarchar(250)),'')+','+isnull(cast(@COLUMN08 as nvarchar(250)),'')+','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMN20,COLUMNA02, COLUMNA03)
 values(@pid1,@Bank,@dt,@ID,'ADVANCE PAYMENT',@Payee,@Hinternalid,@Totbal,@Totbal,@ChqNo,@COLUMN08,@COLUMNA02, @COLUMNA03)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register Intiated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Balance Amount'+ isnull(cast(@balance as nvarchar(250)),'') +','+  '2.Payment Amount'+','+  isnull(cast(@COLUMN05  as nvarchar(250)),'')+','+ '4.Account'+','+ isnull(cast( @Bank  as nvarchar(250)),'')+','+'4.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@COLUMN05 as decimal(18,2))) where column02=@Bank and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
set @ReturnValue = 1
END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE022 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END
exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE022.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_FITABLE022.txt',0
end catch
end





GO
