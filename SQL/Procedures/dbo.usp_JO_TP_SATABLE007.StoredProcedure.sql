USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE007]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE007]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null, @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null, @COLUMN19   nvarchar(250)=null,
	@COLUMN20  nvarchar(250)=null,   @COLUMN21  nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250)=null,       
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE007_SequenceNo
insert into SATABLE007 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,COLUMN16, COLUMN17,  COLUMN18,COLUMN19, COLUMN20, COLUMN21, COLUMN22,
  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19,@COLUMN20, @COLUMN21, @COLUMN22,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application 
set @ReturnValue =(SELECT COLUMN01 FROM SATABLE007 where COLUMN02=@COLUMN02)
END

IF @Direction = 'Select'
BEGIN
select * from SATABLE007
END 

 
IF @Direction = 'Update'
BEGIN
UPDATE SATABLE007 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,  COLUMN16=@COLUMN16, 
   COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,     COLUMN22=@COLUMN22,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
   COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   
 declare @SOID nvarchar(250),@ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250)
 declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
 
 @Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2),@Initialrow INT,@MaxRownum INT,@Item nvarchar(250),@id nvarchar(250),@uom nvarchar(250),@LOT nvarchar(250),@upcno nvarchar(250) = null
      set @SOID=(select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14 in(@SOID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE008 where COLUMN14 in(@SOID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN04 from SATABLE008 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
             
	         select @uom=isnull(COLUMN19,0),@LOT=isnull(COLUMN24,0),@upcno= COLUMN06 from SATABLE008 where COLUMN02=@id and COLUMN04=@Item
             set @Price=(select isnull(COLUMN12,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
			 DECLARE @PrevWIP Decimal(18,2),  @WIP Decimal(18,2), @Prevshipd Decimal(18,2)
			 SET @PrevWIP =(SELECT column09  FROM   satable008  WHERE COLUMN02=@id and COLUMN04=@Item and COLUMN19=@uom) 

			    set @Prevshipd=(select sum(isnull(COLUMN12,0)) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item and COLUMN27=@uom)
				UPDATE SATABLE006 SET   COLUMN12=(CAST(@Prevshipd AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMN27=@uom and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06))
				UPDATE PUTABLE013 SET COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
			   
				set @TotalItemQty=((cast((select max(isnull(COLUMN08,0)) from SATABLE008 where  column02=@COLUMN02 and COLUMN04=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN07,0)) from SATABLE008 where  column02=@COLUMN02 and COLUMN04=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				--update SATABLE008 set COLUMN08=@TotalItemQty, COLUMN09=0 where column02=@COLUMN02

             --Inventory Updation
				--EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
				declare @Opunit nvarchar(250),@Backorder bit
				set @Opunit=(@COLUMNA02)
				set @Opunit=(iif(@Opunit='',0 ,isnull(@Opunit,0)))
				if exists(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
				begin
				set @Backorder=(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
				end
				else
				begin
				set @Backorder=(0)
				end
				--if(@Backorder!=1)
				--begin
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot  AND COLUMN24 =@upcno and columna03 = @columna03)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				 if exists(select COLUMN03 from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)as decimal(18,2)))+ cast(@ItemQty as decimal(18,2));
								set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)

				set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as decimal(18,2));
				UPDATE FITABLE010 SET COLUMN04 =COLUMN04+cast(@ItemQty as decimal(18,2)), COLUMN05=@Qty_Cmtd, COLUMN08=COLUMN04+@ItemQty ,COLUMN18=(@WIP-@PrevWIP),COLUMN12=@Qty_On_Hand*COLUMN17 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03 and columna02 = @columna02 and isnull(columna13,0) = 0 and COLUMN21 =0 AND COLUMN22= 0
				--if(cast(@Qty_On_Hand as decimal(18,2))=0)
				--begin
				--UPDATE FITABLE010 SET COLUMN17=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				--end
				--else
				--begin
				--UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				--end
				end
				--end
				--Inventory Asset table Updations
				delete from PUTABLE011 where COLUMN02=@SOID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN15 and COLUMNA03=@COLUMNA03
				delete from PUTABLE017 where COLUMN05=@SOID and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN15 and COLUMNA03=@COLUMNA03 and column06 = 'JobOrder Issue'
			 --set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
if  exists( SELECT 1 FROM PUTABLE013 WHERE COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02))
begin
delete from PUTABLE013  where COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
end
UPDATE SATABLE008 SET COLUMNA13=1 WHERE COLUMN14 in( select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02 )
END


else IF @Direction = 'Delete'
BEGIN

      set @COLUMN06=(select COLUMN06 from SATABLE007 WHERE COLUMN02 = @COLUMN02)

      set @COLUMN15=(select COLUMN15 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN05=(select COLUMN05 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN04=(select COLUMN04 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA03=(select COLUMNA03 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA02=(select COLUMNA02 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @SOID=(select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
	  set @Initialrow =(1)
	   DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14 in(@SOID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE008 where COLUMN14 in(@SOID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN04 from SATABLE008 where COLUMN02=@id)
			 select @uom=isnull(COLUMN19,0),@LOT=isnull(COLUMN24,0),@upcno = COLUMN06 from SATABLE008 where COLUMN02=@id and COLUMN04=@Item
             set @ItemQty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item and COLUMN19=@uom)
             --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue

             set @Price=(select isnull(COLUMN12,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
			 
			 --Received Qty Updation
			     set @Prevshipd=(select sum(isnull(COLUMN12,0)) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item and COLUMN27=@uom)
				UPDATE SATABLE006 SET   COLUMN12=(CAST(@Prevshipd AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMN27=@uom and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06))
				UPDATE PUTABLE013 SET COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
			   
				set @TotalItemQty=((cast((select max(isnull(COLUMN08,0)) from SATABLE008 where  column02=@COLUMN02 and COLUMN04=@Item and COLUMN19=@uom) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN11,0)) from SATABLE008 where  column02=@COLUMN02 and COLUMN04=@Item and COLUMN19=@uom) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				--update SATABLE008 set COLUMN08=@TotalItemQty, COLUMN09=0, COLUMN11=@RemainItemQty where column02=@COLUMN02
				--EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
				set @Opunit=(@COLUMNA02)
				set @Opunit=(iif(@Opunit='',0 ,isnull(@Opunit,0)))
				if exists(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
				begin
				set @Backorder=(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
				end
				else
				begin
				set @Backorder=(0)
				end
				--if(@Backorder!=1)
				--begin
             --Inventory Updation
	     		     --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				 if exists(select COLUMN03 from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)as decimal(18,2)))+ cast(@ItemQty as decimal(18,2));

				 SET @PrevWIP =(SELECT column09  FROM   satable008  WHERE COLUMN02=@id and COLUMN04=@Item and COLUMN19=@uom) 
				 set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03)

				set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as decimal(18,2));
				UPDATE FITABLE010 SET COLUMN04 =COLUMN04+@ItemQty, COLUMN05=@Qty_Cmtd, COLUMN08=COLUMN04+@ItemQty ,COLUMN18=(@WIP-@PrevWIP),COLUMN12=@Qty_On_Hand*COLUMN17 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN22,0)=@lot AND COLUMN24 =@upcno and columna03 = @columna03 and columna02 = @columna02 and isnull(columna13,0) = 0 and COLUMN21 =0 AND COLUMN22= 0
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
    --            if(cast(@Qty_On_Hand as decimal(18,2))=0)
				--begin
				--UPDATE FITABLE010 SET COLUMN17=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				--end
				--else
				--begin
				--UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom
				--end
				end
				--end
				--Inventory Asset table Updations
				delete from PUTABLE011 where COLUMN02=@SOID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN15 and COLUMNA03=@COLUMNA03
				delete from PUTABLE017 where COLUMN05=@SOID and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN15 and COLUMNA03=@COLUMNA03 and column06 = 'JobOrder Issue'
			 --set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1
  UPDATE SATABLE007 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
UPDATE SATABLE008 SET COLUMNA13=1 WHERE COLUMN14 in( select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02 )
--EMPHCS894 rajasekhar reddy patakota 8/9/2015 For sales order after reversal of invoice and Item issue , the status is showing with internal id 21 rather than status in sales order info page
   declare @TQty decimal(18,2),@IFQty decimal(18,2),@IVQty decimal(18,2),@Result nvarchar(250)
   SET @TQty=(SELECT sum(ISNULL(COLUMN07,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMN03=@Item and COLUMN27=@uom)
   SET @IFQty=(SELECT sum(ISNULL(COLUMN12,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMN03=@Item and COLUMN27=@uom)
   SET @IVQty=(SELECT sum(ISNULL(COLUMN13,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06) and COLUMN03=@Item and COLUMN27=@uom)
   --IF(@IFQty=(0.00))
   --begin
   --set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=24)
   --end
   --else IF(@IVQty=(0.00))
   --begin
   --if(@TQty=@IFQty)
   --begin
   --set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
   --end
   --else 
   --begin
   --set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
   --end
   --end
   --ELSE 
   --BEGIN
   --if(@TQty=@IVQty)
   --begin
   --set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
   --end
   --else 
   --begin
   --set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
   --end
   --end
if(@IFQty=0.00)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=27)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end
   UPDATE SATABLE005 SET COLUMN16=@Result WHERE COLUMN02=@COLUMN06 and columna03=@columna03
   --UPDATE SATABLE005 SET COLUMN16=21 WHERE COLUMN02=@COLUMN06 and columna03=@columna03
END
end try
begin catch

DECLARE @tempSTR NVARCHAR(MAX)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(250))))
    exec [CheckDirectory] @tempSTR,'Exception_JO_SATABLE007.txt',0

return 0
end catch

end






GO
