USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_SATABLE005]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAR_TP_SATABLE005]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,
	--EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,
		--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN43   nvarchar(250)=null,
	@COLUMN44   nvarchar(250)=null,  @COLUMN45   nvarchar(250)=null,  @COLUMN46   nvarchar(250)=null,
	--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
	@COLUMN47   nvarchar(250)=null,  @COLUMN48   nvarchar(250)=null,  @COLUMN49   nvarchar(250)=null, 
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	@COLUMN50   nvarchar(250)=null,  @COLUMN51   nvarchar(250)=null, 
	@COLUMN59   nvarchar(250)=null,  @COLUMN60   nvarchar(250)=null,  @COLUMN61   nvarchar(250)=null,
	@COLUMN62   nvarchar(250)=null,  @COLUMN63   nvarchar(250)=null,  @COLUMN64   nvarchar(250)=null,
	@COLUMN67   nvarchar(250)=null,  @COLUMN68   nvarchar(250)=null,  @COLUMN69   nvarchar(250)=null,
	@COLUMN70   nvarchar(250)=null,  @COLUMN71   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@TabelName  nvarchar(250)=null,  @ReturnValue int=null OUTPUT
)
AS

BEGIN
begin try
set @COLUMN51 = ISNULL(@COLUMN51,'22335')
IF @Direction = 'Insert'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN29=(1003)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE005_SequenceNo
insert into SATABLE005 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,  
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   COLUMN35,   COLUMN36,  COLUMN37,  COLUMN38,  COLUMN39,  COLUMN40,  COLUMN41, COLUMN42,  COLUMN43,  COLUMN44,  COLUMN45,
    --EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
    --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN46,  COLUMN47,  COLUMN48,  COLUMN49,  COLUMN50,  COLUMN51,  COLUMN59,  COLUMN60,  COLUMN67,  COLUMN68,  COLUMN69,  
   COLUMN70,  COLUMN71,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   --EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN47,   @COLUMN48, @COLUMN49,  @COLUMN50,  @COLUMN51,
   @COLUMN59,  @COLUMN60,  @COLUMN67,  @COLUMN68,  @COLUMN69,  @COLUMN70,   @COLUMN71,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
declare @newID int,@Totamt decimal(18,2),@Creditamt decimal(18,2),@ReverseCharged bit,@TOTALAMT DECIMAL(18,2),@Intrnal nvarchar(250),@amnt DECIMAL(18,2)
set @Intrnal=(select max(COLUMN01) from SATABLE005 where COLUMN02=@COLUMN02)
set @ReverseCharged = (select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@Intrnal)
if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN15,0)as decimal(18,2))-cast(ISNULL(@COLUMN32,0)as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN15,0))
end
if(@COLUMN51=22305)
begin
						if not exists(select COLUMN09  from PUTABLE016 where  COLUMN09=@COLUMN04 and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
						    BEGIN 
							exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

							set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1 
							insert into PUTABLE016	                      (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN18,COLUMN09,COLUMN10,COLUMN12,COLUMN14,COLUMNA02,COLUMNA03)values(@newID,2000,@COLUMN06,@Intrnal,'Credit Memo',@COLUMN05,@COLUMN09,@COLUMN04,'Open',@TOTALAMT,@TOTALAMT,@COLUMN24,@COLUMNA03)
							END
END
else
begin
						if not exists(select COLUMN05  from PUTABLE018 where  COLUMN05=@COLUMN04 and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
						    BEGIN 
							exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

							set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE018) as int)+1 
							insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02,COLUMN14,COLUMN15,COLUMNA03)
							values(@newID,3000,@COLUMN06,@COLUMN04,'Credit Memo',@COLUMN05,@COLUMN09,-cast(@TOTALAMT as decimal(18,2)),@COLUMN06,@TOTALAMT,'Open', @column24,@TOTALAMT,@COLUMN04,@COLUMNA03)
							END
						else
							BEGIN
								set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@COLUMN04     and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
								set @Creditamt=(select COLUMN14 from PUTABLE018  where COLUMN05=@COLUMN04  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
								--EMPHCS1041 by gnaneshwar 25/8/2015 During sales return the amount is wrong udpating in Accounts receivable-CR00001-Amount charged
								update PUTABLE018 set COLUMN04=@COLUMN06,
								COLUMN09=(cast(@Totamt as DECIMAL(18,2))-cast(@TOTALAMT as DECIMAL(18,2))),
								COLUMN12=(-(cast(@Totamt as DECIMAL(18,2)))+cast(@TOTALAMT as DECIMAL(18,2))),
								COLUMN14=(cast(@Creditamt as DECIMAL(18,2))+cast(@TOTALAMT as DECIMAL(18,2))) 
								where COLUMN05=@COLUMN04  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
							END
END
--EMPHCS1570	comission amount worng in invoice and not considerting in Credit memo BY RAJ.Jr
declare @SalesRep int,@Type int,@Commission decimal(18,2),@number nvarchar(250)
set @number=(select max(COLUMN01) from SATABLE005 where COLUMN02=@COLUMN02)
set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)
if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22303)
	BEGIN
		declare @PNewID int
		set @amnt = ((cast(@COLUMN12 as DECIMAL(18,2))*@Commission/100))
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = '22492',  @COLUMN09 = @SalesRep,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @amnt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
		insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02,COLUMNA03)
		values(@PNewID,@COLUMN06,'Credit Memo',@number,@COLUMN05,@COLUMN09,@COLUMN04,(cast(@COLUMN12 as DECIMAL(18,2))),
		((cast(@COLUMN12 as DECIMAL(18,2))*@Commission/100)),0,(cast(@COLUMN12 as DECIMAL(18,2))),@SalesRep,@Commission,'OPEN',@COLUMNA02,@COLUMNA03) 				
	END
declare @AID1 nvarchar(250)
--set @COLUMN60=(iif(cast(@COLUMN60 as nvarchar(250))='',0,isnull(@COLUMN60,0)))
IF(cast(@COLUMN60 as decimal(18,2))>0.00)
BEGIN

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @COLUMN60,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID1=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
values(@AID1,@COLUMN06,'Credit Memo',@COLUMN04,@COLUMN05,@number,1049,0,@COLUMN60,@COLUMNA02 , @COLUMNA03,0,@COLUMN35) 
END
IF(CAST(ISNULL(@COLUMN71,0)AS DECIMAL(18,2))!=0)
BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22407', @COLUMN11 = '1145',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN71,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @AID1=((SELECT MAX(ISNULL(COLUMN02,1000)) from FITABLE035)+1)
insert into FITABLE035(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,COLUMNA02, COLUMNA03 )
values(@AID1,  'Credit Memo',  @COLUMN06,  @number,  @COLUMN05,0,ISNULL(@COLUMN71,0),@COLUMN04,1145,@COLUMN35,@COLUMNA02, @COLUMNA03)
END

	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(@number)
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from SATABLE005
END 
 
ELSE IF @Direction = 'Update'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN29=(1003)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @PrevCDID NVARCHAR(250), @Location NVARCHAR(250), @Project NVARCHAR(250), @linelocation NVARCHAR(250)
set @PrevCDID=(select COLUMN04 from SATABLE005 where COLUMN02=@COLUMN02)
set @Location=(select COLUMN48 from SATABLE005 where COLUMN02=@COLUMN02)
set @Intrnal=(select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN02)
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
set @Project=(select column35 from SATABLE005 where COLUMN02=@COLUMN02)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
UPDATE SATABLE005 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,       
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,  
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,
   COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,
   COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,  
   --EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
   COLUMN36=@COLUMN36,    COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,    COLUMN39=@COLUMN39,    COLUMN40=@COLUMN40, 
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   COLUMN41=@COLUMN41,    COLUMN42=@COLUMN42,    COLUMN43=@COLUMN43,   COLUMN44=@COLUMN44,    COLUMN45=@COLUMN45,
   --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN46=@COLUMN46,    COLUMN47=@COLUMN47,    COLUMN48=@COLUMN48,    COLUMN49=@COLUMN49,    COLUMN50=@COLUMN50, 
   COLUMN51=@COLUMN51,    COLUMN59=@COLUMN59,    COLUMN60=@COLUMN60,    COLUMN67=@COLUMN67,    COLUMN68=@COLUMN68,  
   COLUMN69=@COLUMN69,    COLUMN70=@COLUMN70,    COLUMN71=@COLUMN71,    COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,                         COLUMND06=@COLUMND06,  
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02   
--EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
--UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN19 in( select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02 )
--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	delete from PUTABLE018  where COLUMN05=@PrevCDID  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	delete from FITABLE064  where COLUMN05=@Intrnal  and COLUMN03 in('Credit Memo','Markdown Register','Sales Claim Register') and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	if(@COLUMN51=22305)
	begin
	delete from PUTABLE016  where COLUMN09=@PrevCDID  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	end
	--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	declare @Num nvarchar(250), @RefLineId nvarchar(250)
	set @Num=(select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN02);
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	delete from FITABLE026 where column05=@Num and column09=@PrevCDID and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	delete from	FITABLE034 where COLUMN03='Sales Claim Register' and 	COLUMN09=@PrevCDID and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	delete from	FITABLE034 where COLUMN03='Credit Memo' and 	COLUMN09=@PrevCDID and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	delete from	FITABLE035 where COLUMN03='Credit Memo' and COLUMN05=@Num and	COLUMN09=@PrevCDID and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	delete from FITABLE036 where column05=@Num and column09=@PrevCDID and column03='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	--EMPHCS1570	comission amount worng in invoice and not considerting in Credit memo BY RAJ.Jr
	delete from FITABLE027 where column05=@Num and column09=@PrevCDID and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	--set @COLUMN60=(iif(cast(@COLUMN60 as nvarchar(250))='',0,isnull(@COLUMN60,0)))
	IF(cast(@COLUMN60 as decimal(18,2))>0.00)
	BEGIN
	
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22344', @COLUMN11 = '1049',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @COLUMN60,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID1=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
	insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
	values(@AID1,@COLUMN06,'Credit Memo',@COLUMN04,@COLUMN05,@Num,1049,0,@COLUMN60,@COLUMNA02 , @COLUMNA03,0,@COLUMN35) 
	END
    IF(CAST(ISNULL(@COLUMN71,0)AS DECIMAL(18,2))!=0)
    BEGIN
    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22407', @COLUMN11 = '1145',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN71,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @AID1=((SELECT MAX(ISNULL(COLUMN02,1000)) from FITABLE035)+1)
    insert into FITABLE035(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,   COLUMN10,COLUMN11,COLUMNA02, COLUMNA03 )
    values(@AID1,  'Credit Memo',  @COLUMN06,  @Num,  @COLUMN05,0,ISNULL(@COLUMN71,0),@COLUMN04,1145,@COLUMN35,@COLUMNA02, @COLUMNA03)
    END
	set @ReverseCharged = (select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@Num)
	if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
	begin
	set @TOTALAMT=(cast(ISNULL(@COLUMN15,0)as decimal(18,2))-cast(ISNULL(@COLUMN32,0)as decimal(18,2)))
	end
	else
	begin
	set @TOTALAMT=(ISNULL(@COLUMN15,0))
	end
	if(@COLUMN51=22305)
	begin
if not exists(select COLUMN09  from PUTABLE016 where  COLUMN09=@COLUMN04 and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
						    BEGIN 
							exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1 
							insert into PUTABLE016	                      (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN18,COLUMN09,COLUMN10,COLUMN12,COLUMN14,COLUMNA02,COLUMNA03)values(@newID,2000,@COLUMN06,@Num,'Credit Memo',@COLUMN05,@COLUMN09,@COLUMN04,'Open',@TOTALAMT,@TOTALAMT,@COLUMN24,@COLUMNA03)
							END
END
else
begin
if not exists(select COLUMN05  from PUTABLE018 where  COLUMN05=@PrevCDID and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
	    BEGIN 
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN51,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE018) as int)+1 
		insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMNA02,COLUMN14,COLUMN15,COLUMNA03)
		values(@newID,3000,@COLUMN06,@COLUMN04,'Credit Memo',@COLUMN05,@COLUMN09,-cast(@TOTALAMT as decimal(18,2)),@COLUMN06,@TOTALAMT,'Open', @column24,@TOTALAMT,@COLUMN04,@COLUMNA03)
		END
	else
		BEGIN
		--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
			set @Totamt=(select COLUMN09 from PUTABLE018  where COLUMN05=@PrevCDID     and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			set @Creditamt=(select COLUMN14 from PUTABLE018  where COLUMN05=@PrevCDID  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			--EMPHCS1041 by gnaneshwar 25/8/2015 During sales return the amount is wrong udpating in Accounts receivable-CR00001-Amount charged
			update PUTABLE018 set COLUMN04=@COLUMN06,
			COLUMN09=(cast(@Totamt as DECIMAL(18,2))-cast(@TOTALAMT as DECIMAL(18,2))),
			COLUMN12=(-(cast(@Totamt as DECIMAL(18,2)))+cast(@TOTALAMT as DECIMAL(18,2))),
			COLUMN14=(cast(@Creditamt as DECIMAL(18,2))+cast(@TOTALAMT as DECIMAL(18,2))) 
			where COLUMN05=@PrevCDID  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
		END
end
--EMPHCS1570	comission amount worng in invoice and not considerting in Credit memo BY RAJ.Jr
		set @number=(select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN02)
set @SalesRep = (select isnull(COLUMN22,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Type = (select isnull(COLUMN24,0)  from SATABLE002 where COLUMN02=@COLUMN05)
set @Commission = (select isnull(COLUMN23,0)  from SATABLE002 where COLUMN02=@COLUMN05 and COLUMN22=@SalesRep)
if(@SalesRep!=0 and cast(@Commission as int)>0 and @Type!=0 and @Type=22303)
	BEGIN
	set @amnt = ((cast(@COLUMN12 as DECIMAL(18,2))*@Commission/100))
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Intrnal,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = '22492',  @COLUMN09 = @SalesRep,    @COLUMN10 = NULL, @COLUMN11 = NULL,
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN35,	@COLUMN14 = @amnt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

		set @PNewID=cast((select ISNULL(MAX(COLUMN02),10000) from FITABLE027) as int)+1
		insert into FITABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMNA02,COLUMNA03)
		values(@PNewID,@COLUMN06,'Credit Memo',@number,@COLUMN05,@COLUMN09,@COLUMN04,(cast(@COLUMN12 as DECIMAL(18,2))),
		((cast(@COLUMN12 as DECIMAL(18,2))*@Commission/100)),0,(cast(@COLUMN12 as DECIMAL(18,2))),@SalesRep,@Commission,'OPEN',@COLUMNA02,@COLUMNA03) 				
	END
 declare @SOID nvarchar(250),@Qty nvarchar(250),@Item nvarchar(250),@ItemQty nvarchar(250),@id nvarchar(250),@uom nvarchar(250)
 DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
declare @RefundQty DECIMAL(18,2), @RefID int, @Refreceipt int, @TQty DECIMAL(18,2), @RRQty DECIMAL(18,2)
declare @PID int, @Result nvarchar(250), @date nvarchar(250), @chk bit, @AID int, @SBAL DECIMAL(18,2), @CBAL DECIMAL(18,2)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @number1 int, @DT date, @PRICE DECIMAL(18,2), @lineiid nvarchar(250)=null, @CUST int,@lot nvarchar(250)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
Declare @CustID int, @memo nvarchar(250), @dueDT date,  @tmpnewID int, @AvgPrice DECIMAL(18,2), @PRATE DECIMAL(18,2)
             set @date=(select COLUMN06 from SATABLE005 where COLUMN01=@SOID);
             set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@SOID);
             set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
             set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
      set @SOID=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
      --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE006 where COLUMN19=@SOID  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
	  --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE006 where COLUMN19=@SOID   and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
			 set @uom=(select COLUMN27 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			 set @RefLineId=(select COLUMN36 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
             set @lot=(select isnull(COLUMN17,0) from SATABLE006 where COLUMN02=@id)
             set @lot=iif(@lot='',0,isnull(@lot,0))
	     set @ItemQty=(select isnull(COLUMN07,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
	     --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
             set @Price=(select isnull(COLUMN09,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
			 --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
			 if(@COLUMN33!='' and isnull(@COLUMN33,0)!=0 and isnull(@COLUMN33,0)!='0')
			 begin	
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module		 
			 if(isnull(@RefLineId,0)>0)
			 begin
			 set @RefundQty=(select isnull(COLUMN13,0) from PUTABLE002 where COLUMN02= (@RefLineId) and isnull(COLUMNA13,0)=0 )
             UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN02= (@RefLineId) and isnull(COLUMNA13,0)=0
             UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where  COLUMN36= (@RefLineId) and isnull(COLUMNA13,0)=0
             end
			 else
			 begin
			 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
             set @RefundQty=isnull((select COLUMN13 from PUTABLE002 where COLUMN19 in (@PID) and COLUMN03=@Item and isnull(COLUMN26,0)=ISNULL(@uom,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lot,0)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 ),0);
             UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN03=@Item and isnull(COLUMN26,0)=ISNULL(@uom,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lot,0) and COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN19=@SOID and COLUMN03=@Item and isnull(COLUMN27,0)=ISNULL(@uom,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lot,0)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             end
			 set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
             SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMN19 in (@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 )
             if(@TQty=@RRQty)
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
             UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=62) where COLUMN01=(@PID)
             end
             else 
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
	     --EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
             UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@PID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             end
			 end
			 else			 
			 begin
			 
			 declare @Qty_On_Hand	decimal(18,2), @lotno nvarchar(250),@InvoiceID nvarchar(250),@upcno nvarchar(250) = null
			 set @chk=(select column48 from matable007 where column02=@Item)
			 set @lotno=(select COLUMN17 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 set @upcno=(select COLUMN04 from SATABLE006 where COLUMN02=@id)
			 set @Location=(@Location)
			 set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 set @linelocation=(select cast(COLUMN38 as nvarchar(250)) from SATABLE006 where COLUMN02=@id)
			 set @Location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
		     set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
			 if(@chk=1)
			 begin
			 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			 set @PRATE=(isnull(@AvgPrice,0))
			 --IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
			 set @Qty=cast(@ItemQty as decimal(18,2));
			 set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
			 UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))-(cast(@ItemQty as decimal(18,2))*cast(@PRATE as decimal(18,2))))	 
			 WHERE COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where   COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
			 if(cast(@Qty_On_Hand as decimal(18,2))=0)
			 begin
			 UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 else
			 begin
			 UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project AND COLUMN24 = @upcno
			 end
			 set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@SOID)
			 --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06='Credit Memo' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			 end
			 set @InvoiceID=(SELECT COLUMN01 FROM SATABLE009 WHERE COLUMN02 in(@COLUMN49) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			 set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			 set @TQty=(SELECT sum(COLUMN10) FROM SATABLE010 WHERE COLUMN15 in(@InvoiceID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			 set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
			 if(@TQty=@RRQty)
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
			 end

			 end

             UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@SOID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
              
			 if exists(select column01 from SATABLE006 where column19=@SOID  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
					begin 
			set @COLUMN02=(select COLUMN01 from SATABLE005 where COLUMN01=@SOID);
			set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@SOID);
			set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@SOID);
			set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@SOID)
			set @memo = (select COLUMN09 from SATABLE005 where COLUMN01=@SOID)
			set @Num = (select COLUMN04 from SATABLE005 where COLUMN01=@SOID)
			set @dueDT = (select COLUMN08  from SATABLE005 where COLUMN01=@SOID)
					
set @lineiid=(select max(column01) from SATABLE006 where column02=@id)
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
set @PRICE=(select column17 from matable007 where column02=@Item)
set @CUST=(select column05 from SATABLE005 where column01=@SOID)
set @chk=(select column48 from matable007 where column02=@Item)
delete from	FITABLE025 where COLUMN04='Markdown Register' and 	COLUMN05=@lineiid and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03 and COLUMN06 = @CUST
    

if(@chk=0)
	begin	
	delete from FITABLE025 WHERE COLUMN08=1053  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	end
else
	begin	
	delete from FITABLE025 WHERE COLUMN08=1052  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
    delete from FITABLE025 WHERE COLUMN08=1056  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	end        
	end
		     --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
			 UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN02= @id  
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
		END
  CLOSE cur1 
			 delete from FITABLE036 where COLUMN05 in(SELECT COLUMN01 from SATABLE005 where COLUMN02 = @COLUMN02 ) and COLUMN03='Credit Memo' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

set @COLUMNA02=(select COLUMNA02 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMNA03=(select COLUMNA03 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMN24=(select COLUMN24 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMN04=(select COLUMN04 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
set @COLUMN33=(select COLUMN33 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMN31=(select COLUMN31 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMN49=(select COLUMN49 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @Num=(select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN02);
set @Project=(select column35 from SATABLE005 where column02=@COLUMN02)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
set @COLUMNA08=(select COLUMNA08 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMNA07=(select GETDATE())
set @COLUMNB01=(select COLUMN04 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @COLUMNB02=(select COLUMN03 from SATABLE005 WHERE COLUMN02 = @COLUMN02)

	delete from FITABLE064  where COLUMN05=@Num  and COLUMN03 in('Credit Memo','Markdown Register','Sales Claim Register') and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	delete from FITABLE026 where column05=@Num and column09=@COLUMN04 and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	delete from FITABLE036 where column05=@Num and column09=@COLUMN04 and column03='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	--EMPHCS1570	comission amount worng in invoice and not considerting in Credit memo BY RAJ.Jr
	delete from FITABLE027 where column05=@Num and column09=@COLUMN04 and column04='Credit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	delete from	FITABLE034 where COLUMN03='Sales Claim Register' and 	COLUMN09=@COLUMN04 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
    delete from	FITABLE034 where COLUMN03='Credit Memo' and 	COLUMN09=@COLUMN04 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	delete from	FITABLE035 where COLUMN03='Credit Memo' and COLUMN05=@Num and	COLUMN09=@COLUMN04 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03
	  set @Initialrow =1
      set @SOID=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
	   set @date=(select COLUMN06 from SATABLE005 where COLUMN01=@SOID);
             set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@SOID);
             set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
             set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
      --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE006 where COLUMN19 in(@SOID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
	  --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE006 where COLUMN19 in(@SOID)   and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
			 set @uom=(select COLUMN27 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			 set @RefLineId=(select COLUMN36 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
             set @lot=(select isnull(COLUMN17,0) from SATABLE006 where COLUMN02=@id)
             set @lot=iif(@lot='',0,isnull(@lot,0))
	     set @ItemQty=(select isnull(COLUMN07,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
	     --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 set @Price=(select isnull(COLUMN09,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
			 --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
			 if(@COLUMN33!='' and isnull(@COLUMN33,0)!=0 and isnull(@COLUMN33,0)!='0')
			 begin		
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module	 
			 if(isnull(@RefLineId,0)>0)
			 begin
			 set @RefundQty=(select isnull(COLUMN13,0) from PUTABLE002 where COLUMN02= (@RefLineId) and isnull(COLUMNA13,0)=0 )
             UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN02= (@RefLineId) and isnull(COLUMNA13,0)=0
             UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where  COLUMN36= (@RefLineId) and isnull(COLUMNA13,0)=0
             end
			 else
			 begin
			 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
             set @RefundQty=isnull((select COLUMN13 from PUTABLE002 where COLUMN19 in (@PID) and COLUMN03=@Item  and isnull(COLUMN26,0)=ISNULL(@uom,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lot,0) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 ),0);
             UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN03=@Item and isnull(COLUMN26,0)=ISNULL(@uom,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lot,0) and COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))- CAST(@ItemQty AS DECIMAL(18,2))) where COLUMN02=@id and COLUMN03=@Item  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
             end
             set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
             SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMN19 in (@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 )
             if(@TQty=@RRQty)
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
             UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=62) where COLUMN01=(@PID)
             end
             else 
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
             UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@PID)
             end
			 end
			  else			 
			 begin
			 
			 set @chk=(select column48 from matable007 where column02=@Item)
			 set @lotno=(select COLUMN17 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 set @upcno=(select COLUMN04 from SATABLE006 where COLUMN02=@id)
			 set @Location=(@COLUMN48)
			 set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 set @linelocation=(select cast(COLUMN38 as nvarchar(250)) from SATABLE006 where COLUMN02=@id)
     set @Location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
     set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
     
			 if(@chk=1)
			 begin
			 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 set @Qty=cast(@ItemQty as decimal(18,2));
			 set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
			 --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			 set @PRATE=(isnull(@AvgPrice,0))
			 --IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
			 UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))-(cast(@ItemQty as decimal(18,2))*cast(@PRATE as decimal(18,2))))
			 ,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE'	 
			 WHERE COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project AND COLUMN24 =@upcno
			 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where   COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
			 if(cast(@Qty_On_Hand as decimal(18,2))=0)
			 begin
			 UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 else
			 begin
			 UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project AND COLUMN24 = @upcno )as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='DELETE' WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@SOID)
			 --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06='Credit Memo' and COLUMN07=@CustID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			 end
			 set @InvoiceID=(SELECT COLUMN01 FROM SATABLE009 WHERE COLUMN02 in(@COLUMN49) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			 set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			 set @TQty=(SELECT sum(COLUMN10) FROM SATABLE010 WHERE COLUMN15 in(@InvoiceID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
			 set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
			 if(@TQty=@RRQty)
             begin
             set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
			 end
			 end
             UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@SOID)
              
			 if exists(select column01 from SATABLE006 where column19 in(@SOID)   and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
					begin 
			set @lineiid=(select COLUMN01 from SATABLE005 where COLUMN01=@SOID);
			set @lineiid=(select max(column01) from SATABLE006 where column02=@id)
			set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@SOID);
			set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@SOID);
			set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@SOID)
			set @memo = (select COLUMN09 from SATABLE005 where COLUMN01=@SOID)
			set @Num = (select COLUMN04 from SATABLE005 where COLUMN01=@SOID)
			set @dueDT = (select COLUMN08  from SATABLE005 where COLUMN01=@SOID)
			declare @payeeid nvarchar(250)
			set @payeeid = (select COLUMN51  from SATABLE005 where COLUMN01=@SOID)
	if(@payeeid=22305)
	begin
	delete from PUTABLE016  where COLUMN09=@Num  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	end
 delete from PUTABLE018  where COLUMN05=@Num  and COLUMN06='Credit Memo' and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
delete from	FITABLE025 where COLUMN04='Markdown Register' and 	COLUMN05=@lineiid and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null) and COLUMNA03=@COLUMNA03 and COLUMN06 = @CustID
    					
--set @lineiid=(select max(column01) from SATABLE006 where column02=@id)
set @CUST=(select column05 from SATABLE005 where column01=@SOID)
set @chk=(select column48 from matable007 where column02=@Item)

if(@chk=0)
	begin	
	delete from FITABLE025 WHERE COLUMN08=1053  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	end
else
	begin	
	delete from FITABLE025 WHERE COLUMN08=1052  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
    delete from FITABLE025 WHERE COLUMN08=1056  and COLUMN04='Credit Memo' and COLUMN05=@lineiid AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 ;
	end        
	end
		     --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
			 UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN02= @id  
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
		END
	declare @RIQty decimal(18,2),@RIResult nvarchar(250)
   set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@SOID);
   set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
   set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
   SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
   set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM SATABLE006 WHERE COLUMN19 in(@SOID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
   set @RIQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE COLUMN19 in(@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
   if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
   begin
   if(@TQty=@RRQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=109)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=108)
   end
   UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@SOID)
   IF(@RIQty=(0.00))
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=95)
   set @RIResult=(select COLUMN04 from CONTABLE025 where COLUMN02=110)
   end
   else IF(@RRQty=(0.00))
   begin
   if(@TQty=@RIQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=104)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=102)
   end
   end
   ELSE 
   BEGIN
   if(@TQty=@RRQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
   set @RIResult=(select COLUMN04 from CONTABLE025 where COLUMN02=109)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=103)
   set @RIResult=(select COLUMN04 from CONTABLE025 where COLUMN02=108)
   end
   end
   UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=108) WHERE COLUMN02=@Refreceipt and columna03=@columna03
   UPDATE PUTABLE001 SET COLUMN16=@Result WHERE COLUMN02=@RefID and columna03=@columna03
   END
UPDATE SATABLE005 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02
			 delete from FITABLE036 where COLUMN05 in(SELECT COLUMN01 from SATABLE005 where COLUMN02 = @COLUMN02 ) and COLUMN03='Credit Memo' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03

END

end try
begin catch
declare @tempSTR nvarchar(max)=''
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_EXCEPTION_SATABLE005.txt',0

return 0
end catch
end





























GO
