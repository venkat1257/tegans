USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE020]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE020]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT
)
AS
BEGIN
 begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE020_SequenceNo
if(@COLUMN18='False'or @COLUMN18='0')
begin
set @COLUMN19=(null)
end
if(@COLUMN11='22492')
begin
set @COLUMN21=(select COLUMN06 FROM MATABLE010 where COLUMN02=@COLUMN17)
end
else if(@COLUMN11='22335')
begin
set @COLUMN21=(select COLUMN05 FROM SATABLE002 where COLUMN02=@COLUMN17)
end
else 
begin
set @COLUMN21=(select COLUMN05 FROM SATABLE001 where COLUMN02=@COLUMN17)
end
set @COLUMN21=((@COLUMN21)+' - '+(@COLUMN09))

declare @tempSTR nvarchar(max)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment Header Values are Intiated for FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
	IF(@COLUMN28 = 22713)
	SET @COLUMN18= (cast(cast(ISNULL(@COLUMN10,0)as decimal(18,2))-cast(ISNULL(@COLUMN26,0)as decimal(18,2))as nvarchar(250)))
	ELSE
	SET @COLUMN18= (cast((ISNULL(@COLUMN10,0))as nvarchar(250)))
insert into FITABLE020
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN25,  COLUMN26, 
   COLUMN28,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN25,  @COLUMN26,  @COLUMN28,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment Header Intiated Values are Created in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
   --------
   declare @status nvarchar(250)
   set @status=(select column04 from contable025 where column02=64)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment Status Updation for FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Status'+
   isnull(cast(@status as nvarchar(250)),'') +','+  '2.Advance Payment No'+','+  isnull(cast(@COLUMN02  as nvarchar(250)),'')+','+ '4.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
 update FITABLE020 set  COLUMN16=@status where column02=@COLUMN02 and COLUMNA03=@COLUMNA03
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Payment Updated in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''))
 declare @balance decimal(18,2) 
 declare @pid nvarchar(250)=null
 declare @internalid nvarchar(250)=null
set @internalid= (select column01 from FITABLE020 where COLUMN02=@COLUMN02)
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN06 and COLUMNA03=@COLUMNA03)
set @pid=((select max(column02) from PUTABLE019)+1)
--if(@COLUMN20!=22627)
--begin
--set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
--   'The Values are '+
--   cast(isnull(@pid,999)as nvarchar(250)) +','+ isnull(cast(@COLUMN06 as nvarchar(250)),'')+','+ isnull( cast(@COLUMN05 as nvarchar(250)),'')+','+ isnull(cast(@COLUMN04 as nvarchar(250)),'')+','+  'ADVANCE PAYMENT'+','+ isnull( cast(@COLUMN17 as nvarchar(250)),'')+','+ isnull( cast(@internalid as nvarchar(250)),'')+','+ isnull( cast(@COLUMN10 as nvarchar(250)),'')+','+ isnull( cast((@balance -@COLUMN10) as nvarchar(250)),'')+','+  isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
--Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMNA02, COLUMNA03)
-- values(@pid,@COLUMN06,@COLUMN05,@COLUMN04,'ADVANCE PAYMENT',@COLUMN17,@internalid,@COLUMN10,(cast(@balance as decimal(18,2))-cast(@COLUMN10 as decimal(18,2))),@COLUMN09,@COLUMNA02, @COLUMNA03)
-- set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--					'Bank Register Intiated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
--set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
--   'The Values are 1.Balance Amount'+ isnull(cast(@balance as nvarchar(250)),'') +','+  '2.Payment Amount'+','+  isnull(cast(@COLUMN10  as nvarchar(250)),'')+','+ '4.Account'+','+ isnull(cast( @COLUMN06  as nvarchar(250)),'')+','+'4.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
-- update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))-cast(@COLUMN10 as decimal(18,2))) where column02=@COLUMN06 and COLUMNA03=@COLUMNA03
-- set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--					'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
-- end
  
declare @IDAP nvarchar(250)
set @IDAP=((select max(column02) from FITABLE033)+1)
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue =(select COLUMN01 from FITABLE020 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE020
END  

ELSE IF @Direction = 'Update'

BEGIN
declare @amt decimal(18,2),@bank nvarchar(250),@Trans# nvarchar(250)
set @Trans#=(select column04 from FITABLE020 where COLUMN02=@COLUMN02 and COLUMNA03=@COLUMNA03)
set @bank=(select column06 from FITABLE020 where COLUMN02=@COLUMN02 and COLUMNA03=@COLUMNA03)
set @amt=(select column10 from FITABLE020 where COLUMN02=@COLUMN02 and COLUMNA03=@COLUMNA03)
set @internalid= (select column01 from FITABLE020 where column02=@COLUMN02 and COLUMNA03=@COLUMNA03)
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN06 and COLUMNA03=@COLUMNA03)
set @pid=((select max(column02) from PUTABLE019)+1)
if(@COLUMN20!=22627)
begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are deleteing for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(cast(@bank as nvarchar(250)),'') +','+ isnull(cast(@internalid as nvarchar(250)),'')+','+ isnull( cast(@Trans# as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
   --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
delete from fitable064 where column03 in ('ADVANCE PAYMENT','PDC Issued') and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
delete from PUTABLE019 where COLUMN03=@bank and COLUMN06='ADVANCE PAYMENT' and COLUMN08=@internalid and COLUMNA03=@COLUMNA03 and COLUMN05=@Trans#
delete from FITABLE052 WHERE COLUMN03=@bank and COLUMN06='ADVANCE PAYMENT' and COLUMN09=@internalid AND COLUMN05=@Trans# and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register deleteing Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are 1.Balance Amount'+ isnull(cast(@balance as nvarchar(250)),'') +','+  '2.Account'+','+ isnull(cast( @COLUMN06  as nvarchar(250)),'')+','+'3.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
   --EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
update FITABLE001 set  COLUMN10=(cast(@balance as decimal(18,2))+cast(@amt as decimal(18,2))) where column02=@COLUMN06 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
end
    set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Advance Payment Header Values are Intiated for FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE FITABLE020 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    --COLUMN16=@COLUMN16,    
   COLUMN17=@COLUMN17,	 COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    --COLUMN21=@COLUMN21,
   COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,    COLUMN28=@COLUMN28,    COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Advance Payment Header Intiated Values are Updated in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )  
set @balance=cast(((select column10 from FITABLE001 where COLUMN02=@COLUMN06 and COLUMNA03=@COLUMNA03))  as decimal(18,2))
set @internalid= (select column01 from FITABLE020 where column02=@COLUMN02 and COLUMNA03=@COLUMNA03)
--if(@COLUMN20!=22627)
--begin
--set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
--   'The Values are '+
--   cast(isnull(@pid,999)as nvarchar(250)) +','+ isnull(cast(@COLUMN06 as nvarchar(250)),'')+','+ isnull( cast(@COLUMN05 as nvarchar(250)),'')+','+ isnull(cast(@COLUMN04 as nvarchar(250)),'')+','+  'ADVANCE PAYMENT'+','+ isnull( cast(@COLUMN17 as nvarchar(250)),'')+','+ isnull( cast(@internalid as nvarchar(250)),'')+','+ isnull( cast(@COLUMN10 as nvarchar(250)),'')+','+ isnull( cast((@balance -@COLUMN10) as nvarchar(250)),'')+','+  isnull(cast(@COLUMN09 as nvarchar(250)),'')+','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
--Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN14,COLUMNA02, COLUMNA03)
-- values(@pid,@COLUMN06,@COLUMN05,@COLUMN04,'ADVANCE PAYMENT',@COLUMN17,@internalid,@COLUMN10,(cast(@balance as decimal(18,2))-cast(@COLUMN10 as decimal(18,2))),@COLUMN09,@COLUMNA02, @COLUMNA03)
-- set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--					'Bank Register Intiated Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
--set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Chart Of Accounts Balance Updation for FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
--   'The Values are 1.Balance Amount'+ (isnull(cast(@balance -@COLUMN10 as nvarchar(250)),'')) +','+  '2.Account'+','+ isnull(cast( @COLUMN06  as nvarchar(250)),'')+','+'3.Account Owner'+','+ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
--update FITABLE001 set  COLUMN10=(cast(@balance -@COLUMN10 as decimal(18,2))) where column02=@COLUMN06 and COLUMNA03=@COLUMNA03
--set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--					'Chart Of Accounts Balance Updated in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
--end
delete from FITABLE026  where COLUMN04='ADVANCE PAYMENT'and  COLUMN05 =@internalid and  COLUMN09 =@Trans# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'
BEGIN
-- //EMPHCS1412	Issue Cheque and Receive payment edit,delete checking By Raj.Jr 08/12/2015
declare @Cheque# nvarchar(250),@Linternalid nvarchar(250),@Initialrow nvarchar(250),@id nvarchar(250),@MaxRownum nvarchar(250),@Acc nvarchar(250),@Project nvarchar(250),@ACCNAME nvarchar(250),@Bankbal decimal(18,2),@taxamt decimal(18,2),@ICID nvarchar(250),@PDC nvarchar(250),@taxtype nvarchar(250)
set @Cheque#=(select column04 from FITABLE020 where COLUMN02=@COLUMN02)
set @internalid= (select column01 from FITABLE020 where COLUMN02=@COLUMN02)
set @COLUMNA02=(select COLUMNA02 from FITABLE020 where COLUMN02=@COLUMN02)
set @COLUMNA03= (select COLUMNA03 from FITABLE020 where COLUMN02=@COLUMN02)
set @PDC= (select COLUMN20 from FITABLE020 where COLUMN02=@COLUMN02)
set @taxtype= (select COLUMN17 from FITABLE020 where COLUMN02=@COLUMN02)
delete from fitable064 where column03 in ('ADVANCE PAYMENT','PDC Issued') and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance Payment record Deletion  in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@COLUMNA13 as nvarchar(250)),'') +','+ ISNULL(cast(@COLUMN02 as nvarchar(250)),'') +  '  '))
    UPDATE FITABLE020 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Payment record Deletion  in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''))
--UPDATE FITABLE041 SET COLUMNA13=@COLUMNA13 WHERE COLUMN09=(select column01 from FITABLE020 where column02=@COLUMN02 and COLUMNA03=@COLUMNA03)
--delete from FITABLE041 where COLUMN09=@internalid and column02=@COLUMN02 and COLUMNA03=@COLUMNA03
		 set @Acc= (select column06 from FITABLE020 where COLUMN02=@COLUMN02)
		 

 --set @Initialrow =1
 --     set @ICID=(select COLUMN01 from FITABLE020 WHERE COLUMN02 = @COLUMN02)
	--  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE022 where COLUMN09=@ICID
 --     OPEN cur1
	--  FETCH NEXT FROM cur1 INTO @id
 --     SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE022 where COLUMN09=@ICID)
 --        WHILE @Initialrow <= @MaxRownum
 --        BEGIN 
		 set @Bankbal=(select column10 from FITABLE001 where COLUMN02=@Acc and COLUMNA03=@COLUMNA03)
		 --EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		 set @balance=(select isnull(column05,0) from FITABLE022 where COLUMN09=@internalid and COLUMNA03=@COLUMNA03)
		  set @taxamt=(select isnull(column06,0) from FITABLE022 where COLUMN09=@internalid and COLUMNA03=@COLUMNA03)
		 set @Project= (select isnull(column08,0) from FITABLE022 where COLUMN09=@internalid)
		 set @Linternalid= (select column01 from FITABLE022 where COLUMN09=@internalid)
		 if(@PDC=22627)
		 begin
		 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PDC Issued record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@Linternalid as nvarchar(250)),'') +','+isnull(cast(@Cheque# as nvarchar(250)),'') +','+'PDC Issued' +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+ ISNULL(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
		 delete from FITABLE026  where COLUMN05=@Linternalid AND COLUMN09=@Cheque# AND COLUMN17='PDC Issued' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'PDC Issued record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register record Deletion  in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@Acc as nvarchar(250)),'') +','+'ADVANCE PAYMENT' +','+isnull(cast(@Project as nvarchar(250)),'') +','+ ISNULL(cast(@internalid as nvarchar(250)),'') + isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
	delete from PUTABLE019  where COLUMN03=@Acc  and COLUMN06='ADVANCE PAYMENT'  and COLUMN08 =@Linternalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	delete from FITABLE052 WHERE COLUMN03=@Acc  and COLUMN06='ADVANCE PAYMENT' and COLUMN09=@Linternalid and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register record Deletion  in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
	end
	else 
	begin
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register record Deletion  in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@Acc as nvarchar(250)),'') +','+'ADVANCE PAYMENT' +','+isnull(cast(@Project as nvarchar(250)),'') +','+ ISNULL(cast(@internalid as nvarchar(250)),'') + isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
	delete from PUTABLE019  where COLUMN03=@Acc  and COLUMN06='ADVANCE PAYMENT'  and COLUMN08 =@internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	delete from FITABLE052 WHERE COLUMN03=@Acc  and COLUMN06='ADVANCE PAYMENT' and COLUMN09=@internalid and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	delete from FITABLE026  where COLUMN04='ADVANCE PAYMENT'and  COLUMN05 =@internalid and  COLUMN09 =@Cheque# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Bank Register record Deletion  in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''))
  end
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
  'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''+
 'The Values are '+ isnull(cast((@Bankbal+@balance+@taxamt) as nvarchar(250)),'') +','+isnull(cast(@Acc as nvarchar(250)),'') +','+ isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
	update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))+cast(@balance as decimal(18,2))+cast(@taxamt as decimal(18,2))) where column02=@Acc and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'chart of accounts record updation  in FITABLE001 Table '+'' + CHAR(13)+CHAR(10) + ''))
	
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advace payment record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@Linternalid as nvarchar(250)),'') +','+isnull(cast(@taxtype as nvarchar(250)),'') +','+isnull(cast(@Cheque# as nvarchar(250)),'') +','+'ADVANCE PAYMENT' +','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
	delete from FITABLE026  where COLUMN05=@Linternalid AND COLUMN08=@taxtype and @COLUMN09=@Cheque# AND COLUMN04='ADVANCE PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advace payment record Deletion  in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
   'Advace payment record Deletion  in FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@Linternalid as nvarchar(250)),'') +','+isnull(cast(@Cheque# as nvarchar(250)),'') +','+'Advances Paid' +','+ isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') + '  '))
	--delete from FITABLE026  where COLUMN05=@Linternalid AND COLUMN09=@Cheque# AND COLUMN17='Advances Paid' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
	delete from FITABLE034 where COLUMN09=@Cheque# and COLUMN03='ADVANCE PAYMENT' and COLUMN10=1120 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advace payment record Deletion  in FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''))

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advace payment - Line: record Deletion  in FITABLE022 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+ isnull(cast(@COLUMNA13 as nvarchar(250)),'') +','+isnull(cast(@internalid as nvarchar(250)),'') + '  '))
	update FITABLE022 set COLUMNA13=@COLUMNA13 where COLUMN09=@internalid
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advace payment - Line: record Deletion  in FITABLE022 Table '+'' + CHAR(13)+CHAR(10) + ''))
	 --FETCH NEXT FROM cur1 INTO @id
  --           SET @Initialrow = @Initialrow + 1 
		--END
  --CLOSE cur1 
END
 exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE020.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE020.txt',0
end catch
end








GO
