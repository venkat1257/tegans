USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[InvntItemQtyChk]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InvntItemQtyChk]
(
	@Item  nvarchar(250)= null,
	@OPUnit nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@UOM nvarchar(250)= null,
	@QTY nvarchar(250)= null,
	@Location nvarchar(250)= null,
	@Lot nvarchar(250)= null,
	@Project nvarchar(250)= null
)
as 
begin
	set @UOM=(select iif((@UOM is not null and @UOM>0),@UOM,(select max(column02) from fitable037 where column07=1 and column08=1)))
	--EMPHCS1763 rajasekhar reddy patakota 21/06/2016 Inventory Shrinkage Account setup in Inventory Adjustment
	select iif(m.COLUMN48=0,@QTY,f.column04) as qty,isnull(f.column17,0) Price from FITABLE010 f
	left join MATABLE007 m on m.COLUMN02=f.COLUMN03 and f.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and f.COLUMNA03=@AcOwner 
	 where f.COLUMN03=@Item and f.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and f.COLUMNA03=@AcOwner and isnull(f.COLUMN19,0)=isnull(@UOM,0)  AND isnull(f.COLUMN21,0)=isnull(@Location,0) AND isnull(f.COLUMN22,0)=isnull(@Lot,0)
	 AND isnull(f.COLUMN23,0)=isnull(@Project,0)
end



GO
