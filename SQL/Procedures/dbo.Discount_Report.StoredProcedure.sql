USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[Discount_Report]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Discount_Report](@FromDate nvarchar(250),@ToDate nvarchar(250),@OPUnit nvarchar(250),@AcOwner nvarchar(250))
as
begin
---EMPHCS907	 After deleting invoice - Taxes are not cleared  By Raj.Jr 11/8/2015
select FIT025.COLUMN03 as Date,FIT025.COLUMN04 as TransactionType,FIT025.COLUMN05 as Transactionid,
FIT025.COLUMN06 as Customerid,FIT025.COLUMN10 as Amount,FIT025.COLUMNA02 as operatingunitid,
SA002.COLUMN05 as Customername,SA009.COLUMN04 as TransactionNo,CON007.COLUMN03 as operatingunit,
SA009.COLUMN20 as TotalAmount from FITABLE025 FIT025 
inner join SATABLE002 SA002 on FIT025.COLUMN06=SA002.COLUMN02
 left outer join SATABLE009 SA009 on FIT025.COLUMN05=SA009.COLUMN01
  left outer join CONTABLE007 CON007 on FIT025.COLUMNA02=CON007.COLUMN02 
  where isnull((FIT025.COLUMNA13),0)=0 and FIT025.COLUMN08=1049 and FIT025.COLUMN03 between @FromDate and @ToDate and 
  FIT025.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and FIT025.COLUMNA03=@AcOwner
end



GO
