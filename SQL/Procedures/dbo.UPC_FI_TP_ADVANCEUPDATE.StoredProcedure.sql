USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[UPC_FI_TP_ADVANCEUPDATE]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UPC_FI_TP_ADVANCEUPDATE]
(
	@ID INT,
	@REF NVARCHAR(20),
	@REM decimal(18,2)
)
AS
	BEGIN
		DECLARE @COLUMN09 INT
		SET @COLUMN09=(SELECT COLUMN09 FROM FITABLE022 WHERE COLUMN02=@ID)
		IF(@REF='CLOSE')
			BEGIN
				UPDATE FITABLE020 SET COLUMN16='CLOSE',COLUMN18=@REM WHERE COLUMN01=@COLUMN09
			END
		ELSE
			BEGIN
				UPDATE FITABLE020 SET COLUMN16='OPEN',COLUMN18=@REM WHERE COLUMN01=@COLUMN09
			END

	END





GO
