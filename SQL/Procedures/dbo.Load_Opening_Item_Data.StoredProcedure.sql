USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[Load_Opening_Item_Data]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Load_Opening_Item_Data]
as 

declare @ItemName nvarchar(1000)
declare @Uom nvarchar(50)
declare @Amount decimal(18,2)
declare @inFlag int
declare @cntFlag int
declare @ItemDetails table (id int identity(1,1),name nvarchar(1000),uom nvarchar(50),amount decimal(18,2))

begin
set @inFlag=1
insert into @ItemDetails select column03,column04,column05 from CONTABLE028
select @cntFlag=count(1) from @ItemDetails

while(@inFlag<=@cntFlag)
begin
select @ItemName=name,@Uom=uom,@Amount=amount from @ItemDetails where id=@inFlag

print @ItemName+'-'+@Uom+'-'+convert(varchar(50),@Amount)

set @inFlag=@inFlag+1
end
end
GO
