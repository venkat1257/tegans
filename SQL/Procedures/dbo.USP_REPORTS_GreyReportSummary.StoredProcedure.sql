USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_GreyReportSummary]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[USP_REPORTS_GreyReportSummary]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Joborderno      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
)
as 
begin
if @Joborderno!=''
begin
 set @whereStr= ' where GRN in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Joborderno+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end

 if(@ReportName='GreyReportSummary')
 begin

select Q.ou ,Q.lotno ,Q.cust ,Q.itemGrn ,SUM(Q.orderRQty) orderRQty  ,Q.dt ,Q.RDtt ,Q.IDtt,Q.Dtt ,SUM(Q.IssueQty) IssueQty ,SUM(Q.RPcs) RPcs ,SUM(Q.IPcs) IPcs
 ,Q.OP ,Q.GRN,Q.invNo,Q.dcNo
into #GreyReportSummary from (
select  c7.COLUMN03 ou ,gh.COLUMN04 lotno ,S2.COLUMN05 cust,m7.COLUMN04 itemGrn
,(p2.COLUMN12) orderRQty ,FORMAT(p1.COLUMN06,@DateF) dt,p1.COLUMN06 Dtt
,FORMAT(p3.COLUMN09,@DateF) RDtt,FORMAT(s7.COLUMN08,@DateF) IDtt
,(isnull(p2.COLUMNB12,0)) IssueQty 
,
iif( isnull(p2.COLUMN12,0) >0,(iif((isnull(p3.COLUMN02,0))>0,1,'')),'') RPcs,
iif( isnull(p2.COLUMNB12,0) >0,(iif(((isnull(s7.COLUMN02,0))>0 and isnull(p2.COLUMN12,0)>0),1,'')),'') IPcs
,gh.COLUMN09 OP ,gh.COLUMN04  GRN ,gh.COLUMN12 dcNo, s7.COLUMN13 invNo
from PUTABLE022 gh inner join  PUTABLE023 gl on gh.COLUMN01=gl.COLUMN10 and gh.COLUMNA03=gl.COLUMNA03 and isnull(gl.COLUMNA13,0)=0

inner join PUTABLE001 p1 on p1.COLUMN11=gh.COLUMN02 and p1.COLUMNA03 =gh.COLUMNA03 and isnull(gh.COLUMNA13,0)=0 and  p1.COLUMN03=1586 

 and p1.COLUMN06 between @FromDate and @ToDate and p1.COLUMN06 >=@FiscalYearStartDt
inner join  PUTABLE002 p2 on  p2.COLUMN19 =p1.COLUMN01 and p2.COLUMNA03 =p1.COLUMNA03   and p2.COLUMN03=gl.COLUMN03
 inner  join PUTABLE003 p3 on p3.COLUMN06= p1.COLUMN02 and p3.COLUMNA03 =p1.COLUMNA03 and p3.COLUMN10= p1.COLUMN11
 inner  join PUTABLE004 p4 on  p4.COLUMN12=p3.COLUMN01
 and p4.COLUMN03=p2.COLUMN03  and  p4.COLUMNA03 =p2.COLUMNA03  and p4.COLUMN26=p2.COLUMN02
  and isnull(p4.COLUMNA13,0)=0
  
 left outer join SATABLE007 s7 on s7.COLUMN06 = p1.COLUMN02 and s7.COLUMNA03 =p1.COLUMNA03  and isnull(s7.COLUMNA13,0)=0
 left join SATABLE008 s8 on s8.COLUMN14 =s7.COLUMN01 and   s8.COLUMN26= p2.COLUMN02   and isnull(s8.COLUMNA13,0)=0

  left join CONTABLE007 c7 on c7.COLUMN02=gh.COLUMN09  and isnull(c7.COLUMNA13,0)=0 and  c7.COLUMNA03=gh.COLUMNA03
 LEFT JOIN SATABLE002 S2 ON  S2.COLUMN02=gh.COLUMN05 and isnull(S2.COLUMNA13,0)=0 and  S2.COLUMNA03=gh.COLUMNA03
 left  join MATABLE007 m7 on m7.COLUMN02=gl.COLUMN03 and isnull(m7.COLUMNA13,0)=0   and  m7.COLUMNA03=gh.COLUMNA03

  where  isnull((gh.COLUMNA13),0)=0  AND 
(gh.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',', @OPUnit) s) or gh.columna02 is null) AND  gh.COLUMNA03=@AcOwner
group by c7.COLUMN03 ,gh.COLUMN04 ,S2.COLUMN05 ,m7.COLUMN04 ,p3.COLUMN09 ,s7.COLUMN08,p2.COLUMN02
,p2.COLUMN12,p2.COLUMNB12,P3.COLUMN02,S7.COLUMN02,p4.COLUMN08,p1.COLUMN06,gh.COLUMN09,gh.COLUMN04,gh.COLUMN12 ,s7.COLUMN13 

) Q group by Q.ou ,Q.lotno ,Q.cust ,Q.itemGrn ,Q.RDtt ,Q.IDtt  ,Q.IDtt ,Q.dt ,Q.Dtt ,Q.OP ,Q.GRN ,Q.invNo,Q.dcNo


set @Query1='select * from #GreyReportSummary '+@whereStr+' order by Dtt desc'
exec (@Query1)


end
end


GO
