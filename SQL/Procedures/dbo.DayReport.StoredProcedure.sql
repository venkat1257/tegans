USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[DayReport]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DayReport]
(
@FromDate    nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnitStatus      int= null,
@ToDate      nvarchar(250)= null,
@DateF nvarchar(250)=null)
as 
BEGIN
--if(@OPUnit!='' and @FromDate!='' and @ToDate!='')
--BEGIN
--SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Invoice,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS SalesOrder,
--p.[COLUMN08 ] AS Date,p.[COLUMN09 ] AS Reference,p.[COLUMN12 ] AS Memo,o.[COLUMN03] AS 
--OperatingUnit,d.[COLUMN04 ] AS Department,i.COLUMN10 As Quantity,i.COLUMN11 As AvailableQty,i.COLUMN13 As Price,i.COLUMN14 As Amount
-- From SATABLE009 p 
-- left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 
-- left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 
-- left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 
-- left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06
-- left outer join  SATABLE010 i on i.COLUMN15=p.COLUMN01
--where p.COLUMN14=@OPUnit and p.COLUMN08 between @FromDate and  @ToDate AND p.COLUMNA02=@OPUnit AND  p.COLUMNA03=@AcOwner
--order by p.COLUMN02	desc
--end
 if(@FromDate!='' and @ToDate!='')
BEGIN
---EMPHCS907	 After deleting invoice - Taxes are not cleared  By Raj.Jr 11/8/2015
--SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Invoice,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS SalesOrder,
--p.[COLUMN08 ] AS Date,p.[COLUMN09 ] AS Reference,p.[COLUMN12 ] AS Memo,o.[COLUMN03] AS 
--OperatingUnit,d.[COLUMN04 ] AS Department,i.COLUMN10 As Quantity,i.COLUMN11 As AvailableQty,i.COLUMN13 As Price,i.COLUMN14 As Amount
-- From SATABLE009 p 
-- left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 
-- left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 
-- left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 
-- left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06
-- left outer join  SATABLE010 i on i.COLUMN15=p.COLUMN01
select * from((SELECT o.COLUMN03 ou,FORMAT(p.[COLUMN04 ],@DateF) AS Date,b.[COLUMN04 ] AS Bank,
(case when p.[COLUMN06 ]='EXPENCE' then 'EXPENSE'  when p.[COLUMN06 ]='EXPENCE TRACKING' then 'EXPENSE TRACKING' else  p.[COLUMN06 ] end) AS Type,p.[COLUMN05 ] AS TransactionNo,
(case when p.[COLUMN06 ]='PAYMENT' then (select COLUMN10 from satable011 where COLUMN01 =  p.[COLUMN08 ]  )
      when p.[COLUMN06 ]='BILL PAYMENT' then (select COLUMN10 from putable014 where COLUMN01 =  p.[COLUMN08 ]  )
      when p.[COLUMN06 ]='DEPOSIT' then (  p.[COLUMN19 ]  )
      when p.[COLUMN06 ]='WITHDRAW' then (  p.[COLUMN19 ]  )
	  when p.[COLUMN06 ]='PAYMENT VOUCHER' then (  F22.[COLUMN04]  )
	  when p.[COLUMN06 ]='ADVANCE RECEIPT' then (  F23.[COLUMN11]  )
	  when p.[COLUMN06 ]='ADVANCE PAYMENT' then (  F20.[COLUMN08]  )
	  when p.[COLUMN06 ]='RECEIPT VOUCHER' then (  F24.[COLUMN04]  )
	  when p.[COLUMN06 ]='EXPENSE' then ( IIF(ISNULL(p.COLUMN09,'')!='',p.[COLUMN09],F45.[COLUMN13]  ))
	  else p.COLUMN09 end)
 AS memo,
p.[COLUMN10 ] AS DebitAmount,p.[COLUMN11 ] AS CreditAmount,p.COLUMN04 From PUTABLE019 p 
 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMNa02
 inner join  FITABLE001 b on b.COLUMN02=p.COLUMN03
 LEFT JOIN FITABLE022 F22 ON F22.COLUMN01 = p.COLUMN08 AND F22.COLUMNA03 = p.COLUMNA03 AND F22.COLUMNA02 = p.COLUMNA02 AND ISNULL(F22.COLUMNA13,0)=0
 LEFT JOIN FITABLE023 F23 ON F23.COLUMN01 = p.COLUMN08 AND F23.COLUMN04 = p.COLUMN05 AND F23.COLUMNA03 = p.COLUMNA03 
 AND F23.COLUMNA02 = p.COLUMNA02 AND ISNULL(F23.COLUMNA13,0)=0
 LEFT JOIN FITABLE020 F20 ON F20.COLUMN01 = p.COLUMN08 AND F20.COLUMN04 = p.COLUMN05 AND F20.COLUMNA03 = p.COLUMNA03 
 AND F20.COLUMNA02 = p.COLUMNA02 AND ISNULL(F20.COLUMNA13,0)=0
 LEFT JOIN FITABLE024 F24 ON F24.COLUMN01 = p.COLUMN08 AND F24.COLUMN05 = p.COLUMN11 AND F24.COLUMNA03 = p.COLUMNA03 
 AND F24.COLUMNA02 = p.COLUMNA02 AND ISNULL(F24.COLUMNA13,0)=0
 LEFT JOIN FITABLE045 F45 ON F45.COLUMN01 = p.COLUMN08 AND F45.COLUMN04 = p.COLUMN05 AND F45.COLUMNA03 = p.COLUMNA03 
 AND F45.COLUMNA02 = p.COLUMNA02 AND ISNULL(F45.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0  and p.COLUMN04 between @FromDate and  @ToDate AND (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or isnull(P.COLUMNA02,0)=0) AND  p.COLUMNA03=@AcOwner
)
union all
(SELECT o.COLUMN03 ou,FORMAT(p.[COLUMN04 ],@DateF) AS Date,b.[COLUMN04 ] AS Bank,
(case when p.[COLUMN06 ]='EXPENCE' then 'EXPENSE'  when p.[COLUMN06 ]='EXPENCE TRACKING' then 'EXPENSE TRACKING' else  p.[COLUMN06 ] end) AS Type,p.[COLUMN05 ] AS TransactionNo,
(case when p.[COLUMN06 ]='PAYMENT' then (select COLUMN10 from satable011 where COLUMN01 =  p.[COLUMN09 ]  )
      when p.[COLUMN06 ]='BILL PAYMENT' then (select COLUMN10 from putable014 where COLUMN01 =  p.[COLUMN09 ]  )
      --when p.[COLUMN06 ]='DEPOSIT' then (  p.[COLUMN19 ]  )
      --when p.[COLUMN06 ]='WITHDRAW' then (  p.[COLUMN19 ]  )
	  else p.COLUMN10 end)  
 AS memo,
p.[COLUMN11 ] AS DebitAmount,p.[COLUMN12 ] AS CreditAmount,p.COLUMN04 From FITABLE052 p 
 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMNa02
 inner join  FITABLE001 b on b.COLUMN02=p.COLUMN03
where isnull((p.COLUMNA13),0)=0  and p.COLUMN04 between @FromDate and  @ToDate AND (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or isnull(P.COLUMNA02,0)=0) AND  p.COLUMNA03=@AcOwner
)) p order by p.COLUMN04 desc
end
else 
BEGIN
--SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Invoice,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS SalesOrder,
--p.[COLUMN08 ] AS Date,p.[COLUMN09 ] AS Reference,p.[COLUMN12 ] AS Memo,o.[COLUMN03] AS 
--OperatingUnit,d.[COLUMN04 ] AS Department,i.COLUMN10 As Quantity,i.COLUMN11 As AvailableQty,i.COLUMN13 As Price,i.COLUMN14 As Amount
-- From SATABLE009 p 
-- left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 
-- left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 
-- left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 
-- left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06
-- left outer join  SATABLE010 i on i.COLUMN15=p.COLUMN01
select * from((SELECT o.COLUMN03 ou,FORMAT(p.[COLUMN04 ],@DateF) AS Date,b.[COLUMN04 ] AS Bank,(case when p.[COLUMN06 ]='EXPENCE' then 'EXPENSE'  when p.[COLUMN06 ]='EXPENCE TRACKING' then 'EXPENSE TRACKING' else  p.[COLUMN06 ] end) AS Type,
p.[COLUMN05 ] AS TransactionNo,
(case when p.[COLUMN06 ]='PAYMENT' then (select COLUMN10 from satable011 where COLUMN01 =  p.[COLUMN08 ]  )
      when p.[COLUMN06 ]='BILL PAYMENT' then (select COLUMN10 from putable014 where COLUMN01 =  p.[COLUMN08 ]  )
      when p.[COLUMN06 ]='DEPOSIT' then (  p.[COLUMN19 ]  )
      when p.[COLUMN06 ]='WITHDRAW' then (  p.[COLUMN19 ]  )
	  when p.[COLUMN06 ]='PAYMENT VOUCHER' then (  F22.[COLUMN04]  )
	  when p.[COLUMN06 ]='ADVANCE RECEIPT' then (  F23.[COLUMN11]  )
	  when p.[COLUMN06 ]='ADVANCE PAYMENT' then (  F20.[COLUMN08]  )
	  when p.[COLUMN06 ]='RECEIPT VOUCHER' then (  F24.[COLUMN04]  )
	  when p.[COLUMN06 ]='EXPENSE' then ( IIF(ISNULL(p.COLUMN09,'')!='',p.[COLUMN09],F45.[COLUMN13]  ))
	  else p.COLUMN09 end)  
 AS memo,
p.[COLUMN10 ] AS DebitAmount,p.[COLUMN11 ] AS CreditAmount,p.COLUMN04 From PUTABLE019 p 
 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMNa02
 inner join  FITABLE001 b on b.COLUMN02=p.COLUMN03
 LEFT JOIN FITABLE022 F22 ON F22.COLUMN01 = p.COLUMN08 AND F22.COLUMNA03 = p.COLUMNA03 AND F22.COLUMNA02 = p.COLUMNA02 AND ISNULL(F22.COLUMNA13,0)=0
 LEFT JOIN FITABLE023 F23 ON F23.COLUMN01 = p.COLUMN08 AND F23.COLUMN04 = p.COLUMN05 AND F23.COLUMNA03 = p.COLUMNA03 
 AND F23.COLUMNA02 = p.COLUMNA02 AND ISNULL(F23.COLUMNA13,0)=0
 LEFT JOIN FITABLE020 F20 ON F20.COLUMN01 = p.COLUMN08 AND F20.COLUMN04 = p.COLUMN05 AND F20.COLUMNA03 = p.COLUMNA03 
 AND F20.COLUMNA02 = p.COLUMNA02 AND ISNULL(F20.COLUMNA13,0)=0
 LEFT JOIN FITABLE024 F24 ON F24.COLUMN01 = p.COLUMN08 AND F24.COLUMN05 = p.COLUMN11 AND F24.COLUMNA03 = p.COLUMNA03 
 AND F24.COLUMNA02 = p.COLUMNA02 AND ISNULL(F24.COLUMNA13,0)=0
 LEFT JOIN FITABLE045 F45 ON F45.COLUMN01 = p.COLUMN08 AND F45.COLUMN04 = p.COLUMN05 AND F45.COLUMNA03 = p.COLUMNA03 
 AND F45.COLUMNA02 = p.COLUMNA02 AND ISNULL(F45.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and  (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or isnull(P.COLUMNA02,0)=0) AND  p.COLUMNA03=@AcOwner
)
union all
(SELECT o.COLUMN03 ou,FORMAT(p.[COLUMN04 ],@DateF) AS Date,b.[COLUMN04 ] AS Bank,
(case when p.[COLUMN06 ]='EXPENCE' then 'EXPENSE'  when p.[COLUMN06 ]='EXPENCE TRACKING' then 'EXPENSE TRACKING' else  p.[COLUMN06 ] end) AS Type,p.[COLUMN05 ] AS TransactionNo,
(case when p.[COLUMN06 ]='PAYMENT' then (select COLUMN10 from satable011 where COLUMN01 =  p.[COLUMN09 ]  )
      when p.[COLUMN06 ]='BILL PAYMENT' then (select COLUMN10 from putable014 where COLUMN01 =  p.[COLUMN09 ]  )
      --when p.[COLUMN06 ]='DEPOSIT' then (  p.[COLUMN19 ]  )
      --when p.[COLUMN06 ]='WITHDRAW' then (  p.[COLUMN19 ]  )
	  else p.COLUMN10 end)  
 AS memo,
p.[COLUMN11 ] AS DebitAmount,p.[COLUMN12 ] AS CreditAmount,p.COLUMN04 From FITABLE052 p 
 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMNa02
 inner join  FITABLE001 b on b.COLUMN02=p.COLUMN03
where isnull((p.COLUMNA13),0)=0 AND (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or isnull(P.COLUMNA02,0)=0) AND  p.COLUMNA03=@AcOwner
)) p order by p.COLUMN04 desc
end
END

GO
