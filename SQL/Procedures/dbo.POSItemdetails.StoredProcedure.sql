USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[POSItemdetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[POSItemdetails](@ItemID nvarchar(250)=null,@uom nvarchar(250)=null,@LotId nvarchar(250)=null,@LocationId nvarchar(250)=null,@Type nvarchar(250)=null,@OPUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null,@Customer nvarchar(250)=null)
as
begin
declare @uomid nvarchar(250)
set @uomid=(iif(@uom='','0',isnull(@uom,0)))
set @LotId=(iif(@LotId='',0,isnull(@LotId,0)))
set @LocationId=(iif(@LocationId='',0,isnull(@LocationId,0)))
DECLARE @IsLot nvarchar(250)=null,@IsLocation nvarchar(250)=null
set @IsLot=(select top(1)isnull(COLUMN07,0)COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN31'  and column04=110010828 AND COLUMNA03=@AcOwner and isnull(COLUMNA13,'False')='False')
if (@IsLot != '' and @IsLot != 'N' and @IsLot != '0')
begin
set @IsLot=('1')
end
set @IsLocation=(select top(1)isnull(COLUMN07,0)COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN39'  and column04=110010827 AND COLUMNA03=@AcOwner and isnull(COLUMNA13,'False')='False')
if (@IsLocation != '' and @IsLocation != 'N' and @IsLocation != '0')
begin
set @IsLocation=('1')
end
if((ISNUMERIC(@ItemID)>0) and ((select COLUMN02 from MATABLE007 Where COLUMN02=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)>0))
begin
if(@LotId=0 and @Type='POS' and @IsLot='1')
begin
set @LotId=(select top(1)COLUMN02 from FITABLE043 Where  (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' and COLUMN09=cast(@ItemID as nvarchar) and isnull(COLUMN15,0)=@uomid order by cast(COLUMN05 as date),COLUMN02)
end
set @LotId=(iif(@LotId='',0,isnull(@LotId,0)))
if(@LocationId=0 and @Type='POS' and @IsLocation='1')
begin
set @LocationId=(select top(1)l.COLUMN02 from CONTABLE030 l left join fiTABLE010 f on f.COLUMN21=l.COLUMN02 and f.COLUMNA03=l.COLUMNA03 and f.COLUMN03=@ItemID Where  (l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)  AND l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False' and isnull(l.COLUMN07,'False')='False' order by isnull(f.COLUMN04,0) desc)
end
set @LocationId=(iif(@LocationId='',0,isnull(@LocationId,0)))
Select i.COLUMN02 ItemId,i.COLUMN04 ItemName,m.COLUMN04 Units, IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0))=0) ,1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0)) Quantity,(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(i.COLUMN51,0) end) Price,
(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(i.COLUMN51,0) end)*IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0))=0) ,1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0)) Amount,(case when isnull(i.COLUMN54,0)=1000 then 1000 else isnull(i.COLUMN54,1000) end) TaxType,(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end) unitid,i.COLUMN50 SalesDesc,IIF(ISNULL(C.COLUMN04,0)=1 ,10000,IIF(ISNULL(NV.COLUMN04,0)<0 ,0,ISNULL(NV.COLUMN04,0))) AVL,iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0)) TrackQty,
(case when isnull(cp.COLUMN05,0)>0 then cp.COLUMN06 else 1000 end) pricelevel,0 LOT,i.COLUMN06 UPC  From MATABLE007 i
 left join MATABLE024 sp on sp.COLUMN07=i.COLUMN02 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=i.COLUMNA03 and sp.COLUMNA13=0 
 left join MATABLE026 cp on cp.COLUMN03=@Customer and cp.COLUMN05=i.COLUMN02 and (isnull(cp.COLUMN04,0)in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(cp.COLUMN04,0)=0) and cp.COLUMNA03=@AcOwner and cp.COLUMNA13=0 
 left join FITABLE043 lt on lt.COLUMN02=@LotId and isnull(lt.COLUMN15,0)=(case when isnull(lt.COLUMN15,0)=0 then 0 when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end) and lt.COLUMN09=i.COLUMN02 and (isnull(lt.COLUMN10,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=i.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
 left join MATABLE002 m on  m.COLUMN02=(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end)  and isnull(m.COLUMNA13,'False')='False'  
 left join (SELECT MAX(CAST((ISNULL(COLUMN04,0))AS INT)) COLUMN04,COLUMNA03 FROM CONTABLE031 WHERE COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0 and isnull(COLUMNA13,'False')='False'  AND COLUMN03='Allow BackOrder'
 GROUP BY COLUMNA03) C on  C.COLUMNA03=i.COLUMNA03  and isnull(m.COLUMNA13,'False')='False'
 left join (SELECT SUM(COLUMN04) COLUMN04,COLUMN03,COLUMNA02,COLUMNA03,COLUMN19 FROM FITABLE010 WHERE COLUMN03=@ITEMID AND iif(COLUMN22='',0,isnull(COLUMN22,0))=@LotId AND iif(COLUMN21='',0,isnull(COLUMN21,0))=@LocationId AND iif((@uom!='' and @uom is not null),COLUMN19,10000)=iif((@uom!='' and @uom is not null),@uom,10000) AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@ACOWNER and ISNULL(COLUMNA13,0)=0
 GROUP BY COLUMNA03,COLUMNA02,COLUMN03,COLUMN19) nv on nv.COLUMN03=i.COLUMN02 and nv.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  and NV.COLUMNA03=i.COLUMNA03 and iif((@uom!='' and @uom is not null),@uom,NV.COLUMN19)=iif((@uom!='' and @uom is not null),@uom,i.COLUMN63 )
 Where  (i.COLUMN02=cast(@ItemID as nvarchar(250)) or i.COLUMN06=cast(@ItemID as nvarchar(250))) and  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and 
  isnull(i.COLUMN47,'False')='False' and  isnull(i.COLUMNA13,'False')='False'
end
else if exists(select top(1)COLUMN02 from MATABLE007 Where (COLUMN06=cast(@ItemID as nvarchar(250)) or COLUMN04=cast(@ItemID as nvarchar(250))) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
begin
if(@LotId=0 and @Type='POS' and @IsLot='1')
begin
set @LotId=(select top(1)COLUMN02 from FITABLE043 Where isnull(COLUMN15,0)=@uomid and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' and COLUMN09=(select COLUMN02 from MATABLE007 Where COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0 and (COLUMN02=cast(@ItemID as nvarchar(250))  or COLUMN06=cast(@ItemID as nvarchar(250)) or COLUMN04=cast(@ItemID as nvarchar(250)))) order by cast(COLUMN05 as date),COLUMN02)
end
set @LotId=(iif(@LotId='',0,isnull(@LotId,0)))
if(@LocationId=0 and @Type='POS' and @IsLocation='1')
begin
set @LocationId=(select top(1)l.COLUMN02 from CONTABLE030 l left join fiTABLE010 f on f.COLUMN21=l.COLUMN02 and f.COLUMNA03=l.COLUMNA03 and f.COLUMN03=(select COLUMN02 from MATABLE007 Where COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0 and (COLUMN02=cast(@ItemID as nvarchar(250))  or COLUMN06=cast(@ItemID as nvarchar(250)) or COLUMN04=cast(@ItemID as nvarchar(250))))  Where  (l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)  AND l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False' and isnull(l.COLUMN07,'False')='False' order by isnull(f.COLUMN04,0) desc)
end
set @LocationId=(iif(@LocationId='',0,isnull(@LocationId,0)))
Select i.COLUMN02 ItemId,i.COLUMN04 ItemName,m.COLUMN04 Units, IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0))=0) ,1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0)) Quantity,(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(i.COLUMN51,0) end) Price,
IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0))=0) ,1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0))*(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(i.COLUMN51,0) end)  Amount,(case when isnull(i.COLUMN54,0)=1000 then 1000 else isnull(i.COLUMN54,1000) end) TaxType,(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end) unitid,i.COLUMN50 SalesDesc,IIF(ISNULL(C.COLUMN04,0)=1 ,10000,IIF(ISNULL(NV.COLUMN04,0)<0 ,0,ISNULL(NV.COLUMN04,0))) AVL,iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0)) TrackQty,
(case when isnull(cp.COLUMN05,0)>0 then cp.COLUMN06 else 1000 end) pricelevel,0 LOT,i.COLUMN06 UPC  From MATABLE007 i
 left join MATABLE024 sp on sp.COLUMN07=i.COLUMN02 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=i.COLUMNA03 and sp.COLUMNA13=0 
 left join MATABLE026 cp on cp.COLUMN03=@Customer and cp.COLUMN05=i.COLUMN02 and (isnull(cp.COLUMN04,0)in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(cp.COLUMN04,0)=0) and cp.COLUMNA03=@AcOwner and cp.COLUMNA13=0 
 left join FITABLE043 lt on lt.COLUMN02=@LotId and isnull(lt.COLUMN15,0)=(case when isnull(lt.COLUMN15,0)=0 then 0 when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end) and lt.COLUMN09=i.COLUMN02 and (isnull(lt.COLUMN10,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=i.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
 left join MATABLE002 m on  m.COLUMN02=(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end)  and isnull(m.COLUMNA13,'False')='False' 
 left join (SELECT MAX(CAST((ISNULL(COLUMN04,0))AS INT)) COLUMN04,COLUMNA03 FROM CONTABLE031 WHERE COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0 and isnull(COLUMNA13,'False')='False'  AND COLUMN03='Allow BackOrder'
 GROUP BY COLUMNA03) C on  C.COLUMNA03=i.COLUMNA03  and isnull(m.COLUMNA13,'False')='False'
 left join (SELECT SUM(COLUMN04) COLUMN04,COLUMN03,COLUMNA02,COLUMNA03,COLUMN19 FROM FITABLE010 WHERE  iif(COLUMN22='',0,isnull(COLUMN22,0))=@LotId and iif(COLUMN21='',0,isnull(COLUMN21,0))=@LocationId AND COLUMN03 in(select column02 from matable007 where ((COLUMN02=cast(@ItemID as nvarchar(250)) or COLUMN06=cast(@ItemID as nvarchar(250)) or COLUMN04=cast(@ItemID as nvarchar(250))) AND iif((@uom!='' and @uom is not null),COLUMN19,10000)=(case when (@uom!='' and @uom is not null) then @uom else 10000 end)  AND COLUMNA03=@ACOWNER and ISNULL(COLUMNA13,0)=0)) AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@ACOWNER and ISNULL(COLUMNA13,0)=0
 GROUP BY COLUMNA03,COLUMNA02,COLUMN03,COLUMN19) nv on nv.COLUMN03=i.COLUMN02 and nv.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  and NV.COLUMNA03=i.COLUMNA03   and iif((@uom!='' and @uom is not null),@uom,NV.COLUMN19)=iif((@uom!='' and @uom is not null),@uom,i.COLUMN63 )
 Where  (i.COLUMN02=cast(@ItemID as nvarchar(250)) or i.COLUMN06=@ItemID or i.COLUMN04=@ItemID) and  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and 
  isnull(i.COLUMN47,'False')='False' and  isnull(i.COLUMNA13,'False')='False'
end
else if exists(select top(1)COLUMN02 from MATABLE021 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
begin
declare @AliasItem nvarchar(250)
set @AliasItem=(@ItemID)
if((ISNUMERIC(@ItemID)>0) and ((select COLUMN02 from MATABLE007 Where COLUMN02=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)>0))
begin
set @ItemID=(@ItemID)
end
else
begin
set @ItemID=(select top(1)COLUMN05 from MATABLE021 Where (COLUMN04=cast(@ItemID as nvarchar(250)) or COLUMN05=cast(@ItemID as nvarchar(250))) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
end
if(@LotId=0 and @Type='POS' and @IsLot='1')
begin
set @LotId=(select top(1)COLUMN02 from FITABLE043 Where isnull(COLUMN15,0)=@uomid and (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' and COLUMN09=(select COLUMN02 from MATABLE007 Where COLUMN02=cast(@ItemID as nvarchar(250))) order by cast(COLUMN05 as date),COLUMN02)
end
set @LotId=(iif(@LotId='',0,isnull(@LotId,0)))
if(@LocationId=0 and @Type='POS' and @IsLocation='1')
begin
set @LocationId=(select top(1)l.COLUMN02 from CONTABLE030 l left join fiTABLE010 f on f.COLUMN21=l.COLUMN02 and f.COLUMNA03=l.COLUMNA03 and f.COLUMN03=(select COLUMN02 from MATABLE007 Where COLUMN02=cast(@ItemID as nvarchar(250))) Where  (l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)  AND l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False' and isnull(l.COLUMN07,'False')='False' order by isnull(f.COLUMN04,0) desc)
end
set @LocationId=(iif(@LocationId='',0,isnull(@LocationId,0)))
Select mi.COLUMN02 ItemId,mi.COLUMN04 ItemName,m.COLUMN04 Units,IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(mi.COLUMN48='',0,ISNULL(mi.COLUMN48,0))=0),1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0)) Quantity,
(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) else iif(isnull(i.COLUMN09,0)=0,isnull(mi.COLUMN51,0),isnull(i.COLUMN09,0)) end) Price,
cast((cast((IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(mi.COLUMN48='',0,ISNULL(mi.COLUMN48,0))=0),1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0))) as decimal(18,2))*cast(((case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) else iif(isnull(i.COLUMN09,0)=0,isnull(mi.COLUMN51,0),isnull(i.COLUMN09,0)) end)) as decimal(18,2))) as decimal(18,2)) Amount,
(case when isnull(mi.COLUMN54,0)=1000 then 1000 else isnull(mi.COLUMN54,1000) end) TaxType,(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN06,0) end) unitid,mi.COLUMN50 SalesDesc,IIF(ISNULL(C.COLUMN04,0)=1 ,10000,IIF(ISNULL(NV.COLUMN04,0)<0 ,0,ISNULL(NV.COLUMN04,0))) AVL,iif(mi.COLUMN48='',0,ISNULL(mi.COLUMN48,0)) TrackQty,
(case when isnull(cp.COLUMN05,0)>0 then cp.COLUMN06 else 1000 end) pricelevel,0 LOT,i.COLUMN04 UPC  From MATABLE021 i
 left join MATABLE024 sp on sp.COLUMN07=i.COLUMN05 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=i.COLUMNA03 and sp.COLUMNA13=0 
 left join MATABLE026 cp on cp.COLUMN03=@Customer and cp.COLUMN05=i.COLUMN05 and (isnull(cp.COLUMN04,0)in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(cp.COLUMN04,0)=0) and cp.COLUMNA03=@AcOwner and cp.COLUMNA13=0 
 left join FITABLE043 lt on lt.COLUMN02=@LotId and lt.COLUMN09=i.COLUMN05 and (isnull(lt.COLUMN10,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=i.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
 left join MATABLE002 m on  m.COLUMN02=(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN06,0) end)  and isnull(m.COLUMNA13,'False')='False' left join MATABLE007 mi on  mi.COLUMN02=i.COLUMN05
 left join (SELECT MAX(CAST((ISNULL(COLUMN04,0))AS INT)) COLUMN04,COLUMNA03 FROM CONTABLE031 WHERE COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0 and isnull(COLUMNA13,'False')='False'  AND COLUMN03='Allow BackOrder'
 GROUP BY COLUMNA03) C on  C.COLUMNA03=i.COLUMNA03  and isnull(m.COLUMNA13,'False')='False'
 left join (SELECT ISNULL((ISNULL(COLUMN04,0)),0) COLUMN04,COLUMN03,COLUMNA02,COLUMNA03,COLUMN19,COLUMN24 FROM FITABLE010 WHERE COLUMN03=@ITEMID AND iif(COLUMN22='',0,isnull(COLUMN22,0))=@LotId and iif(COLUMN21='',0,isnull(COLUMN21,0))=@LocationId AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@ACOWNER and ISNULL(COLUMNA13,0)=0
 ) nv on nv.COLUMN03=mi.COLUMN02 and nv.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  and NV.COLUMNA03=i.COLUMNA03   and iif((@uom!='' and @uom is not null),@uom,NV.COLUMN19)=iif((@uom!='' and @uom is not null),@uom,i.COLUMN06)
  Where  (cast(i.COLUMN04 as nvarchar(250))=@AliasItem and cast(i.COLUMN05 as nvarchar(250))=@ItemID) and
  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and isnull(i.COLUMNA13,'False')='False'
end
else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
begin
select @LotId=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
if(@LotId=0 and @Type='POS' and @IsLot='1')
begin
set @LotId=(select top(1)COLUMN02 from FITABLE043 Where  (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' and COLUMN09=cast(@ItemID as nvarchar) and isnull(COLUMN15,0)=@uomid order by cast(COLUMN05 as date),COLUMN02)
end
set @LotId=(iif(@LotId='',0,isnull(@LotId,0)))
if(@LocationId=0 and @Type='POS' and @IsLocation='1')
begin
set @LocationId=(select top(1)l.COLUMN02 from CONTABLE030 l left join fiTABLE010 f on f.COLUMN21=l.COLUMN02 and f.COLUMNA03=l.COLUMNA03 and f.COLUMN03=@ItemID Where  (l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)  AND l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False' and isnull(l.COLUMN07,'False')='False' order by isnull(f.COLUMN04,0) desc)
end
set @LocationId=(iif(@LocationId='',0,isnull(@LocationId,0)))
Select i.COLUMN02 ItemId,i.COLUMN04 ItemName,m.COLUMN04 Units, IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0))=0) ,1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0)) Quantity,(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(i.COLUMN51,0) end) Price,
(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) when isnull(sp.COLUMN05,0)>0 then isnull(sp.COLUMN05,0) else isnull(i.COLUMN51,0) end)*IIF(((ISNULL(NV.COLUMN04,0))>0 or iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0))=0) ,1,IIF(ISNULL(C.COLUMN04,0)=1 OR (ISNULL(NV.COLUMN04,0))=1,1,0)) Amount,(case when isnull(i.COLUMN54,0)=1000 then 1000 else isnull(i.COLUMN54,1000) end) TaxType,(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end) unitid,i.COLUMN50 SalesDesc,IIF(ISNULL(C.COLUMN04,0)=1 ,10000,IIF(ISNULL(NV.COLUMN04,0)<0 ,0,ISNULL(NV.COLUMN04,0))) AVL,iif(i.COLUMN48='',0,ISNULL(i.COLUMN48,0)) TrackQty,
(case when isnull(cp.COLUMN05,0)>0 then cp.COLUMN06 else 1000 end) pricelevel,@LotId LOT,i.COLUMN06 UPC  From MATABLE007 i
 left join MATABLE024 sp on sp.COLUMN07=i.COLUMN02 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=i.COLUMNA03 and sp.COLUMNA13=0 
 left join MATABLE026 cp on cp.COLUMN03=@Customer and cp.COLUMN05=i.COLUMN02 and (isnull(cp.COLUMN04,0)in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(cp.COLUMN04,0)=0) and cp.COLUMNA03=@AcOwner and cp.COLUMNA13=0 
 left join FITABLE043 lt on lt.COLUMN02=@LotId and isnull(lt.COLUMN15,0)=(case when isnull(lt.COLUMN15,0)=0 then 0 when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end) and lt.COLUMN09=i.COLUMN02 and (isnull(lt.COLUMN10,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=i.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
 left join MATABLE002 m on  m.COLUMN02=(case when (@uom!='' and @uom is not null) then @uom else isnull(i.COLUMN63,0) end)  and isnull(m.COLUMNA13,'False')='False'  
 left join (SELECT MAX(CAST((ISNULL(COLUMN04,0))AS INT)) COLUMN04,COLUMNA03 FROM CONTABLE031 WHERE COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0 and isnull(COLUMNA13,'False')='False'  AND COLUMN03='Allow BackOrder'
 GROUP BY COLUMNA03) C on  C.COLUMNA03=i.COLUMNA03  and isnull(m.COLUMNA13,'False')='False'
 left join (SELECT SUM(COLUMN04) COLUMN04,COLUMN03,COLUMNA02,COLUMNA03,COLUMN19,COLUMN24 FROM FITABLE010 WHERE COLUMN03=@ITEMID AND iif(COLUMN22='',0,isnull(COLUMN22,0))=@LotId AND iif(COLUMN21='',0,isnull(COLUMN21,0))=@LocationId AND iif((@uom!='' and @uom is not null),COLUMN19,10000)=iif((@uom!='' and @uom is not null),@uom,10000) AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@ACOWNER and ISNULL(COLUMNA13,0)=0
 GROUP BY COLUMNA03,COLUMNA02,COLUMN03,COLUMN19,COLUMN24) nv on nv.COLUMN03=i.COLUMN02 and nv.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  and NV.COLUMNA03=i.COLUMNA03 and iif((@uom!='' and @uom is not null),@uom,NV.COLUMN19)=iif((@uom!='' and @uom is not null),@uom,i.COLUMN63 )
 Where  (i.COLUMN02=cast(@ItemID as nvarchar(250)) or i.COLUMN06=cast(@ItemID as nvarchar(250))) and  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and 
  isnull(i.COLUMN47,'False')='False' and  isnull(i.COLUMNA13,'False')='False'
end
end


GO
