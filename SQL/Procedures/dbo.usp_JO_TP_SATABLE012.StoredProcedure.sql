USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE012]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE012]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  @COLUMN05   nvarchar(250)=null,  
	@COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  @COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  
	@COLUMN10   nvarchar(250)=null,  @COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null, 
    @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@ReturnValue nvarchar(250)=null out
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
DECLARE @tempSTR nvarchar(MAX)
DECLARE @Num nvarchar(250),@Memo nvarchar(250),@VenID nvarchar(250),@JBNO nvarchar(250)
declare @date date
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
--set @COLUMN08 =(select MAX(COLUMN01) from  SATABLE011);
set @Num =(select COLUMN04 from  SATABLE009 where COLUMN02=@COLUMN03);

--set @COLUMN09 =(select COLUMN13 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN10 =(select COLUMN14 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN11 =(select COLUMN15 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN12 =(select COLUMN16 from  SATABLE011 Where COLUMN01=@COLUMN08 );
SET @date=(SELECT COLUMN19 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @VenID=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @JBNO=(SELECT COLUMN04 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @Memo=(SELECT COLUMN10 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null ) THEN (select COLUMN14 from SATABLE011 
   where COLUMN01=@COLUMN08) else @COLUMNA02  END )
if(@COLUMN05!= '' and @COLUMN05 is not null)
begin
set @COLUMN05=(cast(@COLUMN05 as DECIMAL(18,2))-(cast(@COLUMN06 as DECIMAL(18,2))+cast(isnull(@COLUMN09,0 )as DECIMAL(18,2))))
end
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE012_SequenceNo
insert into SATABLE012 

(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,  COLUMN15,  
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, 
   COLUMND08, COLUMND09, COLUMND10
)

values

( @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  
  @COLUMN12, @COLUMN15,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, 
  @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, 
  @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, 
  @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
if exists(select column01 from SATABLE012 where column08=@COLUMN08)
					begin
--declare @col6 int
--declare @col5 int
DECLARE @RefSOID nvarchar(250)=null
DECLARE @RefBillID nvarchar(250)=null
DECLARE @RefPaymentID nvarchar(250)=null
DECLARE @AMT DECIMAL(18,2)
DECLARE @PAMT DECIMAL(18,2)
declare @Tamt DECIMAL(18,2)
declare @PIQty DECIMAL(18,2)
declare @PBQty DECIMAL(18,2)
DECLARE @deposit DECIMAL(18,2)
set @deposit=  (SELECT COLUMN08 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @RefSOID=(SELECT COLUMN06 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
SET @RefBillID=(SELECT COLUMN07 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
SET @RefPaymentID=(SELECT COLUMN02 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
SET @AMT=(SELECT COLUMN15 FROM SATABLE005 WHERE COLUMN02=@RefSOID);
SET @Tamt=(SELECT sum(COLUMN11) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@RefSOID))
SET @PAMT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN08 in(select COLUMN01 from SATABLE011 where COLUMN06=@RefSOID))
SET @PIQty=isnull((SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
SET @PBQty=isnull((SELECT (19000-(sum(COLUMN06)+sum(COLUMN09))) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
IF(@PBQty>0)
begin
update SATABLE011 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=39) where COLUMN01=@COLUMN08
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=39) where COLUMN02=@COLUMN03
Update SATABLE005 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=56) where COLUMN02=@RefSOID
end
else IF(@PBQty=0.00)
begin
update SATABLE011 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=40) where COLUMN01=@COLUMN08
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=40) where COLUMN02=@COLUMN03
update SATABLE005 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=54) where COLUMN02=@RefSOID
end
declare @advance decimal(18,2)
set @advance=@COLUMN09
	if(@COLUMN09!= '0' and @COLUMN09!= '0.00' and @COLUMN09!= '')
	begin
	DECLARE @MaxRownum INT,@id INT,@cretAmnt decimal(18,2)
      DECLARE @Initialrow INT=1
		set @COLUMN05 =(select COLUMN05 from  SATABLE011 Where COLUMN01=@COLUMN08 );
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@COLUMN05 AND 
			   ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)
			  FITABLE020.COLUMN16='OPEN' and FITABLE020.column02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN15) s) AND ISNULL(FITABLE020.COLUMNA13,0)=0
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		 if(@id!='' and @id is not null)
		   BEGIN 
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363'  AND FITABLE020.COLUMN07=@COLUMN05 AND 
			   ---EMPHCS1533	Advance Selection According to Satya issues BY RAj Jr(COLUMN17 for ADVID)
			  FITABLE020.COLUMN16='OPEN' and FITABLE020.column02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN15) s)
			   AND ISNULL(FITABLE020.COLUMNA13,0)=0 )
			   WHILE @Initialrow <= @MaxRownum
			   BEGIN
		        if(@COLUMN09!='0' and @COLUMN09!= '0.00' and cast(@COLUMN09 as decimal(18,2))>0)
			  begin
				set @cretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 if(cast(@COLUMN09 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(0), COLUMN16='CLOSE'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					--EMPHCS1410	Logs Creation by srinivas	
						set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Advance Amount Reset in FITABLE020 Table by 0 for '+cast(@id as nvarchar(250))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						)

					set @COLUMN09=(cast(@COLUMN09 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN09 as DECIMAL(18,2))<=@cretAmnt)				 
				 begin
					update FITABLE020 set column18=(cast(isnull(column18,0) as DECIMAL(18,2))-cast(isnull(@COLUMN09,0) as DECIMAL(18,2))), 
					COLUMN16='OPEN'  where column02= @id and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					--EMPHCS1410	Logs Creation by srinivas	
						set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						'Advance Amount Reset in FITABLE020 Table by '+cast(@COLUMN14 as nvarchar(250))+' for '+cast(@id as nvarchar(250))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						)

				 set @COLUMN09='0'
				 end
				end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1 
				END
				CLOSE curA 
				deallocate curA
			END
		enD

set @COLUMN09=(select COLUMN09 from SATABLE012 where column02=@COLUMN02)
	if(cast(isnull(@COLUMN09,0) as decimal(18,2))> 0)
	begin
	if exists(select COLUMN05  from PUTABLE016 where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
	begin
	declare @pstatus nvarchar(250),@dueamt decimal(18,2),@paidamt decimal(18,2)
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02) 
           set @pstatus=('Paid')
		   set @COLUMN05 =(select COLUMN05 from PUTABLE015 where column02=@COLUMN02);
		   if(cast(@COLUMN05 as decimal(18,2))=0)
		   begin
		   set @pstatus=('CLOSE')
		   end
		    update PUTABLE016 set COLUMN10=@pstatus,
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@COLUMN09,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@COLUMN09,0) as decimal(18,2))) where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	end
	else
	begin
	declare @newID1 int,@VendorID int,@Bank nvarchar(250)=null,@Memo1 nvarchar(250),@project1 int,@OPunit int
	set @newID1=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1
	set @VendorID=(select COLUMN05 from SATABLE011 where column01=@COLUMN08)
	set @Bank=(select COLUMN08 from SATABLE011 where column01=@COLUMN08)
	set @Memo1=(select COLUMN10 from SATABLE011 where column01=@COLUMN08)
	set @OPunit=(select COLUMN14 from SATABLE011 where column01=@COLUMN08)
	set @project1=(select isnull(COLUMN23,0) from SATABLE011 where column01=@COLUMN08)
	if(@Bank='' or @Bank=null)
	begin
	set @Bank=NULL
	end
	insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03 )
			values(@newID1,2000,@date,@COLUMN08,'JOBBER PAYMENT',@VendorID,@Bank,@JBNO,'Paid',@COLUMN09,@COLUMN09,@Memo1,@OPunit, @COLUMNA03)
	end
	end
declare @Vid1 nvarchar(250)
set @Vid1=((select isnull(max(column02),999) from FITABLE034)+1)

insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,   COLUMN08,COLUMN07,   COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
values(ISNULL(@Vid1,999),  'JOBBER PAYMENT',  @date,  @Memo,  @venID,  @advance,0,@JBNO,'1120',0,@COLUMNA02, @COLUMNA03)

end
set @ReturnValue = 1
END

 

IF @Direction = 'Select'

BEGIN

select * from SATABLE012

END 

 

IF @Direction = 'Update'

BEGIN
set @COLUMN10 =(select COLUMN14 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN11 =(select COLUMN15 from  SATABLE011 Where COLUMN01=@COLUMN08 );
set @COLUMN12 =(select COLUMN16 from  SATABLE011 Where COLUMN01=@COLUMN08 );
declare @trans nvarchar(250) 
set @trans=(select COLUMN04 from  SATABLE011 Where COLUMN01=@COLUMN08 );
SET @date=(SELECT COLUMN19 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @VenID=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @JBNO=(SELECT COLUMN04 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
SET @Memo=(SELECT COLUMN10 FROM SATABLE011 WHERE COLUMN01=@COLUMN08)
declare @Pay decimal(18,2)
set @pay=(select (cast(sum(isnull(COLUMN06,0)) as DECIMAL(18,2))+cast(sum(isnull(COLUMN09,0 ))as DECIMAL(18,2))) from SATABLE012 where COLUMN02=@COLUMN02 and isnull(COLUMNA13,0)=0)
if(@COLUMN04=@COLUMN05)
begin
set @COLUMN05=0.00
end
UPDATE SATABLE012 SET
	COLUMN02=@COLUMN02,   COLUMN03=@COLUMN03,   COLUMN04=@COLUMN04,  COLUMN05=@COLUMN05,  COLUMN06=@COLUMN06,  COLUMN07=@COLUMN07,					COLUMN08=@COLUMN08,   COLUMN09=@COLUMN09,   COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11,  COLUMN12=@COLUMN12,  COLUMNA01=@COLUMNA01,				COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03, COLUMNA04=@COLUMNA04,COLUMNA05=@COLUMNA05, COLUMNA07=@COLUMNA07,				COLUMNA08=@COLUMNA08, COLUMNA09=@COLUMNA09, COLUMNA10=@COLUMNA10,COLUMNA11=@COLUMNA11,COLUMNA12=@COLUMNA12,COLUMNA13=@COLUMNA13,				COLUMNB01=@COLUMNB01, COLUMNB02=@COLUMNB02, COLUMNB03=@COLUMNB03,COLUMNB04=@COLUMNB04,COLUMNB05=@COLUMNB05,COLUMNB06=@COLUMNB06,				COLUMNB07=@COLUMNB07, COLUMNB08=@COLUMNB08,COLUMNB09=@COLUMNB09,COLUMNB10=@COLUMNB10,
	COLUMNB11=@COLUMNB11, COLUMNB12=@COLUMNB12, COLUMND01=@COLUMND01,COLUMND02=@COLUMND02,COLUMND03=@COLUMND03,COLUMND04=@COLUMND04,
	COLUMND05=@COLUMND05, COLUMND06=@COLUMND06, COLUMND07=@COLUMND07,COLUMND08=@COLUMND08,COLUMND09=@COLUMND09,COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02

   SET @RefSOID=(SELECT COLUMN06 FROM SATABLE011 WHERE COLUMN01=@COLUMN08);
  SET @PAMT=(SELECT SUM(COLUMN06) FROM SATABLE012 WHERE COLUMN03=@COLUMN03)
	SET @PIQty=isnull((SELECT max(COLUMN04) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
	SET @PBQty=isnull((SELECT (@PIQty-(sum(COLUMN06)+sum(COLUMN09))) FROM SATABLE012 WHERE COLUMN03=@COLUMN03 and isnull(COLUMNA13,0)=0),0)
IF(@PBQty>0)
begin
update SATABLE011 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=39) where COLUMN01=@COLUMN08
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=39) where COLUMN02=@COLUMN03
Update SATABLE005 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=56) where COLUMN02=@RefSOID
end
else IF(@PBQty=0.00)
begin
update SATABLE011 set column11=(select COLUMN04 from CONTABLE025 where COLUMN02=40) where COLUMN01=@COLUMN08
UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=40) where COLUMN02=@COLUMN03
update SATABLE005 set column16=(select COLUMN04 from CONTABLE025 where COLUMN02=54) where COLUMN02=@RefSOID
end

set @COLUMN09=(select COLUMN09 from SATABLE012 where column02=@COLUMN02)
	if(cast(isnull(@COLUMN09,0) as decimal(18,2))> 0)
	begin
	if exists(select COLUMN05  from PUTABLE016 where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
	begin
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02) 
           set @pstatus=('Paid')
		   set @COLUMN05 =(select COLUMN05 from PUTABLE015 where column02=@COLUMN02);
		   if(cast(@COLUMN05 as decimal(18,2))=0)
		   begin
		   set @pstatus=('CLOSE')
		   end
		    update PUTABLE016 set COLUMN10=@pstatus,
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@COLUMN09,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@COLUMN09,0) as decimal(18,2))) where COLUMN09=@JBNO and COLUMN05=@COLUMN08 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	end
	else
	begin
	set @newID1=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE016) as int)+1
	set @VendorID=(select COLUMN05 from SATABLE011 where column01=@COLUMN08)
	set @Bank=(select COLUMN08 from SATABLE011 where column01=@COLUMN08)
	set @Memo1=(select COLUMN10 from SATABLE011 where column01=@COLUMN08)
	set @OPunit=(select COLUMN14 from SATABLE011 where column01=@COLUMN08)
	set @project1=(select isnull(COLUMN23,0) from SATABLE011 where column01=@COLUMN08)
	if(@Bank='' or @Bank=null)
	begin
	set @Bank=NULL
	end
	insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03 )
			values(@newID1,2000,@date,@COLUMN08,'JOBBER PAYMENT',@VendorID,@Bank,@JBNO,'Paid',@COLUMN09,@COLUMN09,@Memo1,@OPunit, @COLUMNA03)
	end
	end
--set @Vid1=((select isnull(max(column02),999) from FITABLE034)+1)

--insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,   COLUMN08,COLUMN07,   COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
--values(ISNULL(@Vid1,999),  'JOBBER PAYMENT',  @date,  @Memo,  @venID,  @COLUMN09,0,@JBNO,'1120',0,@COLUMNA02, @COLUMNA03)
END

 

else IF @Direction = 'Delete'

BEGIN
--EMPHCS1053 rajasekhar reddy patakota 29/08/2015 Jobber Payments Functionality Changes
UPDATE SATABLE012 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02

END
exec [CheckDirectory] @tempSTR,'usp_JO_TP_SATABLE012.txt',0
end try
begin catch
		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_JO_SATABLE012.txt',0

if not exists(select column01 from SATABLE012 where column08=@COLUMN08)
					begin
					delete SATABLE011 where column01=@COLUMN08
					return 0
					end
return 0
end catch

end















GO
