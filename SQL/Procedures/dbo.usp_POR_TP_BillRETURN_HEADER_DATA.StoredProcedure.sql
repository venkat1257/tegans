USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_BillRETURN_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_POR_TP_BillRETURN_HEADER_DATA]
(
	@PurchaseOrderID int
)

AS
BEGIN
	SELECT a.COLUMN05,a.COLUMN12,a.COLUMN10 COLUMN11,a.COLUMN21,a.COLUMN04, a.COLUMN14, a.COLUMN15,a.COLUMN16,a.COLUMN17,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	a.COLUMN22,a.COLUMN14,a.COLUMN24,a.COLUMN26,a.COLUMN29,a.COLUMN43,a.COLUMN41
,(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) cName
	FROM PUTABLE005 a WHERE COLUMN02= @PurchaseOrderID
END









GO
