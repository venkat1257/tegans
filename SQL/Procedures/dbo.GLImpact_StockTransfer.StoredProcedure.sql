USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GLImpact_StockTransfer]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GLImpact_StockTransfer] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,p.COLUMN07 Memo,sum(p17.COLUMN10) Credit,sum(p17.COLUMN11) Debit  from PRTABLE003 p
INNER JOIN PRTABLE004 PR4 ON PR4.COLUMN08=p.COLUMN01 AND PR4.COLUMNA03=p.COLUMNA03 AND PR4.COLUMNA02=p.COLUMNA02 AND isnull(PR4.COLUMNA13,0)=0
left outer join CONTABLE007 C7 ON C7.COLUMN02 = p.COLUMN05 AND C7.COLUMNA03 = p.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
left outer join putable017 p17 on p17.COLUMN05= PR4.COLUMN01  and p17.COLUMNA03= PR4.COLUMNA03 and p17.COLUMNA02= PR4.COLUMNA02 and p17.COLUMN06='Stock Transfer' and isnull(p17.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03 AND f1.COLUMNA03 = p17.COLUMNA03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04,p.COLUMN07
UNION ALL
select f1.COLUMN04 Account,p.COLUMN07 Memo,sum(p17.COLUMN11) Credit,sum(p17.COLUMN10) Debit  from PRTABLE003 p
INNER JOIN PRTABLE004 PR4 ON PR4.COLUMN08=p.COLUMN01 AND PR4.COLUMNA03=p.COLUMNA03 AND PR4.COLUMNA02=p.COLUMNA02 AND isnull(PR4.COLUMNA13,0)=0
left outer join CONTABLE007 C7 ON C7.COLUMN02 = p.COLUMN06 AND C7.COLUMNA03 = p.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
left outer join putable017 p17 on p17.COLUMN05= PR4.COLUMN01  and p17.COLUMNA03= PR4.COLUMNA03 and p17.COLUMNA02= PR4.COLUMNA02 and p17.COLUMN06='Stock Transfer' and isnull(p17.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03 AND f1.COLUMNA03 = p17.COLUMNA03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04,p.COLUMN07
)
select [Account],[Memo],[Credit],[Debit] from MyTable
group by [Account],[Memo],[Credit],[Debit]
end



GO
