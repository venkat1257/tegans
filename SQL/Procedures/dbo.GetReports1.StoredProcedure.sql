USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetReports1]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   procedure [dbo].[GetReports1]
(
@ReportName nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@DateF nvarchar(250)=null)
as 
begin
  declare @temp1 decimal(18,2)
  declare @temp2 decimal(18,2)
IF  @ReportName='PayableReport'
BEGIN

--EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
if((@FromDate!='' and @ToDate!='' and @Type!='' and @Vendor!='' and @OperatingUnit!='' and @Project!='' ))
begin
set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016
	
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))
set @temp2=max(@temp2);

;With MyTable AS
(
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance' AS AAA ,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select  COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02 ) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF)  as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,
COLUMN12,COLUMN13,

 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) 
COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 ,

(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end) AS AAA,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF)  as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, COLUMN18 COLUMN12,0 COLUMN13,
 ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate  from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Vendor!='' and @OperatingUnit!=''))
begin
set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)



set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016

where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2=max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10, (case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance1' AS AAA,NULL orderDate  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
HAVING isnull(isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0),0) != 0 
union all
select  COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02 ) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,
COLUMN13 COLUMN12,COLUMN12 COLUMN13,
 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 ,

(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end) AS AAA,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, COLUMN18 COLUMN12,0 COLUMN13,
 ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18  , NULL AS AAA,COLUMN05 orderDate  from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''and @OperatingUnit!='' ))

begin
set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner))



set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016

where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner))
set @temp2=max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10, (case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance' AS AAA,NULL orderDate  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner )
 HAVING isnull(isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0),0) != 0 

union all
select  COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02 ) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,
COLUMN13 COLUMN12,COLUMN12 COLUMN13,
 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 ,

(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end) AS AAA,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) ) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, COLUMN18 COLUMN12,0 COLUMN13,
 ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18  , NULL AS AAA,COLUMN05 orderDate  from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner 
)
Select *
From MyTable
Order by orderDate
end

--EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
else if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!=''   and @Project!=''  ))
begin


set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))


set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016

where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))
set @temp2=max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance' AS AAA,NULL orderDate  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,
COLUMN12,COLUMN13,
 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0)

COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,

(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end) AS AAA,COLUMN04 orderDate  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, COLUMN18 COLUMN12,0 COLUMN13,
 ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by orderDate
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!='' ))

begin
set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016 


where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016

	
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) 
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2=max(@temp2);

--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
;With MyTable AS
(

select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance' AS AAA,NULL orderDate   from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
HAVING isnull(isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0),0) != 0 
union all

select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06, FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN13 as COLUMN12,COLUMN12 as COLUMN13,
 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) AS 
COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 ,
(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end)  AS AAA,COLUMN04 orderDate

 from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner




union all

select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06, FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, COLUMN18 COLUMN12, 0 COLUMN13,
  ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as COLUMN14,
 NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by   orderDate
end
--EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!=''  and @Project!='' )
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=@OPUnit ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  
)
Select *
From MyTable
Order by  orderDate
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02   AND  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s))  AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) 
 AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
 union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate --and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by  orderDate
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''  and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by orderDate
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' )

begin
set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016 


where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and   COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016

	
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate  
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2=max(@temp2);

--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015

;With MyTable AS
(
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance' AS AAA,NULL orderDate   from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner

union all

select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN13 as COLUMN12,COLUMN12 as COLUMN13,
 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) AS 
COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 ,
(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end)  AS AAA,COLUMN04 orderDate

 from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner




union all

select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,
  ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as COLUMN14,
 NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  

)
Select *
From MyTable
Order by orderDate
end


else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''))
begin
set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016 


where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 
	from putable016

	
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) 
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2=max(@temp2);

--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
;With MyTable AS
(

select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 ,'Opening Balance' AS AAA,NULL orderDate   from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
GROUP BY putable016.COLUMNA02  HAVING isnull(isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0),0) != 0 
union all

select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06, FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN13 as COLUMN12,COLUMN12 as COLUMN13,
 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) AS 
COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 ,
(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end)  AS AAA,COLUMN04 orderDate

 from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner




union all

select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06, FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, COLUMN18 COLUMN12, 0 COLUMN13,
  ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as COLUMN14,
 NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by orderDate
end
--begin

--set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
--where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02)

--set @temp2=(select (SUM(isnull(COLUMN12,0)) 
--	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 

--from putable016
--where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner)

--set @temp2=max(@temp2);
--;With MyTable AS
--(
----EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
--select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
--NULL COLUMN08,null COLUMN09,null COLUMN06,@FromDate	 COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
--			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
--			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
--			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,
--CAST(@temp1 AS money) COLUMN14,
--NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,'Opening Balance' AS AAA  from putable016
--where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02
--union all
--select  COLUMN02,
--(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,

-- (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
--	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN14

--,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,

--(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
-- when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

--else ('PURCHASE A/C') end) AS AAA
 






--from putable016
--where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner
--union all
--select NULL 
--COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
--(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
--COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
--(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
--when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
-- when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
-- COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,
-- ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
--	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA from fitable020
--where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
----order by COLUMN02 desc
--)
--Select *
--From MyTable
--Order by COLUMN11 desc
--end

else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02   AND  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s))  AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) 
 AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
 union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate --and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by  orderDate
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02   AND  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s))  AND  COLUMNA03=@AcOwner  group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) 
 AND  COLUMNA03=@AcOwner
 union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate --and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by orderDate
end
else if(@FromDate!='' and @ToDate!='' and @Project!='')
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02 AND COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 orderDate from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
)
Select *
From MyTable
Order by orderDate
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''))
begin

set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02)

set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 

from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner)

set @temp2=max(@temp2);
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN08,null COLUMN09,null COLUMN06,@FromDate	 COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,
CONVERT(varchar,CAST(@temp1 AS money), 1) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,'Opening Balance' AS AAA,NULL orderDate  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,

 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN14

,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,

(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end) AS AAA,COLUMN04 orderDate

from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner
union all
select NULL 
COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,
 ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
)
Select *
From MyTable
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!='' ))
begin

set @temp1=(select  ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0)  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02)

set @temp2=(select (SUM(isnull(COLUMN12,0)) 
	-SUM(isnull(COLUMN13,0)) )+ISNULL(@temp1,0) 

from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner)

set @temp2=max(@temp2);
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN08,null COLUMN09,null COLUMN06,@FromDate	 COLUMN11 ,NULL COLUMN07,NULL COLUMN10,(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) < 0 
			then ISNULL((isnull(sum(COLUMN13),0)-isnull(sum(COLUMN12),0)),0)end)COLUMN12,
			(case when ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) > 0 
			then ISNULL((isnull(sum(COLUMN12),0)-isnull(sum(COLUMN13),0)),0) end) COLUMN13,
CONVERT(varchar,CAST(@temp1 AS money), 1) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,'Opening Balance' AS AAA,NULL orderDate  from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04,@DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,

 (SUM(isnull(COLUMN12,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN13,0)) OVER(ORDER BY COLUMN04 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN14

,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,

(case when putable016.COLUMN06='BILL PAYMENT' then (select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08) 
 when putable016.COLUMN06='BILL' then ('PURCHASE A/C' )

else ('PURCHASE A/C') end) AS AAA,COLUMN04 orderDate

from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner
union all
select NULL 
COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05,@DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,
 ((SUM(isnull(0,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN18,0)) OVER(ORDER BY COLUMN05 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ @temp2) as  COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18 , NULL AS AAA,COLUMN05 orderDate from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner   
)
Select *
From MyTable
Order by orderDate   
end

end


ELSE IF  @ReportName='ReceivableReport'
BEGIN
if((@FromDate!='' and @ToDate!='' and @Type!='' and @Customer!='' and @OperatingUnit!='' and @Project!='' ))
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0 AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,
isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,COLUMN04 COLUMN04, COLUMN09 COLUMN05,
NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,
NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 ,
--(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= max(putable018.COLUMN05) AND COLUMNA03=putable018.COLUMNA03 ) )
-- when putable018.COLUMN06='Invoice' then ('SALES A/C' )

--else ('SALES A/C') end) 
null AS AAA,COLUMN04 orderDate from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)and COLUMN03='PAYMENT'and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07

union all
select COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,

COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate  from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 , 'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036  
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((SUM(isnull(0,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0))

 COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08  ,'SALES A/C'  AS AAA,COLUMN05 orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,COLUMN02,COLUMN19

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08 , 'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036  
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Customer!='' and @OperatingUnit!=''))
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0  and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,
isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0  and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
HAVING ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) != 0 
union all
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
--select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE025.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN04 COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE025.COLUMN06) COLUMN07,CONVERT(VARCHAR(10),  COLUMN03, 103) COLUMN04, NULL COLUMN05,
--COLUMN09 COLUMN09,

--COLUMN11 COLUMN11,
--(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
--	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 ,
----(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= max(putable018.COLUMN05) AND COLUMNA03=putable018.COLUMNA03 ) )
---- when putable018.COLUMN06='Invoice' then ('SALES A/C' )

----else ('SALES A/C') end) 
--null AS AAA from FITABLE025 
--where isnull((COLUMNA13),0)=0 and   COLUMN03 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
--and  COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
--union all
select COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN05,

COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) 
COLUMN12,COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate  from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08  ,'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((SUM(isnull(0,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0))

 COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08  ,'SALES A/C'  AS AAA,COLUMN05 orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)     group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,COLUMN02,COLUMN19

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)     group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA,orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by orderDate
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @OperatingUnit!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 orderDate from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 orderDate from FITABLE036 
where isnull((COLUMNA13),0)=0 AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from FITABLE036 
where isnull((COLUMNA13),0)=0 AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 < @FromDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @OperatingUnit!=''))
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0  and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) 
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,
isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0  and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
HAVING ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) != 0 
union all
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
--select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE025.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN04 COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE025.COLUMN06) COLUMN07,CONVERT(VARCHAR(10),  COLUMN03, 103) COLUMN04, NULL COLUMN05,
--COLUMN09 COLUMN09,

--COLUMN11 COLUMN11,
--(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
--	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 ,
----(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= max(putable018.COLUMN05) AND COLUMNA03=putable018.COLUMNA03 ) )
---- when putable018.COLUMN06='Invoice' then ('SALES A/C' )

----else ('SALES A/C') end) 
--null AS AAA from FITABLE025 
--where isnull((COLUMNA13),0)=0 and   COLUMN03 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
--and  COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
--union all

select COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF), COLUMN05,

COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0)
COLUMN12,COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate  from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08  ,'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((SUM(isnull(0,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0))

 COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08  ,'SALES A/C'  AS AAA,COLUMN05 orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)     group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,COLUMN02,COLUMN19

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)     group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''  and @Project!='' ))
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0 AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 <@FromDate  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))
set @temp2= max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08  ,'Opening Balance' AS AAA,NULL orderDate  from putable018 where isnull((COLUMNA13),0)=0 AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 <@FromDate  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where isnull((COLUMNA13),0)=0 and  COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF)  COLUMN04, COLUMN05,
COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,
COLUMN14,COLUMN15,COLUMN08  ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate from putable018 
where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 ,'SALES A/C'  AS AAA,COLUMN04 orderDate from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF)  COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((sum(cast(isnull(0,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0))  COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,'SALES A/C'  AS AAA,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,COLUMN02,COLUMN19

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL  COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL  COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''))
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);


;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
HAVING ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)!=0 
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where isnull((COLUMNA13),0)=0 and  COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN05,
COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,
COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02 in(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate from putable018 
where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner and isnull(columna13,0)=0 
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 , 'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(columna13,0)=0 and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,SUM(isnull(COLUMN07,0)) COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08 , 'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 <@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(columna13,0)=0 and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((sum(cast(isnull(0,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0)) COLUMN12,
NULL  as COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,'SALES A/C'  AS AAA,COLUMN05 orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and isnull(columna13,0)=0   group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN02,FITABLE023.COLUMN19
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN12,
NULL  as COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and isnull(columna13,0)=0   group by FITABLE023.COLUMNA02
)
--Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
--((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
--From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
--Order by  orderDate
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,iif(SUM(isnull(COLUMN09,0))-sum(isnull(COLUMN11,0))>0,SUM(isnull(COLUMN09,0))-sum(isnull(COLUMN11,0)),0)COLUMN09,iif(SUM(isnull(COLUMN09,0))-sum(isnull(COLUMN11,0))<0,SUM(isnull(COLUMN11,0))-sum(isnull(COLUMN09,0)),0)COLUMN11,
(( sum(iif(SUM(isnull(COLUMN09,0))-sum(isnull(COLUMN11,0))>0,SUM(isnull(COLUMN09,0))-sum(isnull(COLUMN11,0)),SUM(isnull(COLUMN09,0))-sum(isnull(COLUMN11,0)))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
end

else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' and @Project!='' )
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 orderDate from putable018 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 orderDate   from FITABLE036
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)and   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate   from FITABLE036
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)and   COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,orderDate
Order by  orderDate
end

else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate    
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where isnull((COLUMNA13),0)=0 and  COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN05,
COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,
COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02 in(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate from putable018 
where   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 , 'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((sum(cast(isnull(0,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0)) COLUMN12,
NULL  as COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,'SALES A/C'  AS AAA,COLUMN05 orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN02,FITABLE023.COLUMN19

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08 , 'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where   COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,
NULL  as COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @Project!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN04 <@FromDate AND  COLUMNA03=@AcOwner and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) GROUP BY putable018.COLUMNA02
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 orderDate from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 orderDate   from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate   from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner  and COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
Order by  orderDate
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 and  COLUMN04 <@FromDate AND  COLUMNA03=@AcOwner GROUP BY putable018.COLUMNA02
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 orderDate from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 orderDate   from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate   from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @Project!=''))
begin

set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN04 <@FromDate and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) )

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s))
set @temp2= max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN04 <@FromDate and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) GROUP BY putable018.COLUMNA02
HAVING ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) != 0 
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 orderDate from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 orderDate   from FITABLE036  
where isnull((COLUMNA13),0)=0  and COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((sum(cast(isnull(0,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0)) COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,COLUMN02,COLUMN19

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate   from FITABLE036  
where isnull((COLUMNA13),0)=0  and COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner  and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08, orderDate
Order by  orderDate
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!=''))
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0 and  COLUMN04 <@FromDate and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
AND  COLUMNA03=@AcOwner )


set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);

;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  null as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08 ,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 and  COLUMN04 <@FromDate and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
AND  COLUMNA03=@AcOwner HAVING ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) != 0 
union all
select null COLUMN02, null as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,COLUMN09,COLUMN11,
 (SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0)
COLUMN12,COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02=(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 , 'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02, null as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((sum(cast(isnull(0,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0)) COLUMN12,
NULL as  COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08 ,'SALES A/C'  AS AAA,COLUMN05 orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  group by FITABLE023.COLUMN02, FITABLE023.COLUMN19, FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02, null as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,
NULL as  COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  group by FITABLE023.COLUMN02
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,
((sum(cast(isnull(COLUMN09,0)as decimal(18,2))-cast(isnull(COLUMN11,0)as decimal(18,2))) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by orderDate
end

else if(@FromDate!='' and @ToDate!='')
begin
set @temp1= (select  ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)  from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  
 AND  COLUMNA03=@AcOwner)

set @temp2=(select ((SUM(isnull(COLUMN09,0))
	-SUM(isnull(COLUMN11,0)))+ISNULL(@temp1,0) ) from putable018 
where    COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner)
set @temp2= max(@temp2);


;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02, null as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04 ,NULL COLUMN05,(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)>0 
			then ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)end)COLUMN09,
			(case when ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0)<0 
			then ISNULL((isnull(sum(COLUMN11),0)-isnull(sum(COLUMN09),0)),0) end) COLUMN11,ISNULL((isnull(sum(COLUMN09),0)-isnull(sum(COLUMN11),0)),0) COLUMN12,
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance' AS AAA,NULL orderDate from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate    
 AND  COLUMNA03=@AcOwner
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where isnull((COLUMNA13),0)=0 and  COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF), COLUMN05,
COLUMN09,COLUMN11,
(SUM(isnull(COLUMN09,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(COLUMN11,0)) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp1,0) COLUMN12,
COLUMN14,COLUMN15,COLUMN08 ,
(case when putable018.COLUMN06='Payment' then (select column04 from FITABLE001 where COLUMN02 in(select COLUMN08 FROM SATABLE011 WHERE COLUMN04= putable018.COLUMN05 AND COLUMNA03=putable018.COLUMNA03 AND COLUMNA02=putable018.COLUMNA02 AND isnull(COLUMNA13,0)=0 ) )
 when putable018.COLUMN06='Invoice' then ('SALES A/C' )

else ('SALES A/C') end)  AS AAA,COLUMN04 orderDate from putable018 
where    COLUMN04 between @FromDate and @ToDate and  COLUMNA03=@AcOwner
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'Discount' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 , 'SALES A/C'  AS AAA,COLUMN04 orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09,COLUMN02,COLUMN07
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
((sum(cast(isnull(0,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	- sum(cast(isnull(COLUMN19,0)as decimal(18,2))) OVER(ORDER BY COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+ISNULL(@temp2,0)) COLUMN12,
NULL  as COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,'SALES A/C'  AS AAA,COLUMN05 orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN02,FITABLE023.COLUMN19
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(isnull(COLUMN07,0)) COLUMN11,NULL COLUMN12 ,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08 , 'Opening Balance'  AS AAA,NULL orderDate  from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner  and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,NULL COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,
NULL COLUMN12,NULL  as COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,'Opening Balance'  AS AAA,NULL orderDate  from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 <  @FromDate AND  COLUMNA03=@AcOwner group by FITABLE023.COLUMNA02 
)
Select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,iif(SUM(COLUMN09)-sum(COLUMN11)>0,SUM(COLUMN09)-sum(COLUMN11),0) COLUMN09,iif(SUM(COLUMN09)-sum(COLUMN11)>0,0,SUM(COLUMN11)-sum(COLUMN09)) COLUMN11,
((sum((COLUMN09))-sum(isnull(COLUMN11,0))-sum(0) OVER(Order by orderDate asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) )) COLUMN12,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
From MyTable group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,COLUMN04,COLUMN05,COLUMN14,COLUMN15,COLUMN08,AAA, orderDate
Order by  orderDate 
end

END

end




GO
