USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE017]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_SATABLE017]



(

    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null, 
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@ReturnValue nvarchar(250)=null

)
AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
insert into SATABLE017 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11, 
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15, COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,@COLUMNA01, @COLUMNA02, @COLUMNA03, 
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
) 
if(cast(@COLUMN10 as decimal(18,2))=cast(@COLUMN13 as decimal(18,2)))
begin
UPDATE SATABLE007 SET COLUMN14='QC Completed' WHERE COLUMN06=@COLUMN06
UPDATE SATABLE017 SET COLUMN14='QC Completed' WHERE COLUMN02=@COLUMN02
end
else
begin
UPDATE SATABLE007 SET COLUMN14='QC Failed' WHERE COLUMN06=@COLUMN06
UPDATE SATABLE017 SET COLUMN14='QC Failed' WHERE COLUMN02=@COLUMN02
end

END
 ELSE IF @Direction = 'Select'
BEGIN
select * from SATABLE017
END 
ELSE IF @Direction = 'Update'
BEGIN
UPDATE SATABLE017 SET
   COLUMN13=@COLUMN13,    COLUMNA07=@COLUMNA07  WHERE COLUMN02 = @COLUMN02
if(cast(@COLUMN10 as decimal(18,2))=cast(@COLUMN13 as decimal(18,2)))
begin
UPDATE SATABLE007 SET COLUMN14='QC Completed' WHERE COLUMN06=@COLUMN06
UPDATE SATABLE017 SET COLUMN14='QC Completed' WHERE COLUMN02=@COLUMN02
end
else
begin
UPDATE SATABLE007 SET COLUMN14='QC Failed' WHERE COLUMN06=@COLUMN06
UPDATE SATABLE017 SET COLUMN14='QC Failed' WHERE COLUMN02=@COLUMN02
end
set @ReturnValue = 1
end
ELSE IF @Direction = 'Delete'
BEGIN
UPDATE SATABLE017 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
SET @COLUMN09=(SELECT COLUMN01 FROM SATABLE017 WHERE COLUMN02=@COLUMN02)
UPDATE SATABLE018 SET COLUMNA13=@COLUMNA13 WHERE COLUMN09 = @COLUMN09
END
end try
begin catch
			SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
			DECLARE @tempSTR NVARCHAR(MAX)
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
    exec [CheckDirectory] @tempSTR,'usp_EXCEPTION_SAL_SATABLE017.txt',0
return 0
end catch
end














GO
