USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_C_Address]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAL_TP_C_Address]
(
	@CustomerId int
)

AS
BEGIN
	SELECT a.COLUMN06,a.COLUMN07+a.column08 COLUMN07,a.COLUMN08,a.COLUMN09,a.COLUMN10,st.COLUMN03 COLUMN11,ct.COLUMN03 COLUMN16,
	--a.COLUMN10,a.COLUMN16,
	--EMPHCS1501	Invoice Print changes adding Drug LIC,MRP,PTR By Raj
	s.COLUMN21,s.COLUMN28,s.COLUMN34,a.COLUMN12,s.COLUMN05,a.COLUMN26 COLUMN25,a.COLUMN27 COLUMN26,a.COLUMN28 COLUMN27,
	a.COLUMN31 SHcity,ss.COLUMN03 COLUMN29,sc.COLUMN03 COLUMN30,a.COLUMN32 COLUMN31,s.COLUMN11 custPhNo,s.COLUMN10 Custmail,s.COLUMN42 CustGSTIN
	FROM dbo.SATABLE003 a left join SATABLE002 s on s.COLUMN02=@CustomerId 
	left join MATABLE017 st on st.COLUMN02=a.COLUMN11
	left join MATABLE016 ct on ct.COLUMN02=a.COLUMN16
	left join MATABLE017 ss on ss.COLUMN02=a.COLUMN30
	left join MATABLE016 sc on sc.COLUMN02=a.COLUMN29
	WHERE a.COLUMN19='Customer' and a.COLUMN02 = s.COLUMN16
END



--select*from satable003 where columna03=56636










GO
