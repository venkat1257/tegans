USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_proc_GETHSNDETAILS]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_proc_GETHSNDETAILS](@HSNcode nvarchar(250)=null,@Desc nvarchar(max)=null,
@Chapter nvarchar(250)=null,@Scheme nvarchar(250)=null,@Rate nvarchar(250)=null,@OPUnit nvarchar(250)=null,
@AcOwner nvarchar(250)=null,@whereStr nvarchar(max)=null)
AS
BEGIN
if (@HSNcode!='')
begin
set @whereStr=' where COLUMN04 like ''%'+@HSNcode+'%'''
end
if (@Desc!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where REPLACE(COLUMN05,'''''''','''') like ''%'+@Desc+'%'''
end
else
begin
set @whereStr=@whereStr+' and REPLACE(COLUMN05,'''''''','''') like ''%'+@Desc+'%'''
end
end
if (@Chapter!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where COLUMN06 like ''%'+@Chapter+'%'''
end
else
begin
set @whereStr=@whereStr+' and COLUMN06 like ''%'+@Chapter+'%'''
end
end
if (@Scheme!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where COLUMN07 like ''%'+@Scheme+'%'''
end
else
begin
set @whereStr=@whereStr+' and COLUMN07 like ''%'+@Scheme+'%'''
end
end
if (@Rate!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where COLUMN08 like ''%'+@Rate+'%'''
end
else
begin
set @whereStr=@whereStr+' and COLUMN08 like ''%'+@Rate+'%'''
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1 and isnull(COLUMNA13,0)=0'
end
else
begin
 set @whereStr=@whereStr+' and isnull(COLUMNA13,0)=0'
end

exec('
SELECT top(1000) COLUMN02,COLUMN04 [HSN/SAC Code],COLUMN05 [HSN/SAC Description],COLUMN06 Chapter,COLUMN07 Scheme,COLUMN08 Rate FROM MATABLE032 '+@whereStr+'')

END
GO
