USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_SATABLE007 ]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_POR_TP_SATABLE007 ]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null, @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null, @COLUMN19   nvarchar(250)=null,
	@COLUMN20  nvarchar(250)=null,   @COLUMN21  nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,@COLUMN23   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250)=null,       
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN20=(1003)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE007_SequenceNo
insert into SATABLE007 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,COLUMN16, COLUMN17,  COLUMN18,COLUMN19, COLUMN20, COLUMN21, COLUMN22,COLUMN23,
  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19,@COLUMN20, @COLUMN21, @COLUMN22, @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(Select COLUMN01 from SATABLE007 where COLUMN02=@COLUMN02)
END

IF @Direction = 'Select'
BEGIN
select * from SATABLE007
END 

 
IF @Direction = 'Update'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN20=(1003)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @SOID nvarchar(250),@location nvarchar(250)=null,@linelocation nvarchar(250)=null,@lotno nvarchar(250)=null,@lIID nvarchar(250)=null
set @SOID=(select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
set @location =(select COLUMN23 from  SATABLE007 Where COLUMN01=@SOID );
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
 UPDATE SATABLE007 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,  COLUMN16=@COLUMN16, 
   COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,     COLUMN22=@COLUMN22,COLUMN23=@COLUMN23,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
      COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   
 declare @ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250)
 declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
 --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
 @Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2),@Initialrow INT,@MaxRownum INT,@Item nvarchar(250),@id nvarchar(250),@uom nvarchar(250)
      set @SOID=(select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
declare @FRMID nvarchar(250),@FRMType nvarchar(250),@RefLineId nvarchar(250)
set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN02=@COLUMN02)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
    if (@FRMID=1276)
	begin
	set @FRMType=('Item Issue')
	end
    else if (@FRMID=1354)
	begin
	set @FRMType=('Return Issue')
	end
	else
	begin
	set @FRMType=('JobOrder Issue')
	end 
	  --Inventory Asset table Updations
				delete from PUTABLE011 where COLUMN02=@SOID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN15 and COLUMNA03=@COLUMNA03
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06=@FRMType and  COLUMN13=@COLUMN15 and COLUMNA03=@COLUMNA03
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14 in(@SOID) and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE008 where COLUMN14 in(@SOID) and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN04 from SATABLE008 where COLUMN02=@id)
             --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	     set @RefLineId=(select COLUMN26 from SATABLE008 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
             --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
	     set @uom=(select isnull(COLUMN19,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
             set @Price=(select isnull(COLUMN12,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
	     --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			 set @lotno= (select COLUMN24 from SATABLE008 where COLUMN02=@id)
			 set @lotno=(case when @lotno='' then '0' when isnull(@lotno,'0')='0' then '0'  else @lotno end)
			 set @linelocation=(select cast(COLUMN28 as nvarchar(250)) from SATABLE008 where COLUMN02=@id)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
			 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
             set @lIID=(select COLUMN01 from SATABLE008 where COLUMN02=@id)

			 --Received Qty Updation
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefLineId,0)>0)
				begin
				set @COLUMN12=(select isnull(COLUMN12,0) from SATABLE006 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0)
				UPDATE SATABLE006 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0
				end
				else
				begin
				set @COLUMN12=(select isnull(COLUMN12,0) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item and isnull(COLUMN27,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) and isnull(COLUMNA13,0)=0)
				UPDATE SATABLE006 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and isnull(COLUMN27,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06) and isnull(COLUMNA13,0)=0)
				end
				UPDATE PUTABLE013 SET COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
			   
				set @TotalItemQty=((cast((select max(isnull(COLUMN08,0)) from SATABLE008 where  column02=@id and COLUMN04=@Item and isnull(COLUMNA13,0)=0) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN11,0)) from SATABLE008 where  column02=@id and COLUMN04=@Item and isnull(COLUMNA13,0)=0) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				update SATABLE008 set COLUMN08=@TotalItemQty, COLUMN09=0, COLUMN11=@RemainItemQty where column02=@id and isnull(COLUMNA13,0)=0

             --Inventory Updation
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
					 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				declare @TrackQty bit,@uomselection nvarchar(250)
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False')
				begin
				--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				if exists(SELECT COLUMN12 FROM FITABLE038 WHERE column03='RETURN ISSUE' and column04=@COLUMN04 and column05=@lIID and COLUMN06=@Item and isnull(COLUMN08,10000)=@uom and datalength(isnull(COLUMN12,0))>0 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
				begin
				exec usp_PUR_TP_InventoryLOT  @COLUMN04,@lIID,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@location,@lotno
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else
				begin
	                     --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
			     --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
				if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
				 --if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				declare @AvgPrice decimal(18,2)
				set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
				set @Qty_Cmtd=(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)))+ cast(@ItemQty as decimal(18,2));
				--set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))+cast(@ItemQty as decimal(18,2)))
				--if(  @Qty_Cmtd>=cast(@ItemQty as decimal(18,2)))
				--Begin
				--set @Qty_Cmtd = (cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				--end
				--else if(  @Qty_Cmtd<cast(@ItemQty as decimal(18,2)))
				--begin
				--				set @Qty_Cmtd = (cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				--end
				set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as decimal(18,2));
				--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@AvgPrice as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				end
				 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				end	
				else
				begin
				exec usp_PUR_TP_InventoryUOM  @COLUMN04,@SOID,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',null,null
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN05=@SOID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				end
				--set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
if  exists( SELECT 1 FROM PUTABLE013 WHERE COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02))
begin
delete from PUTABLE013  where COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
end
UPDATE SATABLE008 SET COLUMNA13=1 WHERE COLUMN14 in( select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02 )
END


else IF @Direction = 'Delete'
BEGIN

      set @COLUMN06=(select COLUMN06 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
      --EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
      set @COLUMN15=(select COLUMN15 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN05=(select COLUMN05 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN06=(select COLUMN06 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN04=(select COLUMN04 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA03=(select COLUMNA03 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA02=(select COLUMNA03 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
      set @SOID=(select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02)
	set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN02=@COLUMN02)
	set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
    if (@FRMID=1276)
	begin
	set @FRMType=('Item Issue')
	end
    else if (@FRMID=1354)
	begin
	set @FRMType=('Return Issue')
	end
	else
	begin
	set @FRMType=('JobOrder Issue')
	end 
	  --Inventory Asset table Updations
				delete from PUTABLE011 where COLUMN02=@SOID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN15 and COLUMNA03=@COLUMNA03
				delete from PUTABLE017 where COLUMN05=@SOID and COLUMN06=@FRMType and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN15 and COLUMNA03=@COLUMNA03
	  set @Initialrow =(1)
	   DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE008 where COLUMN14 in(@SOID) and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE008 where COLUMN14 in(@SOID) and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN04 from SATABLE008 where COLUMN02=@id)
             --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	     set @RefLineId=(select COLUMN26 from SATABLE008 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN09,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
             --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
	     set @uom=(select isnull(COLUMN19,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
             set @Price=(select isnull(COLUMN12,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
	     --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			 set @location =(select COLUMN23 from  SATABLE007 Where COLUMN01=@SOID );
			 set @linelocation=(select cast(COLUMN28 as nvarchar(250)) from SATABLE008 where COLUMN02=@id)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when (@location!= '' and @location is not null and @location!= '0' )  then @location when isnull(@location,'0')='0' then '0'  else '0' end)
			 set @lotno= (select COLUMN24 from SATABLE008 where COLUMN02=@id)
			 set @lotno=(case when @lotno='' then '0' when isnull(@lotno,'0')='0' then '0'  else @lotno end)
             set @lIID=(select COLUMN01 from SATABLE008 where COLUMN02=@id)
			 --EMPHCS1175	After creation of invoice , if user deletes item issue , system is not deducting inventory BYRAJ.Jr 16/9/2015
			  set @uom=(select isnull(COLUMN19,0) from SATABLE008 where COLUMN02=@id and COLUMN04=@Item)
			 --Received Qty Updation
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefLineId,0)>0)
				begin
				set @COLUMN12=(select isnull(COLUMN12,0) from SATABLE006 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0)
				UPDATE SATABLE006 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0
				end
				else
				begin
				set @COLUMN12=(select isnull(COLUMN12,0) from SATABLE006 where COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @COLUMN06) and COLUMN03=@Item and isnull(COLUMN27,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) and isnull(COLUMNA13,0)=0)
				UPDATE SATABLE006 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and isnull(COLUMN27,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@COLUMN06)) and isnull(COLUMNA13,0)=0
				end
				UPDATE PUTABLE013 SET COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@COLUMN06))
			   
				set @TotalItemQty=((cast((select max(isnull(COLUMN08,0)) from SATABLE008 where  column02=@id and COLUMN04=@Item and isnull(COLUMNA13,0)=0) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select (isnull(COLUMN11,0)) from SATABLE008 where  column02=@id and COLUMN04=@Item and isnull(COLUMNA13,0)=0) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=(cast(@RemainItemQty AS decimal(18,2))+ CAST(@ItemQty AS decimal(18,2)))
				update SATABLE008 set COLUMN08=@TotalItemQty, COLUMN09=0, COLUMN11=@RemainItemQty where column02=@id and isnull(COLUMNA13,0)=0

             --Inventory Updation
	     		     --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
			      --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@Item)
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
if(@TrackQty=1)
		begin
		if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False')
		begin
		--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				if exists(SELECT COLUMN12 FROM FITABLE038 WHERE column03='RETURN ISSUE' and column04=@COLUMN04 and column05=@lIID and COLUMN06=@Item and isnull(COLUMN08,10000)=@uom and datalength(isnull(COLUMN12,0))>0 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
				begin
				exec usp_PUR_TP_InventoryLOT  @COLUMN04,@lIID,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@location,@lotno
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else
				begin
			    --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
				if exists(select COLUMN03 from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
				 --if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)))+ cast(@ItemQty as decimal(18,2));
				set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
				--set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))+cast(@ItemQty as decimal(18,2)))
				--if(  @Qty_Cmtd>=cast(@ItemQty as decimal(18,2)))
				--Begin
				--set @Qty_Cmtd = (cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				--end
				--else if(  @Qty_Cmtd<cast(@ItemQty as decimal(18,2)))
				--begin
				--				set @Qty_Cmtd = (cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				--end
				set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as decimal(18,2));
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+((cast(@ItemQty as decimal(18,2)))*(cast(@AvgPrice as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
                set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN15 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				end
				end
				 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
				else
				begin
				exec usp_PUR_TP_InventoryUOM  @COLUMN04,@SOID,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',null,null
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN05=@SOID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				end
				--set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1
  UPDATE SATABLE007 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
UPDATE SATABLE008 SET COLUMNA13=1 WHERE COLUMN14 in( select COLUMN01 from SATABLE007 WHERE COLUMN02 = @COLUMN02 )
--EMPHCS894 rajasekhar reddy patakota 8/9/2015 For sales order after reversal of invoice and Item issue , the status is showing with internal id 21 rather than status in sales order info page
   declare @TQty decimal(18,2),@IFQty decimal(18,2),@IVQty decimal(18,2),@Result nvarchar(250)
   SET @TQty=(SELECT  sum(ISNULL(COLUMN07,0)) FROM SATABLE006 WHERE isnull(COLUMNA13,0)=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06))
   SET @IFQty=(SELECT sum(ISNULL(COLUMN12,0)) FROM SATABLE006 WHERE isnull(COLUMNA13,0)=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06))
   SET @IVQty=(SELECT sum(ISNULL(COLUMN13,0)) FROM SATABLE006 WHERE isnull(COLUMNA13,0)=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN06))
   IF(@IFQty=(0.00))
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=95)
   end
   else IF(@IVQty=(0.00))
   begin
   if(@TQty=@IFQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=98)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=96)
   end
   end
   ELSE 
   BEGIN
   if(@TQty=@IVQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=97)
   end
   end
   UPDATE SATABLE005 SET COLUMN16=@Result WHERE COLUMN02=@COLUMN06 and columna03=@columna03
   --UPDATE SATABLE005 SET COLUMN16=21 WHERE COLUMN02=@COLUMN06 and columna03=@columna03
END
end try
begin catch
	declare @tempSTR nvarchar(max)	
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_POR_TP_SATABLE007.txt',0
return 0
end catch

end













GO
