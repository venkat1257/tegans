USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_TP_InventoryAssetUpdate]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_TP_InventoryAssetUpdate](@HeaderId nvarchar(250)=null,@HeaderNo nvarchar(250)=null,@LineId nvarchar(250)=null,
@ItemId nvarchar(250)=null,@Uom nvarchar(250)=null,@Location nvarchar(250)=null,@Lot nvarchar(250)=null,@BillAvgPrice decimal(18,2),
@OPUnit nvarchar(250)=null,@Acowner nvarchar(250)=null)
as 
begin
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@Acowner AND COLUMN08= '1'  order by COLUMN01 desc
declare @type nvarchar(250)=null, @id nvarchar(250)=null, @lineitem nvarchar(250)=null,@lineQty decimal(18,2)=null, @tranlineid nvarchar(250)=null,
@MaxRownum INT,@Initialrow INT=1,@id1 nvarchar(250)=null,@MaxRownum1 INT,@Initialrow1 INT=1
 DECLARE cur2 CURSOR FOR SELECT COLUMN02 from PUTABLE017 where columna02=@OPUnit and COLUMN06!='BILL' and COLUMN06!='Item Receipt' and  columna03=@Acowner and columnd05=@ItemId and isnull(columna13,0)=0
		  OPEN cur2
		  FETCH NEXT FROM cur2 INTO @id
		  SET @MaxRownum = (SELECT COUNT(*) from PUTABLE017 where columna02=@OPUnit and COLUMN06!='BILL' and COLUMN06!='Item Receipt' and  columna03=@Acowner and columnd05=@ItemId and isnull(columna13,0)=0)
		     WHILE @Initialrow <= @MaxRownum
		     BEGIN 
				set @type=(select column06 from PUTABLE017 where column02=@id)
				set @tranlineid=(select columnd06 from PUTABLE017 where column02=@id)
				if(@type='INVOICE')
				begin
				set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
					 set @lineQty=(SELECT COLUMN10 from SATABLE010 where column01 in(@tranlineid)  and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update PUTABLE017 set COLUMN11=cast(cast((@lineQty*@BillAvgPrice) as decimal(18,2)) as decimal(18,2)) where COLUMN02=@id AND COLUMN04>=@FiscalYearStartDt
					 end
			 	end
				--else if(@type='BILL')
				--begin
				--set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
				--	 set @lineQty=(SELECT COLUMN09 from PUTABLE006 where column01 in(@tranlineid) and column04=@ItemId and column19=@Uom and iif(column27='',0,isnull(column27,0))=isnull(@Lot,0) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
				--	 update PUTABLE017 set COLUMN10=cast(cast((@lineQty*@BillAvgPrice) as decimal(18,2)) as decimal(18,2)) where COLUMN02=@id
			 --	end
				--else if(@type='Item Receipt')
				--begin
				--set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
				--	 set @lineQty=(SELECT COLUMN08 from PUTABLE004 where column01 in(@tranlineid) and column03=@ItemId and column17=@Uom and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
				--	 update PUTABLE017 set COLUMN10=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id
			 --	end
				else if(@type='Item Issue' OR @type='JobOrder Issue')
				begin
				set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
					 set @lineQty=(SELECT COLUMN09 from SATABLE008 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update PUTABLE017 set COLUMN11=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id AND COLUMN04>=@FiscalYearStartDt
					 end
			 	end
				--else if(@type='Inventory Adjustment')
				--begin
				--set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
				--declare @decamt decimal(18,2)
				--set @decamt=(select isnull(column11,0) from PUTABLE017 where column02=@id)
				--	 --if(@decamt<=0)
				--	 --begin
				--	 --set @lineQty=(SELECT COLUMN08 from FITABLE015 where column01 in(@tranlineid) and column03=@ItemId and column22=@Uom and iif(column23='',0,isnull(column23,0))=isnull(@Lot,0) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
				--	 --update PUTABLE017 set COLUMN10=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id
				--	 --end
				--	 --else begin
				--	 if(@decamt>0) begin
				--	 set @lineQty=(SELECT COLUMN16 from FITABLE015 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
				--	 if(@lineQty>0)
				--	 begin
				--	 update PUTABLE017 set COLUMN11=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id
				--	 end
				--	 end
			 --	end
				--else if(@type='Return Receipt')
				--begin
				--set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
				--	 set @lineQty=(SELECT COLUMN08 from PUTABLE004 where column01 in(@tranlineid) and COLUMN03=@ItemId and column17=@Uom and iif(column24='',0,isnull(column24,0))=isnull(@Lot,0) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
				--	 update PUTABLE017 set COLUMN10=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id
			 --	end
				else if(@type='Return Issue')
				begin
				set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
					 set @lineQty=(SELECT COLUMN09 from SATABLE008 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update PUTABLE017 set COLUMN11=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id AND COLUMN04>=@FiscalYearStartDt
					 end
			 	end
				else if(@type='Credit Memo')
				begin
				set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
					 set @lineQty=(SELECT COLUMN07 from SATABLE006 where column01 in(@tranlineid) and column03=@ItemId and column27=@Uom and (iif(column17='',0,isnull(column17,0)))=isnull(@Lot,0) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 update PUTABLE017 set COLUMN10=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id AND COLUMN04>=@FiscalYearStartDt
			 	end
				else if(@type='Debit Memo')
				begin
				set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
					 set @lineQty=(SELECT COLUMN07 from PUTABLE002 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update PUTABLE017 set COLUMN11=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id AND COLUMN04>=@FiscalYearStartDt
					 end
			 	end
				else if(@type='Stock Transfer')
				begin
				set @HeaderId=(select column05 from PUTABLE017 where column02=@id)
					 set @lineQty=(SELECT COLUMN06 from PRTABLE004 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update PUTABLE017 set COLUMN11=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id AND COLUMN04>=@FiscalYearStartDt
					 end
			 	end
				 FETCH NEXT FROM cur2 INTO @id
		         SET @Initialrow = @Initialrow + 1 
			END
		CLOSE cur2 
		deallocate cur2 
end






GO
