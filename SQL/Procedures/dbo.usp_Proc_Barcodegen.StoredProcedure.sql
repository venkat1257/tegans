USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Proc_Barcodegen]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_Proc_Barcodegen](@BillNO nvarchar(250)=null,
@OPUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null,@ItemID nvarchar(250)=null,@BarCode nvarchar(250)=null)
AS
BEGIN
if(isnull(@OPUnit,'')='') 
SELECT @OPUnit=COLUMNA02 from PUTABLE005 where COLUMN02=@BillNO and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
if exists(select COLUMN04 from CONTABLE031 where COLUMNA03=@AcOwner and  COLUMN03='Unique Barcode required during Inward' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
DECLARE @bcnt INT = 1,@billcnt INT = 0,@icnt INT = 1,@itemcnt INT = 0,@Date nvarchar(250)=GETDATE(),@RefID nvarchar(250)=null,@HID nvarchar(250)=null,@Item nvarchar(250)=null,@Qty decimal(18,2)=0,@Price decimal(18,2)=0,@UOM nvarchar(250)=null,@Vendor nvarchar(250)=null
select @billcnt=COUNT(*) from PUTABLE005 p inner join PUTABLE006 l on l.COLUMN13=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and l.COLUMNA02=p.COLUMNA02 and isnull(l.COLUMNA13,0)=0  where p.COLUMN02=@BillNO and p.COLUMNA03=@AcOwner and p.COLUMNA02=@OPUnit and isnull(p.COLUMNA13,0)=0
		WHILE @bcnt <= @billcnt
		BEGIN

		SELECT @RefID=q.RefID,@Item=q.Item,@Qty=q.qty,@Price=q.Price,@UOM=q.UOM,@Vendor=q.Vendor FROM(select l.COLUMN02 'RefID',l.COLUMN04 'Item',isnull(l.COLUMN09,0)'qty',l.COLUMN19 'UOM',l.COLUMN11 'Price',
		p.COLUMN05 'Vendor',row_number() over (order by cast(l.column02 as int))rno from PUTABLE005 p inner join PUTABLE006 l 
		on l.COLUMN13=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and l.COLUMNA02=p.COLUMNA02 and isnull(l.COLUMNA13,0)=0  
		where p.COLUMN02=@BillNO and p.COLUMNA03=@AcOwner and p.COLUMNA02=@OPUnit and isnull(p.COLUMNA13,0)=0)q where q.rno=@bcnt
	    IF((SELECT  COUNT(*) FROM MATABLE021 WHERE COLUMN03=1530 and COLUMN12=@RefID and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)<cast(@Qty as int))
		BEGIN
		SET @icnt=(1);SET @itemcnt=(@Qty);
		WHILE @icnt <= @itemcnt
		BEGIN
		IF((SELECT  COUNT(*) FROM MATABLE021 WHERE COLUMN03=1530 and COLUMN12=@RefID and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)<cast(@Qty as int))
		BEGIN
		
		IF exists(SELECT  * FROM MYTABLE002 WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		BEGIN
		declare @SEQVAL nvarchar(250)=null,@SUFF nvarchar(250)=null,@TRANS nvarchar(250)=null
			Select @SEQVAL=(SELECT  MAX(iif(isnull(COLUMN09,'')='',0,COLUMN09))+1 FROM MYTABLE002 WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
			Select @SUFF=(SELECT  MAX(isnull(COLUMN10,'')) FROM MYTABLE002 WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
			SET @TRANS=((SELECT MAX(COLUMN06) FROM MYTABLE002 WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)+CAST((REPLICATE('0',5-LEN(@SEQVAL))+CAST(@SEQVAL AS NVARCHAR(250))) AS NVARCHAR(250)))+CAST(@SUFF AS NVARCHAR(250))
		END
		ELSE
		BEGIN
		    if exists(Select COLUMN02 PO FROM MATABLE021  Where COLUMN03=1530 and  COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE 'BC%')
		    begin
			Select @SEQVAL=substring(COLUMN04,5,len(COLUMN04))+1 FROM MATABLE021  Where COLUMN03=1530 and  COLUMNA03=@AcOwner AND COLUMN02=(Select max(CAST(COLUMN02 as int)) PO FROM MATABLE021  Where COLUMN03=1530 and  COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE 'BC%') AND COLUMN04 LIKE 'BC%'  ORDER BY CAST(COLUMN02 as int) DESC
			set @SEQVAL=iif(CAST(@SEQVAL AS NVARCHAR(250))='','1',@SEQVAL)
			SET @TRANS=('BC'+CAST((REPLICATE('0',5-LEN(@SEQVAL))+CAST(@SEQVAL AS NVARCHAR(250))) AS NVARCHAR(250)))
			end
			else
			begin
			SET @TRANS=('BC'+CAST((REPLICATE('0',5-LEN(1))+CAST(1 AS NVARCHAR(250))) AS NVARCHAR(250)))
			END   
		END   
		EXEC [usp_MAS_BL_MATABLE021] 	
		1,1530, @TRANS ,@Item,@UOM,1,@Vendor,@Price,NULL,NULL,@RefID,
		NULL  , NULL,   @AcOwner,   NULL,	NULL,   @Date,  @Date,NULL  ,   NULL  ,  NULL  ,NULL,1,0,	
		NULL,  NULL,  NULL, NULL  ,  NULL  ,  NULL  ,	NULL,  NULL,  NULL, 	NULL,  NULL,   NULL,
		NULL  ,  NULL  ,  NULL  ,	NULL,  NULL,  NULL,	NULL,  NULL,  NULL,  NULL  ,'Insert','MATABLE021', NULL
		
		IF exists(SELECT  * FROM MYTABLE002 WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
        BEGIN
	    Select @SEQVAL=(SELECT  MAX(iif(isnull(COLUMN09,'')='',0,COLUMN09))+1 FROM MYTABLE002 WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
	    UPDATE MYTABLE002 set COLUMN09=@SEQVAL WHERE COLUMN03=1530 and COLUMN11='default' and (COLUMNA02=@OPUnit or COLUMNA02 is null) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
        END 
        END
		ELSE
		BEGIN
		set @icnt=(@itemcnt)
        END
		
		set @icnt=(@icnt+1)
		END
		END
		set @bcnt=(@bcnt+1)
		END
		set @HID=(SELECT COLUMN01 FROM PUTABLE005 WHERE COLUMN03=1273 and COLUMN02=@BillNO and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		SELECT p.COLUMN05,p.COLUMN07,p.COLUMN04 'BarCode',p.COLUMN06 'UOM' FROM MATABLE021 p
		LEFT JOIN PUTABLE006 l on l.COLUMN02=p.COLUMN12 and l.COLUMNA03=p.COLUMNA03 and ISNULL(l.COLUMNA13,0)=0 and l.COLUMN13=@HID
		 WHERE p.COLUMN03=1530 and (p.COLUMNA02=@OPUnit or p.COLUMNA02 is null) and p.COLUMNA03=@AcOwner and ISNULL(p.COLUMNA13,0)=0 and l.COLUMN13=@HID
		end
		else
		begin
		select isnull(l.COLUMN04,0)COLUMN04,isnull(l.COLUMN09,0)COLUMN09,l.COLUMN19 'UOM' from PUTABLE005 p inner join PUTABLE006 l on 
		l.COLUMN13=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and l.COLUMNA02=p.COLUMNA02 and isnull(l.COLUMNA13,0)=0  
		where p.COLUMN02=@BillNO and p.COLUMNA03=@AcOwner and p.COLUMNA02=@OPUnit and isnull(p.COLUMNA13,0)=0
		end
--else
--begin
--Select p.COLUMN04 'Name',  (select COLUMN04 from MATABLE002 where column02=p.COLUMN42) 'Color',  
--(select COLUMN04 from MATABLE002 where column02=p.COLUMN63) 'Units',  
--(select COLUMN04 from MATABLE004 where column02=p.COLUMN11) 'Group',  
--(select COLUMN04 from MATABLE005 where column02=p.COLUMN10) 'Brand/Company',@BarCode as 'BarCode' From MATABLE007 p  
--where p.COLUMN02 in(@ItemID) and isnull(p.COLUMN47,0)=0 and p.COLUMNA03=@AcOwner and  
--(p.COLUMNA02 in(@OPUnit) or p.COLUMNA02 is null)  and isnull(p.COLUMNA13,0)=0 
--END
END
GO
