USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PURR_TP_PUTABLE001]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PURR_TP_PUTABLE001]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,
	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN43   nvarchar(250)=null,
	@COLUMN44   nvarchar(250)=null,  @COLUMN45   nvarchar(250)=null,  @COLUMN46   nvarchar(250)=null,  
	--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
	@COLUMN47   nvarchar(250)=null,  @COLUMN48   nvarchar(250)=null,  @COLUMN49   nvarchar(250)=null,
	@COLUMN50   nvarchar(250)=null,  @COLUMN51   nvarchar(250)=null,  @COLUMN52   nvarchar(250)=null,  
	@COLUMN53   nvarchar(250)=null,  @COLUMN54   nvarchar(250)=null,  @COLUMN70   nvarchar(250)=null,
	@COLUMN71   nvarchar(250)=null,  @COLUMN72   nvarchar(250)=null,  @COLUMN73   nvarchar(250)=null,  
	@COLUMN74   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null, 
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)
AS
BEGIN
 begin try
  declare @tempSTR nvarchar(max)
IF @Direction = 'Insert'
BEGIN
if(@COLUMN11 like 'IV%')
begin
set @COLUMN33='INVOICE'
end
else if(@COLUMN11 like 'SO%')
begin
set @COLUMN33='SALESORDER'
end
if(@COLUMN04 like 'PQ%')
begin
set @COLUMN16=(select column04 from CONTABLE025 where COLUMN02=65)
end
else if(@COLUMN34 !='')
begin
update  putable001 set COLUMN16=(select column04 from CONTABLE025 where COLUMN02=66) where column02=@COLUMN34
end
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN29=(1003)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE001_SequenceNo
insert into PUTABLE001 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,  
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   COLUMN35,  COLUMN36,  --COLUMN37,  COLUMN38,  COLUMN39,  COLUMN40,  COLUMN41,  COLUMN42,  COLUMN43,  COLUMN44,  COLUMN45,
   --COLUMN46,  
   --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
   COLUMN48,  COLUMN49,  COLUMN50,  COLUMN70,  COLUMN71,  COLUMN72,  COLUMN73,  COLUMN74,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04,
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03,
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10,  COLUMN47
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36, -- @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   --@COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46, 
   --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
   @COLUMN48,  @COLUMN49,  @COLUMN50,  @COLUMN70,  @COLUMN71,  @COLUMN72,  @COLUMN73,  @COLUMN74,  
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10,  @COLUMN47
)  

declare @amntT decimal(18,2),@newID int,@tmpnewID int,@Num nvarchar(250),@Tax nvarchar(250), @TaxAG nvarchar(250), @TAXRATE nvarchar(250),@TaxColumn17 nvarchar(250),@ReverseCharged bit,@TOTALAMT DECIMAL(18,2)
set @newID=((select MAX(isnull(COLUMN02,999)) from PUTABLE016)+1)
set @Num=(select COLUMN01 from PUTABLE001 where COLUMN02=@COLUMN02);
set @ReverseCharged = (select isnull(COLUMN73,0) from PUTABLE001 where COLUMN01=@Num)
set @COLUMN50 = ISNULL(@COLUMN50,'22305')
if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN15,0)as decimal(18,2))-cast(ISNULL(@COLUMN32,0)as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN15,0))
end
set @amntT=(-cast(@TOTALAMT as decimal(18,2)))
if(@COLUMN50=22335)
begin

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Num,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN50,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN36,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=((select MAX(isnull(COLUMN02,999)) from PUTABLE018)+1)
insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN14,COLUMN13,COLUMNA02,COLUMNA03)
values(@newID,3000,@COLUMN06,@COLUMN04,'Debit Memo',@COLUMN05,@COLUMN09,-@amntT,@COLUMN06,@TOTALAMT,-@amntT,'Open',@column24,@COLUMNA03)
end
else
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Num,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN50,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN36,	@COLUMN14 = 0,       @COLUMN15 = @TOTALAMT,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into PUTABLE016	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN18,COLUMN12,COLUMN11,COLUMN14,COLUMN10,COLUMNA02,COLUMN19,COLUMNA03,COLUMN09)
values(@newID,2000,@COLUMN06,@Num,'Debit Memo',@COLUMN05,@COLUMN09,@amntT,@COLUMN06,@amntT,'Open',@column24,@Num,@COLUMNA03,@COLUMN04)
end
IF(CAST(ISNULL(@COLUMN74,0)AS DECIMAL(18,2))!=0)
BEGIN
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Num,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN50,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22408', @COLUMN11 = '1146',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN36,	@COLUMN14 = @COLUMN74,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @newID=((SELECT MAX(ISNULL(COLUMN02 ,1000)) FROM FITABLE036)+1)
insert into FITABLE036(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10, COLUMN11,	COLUMNA02, COLUMNA03)
values(@newID,'Debit Memo',@COLUMN06,@Num,@COLUMN05,0,ISNULL(@COLUMN74,0),@COLUMN04,1146,@COLUMN36,@COLUMNA02, @COLUMNA03)
END
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(@Num)
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from PUTABLE001
END 

ELSE IF @Direction = 'Update'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN29=(1003)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @PrevDBID NVARCHAR(250), @Location NVARCHAR(250), @linelocation NVARCHAR(250), @Project NVARCHAR(250)
set @PrevDBID=(select COLUMN04 from PUTABLE001 where COLUMN02=@COLUMN02)
set @Location=(select COLUMN47 from PUTABLE001 where COLUMN02=@COLUMN02)
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
set @Project= (select COLUMN36 from PUTABLE001 where COLUMN02=@COLUMN02)
set @Project= (case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
UPDATE PUTABLE001 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,     
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21, 
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,
   COLUMN27= @COLUMN27,   COLUMN28= @COLUMN28,   COLUMN29= @COLUMN29,   COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,
   COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,    COLUMN36=@COLUMN36,
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   --COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,    COLUMN39=@COLUMN39,    COLUMN40=@COLUMN40,    COLUMN41=@COLUMN41,
   --COLUMN42=@COLUMN42,    COLUMN43=@COLUMN43,    COLUMN44=@COLUMN44,    COLUMN45=@COLUMN45,    COLUMN46=@COLUMN46,    COLUMN47=@COLUMN47, 
   --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
   COLUMN48=@COLUMN48,    COLUMN49=@COLUMN49,    COLUMN50=@COLUMN50,    COLUMN70=@COLUMN70,    COLUMN71=@COLUMN71,  
   COLUMN72=@COLUMN72,    COLUMN73=@COLUMN73,    COLUMN74=@COLUMN74,
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08, 
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,
   --EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
   COLUMND04=@COLUMND04,                         COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
set @newID=((select MAX(isnull(COLUMN02,999)) from PUTABLE016)+1)
set @Num=(select COLUMN01 from PUTABLE001 where COLUMN02=@COLUMN02);
set @COLUMN50 = ISNULL(@COLUMN50,'22305')
DELETE FROM FITABLE064 WHERE COLUMN03 = 'Debit Memo' AND COLUMN05 = @Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if(@COLUMN50=22335)
begin
delete from PUTABLE018 where  column05=@PrevDBID and column06='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
end
else
begin
delete from PUTABLE016 where column05=@Num and column09=@PrevDBID and column06='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
end
delete from FITABLE026 where column05=@Num and column09=@PrevDBID and column04='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where column09=@PrevDBID and column03='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
--EMPHCS1564	CST not inserting while Debit memo BY RAJ.Jr
delete from FITABLE036 where column05=@Num and column09=@PrevDBID and column03='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
set @ReverseCharged = (select isnull(COLUMN73,0) from PUTABLE001 where COLUMN01=@Num)
if(@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1')
begin
set @TOTALAMT=(cast(ISNULL(@COLUMN15,0)as decimal(18,2))-cast(ISNULL(@COLUMN32,0)as decimal(18,2)))
end
else
begin
set @TOTALAMT=(ISNULL(@COLUMN15,0))
end
set @amntT=(-cast(@TOTALAMT as decimal(18,2)))
if(@COLUMN50=22335)
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Num,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN50,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22264', @COLUMN11 = '3000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN36,	@COLUMN14 = @TOTALAMT,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @newID=((select MAX(isnull(COLUMN02,999)) from PUTABLE018)+1)
insert into PUTABLE018	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN14,COLUMN13,COLUMNA02,COLUMNA03)
values(@newID,3000,@COLUMN06,@COLUMN04,'Debit Memo',@COLUMN05,@COLUMN09,-@amntT,@COLUMN06,@TOTALAMT,-@amntT,'Open',@column24,@COLUMNA03)
end
else
begin
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Num,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN50,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN36,	@COLUMN14 = 0,       @COLUMN15 = @TOTALAMT,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into PUTABLE016	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN18,COLUMN12,COLUMN11,COLUMN14,COLUMN10,COLUMNA02,COLUMN19,COLUMNA03,COLUMN09)
values(@newID,2000,@COLUMN06,@Num,'Debit Memo',@COLUMN05,@COLUMN09,@amntT,@COLUMN06,@amntT,'Open',@column24,@Num,@COLUMNA03,@COLUMN04)
end
IF(CAST(ISNULL(@COLUMN74,0)AS DECIMAL(18,2))!=0)
BEGIN

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Debit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @Num,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN06,   @COLUMN08 = @COLUMN50,  @COLUMN09 = @COLUMN05,    @COLUMN10 = '22408', @COLUMN11 = '1146',
		@COLUMN12 = @COLUMN09,   @COLUMN13 = @COLUMN36,	@COLUMN14 = @COLUMN74,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
SET @newID=((SELECT MAX(ISNULL(COLUMN02 ,1000)) FROM FITABLE036)+1)
insert into FITABLE036(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09, COLUMN10, COLUMN11,	COLUMNA02, COLUMNA03)
values(@newID,'Debit Memo',@COLUMN06,@Num,@COLUMN05,0,ISNULL(@COLUMN74,0),@COLUMN04,1146,@COLUMN36,@COLUMNA02, @COLUMNA03)
END
				
 --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
 	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
 declare @POID nvarchar(250),@Qty nvarchar(250),@Item nvarchar(250),@ItemQty nvarchar(250),@id nvarchar(250),@uom nvarchar(250),@TrackQty bit
 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
declare @RefID int,@Refreceipt int,@TQty decimal(18,2),@RRQty decimal(18,2),@PID int,@RefundQty decimal(18,2),@lot nvarchar(250),@RefLineId nvarchar(250)
DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
      set @POID=(select COLUMN01 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      --EMPHCS867 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in Purchase Order
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE002 where COLUMN19=@POID and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE002 where COLUMN19=@POID and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE002 where COLUMN02=@id and COLUMN19=@POID)
	     --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
             set @RefLineId=(select COLUMN34 from PUTABLE002 where COLUMN02=@id and COLUMN19=@POID)
             set @ItemQty=(select isnull(COLUMN07,0) from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@POID)
	     --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
            set @uom=(select isnull(COLUMN26,0) from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item)
	    --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
            set @lot=(select isnull(COLUMN17,0) from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item)
            set @linelocation=(select cast(COLUMN37 as nvarchar(250)) from PUTABLE002 where COLUMN02=@id)
			set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			set @Refreceipt=(select COLUMN33 from PUTABLE001 where COLUMN01=@Num and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03);
			set @RefID=(select COLUMN06 from SATABLE007 where COLUMN02=@Refreceipt and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03);
			set @PID=(select COLUMN01 from SATABLE005 where COLUMN02=@RefID  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03);
			--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			if(isnull(@RefLineId,0)>0)
			begin
			set @RefundQty=(select sum(isnull(COLUMN13,0)) from SATABLE006 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0)
			end
			else
			begin
			--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
			set @RefundQty=isnull((select sum(COLUMN13) from SATABLE006 where COLUMN19 in (@PID) and COLUMN03=@Item and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lot,0) and isnull(COLUMNA13,0)=0  and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03),0);
			end
			if(@COLUMN33!='' and isnull(@COLUMN33,0)!=0 and isnull(@COLUMN33,0)!='0')
			 begin
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			if(isnull(@RefLineId,0)>0)
			begin
			set @RefundQty=(select sum(isnull(COLUMN13,0)) from SATABLE006 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0)
			UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0
			UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN34=@RefLineId and isnull(COLUMNA13,0)=0
	        end
			else
			begin
			UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMN19 in (@PID) and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lot,0)  and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
			UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN19=@Num and COLUMN03=@Item and isnull(COLUMN26,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lot,0)  and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	        end
			end
			else			 
			 begin
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			 if(isnull(@RefLineId,0)>0)
			begin
			UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN34=@RefLineId and isnull(COLUMNA13,0)=0
	        end
			else
			begin
			--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
			 UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN19=@Num and COLUMN03=@Item  and isnull(COLUMN26,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lot,0) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
	        end

			 declare @Qty_On_Hand	decimal(18,2), @lotno nvarchar(250), @InvoiceID nvarchar(250),
			 @Price	decimal(18,2),@BillNo nvarchar(250),@BillId nvarchar(250),@CustID nvarchar(250),@chk bit,@upcno nvarchar(250) = null
			 set @chk=(select column48 from matable007 where column02=@Item)
			 set @lotno=(select COLUMN17 from PUTABLE002 where COLUMN02=@id)
			 set @upcno=(select COLUMN04 from PUTABLE002 where COLUMN02=@id)
			 set @Location=(@Location)
			 set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
			 if(@chk=1)
			 begin
			 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND isnull(COLUMN19,0)=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 set @Qty=cast(@ItemQty as decimal(18,2));
			 set @Qty_On_Hand=( @Qty_On_Hand + @Qty);
			 UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))+(cast(@ItemQty as decimal(18,2))*cast(@Price as decimal(18,2))))	 
			 WHERE COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project
			 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where   COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
			 if(cast(@Qty_On_Hand as decimal(18,2))=0)
			 begin
			 UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 else
			 begin
			 UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 set @CustID = (select COLUMN05 from PUTABLE001 where COLUMN01=@POID)
			 --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 delete from PUTABLE017 where COLUMN05=@POID and COLUMN06='Debit Memo' and COLUMN07=@CustID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			 end

			 end
		 --EMPHCS867 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in Purchase Order
			 UPDATE PUTABLE002 SET COLUMNA13=1 WHERE COLUMN02=@id
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
			 delete from FITABLE036 where COLUMN05 in(SELECT COLUMN01 from PUTABLE002 where COLUMN19=@POID ) and COLUMN03='Debit Memo' and COLUMN06=@CustID and COLUMN10 not in(1146) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
set @ReturnValue = 1

END
ELSE IF @Direction = 'Delete'
BEGIN
--EMPHCS867 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in Purchase Order
set @Initialrow=(1)
      set @POID=(select COLUMN01 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN24=(select COLUMN24 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN04=(select COLUMN04 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA03=(select COLUMNA03 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA02=(select COLUMNA02 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
      set @COLUMN48=(select COLUMN48 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN47=(select COLUMN47 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
	  set @Location=(@COLUMN47)
	  set @Project = (select COLUMN36 from PUTABLE001 where COLUMN02 = @COLUMN02)
          set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)
      set @Num=(select COLUMN01 from PUTABLE001 where COLUMN02=@COLUMN02);
	  set @COLUMN33=(select COLUMN33 from PUTABLE001 where COLUMN02=@COLUMN02);
	  declare @partyTyp nvarchar(250)
	  set @partyTyp=(select COLUMN50 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
	DELETE FROM FITABLE064 WHERE COLUMN03 = 'Debit Memo' AND COLUMN05 = @Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	  if(@partyTyp=22335)
	  begin
	  delete from PUTABLE018 where  column05=@COLUMN04 and column06='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	  end
	  else
	  begin
	  delete from PUTABLE016 where column05=@Num and column09=@COLUMN04 and column06='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	  end
	  delete from FITABLE026 where column05=@Num and column09=@COLUMN04 and column04='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
	  delete from FITABLE034 where column09=@COLUMN04 and column03='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
--EMPHCS1564	CST not inserting while Debit memo BY RAJ.Jr
	  delete from FITABLE036 where column05=@Num and column09=@COLUMN04 and column03='Debit Memo' and columnA02=@columnA02 and columnA03=@columnA03
DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE002 where COLUMN19=@POID and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE002 where COLUMN19=@POID and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE002 where COLUMN02=@id and COLUMN19=@POID)
	     --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
             set @RefLineId=(select COLUMN34 from PUTABLE002 where COLUMN02=@id and COLUMN19=@POID)
             set @ItemQty=(select isnull(COLUMN07,0) from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@POID)
	     --EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
	         set @uom=(select isnull(COLUMN26,0) from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item)
		 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
            set @lot=(select isnull(COLUMN17,0) from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item)
			set @upcno=(select COLUMN04 from PUTABLE002 where COLUMN02=@id and COLUMN03=@Item)
            set @linelocation=(select cast(COLUMN37 as nvarchar(250)) from PUTABLE002 where COLUMN02=@id)
			set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			set @Refreceipt=(select COLUMN33 from PUTABLE001 where COLUMN01=@Num  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03);
			set @RefID=(select COLUMN06 from SATABLE007 where COLUMN02=@Refreceipt  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03);
			set @PID=(select COLUMN01 from SATABLE005 where COLUMN02=@RefID  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03);
			--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			 if(isnull(@RefLineId,0)>0)
			begin
			set @RefundQty=isnull((select sum(COLUMN13) from SATABLE006 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0),0);
			end
			else
			begin
			--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
			set @RefundQty=isnull((select sum(COLUMN13) from SATABLE006 where COLUMN19 in (@PID) and COLUMN03=@Item and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lot,0)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03),0);
			end
			if(@COLUMN33!='' and isnull(@COLUMN33,0)!=0 and isnull(@COLUMN33,0)!='0')
			 begin	
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module		
			 if(isnull(@RefLineId,0)>0)
			begin
			UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0
			end
			else
			begin
			UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and COLUMN19 in (@PID) and isnull(COLUMN27,0)=isnull(@uom,0) and isnull(COLUMN17,0)=isnull(@lot,0) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
			end
			UPDATE SATABLE005 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=02) where COLUMN01=(@PID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
			end
			else			 
			 begin
			 set @chk=(select column48 from matable007 where column02=@Item)
			 set @lotno=(select COLUMN17 from PUTABLE002 where COLUMN02=@id)
			 set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
			 set @BillNo=(select COLUMN48 from PUTABLE001 where COLUMN01=@POID);
			 set @BillId=(select COLUMN01 from PUTABLE005 where COLUMN02=@BillNo);
			 set @RefundQty=isnull((select sum(isnull(COLUMN09,0)) from PUTABLE006 where COLUMN13 in (@BillId) and COLUMN03=@Item  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03),0);
			 if(@chk=1)
			 begin
			 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 set @Price=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND isnull(COLUMN19,0)=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno);
			 set @Qty=cast(@ItemQty as decimal(18,2));
			 set @Qty_On_Hand=( @Qty_On_Hand + @Qty);
			 UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))+(cast(@ItemQty as decimal(18,2))*cast(@Price as decimal(18,2))))	 
			 WHERE COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project
			 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where   COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2)));
			 if(cast(@Qty_On_Hand as decimal(18,2))=0)
			 begin
			 UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 else
			 begin
			 UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@Item AND COLUMN13=@COLUMNA02 AND COLUMN19=@uom AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@Project and COLUMN24=@upcno
			 end
			 set @CustID = (select COLUMN05 from PUTABLE001 where COLUMN01=@POID)
			 --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			 delete from PUTABLE017 where COLUMN05=@POID and COLUMN06='Debit Memo' and COLUMN07=@CustID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
			 end

			 end
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			 if(isnull(@RefLineId,0)>0)
			begin
			UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN34=@RefLineId and isnull(COLUMNA13,0)=0
			end
			else
			begin
			UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN02=@id and COLUMN19=@Num and COLUMN03=@Item  and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
			end
			UPDATE PUTABLE001 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=02) where COLUMN01=(@Num) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
			UPDATE PUTABLE002 SET COLUMNA13=1 WHERE COLUMN02=@id             
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
  declare @RIQty decimal(18,2),@Result nvarchar(250),@RIResult nvarchar(250)
   set @Refreceipt=(select COLUMN33 from PUTABLE001 where COLUMN01=@POID);
   set @RefID=(select COLUMN06 from SATABLE007 where COLUMN02=@Refreceipt);
   set @PID=(select COLUMN01 from SATABLE005 where COLUMN02=@RefID);
   SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM SATABLE006 WHERE COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
   set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in(@POID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
   set @RIQty=(SELECT sum(isnull(COLUMN12,0)) FROM SATABLE006 WHERE COLUMN19 in(@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
   if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
   begin
   if(@TQty=@RRQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=61)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=60)
   end
   UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@POID)
   IF(CAST(@RIQty AS DECIMAL(18,2))=(0.00))
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=95)
   set @RIResult=(select COLUMN04 from CONTABLE025 where COLUMN02=110)
   end
   else IF(CAST(@RRQty AS DECIMAL(18,2))=(0.00))
   begin
   if(@TQty=@RIQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=98)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=96)
   end
   end
   ELSE 
   BEGIN
   if(@TQty=@RRQty)
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
   set @RIResult=(select COLUMN04 from CONTABLE025 where COLUMN02=112)
   end
   else 
   begin
   set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=97)
   set @RIResult=(select COLUMN04 from CONTABLE025 where COLUMN02=111)
   end
   end
   UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=111) WHERE COLUMN02=@Refreceipt and columna03=@columna03
   UPDATE SATABLE005 SET COLUMN16=@Result WHERE COLUMN02=@RefID and columna03=@columna03
   END
UPDATE PUTABLE001 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
			 delete from FITABLE036 where COLUMN05 in(SELECT COLUMN01 from PUTABLE002 where COLUMN19=@POID ) and COLUMN03='Debit Memo' and COLUMN06=@CustID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
END

end try

begin catch
	SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PURR_TP_PUTABLE001.txt',0
return 0

end catch

end













GO
