USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Proc_TP_UserConfiguarations]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[usp_Proc_TP_UserConfiguarations]
(@AcOwner int,@Opunit int=null,@Name nvarchar(250)=null)
as
Begin
set @Opunit=(iif(@Opunit='',0 ,isnull(@Opunit,0)))

select top(1)isnull(t.COLUMN04,0) 'Inclusive',isnull(p.COLUMN04,0) 'Pricing',isnull(r.COLUMN04,0) 'RoundOff',r.COLUMN05 'RoundOffType',
isnull(c.COLUMN04,0) 'Cache',isnull(b.COLUMN04,0) 'BackOrder' from CONTABLE031 n
left join CONTABLE031 p on p.COLUMNA03=@AcOwner and  (isnull(p.COLUMNA02,0)=@Opunit or p.COLUMNA02 is null) and  p.COLUMN03='Customer Level Pricing' and 
isnull(p.column04,0)=1 and isnull(p.columna13,0)=0
left join CONTABLE031 r on r.COLUMNA03=@AcOwner and  (isnull(r.COLUMNA02,0)=@Opunit or r.COLUMNA02 is null) and  r.COLUMN03='Allow Round off' and 
isnull(r.column04,0)=1 and isnull(r.columna13,0)=0
left join CONTABLE031 c on c.COLUMNA03=@AcOwner and  (isnull(c.COLUMNA02,0)=@Opunit or c.COLUMNA02 is null) and  c.COLUMN03='Allow Redies Cache' and 
isnull(c.columna13,0)=0
left join CONTABLE031 b on b.COLUMNA03=@AcOwner and  (isnull(b.COLUMNA02,0)=@Opunit or b.COLUMNA02 is null) and  b.COLUMN03='Allow BackOrder' and 
isnull(b.column04,0)=1 and isnull(b.columna13,0)=0
left join CONTABLE031 t on t.COLUMNA03=@AcOwner and  (isnull(t.COLUMNA02,0)=@Opunit or t.COLUMNA02 is null) and  t.COLUMN03='Tax Inclusive' and 
isnull(t.column04,0)=1 and isnull(t.columna13,0)=0
where n.COLUMNA03=@AcOwner and  (isnull(n.COLUMNA02,0)=@Opunit or n.COLUMNA02 is null) and isnull(n.column04,0)=1 and isnull(n.columna13,0)=0
end

GO
