USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[AgingREPORTS]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROC [dbo].[AgingREPORTS]
(
	@ReportName  nvarchar(250)= null,
	@FrDate    nvarchar(250)= null,
	@ToDate      nvarchar(250)= null,
	@Customer      nvarchar(250)= null,
	@OPUnit nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@SalesRep nvarchar(250)= null,
	@Operating      nvarchar(250)= null,
	@ProjectName      nvarchar(250)= null,
	@DateF nvarchar(250)=null
)
AS
	BEGIN
	--EMPHCS1706 InventoyByUOM and AR Aging Repot Chages ading Location And Sales rep by GNANESHWAR ON 22/4/2016
declare @whereStr nvarchar(max)=null, 
	@SalesRepc nvarchar(250)= null,@Query1 nvarchar(max)=null
if @SalesRep!=''
begin
set @whereStr=' where SalesRepc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SalesRep+''') s)'
end
if(@Operating!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OPUID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OPUID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1  '
end
Create Table #ARAgingTable (
COLUMN07 varchar(500),
[CURRENT] decimal(18,2),
[1-30] decimal(18,2),
[31-60] decimal(18,2),
[61-90] decimal(18,2),
[91-180] decimal(18,2),
[181-360] decimal(18,2),
[361-520] decimal(18,2),
[520 and Over] decimal(18,2),
Credit decimal(18,2),RowIds int Identity(1,1),
SALESREP nvarchar(250),
SALESREPC nvarchar(250),
CITY nvarchar(250),
OPUID nvarchar(250)
)
declare @cntARAging int
declare @flgARAging int
declare @CustomerName varchar(500),
@CURRENT decimal(18,2),
@1_30 decimal(18,2),
@31_60 decimal(18,2),
@61_90 decimal(18,2),
@91_180 decimal(18,2),
@181_360 decimal(18,2),
@361_520 decimal(18,2),
@520 decimal(18,2),
@Credit decimal(18,2),@CreditBal decimal(18,2),
@CSALESREP nvarchar(250),
@CSALESREPC nvarchar(250),
@CITY nvarchar(250),
@OPUID nvarchar(250)

select * into #AGINGSUMMARY from (
select a.COLUMN07,sum(a.[CURRENT]) [CURRENT],sum(a.[1-30]) [1-30],sum(a.[31-60]) [31-60],sum(a.[61-90]) [61-90],
sum(a.[91-180]) [91-180],sum(a.[181-360]) [181-360],sum(a.[361-520]) [361-520],sum(a.[520 and Over]) [520 and Over],sum(a.[Credit/Advance]) [Credit/Advance] ,a.SalesRep,a.SalesRepc,a.CITY,a.OPUID
from (
SELECT  COLUMN07,CITY,(CASE WHEN  ((cast(isnull([days],0) as int)) =0 and cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)[CURRENT],
					(CASE WHEN ((cast(isnull([days],0) as int) <= 30 and (cast(isnull([days],0) as int))>=1) and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end) [1-30],
					(CASE WHEN  ((cast(isnull([days],0) as int)) <= 60 and cast(isnull([days],0) as int) >=31and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [31-60],
					(CASE WHEN ((cast(isnull([days],0) as int))<= 90 and cast(isnull([days],0) as int)>=61 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [61-90],
					(CASE WHEN ((cast(isnull([days],0) as int))<= 180 and(cast(isnull([days],0) as int))>= 91 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [91-180],
					(CASE WHEN  ((cast(isnull([days],0) as int))<= 360 and(cast(isnull([days],0) as int))>= 181 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [181-360],
					(CASE WHEN  ((cast(isnull([days],0) as int))<= 520 and (cast(isnull([days],0) as int))>=361 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [361-520],
					(CASE WHEN  ((cast([days] as int)) >=521 and  cast([type] as varchar(250))!='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [520 and Over],
					(CASE WHEN  ((cast([days] as int)) =0 and  cast([type] as varchar(250))='Credit' )
						then ( (sum(isnull(amount,0)) ))end)  [Credit/Advance] ,SalesRep,SalesRepc,OPUID
					FROM(SELECT (SATABLE002.COLUMN05) COLUMN07,DATEDIFF(DAY,(iif(min(SALES.column08) is null,max(SALES1.column08),min(SALES.column08))),GETDATE()) [days],
						 ( (cast(sum(isnull(PUTABLE018.COLUMN09,0))as decimal(18,2))-cast(sum(isnull(PUTABLE018.COLUMN11,0))as decimal(18,2))-sum(isnull(SATABLE012.COLUMN06,0))-sum(isnull(SATABLE012.COLUMN13,0))-sum(isnull(SATABLE012.COLUMN09,0))))Amount,''[type]
						 ,MATABLE010.COLUMN06 SalesRep, SATABLE002.COLUMN22 SalesRepc,
						 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= SATABLE002.COLUMN16) AS CITY,PUTABLE018.COLUMNA02 OPUID FROM PUTABLE018 
					left join SATABLE009 on SATABLE009.COLUMN04=PUTABLE018.COLUMN05 and  SATABLE009.COLUMNA03=@AcOwner AND SATABLE009.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE009.COLUMNA13,0)=0 AND SATABLE009.COLUMNA02=PUTABLE018.COLUMNA02 AND SATABLE009.COLUMN05=PUTABLE018.COLUMN07
					left join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					left join MATABLE010 on MATABLE010.COLUMN02=SATABLE002.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1
					--
					left join (select sum(isnull(SATABLE012.COLUMN06,0)) COLUMN06,COLUMN03,COLUMNA03,COLUMNA02,COLUMNA13,sum(isnull(COLUMN13,0))COLUMN13,sum(isnull(SATABLE012.COLUMN14,0))COLUMN14 ,sum(isnull(COLUMN09,0))COLUMN09 from SATABLE012  where  COLUMNA03=@AcOwner and COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE012.COLUMNA13,0)=0 group by COLUMN03,COLUMNA03,COLUMNA02,COLUMNA13) SATABLE012 
					on SATABLE012.COLUMN03=SATABLE009.COLUMN02 and  SATABLE012.COLUMNA03=@AcOwner AND SATABLE012.COLUMNA02=PUTABLE018.COLUMNA02 and isnull(SATABLE012.COLUMNA13,0)=0
					left join (select column08,COLUMN04,COLUMN05,COLUMNA02 from SATABLE009 where COLUMNA03=@AcOwner and isnull(SATABLE009.COLUMNA13,0)=0 and COLUMN13 not in('AMOUNT FULLY RECEIVED','FULLY RECEIVED')
					group by column08,COLUMN04,COLUMN05,COLUMNA02) SALES
					on SALES.COLUMN04=PUTABLE018.COLUMN05  AND SALES.COLUMNA02 =PUTABLE018.COLUMNA02  AND SALES.COLUMN05=PUTABLE018.COLUMN07
					left join (select column08,COLUMN04,COLUMN05,COLUMNA02 from SATABLE009 where COLUMNA03=@AcOwner and isnull(SATABLE009.COLUMNA13,0)=0 and COLUMN13  in('AMOUNT FULLY RECEIVED','FULLY RECEIVED')
					group by column08,COLUMN04,COLUMN05,COLUMNA02) SALES1
					on SALES1.COLUMN04=PUTABLE018.COLUMN05  AND SALES1.COLUMNA02 =PUTABLE018.COLUMNA02  AND SALES1.COLUMN05=PUTABLE018.COLUMN07
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  and isnull(SATABLE009.COLUMNA13,0)=0 and  PUTABLE018.COLUMN06 in ('Invoice','RECEIVE PAYMENT','JOURNAL ENTRY','PAYMENT VOUCHER')
					--
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner
					and isnull(PUTABLE018.COLUMNA13,0)=0 and isnull(SATABLE012.COLUMNA13,0)=0
					group by SATABLE002.COLUMN05, SATABLE009.COLUMN04,SATABLE002.COLUMN22,MATABLE010.COLUMN06,SATABLE002.COLUMN16,PUTABLE018.COLUMNA02
					-- Opening Balance
					union all
					SELECT (SATABLE002.COLUMN05) COLUMN07, DATEDIFF(DAY,PUTABLE018.COLUMN04,GETDATE()) [DAY],
						 SUM(ISNULL(PUTABLE018.COLUMN09,0))-SUM(ISNULL(PUTABLE018.COLUMN11,0)) as Amount,''[type],
						 '' SalesRep, SATABLE002.COLUMN22 SalesRepc,
						 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= SATABLE002.COLUMN16) AS CITY,PUTABLE018.COLUMNA02 OPUID FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)  
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  and PUTABLE018.COLUMN04 <=getdate()  and  PUTABLE018.COLUMN06 in ('Opening Balance')
and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner  and isnull(PUTABLE018.COLUMNA13,0)=0 
	group by SATABLE002.COLUMN05, SATABLE002.COLUMN22,SATABLE002.COLUMN16,PUTABLE018.COLUMNA02,PUTABLE018.COLUMN04
					--advance
					union all
					SELECT (SATABLE002.COLUMN05) COLUMN07,0 [days],
						 sum(cast(isnull(FITABLE026.COLUMN10,0) as decimal(18,2))) Amount,'Credit'[type]  ,null SalesRep , SATABLE002.COLUMN22 SalesRepc,
						 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= (SELECT COLUMN16 FROM SATABLE002 WHERE 
					(COLUMN02 = FITABLE026.COLUMN08)   and COLUMNA03=@AcOwner AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s))) AS CITY,FITABLE026.COLUMNA02 OPUID FROM FITABLE023 FITABLE026 
					inner join SATABLE002 on SATABLE002.COLUMN02=FITABLE026.COLUMN08 and  SATABLE002.COLUMNA03=@AcOwner and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					inner join fiTABLE024 on fiTABLE024.COLUMN09=FITABLE026.COLUMN01 and  fiTABLE024.COLUMNA03=@AcOwner and isnull(fiTABLE024.COLUMNA13,0)=0 and (fiTABLE024.COLUMN03 in (3000,5248) or fiTABLE024.COLUMN03 is null)
					--left join MATABLE010 on MATABLE010.COLUMN02=SATABLE002.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1
					--left join (select sum(isnull(p.column09,0)) column09,p.COLUMNA03,r.COLUMN08 COLUMN05,r.COLUMN02 from SATABLE012 p inner join FITABLE023 r on p.COLUMN15=r.COLUMN02 and 
					-- p.COLUMNA02=r.COLUMNA02 and  p.COLUMNA03=r.COLUMNA03 and isnull(r.COLUMNA13,0)=0
					-- where p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0
					--group by p.COLUMNA03,r.COLUMN08,r.COLUMN02) CreditMemo
					--on CreditMemo.COLUMN05=FITABLE026.COLUMN08 and CreditMemo.COLUMNA03=FITABLE026.COLUMNA03
					WHERE  isnull(FITABLE026.COLUMNA13,0)=0 
					and FITABLE026.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND FITABLE026.COLUMNA03=@AcOwner
					and isnull(FITABLE026.COLUMNA13,0)=0 
					group by SATABLE002.COLUMN05,SATABLE002.COLUMN22,SATABLE002.COLUMN16,FITABLE026.COLUMN08,FITABLE026.COLUMN02,FITABLE026.COLUMNA02
					union all
					SELECT (SATABLE002.COLUMN05) COLUMN07,0 [days],
						 max(isnull(CreditMemo.column15,0)) Amount,'Credit'[type]  ,MATABLE010.COLUMN06 SalesRep , SATABLE002.COLUMN22 SalesRepc,
						 (SELECT COLUMN10  FROM SATABLE003 WHERE COLUMN19='Customer' AND COLUMN02= (SELECT COLUMN16 FROM SATABLE002 WHERE 
(COLUMN02 = PUTABLE018.COLUMN07)   and COLUMNA03=@AcOwner AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s))) AS CITY,PUTABLE018.COLUMNA02 OPUID FROM PUTABLE018 
					inner join SATABLE002 on SATABLE002.COLUMN02=PUTABLE018.COLUMN07 and  SATABLE002.COLUMNA03=@AcOwner AND PUTABLE018.COLUMNA02 =PUTABLE018.COLUMNA02  and isnull(SATABLE002.COLUMNA13,0)=0 and (SATABLE002.COLUMN05!='' or SATABLE002.COLUMN05 is not null)
					left join MATABLE010 on MATABLE010.COLUMN02=SATABLE002.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1
					left join (select sum(isnull(column12,0))+sum(isnull(column32,0)) column15,COLUMN03,COLUMNA03,COLUMN05 from SATABLE005 where COLUMN03='1330' and COLUMNA03=@AcOwner AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(SATABLE005.COLUMNA13,0)=0
					group by COLUMN03,COLUMNA03,COLUMN05) CreditMemo
					on CreditMemo.COLUMN05=PUTABLE018.COLUMN07  and CreditMemo.COLUMNA03=PUTABLE018.COLUMNA03
					  left join (select sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN19,COLUMN03,COLUMNA03,COLUMN08 from FITABLE023 where COLUMN03='1386' and COLUMNA03=@AcOwner AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and isnull(FITABLE023.COLUMNA13,0)=0
					group by COLUMN03,COLUMNA03,COLUMN08) Advance
					on Advance.COLUMN08=PUTABLE018.COLUMN07  and Advance.COLUMNA03=PUTABLE018.COLUMNA03
					WHERE  isnull(PUTABLE018.COLUMNA13,0)=0  
					--
					and PUTABLE018.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND PUTABLE018.COLUMNA03=@AcOwner
					and isnull(PUTABLE018.COLUMNA13,0)=0 
					group by SATABLE002.COLUMN05,SATABLE002.COLUMN22,MATABLE010.COLUMN06,SATABLE002.COLUMN16,PUTABLE018.COLUMN07,PUTABLE018.COLUMNA02
					  )a group by column07,[days],[type],SalesRep,SalesRepc,CITY,OPUID
					having ( (cast(sum(isnull(a.Amount,0))as decimal(18,2))))!=0
					--
					) a group by a.COLUMN07 ,a.SalesRep,a.SalesRepc,a.CITY,OPUID) Query
--set @Query1='select * from #AGINGSUMMARY '+@whereStr
--exec (@Query1) 
Insert into #ARAgingTable select * FROM #AGINGSUMMARY
set @flgARAging=1
select @cntARAging=COUNT(1) from #ARAgingTable
while (@flgARAging <=@cntARAging)
begin
select @CustomerName=COLUMN07,@CURRENT=isnull([CURRENT],0),@1_30=isnull([1-30],0),@31_60=isnull([31-60],0),@61_90=isnull([61-90],0),
@91_180=isnull([91-180],0),@181_360=isnull([181-360],0),@361_520=isnull([361-520],0),@520=isnull([520 and Over],0),@Credit=isnull(Credit,0),@CSALESREP=SALESREP,@CSALESREPC=SALESREPC,@CITY=CITY,@OPUID=OPUID
from #ARAgingTable where RowIds=@flgARAging
set @CreditBal=(@Credit)
--seventh column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@520)<0
begin
set @CreditBal=@520-@CreditBal
update #ARAgingTable set [520 and Over]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [520 and Over]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@520
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--six column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@361_520)<0
begin
set @CreditBal=@361_520-@CreditBal
update #ARAgingTable set [361-520]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [361-520]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@361_520
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--fifth column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@181_360)<0
begin
set @CreditBal=@181_360-@CreditBal
update #ARAgingTable set [181-360]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [181-360]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@181_360
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--fourth column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@91_180)<0
begin
set @CreditBal=@91_180-@CreditBal
update #ARAgingTable set [91-180]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [91-180]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@91_180
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--third column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@61_90)<0
begin
set @CreditBal=@61_90-@CreditBal
update #ARAgingTable set [61-90]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [61-90]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@61_90
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--second column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if (@CreditBal-@31_60)<0
begin
set @CreditBal=@31_60-@CreditBal
update #ARAgingTable set [31-60]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [31-60]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@31_60
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--first column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if(@CreditBal-@1_30)<0
begin
set @CreditBal=@1_30-@CreditBal
update #ARAgingTable set [1-30]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [1-30]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@1_30
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
--current column
if cast(@CreditBal as decimal(18,2))!= 0.00
begin
if(@CreditBal-@CURRENT)<0
begin
set @CreditBal=@CURRENT-@CreditBal
update #ARAgingTable set [CURRENT]=@CreditBal where RowIds=@flgARAging
set @CreditBal=0
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
else
begin
update #ARAgingTable set [CURRENT]=0 where RowIds=@flgARAging
set @CreditBal=@CreditBal-@CURRENT
if(@CreditBal>0)begin set @CreditBal=-(@CreditBal) end
update #ARAgingTable set Credit=@CreditBal where RowIds=@flgARAging
end
end
set @flgARAging=@flgARAging+1
end
--select * from #ARAgingTable
truncate table #AGINGSUMMARY
insert into #AGINGSUMMARY(COLUMN07,[CURRENT],[1-30],[31-60],[61-90],[91-180],[181-360],[361-520],[520 and Over],[Credit/Advance],SALESREP,SALESREPC,CITY,OPUID) select * from (
select a.COLUMN07,sum(a.[CURRENT]) [CURRENT],sum(a.[1-30]) [1-30],sum(a.[31-60]) [31-60],sum(a.[61-90]) [61-90],sum(a.[91-180]) [91-180],
sum(a.[181-360]) [181-360],sum(a.[361-520]) [361-520],sum(a.[520 and Over]) [520 and Over],sum(a.[Credit]) [Credit/Advance] ,SALESREP,SALESREPC,CITY,OPUID
from #ARAgingTable a  group by a.COLUMN07,SALESREP,SALESREPC,CITY,OPUID having ((sum(isnull(a.Credit,0))+sum(isnull(a.[CURRENT],0))+sum(isnull(a.[1-30],0))+sum(isnull(a.[31-60],0))+sum(isnull(a.[61-90],0))+sum(isnull(a.[91-180],0))+sum(isnull(a.[181-360],0))+sum(isnull(a.[361-520],0))+sum(isnull(a.[520 and Over],0)))!=0)) b
--select * from #AGINGSUMMARY
set @Query1='select * from #AGINGSUMMARY '+@whereStr
exec (@Query1) 
END






GO
