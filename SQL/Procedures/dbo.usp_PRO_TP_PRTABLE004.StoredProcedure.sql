USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PRTABLE004]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PRO_TP_PRTABLE004]
(
        @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null, 
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
 	--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null, 
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
        @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null, @UOMData  XML=null,
	@tmpnewID1 int=null, @newID int=null
)
AS
BEGIN
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
set @COLUMN13=(select iif((@COLUMN13!='' and @COLUMN13>0),@COLUMN13,(select max(column02) from fitable037 where column07=1 and column08=1)))
begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE004_SequenceNo
declare @TrackQty  bit
  set @COLUMN14=(iif(isnull(@COLUMN14,'')='','0',@COLUMN14))
--set @COLUMN08= (SELECT MAX(COLUMN01) FROM PRTABLE003) 
  set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null and @COLUMN08!= '0' ) THEN (select COLUMN05 from PRTABLE003 
  where COLUMN01=@COLUMN08) else @COLUMNA02  END )
  --EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
   declare @tempSTR nvarchar(max), @date  nvarchar(250)
   set @tempSTR=('Target1:Line Hader:Stock Transfer Line Inserting Values are '+isnull(@COLUMN02,'')+','+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','+isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+' of  '+isnull(@TabelName,'')+' at ');
   -- exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
insert into PRTABLE004 
(
   --EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment	
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,  COLUMN13, COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,@COLUMN13,  @COLUMN06,  @COLUMN11,  @COLUMN12,  @COLUMN13, @COLUMN14,  
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
declare @Sourceou nvarchar(250)=null,@Destinationou nvarchar(250)=null,@Memo nvarchar(250)=null,@SAvgCost decimal(18,2)=null,@DAvgCost  decimal(18,2)=null,
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
@SQOH decimal(18,2)=null,@SAvlQty decimal(18,2)=null,@DQOH decimal(18,2)=null,@DAvlQty decimal(18,2)=null,@IntenalID int,
@Sourceproject nvarchar(250)=null,@Destinationproject nvarchar(250)=null
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
declare @InewID int,@ItmpnewID1 int ,@uomselection nvarchar(250)=null ,@stno nvarchar(250)=null,@SL int,@DL int
set @Memo= (SELECT COLUMN07 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @Sourceou= (SELECT COLUMN05 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @Destinationou= (SELECT COLUMN06 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @Sourceproject = (select COLUMN11 from PRTABLE003 Where COLUMN01=(@COLUMN08))
set @Sourceproject=(case when @Sourceproject='' then 0 when isnull(@Sourceproject,0)=0 then 0  else @Sourceproject end)
set @Destinationproject = (select COLUMN12 from PRTABLE003 Where COLUMN01=(@COLUMN08))
set @Destinationproject = (case when @Destinationproject='' then 0 when isnull(@Destinationproject,0)=0 then 0  else @Destinationproject end)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @SL= (SELECT COLUMN09 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @DL= (SELECT COLUMN10 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @SL=(iif(isnull(@SL,'')='','0',@SL))
set @DL=(iif(isnull(@DL,'')='','0',@DL))
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
declare @totprice decimal(18,2)
set @date= (SELECT COLUMN08 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @totprice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and isnull(COLUMN22,0)=@COLUMN14 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)*(cast( @COLUMN06 as decimal(18,2)))
set @SQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and isnull(COLUMN22,0)=@COLUMN14 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
set @SAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and isnull(COLUMN22,0)=@COLUMN14 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and isnull(COLUMN22,0)=@COLUMN14 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and isnull(COLUMN22,0)=@COLUMN14 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)


--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @IntenalID=(select max(column01) from PRTABLE004 where COLUMN02=@COLUMN02) 
 set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
  --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN03)
set @stno= (SELECT COLUMN04 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
   set @tempSTR=(null)
   set @tempSTR=('Target2:Line Hader:Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@SQOH as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@SAvlQty as nvarchar(250)),'')+',3.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',4.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',7.Units '+isnull(cast(@COLUMN13 as nvarchar(250)),'')+',8.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',9.Source Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+',10.Destination Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
   set @tempSTR=(null)
   set @tempSTR=('Target3:Line Hader:Stock Transfer Insert MultiUOM Values are 1.Multi UOM Selection '+isnull(cast(@uomselection as nvarchar(250)),'')+',2.MultiUOM Data '+CONVERT(nvarchar(max), @UOMData, 1)+',3.Stock Transfer NO '+isnull(cast(@stno as nvarchar(250)),'')+',4.Header internal id '+isnull(cast(@COLUMN08 as nvarchar(250)),'')+',5.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',6.Source Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
   exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
EXEC usp_PUR_TP_InsertUOM @UOMData,'StockTransfer',@stno,@COLUMN08,@COLUMN03,@Sourceou,@COLUMNA03
end
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
if(cast( @COLUMN06 as decimal(18,2))>0 and @TrackQty=1)
begin
if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False')
begin
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if exists(select column13  FROM FITABLE010 WHERE COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=@COLUMN14 and  COLUMN03=@COLUMN03 and COLUMN19=@COLUMN13 and  COLUMNA03=@COLUMNA03 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
begin
update FITABLE010 set COLUMN04=(cast( isnull(COLUMN04,0) as decimal(18,2))-cast( @COLUMN06 as decimal(18,2))), COLUMN08=(cast( @SAvlQty as decimal(18,2))-cast( @COLUMN06 as decimal(18,2)))
,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)as decimal(18,2))-((cast(@COLUMN06 as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject AND COLUMN24 = @COLUMNB01 )as decimal(18,2)))))
where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01
set @SQOH= (SELECT COLUMN04 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @SAvlQty= (SELECT COLUMN08 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
   set @tempSTR=(null)
   set @tempSTR=('Target4:Line Hader:After Updation Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@SQOH as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@SAvlQty as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if(cast(@SQOH as decimal(18,2))=0)
	begin
		UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01
	end
else
	begin
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)as decimal(18,2))/(cast( @SQOH as decimal(18,2))))as decimal(18,2))  WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject AND COLUMN24 =@COLUMNB01
	end
END
else
begin
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1
declare @poprice decimal(18,2)
set @poprice=(select top(1)isnull(COLUMN04,0)  FROM MATABLE024 WHERE COLUMN06='Purchase' and COLUMN07=@COLUMN03 and isnull(COLUMNA13,0)=0 and COLUMNA03=@COLUMNA03)
set @totprice=(cast(cast(@poprice as decimal(18,2))*cast(@COLUMN06 as decimal(18,2)) as decimal(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04 , COLUMN08, COLUMN13,COLUMN19,COLUMN12,COLUMN17,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
(
  @newID,@COLUMN03,-(cast(@COLUMN06 as decimal(18,2))) ,-(cast(@COLUMN06 as decimal(18,2))), @Sourceou,@COLUMN13,-(@totprice),@poprice,@SL,@COLUMN14,@Sourceou,@COLUMNA03,@Sourceproject,@COLUMNB01
)
end
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if exists(select column13  FROM FITABLE010 WHERE COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=@COLUMN14 and  COLUMN03=@COLUMN03 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
begin
--EMPHCS1381 rajasekhar reddy 18/11/2015 Adding Date column in Stock transfer
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
   set @tempSTR=(null)
   set @tempSTR=('Target5:Line Hader:Stock Transfer Destination Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
update FITABLE010 set COLUMN04=(cast( isnull(COLUMN04,0) as decimal(18,2))+cast( @COLUMN06 as decimal(18,2))), COLUMN08=(cast( @DQOH as decimal(18,2))+cast( @COLUMN06 as decimal(18,2)))
,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)as decimal(18,2))+((cast(@COLUMN06 as decimal(18,2)))*(cast((select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject AND COLUMN24 =@COLUMNB01)as decimal(18,2)))))
where COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject AND COLUMN24 =@COLUMNB01
set @DQOH= (SELECT COLUMN04 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @DAvlQty= (SELECT COLUMN08 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
   set @tempSTR=(null)
   set @tempSTR=('Target6:Line Hader:After Updation Stock Transfer Destination Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if(cast(@DQOH as decimal(18,2))=0)
	begin
		UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01
	end
else
	begin
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)as decimal(18,2))/(cast( @DQOH as decimal(18,2))))as decimal(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject AND COLUMN24 =@COLUMNB01
	end
end
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
declare @Avgprice decimal(18,2)
--EMPHCS1172	Inventory not Updating while stock transfering between operating units BY RAJ.Jr 18/9/2015
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @poprice=(select top(1)isnull(COLUMN04,0)  FROM MATABLE024 WHERE COLUMN06='Purchase' and COLUMN07=@COLUMN03 and isnull(COLUMNA13,0)=0 and COLUMNA03=@COLUMNA03)
IF(@poprice=0 or @poprice='0')
begin
SET @poprice= (select column17  FROM FITABLE010 WHERE COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=@COLUMN14 and  COLUMN03=@COLUMN03 and COLUMN19=@COLUMN13 and  COLUMNA03=@COLUMNA03 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
end
set @totprice=(cast(cast(@poprice as decimal(18,2))*cast(@COLUMN06 as decimal(18,2)) as decimal(18,2)))
set @Avgprice=(@totprice)/(cast( @COLUMN06 as decimal(18,2))) 
  set @tempSTR=(null)
   set @tempSTR=('Target7:Line Hader:Destination New Inventory Values are 1.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+',4.Units '+isnull(cast(@COLUMN13 as nvarchar(250)),'')+',5.Total Price '+isnull(cast(@totprice as nvarchar(250)),'')+',5.Average Price '+isnull(cast(@Avgprice as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04 , COLUMN08, COLUMN13,COLUMN19,COLUMN12,COLUMN17,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
(
  @newID,@COLUMN03,@COLUMN06 ,@COLUMN06, @Destinationou,@COLUMN13,@totprice,@poprice,@DL,@COLUMN14,@Destinationou,@COLUMNA03,@Destinationproject,@COLUMNB01 
)
end
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
end
else
begin
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 set @tempSTR=(null)
				set @tempSTR=('Target8:Line Hader:Updation of Inventory Values for Multiuom 1.Stock Transfer NO '+isnull(cast(@stno as nvarchar(250)),'')+',2.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',3.UOM '+isnull(cast(@COLUMN13 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				 exec usp_PUR_TP_InventoryUOM @stno,@COLUMN08,@COLUMN03,@COLUMN05,@COLUMNA02,@COLUMN13,@COLUMNA03,'Insert',null,@COLUMN14
end
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
set @SAvgCost= (SELECT avg(COLUMN09) FROM FITABLE038 WHERE COLUMN03='StockTransfer' and COLUMN04=@stno and COLUMN05=@COLUMN08 and COLUMN06=@COLUMN03 and COLUMNA02=@Sourceou and COLUMNA03=@COLUMNA03)
end
else
begin
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
end
set @SAvgCost= (cast(cast(@SAvgCost as decimal(18,2))*cast(@COLUMN06 as decimal(18,2)) as decimal(18,2)))
  set @tempSTR=(null)
   set @tempSTR=('Target9:Line Hader:Source Inventory Asset Values are 1.Average Cost '+isnull(cast(@SAvgCost as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
insert into PUTABLE017 
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06, COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMNA02,COLUMNA03, COLUMND05,COLUMND06)
values
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
( @InewID,'1000', @date ,@IntenalID,'Stock Transfer' ,@Memo,@SAvgCost,@SAvgCost,@Sourceou,@COLUMN07,@SL,@Sourceou,@COLUMNA03,@COLUMN03,@IntenalID)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
set @DAvgCost= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL AND isnull(COLUMN23,0)=@Destinationproject and COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='StockTransfer' and COLUMN04=@stno and COLUMN05=@COLUMN08 and COLUMN06=@COLUMN03 and COLUMNA02=@Sourceou and isnull(COLUMN21,0)=@SL and isnull(COLUMN22,0)=@COLUMN14 and COLUMNA03=@COLUMNA03 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01) AND COLUMN24 =@COLUMNB01)

end
else
begin
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @DAvgCost= (SELECT isnull(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
end
set @DAvgCost= (cast(cast( isnull(@DAvgCost,0) as decimal(18,2))*cast(isnull(@COLUMN06,0) as decimal(18,2)) as decimal(18,2)))
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
  set @tempSTR=(null)
   set @tempSTR=('Target10:Line Hader:Destination Inventory Asset Values are 1.Average Cost '+isnull(cast(@DAvgCost as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
if(@DAvgCost=0)
begin
set @DAvgCost= (cast( isnull(@SAvgCost,0) as decimal(18,2))*cast(isnull(@COLUMN06,0) as decimal(18,2)))
end
set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into PUTABLE017 
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06, COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMNA02,COLUMNA03, COLUMND05,COLUMND06)
values
( @InewID,'1000', @date ,@IntenalID,'Stock Transfer' ,@Memo,@SAvgCost,@SAvgCost,@Destinationou,@COLUMN07,@DL,@Destinationou,@COLUMNA03,@COLUMN03,@IntenalID)

end

set @ReturnValue = 1
END

 ELSE IF @Direction = 'Select'
 BEGIN
 select * from PRTABLE004
 END 
 
ELSE IF @Direction = 'Update'
BEGIN
  set @COLUMNA02=( CASE WHEN (@COLUMN08!= '' and @COLUMN08 is not null and @COLUMN08!= '0' ) THEN (select COLUMN05 from PRTABLE003 
  where COLUMN01=@COLUMN08) else @COLUMNA02  END )
  set @COLUMN14=(iif(isnull(@COLUMN14,'')='','0',@COLUMN14))
  --EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
   set @tempSTR=('Target1:Line Update:Stock Transfer Line Updating Values are '+isnull(@COLUMN02,'')+','+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','+isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_TP_PRTABLE004.txt',0
  if exists( SELECT 1 FROM PRTABLE004 WHERE COLUMN02=@COLUMN02 and COLUMN08=@COLUMN08)
  begin
UPDATE PRTABLE004 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   --EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN13,    COLUMN10=@COLUMN06,    COLUMN11=@COLUMN11,
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
end
else
begin
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE004_SequenceNo
insert into PRTABLE004 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,  COLUMN13, COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,@COLUMN13,  @COLUMN06,  @COLUMN11,  @COLUMN12,  @COLUMN13,@COLUMN14,   
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
end
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
 set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
set @Memo= (SELECT COLUMN07 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @Sourceou= (SELECT COLUMN05 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @Destinationou= (SELECT COLUMN06 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @Sourceproject = (select COLUMN11 from PRTABLE003 Where COLUMN01=(@COLUMN08))
set @Sourceproject=(case when @Sourceproject='' then 0 when isnull(@Sourceproject,0)=0 then 0  else @Sourceproject end)
set @Destinationproject = (select COLUMN12 from PRTABLE003 Where COLUMN01=(@COLUMN08))
set @Destinationproject = (case when @Destinationproject='' then 0 when isnull(@Destinationproject,0)=0 then 0  else @Destinationproject end)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @SL= (SELECT COLUMN09 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
set @DL= (SELECT COLUMN10 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
  set @SL=(iif(isnull(@SL,'')='','0',@SL))
  set @DL=(iif(isnull(@DL,'')='','0',@DL))
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @date= (SELECT COLUMN08 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @totprice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)*(cast( @COLUMN06 as decimal(18,2)))
set @SQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
set @SAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)

set @IntenalID=(select column01 from PRTABLE004 where COLUMN02=@COLUMN02) 
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @tempSTR=(null)
   set @tempSTR=('Target2:Line Update:Stock Transfer Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@SQOH as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@SAvlQty as nvarchar(250)),'')+',3.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',4.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+',5.Track Quantity OnHand '+CONVERT(nvarchar(max), @TrackQty, 1)+',6.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',7.Units '+isnull(cast(@COLUMN13 as nvarchar(250)),'')+',8.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',9.Source Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+',10.Destination Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
declare @Qty decimal(18,2)
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN03)
set @stno= (SELECT COLUMN04 FROM PRTABLE003 WHERE COLUMN01=(@COLUMN08))

if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
   set @tempSTR=(null)
   set @tempSTR=('Target3:Line Update:Stock Transfer MultiUOM Values are 1.Multi UOM Selection '+isnull(cast(@uomselection as nvarchar(250)),'')+',2.MultiUOM Data '+CONVERT(nvarchar(max), @UOMData, 1)+',3.Stock Transfer NO '+isnull(cast(@stno as nvarchar(250)),'')+',4.Header internal id '+isnull(cast(@COLUMN08 as nvarchar(250)),'')+',5.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',6.Source Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
   exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
if(@UOMData is null)
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@stno and COLUMN06=@COLUMN03 and COLUMN05=@COLUMN08 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
else
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'StockTransfer',@stno,@COLUMN08,@COLUMN03,@Sourceou,@COLUMNA03
end
end
if(cast( @COLUMN06 as decimal(18,2))>(cast( @DQOH as decimal(18,2))))
begin
set @Qty=cast( @COLUMN06 as decimal(18,2))-(cast( @DQOH as decimal(18,2)))
end
else
begin 
set @Qty=cast( @COLUMN06 as decimal(18,2))-(cast( @DQOH as decimal(18,2)))
end
if(cast( @COLUMN06 as decimal(18,2))>0 and @TrackQty=1)
begin
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
if(@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False')
begin
if exists(select column13  FROM FITABLE010 WHERE COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=@COLUMN14 and  COLUMN03=@COLUMN03 and COLUMN19=@COLUMN13 and  COLUMNA03=@COLUMNA03 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
begin
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
update FITABLE010 set COLUMN04=(cast( isnull(COLUMN04,0) as decimal(18,2))-cast( @COLUMN06 as decimal(18,2))), COLUMN08=(cast( @SAvlQty as decimal(18,2))-cast( @COLUMN06 as decimal(18,2)))
,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)as decimal(18,2))-((cast(@COLUMN06 as decimal(18,2)))*(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject AND COLUMN24 =@COLUMNB01)))
where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @SQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01) 
set @tempSTR=(null)
set @SAvlQty= (SELECT COLUMN08 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01) 
set @tempSTR=(null)
   set @tempSTR=('Target4:Line Update:After Updation Inventory Values are 1.Source Quantity Onhand '+isnull(cast(@SQOH as nvarchar(250)),'')+',2.Source Available Quantity '+isnull(cast(@SAvlQty as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if((cast(@SQOH as decimal(18,2)))=0)
	begin
		UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01
	end
else
	begin
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)as decimal(18,2))/(cast( @SQOH as decimal(18,2))))as decimal(18,2))  WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject AND COLUMN24 =@COLUMNB01
	end
end
else
begin
set @newID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE010) as int)+1
set @poprice=(select top(1)isnull(COLUMN04,0)  FROM MATABLE024 WHERE COLUMN06='Purchase' and COLUMN07=@COLUMN03 and isnull(COLUMNA13,0)=0 and COLUMNA03=@COLUMNA03)
set @totprice=(cast(cast(@poprice as decimal(18,2))*cast(@COLUMN06 as decimal(18,2)) as decimal(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04 , COLUMN08, COLUMN13,COLUMN19,COLUMN12,COLUMN17,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
(
  @newID,@COLUMN03,-(cast(@COLUMN06 as decimal(18,2))) ,-(cast(@COLUMN06 as decimal(18,2))), @Sourceou,@COLUMN13,-(@totprice),@poprice,@SL,@COLUMN14,@Sourceou,@COLUMNA03,@Sourceproject,@COLUMNB01
)
end
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if exists(select column13  FROM FITABLE010 WHERE COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=@COLUMN14 and  COLUMN03=@COLUMN03 and COLUMN19=@COLUMN13 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
begin
--EMPHCS1381 rajasekhar reddy 18/11/2015 Adding Date column in Stock transfer
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
 set @tempSTR=(null)
   set @tempSTR=('Target5:Line Update:Stock Transfer Destination Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
update FITABLE010 set COLUMN04=(cast( isnull(COLUMN04,0) as decimal(18,2))+cast( @COLUMN06 as decimal(18,2))), COLUMN08=(cast( @DQOH as decimal(18,2))+cast( @COLUMN06 as decimal(18,2)))
,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)as decimal(18,2))+((cast(@COLUMN06 as decimal(18,2)))*(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject AND COLUMN24 =@COLUMNB01)))
where COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01 
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
set @DQOH= (SELECT isnull(COLUMN04,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
set @DAvlQty= (SELECT isnull(COLUMN08,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
   set @tempSTR=(null)
   set @tempSTR=('Target6:Line Update:After Updation Stock Transfer Destination Inventory Values are 1.Destination Quantity Onhand '+isnull(cast(@DQOH as nvarchar(250)),'')+',2.Destination Available Quantity '+isnull(cast(@DAvlQty as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if((cast(@DQOH as decimal(18,2)))=0)
	begin
		UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01
	end
else
	begin
		UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)as decimal(18,2))/(cast( @DQOH as decimal(18,2))))as decimal(18,2)) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject AND COLUMN24 =@COLUMNB01
	end
end
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Inventory Adjustment
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--EMPHCS1172	Inventory not Updating while stock transfering between operating units BY RAJ.Jr 18/9/2015
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
--set @totprice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@Sourceou and COLUMN19=@COLUMN13)*(cast( @COLUMN06 as decimal(18,2)))
set @Avgprice=(@totprice)/(cast( @COLUMN06 as decimal(18,2)))
set @poprice=(select top(1)isnull(COLUMN04,0)  FROM MATABLE024 WHERE COLUMN06='Purchase' and COLUMN07=@COLUMN03 and isnull(COLUMNA13,0)=0 and COLUMNA03=@COLUMNA03)
IF(@poprice=0 or @poprice='0') 
begin
SET @poprice= (select column17  FROM FITABLE010 WHERE COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=@COLUMN14 and  COLUMN03=@COLUMN03 and COLUMN19=@COLUMN13 and  COLUMNA03=@COLUMNA03 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
end
set @totprice=(cast(cast(@poprice as decimal(18,2))*cast(@COLUMN06 as decimal(18,2)) as decimal(18,2)))
 set @tempSTR=(null)
   set @tempSTR=('Target7:Line Update:Destination New Inventory Values are 1.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+',4.Units '+isnull(cast(@COLUMN13 as nvarchar(250)),'')+',5.Total Price '+isnull(cast(@totprice as nvarchar(250)),'')+',5.Average Price '+isnull(cast(@Avgprice as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04 , COLUMN08, COLUMN13,COLUMN19,COLUMN12,COLUMN17,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN23,COLUMN24
)
values
(
  @newID,@COLUMN03,@COLUMN06 ,@COLUMN06, @Destinationou,@COLUMN13,@totprice,@poprice,@DL,@COLUMN14,@Destinationou,@COLUMNA03 ,@Destinationproject,@COLUMNB01
)
end
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
end

else
begin
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 set @tempSTR=(null)
				set @tempSTR=('Target8:Line Update:Updation of Inventory Values for Multiuom 1.Stock Transfer NO '+isnull(cast(@stno as nvarchar(250)),'')+',2.Item '+isnull(cast(@COLUMN03 as nvarchar(250)),'')+',3.UOM '+isnull(cast(@COLUMN13 as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
				exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
				 exec usp_PUR_TP_InventoryUOM @stno,@COLUMN08,@COLUMN03,@COLUMN05,@COLUMNA02,@COLUMN13,@COLUMNA03,'Insert',null,@COLUMN14
end
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
set @SAvgCost= (SELECT avg(COLUMN09) FROM FITABLE038 WHERE COLUMN03='StockTransfer' and COLUMN04=@stno and COLUMN05=@COLUMN08 and COLUMN06=@COLUMN03 and COLUMNA02=@Sourceou and COLUMNA03=@COLUMNA03)
end
else
begin
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMN19=@COLUMN13 and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01)
end
set @SAvgCost= (cast(cast(@SAvgCost as decimal(18,2))*cast(@COLUMN06 as decimal(18,2))as decimal(18,2)))
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 set @tempSTR=(null)
   set @tempSTR=('Target9:Line Update:Source Inventory Asset Values are 1.Average Cost '+isnull(cast(@SAvgCost as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Sourceou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into PUTABLE017 
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06, COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMNA02,COLUMNA03, COLUMND05,COLUMND06)
values
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
( @InewID,'1000', @date ,@IntenalID,'Stock Transfer' ,@Memo,@SAvgCost,@SAvgCost,@Sourceou,@COLUMN07,@SL,@Sourceou,@COLUMNA03,@COLUMN03,@IntenalID)
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
set @DAvgCost= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and isnull(COLUMN21,0)=@DL and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='StockTransfer' and COLUMN04=@stno and COLUMN05=@COLUMN08 and COLUMN06=@COLUMN03 and COLUMNA02=@Sourceou and isnull(COLUMN21,0)=@SL and COLUMNA03=@COLUMNA03 AND isnull(COLUMN23,0)=@Sourceproject and COLUMN24=@COLUMNB01) AND COLUMN24 =@COLUMNB01)
end
else
begin
set @DAvgCost= (SELECT isnull(COLUMN17,0) FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Destinationou and COLUMN19=@COLUMN13 and isnull(COLUMN21,0)=@DL and isnull(COLUMN22,0)=@COLUMN14 AND isnull(COLUMN23,0)=@Destinationproject and COLUMN24=@COLUMNB01)
end
set @DAvgCost= (cast(cast( isnull(@DAvgCost,0) as decimal(18,2))*cast(isnull(@COLUMN06,0) as decimal(18,2))as decimal(18,2)))
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
 set @tempSTR=(null)
   set @tempSTR=('Target10:Line Update:Destination Inventory Asset Values are 1.Average Cost '+isnull(cast(@DAvgCost as nvarchar(250)),'')+',2.Quantity '+isnull(cast(@COLUMN06 as nvarchar(250)),'')+',3.Operating Unit '+isnull(cast(@Destinationou as nvarchar(250)),'')+' of  '+isnull(@TabelName,'')+' at ');
    exec [CheckDirectory] @tempSTR,'usp_PRO_BL_StockTransfer.txt',0
if(@DAvgCost=0)
begin
set @DAvgCost= (cast( isnull(@SAvgCost,0) as decimal(18,2))*cast(isnull(@COLUMN06,0) as decimal(18,2)))
end
set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end
--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
insert into PUTABLE017 
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06, COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMNA02,COLUMNA03, COLUMND05,COLUMND06)
values
( @InewID,'1000', @date ,@IntenalID,'Stock Transfer' ,@Memo,@SAvgCost,@SAvgCost,@Destinationou,@COLUMN07,@DL,@Destinationou,@COLUMNA03,@COLUMN03,@IntenalID) 
end

set @ReturnValue = 1
END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE PRTABLE004 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END
end try
begin catch
--EMPHCS1410 rajasekhar reddy patakota 8/12/2015 Logs Creation For Stock transfer
--set @tempSTR=(null)
		set @tempSTR=(cast((SELECT  ERROR_NUMBER())as nvarchar(250)) +'ErrorNumber'+ cast((SELECT  ERROR_LINE())as nvarchar(250)) +'ErrorLine' + cast((SELECT  ERROR_MESSAGE())as nvarchar(250)) +'ErrorMessage' +' at ');
		exec [CheckDirectory] @tempSTR,'usp_PRO_TP_Exception_PRTABLE004.txt',0
return 0
end catch
end
























GO
