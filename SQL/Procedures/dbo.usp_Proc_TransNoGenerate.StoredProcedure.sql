USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Proc_TransNoGenerate]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_Proc_TransNoGenerate]
(
@Formid  nvarchar(250) = null,@TblID  nvarchar(250) = null,@OPUnit nvarchar(250) = null,@AcOwner nvarchar(250) = null,
@Type nvarchar(250) = null
)
AS
BEGIN
declare @id int ,@c int=0 ,@CurrentNolgth int=5,@Operchk nvarchar(250)= '',@FormName nvarchar(250)=null,@listTM nvarchar(250)=null,@Tranlist nvarchar(250)=null,
@Prefix nvarchar(250)=null,@Endno nvarchar(250)=null,@Sufix nvarchar(250)=null,@CurrentNo nvarchar(250)=null,@sNum nvarchar(250)=null,
@Override nvarchar(250)=null,@ColumnName nvarchar(250)=null,@fino nvarchar(250)=null,@SqlCmd nvarchar(max)=null,@TransNo nvarchar(max)='',
@TranColumn nvarchar(250)=null,@FormidColumn nvarchar(250)=null,@TblName nvarchar(250)=null,@TblCond nvarchar(250)='',@ab nvarchar(250)=null
SELECT @FormName=COLUMN04 FROM CONTABLE0010 Where COLUMN02 = @Formid
if (@OPUnit!='' and @OPUnit is not null)
begin
set @Operchk = (' and (COLUMNA02='+@OPUnit+' or COLUMNA02 is null ) ')
SELECT top(1)@listTM=COLUMN04,@Prefix=COLUMN06,@Sufix=iif(isnull(COLUMN10,'')='','',COLUMN10),@sNum=COLUMN07,@CurrentNo=COLUMN09,@Override=COLUMN12 FROM MYTABLE002 
Where COLUMN05 = @FormName and COLUMN11 ='default'  and COLUMNA03 = @AcOwner and COLUMNA02 = @OPUnit and isnull(COLUMNA13,0)=0
end
else
begin
set @Operchk = ('')
end
if(isnull(@listTM,'')='')
begin
SELECT top(1)@listTM=COLUMN04,@Prefix=COLUMN06,@Sufix=iif(isnull(COLUMN10,'')='','',COLUMN10),@sNum=COLUMN07,@CurrentNo=COLUMN09,@Override=COLUMN12 
FROM MYTABLE002 Where COLUMN05 = @FormName and COLUMN11 ='default'  and COLUMNA03 = @AcOwner and COLUMNA02 is null and isnull(COLUMNA13,0)=0
end
set @Override=(iif(isnull(@Override,'0')='0','False',@Override))
SELECT top(1)@Tranlist=COLUMN04 FROM MYTABLE002 Where COLUMN05 = @FormName and COLUMN11 ='default'  and COLUMNA03 = @AcOwner and isnull(COLUMNA13,0)=0
if (@Formid = '1275')
begin
set @fino = iif(@Prefix!='',@Prefix,'SO');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE005')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1330')
begin
set @fino = iif(@Prefix!='',@Prefix,'CM');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE005')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1353')
begin
set @fino = iif(@Prefix!='',@Prefix,'PR');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE005')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1283')
begin
set @fino = iif(@Prefix!='',@Prefix,'JO');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE005')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1527')
begin
set @fino = iif(@Prefix!='',@Prefix,'WC');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE044')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1528')
begin
set @fino = iif(@Prefix!='',@Prefix,'EP');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE045')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1414')
begin
set @fino = iif(@Prefix!='',@Prefix,'JV');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE031')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '13780' )
begin
set @fino = iif(@Prefix!='',@Prefix,'MS');
set @TranColumn=('COLUMN03')set @TblName=('PRTABLE002')--set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1378')
begin
set @fino = iif(@Prefix!='',@Prefix,'PJ');
set @TranColumn=('COLUMN04')set @TblName=('PRTABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1404')
begin
set @fino = iif(@Prefix!='',@Prefix,'DV');
set @TranColumn=('COLUMN15')set @TblName=('MATABLE019')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1615')
begin
set @fino = iif(@Prefix!='',@Prefix,'OBC');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE049')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1616')
begin
set @fino = iif(@Prefix!='',@Prefix,'OBV');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE049')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1625')
begin
set @fino = iif(@Prefix!='',@Prefix,'RS');
set @TranColumn=('COLUMN08')set @TblName=('MATABLE030')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1378')
begin
set @fino = iif(@Prefix!='',@Prefix,'MS');
set @TranColumn=('COLUMN03')set @TblName=('PRTABLE002')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1380')
begin
set @fino = iif(@Prefix!='',@Prefix,'PW');
set @TranColumn=('COLUMN04')set @TblName=('PRTABLE005')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1381')
begin
set @fino = iif(@Prefix!='',@Prefix,'RC');
set @TranColumn=('COLUMN04')set @TblName=('PRTABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1602')
begin
set @fino = iif(@Prefix!='',@Prefix,'RC');
set @TranColumn=('COLUMN04')set @TblName=('PRTABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1606')
begin
set @fino = iif(@Prefix!='',@Prefix,'PE');
set @TranColumn=('COLUMN04')set @TblName=('PRTABLE009')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1379')
begin
set @fino = iif(@Prefix!='',@Prefix,'ST');
set @TranColumn=('COLUMN04')set @TblName=('PRTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1375')
begin
set @fino = iif(@Prefix!='',@Prefix,'OP');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE013')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1376')
begin
set @fino = iif(@Prefix!='',@Prefix,'SQ');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE015')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1293')
begin
set @fino = iif(@Prefix!='',@Prefix,'EX');
set @TranColumn=('COLUMN17')set @TblName=('FITABLE012')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1349')
begin
set @fino = iif(@Prefix!='',@Prefix,'WT');
set @TranColumn=('COLUMN05')set @TblName=('FITABLE062')set @FormidColumn=('COLUMN13')
end
else if (@Formid = '1350')
begin
set @fino = iif(@Prefix!='',@Prefix,'DP');
set @TranColumn=('COLUMN05')set @TblName=('FITABLE062')set @FormidColumn=('COLUMN13')
end
else if (@Formid = '1351')
begin
set @fino = iif(@Prefix!='',@Prefix,'TR');
set @TranColumn=('COLUMN05')set @TblName=('FITABLE062')set @FormidColumn=('COLUMN13')
end
else if (@Formid = '1349')
begin
set @fino = iif(@Prefix!='',@Prefix,'WT');
set @TranColumn=('COLUMN05')set @TblName=('PUTABLE019')set @FormidColumn=('COLUMN13')
end
else if (@Formid = '1350')
begin
set @fino = iif(@Prefix!='',@Prefix,'DP');
set @TranColumn=('COLUMN05')set @TblName=('PUTABLE019')set @FormidColumn=('COLUMN13')
end
else if (@Formid = '1351')
begin
set @fino = iif(@Prefix!='',@Prefix,'TR');
set @TranColumn=('COLUMN05')set @TblName=('PUTABLE019')set @FormidColumn=('COLUMN13')
end
else if (@Formid = '1276')
begin
set @fino = iif(@Prefix!='',@Prefix,'II');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1354')
begin
set @fino = iif(@Prefix!='',@Prefix,'RI');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1284')
begin
set @fino = iif(@Prefix!='',@Prefix,'JI');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1277')
begin
if( @AcOwner = '56608')
begin
set @fino = iif(@Prefix!='',@Prefix,'');
end
else
begin
set @fino = iif(@Prefix!='',@Prefix,'IV');
end
set @TranColumn=('COLUMN04')set @TblName=('SATABLE009')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1532')
begin
set @fino = iif(@Prefix!='',@Prefix,'SR');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE009')set @FormidColumn=('COLUMN03')
END
else if (@Formid = '1604')
begin
set @fino = iif(@Prefix!='',@Prefix,'SER');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE009')set @FormidColumn=('COLUMN03')
END
else if (@Formid = '1603')
begin
set @fino = iif(@Prefix!='',@Prefix,'RR');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE005')set @FormidColumn=('COLUMN03')
END
else if (@Formid = '1287')
begin
set @fino = iif(@Prefix!='',@Prefix,'JB');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE009')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1278')
begin
set @fino = iif(@Prefix!='',@Prefix,'CP');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE011')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1288')
begin
set @fino = iif(@Prefix!='',@Prefix,'JP');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE011')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1328')
begin
set @fino = iif(@Prefix!='',@Prefix,'SR');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1355')
begin
set @fino = iif(@Prefix!='',@Prefix,'RF');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1374')
begin
set @fino = iif(@Prefix!='',@Prefix,'PQ');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1251')
begin
set @fino = iif(@Prefix!='',@Prefix,'PO');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1272')
begin
set @fino = iif(@Prefix!='',@Prefix,'IR');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1329')
begin
set @fino = iif(@Prefix!='',@Prefix,'RR');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1286')
begin
set @fino = iif(@Prefix!='',@Prefix,'JR');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1273')
begin
set @fino = iif(@Prefix!='',@Prefix,'BL');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE005')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1274')
begin
set @fino = iif(@Prefix!='',@Prefix,'PB');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE014')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1252')
begin
set @fino = iif(@Prefix!='',@Prefix,'VR');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1265')
begin
set @fino = iif(@Prefix!='',@Prefix,'CUST');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE002')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1260')
begin
set @fino = iif(@Prefix!='',@Prefix,'EMP');
set @TranColumn=('COLUMN04')set @TblName=('MATABLE010')set @FormidColumn=('COLUMN03')set @TblCond=(' AND COLUMN20=0 ')
end
else if (@Formid = '1260')
begin
set @fino = iif(@Prefix!='',@Prefix,'LM');
set @TranColumn=('COLUMN04')set @TblName=('MATABLE010')set @FormidColumn=('COLUMN03')set @TblCond=(' AND COLUMN20=1 ')
end
else if (@Formid = '1261')
begin
set @fino = iif(@Prefix!='',@Prefix,'IM');
set @TranColumn=('COLUMN41')set @TblName=('MATABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1285')
begin
set @fino = iif(@Prefix!='',@Prefix,'MI');
set @TranColumn=('COLUMN41')set @TblName=('MATABLE007')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1357')
begin
set @fino = iif(@Prefix!='',@Prefix,'IA');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE014')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1634')
begin
set @fino = iif(@Prefix!='',@Prefix,'IC');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE014')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1358')
begin
set @fino = iif(@Prefix!='',@Prefix,'OS');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE016')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1362')
begin
set @fino = iif(@Prefix!='',@Prefix,'LO');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE019')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1363')
begin
set @fino = iif(@Prefix!='',@Prefix,'IC');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE020')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1525')
begin
set @fino = iif(@Prefix!='',@Prefix,'PV');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE020')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1386')
begin
set @fino = iif(@Prefix!='',@Prefix,'RP');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE023')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1526')
begin
set @fino = iif(@Prefix!='',@Prefix,'RV');
set @TranColumn=('COLUMN04')set @TblName=('FITABLE023')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1366')
begin
set @fino = iif(@Prefix!='',@Prefix,'SA');
set @TranColumn=('COLUMN04')set @TblName=('SETABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1367')
begin
set @fino = iif(@Prefix!='',@Prefix,'TM');
set @TranColumn=('COLUMN04')set @TblName=('SETABLE002')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1368')
begin
set @fino = iif(@Prefix!='',@Prefix,'BB');
set @TranColumn=('COLUMN04')set @TblName=('SETABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1369')
begin
set @fino = iif(@Prefix!='',@Prefix,'MC');
set @TranColumn=('COLUMN04')set @TblName=('SETABLE006')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1388')
begin
set @fino = iif(@Prefix!='',@Prefix,'CT');
set @TranColumn=('COLUMN04')set @TblName=('MATABLE018')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1629')
begin
set @fino = iif(@Prefix!='',@Prefix,'IM');
set @TranColumn=('COLUMN04')set @TblName=('SETABLE019')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1656')
begin
set @fino = iif(@Prefix!='',@Prefix,'SEO');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE001')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1657')
begin
set @fino = iif(@Prefix!='',@Prefix,'SIR');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1659')
begin
set @fino = iif(@Prefix!='',@Prefix,'MR');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1587')
begin
set @fino = iif(@Prefix!='',@Prefix,'JRI');
set @TranColumn=('COLUMN04')set @TblName=('PUTABLE003')set @FormidColumn=('COLUMN03')
end
else if (@Formid = '1669')
begin
set @fino = iif(@Prefix!='',@Prefix,'MRP');
set @TranColumn=('COLUMN04')set @TblName=('SATABLE022')set @FormidColumn=('COLUMN03')
end

set @Sufix=(iif(isnull(@Sufix,'')='','',@Sufix))
if (@listTM != '' and @listTM is not null)
begin
if (@CurrentNo = '' or @CurrentNo is null) set @CurrentNo = ('00001')
else set @CurrentNo=(Cast(@CurrentNo as int) + 1) 
set @CurrentNolgth = (iif(LEN(Cast(@CurrentNo as int))>5,LEN(@CurrentNo),5))
set @TransNo = (@fino + (CAST((REPLICATE('0',@CurrentNolgth-LEN(@CurrentNo))+CAST(@CurrentNo AS NVARCHAR(250))) AS NVARCHAR(250))) + @Sufix)
select @TransNo PO,@Override 'Override',CONCAT(@TblName,@TranColumn) 'TransColumn','1'
end
else
begin
DECLARE @Query NVARCHAR(max) = '',@ParmDefinition nvarchar(500),@hasRow NVARCHAR(250) = ''
SELECT @Query = 'select top(1)@retvalOUT='+@TranColumn+' from '+@TblName+' WHERE '+@TranColumn+' like '''+@fino+'%'' and ' + @FormidColumn + '=' + @Formid + ' AND isnull(COLUMNA13,0)=0 and COLUMNA03=' + @AcOwner + ' '+@Operchk+' order by cast(COLUMN02 as int) desc'
--EXEC (@Query)
SET @ParmDefinition = N'@retvalOUT NVARCHAR(250) OUTPUT';
EXEC sp_executesql @Query, @ParmDefinition, @retvalOUT=@hasRow OUTPUT;
--SELECT @hasRow =@@ROWCOUNT 
IF @hasRow=''
begin
set @CurrentNo = ('00001')
set @TransNo = (@fino + (CAST((REPLICATE('0',5-LEN(@CurrentNo))+CAST(@CurrentNo AS NVARCHAR(250))) AS NVARCHAR(250))) + @Sufix)
select @TransNo PO,@Override 'Override',CONCAT(@TblName,@TranColumn) 'TransColumn','1'
end
else
begin
set @TransNo=('replace(replace(iif(isnull('+@TranColumn+','''')='''',''00001'','+@TranColumn+'),'''+@fino+''',''''),'''+@Sufix+''','''')')
set @TransNo=('cast(iif(isnumeric('+@TransNo+')=0,0,'+@TransNo+') as int)+1')
set @TransNo=('CAST((REPLICATE(''0'',5-LEN('+@TransNo+'))+CAST('+@TransNo+' AS NVARCHAR(250))) AS NVARCHAR(250))')
set @SqlCmd=('Select (CONCAT('''+@fino+''','+@TransNo+','''+@Sufix+''')) PO,'''+@Override+''' Override,CONCAT('''+@TblName+''','''+@TranColumn+''') TransColumn,0 FROM '+@TblName+'  Where  ' + @FormidColumn + '=' + @Formid + ' '+@TblCond+' and COLUMNA03=' + @AcOwner + ' '+@Operchk+' AND COLUMN02=(Select max(COLUMN02) PO FROM '+@TblName+'  Where  ' + @FormidColumn + '=' + @Formid + ' '+@TblCond+' and COLUMNA03=' + @AcOwner + ' '+@Operchk+' AND isnull(COLUMNA13,0)=0   AND '+@TranColumn+' LIKE ''' + @fino + '%'') AND '+@TranColumn+' LIKE ''' + @fino + '%''  ORDER BY COLUMN02 DESC')
--set @SqlCmd=('Select substring('+@TranColumn+',cast(len('''+@fino+''')as int)+1,len('+@TranColumn+')) PO FROM '+@TblName+'  Where  ' + @FormidColumn + '=' + @Formid + ' '+@TblCond+' and COLUMNA03=' + @AcOwner + '  AND COLUMN02=(Select max(COLUMN02) PO FROM '+@TblName+'  Where  ' + @FormidColumn + '=' + @Formid + ' '+@TblCond+' and COLUMNA03=' + @AcOwner + ' AND isnull(COLUMNA13,0)=0   AND '+@TranColumn+' LIKE ''' + @fino + '%'') AND '+@TranColumn+' LIKE ''' + @fino + '%''  ORDER BY COLUMN02 DESC')
EXEC (@SqlCmd)
end
end
END 





GO
