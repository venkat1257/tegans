USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[VendorPaymentGLImpact]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VendorPaymentGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,P14.column10 Memo,p19.COLUMN10 Credit,0 Debit  from PUTABLE014 P14
left outer join putable019 p19 on p19.COLUMN05= P14.COLUMN04 and p19.COLUMN07= P14.COLUMN05 and p19.COLUMNA03= P14.COLUMNA03 and p19.COLUMNA02= P14.COLUMNA02 and p19.COLUMN06='BILL PAYMENT' and isnull(p19.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p19.COLUMN03
where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner and f1.COLUMNA03 = P14.COLUMNA03 and isnull(P14.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,P14.column10 Memo,F52.COLUMN11 Credit, 0 Debit  from PUTABLE014 P14
left outer join FITABLE052 F52 on F52.COLUMN05= P14.COLUMN04  and F52.COLUMNA03= P14.COLUMNA03 and F52.COLUMNA02= P14.COLUMNA02 and F52.COLUMN06='BILL PAYMENT' and isnull(F52.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F52.COLUMN03
where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner and f1.COLUMNA03 = P14.COLUMNA03 and isnull(P14.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,P14.column10 Memo,0 Credit,P16.COLUMN13 Debit  from PUTABLE014 P14
left outer join PUTABLE016 P16 on P16.COLUMN09= P14.COLUMN04  and P16.COLUMNA03= P14.COLUMNA03 and P16.COLUMNA02= P14.COLUMNA02 and P16.COLUMN06='BILL PAYMENT' and isnull(P16.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = P16.COLUMN03
where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner and f1.COLUMNA03 = P14.COLUMNA03 and isnull(P14.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,P14.column10 Memo,0 Credit,f27.COLUMN10 Debit  from PUTABLE014 P14
left outer join FITABLE027 f27 on f27.COLUMN09= P14.COLUMN04 and f27.COLUMNA03= P14.COLUMNA03 and f27.COLUMNA02= P14.COLUMNA02 and f27.COLUMN04='COMMISSION PAYMENT' and isnull(f27.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f27.COLUMN08
where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner and f1.COLUMNA03 = P14.COLUMNA03 and isnull(P14.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,P14.column10 Memo,sum(f34.COLUMN08) Credit,0Debit  from PUTABLE014 P14
left outer join FITABLE034 f34 on f34.COLUMN09= P14.COLUMN04 and f34.COLUMNA03= P14.COLUMNA03 and f34.COLUMNA02= P14.COLUMNA02 and f34.COLUMN03='BILL PAYMENT' and isnull(f34.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10
where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner and f1.COLUMNA03 = P14.COLUMNA03 and isnull(P14.COLUMNA13,0)=0
group by f1.COLUMN04,P14.column10
union all
select f1.COLUMN04 Account,P14.column10 Memo,sum(f26.COLUMN12) Credit,0 Debit  from PUTABLE014 P14
left outer join FITABLE026 f26 on f26.COLUMN09= P14.COLUMN04  AND f26.COLUMN06= P14.COLUMN05 and f26.COLUMNA03= P14.COLUMNA03 and f26.COLUMNA02= P14.COLUMNA02 and f26.COLUMN04='BILL PAYMENT' and isnull(f26.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f26.COLUMN08
where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner and f1.COLUMNA03 = P14.COLUMNA03 and isnull(P14.COLUMNA13,0)=0
group by f1.COLUMN04,P14.column10
--UNION ALL
--select 'Advance Paid' Account,P14.column10 Memo,P19.COLUMN16 Credit,0 Debit  from PUTABLE014 P14
--left outer join PUTABLE015 p19 on p19.COLUMN08= P14.COLUMN01 and p19.COLUMNA03= P14.COLUMNA03 and p19.COLUMNA02= P14.COLUMNA02 
--and isnull(p19.COLUMNA13,0)=0
--where P14.COLUMN02=@ide and P14.COLUMNA03=@AcOwner
--and isnull(P14.COLUMNA13,0)=0
)
select [Account],[Memo],[Credit],[Debit] from MyTable
group by [Account],[Memo],[Credit],[Debit]
end


GO
