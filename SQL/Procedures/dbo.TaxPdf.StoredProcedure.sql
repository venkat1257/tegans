USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[TaxPdf]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TaxPdf]
(
@FromDate nvarchar(250)=null,
@ToDate nvarchar(250)=null,
@OperatingUnit nvarchar(250)=null,
@OPUnit nvarchar(250)=null,
@AcOwner nvarchar(250)=null,
@DateF nvarchar(250)=null,
@FormID nvarchar(250)=null
)
AS 
BEGIN
if(@FormID=1)
BEGIN
SELECT sum((ISNULL(s.COLUMN22,0) + ISNULL(s.COLUMN24,0) + ISNULL(s.COLUMN72,0)-ISNULL(s.COLUMN25,0))) STAmt,sum(ISNULL(s.COLUMN56,0)) Discount,sum(ISNULL(s.COLUMN68,0)) Roundoff,
SUM(isnull(s.COLUMN20,0)) Total,SUM(isnull(s.COLUMN71,0)) ShippingAmt from 
SATABLE009 s 
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner and s.COLUMN03 in(1277,1532) 
AND s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND s.column08 between @FromDate and @ToDate
END
ELSE
if(@FormID=2)
BEGIN
SELECT sum(ISNULL(s.COLUMN22,0)+ISNULL(s.COLUMN63,0)+ISNULL(s.COLUMN24,0)-ISNULL(s.COLUMN41,0)) STAmt,0 Discount,sum(ISNULL(s.COLUMN59,0)) Roundoff,
SUM(isnull(s.COLUMN14,0)) Total,SUM(isnull(s.COLUMN62,0)) ShippingAmt from 
PUTABLE005 s 
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner
AND s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND s.column08 between @FromDate and @ToDate
END
END
 

GO
