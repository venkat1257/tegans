USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTREPORTSGSTR1]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_GSTREPORTSGSTR1](@FromDate NVARCHAR(250),
	@ToDate NVARCHAR(250),
	@GSTIN NVARCHAR(250) = NULL,
	@GSTCATEGORY INT = NULL,
	@AcOwner INT,
	@OPUnit NVARCHAR(250) =NULL,
	@OperatingUnit INT = NULL,
	@DateF NVARCHAR(250) = NULL,@whereStr nvarchar(1000)=null,@crwhereStr nvarchar(1000)=null,@advwhereStr nvarchar(1000)=null)
AS
BEGIN
IF @FromDate!='' and  @ToDate!=''
begin
  		set @whereStr=' and S9.COLUMN08 between '''+@FromDate+''' AND '''+@ToDate+''''
  		set @crwhereStr=' and S9.COLUMN06 between '''+@FromDate+''' AND '''+@ToDate+''''
  		set @advwhereStr=' and S9.COLUMN05 between '''+@FromDate+''' AND '''+@ToDate+''''
END
if(@OPUnit!='')
begin
	set @whereStr=@whereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','',S9.COLUMNA02) s)'
	set @crwhereStr=@crwhereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','',S9.COLUMNA02) s)'
	set @advwhereStr=@advwhereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','',S9.COLUMNA02) s)'
end
if(@whereStr='' or @whereStr is null)
begin
	set @whereStr=' and 1=1'
end
if(@crwhereStr='' or @crwhereStr is null)
begin
	set @crwhereStr=' and 1=1'
end
if(@advwhereStr='' or @advwhereStr is null)
begin
	set @advwhereStr=' and 1=1'
end
exec('SELECT (Q.CNT)CNT,Q.TableNo,Q.Particulars,Q.FRMID,Q.TRANS,Q.Date,Q.Party,(Q.SUBTOTAL)SUBTOTAL,(Q.TTAXRATE)TTAXRATE,(Q.TTAX)TTAX,(Q.IGSTAMT)IGSTAMT,(Q.CGSTAMT)CGSTAMT,(Q.SGSTAMT)SGSTAMT,(Q.CESSTAMT)CESSTAMT,Q.PLACE PLACE,Q.GSTIN GSTIN FROM(
(SELECT 0 CNT,'' 4A, 4B, 4C, 6B, 6C'' TableNo,''B2B Invoices(Taxable Outward Supplies to registered person)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 5A, 5B'' TableNo,''B2C (Large) Invoices(Taxable Outward Supplies to a unregistered person where Place of Supply ( State Code) is other than the state where supplier is located ( Inter-State Supplies) and Invoice value is more than Rs. 2.5 Lakh)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 7'' TableNo,''B2C (Others)(Taxable Supplies(Net of debit notes and credit notes) to unregistered persons other than the supplies covered in Table 5)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 9B'' TableNo,''Credit / Debit Notes (Registered)(Details of Credit / Debit Notes issued to registered taxpayers)'' Particulars,1330 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,''(Original Invoice Number&Original Invoice Date)'' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 9B'' TableNo,''Credit / Debit Notes (Unregistered)(Details of Credit/Debit Notes for unregistered user)'' Particulars,1330 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,''(Original Invoice Number&Original Invoice Date)'' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 8A, 8B, 8C, 8D'' TableNo,''Nil Rated Supplies'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 8A, 8B, 8C, 8D'' TableNo,''Exempted(Other than Nil rated/non-GST supply)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 8A, 8B, 8C, 8D'' TableNo,''Non-GST Supplies '' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,'' 6A'' TableNo,''Exports Invoices(Supplies Exported)'' Particulars,1277 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,''11A(1), 11A(2)'' TableNo,''Tax Liability (Advances Received)(Advance amount received in the tax period for which invoice has not been issued (tax amount to be added to output tax liability))'' Particulars,1386 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,''11B(1), 11B(2)'' TableNo,''Adjustment of Advances(Advance amount received in earlier tax period and adjusted against the supplies being shown in this tax period in Table Nos. 4,5,6 and 7)'' Particulars,1386 FRMID,NULL TRANS,NULL Date,NULL Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,'''' GSTIN
UNION ALL
SELECT 0 CNT,''12'' TableNo,''12 - HSN-wise summary of outward supplies'' Particulars,1277 FRMID,''(UOM)'' TRANS,''(Quantity)'' Date,''(Description)'' Party,0 SUBTOTAL,0 TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT,'''' PLACE,''(HSN/SAC)'' GSTIN
)UNION ALL(
SELECT COUNT(distinct P.TRANS)CNT,P.TableNo,P.Particulars,P.FRMID,P.TRANS,P.Date,P.Party,SUM(P.SUBTOTAL)SUBTOTAL,(P.TTAXRATE)TTAXRATE,SUM(P.TTAX)TTAX,SUM(P.IGSTAMT)IGSTAMT,SUM(P.CGSTAMT)CGSTAMT,SUM(P.SGSTAMT)SGSTAMT,SUM(P.CESSTAMT)CESSTAMT,P.PLACE PLACE,P.GSTIN GSTIN FROM(
SELECT 0 CNT,'' 4A, 4B, 4C, 6B, 6C'' TableNo,''B2B Invoices(Taxable Outward Supplies to registered person)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and isnull(a.column21,1000) not in(1000,0) and s9.column03 in(1277,1532,1604) and (case when (S9.COLUMN65!='''' and S9.COLUMN65!=0) then S9.COLUMN65 else S.COLUMN41 end)=23601 and S9.COLUMN24>0 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,P.COLUMN05,P.COLUMN06,s.COLUMN42
UNION ALL
SELECT 0 CNT,'' 4A, 4B, 4C, 6B, 6C'' TableNo,''B2B Invoices(Taxable Outward Supplies to registered person)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)as decimal(18,2))SUBTOTAL,0TTAXRATE,
isnull(S9.COLUMN72,0) TTAX,0 IGSTAMT,0 CGSTAMT, 0 SGSTAMT,0 CESSTAMT,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE002 s on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
WHERE isnull(S9.COLUMNA13,0)=0 and isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)!=0 and s9.column03 in(1277,1532,1604) and (case when (S9.COLUMN65!='''' and S9.COLUMN65!=0) then S9.COLUMN65 else S.COLUMN41 end)=23601 and S9.COLUMN24>0 and s9.columna03='+@AcOwner+' '+@whereStr+'
UNION ALL
SELECT 0 CNT,'' 6A'' TableNo,''Exports Invoices(Supplies Exported)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and s9.COLUMN66!=s.COLUMN43 and t.COLUMN16=23584  and (s9.COLUMN64 is not null and s9.column64 != '''') and S9.COLUMN24>0  and s9.column02=-11 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S9.COLUMN64,S.COLUMN05,P.COLUMN05,P.COLUMN06,s.COLUMN42
UNION ALL
SELECT 0 CNT,'' 5A, 5B'' TableNo,''B2C (Large) Invoices(Taxable Outward Supplies to a unregistered person where Place of Supply ( State Code) is other than the state where supplier is located ( Inter-State Supplies) and Invoice value is more than Rs. 2.5 Lakh)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and s9.COLUMN65=23602 and s9.COLUMN20>250000 and S9.COLUMN24>0 and t.COLUMN16 = 23584 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,P.COLUMN05,P.COLUMN06,s.COLUMN42 
UNION ALL
SELECT 0 CNT,'' 5A, 5B'' TableNo,''B2C (Large) Invoices(Taxable Outward Supplies to a unregistered person where Place of Supply ( State Code) is other than the state where supplier is located ( Inter-State Supplies) and Invoice value is more than Rs. 2.5 Lakh)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)as decimal(18,2))SUBTOTAL,0TTAXRATE,
0TTAX,0 IGSTAMT,0 CGSTAMT, 0 SGSTAMT,0 CESSTAMT,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)!=0 and s9.column03 in(1277,1532,1604) and s9.COLUMN65=23602 and s9.COLUMN20>250000 and S9.COLUMN24>0 and t.COLUMN16 = 23584 and s9.columna03='+@AcOwner+' '+@whereStr+'
group by S9.COLUMN04,S9.COLUMN08,S.COLUMN05,S9.COLUMN03,p.column06,p.column05,s.COLUMN42,S9.COLUMN35,S9.COLUMN36,S9.COLUMN44
UNION ALL
SELECT 0 CNT,'' 7'' TableNo,''B2C (Others)(Taxable Supplies(Net of debit notes and credit notes) to unregistered persons other than the supplies covered in Table 5)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and (case when (S9.COLUMN65!='''' and S9.COLUMN65!=0) then S9.COLUMN65 else S.COLUMN41 end)=23602  and S9.COLUMN24>0 and s9.COLUMN20 <= iif( t.COLUMN16 = 23584,250000,s9.COLUMN20)  and s9.columna03='+@AcOwner+' '+@whereStr+'  --and s9.COLUMN20<=250000
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,P.COLUMN05,P.COLUMN06,s.COLUMN42 
UNION ALL
SELECT 0 CNT,'' 7'' TableNo,''B2C (Others)(Taxable Supplies(Net of debit notes and credit notes) to unregistered persons other than the supplies covered in Table 5)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
(cast(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)as decimal(18,2)))SUBTOTAL,0TTAXRATE,
0TTAX,0 IGSTAMT,0 CGSTAMT, 0 SGSTAMT,0 CESSTAMT,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)!=0 and isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604)   and S9.COLUMN24>0 and s9.COLUMN20 <= iif( t.COLUMN16 = 23584,250000,s9.COLUMN20)  and s9.columna03='+@AcOwner+' '+@whereStr+'  -- and (case when (S9.COLUMN65!='''' and S9.COLUMN65!=0) then S9.COLUMN65 else S.COLUMN41 end)=23602
group by S9.COLUMN04,S9.COLUMN08,S.COLUMN05,S9.COLUMN03,p.column06,p.column05,s.COLUMN42,S9.COLUMN35,S9.COLUMN36,S9.COLUMN44
UNION ALL
SELECT 0 CNT,'' 9B'' TableNo,''Credit / Debit Notes (Registered)(Details of Credit / Debit Notes issued to registered taxpayers)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN06,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN07,0)*(iif(isnull(a.COLUMN32,0)=0,isnull(a.COLUMN09,0),isnull(a.COLUMN32,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column04+''-''+cast(p.column08 as nvarchar(25))) PLACE,s.COLUMN42 GSTIN
FROM SATABLE005 S9  
inner join SATABLE006 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN19 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join SATABLE009 p on S9.COLUMN49=p.COLUMN02 and p.COLUMNA03='''+@AcOwner+'''  and isnull(p.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03=1330 and s9.column67 is not null and s9.column67 !='''' and  S9.COLUMN32>0 and s9.columna03='+@AcOwner+' '+@crwhereStr+'
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,S9.COLUMN04,S9.COLUMN03,S9.COLUMN06,S.COLUMN05,P.COLUMN04,P.COLUMN08,s.COLUMN42 
UNION ALL
SELECT 0 CNT,'' 9B'' TableNo,''Credit / Debit Notes (Registered)(Details of Credit / Debit Notes issued to registered taxpayers)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN06,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN17,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*SUM(CAST(isnull(t.COLUMN17,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column04+''-''+cast(p.column08 as nvarchar(25))) PLACE,s.COLUMN34 GSTIN
FROM PUTABLE001 S9  
inner join PUTABLE002 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN19 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE001 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join PUTABLE005 p on S9.COLUMN48=p.COLUMN02 and p.COLUMNA03='''+@AcOwner+''' and S9.COLUMNA02=p.COLUMNA02  and isnull(p.COLUMNA13,0)=0   
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03=1355 and s.column34 is not null and s.column34 !='''' and s9.columna03='+@AcOwner+' '+@crwhereStr+'
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,S9.COLUMN04,S9.COLUMN03,S9.COLUMN06,S.COLUMN05,P.COLUMN04,P.COLUMN08,s.COLUMN34 
UNION ALL 
SELECT 0 CNT,'' 9B'' TableNo,''Credit / Debit Notes (Unregistered)(Details of Credit/Debit Notes for unregistered user)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN06,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN07,0)*(iif(isnull(a.COLUMN32,0)=0,isnull(a.COLUMN09,0),isnull(a.COLUMN32,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column04+''-''+cast(p.column08 as nvarchar(25))) PLACE,s.COLUMN42 GSTIN
FROM SATABLE005 S9  
inner join SATABLE006 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN19 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join SATABLE009 p on S9.COLUMN49=p.COLUMN02 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03=1330 and (s9.column67 is null or s9.column67 ='''') and S9.COLUMN24 >0 and s9.columna03='+@AcOwner+' '+@crwhereStr+'
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,S9.COLUMN04,S9.COLUMN03,S9.COLUMN06,S.COLUMN05,P.COLUMN04,P.COLUMN08,s.COLUMN42 
UNION ALL

SELECT 0 CNT,'' 9B'' TableNo,''Credit / Debit Notes (Unregistered)(Details of Credit/Debit Notes for unregistered user)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN06,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN17,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN07,0)*isnull(a.COLUMN32,0))*SUM(CAST(isnull(t.COLUMN17,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column04+''-''+cast(p.column08 as nvarchar(25))) PLACE,s.COLUMN34 GSTIN
FROM PUTABLE001 S9  
inner join PUTABLE002 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN19 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE001 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join PUTABLE005 p on S9.COLUMN48=p.COLUMN02 and p.COLUMNA03='''+@AcOwner+''' and S9.COLUMNA02=p.COLUMNA02  and isnull(p.COLUMNA13,0)=0   
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03=1355 and (s.column34 is null or s.column34 ='''') and s9.columna03='+@AcOwner+' '+@crwhereStr+'
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,S9.COLUMN04,S9.COLUMN03,S9.COLUMN06,S.COLUMN05,P.COLUMN04,P.COLUMN08,s.COLUMN34 
UNION ALL
SELECT 0 CNT,'' 8A, 8B, 8C, 8D'' TableNo,''Nil Rated Supplies'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(case when (s9.COLUMN66!=s.COLUMN43 and isnull(s9.COLUMN66,0)!=''0'' and s9.COLUMN66 is not null) then ''Inter-state supplies to registered person''
       when (s9.COLUMN66!=s.COLUMN43 and (isnull(s9.COLUMN66,0)=''0'' or s9.COLUMN66 is  null)) then ''Inter-state supplies to unregistered  person'' 
	   when (s9.COLUMN66=s.COLUMN43 and isnull(s9.COLUMN66,0)!=''0'' and s9.COLUMN66 is not null) then ''Intra-state supplies to registered person''
       when (s9.COLUMN66=s.COLUMN43 and (isnull(s9.COLUMN66,0)=''0'' or s9.COLUMN66 is  null)) then ''Intra-state supplies to unregistered  person'' end) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
inner join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMN07=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and t.column07 = 0   and s9.columna03='+@AcOwner+' '+@whereStr+' --and s9.column02=-11
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,S9.COLUMN66,S.COLUMN43,P.COLUMN05,P.COLUMN06,s.COLUMN42 
UNION ALL
SELECT 0 CNT,'' 8A, 8B, 8C, 8D'' TableNo,''Exempted(Other than Nil rated/non-GST supply)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN08,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(case when (isnull(s9.COLUMN66,0)!=isnull(s.COLUMN43,0) and isnull(s.COLUMN42,0)!=''0'' and s.COLUMN42 is not null) then ''Inter-state supplies to registered person''
       when (isnull(s9.COLUMN66,0)!=isnull(s.COLUMN43,0) and (isnull(s.COLUMN42,0)=''0'' or s.COLUMN42 is  null)) then ''Inter-state supplies to unregistered  person'' 
	   when (isnull(s9.COLUMN66,0)=isnull(s.COLUMN43,0) and isnull(s.COLUMN42,0)!=''0'' and s.COLUMN42 is not null) then ''Intra-state supplies to registered person''
       when (isnull(s9.COLUMN66,0)=isnull(s.COLUMN43,0) and (isnull(s.COLUMN42,0)=''0'' or s.COLUMN42 is  null)) then ''Intra-state supplies to unregistered  person'' end) PLACE,s.COLUMN42 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S9.COLUMN66=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMN07=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and isnull(a.column21,1000)in(1000,0)   and s9.columna03='+@AcOwner+' '+@whereStr+' --and s9.column02=-11
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,S9.COLUMN66,S.COLUMN43,S.COLUMN42,P.COLUMN05,P.COLUMN06,s.COLUMN42 
UNION ALL
SELECT COUNT(*)CNT,''11A(1), 11A(2)'' TableNo,''Tax Liability (Advances Received)(Advance amount received in the tax period for which invoice has not been issued (tax amount to be added to output tax liability))'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN05,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull(a.COLUMN05,0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull(a.COLUMN05,0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN05 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM FITABLE023 S9  
inner join FITABLE024 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN09 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN08=s.COLUMN02 and (isnull(S9.COLUMN20,22335)=22335 or cast(S9.COLUMN20 as nvarchar(250))='''') and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S.COLUMN43=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(S9.COLUMN12,''-'',''''))) s)) or t.COLUMN02=S9.COLUMN12) and t.COLUMNA03=S9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03=1386 and s9.column24>0  and s9.columna03='+@AcOwner+' '+@advwhereStr+'
group by a.COLUMN02,a.COLUMN05,S9.COLUMN04,S9.COLUMN03,S9.COLUMN05,S.COLUMN05,s.COLUMN42,P.COLUMN05,P.COLUMN06 
UNION ALL
SELECT COUNT(*)CNT,''11B(1), 11B(2)'' TableNo,''Adjustment of Advances(Advance amount received in earlier tax period and adjusted against the supplies being shown in this tax period in Table Nos. 4,5,6 and 7)'' Particulars,S9.COLUMN04 TRANS,format(S9.COLUMN05,'''+@DateF+''') [Date],S.COLUMN05 Party,S9.COLUMN03 FRMID,
cast(isnull((s9.COLUMN10 - s9.COLUMN19),0)as decimal(18,2))SUBTOTAL,sum(cast(isnull(t.COLUMN07,0) as decimal(18,2)))TTAXRATE,
cast(((isnull((s9.COLUMN10 - s9.COLUMN19),0))*SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2))TTAX,
cast(((isnull((s9.COLUMN10 - s9.COLUMN19) ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull((s9.COLUMN10 - s9.COLUMN19) ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull((s9.COLUMN10 - s9.COLUMN19) ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull((s9.COLUMN10 - s9.COLUMN19) ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
,(p.column06+''-''+p.column05) PLACE,s.COLUMN42 GSTIN
FROM FITABLE023 S9  
--inner join FITABLE024 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN09 and a.COLUMNA02=S9.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join SATABLE002 s on S9.COLUMN08=s.COLUMN02 and (isnull(S9.COLUMN20,22335)=22335 or cast(S9.COLUMN20 as nvarchar(250))='''') and S.COLUMNA03='''+@AcOwner+''' and isnull(S.COLUMNA13,0)=0 
left join MATABLE017 p on S.COLUMN43=p.COLUMN02  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(S9.COLUMN12,''-'',''''))) s)) or t.COLUMN02=S9.COLUMN12) and t.COLUMNA03=S9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03=1386 and s9.column24>0 and (S9.COLUMN10-S9.COLUMN19)>0 and s9.columna03='+@AcOwner+' '+@advwhereStr+'
group by S9.COLUMN04,S9.COLUMN03,S9.COLUMN10,S9.COLUMN19,S9.COLUMN05,S.COLUMN05,s.COLUMN42,P.COLUMN05,P.COLUMN06 
UNION ALL
SELECT p.CNT,P.TableNo,P.Particulars,P.TRANS,cast(sum(cast(P.Date as decimal(18,2)))as nvarchar(25))Date,P.Party,P.FRMID,sum(P.SUBTOTAL)SUBTOTAL,(P.TTAXRATE)TTAXRATE,SUM(P.TTAX)TTAX,SUM(P.IGSTAMT)IGSTAMT,SUM(P.CGSTAMT)CGSTAMT,SUM(P.CGSTAMT)SGSTAMT,SUM(P.CESSTAMT)CESSTAMT,P.PLACE PLACE,P.GSTIN GSTIN FROM(
SELECT ''''CNT,''12'' TableNo,''12 - HSN-wise summary of outward supplies'' Particulars,u.column04 TRANS,cast((a.column10) as nvarchar(250)) [Date],p.column50 Party,NULL FRMID,
(cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2)))SUBTOTAL,''0''TTAXRATE,
2*(cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2)))TTAX,
(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) IGSTAMT,
(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) CGSTAMT,0 SGSTAMT,
(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) CESSTAMT
,'''' PLACE,h.COLUMN04 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE007 p on a.COLUMN05 = p.COLUMN02 and a.COLUMNA03=p.COLUMNA03
left join MATABLE032 h on p.COLUMN75 = h.COLUMN02 
left join MATABLE002 u on u.COLUMN02 = a.COLUMN22 
inner join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0   and t.COLUMN16=23582 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and s9.columna03='+@AcOwner+' '+@whereStr+'
union all
SELECT ''''CNT,''12'' TableNo,''12 - HSN-wise summary of outward supplies'' Particulars,u.column04 TRANS,cast((a.column10) as nvarchar(250)) [Date],p.column50 Party,NULL FRMID,
(cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2)))SUBTOTAL,''0''TTAXRATE,
(cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2)))TTAX,
(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) IGSTAMT,0 CGSTAMT,0 SGSTAMT,
(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) CESSTAMT
,'''' PLACE,h.COLUMN04 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE007 p on a.COLUMN05 = p.COLUMN02 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0
left join MATABLE032 h on p.COLUMN75 = h.COLUMN02 
left join MATABLE002 u on u.COLUMN02 = a.COLUMN22 
inner join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0   and t.COLUMN16=23584 
WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and s9.columna03='+@AcOwner+' '+@whereStr+'
union all
SELECT ''''CNT,''12'' TableNo,''12 - HSN-wise summary of outward supplies'' Particulars,u.column04 TRANS,cast((a.column10) as nvarchar(250)) [Date],p.column50 Party,NULL FRMID,
(cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2)))SUBTOTAL,''0''TTAXRATE,0 TTAX,0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
,'''' PLACE,h.COLUMN04 GSTIN
FROM SATABLE009 S9  
inner join SATABLE010 a on  s9.COLUMNA03='''+@AcOwner+''' and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=S9.COLUMNA02 and a.COLUMNA03='''+@AcOwner+''' and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE007 p on a.COLUMN05 = p.COLUMN02 and p.COLUMNA03='''+@AcOwner+''' and isnull(p.COLUMNA13,0)=0
left join MATABLE032 h on p.COLUMN75 = h.COLUMN02 
left join MATABLE002 u on u.COLUMN02 = a.COLUMN22 
--inner join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0   and t.COLUMN16=23584 
WHERE isnull(S9.COLUMNA13,0)=0 and isnull(a.column21,1000)in(1000,0) and s9.column03 in(1277,1532,1604) and s9.columna03='+@AcOwner+' '+@whereStr+'
--group by u.COLUMN04,p.COLUMN50,S9.COLUMN03,h.COLUMN04,a.COLUMN10,a.COLUMN13,a.COLUMN35,t.COLUMN07,t.COLUMN16
)p group by p.CNT,p.TRANS,p.Party,P.FRMID,p.GSTIN,p.TableNo,p.Particulars,p.PLACE,P.TTAXRATE

) P GROUP BY P.FRMID,p.TRANS,P.Date,P.Party,P.TableNo,P.Particulars,P.PLACE,p.GSTIN,P.TTAXRATE
))Q --GROUP BY Q.TableNo,Q.Particulars,Q.FRMID
')
END





--SELECT p.CNT,P.TableNo,P.Particulars,P.TRANS,cast(sum(cast(P.Date as decimal(18,2)))as nvarchar(25))Date,P.Party,P.FRMID,sum(P.SUBTOTAL)SUBTOTAL,(P.TTAXRATE) TTAXRATE,SUM(P.TTAX)TTAX,SUM(P.IGSTAMT)IGSTAMT,SUM(P.CGSTAMT)CGSTAMT,SUM(P.CGSTAMT)SGSTAMT,SUM(P.CESSTAMT)CESSTAMT,P.PLACE PLACE,P.GSTIN GSTIN FROM(
--SELECT ''CNT,'12' TableNo,'12 - HSN-wise summary of outward supplies' Particulars,u.column04 TRANS,cast((a.column10) as nvarchar(250)) [Date],p.column50 Party,NULL FRMID,
--(cast(isnull(a.COLUMN10,0)*(iif(isnull(a.COLUMN35,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35,0)))as decimal(18,2)))SUBTOTAL,isnull(t.COLUMN07,0) TTAXRATE,
--2*(cast(((isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0))*(CAST(isnull(t.COLUMN07,0) as decimal(18,2)))/100)as decimal(18,2)))TTAX,
--(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) IGSTAMT,
--(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) CGSTAMT,0 SGSTAMT,
--(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2))) CESSTAMT
--,'' PLACE,h.COLUMN04 GSTIN--,a.COLUMN21
--FROM SATABLE009 S9  
--inner join SATABLE010 a on s9.COLUMNA03=56667 and S9.COLUMN01=a.COLUMN15 and a.COLUMNA02=56983 and a.COLUMNA03=56667 and isnull(a.COLUMNA13,0)=0 
----inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
--left join MATABLE007 p on a.COLUMN05 = p.COLUMN02 and a.COLUMNA03=p.COLUMNA03
--left join MATABLE032 h on p.COLUMN75 = h.COLUMN02 
--left join MATABLE002 u on u.COLUMN02 = a.COLUMN22 
--inner join (
--select sum(t.column07) column07,'-'+t1.COLUMN02 COLUMN02,t.COLUMNA03 COLUMNA03,t.COLUMNA13 COLUMNA13,'23582' COLUMN16 from matable013 t
--inner join matable014 t1 on t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(t1.COLUMN05)))) and t.columna03 = t1.columna03 
-- where    t.columna03 = 56667 
-- group by t1.COLUMN02,t.COLUMNA03,t.COLUMNA13 --,t.COLUMN16 
-- union all
--select sum(t.column07)column07 ,t.COLUMN02 COLUMN02,COLUMNA03, COLUMNA13, COLUMN16 from matable013 t
-- where    t.columna03 = 56667 --and t1.column02 is null
-- group by t.COLUMN02,COLUMNA03,COLUMNA13,COLUMN16
 
-- ) t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0   --and t.COLUMN16 23582 
--WHERE isnull(S9.COLUMNA13,0)=0 and s9.column03 in(1277,1532,1604) and s9.columna03=56667 and S9.COLUMN08 between '03/28/18' AND '03/28/18' and a.COLUMNA02=56983



--)p group by p.CNT,p.TRANS,p.Party,P.FRMID,p.GSTIN,p.TableNo,p.Particulars,p.PLACE,P.TTAXRATE--,p.COLUMN21
GO
