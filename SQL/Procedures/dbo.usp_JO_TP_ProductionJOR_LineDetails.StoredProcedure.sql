USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_ProductionJOR_LineDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_ProductionJOR_LineDetails](
@ProductionID nvarchar(250),@AcOwner nvarchar(250)=null,@Opunit nvarchar(250)=null,@JoborderID nvarchar(250)=null,
@JoborderIssueID nvarchar(250)=null)
as
begin
SELECT l.COLUMN02 LineID,l.COLUMN04 COLUMN03,l.COLUMN05 COLUMN04,l.COLUMN13 COLUMN17,(isnull(l.COLUMN07,0)-sum(isnull(rl.COLUMN08,0))) COLUMN06,(isnull(l.COLUMN07,0)-sum(isnull(rl.COLUMN08,0))) COLUMN08,0 COLUMN09,
(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end) COLUMN10,cast(cast((isnull(l.COLUMN07,0)-sum(isnull(rl.COLUMN08,0))) as decimal(18,2))*cast(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end as decimal(18,2))as decimal(18,2)) COLUMN11
from SATABLE020 l inner join SATABLE019 h on h.COLUMN01=l.COLUMN09 and h.COLUMNA03=l.COLUMNA03 and isnull(l.COLUMNA13,0)=0
left join PUTABLE003 r on r.COLUMN23=h.COLUMN02 and r.COLUMNA03=h.COLUMNA03 and isnull(r.COLUMNA13,0)=0 
left join PUTABLE004 rl on rl.COLUMN12=r.COLUMN01 and l.COLUMN02=rl.COLUMN26  and r.COLUMNA03=rl.COLUMNA03 and isnull(rl.COLUMNA13,0)=0 
left join MATABLE007 MA07 on MA07.COLUMN02=l.COLUMN04 and MA07.COLUMNA03=l.COLUMNA03 and isnull(MA07.COLUMNA13,0)=0 
left join MATABLE024 pp on pp.COLUMN07=l.COLUMN04 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=l.COLUMNA02  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=l.COLUMNA03 and isnull(pp.COLUMNA13,0)=0 
where h.COLUMN02=@ProductionID and h.COLUMNA03=@AcOwner and isnull(h.COLUMNA13,0)=0 and (h.COLUMN13='' or isnull(h.COLUMN13,0)=0) 
group by l.COLUMN02,l.COLUMN04,l.COLUMN05,l.COLUMN13,pp.COLUMN04,MA07.COLUMN17,l.COLUMN07 having (isnull(l.COLUMN07,0)-sum(isnull(rl.COLUMN08,0)))>0
end
GO
