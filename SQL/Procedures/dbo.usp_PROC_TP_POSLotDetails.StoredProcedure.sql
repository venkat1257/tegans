USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PROC_TP_POSLotDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PROC_TP_POSLotDetails]
(
	@ItemID nvarchar(250)=NULL,@UOM nvarchar(250)=NULL,@Lot nvarchar(250)=NULL,@DateF nvarchar(250)=NULL,@Type nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL
)
AS
BEGIN
set @UOM=(iif(isnull(@UOM,'')='','0',@UOM))
set @Lot=(iif(isnull(@Lot,'')='','0',@Lot))
if(@Lot!='0' and @Type='LOT')
begin
select COLUMN02,COLUMN04,format(COLUMN05,@DateF)COLUMN05  from FITABLE043 Where isnull(COLUMN02,0)=@Lot and (COLUMNA02=@OPUnit or COLUMNA02 is null)
AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' order by convert(datetime, COLUMN05, 101)
END
ELSE
BEGIN
IF not exists(select COLUMN02 FROM FITABLE043 where COLUMN09=@ItemID  And isnull(COLUMN15,0)=@UOM And  
(COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)
  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False')
begin
select COLUMN02,COLUMN04,format(COLUMN05,@DateF)COLUMN05 FROM FITABLE043 where COLUMN09=@ItemID  And isnull(COLUMN15,0)=0  And  
(COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)
  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' order by convert(datetime, COLUMN05, 101)
END
Else
begin
select COLUMN02,COLUMN04,format(COLUMN05,@DateF)COLUMN05  from FITABLE043 Where COLUMN09=@ItemID and isnull(COLUMN15,0)=@UOM and (COLUMNA02=@OPUnit or COLUMNA02 is null)
AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' order by convert(datetime, COLUMN05, 101)
END
END
END





GO
