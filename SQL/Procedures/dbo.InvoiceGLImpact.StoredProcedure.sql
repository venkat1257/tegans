USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[InvoiceGLImpact]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InvoiceGLImpact]
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@FormId nvarchar(250)= null
)
AS
BEGIN

;with MyTable as (

select f1.COLUMN04 Account,s9.column12 Memo,sum(p18.COLUMN11) Credit,sum(p18.COLUMN09) Debit,1 id  from satable009 s9
left join putable018 p18 on p18.COLUMN05= s9.COLUMN04 and p18.COLUMN07= s9.COLUMN05 and p18.COLUMNA03=@AcOwner and p18.COLUMNA02= s9.COLUMNA02 and p18.COLUMN06='Invoice' and isnull(p18.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = p18.COLUMN03 and (f1.COLUMNA03= p18.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where s9.COLUMN02=@ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0
group by f1.COLUMN04,s9.column12
--union all
--select f1.COLUMN04 Account,s9.column12 Memo,sum(p17.COLUMN10) Credit,sum(p17.COLUMN11) Debit,2 id  from satable009 s9
--inner join putable017 p17 on  p17.COLUMN05= s9.COLUMN01 and p17.COLUMNA03= s9.COLUMNA03 and p17.COLUMNA02= s9.COLUMNA02 
--and p17.COLUMN06 = 'INVOICE' and isnull(p17.COLUMNA13,0)=0
--left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03
--where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and f1.COLUMNA03 = s9.COLUMNA03 and isnull(s9.COLUMNA13,0)=0
--group by f1.COLUMN04,s9.column12
union all
select f.Account,f.Memo,sum(f.Credit) Credit,sum(f.Debit) Credit,3 id from (
select f1.COLUMN04 Account,s9.column12 Memo,sum(f25.COLUMN09) Credit,sum(f25.COLUMN11) Debit from satable009 s9
left join satable010 s10 on s10.COLUMN15=s9.COLUMN01 and  s10.COLUMNA03=@AcOwner and  s10.COLUMNA02= s9.COLUMNA02 and  isnull(s10.COLUMNA13,0)=0
inner join fitable025 f25 on f25.COLUMN05 = s10.COLUMN01 and f25.COLUMNA03= s10.COLUMNA03 and f25.COLUMNA02= s10.COLUMNA02 and f25.COLUMN04 = 'INVOICE' and isnull(f25.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f25.COLUMN08 and (f1.COLUMNA03= f25.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0 AND f1.column02!=1056
group by f1.COLUMN04,s9.column12
union all
select f1.COLUMN04 Account,s9.column12 Memo,sum(f25.COLUMN09) Credit,sum(f25.COLUMN11) Debit from satable009 s9  
--left outer join satable010 s10 on s10.COLUMN15=s9.COLUMN01 and  s10.COLUMNA03= s9.COLUMNA03
inner join fitable025 f25 on f25.COLUMN05 = s9.COLUMN01 and f25.COLUMNA03=@AcOwner and f25.COLUMNA02= s9.COLUMNA02 and f25.COLUMN04 = 'INVOICE' and isnull(f25.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f25.COLUMN08 and (f1.COLUMNA03= f25.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0 AND f1.column02!=1056
group by f1.COLUMN04,s9.column12 ) f group by f.Account,f.Memo
union all
select f1.COLUMN04 Account,s9.column12 Memo,sum(f34.COLUMN07) Credit,sum(f34.COLUMN08) Debit,4 id  from satable009 s9
inner join fitable034 f34 on  f34.COLUMN09= s9.COLUMN04 and f34.COLUMNA03=@AcOwner and f34.COLUMNA02= s9.COLUMNA02 and f34.COLUMN03 = 'TDS Receivable' and isnull(f34.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10 and (f1.COLUMNA03 = f34.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0
group by  f1.COLUMN04,s9.column12
union all
--
select f1.COLUMN04 Account,s9.column12 Memo,sum(f36.COLUMN08) Credit,sum(f36.COLUMN07) Debit,5 id from satable009 s9
inner join fitable036 f36 on f36.COLUMN09 = s9.COLUMN04 and f36.COLUMNA03=@AcOwner and f36.COLUMNA02= s9.COLUMNA02 and f36.COLUMN03 = 'INVOICE' and isnull(f36.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f36.COLUMN10 and (f1.COLUMNA03 = f36.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0
group by f1.COLUMN04,s9.column12 
union all
select f1.COLUMN04 Account,s9.column12 Memo,sum(f27.COLUMN12) Credit,sum(f27.COLUMN13) Debit,6 id from satable009 s9
inner join fitable027 f27 on f27.COLUMN09 = s9.COLUMN04 and f27.COLUMNA03=@AcOwner and f27.COLUMNA02= s9.COLUMNA02 and f27.COLUMN04 = 'INVOICE' and isnull(f27.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f27.COLUMN06 and (f1.COLUMNA03 = f27.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and f1.COLUMNA03 = s9.COLUMNA03 and isnull(s9.COLUMNA13,0)=0
group by f1.COLUMN04,s9.column12 
union all
select f1.COLUMN04 Account,s9.column12 Memo,sum(f35.COLUMN07) Credit,sum(f35.COLUMN08) Debit,7 id from satable009 s9
inner join fitable035 f35 on f35.COLUMN09 = s9.COLUMN04 and f35.COLUMNA03=@AcOwner and f35.COLUMNA02= s9.COLUMNA02 and f35.COLUMN03 = 'INVOICE' and isnull(f35.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f35.COLUMN10 and (f1.COLUMNA03 = f35.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0
group by f1.COLUMN04,s9.column12 
UNION ALL
select f1.COLUMN04 Account,s9.column12 Memo,sum(f34.COLUMN08) Credit,sum(f34.COLUMN07) Debit,8 id  from satable009 s9
inner join fitable034 f34 on  f34.COLUMN09= s9.COLUMN04 and f34.COLUMNA03=@AcOwner and f34.COLUMNA02= s9.COLUMNA02 and isnull(f34.COLUMNA13,0)=0 and f34.COLUMN03 = 'INVOICE'
left join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10 and (f1.COLUMNA03 = f34.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and f1.COLUMNA03 = s9.COLUMNA03 and isnull(s9.COLUMNA13,0)=0
group by  f1.COLUMN04,s9.column12
union all
select f1.COLUMN04 Account,s9.column12 Memo,sum(f26.COLUMN11) Credit,sum(f26.COLUMN12) Debit,9 id from satable009 s9
inner join fitable026 f26 on f26.COLUMN09 = s9.COLUMN04 and f26.COLUMNA03=@AcOwner and f26.COLUMNA02= s9.COLUMNA02 and f26.COLUMN04 = 'INVOICE' and isnull(f26.COLUMNA13,0)=0
left join FITABLE001 f1 on f1.COLUMN02 = f26.COLUMN08 and (f1.COLUMNA03 = f26.COLUMNA03 or isnull(f1.COLUMNA03,0)=0) and isnull(f1.COLUMNA13,0)=0
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and f1.COLUMNA03 = s9.COLUMNA03 and isnull(s9.COLUMNA13,0)=0
group by f1.COLUMN04,s9.column12 
union all 
select f26.COLUMN17 Account,s9.column12 Memo,sum(f26.COLUMN11) Credit,sum(f26.COLUMN12) Debit,10 id from satable009 s9
left join fitable026 f26 on f26.COLUMN09 = s9.COLUMN04 and  f26.COLUMN06 = s9.COLUMN05 and f26.COLUMNA03=@AcOwner and f26.COLUMNA02= s9.COLUMNA02 and f26.COLUMN04 = 'INVOICE' and isnull(f26.COLUMNA13,0)=0
left join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 and isnull(m13.COLUMNA13,0)=0 
where  s9.COLUMN02= @ide and s9.COLUMNA03=@AcOwner and isnull(s9.COLUMNA13,0)=0 and f26.COLUMN17!= 'RCM Interim For Sale'
group by f26.COLUMN17,s9.column12 
)
select [Account],[Memo],sum(Credit)Credit,sum(Debit)Debit,[id] from MyTable
group by [Account],[Memo],[id] order by id asc
end
 








GO
