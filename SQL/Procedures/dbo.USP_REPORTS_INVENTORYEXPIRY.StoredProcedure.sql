USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_INVENTORYEXPIRY]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_REPORTS_INVENTORYEXPIRY]
(
@Item nvarchar(250)=null,
@FromDate nvarchar(250)=null,
@ToDate nvarchar(250)=null,
@Batch nvarchar(250)=null,
@Brand nvarchar(250)=null,
@UPC nvarchar(250)=null,
@OperatingUnit nvarchar(250)=null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@Location nvarchar(250)= null,
@DateF nvarchar(250)=null,
@reorder nvarchar(250)=null
)
as
begin
declare @whereStr nvarchar(1000)=null
declare @Query1 nvarchar(max)=null 
if @Item!=''
begin
 set @whereStr= 'a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='a.COLUMN13 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN13 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Batch!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' bm.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Batch+''') s)'
end
else
begin
set @whereStr=@whereStr+' and bm.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Batch+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' b.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and b.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='b.COLUMN06 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
else
begin
set @whereStr=@whereStr+' and b.COLUMN06 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
end
if @reorder!='false'
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='a.COLUMN04<b.COLUMN72'-- in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@reorder+''') s)'
end
else
begin
set @whereStr=@whereStr--+' and b.COLUMN72 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@reorder+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='a.COLUMN21 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN21 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' 1=1'
 end
declare @DecimalPositions int
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end

exec ('
select b.column04  Item,(case when a.COLUMN19=10000 then e.column05 else m.COLUMN04 end) uom, b.COLUMN06 UPC,b.COLUMN50 ''Desc'',o.COLUMN03 ou,l.COLUMN04 Location,bm.COLUMN02 LotId,bm.COLUMN04 Lot,FORMAT(bm.COLUMN05,'''+@DateF+''') ExpiryDate,Convert(float,convert(double precision,a.COLUMN04))COLUMN04 ,0 COLUMN05 ,0 COLUMN06 ,0 COLUMN07 ,
--EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
Convert(float,convert(double precision,a.COLUMN08))COLUMN08  ,a.COLUMN09 ,a.COLUMN12,a.COLUMN18,b.COLUMN10 BrandID,m5.COLUMN04 as Brand,a.COLUMN13 OPID,a.COLUMN03 ItemID,a.COLUMN21 LocationID,(case when isnull(bm.COLUMN11,0)>0 then isnull(bm.COLUMN11,0) when isnull(sp1.COLUMN04,0)>0 then isnull(sp1.COLUMN04,0) else isnull(b.COLUMN17,0) end) Pprice,
(case when isnull(bm.COLUMN12,0)>0 then isnull(bm.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(b.COLUMN51,0) end) SPrices,iif(isnull(bm.COLUMN14,0)>0,bm.COLUMN14,sp.COLUMN05) SMRP,isnull(b.COLUMN72,0) reorder,
cast((case when isnull(bm.COLUMN12,0)>0 then isnull(bm.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(b.COLUMN51,0) end)*a.COLUMN04 as decimal(18,2))TOTAMT from FITABLE010 a 
inner join MATABLE007 b  on b.COLUMN02=a.COLUMN03 and  b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
--left outer join  MATABLE007 b1 on b1.COLUMN73=a.COLUMNA02 where  b1.COLUMN72>0
left outer join MATABLE024 sp on sp.COLUMN07=b.COLUMN02 and sp.COLUMN06=''Sales'' and (isnull(sp.COLUMN03,0)=b.COLUMNA02  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=b.COLUMNA03 and sp.COLUMNA13=0 
 left outer join MATABLE024 sp1 on sp1.COLUMN07=b.COLUMN02 and sp1.COLUMN06=''Purchase'' and (isnull(sp1.COLUMN03,0)=b.COLUMNA02  or isnull(sp1.COLUMN03,0)=0) and sp1.COLUMNA03=b.COLUMNA03 and sp1.COLUMNA13=0 
inner join CONTABLE007 o on o.COLUMN02=a.COLUMN13 left join MATABLE002 m on m.COLUMN02=a.COLUMN19 left join fitable037 e on e.COLUMN02=10000
left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and (l.COLUMNA02=a.COLUMN13 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03
left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and bm.COLUMN09=b.COLUMN02 and (isnull(bm.COLUMN10,0)=isnull(b.COLUMNA02,0)  or (bm.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable('','', '''+@OPUnit+''') s) or bm.COLUMN10 is null)) and bm.COLUMNA03=a.COLUMNA03 and isnull(bm.COLUMNA13,0)=0 
left outer join MATABLE005 m5 on m5.COLUMN02=b.COLUMN10 and b.COLUMN02=a.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+'
where isnull(a.COLUMNA13,0)=0  AND (a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','', '''+@OPUnit+''') s) or a.columna02 is null) AND  a.COLUMNA03='+@AcOwner+' and cast(bm.COLUMN05 as date) between '''+@FromDate+''' and '''+@ToDate+''' and 
'+@whereStr +' order by Item')
--set @Query1='select * from #CurrentInventoryReport'+@whereStr +' order by Item'
--exec (@Query1) 
end







GO
