USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_WO_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PRO_TP_WO_LINE_DATA]
(
	@WorkOrderID int
)

AS
BEGIN
if exists(select COLUMN05 from PRTABLE007 where COLUMN05 in (@WorkOrderID))
begin
--EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
	select max(a.COLUMN03) as COLUMN03,max(a.COLUMN05) as COLUMN04,(cast(max(a.COLUMN06) as decimal(18,2))-cast(isnull(sum(d.COLUMN06),0) as decimal(18,2))) as COLUMN05,
	 (isnull(f.COLUMN04,0)) as COLUMN07,m.COLUMN48,
	  max( a.COLUMN09) as COLUMN08,d.COLUMN11
	FROM dbo.PRTABLE006 a left outer join PRTABLE005 c on c.COLUMN01=a.COLUMN11
	left outer join PRTABLE007 b on c.COLUMN02=b.COLUMN05
	--EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
	left outer join PRTABLE008 d on d.COLUMN10=b.COLUMN01 and a.COLUMN03=d.COLUMN03  and isnull(d.COLUMNA13,0)=0
	--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
	left outer join FITABLE010 f on f.COLUMN13=b.COLUMN11 and d.COLUMN03=f.COLUMN03 and d.COLUMN11=f.COLUMN19
	--EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
	left join MATABLE007 m on m.COLUMN02=d.COLUMN03  and isnull(m.COLUMNA13,0)=0  and isnull(d.COLUMNA13,0)=0
	WHERE a.COLUMN11 in (select COLUMN01 FROM dbo.PRTABLE005 WHERE COLUMN02= @WorkOrderID)  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN03,f.COLUMN04,d.COLUMN11,m.COLUMN48
end
else
begin
	select  a.COLUMN03 as COLUMN03,a.COLUMN05 as COLUMN04,a.COLUMN06 as COLUMN05,
 	 --EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
	 (isnull(f.COLUMN04,0)) as COLUMN07,m.COLUMN48,
	   a.COLUMN09 as COLUMN08,a.COLUMN12 COLUMN11
	   --EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
	FROM dbo.PRTABLE006 a inner join PRTABLE005 b on b.COLUMN01=a.COLUMN11   and isnull(a.COLUMNA13,0)=0
	left outer join FITABLE010 f on f.COLUMN13=b.COLUMN10 and a.COLUMN03=f.COLUMN03 and a.COLUMN12=f.COLUMN19
	--EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
	left join MATABLE007 m on m.COLUMN02=a.COLUMN03  and isnull(m.COLUMNA13,0)=0 and isnull(a.COLUMNA13,0)=0
	WHERE a.COLUMN11 in (select COLUMN01 FROM dbo.PRTABLE005 WHERE COLUMN02= @WorkOrderID) and a.COLUMNA13=0
	end
END







GO
