USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_ItemDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_ItemDetails]
(
	--EMPHCS927 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	@ItemID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL,@uom nvarchar(250)=NULL,@location nvarchar(250)=NULL,
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	@StockItem nvarchar(250)=NULL,@lotno nvarchar(250)=NULL,@Project nvarchar(250)=NULL
)
AS
BEGIN
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
set @location=(iif(@location is null or @location='',0,@location ))
if(@OPUnit='' or @OPUnit=null) set @OPUnit=0;
--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
declare @uomsetup nvarchar(250)=NULL

	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
set @Project=(iif(@Project is null or @Project='',0,@Project ))
    if(@ItemID !='')
	begin
	--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
	set @uomsetup=(@ItemID)
	IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
	    
    if(@OPUnit !='' or @OPUnit !=null)
	begin
		--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
	IF NOT EXISTS(SELECT * FROM FITABLE039 MA07 inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 WHERE (MA07.COLUMN05=@ItemID  OR m10.COLUMN06=@ItemID) and MA07.COLUMNA03=@AcOwner and (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and MA07.COLUMNA13=0 )
		BEGIN
		--EMPHCS1167 Service items calculation by srinivas 22/09/2015
		--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
			--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
			--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
		IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
	    select  MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN50 COLUMN09,convert(float,convert(double precision,iif(cast(FI10.COLUMN08 as nvarchar)='',0,isnull(FI10.COLUMN08,0))))COLUMN08,
		(case when isnull(lt.COLUMN11,0)>0 then isnull(lt.COLUMN11,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else MA07.COLUMN17 end)COLUMN17,MA07.COLUMN45,MA07.COLUMN29,(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(ss.COLUMN04,0)>0 then isnull(ss.COLUMN04,0) else MA07.COLUMN51 end)COLUMN51,MA07.COLUMN50 sdesc, MA07.COLUMN69 sQdesc,convert(float,convert(double precision,iif(cast(FI10.COLUMN08 as nvarchar)='',0,isnull(FI10.COLUMN08,0))))COLUMN08,MA07.COLUMN57 pdesc, MA07.COLUMN68 pQdesc,MA07.COLUMN63,MA07.COLUMN54,MA07.COLUMN61,MA07.COLUMN64,MA07.COLUMN48,MA07.COLUMN66,MA07.COLUMN65,
		(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty,MA07.COLUMN04 iName,iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT from MATABLE007 MA07 
		left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN02 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02=@OPUnit  or ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and ia.COLUMNA13=0 
		left join MATABLE024 sp on sp.COLUMN07=MA07.COLUMN02 and sp.COLUMN06='Purchase' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0 
		left join MATABLE024 ss on ss.COLUMN07=MA07.COLUMN02 and ss.COLUMN06='Sales' and (isnull(ss.COLUMN03,0)=@OPUnit  or isnull(ss.COLUMN03,0)=0) and ss.COLUMNA03=@AcOwner and ss.COLUMNA13=0
	        left join FITABLE043 lt on lt.COLUMN02=@lotno and lt.COLUMN09=MA07.COLUMN02 and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=MA07.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
		left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02 and FI10.COLUMN24=MA07.COLUMN06 and FI10.COLUMN13=@OPUnit and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@lotno and isnull(FI10.COLUMN23,0)=@Project
		left join MATABLE032 hs on hs.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
		left join MATABLE032 sc on sc.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
		--inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
		--EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
		WHERE   isnull(MA07.COLUMN47,'False')='False' and MA07.COLUMNA13=0 and (MA07.COLUMN02=@ItemID or MA07.COLUMN06=@ItemID) and (MA07.COLUMNA02=@OPUnit or MA07.COLUMNA02 is null) and  MA07.COLUMNA03=@AcOwner
        end
	--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
	else
		begin
		--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
			IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
			begin
			set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
			end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
			select MA07.COLUMN05 COLUMN02,m10.COLUMN06,m10.COLUMN50 COLUMN09,MA07.COLUMN09 COLUMN17,m10.COLUMN45,m10.COLUMN29,MA07.COLUMN10 COLUMN51,m10.COLUMN50 sdesc, m10.COLUMN69 sQdesc,
			--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
			--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
			convert(float,convert(double precision,iif(cast(FI10.COLUMN08 as nvarchar)='',0,isnull(FI10.COLUMN08,0))))COLUMN08,m10.COLUMN57 pdesc, m10.COLUMN68 pQdesc,m10.COLUMN63,m10.COLUMN54,m10.COLUMN61,m10.COLUMN64,m10.COLUMN48,m10.COLUMN66,m10.COLUMN65,
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty, m10.COLUMN04 iName,iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT  from FITABLE039 MA07
			inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05  and  m10.COLUMNA13=0 and  isnull(m10.COLUMN47,'False')='False' and  m10.COLUMNA03=@AcOwner
			left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN05 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02=@OPUnit or ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and isnull(ia.COLUMNA13,0)=0 
			left join MATABLE024 sp on sp.COLUMN07=MA07.COLUMN02 and sp.COLUMN06='Purchase' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0 
		    left join FITABLE043 lt on lt.COLUMN02=@lotno and lt.COLUMN09=m10.COLUMN02 and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=m10.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
		    left outer join	FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN05 and FI10.COLUMN24=MA07.COLUMN06 and FI10.COLUMN13=@OPUnit and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@lotno and isnull(FI10.COLUMN23,0)=@Project 
			left join MATABLE032 hs on hs.COLUMN02=m10.COLUMN75 and m10.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
		    left join MATABLE032 sc on sc.COLUMN02=m10.COLUMN75 and m10.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
		    --EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
			WHERE   isnull(MA07.COLUMNA13,'False')='False' and  (MA07.COLUMN05=@ItemID OR m10.COLUMN06=@ItemID) AND (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and  MA07.COLUMNA03=@AcOwner
		end

	end
	--	else if(@AcOwner !='' or @AcOwner !=null)
	--begin
	--select MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN09,MA07.COLUMN17,MA07.COLUMN45,MA07.COLUMN29,MA07.COLUMN51,MA07.COLUMN50,FI10.COLUMN08,MA07.COLUMN57,MA07.COLUMN63,MA07.COLUMN54,MA07.COLUMN61 from MATABLE007 MA07 left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02 and FI10.COLUMN19=@uom
	----inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
	-- WHERE   MA07.COLUMN47='False' and  (MA07.COLUMN02=@ItemID or MA07.COLUMN06=@ItemID) and  MA07.COLUMNA03=@AcOwner
 --       end
		else
	begin
	--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
	IF NOT EXISTS(SELECT * FROM FITABLE039 MA07 inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 WHERE (MA07.COLUMN05=@ItemID  OR m10.COLUMN06=@ItemID) and MA07.COLUMNA03=@AcOwner  and MA07.COLUMNA13=0 )
		BEGIN
		--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
		IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
	    --EMPHCS1167 Service items calculation by srinivas 22/09/2015
		--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
			--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
			--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
	select MA07.COLUMN02,MA07.COLUMN06,MA07.COLUMN50 COLUMN09,
	(case when isnull(lt.COLUMN11,0)>0 then isnull(lt.COLUMN11,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else MA07.COLUMN17 end)COLUMN17,MA07.COLUMN45,MA07.COLUMN29,(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(ss.COLUMN04,0)>0 then isnull(ss.COLUMN04,0) else MA07.COLUMN51 end)COLUMN51,MA07.COLUMN50 sdesc, MA07.COLUMN69 sQdesc,convert(float,convert(double precision,iif(cast(FI10.COLUMN08 as nvarchar)='',0,isnull(FI10.COLUMN08,0))))COLUMN08,MA07.COLUMN57 pdesc, MA07.COLUMN68 pQdesc,MA07.COLUMN63,MA07.COLUMN54,MA07.COLUMN61,MA07.COLUMN64,MA07.COLUMN48,MA07.COLUMN66,MA07.COLUMN65,
	(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty,MA07.COLUMN04 iName,iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT from MATABLE007 MA07 
	left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN02 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and ia.COLUMNA13=0
	left join MATABLE024 sp on sp.COLUMN07=MA07.COLUMN02 and sp.COLUMN06='Purchase' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0
	left join MATABLE024 ss on ss.COLUMN07=MA07.COLUMN02 and ss.COLUMN06='Sales' and (isnull(ss.COLUMN03,0)=@OPUnit  or isnull(ss.COLUMN03,0)=0) and ss.COLUMNA03=@AcOwner and ss.COLUMNA13=0
	left join FITABLE043 lt on lt.COLUMN02=@lotno and lt.COLUMN09=MA07.COLUMN02 and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=MA07.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
	left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02 and FI10.COLUMN24=MA07.COLUMN06 and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location  and isnull(FI10.COLUMN22,0)=@lotno and isnull(FI10.COLUMN23,0)=@Project
	left join MATABLE032 hs on hs.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
	left join MATABLE032 sc on sc.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	--inner join FITABLE010 on FITABLE010.COLUMN03=MATABLE007.COLUMN02
	--EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
	 WHERE   isnull(MA07.COLUMN47,'False')='False' and  MA07.COLUMNA13=0 and  (MA07.COLUMN02=@ItemID or MA07.COLUMN06=@ItemID)  and  MA07.COLUMNA03=@AcOwner
        end
	--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
	else
		begin
		--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
			IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
			begin
			set @ItemID=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@ItemID and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
			end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@ItemID=COLUMN09 from FITABLE043 Where COLUMN04=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
			select MA07.COLUMN05 COLUMN02,m10.COLUMN06,m10.COLUMN50 COLUMN09,MA07.COLUMN09 COLUMN17,m10.COLUMN45,m10.COLUMN29,MA07.COLUMN10 COLUMN51,m10.COLUMN50 sdesc, m10.COLUMN69 sQdesc,
			--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
			--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
			convert(float,convert(double precision,iif(cast(FI10.COLUMN08 as nvarchar)='',0,isnull(FI10.COLUMN08,0))))COLUMN08,
m10.COLUMN57 pdesc, m10.COLUMN68 pQdesc,m10.COLUMN63,m10.COLUMN54,m10.COLUMN61,m10.COLUMN64,m10.COLUMN48,m10.COLUMN66,m10.COLUMN65,
			(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty ,m10.COLUMN04 iName,iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT from FITABLE039 MA07
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 and  isnull(m10.COLUMN47,'False')='False' and  m10.COLUMNA13=0  and  m10.COLUMNA03=@AcOwner
			left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN05 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and ia.COLUMNA13=0
			left join FITABLE043 lt on lt.COLUMN02=@lotno and lt.COLUMN09=m10.COLUMN02 and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=m10.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
			left outer join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02 and FI10.COLUMN24=MA07.COLUMN06 and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location  and isnull(FI10.COLUMN22,0)=@lotno and isnull(FI10.COLUMN23,0)=@Project
			left join MATABLE032 hs on hs.COLUMN02=m10.COLUMN75 and m10.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
		    left join MATABLE032 sc on sc.COLUMN02=m10.COLUMN75 and m10.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
		    --EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
			WHERE   isnull(MA07.COLUMNA13,'False')='False' and    (MA07.COLUMN05=@ItemID OR m10.COLUMN06=@ItemID)  and  MA07.COLUMNA03=@AcOwner
		end
		end	
		end	
	 else
	 begin
	 --EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
	 set @uomsetup=(@StockItem)
	IF NOT EXISTS(SELECT * FROM FITABLE039 MA07 inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 WHERE (MA07.COLUMN05=@StockItem  OR m10.COLUMN06=@StockItem) and MA07.COLUMNA03=@AcOwner and (MA07.COLUMN04=@OPUnit or MA07.COLUMN04 is null) and MA07.COLUMNA13=0 )
		BEGIN
		IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@StockItem and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @StockItem=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@StockItem and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @StockItem=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@StockItem=COLUMN09 from FITABLE043 Where COLUMN04=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
	    --EMPHCS1167 Service items calculation by srinivas 22/09/2015
		--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
		--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
		--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
         select MA07.COLUMN02,MA07.COLUMN50 COLUMN09, MA07.COLUMN29,(case when isnull(lt.COLUMN11,0)>0 then isnull(lt.COLUMN11,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else MA07.COLUMN17 end)COLUMN17,(case when isnull(lt.COLUMN12,0)>0 then isnull(lt.COLUMN12,0) when isnull(ss.COLUMN04,0)>0 then isnull(ss.COLUMN04,0) else MA07.COLUMN51 end)COLUMN51,FI10.COLUMN04,
MA07.COLUMN50 sdesc, MA07.COLUMN69 sQdesc,MA07.COLUMN57 pdesc, MA07.COLUMN68 pQdesc,MA07.COLUMN61,MA07.COLUMN63,MA07.COLUMN54,MA07.COLUMN45,MA07.COLUMN64 ,MA07.COLUMN48,MA07.COLUMN66,MA07.COLUMN65,
		 (case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty,MA07.COLUMN04 iName,iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT from MATABLE007 MA07 
		left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN02 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02=@OPUnit or ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and ia.COLUMNA13=0 
		left join MATABLE024 sp on sp.COLUMN07=MA07.COLUMN02 and sp.COLUMN06='Purchase' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0
	    left join MATABLE024 ss on ss.COLUMN07=MA07.COLUMN02 and ss.COLUMN06='Sales' and (isnull(ss.COLUMN03,0)=@OPUnit  or isnull(ss.COLUMN03,0)=0) and ss.COLUMNA03=@AcOwner and ss.COLUMNA13=0
	    left join FITABLE043 lt on lt.COLUMN02=@lotno and lt.COLUMN09=MA07.COLUMN02 and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=MA07.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
		left outer join 
		--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	 	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	 FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02   and FI10.COLUMN24=MA07.COLUMN06 and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then MA07.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location  and isnull(FI10.COLUMN22,0)=@lotno and FI10.COLUMN13=@OPUnit and isnull(FI10.COLUMN23,0)=@Project 
	 left join MATABLE032 hs on hs.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
	 left join MATABLE032 sc on sc.COLUMN02=MA07.COLUMN75 and MA07.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	 --EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
	 WHERE    isnull(MA07.COLUMN47,'False')='False' and  MA07.COLUMNA13=0 and ( MA07.COLUMN02=@StockItem or MA07.COLUMN06=@StockItem)  and  MA07.COLUMNA03=@AcOwner
	 --EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
        end
	else
		begin
		--EMPHCS1542 rajasekhar reddy patakota 27/01/2016 Upc Scan details fill in so,po transactions
			IF EXISTS(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@StockItem and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
			begin
			set @StockItem=(SELECT COLUMN05 FROM MATABLE021 WHERE COLUMN04=@StockItem and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
			end
		ELSE IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @StockItem=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
		else if exists(select COLUMN02 from FITABLE043 Where COLUMN04=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)
		begin
		select @lotno=COLUMN02,@StockItem=COLUMN09 from FITABLE043 Where COLUMN04=cast(@StockItem as nvarchar(250)) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
		set @lotno=(iif(@lotno is null or @lotno='',0,@lotno ))
		end
			select MA07.COLUMN05 COLUMN02,m10.COLUMN06,m10.COLUMN50 COLUMN09,MA07.COLUMN09 COLUMN17,m10.COLUMN45,m10.COLUMN29,MA07.COLUMN10 COLUMN51,m10.COLUMN50 sdesc, m10.COLUMN69 sQdesc,
			--EMPHCS1326 Rajasekhar reddy patakota 10/11/2015 Adding Lot No In Item Master
			--EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
			convert(float,convert(double precision,iif(cast(FI10.COLUMN08 as nvarchar)='',0,isnull(FI10.COLUMN08,0))))COLUMN08,m10.COLUMN57 pdesc, m10.COLUMN68 pQdesc,m10.COLUMN63,m10.COLUMN54,m10.COLUMN61,m10.COLUMN64,m10.COLUMN48,m10.COLUMN66,m10.COLUMN65,
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			(case when (@uom!='' and @uom is not null) then @uom  when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) units,ia.COLUMN07 qty, m10.COLUMN04 iName,iif(isnull(hs.COLUMN04,'')='',sc.COLUMN04,hs.COLUMN04) hsncode,@lotno LOT from FITABLE039 MA07
			inner join MATABLE007 m10 on m10.COLUMN02=MA07.COLUMN05 and m10.COLUMNA13=0 and isnull(m10.COLUMN47,'False')='False' and  m10.COLUMNA03=@AcOwner
			left join FITABLE043 lt on lt.COLUMN02=@lotno and lt.COLUMN09=m10.COLUMN02 and (isnull(lt.COLUMN10,0)=@OPUnit  or isnull(lt.COLUMN10,0)=0) and lt.COLUMNA03=m10.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 
			left join MATABLE021 ia on ia.COLUMN05=MA07.COLUMN05 and ia.COLUMN04=@uomsetup and (ia.COLUMNA02=@OPUnit or ia.COLUMNA02 is null) and ia.COLUMNA03=@AcOwner and ia.COLUMNA13=0 left outer join 
			--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
			FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN05 and FI10.COLUMN24=MA07.COLUMN06 and FI10.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom when (ia.COLUMN06='' or ia.COLUMN06 is null) then m10.COLUMN63 else ia.COLUMN06 end) and isnull(FI10.COLUMN21,0)=@location  and isnull(FI10.COLUMN22,0)=@lotno and FI10.COLUMN13=@OPUnit and isnull(FI10.COLUMN23,0)=@Project
			left join MATABLE032 hs on hs.COLUMN02=m10.COLUMN75 and m10.COLUMN05='ITTY001' and isnull(hs.COLUMNA13,0)=0 
		    left join MATABLE032 sc on sc.COLUMN02=m10.COLUMN75 and m10.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
		    --EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
			WHERE   isnull(MA07.COLUMNA13,'False')='False' and   (MA07.COLUMN05=@StockItem OR  m10.COLUMN06=@StockItem) and  MA07.COLUMNA03=@AcOwner
		end
		end	
END









GO
