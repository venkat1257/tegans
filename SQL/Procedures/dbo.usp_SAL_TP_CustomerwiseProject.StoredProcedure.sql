USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_CustomerwiseProject]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_SAL_TP_CustomerwiseProject]
(
	@CustomerID nvarchar(250)=null,@OPUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null
)

AS
BEGIN
set @OPUnit=(iif(@OPUnit='',0,isnull(@OPUnit,0)))
select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001 where COLUMN06=@CustomerID and (COLUMNA02=@OPUnit or isnull(COLUMNA02,0)=0)
 and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
GO
