USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_INVOICE_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_INVOICE_LINE_DATA]
(
	@SalesOrderID int= null,
	@Form varchar(50)
)

AS
BEGIN
   declare @tmpVal int
   declare @tmpVal1 int
   declare @billedqty int
   declare @ivlid int
   declare @billedqty1 int
   declare @IIID int
   declare @ITEMID int
   if(@Form='0')
   BEGIN
 --set @IIID=(select column02 from SATABLE007 where  COLUMN06=cast(@SalesOrderID as nvarchar));

   set @tmpVal=isnull((select sum(COLUMN12) from SATABLE006 WHERE COLUMN19 = (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)),0)
   set @tmpVal1=isnull((select sum(COLUMN13) from SATABLE006 WHERE COLUMN19 = (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)),0)
   set @billedqty1=isnull(((select sum(COLUMN09) from SATABLE010 where COLUMNA13=0 and COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast(@SalesOrderID as nvarchar) ) )),0)

 --  if(@billedqty1>0)
 --  begin 
 --  SELECT distinct isnull(d.COLUMN02,0) as COLUMN04, a.COLUMN04 as COLUMN05,a.COLUMN05 as COLUMN06 , 
	--max( a.COLUMN06) as COLUMN07,max(a.COLUMN07) as COLUMN08,isnull(sum(b.COLUMN10),0) as COLUMN09,
	--(cast(avg(a.COLUMN09)as int)- isnull(sum(b.COLUMN10),0)) as COLUMN10,
	--isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN04),0) as COLUMN11,0 as COLUMN12,
	--cast(avg(a.COLUMN12)as int) as COLUMN13,cast(((avg(a.COLUMN09)- isnull(sum(b.COLUMN10),0))*avg(a.COLUMN12))as int) as COLUMN14
	--FROM dbo.SATABLE008 a inner join SATABLE007 d on d.COLUMN01=a.COLUMN14 
	----inner join FITABLE010 f on a.COLUMN04=f.COLUMN03 
	-- left outer join SATABLE010 b on b.COLUMN04 =d.COLUMN02 and a.COLUMN04=b.COLUMN05
	--WHERE a.COLUMN14 in(SELECT COLUMN01 FROM dbo.SATABLE007 WHERE  COLUMN06=@SalesOrderID) group by d.COLUMN02,a.COLUMN04 ,a.COLUMN05

 --  end
 --  elsebeginend
   
   if(@tmpVal=0 and @tmpVal1=0)
   begin
    SELECT  0 as COLUMN04,a.COLUMN03 as COLUMN05,a.COLUMN05 as COLUMN06,a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08,isnull( a.COLUMN13,0) as COLUMN09,
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	 (select case when (a.COLUMN07)>isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN03 and a.COLUMNA02=f.COLUMN13  and a.COLUMN27=f.COLUMN19 AND isnull(f.COLUMN21,0)=isnull(b.COLUMN48,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))),0)
	 then isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN03  and a.COLUMNA02=f.COLUMN13  and a.COLUMN27=f.COLUMN19 AND isnull(f.COLUMN21,0)=isnull(b.COLUMN48,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))),0) else
	 (a.COLUMN07) end) as COLUMN10,isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN03 and a.COLUMNA02=f.COLUMN13  and a.COLUMN27=f.COLUMN19 AND isnull(f.COLUMN21,0)=isnull(b.COLUMN48,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))),0) as COLUMN11,
	 (select case when (a.COLUMN07)>isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN03 and a.COLUMNA02=f.COLUMN13  and a.COLUMN27=f.COLUMN19 AND isnull(f.COLUMN21,0)=isnull(b.COLUMN48,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))),0)
	 then isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN03 and a.COLUMNA02=f.COLUMN13  and a.COLUMN27=f.COLUMN19 AND isnull(f.COLUMN21,0)=isnull(b.COLUMN48,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))),0) else
	 (a.COLUMN07) end) as COLUMN12,a.COLUMN09 as COLUMN13,a.COLUMN07*a.COLUMN09 as COLUMN14,a.COLUMN26 COLUMN21
	 --EMPHCS1167 Service items calculation by srinivas 22/09/2015
	 --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	 ,((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast(a.COLUMN07*a.COLUMN09-isnull(a.COLUMN10,0) as decimal(18,2)))) COLUMN23,isnull(a.COLUMN10,0) COLUMN19,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 ,a.COLUMN17 as COLUMN31,a.COLUMN02 LineID,a.COLUMN37,a.COLUMN38,hs.COLUMN04 hsncode
	 FROM dbo.SATABLE006  a inner join SATABLE005 b on b.COLUMN01=a.COLUMN19 and isnull(b.columna13,0)=0
	 --EMPHCS924 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice 
	 left outer join FITABLE010 f on a.COLUMN03=f.COLUMN03 and a.COLUMNA02=f.COLUMN13  and a.COLUMN27=f.COLUMN19 AND isnull(f.COLUMN21,0)=iif((isnull(a.COLUMN38,0)!=0 and a.COLUMN38!=''),a.COLUMN38,isnull(b.COLUMN48,0)) AND isnull(f.COLUMN22,0)=iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))
	 --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	  left join MATABLE013 m on  m.column02=a.COLUMN21
	  inner join MATABLE007 m7 on  m7.column02=a.COLUMN03
	  left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
      --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	 WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID and isnull(columna13,0)=0)
   end
   else if(@tmpVal>0 and @tmpVal1=0)
   begin
	 SELECT distinct d.COLUMN02 as COLUMN04, a.COLUMN04 as COLUMN05,a.COLUMN05 as COLUMN06 , 
	a.COLUMN06 as COLUMN07,a.COLUMN07 as COLUMN08,0 as COLUMN09,
	(a.COLUMN09) as COLUMN10,
	--isnull((select COLUMN08 from FITABLE010 where COLUMN03=a.COLUMN04  and columna02=a.columna02),0) as COLUMN11,
	f.COLUMN08 COLUMN11,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	(a.COLUMN09) as COLUMN12,c.COLUMN09 as COLUMN13,cast((a.COLUMN09)*a.COLUMN12 as decimal(18,2)) as COLUMN14,c.COLUMN26 COLUMN21,
	cast((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast(((a.COLUMN09)*a.COLUMN12)-isnull((c.COLUMN10),0) as decimal(18,2))) as decimal(18,2)) COLUMN23
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
	--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	,isnull((c.COLUMN10),0)COLUMN19,a.COLUMN19 as COLUMN22,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
    ,a.COLUMN24 as COLUMN31,a.COLUMN26 LineID,c.COLUMN37,a.COLUMN28 COLUMN38,hs.COLUMN04 hsncode
	     --EMPHCS903 rajasekhar reddy patakota 10/8/2015 After deleting two lines from item issue II00040 ,system deleted only one line and invoice it is showing all 3 lines
	FROM dbo.SATABLE008 a inner join SATABLE007 d on d.COLUMN01=a.COLUMN14 and  a.columna13=0 and d.COLUMN06 in(@SalesOrderID)
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	  left outer join FITABLE010 f on a.COLUMN04=f.COLUMN03 and a.COLUMNA03=f.COLUMNA03 and a.COLUMNA02=f.COLUMNA02 and a.COLUMN20=f.COLUMN19 AND isnull(f.COLUMN21,0)=iif((isnull(a.COLUMN28,0)!=0 and a.COLUMN28!=''),a.COLUMN28,isnull(d.COLUMN23,0)) AND isnull(f.COLUMN22,0)=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	  inner join SATABLE006 c on  iif(isnull(a.COLUMN26,0)=0,0,isnull(c.COLUMN02,0))=isnull(a.COLUMN26,0) and c.COLUMN19=(SELECT COLUMN01 FROM dbo.
	  --EMPHCS857 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in invoice
	  --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
  SATABLE005 WHERE COLUMN02 in(@SalesOrderID)) and c.COLUMN03=a.COLUMN04 and isnull(c.columna13,0)=0   and c.column09=a.column12  and c.COLUMN27=a.COLUMN19 and iif(c.COLUMN17='',0,isnull(c.COLUMN17,0))=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0)) and  c.columna13=0   
  --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
  left join MATABLE013 m on  m.column02=c.COLUMN26
	  inner join MATABLE007 m7 on  m7.column02=a.COLUMN04
	  left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0  order by a.COLUMN26 asc

      --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
   end
   else if(@tmpVal=0 and @tmpVal1>0)
   begin
   SELECT  0 as COLUMN04,a.COLUMN05 as COLUMN05,max(a.COLUMN06) as COLUMN06,max(a.COLUMN07) as COLUMN07 , 
	 max(a.COLUMN08) as COLUMN08, max(a.COLUMN09) as COLUMN09,
	 --EMPHCS924 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice 
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	 (select case when min(a.COLUMN08-a.COLUMN09)>isnull((select COLUMN08 from FITABLE010 where COLUMN03=a.COLUMN05 and a.COLUMNA02=COLUMNA02 and a.COLUMN24=COLUMN19 AND isnull(COLUMN21,0)=isnull(b.COLUMN39,0) AND isnull(COLUMN22,0)=isnull(a.COLUMN31,0)),0)
	 then isnull((select COLUMN08 from FITABLE010 where COLUMN03=a.COLUMN05 and a.COLUMNA02=COLUMNA02 and a.COLUMN24=COLUMN19 AND isnull(COLUMN21,0)=isnull(b.COLUMN39,0) AND isnull(COLUMN22,0)=isnull(a.COLUMN31,0)),0) else
	 min(a.COLUMN08-a.COLUMN09) end) as COLUMN10,isnull((select COLUMN08 from FITABLE010 where COLUMN03=a.COLUMN05 and a.COLUMNA02=COLUMNA02 and a.COLUMN24=COLUMN19 AND isnull(COLUMN21,0)=isnull(b.COLUMN39,0) AND isnull(COLUMN22,0)=isnull(a.COLUMN31,0)),0) as COLUMN11,
	 	 (select case when min(a.COLUMN08-a.COLUMN09)>isnull((select COLUMN08 from FITABLE010 where COLUMN03=a.COLUMN05 and a.COLUMNA02=COLUMNA02 and a.COLUMN24=COLUMN19 AND isnull(COLUMN21,0)=isnull(b.COLUMN39,0) AND isnull(COLUMN22,0)=isnull(a.COLUMN31,0)),0)
	 then isnull((select COLUMN08 from FITABLE010 where COLUMN03=a.COLUMN05 and a.COLUMNA02=COLUMNA02 and a.COLUMN24=COLUMN19 AND isnull(COLUMN21,0)=isnull(b.COLUMN39,0) AND isnull(COLUMN22,0)=isnull(a.COLUMN31,0)),0) else
	 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 min(a.COLUMN08-a.COLUMN09) end) as COLUMN12,a.COLUMN13 as COLUMN13,min((a.COLUMN08-a.COLUMN09)*a.COLUMN09) as COLUMN14,a.COLUMN21
	 --EMPHCS1167 Service items calculation by srinivas 22/09/2015
	 --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	 ,((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast(min((a.COLUMN08-a.COLUMN09)*a.COLUMN09)-isnull(max(a.COLUMN19),0) as decimal(18,2)))) COLUMN23,isnull(max(a.COLUMN19),0)COLUMN19,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 ,a.COLUMN31,a.COLUMN36 LineID,a.COLUMN37,a.COLUMN38,hs.COLUMN04 hsncode
	 FROM dbo.SATABLE010  a inner join SATABLE009 b on b.COLUMN06=cast( @SalesOrderID as nvarchar)
	  --inner join FITABLE010 f on a.COLUMN03=f.COLUMN03 
	  --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	   left join MATABLE013 m on  m.column02=a.COLUMN21
	   inner join MATABLE007 m7 on  m7.column02=a.COLUMN05
	   left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
       --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	  --EMPHCS924 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice 
	  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 WHERE a.COLUMNA13=0 and a.COLUMN15 in (SELECT COLUMN01 FROM dbo.SATABLE009 WHERE COLUMN06 in (cast( @SalesOrderID as nvarchar))) 
	 group by a.COLUMN05,a.COLUMN21,m.column07,a.COLUMNA02,a.COLUMN24,m7.COLUMN48,a.COLUMN31,b.COLUMN39,a.COLUMN36 ,a.COLUMN37,a.COLUMN13,a.COLUMN38,hs.COLUMN04  order by a.COLUMN36 asc
   end

   else if(@tmpVal>0 and @tmpVal1>0 and @tmpVal!=@tmpVal1)
   begin
  SELECT  b.COLUMN02 as COLUMN04,c.COLUMN04 as COLUMN05,max(c.COLUMN05) as COLUMN06,max(c.COLUMN06) as COLUMN07 , 
 max(c.COLUMN07) as COLUMN08, max(isnull(a.COLUMN08,0)) as COLUMN09,((c.COLUMN09) -sum( isnull(d.COLUMN10,0))) as COLUMN10,
 --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
((c.COLUMN09) -sum( isnull(d.COLUMN10,0))) as COLUMN12,max(f.COLUMN08) as COLUMN11,a.COLUMN09 COLUMN13,
((c.COLUMN09) -sum( isnull(d.COLUMN10,0)))*max(c.COLUMN12) as COLUMN14,a.COLUMN26 AS COLUMN21,c.COLUMN17 as COLUMN19
--EMPHCS1167 Service items calculation by srinivas 22/09/2015
--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
,((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast(((c.COLUMN09) -sum( isnull(d.COLUMN10,0)))*max(c.COLUMN12)-isnull(c.COLUMN17,0) as decimal(18,2)))) COLUMN23,a.COLUMN27 as COLUMN22,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
,c.COLUMN24 as COLUMN31,c.COLUMN26 LineID,a.COLUMN37,c.COLUMN28 COLUMN38,hs.COLUMN04 hsncode
--EMPHCS857 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in invoice
FROM SATABLE008 c inner join SATABLE007 b on b.COLUMN01=c.COLUMN14 and b.COLUMN06 in(@SalesOrderID)   and c.COLUMNA13=0
--left  join SATABLE010 d  on   c.COLUMN03=d.COLUMN04
--EMPHCS924 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice 
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
 left outer join FITABLE010 f on c.COLUMN04=f.COLUMN03 and c.COLUMNA03=f.COLUMNA03 and c.COLUMNA02=f.COLUMNA02  and c.COLUMN20=f.COLUMN19 AND isnull(f.COLUMN21,0)=iif((isnull(c.COLUMN28,0)!=0 and c.COLUMN28!=''),c.COLUMN28,isnull(b.COLUMN23,0)) AND isnull(f.COLUMN22,0)=iif(c.COLUMN24='',0,isnull(c.COLUMN24,0))
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
inner join dbo.SATABLE006 a  on iif(isnull(c.COLUMN26,0)=0,0,isnull(a.COLUMN02,0))=isnull(c.COLUMN26,0) and a.COLUMN19 in(SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID) and a.COLUMN03=c.COLUMN04 and a.COLUMN27=c.COLUMN19 and iif(a.COLUMN17='',0,isnull(a.COLUMN17,0))=iif(c.COLUMN24='',0,isnull(c.COLUMN24,0))
left outer join SATABLE010 d  on iif(isnull(c.COLUMN26,0)=0,0,isnull(d.COLUMN36,0))=isnull(c.COLUMN26,0) and d.COLUMN04=b.COLUMN02 and d.COLUMNA13=0 and c.COLUMN04=d.COLUMN05 and d.COLUMN22=c.COLUMN19 and d.COLUMN31=c.COLUMN24
left join MATABLE013 m on  m.column02=a.COLUMN26
--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	  inner join MATABLE007 m7 on  m7.column02=a.COLUMN03
	  left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
      --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
--EMPHCS857 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in invoice
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
group by b.COLUMN02,c.COLUMN04,m.column07,a.COLUMN26,a.COLUMN27,c.COLUMN17,a.COLUMN19,c.COLUMN09,d.COLUMNA13,m7.COLUMN48,c.COLUMN24,a.COLUMN37,a.COLUMN09,c.COLUMN28
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			,c.COLUMN26,hs.COLUMN04
			having  (max(c.COLUMN09) -sum( isnull(d.COLUMN10,0)))>0 
			order by c.COLUMN26 asc
			end
   else
   begin
	 --SELECT  b.COLUMN02 as COLUMN04,a.COLUMN03 as COLUMN05, a.COLUMN05 as COLUMN06, a.COLUMN15 as COLUMN07 , 
	 --a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09, ( c.COLUMN09) as COLUMN10,
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	 -- ( c.COLUMN09) as COLUMN12, a.COLUMN09 as COLUMN13, (c.COLUMN09)*a.COLUMN09 as COLUMN14
	 --FROM dbo.SATABLE006  a inner join  SATABLE007 b on b.COLUMN06=cast(@SalesOrderID as nvarchar)
	 --inner join SATABLE008 c on c.COLUMN14=b.COLUMN01
	 ----inner join FITABLE010 f on c.COLUMN04=f.COLUMN03 
	 --and c.COLUMN04=a.COLUMN03
	 --WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)  
	 SELECT   d.COLUMN02 as COLUMN04, a.COLUMN04 as COLUMN05, a.COLUMN05 as COLUMN06,a.COLUMN06 as COLUMN07,
	 cast(avg(cast(a.COLUMN07 as int))as int) as COLUMN08  ,
	isnull(cast(max(b.COLUMN09)as int),0) as COLUMN09,min(a.COLUMN09-isnull(b.COLUMN09,0)) as COLUMN10,
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN04 and f.columna02=a.columna02 AND isnull(f.COLUMN21,0)=isnull(d.COLUMN23,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))),0) as COLUMN11,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	min(a.COLUMN09-isnull(b.COLUMN09,0)) as COLUMN12,e.COLUMN09 as COLUMN13,
	cast(min((a.COLUMN09-isnull(b.COLUMN09,0))*a.COLUMN12) as int) as COLUMN14,b.COLUMN21
	,((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast(cast(min((a.COLUMN09-isnull(b.COLUMN09,0))*a.COLUMN12) as int)-isnull(max(b.COLUMN19),0) as decimal(18,2)))) COLUMN23
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
	,isnull(max(b.COLUMN19),0)COLUMN19,a.COLUMN19 as COLUMN22,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	,a.COLUMN24 as COLUMN31,a.COLUMN26 LineID,b.COLUMN37,a.COLUMN28 COLUMN38,hs.COLUMN04 hsncode
	 FROM  dbo.SATABLE008 a 
	inner join SATABLE007 d on d.COLUMN06=@SalesOrderID  and d.COLUMN01=a.COLUMN14
	left outer join SATABLE009 c on  c.COLUMN06=@SalesOrderID 
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	left outer join SATABLE010 b on iif(isnull(a.COLUMN26,0)=0,0,isnull(b.COLUMN36,0))=isnull(a.COLUMN26,0) and b.COLUMN15 =c.COLUMN01 and b.COLUMNA13=0 and d.COLUMN02=b.COLUMN04 and b.COLUMN05=a.COLUMN04 and b.COLUMN22=a.COLUMN19 and isnull(b.COLUMN31,0)=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	left outer join SATABLE006 e on iif(isnull(a.COLUMN26,0)=0,0,isnull(a.COLUMN26,0))=isnull(e.COLUMN02,0)  and b.COLUMNA13=0 and a.COLUMN04=e.COLUMN03 and e.COLUMN27=a.COLUMN19 and isnull(e.COLUMN17,0)=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
    left join MATABLE013 m on  m.column02=b.COLUMN21
	  inner join MATABLE007 m7 on  m7.column02=a.COLUMN04
	   left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
      --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	group by d.COLUMN02,a.COLUMN03,a.COLUMN04,a.COLUMN05,a.COLUMN06,a.COLUMNA02,b.COLUMN21,m.column07,a.COLUMN19,m7.COLUMN48,a.COLUMN24,d.COLUMN23,b.COLUMN37,e.COLUMN09,a.COLUMN28
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	,a.COLUMN26,hs.COLUMN04 having  (min(a.COLUMN09-isnull(b.COLUMN09,0)))>0
	order by a.COLUMN26 asc
END
end

else
Begin
 --set @IIID=(select column02 from SATABLE007 where COLUMN04=@Form and COLUMN06=cast(@SalesOrderID as nvarchar));
set @billedqty=(select max(COLUMN10) from SATABLE010 where  COLUMNA13=0 and COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast(@SalesOrderID as nvarchar) and columna13=0 ) and COLUMN04 in(select column02 from SATABLE007 where COLUMN04=@Form and COLUMN06=cast(@SalesOrderID as nvarchar)and columna13=0 ) )
set @ivlid=(select distinct max(column15) from SATABLE010 where COLUMNA13=0 and COLUMN15 in(select COLUMN01 from SATABLE009 where COLUMN06=cast(@SalesOrderID as nvarchar)and columna13=0  )  and COLUMN04 in(select column02 from SATABLE007 where COLUMN04=@Form and COLUMN06=cast(@SalesOrderID as nvarchar)and columna13=0 ) )
--EMPHCS903 rajasekhar reddy patakota 10/8/2015 After deleting two lines from item issue II00040 ,system deleted only one line and invoice it is showing all 3 lines
if(@billedqty>0)
begin  
    SELECT distinct d.COLUMN02 as COLUMN04, a.COLUMN04 as COLUMN05,a.COLUMN05 as COLUMN06 , 
	a.COLUMN06 as COLUMN07,max(a.COLUMN07) as COLUMN08,isnull(max(b.COLUMN09),0) as COLUMN09,
	--EMPHCS924 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice 
	((isnull(a.COLUMN09,0))-sum( isnull(b.COLUMN10,0))) as COLUMN10,isnull((select f.COLUMN08 from FITABLE010 f where f.COLUMN03=a.COLUMN04  and f.columna02=a.columna02  and a.COLUMN20=f.COLUMN19  AND isnull(f.COLUMN21,0)=isnull(d.COLUMN23,0) AND isnull(f.COLUMN22,0)=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))),0) as COLUMN11,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	((isnull(a.COLUMN09,0))-sum( isnull(b.COLUMN10,0))) as COLUMN12,e.COLUMN09 as COLUMN13,cast((((isnull(a.COLUMN09,0))-sum( isnull(b.COLUMN10,0)))*a.COLUMN12) as decimal(18,2)) as COLUMN14,e.COLUMN26 COLUMN21,
	cast((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast((((isnull(a.COLUMN09,0))-sum( isnull(b.COLUMN10,0)))*a.COLUMN12-isnull((b.COLUMN19),0)) as decimal(18,2))) as decimal(18,2)) COLUMN23
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
	--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	,cast(isnull((b.COLUMN19),0) as decimal(18,2))COLUMN19,a.COLUMN19 as COLUMN22,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN24 as COLUMN31,a.COLUMN26 LineID,b.COLUMN37,a.COLUMN28 COLUMN38,hs.COLUMN04 hsncode
	 --EMPHCS903 rajasekhar reddy patakota 10/8/2015 After deleting two lines from item issue II00040 ,system deleted only one line and invoice it is showing all 3 lines
	FROM dbo.SATABLE008 a left join SATABLE007 d on d.COLUMN01=a.COLUMN14 and a.COLUMNA13=0
	 -- inner join FITABLE010 f on a.COLUMN04=f.COLUMN03
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	left join SATABLE010 b on iif(isnull(a.COLUMN26,0)=0,0,isnull(b.COLUMN36,0))=isnull(a.COLUMN26,0) and b.COLUMN04 =d.COLUMN02  and b.COLUMNA13=0 and b.column05=a.COLUMN04 and b.COLUMN22=a.COLUMN19 and b.COLUMN31=a.COLUMN24
	left join SATABLE009 c on c.COLUMN01 =b.COLUMN15 and b.COLUMNA13=0 and a.COLUMN04=b.COLUMN05  and c.COLUMN06=d.COLUMN06
	inner join SATABLE005 g on g.COLUMN02=d.COLUMN06 and isnull(g.columna13,0)=0 inner join SATABLE006 e on e.COLUMN19=g.COLUMN01 and e.COLUMN03=a.COLUMN04  and e.COLUMN27=a.COLUMN19 and iif(e.COLUMN17='',0,isnull(e.COLUMN17,0))=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	left join MATABLE013 m on  m.column02=e.COLUMN26
	  inner join MATABLE007 m7 on  m7.column02=a.COLUMN04
	   left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
      --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	WHERE  a.COLUMN14 in(SELECT COLUMN01 FROM dbo.SATABLE007 WHERE COLUMN04= @Form and COLUMN06=cast(@SalesOrderID as nvarchar)) 
	group by a.COLUMN05,d.COLUMN02,a.COLUMN04,a.COLUMN06,a.COLUMN09,a.COLUMNA02,a.COLUMN12,
	--EMPHCS857 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in invoice
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	b.COLUMN21,m.COLUMN07,a.COLUMN19,b.COLUMN19,e.COLUMN26,a.COLUMN09,a.COLUMN20,b.COLUMNA13,m7.COLUMN48,a.COLUMN24,d.COLUMN23,a.COLUMN26,b.COLUMN37,e.COLUMN09,a.COLUMN28,hs.COLUMN04 having ((isnull(a.COLUMN09,0))-sum( isnull(b.COLUMN10,0)))>0 
	order by a.COLUMN26 asc
   end
   else
   begin
   SELECT  d.COLUMN02 as COLUMN04, a.COLUMN04 as COLUMN05,max(a.COLUMN05) as COLUMN06 , 
	max(a.COLUMN06) as COLUMN03,max(a.COLUMN07) as COLUMN08,0 as COLUMN09,
	max(a.COLUMN09) as COLUMN10,max(isnull((b.COLUMN08),0)) as COLUMN11,
	max(a.COLUMN09) as COLUMN12,(e.COLUMN09) as COLUMN13,cast(max((a.COLUMN09)*(a.COLUMN12)) as decimal(18,2)) as COLUMN14,e.COLUMN26 COLUMN21,
	cast((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*(cast(max((a.COLUMN09)*(a.COLUMN12)) as decimal(18,2))-isnull(max(e.COLUMN10),0)) as decimal(18,2)) COLUMN23
	--EMPHCS1167 Service items calculation by srinivas 22/09/2015
	--EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	,isnull(max(e.COLUMN10),0)COLUMN19,a.COLUMN19 as COLUMN22,m7.COLUMN48 as COLUMN48,isnull(m.column07,0) tax
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	,a.COLUMN24 as COLUMN31,a.COLUMN26 LineID,e.COLUMN37,a.COLUMN28 COLUMN38,hs.COLUMN04 hsncode
	 FROM dbo.SATABLE008 a inner join SATABLE007 d on d.COLUMN01 =a.COLUMN14
	--EMPHCS924 rajasekhar reddy patakota 12/8/2015 uom condition checking in Invoice 
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
	left outer join FITABLE010 b on a.COLUMN04=b.COLUMN03 and b.COLUMNA02=d.COLUMN15  and a.COLUMN20=b.COLUMN19 AND isnull(b.COLUMN21,0)=iif((isnull(a.COLUMN28,0)!=0 and a.COLUMN28!=''),a.COLUMN28,isnull(d.COLUMN23,0)) AND isnull(b.COLUMN22,0)=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	--EMPHCS857 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in invoice
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	inner join SATABLE005 c on c.COLUMN02=d.COLUMN06 and isnull(c.columna13,0)=0 inner join SATABLE006 e on iif(isnull(a.COLUMN26,0)=0,0,isnull(e.COLUMN02,0))=isnull(a.COLUMN26,0) and e.COLUMN19=c.COLUMN01  and  e.columna13=0 and e.COLUMN03=a.COLUMN04 and e.COLUMN27=a.COLUMN19 and iif(e.COLUMN17='',0,isnull(e.COLUMN17,0))=iif(a.COLUMN24='',0,isnull(a.COLUMN24,0))
	 --inner join SATABLE006 e on e.COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02=@SalesOrderID)
	 --EMPHCS1227 rajasekhar reddy patakota 05/10/2015 FIELDS COUNT SHOULD GET DECREMENTED IN BILL AND INVOICE
	 left join MATABLE013 m on  m.column02=e.column26
	  inner join MATABLE007 m7 on  m7.column02=a.COLUMN04
	   left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75  and isnull(hs.COLUMNA13,0)=0 
      --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0
	--EMPHCS903 rajasekhar reddy patakota 10/8/2015 After deleting two lines from item issue II00040 ,system deleted only one line and invoice it is showing all 3 lines
	WHERE a.columna13=0 and  a.COLUMN14 in (SELECT COLUMN01 FROM dbo.SATABLE007 WHERE COLUMN04 in( @Form) and COLUMN06 in(cast(@SalesOrderID as nvarchar)))
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   group by d.COLUMN02,a.COLUMN04,e.COLUMN26,m.column07,a.COLUMN19,m7.COLUMN48,a.COLUMN12,a.COLUMN24,a.COLUMN26,e.COLUMN37,e.COLUMN09,a.COLUMN28,hs.COLUMN04
   order by a.COLUMN26 asc
   end
end
END








GO
