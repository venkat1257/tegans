USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PROC_TP_LotWisePrice]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PROC_TP_LotWisePrice]
(
	@ItemID nvarchar(250)=NULL,@UOM nvarchar(250)=NULL,@Lot nvarchar(250)=NULL,@location nvarchar(250)=NULL,@FrmID nvarchar(250)=NULL,@DateF nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL,@Project nvarchar(250)=NULL
)
AS
BEGIN
set @DateF=(iif(isnull(@DateF,'')='','dd/MM/yyyy',@DateF))
set @UOM=(iif(isnull(@UOM,'')='','0',@UOM))
set @location=(iif(isnull(@location,'')='','0',@location))
set @Project=(iif(isnull(@Project,'')='','0',@Project))
if(@Lot!='' and @Lot!='0')
begin
select b.COLUMN02,b.COLUMN04,(case when ISNULL(c.COLUMN04,0)=1 then 1 else cast(isnull(FI10.COLUMN04,0) as decimal(18,2)) end) AvailQty,
(case when isnull(b.COLUMN11,0)>0 then isnull(b.COLUMN11,0) when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(MA07.COLUMN17,0) end)PPrice,
(case when isnull(b.COLUMN12,0)>0 then isnull(b.COLUMN12,0) when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(MA07.COLUMN51,0) end)SPrice,format(b.COLUMN05,@DateF) 'ExpDate'
FROM FITABLE043 b
left join MATABLE024 sp on sp.COLUMN07=b.COLUMN09 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=b.COLUMNA03 and sp.COLUMNA13=0 
left join MATABLE024 pp on pp.COLUMN07=b.COLUMN09 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=b.COLUMNA03 and pp.COLUMNA13=0 
left join MATABLE007 MA07 on MA07.COLUMN02=b.COLUMN09 and MA07.COLUMNA03=b.COLUMNA03 and isnull(MA07.COLUMNA13,0)=0 
left join FITABLE010 FI10 on FI10.COLUMN03=MA07.COLUMN02 and (FI10.COLUMN13=@OPUnit or FI10.COLUMN13 is null) and FI10.COLUMN19=(case when (@UOM!='' and @UOM is not null) then @UOM else 10000 end) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@Lot
and isnull(FI10.COLUMN23,0)=@Project
left join CONTABLE031 c on c.COLUMNA03=b.COLUMNA03 and ISNULL(c.COLUMNA13,0)=0 AND c.COLUMN03='Allow BackOrder'
where b.COLUMN02=@Lot  And b.COLUMN09=@ItemID  And (b.COLUMNA02=@OPUnit or b.COLUMNA02 is null) AND b.COLUMNA03=@AcOwner  and isnull(b.COLUMNA13,0)=0
end
else
begin
select b.COLUMN02,b.COLUMN04,(case when ISNULL(c.COLUMN04,0)=1 then 1 else cast(isnull(FI10.COLUMN04,0) as decimal(18,2)) end) AvailQty,
(case when isnull(pp.COLUMN04,0)>0 then isnull(pp.COLUMN04,0) else isnull(b.COLUMN17,0) end)PPrice,
(case when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(b.COLUMN51,0) end)SPrice,format(getdate(),@DateF) 'ExpDate'
FROM MATABLE007 b
left join MATABLE024 sp on sp.COLUMN07=b.COLUMN02 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0)=@OPUnit  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=b.COLUMNA03 and sp.COLUMNA13=0 
left join MATABLE024 pp on pp.COLUMN07=b.COLUMN02 and pp.COLUMN06='Purchase' and (isnull(pp.COLUMN03,0)=@OPUnit  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03=b.COLUMNA03 and pp.COLUMNA13=0 
left join FITABLE010 FI10 on FI10.COLUMN03=b.COLUMN02 and (FI10.COLUMN13=@OPUnit or FI10.COLUMN13 is null) and FI10.COLUMN19=(case when (@UOM!='' and @UOM is not null) then @UOM else 10000 end) and isnull(FI10.COLUMN21,0)=@location and isnull(FI10.COLUMN22,0)=@Lot
and isnull(FI10.COLUMN23,0)=@Project
left join CONTABLE031 c on c.COLUMNA03=b.COLUMNA03 and ISNULL(c.COLUMNA13,0)=0 AND c.COLUMN03='Allow BackOrder'
where b.COLUMN02=@ItemID And (b.COLUMNA02=@OPUnit or b.COLUMNA02 is null) AND b.COLUMNA03=@AcOwner  and isnull(b.COLUMNA13,0)=0
END
END






GO
