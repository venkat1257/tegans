USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ReturnsGetReports]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ReturnsGetReports]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand       nvarchar(250)= null,
@Location    nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@DateF nvarchar(250)=null)
as 
begin
DECLARE @PERECENT DECIMAL(18,2)
IF  @ReportName='DebitMemoBySuppliersSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #ReturnsSuppliersDetails from (
select o.COLUMN03 as OPUnit,(case when p.COLUMN50='22335' then s2.COLUMN05 else s.COLUMN05 end) Vendor,FORMAT(p.COLUMN06,@DateF) Date,p.COLUMN06 Dtt,sum(f.COLUMN07) Qty,sum(f.COLUMN24) Amount,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand ,c1.COLUMN04 Location,c1.COLUMN02 LocationID
--EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
,p.COLUMN04 Trans# ,p.COLUMN03 fid  , p.COLUMN05 v , @FromDate fd , @ToDate td  from PUTABLE001 p 
left outer join PUTABLE002 f on f.COLUMN19=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN24 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and  isnull(s2.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03 
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355  and p.COLUMN06 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner group by m5.COLUMN02,m5.COLUMN04,c1.COLUMN04,c1.COLUMN02,p.COLUMN05,o.COLUMN03,s.COLUMN05,p.COLUMN06,p.COLUMN12,p.COLUMN24,p.COLUMN04  ,p.COLUMN03,p.COLUMN50,s2.COLUMN05  ) Query

set @Query1='select * from #ReturnsSuppliersDetails'+@whereStr+' order by Dtt desc'
exec (@Query1) 
end
Else if @ReportName='DebitMemoBySuppliersDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #ReturnsSuppliersSummary from (
select o.COLUMN03 as OPUnit,(case when p.COLUMN50='22335' then s2.COLUMN05 else s.COLUMN05 end) Vendor,FORMAT(p.COLUMN06,@DateF) Date,p.COLUMN06 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN09 memo,f.COLUMN07 Qty,f.COLUMN09 rate,f.COLUMN24 Amount,p.COLUMN05 VID,p.COLUMN05 CID,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand,c1.COLUMN04 Location,c1.COLUMN02 LocationID
--EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
				,p.COLUMN03 fid from PUTABLE001 p 
left outer join PUTABLE002 f on f.COLUMN19=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN24 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and  isnull(s2.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03 
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355 and p.COLUMN06 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND P.COLUMNA03=@AcOwner) Query
set @Query1='select * from #ReturnsSuppliersSummary'+@whereStr+' order by Dtt desc'
exec (@Query1) 
END
Else IF  @ReportName='CreditMemoByCustomerSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #ReturnsCustomerSummary from (
SELECT  o.COLUMN03 ou,(case when a.COLUMN51='22305' then s1.COLUMN05 else s.COLUMN05 end) customer,FORMAT(cast( a.COLUMN06 as date),@DateF)dt,a.COLUMN06 Dtt,c.COLUMN04 TrnsType, m.COLUMN04 itm ,a.COLUMN04 Creditnum ,b.COLUMN06 descc,b.COLUMN07 qty ,b.COLUMN09 price  ,b.COLUMN25 amont,a.COLUMN05 CID,a.COLUMN24 OPID,m5.COLUMN04 Brand 
, isnull(m.COLUMN10,0) BrandID  ,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,a.COLUMN04 Trans# ,a.COLUMN03 fid   , a.COLUMN05 c , @FromDate fd , @ToDate td  FROM SATABLE005 a 
inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 
left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN24 
left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03
left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03
left outer join CONTABLE0010 c on c.COLUMN02=a.COLUMN03 
left outer join MATABLE007 m on m.COLUMN02=b.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0  
left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
where isnull(a.COLUMNA13,0)=0 and a.COLUMN03=1330
 and a.COLUMN06 between @FromDate and @ToDate and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner) Query
set @Query1='select * from #ReturnsCustomerSummary'+@whereStr+' order by Dtt desc'
exec (@Query1) 
END
Else if @ReportName='CreditMemoByCustomerDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #ReturnsCustomerDetails from (
SELECT  o.COLUMN03 ou,(case when a.COLUMN51='22305' then s1.COLUMN05 else s.COLUMN05 end) customer,FORMAT(cast( a.COLUMN06 as date),@DateF) dt, a.COLUMN06 Dtt,c.COLUMN04 TrnsType, m.COLUMN04 itm ,a.COLUMN04 Creditnum ,b.COLUMN06 descc,b.COLUMN07 qty ,b.COLUMN09 price  ,b.COLUMN25 amont,a.COLUMN05 CID,a.COLUMN05 VID,a.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand ,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,a.COLUMN04 Trans# ,a.COLUMN03 fid  FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN24 left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03 left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03 left outer join CONTABLE0010 c on c.COLUMN02=a.COLUMN03 left outer join MATABLE007 m on m.COLUMN02=b.COLUMN03 left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0 
left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
 where isnull(a.COLUMNA13,0)=0 and a.COLUMN03=1330
 and a.COLUMN06 between @FromDate and @ToDate and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner) Query
set @Query1='select * from #ReturnsCustomerDetails'+@whereStr+' order by Dtt desc'
exec (@Query1) 
END
IF  @ReportName='DebitMemoByProductDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #DebitMemoByProductDetails from (
select o.COLUMN03 as OPUnit,(case when p.COLUMN50='22335' then s2.COLUMN05 else s.COLUMN05 end) Vendor,FORMAT(p.COLUMN06,@DateF) Date,p.COLUMN06 Dtt,c.COLUMN04 TransType,p.COLUMN04 Trans#,m.COLUMN04 item,L.COLUMN04 Lot,p.COLUMN09 memo,f.COLUMN07 Qty,f.COLUMN09 rate,f.COLUMN24 Amount,p.COLUMN05 VID,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand
 ,c1.COLUMN04 Location,c1.COLUMN02 LocationID,p.COLUMN03 fid  from PUTABLE001 p 
left outer join PUTABLE002 f on f.COLUMN19=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN24 and o.COLUMNA03=p.COLUMNA03 and  isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 and s2.COLUMNA03=p.COLUMNA03 and  isnull(s2.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 and L.COLUMNA03=f.COLUMNA03 and isnull(L.COLUMNA13,0)=0
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join MATABLE007 m on m.COLUMN02=f.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355  and p.COLUMN06 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner) Query

set @Query1='select * from #DebitMemoByProductDetails'+@whereStr+' order by Dtt desc'
exec (@Query1) 
end
Else if @ReportName='DebitMemoByProductSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
SET @PERECENT=(SELECT SUM(f.COLUMN24) FROM PUTABLE002 f
INNER JOIN MATABLE007 M7 ON M7.COLUMN02=f.COLUMN03 
INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=f.COLUMNA02
inner join PUTABLE001 p on p.COLUMN01=f.COLUMN19 
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 where isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1355 and p.COLUMN06 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner )
select * into #DebitMemoByProductSummary from (
select o.COLUMN03 as OPUnit,m.COLUMN04 itm,L.COLUMN04 Lot,sum(f.COLUMN07) Qty,sum(f.COLUMN24) amont,CAST((100/@PERECENT)*(SUM(f.COLUMN24)) AS DECIMAL(18,4)) [% OF SALE],CAST(SUM(f.COLUMN24)/SUM(f.COLUMN07)AS DECIMAL(18,2)) [AVG PRICE],@PERECENT TOTAL,p.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand,c1.COLUMN04 Location,c1.COLUMN02 LocationID 
,p.COLUMN04 Trans# ,p.COLUMN03 fid  from PUTABLE002 f
inner join PUTABLE001 p on p.COLUMN01=f.COLUMN19 and p.COLUMNA03=f.COLUMNA03
inner join CONTABLE007 o on o.COLUMN02=f.COLUMNA02 and o.COLUMNA03=f.COLUMNA03
left outer join FITABLE043 L on L.COLUMN02=f.COLUMN17 and l.COLUMNA03=f.COLUMNA03
inner join MATABLE007 m on m.COLUMN02=f.COLUMN03  and m.COLUMNA03=f.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=f.COLUMN03 and isnull(m5.COLUMNA13,0)=0 and m5.COLUMNA03=f.COLUMNA03
left outer join CONTABLE030 c1 on  c1.COLUMN02=p.COLUMN47 and c1.COLUMNA03=p.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0 and c1.COLUMNA03=f.COLUMNA03
where isnull(p.COLUMNA13,0)=0 and isnull(f.COLUMNA13,0)=0 and isnull(c1.COLUMNA13,0)=0 and p.COLUMN03=1355 and p.COLUMN06 between @FromDate and @ToDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner  group by o.COLUMN03,m.COLUMN04,L.COLUMN04,p.COLUMN24,m5.COLUMN02,m5.COLUMN04 ,c1.COLUMN04 ,c1.COLUMN02 ,p.COLUMN04  ,p.COLUMN03   HAVING SUM(ISNULL(f.COLUMN24,0))>0) Query
set @Query1='select * from #DebitMemoByProductSummary'+@whereStr
exec (@Query1) 
END
Else IF  @ReportName='CreditMemoByProductDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #CreditMemoByProductDetails from (
SELECT  o.COLUMN03 ou,(case when a.COLUMN51='22305' then s1.COLUMN05 else s.COLUMN05 end) customer,FORMAT(cast( a.COLUMN06 as date),@DateF) dt,a.COLUMN06 Dtt,c.COLUMN04 TrnsType, m.COLUMN04 itm ,L.COLUMN04 Lot,a.COLUMN04 Creditnum ,b.COLUMN06 descc,b.COLUMN07 qty ,b.COLUMN09 price  ,b.COLUMN25 amont,a.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand
,c1.COLUMN04 Location,c1.COLUMN02 LocationID,a.COLUMN04 Trans# ,a.COLUMN03 fid FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN24 left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03 left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03 left outer join CONTABLE0010 c on c.COLUMN02=a.COLUMN03 left outer join MATABLE007 m on m.COLUMN02=b.COLUMN03 left outer join FITABLE043 L on L.COLUMN02=b.COLUMN17 and L.COLUMNA03=b.COLUMNA03 and isnull(L.COLUMNA13,0)=0 left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0 left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
where isnull(a.COLUMNA13,0)=0 and a.COLUMN03 in(1330,1603) and a.COLUMN06 between @FromDate and @ToDate and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner ) Query
set @Query1='select * from #CreditMemoByProductDetails'+@whereStr+' order by Dtt desc'
exec (@Query1) 
END
Else if @ReportName='CreditMemoByProductSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocationID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
SET @PERECENT=(SELECT SUM(b.COLUMN25) FROM SATABLE006 b
INNER JOIN MATABLE007 M7 ON M7.COLUMN02=b.COLUMN03 
INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=b.COLUMNA02
left outer join FITABLE043 L on L.COLUMN02=b.COLUMN17 
inner join SATABLE005 a on a.COLUMN01=b.COLUMN19 where isnull(b.COLUMNA13,0)=0 and a.COLUMN03=1330 and a.COLUMN06 between @FromDate and @ToDate and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner )
select * into #CreditMemoByProductSummary from (
SELECT  o.COLUMN03 ou, m.COLUMN04 itm ,L.COLUMN04 Lot,sum(b.COLUMN07) qty ,sum(b.COLUMN25) amont,CAST((100/@PERECENT)*(SUM(b.COLUMN25)) AS DECIMAL(18,4)) [% OF SALE],CAST(SUM(b.COLUMN25)/SUM(b.COLUMN07)AS DECIMAL(18,2)) [AVG PRICE],@PERECENT TOTAL,a.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,a.COLUMN04 Trans# ,a.COLUMN03 fid FROM SATABLE006 b  
inner join CONTABLE007 o on o.COLUMN02=b.COLUMNA02  
inner join MATABLE007 m on m.COLUMN02=b.COLUMN03 
inner join SATABLE005 a on isnull(a.COLUMNA13,0)=0 and a.COLUMN01=b.COLUMN19 
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=b.COLUMN17
where isnull(b.COLUMNA13,0)=0 and a.COLUMN03 in(1330,1603) and a.COLUMN06 between @FromDate and @ToDate and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner group by o.COLUMN03,m.COLUMN04,L.COLUMN04,a.COLUMN24,m5.COLUMN02,m5.COLUMN04,c1.COLUMN04, c1.COLUMN02,a.COLUMN04  ,a.COLUMN03   HAVING SUM(ISNULL(b.COLUMN25,0))>0) Query
set @Query1='select * from #CreditMemoByProductSummary'+@whereStr
exec (@Query1) 
END
end






GO
