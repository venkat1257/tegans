USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PAYBILL_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_PAYBILL_HEADER_DATA]
(
	@PurchaseOrderID int,
	@Form varchar(50)
)

AS
BEGIN
if(@Form='0')
begin
-- EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
   SELECT   COLUMN05 ,COLUMN02 , COLUMN10 ,(null) COLUMN09,COLUMN12 COLUMN13,
   isnull((select top 1(COLUMN11) from PUTABLE014  where COLUMN06=@PurchaseOrderID order by COLUMN02 DESC),(select COLUMN04 from CONTABLE025 where COLUMN02=19)) COLUMN12, 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN21,COLUMN15, COLUMN16,COLUMN17,COLUMN18,COLUMN20 ,COLUMN26 ,COLUMN29 Location
    ,(select column05 from SATABLE001 where COLUMN02=PUTABLE005.COLUMN05) as cName
   FROM PUTABLE005 WHERE  COLUMN06= @PurchaseOrderID
   end
   else if(@PurchaseOrderID='0')
    begin
   SELECT   COLUMN05 ,COLUMN02 , COLUMN10 ,(null)  COLUMN09,COLUMN12 COLUMN13,
   isnull((select top 1(COLUMN11) from PUTABLE014  where COLUMN06=@PurchaseOrderID order by COLUMN02 DESC),(select COLUMN04 from CONTABLE025 where COLUMN02=19)) COLUMN12, 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN21,COLUMN15, COLUMN16,COLUMN17,COLUMN18,COLUMN20 ,COLUMN26 ,COLUMN29 Location
    ,(select column05 from SATABLE001 where COLUMN02=PUTABLE005.COLUMN05) as cName
   FROM PUTABLE005 WHERE  (column02=@Form or column04=@Form )
   end
   else
   begin
   SELECT   COLUMN05 ,COLUMN02 , COLUMN10 ,(null)  COLUMN09,COLUMN12 COLUMN13,
   isnull((select top 1(COLUMN11) from PUTABLE014  where COLUMN06=@PurchaseOrderID order by COLUMN02 DESC),(select COLUMN04 from CONTABLE025 where COLUMN02=19)) COLUMN12, 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN21,COLUMN15, COLUMN16,COLUMN17,COLUMN18,COLUMN20 ,COLUMN26 ,COLUMN29 Location
    ,(select column05 from SATABLE001 where COLUMN02=PUTABLE005.COLUMN05) as cName
   FROM PUTABLE005 WHERE  COLUMN06= @PurchaseOrderID and (column02=@Form or column04=@Form )
   end
END











GO
