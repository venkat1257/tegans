USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_GreyReport]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[USP_REPORTS_GreyReport]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Joborderno      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
)
as 
begin
if @Joborderno!=''
begin
 set @whereStr= ' and gh.COLUMN04 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Joborderno+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and gh.COLUMN09 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and gh.COLUMN09 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and 1=1'
end

 if(@ReportName='GreyReport')
 begin
exec('
(select c7.COLUMN03 ou,S2.COLUMN05 cust,p1.COLUMN04 orderno,FORMAT(p1.COLUMN06,'''+@DateF+''') dt,p1.COLUMN06 Dtt,gh.COLUMN04 lotno,
sum(cast(isnull(gl.COLUMN05,0)as decimal(18,2))) pcs,sum(isnull(gl.COLUMN06,0)) grymtr,(isnull(p2.COLUMN12,0)) chkmtr,
sum(isnull(gl.COLUMN07,0)) grywdth,sum(cast(iif(gl.COLUMN08='''',0,isnull(gl.COLUMN08,0))as decimal(18,2))) fold,
sum(cast(isnull(gl.COLUMN09,0)as decimal(18,2))) AvgWt,p2.COLUMN36 sno,
 (isnull(p2.COLUMN07,0)) qty,(isnull(p2.COLUMN12,0)) orderRQty,(isnull(p2.COLUMNB12,0)) IssueQty, 

  iif( ((p4.COLUMN02)) >0,(isnull(p2.COLUMN07,0) - isnull(p2.COLUMN12,0)),0)  RDue,
iif( ((s8.COLUMN02)) >0, (isnull(p2.COLUMN12,0) - isnull(p2.COLUMNB12,0)) ,0) IDue,
iif( isnull(p2.COLUMN12,0) >0,'''',(iif((isnull(p3.COLUMN02,0))>0,1,''''))) RPcs,
--iif( isnull(p2.COLUMN07,0) = (isnull(p2.COLUMN07,0) - isnull(p2.COLUMN12,0)) , 0,  iif( (max(p3.COLUMN02)) >0,0,1)) RPcs ,
iif( isnull(p2.COLUMNB12,0) >0,'''',(iif(((isnull(s7.COLUMN02,0))>0 and isnull(p2.COLUMN12,0)>0),1,''''))) IPcs ,
gh.COLUMN09 OPID
from PUTABLE022 gh inner join  PUTABLE023 gl on gh.COLUMN01=gl.COLUMN10 and gh.COLUMNA03=gl.COLUMNA03 and isnull(gl.COLUMNA13,0)=0
left join PUTABLE001 p1 on p1.COLUMN11=gh.COLUMN02 and p1.COLUMNA03 =gh.COLUMNA03 and isnull(gh.COLUMNA13,0)=0 and  p1.COLUMN03=1586 and p1.COLUMN06 between '''+@FromDate+''' and '''+@ToDate+''' and p1.COLUMN06 >='''+@FiscalYearStartDt+'''
left join  PUTABLE002 p2 on p2.COLUMN19 =p1.COLUMN01 and p2.COLUMNA03 =p1.COLUMNA03 
 left outer  join PUTABLE003 p3 on p3.COLUMN06= p1.COLUMN02  and p3.COLUMNA03 =p1.COLUMNA03 
 left outer  join PUTABLE004 p4 on p4.COLUMN12=p3.COLUMN01 and p4.COLUMN26= p2.COLUMN02  and isnull(p4.COLUMNA13,0)=0
 left outer join SATABLE007 s7 on s7.COLUMN06 = p1.COLUMN02 and s7.COLUMNA03 =p1.COLUMNA03  and isnull(s7.COLUMNA13,0)=0
 left join SATABLE008 s8 on s8.COLUMN14 =s7.COLUMN01 and   s8.COLUMN26= p2.COLUMN02   and isnull(s8.COLUMNA13,0)=0
 --left  join MATABLE007 m7 on m7.COLUMN02=p2.COLUMN03 and isnull(m7.COLUMNA13,0)=0   and  m7.COLUMNA03=p2.COLUMNA03
 --left join MATABLE013 m13 on m13.COLUMN02=p2.COLUMN25 and isnull(m13.COLUMNA13,0)=0 and  m13.COLUMNA03=p2.COLUMNA03
 left join CONTABLE007 c7 on c7.COLUMN02=gh.COLUMN09  and isnull(c7.COLUMNA13,0)=0 and  c7.COLUMNA03=gh.COLUMNA03
 LEFT JOIN SATABLE002 S2 ON  S2.COLUMN02=gh.COLUMN05 and isnull(S2.COLUMNA13,0)=0 and  S2.COLUMNA03=gh.COLUMNA03
  where  isnull((gh.COLUMNA13),0)=0 '+@whereStr+' and
( gh.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) or gh.columna02 is null) AND  gh.COLUMNA03='''+@AcOwner+'''
group by p1.COLUMN04,p1.COLUMN06,p2.COLUMN36,c7.COLUMN03,gh.COLUMN09,S2.COLUMN05,gh.COLUMN04,
p2.COLUMN07,p2.COLUMN12,p2.COLUMNB12,p4.COLUMN02,s8.COLUMN02,s7.COLUMN02,p3.COLUMN02
) order by Dtt desc  ') 
end
end

GO
