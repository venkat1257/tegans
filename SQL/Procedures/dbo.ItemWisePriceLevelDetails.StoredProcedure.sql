USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ItemWisePriceLevelDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ItemWisePriceLevelDetails](@AcOwner nvarchar(250)=null,@OPUNIT nvarchar(250)=null,
@Item nvarchar(250)=null)
as
begin
if exists(select column04 from matable025 where column03 in(@Item)
and column08=1 and isnull(columna13,0)=0 and columna03=@AcOwner)
begin
select column02,column04,COLUMN13 from matable023 where column02 in(select column04 from matable025 where 
column03 in(@Item) and column08=1 and isnull(columna13,0)=0 and
 (columna03=@AcOwner or isnull(columna03,0)=0)) and isnull(columna13,0)=0 and (COLUMN07='Item' or isnull(COLUMN07,'')='')
end
else
begin
select column02,column04,COLUMN13 from matable023 where  (columna03=@AcOwner or isnull(columna03,0)=0) and isnull(columna13,0)=0 and 
(COLUMN07='Item' or isnull(COLUMN07,'')='')
end
end

GO
