USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Proc_InputTaxCreditDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[usp_Proc_InputTaxCreditDetails]
(
@OPUnit nvarchar(250) = null,@AcOwner nvarchar(250) = null
)
AS
BEGIN
set @OPUnit=(iif(isnull(cast(@OPUnit as nvarchar(250)),'')='','0',@OPUnit))
select COLUMN02,COLUMN04 'StateCode','' 'OPUnit','' 'TaxType','' 'Vendor','' 'Bill','' 'Status' from MATABLE002  where COLUMN03 = 11203 and isnull(COLUMNA13,0)=0
union all
select COLUMN02,'' 'StateCode',COLUMN03 'OPUnit','' 'TaxType','' 'Vendor','' 'Bill','' 'Status' from CONTABLE007 where COLUMNA03=@AcOwner and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0
union all
select COLUMN02,'' 'StateCode','' 'OPUnit',COLUMN04 'TaxType','' 'Vendor','' 'Bill','' 'Status' from MATABLE002  where COLUMN02 in (23582,23583,23584,23585,23586) and isnull(COLUMNA13,0)=0
union all
select COLUMN02,'' 'StateCode','' 'OPUnit','' 'TaxType',COLUMN05 'Vendor','' 'Bill','' 'Status' from SATABLE001 where COLUMNA03=@AcOwner and (isnull(COLUMNA02,0)=@OPUnit or isnull(COLUMNA02,0)=0) and isnull(COLUMNA13,0)=0
union all
select bh.COLUMN02,'' 'StateCode','' 'OPUnit','' 'TaxType','' 'Vendor',bh.COLUMN04 'Bill','' 'Status' from PUTABLE005 bh
inner join PUTABLE006 bl on bl.COLUMN13=bh.COLUMN01 and bl.COLUMNA02=bh.COLUMNA02 and bl.COLUMNA03=bh.COLUMNA03 and isnull(bl.COLUMNA13,0)=0
left join PUTABLE001 dh on dh.COLUMN48=bh.COLUMN02 and dh.COLUMNA02=bh.COLUMNA02 and dh.COLUMNA03=bh.COLUMNA03 and isnull(dh.COLUMNA13,0)=0
left join PUTABLE002 dl on dl.COLUMN19=dh.COLUMN01 and dl.COLUMN03=bl.COLUMN04 and dl.COLUMN26=bl.COLUMN19 and iif(cast(dl.COLUMN17 as nvarchar(250))='','0',isnull(dl.COLUMN17,0))=iif(cast(bl.COLUMN27 as nvarchar(250))='','0',isnull(bl.COLUMN27,0)) and dl.COLUMNA02=dh.COLUMNA02 and dl.COLUMNA03=dh.COLUMNA03 and isnull(dl.COLUMNA13,0)=0
where bh.COLUMNA03=@AcOwner and isnull(bh.COLUMNA02,0)=@OPUnit and isnull(bh.COLUMNA13,0)=0 and (isnull(bl.COLUMN09,0)-isnull(dl.COLUMN07,0))!=0
group by bh.COLUMN02,bh.COLUMN04
union all
select COLUMN02,'' 'StateCode','' 'OPUnit','' 'TaxType','' 'Vendor','' 'Bill',COLUMN04 'Status' from MATABLE002  where COLUMN03=11206 and isnull(COLUMNA13,0)=0
END 

GO
