USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ServiceItemdetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ServiceItemdetails](@ItemID nvarchar(250)=null,@OPUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null)
as
begin
if((ISNUMERIC(@ItemID)>0) and ((select COLUMN02 from MATABLE007 Where COLUMN02=cast(@ItemID as nvarchar))>0))
begin
Select i.COLUMN02 ItemId,i.COLUMN04 ItemName,m.COLUMN04 Units, 1 Quantity,isnull(MATABLE024.COLUMN04,0) Price,
isnull(MATABLE024.COLUMN04,0) Amount,(case when isnull(i.COLUMN54,0)=1000 then 0 else isnull(i.COLUMN54,0) end) TaxType,isnull(i.COLUMN63,0) unitid,
iif(MATABLE013.COLUMN16=22401,isnull(MATABLE013.column07,0)/100,0) Servicetax  From MATABLE007 i
 left join MATABLE002 m on  m.COLUMN02=i.COLUMN63  and isnull(m.COLUMNA13,'False')='False'  
 left join MATABLE024 MATABLE024 on  isnull(MATABLE024.COLUMNA13,0)=0 and (isnull(MATABLE024.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(MATABLE024.COLUMN03,0)=0)
 and MATABLE024.COLUMNA03=i.COLUMNA03 and MATABLE024.COLUMN07=i.COLUMN02 and MATABLE024.COLUMN06='sales'
 left join MATABLE013 MATABLE013 on  MATABLE013.COLUMNA02=i.COLUMNA02 and MATABLE013.COLUMNA03=i.COLUMNA03 and MATABLE013.COLUMN02=i.COLUMN54-- and MATABLE024.COLUMN06='sales'
 Where  (i.COLUMN02=cast(@ItemID as nvarchar) or i.COLUMN06=@ItemID) and
  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and 
  isnull(i.COLUMN47,'False')='False' and  isnull(i.COLUMNA13,'False')='False'
end
else if exists(select COLUMN02 from MATABLE007 Where COLUMN06=cast(@ItemID as nvarchar))
begin
Select i.COLUMN02 ItemId,i.COLUMN04 ItemName,m.COLUMN04 Units, 1 Quantity,isnull(MATABLE024.COLUMN04,0) Price,
isnull(MATABLE024.COLUMN04,0) Amount,(case when isnull(i.COLUMN54,0)=1000 then 0 else isnull(i.COLUMN54,0) end) TaxType,isnull(i.COLUMN63,0) unitid,
iif(MATABLE013.COLUMN16=22401,isnull(MATABLE013.column07,0)/100,0) Servicetax  From MATABLE007 i
 left join MATABLE002 m on  m.COLUMN02=i.COLUMN63  and isnull(m.COLUMNA13,'False')='False' 
 left join MATABLE024 MATABLE024 on  isnull(MATABLE024.COLUMNA13,0)=0 and (isnull(MATABLE024.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(MATABLE024.COLUMN03,0)=0)
  and MATABLE024.COLUMNA03=i.COLUMNA03 and MATABLE024.COLUMN07=i.COLUMN02 and MATABLE024.COLUMN06='sales'
 left join MATABLE013 MATABLE013 on  MATABLE013.COLUMNA02=i.COLUMNA02 and MATABLE013.COLUMNA03=i.COLUMNA03 and MATABLE013.COLUMN02=i.COLUMN54-- and MATABLE024.COLUMN06='sales'
 Where  (i.COLUMN02=cast(@ItemID as nvarchar) or i.COLUMN06=@ItemID) and
  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and 
  isnull(i.COLUMN47,'False')='False' and  isnull(i.COLUMNA13,'False')='False'
end
else if exists(select COLUMN02 from MATABLE021 Where COLUMN04=cast(@ItemID as nvarchar))
begin
Select mi.COLUMN02 ItemId,mi.COLUMN04 ItemName,m.COLUMN04 Units,iif(isnull(i.COLUMN07,0)=0,1,i.COLUMN07) Quantity,
iif(isnull(i.COLUMN09,0)=0,isnull(MATABLE024.COLUMN04,0),i.COLUMN09) Price,
cast((cast((iif(isnull(i.COLUMN07,0)=0,1,i.COLUMN07)) as decimal(18,2))*cast((iif(isnull(i.COLUMN09,0)=0,isnull(MATABLE024.COLUMN04,0),i.COLUMN09)) as decimal(18,2))) as decimal(18,2)) Amount,
(case when isnull(mi.COLUMN54,0)=1000 then 0 else isnull(mi.COLUMN54,0) end) TaxType,isnull(i.COLUMN06,0) unitid,
iif(MATABLE013.COLUMN16=22401,isnull(MATABLE013.column07,0)/100,0) Servicetax
  From MATABLE021 i
 left join MATABLE002 m on  m.COLUMN02=i.COLUMN06  and isnull(m.COLUMNA13,'False')='False' 
 left join MATABLE007 mi on  mi.COLUMN02=i.COLUMN05
 left join MATABLE024 MATABLE024 on  isnull(MATABLE024.COLUMNA13,0)=0 and (isnull(MATABLE024.COLUMN03,0) in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(MATABLE024.COLUMN03,0)=0)
  and MATABLE024.COLUMNA03=i.COLUMNA03 and MATABLE024.COLUMN07=mi.COLUMN02 and MATABLE024.COLUMN06='sales'
 left join MATABLE013 MATABLE013 on  MATABLE013.COLUMNA02=i.COLUMNA02 and MATABLE013.COLUMNA03=i.COLUMNA03 and MATABLE013.COLUMN02=mi.COLUMN54-- and MATABLE024.COLUMN06='sales'
  Where  (cast(i.COLUMN04 as nvarchar)=@ItemID) and
  (i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or i.COLUMNA02 is null)  AND i.COLUMNA03=@AcOwner and isnull(i.COLUMNA13,'False')='False'
end
end


GO
