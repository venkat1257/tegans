USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[InvoiceHeaderPriceLevelDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InvoiceHeaderPriceLevelDetails](@AcOwner nvarchar(250)=null,@Pricelevel nvarchar(250)=null,
@Type nvarchar(250)=null,@Price decimal(18,2)=null,@Qty decimal(18,2)=null,@Date nvarchar(250)=null)
as
begin
set @Price=(iif(cast(@Price as nvarchar)='',0,isnull(@Price,0)))
set @Qty=(iif(cast(@Qty as nvarchar)='',0,isnull(@Qty,0)))
set @Date=(cast(iif(cast(@Date as nvarchar)='',getdate(),isnull(@Date,getdate()))as date))
if(@Type='Header')
begin
select cast(iif((p.COLUMN05=22778),
cast(isnull(@Price,0) as decimal(18,2))-(((cast(isnull(@Price,0) as decimal(18,2)))*(cast(isnull(p.COLUMN06,0) as decimal(18,2))
*0.01))+(cast(isnull(p.COLUMN11,0) as decimal(18,2)))),0) as decimal(18,2)) Rate,p.COLUMN05 pricetype,
iif((p.COLUMN05=22778),cast((cast(isnull(@Price,0) as decimal(18,2)))*(cast(isnull(p.COLUMN06,0) as decimal(18,2))*0.01)+(cast(isnull(p.COLUMN11,0) 
as decimal(18,2)))as decimal(18,2)),0) Discount from MATABLE023 p Where  p.COLUMN02=@Pricelevel AND p.COLUMNA03=@AcOwner
 and isnull(p.COLUMNA13,0)=0 and (iif(cast(p.COLUMN09 as nvarchar)='',0,isnull(p.COLUMN09,0))<=@Price or iif(cast(p.COLUMN09 as nvarchar)='',0,isnull(p.COLUMN09,0))=0) and 
 (iif(cast(p.COLUMN10 as nvarchar)='',0,isnull(p.COLUMN10,0))>=@Price or iif(cast(p.COLUMN10 as nvarchar)='',0,isnull(p.COLUMN10,0))=0)
end
else if(@Type='Line')
begin

select cast(iif((p.COLUMN05=22778 or p.COLUMN05=22831),cast(isnull(@Price,0) as decimal(18,2))-
((cast(isnull(@Price,0) as decimal(18,2)))*(cast(isnull(p.COLUMN06,0) as decimal(18,2))*0.01))-cast(isnull(p.COLUMN11,0) as decimal(18,2)),
(cast(isnull(@Price,0) as decimal(18,2)))+((cast(isnull(@Price,0) as decimal(18,2)))*
(cast(isnull(p.COLUMN06,0) as decimal(18,2))*0.01))+cast(isnull(p.COLUMN11,0) as decimal(18,2))) as decimal(18,2)) Rate,p.COLUMN05 pricetype,0 Discount from MATABLE023 p
 Where  p.COLUMN02=@Pricelevel AND p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0  and (iif(cast(p.COLUMN12 as nvarchar)='',0,isnull(p.COLUMN12,0))=@Qty or iif(cast(p.COLUMN12 as nvarchar)='',0,isnull(p.COLUMN12,0))=0)
 and (@Date between cast(isnull(p.COLUMN14,@Date) as date) and cast(isnull(p.COLUMN15,@Date) as date))
--if(@Qty=0)
--begin
--end
--else if(@Qty>0)
--begin
--select cast(iif((p.COLUMN05=22778 or p.COLUMN05=22831),cast(isnull(@Price,0) as decimal(18,10))-
--((cast(isnull(@Price,0) as decimal(18,10)))*(cast(isnull(p.COLUMN06,0) as decimal(18,10))*0.01))-iif(cast(isnull(p.COLUMN11,0) as decimal(18,10))>0,cast(isnull(@Price,0) as decimal(18,10))-((cast(isnull(@Price,0) as decimal(18,10))*cast(isnull(@Qty,0) as decimal(18,10)))-(cast(isnull(p.COLUMN11,0) as decimal(18,10))))/cast(isnull(@Qty,0) as decimal(18,10)),0),
--(cast(isnull(@Price,0) as decimal(18,10)))+((cast(isnull(@Price,0) as decimal(18,10)))*
--(cast(isnull(p.COLUMN06,0) as decimal(18,10))*0.01))+iif(cast(isnull(p.COLUMN11,0) as decimal(18,10))>0,cast(isnull(@Price,0) as decimal(18,10))-((cast(isnull(@Price,0) as decimal(18,10))*cast(isnull(@Qty,0) as decimal(18,10)))+(cast(isnull(p.COLUMN11,0) as decimal(18,10))))/cast(isnull(@Qty,0) as decimal(18,10)),0)) as decimal(18,2)) Rate,p.COLUMN05 pricetype from MATABLE023 p
-- Where  p.COLUMN02=@Pricelevel AND p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0 and (iif(cast(p.COLUMN12 as nvarchar)='',0,isnull(p.COLUMN12,0))>=@Qty or iif(cast(p.COLUMN12 as nvarchar)='',0,isnull(p.COLUMN12,0))=0)
--end
 --and (iif(cast(p.COLUMN09 as nvarchar)='',0,isnull(p.COLUMN09,0))<=@Price or iif(cast(p.COLUMN09 as nvarchar)='',0,isnull(p.COLUMN09,0))=0) and 
 --(iif(cast(p.COLUMN10 as nvarchar)='',0,isnull(p.COLUMN10,0))>=@Price or iif(cast(p.COLUMN10 as nvarchar)='',0,isnull(p.COLUMN10,0))=0)
end
end


GO
