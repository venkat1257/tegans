USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CRETAE_ACCOUNT]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CRETAE_ACCOUNT]
(
	@EMPNAME NVARCHAR(250),
	@EMPEMAIL NVARCHAR(250),
	@EMPPWD NVARCHAR(250),
	@COMPANY NVARCHAR(250),
	@COLUMNA06 DATETIME = GETDATE,
	@COLUMNA07 DATETIME = GETDATE,
	@EMPMOBILE NVARCHAR(250) = NULL,
	@ADDS NVARCHAR(250) = NULL,
	@ADDS1 NVARCHAR(250) = NULL,
	@COUNTRY NVARCHAR(250) = NULL,
	@STATE NVARCHAR(250) = NULL,
	@CITY NVARCHAR(250) = NULL,
	@ZIP NVARCHAR(250) = NULL,
	@MULTI NVARCHAR(250) = '0',
	@LOGO NVARCHAR(250) = NULL,
	@PLOGO NVARCHAR(250) = NULL,
	@ACTYPE NVARCHAR(250) = '10000',
	@GSTIN NVARCHAR(250) = NULL,
	@PERMISSIONS NVARCHAR(250) = '',
	@BUZTYPE NVARCHAR(250) = '',
	@LGTYPE NVARCHAR(250) = NULL,
	@PARTNER NVARCHAR(250) = NULL,
	@PDISPLAY NVARCHAR(250) = NULL
)
AS
BEGIN
	BEGIN TRY	
		BEGIN TRAN	
			SELECT @PLOGO = COLUMN07,@PDISPLAY = COLUMN15 FROM MATABLE035 WHERE COLUMN02 = @PARTNER AND isnull(COLUMN15,0) = '1'
			DECLARE @COLUMN02 INT = NULL,@COLUMN2 INT = NULL,
					@ID INT = NULL,
					@ID1 INT = NULL,
					@REF BIGINT = NULL,
					@COLUMNA01 INT = NULL,
					@COLUMNA03 INT = NULL
			SET @COLUMNA01 = (SELECT TOP 1 C2.COLUMNA01 FROM CONTABLE002 C2
			INNER JOIN MATABLE010 M10 ON M10.COLUMN13=@EMPEMAIL AND M10.COLUMN21=@EMPPWD AND M10.COLUMNA03=C2.COLUMN02)
			IF(@COMPANY='' OR @COMPANY IS NULL) SELECT @COMPANY = @EMPNAME
			SELECT @COLUMN02=MAX(COLUMN02)+1 FROM CONTABLE002
			--CA ACCOUNT
			--IF(@MULTI = '1')
			--	BEGIN
			--		INSERT INTO CONTABLE032(COLUMN02,COLUMN03,COLUMN04,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13)
			--			   VALUES (@COLUMN02,@COMPANY,@COMPANY,@COLUMNA06,@COLUMNA07,1,0)
			--		SELECT @COLUMNA01=MAX(COLUMN01)+1 FROM CONTABLE032
			--	END
			--ACCOUNT OWNER
			INSERT INTO CONTABLE002(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13,COLUMNA01,COLUMND01,COLUMN26,COLUMN27,COLUMN28)
						   VALUES (@COLUMN02,@COMPANY,@COMPANY,@COMPANY,0,@COLUMNA06,@COLUMNA07,1,0,IIF(@MULTI='1',@COLUMN02,@COLUMNA01),@LOGO,@PARTNER,@PDISPLAY,@PLOGO)
			SELECT @COLUMN2=MAX(COLUMN02)+1 FROM CONTABLE008
			--COMPANY
			INSERT INTO CONTABLE008(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13,COLUMN10,COLUMN11,COLUMN15,COLUMN13,COLUMN12,COLUMN14,COLUMN28,COLUMND02,COLUMN06,COLUMND01)
						   VALUES (@COLUMN2,@COMPANY,@COMPANY,0,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0,@ADDS,@ADDS1,@COUNTRY,@STATE,@CITY,@ZIP,@LOGO,@GSTIN,@BUZTYPE,@STATE)
			--OPERATING UNIT SELECT*FROM CONTABLE007
			SELECT @COLUMN2=MAX(COLUMN02)+1 FROM CONTABLE007
			INSERT INTO CONTABLE007(COLUMN02,COLUMN03,COLUMN04,COLUMN07,COLUMN09,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13,COLUMN39,COLUMN40,COLUMN13,COLUMN14,COLUMN18,COLUMN16,COLUMN15,COLUMN17,COLUMN05,COLUMND02,COLUMND01)
						   VALUES (@COLUMN2,iif(@city is null,'UNIT',@city),iif(@city is null,'UNIT',@city),0,0,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0,1,0,@ADDS,@ADDS1,@COUNTRY,@STATE,@CITY,@ZIP,@LOGO,@GSTIN,@STATE)
			--EMPLOYEE
			--SELECT @ID=MAX(COLUMN02)+1 FROM MATABLE010
			select  @ID = next value for MATABLE010_SEQUENCE
			INSERT INTO MATABLE010(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN11,COLUMN13,COLUMN14,COLUMN15,COLUMN19,COLUMN20,COLUMN21,
									COLUMN30,COLUMN32,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA12,COLUMNA13,COLUMN33,COLUMNA02,COLUMN26)	
							VALUES(@ID,1260,'EMP00001','',@EMPNAME,'',@EMPNAME,@EMPEMAIL,@EMPMOBILE,'','',1,@EMPPWD,
									0,0,@COLUMN02,@COLUMNA06,@COLUMNA07,@ID,1,0,'',@COLUMN2,@COLUMN2)
			SELECT @REF=COLUMN01 FROM MATABLE010	WHERE COLUMN02=@ID
			--EMPLOYEE ADDRESS DETAILS
			--SELECT @ID1=MAX(CAST(COLUMN02 AS INT))+1 FROM SATABLE003
			select  @ID1 = next value for SATABLE003_SequenceNo
			INSERT INTO SATABLE003(COLUMN02,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11,COLUMN12,COLUMN16,COLUMN17,COLUMN18,COLUMN19,COLUMN20,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13,COLUMN21,COLUMNA02)
						   VALUES (@ID1,'','','','','','',0,0,0,'Employee',@REF,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0,'',@COLUMN2)
			--ADDRESS REF UPDATE IN EMPLOYEE MASTER
			UPDATE MATABLE010 SET COLUMN25=@ID1 WHERE COLUMN01=@REF
			--EMAIL DETAILS
			--SELECT @ID1=MAX(COLUMN02)+1 FROM SETABLE013
			INSERT INTO SETABLE013(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13,COLUMNA03,COLUMNA08,COLUMNA02)
						   VALUES (@ID1,@COLUMNA06,@EMPEMAIL,'',@ID,@REF,@COLUMNA06,@COLUMNA07,1,0,@COLUMN02,@ID,@COLUMN2)
			--EMPLOYEE ROLE CONFIGURATION
			SELECT @ID1=MAX(CAST(COLUMN02 AS INT))+1 FROM CONTABLE027
			INSERT INTO CONTABLE027(COLUMN02,COLUMN03,COLUMN04,COLUMN07,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA12,COLUMNA13)
						   VALUES (@ID1,@ID,5656526,0,@COLUMN02,@COLUMNA06,@COLUMNA07,@ID,1,0)
			--EMPLOYEE PERMISSIONS CONFIGURATION
			select  @ID1 = next value for MATABLE028_SequenceNo
			INSERT INTO MATABLE028(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMNA02,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA12,COLUMNA13)
						   VALUES (@ID1,@PERMISSIONS,@ID,@REF,@COLUMN2,@COLUMN02,@COLUMNA06,@COLUMNA07,@ID,1,0)
			--MENU CREATION
			INSERT INTO CONTABLE003(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMNA03,COLUMNA12,COLUMNA13,COLUMND05,COLUMND06)
							SELECT COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,@COLUMN02,COLUMNA12,COLUMNA13,COLUMND05,COLUMND06 
							FROM CONTABLE003 WHERE COLUMNA03=@ACTYPE AND COLUMN04=565698 
			--CHART OF ACCOUNTS CREATION
			INSERT INTO FITABLE001(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMNA03,COLUMNA08,COLUMNA12,COLUMNA13,COLUMN15)
							SELECT COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,0,COLUMNA06,0,0,0,@COLUMN02,@ID,COLUMNA12,COLUMNA13,COLUMN15 
							FROM FITABLE001 WHERE COLUMNA03=10000 AND COLUMN02<=4000
			--TRANSACTION NUMBER SETTINGS
			MERGE MYTABLE002 AS T
			USING (SELECT  row_number() over(order by M2.COLUMN01) RNO,COLUMN03,COLUMN04,COLUMN05,COLUMN06,1 COLUMN07,99999 COLUMN08,COLUMN09,
					'' COLUMN10,COLUMN11,0 COLUMN12,COLUMNA03 FROM  MYTABLE002 M2 WHERE COLUMNA03=10000 AND COLUMNA02 IS NULL) AS S
			ON T.COLUMNA03=@COLUMN02
			WHEN NOT MATCHED BY TARGET
			THEN INSERT(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMNA03)
				VALUES(cast((select MAX(COLUMN02) from MYTABLE002) as int)+S.RNO,S.COLUMN03,S.COLUMN04,S.COLUMN05,S.COLUMN06,S.COLUMN07,S.COLUMN08,S.COLUMN09,
					S.COLUMN10,S.COLUMN11,S.COLUMN12,@COLUMN02)
					OUTPUT $action, Inserted.*,Deleted.*;
			--KPI SETTINGSSELECT*FROM SETABLE008 WHERE COLUMN07=1000
			MERGE SETABLE008 AS T
			USING (SELECT  row_number() over(order by COLUMN01) RNO,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07 FROM SETABLE008 WHERE COLUMN07=1000 AND COLUMNA02 IS NULL) AS S
			ON T.COLUMN07=@ID
			WHEN NOT MATCHED BY TARGET
			THEN INSERT(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07)
				VALUES(cast((select MAX(COLUMN02) from SETABLE008) as int)+S.RNO,S.COLUMN03,S.COLUMN04,S.COLUMN05,S.COLUMN06,@ID)
					OUTPUT $action, Inserted.*,Deleted.*;
			--KPI CONFIGURATION
			SELECT @ID1=MAX(CAST(COLUMN02 AS INT))+1 FROM MYTABLE001
			INSERT INTO MYTABLE001(COLUMN02,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA12,COLUMNA13,COLUMN10)
						   VALUES (@ID1,'dd/MM/yyyy','English','Blue','KPIs',20,@ID,@COLUMNA06,@COLUMNA07,@ID,1,0,'dd/mm/yy')
			--DEFAULT UOMS
			MERGE MATABLE002 AS T
			USING (SELECT  row_number() over(order by M2.COLUMN01) RNO,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMNA03 
			FROM  MATABLE002 M2 WHERE COLUMNA03=10000 AND COLUMNA02 IS NULL) AS S
			ON T.COLUMNA03=@COLUMN02
			WHEN NOT MATCHED BY TARGET
			THEN INSERT(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13)
				VALUES(cast((select MAX(COLUMN02) from MATABLE002) as int)+S.RNO,S.COLUMN03,S.COLUMN04,S.COLUMN05,S.COLUMN06,S.COLUMN07,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0)
					OUTPUT $action, Inserted.*,Deleted.*;
			--DEFAULT TAXES
			MERGE MATABLE013 AS T
			USING (SELECT  row_number() over(order by M2.COLUMN01) RNO,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN12,COLUMN13,COLUMN15,COLUMN17,COLUMN19,COLUMN20,COLUMN21,COLUMN22,COLUMN23,COLUMN25,COLUMNA03,COLUMN16 
			FROM  MATABLE013 M2 WHERE COLUMNA03=10000 AND COLUMNA02 IS NULL) AS S
			ON T.COLUMNA03=@COLUMN02
			WHEN NOT MATCHED BY TARGET
			THEN INSERT(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN12,COLUMN13,COLUMN15,COLUMN17,COLUMN19,COLUMN20,COLUMN21,COLUMN22,COLUMN23,COLUMN25,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13,COLUMN16)
				VALUES(cast((select MAX(CAST(COLUMN02 AS INT)) from MATABLE013) as int)+S.RNO,
				S.COLUMN03,S.COLUMN04,S.COLUMN05,S.COLUMN06,S.COLUMN07,S.COLUMN08,S.COLUMN12,S.COLUMN13,S.COLUMN15,S.COLUMN17,S.COLUMN19,S.COLUMN20,S.COLUMN21,S.COLUMN22,S.COLUMN23,S.COLUMN25,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0,S.COLUMN16)
					OUTPUT $action, Inserted.*,Deleted.*;
					SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;
					SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;
					SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;SELECT NEXT VALUE FOR MATABLE013_SequenceNo;
			--DEFAULT GROUP TAXES
			--MERGE MATABLE014 AS T
			--USING (SELECT  row_number() over(order by M2.COLUMN01) RNO,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMNA03 
			--FROM  MATABLE014 M2 WHERE COLUMNA03=10000 AND COLUMNA02 IS NULL) AS S
			--ON T.COLUMNA03=@COLUMN02
			--WHEN NOT MATCHED BY TARGET
			--THEN INSERT(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13)
			--	VALUES(cast((select MAX(CAST(COLUMN02 AS INT)) from MATABLE014) as int)+S.RNO,
			--	S.COLUMN03,S.COLUMN04,S.COLUMN05,S.COLUMN06,S.COLUMN07,S.COLUMN08,S.COLUMN09,S.COLUMN12,S.COLUMN12,S.COLUMN14,S.COLUMN15,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0)
			--		OUTPUT $action, Inserted.*,Deleted.*;
			--AUTO FILD CONFIG
			MERGE SETABLE012 AS T
			USING (SELECT  row_number() over(order by M2.COLUMN01) RNO,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMNA03 
			FROM  SETABLE012 M2 WHERE COLUMNA03=10000 AND COLUMNA02 IS NULL) AS S
			ON T.COLUMNA03=@COLUMN02
			WHEN NOT MATCHED BY TARGET
			THEN INSERT(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA12,COLUMNA13)
				VALUES(cast((select MAX(CAST(COLUMN02 AS INT)) from SETABLE012) as int)+S.RNO,S.COLUMN03,S.COLUMN04,S.COLUMN05,S.COLUMN06,S.COLUMN07,S.COLUMN08,S.COLUMN09,@COLUMN02,@COLUMNA06,@COLUMNA07,1,0)
					OUTPUT $action, Inserted.*,Deleted.*;
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		declare	@tempSTR nvarchar(max)	
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
		'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
		exec [CheckDirectory] @tempSTR,'Exception_CREATEAC.txt',0
	END CATCH
END 

--SELECT * FROM CONTABLE002 WHERE COLUMN02=56662
--SELECT * FROM MATABLE010  WHERE COLUMN13='baddularamesh2013@gmail.com'
--SELECT * FROM SETABLE013  WHERE COLUMNA03=56662
--SELECT * FROM SATABLE003  WHERE COLUMNA03=56662
--SELECT * FROM CONTABLE027 WHERE COLUMNA03=56662
--SELECT * FROM CONTABLE003 WHERE COLUMNA03=56662
--SELECT * FROM FITABLE001  WHERE COLUMNA03=56662
--SELECT * FROM MYTABLE002  WHERE COLUMNA03=56662
--SELECT * FROM SETABLE008  WHERE COLUMN07=1662
--SELECT * FROM MYTABLE001  WHERE COLUMN09=1662
--USE [MySmartSCM_PRD]
--GO

--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[CRETAE_ACCOUNT]
--		@EMPNAME = N'Snitha Corporation',
--		@EMPEMAIL = N'sunithacorporation@gmail.com',
--		@EMPPWD = N'sunitha2017',
--		@COMPANY = N'Snitha Corporation',
--		@COLUMNA06 = N'9/8/2017',
--		@COLUMNA07 = N'9/8/2017',
--		@EMPMOBILE = N'9999999999',
--		@ADDS = N' ',
--		@ADDS1 = N' ',
--		@COUNTRY = N'1007',
--		@STATE = N'1025',
--		@CITY = N'HYD'

--SELECT	'Return Value' = @return_value

--GO

GO
