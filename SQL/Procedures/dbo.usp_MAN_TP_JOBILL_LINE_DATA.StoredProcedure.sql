USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAN_TP_JOBILL_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAN_TP_JOBILL_LINE_DATA]
(
	@SalesOrderID int= null,
	@Form varchar(50)
)

AS
BEGIN
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
   declare @IRQty decimal(18,2)
   declare @IIQty  decimal(18,2)
   declare @IRID  int
   declare @tmpVal  decimal(18,2)
   declare @tmpVal1  decimal(18,2)
   declare @Shipedqty  decimal(18,2)
   declare @ivlid  int
   declare @Shipedqty1  decimal(18,2)
   declare @ITEMID int

   if(@Form='0')
   BEGIN
   --EMPHCS954 rajasekhar reddy patakota 14/8/2015 job receipts are not populated after job order selection and job receipt drop down should populate only job order receipts
   set @tmpVal=(select sum(isnull(COLUMN12,0)) from SATABLE006 WHERE COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID))
   set @IRQty=(select sum(isnull(COLUMN08,0)) from PUTABLE004 WHERE COLUMN12 in (SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE COLUMN06=cast( @SalesOrderID as nvarchar) and COLUMN17='1000'))
   set @IIQty=(select sum(isnull(COLUMN09,0)) from SATABLE008 WHERE COLUMN14 in (SELECT COLUMN01 FROM dbo.SATABLE007 WHERE COLUMN06= @SalesOrderID))
   set @tmpVal1=(select sum(isnull(COLUMN13,0)) from SATABLE006 WHERE COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID))
   set @Shipedqty1=((select sum(isnull(COLUMN09,0)) from SATABLE010 where COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast( @SalesOrderID as nvarchar) ) ))
   
   --joii
   if(@Shipedqty1>0)
   begin
   if(@IRQty>0)
   begin
    --set @IRID=(SELECT COLUMN02 FROM PUTABLE003 WHERE COLUMN17= '1000' and COLUMN06= @SalesOrderID)
   SELECT distinct d.COLUMN02 as COLUMN04, a.COLUMN03 as COLUMN05,a.COLUMN17 as COLUMN07,a.COLUMN04 as COLUMN06 , 
	 max(a.COLUMN05) as COLUMN07,max(a.COLUMN06) as COLUMN08,isnull(max(b.COLUMN09),0) as COLUMN09,
	((max(a.COLUMN08)-isnull(max(b.COLUMN09),0))) as COLUMN10,0 as COLUMN12 ,M7.COLUMN64 as COLUMN21,a.COLUMN10 as COLUMN13,a.COLUMN11 as COLUMN14

	FROM dbo.PUTABLE004 a inner join PUTABLE003 d on d.COLUMN01=a.COLUMN12  and  d.COLUMN17= '1000'
	  --inner join FITABLE010 f on a.COLUMN04=f.COLUMN03 
	  left outer join MATABLE007 M7 on M7.COLUMN02=a.COLUMN03
	 left outer join SATABLE010 b on b.COLUMN04 =d.COLUMN02 and 
	b.COLUMN05= a.COLUMN03 and b.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast( @SalesOrderID as nvarchar) )
	WHERE a.COLUMN12 in (SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE  COLUMN06=cast( @SalesOrderID as nvarchar) and  COLUMN17= '1000' ) group by  d.COLUMN02,a.COLUMN03,a.COLUMN17,a.COLUMN04,M7.COLUMN64 ,a.COLUMN10,a.COLUMN11
	end
	else
	begin
	SELECT distinct d.COLUMN02 as COLUMN04, a.COLUMN04 as COLUMN05,a.COLUMN05 as COLUMN06 , 
	max( a.COLUMN06) as COLUMN07,max(a.COLUMN07) as COLUMN08,isnull(sum(b.COLUMN10),0) as COLUMN09,
	(cast(avg(a.COLUMN09)as int)- isnull(sum(b.COLUMN10),0)) as COLUMN10,0 as COLUMN12 ,M7.COLUMN64 as COLUMN21
	--isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN04),0) as COLUMN11,
	--,cast(avg(a.COLUMN12)as int) as COLUMN13,cast(((avg(a.COLUMN09)- isnull(sum(b.COLUMN10),0))*avg(a.COLUMN12))as int) as COLUMN14
	FROM dbo.SATABLE008 a inner join SATABLE007 d on d.COLUMN01=a.COLUMN14  and  d.COLUMN20= '1000'
	--inner join FITABLE010 f on a.COLUMN04=f.COLUMN03 
	  left outer join MATABLE007 M7 on M7.COLUMN02=a.COLUMN03
	 left outer join SATABLE010 b on b.COLUMN05=a.COLUMN04  and b.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast( @SalesOrderID as nvarchar) )
	WHERE a.COLUMN14 in(SELECT COLUMN01 FROM dbo.SATABLE007 WHERE  COLUMN06=cast( @SalesOrderID as nvarchar)) group by d.COLUMN02,a.COLUMN04 ,a.COLUMN05,M7.COLUMN64
	end
   end
   else if(@IRQty>0)
   begin
   --EMPHCS954 rajasekhar reddy patakota 14/8/2015 job receipts are not populated after job order selection and job receipt drop down should populate only job order receipts
   --set @IRID=(SELECT COLUMN02 FROM PUTABLE003 WHERE COLUMN17= '1000' and COLUMN06= cast( @SalesOrderID as nvarchar))


    SELECT distinct b.COLUMN02 as COLUMN04,c.COLUMN03 as COLUMN05,c.COLUMN17 as COLUMN07,c.COLUMN04 as COLUMN06,c.COLUMN05 as COLUMN07 , 
	 c.COLUMN06 as COLUMN08,0 as COLUMN09,c.COLUMN08 as COLUMN10,0 as COLUMN12 ,M7.COLUMN64 as COLUMN21,c.COLUMN10 as COLUMN13,c.COLUMN11 as COLUMN14

	FROM PUTABLE004 c inner join PUTABLE003 b on b.COLUMN01=c.COLUMN12 and b.COLUMN06=cast( @SalesOrderID as nvarchar)  and  b.COLUMN17= '1000'
	--inner join FITABLE010 f on c.COLUMN03=f.COLUMN03 
	left outer join MATABLE007 M7 on M7.COLUMN02=c.COLUMN03
	--left outer join dbo.SATABLE008 a  on a.COLUMN14=(SELECT COLUMN01 FROM dbo.SATABLE007 WHERE COLUMN02= @SalesOrderID)

   end
   else if(@IIQty>0)
   begin
   if(@tmpVal=0 and @tmpVal1=0)
   begin
     SELECT  0 as COLUMN04,a.COLUMN03 as COLUMN05,a.COLUMN05 as COLUMN06,a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09,
	 (select case when (a.COLUMN07-isnull(a.COLUMN13,0))>isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03  AND COLUMN13=A.COLUMN20),0) 
	 then isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) else
	 (a.COLUMN07-isnull(a.COLUMN13,0)) end) as COLUMN10,0 as COLUMN12
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	 --,a.COLUMN09 as COLUMN13,a.COLUMN07*a.COLUMN09 as COLUMN14
	 FROM dbo.SATABLE006  a inner join SATABLE005 b on b.COLUMN01=a.COLUMN19 
	 --inner join FITABLE010 f on a.COLUMN03=f.COLUMN03 
	 WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)
   end
   else if(@tmpVal=0 and @tmpVal1>0)
   begin
   SELECT  b.COLUMN02 as COLUMN04,a.COLUMN03 as COLUMN05,a.COLUMN05 as COLUMN06,a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09,
	 (select case when (a.COLUMN07-isnull(a.COLUMN13,0))>isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) 
	 then isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) else
	 (a.COLUMN07-isnull(a.COLUMN13,0)) end) as COLUMN10,0 as COLUMN12
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	 --,a.COLUMN09 as COLUMN13,a.COLUMN07*a.COLUMN09 as COLUMN14
	 FROM dbo.SATABLE006  a inner join SATABLE007 b on b.COLUMN06=cast( @SalesOrderID as nvarchar)
	 --  inner join FITABLE010 f on a.COLUMN03=f.COLUMN03 
	 WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)
   end
   else
   begin
	SELECT 0 as COLUMN04,a.COLUMN03 as COLUMN05, a.COLUMN05 as COLUMN06, a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09, ( c.COLUMN09) as COLUMN10,0 as COLUMN12
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	--, a.COLUMN09 as COLUMN13, (c.COLUMN09)*a.COLUMN09 as COLUMN14
	FROM dbo.SATABLE006  a inner join  SATABLE007 b on b.COLUMN06=cast( @SalesOrderID as nvarchar)
	inner join SATABLE008 c on c.COLUMN14=b.COLUMN01
	--  inner join FITABLE010 f on c.COLUMN04=f.COLUMN03 
	  and c.COLUMN04=a.COLUMN03
	WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)  
	end
   end
   else 
   begin
    SELECT  0 as COLUMN04,a.COLUMN03 as COLUMN05,a.COLUMN05 as COLUMN06,a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09,
	 (select case when (a.COLUMN07-isnull(a.COLUMN13,0))>isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) 
	 then isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) else
	 (a.COLUMN07-isnull(a.COLUMN13,0)) end) as COLUMN10,0 as COLUMN12
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	--,a.COLUMN09 as COLUMN13,a.COLUMN07*a.COLUMN09 as COLUMN14
	FROM dbo.SATABLE006  a inner join SATABLE005 b on b.COLUMN01=a.COLUMN19 
	--inner join FITABLE010 f on a.COLUMN03=f.COLUMN03 
	WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)
    end
	
END
else
 BEGIN
   set @tmpVal=(select isnull(sum(isnull(COLUMN12,0)),0) from SATABLE006 WHERE COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID))
   set @IRQty=(select isnull(sum(isnull(COLUMN08,0)),0) from PUTABLE004 WHERE COLUMN12 in (SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE COLUMN06= cast( @SalesOrderID as nvarchar) and COLUMN02=@Form))
   set @IIQty=(select isnull(sum(isnull(COLUMN09,0)),0) from SATABLE008 WHERE COLUMN14 in (SELECT COLUMN01 FROM dbo.SATABLE007 WHERE COLUMN06=cast( @SalesOrderID as nvarchar)))
   set @tmpVal1=(select isnull(sum(isnull(COLUMN13,0)),0) from SATABLE006 WHERE COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID))
   set @Shipedqty1=((select isnull(sum(isnull(COLUMN09,0)),0) from SATABLE010 where COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast( @SalesOrderID as nvarchar) ) ))
   
   --joii
    if(@Shipedqty1>0)
   begin
    --set @IRID=(SELECT COLUMN02 FROM PUTABLE003 WHERE COLUMN17= '1000' and COLUMN06= @SalesOrderID)
   SELECT distinct d.COLUMN02 as COLUMN04, a.COLUMN03 as COLUMN05,a.COLUMN17 as COLUMN07,a.COLUMN04 as COLUMN06 , 
	 max(a.COLUMN05) as COLUMN07,max(a.COLUMN06) as COLUMN08,isnull(max(b.COLUMN09),0) as COLUMN09,
	((max(a.COLUMN08)-isnull(max(b.COLUMN09),0))) as COLUMN10,0 as COLUMN12 ,M7.COLUMN64 as COLUMN21,a.COLUMN10 as COLUMN13,a.COLUMN11 as COLUMN14
	FROM dbo.PUTABLE004 a inner join PUTABLE003 d on d.COLUMN01=a.COLUMN12
	  --inner join FITABLE010 f on a.COLUMN04=f.COLUMN03
	  left outer join MATABLE007 M7 on M7.COLUMN02=a.COLUMN03
	 left outer join SATABLE010 b on b.COLUMN04 =d.COLUMN02 and 
	b.COLUMN05= a.COLUMN03 and b.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06=cast( @SalesOrderID as nvarchar) )
	WHERE a.COLUMN12 in (SELECT COLUMN01 FROM dbo.PUTABLE003 WHERE  COLUMN06=cast( @SalesOrderID as nvarchar) and  (  COLUMN02= @Form) ) group by  d.COLUMN02,a.COLUMN03,a.COLUMN17,a.COLUMN04,M7.COLUMN64,a.COLUMN10,a.COLUMN11
   end
   else
   begin
   if(@IRQty>0)
   begin
   set @IRID=(SELECT COLUMN02 FROM PUTABLE003 WHERE COLUMN04= @Form or COLUMN02= @Form)

    SELECT distinct b.COLUMN02 as COLUMN04,c.COLUMN03 as COLUMN05,c.COLUMN17 as COLUMN07,c.COLUMN04 as COLUMN06,c.COLUMN05 as COLUMN07 , 
	c.COLUMN06 as COLUMN08,0 as COLUMN09,c.COLUMN08 as COLUMN10,0 as COLUMN12 ,M7.COLUMN64 as COLUMN21,c.COLUMN10 as COLUMN13,c.COLUMN11 as COLUMN14
	FROM PUTABLE004 c inner join PUTABLE003 b on b.COLUMN01=c.COLUMN12  and (  b.COLUMN02= @Form)
	--inner join FITABLE010 f on c.COLUMN03=f.COLUMN03
	 left outer join MATABLE007 M7 on M7.COLUMN02=c.COLUMN03
	where c.COLUMN12 in (SELECT COLUMN01 FROM PUTABLE003 WHERE   COLUMN02= @Form)

   end
   else if(@IIQty>0)
   begin
   if(@tmpVal=0 and @tmpVal1=0)
   begin
    SELECT  0 as COLUMN04,a.COLUMN03 as COLUMN05,a.COLUMN05 as COLUMN06,a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09,
	 (select case when (a.COLUMN07-isnull(a.COLUMN13,0))>isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) 
	 then isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) else
	 (a.COLUMN07-isnull(a.COLUMN13,0)) end) as COLUMN10,0 as COLUMN12
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	--,a.COLUMN09 as COLUMN13,a.COLUMN07*a.COLUMN09 as COLUMN14
	FROM dbo.SATABLE006  a inner join SATABLE005 b on b.COLUMN01=a.COLUMN19 
	--inner join FITABLE010 f on a.COLUMN03=f.COLUMN03 
	WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)
   end
   else if(@tmpVal=0 and @tmpVal1>0)
   begin
   SELECT  b.COLUMN02 as COLUMN04,a.COLUMN03 as COLUMN05,a.COLUMN05 as COLUMN06,a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09,
	 (select case when (a.COLUMN07-isnull(a.COLUMN13,0))>isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) 
	 then isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03 AND COLUMN13=A.COLUMN20),0) else
	 (a.COLUMN07-isnull(a.COLUMN13,0)) end) as COLUMN10,0 as COLUMN12
	 --isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	 --,a.COLUMN09 as COLUMN13,a.COLUMN07*a.COLUMN09 as COLUMN14
	 FROM dbo.SATABLE006  a inner join SATABLE007 b on b.COLUMN06=cast( @SalesOrderID as nvarchar)
	 --  inner join FITABLE010 f on a.COLUMN03=f.COLUMN03 
	 WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)
   end
   else
   begin
	SELECT  b.COLUMN02 as COLUMN04,a.COLUMN03 as COLUMN05, a.COLUMN05 as COLUMN06, a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09, ( c.COLUMN09) as COLUMN10,0 as COLUMN12
	-- isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	--, a.COLUMN09 as COLUMN13, (c.COLUMN09)*a.COLUMN09 as COLUMN14
	FROM dbo.SATABLE006  a inner join  SATABLE007 b on b.COLUMN06=cast( @SalesOrderID as nvarchar)
	inner join SATABLE008 c on c.COLUMN14=b.COLUMN01
	--inner join FITABLE010 f on c.COLUMN04=f.COLUMN03 
	and c.COLUMN04=a.COLUMN03
	WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)  
	end
   end
   else 
   begin
    SELECT  b.COLUMN02 as COLUMN04,a.COLUMN03 as COLUMN05, a.COLUMN05 as COLUMN06, a.COLUMN15 as COLUMN07 , 
	 a.COLUMN07 as COLUMN08, a.COLUMN13 as COLUMN09, ( c.COLUMN09) as COLUMN10,0 as COLUMN12
	-- isnull((select COLUMN04 from FITABLE010 where COLUMN03=a.COLUMN03),0) as COLUMN11,
	--, a.COLUMN09 as COLUMN13, (c.COLUMN09)*a.COLUMN09 as COLUMN14
	FROM dbo.SATABLE006  a inner join  SATABLE007 b on b.COLUMN06=cast( @SalesOrderID as nvarchar)
	inner join SATABLE008 c on c.COLUMN14=b.COLUMN01
	--inner join FITABLE010 f on c.COLUMN04=f.COLUMN03 
	and c.COLUMN04=a.COLUMN03
	WHERE a.COLUMN19 in (SELECT COLUMN01 FROM dbo.SATABLE005 WHERE COLUMN02= @SalesOrderID)  
    end
	
END

end



END




 
















GO
