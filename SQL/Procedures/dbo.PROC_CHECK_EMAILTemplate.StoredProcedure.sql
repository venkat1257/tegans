USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PROC_CHECK_EMAILTemplate]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PROC_CHECK_EMAILTemplate]
(@Ide INT =NULL,@FormName nvarchar(250)=NULL,@DateFormat nvarchar(250)=NULL,@ACOWNER INT=NULL)
AS 
BEGIN
declare @FormId INT =NULL,@Type INT =NULL,@Body nvarchar(250)=NULL,@Transno nvarchar(250)=NULL,@Cust nvarchar(250)=NULL,
@Date nvarchar(250)=NULL,@DueDate nvarchar(250)=NULL,@TemId nvarchar(250)=NULL,@TotalAmt decimal(18,2) =NULL,@DueAmt decimal(18,2) =NULL
set @FormId=(SELECT COLUMN02 FROM CONTABLE0010 WHERE COLUMN04=@FormName and COLUMNA03 is null)
if(@FormId=1375)
begin
set @Type=(22970)
end
else if(@FormId=1275)
begin
set @Type=(22972)
end
else if(@FormId=1276)
begin
set @Type=(22973)
end
else if(@FormId=1277)
begin
set @Type=(22974)
end
else if(@FormId=1278)
begin
set @Type=(22975)
end

set @TemId=(SELECT COLUMN02 TemId FROM SETABLE017 WHERE COLUMNA03=@ACOWNER and COLUMN04=@Type and isnull(COLUMNA13,0)=0 and COLUMN06=1)
if not exists(SELECT COLUMN02 TemId FROM SETABLE017 WHERE COLUMNA03=@ACOWNER and COLUMN04=@Type and isnull(COLUMNA13,0)=0 and COLUMN06=1)
begin
set @TemId=(SELECT COLUMN02 TemId FROM SETABLE017 WHERE isnull(COLUMNA03,0)=0 and COLUMN04=@Type and isnull(COLUMNA13,0)=0 and COLUMN06=1)
end
else if(@TemId='' or @TemId=NULL)
begin
set @TemId=(SELECT COLUMN02 TemId FROM SETABLE017 WHERE isnull(COLUMNA03,0)=0 and COLUMN04=@Type and isnull(COLUMNA13,0)=0 and COLUMN06=1)
end
set @Type=(SELECT COLUMN04 Type FROM SETABLE017 WHERE COLUMN02=@TemId)
set @Body=(SELECT COLUMN09 Body FROM SETABLE017 WHERE COLUMN02=@TemId)
if(@FormId=1375 and @Type=22970)
begin
set @Transno=(SELECT p.COLUMN04 Transno FROM SATABLE013 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Cust=(SELECT s.COLUMN05 Cust FROM SATABLE013 p left join SATABLE002 s on s.COLUMN02=p.COLUMN05 and 
s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @TotalAmt=(SELECT p.COLUMN17 TotalAmt FROM SATABLE013 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueAmt=(SELECT p.COLUMN17 DueAmt FROM SATABLE013 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Date=(SELECT p.COLUMN07 Date FROM SATABLE013 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueDate=(SELECT p.COLUMN08 DueDate FROM SATABLE013 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
end
else if(@FormId=1275 and @Type=22972)
begin
set @Transno=(SELECT p.COLUMN04 Transno FROM SATABLE005 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Cust=(SELECT s.COLUMN05 Cust FROM SATABLE005 p left join SATABLE002 s on s.COLUMN02=p.COLUMN05 and 
s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @TotalAmt=(SELECT p.COLUMN15 TotalAmt FROM SATABLE005 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueAmt=(SELECT p.COLUMN15 DueAmt FROM SATABLE005 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Date=(SELECT p.COLUMN06 Date FROM SATABLE005 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueDate=(SELECT p.COLUMN08 DueDate FROM SATABLE005 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
end
else if(@FormId=1276 and @Type=22973)
begin
set @Transno=(SELECT p.COLUMN04 Transno FROM SATABLE007 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Cust=(SELECT s.COLUMN05 Cust FROM SATABLE007 p left join SATABLE002 s on s.COLUMN02=p.COLUMN05 and 
s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @TotalAmt=(SELECT sum(isnull(q.COLUMN13,0)) TotalAmt FROM SATABLE007 p inner join SATABLE008 q on p.COLUMN01=q.COLUMN14 and isnull(q.COLUMNA13,0)=0 and p.COLUMNA03=q.COLUMNA03  WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueAmt=(SELECT sum(isnull(q.COLUMN13,0)) TotalAmt FROM SATABLE007 p inner join SATABLE008 q on p.COLUMN01=q.COLUMN14 and isnull(q.COLUMNA13,0)=0 and p.COLUMNA03=q.COLUMNA03  WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
--set @TotalAmt=(0)
--set @DueAmt=(0)
set @Date=(SELECT p.COLUMN08 Date FROM SATABLE007 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueDate=(SELECT p.COLUMN08 DueDate FROM SATABLE007 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
end
else if(@FormId=1277 and @Type=22974)
begin
set @Transno=(SELECT p.COLUMN04 Transno FROM SATABLE009 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Cust=(SELECT s.COLUMN05 Cust FROM SATABLE009 p left join SATABLE002 s on s.COLUMN02=p.COLUMN05 and 
s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @TotalAmt=(SELECT p.COLUMN20 TotalAmt FROM SATABLE009 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueAmt=(SELECT p.COLUMN20 DueAmt FROM SATABLE009 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Date=(SELECT p.COLUMN08 Date FROM SATABLE009 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueDate=(SELECT p.COLUMN10 DueDate FROM SATABLE009 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
end
else if(@FormId=1278 and @Type=22975)
begin
set @Transno=(SELECT p.COLUMN04 Transno FROM SATABLE011 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Cust=(SELECT s.COLUMN05 Cust FROM SATABLE011 p left join SATABLE002 s on s.COLUMN02=p.COLUMN05 and 
s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @TotalAmt=(SELECT p.COLUMN13 TotalAmt FROM SATABLE011 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueAmt=(SELECT p.COLUMN13 DueAmt FROM SATABLE011 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @Date=(SELECT p.COLUMN19 Date FROM SATABLE011 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
set @DueDate=(SELECT p.COLUMN19 DueDate FROM SATABLE011 p WHERE p.COLUMN02=@Ide and p.COLUMNA03=@ACOWNER and isnull(p.COLUMNA13,0)=0)
end
if( @Type is not null and @Type!='')
begin
SELECT replace(replace(replace(replace(replace(replace(@Body,'[Trans No]',@Transno),'[Contact Name]',@Cust),'[Total Amount]',@TotalAmt),'[Due Amount]',@DueAmt),'[Date]',format(cast(@Date as date),@DateFormat)),'[Due Date]',format(cast(@DueDate as date),@DateFormat)) Body 
END
END

GO
