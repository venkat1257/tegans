USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_CLOSESTOCK]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_REPORTS_CLOSESTOCK](
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
 )
as
--begin
--if @Item!=''
--begin
-- set @whereStr= ' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
--end
--if @OperatingUnit!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
--end
--end
--if @Brand!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
----EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
--end
--end
--if @Location!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
--end
--end
----if @Type!=''
----begin
----if(@whereStr='' or @whereStr is null)
----begin
----set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
----end
----else
----begin
----set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
----end
----end select * from fitable048
--select top 1 @FiscalYearStartDt=column05 from FITABLE048 where COLUMNA03=@AcOwner AND COLUMN09=1 order by COLUMN01 desc

--if @Project!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
--end
--end
--if @UPC!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s) '
--end
--else
--begin
--set @whereStr=@whereStr+' and upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
--end
--end
--if(@whereStr='' or @whereStr is null)
--begin
-- set @whereStr=' where 1=1'
-- end
 
--declare @DecimalPositions int
--if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
--begin
--set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
--end
--else
--begin set @DecimalPositions=(2) end

--select Q.ou,f.COLUMN04 item,f.COLUMN06 upc,Q.uomid,Q.uom,Q.Location,Q.Lot,Q.tno,Q.tdate,Q.Dtt,Q.vendor,
----EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
--convert(float,convert(double precision,Q.openbal)) openbal,
--convert(float,convert(double precision,Q.qr)) qr,
--convert(float,convert(double precision,Q.qi)) qi,
--Q.price,Q.amt,Q.transType,Q.ItemID,Q.OPID,Q.TransTypeID,Q.Project,Q.fid,(case when f.COLUMN10=0 then null else f.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,Q.LocID,Q.LotID,f.column50 descr into #StockLedgerReport from (
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--(select c7.COLUMN03 ou,c.COLUMN03 item,c.COLUMN03 upc,c.COLUMN17 uomid,(case when c.COLUMN17=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24)) Lot,
--b.COLUMN04 tno,FORMAT(b.COLUMN09,@DateF) tdate,b.COLUMN09 Dtt, (case when b.COLUMN03=1272 then (s1.COLUMN05) else (s2.COLUMN05) end) vendor ,
--0 openbal,iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,c.COLUMN08) qr,0 qi,c.COLUMN10 price,((c.COLUMN08))*(c.COLUMN10) amt,'Receipt' transType,c.COLUMN03 ItemID,b.COLUMN13 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN21 Project 
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
--,b.COLUMN03  fid,c.COLUMN03 BrandID,c.COLUMN03 as Brand,b.COLUMN22 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24) LotID,c.column03 descr
--from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 and b.column03!=1587
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
----inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner  
--left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN13 left join satable001 s1 on s1.column02=b.column05 and s1.columna03=b.columna03 left join satable002 s2 on s2.column02=b.column05 and s2.columna03=b.columna03
--left join MATABLE002 m on m.COLUMN02=c.COLUMN17 and m.COLUMNA03=c.COLUMNA03  left join CONTABLE030 l on l.COLUMN02=b.COLUMN22 and l.COLUMNA02=b.COLUMN13 and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN24 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN03 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
--b.COLUMN09 between @FromDate and @ToDate and b.COLUMN09 <=@FiscalYearStartDt
--)
-- UNION ALL
--(select c7.COLUMN03 ou,c.COLUMN04 item,c.COLUMN04 upc,iif(isnull(mu.COLUMN08,0)!=0,mu.COLUMN08,c.COLUMN19) uomid,(case when (iif(mu.COLUMN08!='',mu.COLUMN08,c.COLUMN19))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08!=0,mu.COLUMN08,c.COLUMN19))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--b.COLUMN04 tno,FORMAT(b.COLUMN08,@DateF) tdate,b.COLUMN08 Dtt, (s1.COLUMN05) vendor ,
--0 openbal,iif(mu.COLUMN08!='',mu.COLUMN07,c.COLUMN09) qr,0 qi,c.COLUMN11 price,cast((c.COLUMN09)*(c.COLUMN11)as decimal(18,2)) amt,'Bill' transType,c.COLUMN04 ItemID,b.COLUMN15 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN26 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,b.COLUMN03  fid,c.COLUMN04 BrandID,c.COLUMN04 as Brand,b.COLUMN29 LocID,c.COLUMN27 LotID,c.column04 descr
--from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN04 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
----inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN15  left join satable001 s1 on s1.column02=b.column05 and s1.columna03=b.columna03
-- left join MATABLE002 m on m.COLUMN02=c.COLUMN19 and m.COLUMNA03=c.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=b.COLUMN29 and l.COLUMNA02=b.COLUMN15 and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN27 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
-- --left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
-- where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null)  AND isnull((b.COLUMNA13),0)=0 and  
-- ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 
-- and b.COLUMN08 between @FromDate and @ToDate and b.COLUMN08 <=@FiscalYearStartDt
--)
-- UNION ALL
-- --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--(select c7.COLUMN03 ou,d.COLUMN04 item,d.COLUMN04 upc,d.COLUMN19 uomid,(case when d.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24)) Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt,(case when a.COLUMN02=1354 then (s1.COLUMN05) else (s2.COLUMN05) end) vendor,
--0 openbal, 0 qr,iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,d.COLUMN09) qi,d.COLUMN12 price,-((d.COLUMN09))*(d.COLUMN12) amt,'Issue' transType,d.COLUMN04 ItemID,a.COLUMN15 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN22 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
--,a.COLUMN03 fid,d.COLUMN04 BrandID,d.COLUMN04 as Brand,a.COLUMN23 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24) LotID,d.column04 descr
--from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0 and a.column03!=1588
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN15  left join satable001 s1 on s1.column02=a.column05 and s1.columna03=a.columna03 left join satable002 s2 on s2.column02=a.column05 and s2.columna03=a.columna03
--left join MATABLE002 m on m.COLUMN02=d.COLUMN19 and m.COLUMNA03=d.COLUMNA03   left join CONTABLE030 l on l.COLUMN02=a.COLUMN23 and l.COLUMNA02=a.COLUMN15 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN24 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN04 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
-- where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
-- AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 <=@FiscalYearStartDt
--)
-- UNION ALL
-- --EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--(select c7.COLUMN03 ou,d.COLUMN05 item,d.COLUMN05 upc,iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, s2.COLUMN05 vendor,
--0 openbal, 0 qr,iif(mu.COLUMN08!='',mu.COLUMN07,d.COLUMN10) qi ,d.COLUMN13 price, -((d.COLUMN10))*(d.COLUMN13)  amt,'Invoice' transType,d.COLUMN05 ItemID,a.COLUMN14 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN29 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,a.COLUMN03 fid,d.COLUMN05 BrandID,d.COLUMN05 as Brand,a.COLUMN39 LocID,d.COLUMN31 LotID,d.column05 descr
--from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN05 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN14  left join satable002 s2 on s2.column02=a.column05 and s2.columna03=a.columna03
-- --left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and m.COLUMNA03=d.COLUMNA03 
-- left join CONTABLE030 l on l.COLUMN02=a.COLUMN39 and l.COLUMNA02=a.COLUMN14 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN31 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
-- --left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
-- where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
-- AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
-- and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 <=@FiscalYearStartDt
--)

--UNION ALL 
--  (

--select c7.COLUMN03 ou,c.COLUMN03 item,c.COLUMN03 upc ,c.COLUMN11 uomid,(case when (c.COLUMN11)=10000 then (select column05 from fitable037 where column02=10000) 
--else (select COLUMN04 from MATABLE002 where column02=(c.COLUMN11)) end) uom,'' Location,'' Lot,b.COLUMN04 tno,
--FORMAT(b.COLUMN09,@DateF) tdate,b.COLUMN09 Dtt, s2.COLUMN05 vendor, 0 openbal, 0 qr, C.COLUMN06 qi,c.COLUMN08 price,
--((c.COLUMN06)*(c.COLUMN08)) amt,'Consumption' transType,c.COLUMN03 ItemID,b.COLUMN11 OPID, (case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN08 Project
--,b.COLUMN03  fid,c.COLUMN03 BrandID,c.COLUMN03 as Brand,'' LocID,'' LotID,c.column03 descr
-- from PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
-- --inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner
-- left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN11  left join satable002 s2 on s2.column02=b.column06 and s2.columna03=b.columna03
--  --left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--   where  isnull((b.COLUMNA13),0)=0 and  
-- ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 

-- and b.COLUMN09 between @FromDate and @ToDate and b.COLUMN09 <=@FiscalYearStartDt

-- )

-- UNION ALL
-- --EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--  (select c7.COLUMN03  ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,dl.COLUMN04 Location,null Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, null vendor,
--0 openbal,case when a.COLUMN06!='' then (iif(mu.COLUMN08!='',mu.COLUMN07,d.COLUMN06)) else 0 end qr, 0  qi ,i.COLUMN17 price,(i.COLUMN17*(d.COLUMN06)) amt,'Transfer' transType,d.COLUMN03 ItemID,a.COLUMN06 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
-- ,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN10 LocID,NULL LotID,d.column03 descr
-- from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on (c7.column02=a.column06)  --left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 
-- left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
-- left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMNA02 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
-- --left join MATABLE002 m on m.COLUMN02=d.COLUMN13 and m.COLUMNA03=d.COLUMNA03 
-- left join CONTABLE030 dl on dl.COLUMN02=a.COLUMN10  and dl.COLUMNA03=a.COLUMNA03
-- --left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 <=@FiscalYearStartDt)
--UNION ALL
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--(select c7.COLUMN03  ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else ( select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,sl.COLUMN04 Location,null Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, null vendor,
--0 openbal,0  qr,case when a.COLUMN05!='' then  (iif(mu.COLUMN08!='',mu.COLUMN07,d.COLUMN06)) else 0 end qi ,i.COLUMN17 price,-(i.COLUMN17*(d.COLUMN06)) amt,'Transfer' transType,d.COLUMN03 ItemID,a.COLUMN05 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project 
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN09 LocID,NULL LotID,d.column03 descr
-- from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on (c7.Column02=a.column05) --left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMN05 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--  --left join MATABLE002 m on m.COLUMN02=d.COLUMN13 and m.COLUMNA03=d.COLUMNA03 
-- left join CONTABLE030 sl on sl.COLUMN02=a.COLUMN09  and sl.COLUMNA03=a.COLUMNA03 
-- --left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 <=@FiscalYearStartDt
--)UNION ALL
-- (select c7.COLUMN03 ou,d.COLUMN03 item,d.COLUMN03 upc,d.COLUMN22 uomid,(case when d.COLUMN22=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN05,@DateF) tdate,a.COLUMN05 Dtt, null vendor,
--0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,isnull(d.COLUMN10,0) price,CAST((isnull(d.COLUMN08,0)-isnull(d.COLUMN16,0))*isnull(d.COLUMN10,0) AS DECIMAL(18,2)) amt,'Adjustment' transType,d.COLUMN03 ItemID,a.COLUMN10 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,'' Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN16 LocID,d.COLUMN23 LotID,d.column03 descr
--from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and d.COLUMNa02=a.COLUMNa02 and d.COLUMNa03=a.COLUMNa03  and   isnull((d.COLUMNA13),0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN10 
--left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and m.COLUMNA03=d.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN16 and l.COLUMNA02=a.COLUMN10 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN23 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and isnull(bm.COLUMNA13,0)=0 and bm.COLUMNA03=d.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and a.COLUMN05 <=@FiscalYearStartDt
--)
--UNION ALL
--(select c7.COLUMN03 ou,c.COLUMN03 item,c.COLUMN03 upc,c.COLUMN26 uomid,(case when c.COLUMN26=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--b.COLUMN04 tno,FORMAT(b.COLUMN06,@DateF) tdate,b.COLUMN06 Dtt, (case when b.COLUMN50='22335' then s2.COLUMN05 else s1.COLUMN05 end) vendor ,
--0 openbal,0 qr,c.COLUMN07 qi,c.COLUMN09 price,-((c.COLUMN07)*(c.COLUMN09)) amt,'Debit Memo' transType,c.COLUMN03 ItemID,b.COLUMN24 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN36 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,b.COLUMN03  fid,c.COLUMN03 BrandID,c.COLUMN03 as Brand,b.COLUMN47 LocID,c.COLUMN17 LotID,c.column03 descr
--from PUTABLE001 b inner join PUTABLE002 c on c.COLUMN19=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
----inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner  
--left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN24 
--left join MATABLE002 m on m.COLUMN02=c.COLUMN26 and m.COLUMNA03=c.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=b.COLUMN47 and l.COLUMNA02=b.COLUMN24 and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN17 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
--left outer join SATABLE001 s1 on s1.COLUMN02=b.COLUMN05 and s1.COLUMNA03=b.COLUMNA03
--left outer join SATABLE002 s2 on s2.COLUMN02=b.COLUMN05 and s2.COLUMNA03=b.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
--b.COLUMN06 between @FromDate and @ToDate and b.COLUMN03=1355 and b.COLUMN33='' and b.COLUMN06 <=@FiscalYearStartDt
--)
-- UNION ALL
-- (select c7.COLUMN03 ou,d.COLUMN03 item,d.COLUMN03 upc,d.COLUMN27 uomid,(case when d.COLUMN27=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--a.COLUMN04 tno,FORMAT(a.COLUMN06,@DateF) tdate,a.COLUMN06 Dtt, (case when a.COLUMN51='22305' then s1.COLUMN05 else s2.COLUMN05 end) vendor,
--0 openbal, d.COLUMN07 qr,0 qi,d.COLUMN09 price, ((d.COLUMN07)*(d.COLUMN09))  amt,'Credit Memo' transType,d.COLUMN03 ItemID,a.COLUMN24 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN35 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,a.COLUMN03 fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN48 LocID,d.COLUMN17 LotID,d.column03 descr
--from SATABLE005 a inner join SATABLE006 d on d.COLUMN19=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
----inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN24 
--left join MATABLE002 m on m.COLUMN02=d.COLUMN27 and m.COLUMNA03=d.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN48 and l.COLUMNA02=a.COLUMN24 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN17 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
--left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03
--left outer join SATABLE002 s2 on s2.COLUMN02=a.COLUMN05 and s2.COLUMNA03=a.COLUMNA03
----left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN06 between @FromDate and @ToDate and a.COLUMN03=1330 and a.COLUMN33='' and a.COLUMN06 <=@FiscalYearStartDt
--)
-- UNION ALL
-- ----------------
--(select ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,isnull(sum(openbal),0) as openbal,qr,qi,null price,null amt,transType,ItemID,OPID,0 TransTypeID,null Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,fid,BrandID,Brand, LocID,LotID,max(descr) descr from (
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,c.COLUMN03 item,c.COLUMN03 upc,c.COLUMN17 uomid,(case when c.COLUMN17=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24)) Lot,
--null tno,null tdate,null Dtt, null vendor ,
--iif(isnull(mu.COLUMN12,0)!=0,sum(isnull(mu.column07,0)),sum(isnull(c.column08,0))) openbal,null qr,null qi,sum(isnull(c.column10,0)) price,((sum(isnull(c.column08,0)))*(sum(isnull(c.column10,0)))) amt,null transType,c.COLUMN03 ItemID,b.COLUMN13 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN21 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
--,null  fid,c.COLUMN03 BrandID,c.COLUMN03 as Brand,b.COLUMN22 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24) LotID,c.COLUMN03 descr
--from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 and b.column03!=1587
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
-- left join MATABLE002 m on m.COLUMN02=c.COLUMN17 and m.COLUMNA03=c.COLUMNA03  left join CONTABLE030 l on l.COLUMN02=b.COLUMN22 and l.COLUMNA02=b.COLUMN13 and l.COLUMNA03=b.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=c.COLUMN24 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
-- --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
-- left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN03 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  
-- AND  b.COLUMNA03=@AcOwner and (b.COLUMN09< @FromDate) and b.COLUMN09 <=@FiscalYearStartDt and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
-- group by f.COLUMN10,c.COLUMN24,f.COLUMN06,m5.COLUMN04,b.COLUMN13,f.COLUMN04,c.COLUMN03,b.COLUMN03,b.COLUMN21,c.COLUMN17,m.COLUMN04,l.COLUMN04,f.COLUMN17,mu.COLUMN12,b.COLUMN22
-- union all
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,c.COLUMN04 item,c.COLUMN04 upc,iif(isnull(mu.COLUMN08,0)!=0,mu.COLUMN08,c.COLUMN19) uomid,(case when (iif(mu.COLUMN08!='',mu.COLUMN08,c.COLUMN19))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08!=0,mu.COLUMN08,c.COLUMN19))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,null tdate,null Dtt, null vendor ,
--iif(mu.COLUMN08!='',sum(isnull(mu.column07,0)),sum(isnull(c.COLUMN09,0))) openbal,null qr,null qi,sum(isnull(c.COLUMN11,0)) price,cast((sum(isnull(c.COLUMN09,0))*(sum(isnull(c.COLUMN11,0))))as decimal(18,2)) amt,null transType,c.COLUMN04 ItemID,b.COLUMN15 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN26 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null  fid,c.COLUMN04 BrandID,c.COLUMN04 as Brand,b.COLUMN29 LocID,c.COLUMN27 LotID,c.COLUMN04 descr
--from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
--left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN04 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--left join MATABLE002 m on m.COLUMN02=c.COLUMN19 and m.COLUMNA03=c.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=b.COLUMN29 and l.COLUMNA02=b.COLUMN15 and l.COLUMNA03=b.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=c.COLUMN27 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
-- where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0  
-- AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
-- --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
-- AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN08 <=@FiscalYearStartDt and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
-- group by f.COLUMN10,c.COLUMN27,bm.COLUMN04,f.COLUMN06,m5.COLUMN04,b.COLUMN15,f.COLUMN04,c.COLUMN04,b.COLUMN03,b.COLUMN26,c.COLUMN19,m.COLUMN04,l.COLUMN04,mu.COLUMN08,b.COLUMN29)
--  union all
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,d.COLUMN04 item,d.COLUMN04 upc,d.COLUMN19 uomid,(case when d.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24)) Lot,
--null tno,null tdate,null Dtt, null vendor,
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
---(iif(isnull(mu.COLUMN12,0)!=0,sum(isnull(mu.column07,0)),sum(isnull(d.column09,0)))) openbal, null qr,null qi ,sum(isnull(d.column12,0)) price,-(((sum(isnull(d.column09,0))))*(sum(isnull(d.column12,0)))),null transType,d.COLUMN04 ItemID,a.COLUMN15 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN22 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
--,null fid,d.COLUMN04 BrandID,d.COLUMN04 as Brand,a.COLUMN23 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24) LotID,d.COLUMN04 descr
--from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0 and a.column03!=1588
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join MATABLE002 m on m.COLUMN02=d.COLUMN19 and m.COLUMNA03=d.COLUMNA03   left join CONTABLE030 l on l.COLUMN02=a.COLUMN23 and l.COLUMNA02=a.COLUMN15 and l.COLUMNA03=a.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=d.COLUMN24 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN04 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  
--AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and a.COLUMN08 <=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
--group by g.COLUMN10,g.COLUMN06,d.COLUMN24,m5.COLUMN04,a.COLUMN15,g.COLUMN04,d.COLUMN04,a.COLUMN03,a.COLUMN22,d.COLUMN19,m.COLUMN04,l.COLUMN04,mu.COLUMN12,a.COLUMN23)
--  union all
--  --EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,d.COLUMN05 item,d.COLUMN05 upc,iif(mu.COLUMN08 !=0,mu.COLUMN08,d.COLUMN22) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN22))) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,null tdate,null Dtt, null vendor,
---(iif(mu.COLUMN08!='',sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN10,0)))) openbal, null qr,null qi,sum(isnull(d.COLUMN13,0)) price, -((iif(mu.COLUMN08!='',sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN10,0))))*(sum(isnull(d.COLUMN13,0))))  amt,null transType,d.COLUMN05 ItemID,a.COLUMN14 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN29 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null fid,d.COLUMN05 BrandID,d.COLUMN05 as Brand,a.COLUMN39 LocID,d.COLUMN31 LotID,d.COLUMN05 descr
--from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01  and  isnull((d.COLUMNA13),0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN05 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
----left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and m.COLUMNA03=d.COLUMNA03 
--left join CONTABLE030 l on l.COLUMN02=a.COLUMN39 and l.COLUMNA02=a.COLUMN14 and l.COLUMNA03=a.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=d.COLUMN31 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
--where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
--AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--and a.COLUMN08<@FromDate and a.COLUMN08 <=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
--group by g.COLUMN10,d.COLUMN31,bm.COLUMN04,g.COLUMN06,m5.COLUMN04,a.COLUMN14,g.COLUMN04,d.COLUMN05,a.COLUMN03,a.COLUMN29,d.COLUMN22,mu.COLUMN08,l.COLUMN04,a.COLUMN39)
-- union all
 
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN11) ou,c.COLUMN03 item,c.COLUMN03 upc ,c.COLUMN11 uomid,(case when (c.COLUMN11)=10000 then (select column05 from fitable037 where column02=10000) 
--else (select COLUMN04 from MATABLE002 where column02=(c.COLUMN11)) end) uom,NULL Location,NULL Lot,null tno,
--null tdate,null Dtt, null vendor, -sum(isnull(C.COLUMN06,0)) openbal, null qr, null qi,sum(isnull(c.COLUMN08,0)) price,
--(sum(isnull(c.COLUMN06,0))*(sum(isnull(c.COLUMN08,0)))) amt,null transType,c.COLUMN03 ItemID,b.COLUMN11 OPID, (case when ''='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN08 Project
--,null  fid,c.COLUMN03 BrandID,c.COLUMN03 as Brand,'' LocID,'' LotID,c.COLUMN03 descr
-- from PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
-- inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner
-- left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
--  left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
--   where  isnull((b.COLUMNA13),0)=0 and  
-- ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 

--and b.COLUMN09 <@FromDate  and b.COLUMN09 <=@FiscalYearStartDt

-- group by b.COLUMN11,f.COLUMN04,f.COLUMN06,c.COLUMN11,c.COLUMN03,b.COLUMN08,f.COLUMN10,m5.COLUMN04)

--union all
-- --EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
-- (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN05) ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,sl.COLUMN04 Location,null Lot,
--null tno,null tdate,null Dtt, null vendor,
---(iif(mu.COLUMN08  !=0,sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN06,0)))) openbal,null qr,null qi ,sum(isnull(i.column07,0)) price,(sum(isnull(i.column07,0))*(sum(isnull(d.column06,0)))) amt,null transType,d.COLUMN03 ItemID,a.COLUMN05 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN09 LocID,NULL LotID,d.COLUMN03 descr
--from PRTABLE003 a left join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0 and  a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and i.COLUMN13=d.COLUMN05 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
-- --left join MATABLE002 m on m.COLUMN02=s.COLUMN13 and m.COLUMNA03=s.COLUMNA03 
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
--left join CONTABLE030 sl on sl.COLUMN02=a.COLUMN09 and sl.COLUMNA02=a.COLUMN05 and sl.COLUMNA03=a.COLUMNA03
--where  isnull((a.COLUMNA13),0)=0 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.COLUMN08 <=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--group by g.COLUMN10,g.COLUMN06,m5.COLUMN04,a.columna02,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN07,mu.COLUMN08,sl.COLUMN04,a.COLUMN05,a.COLUMN06,a.COLUMN09,d.COLUMN13
--) union all
-- --EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
-- (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN06) ou,d.COLUMN03 item,d.COLUMN03 upc,(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13)) uomid,(case when (iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from matable002 where column02=(iif(mu.COLUMN08  !=0,mu.COLUMN08,d.COLUMN13))) end) uom,dl.COLUMN04 Location,null Lot,
--null tno,null tdate,null Dtt, null vendor,
--(iif(mu.COLUMN08  !=0,sum(isnull(mu.column07,0)),sum(isnull(d.COLUMN06,0)))) openbal,null qr,null qi ,SUM(ISNULL(i.COLUMN17,0)) price,(SUM(ISNULL(i.COLUMN17,0))*(sum(isnull(d.column06,0)))) amt,null transType,d.COLUMN03 ItemID,a.COLUMN06 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,d.COLUMN07 Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN10 LocID,NULL LotID,d.COLUMN03 descr
--from PRTABLE003 a left join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0 and  a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and i.COLUMN13=d.COLUMN06 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0
--left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
-- --left join MATABLE002 m on m.COLUMN02=s.COLUMN13 and m.COLUMNA03=s.COLUMNA03 
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
--left join CONTABLE030 dl on dl.COLUMN02=a.COLUMN10 and dl.COLUMNA02=a.COLUMN06 and dl.COLUMNA03=a.COLUMNA03
--where  isnull((a.COLUMNA13),0)=0 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.COLUMN08 <=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--group by g.COLUMN10,g.COLUMN06,m5.COLUMN04,a.columna02,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN07,d.COLUMN13,mu.COLUMN08,dl.COLUMN04,a.COLUMN05,a.COLUMN06,a.COLUMN10
--) union all
-- (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,d.COLUMN03 item,d.COLUMN03 upc,d.COLUMN22 uomid,(case when d.COLUMN22=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,null tdate,null Dtt, null vendor,
--sum(isnull(d.COLUMN08,0))-sum(isnull(d.COLUMN16,0)) openbal,null qr,null qi ,sum(isnull(d.COLUMN10,0)) price,CAST((sum(isnull(d.COLUMN08,0))-sum(isnull(d.COLUMN16,0)))*sum(isnull(d.COLUMN10,0)) AS DECIMAL(18,2)) amt,null transType,d.COLUMN03 ItemID,a.COLUMN10 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,'' Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN16 LocID,d.COLUMN23 LotID,d.COLUMN03 descr
--from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01  and d.COLUMNa02=a.COLUMNa02 and d.COLUMNa03=a.COLUMNa03 and   isnull((d.COLUMNA13),0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
--left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and m.COLUMNA03=d.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN16 and l.COLUMNA02=a.COLUMN10 and l.COLUMNA03=a.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=d.COLUMN23 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and isnull(bm.COLUMNA13,0)=0 and bm.COLUMNA03=d.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
--where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
-- AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate and a.COLUMN05 <=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))  
--group by g.COLUMN10,d.COLUMN23,bm.COLUMN04,g.COLUMN06,m5.COLUMN04,a.column10,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN22,m.COLUMN04,l.COLUMN04,a.COLUMN16
--)
--union all
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN24) ou,c.COLUMN03 item,c.COLUMN03 upc,c.COLUMN26 uomid,(case when c.COLUMN26=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,null tdate,null Dtt, null vendor ,
---(sum(isnull(c.COLUMN07,0))) openbal,null qr,null qi,sum(isnull(c.COLUMN09,0)) price,-((sum(isnull(c.COLUMN07,0)))*(sum(isnull(c.COLUMN09,0)))) amt,null transType,c.COLUMN03 ItemID,b.COLUMN24 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,'' Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null  fid,c.COLUMN03 BrandID,c.COLUMN03 as Brand,b.COLUMN47 LocID,c.COLUMN17 LotID,c.COLUMN03 descr
--from PUTABLE001 b inner join PUTABLE002 c on c.COLUMN19=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner  left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
--left join MATABLE002 m on m.COLUMN02=c.COLUMN26 and m.COLUMNA03=c.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=b.COLUMN47 and l.COLUMNA02=b.COLUMN24 and l.COLUMNA03=b.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=c.COLUMN17 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
--where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
--b.COLUMN06 < @FromDate and b.COLUMN06 <=@FiscalYearStartDt and b.COLUMN03=1355 and b.COLUMN33='' and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
--group by f.COLUMN10,c.COLUMN17,bm.COLUMN04,f.COLUMN06,m5.COLUMN04,b.COLUMN24,f.COLUMN04,c.COLUMN03,b.COLUMN03,c.COLUMN26,m.COLUMN04,l.COLUMN04,b.COLUMN47
--)
--UNION all
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,d.COLUMN03 item,d.COLUMN03 upc,d.COLUMN27 uomid,(case when d.COLUMN27=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,null tdate,null Dtt, null vendor,
--sum(isnull(d.COLUMN07,0)) openbal, null qr,null qi,sum(isnull(d.COLUMN09,0)) price, ((sum(isnull(d.COLUMN07,0)))*(sum(isnull(d.COLUMN09,0))))  amt,null transType,d.COLUMN03 ItemID,a.COLUMN24 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,'' Project
----EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--,null fid,d.COLUMN03 BrandID,d.COLUMN03 as Brand,a.COLUMN48 LocID,d.COLUMN17 LotID,d.COLUMN03 descr
--from SATABLE005 a inner join SATABLE006 d on d.COLUMN19=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
----EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
--inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 
--left join MATABLE002 m on m.COLUMN02=d.COLUMN27 and m.COLUMNA03=d.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN48 and l.COLUMNA02=a.COLUMN24 and l.COLUMNA03=a.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=d.COLUMN17 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
--left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
--where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN06<@FromDate and a.COLUMN06 <=@FiscalYearStartDt and a.COLUMN03=1330 and a.COLUMN33='' and   ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
--group by g.COLUMN10,d.COLUMN17,bm.COLUMN04,g.COLUMN06,m5.COLUMN04,a.COLUMN24,g.COLUMN04,d.COLUMN03,a.COLUMN03,d.COLUMN27,m.COLUMN04,l.COLUMN04,a.COLUMN48
--)
----Fiscal year opening select * from fitable047
--UNION all
--(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,a.COLUMN03 item,a.COLUMN03 upc,a.COLUMN19 uomid,(case when a.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,bm.COLUMN04 Lot,
--null tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt, null vendor,
--(isnull(a.COLUMN04,0)) openbal, null qr,null qi,(isnull(a.COLUMN17,0)) price, (((isnull(a.COLUMN04,0)))*((isnull(a.COLUMN17,0))))  amt,a.COLUMN14 transType,a.COLUMN03 ItemID,a.COLUMN13 OPID,0 TransTypeID,'' Project
--,null fid,a.COLUMN03 BrandID,a.COLUMN03 as Brand,a.COLUMN21 LocID,a.COLUMN22 LotID,a.COLUMN03 descr
--from fitable047 a 
--left join MATABLE002 m on m.COLUMN02=a.COLUMN19 and m.COLUMNA03=a.COLUMNA03 left join CONTABLE030 l on l.COLUMN02=a.COLUMN21 and l.COLUMNA02=a.COLUMN13 and l.COLUMNA03=a.COLUMNA03 
--left join FITABLE043 bm on bm.COLUMN02=a.COLUMN22 and (isnull(bm.COLUMN10,0)=a.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=a.COLUMNA03
--where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
--AND  a.COLUMNA03=@AcOwner and a.COLUMN08 <=@FiscalYearStartDt and a.COLUMN14='Opening Stock' and   ( (@Type!=null and a.COLUMN14 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))
--)
--) p where ((TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable(',',isnull(@Type,0)) s)) or TransTypeID=0) group by ou,item,upc,uomid,uom,Location,LocID,Lot,tno,tdate,Dtt,vendor,qr,qi,transType,ItemID,OPID,fid,BrandID,Brand, LocID,LotID 
--)  ) Q  	 inner join MATABLE007 f on f.COLUMN02=q.item and isnull(f.COLUMNA13,0)=0 and f.COLUMN48=1 and f.COLUMNA03=@AcOwner 
--			 left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner 
--if(@Type=null)
--begin
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--set @Query1='select ou,item, upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,openbal,qr,qi,null price,null amt,transType,ItemID,OPID,0 TransTypeID,null Project,fid,BrandID,Brand, LocID,LotID,descr,F10.COLUMN17 AvgPrice,isnull(openbal,0)+isnull(qr,0)-isnull(qi,0) closing,(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0))*isnull(F10.COLUMN17,0) asset from #StockLedgerReport left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID AND F10.COLUMN13=OPID AND F10.COLUMNA02=OPID AND F10.COLUMN19=UOMID AND iif(F10.COLUMN21='',0,isnull(F10.COLUMN21,0))=iif(LocID='',0,isnull(LocID,0)) AND iif(F10.COLUMN22='',0,isnull(F10.COLUMN22,0))=iif(LotID='',0,isnull(LotID,0)) and isnull(F10.COLUMNA13,0)=0  '+@whereStr+' order by Dtt desc' 
--end
--else
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
--end
--else
--begin
--set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
--end
----EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
--set @Query1='select ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,openbal,qr,qi,null price,null amt,transType,ItemID,OPID,0 TransTypeID,null Project,fid,BrandID,Brand, LocID,LotID,descr,F10.COLUMN17 AvgPrice,isnull(openbal,0)+isnull(qr,0)-isnull(qi,0) closing,(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0))*isnull(F10.COLUMN17,0) asset from #StockLedgerReport
--			 left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID AND F10.COLUMN13=OPID AND F10.COLUMNA02=OPID AND F10.COLUMN19=UOMID AND isnull(F10.COLUMN21,0)=isnull(LocID,0) AND isnull(F10.COLUMN22,0)=isnull(LotID,0) and isnull(F10.COLUMNA13,0)=0   '+@whereStr+'   order by case IsNumeric(item) when 1 then Replicate(''0'', 100 - Len(item)) + item else item end,Dtt desc' 
-- --order by Dtt desc
-- end
--exec (@Query1) 
--end

CREATE TABLE #GETCLOSESTOCK(
		OU NVARCHAR(250),ITEM NVARCHAR(250),UPC NVARCHAR(250),UOMID INT,UOM NVARCHAR(250),LOCATION NVARCHAR(250),
		LOT NVARCHAR(250),TNO NVARCHAR(250),TDATE NVARCHAR(250) ,DTT NVARCHAR(250),VENDOR NVARCHAR(250),CustID INT,OPENBAL DECIMAL(18,2),
		QR DECIMAL(18,2),QI DECIMAL(18,2),PRICE DECIMAL(18,2),AMT DECIMAL(18,2),TRANSTYPE NVARCHAR(250),ITEMID INT,OPID INT,
		TRANSTYPEID INT,PROJECT NVARCHAR(250),FID INT,BRANDID INT,BRAND NVARCHAR(250), LOCID INT,LOTID INT,DESCR NVARCHAR(250),
		AVGPRICE DECIMAL(18,2),CLOSING DECIMAL(18,2),ASSET DECIMAL(18,2) ,ProjectID int
		)
		INSERT #GETCLOSESTOCK EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = @FROMDATE,
		@TODATE = @TODATE,		@ACOWNER = @ACOWNER,		@TYPE = @TYPE,		@LOCATION = @LOCATION,
		@OPUNIT = @OPUNIT,		@OPERATINGUNIT = @OPERATINGUNIT,		@OPERATING = @OPERATING,
		@DATEF = @DATEF
		SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset FROM #GETCLOSESTOCK 
		where CLOSING != 0
		group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU
		DROP TABLE #GETCLOSESTOCK

		
--CREATE TABLE #GETCLOSESTOCK(
--		OU NVARCHAR(250),ITEM NVARCHAR(250),UPC NVARCHAR(250),UOMID INT,UOM NVARCHAR(250),LOCATION NVARCHAR(250),
--		LOT NVARCHAR(250),TNO NVARCHAR(250),TDATE NVARCHAR(250) ,DTT NVARCHAR(250),VENDOR NVARCHAR(250),CustID NVARCHAR(250),OPENBAL DECIMAL(18,2),
--		QR DECIMAL(18,2),QI DECIMAL(18,2),PRICE DECIMAL(18,2),AMT DECIMAL(18,2),TRANSTYPE NVARCHAR(250),ITEMID INT,OPID INT,
--		TRANSTYPEID INT,PROJECT NVARCHAR(250),FID INT,BRANDID INT,BRAND NVARCHAR(250), LOCID INT,LOTID INT,DESCR NVARCHAR(250),
--		AVGPRICE DECIMAL(18,2),CLOSING DECIMAL(18,2),ASSET DECIMAL(18,2),PROJECTID NVARCHAR(250) 
--		)
--		INSERT #GETCLOSESTOCK EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = '1/1/2011',
--		@TODATE = '1/1/2019',		@ACOWNER = 56829,		@TYPE = '',		@LOCATION = '0',
--		@OPUNIT = 57326,		@OPERATINGUNIT = 57326,		@OPERATING = 57326,
--		@DATEF = 'dd/MM/yyyy'
--		--SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
--		--where CLOSING != 0
--		--group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID
--		--update f set column04 = closing, column08 = closing, column12 = column17*closing from fitable010 f 
--		select closing,column04 from fitable010 f 
--		inner join(SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
--		where CLOSING != 0
--		group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID)  g on f.columna02  = g.opid  and f.column03 = g.itemid  and column19 = g.uomid  and column23 = g.projectid COLLATE DATABASE_DEFAULT and isnull(columna13,0)=0
--		DROP TABLE #GETCLOSESTOCK
--		--select*from contable007 where columna03 = 56829		
--		--select*from fitable010 where columna03 = 56829 and isnull(columna13,0)=0 order by column03

--CREATE TABLE #GETCLOSESTOCK(
--		OU NVARCHAR(250),ITEM NVARCHAR(250),UPC NVARCHAR(250),UOMID INT,UOM NVARCHAR(250),LOCATION NVARCHAR(250),
--		LOT NVARCHAR(250),TNO NVARCHAR(250),TDATE NVARCHAR(250) ,DTT NVARCHAR(250),VENDOR NVARCHAR(250),CustID NVARCHAR(250),OPENBAL DECIMAL(18,2),
--		QR DECIMAL(18,2),QI DECIMAL(18,2),PRICE DECIMAL(18,2),AMT DECIMAL(18,2),TRANSTYPE NVARCHAR(250),ITEMID INT,OPID INT,
--		TRANSTYPEID INT,PROJECT NVARCHAR(250),FID INT,BRANDID INT,BRAND NVARCHAR(250), LOCID INT,LOTID INT,DESCR NVARCHAR(250),
--		AVGPRICE DECIMAL(18,2),CLOSING DECIMAL(18,2),ASSET DECIMAL(18,2),PROJECTID NVARCHAR(250) 
--		)
--		INSERT #GETCLOSESTOCK EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = '1/1/2010',
--		@TODATE = '1/1/2019',		@ACOWNER = 56837,		@TYPE = '',		
--		@OPUNIT = '57344',		@OPERATINGUNIT = '57344',		@OPERATING = '57344',
--		@DATEF = 'dd/MM/yyyy'
--		--SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
--		--where CLOSING != 0
--		--group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID
--		update f set column04 = closing, column08 = closing, column12 = column17*closing from fitable010 f 
--		--select item,closing,column04 from fitable010 f 
--		INNER join(SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
--		where CLOSING != 0
--		group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID)  g on f.columna02  = g.opid  and f.column03 = g.itemid  and column19 = g.uomid  and ISNULL(column23,'0') = ISNULL(g.projectid,'0') COLLATE DATABASE_DEFAULT and isnull(columna13,0)=0
--		--order by item
--		DROP TABLE #GETCLOSESTOCK
--		--select*from contable007 where columna03 = 56829		
--		--select*from fitable010 where columna03 = 56837 and columna02 = 57344 AND COLUMNB01 IS NULL and isnull(columna13,0)=0 order by column03		
--		--UPDATE fitable010 SET COLUMN04 = 0,COLUMN08 = 0,COLUMN12 = 0 where columna03 = 56837 and columna02 = 57344 AND COLUMNB01 IS NULL and isnull(columna13,0)=0 order by column

----------------------------------------------------RK ---------------------------------------------------------------------

--CREATE TABLE #GETCLOSESTOCK(
--		OU NVARCHAR(250),ITEM NVARCHAR(250),UPC NVARCHAR(250),UOMID INT,UOM NVARCHAR(250),LOCATION NVARCHAR(250),
--		LOT NVARCHAR(250),TNO NVARCHAR(250),TDATE NVARCHAR(250) ,DTT NVARCHAR(250),VENDOR NVARCHAR(250),CustID NVARCHAR(250),OPENBAL DECIMAL(18,2),
--		QR DECIMAL(18,2),QI DECIMAL(18,2),PRICE DECIMAL(18,2),AMT DECIMAL(18,2),TRANSTYPE NVARCHAR(250),ITEMID INT,OPID INT,
--		TRANSTYPEID INT,PROJECT NVARCHAR(250),FID INT,BRANDID INT,BRAND NVARCHAR(250), LOCID INT,LOTID INT,DESCR NVARCHAR(250),
--		AVGPRICE DECIMAL(18,2),CLOSING DECIMAL(18,2),ASSET DECIMAL(18,2),PROJECTID NVARCHAR(250) 
--		)
--		INSERT #GETCLOSESTOCK EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = '1/1/2010',
--		@TODATE = '1/1/2019',		@ACOWNER = 56571,		@TYPE = '',		
--		@OPUNIT = '56700',		@OPERATINGUNIT = '56700',		@OPERATING = '56700',
--		@DATEF = 'dd/MM/yyyy'
--		--SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
--		--where CLOSING != 0
--		--group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID
--		--update f set column04 = closing, column08 = closing, column12 = column17*closing from fitable010 f 
--		select item,closing,column04 from fitable010 f 
--		INNER join(SELECT ItemID,Item,uomid,uom,Location,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset FROM #GETCLOSESTOCK 
--		where CLOSING != 0
--		group by ItemID,Item,uomid,uom,Location,Lot,OPID,OU)  g on f.columna02  = g.opid  and f.column03 = g.itemid  and column19 = g.uomid   and isnull(columna13,0)=0
--		--order by item
--		DROP TABLE #GETCLOSESTOCK
--		--select*from contable007 where columna03 = 56829		
--		--select*from fitable010 where column03 =26438 and columna03 = 56571 and columna02 = 56700 AND COLUMNB01 IS NULL and isnull(columna13,0)=0 order by column03		
--		--UPDATE fitable010 SET COLUMN04 = 3,COLUMN08 = 3,COLUMN12 = 0 where column03 =26438 and columna03 = 56571 and columna02 = 56700 AND COLUMN02 =78883  AND COLUMNB01 IS NULL and isnull(columna13,0)=0 order by column


		--EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = '4/1/2019',
		--@TODATE = '4/1/2019',		@ACOWNER = 56578,		@TYPE = '',		@LOCATION = '',
		--@OPUNIT = 57416,		@OPERATINGUNIT = 57416,		@OPERATING = 57416,
		--@DATEF = 'dd/MM/yyyy'



		--------------------------------------------------------------------------------------------------------------------------------

		--CREATE TABLE #GETCLOSESTOCK(
		--OU NVARCHAR(250),ITEM NVARCHAR(250),UPC NVARCHAR(250),UOMID INT,UOM NVARCHAR(250),LOCATION NVARCHAR(250),
		--LOT NVARCHAR(250),TNO NVARCHAR(250),TDATE NVARCHAR(250) ,DTT NVARCHAR(250),VENDOR NVARCHAR(250),CustID NVARCHAR(250),OPENBAL DECIMAL(18,2),
		--QR DECIMAL(18,2),QI DECIMAL(18,2),PRICE DECIMAL(18,2),AMT DECIMAL(18,2),TRANSTYPE NVARCHAR(250),ITEMID INT,OPID INT,
		--TRANSTYPEID INT,PROJECT NVARCHAR(250),FID INT,BRANDID INT,BRAND NVARCHAR(250), LOCID INT,LOTID INT,DESCR NVARCHAR(250),
		--AVGPRICE DECIMAL(18,2),CLOSING DECIMAL(18,2),ASSET DECIMAL(18,2),PROJECTID NVARCHAR(250) 
		--)
		--INSERT #GETCLOSESTOCK EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = '1/1/2010',
		--@TODATE = '7/30/2019',		@ACOWNER = 56836,		@TYPE = '',		
		--@OPUNIT = '57343',		@OPERATINGUNIT = '57343',		@OPERATING = '57343',
		--@DATEF = 'dd/MM/yyyy'
		--SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
		--where CLOSING != 0
		--group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID
	    --update f set column04 = closing, column08 = closing, column12 = column17*closing from fitable010 f 
		--select item,closing,column04,uomid from fitable010 f 
		--INNER join(SELECT ItemID,Item,uomid,uom,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset FROM #GETCLOSESTOCK 
		--where CLOSING != 0
		--group by ItemID,Item,uomid,uom,OPID,OU)  g on f.columna02  = g.opid  and f.column03 = g.itemid  and column19 = g.uomid and isnull(columna13,0)=0
		----order by item
		--DROP TABLE #GETCLOSESTOCK
		--select*from contable007 where columna03 = 56829		
	--select * from fitable010 where columna03 = 56836 and columna02 = 57343  and isnull(columna13,0)!=0  and column04 < 0 --order by column03		
	--UPDATE fitable010 SET COLUMN04 = 0,COLUMN08 = 0,COLUMN12 = 0 where columna03 = 56836 and columna02 = 57343  and isnull(columna13,0)=0 


--select*from MATABLE007 where columna03 = 56836



------------------------------------------------------------------------------------------------------------------------------------------------



--CREATE TABLE #GETCLOSESTOCK(
--		OU NVARCHAR(250),ITEM NVARCHAR(250),UPC NVARCHAR(250),UOMID INT,UOM NVARCHAR(250),LOCATION NVARCHAR(250),
--		LOT NVARCHAR(250),TNO NVARCHAR(250),TDATE NVARCHAR(250) ,DTT NVARCHAR(250),VENDOR NVARCHAR(250),CustID NVARCHAR(250),OPENBAL DECIMAL(18,2),
--		QR DECIMAL(18,2),QI DECIMAL(18,2),PRICE DECIMAL(18,2),AMT DECIMAL(18,2),TRANSTYPE NVARCHAR(250),ITEMID INT,OPID INT,
--		TRANSTYPEID INT,PROJECT NVARCHAR(250),FID INT,BRANDID INT,BRAND NVARCHAR(250), LOCID INT,LOTID INT,DESCR NVARCHAR(250),
--		AVGPRICE DECIMAL(18,2),CLOSING DECIMAL(18,2),ASSET DECIMAL(18,2),PROJECTID NVARCHAR(250) 
--		)
--		INSERT #GETCLOSESTOCK EXEC USP_REPORTS_STOCKLEDGER  @FROMDATE = '1/1/2010',
--		@TODATE = '7/30/2019',		@ACOWNER = 56836,		@TYPE = '',		
--		@OPUNIT = '57343',		@OPERATINGUNIT = '57343',		@OPERATING = '57343',
--		@DATEF = 'dd/MM/yyyy'
--		--SELECT ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset,PROJECTID FROM #GETCLOSESTOCK 
--		--where CLOSING != 0
--		--group by ItemID,Item,uomid,uom,LocID,Location,LotID,Lot,OPID,OU,PROJECTID
--	    update f set column04 = closing, column08 = closing, column12 = column17*closing from fitable010 f 
--		--select item,closing,column04,uomid from fitable010 f 
--		INNER join(SELECT ItemID,Item,uomid,uom,OPID,OU,sum(closing)closing,avg(AvgPrice)AvgPrice,sum(asset)asset FROM #GETCLOSESTOCK 
--		where CLOSING != 0
--		group by ItemID,Item,uomid,uom,OPID,OU)  g on f.columna02  = g.opid  and f.column03 = g.itemid  and column19 = g.uomid and isnull(columna13,0)=0 AND F.COLUMNA03 = 56836
--		--order by item
--		DROP TABLE #GETCLOSESTOCK
--		--select*from contable007 where columna03 = 56829		
--	--select * from fitable010 where columna03 = 56836 and isnull(column04,0)!=0  and column04 > 0 --order by column03			
--	--select DISTINCT(COLUMN03) from fitable010 where columna02 IN(-57343,57343,57342) and isnull(column04,0)!=0  and columnA03 = 56837 --order by column03		
--	--select * from fitable010 where columna02 IN(-57343,57343,57342) and isnull(column04,0)!=0  and columnA03 = 56837 --order by column03		
--	--UPDATE fitable010 SET COLUMNA02 = 57343,COLUMNA03 = 56836 where columna02 IN(-57343,57342) and isnull(column04,0)!=0  and columnA03 = 56837 --order by column03		
--	--UPDATE fitable010 SET COLUMN04 = 0,COLUMN08 = 0,COLUMN12 = 0 where columna03 = 56836 and columna02 = 57343  and isnull(columna13,0)=0 


----select*from MATABLE007 where columna03 = 56836
----select*from MATABLE007 where column02 IN(145443,145445,145523,145525)
GO
