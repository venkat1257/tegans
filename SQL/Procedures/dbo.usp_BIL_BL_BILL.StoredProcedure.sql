USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_BIL_BL_BILL]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_BIL_BL_BILL]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
    @COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20  nvarchar(250)=null,   @COLUMN21  nvarchar(250)=null,   @COLUMN22  nvarchar(250)=null, 
	@COLUMN23  nvarchar(250)=null,   @COLUMN24  nvarchar(250)=null,	  @COLUMN25  nvarchar(250)=null,
	@COLUMN26  nvarchar(250)=null,   @COLUMN27  nvarchar(250)=null,   @COLUMN28  nvarchar(250)=null, 
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	@COLUMN29  nvarchar(250)=null,   @COLUMN30  nvarchar(250)=null,   @COLUMN31  nvarchar(250)=null,
	@COLUMN32  nvarchar(250)=null,  @COLUMN33  nvarchar(250)=null,    @COLUMN34  nvarchar(250)=null,
	@COLUMN35  nvarchar(250)=null,  @COLUMN36  nvarchar(250)=null,    @COLUMN37  nvarchar(250)=null,
	@COLUMN38  nvarchar(250)=null,  @COLUMN39  nvarchar(250)=null,    @COLUMN40  nvarchar(250)=null,
	--EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	@COLUMN41  nvarchar(250)=null,   @COLUMN42  nvarchar(250)=null,   @COLUMN43  nvarchar(250)=null,
	@COLUMN44  nvarchar(250)=null,   @COLUMN45  nvarchar(250)=null,   @COLUMN46  nvarchar(250)=null,
	@COLUMN47  nvarchar(250)=null,   @COLUMN48  nvarchar(250)=null,   @COLUMN49  nvarchar(250)=null,   
	@COLUMN50  nvarchar(250)=null,   @COLUMN51  nvarchar(250)=null,   @COLUMN52  nvarchar(250)=null,
	@COLUMN53  nvarchar(250)=null,   @COLUMN54  nvarchar(250)=null,   @COLUMN55  nvarchar(250)=null,   
	@COLUMN56  nvarchar(250)=null,   @COLUMN57  nvarchar(250)=null,   @COLUMN58  nvarchar(250)=null,
	@COLUMN59  nvarchar(250)=null,   @COLUMN60  nvarchar(250)=null,   @COLUMN61  nvarchar(250)=null,   
	@COLUMN62  nvarchar(250)=null,   @COLUMN63  nvarchar(250)=null,   @COLUMN64  nvarchar(250)=null,
	@COLUMN66  nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null, 
	@COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null, 
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null, 
	@COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null, 
	@COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null, 
	@COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  
	@COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null, 
	@COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  
	@COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  
	@COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   
	--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@Direction  nvarchar(250),       @TabelName  nvarchar(250),       @ReturnValue  int=null OUTPUT,  
	@UOMData  XML=null
)
AS
BEGIN
begin try
   declare @tempSTR nvarchar(max)
   set @tempSTR=('the values '+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','
   +isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+','+isnull(@COLUMN14,'')+','+isnull(@COLUMN15,'')+','+isnull(@COLUMN16,'')+','+isnull(@COLUMN17,'')+','+isnull(@COLUMN18,'')+','+isnull(@COLUMN19,'')+','+isnull(@COLUMN20,'')+','+isnull(@COLUMN21,'')+','+isnull(@COLUMN22,'')+','+isnull(@COLUMN23,'')+','+isnull(@COLUMN24,'')+','+isnull(@COLUMN25,'')+' of  '+isnull(@TabelName,'')+' succeeded at  ');

IF  @TabelName='PUTABLE005'
BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN15!= '' and @COLUMN15 is not null and @COLUMN15!= '0') THEN @COLUMN15 else @COLUMNA02  END )
EXEC usp_PUR_TP_PUTABLE005 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,   @COLUMN30, @COLUMN31,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill 
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,   @COLUMN40, @COLUMN41,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN47,  @COLUMN48,  @COLUMN49,  @COLUMN50,  @COLUMN51,
   @COLUMN52,  @COLUMN53,  @COLUMN54,  @COLUMN55,  @COLUMN56,  @COLUMN57,  @COLUMN58,  @COLUMN59,  @COLUMN60,
   @COLUMN61,  @COLUMN62,  @COLUMN63,  @COLUMN64,  @COLUMN66,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, 
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, 
   @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, 
   @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09,
   --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
   @COLUMND10, @Direction, @TabelName, @ReturnValue OUTPUT
select @ReturnValue
END
ElSE IF  @TabelName='PUTABLE006'
BEGIN
EXEC usp_PUR_TP_PUTABLE006 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27, @COLUMN28,  @COLUMN29,  @COLUMN30,   @COLUMN31,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10, 
   --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
   @Direction, @TabelName, @ReturnValue,@UOMData
END 
   exec [CheckDirectory] @tempSTR,'usp_PUR_BL_BILL.txt',0
END TRY
			BEGIN CATCH	 
			SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
			begin
   set @tempSTR='transaction failed at  ';
   exec [CheckDirectory] @tempSTR,'usp_PUR_BL_BILL.txt',0
			return 0
			end
			END CATCH
end






GO
