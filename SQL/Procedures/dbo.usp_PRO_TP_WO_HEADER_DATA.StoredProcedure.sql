USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_WO_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PRO_TP_WO_HEADER_DATA]
(
	@WorkOrderID int
)

AS
BEGIN
	SELECT COLUMN05,COLUMN02 ,
	COLUMN08 , 
	COLUMN09 ,COLUMN10 ,
	COLUMN11,COLUMN12
	,(select COLUMN05 from SATABLE002 where COLUMN02=PRTABLE005.COLUMN05) cName,COLUMN19
	FROM PRTABLE005 WHERE  COLUMN02= @WorkOrderID
END









GO
