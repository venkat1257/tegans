USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_Proc_TP_BANKAUTORECONCILATION]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Proc_TP_BANKAUTORECONCILATION]

(
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@Bank        nvarchar(250)= null,
@Reconcilid        nvarchar(250)= null,
@ImportId        nvarchar(250)= null,
@DateF nvarchar(250)=null,
@Eid nvarchar(250)=null,
@ReturnValue int=null OUTPUT
)

AS

BEGIN
begin try

declare @tblid int
set @Bank=(SELECT COLUMN08 FROM SETABLE019 WHERE COLUMN02=@ImportId)
set @DateF=(iif(@DateF='','dd/MM/yyyy',@DateF))
Create Table #SysTable (Checkrow nvarchar(250),Bank nvarchar(250),[Date] nvarchar(250),Trans nvarchar(250),[Type] nvarchar(250),
Memo nvarchar(250),Name nvarchar(250),PaymentAmount decimal(18,2),DepositAmount decimal(18,2),TransID nvarchar(250),
BalanceAmount decimal(18,2),Partytype nvarchar(250),OperatingUnit nvarchar(250),OperatingUnitId nvarchar(250),BankID nvarchar(250),ab nvarchar(250),BankType nvarchar(250),
ReconcilStatus nvarchar(250),Reconcilid nvarchar(250)
)

Create Table #BankTable ([Date] nvarchar(250),Memo nvarchar(250),Ref nvarchar(250),Debit decimal(18,2),Credit decimal(18,2),Balance decimal(18,2),
Bank nvarchar(250),[Status] nvarchar(250),BankId nvarchar(250),chkrow nvarchar(250),BankReconcilid nvarchar(250),BankType nvarchar(250),OperatingUnit nvarchar(250)
)

INSERT INTO #BankTable
EXEC usp_FIN_REPORT_SYSRECONCILATIONDATA '',@OPUnit,@AcOwner,@ImportId ,'23134',@DateF;


INSERT INTO #SysTable
EXEC usp_FIN_REPORT_BANKRECONCILATION '','','',@OPUnit,@AcOwner,@Bank ,'23134',@DateF;

set @tblid=(select max(isnull(cast(COLUMN02 as int),0)) from SETABLE021)
INSERT INTO SETABLE021 (COLUMN02, COLUMN03, COLUMN04, COLUMN05, COLUMN06, COLUMN07, COLUMN08, COLUMN09, COLUMN10, COLUMN11, COLUMN12, COLUMNA02, COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA12,COLUMNA13)
SELECT (@tblid+(row_number() over(order by b.chkrow))), b.chkrow, a.[BankType], getdate(), a.Trans, a.[Date],a.[Type],a.TransID,a.Memo,iif(a.[BankType]='Receivable',ISNULL(a.DepositAmount,0),ISNULL(a.PaymentAmount,0)),@Bank,a.OperatingUnitId,@AcOwner,GETDATE(),GETDATE(),@Eid,1,0
FROM #SysTable a
INNER JOIN #BankTable b ON a.[Date] = b.[Date] and a.OperatingUnitId = b.OperatingUnit and ((a.PaymentAmount = b.Credit and ISNULL(a.PaymentAmount,0)!=0) or (a.DepositAmount = b.Debit and ISNULL(a.DepositAmount,0)!=0))

update SETABLE020 set COLUMN12=23133 where COLUMN02 in(SELECT b.chkrow FROM #SysTable a
INNER JOIN #BankTable b ON a.[Date] = b.[Date] and a.OperatingUnitId = b.OperatingUnit and ((a.PaymentAmount = b.Credit and ISNULL(a.PaymentAmount,0)!=0) or (a.DepositAmount = b.Debit and ISNULL(a.DepositAmount,0)!=0))) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
if exists(SELECT TOP(1)COLUMN02 FROM SETABLE020 where COLUMN02 in(SELECT b.chkrow FROM #SysTable a
INNER JOIN #BankTable b ON a.[Date] = b.[Date] and a.OperatingUnitId = b.OperatingUnit and ((a.PaymentAmount = b.Credit and ISNULL(a.PaymentAmount,0)!=0) or (a.DepositAmount = b.Debit and ISNULL(a.DepositAmount,0)!=0))) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
set @ReturnValue =(1)
else
set @ReturnValue =(0)
end try
begin catch
declare @tempSTR nvarchar(max)=null
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage	
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_BankAutoReconciliation.txt',0
return 0
end catch
end
GO
