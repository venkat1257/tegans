USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CustomerPaymentsGLImpact]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CustomerPaymentsGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,S11.column10 Memo,sum(p19.COLUMN11) Credit,0 Debit  from SATABLE011 S11
left outer join putable019 p19 on p19.COLUMN05= S11.COLUMN04 and p19.COLUMN07= S11.COLUMN05 and p19.COLUMNA03= S11.COLUMNA03 and p19.COLUMNA02= S11.COLUMNA02 and p19.COLUMN06='PAYMENT' and isnull(p19.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p19.COLUMN03
where S11.COLUMN02=@ide and S11.COLUMNA03=@AcOwner and f1.COLUMNA03 = S11.COLUMNA03 and isnull(S11.COLUMNA13,0)=0
group by f1.COLUMN04,S11.column10
union all
select f1.COLUMN04 Account,S11.column10 Memo,sum(F52.COLUMN12) Credit,0 Debit  from SATABLE011 S11
left outer join FITABLE052 F52 on F52.COLUMN05= S11.COLUMN04  and F52.COLUMNA03= S11.COLUMNA03 and F52.COLUMNA02= S11.COLUMNA02 and F52.COLUMN06='PAYMENT' and isnull(F52.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F52.COLUMN03
where S11.COLUMN02=@ide and S11.COLUMNA03=@AcOwner and f1.COLUMNA03 = S11.COLUMNA03 and isnull(S11.COLUMNA13,0)=0
group by f1.COLUMN04,S11.column10
union all
select f1.COLUMN04 Account,S11.column10 Memo,0 Credit,sum(P18.COLUMN11) Debit  from SATABLE011 S11
left outer join PUTABLE018 P18 on P18.COLUMN05= S11.COLUMN04  and P18.COLUMNA03= S11.COLUMNA03 and P18.COLUMNA02= S11.COLUMNA02 and P18.COLUMN06='PAYMENT' and isnull(P18.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = P18.COLUMN03
where S11.COLUMN02=@ide and S11.COLUMNA03=@AcOwner and f1.COLUMNA03 = S11.COLUMNA03 and isnull(S11.COLUMNA13,0)=0
group by f1.COLUMN04,S11.column10
union all
select f1.COLUMN04 Account,S11.column10 Memo,sum(f36.COLUMN07) Credit,0 Debit  from SATABLE011 S11
left outer join FITABLE036 f36 on f36.COLUMN09= S11.COLUMN04  AND f36.COLUMN06= S11.COLUMN05 and f36.COLUMNA03= S11.COLUMNA03 and f36.COLUMNA02= S11.COLUMNA02 and f36.COLUMN03='PAYMENT' and isnull(f36.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f36.COLUMN10
where S11.COLUMN02=@ide and S11.COLUMNA03=@AcOwner and f1.COLUMNA03 = S11.COLUMNA03 and isnull(S11.COLUMNA13,0)=0
group by f1.COLUMN04,S11.column10
union all
select f1.COLUMN04 Account,S11.column10 Memo,sum(f26.COLUMN12) Credit,0 Debit  from SATABLE011 S11
left outer join FITABLE026 f26 on f26.COLUMN09= S11.COLUMN04  AND f26.COLUMN06= S11.COLUMN05 and f26.COLUMNA03= S11.COLUMNA03 and f26.COLUMNA02= S11.COLUMNA02 and f26.COLUMN04='PAYMENT' and isnull(f26.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f26.COLUMN08
where S11.COLUMN02=@ide and S11.COLUMNA03=@AcOwner and f1.COLUMNA03 = S11.COLUMNA03 and isnull(S11.COLUMNA13,0)=0
group by f1.COLUMN04,S11.column10
union all
select f1.COLUMN04 Account,S11.column10 Memo,sum(f27.COLUMN10) Credit,0 Debit  from SATABLE011 S11
left outer join FITABLE027 f27 on f27.COLUMN09= S11.COLUMN04 and f27.COLUMNA03= S11.COLUMNA03 and f27.COLUMNA02= S11.COLUMNA02 and f27.COLUMN04='COMMISSION PAYMENT' and isnull(f27.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f27.COLUMN06
where S11.COLUMN02=@ide and S11.COLUMNA03=@AcOwner and f1.COLUMNA03 = S11.COLUMNA03 and isnull(S11.COLUMNA13,0)=0
group by f1.COLUMN04,S11.column10
)
select [Account],[Memo],[Credit],[Debit] from MyTable
group by [Account],[Memo],[Credit],[Debit]
end


GO
