USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_ResourseConsumption]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   procedure [dbo].[USP_REPORTS_ResourseConsumption]
(

@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@IFamily nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
--EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
)
as 
begin
if @Item!=''
begin
 set @whereStr= ' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CustomerID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CustomerID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
--EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
end
end

if @IFamily!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where IFamily in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@IFamily+''') s)'
end
else
begin
set @whereStr=@whereStr+' and IFamily in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@IFamily+''') s)'

end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
end
--if @Type!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
--end
--else
--begin
--set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
--end
--end
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc

if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s) '
end
else
begin
set @whereStr=@whereStr+' and upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end



select Q.ou,Q.item,Q.upc,Q.uomid,Q.uom,Q.Location,Q.Lot,Q.tno,Q.tdate,Q.Dtt,Q.vendor,
convert(float,replace(rtrim(replace(replace(rtrim(replace(Q.qr,'0',' ')),' ','0'),'.',' ')),' ','.'))qr,
convert(float,replace(rtrim(replace(replace(rtrim(replace(Q.qi,'0',' ')),' ','0'),'.',' ')),' ','.'))qi,
Q.price,Q.amt,Q.amt1,Q.transType,
Q.ItemID,Q.OPID,Q.TransTypeID,Q.Project,Q.fid,Q.BrandID,Q.Brand,Q.LocID,Q.LotID,Q.ItemFamily,Q.projNa,Q.IFamily,Q.CustomerID,Q.cust
 into #ReceivableReport from 
(

select ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,qr,qi,price,sum(amt)amt,sum(amt1)amt1,transType,ItemID,OPID,TransTypeID,Project,fid,BrandID,Brand,LocID,LotID,ItemFamily,projNa,IFamily,CustomerID,cust
 from(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN11) ou,null item,NULL upc ,NULL uomid,NULL uom,NULL Location,NULL Lot,'Opening Balance' tno,NULL tdate,NULL Dtt, NULL vendor, NULL qr, NULL qi,NULL price,
 ((c.COLUMN06)*(c.COLUMN08)) amt, 0 amt1 ,NULL transType,null ItemID,b.COLUMN11 OPID, NULL TransTypeID,b.COLUMN08 Project,NULL fid,NULL BrandID,NULL as Brand,NULL LocID,NULL LotID,isnull(m3.COLUMN04,'') ItemFamily ,isnull( pr1.COLUMN05,'') projNa  ,f.COLUMN12 IFamily,NULL CustomerID ,null cust
 from PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
 inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner
 left outer join MATABLE003 m3 on m3.COLUMN02= f.COLUMN12 and  m3.COLUMNA03 =f.COLUMNA03
 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
 left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
 left OUTER JOIN SATABLE002 S2 ON  S2.COLUMN02=b.COLUMN06 
 left outer join PRTABLE001 pr1 on pr1.COLUMN02=b.COLUMN08 where  isnull((b.COLUMNA13),0)=0 and  
 ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.COLUMN09 >=@FiscalYearStartDt)D group by ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,vendor,qr,qi,price,transType,ItemID,OPID,TransTypeID,Project,fid,BrandID,Brand,LocID,LotID,ItemFamily,projNa,IFamily,CustomerID,cust
 union all
select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN11) ou,f.COLUMN04 item,f.COLUMN06 upc ,c.COLUMN11 uomid,(case when (c.COLUMN11)=10000 then (select column05 from fitable037 where column02=10000) else (select COLUMN04 from MATABLE002 where column02=(c.COLUMN11)) end) uom,'' Location,'' Lot,b.COLUMN04 tno,FORMAT(b.COLUMN09,@DateF) tdate,b.COLUMN09 Dtt, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN06) vendor, C.COLUMN05 qr, C.COLUMN06 qi,c.COLUMN08 price,
((c.COLUMN06)*(c.COLUMN08)) amt,((c.COLUMN06)*(c.COLUMN08)) amt1,c10.COLUMN04 transType,c.COLUMN03 ItemID,b.COLUMN11 OPID, (case when ''='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN08 Project,b.COLUMN03  fid,(case when f.COLUMN10=0 then null else f.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,'' LocID,'' LotID,isnull(m3.COLUMN04,'') ItemFamily ,isnull( pr1.COLUMN05,'') projNa  ,f.COLUMN12 IFamily
,b.COLUMN06 CustomerID ,S2.COLUMN05 cust from PRTABLE007 b 
inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner
left outer join MATABLE003 m3 on m3.COLUMN02= f.COLUMN12 and  m3.COLUMNA03 =f.COLUMNA03
left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
left OUTER JOIN SATABLE002 S2 ON  S2.COLUMN02=b.COLUMN06 
left outer join PRTABLE001 pr1 on pr1.COLUMN02=b.COLUMN08 where  isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN09 between  @FromDate and @ToDate and b.COLUMN09 >=@FiscalYearStartDt
) Q

set @Query1='select * from #ReceivableReport '+@whereStr+' order by Dtt desc'
exec (@Query1) 


end

GO
