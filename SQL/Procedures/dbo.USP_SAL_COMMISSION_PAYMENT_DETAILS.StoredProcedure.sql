USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_SAL_COMMISSION_PAYMENT_DETAILS]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_SAL_COMMISSION_PAYMENT_DETAILS]
(
@CUSTOMER nvarchar(250)=NULL,
@SALESREP nvarchar(250)=NULL,
@OPERATING nvarchar(250)=NULL,
@FRDATE date=NULL,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnitStatus      int= null,
@TODATE date=NULL
)
--EMPHCS976 COMMISSION REPORT IS NOT GETTING DISPLAYED AND ALSO NOT SHOWING IN VENDOR PAYMENT BY SRINI 22/8/2015
AS	
	BEGIN
		IF(@CUSTOMER!='' AND @SALESREP!='' AND @OPERATING!='' AND @FRDATE!='' AND @TODATE!='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 =@OPERATING 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND SATABLE002.COLUMN02=@CUSTOMER AND MATABLE010.COLUMN02=@SALESREP AND	
				CONTABLE007.COLUMN02=@OPERATING AND	FITABLE027.COLUMN03 BETWEEN @FRDATE AND @TODATE order by FITABLE027.COLUMN03 	desc
END
		ELSE IF(@CUSTOMER!='' AND @SALESREP!='' AND @OPERATING!='' AND @FRDATE='' AND @TODATE='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 =@OPERATING 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND SATABLE002.COLUMN02=@CUSTOMER AND MATABLE010.COLUMN02=@SALESREP AND CONTABLE007.COLUMN02=@OPERATING AND  FITABLE027.COLUMNA03=@AcOwner
				order by FITABLE027.COLUMN03	desc
END
		ELSE IF(@CUSTOMER!='' AND @OPERATING!='' AND @SALESREP='' AND @FRDATE='' AND @TODATE='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 =@OPERATING 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND SATABLE002.COLUMN02=@CUSTOMER AND CONTABLE007.COLUMN02=@OPERATING AND  FITABLE027.COLUMNA03=@AcOwner
				order by FITABLE027.COLUMN03	desc
END
		ELSE IF(@CUSTOMER!='' AND @SALESREP='' AND @OPERATING='' AND @FRDATE='' AND @TODATE='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND SATABLE002.COLUMN02=@CUSTOMER AND	 FITABLE027.COLUMNA03=@AcOwner order by FITABLE027.COLUMN03	desc
END
		ELSE IF(@SALESREP!='' AND @CUSTOMER='' AND @OPERATING='' AND @FRDATE='' AND @TODATE='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND MATABLE010.COLUMN02=@SALESREP AND	FITABLE027.COLUMNA03=@AcOwner order by FITABLE027.COLUMN03 	desc
END
		ELSE IF(@OPERATING!='' AND @CUSTOMER='' AND @SALESREP=''  AND @FRDATE='' AND @TODATE='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 =@OPERATING 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0 AND CONTABLE007.COLUMN02=@OPERATING AND FITABLE027.COLUMNA03=@AcOwner order by FITABLE027.COLUMN03	desc
END
		ELSE IF(@CUSTOMER!='' AND @FRDATE!='' AND @TODATE!='' AND @SALESREP='' AND @OPERATING='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND SATABLE002.COLUMN02=@CUSTOMER AND	FITABLE027.COLUMN03 BETWEEN @FRDATE AND @TODATE AND				  
				FITABLE027.COLUMNA03=@AcOwner order by FITABLE027.COLUMN03	desc
END
		ELSE IF(@SALESREP!='' AND @FRDATE!='' AND @TODATE!='' AND @CUSTOMER='' AND @OPERATING='' )
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND MATABLE010.COLUMN02=@SALESREP AND FITABLE027.COLUMN03 BETWEEN @FRDATE AND @TODATE AND				  
				FITABLE027.COLUMNA03=@AcOwner order by FITABLE027.COLUMN03 	desc
END
		ELSE IF( @OPERATING!='' AND @FRDATE!='' AND @TODATE!='' AND @CUSTOMER='' AND @SALESREP='' )
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 [Date], 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02 =@OPERATING 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0  AND CONTABLE007.COLUMN02=@OPERATING AND FITABLE027.COLUMN03 BETWEEN @FRDATE AND @TODATE  AND  FITABLE027.COLUMNA03=@AcOwner
				order by FITABLE027.COLUMN03	desc
END
		ELSE IF( @FRDATE!='' AND @TODATE!='' AND @CUSTOMER='' AND @SALESREP='' AND @OPERATING='')
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03 , 
				 FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
				 MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02=MATABLE010.columna02 
				 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06 where					
				FITABLE027.COLUMN11>0 and fitable027.columna02 in  (SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND FITABLE027.COLUMN03 BETWEEN @FRDATE AND @TODATE AND FITABLE027.COLUMNA03=@AcOwner order by FITABLE027.COLUMN03	desc
END
		ELSE
			BEGIN
				SELECT CONTABLE007.COLUMN03, SATABLE002.COLUMN05, MATABLE010.COLUMN06,	FITABLE027.COLUMN07,FITABLE027.COLUMN03, FITABLE027.COLUMN11, FITABLE027.COLUMN12 COLUMN10, FITABLE027.COLUMN15 FROM SATABLE002 INNER JOIN
                MATABLE010 INNER JOIN CONTABLE007 ON CONTABLE007.COLUMN02=MATABLE010.COLUMNA02 INNER JOIN FITABLE027 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14 ON SATABLE002.COLUMN02 = FITABLE027.COLUMN06  where					
				FITABLE027.COLUMN11>0 AND   FITABLE027.COLUMNA03=@AcOwner  and fitable027.columna02 in  (SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) order by FITABLE027.COLUMN03 	desc
END
	END
	--exec USP_SAL_COMMISSION_PAYMENT_DETAILS null,null,'56565','1-27-2015','1-27-2015'
	











GO
