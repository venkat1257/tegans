USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[SalesRegister]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SalesRegister]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@Brand      nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@DateF nvarchar(250)=null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null
)
as 
begin
IF  @ReportName='SalesRegister'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @SalesRep!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where SalesRepc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SalesRep+''') s)'
end
else
begin
set @whereStr=@whereStr+' and (SalesRepc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SalesRep+''') s)) '
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)) '
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)) '
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #SalesRegister from (


SELECT  FORMAT(a.COLUMN08, @DateF)  dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,MATABLE010.COLUMN06 SalesRep,m5.COLUMN04 Brand,a.COLUMN04 AS InvoiceNo,a.COLUMN12 Memo,sum(isnull(b.COLUMN10,0)) qty,
(isnull(a.COLUMN20,0)) amont,a.COLUMN05 CID,SATABLE002.COLUMN22 SalesRepc,SATABLE002.COLUMN37 BrandID
FROM SATABLE009 a
inner join SATABLE010 b on  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  AND b.COLUMN15=a.COLUMN01 and b.COLUMNA03=a.COLUMNA03 and b.COLUMNA02=a.COLUMNA02 and isnull(b.columna13,0)=0 
inner join SATABLE002 on SATABLE002.COLUMN02=a.COLUMN05 and  SATABLE002.COLUMNA03=@AcOwner and isnull(SATABLE002.COLUMNA13,0)=0
left join MATABLE005 m5 on m5.COLUMN02=SATABLE002.COLUMN37 and SATABLE002.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
left join MATABLE010 on MATABLE010.COLUMN02=SATABLE002.COLUMN22 and  MATABLE010.COLUMNA03=@AcOwner and MATABLE010.COLUMN30=1 AND ISNULL(MATABLE010.COLUMNA13,0)=0
where isnull(a.COLUMNA13,0)=0 and  a.COLUMN19=1002 and a.COLUMN08 between @FromDate and @ToDate  AND a.COLUMNA03=@AcOwner AND (a.COLUMNA02=@OperatingUnit or a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) )
group by a.COLUMN08,a.COLUMN05,a.COLUMN04,a.COLUMN20,a.COLUMN12,MATABLE010.COLUMN06,m5.COLUMN04,SATABLE002.COLUMN22,SATABLE002.COLUMN37
	) Query 

set @Query1='select dt,customer,InvoiceNo,(amont)amont,CID,(qty)qty,Memo,SalesRep,Brand,SalesRepc,BrandID   from #SalesRegister'+@whereStr +' 
--group by dt,customer,InvoiceNo,CID,Memo,SalesRep,Brand,SalesRepc,BrandID
'
exec (@Query1)



END

end
GO
