USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SOQ_TP_SOQ_Line_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SOQ_TP_SOQ_Line_DATA]
(
	@SalesOrderID int
)

AS
BEGIN
	SELECT  
	--EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
		b.COLUMN03, b.COLUMN04,b.COLUMN05,b.COLUMN06 as COLUMN07,b.COLUMN07 as COLUMN09,b.COLUMN08 as COLUMN25,b.COLUMN09 as COLUMN10,b.COLUMN10 as COLUMN11,b.COLUMN12 as COLUMN27
	,b.COLUMN07 COLUMN32,b.COLUMN13 COLUMN26,hs.COLUMN04 hsncode
	FROM SATABLE015 a inner join SATABLE016 b on a.COLUMN01=b.COLUMN11 
	inner join MATABLE007 m7 on b.COLUMN03=m7.COLUMN02 and b.COLUMNA03=m7.COLUMNA03
	left join MATABLE032 hs on hs.COLUMN02=m7.COLUMN75 and isnull(hs.COLUMNA13,0)=0 
    --left join MATABLE032 sc on sc.COLUMN02=m7.COLUMN75 and m7.COLUMN05='ITTY009' and isnull(sc.COLUMNA13,0)=0 
	 WHERE  a.COLUMN02= @SalesOrderID
END













GO
