USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_TP_ProdIncomeUpdate]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_TP_ProdIncomeUpdate](@HeaderId nvarchar(250)=null,@HeaderNo nvarchar(250)=null,@LineId nvarchar(250)=null,
@ItemId nvarchar(250)=null,@Uom nvarchar(250)=null,@Location nvarchar(250)=null,@Lot nvarchar(250)=null,@BillAvgPrice decimal(18,2),
@OPUnit nvarchar(250)=null,@Acowner nvarchar(250)=null)
as 
begin
declare @type nvarchar(250)=null, @id nvarchar(250)=null, @lineitem nvarchar(250)=null,@lineQty decimal(18,2)=null, @tranlineid nvarchar(250)=null,
@MaxRownum INT,@Initialrow INT=1,@id1 nvarchar(250)=null,@MaxRownum1 INT,@Initialrow1 INT=1,@Item nvarchar(250)=null
 DECLARE cur2 CURSOR FOR SELECT COLUMN02 from fiTABLE025 where columna02=@OPUnit and  columna03=@Acowner and columnd05=@ItemId and  column08='1056'  and isnull(columna13,0)=0
		  OPEN cur2
		  FETCH NEXT FROM cur2 INTO @id
		  SET @MaxRownum = (SELECT COUNT(*) from fiTABLE025 where columna02=@OPUnit and  columna03=@Acowner and columnd05=@ItemId and  column08='1056'  and isnull(columna13,0)=0)
		     WHILE @Initialrow <= @MaxRownum
		     BEGIN 
				set @Lot=(case when @Lot='' then 0 when isnull(@Lot,0)=0 then 0  else @Lot end)
				set @type=(select column04 from fiTABLE025 where column02=@id)
				set @tranlineid=(select column05 from fiTABLE025 where column02=@id)
				if(@type='INVOICE')
				begin
					 set @Item=(SELECT COLUMN05 from SATABLE010 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@Item=@ItemId)
					 begin
					 set @lineQty=(SELECT isnull(COLUMN10,0) from SATABLE010 where column01 in(@tranlineid) and column05=@ItemId and column22=@Uom and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update FITABLE025 set COLUMN09=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id
					 end
					 end
			 	end
				else if(@type='Credit Memo')
				begin
					 set @Item=(SELECT COLUMN03 from SATABLE006 where column01 in(@tranlineid) and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@Item=@ItemId)
					 begin
					 set @lineQty=(SELECT isnull(COLUMN07,0) from SATABLE006 where column01 in(@tranlineid) and column03=@ItemId and column27=@Uom and columna02=@OPUnit and  columna03=@Acowner and isnull(columna13,0)=0)
					 if(@lineQty>0)
					 begin
					 update FITABLE025 set COLUMN11=cast((@lineQty*@BillAvgPrice) as decimal(18,2)) where COLUMN02=@id
					 end
					 end
			 	end
				 FETCH NEXT FROM cur2 INTO @id
		         SET @Initialrow = @Initialrow + 1 
			END
		CLOSE cur2 
		deallocate cur2 
end


GO
