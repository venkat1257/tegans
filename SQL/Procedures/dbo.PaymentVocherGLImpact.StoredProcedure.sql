USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PaymentVocherGLImpact]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentVocherGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,F34.COLUMN05 Memo,0 Credit,sum(F34.COLUMN07) Debit,1 id  from FITABLE020 F20
left outer join FITABLE034 F34 on F34.COLUMN09= F20.COLUMN04 and F34.COLUMNA03= F20.COLUMNA03 and F34.COLUMNA02= F20.COLUMNA02 and F34.COLUMN03='PAYMENT VOUCHER' and isnull(F34.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F34.COLUMN10 and f1.COLUMNA03 = F34.COLUMNA03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F34.COLUMN05
union all
--select f1.COLUMN04 Account,'' Memo,sum(F36.COLUMN07) Credit,0 Debit,2 id  from FITABLE020 F20
--left outer join FITABLE036 F36 on F36.COLUMN09= F20.COLUMN04 and F36.COLUMNA03= F20.COLUMNA03 and F36.COLUMNA02= F20.COLUMNA02 and F36.COLUMN03='PAYMENT VOUCHER' and isnull(F36.COLUMNA13,0)=0
--left outer join FITABLE001 f1 on f1.COLUMN02 = F36.COLUMN10 
--where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
--group by f1.COLUMN04
SELECT f1.COLUMN04 Account,f22.COLUMN04 Memo,0 Credit,SUM(F36.COLUMN07) Debit,2 id  from FITABLE036 F36
left outer join FITABLE020 f20 on CAST(f20.COLUMN01 AS NVARCHAR(250))=CAST(F36.COLUMN05 AS NVARCHAR(250)) and f20.COLUMNA03=F36.COLUMNA03 and f20.COLUMNA02=F36.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
left outer join FITABLE022 f22 on f20.COLUMN01=f22.COLUMN09 and f22.COLUMNA02=f20.COLUMNA02 and f20.COLUMNA03=f22.COLUMNA03 and isnull(f22.COLUMNA13,0)=0  AND F36.COLUMN10= f22.column03 
AND F36.COLUMN07= f22.column05 
left outer join FITABLE001 f1 on f1.COLUMN02 = F36.COLUMN10 AND f1.COLUMNA03 = F36.COLUMNA03 
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 and F36.COLUMN03='PAYMENT VOUCHER'
group by f1.COLUMN04,f22.COLUMN04
union all
select f1.COLUMN04 Account,f22.COLUMN04 Memo,0 Credit,SUM(p16.COLUMN13) Debit,3 id  
FROM PUTABLE016 P16  
LEFT OUTER JOIN FITABLE020 F20 ON F20.COLUMN04 = P16.COLUMN09 AND F20.COLUMNA03 = P16.COLUMNA03 AND F20.COLUMNA02 = P16.COLUMNA02 
AND ISNULL(F20.COLUMNA13,0)=0
left outer join fitable022 f22 on  f22.COLUMN01 = p16.COLUMN05 AND f22.COLUMN05 = p16.COLUMN13 AND f22.COLUMN03 = p16.COLUMN03 AND 
f22.COLUMNA03 = F20.COLUMNA03 AND f22.COLUMNA02 = F20.COLUMNA02 AND ISNULL(f22.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p16.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 
and p16.COLUMN06='PAYMENT VOUCHER' group by f1.COLUMN04,f22.COLUMN04,p16.COLUMN13
union all
select f1.COLUMN04 Account,(case when p19.COLUMN06='PAYMENT VOUCHER' then f22.COLUMN04 else p19.COLUMN09 end) Memo,sum(p19.COLUMN10) Credit,0 Debit,4 id  from putable019 p19
--left outer join putable019 p19 on p19.COLUMN05= F20.COLUMN04 and p19.COLUMNA03= F20.COLUMNA03 and p19.COLUMNA02= F20.COLUMNA02 and p19.COLUMN06='PAYMENT VOUCHER' and isnull(p19.COLUMNA13,0)=0
left outer join FITABLE020 F20 on F20.COLUMN04=p19.COLUMN05 and F20.COLUMNA03=p19.COLUMNA03 and F20.COLUMNA02=p19.COLUMNA02 and isnull(F20.COLUMNA13,0)=0 
left join FITABLE022 f22 on F20.COLUMN01=f22.column09 and F20.COLUMNA03=f22.COLUMNA03 and F20.COLUMNA02=f22.COLUMNA02 and f22.column01 = p19.COLUMN08  and isnull(f22.COLUMNA13,0)=0  and F20.COLUMNA02=f22.COLUMNA02
left outer join FITABLE001 f1 on f1.COLUMN02 = p19.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 and p19.COLUMN06='PAYMENT VOUCHER'
group by f1.COLUMN04,p19.COLUMN09,p19.COLUMN06,f22.column04
union all
select f1.COLUMN04 Account,F52.COLUMN10 Memo,sum(F52.COLUMN11) Credit,0  Debit,5 id  from FITABLE020 F20
left outer join FITABLE052 F52 on F52.COLUMN05= F20.COLUMN04 and F52.COLUMNA03= F20.COLUMNA03 and F52.COLUMNA02= F20.COLUMNA02 and F52.COLUMN06='PAYMENT VOUCHER' and isnull(F52.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F52.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F52.COLUMN10
union all
select f1.COLUMN04 Account,F28.COLUMN08 Memo,0 Credit,sum(F28.COLUMN09)  Debit,6 id  from FITABLE020 F20
left outer join FITABLE028 F28 on F28.COLUMN06= F20.COLUMN04 and F28.COLUMNA03= F20.COLUMNA03 and F28.COLUMNA02= F20.COLUMNA02 and F28.COLUMN05='PAYMENT VOUCHER' and isnull(F28.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F28.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F28.COLUMN08
union all
select f1.COLUMN04 Account,P18.COLUMN08 Memo,0 Credit,sum(P18.COLUMN09) Debit,7 id  from PUTABLE018 P18
left outer join FITABLE020 F20 on CAST(F20.COLUMN04 as nvarchar(250))=cast(P18.column05 as nvarchar(250)) 
and (F20.COLUMNA02=P18.COLUMNA02 OR F20.COLUMNA02 is null) 
and F20.COLUMNA03=P18.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 and F20.COLUMN05 = P18.column04
left outer join FITABLE001 f1 on f1.COLUMN02 = P18.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 and P18.COLUMN06='PAYMENT VOUCHER'
group by f1.COLUMN04,P18.COLUMN08
union all
select f1.COLUMN04 Account,F25.column07 Memo,0 Credit,sum(F25.COLUMN09)  Debit,8 id  from FITABLE020 F20
left outer join FITABLE025 F25 on F25.COLUMN05= F20.COLUMN01 and F25.COLUMNA03= F20.COLUMNA03 and F25.COLUMNA02= F20.COLUMNA02 and F25.COLUMN04='PAYMENT VOUCHER' and isnull(F25.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F25.COLUMN08
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 and f1.column07 = 22406
group by f1.COLUMN04,F25.column07
union all
select f1.COLUMN04 Account,F35.COLUMN05 Memo,sum(F35.COLUMN07) Credit,0  Debit,9 id  from FITABLE020 F20
left outer join FITABLE035 F35 on F35.COLUMN09= F20.COLUMN04 and F35.COLUMNA03= F20.COLUMNA03 and F35.COLUMNA02= F20.COLUMNA02 and F35.COLUMN03='PAYMENT VOUCHER' and isnull(F35.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F35.COLUMN10
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F35.COLUMN05
union all
select f1.COLUMN04 Account,F60.COLUMN05 Memo,0 Credit,sum(F60.COLUMN07)  Debit,10 id  from FITABLE020 F20
left outer join FITABLE060 F60 on F60.COLUMN09= F20.COLUMN04 and F60.COLUMNA03= F20.COLUMNA03 and F60.COLUMNA02= F20.COLUMNA02 and F60.COLUMN03='PAYMENT VOUCHER' and isnull(F60.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F60.COLUMN10
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F60.COLUMN05
union all
select f1.COLUMN04 Account,F63.COLUMN05 Memo,-sum(F63.COLUMN08) Credit,0  Debit,11 id  from FITABLE020 F20
left outer join FITABLE063 F63 on F63.COLUMN09= F20.COLUMN04 and F63.COLUMNA03= F20.COLUMNA03 and F63.COLUMNA02= F20.COLUMNA02 and F63.COLUMN03='PAYMENT VOUCHER' and isnull(F63.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F63.COLUMN10
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F63.COLUMN05
union all
select f1.COLUMN04 Account,F29.COLUMN08 Memo,-sum(F29.COLUMN10) Credit,0 Debit,12 id   from FITABLE020 F20
left outer join  FITABLE029 F29 on F29.COLUMN11= F20.COLUMN04 and F29.COLUMNA03= F20.COLUMNA03 and F29.COLUMNA02= F20.COLUMNA02 and F29.COLUMN03='PAYMENT VOUCHER' and isnull(F29.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F29.COLUMN07
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F29.COLUMN08
union all
select f26.COLUMN17 Account,F26.column07 Memo,0 Credit,sum(F26.COLUMN11) Debit,16 id  from FITABLE020 F20
left outer join FITABLE026 F26 on F26.COLUMN09= F20.COLUMN04 and F26.COLUMNA03= F20.COLUMNA03 and F26.COLUMNA02= F20.COLUMNA02 and F26.COLUMN04='PAYMENT VOUCHER' and isnull(F26.COLUMNA13,0)=0
left outer join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and isnull(F20.COLUMNA13,0)=0 and m13.column16 in (23583,23582,23584,23585,23586) 
group by f26.COLUMN17,F26.column07
union all
select f1.COLUMN04 Account,F26.column07 Memo,-sum(F26.COLUMN12) Credit,0 Debit,13 id  from FITABLE020 F20
left outer join FITABLE026 F26 on F26.COLUMN09= F20.COLUMN04 and F26.COLUMNA03= F20.COLUMNA03 and F26.COLUMNA02= F20.COLUMNA02 and F26.COLUMN04='PAYMENT VOUCHER' and isnull(F26.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F26.COLUMN08 
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and isnull(F20.COLUMNA13,0)=0 and f1.column07 = 22383
group by f1.COLUMN04,F26.column07
union all
select f1.COLUMN04 Account,F25.column07 Memo,-sum(F25.COLUMN11) Credit,0 Debit,14 id  from FITABLE020 F20
left outer join FITABLE025 F25 on F25.COLUMN05= F20.COLUMN01 and F25.COLUMNA03= F20.COLUMNA03 and F25.COLUMNA02= F20.COLUMNA02 and F25.COLUMN04='PAYMENT VOUCHER' and isnull(F25.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F25.COLUMN08
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0 and f1.column07 = 22405
group by f1.COLUMN04,F25.column07
union all
select f1.COLUMN04 Account,F25.column09 Memo,-sum(F25.COLUMN11) Credit,0 Debit,15 id  from FITABLE020 F20
left outer join PUTABLE017 F25 on F25.COLUMN05= F20.COLUMN01 and F25.COLUMNA03= F20.COLUMNA03 and F25.COLUMNA02= F20.COLUMNA02 and F25.COLUMN06='PAYMENT VOUCHER' and isnull(F25.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F25.COLUMN08
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F25.column09
)
select [Account],[Memo],SUM(Credit)Credit,SUM(Debit)Debit,[id] from MyTable
group by [Account],[Memo],[id] order by id asc
end

GO
