USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetExciseTaxes]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetExciseTaxes](@TransID nvarchar(250),@AcOwner nvarchar(250),@Type nvarchar(250)=null)
as begin
declare @lineedcess decimal(18,2),@lineshcess decimal(18,2),@headeredcess decimal(18,2),@headershcess decimal(18,2)
if(@Type='Bill')
begin
set @lineedcess=(select sum(isnull(p.linetotaltax,0))linetotaltax from
(select concat(' EC@',b.COLUMN22,'%') linetaxes,b.COLUMN22 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column20,0)*isnull(b.COLUMN22,0)*0.01) as decimal(18,2)) linetotaltax   from 
  PUTABLE006 a inner join PUTABLE005 h on h.column01=a.COLUMN13 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0
  inner join MATABLE013 b on b.column02=a.column18 and isnull(b.COLUMN20,0)=1 and isnull(b.COLUMN22,0)!=0 and b.COLUMN16=23513 
  and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.COLUMN13=@TransID )p)

set @lineshcess=(select sum(isnull(p.linetotaltax,0))linetotaltax from(
select concat(' SHEC@',b.COLUMN23,'%') linetaxes,b.COLUMN23 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column20,0)*isnull(b.COLUMN23,0)*0.01) as decimal(18,2)) linetotaltax   from 
  PUTABLE006 a inner join PUTABLE005 h on h.column01=a.COLUMN13 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0 
  inner join MATABLE013 b on b.column02=a.column18 and isnull(b.COLUMN21,0)=1 and isnull(b.COLUMN23,0)!=0 
  and b.COLUMN16=23513 and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.COLUMN13=@TransID)p)

select p.linetaxes,p.linetaxamt,sum(isnull(p.linetotaltax,0))linetotaltax from(
select concat(' EC@',b.COLUMN22,'%') linetaxes,b.COLUMN22 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column20,0)*isnull(b.COLUMN22,0)*0.01) as decimal(18,2)) linetotaltax   from 
  PUTABLE006 a inner join PUTABLE005 h on h.column01=a.COLUMN13 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0
  inner join MATABLE013 b on b.column02=a.column18 and isnull(b.COLUMN20,0)=1 and isnull(b.COLUMN22,0)!=0 and b.COLUMN16=23513 
  and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.COLUMN13=@TransID 
   union all 
select concat(' SHEC@',b.COLUMN23,'%') linetaxes,b.COLUMN23 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column20,0)*isnull(b.COLUMN23,0)*0.01) as decimal(18,2)) linetotaltax   from 
  PUTABLE006 a inner join PUTABLE005 h on h.column01=a.COLUMN13 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0 
  inner join MATABLE013 b on b.column02=a.column18 and isnull(b.COLUMN21,0)=1 and isnull(b.COLUMN23,0)!=0 
  and b.COLUMN16=23513 and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.COLUMN13=@TransID
   union all 
select concat(' EC@',b.COLUMN22,'%') linetaxes,b.COLUMN22 linetaxamt,concat(' EC@',b.COLUMN22,'%') headertaxes,b.COLUMN22 headertaxamt,
cast(isnull(h.COLUMN45,0) as decimal(18,2))-cast(isnull(@lineedcess,0) as decimal(18,2)) linetotaltax   from 
  PUTABLE005  h 
  inner join MATABLE013 b on b.column02=h.column23 and isnull(b.COLUMN20,0)=1 and isnull(b.COLUMN22,0)!=0
   and b.COLUMN16=23513 and b.COLUMNA03=h.COLUMNA03 and isnull(h.COLUMNA13,0)=0
   and  h.column01=@TransID and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN45,0)>0
   union all 
select concat(' SHEC@',b.COLUMN23,'%') linetaxes,b.COLUMN23 linetaxamt,concat(' SHEC@',b.COLUMN23,'%') headertaxes,b.COLUMN23 headertaxamt,
cast(isnull(h.COLUMN46,0) as decimal(18,2))-cast(isnull(@lineshcess,0) as decimal(18,2)) linetotaltax   from 
 PUTABLE005  h 
  inner join MATABLE013 b on b.column02=h.column23 and isnull(b.COLUMN21,0)=1 and isnull(b.COLUMN23,0)!=0
   and b.COLUMN16=23513 and b.COLUMNA03=h.COLUMNA03 and isnull(h.COLUMNA13,0)=0 
   and  h.column01=@TransID and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN46,0)>0 
   )p group by linetaxes,p.linetaxamt
end
else
begin
set @lineedcess=(select sum(isnull(p.linetotaltax,0))linetotaltax from
(select concat(' EC@',b.COLUMN22,'%') linetaxes,b.COLUMN22 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column23,0)*isnull(b.COLUMN22,0)*0.01) as decimal(18,2)) linetotaltax   from 
  SATABLE010 a inner join SATABLE009 h on h.column01=a.column15 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0
  inner join MATABLE013 b on b.column02=a.column21 and isnull(b.COLUMN20,0)=1 and isnull(b.COLUMN22,0)!=0 and b.COLUMN16=23513 
  and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.column15=@TransID )p)

set @lineshcess=(select sum(isnull(p.linetotaltax,0))linetotaltax from(
select concat(' SHEC@',b.COLUMN23,'%') linetaxes,b.COLUMN23 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column23,0)*isnull(b.COLUMN23,0)*0.01) as decimal(18,2)) linetotaltax   from 
  SATABLE010 a inner join SATABLE009 h on h.column01=a.column15 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0 
  inner join MATABLE013 b on b.column02=a.column21 and isnull(b.COLUMN21,0)=1 and isnull(b.COLUMN23,0)!=0 
  and b.COLUMN16=23513 and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.column15=@TransID)p)

select p.linetaxes,p.linetaxamt,sum(isnull(p.linetotaltax,0))linetotaltax from(
select concat(' EC@',b.COLUMN22,'%') linetaxes,b.COLUMN22 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column23,0)*isnull(b.COLUMN22,0)*0.01) as decimal(18,2)) linetotaltax   from 
  SATABLE010 a inner join SATABLE009 h on h.column01=a.column15 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0
  inner join MATABLE013 b on b.column02=a.column21 and isnull(b.COLUMN20,0)=1 and isnull(b.COLUMN22,0)!=0 and b.COLUMN16=23513 
  and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.column15=@TransID 
   union all 
select concat(' SHEC@',b.COLUMN23,'%') linetaxes,b.COLUMN23 linetaxamt,NULL headertaxes,NULL headertaxamt,
cast((isnull(a.column23,0)*isnull(b.COLUMN23,0)*0.01) as decimal(18,2)) linetotaltax   from 
  SATABLE010 a inner join SATABLE009 h on h.column01=a.column15 and h.COLUMNA03=a.COLUMNA03 and isnull(h.COLUMNA13,0)=0 
  inner join MATABLE013 b on b.column02=a.column21 and isnull(b.COLUMN21,0)=1 and isnull(b.COLUMN23,0)!=0 
  and b.COLUMN16=23513 and b.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0  and a.column15=@TransID
   union all 
select concat(' EC@',b.COLUMN22,'%') linetaxes,b.COLUMN22 linetaxamt,concat(' EC@',b.COLUMN22,'%') headertaxes,b.COLUMN22 headertaxamt,
cast(isnull(h.COLUMN61,0) as decimal(18,2))-cast(isnull(@lineedcess,0) as decimal(18,2)) linetotaltax   from 
  SATABLE009  h 
  inner join MATABLE013 b on b.column02=h.column23 and isnull(b.COLUMN20,0)=1 and isnull(b.COLUMN22,0)!=0
   and b.COLUMN16=23513 and b.COLUMNA03=h.COLUMNA03 and isnull(h.COLUMNA13,0)=0
   and  h.column01=@TransID and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN61,0)>0
   union all 
select concat(' SHEC@',b.COLUMN23,'%') linetaxes,b.COLUMN23 linetaxamt,concat(' SHEC@',b.COLUMN23,'%') headertaxes,b.COLUMN23 headertaxamt,
cast(isnull(h.COLUMN62,0) as decimal(18,2))-cast(isnull(@lineshcess,0) as decimal(18,2)) linetotaltax   from 
 SATABLE009  h 
  inner join MATABLE013 b on b.column02=h.column23 and isnull(b.COLUMN21,0)=1 and isnull(b.COLUMN23,0)!=0
   and b.COLUMN16=23513 and b.COLUMNA03=h.COLUMNA03 and isnull(h.COLUMNA13,0)=0 
   and  h.column01=@TransID and isnull(h.COLUMNA13,0)=0 and isnull(h.COLUMN62,0)>0 
   )p group by linetaxes,p.linetaxamt
end
end

GO
