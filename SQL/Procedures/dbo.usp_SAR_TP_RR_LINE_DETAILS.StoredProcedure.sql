USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_RR_LINE_DETAILS]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_RR_LINE_DETAILS]
( @ReceiptID int)
AS
BEGIN
	SELECT  
	b.COLUMN03, b.COLUMN04,b.COLUMN05,b.COLUMN07,b.COLUMN27,b.COLUMN08,b.COLUMN26,b.COLUMN09,
	b.COLUMN25,b.COLUMN11,(cast(((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*cast(isnull(b.COLUMN25,0) as decimal(18,2))) as decimal(18,2))) aCOLUMN25,isnull(m.column07,0) aCOLUMN26
	FROM SATABLE005 a inner join SATABLE006 b on a.COLUMN01=b.COLUMN19 and isnull(b.COLUMNA13,0)=0
	left join MATABLE013 m on  m.column02=b.COLUMN21
	WHERE  a.COLUMN02= @ReceiptID and isnull(a.COLUMNA13,0)=0
END
GO
