USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FIN_REPORT_BANKRECONCILATION]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_FIN_REPORT_BANKRECONCILATION]
(
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Type nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
--@OperatingUnit nvarchar(250)=null,
@Bank        nvarchar(250)= null,
@Reconcilid        nvarchar(250)= null,
@DateF nvarchar(250)=null
)
AS
BEGIN
declare @whereStr nvarchar(max), @whereStr1 nvarchar(max)
if (@Reconcilid!='' and @Reconcilid not in(23132))
begin
set @whereStr1=' where Reconcilid in ('''+@Reconcilid+''')'
end
if @Type!=''
begin
if(@whereStr1='' or @whereStr1 is null)
begin
set @whereStr1=' where  BankType='''+@Type+''''
end
else
begin
set @whereStr1=@whereStr1+' and  BankType='''+@Type+''''
end
end
if(@whereStr1='' or @whereStr1 is null)
begin
 set @whereStr1=' where 1=1'
 end
if @OPUnit!=''
begin
set @whereStr=' and p.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
end
if @Bank!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and  p.[COLUMN03 ] in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Bank+''') s)'
end
else
begin
set @whereStr=@whereStr+' and  p.[COLUMN03 ] in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Bank+''') s)'
end
end
if (@FromDate!='' and @ToDate!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and  p.[COLUMN04] between '''+@FromDate+''' and '''+@ToDate+''''
end
else
begin
set @whereStr=@whereStr+' and  p.[COLUMN04] between '''+@FromDate+''' and '''+@ToDate+''''
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
 end
else
begin
 set @whereStr=@whereStr
end
exec(';With MyTable AS
(
	--(SELECT NULL Checkrow,s.column04 Bank,NULL Date,NULL  Dtt,NULL Trans,NULL Type,
	--	''Previous Amount'' Memo,NULL name,NULL PaymentAmount,
	--	NULL DepositAmount,NULL TransID,isnull(sum(p.column11),0)-isnull(sum(p.column10),0) AS BalanceAmount,
	--	NULL Partytype,NULL BankID,NULL OperatingUnit,
	--	0 ab,-1 RNO,NULL BankType,NULL ReconcilStatus,NULL Reconcilid From	PUTABLE019 p 
	--	left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 AND S.COLUMNA03=P.COLUMNA03 where  p.COLUMN04 <'''+@FromDate+''' AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p.COLUMNA03='''+@AcOwner+''' AND ISNULL(P.COLUMNA13,0)=0 '+ @whereStr +' group by s.column04)
--union all
SELECT NULL Checkrow,s.[COLUMN04 ] AS Bank,FORMAT(p.COLUMN04,'''+@DateF+''') AS Date,p.COLUMN04 Dtt,p.[COLUMN05 ] AS Trans,p.COLUMN06 AS Type,
(case when p.COLUMN06=''BILL PAYMENT'' then (select COLUMN10 from PUTABLE014 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''ISSUE CHEQUE'' or p.COLUMN06=''ADVANCE PAYMENT'' then (select COLUMN08 from FITABLE020 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03 )
when p.COLUMN06=''PAYMENT VOUCHER'' then (select COLUMN04 from FITABLE022 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''PAYMENT'' then (select COLUMN10 from SATABLE011 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''JOBBER PAYMENT'' then (select COLUMN10 from SATABLE011 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''RECEIVE PAYMENT'' or p.COLUMN06=''ADVANCE RECEIPT'' then (select COLUMN11 from FITABLE023 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''RECEIPT VOUCHER'' then (select COLUMN11 from FITABLE023 f where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''EXPENCE'' or p.COLUMN06=''EXPENSE'' then (select COLUMN13 from FITABLE045 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03) 
when p.COLUMN06=''COMMISSION PAYMENT'' then (select COLUMN10 from PUTABLE014 where COLUMN01=p.COLUMN08 and COLUMNA03=p.COLUMNA03)
when p.COLUMN06=''JOURNAL ENTRY'' then p.COLUMN09		
else NULL end) as Memo,
(case when p.COLUMN06=''BILL PAYMENT'' or p.COLUMN06=''ISSUE CHEQUE'' or p.COLUMN06=''ADVANCE PAYMENT''  or p.COLUMN06=''JOBBER PAYMENT'' then (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN07) when p.COLUMN06=''PAYMENT VOUCHER'' or p.COLUMN06=''ADVANCE PAYMENT'' then (case when pv1.COLUMN11=22305 then a.COLUMN05 when pv1.COLUMN11=22334 then a.COLUMN05 when pv1.COLUMN11=22335 then b.COLUMN05 when pv1.COLUMN11=22492 then m.COLUMN09 when pv1.COLUMN11=22700 then ''OTHERS'' else '''' end) when p.COLUMN06=''PAYMENT'' or p.COLUMN06=''RECEIVE PAYMENT'' or p.COLUMN06=''ADVANCE RECEIPT'' or p.COLUMN06=''RECEIPT VOUCHER'' then (select COLUMN05 from SATABLE002 where COLUMN02=p.COLUMN07) when p.COLUMN06=''EXPENCE'' or p.COLUMN06=''EXPENSE'' then (select COLUMN09 from MATABLE010 where COLUMN02=p.COLUMN07) when p.COLUMN06=''COMMISSION PAYMENT'' then (select COLUMN09 from MATABLE010 where COLUMN02=p.COLUMN07 and COLUMN30=''True'') WHEN p.COLUMN06=''JOURNAL ENTRY'' then (case when  j.column07=''22305'' or fj.column14=''22305'' then a.COLUMN05 when  j.column07=''22335'' or fj.column14=''22335'' then b.COLUMN05 when  j.column07=''22334'' or fj.column14=''22334'' then a.COLUMN05 when j.column07=''22306'' or fj.column14=''22306'' then m.COLUMN09 when j.column07=''22492'' or fj.column14=''22492'' then m.COLUMN09 else '''' end)
else NULL end) name,p.[COLUMN10 ] AS PaymentAmount,p.[COLUMN11 ] AS DepositAmount,p.COLUMN08 TransID,(SUM(isnull(p.COLUMN11,0))  
-SUM(isnull(p.COLUMN10,0)) ) AS BalanceAmount,
(case when p.COLUMN06=''BILL PAYMENT'' or p.COLUMN06=''ISSUE CHEQUE''   then ''VENDOR'' when p.COLUMN06=''ADVANCE PAYMENT'' or p.COLUMN06=''PAYMENT VOUCHER'' then (case when pv1.COLUMN11=22305 then ''VENDOR'' when pv1.COLUMN11=22334 then ''JOBBER'' when pv1.COLUMN11=22335 then ''CUSTOMER'' when pv1.COLUMN11=22492 then ''EMPLOYEE'' when pv1.COLUMN11=22700 then ''OTHERS'' else '''' end) when p.COLUMN06=''PAYMENT'' or p.COLUMN06=''RECEIVE PAYMENT'' or p.COLUMN06=''ADVANCE RECEIPT'' or p.COLUMN06=''RECEIPT VOUCHER'' then ''CUSTOMER'' when p.COLUMN06=''EXPENCE'' or p.COLUMN06=''EXPENSE'' then ''EMPLOYEE'' when p.COLUMN06=''COMMISSION PAYMENT''  then ''SALES REP'' WHEN p.COLUMN06=''JOURNAL ENTRY'' then (case when  j.column07=''22305'' or fj.column14=''22305'' then ''VENDOR'' when  j.column07=''22335'' or fj.column14=''22335'' then ''CUSTOMER'' when  j.column07=''22334'' or fj.column14=''22334'' then ''JOBBER'' when j.column07=''22306'' or fj.column14=''22306'' then ''SALES REP'' when j.column07=''22492'' or fj.column14=''22492'' then ''EMPLOYEE'' else '''' end)	else NULL end) Partytype,
p.[COLUMN03] AS BankID,o.COLUMN03 OperatingUnit,p.COLUMNA02 OperatingUnitId,max(p.COLUMN02) ab,row_number() over(order by p.COLUMN04) RNO,
(case when isnull(p.COLUMN10,0)>0 then ''Payable'' when isnull(p.COLUMN11,0)>0 then ''Receivable'' else '''' end) BankType,rs.COLUMN04 ReconcilStatus,iif(br.COLUMN02>0,23133,23134) Reconcilid From	PUTABLE019 p 
left join  FITABLE001 s on s.COLUMN02=p.COLUMN03  AND S.COLUMNA03=P.COLUMNA03  left outer join FITABLE044 f44 on f44.COLUMN02=p.COLUMN14 left outer join FITABLE031 j on j.COLUMN01=p.COLUMN08 and j.COLUMN04=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0
left join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=p.COLUMN03 left outer join SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03 and isnull(a.COLUMNA13,0)=0
left join SATABLE002 b on b.COLUMN02=p.COLUMN07  and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
left join MATABLE010 m on m.COLUMN02=p.COLUMN07  and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 left outer join FITABLE022 f22 on f22.COLUMN01=p.COLUMN08 and f22.COLUMNA03=p.COLUMNA03 and f22.COLUMNA02=p.COLUMNA02
left join FITABLE001  pv on pv.COLUMN02=f22.COLUMN03 and pv.COLUMNA03=p.COLUMNA03 
left join FITABLE024 f24 on f24.COLUMN01=p.COLUMN08 and f24.COLUMNA03=p.COLUMNA03  and f24.COLUMNA02=p.COLUMNA02
left join FITABLE001  rv1 on rv1.COLUMN02=f24.COLUMN03 and rv1.COLUMNA03=f24.COLUMNA03
left join FITABLE020 pv1 on pv1.COLUMN01=(case when p.COLUMN06=''PAYMENT VOUCHER'' then f22.COLUMN09 else p.COLUMN08 end) and pv1.COLUMNA03=p.COLUMNA03
left join FITABLE023 rv on rv.COLUMN01=f24.COLUMN09 and rv.COLUMNA03=p.COLUMNA03
left join CONTABLE007 o on o.COLUMN02=p.COLUMNA02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left join SETABLE021 br on br.COLUMN06=p.COLUMN05 and br.COLUMN08=p.COLUMN06 and br.COLUMN09=p.COLUMN08 and br.COLUMN12=p.COLUMN03 and br.COLUMNA03=p.COLUMNA03 and isnull(br.COLUMNA13,0)=0
left join MATABLE002 rs on rs.COLUMN02=(iif(br.COLUMN02>0,23133,23134)) and rs.COLUMN04!=''''
where isnull((p.COLUMNA13),0)=0 and 
p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p.COLUMNA03='''+@AcOwner+''' '+@whereStr+'
group by s.COLUMN04,p.COLUMN03,p.COLUMN04,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN08,p.COLUMN09,p.COLUMN10,p.COLUMN11,o.COLUMN03,p.COLUMNA02,p.COLUMNA03,a.COLUMN05,b.COLUMN05,m.COLUMN09,j.COLUMN07,fj.COLUMN14,pv1.COLUMN11,rs.COLUMN04,br.COLUMN02
having (isnull(p.COLUMN11,0)-isnull(p.COLUMN10,0))!=0)
Select Checkrow,Bank,[Date],Trans,[Type],Memo,Name,PaymentAmount,DepositAmount,TransID,
(SUM(isnull(BalanceAmount,0)) OVER(PARTITION BY Bank ORDER BY Dtt   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	) AS BalanceAmount,Partytype,OperatingUnit,OperatingUnitId,BankID,ab,BankType,ReconcilStatus,Reconcilid
From MyTable '+@whereStr1+' order by RNO desc')
END


GO
