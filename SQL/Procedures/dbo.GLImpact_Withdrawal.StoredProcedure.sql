USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GLImpact_Withdrawal]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GLImpact_Withdrawal] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,p.column19 Memo,p19.COLUMN10 Credit,p19.COLUMN11 Debit  from FITABLE062 p
left outer join putable019 p19 on p19.COLUMN05= p.COLUMN05  and p19.COLUMNA03= p.COLUMNA03 and p19.COLUMNA02= p.COLUMNA02 and p19.COLUMN06 in('WITHDRAW','DEPOSIT','TRANSFER') and isnull(p19.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p19.COLUMN03 AND f1.COLUMNA03 = p19.COLUMNA03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
UNION ALL
select f1.COLUMN04 Account,p.column19 Memo,F52.COLUMN11 Credit,F52.COLUMN12 Debit  from FITABLE062 p
left outer join FITABLE052 F52 on F52.COLUMN05= p.COLUMN05  AND F52.COLUMN09= p.COLUMN01 and F52.COLUMNA03= p.COLUMNA03 and F52.COLUMNA02= p.COLUMNA02 and F52.COLUMN06 in('DEPOSIT','WITHDRAW','TRANSFER') and isnull(F52.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F52.COLUMN03 AND f1.COLUMNA03 = F52.COLUMNA03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
)
select [Account],[Memo],sum(Credit)Credit,sum(Debit)Debit from MyTable
group by [Account],[Memo]
end


GO
