USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[BILlGLImpact]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BILlGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,p.column12 Memo,p16.COLUMN12 Credit,p16.COLUMN13 Debit  from PUTABLE005 p
left outer join putable016 p16 on p16.COLUMN09= p.COLUMN04 and p16.COLUMN07= p.COLUMN05 and p16.COLUMNA03= p.COLUMNA03 and p16.COLUMNA02= p.COLUMNA02 and p16.COLUMN06='BILL' and isnull(p16.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p16.COLUMN03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
--union all
--select f1.COLUMN04 Account,p.column12 Memo,p17.COLUMN10 Credit,p17.COLUMN11 Debit  from PUTABLE005 p
--left outer join putable017 p17 on p17.COLUMN05= p.COLUMN01 and p17.COLUMN07= p.COLUMN05 and p17.COLUMNA03= p.COLUMNA03 and p17.COLUMNA02= p.COLUMNA02 and p17.COLUMN06='BILL' and isnull(p17.COLUMNA13,0)=0
--left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03
--where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
--union all
--select f26.COLUMN17 Account,p.column12 Memo,f26.COLUMN11 Credit,f26.COLUMN12 Debit from PUTABLE005 p
--left outer join fitable026 f26 on f26.COLUMN09 =  p.COLUMN04 and  f26.COLUMN06 =  p.COLUMN05 and f26.COLUMNA03=  p.COLUMNA03 
--and f26.COLUMNA02=  p.COLUMNA02 and f26.COLUMN04 = 'BILL' and isnull(f26.COLUMNA13,0)=0
--left outer join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 
--where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,p.column12 Memo,f36.COLUMN08 Credit,f36.COLUMN07 Debit from PUTABLE005 p
left outer join fitable036 f36 on f36.COLUMN09 = p.COLUMN04  and f36.COLUMNA03= p.COLUMNA03 and f36.COLUMNA02= p.COLUMNA02 and f36.COLUMN03 = 'BILL' and isnull(f36.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f36.COLUMN10 and f1.COLUMNA03 = f36.COLUMNA03
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner  and isnull(p.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,p.column12 Memo,f25.COLUMN09 Credit,f25.COLUMN11 Debit from PUTABLE005 p
left outer join fitable025 f25 on f25.COLUMN05 = p.COLUMN01 and f25.COLUMN06 = p.COLUMN05 and f25.COLUMNA03= p.COLUMNA03 and f25.COLUMNA02= p.COLUMNA02 and f25.COLUMN04 = 'BILL' and isnull(f25.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f25.COLUMN08 and f1.COLUMNA03 = f25.COLUMNA03 
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,p.column12 Memo,f34.COLUMN08 Credit,f34.COLUMN07 Debit from PUTABLE005 p
left outer join fitable034 f34 on f34.COLUMN09 = p.COLUMN04 and f34.COLUMN06 = p.COLUMN05 and f34.COLUMNA03= p.COLUMNA03 and f34.COLUMNA02= p.COLUMNA02 and f34.COLUMN03 = 'BILL' and isnull(f34.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10 and f1.COLUMNA03 = f34.COLUMNA03 AND ISNULL(f1.COLUMNA13,0)=0
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
union all
select f1.COLUMN04 Account,p.column12 Memo,f26.COLUMN11 Credit,f26.COLUMN12 Debit from PUTABLE005 p
left outer join fitable026 f26 on f26.COLUMN06 =  p.COLUMN05 and f26.COLUMNA03=  p.COLUMNA03 
and f26.COLUMNA02= p.COLUMNA02 and f26.COLUMN04 = 'BILL' and isnull(f26.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f26.COLUMN08 and f1.COLUMNA03 = f26.COLUMNA03 AND ISNULL(f1.COLUMNA13,0)=0 
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0 and (isnull(f26.COLUMN11,0)>0 or isnull(f26.COLUMN12,0)>0)
)
select [Account],[Memo],sum(Credit)Credit,sum(Debit)Debit from MyTable
group by [Account],[Memo]
end


GO
