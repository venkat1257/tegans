USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PROC_CHECK_TransactionNo]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PROC_CHECK_TransactionNo](@AcOwner nvarchar(250)=null,@OPUNIT nvarchar(250)=null,
@TransNo nvarchar(250)=null,@FormId nvarchar(250)=null,@Id nvarchar(250)=null)
as
begin
if(@FormId=1251 or @FormId=1355 or @FormId=1328)
begin
select COLUMN02,COLUMN04 from  PUTABLE001 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1275 or @FormId=1353 or @FormId=1330 or @FormId=1283)
begin
select COLUMN02,COLUMN04 from  SATABLE005 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1276 or @FormId=1354 or @FormId=1284)
begin
select COLUMN02,COLUMN04 from  SATABLE007 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1277 and @Id!='0')
begin
select COLUMN02,COLUMN04 from  SATABLE009 where COLUMN02 !=@Id and COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1277 OR @FormId=1532)
begin
select COLUMN02,COLUMN04 from  SATABLE009 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1278)
begin
select COLUMN02,COLUMN04 from  SATABLE011 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1272 or @FormId=1329 or @FormId=1286)
begin
select COLUMN02,COLUMN04 from  PUTABLE003 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1273)
begin
select COLUMN02,COLUMN04 from  PUTABLE005 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1274)
begin
select COLUMN02,COLUMN04 from  PUTABLE014 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1375)
begin
select COLUMN02,COLUMN04 from  SATABLE013 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1376)
begin
select COLUMN02,COLUMN04 from  SATABLE015 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1363 or @FormId=1525)
begin
select COLUMN02,COLUMN04 from  FITABLE020 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1386 or @FormId=1526)
begin
select COLUMN02,COLUMN04 from  FITABLE023 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1414)
begin
select COLUMN02,COLUMN04 from  FITABLE031 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1357)
begin
select COLUMN02,COLUMN04 from  FITABLE014 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1379)
begin
select COLUMN02,COLUMN04 from  PRTABLE003 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1349 or @FormId=1350 or @FormId=1351)
begin
select COLUMN02,COLUMN05 from  FITABLE062 where COLUMN13 =@FormId and COLUMN05 =@TransNo and COLUMN05 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1293)
begin
select COLUMN02,COLUMN17 from  FITABLE012 where COLUMN03 =@FormId and COLUMN17 =@TransNo and COLUMN17 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
else if(@FormId=1528)
begin
select COLUMN02,COLUMN04 from  FITABLE045 where COLUMN03 =@FormId and COLUMN04 =@TransNo and COLUMN04 !='' AND 
COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUNIT) s) AND COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0
end
end



GO
