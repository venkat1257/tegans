USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_VendorAdvanceCalculations]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_PUR_TP_VendorAdvanceCalculations](@refID nvarchar(250)=null,@COLUMNA02 nvarchar(250)=null,
@COLUMNA03 nvarchar(250)=null,@COLUMN02 nvarchar(250)=null,@COLUMN08 nvarchar(250)=null,@VouchrAmt decimal(18,2)=null)
as 
begin

declare @newID1 int,@VendorID int,@Bank nvarchar(250)=null,@Memo1 nvarchar(250),@project1 int,@DATE nvarchar(250),
@IDAR nvarchar(250),@dueamt decimal(18,2),@paidamt decimal(18,2),@COLUMN05 decimal(18,2),@COLUMNA06 nvarchar(250),@COLUMNA08 nvarchar(250),@COLUMNA09 nvarchar(250)
	set @newID1=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1;
	set @VendorID=(select COLUMN05 from PUTABLE014 where column01=@COLUMN08)
	select  @Bank=COLUMN08,@COLUMNA06 = @COLUMNA06,@COLUMNA08 = @COLUMNA08,@COLUMNA09 = @COLUMNA09 from PUTABLE014 where column01=@COLUMN08
	set @Memo1=(select COLUMN10 from PUTABLE014 where column01=@COLUMN08)
	set @DATE=(select COLUMN18 from PUTABLE014 where column01=@COLUMN08)
	set @project1=(select isnull(COLUMN23,0) from PUTABLE014 where column01=@COLUMN08)
	if(cast(isnull(@VouchrAmt,0) as decimal(18,2))> 0)
	begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @refID, @COLUMN05 = @COLUMN08,  @COLUMN06 = NULL,
		@COLUMN07 = @DATE,   @COLUMN08 = '22305',  @COLUMN09 = @VendorID,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @Memo1,   @COLUMN13 = @project1,	@COLUMN14 = '0',       @COLUMN15 = @VouchrAmt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

	if exists(select COLUMN05  from PUTABLE016 where COLUMN09=@refID and COLUMN06='BILL PAYMENT' and COLUMN05=@COLUMN08 AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
	 begin
			declare @pstatus nvarchar(250)
			set @dueamt=(select COLUMN14 from PUTABLE016 where COLUMN09=@refID and COLUMN06='BILL PAYMENT' and COLUMN05=@COLUMN08 AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @paidamt=(select COLUMN13 from PUTABLE016 where COLUMN09=@refID and COLUMN06='BILL PAYMENT' and COLUMN05=@COLUMN08 AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			set @pstatus=('Paid')
			set @COLUMN05 =(select COLUMN05 from PUTABLE015 where column02=@COLUMN02);
		   if(cast(@COLUMN05 as decimal(18,2))=0)
		   begin
		   set @pstatus=('CLOSE')
		   end
            update PUTABLE016 set COLUMN10=@pstatus,
			COLUMN14=(cast(isnull(@dueamt,0) as decimal(18,2))+cast(isnull(@VouchrAmt,0) as decimal(18,2))),
			COLUMN13=(cast(isnull(@paidamt,0) as decimal(18,2))+cast(isnull(@VouchrAmt,0) as decimal(18,2))) 
			where COLUMN09=@refID and COLUMN06='BILL PAYMENT' and COLUMN05=@COLUMN08 AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02
	end
else
begin
	if(@Bank='' or @Bank=null)
	begin
	set @Bank=NULL
	end
insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN13,COLUMN14,COLUMN18,COLUMNA02, COLUMNA03, COLUMN20)
			values(@newID1,2000,@DATE,@COLUMN08,'BILL PAYMENT',@VendorID,@Bank,@refID,'Paid',@VouchrAmt,@VouchrAmt,@Memo1,@COLUMNA02 , @COLUMNA03,@project1) 
end
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL PAYMENT',  @COLUMN04 = @refID, @COLUMN05 = @COLUMN08,  @COLUMN06 = NULL,
		@COLUMN07 = @DATE,   @COLUMN08 = '22305',  @COLUMN09 = @VendorID,    @COLUMN10 = '22402', @COLUMN11 = '1120',
		@COLUMN12 = @Memo1,   @COLUMN13 = @project1,	@COLUMN14 = @VouchrAmt,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

	set @IDAR= (SELECT MAX(COLUMN02) FROM FITABLE034)+1
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,   COLUMN08,COLUMN07,   COLUMN09,COLUMN10,
COLUMN12,	COLUMNA02, COLUMNA03)
values(ISNULL(@IDAR,999), 'BILL PAYMENT', @DATE, @Memo1,@VendorID,@VouchrAmt,0,@refID,'1120',@Project1,@COLUMNA02, @COLUMNA03)
end
end



GO
