USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_PAYMENT_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_PAYMENT_LINE_DATA]
(
-- EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
	@SalesOrderID nvarchar(250),
	@AcOwner int,
 --EMPHCS903   After deleting two lines from item issue II00040 ,system deleted only one line and invoice it is showing all 3 lines srinivas 12/8/2015
	@OPUNIT int,
	@Form nvarchar(50)
)

AS
BEGIN
--declare @Acnt INT
declare @DueAmnt INT
declare @DueAmnt1 int
declare @PayAmnt int
declare @PayAmnt1 int
declare @IVID int
--set @Acnt=(select COLUMNA03 from SATABLE009 where COLUMN05=@SalesOrderID or )
if(@Form='0')
begin
set @DueAmnt=(select isnull(SUM(COLUMN06),0)+isnull(SUM(COLUMN14),0)+isnull(sum(COLUMN09),0) from SATABLE012 where COLUMN08 in (select isnull(max(COLUMN01),0) from SATABLE011 where  isnull(COLUMNA13,0)=0 and COLUMN06=cast( @SalesOrderID as nvarchar)) AND COLUMNA13=0)
if(@DueAmnt=0)
begin  
set @DueAmnt=(select SUM(COLUMN14) from SATABLE010 WHERE  isnull(COLUMNA13,0)=0 and COLUMN15 in(select COLUMN01 from SATABLE009 where  isnull(COLUMNA13,0)=0 and COLUMN05= cast( @SalesOrderID as nvarchar)))
 select distinct b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,(isnull(max (b.COLUMN20),0)) as COLUMN05,
 --//EMPHCS1136 customer name is not getting for  payment by srinivas 13/09/2015
  (select sum(COLUMN15) from SATABLE005 where  COLUMN05=b.COLUMN05 and  column11=(SELECT COLUMN04 FROM SATABLE005 WHERE COLUMN02= b.COLUMN06)    and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0) as COLUMN14,
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06,

  --(select STUFF((select  COLUMN02 from SATABLE005 where  COLUMN05=b.COLUMN05 and  column11=(SELECT COLUMN04 FROM SATABLE005 WHERE COLUMN02= b.COLUMN06)    and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0  FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN16,
  '' COLUMN16,'' directReturnCol
 --(select STUFF((select COLUMN02 from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from SATABLE010 a 
	inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01 and isnull(b.COLUMNA13,0)=0 
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0 
	 WHERE  isnull(a.COLUMNA13,0)=0 and a.COLUMN15 in (select COLUMN01 from SATABLE009 where  isnull(COLUMNA13,0)=0 and COLUMN06= cast( @SalesOrderID as nvarchar))  group by b.COLUMN02,c.COLUMN04, b.COLUMN05,c.COLUMN04,b.COLUMN04,b.COLUMN06

end
else if(@DueAmnt!=0 )
begin
 declare @RetCOUNT1 INT
 set @RetCOUNT1 = (select COUNT(*) from SATABLE005 a inner join SATABLE009 b on a.COLUMN05=b.COLUMN05  where a.COLUMNA02=@OPUNIT and a.COLUMNA03=@AcOwner and  isnull(b.COLUMNA13,0)=0 and a.COLUMN03='1330' and   b.COLUMN02=(select max(COLUMN02) from SATABLE009 where  isnull(COLUMNA13,0)=0 and COLUMN06= cast( @SalesOrderID as nvarchar) and a.COLUMN16!='CLOSE'  and isnull(a.COLUMNA13,0)=0))

if(@RetCOUNT1>=0 )
begin
select distinct e.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03,(max (b.COLUMN20)) as COLUMN04,  

 ( max (b.COLUMN20) -((isnull(sum(d.COLUMN06),0))+(isnull(sum(d.COLUMN09),0))+(isnull(sum(d.COLUMN13),0))+(isnull(sum(d.COLUMN14),0)))) as COLUMN05,
	  (select sum(c.COLUMN15) from SATABLE005 c   where  c.COLUMN05=e.COLUMN05  and c.column11=(select COLUMN04 from SATABLE009 where COLUMN02=c.column04)  and c.COLUMN03='1330' and c.column16!='CLOSE'  and isnull(c.COLUMNA13,0)=0) as COLUMN14
	--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
	  , (select sum(c1.COLUMN15) from SATABLE005 c1   where c1.COLUMN05=e.COLUMN05 and c1.column11='' and  c1.COLUMN03='1330'and c1.column16!='CLOSE' and isnull(c1.COLUMNA13,0)=0) as directReturn,'' AS COLUMN06,
	  --(select STUFF((select  COLUMN02 from SATABLE005 c   where  c.COLUMN05=e.COLUMN05  and c.column11=(select COLUMN04 from SATABLE009 where COLUMN02=c.column04)  and c.COLUMN03='1330' and c.column16!='CLOSE'  and isnull(c.COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN16
	  '' COLUMN16,'' directReturnCol
	   --(select STUFF((select  COLUMN02 from SATABLE005 c1   where c1.COLUMN05=e.COLUMN05 and c1.column11='' and  c1.COLUMN03='1330'and c1.column16!='CLOSE' and isnull(c1.COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol

	from  SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 and isnull(d.COLUMNA13,0)=0
	left outer join SATABLE011 e on e.column01=d.column08 and isnull(e.COLUMNA13,0)=0
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  b.COLUMN06= cast( @SalesOrderID as nvarchar) and isnull(b.COLUMNA13,0)=0  group by b.COLUMN02,c.COLUMN04,e.COLUMN05
end

else
begin
select distinct b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03,(isnull(max (b.COLUMN20),0)) as COLUMN04,

( max (b.COLUMN20) -((isnull(sum(d.COLUMN06),0))+(isnull(sum(d.COLUMN09),0))+(isnull(sum(d.COLUMN13),0))+(isnull(sum(d.COLUMN14),0)))) as COLUMN05,
 null as COLUMN14
	--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
	  , 0 as directReturn,'' AS COLUMN06,null as COLUMN16
	  , '' as directReturnCol
	from SATABLE010 a 
	inner  join SATABLE009 b on a.COLUMN15=b.COLUMN01 and isnull(b.COLUMNA13,0)=0
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 and isnull(d.COLUMNA13,0)=0
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  a.COLUMN15 in (select COLUMN01 from SATABLE009 where COLUMN06= cast( @SalesOrderID as nvarchar) and isnull(COLUMNA13,0)=0) and isnull(a.COLUMNA13,0)=0 group by b.COLUMN02,c.COLUMN04,b.COLUMN05
end

end
end

else
 begin 
 --EMPHCS903   After deleting two lines from item issue II00040 ,system deleted only one line and invoice it is showing all 3 lines srinivas 12/8/2015
 set @IVID=(select column02 from SATABLE009 where COLUMN02=@Form and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT and isnull(COLUMNA13,0)=0);
--if(@SalesOrderID !=0)
--begin
--set @DueAmnt=(select isnull(SUM(COLUMN06),0)+isnull(SUM(COLUMN14),0) from SATABLE012 where COLUMN08 in (select COLUMN01 from SATABLE011 where COLUMN05=cast( @SalesOrderID as nvarchar)) and COLUMN03=@IVID and COLUMNA13=0)

--if(@DueAmnt>0)
--begin  
----set @DueAmnt=(select isnull(sum(COLUMN14),0) from SATABLE010 WHERE  COLUMN15 =(select COLUMN01 from SATABLE009 where COLUMN06= cast( @SalesOrderID as nvarchar)  or COLUMN04=@Form))
----set @DueAmnt1=@DueAmnt-(select isnull(sum(COLUMN06),0) from SATABLE012 where COLUMN08 in (select  COLUMN01 from SATABLE011 where COLUMN06=cast( @SalesOrderID as nvarchar)  ) and COLUMN03=@IVID)

-- select b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04, isnull(min(d.COLUMN05),0) as COLUMN05,
--  --//EMPHCS1136 customer name is not getting for  payment by srinivas 13/09/2015
-- (select COLUMN15 from SATABLE005 where  (COLUMN11=b.column04)  and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and column16!='CLOSE' and columnA13!=1) as COLUMN14,
-- (select sum(COLUMN15) from SATABLE005 where COLUMN05=@SalesOrderID and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06
--	from SATABLE010 a 
--	inner join SATABLE009 b on a.COLUMN15=b.COLUMN01
--	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 
--	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 
--	 WHERE  a.COLUMN15=(select Max(COLUMN01) from SATABLE009 where COLUMN05= cast( @SalesOrderID as nvarchar) and COLUMN02=@Form )group by b.COLUMN02,b.COLUMN04
--end
--else
--begin
-- select b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04,    isnull(max(b.COLUMN20),0) as COLUMN05,
--  --//EMPHCS1136 customer name is not getting for  payment by srinivas 13/09/2015
-- (select COLUMN15 from SATABLE005 where  (COLUMN11=b.column04)  and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and column16!='CLOSE' and columnA13!=1) as COLUMN14,
-- (select sum(COLUMN15) from SATABLE005 where COLUMN05=@SalesOrderID and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06
--	from SATABLE010 a 
--	inner join SATABLE009 b on a.COLUMN15=b.COLUMN01
--	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 
--	 WHERE  a.COLUMN15=(select Max(COLUMN01) from SATABLE009 where COLUMN06= cast( @SalesOrderID as nvarchar) and COLUMN02=@Form) group by b.COLUMN02,b.COLUMN04
--end
--ENd
--ELSE
begin
set @DueAmnt=(select isnull(sum(COLUMN06),0)+isnull(sum(COLUMN14),0)+isnull(sum(COLUMN09),0) from SATABLE012 where  COLUMN03=@IVID and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner AND isnull(COLUMNA13,0)=0)

if(@DueAmnt>0)
begin  

 declare @COUNT INT
 declare @RCOUNT INT
 declare @RetCOUNT INT
 declare @RetAmnt INT
 Declare @n int
 set @n=1

set @COUNT = (select COUNT(*) from SATABLE012 where  COLUMN03=@IVID and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0  and COLUMN14=0)
set @RCOUNT = (select COUNT(*) from SATABLE012 where  COLUMN03=@IVID and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0 and COLUMN14!=0)
set @RetCOUNT =(select COUNT(*) from  SATABLE005 a inner join SATABLE009 b on a.COLUMN05=b.COLUMN05  where a.COLUMNA02=@OPUNIT and a.COLUMNA03=@AcOwner and a.COLUMN03='1330' and   b.COLUMN02=@Form and a.COLUMN16!='CLOSE'  and isnull(a.COLUMNA13,0)=0)
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
set @RetAmnt=(select sum(isnull(column14,0)) from SATABLE012  where column02 not in 
(select top ((select count(*) from SATABLE012 where   COLUMN03=@IVID and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0) - @n ) column02 
from SATABLE012 where   COLUMN03=@IVID and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0) and   COLUMN03=@IVID and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)

if(@RetAmnt!=0 ) 
 begin
  if(@RetCOUNT>=1)
 begin
  select b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04, isnull(max(b.COLUMN20),0)-cast(Sum(isnull(d.COLUMN06,0))+Sum(isnull(d.COLUMN14,0))+Sum(isnull(d.COLUMN13,0))+Sum(isnull(d.COLUMN09,0))
 as decimal(18,2)) as COLUMN05,
   (select sum(COLUMN15) from SATABLE005 where COLUMN05=b.COLUMN05  and column11=b.COLUMN04  and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0) as COLUMN14,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06 ,
   --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=b.COLUMN05  and column11=b.COLUMN04  and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN16,
   '' COLUMN16,'' directReturnCol
 --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol 
	from SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 and isnull(d.COLUMNA13,0)=0
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  b.COLUMN02=@Form and isnull(b.COLUMNA13,0)=0 group by b.COLUMN02,b.COLUMN05, b.COLUMN04,c.COLUMN04
 end
 else
 begin
  select b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04, isnull(max(b.COLUMN20),0)-cast(Sum(isnull(d.COLUMN06,0))+Sum(isnull(d.COLUMN14,0))+Sum(isnull(d.COLUMN13,0))+Sum(isnull(d.COLUMN09,0))
 as decimal(18,2)) as COLUMN05,
 
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 null as COLUMN14 ,0 as directReturn,'' AS COLUMN06 ,null as COLUMN16 ,'' as directReturnCol
	from SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 and isnull(d.COLUMNA13,0)=0
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  b.COLUMN02=@Form  and isnull(b.COLUMNA13,0)=0 group by b.COLUMN02, b.COLUMN04, b.COLUMN05,c.COLUMN04
	 end
 end
 else
 begin
 if(@RetCOUNT>=1)
 begin
  select b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04, isnull(max(b.COLUMN20),0)-cast(Sum(isnull(d.COLUMN06,0))+Sum(isnull(d.COLUMN14,0))+Sum(isnull(d.COLUMN13,0))+Sum(isnull(d.COLUMN09,0))
 as decimal(18,2)) as COLUMN05,
   (select sum(COLUMN15) from SATABLE005 where COLUMN05=b.COLUMN05  and column11=b.COLUMN04  and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0) as COLUMN14,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06  ,
   --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=b.COLUMN05  and column11=b.COLUMN04  and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN16,
   '' COLUMN16,'' directReturnCol
 --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 and isnull(d.COLUMNA13,0)=0
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  b.COLUMN02=@Form and isnull(b.COLUMNA13,0)=0 group by b.COLUMN02,b.COLUMN05, b.COLUMN04,c.COLUMN04
 end
 else
 begin
  select b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04,isnull(max(b.COLUMN20),0)-cast(Sum(isnull(d.COLUMN06,0))+Sum(isnull(d.COLUMN14,0))+Sum(isnull(d.COLUMN13,0))+Sum(isnull(d.COLUMN09,0))
 as decimal(18,2)) as COLUMN05,
 
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 null as COLUMN14 ,0 as directReturn,'' AS COLUMN06 ,null as COLUMN16 ,'' as directReturnCol
	from SATABLE009 b 
	left outer join SATABLE012 d on b.COLUMN02=d.COLUMN03 and isnull(d.COLUMNA13,0)=0
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  b.COLUMN02=@Form and isnull(b.COLUMNA13,0)=0 group by b.COLUMN02, b.COLUMN04, b.COLUMN05,c.COLUMN04
	 end
	 end
end
else
begin
  select b.COLUMN05 as COLUMN20,b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN20),0) as COLUMN04,    isnull(max(b.COLUMN20),0) as COLUMN05,
 (select sum(COLUMN15) from SATABLE005 where  COLUMN05=b.COLUMN05 and column11=b.COLUMN04  and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0) as COLUMN14,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select sum(COLUMN15) from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,
 '' AS COLUMN06,
 --(select STUFF((select  COLUMN02 from SATABLE005 where  COLUMN05=b.COLUMN05 and column11=b.COLUMN04  and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and COLUMN03='1330' and column16!='CLOSE'  and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN16,
 '' COLUMN16,'' directReturnCol
 --(select STUFF((select  COLUMN02 from SATABLE005 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1330'and column16!='CLOSE' and isnull(COLUMNA13,0)=0 FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from SATABLE009 b
	left outer  join SATABLE005 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0
	 WHERE  b.COLUMN02=@Form and isnull(b.COLUMNA13,0)=0 group by b.COLUMN02, b.COLUMN04, b.COLUMN05,c.COLUMN04

end
end
 END  
END













GO
