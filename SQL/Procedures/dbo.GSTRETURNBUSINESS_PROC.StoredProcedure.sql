USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GSTRETURNBUSINESS_PROC]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GSTRETURNBUSINESS_PROC]
(
	@FROMDT NVARCHAR(250),
	@TODT NVARCHAR(250),
	@GSTIN NVARCHAR(250) = NULL,
	@GSTCATEGORY INT = NULL,
	@ACOWNER INT,
	@OPUNIT NVARCHAR(250) =NULL,
	@OPERATING NVARCHAR(250) = NULL,
	@FORMATDT NVARCHAR(250) = NULL,
	@TYPE NVARCHAR(250) = NULL

)
AS

BEGIN
	declare @whereStr nvarchar(1000)=null,@section nvarchar(1000)=null,@invwhereStr nvarchar(1000)=null,@advwhereStr nvarchar(1000)=null,@crwhereStr nvarchar(1000)=null
	if @OPUNIT!=''
		begin
			set @whereStr=' S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
			set @invwhereStr=' S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
			set @advwhereStr=' S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
			set @crwhereStr=' S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'			
		end
		if @GSTIN!=''
		begin
			if(@whereStr='' or @whereStr is null)
				begin
			set @whereStr=' S9.COLUMN64='''+@GSTIN+''''
			set @invwhereStr=' S9.COLUMN64='''+@GSTIN+''''
			set @advwhereStr=' C.COLUMN42='''+@GSTIN+''''
			set @crwhereStr=' S9.COLUMN67='''+@GSTIN+''''
				end
			else 
				begin
			set @whereStr=@whereStr+' and S9.COLUMN64='''+@GSTIN+''''
			set @invwhereStr=@invwhereStr+' and S9.COLUMN64='''+@GSTIN+''''
			set @advwhereStr=@advwhereStr+' and C.COLUMN42='''+@GSTIN+''''
			set @crwhereStr=@crwhereStr+' and S9.COLUMN67='''+@GSTIN+''''
				end
		end
		if(@TYPE='100')
		set @section='(23601)'
		else if(@TYPE='101')
		set @section='(23602,0)'
		else if(@TYPE='102')
		set @section='(23602,0)'
		if(@TYPE='100' OR @TYPE='101' OR @TYPE='102')
		BEGIN
			if(@whereStr='' or @whereStr is null)
				begin
			    set @invwhereStr=' isnull(s9.COLUMN65,0) in'+@section+' '
				end
			else
				begin
			    set @invwhereStr=@invwhereStr+' and isnull(s9.COLUMN65,0) in'+@section+' '
				end
		END
	IF @FROMDT!='' and  @TODT!=''
		begin
		if(@TYPE='103' OR @TYPE='104')
		BEGIN
			if(@whereStr='' or @whereStr is null)
				begin
					set @whereStr=' S9.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @invwhereStr=' S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @advwhereStr=' S9.COLUMN05 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @crwhereStr=' S9.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
			else
				begin
					set @whereStr=@whereStr+' and S9.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @invwhereStr=@invwhereStr+' and S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @advwhereStr=@advwhereStr+' and S9.COLUMN05 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @crwhereStr=@crwhereStr+' and S9.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
		end
		ELSE if(@TYPE='106' OR @TYPE='107')
		BEGIN
			if(@whereStr='' or @whereStr is null)
				begin
					set @whereStr=' S9.COLUMN05 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @advwhereStr=' S9.COLUMN05 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
			else
				begin
					set @whereStr=@whereStr+' and S9.COLUMN05 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @advwhereStr=@advwhereStr+' and S9.COLUMN05 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
		end
		else
		BEGIN
			if(@whereStr='' or @whereStr is null)
				begin
					set @whereStr=' S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @invwhereStr='S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
			else
				begin
					set @whereStr=@whereStr+' and S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
					set @invwhereStr=@invwhereStr+' and S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
		end
		end
	if(@whereStr='' or @whereStr is null)
		begin
			set @whereStr=' 1=1'
		 end
	if(@invwhereStr='' or @invwhereStr is null)
		begin
			set @invwhereStr=' 1=1'
		 end
	if(@advwhereStr='' or @advwhereStr is null)
		begin
			set @advwhereStr=' 1=1'
		 end
	if(@crwhereStr='' or @crwhereStr is null)
		begin
			set @crwhereStr=' 1=1'
		 end
	if(@TYPE='103' )
		BEGIN
			exec ('(SELECT CAST(COUNT(DISTINCT P.GSTIN)AS nvarchar(250)) [No. of Receipients],CAST(sum(P.Invoice)AS nvarchar(250)) [No. of Invoices],'''' as''_'',CAST(sum(P.CRNOTE)AS nvarchar(250))[No. of Notes/Vouchers],'''' as''__'','''' as''___'','''' as''____'','''' as''_____'',CAST(sum(P.Total)AS nvarchar(250))[Total Note/Refund Voucher Value],'''' as''______'',CAST(sum(P.Tax)AS nvarchar(250)) [Total Taxable Value] ,'''' [Total Cess],'''' as''_______'' from
			(SELECT  CAST(S9.COLUMN67 AS nvarchar(250))GSTIN,CAST(COUNT(a.COLUMN04)AS int) Invoice,''''em1,CAST(COUNT(S9.COLUMN04)AS int)CRNOTE,''''em2,''''em3,''''em4,''''em5, CAST(sum(S9.COLUMN15)AS decimal(18,2))Total,''''em6,CAST(sum(S9.COLUMN12)AS decimal(18,2))Tax ,''''em7,''''em8 FROM SATABLE005 S9
			inner join SATABLE009 a on s9.COLUMN49=a.COLUMN02 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			inner join SATABLE002 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			 WHERE s9.column03=1330  and s.column42 is not null and s.column42 !='''' and s9.columna03='''+@ACOWNER+''' and '+@crwhereStr+' 
			GROUP BY S9.COLUMN67,a.COLUMN04 
			union all
			 SELECT  CAST(S.COLUMN34 AS nvarchar(250))GSTIN,CAST(COUNT(a.COLUMN04)AS int) Invoice,''''em1,CAST(COUNT(S9.COLUMN04)AS int)CRNOTE,''''em2,''''em3,''''em4,''''em5, CAST(sum(S9.COLUMN15)AS decimal(18,2))Total,''''em6,CAST(sum(S9.COLUMN32)AS decimal(18,2))Tax ,''''em7,''''em8 FROM PUTABLE001 S9
			inner join puTABLE005 a on s9.COLUMN48=a.COLUMN02 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			inner join SATABLE001 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			 WHERE s9.column03=1355  and s.column34 is not null and s.column34 !='''' and s9.columna03='''+@ACOWNER+''' and '+@crwhereStr+' 
			 GROUP BY S.COLUMN34,a.COLUMN04)P)
			 UNION ALL
			SELECT ''GSTIN/UIN of Recipient'',''Invoice/Advance Receipt Number'',''Invoice/Advance Receipt date'',''Note/Refund Voucher Number'',''Note/Refund Voucher date'',''Document Type'',''Reason For Issuing document'',''Place Of Supply'',''Note/Refund Voucher Value'',''Rate'',''Taxable Value'',''Cess Amount'',''Pre GST'' UNION ALL
			SELECT S9.COLUMN67,b.COLUMN04,CAST(format(b.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)),S9.COLUMN04,CAST(format(S9.COLUMN06,''dd-MMM-yyyy'') AS nvarchar(250)),''C'',CAST(S9.COLUMN09 AS nvarchar(250)),CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST(sum(a.COLUMN11) AS nvarchar(250)),CAST((cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull(sum(isnull(a.column25,0)),0) as decimal(18,2)) AS nvarchar(250)),'''',''''  FROM SATABLE005 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN69
			inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			inner join SATABLE009 b on s9.COLUMN49=b.COLUMN02 and S9.COLUMNA02=b.COLUMNA02 and S9.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
			inner join SATABLE002 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE s9.column03=1330  and s.column42 is not null and s.column42 !='''' and s9.columna03='''+@ACOWNER+''' and  '+@crwhereStr+'
			GROUP BY S9.COLUMN67,b.COLUMN04,b.COLUMN08,S9.COLUMN04,S9.COLUMN06,S9.COLUMN09,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,t.COLUMN07--,a.COLUMN11,a.COLUMN25
			union all
			SELECT S.COLUMN34,b.COLUMN04,CAST(format(b.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)),S9.COLUMN04,CAST(format(S9.COLUMN06,''dd-MMM-yyyy'') AS nvarchar(250)),''D'',CAST(S9.COLUMN09 AS nvarchar(250)),CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST((a.COLUMN11) AS nvarchar(250)),CAST(sum(cast(isnull(t.COLUMN17,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull((isnull(a.column24,0)),0) as decimal(18,2)) AS nvarchar(250)),'''',''''  FROM PUTABLE001 S9  
			inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			inner join PUTABLE005 b on s9.COLUMN48=b.COLUMN02 and S9.COLUMNA02=b.COLUMNA02 and S9.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
			inner join SATABLE001 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S.COLUMN35
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE s9.column03=1355  and s.column34 is not null and s.column34 !='''' and s9.columna03='''+@ACOWNER+''' and  '+@crwhereStr+' 
			GROUP BY S.COLUMN34,b.COLUMN04,b.COLUMN08,S9.COLUMN04,S9.COLUMN06,S9.COLUMN09,a.COLUMN24,t.COLUMN07,a.COLUMN11,M17.COLUMN06,M17.COLUMN05
			')
		END
	ELSE if(@TYPE='104')
		BEGIN
			 exec ('(SELECT CAST(max(P.Type) AS nvarchar(250)),CAST(sum(P.CRNOTE)AS nvarchar(250))[No. of Note/Vouchers],'''' as''_'','''' as''__'',CAST(sum(P.Invoice)AS nvarchar(250))[No. of Invoices],'''' as''___'','''' as''____'','''' as''_____'',CAST(sum(P.Total)AS nvarchar(250))[Total Note Value],'''' as''______'',CAST(sum(P.Tax)AS nvarchar(250))[Total Taxable Value] ,''''[Total Cess],'''' as''_______'' from
			(SELECT ''''Type,CAST(COUNT(S9.COLUMN04)AS int)CRNOTE,''''em1,''''em2,CAST(COUNT(a.COLUMN04)AS int) Invoice,''''em3,''''em4,''''em5, CAST((S9.COLUMN15)AS decimal(18,2))Total,''''em6,CAST((S9.COLUMN12)AS decimal(18,2))Tax ,''''em7,''''em8 FROM SATABLE005 S9 
			inner join SATABLE009 a on s9.COLUMN49=a.COLUMN02 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join SATABLE002 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			WHERE s9.column03=1330 and (s.column42 is null or s.column42 ='''') and s9.columna03='''+@ACOWNER+''' and '+@crwhereStr+'
			group by s9.column15,s9.column12
			union all
			SELECT ''''type,CAST(COUNT(s9.COLUMN04)AS int) CRNOTE,''''em1,''''em2,CAST(COUNT(a.COLUMN04)AS int)Invoice,''''em3,''''em4,''''em5, CAST((S9.COLUMN15)AS decimal(18,2))Total,''''em6,CAST((S9.COLUMN32)AS decimal(18,2))Tax ,''''em7,''''em8 FROM puTABLE001 S9
			inner join puTABLE005 a on s9.COLUMN48=a.COLUMN02 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join SATABLE001 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			 WHERE s9.column03=1355  and (s.column34 is  null or s.column34 ='''')and s9.columna03='''+@ACOWNER+''' and '+@crwhereStr+'
			 GROUP BY S.COLUMN34,a.COLUMN04,S9.COLUMN15,S9.COLUMN32
			 ) p ) UNION ALL
			SELECT ''UR Type'',''Note/Refund Voucher Number'',''Note/Refund Voucher date'',''Document Type'',''Invoice/Advance Receipt Number'',''Invoice/Advance Receipt date'',''Reason For Issuing document'',''Place Of Supply'',''Note/Refund Voucher Value'',''Rate'',''Taxable Value'',''Cess Amount'',''Pre GST'' UNION ALL
			SELECT M2.COLUMN04,S9.COLUMN04,CAST(format(S9.COLUMN06,''dd-MMM-yyyy'') AS nvarchar(250)),''C'',b.COLUMN04,CAST(format(b.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)),CAST(S9.COLUMN09 AS nvarchar(250)),CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST(a.COLUMN11 AS nvarchar(250)),CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull((isnull(a.column25,0)),0) as decimal(18,2)) AS nvarchar(250)),'''',''''  FROM SATABLE005 S9 
			inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			inner join SATABLE009 b on s9.COLUMN49=b.COLUMN02 and S9.COLUMNA02=b.COLUMNA02 and S9.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN69
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			LEFT JOIN SATABLE002 S2 ON S2.COLUMN02 = S9.COLUMN05 AND S2.COLUMNA03 = S9.COLUMNA03
			LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = S9.COLUMN41  WHERE s9.column03=1330 and (s2.column42 is null or s2.column42 ='''') and s9.columna03='''+@ACOWNER+''' and  '+@crwhereStr+'
			GROUP BY M2.COLUMN04,S9.COLUMN04,S9.COLUMN06,S9.COLUMN09,M17.COLUMN06,M17.COLUMN05,a.COLUMN25,t.COLUMN07,b.column08 ,b.column04 ,a.column11 
			union all
			SELECT '''',s9.COLUMN04,CAST(format(b.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)),''D'',b.COLUMN04,CAST(format(S9.COLUMN06,''dd-MMM-yyyy'') AS nvarchar(250)),CAST(S9.COLUMN09 AS nvarchar(250)),CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST((a.COLUMN11) AS nvarchar(250)),CAST(sum(cast(isnull(t.COLUMN17,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull((isnull(a.column24,0)),0) as decimal(18,2)) AS nvarchar(250)),'''',''''  FROM PUTABLE001 S9  
			inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			inner join PUTABLE005 b on s9.COLUMN48=b.COLUMN02 and S9.COLUMNA02=b.COLUMNA02 and S9.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
			inner join SATABLE001 s on s.COLUMN02=s9.COLUMN05 and S9.COLUMNA03=s.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S.COLUMN35
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE s9.column03=1355  and (s.column34 is  null or s.column34 ='''') and s9.columna03='''+@ACOWNER+''' and  '+@crwhereStr+'
			GROUP BY S.COLUMN34,b.COLUMN04,b.COLUMN08,S9.COLUMN04,S9.COLUMN06,S9.COLUMN09,a.COLUMN24,t.COLUMN07,a.COLUMN11,M17.COLUMN06,M17.COLUMN05

			') 
		END
	ELSE if(@TYPE='106')
		BEGIN
			 exec ('SELECT '''' as''_'','''' as''__'',CAST(sum(cast(isnull(a.COLUMN05,0) as decimal(18,2))) AS nvarchar(250))[Total Advance Received],''''[Total Cess] FROM FITABLE023 S9 
			        inner join FITABLE024 a on S9.COLUMN01=a.COLUMN09 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 WHERE s9.column03=1386 and s9.columna03='''+@ACOWNER+''' and isnull(s9.column24,0)>0 and '+@advwhereStr+' UNION ALL
					SELECT ''Place Of Supply'',''Rate'',''Gross Advance Received'',''Cess Amount'' UNION ALL
					SELECT CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull(a.COLUMN05,0) as decimal(18,2)) AS nvarchar(250)),''''  FROM FITABLE023 S9  
					LEFT JOIN SATABLE002 C  ON C.COLUMN02 = S9.COLUMN08 and S9.COLUMN20=22335 and S9.COLUMNA03=C.COLUMNA03 and isnull(C.COLUMNA13,0)=0 
					LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = C.COLUMN43 
					inner join FITABLE024 a on S9.COLUMN01=a.COLUMN09 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(S9.COLUMN12,''-'',''''))) s)) or t.COLUMN02=S9.COLUMN12) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
					WHERE  s9.column03=1386 and s9.columna03='''+@ACOWNER+''' and isnull(s9.column24,0)>0 and  '+@advwhereStr+'
					GROUP BY M17.COLUMN06,M17.COLUMN05,a.COLUMN05,S9.COLUMN24,t.COLUMN07 ')
		END
	ELSE if(@TYPE='107')
		BEGIN
			 exec ('SELECT '''' as''_'','''' as''__'',CAST(sum(cast(isnull(s9.COLUMN10,0) as decimal(18,2))-cast(isnull(S9.COLUMN19,0) as decimal(18,2))) AS nvarchar(250))[Total Advance Adjusted],''''[Total Cess] FROM FITABLE023 S9 
			        inner join FITABLE024 a on S9.COLUMN01=a.COLUMN09 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 WHERE s9.column03=1386 and s9.columna03='''+@ACOWNER+''' and isnull(s9.column24,0)>0 and '+@advwhereStr+' UNION ALL
					SELECT ''Place Of Supply'',''Rate'',''Gross Advance Adjusted'',''Cess Amount'' UNION ALL
					SELECT CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull(s9.COLUMN10,0) as decimal(18,2))-cast(isnull(S9.COLUMN19,0) as decimal(18,2)) AS nvarchar(250)),''''  FROM FITABLE023 S9  
					LEFT JOIN SATABLE002 C  ON C.COLUMN02 = S9.COLUMN08 and S9.COLUMN20=22335 and S9.COLUMNA03=C.COLUMNA03 and isnull(C.COLUMNA13,0)=0 
					LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = C.COLUMN43 
					inner join FITABLE024 a on S9.COLUMN01=a.COLUMN09 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(S9.COLUMN12,''-'',''''))) s)) or t.COLUMN02=S9.COLUMN12) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
					WHERE  s9.column03=1386 and s9.columna03='''+@ACOWNER+''' and isnull(s9.column24,0)>0 and  '+@advwhereStr+'
					GROUP BY M17.COLUMN06,M17.COLUMN05,a.COLUMN05,S9.COLUMN24,S9.COLUMN10,S9.COLUMN19,t.COLUMN07 ')
		END
	ELSE if(@TYPE='108')
		BEGIN
			 exec ('SELECT '''' as''_'',CAST(SUM(P.Total) AS nvarchar(250))[Total Nil Rated Supplies],CAST(SUM(P.GstTotal) AS nvarchar(250))[Total Exempted Supplies],CAST(SUM(P.NonGstTotal) AS nvarchar(250))[Total Non-GST Supplies] from(
					SELECT '''' [DESC],CAST(SUM(isnull(a.column10,0)*isnull(a.column35,0))AS decimal(18,2))Total,0 GstTotal,0 NonGstTotal FROM SATABLE009 S9 
					inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and (case when (isnull(a.COLUMN21,'''')='''' or a.COLUMN21=0) then 1000 else a.COLUMN21 end)=1000 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					WHERE s9.columna03='''+@ACOWNER+'''  and  '+@invwhereStr+' UNION ALL --and s9.column02=-11
					SELECT '''' [DESC],0 Total,CAST(SUM(isnull(ga.column10,0)*isnull(ga.column35,0))AS decimal(18,2))GstTotal,0 NonGstTotal FROM SATABLE009 S9 
					inner join SATABLE010 ga on S9.COLUMN01=ga.COLUMN15 and S9.COLUMN64!='''' and (case when (isnull(ga.COLUMN21,'''')='''' or ga.COLUMN21=0) then 1000 else ga.COLUMN21 end)=1000 and S9.COLUMNA02=ga.COLUMNA02 and S9.COLUMNA03=ga.COLUMNA03 and isnull(ga.COLUMNA13,0)=0 
					WHERE s9.columna03='''+@ACOWNER+'''  and  '+@invwhereStr+' UNION ALL  --and s9.column02=-11
					SELECT '''' [DESC],0 Total,0 GstTotal,CAST(SUM(isnull(na.column10,0)*isnull(na.column35,0))AS decimal(18,2))NonGstTotal FROM SATABLE009 S9 
					inner join SATABLE010 na on S9.COLUMN01=na.COLUMN15 and S9.COLUMN64=''''  and (case when (isnull(na.COLUMN21,'''')='''' or na.COLUMN21=0) then 1000 else na.COLUMN21 end)=1000 and S9.COLUMNA02=na.COLUMNA02 and S9.COLUMNA03=na.COLUMNA03 and isnull(na.COLUMNA13,0)=0 
					WHERE s9.columna03='''+@ACOWNER+'''  and  '+@invwhereStr+')P UNION ALL  --and s9.column02=-11
					SELECT ''Description'',''Nil Rated Supplies'',''Exempted (other than nil rated/non GST supply )'',''Non-GST supplies'' UNION ALL
					SELECT '''',CAST(SUM(P.Total) AS nvarchar(250)),CAST(SUM(P.GstTotal) AS nvarchar(250)),CAST(SUM(P.NonGstTotal) AS nvarchar(250)) from(
					SELECT '''' [DESC],CAST(SUM(isnull(a.column10,0)*isnull(a.column35,0))AS decimal(18,2))Total,0 GstTotal,0 NonGstTotal FROM SATABLE009 S9 
					inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and (case when (isnull(a.COLUMN21,'''')='''' or a.COLUMN21=0) then 1000 else a.COLUMN21 end)=1000 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					WHERE s9.columna03='''+@ACOWNER+'''  and  '+@invwhereStr+' UNION ALL
					SELECT '''' [DESC],0 Total,CAST(SUM(isnull(ga.column10,0)*isnull(ga.column35,0))AS decimal(18,2))GstTotal,0 NonGstTotal FROM SATABLE009 S9 
					inner join SATABLE010 ga on S9.COLUMN01=ga.COLUMN15 and S9.COLUMN64!='''' and (case when (isnull(ga.COLUMN21,'''')='''' or ga.COLUMN21=0) then 1000 else ga.COLUMN21 end)=1000 and S9.COLUMNA02=ga.COLUMNA02 and S9.COLUMNA03=ga.COLUMNA03 and isnull(ga.COLUMNA13,0)=0 
					WHERE s9.columna03='''+@ACOWNER+'''  and  '+@invwhereStr+' UNION ALL  --and s9.column02=-11
					SELECT '''' [DESC],0 Total,0 GstTotal,CAST(SUM(isnull(na.column10,0)*isnull(na.column35,0))AS decimal(18,2))NonGstTotal FROM SATABLE009 S9 
					inner join SATABLE010 na on S9.COLUMN01=na.COLUMN15 and S9.COLUMN64=''''  and (case when (isnull(na.COLUMN21,'''')='''' or na.COLUMN21=0) then 1000 else na.COLUMN21 end)=1000 and S9.COLUMNA02=na.COLUMNA02 and S9.COLUMNA03=na.COLUMNA03 and isnull(na.COLUMNA13,0)=0 
					WHERE s9.columna03='''+@ACOWNER+'''  and  '+@invwhereStr+')P ')  --and s9.column02=-11
		END
	ELSE if(@TYPE='109')
		BEGIN
			 exec ('SELECT CAST(COUNT(distinct P.COLUMN04) AS NVARCHAR(250))[No. of HSN],'''' as''_'','''' as''__'','''' as''___'',CAST(SUM(P.AMT) as NVARCHAR(250))[Total Value],CAST(SUM(P.TAX) as NVARCHAR(250))[Total Taxable Value],CAST(SUM(P.ITAX) as NVARCHAR(250))[Total Integrated Tax],CAST(SUM(P.CTAX) as NVARCHAR(250))[Total Central Tax],CAST(SUM(P.STAX) as NVARCHAR(250))[Total State/UT Tax],''''[Total Cess] FROM(
					--(SELECT M32.COLUMN04,'''' Item,'''' UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,cast(isnull(a.column14,0) + isnull(a.column23,0)  as decimal(18,2))AMT,'''' TAXRATE,(cast(isnull(cast(((isnull(a.column14,0)))as decimal(18,2)),0) as decimal(18,2))) Tax,0 CTAX,0 STAX,0 ITAX,''''em1 FROM SATABLE009 S9
					--inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					--INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 and isnull(M7.COLUMNA13,0)=0
					----LEFT join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
					----LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					--LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					--WHERE  s9.columna03='''+@ACOWNER+''' and '+@invwhereStr+' group by M32.COLUMN04,a.COLUMN35,a.COLUMN10,a.COLUMN14,a.COLUMN23)
					--UNION ALL
					--(SELECT M32.COLUMN04,'''' Item,'''' UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,0 AMT,0 TAXRATE,0 Tax,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(ct.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2))) CTAX,0 STAX,0 ITAX,''''em1 FROM SATABLE009 S9
					--inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					--INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					--INNER join MATABLE013 ct on (ct.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or ct.COLUMN02=a.COLUMN21) and ct.COLUMN16=23582 and ct.COLUMNA03=a.COLUMNA03 and isnull(ct.COLUMNA13,0)=0 
					--LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					--LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					--WHERE  s9.columna03='''+@ACOWNER+''' and '+@invwhereStr+')UNION ALL
					(SELECT M32.COLUMN04,'''' Item,'''' UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,cast(isnull(a.column14,0) + isnull(a.column23,0)  as decimal(18,2)) AMT,0 TAXRATE,(cast(isnull(cast(((isnull(a.column14,0)))as decimal(18,2)),0) as decimal(18,2))) Tax,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(st.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2))) CTAX,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(st.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2)))STAX,0 ITAX,''''em1 FROM SATABLE009 S9
					inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					INNER join MATABLE013 st on (st.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or st.COLUMN02=a.COLUMN21) and st.COLUMN16=23583 and st.COLUMNA03=a.COLUMNA03 and isnull(st.COLUMNA13,0)=0 
					LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					WHERE  s9.columna03='''+@ACOWNER+''' and isnull(s9.COLUMNA13,0)=0 and '+@invwhereStr+')UNION ALL
					(SELECT M32.COLUMN04,'''' Item,'''' UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,cast(isnull(a.column14,0) + isnull(a.column23,0)  as decimal(18,2)) AMT,0 TAXRATE,(cast(isnull(cast(((isnull(a.column14,0)))as decimal(18,2)),0) as decimal(18,2))) Tax,0 CTAX,0 STAX,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(st.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2)))ITAX,''''em1 FROM SATABLE009 S9
					inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					INNER join MATABLE013 st on (st.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or st.COLUMN02=a.COLUMN21) and st.COLUMN16=23584 and st.COLUMNA03=a.COLUMNA03 and isnull(st.COLUMNA13,0)=0 
					LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					WHERE  s9.columna03='''+@ACOWNER+''' and isnull(s9.COLUMNA13,0)=0 and '+@invwhereStr+'))P UNION ALL
					SELECT ''HSN'',''Description'',''UQC'',''Total Quantity'',''Total Value'',''Taxable Value'',''Integrated Tax Amount'',''Central Tax Amount'',''State/UT Tax Amount'',''Cess Amount'' UNION ALL	
					SELECT P.COLUMN04,max(P.Item),P.UOM,CAST(SUM(CAST(P.QTY as decimal(18,2))) as NVARCHAR(250)),CAST(SUM(CAST(P.AMT as decimal(18,2))) as NVARCHAR(250)),CAST(SUM(P.tax) as NVARCHAR(250)),CAST(SUM(P.ITAX) as NVARCHAR(250)),CAST(SUM(P.CTAX) as NVARCHAR(250)),CAST(SUM(P.STAX) as NVARCHAR(250)),'''' FROM(
					--(SELECT M32.COLUMN04,'''' Item,'''' UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,''''AMT,'''' TAXRATE,(cast(isnull(cast(((isnull(a.column35,0)))as decimal(18,2)),0) as decimal(18,2))) Tax,0 CTAX,0 STAX,0 ITAX,''''em1 FROM SATABLE009 S9
					--inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					--INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					----LEFT join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
					----LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					--LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					--WHERE  s9.columna03='''+@ACOWNER+''' and a.COLUMN21 !=1000 and '+@invwhereStr+' )
					--UNION ALL
					--(SELECT M32.COLUMN04,M7.COLUMN04 Item,M2.COLUMN04 UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,CAST(cast(isnull(a.column10,0)*isnull(a.column14,0) as decimal(18,2)) AS nvarchar(250))AMT,0 TAXRATE,0 Tax,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(ct.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2))) CTAX,0 STAX,0 ITAX,''''em1 FROM SATABLE009 S9
					--inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					--INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					--INNER join MATABLE013 ct on (ct.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or ct.COLUMN02=a.COLUMN21) and ct.COLUMN16=23582 and ct.COLUMNA03=a.COLUMNA03 and isnull(ct.COLUMNA13,0)=0 
					--LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					--LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					--WHERE  s9.columna03='''+@ACOWNER+''' and '+@invwhereStr+')UNION ALL
					(SELECT M32.COLUMN04,iif(M7.COLUMN50 ='''',M7.COLUMN04,M7.COLUMN50) Item,M2.COLUMN04 UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,cast(isnull(a.column14,0) + isnull(a.column23,0)  as decimal(18,2))AMT,0 TAXRATE,(cast(isnull(cast(((isnull(a.column14,0)))as decimal(18,2)),0) as decimal(18,2))) Tax,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(st.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2))) CTAX,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(st.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2)))STAX,0 ITAX,''''em1 FROM SATABLE009 S9
					inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					INNER join MATABLE013 st on (st.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or st.COLUMN02=a.COLUMN21) and st.COLUMN16=23583 and st.COLUMNA03=a.COLUMNA03 and isnull(st.COLUMNA13,0)=0 
					LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					WHERE  s9.columna03='''+@ACOWNER+''' and isnull(s9.COLUMNA13,0)=0 and '+@invwhereStr+')UNION ALL
					(SELECT M32.COLUMN04,iif(M7.COLUMN50 ='''',M7.COLUMN04,M7.COLUMN50) Item,M2.COLUMN04 UOM,CAST(CAST((a.COLUMN10)AS DECIMAL(18,2)) AS NVARCHAR(250))QTY,cast(isnull(a.column14,0) + isnull(a.column23,0)  as decimal(18,2))AMT,0 TAXRATE,(cast(isnull(cast(((isnull(a.column14,0)))as decimal(18,2)),0) as decimal(18,2))) Tax,0 CTAX,0 STAX,(cast(isnull(cast(((isnull(a.column10,0)*isnull(a.column35,0))*(cast(st.COLUMN07 as decimal(18,2)))/100)as decimal(18,2)),0) as decimal(18,2)))ITAX,''''em1 FROM SATABLE009 S9
					inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
					INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = S9.COLUMNA03 --and isnull(M7.COLUMNA13,0)=0
					INNER join MATABLE013 st on (st.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or st.COLUMN02=a.COLUMN21) and st.COLUMN16=23584 and st.COLUMNA03=a.COLUMNA03 and isnull(st.COLUMNA13,0)=0 
					LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = a.COLUMN22 
					LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 
					WHERE  s9.columna03='''+@ACOWNER+''' and isnull(s9.COLUMNA13,0)=0 and '+@invwhereStr+'))P GROUP BY P.COLUMN04,P.UOM
					 ')
		END
	else if(@TYPE='100')
		BEGIN
			exec ('SELECT cast(sum(p.[No. of Recipients]) as nvarchar(250))''No. of Recipients'',cast(sum(p.[No. of Invoices]) as nvarchar(250))''No. of Invoices'','''' as''_'', cast(sum(p.[Total Invoice Value]) as nvarchar(250))''Total Invoice Value'','''' as''__'','''' as''___'','''' as''____'','''' as''_____'','''' as''______'',cast(sum(p.[Total Taxable Value]) as nvarchar(250))''Total Taxable Value'' ,''''[Total Cess] FROM(
 SELECT  CAST(COUNT(DISTINCT S9.COLUMN64)AS int)[No. of Recipients],CAST(COUNT(distinct S9.COLUMN04)AS int)[No. of Invoices],'''' as''_'', CAST(SUM(a.COLUMN23+a.column14)AS decimal(18,2))[Total Invoice Value],'''' as''__'','''' as''___'','''' as''____'','''' as''_____'','''' as''______'',CAST((SUM(a.COLUMN14))AS decimal(18,2))[Total Taxable Value] ,''''[Total Cess] FROM SATABLE009 S9 inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 WHERE s9.columna03='''+@ACOWNER+''' and s9.column64 is not null and s9.column64 !='''' and s9.column24 >0 and isnull(s9.COLUMNA13,0)=0 and  '+@invwhereStr+' UNION ALL
			SELECT  CAST(0 AS int)[No. of Recipients],0 [No. of Invoices],'''' as''_'', CAST(SUM(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)+isnull(S9.COLUMN72,0))AS decimal(18,2))[Total Invoice Value],'''' as''__'','''' as''___'','''' as''____'','''' as''_____'','''' as''______'',CAST((SUM(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)))AS decimal(18,2))[Total Taxable Value] ,''''[Total Cess] FROM SATABLE009 S9 WHERE s9.columna03='''+@ACOWNER+''' and s9.column64 is not null and s9.column64 !='''' and s9.column24 >0 and isnull(s9.COLUMNA13,0)=0 and  '+@invwhereStr+')p  UNION ALL
			
			SELECT ''GSTIN/UIN of Recipient'',''Invoice Number'',''Invoice date'',''Invoice Value'',''Place Of Supply'',''Reverse Charge'',''Invoice Type'',''E-Commerce GSTIN'',''Rate'',''Taxable Value'',''Cess Amount''  UNION ALL
			SELECT p.[1],p.[2],p.[3],cast(sum(cast(p.[4] as decimal(18,2)))as nvarchar(25)),p.[5],p.[6],p.[7],p.[8],cast((cast(p.[9] as decimal(18,2)))as nvarchar(25)),cast(sum(cast(p.[10] as decimal(18,2)))as nvarchar(25)),p.[11]  FROM(  
			SELECT S9.COLUMN64 [1],S9.COLUMN04 [2],CAST(format(S9.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)) [3],CAST(cast(SUM(a.COLUMN23+a.column14) as decimal(18,2)) AS nvarchar(250)) [4],CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)) [5],CAST((case when (S9.COLUMN67=''1'' or S9.COLUMN67=''True'') then ''Y'' else ''N'' end) AS nvarchar(250)) [6],''Regular'' [7],'''' [8],CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)) [9],CAST(cast(isnull((isnull(a.column14,0)),0) as decimal(18,2)) AS nvarchar(250)) [10],'''' [11]  FROM SATABLE009 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN66 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			--left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			left join (SELECT SUM(CAST(ISNULL(COLUMN07,0) AS decimal(18,2))) COLUMN07,COLUMN02,COLUMNA03 FROM MATABLE013 where isnull(COLUMNA13,0)=0 and columna03='''+@ACOWNER+''' GROUP BY COLUMNA03,COLUMN02
			Union all SELECT SUM(CAST(ISNULL(a.COLUMN07,0) AS decimal(18,2))) COLUMN07,b.COLUMN02,b.COLUMNA03 FROM MATABLE013 A INNER JOIN MATABLE014 B ON a.COLUMN02 in ((SELECT ListValue FROM dbo.FN_ListToTable('','',(b.COLUMN05)) s)) and b.column05 !='''' and b.columna03=a.COLUMNA03 and 
			isnull(b.COLUMNA13,0)=0 and isnull(a.COLUMNA13,0)=0 where b.columna03='''+@ACOWNER+''' GROUP BY b.COLUMNA03,b.COLUMN02) t on t.COLUMN02=replace(a.COLUMN21,''-'','''') and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=s9.COLUMNA03

			WHERE  s9.columna03='''+@ACOWNER+''' and isnull(s9.COLUMNA13,0)=0 and s9.column64 is not null and s9.column64 !='''' and s9.column24 >0 and  '+@invwhereStr+'
			GROUP BY S9.COLUMN08,S9.COLUMN04,S9.COLUMN64,S9.COLUMN20,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,a.COLUMN02,a.COLUMN14
			UNION ALL
			SELECT S9.COLUMN64 [1],S9.COLUMN04 [2],CAST(format(S9.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)) [3],CAST(SUM(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)+isnull(S9.COLUMN72,0)) AS nvarchar(250)) [4],CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)) [5],CAST((case when (S9.COLUMN67=''1'' or S9.COLUMN67=''True'') then ''Y'' else ''N'' end) AS nvarchar(250)) [6],''Regular'' [7],'''' [8],CAST(sum(isnull(t.COLUMN07,0)) AS nvarchar(250)) [9],CAST(SUM(isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0)) AS nvarchar(250)) [10],'''' [11]  FROM SATABLE009 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN66 
			--inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			--left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(s9.COLUMN69,''-'',''''))) s)) or t.COLUMN02=s9.COLUMN69) and isnull(s9.COLUMN72,0)!=0 and t.COLUMNA03=s9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			left join (SELECT SUM(CAST(ISNULL(COLUMN07,0) AS decimal(18,2))) COLUMN07,COLUMN02,COLUMNA03 FROM MATABLE013 where isnull(COLUMNA13,0)=0 and columna03='''+@ACOWNER+''' GROUP BY COLUMNA03,COLUMN02
			Union all SELECT SUM(CAST(ISNULL(a.COLUMN07,0) AS decimal(18,2))) COLUMN07,b.COLUMN02,b.COLUMNA03 FROM MATABLE013 A INNER JOIN MATABLE014 B ON a.COLUMN02 in ((SELECT ListValue FROM dbo.FN_ListToTable('','',(b.COLUMN05)) s)) and b.column05 !='''' and b.columna03=a.COLUMNA03 and 
			isnull(b.COLUMNA13,0)=0 and isnull(a.COLUMNA13,0)=0 where b.columna03='''+@ACOWNER+''' GROUP BY b.COLUMNA03,b.COLUMN02) t on t.COLUMN02=replace(s9.COLUMN69,''-'','''') and isnull(s9.COLUMN72,0)!=0 and t.COLUMNA03=s9.COLUMNA03
			WHERE  s9.columna03='''+@ACOWNER+''' and (isnull(S9.COLUMN35,0)+isnull(S9.COLUMN36,0)+isnull(S9.COLUMN44,0))!=0 and isnull(s9.COLUMNA13,0)=0 and isnull(s9.column65,0) = 23601 and s9.column24 >0 and  '+@invwhereStr+'
			GROUP BY S9.COLUMN08,S9.COLUMN04,S9.COLUMN64,S9.COLUMN20,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,S9.COLUMN72
			)p group by p.[3],p.[2],p.[1],p.[5],p.[6],p.[7],p.[8],p.[11],p.[9]') 
		END
	else if(@TYPE='101')
		BEGIN
			exec ('SELECT  CAST(COUNT(S9.COLUMN04)AS nvarchar(250))[No. of Invoices],'''' as''_'', CAST(SUM(S9.COLUMN20)AS nvarchar(250))[Total Invoice Value],'''' as''__'','''' as''___'',CAST((SUM(a.COLUMN14))AS nvarchar(250))[Total Taxable Value] ,''''[Total Cess],'''' as''____'' FROM SATABLE009 S9 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE s9.columna03='''+@ACOWNER+'''  and  s9.column03=1277 and (s9.COLUMN65=23602 or isnull(s9.COLUMN65,0)=0)  and s9.COLUMN20>250000 and t.COLUMN16 = 23584 and s9.column24 >0 and  '+@invwhereStr+' UNION ALL
			SELECT ''Invoice Number'',''Invoice date'',''Invoice Value'',''Place Of Supply'',''Rate'',''Taxable Value'',''Cess Amount'',''E-Commerce GSTIN''  UNION ALL
			SELECT p.[1],p.[2],p.[3],p.[4],p.[5],cast(sum(cast(p.[6] as decimal(18,2)))as nvarchar(25)),p.[7],p.[8]  FROM ( 
			SELECT S9.COLUMN04 [1],CAST(format(S9.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250))[2],CAST(S9.COLUMN20 AS nvarchar(250))[3],CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250))[4],CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250))[5],CAST(cast(isnull((isnull(a.column14,0)),0) as decimal(18,2)) AS nvarchar(250))[6],''''[7],''''[8]  FROM SATABLE009 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN66 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE  s9.columna03='''+@ACOWNER+''' and  s9.column03=1277 and (s9.COLUMN65=23602 or isnull(s9.COLUMN65,0)=0)  and s9.COLUMN20>250000 and t.COLUMN16 = 23584 and s9.column24 >0 and  '+@invwhereStr+'
			GROUP BY S9.COLUMN64,S9.COLUMN04,S9.COLUMN08,S9.COLUMN20,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,t.COLUMN07,a.COLUMN02,a.COLUMN14
			)p group by p.[1],p.[2],p.[3],p.[4],p.[5],p.[7],p.[8]') 
		END
	else if(@TYPE='102')
		BEGIN
			exec ('
			SELECT  '''' as''_'','''' as''__'','''' as''___'',cast(sum(cast(p.[Total Taxable Value] as decimal(18,2)))as nvarchar(25))[Total Taxable Value] ,''''[Total Cess],'''' as''____'' FROM ( 
			SELECT  '''' as''_'','''' as''__'','''' as''___'',CAST(((a.COLUMN14))AS nvarchar(250))[Total Taxable Value] ,''''[Total Cess],'''' as''____'' FROM SATABLE009 S9 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE s9.columna03='''+@ACOWNER+'''  and s9.column03=1277 and (s9.COLUMN65=23602 or isnull(s9.COLUMN65,0)=0) and s9.COLUMN20 <= iif( t.COLUMN16 = 23584,250000,s9.COLUMN20)  and s9.column24 >0 and  '+@invwhereStr+'
			group by s9.column22,s9.column25,a.column21,a.column02,a.column14)p  UNION ALL
			SELECT ''Type'',''Place Of Supply'',''Rate'',''Taxable Value'',''Cess Amount'',''E-Commerce GSTIN''  UNION ALL
			SELECT p.[1],p.[2],p.[3],cast(sum(cast(p.[4] as decimal(18,2)))as nvarchar(25)),p.[5],p.[6]  FROM(  
			SELECT ''''[1],CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250))[2],CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250))[3],CAST(cast(isnull((isnull(a.column14,0)),0) as decimal(18,2)) AS nvarchar(250))[4],''''[5],''''[6]  FROM SATABLE009 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN66 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE  s9.columna03='''+@ACOWNER+''' and s9.column03=1277 and (s9.COLUMN65=23602 or isnull(s9.COLUMN65,0)=0) and s9.COLUMN20 <= iif( t.COLUMN16 = 23584,250000,s9.COLUMN20)  and s9.column24 >0 and  '+@invwhereStr+'
			GROUP BY S9.COLUMN64,S9.COLUMN04,S9.COLUMN08,S9.COLUMN20,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,t.COLUMN07,a.COLUMN02,a.COLUMN14
			)p group by p.[1],p.[2],p.[3],p.[5],p.[6]') 
		END
	else if(@TYPE='105')
		BEGIN
			exec ('SELECT  '''' as''_'',CAST(COUNT(S9.COLUMN04)AS nvarchar(250))[No. of Invoices],'''' as''__'', CAST(SUM(S9.COLUMN20)AS nvarchar(250))[Total Invoice Value],'''' as''___'',''''[No. Shipping Bill],'''' as''____'','''' as''_____'',CAST((SUM(S9.COLUMN22)-SUM(S9.COLUMN25))AS nvarchar(250))[Total Taxable Value]  FROM SATABLE009 S9 WHERE s9.columna03='''+@ACOWNER+'''   and  '+@invwhereStr+' UNION ALL  --and s9.column02=-11
			SELECT ''Export Type'',''Invoice Number'',''Invoice date'',''Invoice Value'',''Port Code'',''Shipping Bill Number'',''Shipping Bill Date'',''Rate'',''Taxable Value''  UNION ALL
			SELECT '''',S9.COLUMN04,CAST(format(S9.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)),CAST(S9.COLUMN20 AS nvarchar(250)),'''','''','''',CAST(sum(cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull(SUM(isnull(a.column14,0)),0) as decimal(18,2)) AS nvarchar(250)) FROM SATABLE009 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN66 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE  s9.columna03='''+@ACOWNER+''' and t.COLUMN16=23584  and   '+@invwhereStr+'  --and s9.column02=-11
			GROUP BY S9.COLUMN64,S9.COLUMN04,S9.COLUMN08,S9.COLUMN20,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,t.COLUMN07') 
		END
	else if(@TYPE='110')
		BEGIN
			exec ('SELECT  '''' as''_'','''' as''__'','''' as''___'',CAST(SUM(A.[Total Numbers]) AS nvarchar(250))[Total Numbers] ,CAST(SUM(A.[Total Cancelled]) AS nvarchar(250))[Total Cancelled] FROM (
			SELECT ''''A,''''B,''''C,count(column04)[Total Numbers],0[Total Cancelled] FROM SATABLE009 S9 where  s9.columna03='''+@ACOWNER+''' and isnull(s9.columna13,0) = 0 and  '+@invwhereStr+'   UNION ALL
			SELECT '''','''','''',count(column04)[Total Numbers],0[Total Cancelled] FROM PUTABLE001 S9 where  s9.columna03='''+@ACOWNER+'''  and isnull(s9.columna13,0) = 0  and  S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)and s9.column03 = 1355  and s9.column06 between  '''+@FROMDT+''' AND '''+@TODT+''' UNION ALL
			SELECT '''','''','''',count(column04) ,0 FROM PUTABLE005 S9 where  s9.columna03='''+@ACOWNER+''' and isnull(s9.columna13,0) = 0  and (s9.column55 is null or s9.column55='''') and  '+@invwhereStr+'  
			)A
			UNION ALL
			SELECT ''Nature of Document'',''Sr. No. From'',''Sr. No. To'',''Total Number'',''Cancelled''  UNION ALL
			SELECT ''Invoice for outward supply'',CAST(min(column04) AS nvarchar(250)),CAST(max(column04) AS nvarchar(250)),CAST(count(column04) AS nvarchar(250)),''0'' FROM SATABLE009 S9 where  s9.columna03='''+@ACOWNER+'''  and isnull(s9.columna13,0) = 0  and  '+@invwhereStr+'   UNION ALL
			SELECT ''Debit Note'',CAST(min(column04) AS nvarchar(250)),CAST(max(column04) AS nvarchar(250)),CAST(count(column04) AS nvarchar(250)),''0'' FROM PUTABLE001 S9 where  s9.columna03='''+@ACOWNER+'''  and isnull(s9.columna13,0) = 0  and  S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)and s9.column03 = 1355  and s9.column06 between  '''+@FROMDT+''' AND '''+@TODT+'''  UNION ALL
			SELECT ''Delivery Challan for job work'','''','''','''',''''  UNION ALL
			SELECT ''Invoice for inward supply from unregistered person'',CAST(min(column04) AS nvarchar(250)),CAST(max(column04) AS nvarchar(250)),CAST(count(column04) AS nvarchar(250)),''0'' FROM PUTABLE005 S9 where  s9.columna03='''+@ACOWNER+'''  and isnull(s9.columna13,0) = 0  and (s9.column55 is null or s9.column55='''') and  '+@invwhereStr+'   UNION ALL
			SELECT ''Invoice for inward supply from unregistered person'','''','''','''',''''  UNION ALL
			SELECT ''Refund Voucher'','''','''','''',''''  --UNION ALL
			--SELECT ''Invoice for outward supply'','''','''','''',''''
			') 
		END
	else if(@TYPE='111')
		BEGIN
			exec ('select ''''[UQC],''''[Export Type],''''[Reverse Charge Provisional Assessment],''''[Note Type],''''[Type],''''[Tax Rate],''''[POS],''''[Invoice Type],''''[Reason For Issuing Note],''''[Nature of Document],''''[UR Type] union all
					select '''','''','''','''','''','''','''','''','''','''','''' 
					') 
		END
	else
		BEGIN
			exec ('SELECT  CAST(COUNT(DISTINCT S9.COLUMN64)AS nvarchar(250))[No. of Recipients],CAST(COUNT(S9.COLUMN04)AS nvarchar(250))[No. of Invoices],'''' as''_'', CAST(SUM(S9.COLUMN20)AS nvarchar(250))[Total Invoice Value],'''' as''__'','''' as''___'','''' as''____'','''' as''_____'','''' as''______'',CAST(SUM(S9.COLUMN24)AS nvarchar(250))[Total Taxable Value] ,''''[Total Cess],'''' as''_______'','''' as''________'','''' as''_________'','''' as''__________'' FROM SATABLE009 S9 WHERE s9.columna03='''+@ACOWNER+''' and  '+@invwhereStr+' UNION ALL
			SELECT ''GSTIN/UIN of Recipient'',''Invoice Number'',''Invoice date'',''Invoice Value'',''Place Of Supply'',''Reverse Charge'',''Invoice Type'',''E-Commerce GSTIN'',''Rate'',''Taxable Value'',''Cess Amount'',''Export Type'',''Port Code'',''Shipping Bill Number'',''Shipping Bill Date'' UNION ALL
			SELECT S9.COLUMN64,S9.COLUMN04,CAST(format(S9.COLUMN08,''dd-MMM-yyyy'') AS nvarchar(250)),CAST(S9.COLUMN20 AS nvarchar(250)),CAST((M17.COLUMN06+''-''+M17.COLUMN05) AS nvarchar(250)),CAST((case when (S9.COLUMN67=''1'' or S9.COLUMN67=''True'') then ''Y'' else ''N'' end) AS nvarchar(250)),''Regular'',CAST(S9.COLUMN64 AS nvarchar(250)),CAST((cast(isnull(t.COLUMN07,0) as decimal(18,2))) AS nvarchar(250)),CAST(cast(isnull(cast((SUM(isnull(a.column10,0)*isnull(a.column35,0))*t.COLUMN07/100)as decimal(18,2)),0) as decimal(18,2)) AS nvarchar(250)),'''','''','''','''',''''  FROM SATABLE009 S9  
			LEFT JOIN MATABLE017 M17  ON M17.COLUMN02 = S9.COLUMN66 
			inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
			left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
			WHERE  s9.columna03='''+@ACOWNER+''' and  '+@invwhereStr+'
			GROUP BY S9.COLUMN64,S9.COLUMN04,S9.COLUMN08,S9.COLUMN20,M17.COLUMN06,M17.COLUMN05,S9.COLUMN67,t.COLUMN07') 
		END
END

GO
