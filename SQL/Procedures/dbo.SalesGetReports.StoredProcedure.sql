USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[SalesGetReports]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SalesGetReports]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@DateF nvarchar(250)=null
)
 
as 
begin
IF  @ReportName='SalesByCustomer'
BEGIN
--EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

select * into #SalesByCustomer from (


SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,--a.COLUMN20 amt,
FORMAT(a.COLUMN08, @DateF)  dt,a.COLUMN08 Dtt,
 (select COLUMN04 from contable0010 where column02=1277) TrnsType,(select COLUMN04 from matable007 where column02=b.COLUMN05) itm ,
  a.COLUMN04 num ,  b.COLUMN06 descc,b.COLUMN10 qty ,b.COLUMN13 price  
  ,(b.COLUMN14) amont,(b.COLUMN23) taxamont ,(F.COLUMN04) LOT,M7.COLUMN10 BrandID
  --EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
  ,m5.COLUMN04 as Brand ,a.COLUMNA02 OPID ,a.COLUMN05 CID,a.COLUMN03 fid
   , a.COLUMN05 c , @FromDate fd , @ToDate td 
  --EMPHCS1558 : SalesByCustomerSummary & SalesByCustomerDetails Reports By GNANESHWAR ON 9/2/2016
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 and isnull(b.columna13,0)=0 left outer JOIN FITABLE043 F ON F.COLUMN02=b.COLUMN31 AND b.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0 
 left outer JOIN MATABLE007 M7 ON M7.COLUMN02=b.COLUMN05 AND b.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	 
left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=b.COLUMN05  and isnull(m5.COLUMNA13,0)=0  where isnull((a.COLUMNA13),0)=0 and  a.COLUMN19=1002 and
 a.COLUMN08 between @FromDate and @ToDate  AND   a.COLUMNA03=@AcOwner



	) Query 

set @Query1='select ou,customer,sono,dt,Dtt,TrnsType, descc,LOT ,Brand,BrandID,sum(amont)amont,sum(taxamont)taxamont,num,fid ,c ,  fd ,  td  from #SalesByCustomer'+@whereStr +' group by ou,customer,sono,dt,Dtt,TrnsType, BrandID,descc,LOT ,Brand,num,fid,c ,  fd ,  td order by Dtt desc'
exec (@Query1)


end

ELSE IF @ReportName='SalesByProductDetails'
	BEGIN
		IF(@OperatingUnit!='')

				 if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end 
         if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

			select * into #SalesByProductDetails from (
				SELECT M7.COLUMN04 ITEM,C7.COLUMN03 [OPERATING UNIT],FORMAT((S9.COLUMN08),@DateF) [DATE],S9.COLUMN08 Dtt,'Invoice'[TRANSACTION TYPE],(S9.COLUMN04) [NO],(S2.COLUMN05) [CLIENT],(S9.COLUMN12) [MEMO/DESCREPTION],(S10.COLUMN10) [QTY],(S10.COLUMN13) [RATE],(S10.COLUMN14) AMOUNT  ,(F.COLUMN04) LOT
		--EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
				,M7.COLUMN10 BrandID,m5.COLUMN04 as Brand ,S10.COLUMNA02 OPID  ,S9.COLUMN03 fid  FROM SATABLE010 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
				INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN SATABLE002 S2 ON S2.COLUMN02=S9.COLUMN05    AND S10.COLUMNA03=S2.COLUMNA03 AND ISNULL(S2.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				left outer JOIN FITABLE043 F ON F.COLUMN02=S10.COLUMN31 AND S10.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0  
				 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
				WHERE  S9.COLUMN08 BETWEEN @FromDate and @ToDate AND S10.COLUMNA03=@AcOwner AND ISNULL(S10.COLUMNA13,0)=0
			) Query


		   set @Query1='select * from #SalesByProductDetails'+@whereStr+' order by Dtt desc'
exec (@Query1) 

		
	END

ELSE IF @ReportName='SalesByProductSummary'
	BEGIN
		IF(@OperatingUnit!='')

				 if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end 
         if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

				DECLARE @PERECENT DECIMAL(18,2)
				SET @PERECENT=(SELECT SUM(S10.COLUMN14) FROM SATABLE010 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05 AND S10.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	
				INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				WHERE  S9.COLUMN08 BETWEEN @FromDate and @ToDate AND (S10.COLUMNA02=@OperatingUnit or S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) )AND S10.COLUMNA03=@AcOwner AND ISNULL(S10.COLUMNA13,0)=0)
				
				select * into #SalesByProductSummary from (
				SELECT M7.COLUMN04 ITEM,C7.COLUMN04 [OPERATING UNIT],SUM(S10.COLUMN10) QTY,SUM(S10.COLUMN14) AMOUNT,CAST((100/@PERECENT)*(SUM(S10.COLUMN14)) AS DECIMAL(18,4)) [% OF SALE],CAST(SUM(S10.COLUMN14)/SUM(S10.COLUMN10)AS DECIMAL(18,2)) [AVG PRICE],@PERECENT TOTAL 
				,M7.COLUMN10 BrandID,m5.COLUMN04 as Brand ,S10.COLUMNA02 OPID   ,s9.COLUMN03 fid, null [NO]  FROM SATABLE010 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05 AND S10.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	
				INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
				WHERE   S9.COLUMN08 BETWEEN @FromDate and @ToDate AND (S10.COLUMNA02=@OperatingUnit or S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) )AND S10.COLUMNA03=@AcOwner AND ISNULL(S10.COLUMNA13,0)=0
				GROUP BY S9.COLUMN03, M7.COLUMN04,C7.COLUMN04,m5.COLUMN04,M7.COLUMN10 ,S10.COLUMNA02 HAVING SUM(ISNULL(S10.COLUMN14,0))>0
				) Query

set @Query1='select * from #SalesByProductSummary'+@whereStr
exec (@Query1) 

		
	END

end






GO
