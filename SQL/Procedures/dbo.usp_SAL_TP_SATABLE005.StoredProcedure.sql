USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE005]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_SATABLE005]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,
	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,
	--EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,

	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN43   nvarchar(250)=null,
	@COLUMN44   nvarchar(250)=null,  @COLUMN45   nvarchar(250)=null,  @COLUMN46   nvarchar(250)=null,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	 @COLUMN47   nvarchar(250)=null, @COLUMN48   nvarchar(250)=null,  @COLUMN50   nvarchar(250)=null,
        --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
	@COLUMN51   nvarchar(250)=null,  @COLUMN52   nvarchar(250)=null,   @COLUMN54   nvarchar(250)=null,
	@COLUMN55   nvarchar(250)=null,  @COLUMN56   nvarchar(250)=null,  @COLUMN57   nvarchar(250)=null,
	@COLUMN58   nvarchar(250)=null,  @COLUMN59   nvarchar(250)=null,  @COLUMN60   nvarchar(250)=null,
	@COLUMN67   nvarchar(250)=null,  @COLUMN68   nvarchar(250)=null,  @COLUMN69   nvarchar(250)=null,
	@COLUMN70   nvarchar(250)=null,  @COLUMN71   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@TabelName  nvarchar(250)=null,  @ReturnValue  int=null OUTPUT 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications 
set @COLUMN29=(1002)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE005_SequenceNo
insert into SATABLE005 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,  
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,
   COLUMN35,   COLUMN36,  COLUMN37,  COLUMN38,  COLUMN39,  COLUMN40,  COLUMN41, COLUMN42,  COLUMN43,  COLUMN44,  COLUMN45,
    --EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
   COLUMN46,  COLUMN47,  COLUMN48,  COLUMN50,  COLUMN51,  COLUMN52,  COLUMN54,  
   COLUMN59,  COLUMN60,  COLUMN67,  COLUMN68,  COLUMN69,  COLUMN70,  COLUMN71,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   --EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
   @COLUMN42,  @COLUMN43,  @COLUMN44,  @COLUMN45,  @COLUMN46,  @COLUMN47,  @COLUMN48,  @COLUMN50,  @COLUMN51,  @COLUMN52,
   @COLUMN54,  @COLUMN59,  @COLUMN60,  @COLUMN67,  @COLUMN68,  @COLUMN69,  @COLUMN70,  @COLUMN71,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
update SATABLE013 set COLUMN06=23088 where COLUMN02=@COLUMN54 and COLUMNA03=@COLUMNA03 and ISNULL(COLUMNA13,0)=0
set @ReturnValue =(Select COLUMN01 from SATABLE005 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from SATABLE005

END 

 

ELSE IF @Direction = 'Update'

BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications 
set @COLUMN29=(1002)
declare @location nvarchar(250),@linelocation nvarchar(250),@Project nvarchar(250)
set @location=(select COLUMN48 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @location=(iif(@location='',0,isnull(@location,0)))
set @Project = (select COLUMN35 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)

UPDATE SATABLE005 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,  
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,  
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,
   COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,
   COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,  
   --EMPHCS743 - GNANESHWAR 21/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
   COLUMN36=@COLUMN36,    COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,    COLUMN39=@COLUMN39,    COLUMN40=@COLUMN40, 
   	--EMPHCS1281 Packing and Shipping Charges Calculation in Purchase Order and Bill  and capturing values in  delivery & Package BY RAJ.Jr 7/10/2015
   COLUMN41=@COLUMN41,    COLUMN42=@COLUMN42,    COLUMN43=@COLUMN43,    COLUMN44=@COLUMN44,    COLUMN45=@COLUMN45,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
   COLUMN46=@COLUMN46,    COLUMN47=@COLUMN47,    COLUMN48=@COLUMN48,    COLUMN50=@COLUMN50,    COLUMN51=@COLUMN51,  
   COLUMN52=@COLUMN52,    COLUMN54=@COLUMN54,    COLUMN59=@COLUMN59,    COLUMN60=@COLUMN60,   COLUMN67=@COLUMN67,  
   COLUMN68=@COLUMN68,    COLUMN69=@COLUMN69,    COLUMN70=@COLUMN70,    COLUMN71=@COLUMN71, 
   COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05, 
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
   
--EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
--UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN19 in( select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02 )
--EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
 declare @SOID nvarchar(250),@Qty nvarchar(250),@Item nvarchar(250),@ItemQty nvarchar(250),@id nvarchar(250),@uom nvarchar(250),@lot nvarchar(250)
 DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
      set @SOID=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
      --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE006 where COLUMN19=@SOID and COLUMNA13=0
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
	  --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE006 where COLUMN19=@SOID and COLUMNA13=0)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
	         set @uom=(select isnull(COLUMN27,10000) from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
	         set @lot=(select COLUMN17 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 set @lot=(iif(@lot='',0,isnull(@lot,0)))
			 set @linelocation=(select cast(COLUMN38 as nvarchar(250)) from SATABLE006 where COLUMN02=@id)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
			 set @uom=(iif(@uom='',10000,isnull(@uom,10000)))
             set @ItemQty=(select isnull(COLUMN07,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
             set @Qty=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN24 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04) 
		     	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
			 declare @TrackQty bit
			 set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
			if(@TrackQty=1)
			begin
			 UPDATE FITABLE010 SET COLUMN05=cast(@Qty as decimal(18,2))-cast(@ItemQty as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN24 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04
			 end
		     --EMPHCS650  rajasekhar reddy patakota 21/8/2015 All Screens Delete and updates not working
			 UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN02= @id  
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
		END
  CLOSE cur1 
if  exists( SELECT 1 FROM PUTABLE013 WHERE COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02))
begin
delete from PUTABLE013  where COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
end

set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

UPDATE SATABLE005 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS879 After deletion of item issue , sysetm is not reducing the inventory asset value done by srinivas 8/8/2015
UPDATE SATABLE006 SET COLUMNA13=1 WHERE COLUMN19 in( select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02 )
set @location=(select COLUMN48 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @location=(iif(@location='',0,isnull(@location,0)))
 set @COLUMN24=(select COLUMN24 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @COLUMN04=(select COLUMN04 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
 set @Project = (select COLUMN35 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
set @Project=(case when @Project='' then 0 when isnull(@Project,0)=0 then 0  else @Project end)

      set @Initialrow =1
      set @SOID=(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from SATABLE006 where COLUMN19=@SOID
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM SATABLE006 where COLUMN19=@SOID)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
             set @ItemQty=(select isnull(COLUMN07,0) from SATABLE006 where COLUMN02=@id and COLUMN03=@Item and COLUMN19=@SOID)
             --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order
	         set @uom=(select isnull(COLUMN27,10000) from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
	         set @lot=(select COLUMN17 from SATABLE006 where COLUMN02=@id and COLUMN19=@SOID)
			 set @lot=(iif(@lot='',0,isnull(@lot,0)))
			 set @linelocation=(select cast(COLUMN38 as nvarchar(250)) from SATABLE006 where COLUMN02=@id)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
			 set @uom=(iif(@uom='',10000,isnull(@uom,10000)))
             set @Qty=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN24 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04)
			 	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
			 set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
			if(@TrackQty=1)
			begin
			 UPDATE FITABLE010 SET COLUMN05=cast(@Qty as decimal(18,2))-cast(@ItemQty as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN24 AND COLUMN19=@uom AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lot,0) AND isnull(COLUMN23,0)=isnull(@Project,0) and COLUMN24=@COLUMN04
			 end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
		END
  CLOSE cur1 
if  exists( SELECT 1 FROM PUTABLE013 WHERE COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02))
begin
delete from PUTABLE013  where COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from SATABLE005 WHERE COLUMN02 = @COLUMN02)
end
END

end try
begin catch
	declare @tempSTR nvarchar(max)	
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_SAL_TP_SATABLE005.txt',0
return 0
end catch
end












GO
