USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE008 ]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE008 ]
 (
 @COLUMN02    NVARCHAR(250),       @COLUMN03    NVARCHAR(250)=NULL,  @COLUMN04    NVARCHAR(250)=NULL, 
 @COLUMN05    NVARCHAR(250)=NULL,  @COLUMN06    NVARCHAR(250)=NULL,  @COLUMN07    NVARCHAR(250)=NULL, 
 @COLUMN08    NVARCHAR(250)=NULL,  @COLUMN09    NVARCHAR(250)=NULL,  @COLUMN10    NVARCHAR(250)=NULL, 
 @COLUMN11    NVARCHAR(250)=NULL,  @COLUMN12    NVARCHAR(250)=NULL,  @COLUMN13    NVARCHAR(250)=NULL, 
 @COLUMN14    NVARCHAR(250)=NULL,  @COLUMN15    NVARCHAR(250)=NULL,  @COLUMN16    NVARCHAR(250)=NULL, 
 @COLUMN17    NVARCHAR(250)=NULL,  @COLUMN18    NVARCHAR(250)=NULL,  @COLUMN19    NVARCHAR(250)=NULL, 
 --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
 @COLUMN20    NVARCHAR(250)=NULL,  @COLUMN21    NVARCHAR(250)=NULL,  @COLUMN22    NVARCHAR(250)=NULL,
 @COLUMN23    NVARCHAR(250)=NULL,   @COLUMN24    NVARCHAR(250)=NULL, 
 @COLUMNA01   VARCHAR(100)=NULL,   @COLUMNA02   VARCHAR(100)=NULL,   @COLUMNA03   VARCHAR(100)=NULL, 
 @COLUMNA04   VARCHAR(100)=NULL,   @COLUMNA05   VARCHAR(100)=NULL,   @COLUMNA06   NVARCHAR(250)=NULL, 
 @COLUMNA07   NVARCHAR(250)=NULL,  @COLUMNA08   VARCHAR(100)=NULL,   @COLUMNA09   NVARCHAR(250)=NULL, 
 @COLUMNA10   NVARCHAR(250)=NULL,  @COLUMNA11   VARCHAR(100)=NULL,   @COLUMNA12   NVARCHAR(250)=NULL, 
 @COLUMNA13   NVARCHAR(250)=NULL,  @COLUMNB01   NVARCHAR(250)=NULL,  @COLUMNB02   NVARCHAR(250)=NULL, 
 @COLUMNB03   NVARCHAR(250)=NULL,  @COLUMNB04   NVARCHAR(250)=NULL,  @COLUMNB05   NVARCHAR(250)=NULL, 
 @COLUMNB06   NVARCHAR(250)=NULL,  @COLUMNB07   NVARCHAR(250)=NULL,  @COLUMNB08   NVARCHAR(250)=NULL, 
 @COLUMNB09   NVARCHAR(250)=NULL,  @COLUMNB10   NVARCHAR(250)=NULL,  @COLUMNB11   VARCHAR(100)=NULL, 
 @COLUMNB12   VARCHAR(100)=NULL,   @COLUMND01   NVARCHAR(250)=NULL,  @COLUMND02   NVARCHAR(250)=NULL, 
 @COLUMND03   NVARCHAR(250)=NULL,  @COLUMND04   NVARCHAR(250)=NULL,  @COLUMND05   NVARCHAR(250)=NULL, 
 @COLUMND06   NVARCHAR(250)=NULL,  @COLUMND07   NVARCHAR(250)=NULL,  @COLUMND08   NVARCHAR(250)=NULL, 
 @COLUMND09   NVARCHAR(250)=NULL,  @COLUMND10   VARCHAR(100)=NULL,   @Direction   NVARCHAR(250), 
 @TabelName   NVARCHAR(250)=NULL,  @ReturnValue NVARCHAR(250)=NULL,   
 @Result      NVARCHAR(250)=NULL,  @VendorID    NVARCHAR(250)=NULL) 
AS 
  BEGIN 
      BEGIN try 
  	  --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
	  set @COLUMN19=(select iif((@COLUMN19!='' and @COLUMN19>0),@COLUMN19,(select max(column02) from fitable037 where column07=1 and column08=1)))
          IF @Direction = 'Insert' 
           BEGIN

declare @FRMID int
declare @soid int
declare @DT date
declare @refid bigint
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE008_SequenceNo
--set @COLUMN14 =(select MAX(COLUMN01) from  SATABLE007);

set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)

set @DT= (SELECT COLUMN08 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
set @COLUMN15 =(select COLUMN15 from  SATABLE007 Where COLUMN01=@COLUMN14 );

set @COLUMN16 =(select COLUMN16 from  SATABLE007 Where COLUMN01=@COLUMN14 );

set @COLUMN17 =(select COLUMN17 from  SATABLE007 Where COLUMN01=@COLUMN14 );

set @COLUMN18 =(select COLUMN18 from  SATABLE007 Where COLUMN01=@COLUMN14 );
set @soid =(select COLUMN06 from  SATABLE007 Where COLUMN01=@COLUMN14 );

set @COLUMN11 =(CAST(@COLUMN11 AS decimal(18,2))- CAST(@COLUMN09 AS decimal(18,2)))

  set @COLUMNA02=( CASE WHEN (@COLUMN14!= '' and @COLUMN14 is not null and @COLUMN14!= '0' ) THEN (select COLUMN15 from SATABLE007 

   where COLUMN01=@COLUMN14) else @COLUMNA02  END )
insert into SATABLE008 

(
   --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
   COLUMN02,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07, COLUMN09,  COLUMN10,  COLUMN11, COLUMN12,  COLUMN13,  
   COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18, COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22, COLUMN23,
   COLUMN24, COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02,
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12,
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)

values

( 
   --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
   @COLUMN02,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07, @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13, 
   @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19,  @COLUMN19,  @COLUMN09,  @COLUMN22,  @COLUMN23,
   @COLUMN24,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 

   @COLUMNA06, @COLUMNA07,@COLUMNA08 , @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 

   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 

   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10

) 
--EMPHCS745 Rajasekhar reddy patakota Item Issue Edit is not updating Inventory Asset
--set @COLUMN08=( (select max(isnull(COLUMN08,0)) from SATABLE008 where   COLUMN04=@COLUMN04 and COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN06 in(@soid))))
set @COLUMN08=( (select sum(isnull(COLUMN09,0)) from SATABLE008 where   COLUMN04=@COLUMN04 and columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN06 in(@soid))))
--set @COLUMN08=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2)))
update SATABLE008 set COLUMN08=@COLUMN08 where column02=@COLUMN02
set @refid=(select COLUMN01 from SATABLE008 where   COLUMN02=@COLUMN02)

--IF(@COLUMN07!=((CAST(@COLUMN08 AS int))+(CAST(@COLUMN09 AS int))))

 

--begin

--set @Result='PARTIALY SHIPPED'

--end

--ELSE begin

--set @Result='SHIPPED'

--end

if exists(select column01 from SATABLE008 where column02=@COLUMN02)

					begin

declare @SID int

	declare @TQty decimal(18,2)

	declare @IFQty decimal(18,2)

	declare @IVQty decimal(18,2)



SET @SID=(SELECT COLUMN06 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
set @COLUMN08=(select sum(isnull(COLUMN12,0)) from SATABLE006 where COLUMNA13=0 AND  COLUMN19= (select COLUMN01 from SATABLE005 where COLUMN02= @SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)
SET @TQty=(SELECT sum(ISNULL(COLUMN07,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
UPDATE SATABLE006 SET  COLUMN12=(CAST(@COLUMN09 AS decimal(18,2))) where  COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19 and 
	COLUMN19=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue


SET @IFQty=(SELECT sum(ISNULL(COLUMN12,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)

SET @IVQty=(SELECT sum(ISNULL(COLUMN13,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)

if(@IFQty=0.00)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end
--IF(@TQty=(SELECT sum(ISNULL(COLUMN09,0)) FROM SATABLE008 WHERE COLUMNA13=0 AND COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN06=@SID) ))

--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
--if(@TQty=@IVQty)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
--end
--else if(@IVQty=0)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
--end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
--end
--end
--ELSE 
--BEGIN
--if(@TQty=@IVQty)
--begin
----EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
--end
--else if(@IVQty=0)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
--end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
--end
--end



UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))

--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=27) where COLUMN01=@COLUMN14	


										


--rajasekhar patakota 25/7/2015 Duplicate Updation For Sales Order Billed Quantity
--set @COLUMN08=(select isnull(COLUMN12,0) from SATABLE006 where COLUMN19= (select COLUMN01 from SATABLE005 where COLUMN02= @SID) and COLUMN03=@COLUMN04)

--UPDATE SATABLE006 SET  COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where COLUMN03=@COLUMN04 and 
--	COLUMN19=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))
--UPDATE SATABLE008 SET  COLUMN08=(CAST(@COLUMN08 AS int)+ CAST(@COLUMN09 AS int)) where  COLUMN02=@COLUMN02
										





--UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) 

--                        WHERE COLUMN14=@COLUMN04 and COLUMN02 =(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))







declare @Qty_On_Hand decimal(18,2)

declare @Qty_Commit decimal(18,2)

declare @Qty_Order decimal(18,2)

declare @Qty_AVL decimal(18,2)

declare @Qty_BACK decimal(18,2)

declare @Qty decimal(18,2)

declare @WIP decimal(18,2), @AVG decimal(18,2)=0

declare @Memo nvarchar(250)
--EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
declare @Opunit nvarchar(250),@Backorder bit
set @Opunit=(@COLUMNA02)
set @Opunit=(iif(@Opunit='',0 ,isnull(@Opunit,0)))
--if exists(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
--begin
--set @Backorder=(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
--end
--else
begin
set @Backorder=(0)
end
if(@Backorder!=1)
begin
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04  AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
begin
declare @WipQty decimal(18,2)
set @Qty_BACK =0
--EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
 set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)- cast(@COLUMN09 as decimal(18,2));

 select  @WIP=isnull(COLUMN18,0),@AVG = isnull(COLUMN17,0) from FITABLE010         where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0

 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0);

 set @Qty=cast(@COLUMN09 as decimal(18,2));

set @Qty_On_Hand=( @Qty_On_Hand - @Qty);

set @Qty_AVL=(@Qty_On_Hand-@Qty_Commit)

if(@Qty_AVL<0)

begin

set @Qty_AVL=0

set @Qty_BACK =@Qty_Commit

set @Qty_Commit=0

end

if(@FRMID=1284)

	Begin
	--EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
	set @Qty_AVL=@Qty_On_Hand-(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)

		UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand,

		COLUMN18=(@WIP+@COLUMN09),COLUMN12=(cast(@Qty_On_Hand as decimal(18,2))*cast(@AVG as decimal(18,2))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0

	End

else

	begin

		UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05 =@Qty_Commit, COLUMN07 =@Qty_BACK, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast(@Qty_On_Hand as decimal(18,2))*cast(@COLUMN12 as decimal(18,2)))	 
  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0

	end
--if(@Qty_On_Hand>0)
--begin
--UPDATE FITABLE010 SET COLUMN17=cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as decimal(18,2))/@Qty_On_Hand WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
--end
--else
--begin
--UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
--end

end
else
	begin
		declare @tmpnewID1 int
		declare @newID int
		set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
		if(@tmpnewID1>0)
		begin
		set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
		end
		else
		begin
		set @newID=1000
		end
		insert into FITABLE010 
		(
			COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN17,COLUMN24
		)
		values
		(
			@newID,@COLUMN04,@COLUMN09,'0', '0',@COLUMN09, @COLUMN15,@COLUMN19,'0',@COLUMN24,@COLUMNA02,@COLUMNA03,0,@COLUMN06 
		)
	end
end

SET @VendorID=(SELECT COLUMN05 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)

SET @Memo=(SELECT COLUMN13 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)

--declare @newID int

--declare @tmpnewID1 int 

			set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE017)

if(@tmpnewID1>0)

begin

set @newID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1

end

else

begin

set @newID=10000

end
--EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
if(@Backorder!=1)
begin
insert into PUTABLE017 

( 

   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN13,

   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,



   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,



   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10



)

values

(  

    @newID,'1000', @DT ,@COLUMN14, @VendorID,@Memo,((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AVG as DECIMAL(18,2)))),((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AVG as DECIMAL(18,2)))),
	@COLUMNA02,@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
	@COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
	@COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN04, @refid, @COLUMND07,
	@COLUMND08, @COLUMND09, @COLUMND10

)





update  PUTABLE017 set

COLUMN06='JobOrder Issue',COLUMN08= 'Inventory Issed and Not Billed'   WHERE COLUMN05=(select COLUMN01 from SATABLE007 where  COLUMN01=@COLUMN14)
end
set @ReturnValue = 1

end

else

begin

return 0

end

END
          IF @Direction = 'Select' 
            BEGIN 
                SELECT * 
                FROM   satable008 
            END 

          IF @Direction = 'Update' 
            BEGIN 
			--DECLARE @SOID INT 
			declare @preShp decimal(18,2)
			set @preShp=(SELECT column09 FROM   satable008  WHERE  column02 = @COLUMN02); 
                SET @COLUMN15 =(SELECT column15 FROM   satable007  WHERE  column01 = @COLUMN14); 
                SET @COLUMN16 =(SELECT column16 FROM   satable007 WHERE  column01 = @COLUMN14); 
                SET @COLUMN17 =(SELECT column17 FROM   satable007 WHERE  column01 = @COLUMN14); 
                SET @COLUMN18 =(SELECT column18  FROM   satable007  WHERE  column01 = @COLUMN14); 
                SET @SOID =(SELECT COLUMN06  FROM   satable007  WHERE  column01 = @COLUMN14); 
				set @DT= (SELECT COLUMN08 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
                SET @COLUMNA02=( CASE 
                                   WHEN ( @COLUMN14 != '' AND @COLUMN14 IS NOT NULL AND @COLUMN14 != '0' ) 
								   THEN (SELECT   column15    FROM  satable007  WHERE  column01 = @COLUMN14) 
                                   ELSE @COLUMNA02 
                                 END ) 

                UPDATE       SATABLE008
SET                COLUMN02 = @COLUMN02, COLUMN03 = @COLUMN03, COLUMN04 = @COLUMN04, COLUMN05 = @COLUMN05, COLUMN06 = @COLUMN06, 
                         COLUMN07 = @COLUMN07, COLUMN08 = @COLUMN08, COLUMN09 = @COLUMN09, COLUMN10 = @COLUMN10, COLUMN11 = @COLUMN11, 
                         COLUMN12 = @COLUMN12, COLUMN13 = @COLUMN13, COLUMN14 = @COLUMN14, COLUMN15 = @COLUMN15, COLUMN16 = @COLUMN16, 
                         --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
			 COLUMN19 = @COLUMN19,   COLUMN20 =@COLUMN19,    COLUMN21 =@COLUMN09,  COLUMN22 =@COLUMN22,COLUMN23=  @COLUMN23,
			 COLUMN24=  @COLUMN24,
						 COLUMNA01 = @COLUMNA01, COLUMNA02 = @COLUMNA02, COLUMNA03 = @COLUMNA03, COLUMNA04 = @COLUMNA04, 
                         COLUMNA05 = @COLUMNA05,   COLUMNA07 = @COLUMNA07, COLUMNA08 = @COLUMNA08, COLUMNA09 = @COLUMNA09, 
                         COLUMNA10 = @COLUMNA10, COLUMNA11 = @COLUMNA11, COLUMNA12 = @COLUMNA12, COLUMNA13 = @COLUMNA13, COLUMNB01 = @COLUMNB01, 
                         COLUMNB02 = @COLUMNB02, COLUMNB03 = @COLUMNB03, COLUMNB04 = @COLUMNB04, COLUMNB05 = @COLUMNB05, COLUMNB06 = @COLUMNB06, 
                         COLUMNB07 = @COLUMNB07, COLUMNB08 = @COLUMNB08, COLUMNB09 = @COLUMNB09, COLUMNB10 = @COLUMNB10, COLUMNB11 = @COLUMNB11, 
                         COLUMNB12 = @COLUMNB12, COLUMND01 = @COLUMND01, COLUMND02 = @COLUMND02, COLUMND03 = @COLUMND03, COLUMND04 = @COLUMND04, 
                         COLUMND05 = @COLUMND05, COLUMND06 = @COLUMND06, COLUMND07 = @COLUMND07, COLUMND08 = @COLUMND08, COLUMND09 = @COLUMND09, 
                         COLUMND10 = @COLUMND10
WHERE        COLUMN02 = @COLUMN02
set @refid=(select COLUMN01 from SATABLE008 where   COLUMN02=@COLUMN02)
declare @SORecQty DECIMAL(18,2),@ItemRecQty DECIMAL(18,2),@RecQty DECIMAL(18,2),@BQty DECIMAL(18,2),@JRStatus NVARCHAR(250)
--EMPHCS745 Rajasekhar reddy patakota Item Issue Edit is not updating Inventory Asset
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
                set @SORecQty=(select sum(isnull(COLUMN12,0)) from SATABLE006 where COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @SOID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)
UPDATE SATABLE006 SET   COLUMN12=(CAST(@SORecQty AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19 and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@SOID))
--EMPHCS745 Rajasekhar reddy patakota Item Issue Edit is not updating Inventory Asset
set @ItemRecQty=( (select sum(isnull(COLUMN09,0)) from SATABLE008 where   COLUMN04=@COLUMN04 and columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN06 in(@SOID))))
--set @ItemRecQty=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@ItemRecQty AS decimal(18,2)))
set @COLUMN11 =(CAST(@COLUMN07 AS decimal(18,2))- (CAST(@ItemRecQty AS decimal(18,2))))
update SATABLE008 set COLUMN08=@ItemRecQty,COLUMN11=@COLUMN11 where column02=@COLUMN02
UPDATE PUTABLE013 SET COLUMN12=@ItemRecQty WHERE COLUMN14=@COLUMN04 and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@SOID))

SET @SID=(SELECT COLUMN06 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
SET @TQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)
SET @RecQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)
SET @BQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID) and COLUMN03=@COLUMN04 and COLUMN27=@COLUMN19)
set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
--set @JRStatus= (SELECT COLUMN20 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)

--if(@JRStatus='true' or @JRStatus='1')
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=45)
--UPDATE SATABLE005 set COLUMN16=@Result where COLUMN02=@PID
--end
----else
--begin
----EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
--IF(@TQty=(SELECT sum(COLUMN09) FROM SATABLE008 WHERE COLUMNA13=0 AND COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN06=@SID) ))
--begin
--if(@TQty=@BQty)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
--end
--else if(@BQty=0)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
--end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
--end
--end

--ELSE 
--BEGIN
--if(@TQty=@BQty)
--begin
----EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
--end
--else if(@BQty=0)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
--end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=25)
--end
--end

--end
if(@RecQty=0.00)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end

UPDATE SATABLE005 set COLUMN16=@Result where  COLUMN02 in(select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14)
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=27) where COLUMN01=@COLUMN14	
DECLARE @Qty_On_Hand1 DECIMAL(18,2),@Qty_Cmtd DECIMAL(18,2),@ReceiptType NVARCHAR(250)
--EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
set @Qty_On_Hand1=(select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06  AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
--EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
set @Opunit=(@COLUMNA02)
set @Opunit=(iif(@Opunit='',0 ,isnull(@Opunit,0)))
--if exists(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
--begin
--set @Backorder=(select top(1)COLUMN04 from CONTABLE031 where COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow BackOrder for JobOrder Handling' and isnull(column04,0)=1 and isnull(columna13,0)=0)
--end
--else
begin
set @Backorder=(0)
end
if(@Backorder!=1)
begin
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04  AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)
begin
set @Qty_On_Hand=( cast((select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as DECIMAL(18,2))- cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select sum(isnull(COLUMN05,0)) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select sum(isnull(COLUMN07,0)) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)))
 select  @WIP=isnull(COLUMN18,0),@AVG = isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
--if(@Qty_Cmtd>=cast(@COLUMN09 as DECIMAL(18,2)))
--Begin
--set @Qty_Cmtd = (cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))- cast(@COLUMN09 as DECIMAL(18,2)));
--set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))+ cast(@COLUMN09 as DECIMAL(18,2)));
--end
--else if(@Qty_Cmtd<cast(@COLUMN09 as DECIMAL(18,2)))
--begin
--set @Qty_Cmtd = (-cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))+ cast(@COLUMN09 as DECIMAL(18,2)));
--set @Qty_On_Hand=(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))- cast(@COLUMN09 as DECIMAL(18,2)));
--end
set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as DECIMAL(18,2));
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN18=@COLUMN09,COLUMN12=((cast(@Qty_On_Hand as DECIMAL(18,2)))*(cast(@AVG as DECIMAL(18,2)))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND COLUMN24=@COLUMN06 AND COLUMN22=@COLUMN24 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
--if(cast(@Qty_On_Hand as DECIMAL(18,2))=0.00)
--begin
--UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
--end
--else
--begin
--UPDATE FITABLE010 SET COLUMN17=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0)as DECIMAL(18,2))/cast(@Qty_On_Hand as DECIMAL(18,2))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 and columna03 = @columna03 and isnull(columna13,0) = 0 and iif(cast(COLUMN21 as nvarchar(250))='','0',isnull(COLUMN21,0))=0 AND iif(cast(COLUMN22 as nvarchar(250))='','0',isnull(COLUMN22,0))=0
--end
end

else
	begin
		set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
		if(@tmpnewID1>0)
		begin
		set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
		end
		else
		begin
		set @newID=1000
		end
		insert into FITABLE010 
		(
			COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMN21,COLUMN22,COLUMNA02,COLUMNA03,COLUMN17,COLUMN24
		)
		values
		(
			@newID,@COLUMN04,@COLUMN09,'0', '0',@COLUMN09, @COLUMN15,@COLUMN19,'0',@COLUMN24,@COLUMNA02,@COLUMNA03,0,@COLUMN06
		)
	end
end
set @COLUMN05= CAST(GETDATE() AS DATE);
set @COLUMN02=(select COLUMN01 from  SATABLE007 where COLUMN01=@COLUMN14); 
	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
    declare @COLUMN4 nvarchar(25)
	set @COLUMN4=(select COLUMN06 from  SATABLE007 where COLUMN01=@COLUMN14);
    set @COLUMN20 =(select COLUMN15 from  SATABLE007 Where COLUMN01=@COLUMN14 );
    set @COLUMN21 =(select COLUMN16 from  SATABLE007 Where COLUMN01=@COLUMN14 );
    set @COLUMN22 =(select COLUMN17 from  SATABLE007 Where COLUMN01=@COLUMN14 );
    set @COLUMN23 =(select COLUMN18 from  SATABLE007 Where COLUMN01=@COLUMN14 );
	
	set @ReceiptType='JobOrder Issue';
	
	
insert into PUTABLE011 
( 
   COLUMN02,COLUMN03,  COLUMN04, COLUMN05, COLUMN07,COLUMN08, COLUMN10, COLUMN15,  COLUMN16,  COLUMN20 ,COLUMN21, COLUMN22 ,COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
--	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
   @COLUMN02,  @ReceiptType,   @COLUMN4,  @COLUMN05,@COLUMN06,@COLUMN08,@COLUMN05, '2','2',  @COLUMN20 ,@COLUMN21, @COLUMN22 ,@COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)

SET @VendorID=(SELECT COLUMN05 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
SET @memo=(SELECT COLUMN13 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @newID=10000
end
--EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
if(@Backorder!=1)
begin
	set @COLUMN06='JobOrder Issue';set @COLUMN08= 'Inventory Issued and Not Billed';
	
insert into PUTABLE017 
( 
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
    @newID,'1000', @DT ,@COLUMN02,@COLUMN06, @VendorID,@COLUMN08,@memo,((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AVG as DECIMAL(18,2)))),((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AVG as DECIMAL(18,2)))),
	@COLUMNA02,@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
    @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
	@COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN04, @refid, @COLUMND07,
	@COLUMND08, @COLUMND09, @COLUMND10
)
            END 
            END 
          ELSE IF @Direction = 'Delete' 
            BEGIN 
                UPDATE satable008  SET    columna13 = @COLUMNA13  WHERE  column02 = @COLUMN02 
            END 
      END try 

      BEGIN catch 
	  	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
	   
DECLARE @tempSTR NVARCHAR(MAX)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(250))))
    exec [CheckDirectory] @tempSTR,'Exception_JO_SATABLE008.txt',0

          IF NOT EXISTS(SELECT column01 
                        FROM   satable008 
                        WHERE  column14 = @COLUMN14) 
            BEGIN 
                DELETE satable007 
                WHERE  column01 = @COLUMN14 

                RETURN 0 
            END 

          RETURN 0 
      END catch 
  END 







GO
