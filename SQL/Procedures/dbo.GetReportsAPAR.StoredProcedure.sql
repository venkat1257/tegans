USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetReportsAPAR]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetReportsAPAR]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
	@whereStr nvarchar(1000)=null,
	@Query1 nvarchar(max)=null,
	@Query2 nvarchar(max)=null,
	@DateF nvarchar(250)=null,
	@Category nvarchar(250)=null)
as 
begin

if @Type!=''
begin

 set @whereStr= ' where Type2 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s)'
end

if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OP in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Project!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @Vendor!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where NameV in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and NameV in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if @Category!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Cat in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Cat in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
end
if @Customer!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where NameC in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and NameC in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end

 IF  @ReportName='PayableReport'
BEGIN
exec(';With MyTable AS
(
select  p16.COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=p16.COLUMNA02) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=p16.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)) COLUMN08,p16.COLUMN09,p16.COLUMN06,
FORMAT(p16.COLUMN04, '''+@DateF+''') as COLUMN11 ,(case when p16.COLUMN06=''EXPENSE'' then (select column06 from MATABLE010 where COLUMN02=p16.COLUMN07 and columna03=p16.COLUMNA03)
 else (select column05 from SATABLE001 where COLUMN02=p16.COLUMN07 and columna03=p16.COLUMNA03) end) COLUMN07,p16.COLUMN10,p16.COLUMN12,p16.COLUMN13,null COLUMN14,
 (select column05 from SATABLE002 where COLUMN02=p16.COLUMN15 and columna03=p16.COLUMNA03) COLUMN15,(select column06 from MATABLE010 where COLUMN02=p16.COLUMN07 and columna03=p16.COLUMNA03) COLUMN16,p16.COLUMN17,--p16.COLUMN18
 (CASE WHEN p16.COLUMN06 =''JOURNAL ENTRY'' THEN fj.COLUMN06 ELSE p16.COLUMN18 END) COLUMN18
  , p16.COLUMNA02 OP ,p16.COLUMN07 NameV,p16.COLUMN06 Type2,p16.COLUMN04 orderDate,p16.COLUMN20 Project  from putable016 p16
left outer join FITABLE031 j on j.COLUMN04=p16.COLUMN09 and j.COLUMNA03=p16.COLUMNA03 and j.COLUMNA02=p16.COLUMNA02 and isnull(j.COLUMNA13,0)=0
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and fj.COLUMNA02=j.COLUMNA02 and isnull(fj.COLUMNA13,0)=0 AND (fj.COLUMN05 = p16.COLUMN12 OR fj.COLUMN04 = p16.COLUMN13) AND ISNULL(fj.COLUMN07,0)=ISNULL(p16.COLUMN07,0)
--inner join FITABLE001 f1 on f1.COLUMN02 = p16.COLUMN08 and f1.columna03=p16.columna03 
--left join SATABLE001 s1 on s1.COLUMN02 = p16.COLUMN07  and s1.columna03=p16.columna03 
--inner join CONTABLE007 c7 on c7.COLUMN02 = p16.COLUMNA02 and c7.columna03=p16.columna03 
--left join MATABLE010 m10 on m10.COLUMN02 = p16.COLUMN07  and m10.columna03=p16.columna03 
--left join SATABLE002 s2 on s2.COLUMN02 = p16.COLUMN07  and s2.columna03=p16.columna03 
where isnull((p16.COLUMNA13),0)=0 and p16.COLUMN04 between '''+@FromDate+''' and '''+@ToDate+''' and p16.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p16.COLUMNA03='''+@AcOwner+''' AND p16.column06!=''EXPENSE''

union all

select NULL COLUMN02,c7.COLUMN03 as OperatingUnit,
null  COLUMN08,
fitable020.COLUMN04 COLUMN09,''ADVANCE PAYMENT'' COLUMN06,FORMAT(fitable020.COLUMN05,'''+@DateF+''') as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07 and columna03=fitable020.COLUMNA03)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07  and columna03=fitable020.COLUMNA03) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07 and columna03=fitable020.COLUMNA03) end) 
 COLUMN07,fitable020.COLUMN16 COLUMN10, 0 COLUMN12,fitable020.COLUMN18 COLUMN13,fitable020.COLUMN18 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,fitable020.COLUMN08 COLUMN18 
 ,fitable020.COLUMNA02 OP ,fitable020.COLUMN07 NameV ,''ADVANCE PAYMENT'' Type2,fitable020.COLUMN05 orderDate,0 Project  from fitable020 
--inner join FITABLE001 f1 on f1.COLUMN02 = fitable020.COLUMN06 and f1.columna03=fitable020.columna03 
--left join SATABLE001 s1 on s1.COLUMN02 = fitable020.COLUMN07  and s1.columna03=fitable020.columna03 
--left join SATABLE002 s2 on s2.COLUMN02 = fitable020.COLUMN07  and s2.columna03=fitable020.columna03 
--left join MATABLE010 m10 on m10.COLUMN02 = fitable020.COLUMN07  and m10.columna03=fitable020.columna03 
inner join CONTABLE007 c7 on c7.COLUMN02 = fitable020.COLUMNA02 and c7.columna03=fitable020.columna03 
where fitable020.COLUMN03=1363 and isnull((fitable020.COLUMNA13),0)=0 and fitable020.COLUMN05 between '''+@FromDate+''' and '''+@ToDate+''' and fitable020.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  fitable020.COLUMNA03='''+@AcOwner+''' AND fitable020.COLUMN11=22305


union all

select COLUMN02,OperatingUnit,COLUMN08,COLUMN09,COLUMN06,COLUMN11,COLUMN07,COLUMN10,
iif(SUM(COLUMN13)>sum(COLUMN12),0,SUM(COLUMN12)-sum(COLUMN13)) COLUMN12,
iif(SUM(COLUMN13)>sum(COLUMN12),SUM(COLUMN13)-sum(COLUMN12),0) COLUMN13,
--iif(SUM(COLUMN13)>sum(COLUMN12),SUM(COLUMN13)-sum(COLUMN12),sum(COLUMN12)-sum(COLUMN13))
NULL COLUMN14,COLUMN15,COLUMN16,COLUMN17,COLUMN18,OP , NameV,Type2,COLUMN11 orderDate, Project from (

 select   NULL COLUMN02,c7.COLUMN03 as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,''Opening Balance'' COLUMN06,NULL COLUMN11 ,
s1.column05  COLUMN07,NULL COLUMN10,
--isnull(sum(p16.COLUMN12),0) COLUMN12,isnull(sum(p16.COLUMN13),0) COLUMN13,isnull(sum(p16.COLUMN12)-sum(p16.COLUMN13),0) COLUMN14,
iif(isnull(sum(p16.COLUMN12),0)>isnull(sum(p16.COLUMN13),0),(isnull(sum(p16.COLUMN12),0)-isnull(sum(p16.COLUMN13),0)),0) COLUMN12,
-iif(isnull(sum(p16.COLUMN12),0)>isnull(sum(p16.COLUMN13),0),0,(isnull(sum(p16.COLUMN12),0)-isnull(sum(p16.COLUMN13),0))) COLUMN13,
isnull(sum(p16.COLUMN12)-sum(p16.COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,null COLUMN18 ,p16.COLUMNA02 OP  ,p16.COLUMN07 NameV ,''Opening Balance'' Type2,NULL orderDate,0 Project from putable016 p16
left join SATABLE001 s1 on s1.COLUMN02 = p16.COLUMN07  and s1.columna03=p16.columna03 
inner join CONTABLE007 c7 on c7.COLUMN02 = p16.COLUMNA02 and c7.columna03=p16.columna03 
where isnull((p16.COLUMNA13),0)=0 and p16.COLUMN04 <'''+@FromDate+''' and p16.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p16.COLUMNA03='''+@AcOwner+''' AND p16.column06!=''EXPENSE''
 

GROUP BY p16.COLUMN07,p16.COLUMNA02,s1.column05,p16.COLUMN06,c7.COLUMN03,p16.COLUMN20

union all
select NULL COLUMN02,c7.COLUMN03  as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,''Opening Balance'' COLUMN06,NULL COLUMN11 ,
s1.column05  COLUMN07,NULL COLUMN10,
 0 COLUMN12,sum(f20.COLUMN18) COLUMN13,
sum(f20.COLUMN18) COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,null COLUMN18
,f20.COLUMNA02 OP ,f20.COLUMN07 NameV,''Opening Balance'' Type2,NULL orderDate,0 Project from fitable020 f20
inner join SATABLE001 s1 on s1.COLUMN02 = f20.COLUMN07 and s1.columna03=f20.columna03 
inner join CONTABLE007 c7 on c7.COLUMN02 = f20.COLUMNA02 and c7.columna03=f20.columna03 
where f20.COLUMN03=1363 and isnull((f20.COLUMNA13),0)=0 and f20.COLUMN05<'''+@FromDate+''' and f20.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)
 AND  f20.COLUMNA03='''+@AcOwner+'''  AND f20.COLUMN11=22305
 group by  f20.COLUMNA02,f20.COLUMN07,c7.COLUMN03,s1.column05) 
 a group by 

a.COLUMN02,a.OperatingUnit,a.COLUMN08,a.COLUMN09,a.COLUMN06,a.COLUMN11,a.COLUMN07,a.COLUMN10,a.COLUMN15,a.COLUMN16,a.COLUMN17,a.COLUMN18,a.OP,a.NameV,a.Type2,orderDate,Project

 ) select * from MyTable '+@whereStr+'  order by  orderDate desc ') 


END

ELSE IF  @ReportName='ReceivableReport'
BEGIN

exec(';With MyTable AS
(

select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03='''+@AcOwner+''') as OperatingUnit,''PAYMENT'' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06 and columna03='''+@AcOwner+''') COLUMN07
,(select COLUMN04 from MATABLE002 WHERE COLUMN02=(select column07 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06 and columna03='''+@AcOwner+''') )Category,FORMAT(COLUMN04,'''+@DateF+''') COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,SUM(COLUMN07) COLUMN12,null COLUMN14,NULL COLUMN15,''Discount'' COLUMN08
,COLUMNA02 OP,COLUMN06 NameC, (select COLUMN07 from SATABLE002 where COLUMN02= FITABLE036.COLUMN06 and columna03='''+@AcOwner+''' ) Cat
,''PAYMENT'' Type2,COLUMN04 orderDate,COLUMN11 Project from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between '''+@FromDate+''' and '''+@ToDate+''' AND  COLUMNA03='''+@AcOwner+''' and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and COLUMN03=''PAYMENT''  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and   COLUMN04 between '''+@FromDate+''' and '''+@ToDate+''' and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  COLUMNA03='''+@AcOwner+''')and isnull(COLUMN10,0)=1049 group by FITABLE036.COLUMNA02,COLUMN09,COLUMN06,COLUMN04,COLUMN07,COLUMN11
union all

select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03='''+@AcOwner+''') as OperatingUnit,''ADVANCE RECEIPT'' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08 and columna03='''+@AcOwner+''') COLUMN07
,(select COLUMN04 from MATABLE002 WHERE COLUMN02=(select column07 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08 and columna03='''+@AcOwner+''') )Category, FORMAT(COLUMN05,'''+@DateF+''') COLUMN04,COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN12,null COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08
,COLUMNA02 OP,COLUMN08 NameC,(select COLUMN07 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08 and columna03='''+@AcOwner+''') Cat,''ADVANCE RECEIPT'' Type2,COLUMN05 orderDate,0 Project from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  '''+@FromDate+''' and '''+@ToDate+''' AND  COLUMNA03='''+@AcOwner+''' and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)  group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07 and columna03=putable018.COLUMNA03) COLUMN07
,(select COLUMN04 from MATABLE002 WHERE COLUMN02=(select column07 from SATABLE002 where COLUMN02=putable018.COLUMN07 and columna03='''+@AcOwner+''') )Category, FORMAT(COLUMN04,'''+@DateF+''') COLUMN04,
 IIF(COLUMN06=''JOURNAL ENTRY'', (SELECT COLUMN04  FROM FITABLE031 WHERE COLUMN01=putable018.COLUMN05)   ,COLUMN05) AS COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08
,COLUMNA02 OP ,COLUMN07 NameC,(select COLUMN07 from SATABLE002 where COLUMN02=putable018.COLUMN07 and columna03='''+@AcOwner+''') Cat,COLUMN06 Type2,COLUMN04 orderDate,COLUMN18 Project  from putable018 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between '''+@FromDate+''' and '''+@ToDate+''' and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  COLUMNA03='''+@AcOwner+'''
union all
--opening balance

select COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,Category,COLUMN04,COLUMN05,
iif(SUM(COLUMN09)-sum(COLUMN11)>0,SUM(COLUMN09)-sum(COLUMN11),0) COLUMN09,
iif(SUM(COLUMN09)-sum(COLUMN11)>0,0,SUM(COLUMN11)-sum(COLUMN09)) COLUMN11,
--sum(COLUMN09),sum(COLUMN11),
isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,COLUMN14,COLUMN15,COLUMN08,OP ,NameC,Cat, Type2,COLUMN04 orderDate,Project from (

select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02= p18.COLUMNA02 and  COLUMNA03='''+@AcOwner+''') as OperatingUnit,
''Opening Balance'' COLUMN06,NULL COLUMN16,  s2.column05 COLUMN07,NULL COLUMN04 ,NULL COLUMN05,

iif(isnull(sum(p18.COLUMN09),0)>isnull(sum(p18.COLUMN11),0),(isnull(sum(p18.COLUMN09),0)-isnull(sum(p18.COLUMN11),0)),0) COLUMN09,
iif(isnull(sum(p18.COLUMN09),0)>isnull(sum(p18.COLUMN11),0),0,(isnull(sum(p18.COLUMN11),0)-isnull(sum(p18.COLUMN09),0))) COLUMN11

--,isnull(sum(p18.COLUMN09),0) COLUMN09,isnull(sum(p18.COLUMN11),0) COLUMN11

,isnull(sum(p18.COLUMN09)-sum(p18.COLUMN11),0) COLUMN12,

NULL COLUMN14,NULL COLUMN15,NULL COLUMN08 ,p18.COLUMNA02 OP ,p18.COLUMN07 NameC,s2.COLUMN07  Cat,
M2.column04 Category,''Opening Balance'' Type2,NULL orderDate,0 Project
from putable018 p18
LEFT join SATABLE002 s2 on s2.COLUMN02=p18.COLUMN07 and s2.COLUMNA03=p18.COLUMNA03
left outer JOIN MATABLE002 M2 ON M2.COLUMN02=s2.COLUMN07 AND M2.COLUMNA03=s2.COLUMNA03  AND ISNULL(M2.COLUMNA13,0)=0
 where isnull((p18.COLUMNA13),0)=0 and p18.COLUMN04 <'''+@FromDate+'''   
	
and p18.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p18.COLUMNA03='''+@AcOwner+'''
group by p18.COLUMN07,p18.COLUMNA02,  s2.column05,M2.column04,s2.COLUMN07,P18.COLUMN18  HAVING (isnull(sum(p18.COLUMN11),0)-isnull(sum(p18.COLUMN09),0))!=0

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=f36.COLUMNA02 and  COLUMNA03='''+@AcOwner+''') as OperatingUnit,''Opening Balance'' COLUMN06,NULL COLUMN16,
s2.column05  COLUMN07,NULL COLUMN04, NULL COLUMN05,
0 COLUMN09,SUM(f36.COLUMN07) COLUMN11,SUM(f36.COLUMN07) COLUMN12,
NULL COLUMN14,NULL COLUMN15,NULL COLUMN08 
,f36.COLUMNA02 OP ,f36.COLUMN06 NameC,s2.COLUMN07  Cat,M2.column04 Category ,''Opening Balance'' Type2,NULL orderDate,0 Project from FITABLE036 f36 
inner join SATABLE002 s2 on s2.COLUMN02=f36.COLUMN06 and s2.COLUMNA03=f36.COLUMNA03
left outer JOIN MATABLE002 M2 ON M2.COLUMN02=s2.COLUMN07 AND M2.COLUMNA03=s2.COLUMNA03  AND ISNULL(M2.COLUMNA13,0)=0	
where isnull((f36.COLUMNA13),0)=0   and   f36.COLUMN04 < '''+@FromDate+'''  AND  f36.COLUMNA03='''+@AcOwner+''' and f36.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and f36.COLUMN03=''PAYMENT'' and f36.COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN04 < '''+@FromDate+''' and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  COLUMNA03='''+@AcOwner+''') and isnull(f36.COLUMN10,0)=1049
  group by f36.COLUMNA02,  s2.column05,f36.COLUMN07,M2.column04,s2.COLUMN07 
, f36.COLUMN06,F36.COLUMN11
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=f23.COLUMNA02 and  COLUMNA03='''+@AcOwner+''') as OperatingUnit,''Opening Balance'' COLUMN06,NULL COLUMN16,
s2.column05  COLUMN07,NULL COLUMN04, NULL COLUMN05,
0 COLUMN09,isnull(sum(cast(f23.COLUMN19 as decimal(18,2))),0) COLUMN11,isnull(sum(cast(f23.COLUMN19 as decimal(18,2))),0) COLUMN12,NULL COLUMN14,NULL COLUMN15,NULL COLUMN08 
,f23.COLUMNA02 OP,f23.COLUMN08 NameC,s2.COLUMN07  Cat,M2.column04 Category,''Opening Balance'' Type2,NULL orderDate,0 Project from FITABLE023   f23 
inner join SATABLE002 s2 on s2.COLUMN02=f23.COLUMN08 and  s2.COLUMNa03 = f23.COLUMNa03
left outer JOIN MATABLE002 M2 ON M2.COLUMN02=s2.COLUMN07 AND M2.COLUMNA03=s2.COLUMNA03  AND ISNULL(M2.COLUMNA13,0)=0	
where f23.COLUMN03=1386 and isnull((f23.COLUMNA13),0)=0   and   f23.COLUMN05< '''+@FromDate+''' AND  f23.COLUMNA03='''+@AcOwner+''' and f23.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)     group by f23.COLUMNA02,f23.COLUMN08
,  s2.column05,M2.column04,s2.COLUMN07

) a group by COLUMN02,OperatingUnit,COLUMN06,COLUMN16,COLUMN07,Category,COLUMN04,COLUMN05,COLUMN14,COLUMN15,COLUMN08,OP,NameC,Cat, Type2,orderDate, Project

) 
select * from MyTable '+@whereStr+'  order by  orderDate desc ') 

ENd


end





GO
