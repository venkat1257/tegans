USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAS_TP_FITABLE001]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAS_TP_FITABLE001]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR FITABLE001_SEQUENCENo
set @COLUMN09=(iif(isnull(@COLUMN09,'')='',CONVERT(date, getdate()),@COLUMN09))
--if(@COLUMN14='1' or @COLUMN14='True')
--begin
--update FITABLE001 set COLUMN14='0' where  COLUMNA03=@COLUMNA03 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null)
--end
insert into FITABLE001 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10, COLUMN11, 
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMNA01, 
   COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
   COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, 
   COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN10,  @COLUMN09,@COLUMN10, @COLUMN11, 
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMNA01, 
   @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
   @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
   @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
   @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  


if(@COLUMNA02='')
begin
set @COLUMNA02=@COLUMN13
end
DECLARE @ACCOUNTTYPE NVARCHAR(250),@Increased NVARCHAR(250),@Decreased NVARCHAR(250),@INTERNAL NVARCHAR(25)
set @INTERNAL=(SELECT COLUMN01 FROM FITABLE001 WHERE COLUMN02=@COLUMN02)
IF(@COLUMN07 IN('22263','22330','22383','22405','22407','22411','22406'))
BEGIN
	set @Increased=(@COLUMN10)
	set @Decreased=(0)
END
ELSE
BEGIN
	set @Increased=(0)
	set @Decreased=(@COLUMN10)
END
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Opening Balance',  @COLUMN04 = @COLUMN02, @COLUMN05 = @INTERNAL,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN09,   @COLUMN08 = '0',  @COLUMN09 = '0',    @COLUMN10 = @COLUMN07, @COLUMN11 = @COLUMN02,
		@COLUMN12 = @COLUMN05,   @COLUMN13 = '0',	@COLUMN14 = @Increased,       @COLUMN15 = @Decreased,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

		--EMPHCS780  rajaekhar reddy patakota 29/7/2015 opening balance should create record in bank register of bank type 
	    if(@COLUMN07='22266')
		begin
		declare @tmpnewID1 int, @newID int
		set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE019)
		if(@tmpnewID1>0)
		begin
		set @newID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
		end
		else
		begin
		set @newID=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12, COLUMNA02, COLUMNA03, COLUMNA13)
		 values(@newID,@COLUMN02,@COLUMN09,@COLUMN02,'Opening Balance',NULL,@COLUMN08,'',@COLUMN10,@COLUMN10, @COLUMNA02, @COLUMNA03,0)
		 --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		 end
		
		--Cash
		ELSE IF(@COLUMN07='22409')
		BEGIN
			SET @newID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
			insert into FITABLE052 
			(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
			 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
			values(
			ISNULL(@newID,1000),@COLUMN02, @COLUMN09,@COLUMN02 , 'Opening Balance',NULL,NULL,@COLUMN08,  '',  0,
			   @COLUMN10, @COLUMN10,0,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
		END

		 else if(@COLUMN07='22262')
		begin
		declare @tmpnewID2 int, @newID2 int
		set @tmpnewID2=(select MAX(COLUMN02) from PUTABLE017)
		if(@tmpnewID2>0)
		begin
		set @newID2=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
		end
		else
		begin
		set @newID2=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into PUTABLE017(COLUMN02 ,COLUMN03 ,COLUMN04  ,COLUMN05 ,COLUMN06,COLUMN08 ,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13, COLUMNA02, COLUMNA03, COLUMNA13)
		values(@newID2 ,@COLUMN02 ,@COLUMN09,@COLUMN02, 'Opening Balance', @COLUMN06,@COLUMN04,@COLUMN10,0,@COLUMN10,@COLUMN13, @COLUMNA02, @COLUMNA03,0)
		   

		 end

		 else if(@COLUMN07='22263')
		begin
		declare @tmpnewID3 int, @newID3 int
		set @tmpnewID3=(select MAX(COLUMN02) from PUTABLE016)
		if(@tmpnewID3>0)
		begin
		set @newID3=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		end
		else
		begin
		set @newID3=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into PUTABLE016(COLUMN02 ,COLUMN03,COLUMN04 ,COLUMN05,COLUMN06 ,COLUMN08,COLUMN12,COLUMN13,COLUMN14,COLUMN18, COLUMNA02, COLUMNA03, COLUMNA13)
		
		 values(@newID3  ,@COLUMN02 ,@COLUMN09,@COLUMN02, 'Opening Balance', @COLUMN02,@COLUMN10,0,@COLUMN10,@COLUMN04, @COLUMNA02, @COLUMNA03,0)
		
		   
		 end

		 else if(@COLUMN07='22264')
		begin
		declare @tmpnewID4 int, @newID4 int
		set @tmpnewID4=(select MAX(COLUMN02) from PUTABLE018)
		if(@tmpnewID4>0)
		begin
		set @newID4=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1
		end
		else
		begin
		set @newID4=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN07,COLUMN04 ,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN16, COLUMNA02, COLUMNA03, COLUMNA13)
		  
		 values(@newID4,@COLUMN02,@COLUMN02,@COLUMN09,@COLUMN02,'Opening Balance',@COLUMN04,@COLUMN10,0,@COLUMN10, @COLUMN06, @COLUMNA02, @COLUMNA03,0)
		
		   
		 end

		 else if(@COLUMN07='22330')
		begin
		declare @tmpnewID5 int, @newID5 int
		set @tmpnewID5=(select MAX(COLUMN02) from FITABLE029)
		if(@tmpnewID5>0)
		begin
		set @newID5=cast((select MAX(COLUMN02) from FITABLE029) as int)+1
		end
		else
		begin
		set @newID5=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE029(COLUMN02,COLUMN03,COLUMN04,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN07, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID5, 'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end

		 else if(@COLUMN07='22344')
		begin
		declare @tmpnewID6 int, @newID6 int
		set @tmpnewID6=(select MAX(COLUMN02) from FITABLE036)
		if(@tmpnewID6>0)
		begin
		set @newID6=cast((select MAX(COLUMN02) from FITABLE036) as int)+1
		end
		else
		begin
		set @newID6=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE036(COLUMN02,COLUMN05,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09, COLUMNA02, COLUMNA03, COLUMNA13,column10,column06)

		 values(@newID6,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02, @COLUMNA02, @COLUMNA03,0,@column02,@column02)

		   
		 end

		 else if(@COLUMN07='22383')
		begin
		declare @tmpnewID8 int, @newID8 int
		set @tmpnewID8=(select MAX(COLUMN02) from FITABLE026)
		if(@tmpnewID8>0)
		begin
		set @newID8=cast((select MAX(COLUMN02) from FITABLE026) as int)+1
		end
		else
		begin
		set @newID8=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11,COLUMN12,COLUMN14, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN17)
		 values(@newID8,@COLUMN09,'Opening Balance',@COLUMN08,@COLUMN02,@COLUMN05,@COLUMN02,@COLUMN07,@COLUMN10,0,@COLUMN10,@COLUMNA02, @COLUMNA03,0,@COLUMN04)
	
		   
		 end

		 
		 else if(@COLUMN07='22402')
		begin
		declare @tmpnewID9 int, @newID9 int
		set @tmpnewID9=(select MAX(COLUMN02) from FITABLE034)
		if(@tmpnewID9>0)
		begin
		set @newID9=cast((select MAX(COLUMN02) from FITABLE034) as int)+1
		end
		else
		begin
		set @newID9=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE034(COLUMN02,COLUMN06,COLUMN03,COLUMN04,COLUMN05,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID9,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end

		 
		 else if(@COLUMN07='22403')
		begin
		declare @tmpnewID10 int, @newID10 int
		set @tmpnewID10=(select MAX(COLUMN02) from FITABLE028)
		if(@tmpnewID10>0)
		begin
		set @newID10=cast((select MAX(COLUMN02) from FITABLE028) as int)+1
		end
		else
		begin
		set @newID10=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE028(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)
		values(@newID10,@COLUMN02,@COLUMN09,'Opening Balance',@COLUMN02,@COLUMN02 ,@COLUMN04,@COLUMN10,0, @COLUMNA02, @COLUMNA03,0)
	
		 end


		
		 else if(@COLUMN07='22405')
		begin
		declare @tmpnewID11 int, @newID11 int
		set @tmpnewID11=(select MAX(COLUMN02) from FITABLE035)
		if(@tmpnewID11>0)
		begin
		set @newID11=cast((select MAX(COLUMN02) from FITABLE035) as int)+1
		end
		else
		begin
		set @newID11=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE035(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID11,'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN02,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end

		 
		 else if(@COLUMN07='22406')
		begin
		declare @tmpnewID12 int, @newID12 int
		set @tmpnewID12=(select MAX(COLUMN02) from FITABLE025)
		if(@tmpnewID12>0)
		begin
		set @newID12=cast((select MAX(COLUMN02) from FITABLE025) as int)+1
		end
		else
		begin
		set @newID12=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11, COLUMNA02, COLUMNA03, COLUMNA13)
				 values(@newID12,@COLUMN09,'Opening Balance',@COLUMN02,@COLUMN04,@COLUMN02,@COLUMN10,@COLUMN10,0, @COLUMNA02, @COLUMNA03,0)
	

		   
		 end


		 
		
		 else if(@COLUMN07='22407')
		begin
		declare @tmpnewID13 int, @newID13 int
		set @tmpnewID13=(select MAX(COLUMN02) from FITABLE035)
		if(@tmpnewID13>0)
		begin
		set @newID13=cast((select MAX(COLUMN02) from FITABLE035) as int)+1
		end
		else
		begin
		set @newID13=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE035(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID13,'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN02,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end



		 else if(@COLUMN07='22408')
		begin
		declare @tmpnewID14 int, @newID14 int
		set @tmpnewID14=(select MAX(COLUMN02) from FITABLE036)
		if(@tmpnewID14>0)
		begin
		set @newID14=cast((select MAX(COLUMN02) from FITABLE036) as int)+1
		end
		else
		begin
		set @newID14=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		 
		insert into FITABLE036(COLUMN02,COLUMN05,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09, COLUMNA02, COLUMNA03, COLUMNA13,column10,column06)

		 values(@newID14,@COLUMN02, 'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02, @COLUMNA02, @COLUMNA03,0,@column02,@column02)
		   
		 end
		 --NON CURRENT ASSETS
		 else if(@COLUMN07='22410')
		begin
		set @tmpnewID9=(select MAX(COLUMN02) from FITABLE060)
		if(@tmpnewID9>0)
		begin
		set @newID9=cast((select MAX(COLUMN02) from FITABLE060) as int)+1
		end
		else
		begin
		set @newID9=100000
		end
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		insert into FITABLE060(COLUMN02,COLUMN06,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN05)
		 values(@newID9,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0,@COLUMN02)
		 end
		 --NON CURRENT LIABILITIES 
		 else if(@COLUMN07='22411')
		begin
		set @tmpnewID9=(select MAX(COLUMN02) from FITABLE063)
		if(@tmpnewID9>0)
		begin
		set @newID9=cast((select MAX(COLUMN02) from FITABLE063) as int)+1
		end
		else
		begin
		set @newID9=100000
		end
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		insert into FITABLE063(COLUMN02,COLUMN06,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN05)
		 values(@newID9,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0, @COLUMN02)
		 end

set @ReturnValue = 1

END

 

ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE001
END 
 

ELSE IF @Direction = 'Update'
BEGIN
if(@COLUMNA02='')
set @COLUMNA02=@COLUMN13

set @COLUMN09=(iif(isnull(@COLUMN09,'')='',CONVERT(date, getdate()),@COLUMN09))
declare @opnBal1 decimal(18,2)
declare @opnBal2 decimal(18,2)
declare @opnBal decimal(18,2)
set @opnBal1 = (select COLUMN08 from FITABLE001 where COLUMN02 = @COLUMN02 and COLUMNA03=@COLUMNA03)
set @opnBal2 = (select COLUMN10 from FITABLE001 where COLUMN02 = @COLUMN02 and COLUMNA03=@COLUMNA03)

 declare @oldType nvarchar(250)=null 
 set @oldType=(select COLUMN07 from FITABLE001 where COLUMN02 = @COLUMN02 and COLUMNA03=@COLUMNA03)
 set @COLUMN07=@oldType

if(@opnBal1=@opnBal2)
begin
set @opnBal=@COLUMN10
end
else
begin
set @opnBal=@opnBal1
end

--if(@COLUMN14='1' or @COLUMN14='True')
--begin
--update FITABLE001 set COLUMN14='0' where  COLUMNA03=@COLUMNA03 and (COLUMNA02=@COLUMNA02 or COLUMNA02 is null)
--end
UPDATE FITABLE001 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN10,      COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11, 
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMNA01=@COLUMNA01, 
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA06=@COLUMNA06,
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01, 
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
  --EMPHCS1009 Chart of accounts update should happen based on appowner by Srini 22/08/2015
   WHERE COLUMN02 = @COLUMN02 and COLUMNA03=@COLUMNA03
set @INTERNAL=(SELECT COLUMN01 FROM FITABLE001 WHERE COLUMN02=@COLUMN02)
DELETE FROM FITABLE064 WHERE COLUMN05 = @INTERNAL AND COLUMN03 = 'Opening Balance' AND COLUMNA03  = @COLUMNA03
IF(@COLUMN07 IN('22263','22330','22383','22405','22407','22411','22406'))
BEGIN
	set @Increased=(@COLUMN10)
	set @Decreased=(0)
END
ELSE
BEGIN
	set @Increased=(0)
	set @Decreased=(@COLUMN10)
END
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Opening Balance',  @COLUMN04 = @COLUMN02, @COLUMN05 = @INTERNAL,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN09,   @COLUMN08 = '0',  @COLUMN09 = '0',    @COLUMN10 = @COLUMN07, @COLUMN11 = @COLUMN02,
		@COLUMN12 = @COLUMN05,   @COLUMN13 = '0',	@COLUMN14 = @Increased,       @COLUMN15 = @Decreased,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'


		if(@oldType='22262')
begin  
 delete from PUTABLE017 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02 and COLUMNA03=@COLUMNA03  and column06 = 'Opening Balance'
end
		else if(@oldType='22263')
		BEGIN
delete from PUTABLE016 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02   and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
END
		else if(@oldType='22264')
		BEGIN
delete from PUTABLE018 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02   and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
END
	    else if(@oldType='22330')
		BEGIN	
delete from FITABLE029 where COLUMN11=@COLUMN02 and COLUMN07=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		else if(@oldType='22344')
		BEGIN
delete from FITABLE036 where COLUMN10=@COLUMN02  AND COLUMN05=@COLUMN02 and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		else if(@oldType='22383')
		BEGIN
delete from FITABLE026 where  COLUMN06=@COLUMN02 and COLUMN08=@COLUMN02 and COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance'
		END
		else if(@oldType='22402')
		BEGIN
delete from FITABLE034 where  COLUMN06=@COLUMN02 and COLUMN09=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		else if(@oldType='22403')
		BEGIN
delete from FITABLE028 where  COLUMN06=@COLUMN02 and COLUMN07=@COLUMN02  and COLUMNA03=@COLUMNA03 and column05 = 'Opening Balance'
		END
		else if(@oldType='22405')
		BEGIN
delete from FITABLE035 where  COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		else if(@oldType='22406')
		BEGIN
delete from FITABLE025 where  COLUMN05=@COLUMN02  and COLUMN08=@COLUMN02  and COLUMN04=@oldType  and COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance'
		END
		else if(@oldType='22407')
		BEGIN
delete from FITABLE035 where  COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		else if(@oldType='22408')
		BEGIN
delete from FITABLE036 where  COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		else if(@oldType='22409')
		BEGIN
		delete from FITABLE052 where  COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		END
		
		else if(@oldType='22410')
		BEGIN
		delete from FITABLE060 where  COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END

		else if(@oldType='22411')
		BEGIN
		delete from FITABLE063 where  COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
	--EMPHCS780  rajaekhar reddy patakota 29/7/2015 opening balance should create record in bank register of bank type 
	     if(@COLUMN07!='22266' or @COLUMN12=1)
		begin  
        delete from PUTABLE019 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02   and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
	    end
		else
		begin  
		if exists(select column02 from PUTABLE019 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02   and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance')
		begin
        update PUTABLE019 set  COLUMN11=@COLUMN10,COLUMN04=@COLUMN09,COLUMN12=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02 and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		end
		else
		begin
		set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE019)
		if(@tmpnewID1>0)
		begin
		set @newID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
		end
		else
		begin
		set @newID=100000
		end
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)
		insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12, COLUMNA02, COLUMNA03)
						 values(@newID,@COLUMN02,@COLUMN09,@COLUMN02,'Opening Balance',NULL,@COLUMN08,'',@COLUMN10,@COLUMN10, @COLUMNA02, @COLUMNA03)
		 end
	    end
	    --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		--Cash
		IF(@COLUMN07='22409')
		BEGIN
			SET @newID=(SELECT MAX(ISNULL(COLUMN02 ,999)) FROM FITABLE052)+1
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
			insert into FITABLE052 
			(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,COLUMN07,   COLUMN08,  COLUMN09,   COLUMN10,   COLUMN11,
			 COLUMN12,COLUMN13,  COLUMN16,  COLUMNA02, COLUMNA03, COLUMNA08)
			values(
			ISNULL(@newID,1000),@COLUMN02,  @COLUMN09,@COLUMN02,  'Opening Balance',NULL,NULL,@COLUMN08,  '',  0,
			   @COLUMN10, @COLUMN10,0,  @COLUMNA02, @COLUMNA03, @COLUMNA08)
		END

		ELSE if(@COLUMN07='22262')
		BEGIN
		if exists(select column02 from PUTABLE017 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03  and column06 = 'Opening Balance')
		BEGIN
		update PUTABLE017 set  COLUMN04=@COLUMN09, COLUMN10=@COLUMN10,COLUMN12=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		END
		else
		begin
		declare @tmpnewID2a int, @newID2a int
		set @tmpnewID2a=(select MAX(COLUMN02) from PUTABLE017)
		if(@tmpnewID2a>0)
		begin
		set @newID2a=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
		end
		else
		begin
		set @newID2a=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08 )

		insert into PUTABLE017(COLUMN02 ,COLUMN03 ,COLUMN04  ,COLUMN05 ,COLUMN06,COLUMN08 ,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13, COLUMNA02, COLUMNA03, COLUMNA13)
		values(@newID2a ,@COLUMN02 ,@COLUMN09,@COLUMN02,  'Opening Balance', @COLUMN06,@COLUMN04,@COLUMN10,0,@COLUMN10,@COLUMN13, @COLUMNA02, @COLUMNA03,0)
		   
		 end
		END
		ELSE if(@COLUMN07='22263')
		BEGIN
		if exists(select column02 from PUTABLE016 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02   and COLUMNA03=@COLUMNA03)
		begin
		update PUTABLE016 set  COLUMN06= 'Opening Balance',COLUMN04=@COLUMN09,COLUMN12=@COLUMN10,COLUMN14=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03
		end
		else 
		begin
		declare @tmpnewID3a int, @newID3a int
		set @tmpnewID3a=(select MAX(COLUMN02) from PUTABLE016)
		if(@tmpnewID3a>0)
		begin
		set @newID3a=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		end
		else
		begin
		set @newID3a=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into PUTABLE016(COLUMN02 ,COLUMN03,COLUMN04 ,COLUMN05,COLUMN06 ,COLUMN08,COLUMN12,COLUMN13,COLUMN14,COLUMN18, COLUMNA02, COLUMNA03, COLUMNA13)
		
		 values(@newID3a  ,@COLUMN02 ,@COLUMN09,@COLUMN02, 'Opening Balance', @COLUMN02,@COLUMN10,0,@COLUMN10,@COLUMN04, @COLUMNA02, @COLUMNA03,0)
		
		   
		 end
		END
        ELSE if(@COLUMN07='22264')
		BEGIN
		if exists(select column02 from PUTABLE018 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance')
		BEGIN
		update PUTABLE018 set COLUMN06= 'Opening Balance',COLUMN09=@COLUMN10,COLUMN04=@COLUMN09,COLUMN12=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03
		END
		ELSE
		begin
		declare @tmpnewID4A int, @newID4A int
		set @tmpnewID4A=(select MAX(COLUMN02) from PUTABLE018)
		if(@tmpnewID4A>0)
		begin
		set @newID4A=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1
		end
		else
		begin
		set @newID4A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN07,COLUMN04 ,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN16, COLUMNA02, COLUMNA03, COLUMNA13)
		  
		 values(@newID4A,@COLUMN02,@COLUMN02,@COLUMN09,@COLUMN02,'Opening Balance',@COLUMN04,@COLUMN10,0,@COLUMN10, @COLUMN06, @COLUMNA02, @COLUMNA03,0)
		
		   
		 end

		END
		ELSE if(@COLUMN07='22330')
		BEGIN
		if exists(select column02 from FITABLE029 where COLUMN11=@COLUMN02 and COLUMN07=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance')
		BEGIN
		update FITABLE029 set  COLUMN03= 'Opening Balance',COLUMN04=@COLUMN09,COLUMN09=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN11=@COLUMN02 and COLUMN07=@COLUMN02 and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE 
		begin
		declare @tmpnewID5A int, @newID5A int
		set @tmpnewID5A=(select MAX(COLUMN02) from FITABLE029)
		if(@tmpnewID5A>0)
		begin
		set @newID5A=cast((select MAX(COLUMN02) from FITABLE029) as int)+1
		end
		else
		begin
		set @newID5A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE029(COLUMN02,COLUMN03,COLUMN04,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN07, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID5A, 'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end
		END
		ELSE if(@COLUMN07='22344')
		BEGIN
		if exists(select column02 from FITABLE036 where COLUMN05=@COLUMN02  AND COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance')
		BEGIN
		update FITABLE036 set  COLUMN03= 'Opening Balance',COLUMN04=@COLUMN09,COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN05=@COLUMN02  AND COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID6A int, @newID6A int
		set @tmpnewID6A=(select MAX(COLUMN02) from FITABLE036)
		if(@tmpnewID6A>0)
		begin
		set @newID6A=cast((select MAX(COLUMN02) from FITABLE036) as int)+1
		end
		else
		begin
		set @newID6A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE036(COLUMN02,COLUMN05,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN10, COLUMN06)

		 values(@newID6A,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02, @COLUMNA02, @COLUMNA03,0,@COLUMN02,@COLUMN02)

		   
		 end
		END
		ELSE if(@COLUMN07='22383')
		BEGIN
		if exists(select column02 from FITABLE026 where  COLUMN06=@COLUMN02 and COLUMN08=@COLUMN02  and COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance')
		BEGIN
		update FITABLE026 set  COLUMN04= 'Opening Balance',COLUMN17=@COLUMN04,COLUMN03=@COLUMN09,COLUMN11=@COLUMN10, COLUMN14=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN06=@COLUMN02 and COLUMN08=@COLUMN02  and COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID8A int, @newID8A int
		set @tmpnewID8A=(select MAX(COLUMN02) from FITABLE026)
		if(@tmpnewID8A>0)
		begin
		set @newID8A=cast((select MAX(COLUMN02) from FITABLE026) as int)+1
		end
		else
		begin
		set @newID8A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,COLUMN11,COLUMN12,COLUMN14, COLUMNA02, COLUMNA03, COLUMNA13,column17)
		 values(@newID8A,@COLUMN09,'Opening Balance',@COLUMN08,@COLUMN02,@COLUMN05,@COLUMN02,@COLUMN07,@COLUMN10,0,@COLUMN10,@COLUMNA02, @COLUMNA03,0,@COLUMN04)
	
		   
		 end
		
		END
		ELSE if(@COLUMN07='22402')
		BEGIN
		if exists(select column02 from FITABLE034 where  COLUMN06=@COLUMN02 and COLUMN09=@COLUMN02  and COLUMNA03=@COLUMNA03)
		BEGIN
		update FITABLE034 set  COLUMN03='Opening Balance',COLUMN04=@COLUMN09,COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN06=@COLUMN02 and COLUMN09=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID9A int, @newID9A int
		set @tmpnewID9A=(select MAX(COLUMN02) from FITABLE034)
		if(@tmpnewID9A>0)
		begin
		set @newID9A=cast((select MAX(COLUMN02) from FITABLE034) as int)+1
		end
		else
		begin
		set @newID9A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE034(COLUMN02,COLUMN06,COLUMN03,COLUMN04,COLUMN05,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID9A,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end
		END
		ELSE if(@COLUMN07='22403')
		BEGIN
		if exists(select column02 from FITABLE028 where  COLUMN06=@COLUMN02 and COLUMN07=@COLUMN02  and COLUMNA03=@COLUMNA03)
		BEGIN
		update FITABLE028 set  COLUMN05= 'Opening Balance' ,COLUMN04=@COLUMN09 , COLUMN09=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN06=@COLUMN02 and COLUMN07=@COLUMN02  and COLUMNA03=@COLUMNA03 and column05 = 'Opening Balance'
		END
		 begin
		declare @tmpnewID10A int, @newID10A int
		set @tmpnewID10A=(select MAX(COLUMN02) from FITABLE028)
		if(@tmpnewID10A>0)
		begin
		set @newID10A=cast((select MAX(COLUMN02) from FITABLE028) as int)+1
		end
		else
		begin
		set @newID10A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE028(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)
		values(@newID10A,@COLUMN02,@COLUMN09,'Opening Balance',@COLUMN02,@COLUMN02 ,@COLUMN04,@COLUMN10,0, @COLUMNA02, @COLUMNA03,0)
	
		 end
		END
		ELSE if(@COLUMN07='22405')
		BEGIN
		if exists(select column02 from FITABLE035 where   COLUMN05=@COLUMN02 and  COLUMN10=@COLUMN02   and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance')
		BEGIN
		update FITABLE035 set  COLUMN03= 'Opening Balance',COLUMN04=@COLUMN09, COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where  COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID11A int, @newID11A int
		set @tmpnewID11A=(select MAX(COLUMN02) from FITABLE035)
		if(@tmpnewID11A>0)
		begin
		set @newID11A=cast((select MAX(COLUMN02) from FITABLE035) as int)+1
		end
		else
		begin
		set @newID11A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE035(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID11A,'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN02,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end
		END
		ELSE if(@COLUMN07='22406')
		BEGIN
		if exists(select column02 from FITABLE025 where  COLUMN05=@COLUMN02  and COLUMN08=@COLUMN02  and COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance')
		BEGIN
		update FITABLE025 set  COLUMN04= 'Opening Balance', COLUMN03=@COLUMN09, COLUMN09=@COLUMN10, COLUMN10=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN05=@COLUMN02  and COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID12A int, @newID12A int
		set @tmpnewID12A=(select MAX(COLUMN02) from FITABLE025)
		if(@tmpnewID12A>0)
		begin
		set @newID12A=cast((select MAX(COLUMN02) from FITABLE025) as int)+1
		end
		else
		begin
		set @newID12A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11, COLUMNA02, COLUMNA03, COLUMNA13)
				 values(@newID12A,@COLUMN09,'Opening Balance',@COLUMN02,@COLUMN04,@COLUMN02,@COLUMN10,@COLUMN10,0, @COLUMNA02, @COLUMNA03,0)
	

		   
		 end
		END
		ELSE if(@COLUMN07='22407')
		BEGIN
		if exists(select column02 from FITABLE035 where   COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02   and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance')
		BEGIN
		update FITABLE035 set  COLUMN03= 'Opening Balance',COLUMN04=@COLUMN09, COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where  COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02   and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID13A int, @newID13A int
		set @tmpnewID13A=(select MAX(COLUMN02) from FITABLE035)
		if(@tmpnewID13A>0)
		begin
		set @newID13A=cast((select MAX(COLUMN02) from FITABLE035) as int)+1
		end
		else
		begin
		set @newID13A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		insert into FITABLE035(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13)

		 values(@newID13A,'Opening Balance',@COLUMN09,@COLUMN04,@COLUMN02,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0)

		   
		 end
		END
		ELSE if(@COLUMN07='22408')
		BEGIN
		if exists(select column02 from FITABLE036 where  COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance')
		BEGIN
		update FITABLE036 set  COLUMN03= 'Opening Balance',COLUMN04=@COLUMN09, COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		declare @tmpnewID14A int, @newID14A int
		set @tmpnewID14A=(select MAX(COLUMN02) from FITABLE036)
		if(@tmpnewID14A>0)
		begin
		set @newID14A=cast((select MAX(COLUMN02) from FITABLE036) as int)+1
		end
		else
		begin
		set @newID14A=100000
		end

		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		--set @COLUMNA02=(select MAX(COLUMN26) from MATABLE010 where COLUMN02=@COLUMNA08)

		 
		insert into FITABLE036(COLUMN02,COLUMN05,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN10, COLUMN06)

		 values(@newID14A,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02, @COLUMNA02, @COLUMNA03,0, @COLUMN02, @COLUMN02)
		   
		 end
		END
		ELSE if(@COLUMN07='22410')
		BEGIN
		if exists(select column02 from FITABLE060 where  COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance')
		BEGIN
		update FITABLE060 set  COLUMN03='Opening Balance', COLUMN04=@COLUMN09,COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		set @tmpnewID9A=(select MAX(COLUMN02) from FITABLE060)
		if(@tmpnewID9A>0)
		begin
		set @newID9A=cast((select MAX(COLUMN02) from FITABLE060) as int)+1
		end
		else
		begin
		set @newID9A=100000
		end
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		insert into FITABLE060(COLUMN02,COLUMN06,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN05)
		 values(@newID9A,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0,@COLUMN02)
		 end
		END
		ELSE if(@COLUMN07='22411')
		BEGIN
		if exists(select column02 from FITABLE063 where  COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03  and column03 = 'Opening Balance')
		BEGIN
		update FITABLE063 set  COLUMN03= 'Opening Balance',COLUMN04=@COLUMN09,COLUMN07=@COLUMN10,COLUMNA02=@COLUMNA02 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE
		begin
		set @tmpnewID9A=(select MAX(COLUMN02) from FITABLE063)
		if(@tmpnewID9A>0)
		begin
		set @newID9A=cast((select MAX(COLUMN02) from FITABLE063) as int)+1
		end
		else
		begin
		set @newID9A=100000
		end
		set @COLUMN08=(select MAX(COLUMN01) from FITABLE001 where COLUMN02=@COLUMN02)
		insert into FITABLE063(COLUMN02,COLUMN06,COLUMN03,COLUMN04,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03, COLUMNA13, COLUMN05)
		 values(@newID9A,@COLUMN02,'Opening Balance',@COLUMN09,@COLUMN10,0,@COLUMN02,@COLUMN02, @COLUMNA02, @COLUMNA03,0, @COLUMN02)
		 end
		END
set @ReturnValue = 1
END


ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE001 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02  and COLUMNA03=@COLUMNA03
if(@COLUMN07='22262')
		BEGIN
UPDATE PUTABLE019 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02  and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
        end
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		--Cash
		ELSE IF(@COLUMN07='22409')
		BEGIN
		UPDATE FITABLE052 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02  and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		END
        ELSE if(@COLUMN07='22262')
		BEGIN
		update PUTABLE017 set COLUMNA13=@COLUMNA13 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02   and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		END

		ELSE if(@COLUMN07='22263')
		BEGIN
		update PUTABLE016 set COLUMNA13=@COLUMNA13 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02 and COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22264')
		BEGIN
		update PUTABLE018 set COLUMNA13=@COLUMNA13 where COLUMN03=@COLUMN02 and COLUMN05=@COLUMN02 and  COLUMNA03=@COLUMNA03 and column06 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22330')
		BEGIN
		update FITABLE029 set COLUMNA13=@COLUMNA13 where COLUMN11=@COLUMN02 and COLUMN07=@COLUMN02 and  COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22344')
		BEGIN
		update FITABLE036 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02 and  COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22383')
		BEGIN
		update FITABLE026 set COLUMNA13=@COLUMNA13 where COLUMN06=@COLUMN02 and COLUMN08=@COLUMN02 and  COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22402')
		BEGIN
		update FITABLE034 set COLUMNA13=@COLUMNA13 where COLUMN06=@COLUMN02 and COLUMN09=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22403')
		BEGIN
		update FITABLE028 set COLUMNA13=@COLUMNA13 where COLUMN06=@COLUMN02 and COLUMN07=@COLUMN02  and COLUMNA03=@COLUMNA03 and column05 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22405')
		BEGIN
		update FITABLE035 set COLUMNA13=@COLUMNA13 where COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22406')
		BEGIN
		update FITABLE025 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN02 and  COLUMN08=@COLUMN02 and  COLUMNA03=@COLUMNA03 and column04 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22407')
		BEGIN
		update FITABLE035 set COLUMNA13=@COLUMNA13 where COLUMN06=@COLUMN02 and  COLUMN10=@COLUMN02   and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22408')
		BEGIN
		update FITABLE036 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22410')
		BEGIN
		update FITABLE060 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
		ELSE if(@COLUMN07='22411')
		BEGIN
		update FITABLE063 set COLUMNA13=@COLUMNA13 where COLUMN05=@COLUMN02 and COLUMN10=@COLUMN02  and COLUMNA03=@COLUMNA03 and column03 = 'Opening Balance'
		END
END

end try
begin catch
declare @tempSTR nvarchar(max)
	 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_FITABLE001.txt',0
return 0
end catch
end










GO
