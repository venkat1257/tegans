USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTSOQUOTATION]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_PROC_GSTSOQUOTATION](@SONO nvarchar(250)=null,@TYPE nvarchar(250)=null,@frDT nvarchar(250)=null)
AS
BEGIN
if(@TYPE='HEADER')
begin
select S15.COLUMN04,s.COLUMN05 COLUMN05,S15.COLUMN07,S15.COLUMN01,S15.COLUMN02 COLUMN02,S15.COLUMN15,S15.COLUMN05 cust,
S15.COLUMN13 oper,S15.COLUMN20 as Discount,S15.COLUMN05 as cId,ht.COLUMN07 as HTamnt,c.COLUMN32 as StateLincense,
c.COLUMN33 as CntralLincese,c.COLUMN03 as company,	 c.COLUMN28 as logoName, S15.COLUMN13 as oppunit,  s13.COLUMN04 as ref,
m.COLUMN04 as PaymentTerms,cast(S15.COLUMN08 as date) duedate,c.COLUMN27 vatno,c.COLUMN29 cstno,  c.COLUMN31 panno,
S15.COLUMN22 totalamt,CONVERT(varchar,CAST(S15.COLUMN22 AS money), 1) totalamt1,pm.COLUMN04 Paymentmode  ,s.COLUMN26 cpanno ,
s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,opu.COLUMN03 as ouName,opu.COLUMN30 as OPUVATNO,
opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO, opu.COLUMN34 as OPUPhNo,c.COLUMN35 as tin, c.COLUMN10 as compA1, 
c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip,s.COLUMN11 custPhNo,opu.COLUMN36 as footerText,
s.COLUMN29 ctaxno,m10.COLUMN33 [signature],s.COLUMN42 CustGSTIN,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,
c.COLUMND02 CGSTIN,MC.COLUMN03 compCtry,MS.COLUMN03 compState,c.COLUMN02 CompanyID  
from SATABLE015 S15 left join matable002 pm on pm.column02=S15.COLUMN12  
left join MATABLE022 m on m.column02=S15.column11    left join CONTABLE007 opu on opu.COLUMN02=S15.COLUMN13  
left join SATABLE002 s on s.column02=S15.COLUMN05	 left join CONTABLE008 c on c.COLUMNA03=S15.COLUMNA03 AND (c.COLUMNA02=S15.COLUMNA02 or c.COLUMNA02 is null) AND ISNULL(c.COLUMNA13,0)=0 AND ISNULL(c.COLUMN05,0)=0 
left join MATABLE013 ht on ht.COLUMNA03=S15.COLUMN16 and  ht.COLUMNA03=S15.COLUMNA03 and ht.COLUMNA13=0
left outer join SATABLE013 s13 on s13.COLUMN02=S15.COLUMN18 left outer join MATABLE010 m10 on m10.COLUMN02=S15.COLUMNA08 
left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 left JOIN MATABLE016 MC  ON MC.COLUMN02 = c.COLUMN15 
left JOIN MATABLE017 MS  ON MS.COLUMN02 = c.COLUMN13 
where (S15.column04=@SONO OR S15.column02=@SONO) AND  ISNULL(S15.COLUMNA13,0)=0
end
ELSE if(@TYPE='LINE')
begin
select it.column04 item,it.column06 upc,a.COLUMN06 quantity,a.COLUMN05 'desc',cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) gstrate,
CONVERT(varchar, CAST(a.COLUMN07 AS money), 1) rate,u.COLUMN04 UOM,ht.COLUMN07 as HTamnt,
cast((a.COLUMN08*((ht.COLUMN07))/100) as decimal(18,2)) as lTamnt,hta.column04 names, a.COLUMN08  amt, a.COLUMN08  lineamt,
sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt,M32.COLUMN04 HSNCode,M32.COLUMN04 SACCode,it.COLUMN48 SerItem,CAST(isnull(a.COLUMN09,0) AS DECIMAL(18,0)) PLDiscount
 from  SATABLE016 a  
INNER JOIN SATABLE015 b on b.COLUMN01=a.COLUMN11 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0
inner join matable007 it on it.column02=a.COLUMN03 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE002 u on u.column02=a.COLUMN12 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN13,'-',''))) s)) or t.COLUMN02=a.COLUMN13) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN16 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 hta on hta.column02=ht.COLUMN16 and isnull(hta.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
where b.column02=@SONO and isnull(a.columna13,0)=0
group by a.COLUMN02,it.column04,it.column06,a.COLUMN06,a.COLUMN05,a.COLUMN07,u.COLUMN04,ht.COLUMN07,hta.column04, a.COLUMN08,M32.COLUMN04,it.COLUMN48,a.COLUMN09 ORDER BY a.COLUMN02
END
ELSE if(@TYPE='TOTAL')
begin
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt from(
select isnull(a.COLUMN06,0) qty,cast((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0)) as decimal(18,2)) subtotamt,
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast((isnull(a.COLUMN10,0)) as decimal(18,2)) amt
from  SATABLE016 a  
INNER JOIN SATABLE015 b on b.COLUMN01=a.COLUMN11 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and COLUMN02=replace(a.COLUMN13,'-',''))) s)) or t.COLUMN02=a.COLUMN13) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN06,a.COLUMN07,a.COLUMN10,a.COLUMN02)f
END
ELSE if(@TYPE='LineTax')
begin
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
sum(cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
lt.COLUMN16 TaxType from   SATABLE016 a  
INNER JOIN SATABLE015 b on b.COLUMN01=a.COLUMN11 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0   
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN13,'-',''))) s)) or lt.COLUMN02=a.COLUMN13) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16
END
ELSE if(@TYPE='HSNCODE')
begin
select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,
f.SACCode as SACCode,f.HSNCode as HSNCode,sum(CAST(f.lineamt as decimal(18,2))) lineamt,
f.SerItem as SerItem from (select CONVERT(varchar,CAST(isnull(a.COLUMN07,0) AS money), 1) rate,
(CAST(isnull(a.COLUMN06,0)*isnull(a.COLUMN07,0) as decimal(18,2))) lineamt,sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,
sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,
 cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
 0 as HTamnt,0 as lTamnt , M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,a.COLUMN02 from  SATABLE016 a 
 inner join SATABLE015 b on b.column01=a.COLUMN11 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
 inner join matable007 it on it.column02=a.COLUMN03 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
  left join MATABLE002 u on u.column02=a.COLUMN07 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
  left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN13,'-',''))) s)) or t.COLUMN02=a.COLUMN13) and t.COLUMNA03=A.COLUMNA03 and isnull(t.COLUMNA13,0)=0
   LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 
   LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
   LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 
   where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 
   group by it.column06,a.COLUMN07,a.COLUMN05,a.COLUMN07,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,
   it.COLUMN48,a.COLUMN06,a.COLUMN07,a.COLUMN02) f 
   group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST
END
END




GO
