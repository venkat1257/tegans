USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[Proc_ExpenseTrachReport]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Proc_ExpenseTrachReport](
 @OPUnit nvarchar(250),
 @AcOwner nvarchar(250),
 @FromDate date,
 @OPUnitStatus   int= null,
 @ToDate date,
 @Operating nvarchar(250),
 @Employee nvarchar(250)= null,
 @whereStr nvarchar(1000)=null,
 @Query1 nvarchar(max)=null,
 @Project   nvarchar(250)= null,
 @DateF nvarchar(250)= null,
 @AType nvarchar(250)= null
 )
as
begin
if @Operating!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Operating+''') s)'
end
if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @AType!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where AType in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@AType+''') s)'
end
else
begin
set @whereStr=@whereStr+' and AType in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@AType+''') s)'
end
end
if (@Employee!='' or @Employee!=null)
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Employeee in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Employee+''') s) '
end
else 
begin
set @whereStr=@whereStr+' and Employeee in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Employee+''') s) '
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end
select * into #ExpenseTrack from (
--Select p.COLUMN09 as Expense#,FORMAT(p.COLUMN04,@DateF) Date,p.COLUMN04 Dtt,p.COLUMN05 Memo,m.COLUMN06 Employee,'Advance Paid' Account,1120 AType,
--'0.00' BookingAmt,p.column07 PaidAmt,o.COLUMN03 OPUnit,'Advance Payment' Type,p.COLUMNa02 OPID,0 Project,m.COLUMN02 Employeee From FITABLE034 p 
--left join MATABLE010 m on m.COLUMN02=p.COLUMN06  and p.columna03=m.columna03 
--inner join CONTABLE007 o on o.COLUMN02=p.COLUMNa02 and p.columna03=o.columna03
--where isnull((p.COLUMNA13),0)=0 and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner and p.COLUMN04  between @FromDate  and @ToDate AND p.COLUMN03 = 'ADVANCE PAYMENT' 
Select p.COLUMN09 as Expense#,FORMAT(p.COLUMN04,@DateF) Date,p.COLUMN04 Dtt,p.COLUMN05 Memo, 
--(case --when (p.COLUMN03='ADVANCE PAYMENT' AND (F20.COLUMN11=22305 OR F20.COLUMN11=22334)) then V.COLUMN05
       M.COLUMN06 Employee,
      --when (p.COLUMN03='ADVANCE PAYMENT' AND (F20.COLUMN11=22335)) then C.COLUMN05 end) Employee,
     'Advance Paid' Account,1120 AType,
	 --iif(p.COLUMN03 = 'JOURNAL ENTRY',p.column07,'0.00') BookingAmt,iif(p.COLUMN03 = 'JOURNAL ENTRY',p.column08,p.column07) PaidAmt,
	 p.column08 BookingAmt,p.column07  PaidAmt,
	 o.COLUMN03 OPUnit,'Advance Payment' Type,p.COLUMNa02 OPID,
0 Project,m.COLUMN02 Employeee From FITABLE034 p 
left join MATABLE010 m on m.COLUMN02=p.COLUMN06  and p.columna03=m.columna03 
--left join SATABLE002 S2 ON S2.COLUMN02=p.COLUMN06  and S2.columna03=m.columna03
inner join CONTABLE007 o on o.COLUMN02=p.COLUMNa02 and p.columna03=o.columna03
left JOIN FITABLE020 f20 on cast(f20.COLUMN04 as nvarchar(250))=cast(p.COLUMN09 as nvarchar(250)) AND f20.COLUMNA03=p.COLUMNA03 AND f20.COLUMNA02=p.COLUMNA02 AND f20.COLUMN07=p.COLUMN06 AND f20.COLUMN05=p.COLUMN04
--left outer join SATABLE001 V on V.COLUMN02=p.COLUMN06 and v.COLUMNA03 = p.COLUMNA03 
--left outer join SATABLE002 C on C.COLUMN02=p.COLUMN06 and c.COLUMNA03 = p.COLUMNA03 
left JOIN FITABLE031 f31 on cast(f31.COLUMN04 as nvarchar(250))=cast(p.COLUMN09 as nvarchar(250)) AND f31.COLUMNA03=p.COLUMNA03 AND f31.COLUMNA02=p.COLUMNA02
left JOIN FITABLE032 f32 on f32.COLUMN12 = f31.COLUMN01 AND f31.COLUMNA03=f32.COLUMNA03 AND f31.COLUMNA02=f32.COLUMNA02 and F32.COLUMN14='22492' and F32.COLUMN14!='0' and F32.COLUMN03=p.COLUMN10 and F32.COLUMN07=p.COLUMN06
where isnull((p.COLUMNA13),0)=0 and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner and p.COLUMN04  between @FromDate  and @ToDate AND (p.COLUMN03='ADVANCE PAYMENT' OR p.COLUMN03 = 'JOURNAL ENTRY' or p.COLUMN03 = 'PAYMENT VOUCHER')
union all

Select p.COLUMN04 as Expense#,FORMAT(p.COLUMN05,@DateF) Date,p.COLUMN05 Dtt,p.COLUMN13 Memo,m.COLUMN06 Employee,f1.COLUMN04 Account,f1.COLUMN02 AType,
'0.00' BookingAMt,p.COLUMN19 PaidAmt,o.COLUMN03 OPUnit,c.COLUMN04 Type,p.COLUMN16 OPID,p.COLUMN09 Project,m.COLUMN02 Employeee From FITABLE045 p 
left outer join FITABLE046 f on f.COLUMN06=p.COLUMN01 
left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06  and m.COLUMNA03 = p.COLUMNA03
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN16
left outer join CONTABLE0010 c on c.COLUMN02=p.COLUMN03
left outer join  FITABLE001 f1 ON f1.COLUMN02 = p.COLUMN12 AND f1.COLUMNA03 = p.COLUMNA03 where isnull((p.COLUMNA13),0)=0 and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner and p.COLUMN05  between @FromDate  and @ToDate 

--and f1.COLUMN07=22344 

union all
select  p.COLUMN09 as Expense#,FORMAT(p.COLUMN04,@DateF) Date, p.COLUMN04 Dtt,
(case when p.COLUMN03='EXPENSE' then f12.COLUMN07
when p.COLUMN03='PAYMENT VOUCHER' then f22.COLUMN04
when p.COLUMN03='RECEIPT VOUCHER' then f24.COLUMN04
when p.COLUMN03='JOURNAL ENTRY' then  p.COLUMN05 else null end) Memo,
(case when (p.COLUMN03='PAYMENT VOUCHER' AND (F20.COLUMN11=22305 OR F20.COLUMN11=22334)) then V.COLUMN05
 when (p.COLUMN03='PAYMENT VOUCHER' AND (F20.COLUMN11=22492 )) then M.COLUMN06
 when (p.COLUMN03='RECEIPT VOUCHER' AND (F23.COLUMN20=22335)) then C.COLUMN05
 when (p.COLUMN03='RECEIPT VOUCHER' AND (F23.COLUMN20=22492)) then M.COLUMN06
 when (p.COLUMN03='JOURNAL ENTRY'   AND (F32.COLUMN14=22335)) then C.COLUMN05
 when (p.COLUMN03='JOURNAL ENTRY'   AND (F32.COLUMN14=22305)) then V.COLUMN05
 when (p.COLUMN03='JOURNAL ENTRY'   AND (F32.COLUMN14=22306)) then m1.COLUMN06
 when (p.COLUMN03='JOURNAL ENTRY'   AND (F32.COLUMN14=22334)) then V .COLUMN05
 when (p.COLUMN03='JOURNAL ENTRY') then m.COLUMN06
 when (p.COLUMN03='EXPENSE' ) then M.COLUMN06 end) Employee,f1.COLUMN04 Account,f1.COLUMN02 AType,
 (case when p.COLUMN03='PAYMENT VOUCHER' then P.COLUMN08 else P.COLUMN07 end) BookingAmt,
 (case when p.COLUMN03='PAYMENT VOUCHER' then P.COLUMN07 else P.COLUMN08 end) PaidAmt,
 o.COLUMN03 OPUnit,p.COLUMN03 Type ,p.COLUMNA02  OPID,p.COLUMN11 Project,m.COLUMN02 Employeee  
from FITABLE036 p 
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMNA02
left outer join SATABLE001 V on V.COLUMN02=p.COLUMN06 and v.COLUMNA03 = p.COLUMNA03 
left outer join SATABLE002 C on C.COLUMN02=p.COLUMN06 and c.COLUMNA03 = p.COLUMNA03 
left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06 and m.COLUMNA03 = p.COLUMNA03
left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06 and m1.COLUMNA03 = p.COLUMNA03 and m1.COLUMN30 = 1
--left outer join SATABLE001 J on J.COLUMN02=p.COLUMN06 and J.COLUMNA03 = p.COLUMNA03  and J.COLUMN22= 22286
left outer join FITABLE012 f12 on cast(f12.COLUMN01 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and f12.COLUMNA02 = p.COLUMNA02 and f12.COLUMNA03 = p.COLUMNA03 and isnull(f12.COLUMNA13,0)=0
left outer join FITABLE021 f21 on cast(f12.COLUMN01 as nvarchar(250))=cast(f21.COLUMN06 as nvarchar(250)) and (f21.COLUMN03=p.COLUMN10 or p.COLUMN10 is null) and f12.COLUMNA02 = f21.COLUMNA02 and f12.COLUMNA03 = f21.COLUMNA03 and isnull(f21.COLUMNA13,0)=0 and f21.COLUMN04=p.COLUMN07
left outer join FITABLE020 f20 on cast(f20.COLUMN01 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and f20.COLUMNA02 = p.COLUMNA02 and f20.COLUMNA03 = p.COLUMNA03 and isnull(f20.COLUMNA13,0)=0
left outer join FITABLE023 f23 on cast(f23.COLUMN01 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and f23.COLUMNA02 = p.COLUMNA02 and f23.COLUMNA03 = p.COLUMNA03 and isnull(f23.COLUMNA13,0)=0
left outer join FITABLE022 f22 on f22.COLUMNA03=f20.COLUMNA03 and f22.COLUMN09=f20.COLUMN01 and (f22.COLUMN03=p.COLUMN10 or p.COLUMN10 is null) and f22.COLUMNA02 = p.COLUMNA02 and isnull(f22.COLUMNA13,0)=0 and f22.COLUMN17=p.COLUMN07
left outer join FITABLE024 f24 on f24.COLUMNA03=f23.COLUMNA03 and f24.COLUMN09=f23.COLUMN01 and (f24.COLUMN03=p.COLUMN10 or p.COLUMN10 is null) and f24.COLUMNA02 = p.COLUMNA02 and isnull(f24.COLUMNA13,0)=0 and f24.COLUMN18=p.COLUMN08
left outer join FITABLE031 F31 on F31.COLUMN04 = p.COLUMN09 and F31.COLUMNA03 = p.COLUMNA03 and isnull(F31.COLUMNA13,0)=0 and F31.COLUMN10 = p.COLUMNA02 and F31.COLUMN09 = p.COLUMN04
left outer join FITABLE032 F32 on F32.COLUMN12 = F31.COLUMN01 and cast(F32.COLUMN06 as nvarchar(250))= cast(p.COLUMN05 as nvarchar(250)) and F32.COLUMNA03 = F31.COLUMNA03 and F32.COLUMN03 = p.COLUMN10 and isnull(F32.COLUMNA13,0)=0  and F32.COLUMN07 = p.COLUMN06 --and F32.COLUMN05=p.COLUMN07 
and p.COLUMN03='JOURNAL ENTRY'
left outer join  FITABLE001 f1 ON f1.COLUMN02 = p.COLUMN10 AND f1.COLUMNA03 = p.COLUMNA03 where 
p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner  
and p.COLUMN04  between @FromDate  and @ToDate 
and f1.COLUMN07 in(22344 ,22408) and p.COLUMN10!=(1133) AND p.COLUMN03 IN('EXPENSE','PAYMENT VOUCHER','RECEIPT VOUCHER','JOURNAL ENTRY')
group by p.COLUMN09,p.COLUMN04,p.COLUMN03,f12.COLUMN07,f22.COLUMN04,f24.COLUMN04,p.COLUMN05,
F20.COLUMN11,F23.COLUMN20,F32.COLUMN14,V.COLUMN05,M.COLUMN06,C.COLUMN05,m1.COLUMN06,f1.COLUMN04,
f1.COLUMN02,o.COLUMN03,p.COLUMNA02,p.COLUMN11,m.COLUMN02,P.COLUMN08,P.COLUMN07,f12.COLUMN02,f22.COLUMN02,f24.COLUMN02,F32.COLUMN02,f21.COLUMN02

union all

select  p.COLUMN09 as Expense#,FORMAT(p.COLUMN03,@DateF) Date, p.COLUMN03 Dtt,
(case 
when p.COLUMN04='PAYMENT VOUCHER' then p.COLUMN07
when p.COLUMN04='RECEIPT VOUCHER' then p.COLUMN07
when p.COLUMN04='JOURNAL ENTRY' then F32.COLUMN06
else null end)
  Memo,
--(case 
--when (p.COLUMN04='PAYMENT VOUCHER' AND (F20.COLUMN11=22492 )) then M.COLUMN06
--when (p.COLUMN04='RECEIPT VOUCHER' AND (F23.COLUMN20=22492 )) then M.COLUMN06
--when (p.COLUMN04='JOURNAL ENTRY' AND (F32.COLUMN14=22492 )) then
 M.COLUMN06 Employee,f1.COLUMN04 Account,f1.COLUMN02 AType,
  P.COLUMN11 BookingAmt, P.COLUMN12  PaidAmt, o.COLUMN03 OPUnit,p.COLUMN04 Type ,p.COLUMNA02  OPID,p.COLUMN19 Project,m.COLUMN02 Employeee  
from FITABLE026 p 
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMNA02
left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06 and m.COLUMNA03 = p.COLUMNA03
left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06 and m1.COLUMNA03 = p.COLUMNA03 and m1.COLUMN30 = 1
left outer join FITABLE020 f20 on cast(f20.COLUMN01 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and f20.COLUMNA02 = p.COLUMNA02 and f20.COLUMNA03 = p.COLUMNA03 and isnull(f20.COLUMNA13,0)=0
left outer join FITABLE022 f22 on f22.COLUMNA03=f20.COLUMNA03 and f22.COLUMNA02=f20.COLUMNA02 and f22.COLUMN09=f20.COLUMN01 and f22.COLUMN17=p.COLUMN11 and isnull(f22.COLUMNA13,0)=0
left outer join FITABLE023 f23 on cast(f23.COLUMN01 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and f23.COLUMNA02 = p.COLUMNA02 and f23.COLUMNA03 = p.COLUMNA03 and isnull(f23.COLUMNA13,0)=0
left outer join FITABLE024 f24 on f24.COLUMNA03=f23.COLUMNA03 and f24.COLUMNA02=f23.COLUMNA02 and f24.COLUMN09=f23.COLUMN01  and f24.COLUMN18=p.COLUMN12 and isnull(f24.COLUMNA13,0)=0
left outer join FITABLE031 F31 on F31.COLUMN04 = p.COLUMN09 and F31.COLUMNA03 = p.COLUMNA03 and isnull(F31.COLUMNA13,0)=0 and F31.COLUMN10 = p.COLUMNA02
left outer join FITABLE032 F32 on F32.COLUMN12 = F31.COLUMN01 and F32.COLUMNA03 = F31.COLUMNA03 and F32.COLUMN03 = p.COLUMN08 and isnull(F32.COLUMNA13,0)=0  and F32.COLUMN07 = p.COLUMN06 
and p.COLUMN04='JOURNAL ENTRY'
--left outer join FITABLE012 f12 on cast(f12.COLUMN01 as nvarchar)=cast(p.COLUMN05 as nvarchar)
left outer join  FITABLE001 f1 ON f1.COLUMN02 = p.COLUMN08 AND f1.COLUMNA03 = p.COLUMNA03 where 
p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner  
and p.COLUMN03  between @FromDate  and @ToDate AND p.COLUMN04 IN ('PAYMENT VOUCHER','RECEIPT VOUCHER','JOURNAL ENTRY') 
AND (f20.COLUMN11 = 22492 OR f23.COLUMN20 = 22492 OR F32.COLUMN14 = 22492)

union all

select  p.COLUMN09 as Expense#,FORMAT(p.COLUMN03,@DateF) Date, p.COLUMN03 Dtt,
(case 
when p.COLUMN04='EXPENSE' then f12.COLUMN07
else null end)
  Memo,
(case 
when (p.COLUMN04='EXPENSE') then M.COLUMN06
end) Employee,f1.COLUMN04 Account,f1.COLUMN02 AType,
 (case 
 when p.COLUMN04='PAYMENT VOUCHER' then P.COLUMN12
 else P.COLUMN11 end)
 BookingAmt,
 (case 
  when p.COLUMN04='PAYMENT VOUCHER' then P.COLUMN11
  else P.COLUMN12 end) PaidAmt, o.COLUMN03 OPUnit,p.COLUMN04 Type ,p.COLUMNA02  OPID,p.COLUMN19 Project,m.COLUMN02 Employeee  
from FITABLE026 p 
left outer join CONTABLE007 o on o.COLUMN02=p.COLUMNA02
left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06 and m.COLUMNA03 = p.COLUMNA03
left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN06 and m1.COLUMNA03 = p.COLUMNA03 and m1.COLUMN30 = 1
left outer join FITABLE012 f12 on cast(f12.COLUMN01 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and f12.COLUMNA02 = p.COLUMNA02 and f12.COLUMNA03 = p.COLUMNA03 and isnull(f12.COLUMNA13,0)=0
left outer join  FITABLE001 f1 ON f1.COLUMN02 = p.COLUMN08 AND f1.COLUMNA03 = p.COLUMNA03 where 
p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner  
and p.COLUMN03 between @FromDate and @ToDate AND p.COLUMN04 IN ('EXPENSE') 
union all
Select p.COLUMN04 as Expense#,FORMAT(p.COLUMN05,@DateF) Date,p.COLUMN05 Dtt,p.COLUMN11 Memo,S2.COLUMN05 Employee,'Advance Received' Account,1119 AType,
F24.COLUMN05 BookingAmt,'0.00' PaidAmt,o.COLUMN03 OPUnit,'Advance Receipt' Type,p.COLUMNa02 OPID,0 Project,S2.COLUMN02 Employeee From FITABLE023 p 
LEFT JOIN FITABLE024 F24 ON F24.COLUMN09 = p.COLUMN01 AND F24.COLUMNA03 = p.COLUMNA03 AND ISNULL(F24.COLUMNA13,0)=0
--left join MATABLE010 m on m.COLUMN02=p.COLUMN06  and p.columna03=m.columna03
left join SATABLE002 S2 ON S2.COLUMN02=p.COLUMN08  and p.columna03=S2.columna03
inner join CONTABLE007 o on o.COLUMN02=p.COLUMNa02 and p.columna03=o.columna03
where isnull((p.COLUMNA13),0)=0 and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and p.COLUMNA03=@AcOwner and p.COLUMN05 between @FromDate and @ToDate AND p.COLUMN03 = 1386

) Query
--select * from #ExpenseTrack
set @Query1='select * from #ExpenseTrack'+@whereStr+'  order by  Employee,Dtt desc'
exec (@Query1) 
end







GO
