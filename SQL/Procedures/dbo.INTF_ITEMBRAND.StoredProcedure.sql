USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[INTF_ITEMBRAND]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[INTF_ITEMBRAND]
(
	@COLUMN02 NVARCHAR(250)
)
AS
	BEGIN
		DECLARE @DIRECTION NVARCHAR(10)='INSERT'
		DECLARE @INTF_ITEMBRAND TABLE
		(COLUMN01 INT IDENTITY (1,1),
		COLUMN02 NVARCHAR(250),
		COLUMN04 NVARCHAR(250),
		COLUMN05 NVARCHAR(250),
		COLUMN06 INT,
		COLUMNA02 INT,
		COLUMNA03 INT,
		COLUMNA06 DATETIME,
		COLUMNA07 DATETIME,
		COLUMNA08 INT,
		COLUMNA12 BIT,
		COLUMNA13 BIT)

		INSERT INTO @INTF_ITEMBRAND SELECT   
		IT.COLUMN02,
		IT.COLUMN09,
		IT.COLUMN09,
		IT.COLUMNA02,
		IT.COLUMNA02,
		IT.COLUMNA03,
		IT.COLUMNA06,
		IT.COLUMNA07,
		IT.COLUMNA08,
		IT.COLUMNA12,
		IT.COLUMNA13
		FROM INTFMGR_ITEMMASTER IT
		LEFT JOIN MATABLE005 M5 ON M5.COLUMN04=IT.COLUMN09 AND M5.COLUMNA03=IT.COLUMNA03 AND ISNULL(M5.COLUMNA13,0)=0
		WHERE LTRIM(IT.COLUMN09)!='' AND LTRIM(IT.COLUMN09)!='0'  AND IT.COLUMN02 = @COLUMN02
		DECLARE @CNTACCOUNTS INT ,@FLGACCONTS INT=1
		DECLARE @COLUMN01 NVARCHAR(250),@COLUMN06 NVARCHAR(250),@COLUMN07 NVARCHAR(250),@COLUMNA02 NVARCHAR(250),@COLUMNA08 NVARCHAR(250),
				@COLUMNA03 NVARCHAR(250),@COLUMNA06 NVARCHAR(250),@COLUMNA07 NVARCHAR(250),@COLUMNA12 NVARCHAR(250),
				@COLUMNA13 NVARCHAR(250)
		SELECT @CNTACCOUNTS=COUNT(1) FROM @INTF_ITEMBRAND
		PRINT @CNTACCOUNTS
		IF @CNTACCOUNTS >= 1
		BEGIN
			WHILE (@FLGACCONTS <=@CNTACCOUNTS)
			BEGIN
				SELECT @COLUMN01=COLUMN01,@COLUMN02=COLUMN02,@COLUMN07=COLUMN04,@COLUMN06=COLUMN06,@COLUMNA02=COLUMNA02,@COLUMNA03=COLUMNA03,@COLUMNA06=COLUMNA06,
						@COLUMNA07=COLUMNA07,@COLUMNA08=COLUMNA08,@COLUMNA12=COLUMNA12,@COLUMNA13=COLUMNA13
				FROM @INTF_ITEMBRAND  WHERE COLUMN01=@FLGACCONTS
				SET @DIRECTION='INSERT'
				IF EXISTS(SELECT (COLUMN01) FROM MATABLE005 WHERE COLUMN04=@COLUMN07 AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
				BEGIN 
					SELECT @COLUMN01=COLUMN02 FROM MATABLE005 WHERE COLUMN04=@COLUMN07 AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0
					SET @DIRECTION='UPDATE'
				END
				EXEC [usp_MAS_TP_MATABLE005]
					@COLUMN01,1263,  @COLUMN07,	@COLUMN07,  0,  NULL, NULL,NULL,  0,  0,  0,   '', 
					NULL  , NULL,   @COLUMNA03,   NULL,	NULL,   @COLUMNA06,  @COLUMNA07,  
					@COLUMNA08  ,   NULL  ,  NULL  ,NULL,   @COLUMNA12,  @COLUMNA13,	NULL,  NULL,  NULL, 
					NULL  ,  NULL  ,  NULL  ,	NULL,  NULL,  NULL, 	NULL,  NULL,   NULL,   
					NULL  ,  NULL  ,  NULL  ,	NULL,  NULL,  NULL,	NULL,  NULL,  NULL,  
					NULL  ,   @DIRECTION  ,       'MATABLE005'  , NULL

				SET @FLGACCONTS = @FLGACCONTS + 1
			END
		END
	END


GO
