USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_Sales_AvlQty]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAL_TP_Sales_AvlQty]
(
	@ItemID int,@OPUnit nvarchar(250)=null,@Units nvarchar(250)=null,@Lot nvarchar(250)=null,@Location nvarchar(250)=null,
	@RowId nvarchar(250)=null,@AcOwner nvarchar(250)=null,@HIID nvarchar(250)=null,@Avalqty decimal(18,2)=null,@Invqty decimal(18,2)=null,@LID nvarchar(250)=null,@Project nvarchar(250)=null
)

AS
BEGIN
set @Lot=(iif(@Lot='',0,isnull(@Lot,0)))

set @Location=(iif(@Location='',0,isnull(@Location,0)))
set @Units=(iif(@Units='',10000,isnull(@Units,10000)))
set @Project=(iif(@Project='',0,isnull(@Project,0)))
set @HIID=(select COLUMN15 from SATABLE010 where COLUMN02=@RowId and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
set @LID=(select COLUMN01 from SATABLE010 where COLUMN02=@RowId and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
set @Invqty=(select isnull(sum(isnull(COLUMN10,0)),0)COLUMN10 from SATABLE010 where COLUMN05=@ItemID and COLUMN22=@Units 
and iif(COLUMN38='',0,isnull(COLUMN38,0))=@Location and COLUMN22=@Units and 
iif(COLUMN31='',0,isnull(COLUMN31,0))=@Lot and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0 and COLUMNA02=@OPUnit and COLUMN15=@HIID and COLUMN02>=cast(@RowId as int)
and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
if exists( select * from fitable038 where column06=@ItemID and column05=@LID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
begin
set @Avalqty=(select sum(isnull(COLUMN04,0))COLUMN04 from FITABLE010 where COLUMN03=@ItemID and COLUMN13=@OPUnit and COLUMN19 in(select column08 from fitable038 where column06=@ItemID and column05=@LID and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0) and iif(COLUMN21='',0,isnull(COLUMN21,0))=@Location and
iif(COLUMN22='',0,isnull(COLUMN22,0))=@Lot and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0 and iif(COLUMN23='',0,isnull(COLUMN23,0))=@Project)
end
else
begin
set @Avalqty=(select sum(isnull(COLUMN04,0))COLUMN04 from FITABLE010 where COLUMN03=@ItemID and COLUMN13=@OPUnit and COLUMN19=@Units and iif(COLUMN21='',0,isnull(COLUMN21,0))=@Location and
iif(COLUMN22='',0,isnull(COLUMN22,0))=@Lot and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0 and iif(COLUMN23='',0,isnull(COLUMN23,0))=@Project)
end
set @Invqty=(iif(cast(@Invqty as nvarchar(250))='',0,isnull(@Invqty,0)))
set @Avalqty=(iif(cast(@Avalqty as nvarchar(250))='',0,isnull(@Avalqty,0)))
select  @Invqty+@Avalqty COLUMN04
END


GO
