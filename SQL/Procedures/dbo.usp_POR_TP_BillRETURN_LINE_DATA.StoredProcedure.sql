USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_BillRETURN_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_POR_TP_BillRETURN_LINE_DATA]
(
	@PurchaseOrderID int
)

AS
BEGIN
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
	declare @Query1 nvarchar(max)					
select p.COLUMN03,p.COLUMN05,p.COLUMN07,p.COLUMN27,p.COLUMN26,p.COLUMN09,cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))COLUMN25,cast((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))+((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01))) as decimal(18,2))COLUMN11,p.COLUMN17,cast((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01) as decimal(18,2) ) tax,p.COLUMN38 into #temp6 from(					
SELECT  
     iif(im.column06!='',im.column06,b.COLUMN04) COLUMN03, b.COLUMN05 COLUMN05, iif(im.column06!='',isnull(im.COLUMN07,0),isnull(b.COLUMN09,0)) COLUMN07, iif(im.column06!='',im.COLUMN08,b.COLUMN19) COLUMN27, b.COLUMN18 COLUMN26,
	 b.COLUMN11 COLUMN09, b.COLUMN12 COLUMN25,
	 cast(((cast(isnull(m.column17,0) as decimal(18,2))*(0.01)))*(iif(b.COLUMN31=22556,cast(b.COLUMN12 as decimal(18,2))-(cast(b.COLUMN12 as decimal(18,2))*cast(b.COLUMN32 as decimal(18,2))*0.01), cast(b.COLUMN12 as decimal(18,2))-cast(b.COLUMN32 as decimal(18,2))))+cast(b.COLUMN12 as decimal(18,2)) as decimal(18,2)) COLUMN11,
	 b.column27 COLUMN17,cast(((cast(isnull(m.column17,0) as decimal(18,2))*(0.01)))*((iif(b.COLUMN31=22556,cast(b.COLUMN12 as decimal(18,2))-(cast(b.COLUMN12 as decimal(18,2))*cast(b.COLUMN32 as decimal(18,2))*0.01), cast(b.COLUMN12 as decimal(18,2))-cast(b.COLUMN32 as decimal(18,2))))) as decimal(18,2)) tax,b.column32 COLUMN10,b.column31 COLUMN35,cast((iif(b.COLUMN31=22556,cast((cast(isnull(b.COLUMN12,0) as decimal(18,2))*(cast(isnull(b.COLUMN32,0) as decimal(18,2))*0.01)) as decimal(18,2)),b.COLUMN32)/b.COLUMN09) as decimal(18,2)) discount,isnull(b.column32,0) disc,m.column17 taxper,b.column35 COLUMN38
	--EMPHCS876  rajasekhar reddy patakota 8/8/2015 After clicking on RETURN button from purchase order, system is not considerig deleted lines of that purchase order and showing all lines 
	FROM PUTABLE006 b inner join PUTABLE005 a on a.COLUMN01=b.COLUMN13 left join MATABLE013 m on  m.column02=b.COLUMN18
	left join FITABLE038 im on  im.column06=b.COLUMN04 and im.column05=b.column01 and im.columnA03=b.columnA03 and im.columnA02=b.columnA02 and isnull(im.COLUMNA13,0)=0
	WHERE  a.COLUMN02=@PurchaseOrderID and b.COLUMNA13=0) p
set @Query1='select * from #temp6'
execute(@Query1)
END

GO
