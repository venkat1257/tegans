USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_REPORT_PENDINGJO]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAL_REPORT_PENDINGJO]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@JNO        nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@OPUnitStatus      int= null,
@AcOwner nvarchar(250)= null,
@Item      nvarchar(250)= null,
@DateF nvarchar(250)= null)
AS
BEGIN
if((@FromDate!='' and @ToDate!='' and @JNO!='' and @Jobber!='' and @Item!=''))
	begin
				SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select it.column04 from matable007 it where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED'
 and p.COLUMN09 between @FromDate and @ToDate and (p.COLUMN05=@Jobber and f.COLUMN04=@JNO and i.COLUMN03=@Item)  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end

	Else if(@FromDate!='' and @ToDate!='' and @Item!='')
	begin
		SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select column04 from matable007 where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED' and p.COLUMN09 between @FromDate and @ToDate and  i.COLUMN03=@Item  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end

 Else if(@FromDate!='' and @ToDate!='' and @JNO!='')
	begin
		SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select column04 from matable007 where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED' and p.COLUMN09 between @FromDate and @ToDate and  f.COLUMN04=@JNO  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end
 Else if(@FromDate!='' and @ToDate!=''  and @Jobber!='')
	begin
		SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select it.column04 from matable007 it where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED' and p.COLUMN09 between @FromDate and @ToDate and  p.COLUMN05=@Jobber  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end
 Else if(@FromDate!='' and @ToDate!='')
	begin
		SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select column04 from matable007 where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED' and p.COLUMN09 between @FromDate and @ToDate  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end
 Else if(@JNO!='' and @Jobber!='')
	begin
		SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select column04 from matable007 where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED' and p.COLUMN05=@Jobber and f.COLUMN04=@JNO  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end
 Else if(@JNO!='' or @Jobber!='')
	begin
		SELECT s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS JobOrder,FORMAT(p.[COLUMN09 ] ,@DateF) AS Date, (select column04 from matable007 where COLUMN02= i.COLUMN03) AS Item	,i.COLUMN08 as Qty,p.[COLUMN18 ] AS Status
	From	PUTABLE003 p join puTABLE004 i on i.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05   	   left outer join  MATABLE007 d on d.COLUMN02=i.COLUMN04   left outer join  SATABLE005		f on		f.COLUMN02=p.COLUMN06    where  p.column04 like'%jr%' and p.column18 !='RECEIVED' and p.column18 !='FULLY RECEIVED' and p.COLUMN05=@Jobber or f.COLUMN04=@JNO  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN09	desc
end
	
END

GO
