USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CUSTOMER_RECEIVABLES_ADV_IMPORT]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USE [MySmartSCM_SB_2016]
--GO
--/****** Object:  StoredProcedure [dbo].[CUSTOMER_RECEIVABLES_ADV_IMPORT]    Script Date: 4/11/2016 12:36:36 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE proc [dbo].[CUSTOMER_RECEIVABLES_ADV_IMPORT]
AS 
BEGIN
declare @AccountTable Table(RowIds int Identity(1,1),CUSTOMER_INTERNAL_ID INT,TotalValue decimal(18,2))
declare @REFERANCE nvarchar(50)
declare @DIRECTION nvarchar(50)='Insert'
declare @TABLENAME nvarchar(50)='FITABLE014'
declare @OPUNIT nvarchar(50)
declare @ACOWNER nvarchar(50)
declare @TRANSDATE nvarchar(50)=(select getdate())
declare @ReferredForm nvarchar(50)
declare @AdjustmntNo nvarchar(50)
declare @InternalID nvarchar(50)
declare @TotalValue nvarchar(50)
declare @ADJLINENO nvarchar(50)
declare @ITEM nvarchar(50)
declare @UOM nvarchar(50)
declare @QOH nvarchar(50)
declare @TOTVALUE nvarchar(50)
declare @INCREASE nvarchar(50)
declare @DECREASE nvarchar(50)
declare @REMANING nvarchar(50)
declare @CURRENTQTY nvarchar(50)
declare @ESTEMATEDPRICE nvarchar(50)
declare @LOT nvarchar(50)
declare @CREATEDBY nvarchar(50)
declare @flgAcconts int
declare @cntAccounts int

SELECT TOP 1 @OPUNIT=[OPERATING UNIT],@ACOWNER=[ACCOUNT OWNER] FROM [CUSTOMER_RECEIVABLE_ADV_IMPORT]
set @flgAcconts=1
Insert into @AccountTable select S.COLUMN02,C.AMOUNT from dbo.SATABLE002 S--where  DATEDIFF(DAY,Getdate(),expiry_date) <=15
INNER JOIN [CUSTOMER_RECEIVABLE_ADV_IMPORT] C ON C.[CUSTOMER NAME]=S.COLUMN05 AND S.COLUMNA03=@ACOWNER
select @cntAccounts=COUNT(1) from @AccountTable
print 'step 1'
print @cntAccounts
if @cntAccounts >= 1
begin
--declare @NO int=10000
--set @AdjustmntNo='IA'
while (@flgAcconts <=@cntAccounts)
begin
      select @InternalID=CUSTOMER_INTERNAL_ID, @TotalValue=TotalValue from @AccountTable where RowIds=@flgAcconts
	print 'step 2'
	  set @REFERANCE=(select isnull(max(column02),0)+1 from FITABLE026)

	  --ACCOUNT RECEIVABLE TABLE

--	  INSERT [dbo].[PUTABLE018] ( [COLUMN02], [COLUMN03], [COLUMN04], [COLUMN05], [COLUMN06], [COLUMN07], [COLUMN16], 
--[COLUMN08], [COLUMN13], [COLUMN09], [COLUMN10], [COLUMN11], [COLUMN12], [COLUMN14], [COLUMN15], [COLUMN17], [COLUMN18], 
--[COLUMNA01], [COLUMNA02], [COLUMNA03], [COLUMNA04], [COLUMNA05], [COLUMNA06], [COLUMNA07], [COLUMNA08], [COLUMNA09], 
--[COLUMNA10], [COLUMNA11], [COLUMNA12], [COLUMNA13], [COLUMNB01], [COLUMNB02], [COLUMNB03], [COLUMNB04], [COLUMNB05], 
--[COLUMNB06], [COLUMNB07], [COLUMNB08], [COLUMNB09], [COLUMNB10], [COLUMNB11], [COLUMNB12], [COLUMND01], [COLUMND02], 
--[COLUMND03], [COLUMND04], [COLUMND05], [COLUMND06], [COLUMND07], [COLUMND08], [COLUMND09], [COLUMND10]) 
--VALUES (@REFERANCE, N'3000', getdate(), NULL, N'Opening Balance', @InternalID, NULL, N'', N'Open', 
--@TotalValue, getdate(), NULL, @TotalValue, NULL, NULL, NULL, NULL,
-- NULL, 56750, 56608, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[FITABLE026] ([COLUMN02], [COLUMN03], [COLUMN04], [COLUMN05], [COLUMN06], [COLUMN07], [COLUMN08], 
[COLUMN09], [COLUMN10], [COLUMN11], [COLUMN12], [COLUMN13], [COLUMN14], [COLUMN15], [COLUMN16], [COLUMNA01], [COLUMNA02], 
[COLUMNA03], [COLUMNA04], [COLUMNA05], [COLUMNA06], [COLUMNA07], [COLUMNA08], [COLUMNA09], [COLUMNA10], [COLUMNA11], 
[COLUMNA12], [COLUMNA13], [COLUMNB01], [COLUMNB02], [COLUMNB03], [COLUMNB04], [COLUMNB05], [COLUMNB06], [COLUMNB07], 
[COLUMNB08], [COLUMNB09], [COLUMNB10], [COLUMNB11], [COLUMNB12], [COLUMND01], [COLUMND02], [COLUMND03], [COLUMND04], 
[COLUMND05], [COLUMND06], [COLUMND07], [COLUMND08], [COLUMND09], [COLUMND10], [column17], [COLUMN18], [COLUMN19]) 
VALUES (@REFERANCE, getdate(), N'Opening Balance', NULL, @InternalID, N'', N'1119', NULL, NULL, @TotalValue, NULL, NULL, @TotalValue, NULL, NULL, NULL, @OPUNIT, @ACOWNER, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)


	  set @flgAcconts=@flgAcconts+1
      end 
	  end
	  END
GO
