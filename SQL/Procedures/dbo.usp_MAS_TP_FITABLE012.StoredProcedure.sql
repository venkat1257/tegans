USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_MAS_TP_FITABLE012]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MAS_TP_FITABLE012]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
    @COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT,    @internalid nvarchar(250)=null, 
	@pid nvarchar(250)=null
)

AS
BEGIN
begin try
--Expense Form Chaneging to old model done by SRINIVAS 7/22/2015  
set @COLUMNA02=( CASE WHEN (@COLUMN10!= '' and @COLUMN10 is not null and @COLUMN10!=0 ) THEN @COLUMN10 else @COLUMNA02  END )
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE012_SequenceNo
--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
declare @tempSTR nvarchar(max)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Booking Values are Intiated for FITABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMN21 ,'')+','+isnull(  @COLUMN22,'')+','+isnull(  @COLUMN23,'')+','+isnull( @COLUMN24,'')+','+isnull(  @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into FITABLE012 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11, 
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  
   COLUMN21,  COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25, COLUMN26,  COLUMN27, COLUMN28, COLUMNA01, 
   COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
   COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, 
   COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10, @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19,   @COLUMN20, 
   @COLUMN21,  @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMNA01, 
   @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
   @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
   @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
   @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Booking Header Intiated Values are Created in FITABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
DECLARE @Projectactualcost DECIMAL(18,2), @Totbal DECIMAL(18,2),@balance DECIMAL(18,2),@Project  nvarchar(250)
set @internalid= (select COLUMN01 from FITABLE012 where COLUMN02=@COLUMN02)
set @Project=( @COLUMN25)
set @Totbal=( @COLUMN24)
--if(@COLUMN24!='')
--begin
--set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
--update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN24 as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
--end
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03)
--set @pid=((select max(isnull(column02,999)) from PUTABLE019)+1)
--Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMNA02, COLUMNA03, COLUMN20)
--values(isnull(@pid,999),@COLUMN08,@COLUMN09,@COLUMN17,'EXPENSE',@COLUMN16,@internalid,@Totbal,(@balance -@Totbal),@COLUMNA02, @COLUMNA03,@Project)
--update FITABLE001 set COLUMN10=(@balance -@Totbal),COLUMN09=@COLUMN09 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN17, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN09,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN16,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN07,   @COLUMN13 = @project,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

set @pid=((select max(isnull(column02,999)) from PUTABLE016)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Accounts Payable Values are Intiated for PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@pid,999)as nvarchar(250)) +','+'2000'+','+ cast(isnull(@COLUMN09,'') as nvarchar(250))+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+ cast('EXPENSE' as nvarchar(250))+','+ cast(isnull(@COLUMN16,'') as nvarchar(250))+','+  cast(isnull(@COLUMN08,'') as nvarchar(250))+','+cast(isnull(@COLUMN17,'') as nvarchar(250))+','+'OPEN'+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@Totbal,'')) as nvarchar(250))+','+cast(isnull(@COLUMN07,'') as nvarchar(250))+','+  cast(isnull( @COLUMNA02,'') as nvarchar(250))+','+cast(isnull( @COLUMNA03,'') as nvarchar(250)) +','+cast(isnull(@Project,'') as nvarchar(250)) +  '  '))
Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN12,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
values(@pid,2000,@COLUMN09,@internalid,'EXPENSE',@COLUMN16,@COLUMN08,@COLUMN17,'OPEN',@Totbal,@Totbal,@COLUMN07,@COLUMNA02, @COLUMNA03,@Project)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Accounts Payable Intiated Values are Created in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''))

--insert into PUTABLE011 

--( COLUMN02,  COLUMN03,    COLUMN05, COLUMN12,  COLUMN13,
--   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
--   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
--   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
--)
--values
--(  @COLUMN02A,  'EXPENCE TRACKING',     @COLUMN05,'4','4',
--   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
--   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
--   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
--   @COLUMND08, @COLUMND09, @COLUMND10
--)  
--set @internalid= (select column01 from FITABLE012 where COLUMN02=@COLUMN02)
--declare @balance decimal(18,2)
--set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03)
--set @pid=((select max(isnull(column02,999)) from PUTABLE019)+1)
--Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN10, COLUMN12,COLUMNA02, COLUMNA03, COLUMN20)
--values(isnull(@pid,10001),@COLUMN08,@COLUMN09,@COLUMN17,'EXPENSE',@internalid,@COLUMN06,(@balance -@COLUMN06),@COLUMNA02, @COLUMNA03,@COLUMN25)
--update FITABLE001 set COLUMN10=(@balance -@COLUMN06),COLUMN09=@COLUMN09 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03
--Expense Form Changing to old model done by SRINIVAS 7/22/2015  
--set @pid=((select max(column02) from PUTABLE016)+1)--Insert into putable016 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN10, COLUMN12, COLUMN14, COLUMN16,COLUMNA02, COLUMNA03) 
--values(	@pid,2000,@COLUMN09,@internalid,'EXPENSE',@COLUMN08,@COLUMN17,'PAID',@COLUMN06,@COLUMN06,@COLUMN16,@COLUMNA02, @COLUMNA03 )
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue = (SELECT COLUMN01 FROM FITABLE012 WHERE COLUMN02=@COLUMN02)
END

ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE012
END 
 
ELSE IF @Direction = 'Update'
BEGIN
--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
declare @pbalance decimal(18,2)
set @pbalance= (select COLUMN24 from FITABLE012 where COLUMN02=@COLUMN02)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Booking Values are Intiated for FITABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+
   isnull(@COLUMN21 ,'')+','+isnull(  @COLUMN22,'')+','+isnull(  @COLUMN23,'')+','+isnull( @COLUMN24,'')+','+isnull(  @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE FITABLE012 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,  
   --Expense Form Chaneging to old model done by SRINIVAS 7/22/2015  
   COLUMN08=@COLUMN08,    
   COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,
   COLUMN26=@COLUMN26,    COLUMN27=@COLUMN27, COLUMN28=@COLUMN28,   COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05, 
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01, 
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Booking Header Intiated Values are Created in FITABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
  
set @Project=( @COLUMN25)
set @internalid= (select COLUMN01 from FITABLE012 where COLUMN02=@COLUMN02)
delete from fitable064 where column03 = 'EXPENSE' and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @Totbal=( @COLUMN24)
--if(@COLUMN24!='')
--begin
--set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
--update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))+cast(@COLUMN24 as decimal(18,2))-cast(@pbalance as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
--end 
delete from  PUTABLE016  where COLUMN03=2000 and COLUMN05 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN09=@COLUMN17 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--delete from  PUTABLE019  where COLUMN03=@COLUMN08 and COLUMN08 in(@internalid) and COLUMN06='EXPENSE' and isnull(COLUMN20,0)=@Project and COLUMN05=@COLUMN17 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03)
--set @pid=((select max(isnull(column02,999)) from PUTABLE019)+1)
--Insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN12,COLUMNA02, COLUMNA03, COLUMN20)
--values(isnull(@pid,999),@COLUMN08,@COLUMN09,@COLUMN17,'EXPENSE',@COLUMN16,@internalid,@Totbal,(@balance -@Totbal),@COLUMNA02, @COLUMNA03,@Project)
--update FITABLE001 set COLUMN10=(cast(@balance as decimal(18,2)) -cast(@Totbal as decimal(18,2))+cast(@pbalance as decimal(18,2))),COLUMN09=@COLUMN09 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'EXPENSE',  @COLUMN04 = @COLUMN17, @COLUMN05 = @internalid,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN09,   @COLUMN08 = '22492',  @COLUMN09 = @COLUMN16,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN07,   @COLUMN13 = @project,	@COLUMN14 = @Totbal,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

set @pid=((select max(isnull(column02,999)) from PUTABLE016)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Accounts Payable Values are Intiated for PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@pid,999)as nvarchar(250)) +','+'2000'+','+ cast(isnull(@COLUMN09,'') as nvarchar(250))+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+ cast('EXPENSE' as nvarchar(250))+','+ cast(isnull(@COLUMN16,'') as nvarchar(250))+','+  cast(isnull(@COLUMN08,'') as nvarchar(250))+','+cast(isnull(@COLUMN17,'') as nvarchar(250))+','+'OPEN'+','+  cast(isnull(@Totbal,'') as nvarchar(250))+','+  cast((isnull(@Totbal,'')) as nvarchar(250))+','+cast(isnull(@COLUMN07,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +','+cast(isnull(@Project,'') as nvarchar(250)) +  '  '))
Insert into PUTABLE016(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN12,COLUMN14,  COLUMN18,COLUMNA02, COLUMNA03,COLUMN20)
values(@pid,2000,@COLUMN09,@internalid,'EXPENSE',@COLUMN16,@COLUMN08,@COLUMN17,'OPEN',@Totbal,@Totbal,@COLUMN07,@COLUMNA02, @COLUMNA03,@Project)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Header Update:Accounts Payable Intiated Values are Created in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''))
--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
DECLARE @MaxRownum INT,@id nvarchar(250), @DATE nvarchar(250), @vendor nvarchar(250), @memo nvarchar(250),
 @TYPE nvarchar(250),@AccType NVARCHAR(250),@NUMBER NVARCHAR(250), @Remainbal decimal(18,2), @TYPEID nvarchar(250), @Acc nvarchar(250),@ACCNAME nvarchar(250)
set @TYPEID= (select COLUMN17 from FITABLE012 where column02=@column02)
set @Project= (select COLUMN25 from FITABLE012 where column02=@column02)
set @NUMBER= (select COLUMN01 from FITABLE012 where column02=@column02)
set @vendor= (select column16 from FITABLE012 where column02=@column02)
set @DATE=(select column09 from FITABLE012 where column02=@column02)
     set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Booking Header Values are in FITABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   '1.Transaction No' +','+ cast(isnull(@TYPEID,'') as nvarchar(250))+','+'2.Header Internal ID'+','+  cast(isnull(@NUMBER,'') as nvarchar(250))+','+ '3.Payee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  '4.Date'+','+cast(isnull(@DATE,'') as nvarchar(250))+','+'5.Project'+','+  cast(isnull(@Project,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
 DECLARE @Initialrow INT=1
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE021 where COLUMN06 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE021 where COLUMN06 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN
		 set @Acc= (select column03 from FITABLE021 where COLUMN02=@id)
		 set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Booking Expense Deleted Account Summary '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   '1.Transaction No' +','+ cast(isnull(@TYPEID,'') as nvarchar(250))+','+'2.Header Internal ID'+','+  cast(isnull(@NUMBER,'') as nvarchar(250))+','+ '3.Payee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  '4.Date'+','+cast(isnull(@DATE,'') as nvarchar(250))+','+'5.Project'+','+  cast(isnull(@Project,'') as nvarchar(250))+','+'6.Account' +','+ cast(isnull(@Acc,'') as nvarchar(250))+','+'7.Account Name'+','+  cast(isnull(@ACCNAME,'') as nvarchar(250))+','+'8.Type'+','+'EXPENSE'+','+'9.Account Type'+','+cast(isnull(@TYPE,'') as nvarchar(250))+','+cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
 set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
IF(@TYPE=22266)
BEGIN
delete from PUTABLE019  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='EXPENSE'  and COLUMN08 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Cash
ELSE IF(@TYPE=22409)
BEGIN
	delete from FITABLE052  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='EXPENSE'  and COLUMN09 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
    SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
    update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	delete from  PUTABLE017  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE'  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Payable
ELSE IF(@TYPE=22263)
BEGIN
	delete from  PUTABLE016  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE' and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	delete from  PUTABLE018  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Equity
ELSE IF(@TYPE=22330)
BEGIN
	delete from  FITABLE029  where COLUMN03='EXPENSE' and COLUMN07=@Acc and COLUMN11 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Expense
ELSE IF(@TYPE=22344)
BEGIN 
	delete from  FITABLE036  where COLUMN03='EXPENSE' and COLUMN10=@Acc and COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	delete from  FITABLE026  where COLUMN04='EXPENSE' and  COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	delete from  FITABLE034  where COLUMN03='EXPENSE' and  COLUMN09 =@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	delete from  FITABLE028  where COLUMN05='EXPENSE' and  COLUMN06 =@TYPEID and COLUMN03=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Income
ELSE IF(@TYPE=22405)
BEGIN
	delete from  FITABLE025  where COLUMN04='EXPENSE' and COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	delete from  FITABLE025  where COLUMN04='EXPENSE' and  COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	delete from  FITABLE035  where COLUMN03='EXPENSE' and  COLUMN09=@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	delete from  FITABLE036  where COLUMN03='EXPENSE' and COLUMN09=@TYPEID   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
ELSE IF(@TYPE=22410)
BEGIN
	delete from  FITABLE060  where COLUMN03='EXPENSE' and  COLUMN09 =@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
ELSE IF(@TYPE=22411)
BEGIN
	delete from  FITABLE063  where COLUMN03='EXPENSE' and  COLUMN09 =@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
UPDATE FITABLE021 SET COLUMNA13=1 WHERE COLUMN02=@id
 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end
        CLOSE cur1
        deallocate cur1   

set @ReturnValue = 1
END
 

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE012 SET COLUMNA13=1 WHERE COLUMN02=@COLUMN02
      set @COLUMNA02=(select COLUMNA02 from FITABLE012 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA03=(select COLUMNA03 from FITABLE012 WHERE COLUMN02 = @COLUMN02)
--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
	  set @COLUMN06=(select COLUMN01 from FITABLE012 WHERE COLUMN02 = @COLUMN02)
set @TYPEID= (select COLUMN17 from FITABLE012 where COLUMN02 = @COLUMN02)
set @Project= (select COLUMN25 from FITABLE012 where COLUMN02 = @COLUMN02)
set @NUMBER= (select COLUMN01 from FITABLE012 where  COLUMN02 = @COLUMN02)
set @vendor= (select column16 from FITABLE012 where COLUMN02 = @COLUMN02)
set @DATE=(select column09 from FITABLE012 where COLUMN02 = @COLUMN02)
set @COLUMN08=(select column08 from FITABLE012 where COLUMN02 = @COLUMN02)
set @internalid= (select COLUMN01 from FITABLE012 where COLUMN02 = @COLUMN02)
set @Totbal=(select COLUMN24 from FITABLE012 where COLUMN02 = @COLUMN02)
set @COLUMN24=(select COLUMN24 from FITABLE012 where COLUMN02 = @COLUMN02)
--if(@COLUMN24!='')
--begin
--set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project and COLUMNA03=@COLUMNA03)
--update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as decimal(18,2))-cast(@Totbal as decimal(18,2))) where column02=@Project and COLUMNA03=@COLUMNA03
--end
delete from fitable064 where column03 = 'EXPENSE' and column05 = @internalid and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @pbalance= (select COLUMN24 from FITABLE012 where COLUMN02=@COLUMN02)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Delete:Accounts Payable Deleted Values are for PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   '1.Account'+','+'2000'+','+ '2.Header Internal ID'+','+  cast(isnull(@internalid,'') as nvarchar(250))+','+'3.Type'+','+ cast('EXPENSE' as nvarchar(250))+','+'4.Project'+','+  cast(isnull(@Project,'') as nvarchar(250))+','+'5.Transaction No'+','+  cast(isnull(@TYPEID,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250))  +  '  '))
delete from  PUTABLE016  where COLUMN03=2000 and COLUMN05 in(@internalid) and COLUMN06='EXPENSE'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Header Delete:Accounts Payable Record Deleted in PUTABLE016 Table '+'' + CHAR(13)+CHAR(10) + ''))
--delete from  PUTABLE019  where COLUMN03=@COLUMN08 and COLUMN08 in(@internalid) and COLUMN06='EXPENSE' and isnull(COLUMN20,0)=@Project and COLUMN05=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
set @balance=(select column10 from FITABLE001 where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03)
--update FITABLE001 set COLUMN10=(cast(@balance as decimal(18,2)) +cast(@Totbal as decimal(18,2))) where COLUMN02=@COLUMN08 and COLUMNA03=@COLUMNA03

          set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Update:Expense Booking Header Values are in FITABLE012 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   '1.Transaction No' +','+ cast(isnull(@TYPEID,'') as nvarchar(250))+','+'2.Header Internal ID'+','+  cast(isnull(@NUMBER,'') as nvarchar(250))+','+ '3.Payee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  '4.Date'+','+cast(isnull(@DATE,'') as nvarchar(250))+','+'5.Project'+','+  cast(isnull(@Project,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
  set @Initialrow=(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE021 where COLUMN06 in(@COLUMN06) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE021 where COLUMN06 in(@COLUMN06) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN
		 set @Acc= (select column03 from FITABLE021 where COLUMN02=@id)
		 set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Header Delete:Expense Booking Expense Deleted Account Summary '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   '1.Transaction No' +','+ cast(isnull(@TYPEID,'') as nvarchar(250))+','+'2.Header Internal ID'+','+  cast(isnull(@NUMBER,'') as nvarchar(250))+','+ '3.Payee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  '4.Date'+','+cast(isnull(@DATE,'') as nvarchar(250))+','+'5.Project'+','+  cast(isnull(@Project,'') as nvarchar(250))+','+'6.Account' +','+ cast(isnull(@Acc,'') as nvarchar(250))+','+'7.Account Name'+','+  cast(isnull(@ACCNAME,'') as nvarchar(250))+','+'8.Type'+','+'EXPENSE'+','+'9.Account Type'+','+cast(isnull(@TYPE,'') as nvarchar(250))+','+cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
 IF(@TYPE=22266)
BEGIN
	delete from PUTABLE019  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='EXPENSE' and COLUMN08 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
    SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
    update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Cash
ELSE IF(@TYPE=22409)
BEGIN
	delete from FITABLE052  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='EXPENSE'  and COLUMN09 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
    SET @Remainbal=(ISNULL(@Remainbal,0)+CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
    update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	delete from  PUTABLE017  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Payable
ELSE IF(@TYPE=22263)
BEGIN
	delete from  PUTABLE016  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE' and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	delete from  PUTABLE018  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Equity
ELSE IF(@TYPE=22330)
BEGIN
	delete from  FITABLE029  where COLUMN03='EXPENSE' and COLUMN07=@Acc and COLUMN11 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Expense
ELSE IF(@TYPE=22344)
BEGIN
	delete from  FITABLE036  where COLUMN03='EXPENSE' and COLUMN10=@Acc and COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	delete from  FITABLE026  where COLUMN04='EXPENSE' and  COLUMN09 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	delete from  FITABLE034  where COLUMN03='EXPENSE' and  COLUMN09 =@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	delete from  FITABLE028  where COLUMN05='EXPENSE' and  COLUMN06 =@TYPEID and COLUMN03=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Income
ELSE IF(@TYPE=22405)
BEGIN
	delete from  FITABLE025  where COLUMN04='EXPENSE' and COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	delete from  FITABLE025  where COLUMN04='EXPENSE' and  COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	delete from  FITABLE035  where COLUMN03='EXPENSE' and  COLUMN09=@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	delete from  FITABLE036  where COLUMN03='EXPENSE' and COLUMN09=@TYPEID   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
ELSE IF(@TYPE=22410)
BEGIN
	delete from  FITABLE060  where COLUMN03='EXPENSE' and  COLUMN09 =@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
ELSE IF(@TYPE=22411)
BEGIN
	delete from  FITABLE063  where COLUMN03='EXPENSE' and  COLUMN09 =@TYPEID and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
UPDATE FITABLE021 SET COLUMNA13=1 WHERE COLUMN02=@id
 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end
        CLOSE cur1
        deallocate cur1   

		 
END
 exec [CheckDirectory] @tempSTR,'usp_MAS_TP_FITABLE012.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
	--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_FITABLE012.txt',0
end catch
end





GO
