USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ServiceProIUD]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 create proc [dbo].[ServiceProIUD]
 (
         @COLUMN02 bigint=null,
		 @COLUMN03 nvarchar(250),
		 @COLUMN04 nvarchar(250),
		 @COLUMN05 int,
		 @COLUMN06 nvarchar(250),
		 @COLUMN07 nvarchar(250),
		 @COLUMN08 nvarchar(250),
		 @COLUMN09 nvarchar(250),
		 @COLUMN10 nvarchar(250),
		 @COLUMN11 nvarchar(250),
		 @COLUMN12 nvarchar(250),
		 @COLUMN13 date,
		 @COLUMN14 date,
		 @COLUMN15 nvarchar(250),
		 @COLUMN16 int,
		 @column17 nvarchar(250)=null,
		 @column18 nvarchar(250)=null,
		 @column19 nvarchar(250)=null,
		 @column20 nvarchar(250)=null,
		 @COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
		 @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
		 @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null, 
		 @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null, 
		 @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
		 @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
		 @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
		 @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
		 @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null, 
		 @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
		 @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
		 @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250)  		 			
 )
 As
 Begin
 begin try
 IF @Direction = 'Insert'
    begin
	 Insert into SETABLE023 (COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMN17,COLUMN18,COLUMN19,COLUMN20,COLUMNA03 )
	  values ( @COLUMN03, @COLUMN04, @COLUMN05, @COLUMN06, @COLUMN07, @COLUMN08, @COLUMN09, @COLUMN10, @COLUMN11, @COLUMN12, @COLUMN13, @COLUMN14, @COLUMN15, @COLUMN16, @COLUMN17, @COLUMN18, @COLUMN19,@COLUMN20,@COLUMNA03 )
    end
 IF @Direction = 'Update'
    begin 
	   update SETABLE023 set
       COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
       COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
       COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16 
       where COLUMN02=@COLUMN02
	end
 else IF @Direction = 'Delete'
    begin
	delete SETABLE023 where COLUMN02=@COLUMN02
	end 

end try

begin catch
    
end catch

  end

GO
