USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_RR_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_SAR_TP_RR_LINE_DATA]
( @ReceiptID int)
AS
BEGIN
	SELECT  
	b.COLUMN05 COLUMN03, b.COLUMN06 COLUMN05,(isnull(b.column10,0)-sum(isnull(sl.column07,0))) COLUMN07,b.COLUMN22 COLUMN27,b.COLUMN21 COLUMN26,b.COLUMN13 COLUMN09,
	((isnull(b.column10,0)-sum(isnull(sl.column07,0)))*isnull(b.COLUMN13,0)) COLUMN25,(cast((cast(((isnull(b.column10,0)-sum(isnull(sl.column07,0)))*isnull(b.COLUMN13,0)) as decimal(18,2))+
	cast(((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*((isnull(b.column10,0)-sum(isnull(sl.column07,0)))*isnull(b.COLUMN13,0))) as decimal(18,2))) as decimal(18,2))) COLUMN11,(cast(((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*cast(((isnull(b.column10,0)-sum(isnull(sl.column07,0)))*isnull(b.COLUMN13,0)) as decimal(18,2))) as decimal(18,2))) aCOLUMN25,isnull(m.column07,0) aCOLUMN26
	FROM SATABLE009 a inner join SATABLE010 b on a.COLUMN01=b.COLUMN15 and isnull(b.COLUMNA13,0)=0
	left join SATABLE005 sh on  sh.column33=a.COLUMN02 left join SATABLE006 sl on  sl.column19=sh.COLUMN01 and sl.column03=b.column05 and isnull(sl.COLUMNA13,0)=0
	left join MATABLE013 m on  m.column02=b.COLUMN21
	WHERE  a.COLUMN02= @ReceiptID and isnull(a.COLUMNA13,0)=0
	 group by b.COLUMN05,b.COLUMN06,b.COLUMN10,b.COLUMN22,m.column07,b.COLUMN21,b.COLUMN13,b.column14,b.column19,b.column23
	having (isnull(b.column10,0)-sum(isnull(sl.column07,0)))>0
END
GO
