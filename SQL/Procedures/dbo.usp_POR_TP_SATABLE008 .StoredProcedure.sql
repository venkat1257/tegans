USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_SATABLE008 ]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_POR_TP_SATABLE008 ]
 (
 @COLUMN02    NVARCHAR(250),       @COLUMN03    NVARCHAR(250)=NULL,  @COLUMN04    NVARCHAR(250)=NULL, 
 @COLUMN05    NVARCHAR(250)=NULL,  @COLUMN06    NVARCHAR(250)=NULL,  @COLUMN07    NVARCHAR(250)=NULL, 
 @COLUMN08    NVARCHAR(250)=NULL,  @COLUMN09    NVARCHAR(250)=NULL,  @COLUMN10    NVARCHAR(250)=NULL, 
 @COLUMN11    NVARCHAR(250)=NULL,  @COLUMN12    NVARCHAR(250)=NULL,  @COLUMN13    NVARCHAR(250)=NULL, 
 @COLUMN14    NVARCHAR(250)=NULL,  @COLUMN15    NVARCHAR(250)=NULL,  @COLUMN16    NVARCHAR(250)=NULL, 
 @COLUMN17    NVARCHAR(250)=NULL,  @COLUMN18    NVARCHAR(250)=NULL,  @COLUMN19    NVARCHAR(250)=NULL, 
 --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
 @COLUMN20    NVARCHAR(250)=NULL,  @COLUMN21    NVARCHAR(250)=NULL,  @COLUMN22    NVARCHAR(250)=NULL,
 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
 @COLUMN23    NVARCHAR(250)=NULL,  @COLUMN24    NVARCHAR(250)=NULL,  @COLUMN25    NVARCHAR(250)=NULL,
 @COLUMN26   nvarchar(250)=null,   @COLUMN28   nvarchar(250)=null,
 @COLUMNA01   VARCHAR(100)=NULL,   @COLUMNA02   VARCHAR(100)=NULL,   @COLUMNA03   VARCHAR(100)=NULL, 
 @COLUMNA04   VARCHAR(100)=NULL,   @COLUMNA05   VARCHAR(100)=NULL,   @COLUMNA06   NVARCHAR(250)=NULL, 
 @COLUMNA07   NVARCHAR(250)=NULL,  @COLUMNA08   VARCHAR(100)=NULL,   @COLUMNA09   NVARCHAR(250)=NULL, 
 @COLUMNA10   NVARCHAR(250)=NULL,  @COLUMNA11   VARCHAR(100)=NULL,   @COLUMNA12   NVARCHAR(250)=NULL, 
 @COLUMNA13   NVARCHAR(250)=NULL,  @COLUMNB01   NVARCHAR(250)=NULL,  @COLUMNB02   NVARCHAR(250)=NULL, 
 @COLUMNB03   NVARCHAR(250)=NULL,  @COLUMNB04   NVARCHAR(250)=NULL,  @COLUMNB05   NVARCHAR(250)=NULL, 
 @COLUMNB06   NVARCHAR(250)=NULL,  @COLUMNB07   NVARCHAR(250)=NULL,  @COLUMNB08   NVARCHAR(250)=NULL, 
 @COLUMNB09   NVARCHAR(250)=NULL,  @COLUMNB10   NVARCHAR(250)=NULL,  @COLUMNB11   VARCHAR(100)=NULL, 
 @COLUMNB12   VARCHAR(100)=NULL,   @COLUMND01   NVARCHAR(250)=NULL,  @COLUMND02   NVARCHAR(250)=NULL, 
 @COLUMND03   NVARCHAR(250)=NULL,  @COLUMND04   NVARCHAR(250)=NULL,  @COLUMND05   NVARCHAR(250)=NULL, 
 @COLUMND06   NVARCHAR(250)=NULL,  @COLUMND07   NVARCHAR(250)=NULL,  @COLUMND08   NVARCHAR(250)=NULL, 
 @COLUMND09   NVARCHAR(250)=NULL,  @COLUMND10   VARCHAR(100)=NULL,   @Direction   NVARCHAR(250), 
  --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
  --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
 @TabelName   NVARCHAR(250)=NULL,  @ReturnValue NVARCHAR(250)=NULL,  @LotData  XML=null,  @UOMData  XML=null, 
 @Result      NVARCHAR(250)=NULL,  @VendorID    NVARCHAR(250)=NULL) 
AS 
  BEGIN 
      BEGIN try 
  	  --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
	  set @COLUMN19=(select iif((@COLUMN19!='' and @COLUMN19>0),@COLUMN19,(select max(column02) from fitable037 where column07=1 and column08=1)))
          IF @Direction = 'Insert' 
           BEGIN
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
declare @FRMID int, @soid int,@date nvarchar(250), @lotno nvarchar(250)=null,@Project  nvarchar(250)=null
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
--set @COLUMN14 =(select MAX(COLUMN01) from  SATABLE007);
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE008_SequenceNo
set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
set @date= (SELECT COLUMN08 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
set @COLUMN15 =(select COLUMN15 from  SATABLE007 Where COLUMN01=@COLUMN14 );
set @COLUMN16 =(select COLUMN16 from  SATABLE007 Where COLUMN01=@COLUMN14 );
set @COLUMN17 =(select COLUMN17 from  SATABLE007 Where COLUMN01=@COLUMN14 );
set @COLUMN18 =(select COLUMN18 from  SATABLE007 Where COLUMN01=@COLUMN14 );
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
set @Project =(select COLUMN22 from  SATABLE007 Where COLUMN01=@COLUMN14 );
set @soid =(select COLUMN06 from  SATABLE007 Where COLUMN01=@COLUMN14 );
declare @location nvarchar(250)=null
set @location =(select COLUMN23 from  SATABLE007 Where COLUMN01=@COLUMN14 );
set @Location = (case when (cast(isnull(@COLUMN28,'') as nvarchar(250))!='' and @COLUMN28!=0) then @COLUMN28 else @Location end)
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @location=(case when @location='' then '0' when isnull(@location,'0')='0' then '0'  else @location end)
set @lotno=(@COLUMN24)
set @lotno=(case when @lotno='' then '0' when isnull(@lotno,'0')='0' then '0'  else @lotno end)

set @COLUMN11 =(CAST(@COLUMN11 AS decimal(18,2))- CAST(@COLUMN09 AS decimal(18,2)))
  set @COLUMNA02=( CASE WHEN (@COLUMN14!= '' and @COLUMN14 is not null and @COLUMN14!= '0' ) THEN (select COLUMN15 from SATABLE007 
     where COLUMN01=@COLUMN14) else @COLUMNA02  END )
insert into SATABLE008 
(
   --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
   COLUMN02,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07, COLUMN09,  COLUMN10,  COLUMN11, COLUMN12,  COLUMN13,  
   COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18, COLUMN19,  COLUMN20,  COLUMN21, COLUMN22,  COLUMN23,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN28,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02,
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12,
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
   @COLUMN02,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07, @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13, 
   @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19,  @COLUMN19,  @COLUMN09,  @COLUMN22,  @COLUMN23, 
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN28,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07,@COLUMNA08 , @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS745 Rajasekhar reddy patakota Item Issue Edit is not updating Inventory Asset
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN26,0)>0)
begin
set @COLUMN08=(select sum(isnull(COLUMN09,0)) from SATABLE008 where   COLUMN26=@COLUMN26 and isnull(COLUMNA13,0)=0)
end
else
begin
--set @COLUMN08=( (select max(isnull(COLUMN08,0)) from SATABLE008 where   COLUMN04=@COLUMN04 and COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN06 in(@soid))))
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @COLUMN08=( (select sum(isnull(COLUMN09,0)) from SATABLE008 where   COLUMN04=@COLUMN04 and isnull(COLUMN19,0)=isnull(@COLUMN19,0) and iif(COLUMN24='',0,isnull(COLUMN24,0))=isnull(@lotno,0) and isnull(COLUMNA13,0)=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN06 in(@soid))))
end
--set @COLUMN08=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2)))
update SATABLE008 set COLUMN08=@COLUMN08 where column02=@COLUMN02
declare @lIID nvarchar(250)
set @lIID=(select COLUMN01 from SATABLE008 WHERE COLUMN02=@COLUMN02)
if exists(select column01 from SATABLE008 where column14=@COLUMN14)
					begin
declare @SID int
	declare @TQty decimal(18,2)
	declare @IFQty decimal(18,2)
	declare @IVQty decimal(18,2)
SET @SID=(SELECT COLUMN06 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN26,0)>0)
begin
set @COLUMN08=(select isnull(COLUMN12,0) from SATABLE006 where  COLUMN02=@COLUMN26 and isnull(COLUMNA13,0)=0)
UPDATE SATABLE006 SET  COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where  COLUMN02=@COLUMN26 and isnull(COLUMNA13,0)=0
end
else
begin
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @COLUMN08=(select isnull(COLUMN12,0) from SATABLE006 where isnull(COLUMNA13,0)=0 and isnull(COLUMN27,0)=isnull(@COLUMN19,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) AND  COLUMN19= (select COLUMN01 from SATABLE005 where COLUMN02= @SID) and COLUMN03=@COLUMN04)
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
UPDATE SATABLE006 SET  COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where COLUMN03=@COLUMN04 and isnull(COLUMN27,0)=isnull(@COLUMN19,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) and isnull(COLUMNA13,0)=0 and 
	COLUMN19=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))
end
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
SET @TQty=(SELECT sum(ISNULL(COLUMN07,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID))
SET @IFQty=(SELECT sum(ISNULL(COLUMN12,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID))
SET @IVQty=(SELECT sum(ISNULL(COLUMN13,0)) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID))
IF(@TQty=(SELECT sum(ISNULL(COLUMN09,0)) FROM SATABLE008 WHERE COLUMNA13=0 AND COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN06=@SID) ))
begin
if(@TQty=@IVQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@IVQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=98)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=99)
end
end
ELSE 
BEGIN
if(@TQty=@IVQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@IVQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=96)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=97)
end
end
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=110) where COLUMN01=@COLUMN14	
--rajasekhar patakota 25/7/2015 Duplicate Updation For Sales Order Billed Quantity
--set @COLUMN08=(select isnull(COLUMN12,0) from SATABLE006 where COLUMN19= (select COLUMN01 from SATABLE005 where COLUMN02= @SID) and COLUMN03=@COLUMN04)
--UPDATE SATABLE006 SET  COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where COLUMN03=@COLUMN04 and 
--	COLUMN19=(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))
--UPDATE SATABLE008 SET  COLUMN08=(CAST(@COLUMN08 AS int)+ CAST(@COLUMN09 AS int)) where  COLUMN02=@COLUMN02
UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) 
WHERE COLUMN14=@COLUMN04 and COLUMN02 =(select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14))
declare @Qty_On_Hand decimal(18,2)
declare @Qty_Commit decimal(18,2)
declare @Qty_Order decimal(18,2)
declare @Qty_AVL decimal(18,2)
declare @Qty_BACK decimal(18,2)
declare @Qty decimal(18,2)
declare @WIP decimal(18,2)
declare @Memo nvarchar(250)
declare @TrackQty bit, @uomselection nvarchar(250), @itemissueno nvarchar(250)
set @itemissueno=(select COLUMN04 from  SATABLE007 Where COLUMN01=@COLUMN14)
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)

--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if(@LotData is not null and datalength(@LotData)>0)
begin
EXEC usp_PUR_TP_InsertLOT @LotData,'RETURN ISSUE',@itemissueno,@lIID,@COLUMN04,@COLUMNA02,@COLUMNA03
end
if(@TrackQty=1)
		begin
		--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens		
if(@LotData is not null  and datalength(@LotData)>0)
begin
exec usp_PUR_TP_InventoryLOT @itemissueno,@lIID,@COLUMN04,@COLUMN09,@COLUMNA02,@COLUMN19,@COLUMNA03,'Insert',@location,@lotno
end
else
begin
set @Qty_BACK =0
--EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
begin
 set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)- cast(@COLUMN09 as decimal(18,2));
 set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
 set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
 declare @AvgPrice decimal(18,2)
 set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
 set @Qty=cast(@COLUMN09 as decimal(18,2));
 set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
 set @Qty_AVL=(@Qty_On_Hand-@Qty_Commit)
 if(@Qty_AVL<0)
 begin
 set @Qty_AVL=0
 set @Qty_BACK =@Qty_Commit
 set @Qty_Commit=0
 end
if(@FRMID=1284)
	Begin
	--EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
	set @Qty_AVL=@Qty_On_Hand-(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
		UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand,
		--COLUMN18=(@WIP-@Qty),
		 COLUMN12=(cast		((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))-(cast(@COLUMN09 as decimal(18,2))*cast(@AvgPrice as decimal(18,2))))	   WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
   End
else
	begin
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
		set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)
		UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05 =@Qty_Commit, COLUMN07 =@Qty_BACK, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))-(cast(@COLUMN09 as decimal(18,2))*cast(@AvgPrice as decimal(18,2))))	 
  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
    end
     --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19   AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno 
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19   AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
end
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
else
begin
declare @newIID int,@tmpnewIID1 int,@ASSET decimal(18,2) 
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)
if(@TrackQty=1)
begin
SET @AvgPrice=(ISNULL(@COLUMN12,0))
--set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
SET @ASSET=(-CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03
)
values
(
  @newIID,@COLUMN04,-CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,2)),-CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,2)), @ASSET,@AvgPrice,@COLUMN19,  @lotno, @location,@COLUMN15,@COLUMNA02,@COLUMNA03 )
end
END
end
end
SET @VendorID=(SELECT COLUMN05 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
SET @Memo=(SELECT COLUMN13 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
declare @newID int
declare @tmpnewID1 int 
			set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @newID=10000
end
    if (@FRMID=1276)
	begin
	set @COLUMN06=('Item Issue')
	set @COLUMN08=('Inventory Issed and Not Billed')
	end
    else if (@FRMID=1354)
	begin
	set @COLUMN06=('Return Issue')
	end
	else
	begin
	set @COLUMN06=('JobOrder Issue')
	set @COLUMN08= ('Inventory Issed and Not Billed')
	end
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)
		if(@TrackQty=1)
		begin
		--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
		--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
		if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN ISSUE' and COLUMN04=@itemissueno and COLUMN05=@lIID and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN15 and COLUMNA03=@COLUMNA03)
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN04) and COLUMN13=@COLUMN15 AND isnull(COLUMN19,0)=ISNULL(@COLUMN19,10000) and isnull(COLUMN21,0)=@Location and COLUMN22 in(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN ISSUE' and COLUMN04=@itemissueno and COLUMN05=@lIID and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN15 and COLUMNA03=@COLUMNA03))
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND isnull(COLUMN19,0)=ISNULL(@COLUMN19,10000)  AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0))
			end
		set @lIID=(select COLUMN01 from SATABLE008 WHERE COLUMN02=@COLUMN02)
insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
--EMPHCS1785 rajasekhar reddy patakota 21/07/2016 Rate Calculation with inclusive of tax in Purchase
   @newID,'1000',@date ,@COLUMN14,@COLUMN06, @VendorID,@COLUMN08,@Memo,@COLUMN13,@COLUMN13,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN04, @lIID, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)end
set @ReturnValue = 1
end
else
begin
return 0
end
END
          IF @Direction = 'Select' 
            BEGIN 
                SELECT * 
                FROM   satable008 
            END 

          IF @Direction = 'Update' 
            BEGIN 
			--DECLARE @SOID INT 
                SET @COLUMN15 =(SELECT column15 FROM   satable007  WHERE  column01 = @COLUMN14); 
                SET @COLUMN16 =(SELECT column16 FROM   satable007 WHERE  column01 = @COLUMN14); 
                SET @COLUMN17 =(SELECT column17 FROM   satable007 WHERE  column01 = @COLUMN14); 
                SET @COLUMN18 =(SELECT column18  FROM   satable007  WHERE  column01 = @COLUMN14); 
                SET @SOID =(SELECT COLUMN06  FROM   satable007  WHERE  column01 = @COLUMN14); 
				set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
				set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
				set @date= (SELECT COLUMN08 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @Project =(select COLUMN22 from  SATABLE007 Where COLUMN01=@COLUMN14 );
				--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				set @location =(select COLUMN23 from  SATABLE007 Where COLUMN01=@COLUMN14 );
				set @Location = (case when (cast(isnull(@COLUMN28,'') as nvarchar(250))!='' and @COLUMN28!=0) then @COLUMN28 else @Location end)
				set @location=(case when @location='' then '0' when isnull(@location,'0')='0' then '0'  else @location end)
				set @lotno=(@COLUMN24)
				set @lotno=(case when @lotno='' then '0' when isnull(@lotno,'0')='0' then '0'  else @lotno end)
                SET @COLUMNA02=( CASE 
                                   WHEN ( @COLUMN14 != '' AND @COLUMN14 IS NOT NULL AND @COLUMN14 != '0' ) 
								   THEN (SELECT   column15    FROM  satable007  WHERE  column01 = @COLUMN14) 
                                   ELSE @COLUMNA02 
                                 END ) 

                UPDATE       SATABLE008
SET                COLUMN02 = @COLUMN02, COLUMN03 = @COLUMN03, COLUMN04 = @COLUMN04, COLUMN05 = @COLUMN05, COLUMN06 = @COLUMN06, 
                         COLUMN07 = @COLUMN07, COLUMN08 = @COLUMN08, COLUMN09 = @COLUMN09, COLUMN10 = @COLUMN10, COLUMN11 = @COLUMN11, 
                         COLUMN12 = @COLUMN12, COLUMN13 = @COLUMN13, COLUMN14 = @COLUMN14, COLUMN15 = @COLUMN15, COLUMN16 = @COLUMN16, 
                         --EMPHCS923 rajasekhar reddy patakota 11/8/2015 uom condition checking in Item Issue
			             COLUMN19 = @COLUMN19,   COLUMN20 =@COLUMN19,    COLUMN21 =@COLUMN09,    COLUMN22 =@COLUMN22,    COLUMN23=  @COLUMN23, 
						--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
						 COLUMN24 =@COLUMN24,    COLUMN25=  @COLUMN25,   COLUMN26  = @COLUMN26,  COLUMN28  = @COLUMN28,  
						 COLUMNA01 = @COLUMNA01, COLUMNA02 = @COLUMNA02, COLUMNA03 = @COLUMNA03, COLUMNA04 = @COLUMNA04, 
                         COLUMNA05 = @COLUMNA05,   COLUMNA07 = @COLUMNA07, COLUMNA08 = @COLUMNA08, COLUMNA09 = @COLUMNA09, 
                         COLUMNA10 = @COLUMNA10, COLUMNA11 = @COLUMNA11, COLUMNA12 = @COLUMNA12, COLUMNA13 = @COLUMNA13, COLUMNB01 = @COLUMNB01, 
                         COLUMNB02 = @COLUMNB02, COLUMNB03 = @COLUMNB03, COLUMNB04 = @COLUMNB04, COLUMNB05 = @COLUMNB05, COLUMNB06 = @COLUMNB06, 
                         COLUMNB07 = @COLUMNB07, COLUMNB08 = @COLUMNB08, COLUMNB09 = @COLUMNB09, COLUMNB10 = @COLUMNB10, COLUMNB11 = @COLUMNB11, 
                         COLUMNB12 = @COLUMNB12, COLUMND01 = @COLUMND01, COLUMND02 = @COLUMND02, COLUMND03 = @COLUMND03, COLUMND04 = @COLUMND04, 
                         COLUMND05 = @COLUMND05, COLUMND06 = @COLUMND06, COLUMND07 = @COLUMND07, COLUMND08 = @COLUMND08, COLUMND09 = @COLUMND09, 
                         COLUMND10 = @COLUMND10
WHERE        (COLUMN02 = @COLUMN02)
declare @SORecQty DECIMAL(18,2),@ItemRecQty DECIMAL(18,2),@RecQty DECIMAL(18,2),@BQty DECIMAL(18,2),@JRStatus NVARCHAR(250)
	set @lIID=(select COLUMN01 from SATABLE008 WHERE COLUMN02=@COLUMN02)
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN26,0)>0)
begin
set @SORecQty=(select isnull(COLUMN12,0) from SATABLE006 where  COLUMN02=@COLUMN26 and isnull(COLUMNA13,0)=0)
UPDATE SATABLE006 SET   COLUMN12=(CAST(@SORecQty AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where  COLUMN02=@COLUMN26 and isnull(COLUMNA13,0)=0
set @ItemRecQty=(select sum(isnull(COLUMN09,0)) from SATABLE008 where  COLUMN26=@COLUMN26 and isnull(COLUMNA13,0)=0)
end
else
begin
--EMPHCS745 Rajasekhar reddy patakota Item Issue Edit is not updating Inventory Asset
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
                set @SORecQty=(select isnull(COLUMN12,0) from SATABLE006 where COLUMNA13=0 and isnull(COLUMN27,0)=isnull(@COLUMN19,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02= @SOID) and COLUMN03=@COLUMN04)
UPDATE SATABLE006 SET   COLUMN12=(CAST(@SORecQty AS decimal(18,2))+ CAST(@COLUMN09 AS decimal(18,2))) where COLUMN03=@COLUMN04 and isnull(COLUMN27,0)=isnull(@COLUMN19,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@lotno,0) and isnull(COLUMNA13,0)=0 and COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN02= (@SOID))
--EMPHCS745 Rajasekhar reddy patakota Item Issue Edit is not updating Inventory Asset
set @ItemRecQty=( (select sum(isnull(COLUMN09,0)) from SATABLE008 where   COLUMN04=@COLUMN04 and isnull(COLUMN19,0)=isnull(@COLUMN19,0) and iif(COLUMN24='',0,isnull(COLUMN24,0))=isnull(@lotno,0) and columna13=0 and  COLUMN14 in(select COLUMN01 from  SATABLE007 where COLUMN06 in(@SOID))))
end
--set @ItemRecQty=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@ItemRecQty AS decimal(18,2)))
set @COLUMN11 =(CAST(@COLUMN07 AS decimal(18,2))- (CAST(@ItemRecQty AS decimal(18,2))))
update SATABLE008 set COLUMN08=@ItemRecQty,COLUMN11=@COLUMN11 where column02=@COLUMN02
UPDATE PUTABLE013 SET COLUMN12=@ItemRecQty WHERE COLUMN14=@COLUMN04 and COLUMN02 in (select COLUMN01 from SATABLE005 where COLUMN02 in (@SOID))

SET @SID=(SELECT COLUMN06 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
SET @TQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID))
SET @RecQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID))
SET @BQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMNA13=0 AND COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@SID))
set @FRMID= (SELECT COLUMN03 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
--EMPHCS856 rajasekhar reddy patakota 5/8/2015 Deleted row condition checking in item issue
IF(@TQty=(SELECT sum(COLUMN09) FROM SATABLE008 WHERE COLUMNA13=0 AND COLUMN14 in(select COLUMN01 from SATABLE007 where COLUMN06=@SID) ))
begin
if(@TQty=@BQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@BQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=98)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=99)
end
end

ELSE 
BEGIN
if(@TQty=@BQty)
begin
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=100)
end
else if(@BQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=96)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=97)
end
end

UPDATE SATABLE005 set COLUMN16=@Result where  COLUMN02 in(select COLUMN06 from SATABLE007 where COLUMN01= @COLUMN14)
--EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
UPDATE SATABLE007 set COLUMN14=(select COLUMN04 from CONTABLE025 where COLUMN02=110) where COLUMN01=@COLUMN14	
DECLARE @Qty_On_Hand1 DECIMAL(18,2),@Qty_Cmtd DECIMAL(18,2),@ReceiptType NVARCHAR(250)
 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
set @itemissueno=(select COLUMN04 from  SATABLE007 Where COLUMN01=@COLUMN14)
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if(@LotData is not null  and datalength(@LotData)>0)
begin
EXEC usp_PUR_TP_InsertLOT @LotData,'RETURN ISSUE',@itemissueno,@lIID,@COLUMN04,@COLUMNA02,@COLUMNA03
end
else
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@itemissueno and COLUMN06=@COLUMN04 and COLUMN05=@lIID and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
if( @TrackQty=1 or  @TrackQty='1' or @TrackQty='True')
begin
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens		
if(@LotData is not null and datalength(@LotData)>0)
begin
exec usp_PUR_TP_InventoryLOT @itemissueno,@lIID,@COLUMN04,@COLUMN09,@COLUMNA02,@COLUMN19,@COLUMNA03,'Insert',@location,@lotno
end
else
begin
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
--EMPHCS1222  Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
--if(@Qty_On_Hand1>=0)
begin
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
 set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))- cast(@COLUMN09 as decimal(18,2)));
set @Qty_Cmtd=( cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)));
set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-cast(@COLUMN09 as DECIMAL(18,2)))
--if(@Qty_Cmtd>=cast(@COLUMN09 as DECIMAL(18,2)))
--Begin
--set @Qty_Cmtd = (cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))- cast(@COLUMN09 as DECIMAL(18,2)));
--set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))+ cast(@COLUMN09 as DECIMAL(18,2)));
--end
--else if(@Qty_Cmtd<cast(@COLUMN09 as DECIMAL(18,2)))
--begin
--set @Qty_Cmtd = (-cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))+ cast(@COLUMN09 as DECIMAL(18,2)));
--set @Qty_On_Hand=(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15)as DECIMAL(18,2))- cast(@COLUMN09 as DECIMAL(18,2)));
--end
set @Qty_Avl = cast(@Qty_On_Hand-@Qty_Cmtd as DECIMAL(18,2));
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05=@Qty_Cmtd, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as DECIMAL(18,2))-((cast(@COLUMN09 as DECIMAL(18,2)))*(cast(@AvgPrice as DECIMAL(18,2))))) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
end
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
else
begin
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)
if(@TrackQty=1)
begin
SET @AvgPrice=(ISNULL(@COLUMN12,0))
--set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13  AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
--IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
SET @ASSET=(-CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03
)
values
(
  @newIID,@COLUMN04,-CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,2)),-CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,2)), @ASSET,@AvgPrice,@COLUMN19,  @lotno, @location,@COLUMN15,@COLUMNA02,@COLUMNA03 )
end
END
end
END
set @COLUMN05= (@date);
declare @activetranid nvarchar(250)=null, @activetranno nvarchar(250)=null
set @activetranid=(select COLUMN01 from  SATABLE007 where COLUMN01=@COLUMN14)
    set @activetranno=(select COLUMN06 from  SATABLE007 where COLUMN01=@COLUMN14);
--set @COLUMN02=(select COLUMN01 from  SATABLE007 where COLUMN01=@COLUMN14); 
	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
    set @COLUMN20 =(select COLUMN15 from  SATABLE007 Where COLUMN01=@COLUMN14 );
    set @COLUMN21 =(select COLUMN16 from  SATABLE007 Where COLUMN01=@COLUMN14 );
    set @COLUMN22 =(select COLUMN17 from  SATABLE007 Where COLUMN01=@COLUMN14 );
    set @COLUMN23 =(select COLUMN18 from  SATABLE007 Where COLUMN01=@COLUMN14 );
	if (@FRMID=1276)
	begin
	set @ReceiptType=('Item Issue')
	end
    else if (@FRMID=1354)
	begin
	set @ReceiptType=('Return Issue')
	end
	else
	begin
	set @ReceiptType=('JobOrder Issue')
	end
	
insert into PUTABLE011 
( 
   COLUMN02,COLUMN03,  COLUMN04, COLUMN05, COLUMN07,COLUMN08, COLUMN10, COLUMN15,  COLUMN16,  COLUMN20 ,COLUMN21, COLUMN22 ,COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
--	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
   @activetranid,  @ReceiptType,   @activetranno,  @COLUMN05,@COLUMN06,@COLUMN08,@COLUMN05, '2','2',  @COLUMN20 ,@COLUMN21, @COLUMN22 ,@COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)

SET @VendorID=(SELECT COLUMN05 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
SET @memo=(SELECT COLUMN13 FROM SATABLE007 WHERE COLUMN01=@COLUMN14)
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @newID=10000
end
    if (@FRMID=1276)
	begin
	set @COLUMN06=('Item Issue')
	set @COLUMN08=('Inventory Issed and Not Billed')
	end
    else if (@FRMID=1354)
	begin
	set @COLUMN06=('Return Issue')
	end
	else
	begin
	set @COLUMN06=('JobOrder Issue')
	set @COLUMN08= ('Inventory Issed and Not Billed')
	end
	set @COLUMN04=(select COLUMN04 from  SATABLE008 where COLUMN02=@COLUMN02);
	--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND COLUMN19=@COLUMN19  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
	--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
		if EXISTS(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN ISSUE' and COLUMN04=@itemissueno and COLUMN05=@lIID and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN15 and COLUMNA03=@COLUMNA03)
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE COLUMN03=(@COLUMN04) and COLUMN13=@COLUMN15 AND isnull(COLUMN19,0)=ISNULL(@COLUMN19,10000) and isnull(COLUMN21,0)=@Location and COLUMN22 in(SELECT COLUMN12 FROM FITABLE038 WHERE COLUMN03='RETURN ISSUE' and COLUMN04=@itemissueno and COLUMN05=@lIID and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN15 and COLUMNA03=@COLUMNA03))
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN15 AND isnull(COLUMN19,0)=ISNULL(@COLUMN19,10000)  AND isnull(COLUMN21,0)=isnull(@location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0))
			end
	set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN04)
		if(@TrackQty=1)
		begin
		set @lIID=(select COLUMN01 from SATABLE008 WHERE COLUMN02=@COLUMN02)
insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
	--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	--EMPHCS1785 rajasekhar reddy patakota 21/07/2016 Rate Calculation with inclusive of tax in Purchase
    @newID,'1000', @date ,@COLUMN14,@COLUMN06, @VendorID,@COLUMN08,@memo,@COLUMN13,@COLUMN13,@COLUMNA02,@Project,
	@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
    @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
    --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	@COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN04, @lIID, @COLUMND07,
	@COLUMND08, @COLUMND09, @COLUMND10
)
end
            END 
          ELSE IF @Direction = 'Delete' 
            BEGIN 
                UPDATE satable008  SET    columna13 = @COLUMNA13  WHERE  column02 = @COLUMN02 
            END 
      END try 

      BEGIN catch 
	  	--EMPHCS718 After edit and saving item issue - the inventory asset amount is getting deducted done by srinivas 7/22/2015
	   DECLARE @tempSTR NVARCHAR(MAX)
	   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_SATABLE008.txt',0

          IF NOT EXISTS(SELECT column01 
                        FROM   satable008 
                        WHERE  column14 = @COLUMN14) 
            BEGIN 
                DELETE satable007 
                WHERE  column01 = @COLUMN14 

                RETURN 0 
            END 

          RETURN 0 
      END catch 
  END 







GO
