USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE017]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE017]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
set @COLUMN04=(cast(@COLUMN04 as decimal)-cast(@COLUMN05 as decimal))
   set @COLUMN09 =(select MAX(COLUMN01) from  FITABLE016);
   set @COLUMN10 =(select COLUMN10 from  FITABLE016 Where COLUMN01=@COLUMN09 );
   set @COLUMN11 =(select COLUMN11 from  FITABLE016 Where COLUMN01=@COLUMN09 );
   set @COLUMN12 =(select COLUMN12 from  FITABLE016 Where COLUMN01=@COLUMN09 );
   set @COLUMN13 =(select COLUMN13 from  FITABLE016 Where COLUMN01=@COLUMN09 );
insert into FITABLE017
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13, COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, 
   COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
   COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, 
   COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13, 
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10,
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, 
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, 
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  

declare @newIID int
declare @AccNo nvarchar(250)=null 
declare @Number nvarchar(250)=null 
declare @Status nvarchar(250)=null 
declare @PaymentType nvarchar(250)=null 
declare @RefForm nvarchar(250)=null 
declare @tmpnewIID int 
set @RefForm=(select COLUMN03 from  FITABLE016 Where COLUMN01=@COLUMN09 )
set @PaymentType=(select COLUMN06 from  FITABLE016 Where COLUMN01=@COLUMN09 )
set @AccNo=(select COLUMN07 from  FITABLE016 Where COLUMN01=@COLUMN09 )
set @Number=(select COLUMN04 from  FITABLE016 Where COLUMN01=@COLUMN09 )


if((@COLUMN03=@COLUMN05) or ((cast(@COLUMN04 as int))=0))
begin
set @Status='PAID'
end
else
begin
set @Status='PARTIALLY PAID'
end

if(@PaymentType='22335')
begin
set @tmpnewIID=(select MAX(COLUMN02) from PUTABLE018)
if(@tmpnewIID>0)
		begin
		set @newIID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1
		end
else
		begin
		set @newIID=100000
		end
insert into PUTABLE018 
(
				COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,   COLUMN09,  COLUMN11,  COLUMN12,
  				COLUMN13,  COLUMNA02, COLUMNA03,COLUMN16
)
values
(
				@newIID,   @AccNo,	  getdate(),	 @Number,  'JOURNAL ENTRY',   @COLUMN07 ,@COLUMN06,  
				@COLUMN03, @COLUMN05, @COLUMN04,	 @Status,  @COLUMNA02,        @COLUMNA03,4008
)
UPDATE FITABLE001 SET COLUMN10=((select column10 from FITABLE001 where column02= @AccNo) + @COLUMN05) WHERE COLUMN02=@AccNo
end
else
begin
set @tmpnewIID=(select MAX(COLUMN02) from PUTABLE016)
if(@tmpnewIID>0)
		begin
		set @newIID=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		end
else
		begin
		set @newIID=100000
		end
insert into PUTABLE016 
(
				COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,   COLUMN08,COLUMN09,   COLUMN10,    COLUMN12,
  				COLUMN13,  COLUMN14,  COLUMN18,  COLUMNA02, COLUMNA03
)
values
(
				@newIID,   @AccNo,	  getdate(), @COLUMN09,  'JOURNAL ENTRY',   4008,  @Number,   @Status,									                      @COLUMN03, @COLUMN05, @COLUMN04, @COLUMN06,   @COLUMNA02,       @COLUMNA03
)

if(@PaymentType='22305')
begin
update PUTABLE016 set COLUMN07=@COLUMN07 where COLUMN02=@newIID
end
else if(@PaymentType='22334')
begin
update PUTABLE016 set COLUMN07=@COLUMN07 where COLUMN02=@newIID
end
else if(@PaymentType='22335')
begin
update PUTABLE016 set COLUMN15=@COLUMN07 where COLUMN02=@newIID
end
else if(@PaymentType='22306')
begin
update PUTABLE016 set COLUMN16=@COLUMN07 where COLUMN02=@newIID
end

UPDATE FITABLE001 SET COLUMN10=((select column10 from FITABLE001 where column02= @AccNo) - @COLUMN05) WHERE COLUMN02=@AccNo
end


set @tmpnewIID=(select MAX(COLUMN02) from PUTABLE019)
if(@tmpnewIID>0)
		begin
		set @newIID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
		end
else
		begin
		set @newIID=100000
		end
insert into PUTABLE019 
(
				COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08, COLUMN09,  COLUMN11,  COLUMN12,
  				COLUMN13,  COLUMNA02, COLUMNA03
)
values
(
				@newIID,   @AccNo,	  getdate(),	 @Number,  'JOURNAL ENTRY',   @COLUMN07 ,@COLUMN06,   'OPENING BALANCE', @COLUMN03, @COLUMN04,@RefForm,  @COLUMNA02,@COLUMNA03
)
END

IF @Direction = 'Select'
BEGIN
select * from FITABLE017
END 

IF @Direction = 'Update'
BEGIN
   set @COLUMN10 =(select COLUMN10 from  FITABLE016 Where COLUMN01=@COLUMN09 );
   set @COLUMN11 =(select COLUMN11 from  FITABLE016 Where COLUMN01=@COLUMN09 );
   set @COLUMN12 =(select COLUMN12 from  FITABLE016 Where COLUMN01=@COLUMN09 );
   set @COLUMN13 =(select COLUMN13 from  FITABLE016 Where COLUMN01=@COLUMN09 );
UPDATE FITABLE017 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMNA01=@COLUMNA01, 
    COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA06=@COLUMNA06,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
END
 

else IF @Direction = 'Delete'

BEGIN
UPDATE FITABLE017 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END

end try
begin catch
if not exists(select column01 from FITABLE017 where column09=@COLUMN09)
		begin
			delete from FITABLE017 where column01=@COLUMN09
		end
return 0
end catch
end
















GO
