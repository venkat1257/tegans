USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_FIN_INVIMPORTECXEL]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_FIN_INVIMPORTECXEL]
(
@Items nvarchar(250)= null,
@Units nvarchar(250)= null,
@Lot nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null
)
AS
BEGIN
select m.column02 as ID,
m2.column02 as UOMID,
(case when isnull(f43.COLUMN02,0)>0 then isnull(f43.COLUMN02,0) when isnull(f431.COLUMN02,0)>0 then isnull(f431.COLUMN02,0) else 0 end) as LotID,
isnull(convert(DOUBLE PRECISION,f.column04),0) as QtyAvailable,m24.column04 MRP
from matable007 m 
left join matable002 m2 on (m2.COLUMNA03=@AcOwner or m2.COLUMNA03 is null) and m2.column03=11119 and m2.column04=@Units and isnull (m2.COLUMNA13,0) = 0
left join fitable043 f43 on f43.COLUMNA03=@AcOwner and f43.column04 =@Lot and isnull(f43.column15,0)=m2.COLUMN02 and f43.COLUMN09=m.column02 and isnull (f43.COLUMNA13,0) = 0
left join fitable043 f431 on f431.COLUMNA03=@AcOwner and f431.column04 =@Lot and isnull(f431.column15,0)=0 and f431.COLUMN09=m.column02 and isnull (f431.COLUMNA13,0) = 0
left join FITABLE010 f on f.COLUMN03=m.COLUMN02 and f.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and f.COLUMN19=isnull(m2.column02,10000) and isnull(f.COLUMN21,0)=0 and f.COLUMN22=(case when isnull(f43.COLUMN02,0)>0 then isnull(f43.COLUMN02,0) when isnull(f431.COLUMN02,0)>0 then isnull(f431.COLUMN02,0) else 0 end) and f.COLUMNA03=m.COLUMNA03   and isnull (f.COLUMNA13,0) =0
left join matable024 m24 on m24.column07 = m.column02 and m24.column06 ='Purchase' and m24.COLUMNA03=m.COLUMNA03 and isnull (m24.COLUMNA13,0) = 0
where m.column04=@Items  and m.COLUMNA03=@AcOwner and isnull (m.COLUMNA13,0)=0
end

GO
