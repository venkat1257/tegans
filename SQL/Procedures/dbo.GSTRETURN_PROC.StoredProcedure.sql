USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GSTRETURN_PROC]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GSTRETURN_PROC]
(
	@FROMDT NVARCHAR(250),
	@TODT NVARCHAR(250),
	@GSTIN NVARCHAR(250) = NULL,
	@GSTCATEGORY INT = NULL,
	@ACOWNER INT,
	@OPUNIT NVARCHAR(250) =NULL,
	@OPERATING NVARCHAR(250) = NULL,
	@FORMATDT NVARCHAR(250) = NULL,
	@TYPE NVARCHAR(250) = NULL

)
AS
BEGIN
	declare @whereStr nvarchar(1000)=null
	--if @GSTIN!=''
	--begin
	--	set @whereStr= ' GSTIN = '''+@GSTIN+''' '
	--end
	--if @OPERATING!=''
	--begin
	--	if(@whereStr='' or @whereStr is null)
	--		begin
	--			set @whereStr=' S9.COLUMNA02 = '''+@OPERATING+''''
	--		end
	--	else
	--		begin
	--			set @whereStr=@whereStr+' and S9.COLUMNA02  = '''+@OPERATING+''''
	--		end
	--end
	if @OPUNIT!=''
		begin
			if(@whereStr='' or @whereStr is null)
				begin
					set @whereStr=' S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
				end
			else
				begin
					set @whereStr=@whereStr+'S9.COLUMNA02  in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
				end
		end
	IF @FROMDT!='' and  @TODT!=''
		begin
			if(@whereStr='' or @whereStr is null)
				begin
					set @whereStr=' S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
			else
				begin
					set @whereStr=@whereStr+' and S9.COLUMN08 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
		end
	if(@whereStr='' or @whereStr is null)
		begin
			set @whereStr=' 1=1'
		 end
		 if(@TYPE IS NULL)
		 BEGIN
	 exec ('SELECT  S9.COLUMN04 [NO],FORMAT(S9.COLUMN08, '''+@FORMATDT+''') [DATE],SUM(S9.COLUMN20) [VALUE],M7.COLUMN04 ITEM,0 HSN,SUM(S10.COLUMN14) TAXABLE,
M132.COLUMN07 RATE1,SUM(S101.COLUMN23) VAL1,M13.COLUMN07 RATE2,SUM(S102.COLUMN23) VAL2,M131.COLUMN07 RATE3,SUM(S103.COLUMN23) VAL3 FROM SATABLE009 S9
INNER JOIN SATABLE010 S10 ON S10.COLUMN15=S9.COLUMN01 AND S10.COLUMNA02=S9.COLUMNA02 AND S10.COLUMNA03=S9.COLUMNA03 AND ISNULL(S10.COLUMNA13,0)=0 AND S10.COLUMN21!=1000
INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05 AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
LEFT JOIN MATABLE014 M14 ON M14.COLUMN02=-(CAST(S10.COLUMN21 AS INT)) AND M14.COLUMNA03=S9.COLUMNA03 --AND M13.COLUMN16 IN(23582) 
LEFT JOIN MATABLE013 M1 ON M1.COLUMN02 IN(SELECT ListValue FROM dbo.FN_ListToTable('','',M14.COLUMN05) s) AND M1.COLUMNA03 IN(S9.COLUMNA03) AND M1.COLUMN16 IN(23582)
LEFT JOIN MATABLE013 M11 ON M11.COLUMN02 IN(SELECT ListValue FROM dbo.FN_ListToTable('','',M14.COLUMN05) s) AND M11.COLUMNA03 IN(S9.COLUMNA03) AND M11.COLUMN16 IN(23583)
LEFT JOIN MATABLE013 M111 ON M111.COLUMN02 IN(SELECT ListValue FROM dbo.FN_ListToTable('','',M14.COLUMN05) s) AND M111.COLUMNA03 IN(S9.COLUMNA03) AND M111.COLUMN16 IN(23584)
LEFT JOIN MATABLE013 M13 ON M13.COLUMN02 IN(IIF(CAST(S10.COLUMN21 AS INT)>0,S10.COLUMN21,M1.COLUMN02)) AND M13.COLUMNA03 IN(S9.COLUMNA03) AND M13.COLUMN16 IN(23582)
LEFT JOIN MATABLE013 M131 ON M131.COLUMN02 IN(IIF(CAST(S10.COLUMN21 AS INT)>0,S10.COLUMN21,M11.COLUMN02)) AND M131.COLUMNA03 IN(S9.COLUMNA03) AND M131.COLUMN16 IN(23583) 
LEFT JOIN MATABLE013 M132 ON M132.COLUMN02 IN(IIF(CAST(S10.COLUMN21 AS INT)>0,S10.COLUMN21,M111.COLUMN02)) AND M132.COLUMNA03 IN(S9.COLUMNA03) AND M132.COLUMN16 IN(23584) 
LEFT JOIN SATABLE010 S101 ON S101.COLUMN15=S9.COLUMN01 AND S101.COLUMNA02=S9.COLUMNA02 AND S101.COLUMNA03=S9.COLUMNA03 AND ISNULL(S101.COLUMNA13,0)=0 AND S101.COLUMN21=M132.COLUMN02
LEFT JOIN SATABLE010 S102 ON S102.COLUMN15=S9.COLUMN01 AND S102.COLUMNA02=S9.COLUMNA02 AND S102.COLUMNA03=S9.COLUMNA03 AND ISNULL(S102.COLUMNA13,0)=0 AND S102.COLUMN21=M13.COLUMN02
LEFT JOIN SATABLE010 S103 ON S103.COLUMN15=S9.COLUMN01 AND S103.COLUMNA02=S9.COLUMNA02 AND S103.COLUMNA03=S9.COLUMNA03 AND ISNULL(S103.COLUMNA13,0)=0 AND S103.COLUMN21=M131.COLUMN02
--LEFT JOIN MATABLE032 M32 ON M32.COLUMN02=M7.COLUMN75 
WHERE S9.COLUMNA03='+@ACOWNER+' AND ISNULL(S9.COLUMNA13,0)=0 AND '+@whereStr+'
GROUP BY S9.COLUMN04,S9.COLUMN08,M7.COLUMN04,S10.COLUMN14,M132.COLUMN07,M13.COLUMN07,M131.COLUMN07,M1.COLUMN07,M11.COLUMN07,M111.COLUMN07') 
END
END
begin
declare @whereStr1 nvarchar(1000)=null
if @OPUNIT!=''
		begin
			if(@whereStr1='' or @whereStr1 is null)
				begin
					set @whereStr1=' S5.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
				end
			else
				begin
					set @whereStr1=@whereStr1+'S5.COLUMNA02  in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
				end
		end
	IF @FROMDT!='' and  @TODT!=''
		begin
			if(@whereStr1='' or @whereStr1 is null)
				begin
					set @whereStr1=' S5.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
			else
				begin
					set @whereStr1=@whereStr1+' and S5.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
		end
	if(@whereStr1='' or @whereStr1 is null)
		begin
			set @whereStr1=' 1=1'
		 end
		 if(@TYPE='CD')
		 BEGIN
	 exec ('SELECT  S5.COLUMN67 [NO],S5.COLUMN04 [SNO],FORMAT(S5.COLUMN06, '''+@FORMATDT+''') [DATE] FROM SATABLE005 S5
WHERE S5.COLUMNA03='+@ACOWNER+' AND ISNULL(S5.COLUMNA13,0)=0 AND '+@whereStr1+'
GROUP BY S5.COLUMN67,S5.COLUMN04,S5.COLUMN06') 
END
END
begin
declare @whereStr2 nvarchar(1000)=null
if @OPUNIT!=''
		begin
			if(@whereStr2='' or @whereStr2 is null)
				begin
					set @whereStr2=' P1.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
				end
			else
				begin
					set @whereStr2=@whereStr2+'P1.COLUMNA02  in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUNIT+''') s)'
				end
		end
	IF @FROMDT!='' and  @TODT!=''
		begin
			if(@whereStr2='' or @whereStr2 is null)
				begin
					set @whereStr2=' P1.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
			else
				begin
					set @whereStr2=@whereStr2+' and P1.COLUMN06 between '''+@FROMDT+''' AND '''+@TODT+''''
				end
		end
	if(@whereStr2='' or @whereStr2 is null)
		begin
			set @whereStr2=' 1=1'
		 end
		 if(@TYPE='CD')
		 BEGIN
	 exec ('SELECT  P1.COLUMN70 [NO],P1.COLUMN04 [SNO],FORMAT(P1.COLUMN06, '''+@FORMATDT+''') [DATE] FROM PUTABLE001 P1
WHERE P1.COLUMNA03='+@ACOWNER+' AND ISNULL(P1.COLUMNA13,0)=0 AND '+@whereStr2+'
GROUP BY P1.COLUMN70,P1.COLUMN04,P1.COLUMN06') 
END
END
GO
