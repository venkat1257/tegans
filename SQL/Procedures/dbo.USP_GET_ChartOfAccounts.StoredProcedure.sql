USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ChartOfAccounts]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_GET_ChartOfAccounts](@FromDate nvarchar(250)=null,@ToDate nvarchar(250)=null,
@OperatingUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null,@OPUnit nvarchar(250)=null,@dateF nvarchar(250)=null,
@ide nvarchar(250)=null,@Type nvarchar(250)=null,@Tds nvarchar(250)=null,@GLV nvarchar(25) = null)
as
begin
set @OperatingUnit=(iif(cast(isnull(@OperatingUnit,'') as nvarchar(250))='',@OPUnit,@OperatingUnit))

if(@GLV is null)
begin
DECLARE @PFRDATE  nvarchar(250)= '01/01/1900', @PTODATE DATE = '1/1/2010'
		if(@FromDate!='' and @ToDate!='')
		BEGIN
			--SET @PFRDATE=((SELECT DateAdd(yy, -1, @FrDate)))
			--SET @PTODATE=((SELECT DateAdd(yy, -1, @ToDate)))
			--select top 1 @PFRDATE=column04 from MATABLE020 where COLUMNA03=@AcOwner and column09 = '1' and isnull(columna13,0)=0 --order by COLUMN01 desc
			select @PFRDATE= (column04) from FITABLE048 where COLUMNA03=@AcOwner and column09 = '1' and isnull(columna13,0)=0 --order by COLUMN01 desc
			IF((SELECT MAX(COLUMN04) FROM MATABLE020 WHERE COLUMNA03=@AcOwner AND COLUMN04 = @PFRDATE AND ISNULL(COLUMNA13,0)=0) IS NULL)
			BEGIN
				SET @PTODATE = @PFRDATE
			END
		END
if(@Type = 22262)
begin
select Particulars,sum(cast([Payment Amount]as decimal(18,2)))[Credit],sum(cast([Deposit Amount]as decimal(18,2)))[Debit],
(sum(SUM(cast(isnull([Balance Amount],0)as decimal(18,2)))) OVER(ORDER BY MNTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance],
[Type] from
(
	SELECT ' Opening balance' AS 'Particulars', sum(isnull(p.[COLUMN10],0)) AS [Deposit Amount], 
sum(isnull(p.[COLUMN11],0)) AS [Payment Amount],sum(isnull(P.column10,0)-isnull(P.column11,0))[Balance Amount] ,
'22262' [Type],0 MNTH From	PUTABLE017 p 
where isnull((p.COLUMNA13),0)=0  and p.COLUMN04  between @PTODATE and dateadd(day,-1,@FromDate)  AND p.COLUMN03 = @ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner 
group by  p.column04 
union all
	SELECT DATENAME(month, p.column04) AS 'Particulars', sum(isnull(p.[COLUMN10],0)) AS [Deposit Amount], 
sum(isnull(p.[COLUMN11],0)) AS [Payment Amount],sum(isnull(P.column10,0)-isnull(P.column11,0))[Balance Amount] ,
'22262' [Type],MONTH(p.column04) MNTH From	PUTABLE017 p 
where isnull((p.COLUMNA13),0)=0  and p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN03 = @ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner 
group by  p.column04 )a group by  Particulars,[Type],MNTH order by (MNTH)
end
else
begin
select Particulars,sum(cast([Payment Amount]as decimal(18,2)))[Credit],sum(cast([Deposit Amount]as decimal(18,2)))[Debit],
(sum(SUM(cast(isnull([Balance Amount],0)as decimal(18,2)))) OVER(ORDER BY MNTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance],
[Type] from
(
	SELECT ' Opening balance' AS 'Particulars', sum(isnull(p.[COLUMN15],0)) AS [Deposit Amount], 
sum(isnull(p.[COLUMN14],0)) AS [Payment Amount],
sum(IIF((p.column10 = 22264 or p.column10 = 22266 or p.column10 = 22409 or p.column10 = 22344 or p.column10 = 22408 or p.column10 = 22410 or p.column10 = 22403 or p.column10 = 22402) and p.column11 != 1078,
cast((ISNULL(p.column15,0)-ISNULL(p.column14,0))as decimal(18,2)),
cast((ISNULL(p.column14,0)-ISNULL(p.column15,0))as decimal(18,2))))[Balance Amount] ,
(case when isnull(p.column19,'') like '%input %' then'223831'
	when  isnull(p.column19,'') like '%output %' then '223832' else p.COLUMN10 end) [Type],
'0' MNTH From	FITABLE064 p 
where  p.COLUMNA03=@AcOwner AND p.COLUMN11 = @ide AND p.COLUMN10= @Type  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND isnull((p.COLUMNA13),0)=0  and p.COLUMN07 between @PTODATE and dateadd(day,-1,@FromDate) 
and (p.column10 != 22406 or p.column10 != 22344 or p.column10 != 22405 or p.column10 != 22408 or p.column10 != 22407)
group by  p.column07,p.COLUMN10,isnull(p.column19,'')
union all
	SELECT DATENAME(month, p.column07) AS 'Particulars', sum(isnull(p.[COLUMN15],0)) AS [Deposit Amount], 
sum(isnull(p.[COLUMN14],0)) AS [Payment Amount],
sum(IIF((p.column10 = 22264 or p.column10 = 22266 or p.column10 = 22409 or p.column10 = 22344 or p.column10 = 22408 or p.column10 = 22410 or p.column10 = 22403 or p.column10 = 22402) and p.column11 != 1078,
cast((ISNULL(p.column15,0)-ISNULL(p.column14,0))as decimal(18,2)),
cast((ISNULL(p.column14,0)-ISNULL(p.column15,0))as decimal(18,2))))[Balance Amount] ,
(case when isnull(p.column19,'') like '%input %' then'223831'
	when  isnull(p.column19,'') like '%output %' then '223832' else p.COLUMN10 end) [Type],MONTH(COLUMN07) MNTH From	FITABLE064 p 
where p.COLUMNA03=@AcOwner AND p.COLUMN11 = @ide AND p.COLUMN10= @Type  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND isnull((p.COLUMNA13),0)=0  and p.COLUMN07  between @FromDate and @ToDate --AND p.COLUMN11 = @ide AND p.COLUMN10= @Type  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner 
group by  p.column07,p.COLUMN10,p.COLUMN19 )a group by  Particulars,[Type],MNTH order by (MNTH)
end
end
else
begin
--Bank Register
if (@Type = 22266)
begin
SELECT isnull(p.COLUMN05,'') AS 'Tran#',p.[COLUMN06] AS 'Type',FORMAT(p.[COLUMN04],@dateF) AS Date,  
(case when (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Debit Memo') then sa.COLUMN05 when (p.COLUMN06='INVOICE' or p.COLUMN06='PAYMENT' or p.COLUMN06='Credit Memo') then s2.COLUMN05 when (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then ma.COLUMN06 when (p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='PAYMENT VOUCHER') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) 
when p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 when j.column07='22700' or fj.column14='22700' then sa.COLUMN05 
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end)  when p.COLUMN06='ADVANCE RECEIPT' then s2.COLUMN05 when p.COLUMN06='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20= 22700 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end) else '' end) Party,
c.COLUMN03 'Operating Unit',pr.COLUMN05 as Project,'' Reference#,(case when p.COLUMN06='PAYMENT VOUCHER' then f22.COLUMN04 
when p.COLUMN06='RECEIPT VOUCHER' then f24.COLUMN04
when p.COLUMN06='ADVANCE RECEIPT' then v1.COLUMN11
when p.COLUMN06='ADVANCE PAYMENT' then v.COLUMN08
when p.COLUMN06='JOURNAL ENTRY' then fj.COLUMN06
when p.COLUMN06='WITHDRAW' then F62.COLUMN19
when p.COLUMN06='DEPOSIT' then F62.COLUMN19
when p.COLUMN06='BILL PAYMENT' then P14.COLUMN10
when p.COLUMN06='TRANSFER' then F62.COLUMN19
when p.COLUMN06='EXPENSE' then F45.COLUMN13
else p.COLUMN09 end) Memo, p.[COLUMN11] AS [Deposit Amount], 
p.[COLUMN10] AS [Payment Amount],(SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO, ma1.column09 'Created By' From	PUTABLE019 p 
left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN20 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0 
left outer join   CONTABLE007 c on c.column02=p.columna02 and c.COLUMNA03=p.COLUMNA03 and isnull(c.COLUMNA13,0)=0 
left outer join CONTABLE002 cn on cn.column02=p.columna03  and isnull(cn.COLUMNA13,0)=0 
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN05 and v.COLUMNA03=p.COLUMNA03 and v.COLUMNA02=p.COLUMNA02 and isnull(v.COLUMNA13,0)=0 
--left outer join FITABLE022 f22 on v.COLUMN01=f22.column09 and p.COLUMNA03=f22.COLUMNA03 and f22.column01 = p.COLUMN08 and p.COLUMNA02=f22.COLUMNA02 and isnull(f22.COLUMNA13,0)=0
left join FITABLE022 f22 on v.COLUMN01=f22.column09 and v.COLUMNA03=f22.COLUMNA03 and v.COLUMNA02=f22.COLUMNA02 and f22.column01 = p.COLUMN08  and isnull(f22.COLUMNA13,0)=0  and v.COLUMNA02=f22.COLUMNA02
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN07 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0 
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN07 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0 
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN08 and j.COLUMN04=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0 --left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03))  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 
and ((p.COLUMN03) in (select column02 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)) 
AND (fj.COLUMN04 = p.COLUMN11 OR fj.COLUMN05 = p.COLUMN10) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN05 and v1.COLUMNA03=p.COLUMNA03 and v1.COLUMNA02=p.COLUMNA02 and isnull(v1.COLUMNA13,0)=0 
left outer join FITABLE024 F24 on F24.COLUMN09 = v1.COLUMN01 and F24.COLUMNA03 = v1.COLUMNA03 and F24.COLUMNA02 = v1.COLUMNA02 and isnull(F24.COLUMNA13,0) = 0 and F24.column01 = p.COLUMN08
LEFT OUTER JOIN FITABLE062 F62 ON F62.COLUMN05 = P.COLUMN05 AND F62.COLUMNA03 = P.COLUMNA03 AND F62.COLUMNA02 = P.COLUMNA02 AND ISNULL(F62.COLUMNA13,0)=0
LEFT OUTER JOIN PUTABLE014 P14 ON P14.COLUMN04 = P.COLUMN05 AND P14.COLUMNA03 = P.COLUMNA03 AND P14.COLUMNA02 = P.COLUMNA02 AND ISNULL(P14.COLUMNA13,0)=0
LEFT OUTER JOIN FITABLE045 F45 ON F45.COLUMN04 = P.COLUMN05 AND F45.COLUMNA03 = P.COLUMNA03 AND F45.COLUMNA02 = P.COLUMNA02 AND ISNULL(F45.COLUMNA13,0)=0
left outer join  MATABLE010 ma1 on ma1.COLUMN02=(case when p.COLUMN06='BILL' then (select COLUMNA08 from PUTABLE005 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0) when p.column06='BILL PAYMENT' then (select COLUMNA08 from PUTABLE014 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0)  when p.column06='Debit Memo' then (select COLUMNA08 from PUTABLE001 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0)  when p.column06='INVOICE' then (select COLUMNA08 from SATABLE009 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0)   when p.column06='PAYMENT' then (select COLUMNA08 from SATABLE011 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0)   when p.column06='Crdeit Memo' then (select COLUMNA08 from SATABLE005 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0)    when p.column06='EXPENCE' then (select COLUMNA08 from FITABLE012 where COLUMN04=p.column05 and  COLUMNA03=p.COLUMNA03 and COLUMN01=p.column08 and isnull(COLUMNA13,0)=0)    when (p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='PAYMENT VOUCHER') then V.COLUMNA08  	when (p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIPT VOUCHER') then (v1.COLUMNA08)	 when p.column06='JOURNAL ENTRY' then (j.COLUMNA08) when p.column06='WITHDRAW' then (F62.COLUMNA08) when p.column06='DEPOSIT' then (F62.COLUMNA08) when p.column06='TRANSFER' then (F62.COLUMNA08) when p.column06='Opening Balance' then (s.COLUMNA08) when p.column06='EXPENSE' then (F45.COLUMNA08) end)
where isnull((p.COLUMNA13),0)=0  and p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN03=@ide  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Cash Register
else if (@Type = 22409)
begin
SELECT isnull(p.COLUMN05,'') AS 'Tran#',p.[COLUMN06] AS 'Type',FORMAT(p.[COLUMN04],@dateF) AS Date,  
(case when (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Debit Memo') then sa.COLUMN05  when (p.COLUMN06='INVOICE' or p.COLUMN06='PAYMENT' or p.COLUMN06='Credit Memo') then s2.COLUMN05  when (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then ma.COLUMN06  when (p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='PAYMENT VOUCHER') then (case when v.COLUMN11=22305 or v.COLUMN11= 22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end)  when p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end)  when p.COLUMN06='ADVANCE RECEIPT' then s2.COLUMN05  when p.COLUMN06='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when   v1.COLUMN20=22334 then sa.COLUMN05 else '' end) else '' end) Party,
c.COLUMN03 'Operating Unit',pr.COLUMN05 as Project,'' Reference#,p.COLUMN10 Memo,  p.[COLUMN12] AS [Deposit Amount],  
p.[COLUMN11] AS [Payment Amount],(SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO, ma1.column09 'Created By' From	FITABLE052 p  
left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN16 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0 
left outer join   CONTABLE007 c on c.column02=p.columna02 and c.COLUMNA03=p.COLUMNA03 and isnull(c.COLUMNA13,0)=0  
left outer join CONTABLE002 cn on cn.column02=p.columna03  and isnull(cn.COLUMNA13,0)=0  
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN05 and v.COLUMNA03=p.COLUMNA03 and v.COLUMNA02=p.COLUMNA02 and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN08 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0   
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0 
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN08 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0    
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN09 and j.COLUMN04=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0  --left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (fj.COLUMN03))  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (fj.COLUMN03)) 
--AND (fj.COLUMN04 = p.COLUMN12 OR fj.COLUMN05 = p.COLUMN11) 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN08,0)
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN05 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0 
LEFT OUTER JOIN FITABLE062 F62 ON F62.COLUMN05 = P.COLUMN05 AND F62.COLUMNA03 = P.COLUMNA03 AND F62.COLUMNA02 = P.COLUMNA02 AND ISNULL(F62.COLUMNA13,0)=0  
left outer join  MATABLE010 ma1 on ma1.COLUMN02 = (case when p.COLUMN06='BILL' then (select COLUMNA08 from PUTABLE005 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)  when p.column06='BILL PAYMENT' then (select COLUMNA08 from PUTABLE014 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)   when p.column06='Debit Memo' then (select COLUMNA08 from PUTABLE001 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)   when p.column06='INVOICE' then (select COLUMNA08 from SATABLE009 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)   when p.column06='PAYMENT' then (select COLUMNA08 from SATABLE011 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)   when p.column06='Crdeit Memo' then (select COLUMNA08 from SATABLE005 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)   
--when p.column06='EXPENSE' then (select COLUMNA08 from FITABLE045 where COLUMN04=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)  
when p.column06='EXPENSE' then (select COLUMNA08 from FITABLE012 where COLUMN17=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)
 when p.column06='EXPENCE' then (select COLUMNA08 from FITABLE012 where COLUMN17=p.column05 and COLUMNA03=p.COLUMNA03 and COLUMN01=p.column09 and isnull(COLUMNA13,0)=0)   when (p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='PAYMENT VOUCHER') then (v.COLUMNA08)   when (p.COLUMN06='ADVANCE RECEIPT') then (v1.COLUMNA08)   when (p.COLUMN06='RECEIPT VOUCHER') then (v1.COLUMNA08)   when p.column06='JOURNAL ENTRY' then (j.COLUMNA08) 
when p.column06='WITHDRAW' then (F62.COLUMNA08) when p.column06='DEPOSIT' then (F62.COLUMNA08) when p.column06='TRANSFER' then (F62.COLUMNA08) when p.column06='Opening Balance' then (s.COLUMNA08) end)  
where isnull((p.COLUMNA13),0)=0  and cast(p.COLUMN04 as date)   between @FromDate and @ToDate AND p.COLUMN03=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Inventory Asset
else if (@Type = 22262)
begin
SELECT (CASE  WHEN p.COLUMN06='Item Issue' then isnull((s7.column04),'')   when p.COLUMN06='Credit Memo' then  isnull((s5.column04),'')  when p.COLUMN06='Debit Memo' then  isnull((p1.column04),'') when p.COLUMN06='Return Issue' then  isnull((s7.column04),'') when p.COLUMN06='Stock Transfer' then  isnull((pr3.column04),'') when p.COLUMN06='JobOrder Issue' then  isnull((s7.column04),'') when (p.COLUMN06='Resource Consumption' or p.COLUMN06='Resource Consumption Direct') then  isnull((p7.column04),'') when p.COLUMN06='INVOICE' then  isnull((s9.column04),'') when p.COLUMN06='Inventory Adjustment' then  isnull((f4.column04),'')     when p.COLUMN06='Item Receipt' then  isnull((p3.column04),'') when p.COLUMN06='Work Order' then  isnull((pr5.column04),'')  when p.COLUMN06='BILL' then  isnull((p5.column04),'')  when p.COLUMN06='BILL' then  isnull((p5.column04),'')  when p.COLUMN06='JOURNAL ENTRY' then  isnull((f3.column04),'')  when p.COLUMN06='Return Receipt' then  isnull((p3.column04),'')  when p.COLUMN06='JobOrder Receipt' then  isnull((p3.column04),'')  when p.COLUMN06='Return Issue' then  isnull((s7.column04),'')when p.COLUMN06='PAYMENT VOUCHER' then  isnull((pv.column04),'') when p.COLUMN06='RECEIPT VOUCHER' then  isnull((rv.column04),'')  when p.COLUMN06='EXPENSE' then  isnull((eb.column17),'') else '' end) Tran#,  
p.COLUMN06 Type,FORMAT(p.[COLUMN04],@dateF) AS 'Date',  (CASE  WHEN p.COLUMN06='Item Issue' then b.column05 when (p.COLUMN06='Resource Consumption' or p.COLUMN06='Resource Consumption Direct') then  b.column05  when p.COLUMN06='INVOICE' then  b.column05  when p.COLUMN06='Item Receipt' then  a.column05  when p.COLUMN06='Debit Memo' then  a.column05  when p.COLUMN06='Work Order' then  b.column05  when p.COLUMN06='BILL' then  a.column05  when p.COLUMN06='Return Receipt' then  b.column05     when p.COLUMN06='Credit Memo' then  b.column05  when p.COLUMN06='JobOrder Receipt' then  a.column05  when p.COLUMN06='JobOrder Issue' then  a.column05 WHEN (p.COLUMN06='PAYMENT VOUCHER' or p.COLUMN06='ADVANCE PAYMENT') then (case when pv.column11=22305 or pv.column11=22700 then a.COLUMN05 when pv.column11=22335 then b.COLUMN05 when pv.column11=22492 then ma.COLUMN09 else '' end)
 WHEN (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when 
 rv.column20=22305 or rv.column20=22700 then a.COLUMN05 when rv.column20=22335 then b.COLUMN05 when rv.column20=22492 then ma.COLUMN09 else '' end)
 when p.COLUMN06='JOURNAL ENTRY' then  (case when  j.column07='22305' or fj.column14='22305' then a.COLUMN05 
 when  j.column07='22700' or fj.column14='22700' then a.COLUMN05 
 when  j.column07='22335' or fj.column14='22335' then b.COLUMN05 when  j.column07='22334' or fj.column14='22334' then a.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end)   when p.COLUMN06='Return Issue' then  a.column05 when (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then ma.COLUMN06 else '' end) Party,  
CON07.COLUMN03 as [Operating Unit],pr.COLUMN05 as Project ,(case when p.COLUMN06='BILL' THEN p5.COLUMN09 when p.COLUMN06='INVOICE' THEN s9.COLUMN09 else '' end) AS Reference#,  p.COLUMN09 AS Memo,
p.COLUMN10 AS [Increment Amount],p.COLUMN11 AS [Decrement Amount], (SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,ma1.COLUMN09  as [Created By]  From PUTABLE017 p 
left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02 and e.COLUMNA03=p.COLUMNA03 and isnull(e.COLUMNA13,0)=0 
left outer join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMNA03=p.COLUMNA03  and isnull(a.COLUMNA13,0)=0 
left outer join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and b.COLUMNA03=p.COLUMNA03  and isnull(b.COLUMNA13,0)=0 
left outer join  SATABLE007 s7 on s7.COLUMN01=p.COLUMN05 and s7.COLUMNA03=p.COLUMNA03 and s7.COLUMNA02=p.COLUMNA02 and isnull(s7.COLUMNA13,0)=0 
left outer join  SATABLE005 s5 on s5.COLUMN01=p.COLUMN05 and s5.COLUMNA03=p.COLUMNA03 and s5.COLUMNA02=p.COLUMNA02 and isnull(s5.COLUMNA13,0)=0 
left outer join  SATABLE009 s9 on s9.COLUMN01=p.COLUMN05 and s9.COLUMNA03=p.COLUMNA03 and s9.COLUMNA02=p.COLUMNA02 and isnull(s9.COLUMNA13,0)=0 
left outer join  PUTABLE001 p1 on p1.COLUMN01=p.COLUMN05 and p1.COLUMNA03=p.COLUMNA03 and p1.COLUMNA02=p.COLUMNA02 and isnull(p1.COLUMNA13,0)=0
left outer join  PUTABLE003 p3 on p3.COLUMN01=p.COLUMN05 and p3.COLUMNA03=p.COLUMNA03 and p3.COLUMNA02=p.COLUMNA02 and isnull(p3.COLUMNA13,0)=0
left outer join  PUTABLE005 p5 on p5.COLUMN01=p.COLUMN05 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMNA02=p.COLUMNA02 and isnull(p5.COLUMNA13,0)=0 
left outer join  PRTABLE007 p7 on p7.COLUMN01=p.COLUMN05 and p7.COLUMNA03=p.COLUMNA03 and p7.COLUMNA02=p.COLUMNA02 and isnull(p7.COLUMNA13,0)=0  
left outer join  PRTABLE005 pr5 on pr5.COLUMN01=p.COLUMN05 and pr5.COLUMNA03=p.COLUMNA03 and pr5.COLUMNA02=p.COLUMNA02  and isnull(pr5.COLUMNA13,0)=0  
left outer join  PRTABLE004 pr4 on pr4.COLUMN01=p.COLUMN05 and pr4.COLUMNA03=p.COLUMNA03 and pr4.COLUMNA02=p.COLUMNA02  and isnull(pr4.COLUMNA13,0)=0 
left outer join  PRTABLE003 pr3 on pr3.COLUMN01=pr4.COLUMN08 and pr3.COLUMNA03=p.COLUMNA03 and pr3.COLUMNA02=p.COLUMNA02  and isnull(pr3.COLUMNA13,0)=0 
left outer join  FITABLE014 f4 on f4.COLUMN01=p.COLUMN05 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMNA02=p.COLUMNA02  and isnull(f4.COLUMNA13,0)=0 
left outer join  FITABLE031 f3 on f3.COLUMN01=p.COLUMN05 and f3.COLUMNA03=p.COLUMNA03 and f3.COLUMNA02=p.COLUMNA02  and isnull(f3.COLUMNA13,0)=0 
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN07 left outer join  CONTABLE002 cn on cn.COLUMN02=p.COLUMNA03 and isnull(cn.COLUMNA13,0)=0 
left outer join FITABLE020 pv on pv.COLUMN01=p.COLUMN05 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0 
left outer join FITABLE023 rv on rv.COLUMN01=p.COLUMN05 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0 
left outer join FITABLE012 eb on eb.COLUMN01=p.COLUMN05 and eb.COLUMNA03=p.COLUMNA03 and eb.COLUMNA02=p.COLUMNA02 and isnull(eb.COLUMNA13,0)=0  
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0 
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=1000 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and j.COLUMNA02=fj.COLUMNA02 and isnull(fj.COLUMNA13,0)=0 and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22262
and fj.COLUMN07=p.COLUMN07

left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN14 
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (p.COLUMNA08,pv.COLUMNA08,rv.COLUMNA08,j.COLUMNA08,p7.COLUMNA08)  and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0 
where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Accounts Payable Register
else if (@Type = 22263)
begin
select isnull(p.COLUMN09,'') Tran#,(case when ((p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE' )and isnull(p.COLUMN12,0)!=0) then p.column06  when ((p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') and isnull(p.COLUMN13,0)!=0) then 'EXPENCE' else p.COLUMN06 end)Type,
FORMAT(p.[COLUMN04],@dateF) as [Date] , (case when (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then ma.column06  when (p.COLUMN06='BILL' or p.COLUMN06='BILL PAYMENT' or p.COLUMN06='Debit Memo' or p.COLUMN06='Credit Memo') then sa.column05 
when ( p.COLUMN06='PAYMENT VOUCHER') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335  then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end)
when ( p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (case when rv.COLUMN20=22305 or rv.COLUMN20=22700  then  sa.COLUMN05 when rv.COLUMN20=22335 then s2.COLUMN05 when  rv.COLUMN20=22492 then ma.COLUMN06 when  rv.COLUMN20=22334 then sa.COLUMN05 else '' end)
 when p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) end)  Party,
(c7.COLUMN03) as  [Operating Unit],pr.COLUMN05 as Project,'' as Reference#,(case when p.COLUMN06='JOURNAL ENTRY' then fj.COLUMN06 ELSE p.COLUMN18 END) as Memo,p.COLUMN12 as [Billed Amount],
p.COLUMN13 as [Paid Amount],(SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN13,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,
ma1.column09 as [Created By] from putable016 p 
left outer join FITABLE001 f on f.COLUMN02=p.COLUMN08 and f.COLUMNA03=p.COLUMNA03 AND F.COLUMN07=22263 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN20 and pr.COLUMNA03=p.COLUMNA03 AND ISNULL(pr.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.column02=p.column07 and ma.COLUMNA03=p.COLUMNA03 AND ISNULL(ma.COLUMNA13,0)=0
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN07 and sa.COLUMNA03=p.COLUMNA03 AND ISNULL(sa.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07  and s2.COLUMNA03=p.COLUMNA03 AND ISNULL(s2.COLUMNA13,0)=0
left outer join CONTABLE007 c7 on c7.COLUMN02=p.COLUMNA02  and c7.COLUMNA03=p.COLUMNA03 AND ISNULL(c7.COLUMNA13,0)=0
left outer join FITABLE020 v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and (v.COLUMNA02=p.COLUMNA02 OR v.COLUMNA02 is null) and isnull(v.COLUMNA13,0)=0 
left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN09 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0
left join FITABLE022 f22 on v.COLUMN01=f22.column09 and p.COLUMNA03=f22.COLUMNA03 and CAST(f22.column01 AS NVARCHAR(250)) = CAST(p.COLUMN05 AS NVARCHAR(250)) and p.COLUMNA02=f22.COLUMNA02 and isnull(f22.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA03=p.COLUMNA03 and CAST(p5.COLUMN01 AS NVARCHAR(250))= CAST(p.column05 AS NVARCHAR(250)) and isnull(p5.COLUMNA13,0)=0  
left outer join PUTABLE014  p4 on p4.COLUMN04=p.column09 and p4.COLUMNA03=p.COLUMNA03 and CAST(p4.COLUMN01 AS NVARCHAR(250))=CAST(p.column05 AS NVARCHAR(250)) and isnull(p4.COLUMNA13,0)=0  
left outer join PUTABLE001  p1 on p1.COLUMN04=p.column09 and p1.COLUMNA03=p.COLUMNA03 and CAST(p1.COLUMN01 AS NVARCHAR(250))=CAST(p.column05 AS NVARCHAR(250)) and isnull(p1.COLUMNA13,0)=0  
left outer join FITABLE045  f4 on f4.COLUMN04=p.column09 and f4.COLUMNA03=p.COLUMNA03 and CAST(f4.COLUMN01 AS NVARCHAR(250)) =CAST(p.column05 AS NVARCHAR(250)) and isnull(f4.COLUMNA13,0)=0  
left outer join FITABLE012  f1 on f1.COLUMN17=p.column09 and f1.COLUMNA03=p.COLUMNA03 and CAST(f1.COLUMN01 AS NVARCHAR(250))=CAST(p.column05 AS NVARCHAR(250)) and isnull(f1.COLUMNA13,0)=0  
--left outer join FITABLE021 f21 on f21.COLUMN06=f1.COLUMN01 and f21.COLUMNA03=f1.COLUMNA03 and isnull(f21.COLUMNA13,0)=0
left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=2000 
--AND (fj.COLUMN04 = p.COLUMN13 OR fj.COLUMN05 = p.COLUMN12) AND ISNULL(fj.COLUMN07,0)=ISNULL(j.COLUMN07,0) 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and fj.COLUMNA02=j.COLUMNA02 and isnull(fj.COLUMNA13,0)=0 
and (select column07 from fitable001 where COLUMNA03=fj.COLUMNA03 AND column02=fj.COLUMN03)=22263 
AND (fj.COLUMN04 = p.COLUMN13 OR fj.COLUMN05 = p.COLUMN12) 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
left outer join  MATABLE010 ma1 on ma1.COLUMN02=(case when p.COLUMN06='BILL' then (p5.COLUMNA08) when p.column06='BILL PAYMENT' then (p4.COLUMNA08 ) when p.column06='Debit Memo' then (p1.COLUMNA08)  when p.column06='EXPENSE' then (f1.COLUMNA08) when p.column06='EXPENSE' then (f4.COLUMNA08) 
when p.column06='EXPENCE' then (f1.COLUMNA08) when (p.COLUMN06='ADVANCE PAYMENT' or p.COLUMN06='PAYMENT VOUCHER') then (v.COLUMNA08) 
when (p.COLUMN06='RECEIPT VOUCHER' or p.COLUMN06='ADVANCE RECEIPT' or p.COLUMN06='RECEIVE PAYMENT') then (rv.COLUMNA08)  when p.column06='JOURNAL ENTRY' then (j.COLUMNA08) when p.column06='Opening Balance' then (f.COLUMNA08) end) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0   
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND p.COLUMNA03=@AcOwner AND p.COLUMN03=@ide order by RNO desc
end
--Accounts Receivable Register
else if (@Type = 22264)
begin
select (case when p.COLUMN06='JOURNAL ENTRY' then j.COLUMN04 else isnull(p.COLUMN05,'') end) Tran#,p.COLUMN06 as Type,
FORMAT(p.[COLUMN04],@dateF) as Date,(case when (p.COLUMN06='EXPENSE' or p.COLUMN06='EXPENCE') then ma.column06  when (p.COLUMN06='INVOICE' or p.COLUMN06='Invoice' or p.COLUMN06='Payment' or p.COLUMN06='PAYMENT' or p.COLUMN06='Credit Memo' or p.COLUMN06='Debit Memo') then s2.column05 when ( p.COLUMN06='RECEIPT VOUCHER') then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end) when ( p.COLUMN06='PAYMENT VOUCHER') then (case when v2.COLUMN11=22305 or v2.COLUMN11=22700 then  sa.COLUMN05 when v2.COLUMN11=22335 then s2.COLUMN05 when v2.COLUMN11=22492 then ma.COLUMN06 when  v2.COLUMN11=22334 then sa.COLUMN05 else '' end) when p.COLUMN06='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) else '' end)  Party, 
(cn.COLUMN03) as [Operating Unit],pr.COLUMN05 as Project, '' Reference#,p.COLUMN08 as Memo,p.COLUMN11 as [Amount Received],
p.COLUMN09 as [Amount Billed],(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,(case when  p.COLUMN06='Debit Memo' then -p.COLUMN14 else p.COLUMN14 end) as Credit,
p.COLUMN15 as [Credit Memo#],ma1.column09 [created By] from putable018 p 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN18 AND pr.COLUMNA03=p.COLUMNA03 AND ISNULL(pr.COLUMNA13,0)=0
left outer join CONTABLE007 cn on cn.column02=p.columna02 AND cn.COLUMNA03=p.COLUMNA03 AND ISNULL(cn.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.column02=p.column07 AND ma.COLUMNA03=p.COLUMNA03 AND ISNULL(ma.COLUMNA13,0)=0
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN07 AND sa.COLUMNA03=p.COLUMNA03 AND ISNULL(sa.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 AND s2.COLUMNA03=p.COLUMNA03 AND ISNULL(s2.COLUMNA13,0)=0
left outer join FITABLE023  v1 on CAST(v1.COLUMN04 AS NVARCHAR(250))=cast(p.COLUMN05 as nvarchar(250)) and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0 
left outer join FITABLE031 j on cast(j.COLUMN01 as nvarchar(250))=(case when p.COLUMN06='JOURNAL ENTRY' then cast(p.COLUMN05 as nvarchar(250)) end) and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA02=j.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=@ide and (fj.COLUMN04=p.COLUMN09 or fj.COLUMN05=p.COLUMN11) and ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA02=j.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=@ide 
left outer join SATABLE009 s9 on CAST(s9.COLUMN04 as nvarchar(250))=cast(p.COLUMN05 as nvarchar(250)) and s9.COLUMNA02=p.COLUMNA02 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join FITABLE020  v2 on CAST(v2.COLUMN04 as nvarchar(250))=cast(p.column05 as nvarchar(250)) and (v2.COLUMNA02=p.COLUMNA02 OR v2.COLUMNA02 is null) and v2.COLUMNA03=p.COLUMNA03 and isnull(v2.COLUMNA13,0)=0 and v2.COLUMN05 = p.column04

left outer join SATABLE011 s11 on CAST(s11.COLUMN04 as nvarchar(250))=cast(p.column05 as nvarchar(250)) and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on CAST(s5.COLUMN04 as nvarchar(250))=cast(p.column05 as nvarchar(250)) and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on CAST(f4.COLUMN04 as nvarchar(250))=cast(p.column05 as nvarchar(250)) and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03 and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on CAST(f1.COLUMN04 as nvarchar(250))=cast(p.column05 as nvarchar(250)) and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03 and isnull(f1.COLUMNA13,0)=0  
left outer join FITABLE023 f2 on CAST(f2.COLUMN04 as nvarchar(250))=cast(p.column05 as nvarchar(250)) and f2.COLUMNA02=p.COLUMNA02 and f2.COLUMNA03=p.COLUMNA03 and isnull(f2.COLUMNA13,0)=0 
left outer join FITABLE001 f on f.COLUMN02=p.COLUMN03 and f.COLUMNA03=p.COLUMNA03  and isnull(f.COLUMNA13,0)=0 
left outer join MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,f2.COLUMNA08,j.COLUMNA08,v2.COLUMNA08) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0 
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner AND p.COLUMN03=@ide order by RNO desc
end
--Expense Tracking
else if (@Type = 22344)
begin
if (@ide =1061)
begin
select isnull(p.COLUMN09,'') 'Tran#', p.COLUMN04 Type,FORMAT(p.[COLUMN03],@dateF) as Date,
(case when p.COLUMN04='EXPENSE' then ma.column06  when p.COLUMN04='BILL' then  sa.column05  when p.COLUMN04='INVOICE' or p.COLUMN04='Credit Memo' then s2.column05 when p.COLUMN04='COMMISSION PAYMENT' then ma.column09 else sa.column05 end)  Party,
o.column03 as [Operating Unit],pr.COLUMN05 as Project,'' Reference#,p.COLUMN07 'Memo',p.COLUMN12 'Increased Amount',
(isnull(cast(p.COLUMN11 as decimal(18,2)),0)+isnull(cast(p.COLUMN18 as decimal(18,2)),0)) 'Decreased Amount',
(SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(cast(p.COLUMN11 as decimal(18,2)),0)+isnull(cast(p.COLUMN18 as decimal(18,2)),0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN03) RNO,ma1.column09 [Created by] from FITABLE027 p 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN17 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0  
left outer join CONTABLE007 o on o.column02=p.columnA02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0  
left outer join CONTABLE002 cn on cn.column02=p.columna03 and isnull(o.COLUMNA13,0)=0
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN14  and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMN01=p.COLUMN05 and s9.COLUMNA03=p.COLUMNA03 and s9.COLUMNA02=p.COLUMNA02 and isnull(s9.COLUMNA13,0)=0  
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0  
left outer join PUTABLE014 p14 on p14.COLUMN04=p.column09 and p14.COLUMNA03=p.COLUMNA03 and isnull(p14.COLUMNA13,0)=0  
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0  
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0  
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMN01=p.column05 and isnull(p5.COLUMNA13,0)=0  
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,p14.COLUMNA08) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and p.COLUMN03  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
else
select isnull(p.COLUMN09,'') 'Tran#', p.COLUMN03 Type,FORMAT(p.[COLUMN04],@dateF) as Date,
(case when (p.COLUMN03='EXPENCE' or p.COLUMN03='EXPENSE') then ma.column06  when (p.COLUMN03='BILL' or p.COLUMN03='Debit Memo') then  sa.column05  when (p.COLUMN03='INVOICE' or p.COLUMN03='Credit Memo' or p.COLUMN03='Resource Consumption Direct') then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case 
when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05 
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) 
when p.COLUMN03='PAYMENT VOUCHER' then (case when f20.COLUMN11=22305 or f20.COLUMN11=22700 then  sa.COLUMN05 when f20.COLUMN11=22335 then s2.COLUMN05 when f20.COLUMN11=22492 then ma.COLUMN09 when  f20.COLUMN11=22306 then ma.COLUMN06 when f20.COLUMN11=22334 then sa.COLUMN05 else '' end) 
WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when f23.column20=22305
or f23.column20=22700 then sa.COLUMN05 when f23.column20=22335 then s2.COLUMN05 when f23.COLUMN20=22492 then ma.COLUMN09 when  f23.COLUMN11=22306 then ma.COLUMN06 when f23.COLUMN11=22334 then sa.COLUMN05 else '' end)
else (sa.column05) end)  Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,(case when p.COLUMN03='BILL' THEN p5.COLUMN09 when p.COLUMN03='INVOICE' THEN s9.COLUMN09 else '' end) Reference#,(CASE WHEN p.COLUMN03='EXPENSE' THEN f21.COLUMN05
WHEN p.COLUMN03='PAYMENT VOUCHER' THEN f22.COLUMN04 
WHEN p.COLUMN03='RECEIPT VOUCHER' THEN f24.COLUMN04
WHEN p.COLUMN03='JOURNAL ENTRY' THEN fj.COLUMN06
WHEN p.COLUMN03='BILL' THEN p5.COLUMN12
WHEN p.COLUMN03='INVOICE' THEN s9.COLUMN12
WHEN p.COLUMN03='EXPENSE' OR p.COLUMN03='EXPENCE' THEN f21.COLUMN05
ELSE p.COLUMN05 END) 'Memo',  SUM(isnull(p.COLUMN07,0)) 'Increased Amount',
SUM(isnull(p.COLUMN08,0)) 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,ma1.COLUMN09 'Created by' from FITABLE036 p 
left join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03  
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11 and pr.COLUMNA03=p.COLUMNA03 AND ISNULL(pr.COLUMNA13,0)=0 
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 AND ISNULL(o.COLUMNA13,0)=0 
left outer join CONTABLE002 cn on cn.COLUMN02=p.COLUMNA03 AND ISNULL(cn.COLUMNA13,0)=0
 left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 AND ISNULL(sa.COLUMNA13,0)=0 
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 AND ISNULL(s2.COLUMNA13,0)=0
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 AND ISNULL(ma.COLUMNA13,0)=0
left outer join FITABLE031 j on  j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 
left outer join SATABLE009 s9 on cast(s9.COLUMN01 as nvarchar(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 and p.COLUMN03='Invoice' 
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03))  and p.COLUMN03='JOURNAL ENTRY' 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) and (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) and p.COLUMN03='JOURNAL ENTRY' 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN17=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0
left outer join FITABLE021 f21 on f21.COLUMN06=f1.column01 and f21.COLUMNA02=f1.COLUMNA02 and f21.COLUMNA03=f1.COLUMNA03 and isnull(f21.COLUMNA13,0)=0  AND p.COLUMN10= f21.column03  AND p.COLUMN07= f21.column04
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
left outer join FITABLE031 f3 on f3.COLUMN04=p.column05 and f3.COLUMNA03=p.COLUMNA03 and isnull(f3.COLUMNA13,0)=0 and f3.COLUMNA02=p.COLUMNA02 and p.COLUMN03='JOURNAL ENTRY' 
left outer join FITABLE020 f20 on f20.COLUMN04=p.column09 and f20.COLUMNA03=p.COLUMNA03 and f20.COLUMNA02=p.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
left outer join FITABLE022 f22 on f20.COLUMN01=f22.column09 and f22.COLUMNA02=f20.COLUMNA02 and f20.COLUMNA03=f22.COLUMNA03 and isnull(f22.COLUMNA13,0)=0  AND p.COLUMN10= f22.column03 
AND p.COLUMN07= f22.column05 
left outer join FITABLE023 F23 ON CAST(F23.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) and F23.COLUMNA02=p.COLUMNA02 and F23.COLUMNA03=p.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
left outer join FITABLE024 F24 ON F24.COLUMN09=F23.COLUMN01 and F24.COLUMNA02=F23.COLUMNA02 and F24.COLUMNA03=F23.COLUMNA03 and isnull(F24.COLUMNA13,0)=0 AND p.COLUMN10= f24.column03  AND p.COLUMN08= f24.column05 
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,f3.COLUMNA08,f20.COLUMNA08,f23.COLUMNA08,j.COLUMNA08)  and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN10=@ide  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner
group by p.COLUMN09,p.COLUMN03,p.COLUMN04,ma.column06,sa.column05,s2.column05,j.column07,fj.column14,ma.COLUMN09,f20.COLUMN11,p.COLUMN07,p.COLUMN08,
o.COLUMN03,pr.COLUMN05,p5.COLUMN09,s9.COLUMN09,f21.COLUMN05,ma1.COLUMN09,f24.COLUMN04,f22.COLUMN04,fj.COLUMN06,p5.COLUMN12,s9.COLUMN12,p.COLUMN05,f23.column20,f23.column11 order by RNO desc
end

--Equity Register
else if (@Type = 22330)
begin
select isnull(p.COLUMN11,'') 'Tran#',p.COLUMN03 Type,FORMAT(p.[COLUMN04 ],@dateF) as Date,
(case  when p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end)
WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='ISSUE CHEQUE') then (case when pv.column11=22305 or pv.column11=22700 then sa.COLUMN05 when pv.column11=22335 then s2.COLUMN05 when pv.column11=22492 then ma.COLUMN09 else '' end)
 WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 then sa.COLUMN05 when rv.column20=22335 then s2.COLUMN05 when rv.column20=22492 then ma.COLUMN09 else '' end)
 when (p.COLUMN03='EXPENCE' or  p.COLUMN03='EXPENSE') then ma.column06 else iif(p.COLUMN03='Inventory Adjustment','', sa.COLUMN05)  end) party,
o.column03 [Operating unit],pr.COLUMN05 as Project,'' 'Reference#',p.COLUMN08 Memo,p.COLUMN09 'Increased Amount',
p.COLUMN10 'Decreased Amount',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,ma1.COLUMN09 [Created By] from FITABLE029 p 
inner join FITABLE001 m on m.COLUMN02=p.COLUMN07 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join MATABLE010 ma on ma.column02=p.column06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0
 left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06  and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0
  left outer join FITABLE031 j on  j.COLUMN04=p.COLUMN11 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02 
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and fj.COLUMNA02=j.COLUMNA02 and isnull(fj.COLUMNA13,0)=0 and fj.COLUMN03=p.COLUMN07 
AND (fj.COLUMN04=p.COLUMN10 or fj.COLUMN05=p.COLUMN09) 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0) 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0   
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0  
left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN11 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN11 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02  and isnull(rv.COLUMNA13,0)=0
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (f4.COLUMNA08,f1.COLUMNA08,j.COLUMNA08,m.COLUMNA08,iif(isnull(p.COLUMNA08,0)>0,p.COLUMNA08,0)) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and p.COLUMN04  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner and m.COLUMN02=@ide order by RNO desc
end
else if(@Type = 223831 or @Type = 223832)
--select*from fitable064 where column11 = 4670 and columna03 = 56590
begin
SELECT Tran#, a.Type,a.Date, a.Party,a.[Operating Unit],a.Project,a.Reference#,a.Memo,a.[Increased Amount], a.[Decreased Amount],
(sum(SUM(cast(isnull([Balance Amount],0)as decimal(18,2)))) OVER(ORDER BY Date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))[Balance Amount] ,a.[Created By] from
(
	SELECT isnull(p.COLUMN04,'') 'Tran#',p.COLUMN03 Type,FORMAT(p.[COLUMN07],@dateF) as Date,
	(case	when ( p.column08 = '22334' or p.column08 = '22654' or p.column08 = '22700' or p.column08 = '22305' or p.column03='BILL' or p.column03='Debit Memo') then sa.COLUMN05  
			when ( p.column08 = '22335' or p.COLUMN03='INVOICE' or p.column03='Credit Memo') then s2.COLUMN05 
			when p.column08='22492' or p.column08='22306' then ma.COLUMN09 
			else  '' end) Party,o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#,p.column12 'Memo',
	 (isnull(p.[COLUMN14],0)) AS 'Increased Amount', 
	(isnull(p.[COLUMN15],0)) AS 'Decreased Amount',
cast((ISNULL(p.column14,0)-ISNULL(p.column15,0))as decimal(18,2))[Balance Amount] ,ma1.COLUMN09 'Created By',MONTH(p.COLUMN07) MNTH,p.[COLUMN07] dtt From	FITABLE064 p 
	left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN09 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN09 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN09 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN13 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join  MATABLE010 ma1 on ma1.COLUMN02 = p.COLUMNA08  and ma1.COLUMNA03=p.COLUMNA03 and  isnull(ma1.COLUMNA13,0)=0
where  isnull((p.COLUMNA13),0)=0  and p.COLUMN07 between @FromDate and @ToDate AND p.COLUMN11 = @ide AND p.COLUMN10= '22383'  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner 
)a group by dtt,Tran#, Type,Date, Party,[Operating Unit],Project,Reference#,Memo,[Increased Amount], [Decreased Amount] ,[Created By]
order by dtt desc

end
--Current Liabilities
else if (@Type = 223831)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN04 Type,FORMAT(p.[COLUMN03],@dateF) as Date,
(case when (p.column04='BILL' or p.column04='Debit Memo') then sa.COLUMN05  when (p.COLUMN04='INVOICE' or p.column04='Credit Memo') then s2.COLUMN05 when p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN04='ADVANCE PAYMENT' or p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when   v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when p.COLUMN04='ADVANCE RECEIPT' then s2.COLUMN05  when p.COLUMN04='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end) else  '' end) Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#,
(CASE WHEN p.COLUMN04 ='JOURNAL ENTRY' THEN fj.COLUMN06 
WHEN p.COLUMN04 ='ADVANCE PAYMENT' THEN v.COLUMN08
ELSE  p.COLUMN07 END) 'Memo',p.COLUMN11 'Increased Amount',
p.COLUMN12 'Decreased Amount',(SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN03  ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],ma1.COLUMN09 [Created By],row_number() over(order by p.COLUMN03) RNO,ma1.COLUMN09 'Created By' from FITABLE026 p 
left join MATABLE013 m on m.COLUMN02=p.COLUMN08 left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN18 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN19 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join conTABLe002 cn on cn.column02=p.columna03 and isnull(cn.COLUMNA13,0)=0
left join FITABLE001 f on f.COLUMN02=@ide and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and v.COLUMNA02=p.COLUMNA02 and v.column01=p.column05 and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and sa.COLUMNA02=p.COLUMNA02 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03))  
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA03=p.COLUMNA03 and v1.COLUMNA02=p.COLUMNA02 and isnull(v1.COLUMNA13,0)=0  
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMN01=p.COLUMN05 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0  
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0  
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0  
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0  
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0   
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMN01=p.column05 and isnull(p5.COLUMNA13,0)=0   
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08)  and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where p.COLUMN08=@ide AND (p.COLUMN17 like ('INPUT%')) and isnull((p.COLUMNA13),0)=0 and p.COLUMN03  between @FromDate and @ToDate  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Current Liabilities
else if (@Type = 223832)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN04 Type,FORMAT(p.[COLUMN03],@dateF) as Date, 
(case when (p.column04='BILL' or p.column04='Debit Memo') then sa.COLUMN05  when (p.COLUMN04='INVOICE' or p.column04='Credit Memo') then s2.COLUMN05 when p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN04='ADVANCE PAYMENT' or p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when   v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT' then s2.COLUMN05  when p.COLUMN04='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end) else  '' end) Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,(case when p.COLUMN04='BILL' THEN p5.COLUMN09 when p.COLUMN04='INVOICE' THEN s9.COLUMN09 else '' end) Reference#,(CASE WHEN p.COLUMN04 ='JOURNAL ENTRY' THEN fj.COLUMN06 WHEN p.COLUMN04 ='RECEIVE PAYMENT' or p.COLUMN04 ='ADVANCE RECEIPT' THEN v1.COLUMN11 ELSE  p.COLUMN07 END) 'Memo',p.COLUMN11 'Increased Amount',
p.COLUMN12 'Decreased Amount',(SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN03  ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],ma1.COLUMN09 [Created By],row_number() over(order by p.COLUMN03) RNO,ma1.COLUMN09 [Created By] from FITABLE026 p 
left join MATABLE013 m on m.COLUMN02=p.COLUMN08 
left outer join FITABLE045 ep on ep.COLUMN01=p.COLUMN05 and ep.COLUMNA03=p.COLUMNA03 and isnull(ep.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN19 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join conTABLe002 cn on cn.column02=p.columna03 and isnull(cn.COLUMNA13,0)=0
left join FITABLE001 f on f.COLUMN02=@ide and f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,0)=0
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and v.COLUMNA02=p.COLUMNA02 and v.column01=p.column05 and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMNA02=p.COLUMNA02 and j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03))  
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMN01=p.COLUMN05 and s9.COLUMNA02=p.COLUMNA02 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0  
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0  
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0  
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0  
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0   
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMN01=p.column05 and isnull(p5.COLUMNA13,0)=0  
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where p.COLUMN08=@ide AND (p.COLUMN17 like ('OUTPUT%')) and isnull((p.COLUMNA13),0)=0 and p.COLUMN03  between @FromDate and @ToDate  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Current Liabilities
else if (@Type = 22383)
begin
if (@ide =1101)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN04 Type,FORMAT(p.[COLUMN03],@dateF) as Date,
(case when (p.column04='BILL' or p.column04='Debit Memo') then sa.COLUMN05  when (p.COLUMN04='INVOICE' or p.column04='Credit Memo') then s2.COLUMN05 when p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN04='ADVANCE PAYMENT' or p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when   v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when p.COLUMN04='ADVANCE RECEIPT' then s2.COLUMN05  when p.COLUMN04='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end) else  '' end) Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#,(CASE WHEN p.COLUMN04 ='JOURNAL ENTRY' THEN fj.COLUMN06 ELSE  p.COLUMN07 END) 'Memo',p.COLUMN11 'Increased Amount',
p.COLUMN12 'Decreased Amount',(SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN03  ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],ma1.COLUMN09 [Created By],row_number() over(order by p.COLUMN03) RNO,ma1.COLUMN09 'Created By' from FITABLE026 p 
left join MATABLE013 m on m.COLUMN02=p.COLUMN08 left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN18 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN19 left outer join CONTABLE007 o on o.column02=p.columna02 
left outer join conTABLe002 cn on cn.column02=p.columna03 
left join FITABLE001 f on f.COLUMN02=@ide and f.COLUMNA03=p.COLUMNA03 
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and v.column01=p.column05 and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0  left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and fj.COLUMNA02=j.COLUMNA02 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03)) and (((p.COLUMN12) in (fj.COLUMN05)) or ((p.COLUMN11) in (fj.COLUMN04)))   
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0  
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMN01=p.COLUMN05 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMN01=p.column05 and isnull(p5.COLUMNA13,0)=0  
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08,f.columna08)   and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where p.COLUMN17=(select COLUMN04 from FITABLE001 where COLUMN02=@ide AND COLUMNA03=@AcOwner) and isnull((p.COLUMNA13),0)=0 and p.COLUMN03  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner  --AND  isnull(m2.COLUMN02,'')=iif(isnull(@Tds,'')='','',@Tds)
 order by RNO desc
end
else
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN04 Type,FORMAT(p.[COLUMN03],@dateF) as Date, 
(case when (p.column04='BILL' or p.column04='Debit Memo') then sa.COLUMN05  when (p.COLUMN04='INVOICE' or p.column04='Credit Memo') then s2.COLUMN05 when p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN04='ADVANCE PAYMENT' or p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when   v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when p.COLUMN04='ADVANCE RECEIPT' then s2.COLUMN05  when p.COLUMN04='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then  sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end) when (p.COLUMN04='EXPENCE' or  p.COLUMN04='EXPENSE') then ma.column06 else  '' end) Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,(case when p.COLUMN04='BILL' THEN p5.COLUMN09 when p.COLUMN04='INVOICE' THEN s9.COLUMN09 else '' end) Reference#,(case when p.COLUMN04='JOURNAL ENTRY' then fj.COLUMN06 WHEN p.COLUMN04 ='ADVANCE PAYMENT' THEN v.COLUMN08 WHEN p.COLUMN04 ='RECEIVE PAYMENT' or p.COLUMN04 ='ADVANCE RECEIPT' THEN v1.COLUMN11 else p.COLUMN07 end) 'Memo',p.COLUMN11 'Increased Amount',
p.COLUMN12 'Decreased Amount',(SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN12,0)) OVER(ORDER BY p.COLUMN03  ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],ma1.COLUMN09 [Created By],row_number() over(order by p.COLUMN03) RNO from FITABLE026 p 
left join MATABLE013 m on m.COLUMN02=p.COLUMN08 
left outer join FITABLE045 ep on ep.COLUMN01=p.COLUMN05 and ep.COLUMNA03=p.COLUMNA03 and isnull(ep.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN19 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join conTABLe002 cn on cn.column02=p.columna03 and isnull(cn.COLUMNA13,0)=0
left join FITABLE001 f on f.COLUMN02=@ide and f.COLUMNA03=p.COLUMNA03 
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and v.COLUMNA02=p.COLUMNA02 and v.column01=p.column05 and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03)) and (((p.COLUMN11) = (fj.COLUMN05)) or ((p.COLUMN12) = (fj.COLUMN04))) and isnull(p.column06,0) = isnull(fj.column07,0)
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMN01=p.COLUMN05 and s9.COLUMNA02=p.COLUMNA02 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN17=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMN01=p.column05 and isnull(p5.COLUMNA13,0)=0  
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where (p.column17=f.COLUMN04 or p.COLUMN08=@ide) 
AND (p.COLUMN17 NOT LIKE ('INPUT%')) AND (p.COLUMN17 NOT LIKE ('OUTPUT%')) 
and isnull((p.COLUMNA13),0)=0 and p.COLUMN03  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
end
--Current Assets
else if (@Type = 22402)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN03 Type,FORMAT(p.[COLUMN04],@dateF) as Date,
(case when (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT') then sa.column05  when p.COLUMN03='PAYMENT' then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when (p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='PDC RECEIVED' or p.COLUMN03='Sales Claim Register') then s2.COLUMN05 
WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when v1.column20=22305 or v1.column20=22700 then sa.COLUMN05 when v1.column20='22335' then s2.COLUMN05 when v1.column20=22492 then ma.COLUMN06 else '' end)
when p.COLUMN03='EXPENSE' then ma.COLUMN09 else (sa.column05) end)  Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#, p.COLUMN05 'Memo',p.COLUMN07 'Increased Amount',
p.COLUMN08 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,ma1.COLUMN09 'Created By' from FITABLE034 p 
left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11 and m2.COLUMNA03=p.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join CONTABLe002 cn on cn.column02=p.columna03 and isnull(cn.COLUMNA13,0)=0
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and (v.COLUMNA02=p.COLUMNA02 OR v.COLUMNA02 is null) and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03))  and ((p.COLUMN06) in (fj.COLUMN07)) and ((p.COLUMN07) in (fj.COLUMN04)) and ((p.COLUMN08) in (fj.COLUMN05))   
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
left outer join PUTABLE014 p4 on p4.COLUMN04=p.column09 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03  and isnull(p4.COLUMNA13,0)=0  
left join FITABLE001 F10 ON F10.COLUMN02= p.COLUMN10 AND F10.COLUMNA03= p.COLUMNA03 AND ISNULL(F10.COLUMNA13,0)=0
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08,p4.columna08) and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN10=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner  AND  isnull(m2.COLUMN02,'')=iif(isnull(@Tds,'')='','',@Tds) order by RNO desc
end
--Fixed Assets
else if (@Type = 22403)
begin
select isnull(p.COLUMN06,'') 'Tran#',p.COLUMN05 Type,FORMAT(p.[COLUMN04 ],@dateF) as Date,
(case when (p.COLUMN05='BILL' or p.COLUMN05='BILL PAYMENT') then sa.column05  when p.COLUMN05='PAYMENT' then s2.column05
WHEN (p.COLUMN05='PAYMENT VOUCHER' or p.COLUMN05='ADVANCE PAYMENT' or p.COLUMN05='ISSUE CHEQUE') then (case when pv.column11=22305 or pv.column11=22700 then sa.COLUMN05 when pv.column11=22335 then s2.COLUMN05 when pv.column11=22492 then ma.COLUMN09 else '' end)
 WHEN (p.COLUMN05='RECEIPT VOUCHER' or p.COLUMN05='ADVANCE RECEIPT' or p.COLUMN05='RECEIVE PAYMENT') then (case when rv.column20=22305 or rv.column20=22700 then sa.COLUMN05 when rv.column20=22335 then s2.COLUMN05 when rv.column20=22492 then ma.COLUMN09 else '' end)
 when p.COLUMN05='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 
 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 
 
 else '' end) when (p.COLUMN05='EXPENSE' or p.COLUMN05='EXPENCE') then ma.COLUMN06 end)  Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#, p.COLUMN08 'Memo',p.COLUMN09 'Increased Amount',
p.COLUMN10 'Decreased Amount',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))'Balance Amount',row_number() over(order by p.COLUMN04) RNO,
ma1.COLUMN09 'Created By' from FITABLE028 p 
left join FITABLE001 m on m.COLUMN02=p.COLUMN03  and m.COLUMNA03=p.COLUMNA03 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11 left outer join CONTABLE007 o on o.column02=p.columna02 
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN07 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN07 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0  
left outer join FITABLE031 j on j.COLUMN04=p.COLUMN06 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02  --left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (fj.COLUMN03))  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN03) in (fj.COLUMN03)) and (((p.COLUMN09) in (fj.COLUMN04)) or ((p.COLUMN10) in (fj.COLUMN05))) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN07,0)
left outer join SATABLE009 s9 on s9.COLUMN04=p.column06  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column06 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
left outer join PUTABLE014 p4 on p4.COLUMN04=p.column06 and p4.COLUMNA03=p.COLUMNA03  and isnull(p4.COLUMNA13,0)=0 
left outer join FITABLE020 pv on pv.COLUMN04=p.COLUMN06 and pv.COLUMNA03=p.COLUMNA03 and pv.COLUMNA02=p.COLUMNA02 and isnull(pv.COLUMNA13,0)=0
left outer join FITABLE023 rv on rv.COLUMN04=p.COLUMN06 and rv.COLUMNA03=p.COLUMNA03 and rv.COLUMNA02=p.COLUMNA02 and isnull(rv.COLUMNA13,0)=0   
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,p4.columna08,pv.COLUMNA08,rv.COLUMNA08)  and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and p.COLUMN04  between @FromDate and @ToDate AND  p.COLUMN03=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Income
else if (@Type = 22405)
begin
select (CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then isnull((j.column04),'')  WHEN p.COLUMN04='RECEIPT VOUCHER' then isnull((f23.column04),'')   WHEN p.COLUMN04='PAYMENT VOUCHER' then isnull((f20.column04),'')  when p.COLUMN04='Credit Memo' then isnull(s5.column04,'')   when (p.COLUMN04='INVOICE' or (p.COLUMN04='Markdown Register' and isnull(p.COLUMN09,0)>0)) then isnull((s10.column04),S91.column04)    when p.COLUMN04='PAYMENT' then isnull((s12.column04),'')    when (p.COLUMN04='Credit Memo' or (p.COLUMN04='Markdown Register' and isnull(p.COLUMN11,0)>0)) then isnull((s5.column04),'')    when p.COLUMN04='Debit Memo' then isnull((p1.column04),'')    when p.COLUMN04='BILL' then isnull((p5.column04),'') 
when p.COLUMN04='EXPENSE' then isnull((f1.column17),'') else '' end )'Tran#',  
p.COLUMN04 Type,FORMAT(p.[COLUMN03 ],@dateF) as Date,  (CASE  WHEN (p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='ADVANCE PAYMENT') then (case when f20.column11='22305' or f20.column11='22700' then sa.COLUMN05 when f20.column11='22335' then s2.COLUMN05 when f20.column11='22492' then ma.COLUMN09 else '' end)
 WHEN (p.COLUMN04='RECEIPT VOUCHER' or p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT') then (case when f23.column20=22305 or f23.column20=22700 then sa.COLUMN05 when f23.column20='22335' then s2.COLUMN05 when f23.column20='22492' then ma.COLUMN09 else '' end)
 when p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
 when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN04='INVOICE' or p.COLUMN04='Credit Memo' or p.COLUMN04='Markdown Register') then s2.column05   when (p.COLUMN04='BILL' or p.COLUMN04='Service' or p.COLUMN04='Debit Memo') then sa.column05 when p.COLUMN04='EXPENSE' then ma.column09 else '' end ) 'Party',
i.COLUMN04 Item,cast(cn.COLUMN03 as nvarchar(250)) as 'Operating Unit',pr.COLUMN05 as Project,'' Reference#,p.COLUMN07 'Memo',
p.COLUMN09 as 'Increased Amount',p.COLUMN11 as 'Decreased Amount',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as 'Balance Amount',row_number() over(order by p.COLUMN03) RNO,
ma1.COLUMN09 'Created By' from FITABLE025 p 
left join FITABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 AND ISNULL(m.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 cn on cn.COLUMN02=p.COLUMNA02 and cn.COLUMNA03=p.COLUMNA03 and isnull(cn.COLUMNA13,0)=0
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0  
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and  j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0    
left outer join SATABLE010 s9 on  s9.COLUMN01=p.COLUMN05 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 and (p.COLUMN04='INVOICE' or p.COLUMN04='Markdown Register')  
left outer join SATABLE009 s10 on  s10.COLUMN01=s9.COLUMN15 and s10.COLUMNA03=s9.COLUMNA03 and isnull(s10.COLUMNA13,0)=0  
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03))
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 
and ((p.COLUMN08) in (fj.COLUMN03)) 
--AND (((p.COLUMN09) in (fj.COLUMN05)) or ((p.COLUMN11) in (fj.COLUMN04))) 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)   
left outer join SATABLE012 s11 on s11.COLUMN01=p.column05 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0  and  p.COLUMN04='PAYMENT'  
left outer join SATABLE011 s12 on s12.COLUMN01=s11.column08 and s12.COLUMNA03=p.COLUMNA03 and isnull(s12.COLUMNA13,0)=0  
left outer join SATABLE006 s6 on s6.COLUMN01=p.column05 and s6.COLUMNA03=p.COLUMNA03 and isnull(s6.COLUMNA13,0)=0 and  (p.COLUMN04='Credit Memo' or p.COLUMN04='Markdown Register') 
left outer join SATABLE005 s5 on s5.COLUMN01=s6.column19 and s5.COLUMNA03=s6.COLUMNA03 and isnull(s6.COLUMNA13,0)=0  
left outer join FITABLE045 f4 on  f4.COLUMNA03=p.COLUMNA03 and f4.COLUMN01=p.column05 and isnull(f4.COLUMNA13,0)=0  
left outer join FITABLE012 f1 on  f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0   
left outer join PUTABLE006 p6 on  p6.COLUMNA03=p.COLUMNA03 and p6.COLUMN01=p.column05 and isnull(p6.COLUMNA13,0)=0  and  p.COLUMN04='BILL'  
left outer join PUTABLE005 p5 on  p5.COLUMNA03=p6.COLUMNA03 and p5.COLUMN01=p6.column13 and isnull(p5.COLUMNA13,0)=0   
left outer join PUTABLE002 p2 on  p2.COLUMNA03=p.COLUMNA03 and p2.COLUMN01=p.column05 and isnull(p2.COLUMNA13,0)=0  and  p.COLUMN04='Debit Memo'  
left outer join PUTABLE001 p1 on  p1.COLUMNA03=p6.COLUMNA03 and p1.COLUMN01=p6.column19 and isnull(p1.COLUMNA13,0)=0  
left outer join MATABLE007 i on  i.COLUMN02=iif(p.COLUMN04='Credit Memo',p.column13,s9.column05) and isnull(i.COLUMNA13,0)=0  
left outer join FITABLE020 f20 on  f20.COLUMN01=p.COLUMN05 and f20.COLUMNA03=p.COLUMNA03 and f20.COLUMNA02=p.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
left outer join FITABLE023 f23 on  f23.COLUMN01=p.COLUMN05 and f23.COLUMNA03=p.COLUMNA03 and f23.COLUMNA02=p.COLUMNA02 and isnull(f23.COLUMNA13,0)=0
left outer join SATABLE009 S91 on  S91.COLUMN01=p.COLUMN05 and S91.COLUMNA03=p.COLUMNA03 and S91.COLUMNA02=p.COLUMNA02 and isnull(S91.COLUMNA13,0)=0     
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,f20.COLUMNA08,f23.COLUMNA08)   and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and p.COLUMN08=@ide and  p.COLUMN03  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner 
UNION ALL
select F35.column10 'Tran#',  
F35.COLUMN03 Type,FORMAT(F35.[COLUMN04],@dateF) as Date,  '' 'Party',
'' Item,cast(C7.COLUMN03 as nvarchar(250)) as 'Operating Unit','' as Project,'' Reference#,'' 'Memo',
F35.COLUMN07 as 'Increased Amount',F35.COLUMN08 as 'Decreased Amount',(SUM(isnull(F35.COLUMN07,0)) OVER(ORDER BY F35.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(F35.COLUMN08,0)) OVER(ORDER BY F35.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as 'Balance Amount',row_number() over(order by F35.COLUMN04) RNO,
ma1.COLUMN09 'Created By' from FITABLE035 F35
left outer join FITABLE001 m ON F35.COLUMN10=m.COLUMN02 AND F35.COLUMNA03=m.COLUMNA03
LEFT OUTER JOIN CONTABLE007 C7 ON C7.COLUMN02 = F35.COLUMNA02 AND ISNULL(C7.COLUMNA13,0)=0
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (m.COLUMNA08)
where F35.COLUMN10=@ide 
and F35.COLUMN04 between @FromDate and @ToDate AND F35.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
AND F35.COLUMNA03=@AcOwner order by RNO desc
end
--Cost Of Goods Sold
else if (@Type = 22406)
begin
select (CASE  WHEN p.COLUMN04='JOURNAL ENTRY' then isnull((j.column04),'')   when p.COLUMN04='INVOICE' then isnull((s10.column04),'')    when p.COLUMN04='PAYMENT' then isnull((s11.column04),'')  when p.COLUMN04='Credit Memo' then isnull((s5.column04),'')    when p.COLUMN04='Debit Memo' then isnull((p1.column04),'')    when p.COLUMN04='RECEIPT VOUCHER' then isnull((f23.column04),'')    when p.COLUMN04='Inventory Adjustment' then isnull((f14.column04),'')   
 when p.COLUMN04='BILL' then isnull((p5.column04),'') 
 WHEN p.COLUMN04='EXPENSE' then isnull((f1.column17),'')
WHEN p.COLUMN04='PAYMENT VOUCHER' then isnull((f20.column04),'')
 else '' end )'Tran#',  
p.COLUMN04 Type,FORMAT(p.[COLUMN03],@dateF) as Date,  
(CASE  when p.COLUMN04='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end)  when (p.COLUMN04='INVOICE' or p.COLUMN04='Credit Memo') then s2.column05   when (p.COLUMN04='BILL' or p.COLUMN04='Service' or p.COLUMN04='Debit Memo') then sa.column05 
WHEN (p.COLUMN04='PAYMENT VOUCHER' or p.COLUMN04='ADVANCE PAYMENT') then (case when f20.column11='22305' or f20.column11='22700' then sa.COLUMN05 when f20.column11='22335' then s2.COLUMN05 when f20.column11='22492' then ma.COLUMN09 else '' end)
 WHEN (p.COLUMN04='RECEIPT VOUCHER' or p.COLUMN04='ADVANCE RECEIPT' or p.COLUMN04='RECEIVE PAYMENT') then (case when f23.column20=22305 or f23.column20=22700 then sa.COLUMN05 when f23.column20='22335' then s2.COLUMN05 when f23.column20='22492' then ma.COLUMN09 else '' end)
when p.COLUMN04='EXPENSE' then ma.column09 else '' end )
 'Party',
(cn.COLUMN03) as 'Operating Unit',pr.COLUMN05 as Project,'' Reference#,p.COLUMN07 'Memo',p.COLUMN09 as 'Increased Amount',
p.COLUMN11 as 'Decreased Amount',(SUM(isnull(p.COLUMN09,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN03 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) as 'Balance Amount',row_number() over(order by p.COLUMN03) RNO,
ma1.COLUMN09 'Created By' from FITABLE025 p 
left join FITABLE001 m on m.COLUMN02=p.COLUMN08 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 left outer join CONTABLE007 cn on cn.COLUMN02=p.COLUMNA02 
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE012 f1 on  f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN01=p.column05 and isnull(f1.COLUMNA13,0)=0 
left outer join FITABLE020 f20 on  f20.COLUMN01=p.COLUMN05 and f20.COLUMNA03=p.COLUMNA03 and isnull(f20.COLUMNA13,0)=0 
left outer join FITABLE031 j on j.COLUMN01=p.COLUMN05 and  j.COLUMNA03=p.COLUMNA03 and  j.COLUMNA02=p.COLUMNA02 and isnull(j.COLUMNA13,0)=0  and p.COLUMN04='JOURNAL ENTRY'   
left outer join SATABLE010 s9 on  s9.COLUMN01=p.COLUMN05 and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0    
left outer join SATABLE009 s10 on  s10.COLUMN01=s9.COLUMN15 and s10.COLUMNA03=s9.COLUMNA03 and isnull(s10.COLUMNA13,0)=0  and p.COLUMN04='INVOICE'  
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN08) in (fj.COLUMN03)) and (fj.COLUMN04=p.COLUMN09 or fj.COLUMN05=p.COLUMN11) and ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)  
left outer join SATABLE012 s12 on s12.COLUMN01=p.column05 and s12.COLUMNA03=p.COLUMNA03 and isnull(s12.COLUMNA13,0)=0  
left outer join SATABLE011 s11 on s11.COLUMN01=s12.column08 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 and p.COLUMN04='PAYMENT' 
left outer join SATABLE006 s6 on s6.COLUMN01=p.column05 and s6.COLUMNA03=p.COLUMNA03 and isnull(s6.COLUMNA13,0)=0  
left outer join SATABLE005 s5 on s5.COLUMN01=s6.column19 and s6.COLUMNA03=p.COLUMNA03 and isnull(s6.COLUMNA13,0)=0  and p.COLUMN04='Credit Memo' 
left outer join PUTABLE002 p2 on p2.COLUMN01=p.column05 and p2.COLUMNA03=p.COLUMNA03 and isnull(p2.COLUMNA13,0)=0  
left outer join PUTABLE001 p1 on p1.COLUMN01=p2.column19 and p1.COLUMNA03=p.COLUMNA03 and isnull(p1.COLUMNA13,0)=0  and p.COLUMN04='Debit Memo' 
left outer join FITABLE015 ia1 on  ia1.COLUMNA03=p.COLUMNA03 and ia1.COLUMN01=p.column05 and isnull(ia1.COLUMNA13,0)=0   
left outer join FITABLE014 f14 on  f14.COLUMNA03=ia1.COLUMNA03 and f14.COLUMN01=ia1.column11 and isnull(f14.COLUMNA13,0)=0 and p.COLUMN04='Inventory Adjustment'   
left outer join PUTABLE006 p6 on  p6.COLUMNA03=p.COLUMNA03 and p6.COLUMN01=p.column05 and isnull(p6.COLUMNA13,0)=0   
left outer join PUTABLE005 p5 on  p5.COLUMNA03=p6.COLUMNA03 and p5.COLUMN01=p6.column13 and isnull(p5.COLUMNA13,0)=0  and p.COLUMN04='BILL'  
left outer join FITABLE023 f23 on  f23.COLUMN01=p.COLUMN05 and f23.COLUMNA03=p.COLUMNA03 and f23.COLUMNA02=p.COLUMNA02 and isnull(f23.COLUMNA13,0)=0 and p.COLUMN04='RECEIPT VOUCHER'  
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (ia1.COLUMNA08,s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,
f23.COLUMNA08,f20.COLUMNA08)   and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and p.COLUMN08=@ide and  p.COLUMN03  between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Other Income
else if (@Type = 22407)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN03 Type,FORMAT(p.[COLUMN04],@dateF) as Date,  
(CASE  WHEN p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end)
WHEN (p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='ADVANCE PAYMENT') then (case when f20.column11='22305' or  f20.column11='22700' then sa.COLUMN05 when f20.column11='22335' then s2.COLUMN05 when f20.column11='22492' then ma.COLUMN09 else '' end)
WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when f23.column20=22305 or f23.column20=22700  then sa.COLUMN05 when f23.column20='22335' then s2.COLUMN05 when f23.column20='22492' then ma.COLUMN09 else '' end)
when p.COLUMN03='INVOICE' then (s2.column05) when p.COLUMN03='Credit Memo' then (s2.column05) when p.COLUMN03='BILL' then (sa.column05) when (p.COLUMN03='EXPENSE' or p.COLUMN03='EXPENCE') then ma.COLUMN06 end ) 'Party',
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,(case when p.COLUMN03='BILL' THEN p5.COLUMN09 when p.COLUMN03='INVOICE' THEN s9.COLUMN09 else '' end) Reference#,(case when p.COLUMN03='BILL' THEN p5.COLUMN12 when p.COLUMN03='INVOICE' THEN s9.COLUMN12 
when p.COLUMN03='Credit Memo' THEN s5.COLUMN09
else p.COLUMN05 end) 'Memo',p.COLUMN07 'Increased Amount',
p.COLUMN08 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,
ma1.COLUMN09 [Created By] from FITABLE035 p 
left join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.COLUMNA02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join CONTABLE002 cn on cn.COLUMN02=p.COLUMNA03  and isnull(cn.COLUMNA13,0)=0
left outer join SATABLE009 s9 on s9.COLUMN04=p.COLUMN09 and CAST(s9.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join PUTABLE005 p5 on p5.COLUMN04=p.COLUMN09 and CAST(p5.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMNA02=p.COLUMNA02  and isnull(p5.COLUMNA13,0)=0  
left outer join SATABLE005 s5 on s5.COLUMN04=p.COLUMN09 and CAST(s5.COLUMN01 AS NVARCHAR(250))=p.COLUMN05 and s5.COLUMNA02=p.COLUMNA02  and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0   
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0   
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and (fj.COLUMN03)=p.COLUMN10 --AND (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
left outer join FITABLE020 f20 on  f20.COLUMN04=p.COLUMN09 and f20.COLUMNA03=p.COLUMNA03 and f20.COLUMNA02=p.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
left outer join FITABLE023 f23 on  f23.COLUMN04=p.COLUMN09 and f23.COLUMNA03=p.COLUMNA03 and f23.COLUMNA02=p.COLUMNA02 and isnull(f23.COLUMNA13,0)=0   
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,
f23.COLUMNA08,f20.COLUMNA08,s5.COLUMNA08)   and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN10=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
--Other Expense
else if (@Type = 22408)
begin
select isnull(p.COLUMN09,'') 'Tran#', p.COLUMN03 Type,FORMAT(p.[COLUMN04],@dateF)as Date,
(case when p.COLUMN03='PAYMENT VOUCHER' then (case when f20.COLUMN11=22305 or f20.COLUMN11=22700 then  sa.COLUMN05 when f20.COLUMN11=22335 then s2.COLUMN05 when f20.COLUMN11=22492 then ma.COLUMN09 when  f20.COLUMN11=22306 then ma.COLUMN06 when f20.COLUMN11=22334 then sa.COLUMN05 else '' end) 
WHEN (p.COLUMN03='RECEIPT VOUCHER' or p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='RECEIVE PAYMENT') then (case when f23.column20=22305 or f23.column20=22700 then sa.COLUMN05 when f23.column20=22335 then s2.COLUMN05 when f23.COLUMN20=22492 then ma.COLUMN09 when  f23.COLUMN11=22306 then ma.COLUMN06 when f23.COLUMN11=22334 then sa.COLUMN05 else '' end)
when (p.COLUMN03='EXPENCE' or  p.COLUMN03='EXPENSE') then ma.column06  when (p.COLUMN03='BILL' or p.COLUMN03='Debit Memo') then  sa.column05  when (p.COLUMN03='INVOICE' or p.COLUMN03='Credit Memo') then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case 
when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05 
when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) else (sa.COLUMN04) end)  Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#,(CASE WHEN p.COLUMN03 = 'PAYMENT VOUCHER' THEN F22.COLUMN04 
WHEN p.COLUMN03 = 'RECEIPT VOUCHER' THEN F24.COLUMN04
WHEN p.COLUMN03 = 'JOURNAL ENTRY' THEN fj.COLUMN06
WHEN p.COLUMN03 = 'EXPENSE' THEN f21.COLUMN05
WHEN p.COLUMN03 = 'Debit Memo' THEN p1.COLUMN09
WHEN p.COLUMN03 = 'BILL' THEN p5.COLUMN12
ELSE p.COLUMN05 END) 'Memo',  p.COLUMN07 'Increased Amount',
p.COLUMN08 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,ma1.COLUMN09 'Created by' from FITABLE036 p 
left join FITABLE001 m on m.COLUMN02=p.COLUMN10 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN11 AND pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0 
left outer join CONTABLE007 o on o.column02=p.columna02 AND o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 
left outer join CONTABLE002 cn on cn.COLUMN02=p.COLUMNA03 and isnull(cn.COLUMNA13,0)=0
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 AND sa.COLUMNA03=p.COLUMNA03 AND isnull(sa.COLUMNA13,0)=0
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 AND s2.COLUMNA03=p.COLUMNA03 AND isnull(s2.COLUMNA13,0)=0
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 AND ma.COLUMNA03=p.COLUMNA03 AND isnull(ma.COLUMNA13,0)=0
left outer join FITABLE031 j on  j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02  and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN17=p.column09 and f1.COLUMNA02=p.COLUMNA02 and CAST(f1.COLUMN01 AS NVARCHAR(250)) =
CAST(p.column05 AS NVARCHAR(250)) and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA03=p.COLUMNA03 and p5.COLUMNA02=p.COLUMNA02  and isnull(p5.COLUMNA13,0)=0  
left outer join PUTABLE001 p1 on p1.COLUMN04=p.column09 and p1.COLUMNA03=p.COLUMNA03 and p1.COLUMNA02=p.COLUMNA02  and isnull(p1.COLUMNA13,0)=0
left outer join FITABLE031 f3 on f3.COLUMN04=p.column05 and f3.COLUMNA03=p.COLUMNA03 and isnull(f3.COLUMNA13,0)=0 and f3.COLUMNA02=p.COLUMNA02  
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA02=j.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) AND (((p.COLUMN08) in (fj.COLUMN04)) or ((p.COLUMN07) in (fj.COLUMN05))) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and fj.COLUMNA02=j.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) and (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) and p.COLUMN03='JOURNAL ENTRY' 
AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
left outer join FITABLE020 f20 on f20.COLUMN04=p.column09 AND CAST(f20.COLUMN01 AS NVARCHAR(250))= CAST(p.column05 AS NVARCHAR(250)) and f20.COLUMNA03=p.COLUMNA03 and f20.COLUMNA02=p.COLUMNA02 and isnull(f20.COLUMNA13,0)=0  
left outer join FITABLE022 f22 on f20.COLUMN01=f22.column09 and f20.COLUMNA03=f22.COLUMNA03 and f20.COLUMNA02=f22.COLUMNA02 and isnull(f22.COLUMNA13,0)=0 AND F22.COLUMN05=P.COLUMN07 AND F22.COLUMN03=P.COLUMN10
left outer join FITABLE023 F23 ON CAST(F23.COLUMN01 AS NVARCHAR(250))=CAST(p.COLUMN05 AS NVARCHAR(250)) AND f23.COLUMN04=p.column09 and F23.COLUMNA02=p.COLUMNA02 and F23.COLUMNA03=p.COLUMNA03 and isnull(F23.COLUMNA13,0)=0
left outer join FITABLE024 F24 ON F24.COLUMN09=F23.COLUMN01 and F24.COLUMNA03=F23.COLUMNA03 and F24.COLUMNA02=F23.COLUMNA02 and isnull(F24.COLUMNA13,0)=0 AND F24.COLUMN05=P.COLUMN08 AND F24.COLUMN03=P.COLUMN10
left outer join FITABLE021 f21 on f21.COLUMN06=f1.column01 and f21.COLUMNA03=f1.COLUMNA03 and isnull(f21.COLUMNA13,0)=0 and  f21.COLUMNA02=f1.COLUMNA02 AND f21.COLUMN04=P.COLUMN07 AND f21.COLUMN03=P.COLUMN10
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,f3.COLUMNA08,f23.COLUMNA08,f20.COLUMNA08,j.COLUMNA08,p1.COLUMNA08)  and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN10=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner order by RNO desc
end
else if (@Type = 22410)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN03 Type,FORMAT(p.[COLUMN04],@dateF) as Date,
(case when (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT') then sa.column05  when p.COLUMN03='PAYMENT' then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05 
when  j.column07='22700' or fj.column14='22700' then sa.COLUMN05 
when j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when (p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='PDC RECEIVED' or p.COLUMN03='Sales Claim Register') then s2.COLUMN05 
when p.COLUMN03='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 or v1.COLUMN20=22700 then sa.COLUMN05 when v1.COLUMN20=22335 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end)
when p.COLUMN03='EXPENSE' then ma.COLUMN09 else (sa.column05)  end)  Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#, p.COLUMN05 'Memo',p.COLUMN07 'Increased Amount',
p.COLUMN08 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,(case when isnull(p.COLUMNA08,0)>0  then cast(isnull(ma2.COLUMN09,0)as nvarchar(250)) else ma1.COLUMN09 end) 'Created By' from FITABLE060 p 
left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11 and m2.COLUMNA03=p.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
 left outer join conTABLe002 cn on cn.column02=p.columna03 and isnull(cn.COLUMNA13,0)=0
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and (v.COLUMNA02=p.COLUMNA02 OR v.COLUMNA02 is null) and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02 
 left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03))  AND (((p.COLUMN07) in (fj.COLUMN04)) or ((p.COLUMN08) in (fj.COLUMN05))) AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)  
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and v1.COLUMNA02=p.COLUMNA02 and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
left outer join PUTABLE014 p4 on p4.COLUMN04=p.column09 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03  and isnull(p4.COLUMNA13,0)=0  
LEFT OUTER JOIN FITABLE001 F10 ON F10.COLUMN02 = p.COLUMN10 AND F10.COLUMNA03 = p.COLUMNA03 AND ISNULL(F10.COLUMNA13,0)=0
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08,p4.columna08)   and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
left outer join  MATABLE010 ma2 on ma2.COLUMN02 = p.COLUMNA08 and ma2.COLUMNA03 = p.COLUMNA03
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN10=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner  AND  isnull(m2.COLUMN02,'')=iif(isnull(@Tds,'')='','',@Tds) order by RNO desc
end
else if (@Type = 22411)
begin
select isnull(p.COLUMN09,'') 'Tran#',p.COLUMN03 Type,FORMAT(p.[COLUMN04],@dateF) as Date,
(case when (p.COLUMN03='BILL' or p.COLUMN03='BILL PAYMENT') then sa.column05  when p.COLUMN03='PAYMENT' then s2.column05 when p.COLUMN03='JOURNAL ENTRY' then (case when  j.column07='22305' or fj.column14='22305' then sa.COLUMN05
when j.column07='22700' or fj.column14='22700' then sa.COLUMN05 when  j.column07='22335' or fj.column14='22335' then s2.COLUMN05 when  j.column07='22334' or fj.column14='22334' then sa.COLUMN05 when j.column07='22306' or fj.column14='22306' then ma.COLUMN09 when j.column07='22492' or fj.column14='22492' then ma.COLUMN09 else '' end) when (p.COLUMN03='ADVANCE PAYMENT' or p.COLUMN03='PAYMENT VOUCHER' or p.COLUMN03='PDC ISSUED') then (case when v.COLUMN11=22305 or v.COLUMN11=22700 then  sa.COLUMN05 when v.COLUMN11=22335 then s2.COLUMN05 when  v.COLUMN11=22492 then ma.COLUMN06 when  v.COLUMN11=22334 then sa.COLUMN05 else '' end) when (p.COLUMN03='ADVANCE RECEIPT' or p.COLUMN03='PDC RECEIVED' or p.COLUMN03='Sales Claim Register') then s2.COLUMN05 
when p.COLUMN03='RECEIPT VOUCHER'  then (case when v1.COLUMN20=22305 then  sa.COLUMN05 when v1.COLUMN20=22335 or v1.COLUMN20=22700 then s2.COLUMN05 when v1.COLUMN20=22492 then ma.COLUMN06 when  v1.COLUMN20=22334 then sa.COLUMN05 else '' end)
when p.COLUMN03='EXPENSE' then ma.COLUMN09 else (sa.column05) end)  Party,
o.COLUMN03 [Operating Unit],pr.COLUMN05 as Project,'' Reference#, p.COLUMN05 'Memo',p.COLUMN07 'Increased Amount',
p.COLUMN08 'Decreased Amount',(SUM(isnull(p.COLUMN07,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)-SUM(isnull(p.COLUMN08,0)) OVER(ORDER BY p.COLUMN04 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) [Balance Amount],row_number() over(order by p.COLUMN04) RNO,(case when isnull(p.COLUMNA08,0)>0  then cast(isnull(ma2.COLUMN09,0)as nvarchar(250)) else ma1.COLUMN09 end) 'Created By' from FITABLE063 p 
left join MATABLE002 m2 on m2.COLUMN02=p.COLUMN11 and m2.COLUMNA03=p.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN12 and pr.COLUMNA03=p.COLUMNA03 and isnull(pr.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.column02=p.columna02 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0
left outer join conTABLe002 cn on cn.column02=p.columna03 and isnull(cn.COLUMNA13,0)=0
left outer join FITABLE020  v on v.COLUMN04=p.COLUMN09 and v.COLUMNA03=p.COLUMNA03 and (v.COLUMNA02=p.COLUMNA02 OR v.COLUMNA02 is null) and isnull(v.COLUMNA13,0)=0  
left outer join SATABLE001 sa on sa.COLUMN02=p.COLUMN06 and sa.COLUMNA03=p.COLUMNA03 and isnull(sa.COLUMNA13,0)=0  
left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN06 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.COLUMNA13,0)=0  
left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMN06 and ma.COLUMNA03=p.COLUMNA03 and isnull(ma.COLUMNA13,0)=0   
left outer join FITABLE031 j on j.COLUMN04=p.COLUMN09 and j.COLUMNA02=p.COLUMNA02 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and j.COLUMNA02=p.COLUMNA02  
--left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) 
 left outer join FITABLE032 fj on fj.COLUMN12=j.COLUMN01 and j.COLUMNA02=fj.COLUMNA02 and fj.COLUMNA03=j.COLUMNA03 and isnull(fj.COLUMNA13,0)=0 and ((p.COLUMN10) in (fj.COLUMN03)) 
 --AND (((p.COLUMN07) in (fj.COLUMN05)) or ((p.COLUMN08) in (fj.COLUMN04))) 
 AND ISNULL(fj.COLUMN07,0)=ISNULL(p.COLUMN06,0)
left outer join FITABLE023  v1 on v1.COLUMN04=p.COLUMN09 and (v1.COLUMNA02=p.COLUMNA02 OR v1.COLUMNA02 is null )and v1.COLUMNA03=p.COLUMNA03 and isnull(v1.COLUMNA13,0)=0   
left outer join SATABLE009 s9 on s9.COLUMN04=p.column09 and s9.COLUMNA02=p.COLUMNA02  and s9.COLUMNA03=p.COLUMNA03 and isnull(s9.COLUMNA13,0)=0 
left outer join SATABLE011 s11 on s11.COLUMN04=p.column09 and s11.COLUMNA02=p.COLUMNA02 and s11.COLUMNA03=p.COLUMNA03 and isnull(s11.COLUMNA13,0)=0 
left outer join SATABLE005 s5 on s5.COLUMN04=p.column09 and s5.COLUMNA02=p.COLUMNA02 and s5.COLUMNA03=p.COLUMNA03 and isnull(s5.COLUMNA13,0)=0 
left outer join FITABLE045 f4 on f4.COLUMN04=p.column09 and f4.COLUMNA02=p.COLUMNA02 and f4.COLUMNA03=p.COLUMNA03  and isnull(f4.COLUMNA13,0)=0 
left outer join FITABLE012 f1 on f1.COLUMN04=p.column09 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and isnull(f1.COLUMNA13,0)=0  
left outer join PUTABLE005 p5 on p5.COLUMN04=p.column09 and p5.COLUMNA02=p.COLUMNA02 and p5.COLUMNA03=p.COLUMNA03  and isnull(p5.COLUMNA13,0)=0  
left outer join PUTABLE014 p4 on p4.COLUMN04=p.column09 and p4.COLUMNA02=p.COLUMNA02 and p4.COLUMNA03=p.COLUMNA03  and isnull(p4.COLUMNA13,0)=0 
LEFT OUTER JOIN FITABLE001 F10 ON F10.COLUMN02 = p.COLUMN10 AND F10.COLUMNA03 = p.COLUMNA03 and isnull(F10.COLUMNA13,0)=0 
left outer join  MATABLE010 ma1 on ma1.COLUMN02 in (s9.COLUMNA08,s11.COLUMNA08,s5.COLUMNA08,f4.COLUMNA08,f1.COLUMNA08,p5.COLUMNA08,j.COLUMNA08,v1.COLUMNA08,v.columna08,p4.columna08)  and ma1.COLUMNA03=p.COLUMNA03 and (ma1.COLUMNA02=p.COLUMNA02 or ma1.COLUMNA02 is null) and isnull(ma1.COLUMNA13,0)=0
left outer join  MATABLE010 ma2 on ma2.COLUMN02 = p.COLUMNA08 and ma2.COLUMNA03 = p.COLUMNA03  
where isnull((p.COLUMNA13),0)=0 and  p.COLUMN04  between @FromDate and @ToDate AND p.COLUMN10=@ide AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND p.COLUMNA03=@AcOwner  AND  isnull(m2.COLUMN02,'')=iif(isnull(@Tds,'')='','',@Tds) order by RNO desc
end
end
end










GO
