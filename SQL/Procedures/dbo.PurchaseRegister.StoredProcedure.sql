USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PurchaseRegister]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PurchaseRegister]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@DateF nvarchar(250)=null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null
)
as 
begin
IF  @ReportName='PurchaseRegister'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
select * into #PurchaseRegister from (
SELECT FORMAT(p.COLUMN08,  @DateF) dt,s.COLUMN05 Vendor,p.COLUMN04 BillNo,
p.COLUMN12 memo,
cast(sum(isnull(f.COLUMN09,0)) as decimal(18,2)) Qty,(isnull(p.COLUMN14,0)) Amount,p.COLUMN05 VID from PUTABLE005 p 
left outer join PUTABLE006 f on f.COLUMN13=p.COLUMN01 and f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0
left outer join SATABLE001 s on s.COLUMN02=p.COLUMN05 and s.COLUMNA03=p.COLUMNA03 and  isnull(s.COLUMNA13,0)=0
 where isnull(p.COLUMNA13,0)=0 and p.COLUMN08 between @FromDate and @ToDate
  AND p.COLUMNA03=@AcOwner AND (p.COLUMNA02=@OperatingUnit or p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) )
group by p.COLUMN08,s.COLUMN05,p.COLUMN04,p.COLUMN09,p.COLUMN05,p.COLUMN14,p.COLUMN12
	) Query 

set @Query1='select dt,Vendor,BillNo,sum(Amount)Amount,VID,sum(Qty)Qty,Memo   from #PurchaseRegister'+@whereStr +' group by dt,Vendor,BillNo,VID,Memo'
exec (@Query1)



END

end
GO
