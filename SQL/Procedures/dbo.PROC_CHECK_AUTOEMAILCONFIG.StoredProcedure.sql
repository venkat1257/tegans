USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[PROC_CHECK_AUTOEMAILCONFIG]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PROC_CHECK_AUTOEMAILCONFIG](@TransId nvarchar(250)=null,@FormId nvarchar(250)=null,@Acowner nvarchar(250)=null)
as begin
if(@FormId=1375)
begin
Select COLUMN02,COLUMN21 From SATABLE013 where isnull(COLUMN21,0)=1 and isnull(COLUMNA13,0)=0 
and COLUMNA03=@Acowner and COLUMN01=@TransId
end
--EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
else if(@FormId=1275)
begin
Select COLUMN02,COLUMN52 From SATABLE005 where isnull(COLUMN52,0)=1 and isnull(COLUMNA13,0)=0 
and COLUMNA03=@Acowner and COLUMN01=@TransId
end
else if(@FormId=1277)
begin
Select COLUMN02,COLUMN50 From SATABLE009 where isnull(COLUMN50,0)=1 and isnull(COLUMNA13,0)=0 
and COLUMNA03=@Acowner and COLUMN01=@TransId
end
else if(@FormId=1276)
begin
Select COLUMN02,COLUMN24 From SATABLE007 where isnull(COLUMN24,0)=1 and isnull(COLUMNA13,0)=0 
and COLUMNA03=@Acowner and COLUMN01=@TransId
end
else if(@FormId=1278)
begin
Select COLUMN02,COLUMN24 From SATABLE011 where isnull(COLUMN24,0)=1 and isnull(COLUMNA13,0)=0 
and COLUMNA03=@Acowner and COLUMN01=@TransId
end
--else 
--begin
--select null
--end
end

GO
