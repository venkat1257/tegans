USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_ItemCodeDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[usp_JO_TP_ItemCodeDetails](
@itemcode nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
as
begin

select MA07.COLUMN02 as itemid,ia.COLUMN04 as upc,MA07.COLUMN63 as uom,MA07.COLUMN42 as color,MA07.COLUMN43 as style,MA07.COLUMN44 as size,MA07.COLUMN10 as brand,MA07.COLUMN09 COLUMN09,MA07.COLUMN45 as distP,
sp.COLUMN04 as price,MA07.COLUMN57 as idesc,MA07.COLUMN61 as tax,MA07.COLUMN04 as iName,MA07.COLUMN80 as FabricItem ,isnull(ss.COLUMN05,0) SMRP,ia.COLUMN13 as Colour from MATABLE007 MA07
left join MATABLE024 sp on sp.COLUMN07=MA07.COLUMN02 and sp.COLUMN06='Purchase' and (isnull(sp.COLUMN03,0) in (SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(sp.COLUMN03,0)=0) and 
sp.COLUMNA03=@AcOwner and sp.COLUMNA13=0 
left join MATABLE024 ss on ss.COLUMN07=MA07.COLUMN02 and ss.COLUMN06='Sales' and (isnull(ss.COLUMN03,0) in (SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or isnull(ss.COLUMN03,0)=0) and 
ss.COLUMNA03=@AcOwner and ss.COLUMNA13=0 
inner join MATABLE021 ia on ia.COLUMN04=@itemcode and ia.COLUMN05=MA07.COLUMN02 and ia.COLUMNA03=@AcOwner and ia.COLUMNA13=0 

where MA07.COLUMNA03=@AcOwner and (MA07.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or MA07.COLUMNA02 is null)
end











GO
