USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PAYBILL_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PAYBILL_LINE_DATA]
(
	@PurchaseOrderID int,
	@AcOwner int,
	@OPUNIT int=0,
	@Form varchar(50)
)

AS

BEGIN try
declare @check varchar(50)
-- EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
declare @DueAmnt decimal(18,2)
declare @tmp decimal(18,2)
declare @DueAmnt1 decimal(18,2)
declare @PayAmnt decimal(18,2)
declare @PayAmnt1 decimal(18,2)
declare @RevCharge bit,@RevCharge1 bit
 select @RevCharge = COLUMN66, @RevCharge1 =COLUMN58  from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
--set @RevCharge1 = ( select COLUMN58 from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
if(@Form='0')
begin
set @DueAmnt1=(select (isnull(sum(COLUMN06),0)+isnull(sum(COLUMN16),0)) from PUTABLE015 WHERE  COLUMN08 in(select isnull(max(COLUMN01),0) from PUTABLE014 where COLUMN06= @PurchaseOrderID and isnull(COLUMNA13,0)=0 )  and isnull(COLUMNA13,0)=0)
--end
if(@DueAmnt1=0)
begin
select b.COLUMN02 as COLUMN03,max(b.COLUMN14) as COLUMN04,max(b.COLUMN14) as COLUMN05 

,(select sum(COLUMN15) from PUTABLE001 where    (COLUMN11=b.column06 or COLUMN11=@Form ) and COLUMNA03=@AcOwner and isnull(columna13,0)=0) as COLUMN16


from PUTABLE006 a 
	inner  join PUTABLE005 b on b.COLUMN01 = a.COLUMN13 and isnull(b.COLUMNA13,0)=0
	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06  and isnull(c.COLUMNA13,0)=0


	 WHERE  a.COLUMN13 in(select COLUMN01 from PUTABLE005 where COLUMN06= @PurchaseOrderID) and isnull(a.COLUMNA13,0)=0 group by b.COLUMN02,b.column06

	 end
else if(@DueAmnt1!=0 )
begin
 set @check=( select COLUMN06 from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
   if(@check!='')
begin
select  c.COLUMN02 as COLUMN03,
--IIF(@RevCharge='1',(max(c.COLUMN14)-max(c.COLUMN63)),(isnull(max (a.COLUMN04),max (c.COLUMN14)))) 
(CASE WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(c.COLUMN14)-max(c.COLUMN63)-max(c.COLUMN24)) 
	 WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(c.COLUMN14)-max(c.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(c.COLUMN14)-max(c.COLUMN24))
ELSE isnull(max(c.COLUMN14),0) END)
as COLUMN04,   
 (SELECT CASE WHEN max(a.COLUMN04)>0 THEN min( a.COLUMN05) ELSE max (c.COLUMN14) END) as COLUMN05
 ,(select sum(COLUMN15) from PUTABLE001 where   (COLUMN11=c.column06 and  COLUMN11=@Form ) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0) as COLUMN16
	from PUTABLE006 b inner join PUTABLE005 c on b.COLUMN13=c.COLUMN01 and c.COLUMN06=@PurchaseOrderID  and isnull(c.COLUMNA13,0)=0
	left outer join PUTABLE015 a on  c.COLUMN02=a.COLUMN03	and isnull(a.COLUMNA13,0)=0
	left outer  join PUTABLE001 d on d.COLUMN02=b.COLUMN06 and isnull(d.COLUMNA13,0)=0
	 where  isnull(b.COLUMNA13,0)=0group by c.COLUMN02,c.column06,C.COLUMN58
	end
else
begin
	select  c.COLUMN02 as COLUMN03,
	--IIF(@RevCharge='1',(max(c.COLUMN14)-max(c.COLUMN63)),(isnull(max (a.COLUMN04),max (c.COLUMN14)))) 
 (CASE WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(c.COLUMN14)-max(c.COLUMN63)-max(c.COLUMN24)) 
	 WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(c.COLUMN14)-max(c.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(c.COLUMN14)-max(c.COLUMN24))
ELSE isnull(max(c.COLUMN14),0) END) as COLUMN04,   
 (SELECT CASE WHEN max(a.COLUMN04)>0 THEN min( a.COLUMN05) ELSE max (c.COLUMN14) END) as COLUMN05
 ,(select sum(COLUMN15) from PUTABLE001 where COLUMN11=@Form  and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0) as COLUMN16
	from PUTABLE006 b inner join PUTABLE005 c on b.COLUMN13=c.COLUMN01 and c.COLUMN06=@PurchaseOrderID  and c.COLUMN02=@Form and c.COLUMNA03=@AcOwner and isnull(c.COLUMNA13,0)=0
	left outer join PUTABLE015 a on  c.COLUMN02=a.COLUMN03	and isnull(a.COLUMNA13,0)=0
	left outer  join PUTABLE001 d on d.COLUMN02=b.COLUMN06 and isnull(d.COLUMNA13,0)=0
	where  isnull(b.COLUMNA13,0)=0 group by c.COLUMN02,d.column06,C.COLUMN58
	 end

	 end


	 else
	 begin
select distinct a.COLUMN03 as COLUMN03, max(cast(a.COLUMN04 as int)) as COLUMN04,    0 as COLUMN05

,(select sum(COLUMN15) from PUTABLE001 where    (COLUMN11=b.column06 or COLUMN11=@Form ) and COLUMNA03=@AcOwner and isnull(columna13,0)=0) as COLUMN16

	from PUTABLE015 a inner join PUTABLE014 b on a.COLUMN08=b.COLUMN01 	and isnull(b.COLUMNA13,0)=0	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.COLUMNA13,0)=0

	 WHERE  a.COLUMN08 in(select COLUMN01 from PUTABLE014 where COLUMN06= '' and isnull(COLUMNA13,0)=0) and isnull(a.COLUMNA13,0)=0 group by a.COLUMN03,b.column06
	 end
	 end
	 else
	 begin
set @DueAmnt1=(select column02 from PUTABLE005 where COLUMN02=@Form  and COLUMNA03=@AcOwner  and isnull(COLUMNA13,0)=0)
set @PayAmnt=(select  (isnull(sum(COLUMN06),0)+isnull(sum(COLUMN16),0)+isnull(sum(COLUMN14),0))  from PUTABLE015  where COLUMN03=@Form and isnull(columna13,0)=0 ) 
if(@PayAmnt>0)
begin
set @DueAmnt=(select  (isnull(sum(COLUMN06),0)+isnull(sum(COLUMN16),0)+isnull(sum(COLUMN14),0))  from PUTABLE015   where COLUMN03=@Form and isnull(columna13,0)=0)
set @DueAmnt=abs(@DueAmnt-(select isnull(sum(COLUMN04),0) from PUTABLE015 where COLUMN03=@Form and isnull(columna13,0)=0))
  set @check=( select COLUMN06 from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0)
   if(@check!='')
begin
	 --set @DueAmnt=((select COLUMN14 from PUTABLE005 where COLUMN04= @Form  or COLUMN06= @Form))
	 --EMPHCS1079 when partial payment made for PO, in next payment for same PO amount paid already not considering BY RAJ.Jr 5/9/2015
     select b.COLUMN02 as COLUMN03, --IIF(@RevCharge='1',(max(b.COLUMN14)-max(b.COLUMN63)),isnull(max(b.COLUMN14),0)) 
 (CASE WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
	 WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END) as COLUMN04 , (isnull(max(b.COLUMN14),0)-@PayAmnt)  as COLUMN05
	,(select sum(COLUMN15) from PUTABLE001 where    (COLUMN11=b.column02 ) and COLUMNA03=@AcOwner) as COLUMN16,
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1355'and column16!='CLOSE' and isnull(COLUMNA13,0)=0) as directReturn,'' AS COLUMN06,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN11=b.column04 and COLUMN03='1355' and column16!='CLOSE' and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN18,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=b.COLUMN06 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from PUTABLE006 a inner join PUTABLE005 b on a.COLUMN13 = b.COLUMN01 and isnull(b.columna13,0)=0
		left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN13 in(select COLUMN01 from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner )and isnull(a.columna13,0)=0 group by b.COLUMN02,b.column06,b.column05,b.column04,b.COLUMN58
end
else
begin
 declare @COUNT INT
 declare @RCOUNT INT
 declare @RetCOUNT INT
 declare @RetAmnt INT
 Declare @n int
 set @n=1
set @COUNT = (select COUNT(*) from PUTABLE015 where  COLUMN03=@Form and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0 and COLUMN14=0)
set @RCOUNT = (select COUNT(*) from PUTABLE015 where  COLUMN03=@Form and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0 and COLUMN14!=0)

set @RetCOUNT =(select COUNT(*) from  PUTABLE001 a inner join PUTABLE005 b on a.COLUMN05=b.COLUMN05 and isnull(b.COLUMNA13,0)=0  where a.COLUMNA02=@OPUNIT and a.COLUMNA03=@AcOwner and a.COLUMN03='1355' and   b.COLUMN02=@Form and a.COLUMN16!='CLOSE' and isnull(a.COLUMNA13,0)=0)

set @RetAmnt=(select sum(isnull(column14,0)) from PUTABLE015  where column02 not in 
(select top ((select count(*) from PUTABLE015 where   COLUMN03=@Form and COLUMNA02=@OPUNIT and isnull(COLUMNA13,0)=0 and COLUMNA03=@AcOwner) - @n ) column02 
from PUTABLE015 where   COLUMN03=@Form and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0) and   COLUMN03=@Form and COLUMNA02=@OPUNIT and COLUMNA03=@AcOwner)


	--      select b.COLUMN02 as COLUMN03,isnull(max(b.COLUMN14),0) as COLUMN04 ,    (isnull(max(b.COLUMN14),0)-@PayAmnt)  as COLUMN05
	--,(select sum(COLUMN15) from PUTABLE001 where   COLUMN11=@Form  and COLUMNA03=@AcOwner) as COLUMN16
	--from PUTABLE006 a inner join PUTABLE005 b on a.COLUMN13 = b.COLUMN01
	--	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 
	-- WHERE  a.COLUMN13 in(select COLUMN01 from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner ) group by b.COLUMN02,b.column06
	

	if(@RetAmnt!=0 ) 
 begin
  if(@RetCOUNT>=1)
 begin
 set @PayAmnt=(select  (isnull(sum(COLUMN06),0)+isnull(sum(COLUMN16),0)+isnull(sum(COLUMN14),0))  from PUTABLE015  where COLUMN03=@Form and isnull(columna13,0)=0 ) 
  select b.COLUMN02 as COLUMN03, --IIF(@RevCharge='1',(max(b.COLUMN14)-max(b.COLUMN63)),isnull(max(b.COLUMN14),0)) 
(CASE WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END)
  as COLUMN04, (isnull(max(b.COLUMN14),0)-@PayAmnt) as COLUMN05,
   (select sum(COLUMN15) from PUTABLE001 where COLUMN11=b.column04 and COLUMN03='1355' and column16!='CLOSE' and isnull(columna13,0)=0) as COLUMN16,
--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=b.COLUMN05 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0) as directReturn,'' AS COLUMN06  ,
   (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN11=b.column04 and COLUMN03='1355' and column16!='CLOSE' and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN18,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=b.COLUMN06 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from PUTABLE006 a 
	inner join PUTABLE005 b on a.COLUMN13=b.COLUMN01 and isnull(b.columna13,0)=0
	left outer join PUTABLE015 d on b.COLUMN02=d.COLUMN03 and isnull(d.columna13,0)=0
	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN13=(select Max(COLUMN01) from PUTABLE005 where  COLUMN02=@Form  and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT  ) and isnull(a.columna13,0)=0 group by b.COLUMN02,b.COLUMN05, b.COLUMN04,c.COLUMN04,b.column06,b.COLUMN58
 end
 else
 begin
  select b.COLUMN02 as COLUMN03,
(CASE WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END) as COLUMN04, (isnull(min(d.COLUMN05),0)) as COLUMN05,
 
 null as COLUMN16, 0 as directReturn,'' AS COLUMN06
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 , null as COLUMN18,null as directReturnCol
	from PUTABLE006 a 
	inner join PUTABLE005 b on a.COLUMN13=b.COLUMN01 and isnull(b.columna13,0)=0
	left outer join PUTABLE015 d on b.COLUMN02=d.COLUMN03 and isnull(d.columna13,0)=0
	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN13=(select Max(COLUMN01) from PUTABLE005 where  COLUMN02=@Form  and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT  )and isnull(a.columna13,0)=0 group by b.COLUMN02,b.COLUMN05, b.COLUMN04,c.COLUMN04,b.COLUMN58
 end
 end

 else
 begin
 if(@RetCOUNT>=1)
  begin
  set @PayAmnt=(select  (isnull(sum(COLUMN06),0)+isnull(sum(COLUMN16),0)+isnull(sum(COLUMN14),0))  from PUTABLE015  where COLUMN03=@Form and isnull(columna13,0)=0 ) 
  select b.COLUMN02 as COLUMN03, --IIF(@RevCharge='1',(max(b.COLUMN14)-max(b.COLUMN63)),isnull(max(b.COLUMN14),0)) 
 (CASE WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END)
  as COLUMN04, (isnull(max(b.COLUMN14),0)-@PayAmnt) as COLUMN05,
   (select sum(COLUMN15) from PUTABLE001 where COLUMN11=b.column04 and COLUMN03='1355' and column16!='CLOSE'and isnull(columna13,0)=0) as COLUMN16,
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=b.column05 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0) as directReturn,'' AS COLUMN06  ,
   (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN11=b.column04 and COLUMN03='1355' and column16!='CLOSE'and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN18,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=b.column06 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from PUTABLE006 a 
	inner join PUTABLE005 b on a.COLUMN13=b.COLUMN01 and isnull(b.columna13,0)=0
	left outer join PUTABLE015 d on b.COLUMN02=d.COLUMN03 and isnull(d.columna13,0)=0
	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN13=(select Max(COLUMN01) from PUTABLE005 where  COLUMN02=@Form  and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT  )and isnull(a.columna13,0)=0 group by b.COLUMN02,b.COLUMN05, b.COLUMN04,c.COLUMN04,b.column06,b.column04,b.COLUMN58
 end
 else
 begin
  select b.COLUMN02 as COLUMN03,-- IIF(@RevCharge='1',(max(b.COLUMN14)-max(b.COLUMN63)),isnull(max(b.COLUMN14),0)) 
  (CASE 
	 WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
	 WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END)
  as COLUMN04, (isnull(min(d.COLUMN05),0)) as COLUMN05,
 
 null as COLUMN16, 0 as directReturn,'' AS COLUMN06
 --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 ,null as COLUMN18, null as directReturnCol
	from PUTABLE006 a 
	inner join PUTABLE005 b on a.COLUMN13=b.COLUMN01 and isnull(b.columna13,0)=0
	left outer join PUTABLE015 d on b.COLUMN02=d.COLUMN03 and isnull(d.columna13,0)=0
	left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN13=(select Max(COLUMN01) from PUTABLE005 where  COLUMN02=@Form  and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT  )and isnull(a.columna13,0)=0 group by b.COLUMN02,b.COLUMN05, b.COLUMN04,c.COLUMN04,b.COLUMN58
 end
	 end

	 end
end
else
begin



     select b.COLUMN02 as COLUMN03,
	 --IIF(@RevCharge='1',(max(b.COLUMN14)-max(b.COLUMN63)),isnull(max(b.COLUMN14),0))
	 (CASE 
	 WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
	 WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END) as COLUMN04 ,   

	  --IIF(@RevCharge='1',(max(b.COLUMN14)-max(b.COLUMN63)),isnull(max(b.COLUMN14),0))  
	 (CASE 
	 WHEN (@RevCharge='1' AND @RevCharge1='1') THEN (max(b.COLUMN14)-max(b.COLUMN63)-max(b.COLUMN24)) 
	 WHEN (@RevCharge='1' AND @RevCharge1='0') THEN (max(b.COLUMN14)-max(b.COLUMN63)) 
WHEN (@RevCharge='0' AND @RevCharge1='1')  THEN (max(b.COLUMN14)-max(b.COLUMN24))
ELSE isnull(max(b.COLUMN14),0) END)
	  as COLUMN05
	,(select sum(COLUMN15) from PUTABLE001 where    COLUMN11=b.column04   and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT and  COLUMN03='1355'and column16!='CLOSE' and isnull(columna13,0)=0) as COLUMN16,
 (select sum(COLUMN15) from PUTABLE001 where COLUMN05=b.column05 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0) as directReturn,'' AS COLUMN06
--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
 ,(select STUFF((select ', ' + COLUMN02 from PUTABLE001 where    COLUMN11=b.column04   and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT and  COLUMN03='1355'and column16!='CLOSE' and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as COLUMN18,
 (select STUFF((select ', ' + COLUMN02 from PUTABLE001 where COLUMN05=b.column05 and column11='' and  COLUMN03='1355'and column16!='CLOSE'and isnull(columna13,0)=0 and COLUMNA13='False' FOR XML PATH('')) , 1, 2, '') as COLUMN02 ) as directReturnCol
	from PUTABLE006 a inner join PUTABLE005 b on a.COLUMN13 = b.COLUMN01 and isnull(b.columna13,0)=0
		left outer  join PUTABLE001 c on c.COLUMN02=b.COLUMN06 and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN13 in(select COLUMN01 from PUTABLE005 where COLUMN02=@Form and COLUMNA03=@AcOwner and COLUMNA02=@OPUNIT and isnull(COLUMNA13,0)=0) and isnull(a.columna13,0)=0 group by b.COLUMN02, b.COLUMN05,b.column06,b.column06,b.column04,b.column58


	  end
	 end
END try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
end catch









GO
