USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_OpeningBalanceVendor]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_OpeningBalanceVendor]
(
    @COLUMN02   nvarchar(250)=null,  @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN21   nvarchar(250)=null, 
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN

declare @newIID int
declare @ReferedID int
declare @balance decimal
declare @balance1 decimal(18,2)
declare @balance2 decimal(18,2)
declare @Increase decimal
declare @decrease decimal
declare @Increase1 decimal(18,2)
declare @decrease1 decimal(18,2)
declare @Type INT
declare @Payee nvarchar
declare @tmpnewIID int,@number nvarchar(25)
set @Type=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08  AND COLUMNA03=@COLUMNA03  )
		set @COLUMN04 = (select column05 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE018_SequenceNo
set @COLUMN03 = (select column03 from FITABLE049 where column01=@column21 and COLUMNA03 = @COLUMNA03)
set @COLUMNA02 = (select column07 from FITABLE049 where column01=@column21 and COLUMNA03 = @COLUMNA03)
insert into FITABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07, COLUMN08, COLUMN09,COLUMN10, COLUMN11, COLUMN12,COLUMN21, COLUMNA02,  COLUMNA03, COLUMNA13, COLUMNA06, COLUMNA07, COLUMNA08)
Values(@COLUMN02,@COLUMN03,@COLUMN04,'OB'+@COLUMN02,'Opening Balance',@COLUMN07,@COLUMN08,@COLUMN09, @COLUMN10,0 ,@COLUMN12,@COLUMN21,@COLUMNA02,@COLUMNA03,0, @COLUMNA06, @COLUMNA07,@COLUMNA08)

		--Accounts Payable
		if(@Type=22263)
		BEGIN
		set @tmpnewIID=(select MAX(COLUMN02) from PUTABLE016)
if(@tmpnewIID>0)
		BEGIN
		set @newIID=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		end
else
		begin
		set @newIID=10000
		end
		select @COLUMN05 =column04,@COLUMN04 =column05 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02
			select @number = column01 from FITABLE018 where column02 = @COLUMN02
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Opening Balance',  @COLUMN04 = @COLUMN05, @COLUMN05 = @COLUMN21,  @COLUMN06 = @number,
		@COLUMN07 = @COLUMN04,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN07,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = '0',	@COLUMN14 = @COLUMN09,       @COLUMN15 = @COLUMN10,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into PUTABLE016 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,COLUMN13,COLUMN18, COLUMNA02,COLUMNA03, COLUMNA13, COLUMNA06, COLUMNA07)
Values(@newIID,@COLUMN08,@COLUMN04,@COLUMN02,'Opening Balance',@COLUMN07,@COLUMN08,@COLUMN05,@COLUMN09,@COLUMN10,@COLUMN12,@COLUMNA02,@COLUMNA03,0, @COLUMNA06, @COLUMNA07)
END
END
 

IF @Direction = 'Select'
BEGIN
select * from FITABLE018
END 
 

IF @Direction = 'Update'
BEGIN
set @Type=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03  )
		set @COLUMN04 = (select column05 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
--DELETE FROM FITABLE064 WHERE COLUMN03 = 'OPENING BALANCE' AND COLUMN05 = @COLUMN21 AND COLUMNA03=@COLUMNA03
UPDATE FITABLE018 SET
COLUMN07=@COLUMN07,COLUMN04=@COLUMN04,COLUMN08=@COLUMN08, COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10 ,COLUMN12=@COLUMN12,COLUMNA13=0
WHERE COLUMN02 = @COLUMN02 and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
--Accounts Payable
		if(@Type=22263)
		BEGIN
			select @COLUMN05 =column04,@COLUMN04 =column05 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02
			select @number = column01 from FITABLE018 where column02 = @COLUMN02
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Opening Balance',  @COLUMN04 = @COLUMN05, @COLUMN05 = @COLUMN21,  @COLUMN06 = @number,
		@COLUMN07 = @COLUMN04,   @COLUMN08 = '22305',  @COLUMN09 = @COLUMN07,    @COLUMN10 = '22263', @COLUMN11 = '2000',
		@COLUMN12 = @COLUMN12,   @COLUMN13 = '0',	@COLUMN14 = @COLUMN09,       @COLUMN15 = @COLUMN10,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'

		set @COLUMN05 = (select column04 from FITABLE049 where COLUMN01=@COLUMN21 and COLUMNA02 = @COLUMNA02)
UPDATE  PUTABLE016 SET COLUMN03=@COLUMN08,COLUMN04=@COLUMN04,COLUMN05=@COLUMN02,COLUMN06='Opening Balance',COLUMN07=@COLUMN07,COLUMN08=@COLUMN08,COLUMN09=@COLUMN05,COLUMN12=@COLUMN09,COLUMN13=@COLUMN10,COLUMN18=@COLUMN12,COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03,COLUMNA13=0
where  COLUMN03=@COLUMN08 and COLUMN05=@COLUMN02 and COLUMN06='Opening Balance' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 
		END
       end


 

else IF @Direction = 'Delete'
BEGIN

		UPDATE FITABLE018 SET COLUMNA13=1 WHERE COLUMN02 = @COLUMN02

END
end try
			begin catch
			declare @tempSTR nvarchar(max)
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_FITABLE018.txt',0
return 0
end catch
end

















GO
