USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_GRN_TP_JOBIN_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GRN_TP_JOBIN_LINE_DATA]
(
@GRN int
)
as
BEGIN
declare @id int
declare @LineID int
declare @COLUMN03 int
declare @COLUMN04 nvarchar(250)
declare @COLUMN27 int
declare @pcs decimal(18,2)
declare @max int
declare @inital int
set @inital=1
declare @max1 int
declare @inital1 int
declare p cursor for select a.COLUMN05 pcs,a.COLUMN02 LineID from PUTABLE023 a left outer join MATABLE007 m on m.COLUMN02=a.COLUMN03 where a.COLUMN10 in(select COLUMN01 from PUTABLE022 where COLUMN02=@GRN and COLUMNA13='False') and a.COLUMNA13='False'
open p
fetch next from p into @pcs,@LineID
set @max=(select count(*) from PUTABLE023 where COLUMN10 in(select COLUMN01 from PUTABLE022 where COLUMN02=@GRN and COLUMNA13='False') )
while @inital<=@max
begin
set @inital1=1
declare p1 cursor for select a.COLUMN03 COLUMN03,m.COLUMN06 COLUMN04,a.COLUMN04 COLUMN27,a.COLUMN05 pcs,a.COLUMN02 LineID from PUTABLE023 a left outer join MATABLE007 m on m.COLUMN02=a.COLUMN03 where a.COLUMN10 in(select COLUMN01 from PUTABLE022 where COLUMN02=@GRN and COLUMNA13='False') and a.COLUMNA13='False' and a.COLUMN02=@LineID
open p1
fetch next from p1 into @COLUMN03,@COLUMN04,@COLUMN27,@pcs,@LineID
set @max1=(select (column05) from PUTABLE023 where COLUMN10 in(select COLUMN01 from PUTABLE022 where COLUMN02=@GRN and COLUMNA13='False') and COLUMN02=@LineID )
while @inital1<=@max1
begin
DECLARE @MatchTemp TABLE(
 COLUMN03 int,COLUMN04 nvarchar(250),COLUMN27 int,pcs decimal(18,2),LineID int)
insert into @MatchTemp(COLUMN03,COLUMN04,COLUMN27,pcs,LineID)values(@COLUMN03,@COLUMN04,@COLUMN27,@pcs,@LineID)
FETCH NEXT FROM p1 INTO @COLUMN03,@COLUMN04,@COLUMN27,@pcs,@LineID
             SET @inital1 = @inital1 + 1 
		 end
			 CLOSE p1
deallocate p1
FETCH NEXT FROM p INTO @pcs,@LineID
             SET @inital = @inital + 1 
			 end
			 CLOSE p
deallocate p  
select *from @MatchTemp


END

GO
