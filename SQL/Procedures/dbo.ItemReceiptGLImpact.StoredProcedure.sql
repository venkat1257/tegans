USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[ItemReceiptGLImpact]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemReceiptGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,p.column12 Memo,sum(p17.COLUMN10) Credit,sum(p17.COLUMN11) Debit  from PUTABLE003 p
left outer join putable017 p17 on p17.COLUMN05= p.COLUMN01 and p17.COLUMN07= p.COLUMN05 and p17.COLUMNA03= p.COLUMNA03 and p17.COLUMNA02= p.COLUMNA02 and p17.COLUMN06='Item Receipt' and isnull(p17.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03 AND f1.COLUMNA03 = p17.COLUMNA03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04,p.column12
)
select [Account],[Memo],[Credit],[Debit] from MyTable
group by [Account],[Memo],[Credit],[Debit]
end

--SELECT * FROM PUTABLE003 ORDER BY COLUMN02 DESC;
--SELECT * FROM PUTABLE017 ORDER BY COLUMN02 DESC;

GO
