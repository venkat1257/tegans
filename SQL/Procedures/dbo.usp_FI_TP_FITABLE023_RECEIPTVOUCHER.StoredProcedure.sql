USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE023_RECEIPTVOUCHER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE023_RECEIPTVOUCHER]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	--EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT
)
AS
BEGIN
 begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE023_SequenceNo
insert into FITABLE023
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06,  
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN10,  @COLUMN20,  @COLUMN21, 
   --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   @COLUMN22,  @COLUMN23,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  
 update FITABLE023 set  COLUMN13=(select column04 from contable025 where column02=64) where column02=@COLUMN02
 --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
set @ReturnValue =(select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN02)
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE023
END  

ELSE IF @Direction = 'Update'
BEGIN
UPDATE FITABLE023 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,    
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN10,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,
   --EMPHCS1798 rajasekhar reddy patakota 17/08/2016 Vouchers and Invoice available lot changes
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
 DECLARE @MaxRownum INT,@id nvarchar(250),@internalid nvarchar(250),@Tax nvarchar(250),@Voucher# nvarchar(250),@TaxAG nvarchar(250),
  --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
 @TAXRATE nvarchar(250),@bank nvarchar(250),@paymenttype nvarchar(250),@linetax decimal(18,2),@lineamt decimal(18,2),@Bankbal decimal(18,2),
 @Project nvarchar(250),@DATE date, @TYPEID nvarchar(250)=null,@NUMBER nvarchar(250),@Totbal decimal(18,2)=null,@Acc nvarchar(250),@Hinternalid nvarchar(250),
@ACCNAME nvarchar(250),@TYPE nvarchar(250)=null,@ACCOUNTTYPE nvarchar(250),@Remainbal decimal(18,2)=null
set @internalid= (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN02)

set @Voucher#=(select column04 from FITABLE023 where COLUMN02=@COLUMN02)
set @DATE=(@COLUMN05)
set @TYPEID=(@Voucher#)
set @NUMBER=(@internalid)
DELETE FROM FITABLE064 WHERE COLUMN05 = @internalid AND COLUMN03 IN('RECEIPT VOUCHER',  'PDC RECEIVED') AND COLUMNA02 = @COLUMNA02 AND COLUMNA03 = @COLUMNA03 
declare @LineID nvarchar(250)
      DECLARE @Initialrow INT=1
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE024 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE024 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN
		 set @paymenttype= (select column11 from FITABLE024 where COLUMN02=@id)
		 set @LineID=(select COLUMN01 from FITABLE024 where COLUMN02=@id)
		 set @Tax= (select column15 from FITABLE024 where COLUMN02=@id)
         set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
         set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
		 set @Acc= (select column03 from FITABLE024 where COLUMN02=@id)
		 set @Project= (select isnull(column08,0) from FITABLE024 where COLUMN02=@id)
		 set @bank= (select column12 from FITABLE024 where COLUMN02=@id)
		 set @lineamt= (select column05 from FITABLE024 where COLUMN02=@id)
		 set @linetax= (select column06 from FITABLE024 where COLUMN02=@id)
		 set @Bankbal=(select ISNULL(COLUMN10,0) from FITABLE001 where COLUMN02=@bank and COLUMNA03=@COLUMNA03)
		 if(isnull(cast(@linetax as decimal(18,2)),0)>0)
		 BEGIN
		 delete from  FITABLE026  where COLUMN04='RECEIPT VOUCHER'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end

		  --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
		 set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @Totbal=(cast(isnull(@lineamt,0) as decimal(18,2))+cast(isnull(@linetax,0) as decimal(18,2)))
		 set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=@TYPE)
IF(@TYPE=22266)
BEGIN
	delete from PUTABLE019  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='RECEIPT VOUCHER'  and COLUMN08 in(@LineID) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Cash
ELSE IF(@TYPE=22409)
BEGIN
	delete from FITABLE052  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='RECEIPT VOUCHER'  and COLUMN09 in(@LineID) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	delete from  PUTABLE017  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='RECEIPT VOUCHER'   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Payable
ELSE IF(@TYPE=22263 or @Acc=1117)
BEGIN
	delete from  PUTABLE016  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='RECEIPT VOUCHER'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	delete from  PUTABLE018  where COLUMN03=@Acc and COLUMN05=@Voucher# and COLUMN06='RECEIPT VOUCHER'  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Equity
ELSE IF(@TYPE=22330)
BEGIN
	delete from  FITABLE029  where COLUMN03='RECEIPT VOUCHER' and COLUMN07 in(@Acc) and COLUMN11 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Expense
ELSE IF(@TYPE=22344)
BEGIN
	delete from  FITABLE036  where COLUMN03='RECEIPT VOUCHER' and COLUMN05 in(@NUMBER)  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	delete from  FITABLE026  where COLUMN04='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	delete from  FITABLE034  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	delete from  FITABLE028  where COLUMN05='RECEIPT VOUCHER' and  COLUMN06 =@TYPEID and COLUMN03=@Acc  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Income
ELSE IF(@TYPE=22405)
BEGIN
	delete from  FITABLE025  where COLUMN04='RECEIPT VOUCHER'  and COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	delete from  FITABLE025  where COLUMN04='RECEIPT VOUCHER'  and  COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	delete from  FITABLE035  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09=@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	delete from  FITABLE036  where COLUMN03='RECEIPT VOUCHER' and COLUMN10 in(@Acc) and  COLUMN09 =@TYPEID and COLUMN05 in(@NUMBER)   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current assets
ELSE IF(@TYPE=22410)
BEGIN
	delete from  FITABLE060  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current Liabilities
ELSE IF(@TYPE=22411)
BEGIN
	delete from  FITABLE063  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END

		 if(@paymenttype=22627)
		 begin
		 update FITABLE044 set  COLUMN18=(null),COLUMN19=(null) where column02 in(select COLUMN13 from FITABLE024 where COLUMN02=@id) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		 delete from  FITABLE034  where COLUMN03='PDC RECEIVED'and    COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE018  where   COLUMN05 =@Voucher# and  COLUMN06 ='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 else
		 begin
		 delete from  PUTABLE018  where   COLUMN05 =@Voucher# and  COLUMN06 ='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE019  where   COLUMN05 =@Voucher# and  COLUMN08 =@LineID AND COLUMN06='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		 delete from  FITABLE052  where   COLUMN05 =@Voucher# and  COLUMN09 =@LineID AND COLUMN06='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))-cast(@lineamt as decimal(18,2))-cast(@linetax as decimal(18,2))) where column02=@bank and COLUMNA03=@COLUMNA03
		 end
		 declare @payee nvarchar(250),@cretAmnt decimal(18,2)
		 set @Payee=(select COLUMN08 from FITABLE023 where COLUMN02=@COLUMN02)
		 set @COLUMN17= (select sum(isnull(COLUMN17,0)) from FITABLE024 where COLUMN09 in(@internalid))
		 if(@COLUMN17!= '0' and @COLUMN17!= '')
	begin
	declare @AdvIDs nvarchar(250),@Initialrow1 int,@MaxRownum1 int,@id1 int
      set @Initialrow1=1
	set @AdvIDs=(select COLUMN19 from FITABLE024 where COLUMN02=@id)
			  DECLARE curA CURSOR FOR select  FITABLE023.COLUMN02 from FITABLE023 
			  INNER JOIN FITABLE024 ON FITABLE024.COLUMN09=FITABLE023.COLUMN01 AND ISNULL(FITABLE024.COLUMNA13,0)=0 
			  WHERE  FITABLE023.COLUMNA03=@COLUMNA03 AND FITABLE023.COLUMNA02=@COLUMNA02 AND FITABLE023.COLUMN03='1386' AND FITABLE023.COLUMN08=@Payee AND ISNULL(FITABLE023.COLUMNA13,0)=0 and FITABLE023.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
			  order by FITABLE023.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id1
		  if(@id1!='' and @id1 is not null)
		     BEGIN 
			  set @MaxRownum1=(select  COUNT(*) from FITABLE023 
			  INNER JOIN FITABLE024 ON FITABLE024.COLUMN09=FITABLE023.COLUMN01 AND ISNULL(FITABLE024.COLUMNA13,0)=0 
			  WHERE  FITABLE023.COLUMNA03=@COLUMNA03 AND FITABLE023.COLUMNA02=@COLUMNA02 AND FITABLE023.COLUMN03='1386' AND FITABLE023.COLUMN08=@Payee and FITABLE023.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE023.COLUMNA13,0)=0)
			   WHILE @Initialrow1 <= @MaxRownum1
			   BEGIN
		        if(cast(@COLUMN17 as DECIMAL(18,2))!=0 and @cretAmnt>0)
			  begin
				set @cretAmnt=(select column10 from FITABLE023 where column02= @id1  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				if(cast(@COLUMN17 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE023 set column19=(@cretAmnt), COLUMN13='OPEN'  where column02= @id1  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				--EMPHCS1410	Logs Creation by srinivas	
						--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						--'Advance Amount Reset in FITABLE023 Table by '+cast(@cretAmnt as nvarchar(15))+' for '+cast(@id as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						--)

				set @COLUMN17=(cast(@COLUMN17 as DECIMAL(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN17 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
					update FITABLE023 set column19=(cast(isnull(COLUMN19,0) as DECIMAL(18,2))+cast(isnull(@COLUMN17,0) as DECIMAL(18,2))), COLUMN13='OPEN'  where column02= @id1
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 --EMPHCS1410	Logs Creation by srinivas
						--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						--'Advance Amount Reset in FITABLE023 Table by '+cast(@COLUMN09 as nvarchar(15))+' for '+cast(@id as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						--)

				 set @COLUMN17='0'
				 end
				end
				FETCH NEXT FROM curA INTO @id1
				SET @Initialrow1 = @Initialrow1 + 1 
				END
			END
				CLOSE curA 
				deallocate curA
		end
UPDATE FITABLE024 SET COLUMNA13=1 WHERE COLUMN02=@id
 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end
        CLOSE cur1
        deallocate cur1   
set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN
UPDATE FITABLE023 SET COLUMNA13=1 WHERE COLUMN02=@COLUMN02
set @internalid= (select COLUMN01 from FITABLE023 where COLUMN02=@COLUMN02)
set @Voucher#=(select column04 from FITABLE023 where COLUMN02=@COLUMN02)
set @COLUMNA02=(select COLUMNA02 from FITABLE023 where COLUMN02=@COLUMN02)
set @COLUMNA03= (select COLUMNA03 from FITABLE023 where COLUMN02=@COLUMN02)
set @TYPEID=(@Voucher#)
set @NUMBER=(@internalid)
DELETE FROM FITABLE064 WHERE COLUMN05 = @internalid AND COLUMN03 IN('RECEIPT VOUCHER',  'PDC RECEIVED') AND COLUMNA02 = @COLUMNA02 AND COLUMNA03 = @COLUMNA03 
 set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from FITABLE024 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE024 where COLUMN09 in(@internalid) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN
		 set @AdvIDs=(select COLUMN19 from FITABLE024 where COLUMN02=@id)
		 set @LineID=(select COLUMN01 from FITABLE024 where COLUMN02=@id)
		 set @paymenttype= (select column11 from FITABLE024 where COLUMN02=@id)
		 --set @internalid= (select column01 from FITABLE024 where COLUMN02=@id)
		 set @Tax= (select COLUMN15 from FITABLE024 where COLUMN02=@id)
  set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
  set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
		 set @Acc= (select column03 from FITABLE024 where COLUMN02=@id)
		 set @Project= (select isnull(column08,0) from FITABLE024 where COLUMN02=@id)
		 set @bank= (select column12 from FITABLE024 where COLUMN02=@id)
		 set @lineamt= (select column05 from FITABLE024 where COLUMN02=@id)
		 set @linetax= (select column06 from FITABLE024 where COLUMN02=@id)
		 set @Bankbal=(select ISNULL(COLUMN10,0) from FITABLE001 where COLUMN02=@bank and COLUMNA03=@COLUMNA03)
		 if(isnull(cast(@linetax as decimal(18,2)),0)>0)
		 BEGIN
		 delete from  FITABLE026  where COLUMN04='RECEIPT VOUCHER'and  COLUMN05 =@internalid and  COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end

		  --EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
		 set @ACCNAME=(SELECT COLUMN04 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @TYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
		 set @Totbal=(cast(isnull(@lineamt,0) as decimal(18,2))+cast(isnull(@linetax,0) as decimal(18,2)))
		 set @ACCOUNTTYPE=(SELECT COLUMN03 FROM FITABLE040 WHERE COLUMN06=@TYPE)
IF(@TYPE=22266)
BEGIN
	delete from PUTABLE019  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='RECEIPT VOUCHER'  and COLUMN08 in(@LineID) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
    SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Cash
ELSE IF(@TYPE=22409)
BEGIN
	delete from FITABLE052  where COLUMN03=@Acc and COLUMN05=@TYPEID and COLUMN06='RECEIPT VOUCHER'  and COLUMN09 in(@LineID) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	SET @Remainbal=(SELECT ISNULL(COLUMN10,0) FROM FITABLE001 WHERE COLUMN02=@Acc AND COLUMNA03=@COLUMNA03)
	SET @Remainbal=(ISNULL(@Remainbal,0)-CAST(ISNULL(@Totbal,0) AS DECIMAL(18,2)))
	update FITABLE001 set  COLUMN10=@Remainbal where column02=@Acc and COLUMNA03=@COLUMNA03
END
--Inventory Asset
ELSE IF(@TYPE=22262)
BEGIN
	delete from  PUTABLE017  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='RECEIPT VOUCHER'   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Payable
ELSE IF(@TYPE=22263 or @Acc=1117)
BEGIN
	delete from  PUTABLE016  where COLUMN03=@Acc and COLUMN05 in(@NUMBER) and COLUMN06='RECEIPT VOUCHER'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Accounts Receivable
ELSE IF(@TYPE=22264)
BEGIN
	delete from  PUTABLE018  where COLUMN03=@Acc and COLUMN05=@Voucher# and COLUMN06='RECEIPT VOUCHER'  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Equity
ELSE IF(@TYPE=22330)
BEGIN
	delete from  FITABLE029  where COLUMN03='RECEIPT VOUCHER' and COLUMN07 in(@Acc)  and COLUMN11 =@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Expense
ELSE IF(@TYPE=22344)
BEGIN
	delete from  FITABLE036  where COLUMN03='RECEIPT VOUCHER' and COLUMN10 in(@Acc)  and COLUMN05 in(@NUMBER) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current liabilities
ELSE IF(@TYPE=22383)
BEGIN
	delete from  FITABLE026  where COLUMN04='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Current assets
ELSE IF(@TYPE=22402)
BEGIN
	delete from  FITABLE034  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Fixed assets
ELSE IF(@TYPE=22403)
BEGIN
	delete from  FITABLE028  where COLUMN05='RECEIPT VOUCHER' and  COLUMN06 =@TYPEID and COLUMN03=@Acc  and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Income
ELSE IF(@TYPE=22405)
BEGIN
	delete from  FITABLE025  where COLUMN04='RECEIPT VOUCHER'  and COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Cost of Goods Sold
ELSE IF(@TYPE=22406)
BEGIN
	delete from  FITABLE025  where COLUMN04='RECEIPT VOUCHER'  and  COLUMN05 in(@NUMBER)  and COLUMN08=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Income
ELSE IF(@TYPE=22407)
BEGIN
	delete from  FITABLE035  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09=@TYPEID   and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Other Expense
ELSE IF(@TYPE=22408)
BEGIN
	delete from  FITABLE036  where COLUMN03='RECEIPT VOUCHER' and COLUMN10 in(@Acc)  and COLUMN05 in(@NUMBER)   and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current assets
ELSE IF(@TYPE=22410)
BEGIN
	delete from  FITABLE060  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END
--Non Current Liabilities
ELSE IF(@TYPE=22411)
BEGIN
	delete from  FITABLE063  where COLUMN03='RECEIPT VOUCHER' and  COLUMN09 =@TYPEID  and COLUMN10=@Acc and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
END

		 if(@paymenttype=22627)
		 begin
		 update FITABLE044 set  COLUMN18=(null),COLUMN19=(null) where column02 in(select COLUMN13 from FITABLE024 where COLUMN02=@id) and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
		 delete from  FITABLE034  where COLUMN03='PDC RECEIVED'and    COLUMN09 =@Voucher# and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE018  where   COLUMN05 =@Voucher# and  COLUMN06 ='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE019  where   COLUMN05 =@Voucher# and  COLUMN08 =@LineID AND COLUMN06='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 --EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
		 delete from  FITABLE052  where   COLUMN05 =@Voucher# and  COLUMN09 =@LineID AND COLUMN06='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 end
		 else
		 begin
		 --delete from  PUTABLE018  where   COLUMN05 =@Voucher# and  COLUMN06 ='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  PUTABLE019  where   COLUMN05 =@Voucher# and  COLUMN08 =@LineID AND COLUMN06='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 delete from  FITABLE052  where   COLUMN05 =@Voucher# and  COLUMN09 =@LineID AND COLUMN06='RECEIPT VOUCHER' and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
		 update FITABLE001 set  COLUMN10=(cast(@Bankbal as decimal(18,2))-cast(@lineamt as decimal(18,2))-cast(@linetax as decimal(18,2))) where column02=@bank and COLUMNA03=@COLUMNA03
		 end
		  set @Payee=(select COLUMN08 from FITABLE023 where COLUMN02=@COLUMN02)
		 set @COLUMN17=(select sum(isnull(COLUMN17,0)) from FITABLE024 where COLUMN09 in(@internalid))
		 if(@COLUMN17!= '0' and @COLUMN17!= '')
	begin
	declare @d1 int,@MaxRowA int,@FirstRow int
	set @FirstRow=1
	
			  DECLARE curA CURSOR FOR select  FITABLE023.COLUMN02 from FITABLE023 
			  INNER JOIN FITABLE024 ON FITABLE024.COLUMN09=FITABLE023.COLUMN01 AND ISNULL(FITABLE024.COLUMNA13,0)=0 
			  WHERE  FITABLE023.COLUMNA03=@COLUMNA03 AND FITABLE023.COLUMNA02=@COLUMNA02 AND FITABLE023.COLUMN03='1386' AND FITABLE023.COLUMN08=@Payee AND ISNULL(FITABLE023.COLUMNA13,0)=0 and FITABLE023.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s)
			  order by FITABLE023.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @d1
		  if(@d1!='' and @d1 is not null)
		     BEGIN 
			  set @MaxRowA=(select  COUNT(*) from FITABLE023 
			  INNER JOIN FITABLE024 ON FITABLE024.COLUMN09=FITABLE023.COLUMN01 AND ISNULL(FITABLE024.COLUMNA13,0)=0 
			  WHERE  FITABLE023.COLUMNA03=@COLUMNA03 AND FITABLE023.COLUMNA02=@COLUMNA02 AND FITABLE023.COLUMN03='1386' AND FITABLE023.COLUMN08=@Payee and FITABLE023.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@AdvIDs) s) AND ISNULL(FITABLE023.COLUMNA13,0)=0)
			  WHILE @FirstRow <= @MaxRowA
			   BEGIN
		        if(cast(@COLUMN17 as DECIMAL(18,2))!=0 and @MaxRowA>0)
			  begin
				set @cretAmnt=(select column10 from FITABLE023 where column02= @d1  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				if(cast(@COLUMN17 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE023 set column19=(@cretAmnt), COLUMN13='OPEN'  where column02= @d1  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				--EMPHCS1410	Logs Creation by srinivas	
						--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						--'Advance Amount Reset in FITABLE023 Table by '+cast(@cretAmnt as nvarchar(15))+' for '+cast(@id as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						--)

				set @COLUMN17=(cast(@COLUMN17 as DECIMAL(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN17 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
				 declare @BALcretAmnt DECIMAL(18,2)
				 set @BALcretAmnt=(select column19 from FITABLE023 where column02= @d1  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 if(@BALcretAmnt!=@cretAmnt)
				 begin
					update FITABLE023 set column19=(cast(isnull(@BALcretAmnt,0) as DECIMAL(18,2))+cast(isnull(@COLUMN17,0) as DECIMAL(18,2))), COLUMN13='OPEN'  where column02= @d1
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 --EMPHCS1410	Logs Creation by srinivas
						--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET6***********************'+   '' + CHAR(13)+CHAR(10) + '' +
						--'Advance Amount Reset in FITABLE023 Table by '+cast(@COLUMN09 as nvarchar(15))+' for '+cast(@id as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
						--)

				 set @COLUMN17='0'
				 end
				 end
				 end
				FETCH NEXT FROM curA INTO @d1
				SET @FirstRow = @FirstRow + 1 
				END
			END
				CLOSE curA 
				deallocate curA
		end
UPDATE FITABLE024 SET COLUMNA13=1 WHERE COLUMN02=@id
 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 end
        CLOSE cur1
        deallocate cur1
END
end try
begin catch
declare @tempSTR nvarchar(max)
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE023_Voucher.txt',0
return 0
end catch
end










GO
