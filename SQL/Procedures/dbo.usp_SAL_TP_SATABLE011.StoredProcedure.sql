USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_SATABLE011]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_SATABLE011]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  @COLUMN05   nvarchar(250)=null,  
	@COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  @COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  
	@COLUMN10   nvarchar(250)=null,  @COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,  @COLUMN17   nvarchar(250)=null,
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	@COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  @COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null, 
	@COLUMN22   nvarchar(250)=null,  @COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMNA01  varchar(100)=null,  
    @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null, 
    @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT 
)

AS

BEGIN
begin try
IF @Direction = 'Insert'

BEGIN
 --EMPHCS1410	Logs Creation by srinivas
  declare @tempSTR nvarchar(max)
  --EMPHCS1593	payment calculation regarding bank selection in Bill Payment and Customer payment By RAj.Jr
  if((@COLUMN08='' or @COLUMN08=null))
  begin
  set @COLUMN08=0
  end
  --EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications 
set @COLUMN18=(1002)
  --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE011_SequenceNo
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Header Values are Intiated for SATABLE011 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+ isnull( @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull( @COLUMNA01,'')+','+isnull(@COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 											  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+'')
   )
insert into SATABLE011 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,  
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   COLUMN24,
   COLUMNA01, COLUMNA02, COLUMNA03, 
   COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, 
   COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 

  @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  
  --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  
  @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMNA01, 
  @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
  @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
  @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
  @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Header Values are Created in SATABLE011 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
--EMPHCS1391	Single Header For Vendor and Customer Payment srinivas 26/11/2015
BEGIN
declare @newID int
declare @Totamt DECIMAL(18,2)
declare @dueamt DECIMAL(18,2)
declare @paidamt DECIMAL(18,2)
declare @tmpnewID int 
--set @tmpnewID=(select MAX(COLUMN02) from PUTABLE018);
if(cast(isnull(@COLUMN13,0) as decimal(18,2))>0)
begin
--if not exists(select COLUMN05  from PUTABLE018 where COLUMN05=@COLUMN04 AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and COLUMN07=@COLUMN05)
--BEGIN
--if(@tmpnewID>0)
--begin
--set @newID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1;
--end
--else
--begin
--set @newID=1000;
--end
----EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
----EMPHCS1410	Logs Creation by srinivas
--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Account Receivable Values are Intiated for PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+
--   'The Values are '+
--   cast(@newID as nvarchar(250)) +',3000'+  isnull(@COLUMN19,'')+','+  isnull(@COLUMN04,'')+',Payment'+ isnull(@COLUMN05,'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+',NULL,Paid,'+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN22,'')+'') 			
--   )

--insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08 ,column11,column12,COLUMN13,COLUMNA02, COLUMNA03, COLUMN18)
--			values(@newID,3000,@COLUMN19,@COLUMN04,'Payment',@COLUMN05,@COLUMN10,@COLUMN13,NULL,'Paid',@COLUMNA02 , @COLUMNA03,@COLUMN22) 
--		--EMPHCS1410	Logs Creation by srinivas	
--   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Account Receivable Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
--   )
--end
--else
--begin
--			set @paidamt=(select COLUMN11 from PUTABLE018 where COLUMN05=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN07=@COLUMN05)
--            update PUTABLE018 set COLUMN04=@COLUMN19,
--			COLUMN11=(cast(isnull(@paidamt,0) as DECIMAL(18,2))+cast(isnull(@COLUMN13,0) as DECIMAL(18,2))), COLUMN07=@COLUMN05, COLUMN18=@COLUMN22
--			 where COLUMN05=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--end

--EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions

declare @tmpnewID1 int,@COLUMN01 int ,@AccountType nvarchar(250),@BAL decimal(18,2),@SA11COLUMN04 nvarchar(250) 

SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08 and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
SET @BAL=((select column10 from FITABLE001 where column02= @COLUMN08 and columnA03= @COLUMNA03) +cast(isnull( @COLUMN13,0) as decimal(18,2)))
set @COLUMN01 = (select COLUMN01  from SATABLE011 where COLUMN02=@COLUMN02)
if(@AccountType=22409)
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE052)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE052) as int)+1
end
else
begin
set @newID=1000
end
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Intiated for FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN19,'')+','+ isnull(@COLUMN04,'')+',PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@COLUMN01 as nvarchar(20)),'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN22,'')+'') 			
   )
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @COLUMN01,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN19,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN22,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN13,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
		insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08)
		 values(@newID,@COLUMN08,@COLUMN19,@COLUMN04,'PAYMENT',@COLUMN05,@COLUMN01,@COLUMN10,@COLUMN13,@COLUMN13, @COLUMNA02, @COLUMNA03,@COLUMN22, @COLUMNA08)
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Created in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE019)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
end
else
begin
set @newID=100000
end
	-----EMPHCS801 Bank Register - Decimal values are not effected and Trans# is showing 4 digit for Payment by raj.jr 26/7/2015
	--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
	--EMPHCS1410	Logs Creation by srinivas
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN19,'')+','+ isnull(@COLUMN04,'')+',PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@COLUMN01 as nvarchar(20)),'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN22,'')+'') 			
   )
   
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @COLUMN01,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN19,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN22,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN13,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
		insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20)
		 values(@newID,@COLUMN08,@COLUMN19,@COLUMN04,'PAYMENT',@COLUMN05,@COLUMN01,@COLUMN10,@COLUMN13,@COLUMN13, @COLUMNA02, @COLUMNA03,@COLUMN22)
		 --EMPHCS1410	Logs Creation by srinivas		
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03	 		
   --EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Increased By '+cast(@COLUMN13 as nvarchar(250))+' in FITABLE001 Table For '+@COLUMN08+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
END
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(Select COLUMN01 from SATABLE011 where COLUMN02=@COLUMN02)

END

 

IF @Direction = 'Select'

BEGIN

select * from SATABLE011

END 

 

IF @Direction = 'Update'

BEGIN
 --EMPHCS1593	payment calculation regarding bank selection in Bill Payment and Customer payment By RAj.Jr
  if((@COLUMN08='' or @COLUMN08=null))
  begin
  set @COLUMN08=0
  end
  --EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications 
set @COLUMN18=(1002)
DECLARE @PAcc nvarchar(250)
set @PAcc=(SELECT COLUMN08 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
--EMPHCS1410	Logs Creation by srinivas
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Header Values are Intiated for SATABLE011 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+ isnull( @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull( @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+ isnull( @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+ isnull(@COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull( @COLUMNA01,'')+','+isnull(@COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull(@COLUMNA05,'')+','+				  							 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+ isnull(@COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull(@COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+ isnull(@COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull(@COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+ isnull(@COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull(@COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+'')
   )
   set @COLUMN05=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
UPDATE SATABLE011 SET

    COLUMN02=@COLUMN02,   COLUMN03=@COLUMN03,   COLUMN04=@COLUMN04,   COLUMN06=@COLUMN06,  COLUMN07=@COLUMN07,					
	COLUMN08=@COLUMN08,   COLUMN09=@COLUMN09,   COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11,  COLUMN12=@COLUMN12,  COLUMN13=@COLUMN13,					
	COLUMN14=@COLUMN14,   COLUMN15=@COLUMN15,   COLUMN16=@COLUMN16,  COLUMN17=@COLUMN17,  COLUMN18=@COLUMN18,  COLUMN19=@COLUMN19,					
	--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	COLUMN20=@COLUMN20,   COLUMN21=@COLUMN21,   COLUMN22=@COLUMN22,   COLUMN23=@COLUMN23,  COLUMN24=@COLUMN24,  
	COLUMNA01=@COLUMNA01, COLUMNA02=@COLUMNA02, COLUMNA03=@COLUMNA03, COLUMNA04=@COLUMNA04, COLUMNA05=@COLUMNA05,				
	COLUMNA07=@COLUMNA07, COLUMNA08=@COLUMNA08,COLUMNA09=@COLUMNA09,COLUMNA10=@COLUMNA10,COLUMNA11=@COLUMNA11,          
    COLUMNA12=@COLUMNA12, COLUMNA13=@COLUMNA13, COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03=@COLUMNB03,COLUMNB04=@COLUMNB04,				
	COLUMNB05=@COLUMNB05, COLUMNB06=@COLUMNB06,COLUMNB07=@COLUMNB07, COLUMNB08=@COLUMNB08,COLUMNB09=@COLUMNB09,COLUMNB10=@COLUMNB10,
	COLUMNB11=@COLUMNB11, COLUMNB12=@COLUMNB12, COLUMND01=@COLUMND01,COLUMND02=@COLUMND02,COLUMND03=@COLUMND03,COLUMND04=@COLUMND04,
	COLUMND05=@COLUMND05, COLUMND06=@COLUMND06, COLUMND07=@COLUMND07,COLUMND08=@COLUMND08,COLUMND09=@COLUMND09,COLUMND10=@COLUMND10
	WHERE COLUMN02 = @COLUMN02
	--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Customer Patment Line Values are Updated in SATABLE011 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )

	--EMPHCS1208	CUSTOMER PAYMENT EDIT NOT WORKING FOR ACCOUNT TABLES BY SRINIVAS 25/09/2015
	DECLARE @INTERNAL INT,@AMOUNT DECIMAL(18,2)
	SET @INTERNAL=(SELECT COLUMN01 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	DELETE FROM FITABLE064 where COLUMN05 = @INTERNAL AND COLUMN03 in('COMMISSION PAYMENT','PAYMENT') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	SET @AMOUNT=(SELECT SUM(isnull(COLUMN06,0)) FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL) and COLUMNA03= @COLUMNA03 and isnull(COLUMNA13,0)=0)
	--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
	UPDATE FITABLE001 SET COLUMN10=(SELECT COLUMN10 FROM FITABLE001 WHERE COLUMN02=@PAcc and columnA03= @COLUMNA03)-cast(isnull(@AMOUNT,0) as decimal(18,2)) WHERE COLUMN02=@PAcc and columnA03= @COLUMNA03
	--UPDATE PUTABLE018 SET COLUMNA13=1 WHERE COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--UPDATE PUTABLE019 SET COLUMNA13=1 WHERE COLUMN05=@COLUMN04 AND COLUMN08=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	UPDATE FITABLE027 SET COLUMNA13=1 WHERE COLUMN05 IN(SELECT COLUMN01 FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL)) AND COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	UPDATE FITABLE036 SET COLUMNA13=1 WHERE COLUMN03= 'PAYMENT' AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN05 IN(SELECT COLUMN01 FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL))
	
	declare @retAmnt decimal(18,2),@id int,@id1 int
	declare @cretAmnt decimal(18,2)
	SET @COLUMN14=(SELECT sum(isnull(COLUMN14,0)) FROM SATABLE012 WHERE COLUMN08 =(@INTERNAL))
	SET @COLUMN09=(SELECT sum(isnull(COLUMN09,0)) FROM SATABLE012 WHERE COLUMN08 =(@INTERNAL))
	--SET @COLUMN03=(SELECT COLUMN03 FROM SATABLE012 WHERE COLUMN08 =(@INTERNAL))
   delete from PUTABLE018 WHERE COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN17=@INTERNAL
   delete from PUTABLE019 where COLUMN08=@INTERNAL AND COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
   delete from FITABLE052 where COLUMN09=@INTERNAL AND COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	
--set @tmpnewID=(select MAX(COLUMN02) from PUTABLE018);
if(cast(isnull(@COLUMN13,0) as decimal(18,2))>0)
begin
--if(@COLUMN03!='1532')
--begin
--if not exists(select COLUMN05  from PUTABLE018 where COLUMN05=@COLUMN04 AND COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and COLUMN07=@COLUMN05)
--BEGIN
--if(@tmpnewID>0)
--begin
--set @newID=cast((select MAX(COLUMN02) from PUTABLE018) as int)+1;
--end
--else
--begin
--set @newID=1000;
--end
--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Account Receivable Values are Intiated for PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+
--   'The Values are '+
--   cast(@newID as nvarchar(250)) +',3000'+  isnull(@COLUMN19,'')+','+  isnull(@COLUMN04,'')+',Payment'+ isnull(@COLUMN05,'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+',NULL,Paid,'+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN22,'')+'') 			
--   )

--insert into PUTABLE018(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08 ,column11,column12,COLUMN13,COLUMNA02, COLUMNA03, COLUMN18, COLUMNA08)
--			values(@newID,3000,@COLUMN19,@COLUMN04,'Payment',@COLUMN05,@COLUMN10,@COLUMN13,NULL,'Paid',@COLUMNA02 , @COLUMNA03,@COLUMN22, @COLUMNA08) 
--		--EMPHCS1410	Logs Creation by srinivas	
--   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--   'Account Receivable Values are Created in PUTABLE018 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
--   )
--end
--else
--begin
--			set @paidamt=(select COLUMN11 from PUTABLE018 where COLUMN05=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03 and COLUMN07=@COLUMN05)
--            update PUTABLE018 set COLUMN04=@COLUMN19,
--			COLUMN11=(cast(isnull(@paidamt,0) as DECIMAL(18,2))+cast(isnull(@COLUMN13,0) as DECIMAL(18,2))), COLUMN07=@COLUMN05, COLUMN18=@COLUMN22
--			 where COLUMN05=@COLUMN04 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--end
--end

SET @AccountType=(select COLUMN07 from FITABLE001 where COLUMN02=@COLUMN08 and  COLUMNA03=@COLUMNA03 and  isnull(COLUMNA13,0)=0)	
SET @BAL=((select column10 from FITABLE001 where column02= @COLUMN08 and columnA03= @COLUMNA03) +cast(isnull( @COLUMN13,0) as decimal(18,2)))
set @COLUMN01 = (select COLUMN01  from SATABLE011 where COLUMN02=@COLUMN02)
if(@AccountType=22409)
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE052)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE052) as int)+1
end
else
begin
set @newID=1000
end
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Intiated for FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN19,'')+','+ isnull(@COLUMN04,'')+',PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@COLUMN01 as nvarchar(20)),'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN22,'')+'') 			
   )
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @COLUMN01,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN19,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN22,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN13,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
		insert into FITABLE052(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13, COLUMNA02, COLUMNA03, COLUMN16, COLUMNA08)
		 values(@newID,@COLUMN08,@COLUMN19,@COLUMN04,'PAYMENT',@COLUMN05,@COLUMN01,@COLUMN10,@COLUMN13,@COLUMN13, @COLUMNA02, @COLUMNA03,@COLUMN22, @COLUMNA08)
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Cash Register Values are Created in FITABLE052 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from PUTABLE019)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from PUTABLE019) as int)+1
end
else
begin
set @newID=100000
end
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Intiated for PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +','+  isnull(@COLUMN08,'')+','+  isnull(@COLUMN19,'')+','+ isnull(@COLUMN04,'')+',PAYMENT'+  isnull(@COLUMN05,'')+','+  isnull(cast(@COLUMN01 as nvarchar(20)),'')+','+  isnull(@COLUMN10,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMN13,'')+','+  isnull(@COLUMNA02,'')+','+  isnull(@COLUMNA03,'')+','+  isnull(@COLUMN22,'')+'') 			
   )
   
   EXec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'PAYMENT',  @COLUMN04 = @COLUMN04, @COLUMN05 = @COLUMN01,  @COLUMN06 = NULL,
		@COLUMN07 = @COLUMN19,   @COLUMN08 = '22335',  @COLUMN09 = @COLUMN05,    @COLUMN10 = @AccountType, @COLUMN11 = @COLUMN08,
		@COLUMN12 = @COLUMN10,   @COLUMN13 = @COLUMN22,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN13,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	
		insert into PUTABLE019(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN11,COLUMN12, COLUMNA02, COLUMNA03, COLUMN20, COLUMNA08)
		 values(@newID,@COLUMN08,@COLUMN19,@COLUMN04,'PAYMENT',@COLUMN05,@COLUMN01,@COLUMN10,@COLUMN13,@COLUMN13, @COLUMNA02, @COLUMNA03,@COLUMN22, @COLUMNA08)
		 --EMPHCS1410	Logs Creation by srinivas		
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Register Values are Created in PUTABLE019 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
UPDATE FITABLE001 SET COLUMN10=@BAL WHERE COLUMN02=@COLUMN08 AND COLUMNA03=@COLUMNA03	 	
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bank Account Values Increased By '+cast(@COLUMN13 as nvarchar(250))+' in FITABLE001 Table For '+@COLUMN08+' '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
end
--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
	declare @VInitialrow int,@vid nvarchar(250),@vMaxRownum int,@VoucherCrdAmt nvarchar(250),@VochrAmt decimal(18,2),
	@Voucherids nvarchar(250),@MainString nvarchar(250),@Vochertype nvarchar(250),@delimiterstr nvarchar(250)
	,@Vocherid nvarchar(250),@VTransno nvarchar(250),@TransAmt decimal(18,2),@PrevAmt decimal(18,2)

	SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
SET @VInitialrow = 1
		  DECLARE curv CURSOR FOR SELECT COLUMN02 from SATABLE012 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0
		  OPEN curv
		  FETCH NEXT FROM curv INTO @vid
		  SET @vMaxRownum = (SELECT COUNT(*) FROM SATABLE012 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0)
		     WHILE @vInitialrow <= @vMaxRownum
		     BEGIN 
			 set @VoucherCrdAmt=(SELECT isnull(COLUMN14,0) from SATABLE012 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
			 set @Voucherids=(SELECT isnull(COLUMN16,0) from SATABLE012 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
	if(@VoucherCrdAmt!= '0' and @VoucherCrdAmt!= '0.00' and @VoucherCrdAmt!= '')
	begin
		set @VochrAmt=(@VoucherCrdAmt)
		set @MainString=(@Voucherids)
		declare @cnt int,@vochercnt int
		set @cnt = (0);set @vochercnt=(1);
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='C')
		begin
		set @VTransno=(select column04 from satable005 where column02=@Vocherid)
		set @TransAmt=(select isnull(column15,0) from satable005 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from satable005 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from satable005 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update satable005 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update satable005 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		update satable005 set column16='OPEN' where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE023 where column02=@Vocherid)
		set @PrevAmt=(select cast(isnull(COLUMN10,0) as decimal(18,2))-cast(isnull(COLUMN19,0) as decimal(18,2)) from FITABLE023 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE023 set COLUMN19=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE023 set COLUMN19=@TransAmt where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='R')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN16,0) from FITABLE024 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE024 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE024 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE024 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN05,0) from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END

		        SET @COLUMN17=(SELECT COLUMN17 FROM SATABLE012 WHERE COLUMN02 =@vid)
		        SET @COLUMN18=(SELECT COLUMN18 FROM SATABLE012 WHERE COLUMN02 =@vid)
		        SET @COLUMN14=(SELECT COLUMN14 FROM SATABLE012 WHERE COLUMN02 =@vid)
		        SET @COLUMN06=(SELECT COLUMN06 FROM SATABLE012 WHERE COLUMN02 =@vid)
				if(@COLUMN17='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='PaymentVoucher')
				begin
				update FITABLE022 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='AdvancePayment')
				begin
				update FITABLE020 set COLUMN18=(cast(isnull(COLUMN18,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='DebitMemo')
				begin
				update PUTABLE001 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
    FETCH NEXT FROM curv INTO @vid
		         SET @vInitialrow = @vInitialrow + 1 
			END
		CLOSE curv 
		deallocate curv

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
   'Advance Received Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+ 'PAYMENT'+',' +  isnull( cast(@COLUMNA02 as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
--delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
delete from FITABLE026 where column05=@INTERNAL and column04='PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where column05=@INTERNAL and column03='PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET7***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
					update satable012 set columna13=1 where column08=@INTERNAL and COLUMNA03= @COLUMNA03
set @ReturnValue =(Select COLUMN01 from SATABLE011 where COLUMN02=@COLUMN02)
END

 

else IF @Direction = 'Delete'

BEGIN
declare @d1 int
	UPDATE SATABLE011 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Payment Header Data Deleted in SATABLE011 for '+isnull(@COLUMN02,'')+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	--EMPHCS1208	CUSTOMER PAYMENT EDIT NOT WORKING FOR ACCOUNT TABLES BY SRINIVAS 25/09/2015
	UPDATE SATABLE011 SET COLUMNA11=31 WHERE COLUMN02 = @COLUMN02
	SET @INTERNAL=(SELECT COLUMN01 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN08=(SELECT COLUMN08 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMNA02=(SELECT COLUMNA02 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMNA03=(SELECT COLUMNA03 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @AMOUNT=(SELECT SUM(isnull(COLUMN06,0)) FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL) and COLUMNA03= @COLUMNA03 and isnull(COLUMNA13,0)=0)
	DELETE FROM FITABLE064 where COLUMN05 = @INTERNAL AND COLUMN03 in('COMMISSION PAYMENT','PAYMENT') AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
	UPDATE FITABLE001 SET COLUMN10=(SELECT COLUMN10 FROM FITABLE001 WHERE COLUMN02=@COLUMN08 and columnA03= @COLUMNA03) -cast(isnull(@AMOUNT,0) as decimal(18,2)) WHERE COLUMN02=@COLUMN08 and columnA03= @COLUMNA03
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Bank Amount Decreased By '+cast(@AMOUNT as nvarchar(20))+' in FITABLE001 for '+isnull(cast(@COLUMN08 as nvarchar(20)),'')+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	DELETE FROM PUTABLE018 WHERE COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN17=@INTERNAL
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Account Receivable Data Deleted in PUTABLE018 for '+isnull(@COLUMN04,'')+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	DELETE FROM PUTABLE019 WHERE COLUMN05=@COLUMN04 AND COLUMN08=@INTERNAL AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	delete from FITABLE052 where COLUMN09=@INTERNAL AND COLUMN05=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Bank Register Data Deleted in PUTABLE019 for '+@COLUMN04+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)

	DELETE FROM FITABLE027 WHERE COLUMN05 IN(SELECT COLUMN01 FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL)) AND COLUMN09=@COLUMN04 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	DELETE FROM FITABLE036 WHERE  COLUMN03= 'PAYMENT' AND COLUMN10=1049 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 AND COLUMN05 IN(SELECT COLUMN01 FROM SATABLE012 WHERE COLUMN08 IN(@INTERNAL))
	--UPDATE SATABLE012 SET COLUMNA13=1 WHERE  COLUMN08 IN(@INTERNAL)
	
	----EMPHCS1410	Logs Creation by srinivas
	--set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	--'Payment Line Data Deleted in SATABLE012 for '+cast(@INTERNAL as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	--)

	SET @COLUMN14=(SELECT sum(isnull(COLUMN14,0)) FROM SATABLE012 WHERE COLUMN08 =(@INTERNAL))
	SET @COLUMN09=(SELECT sum(isnull(COLUMN09,0)) FROM SATABLE012 WHERE COLUMN08 =(@INTERNAL))
	--SET @COLUMN03=(SELECT COLUMN03 FROM SATABLE012 WHERE COLUMN08 =(@INTERNAL))
	SET @COLUMN04=(SELECT COLUMN04 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	SET @COLUMN05=(SELECT COLUMN05 FROM SATABLE011 WHERE COLUMN02=@COLUMN02)
	--EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
SET @VInitialrow = 1
		  DECLARE curv CURSOR FOR SELECT COLUMN02 from SATABLE012 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0
		  OPEN curv
		  FETCH NEXT FROM curv INTO @vid
		  SET @vMaxRownum = (SELECT COUNT(*) FROM SATABLE012 where COLUMN08= @INTERNAL and isnull(COLUMNA13,0)=0)
		     WHILE @vInitialrow <= @vMaxRownum
		     BEGIN 
			 set @VoucherCrdAmt=(SELECT isnull(COLUMN14,0) from SATABLE012 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
			 set @Voucherids=(SELECT isnull(COLUMN16,0) from SATABLE012 where COLUMN02=@vid and isnull(COLUMNA13,0)=0)
	if(@VoucherCrdAmt!= '0' and @VoucherCrdAmt!= '0.00' and @VoucherCrdAmt!= '')
	begin
		set @VochrAmt=(@VoucherCrdAmt)
		set @MainString=(@Voucherids)
		set @cnt = (0);set @vochercnt=(1);
		WHILE @cnt < @vochercnt
		BEGIN

		if(@VochrAmt=0)begin set @vochercnt=0; end
		set @Vochertype=(Select Substring(@MainString,0,CharIndex(',',@MainString)))--getting first record
		if((Select len(Substring(@MainString,0,CharIndex(',',@MainString))))=0)
		begin
        set @MainString=(@MainString)--only one record i.e first record
        set @Vochertype=(@MainString)
		set @vochercnt=0;
		end
		else
		begin
		set @delimiterstr=(select @Vochertype + ',')
		set @MainString=(Select REPLACE(@MainString,@delimiterstr, ''))--removing first record
		set @MainString=(Select REPLACE(@MainString,@Vochertype, ''))--removing first record
		end
		set @Vocherid=(SELECT SUBSTRING(@Vochertype,2,(LEN(@Vochertype))))--getting first record id
		set @Vochertype=(SELECT SUBSTRING (@Vochertype, 1, 1))--getting first record id type
		
		if(@Vochertype='C')
		begin
		set @VTransno=(select column04 from satable005 where column02=@Vocherid)
		set @TransAmt=(select isnull(column15,0) from satable005 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from satable005 where column02=@Vocherid)
		update satable005 set COLUMN16='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN15,0) from satable005 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update satable005 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update satable005 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='A')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select (isnull(COLUMN10,0)) from FITABLE023 where column02=@Vocherid)
		set @PrevAmt=(select cast(isnull(COLUMN10,0) as decimal(18,2))-cast(isnull(COLUMN19,0) as decimal(18,2)) from FITABLE023 where column02=@Vocherid)
		update FITABLE023 set COLUMN13='OPEN' where column02=@Vocherid and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE023 set COLUMN19=(isnull(@PrevAmt,0)+@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE023 set COLUMN19=@TransAmt where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='R')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN16,0) from FITABLE024 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE024 where column02=@Vocherid)
		update FITABLE023 set COLUMN13='OPEN' where column01 in(select COLUMN09 from FITABLE024 where COLUMN02=@Vocherid and columna03=@columna03) and columna03=@columna03
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE024 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE024 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		else if(@Vochertype='J')
		begin
		--set @VTransno=(select COLUMN04 from FITABLE023 where column02=@Vocherid)
		set @TransAmt=(select isnull(COLUMN05,0) from FITABLE032 where column02=@Vocherid)
		set @PrevAmt=(select isnull(COLUMND05,0) from FITABLE032 where column02=@Vocherid)
		--if(@PrevAmt<=0)begin set @PrevAmt=(select isnull(COLUMN10,0) from FITABLE023 where column02=@Vocherid) end
		if(cast(@PrevAmt as decimal(18,2))>@VochrAmt)
		begin
		update FITABLE032 set COLUMND05=(isnull(@PrevAmt,0)-@VochrAmt) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=0
		end
		else
		begin
		update FITABLE032 set COLUMND05=(0) where column02=@Vocherid and columna03=@columna03
		set @VochrAmt=(@VochrAmt-@PrevAmt)
		end
		end
		if(@vochercnt!=0)
		begin
		set @vochercnt=(@vochercnt+1)
		end
		End
		END

		        SET @COLUMN17=(SELECT COLUMN17 FROM SATABLE012 WHERE COLUMN02 =@vid)
		        SET @COLUMN18=(SELECT COLUMN18 FROM SATABLE012 WHERE COLUMN02 =@vid)
		        SET @COLUMN14=(SELECT COLUMN14 FROM SATABLE012 WHERE COLUMN02 =@vid)
		        SET @COLUMN06=(SELECT COLUMN06 FROM SATABLE012 WHERE COLUMN02 =@vid)
				if(@COLUMN17='Journal')
				begin
				update FITABLE032 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='PaymentVoucher')
				begin
				update FITABLE022 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='AdvancePayment')
				begin
				update FITABLE020 set COLUMN18=(cast(isnull(COLUMN18,0) as decimal(18,2))+cast(isnull(@COLUMN06,0) as decimal(18,2))+cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else if(@COLUMN17='DebitMemo')
				begin
				update PUTABLE001 set COLUMND05=(cast(isnull(COLUMND05,0) as decimal(18,2))-cast(isnull(@COLUMN06,0) as decimal(18,2))-cast(isnull(@COLUMN14,0) as decimal(18,2))) where COLUMN02=@COLUMN18 and  COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
    FETCH NEXT FROM curv INTO @vid
		         SET @vInitialrow = @vInitialrow + 1 
			END
		CLOSE curv 
		deallocate curv

		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET8***********************'+   '' + CHAR(13)+CHAR(10) + '' +
		--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
   'Advance Received Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(cast(@COLUMN04 as nvarchar(250)),'') +','+ 'PAYMENT'+',' +  isnull( cast(@COLUMNA02 as nvarchar(250)),'')+',' + isnull( cast(@COLUMNA03 as nvarchar(250)),'')+',' + '  '))
--delete from FITABLE034 where COLUMN09=@COLUMN04 and COLUMN03='PAYMENT' and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
delete from FITABLE026 where column05=@INTERNAL and column04='PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
delete from FITABLE034 where column05=@INTERNAL and column03='PAYMENT' and column09=@COLUMN04 and columnA02=@columnA02 and columnA03=@columnA03
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET8***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance Received Values are Deleted in FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''))
DECLARE @MaxRowA1 INT,@MaxRownumA1 INT,@dA1 INT
	DECLARE @FirstRowA1 INT=1,@InitialrowA1 int
	 DECLARE curP1 CURSOR FOR SELECT COLUMN02 from SATABLE012 where COLUMN08 in(@INTERNAL) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 
      OPEN curP1
	  FETCH NEXT FROM curP1 INTO @dA1
      SET @MaxRowA1 = (SELECT COUNT(*) FROM SATABLE012 where COLUMN08 in(@INTERNAL) and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 )
         WHILE @FirstRowA1 <= @MaxRowA1
         BEGIN
		    set @COLUMN18 =(select iif(isnull(COLUMN18,0)='',0,isnull(COLUMN18,0))COLUMN18 from  SATABLE012 Where COLUMN02=@dA1 );
            if(@COLUMN18=0)
			begin
			UPDATE SATABLE009 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=13) where COLUMN02 in(SELECT COLUMN03 FROM SATABLE012 WHERE COLUMN08=(@INTERNAL) and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02)
			end
			UPDATE SATABLE012 SET COLUMNA13=1 WHERE  COLUMN02=@dA1
	FETCH NEXT FROM curP1 INTO @dA1
             SET @FirstRowA1 = @FirstRowA1 + 1 
			 end
        CLOSE curP1
        deallocate curP1
	--EMPHCS1410	Logs Creation by srinivas
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET9***********************'+   '' + CHAR(13)+CHAR(10) + '' +
	'Payment Line Data Deleted in SATABLE012 for '+cast(@d1 as nvarchar(15))+''+'' + CHAR(13)+CHAR(10) + ''+' ')
	)
END
    --EMPHCS1410	Logs Creation by srinivas
    exec [CheckDirectory] @tempSTR,'usp_SAL_TP_SATABLE011.txt',0

end try
begin catch
--EMPHCS1410	Logs Creation by srinivas
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_SATABLE011.txt',0

return 0
end catch

end








GO
