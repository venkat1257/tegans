USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_ItemWiseLotDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_ItemWiseLotDetails]
(
	@ItemID nvarchar(250)=NULL,@UOM nvarchar(250)=NULL,@Type nvarchar(250)=NULL,@FrmID nvarchar(250)=NULL,@OPUnit nvarchar(250)=NULL,@AcOwner nvarchar(250)=NULL
)
AS
BEGIN
set @Type=(isnull(@Type,0))
set @UOM=(iif(isnull(@UOM,'')='','0',@UOM))
if(@FrmID=1277 and @Type!='Edit')
begin
if not exists(select COLUMN02 FROM FITABLE043 where COLUMN09=@ItemID And isnull(COLUMN15,0)=(@UOM) And COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0  AND (COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or isnull(COLUMNA02,0)=0))
begin
select l.COLUMN02,l.COLUMN04 FROM FITABLE043 l
inner join FITABLE010 f on f.COLUMN03=l.COLUMN09 and f.COLUMN19=iif(@UOM=f.COLUMN19,@UOM,0) and f.COLUMN22=l.COLUMN02 and isnull(f.COLUMN21,0)=0 and  f.COLUMNA02=@OPUnit and  f.COLUMNA03=l.COLUMNA03
where l.COLUMN09=@ItemID   And isnull(l.COLUMN15,0)=0   And  l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False'  AND 
(l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)
and isnull(f.COLUMN04,0)>0
end
else
begin
select l.COLUMN02,l.COLUMN04 FROM FITABLE043 l
inner join FITABLE010 f on f.COLUMN03=l.COLUMN09 and f.COLUMN19=iif(@UOM='0',f.COLUMN19,@UOM) and f.COLUMN22=l.COLUMN02 and isnull(f.COLUMN21,0)=0 and  f.COLUMNA02=@OPUnit and  f.COLUMNA03=l.COLUMNA03
where l.COLUMN09=@ItemID   And (isnull(l.COLUMN15,0)=(@UOM) or isnull(l.COLUMN15,0)=0)   And  l.COLUMNA03=@AcOwner  and isnull(l.COLUMNA13,'False')='False'  AND 
(l.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or l.COLUMNA02 is null)
and isnull(f.COLUMN04,0)>0
end
end
else
begin
IF not exists(select COLUMN02 FROM FITABLE043 where COLUMN09=@ItemID  And isnull(COLUMN15,0)=@UOM And  
(COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)
  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False')
begin
select COLUMN02,COLUMN04 FROM FITABLE043 where COLUMN09=@ItemID  And isnull(COLUMN15,0)=0  And  
(COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)
  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False'
END
Else
begin
select COLUMN02,COLUMN04 FROM FITABLE043 where COLUMN09=@ItemID  And (isnull(COLUMN15,0)=@UOM or isnull(COLUMN15,0)=0)  And  
(COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or COLUMNA02 is null)
  AND COLUMNA03=@AcOwner  and isnull(COLUMNA13,'False')='False' 
END
END
END
GO
