USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_STOCKSUMMARY]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[USP_REPORTS_STOCKSUMMARY](
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
--EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@FiscalYearEndDt nvarchar(250)= '01/01/2100',
@DateF nvarchar(250)=null
 )
as
begin
if @Item!=''
begin
 set @whereStr= ' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
--if @OperatingUnit!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
--end
--end
--if @Brand!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'

--end
--end
--if @Location!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
--end
--end
--if @Customer!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where CustID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
--end
--else
--begin
--set @whereStr=@whereStr+' and CustID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
--end
--end
--if @Type!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
--end
--else
--begin
--set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
--end
--end
select top 1 @FiscalYearStartDt=column04 ,@FiscalYearEndDt=column05 from FITABLE048 where COLUMNA03=@AcOwner AND COLUMN08= '1' order by COLUMN01 desc

if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where ProjectID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and ProjectID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s) '
end
else
begin
set @whereStr=@whereStr+' and upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end
 
declare @DecimalPositions int
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@AcOwner and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end

select f.COLUMN04 item,

convert(float,convert(double precision,Q.openbal)) openbal,
convert(float,convert(double precision,Q.qr)) qr,
convert(float,convert(double precision,Q.qi)) qi,
Q.price,Q.amt AMT,Q.ItemID,Q.Invalvue,Q.Ouvalvue,Q.Opbvalvue,PR.COLUMN05 Project,Q.ProjectID into #StockLedgerReport from (

(select c.COLUMN03 item,

0 openbal,iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,c.COLUMN08) qr,0 qi,c.COLUMN10 price,(c.COLUMN10)*c.COLUMN08 amt,c.COLUMN03 ItemID,
CAST(isnull(c.COLUMN08,0)*isnull(i.COLUMN17,0) AS DECIMAL(18,2)) Invalvue,0 Ouvalvue,0 Opbvalvue,b.COLUMN21 Project,b.COLUMN21 ProjectID

from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 and b.column03!=1587
 
left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN13 left join satable001 s1 on s1.column02=b.column05 and s1.columna03=b.columna03 left join satable002 s2 on s2.column02=b.column05 and s2.columna03=b.columna03
left join MATABLE002 m on m.COLUMN02=c.COLUMN17 and (m.COLUMNA03=c.COLUMNA03 or isnull(m.COLUMNA03,0)=0)  left join CONTABLE030 l on l.COLUMN02=(case when (cast(c.COLUMN28 as nvarchar(250))!='' and cast(c.COLUMN28 as nvarchar(250))!='0') then c.COLUMN28 else b.COLUMN22 end) and (l.COLUMNA02=b.COLUMN13 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN24 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03

left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN03 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join FITABLE010 i on i.COLUMN03=c.COLUMN03 and i.COLUMN19=c.COLUMN17
 and isnull(i.COLUMN21,0)=ISNULL(c.COLUMN28,0) and i.COLUMNA02=b.COLUMN13 and isnull(i.COLUMN22,0)=isnull(c.COLUMN24,0)
 and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(b.COLUMN21,0)=isnull(i.COLUMN23,0)
where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and b.COLUMN09 >=@FiscalYearStartDt
)
 UNION ALL
(select c.COLUMN04 item,
0 openbal,iif(mu.COLUMN08!='',mu.COLUMN07,c.COLUMN09) qr,0 qi,(isnull(c.COLUMN33,c.COLUMN11)) price,cast(((isnull(c.COLUMN33,c.COLUMN11)))as decimal(18,2))*c.COLUMN09 amt,c.COLUMN04 ItemID,CAST(isnull(c.COLUMN09,0)*isnull(c.COLUMN33,0) AS DECIMAL(18,2)) Invalvue,0 Ouvalvue,0 Opbvalvue,b.COLUMN26 Project,b.COLUMN26 ProjectID
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0

left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN04 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0

left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN15  left join satable001 s1 on s1.column02=b.column05 and s1.columna03=b.columna03
 left join MATABLE002 m on m.COLUMN02=c.COLUMN19 and (m.COLUMNA03=c.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=(case when (cast(c.COLUMN35 as nvarchar(250))!='' and cast(c.COLUMN35 as nvarchar(250))!='0') then c.COLUMN35 else b.COLUMN29 end) and (l.COLUMNA02=b.COLUMN15 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN27 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03

 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null)  AND isnull((b.COLUMNA13),0)=0 and  
 ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 
 and b.COLUMN08 between @FromDate and @ToDate and b.COLUMN08 >=@FiscalYearStartDt
)
UNION ALL
(select c.COLUMN03 item,
0 openbal,0 qr,c.COLUMN07 qi,NULL price,-((c.COLUMN07)*(c.COLUMN09)) amt,c.COLUMN03 ItemID,0 Invalvue,CAST(isnull(c.COLUMN07,0)*isnull(i.COLUMN17,0) AS DECIMAL(18,2)) Ouvalvue,0 Opbvalvue,b.COLUMN36 Project,b.COLUMN36 ProjectID
from PUTABLE001 b inner join PUTABLE002 c on c.COLUMN19=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
 left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN24 
left join MATABLE002 m on m.COLUMN02=c.COLUMN26 and (m.COLUMNA03=c.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=(case when (cast(c.COLUMN37 as nvarchar(250))!='' and cast(c.COLUMN37 as nvarchar(250))!='0') then c.COLUMN37 else b.COLUMN47 end) and (l.COLUMNA02=b.COLUMN24 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=b.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=c.COLUMN17 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
left outer join SATABLE001 s1 on s1.COLUMN02=b.COLUMN05 and s1.COLUMNA03=b.COLUMNA03
left outer join SATABLE002 s2 on s2.COLUMN02=b.COLUMN05 and s2.COLUMNA03=b.COLUMNA03
left join FITABLE010 i on i.COLUMN03=c.COLUMN03 and i.COLUMN19=c.COLUMN26
 and isnull(i.COLUMN21,0)=0 and i.COLUMNA02=b.COLUMN24 and isnull(i.COLUMN22,0)=c.COLUMN17
 and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(b.COLUMN36,0)=isnull(i.COLUMN23,0)
where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN06 between @FromDate and @ToDate and b.COLUMN03=1355 and b.COLUMN33='' and b.COLUMN06 >=@FiscalYearStartDt
)
UNION ALL
 
(select d.COLUMN04 item,
0 openbal, 0 qr,iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,d.COLUMN09) qi,NULL price,-((d.COLUMN09))*(i.COLUMN17) amt,d.COLUMN04 ItemID,0 Invalvue,CAST(isnull(d.COLUMN09,0)*isnull(i.COLUMN17,0) AS DECIMAL(18,2)) Ouvalvue,0 Opbvalvue,a.COLUMN22 Project,a.COLUMN22 ProjectID
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0 and a.column03!=1588
 
left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN15  left join satable001 s1 on s1.column02=a.column05 and s1.columna03=a.columna03 left join satable002 s2 on s2.column02=a.column05 and s2.columna03=a.columna03
left join MATABLE002 m on m.COLUMN02=d.COLUMN19 and (m.COLUMNA03=d.COLUMNA03 or isnull(m.COLUMNA03,0)=0)   left join CONTABLE030 l on l.COLUMN02=(case when (cast(d.COLUMN28 as nvarchar(250))!='' and cast(d.COLUMN28 as nvarchar(250))!='0') then d.COLUMN28 else a.COLUMN23 end) and (l.COLUMNA02=a.COLUMN15 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN24 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03

left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN04 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
 left join FITABLE010 i on i.COLUMN03=d.COLUMN04 and i.COLUMN19=d.COLUMN19
 and isnull(i.COLUMN21,0)=0 and i.COLUMNA02=a.COLUMN15 and isnull(i.COLUMN22,0)=isnull(d.COLUMN24,0)
 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(a.COLUMN22,0)=isnull(i.COLUMN23,0)
 where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt
)
UNION ALL
 
(select d.COLUMN05 item,
0 openbal, 0 qr,iif(mu.COLUMN08!='',mu.COLUMN07,d.COLUMN10) qi ,NULL price, -((d.COLUMN10))*(i.COLUMN17) amt,d.COLUMN05 ItemID,0 Invalvue,CAST(isnull(d.COLUMN10,0)*isnull(i.COLUMN17,0) AS DECIMAL(18,2)) Ouvalvue,0 Opbvalvue,a.COLUMN29 Project,a.COLUMN29 ProjectID
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN05 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN14  left join satable002 s2 on s2.column02=a.column05 and s2.columna03=a.columna03
left join CONTABLE030 l on l.COLUMN02=(case when (cast(d.COLUMN38 as nvarchar(250))!='' and cast(d.COLUMN38 as nvarchar(250))!='0') then d.COLUMN38 else a.COLUMN39 end) and (l.COLUMNA02=a.COLUMN14 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN31 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN05 and i.COLUMN19=d.COLUMN22 and isnull(i.COLUMN21,0)=isnull(d.COLUMN38,0) and isnull(i.COLUMN22,0)=isnull(d.COLUMN31,0) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 AND isnull(i.COLUMN23,0)= isnull(a.COLUMN29,0)
 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
 AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
 and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt
)
UNION ALL
 (select d.COLUMN03 item,
0 openbal, d.COLUMN07 qr,0 qi,NULL price, ((d.COLUMN07)*(i.COLUMN17))  amt,d.COLUMN03 ItemID,CAST(isnull(d.COLUMN07,0)*isnull(i.COLUMN17,0) AS DECIMAL(18,2)) Invalvue,0 Ouvalvue,0 Opbvalvue,a.COLUMN35 Project,a.COLUMN35 ProjectID
from SATABLE005 a inner join SATABLE006 d on d.COLUMN19=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN24 
left join MATABLE002 m on m.COLUMN02=d.COLUMN27 and (m.COLUMNA03=d.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=(case when (cast(d.COLUMN38 as nvarchar(250))!='' and cast(d.COLUMN38 as nvarchar(250))!='0') then d.COLUMN38 else a.COLUMN48 end) and (l.COLUMNA02=a.COLUMN24 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN17 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03
left outer join SATABLE002 s2 on s2.COLUMN02=a.COLUMN05 and s2.COLUMNA03=a.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN27 
and isnull(i.COLUMN21,0)=isnull(d.COLUMN38,0) and isnull(i.COLUMN22,0)=isnull(d.COLUMN17,0) 
and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 AND isnull(i.COLUMN23,0)= isnull(a.COLUMN35,0)
where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND  a.COLUMNA03=@AcOwner and a.COLUMN06 between @FromDate and @ToDate and (a.COLUMN03=1330 or a.COLUMN03=1603) and isnull(a.COLUMN33,'')='' and a.COLUMN06 >=@FiscalYearStartDt
)
UNION ALL
 
  (select d.COLUMN03 item,
0 openbal,isnull(d.COLUMN06,0) qr, 0  qi ,NULL price,0 amt,d.COLUMN03 ItemID,(i.COLUMN17*(d.COLUMN06)) Invalvue,0 Ouvalvue,0 Opbvalvue,a.COLUMN12 Project,a.COLUMN12 ProjectID
 from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0

left join CONTABLE007 c7 on (c7.column02=a.column06)  
 left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
 left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMNA02 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(i.COLUMN23,0)=isnull(a.COLUMN12,0)

 --left join FITABLE010 ii on ii.COLUMN03=d.COLUMN03 and ii.COLUMNA02=a.COLUMN05 
 --and ii.COLUMNA03=a.COLUMNA03 and isnull(ii.COLUMNA13,0)=0 --and isnull(a.COLUMN11,0)=isnull(i.COLUMN23,0)



 left join CONTABLE030 dl on dl.COLUMN02=a.COLUMN10  and dl.COLUMNA03=a.COLUMNA03
 
where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt)
UNION ALL

(select d.COLUMN03 item,
0 openbal,(-ISNULL(d.COLUMN06,0))  qr,0 qi ,isnull(i.COLUMN17,0) price,0 amt,d.COLUMN03 ItemID,
-(i.COLUMN17*(d.COLUMN06)) Invalvue,
0 Ouvalvue,0 Opbvalvue,
a.COLUMN11 Project,a.COLUMN11 ProjectID from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and d.COLUMNA03=a.COLUMNA03 and  isnull((d.COLUMNA13),0)=0

left join CONTABLE007 c7 on (c7.Column02=a.column05) 
left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13
 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMN05 
 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(a.COLUMN11,0)=isnull(i.COLUMN23,0)
 
 left join CONTABLE030 sl on sl.COLUMN02=a.COLUMN09  and sl.COLUMNA03=a.COLUMNA03 
 
where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt
)
UNION ALL
 (select d.COLUMN03 item,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,isnull(d.COLUMN10,0) price,CAST((isnull(d.COLUMN08,0)-isnull(d.COLUMN16,0))*isnull(d.COLUMN10,0) AS DECIMAL(18,2)) amt,d.COLUMN03 ItemID,CAST((isnull(d.COLUMN08,0)-isnull(d.COLUMN16,0))*isnull(d.COLUMN10,0) AS DECIMAL(18,2)) Invalvue,0 Ouvalvue,0 Opbvalvue,a.COLUMN26 Project,a.COLUMN26 ProjectID
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and d.COLUMNa02=a.COLUMNa02 and d.COLUMNa03=a.COLUMNa03  and   isnull((d.COLUMNA13),0)=0

left join CONTABLE007 c7 on c7.COLUMN02=a.COLUMN10 
left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and (m.COLUMNA03=d.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=a.COLUMN16 and (l.COLUMNA02=a.COLUMN10 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN23 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and isnull(bm.COLUMNA13,0)=0 and bm.COLUMNA03=d.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN22
 and isnull(i.COLUMN21,0)=ISNULL(a.COLUMN16,0) and i.COLUMNA02=a.COLUMN10 and isnull(i.COLUMN22,0)=ISNULL(d.COLUMN23,0)
 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(a.COLUMN26,0)=isnull(i.COLUMN23,0)

where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and a.COLUMN05 >=@FiscalYearStartDt
)
UNION ALL 
  (

select c.COLUMN03 item, 0 openbal, 0 qr, C.COLUMN06 qi,NULL price,
((isnull(c.COLUMN06,0))*((isnull(i.COLUMN17,0)))) amt,c.COLUMN03 ItemID
,0 Invalvue,((isnull(c.COLUMN06,0))*((isnull(i.COLUMN17,0)))) Ouvalvue,0 Opbvalvue,b.COLUMN08 Project,b.COLUMN08 ProjectID
from PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and c.columna03=b.columna03 and isnull(c.COLUMNA13,0)=0 
 --inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner
 left join CONTABLE007 c7 on c7.COLUMN02=b.COLUMN11  
 left join satable002 s2 on s2.column02=b.column06 and s2.columna03=b.columna03 and isnull(s2.COLUMNA13,0)=0
  --left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
  left join FITABLE010 i on i.COLUMN03=c.COLUMN03 and i.COLUMN19=c.COLUMN11
 and isnull(i.COLUMN21,0)=0 and i.COLUMNA02=b.COLUMN11 
 and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(b.COLUMN08,0)=isnull(i.COLUMN23,0)
   where  isnull((b.COLUMNA13),0)=0 and  
 ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 

 and b.COLUMN09 between @FromDate and @ToDate and b.COLUMN09 >=@FiscalYearStartDt

 )
UNION ALL
 ----------------
(select item,isnull(sum(openbal),0) as openbal,qr,qi, IIF(PRICE IS NULL,NULL,AVG(price))price, ISNULL(SUM(ISNULL(AMT,0)),0)AMT,ItemID, cast(isnull(sum(Invalvue),0) as decimal(18,2)) as Invalvue, cast(isnull(sum(Ouvalvue),0) as decimal(18,2)) as Ouvalvue,cast(isnull(sum(Opbvalvue),0) as decimal(18,2)) as Opbvalvue,Project,ProjectID from (

select c.COLUMN03 item,
iif(isnull(mu.COLUMN12,0)!=0,(isnull(mu.column07,0)),(isnull(c.column08,0))) openbal,null qr,null qi,(isnull(c.column10,0)) price,(((CAST(isnull(i.column17,0)AS DECIMAL(18,2)))))*(c.COLUMN08) amt,c.COLUMN03 ItemID,0 Invalvue,0 Ouvalvue,(((CAST(isnull(i.column17,0)AS DECIMAL(18,2)))))*(c.COLUMN08) Opbvalvue,b.COLUMN21 Project,b.COLUMN21 ProjectID
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 and b.column03!=1587

inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
 left join MATABLE002 m on m.COLUMN02=c.COLUMN17 and (m.COLUMNA03=c.COLUMNA03 or isnull(m.COLUMNA03,0)=0)  left join CONTABLE030 l on l.COLUMN02=(case when (cast(c.COLUMN28 as nvarchar(250))!='' and cast(c.COLUMN28 as nvarchar(250))!='0') then c.COLUMN28 else b.COLUMN22 end) and (l.COLUMNA02=b.COLUMN13 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=b.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=c.COLUMN24 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03

 left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN03 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
 left join FITABLE010 i on i.COLUMN03=c.COLUMN03 and i.COLUMN19=c.COLUMN17
 and isnull(i.COLUMN21,0)=ISNULL(c.COLUMN28,0) and i.COLUMNA02=b.COLUMN13 and isnull(i.COLUMN22,0)=isnull(c.COLUMN24,0)
 and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(b.COLUMN21,0)=isnull(i.COLUMN23,0)
where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  
 AND  b.COLUMNA03=@AcOwner and (b.COLUMN09< @FromDate) and b.COLUMN09 >=@FiscalYearStartDt and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
 
 union all
(select c.COLUMN04 item,
iif(mu.COLUMN08!='',(isnull(mu.column07,0)),(isnull(c.COLUMN09,0))) openbal,null qr,null qi,(isnull(c.COLUMN33,c.COLUMN11)) price,cast(((((isnull(i.COLUMN17,i.COLUMN17)))))as decimal(18,2))*(c.COLUMN09) amt,c.COLUMN04 ItemID,0 Invalvue,0 Ouvalvue,cast(((((isnull(i.COLUMN17,i.COLUMN17))))) as decimal(18,2))*(c.COLUMN09) Opbvalvue,b.COLUMN26 Project,b.COLUMN26 ProjectID
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 

inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN04 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join MATABLE002 m on m.COLUMN02=c.COLUMN19 and (m.COLUMNA03=c.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=(case when (cast(c.COLUMN35 as nvarchar(250))!='' and cast(c.COLUMN35 as nvarchar(250))!='0') then c.COLUMN35 else b.COLUMN29 end) and (l.COLUMNA02=b.COLUMN15 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=b.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=c.COLUMN27 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
left join FITABLE010 i on i.COLUMN03=c.COLUMN04 and i.COLUMN19=c.COLUMN19 and isnull(i.COLUMN21,0)=isnull(c.COLUMN35,0) and isnull(i.COLUMN22,0)=isnull(c.COLUMN27,0) and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 AND isnull(i.COLUMN23,0)= isnull(b.COLUMN26,0)
 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0  
 AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 
 AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 
 
 )
  union all
(select d.COLUMN04 item,
-(iif(isnull(mu.COLUMN12,0)!=0,(isnull(mu.column07,0)),(isnull(d.column09,0)))) openbal, null qr,null qi ,NULL price,-((((isnull(d.column09,0))))*((isnull(i.column17,0)))) amt,d.COLUMN04 ItemID,0 Invalvue,0 Ouvalvue,-((((isnull(d.column09,0))))*((isnull(i.column17,0)))) Opbvalvue,a.COLUMN22 Project,a.COLUMN22 ProjectID
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0 and a.column03!=1588

inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
left join MATABLE002 m on m.COLUMN02=d.COLUMN19 and (m.COLUMNA03=d.COLUMNA03 or isnull(m.COLUMNA03,0)=0)   left join CONTABLE030 l on l.COLUMN02=(case when (cast(d.COLUMN28 as nvarchar(250))!='' and cast(d.COLUMN28 as nvarchar(250))!='0') then d.COLUMN28 else a.COLUMN23 end) and (l.COLUMNA02=a.COLUMN15 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=d.COLUMN24 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03

left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN04 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join FITABLE010 i on i.COLUMN03=d.COLUMN04 and i.COLUMN19=d.COLUMN19
 and isnull(i.COLUMN21,0)=0 and i.COLUMNA02=a.COLUMN15 and isnull(i.COLUMN22,0)=isnull(d.COLUMN24,0)
 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(a.COLUMN22,0)=isnull(i.COLUMN23,0)
where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  
AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 

)
  union all
  
(select d.COLUMN05 item,
-(iif(mu.COLUMN08!='',(isnull(mu.column07,0)),(isnull(d.COLUMN10,0)))) openbal, null qr,null qi,NULL price, -((iif(mu.COLUMN08!='',(isnull(mu.column07,0)),(isnull(d.COLUMN10,0))))*((isnull(i.COLUMN17,0))))  amt,d.COLUMN05 ItemID,0 Invalvue,0 Ouvalvue,-((iif(mu.COLUMN08!='',(isnull(mu.column07,0)),(isnull(d.COLUMN10,0))))*((isnull(i.COLUMN17,0)))) Opbvalvue,a.COLUMN29 Project,a.COLUMN29 ProjectID
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01  and  isnull((d.COLUMNA13),0)=0

left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN05 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 

left join CONTABLE030 l on l.COLUMN02=(case when (cast(d.COLUMN38 as nvarchar(250))!='' and cast(d.COLUMN38 as nvarchar(250))!='0') then d.COLUMN38 else a.COLUMN39 end) and (l.COLUMNA02=a.COLUMN14 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=d.COLUMN31 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN05 and i.COLUMN19=d.COLUMN22 and isnull(i.COLUMN21,0)=isnull(d.COLUMN38,0) and isnull(i.COLUMN22,0)=isnull(d.COLUMN31,0) and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 AND isnull(i.COLUMN23,0)= isnull(a.COLUMN29,0)
where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0  
AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 

and a.COLUMN08<@FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 

)
 union all
 
(select c.COLUMN03 item, -(isnull(c.COLUMN06,0)) openbal, null qr, null qi,NULL price,
((isnull(c.COLUMN06,0))*((isnull(i.COLUMN17,0)))) amt,c.COLUMN03 ItemID,0 Invalvue,0 Ouvalvue,-((isnull(i.COLUMN04,0))*((isnull(i.COLUMN17,0)))) Opbvalvue,b.COLUMN08 Project,b.COLUMN08 ProjectID
 from PRTABLE007 b inner join PRTABLE008 c on c.COLUMN10=b.COLUMN01 and c.COLUMNA03=b.COLUMNA03 and isnull(c.COLUMNA13,0)=0 
 inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner
 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
  left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
   left join FITABLE010 i on i.COLUMN03=c.COLUMN03 and i.COLUMN19=c.COLUMN11
 and isnull(i.COLUMN21,0)=0 and i.COLUMNA02=b.COLUMN11 
 and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(b.COLUMN08,0)=isnull(i.COLUMN23,0)
where  isnull((b.COLUMNA13),0)=0 and  
 ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner 
and b.COLUMN09 <@FromDate  and b.COLUMN09 >=@FiscalYearStartDt
)
union all
 (select d.COLUMN03 item,
-ISNULL(d.COLUMN06,0) openbal,0  qr,0 qi ,NULL price,0 amt,d.COLUMN03 ItemID,0 Invalvue,0 Ouvalvue,-(i.COLUMN17*(d.COLUMN06)) Opbvalvue,a.COLUMN11 Project,a.COLUMN11 ProjectID

 from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0

left join CONTABLE007 c7 on (c7.Column02=a.column05) 
left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13
 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMN05 
 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(a.COLUMN11,0)=isnull(i.COLUMN23,0)
 
 left join CONTABLE030 sl on sl.COLUMN02=a.COLUMN09  and sl.COLUMNA03=a.COLUMNA03
where  isnull((a.COLUMNA13),0)=0 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))

) union all
 
 (select d.COLUMN03 item,
isnull(d.COLUMN06,0) openbal,0 qr, 0  qi ,NULL price,0 amt,d.COLUMN03 ItemID,0 Invalvue,0 Ouvalvue,(i.COLUMN17*(d.COLUMN06)) Opbvalvue,a.COLUMN12 Project,a.COLUMN12 ProjectID
 from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0

left join CONTABLE007 c7 on (c7.column02=a.column06)  
 left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=a.COLUMN01 and mu.COLUMN06=d.COLUMN03 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
 left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN13 and isnull(i.COLUMN21,0)=isnull(a.COLUMN09,0) and i.COLUMNA02=a.COLUMNA02 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(i.COLUMN23,0)=isnull(a.COLUMN12,0)

 left join CONTABLE030 dl on dl.COLUMN02=a.COLUMN10  and dl.COLUMNA03=a.COLUMNA03
 
where  isnull((a.COLUMNA13),0)=0 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.COLUMN08 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))

) union all
 (select d.COLUMN03 item,
(isnull(d.COLUMN08,0))-(isnull(d.COLUMN16,0)) openbal,null qr,null qi ,(isnull(d.COLUMN10,0)) price,CAST(((isnull(d.COLUMN08,0))-(isnull(d.COLUMN16,0)))*(isnull(i.COLUMN17,0)) AS DECIMAL(18,2)) amt,d.COLUMN03 ItemID,0 Invalvue,0 Ouvalvue,CAST(((isnull(d.COLUMN08,0))-(isnull(d.COLUMN16,0)))*(isnull(i.COLUMN17,0)) AS DECIMAL(18,2)) Opbvalvue,a.COLUMN26 Project,a.COLUMN26 ProjectID
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01  and d.COLUMNa02=a.COLUMNa02 and d.COLUMNa03=a.COLUMNa03 and   isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
left join MATABLE002 m on m.COLUMN02=d.COLUMN22 and (m.COLUMNA03=d.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=a.COLUMN16 and (l.COLUMNA02=a.COLUMN10 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=d.COLUMN23 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and isnull(bm.COLUMNA13,0)=0 and bm.COLUMNA03=d.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN22
 and isnull(i.COLUMN21,0)=ISNULL(a.COLUMN16,0) and i.COLUMNA02=a.COLUMN10 and isnull(i.COLUMN22,0)=ISNULL(d.COLUMN23,0)
 and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(a.COLUMN26,0)=isnull(i.COLUMN23,0)
where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
 AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate and a.COLUMN05 >=@FiscalYearStartDt and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1))  

)
union all
(select c.COLUMN03 item,
-((isnull(c.COLUMN07,0))) openbal,null qr,null qi,NULL price,-(((isnull(c.COLUMN07,0)))*((isnull(c.COLUMN09,0)))) amt,c.COLUMN03 ItemID,0 Invalvue,0 Ouvalvue,-(((isnull(c.COLUMN07,0)))*((isnull(I.COLUMN17,0)))) Opbvalvue,b.COLUMN36 Project,b.COLUMN36 ProjectID 
from PUTABLE001 b inner join PUTABLE002 c on c.COLUMN19=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner  left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
left join MATABLE002 m on m.COLUMN02=c.COLUMN26 and (m.COLUMNA03=c.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=(case when (cast(c.COLUMN37 as nvarchar(250))!='' and cast(c.COLUMN37 as nvarchar(250))!='0') then c.COLUMN37 else b.COLUMN47 end) and (l.COLUMNA02=b.COLUMN24 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=b.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=c.COLUMN17 and (isnull(bm.COLUMN10,0)=c.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=c.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
left join FITABLE010 i on i.COLUMN03=c.COLUMN03 and i.COLUMN19=c.COLUMN26
 and isnull(i.COLUMN21,0)=0 and i.COLUMNA02=b.COLUMN24 and isnull(i.COLUMN22,0)=c.COLUMN17
 and i.COLUMNA03=b.COLUMNA03 and isnull(i.COLUMNA13,0)=0 and isnull(b.COLUMN36,0)=isnull(i.COLUMN23,0)
where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN06 < @FromDate and b.COLUMN06 >=@FiscalYearStartDt and b.COLUMN03=1355 and b.COLUMN33='' and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 

)
UNION all
(select d.COLUMN03 item,
(isnull(d.COLUMN07,0)) openbal, null qr,null qi,NULL price, (((isnull(d.COLUMN07,0)))*((isnull(i.COLUMN17,0))))  amt,d.COLUMN03 ItemID
,0 Invalvue,0 Ouvalvue,(((isnull(d.COLUMN07,0)))*((isnull(i.COLUMN17,0)))) Opbvalvue,a.COLUMN35 Project,a.COLUMN35 ProjectID
from SATABLE005 a inner join SATABLE006 d on d.COLUMN19=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 
left join MATABLE002 m on m.COLUMN02=d.COLUMN27 and (m.COLUMNA03=d.COLUMNA03 or isnull(m.COLUMNA03,0)=0) left join CONTABLE030 l on l.COLUMN02=(case when (cast(d.COLUMN38 as nvarchar(250))!='' and cast(d.COLUMN38 as nvarchar(250))!='0') then d.COLUMN38 else a.COLUMN48 end) and (l.COLUMNA02=a.COLUMN24 or isnull(l.COLUMNA02,0)=0) and l.COLUMNA03=a.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=d.COLUMN17 and (isnull(bm.COLUMN10,0)=d.COLUMNA02  or isnull(bm.COLUMN10,0)=0) and bm.COLUMNA03=d.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
left join FITABLE010 i on i.COLUMN03=d.COLUMN03 and i.COLUMN19=d.COLUMN27 
and isnull(i.COLUMN21,0)=isnull(d.COLUMN38,0) and isnull(i.COLUMN22,0)=isnull(d.COLUMN17,0) 
and i.COLUMNA03=a.COLUMNA03 and isnull(i.COLUMNA13,0)=0 AND isnull(i.COLUMN23,0)= isnull(a.COLUMN35,0)
where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
AND  a.COLUMNA03=@AcOwner and a.COLUMN06<@FromDate and a.COLUMN06 >=@FiscalYearStartDt and (a.COLUMN03=1330 or a.COLUMN03=1603) and isnull(a.COLUMN33,'')='' and   ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) 

)

UNION ALL

SELECT M7.COLUMN02 item,
F57.COLUMN07 openbal,0 qr,0 qi,NULL price,F57.COLUMN09 amt,M7.COLUMN02 ItemID,0 Invalvue,0 Ouvalvue,0 Opbvalvue,'' Project,'' ProjectID
FROM FITABLE054 F54
INNER JOIN FITABLE057 F57 ON F57.COLUMN11 = F54.COLUMN02 AND F57.COLUMNA03 = @AcOwner and f57.columna13 = 0

INNER JOIN MATABLE007 M7 ON M7.COLUMN02 = F57.COLUMN03 AND M7.COLUMNA03 = @AcOwner
INNER JOIN CONTABLE007 C7 ON C7.COLUMN02 = F57.COLUMNA02 AND C7.COLUMNA03 = @AcOwner
LEFT JOIN MATABLE002 M2 ON M2.COLUMN02 = F57.COLUMN04 AND M2.COLUMNA03 = @AcOwner
LEFT JOIN CONTABLE030 C3 ON C3.COLUMN02 = F57.COLUMN05 AND C3.COLUMNA03 = @AcOwner
LEFT JOIN FITABLE043 F3 ON F3.COLUMN02 = F57.COLUMN06 AND F3.COLUMNA03 = @AcOwner
WHERE F54.COLUMN05 between @FiscalYearStartDt and @FiscalYearEndDt AND F54.COLUMNA03 = @AcOwner AND F57.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and f54.columna13 = 0
) p  group by PRICE,item,qr,qi,ItemID,Project,ProjectID
)

) Q  	 inner join MATABLE007 f on f.COLUMN02=q.item and isnull(f.COLUMNA13,0)=0 and f.COLUMN48=1 and f.COLUMNA03=@AcOwner 
         inner join PRTABLE001 PR on PR.COLUMN02=q.Project and isnull(PR.COLUMNA13,0)=0 and PR.COLUMNA03=@AcOwner 
			 left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner 
if(@Type=null)
begin
--set @Query1='select item,openbal,qr,qi,null price,null amt,IIF(PRICE IS NULL,F10.COLUMN17,PRICE) AvgPrice,isnull(openbal,0)+isnull(qr,0)-isnull(qi,0) closing,IIF(PRICE IS NULL,(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0))*(isnull(F10.COLUMN17,0)),AMT) asset, Invalvue,Ouvalvue,Opbvalvue from #StockLedgerReport left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID and isnull(F10.COLUMNA13,0)=0   '+@whereStr+'' 
set @Query1='select item,SUM(openbal) openbal,SUM(isnull(qr,0)) qr,SUM(isnull(qi,0)) qi,null price,null amt,ItemID,SUM(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0)) closing,cast(isnull(sum(Invalvue),0) as decimal(18,2)) Invalvue,cast(isnull(sum(Ouvalvue),0) as decimal(18,2)) Ouvalvue,cast(isnull(sum(Opbvalvue),0) as decimal(18,2)) Opbvalvue
,cast(SUM(isnull(Opbvalvue,0)+isnull(Invalvue,0)-isnull(Ouvalvue,0)) as decimal(18,2)) clsvalue,Project,ProjectID
 from #StockLedgerReport  '+@whereStr+' GROUP BY item,ItemID,Project,ProjectID'

end
else
begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
--end
--else
--begin
--set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
--end

--set @Query1='select item,openbal,qr,qi,null price,null amt,IIF(PRICE IS NULL,F10.COLUMN17,PRICE) AvgPrice,isnull(openbal,0)+isnull(qr,0)-isnull(qi,0) closing,IIF(PRICE IS NULL,(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0))*(isnull(F10.COLUMN17,0)),AMT) asset, Invalvue,Ouvalvue,Opbvalvue from #StockLedgerReport left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID and isnull(F10.COLUMNA13,0)=0   '+@whereStr+'' 

set @Query1='select item,SUM(openbal) openbal,SUM(isnull(qr,0)) qr,SUM(isnull(qi,0)) qi,null price,null amt,ItemID,SUM(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0)) closing,cast(isnull(sum(Invalvue),0) as decimal(18,2)) Invalvue,cast(isnull(sum(Ouvalvue),0) as decimal(18,2)) Ouvalvue,cast(isnull(sum(Opbvalvue),0) as decimal(18,2)) Opbvalvue
,cast(SUM(isnull(Opbvalvue,0)+isnull(Invalvue,0)-isnull(Ouvalvue,0)) as decimal(18,2)) clsvalue,Project,ProjectID
 from #StockLedgerReport  '+@whereStr+' GROUP BY item,ItemID,Project,ProjectID'


--set @Query1='select item,SUM(openbal) openbal,SUM(qr) qr,SUM(qi) qi,null price,null amt,ItemID,IIF(PRICE IS NULL,SUM(F10.COLUMN17),PRICE) AvgPrice,SUM(isnull(openbal,0)+isnull(qr,0)-isnull(qi,0)) closing,IIF(PRICE IS NULL,SUM((isnull(openbal,0)+isnull(qr,0)-isnull(qi,0))*(isnull(F10.COLUMN17,0))),SUM(AMT)) asset from #StockLedgerReport left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID and isnull(F10.COLUMNA13,0)=0  '+@whereStr+' GROUP BY item,ItemID,PRICE
 
end
exec (@Query1)

end







GO
