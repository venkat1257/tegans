USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PR_ProjectDeleteCheck]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_PRO_TP_PR_ProjectDeleteCheck]
(
	@TransactionID int = null ,
	@FormId int = null ,
	@TableName nvarchar(250) = null ,
	@acOW nvarchar(250) = null
)

AS
	BEGIN
		BEGIN	if(@TableName='PUTABLE001')
			begin 
			if(@FormId=1374)
			begin 
				SELECT  a.COLUMN02 COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM PUTABLE001 a  inner join PUTABLE001 b on a.COLUMN02 in(b.COLUMN34 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
		--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
			else if(@FormId=1355)
			begin 
			SELECT a.COLUMN02 COLUMN05
			FROM PUTABLE001 a  inner join PUTABLE015 b on 'D'+cast(a.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and b.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0
			where   a.COLUMN02=@TransactionID and isnull(a.COLUMNA13,0)=0
			end
			end
			else if(@TableName='PUTABLE003')
			begin
			if(@FormId=1286)
			begin
			SELECT  a.COLUMN02 COLUMN05
				FROM PUTABLE003 a  inner join SATABLE010 b on a.COLUMN02 in(b.COLUMN04 ) and b.COLUMNA13=0
				inner join SATABLE009 C on C.COLUMN01 in(b.COLUMN15 ) and C.COLUMNA13=0 and C.COLUMN03=1287
				where   a.COLUMN02=@TransactionID
			end
			begin 
				SELECT  a.COLUMN02 COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM PUTABLE003 a  inner join PUTABLE006 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			end
			else if(@TableName='PUTABLE005')
			begin 
			if(@FormId=1355)
			begin
           --EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
	    if exists (SELECT max(a.COLUMN02) COLUMN05 from PUTABLE005 a  inner join PUTABLE001 s5 on s5.COLUMN11=a.COLUMN04  and  s5.COLUMN03=1355  and s5.COLUMNA13=0
			 where   a.COLUMN02=@TransactionID and a.COLUMN02=s5.COLUMN48 )
			begin
			SELECT max(a.COLUMN02) COLUMN05 from PUTABLE005 a  inner join PUTABLE001 s5 on s5.COLUMN11=a.COLUMN04  and  s5.COLUMN03=1355  and s5.COLUMNA13=0
			 where   a.COLUMN02=@TransactionID and a.COLUMN02=s5.COLUMN48 
			end
			end
			else if(@FormId=1273)
			begin
				SELECT  a.COLUMN02 COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM PUTABLE005 a  inner join PUTABLE015 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			 end
			end
			else if(@TableName='PUTABLE014')
			begin 
				SELECT  NULL COLUMN05
				FROM PUTABLE014 a 
				where   a.COLUMN02=@TransactionID
			end
			else if(@TableName='PRTABLE001')
			begin 
				SELECT  a.COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM PRTABLE001 a  inner join PRTABLE005 b on a.COLUMN02 in(b.COLUMN08 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			else if(@TableName='SATABLE005')
			begin 
			if(@FormId=1376)
			begin 
				SELECT  a.COLUMN02 COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE005 a  inner join SATABLE005 b on a.COLUMN02 in(b.COLUMN35 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
		--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
		
			else if(@FormId=1330)
			begin 
			SELECT a.COLUMN02 COLUMN05
			FROM SATABLE005 a  inner join SATABLE012 b on 'C'+cast(a.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and b.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID and isnull(a.COLUMNA13,0)=0
			end	
			else if(@FormId=1283)
			begin 
			SELECT s5.COLUMN02 COLUMN05
				FROM SATABLE005 s5 inner join SATABLE007 s07 on s5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',s07.COLUMN06) s)  and s5.COLUMNA13=0 and s07.COLUMNA13=0
				where   s5.COLUMN02=@TransactionID 
			end			
			end
			else if(@TableName='SATABLE007')
			begin 
			if(@FormId=1284)
			begin 
			SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE007 a  inner join PUTABLE003 b on a.COLUMN04 in(b.COLUMN10 ) and b.COLUMNA13=0 and b.COLUMNA03=a.COLUMNA03 and a.COLUMNA02=b.COLUMNA02
				where   a.COLUMN02=@TransactionID
			end
			else if(@FormId=1354)
			begin 
			SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE007 a  inner join PUTABLE001 b on a.COLUMN02 in(b.COLUMN33 ) and isnull(b.COLUMNA13,0)=0 and b.COLUMNA03=a.COLUMNA03 and a.COLUMNA02=b.COLUMNA02
				where   a.COLUMN02=@TransactionID
			end
			else
			begin
				SELECT  a.COLUMN02 COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE007 a  inner join SATABLE010 b on a.COLUMN02 in(b.COLUMN04 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			end
			else if(@TableName='SATABLE009')
			begin 
			if(@FormId=1330)
			begin
		--EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
			if exists (SELECT max(a.COLUMN02) COLUMN05 from SATABLE009 a  inner join SATABLE005 s5 on s5.COLUMN11=a.COLUMN04 and s5.COLUMN03=1330  and s5.COLUMNA13=0
			 where   a.COLUMN02=@TransactionID )
			begin
			SELECT max(a.COLUMN02) COLUMN05 from SATABLE009 a  inner join SATABLE005 s5 on s5.COLUMN11=a.COLUMN04 and s5.COLUMN03=1330  and s5.COLUMNA13=0 where 
			  a.COLUMN02=@TransactionID  and a.COLUMN02=s5.COLUMN49
			end
			end
			else if(@FormId=1277)
			begin
			SELECT  a.COLUMN02 COLUMN05
				--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE009 a  inner join SATABLE012 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			else if(@FormId=1287)
			begin
			SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE009 a  inner join SATABLE012 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			end
			--else if(@TableName='SATABLE011')
			--begin 
			--	SELECT  a.COLUMN02 COLUMN05
			--	FROM SATABLE011 a 
			--	where   a.COLUMN02=@TransactionID
			--end
			--EMPHCS1393 rajasekhar reddy patakota 30/11/2015 Payment Voucher & Receipt Voucher set up at account level
			else if(@TableName='FITABLE044')
			begin 
			declare @vochertype nvarchar(250)=null
			set @vochertype=(select COLUMN05 from FITABLE044 where COLUMN02=@TransactionID)
			 if(@vochertype='22626')
			begin 
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE022 a  left join FITABLE044 b on a.COLUMN12 in(b.COLUMN02 ) and isnull(a.COLUMNA13,0)=0
				where   b.COLUMN02=@TransactionID
			end
			else if(@vochertype='22625')
			begin 
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE024 a  left join FITABLE044 b on a.COLUMN13 in(b.COLUMN02 ) and isnull(a.COLUMNA13,0)=0
				where   b.COLUMN02=@TransactionID
			end
			end
			--EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
			else if(@TableName='FITABLE012')
			begin 
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE012 a  inner join FITABLE046 b on a.COLUMN02 in(b.COLUMN03 ) and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID
			end
			--EMPHCS1541	If advance is used in any payment,when deleteing advance it should give alert
			else if(@TableName='FITABLE020')
			begin 
				if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE020 a  inner join PUTABLE015 b on 'A'+cast(a.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and a.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE020 a  inner join PUTABLE015 b on 'A'+cast(a.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and a.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0
				end
				else if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE020 a  inner join FITABLE022 p on a.COLUMN01=p.COLUMN09 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join PUTABLE015 b on 'P'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE020 a  inner join FITABLE022 p on a.COLUMN01=p.COLUMN09 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join PUTABLE015 b on 'P'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0
				end
				else if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE020 a  inner join FITABLE046 b on a.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN11) s) and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE020 a  inner join FITABLE046 b on a.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN11) s) and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID and isnull(a.COLUMNA13,0)=0 
				end
			end
			--EMPHCS1541	If advance is used in any payment,when deleteing advance it should give alert
			else if(@TableName='FITABLE023')
			begin 
				if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE023 a  inner join SATABLE012 b on 'A'+cast(a.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and a.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE023 a  inner join SATABLE012 b on 'A'+cast(a.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and a.COLUMNA03=b.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0
				end
				else if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE023 a  inner join FITABLE024 p on a.COLUMN01=p.COLUMN09 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join SATABLE012 b on 'R'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE023 a  inner join FITABLE024 p on a.COLUMN01=p.COLUMN09 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join SATABLE012 b on 'R'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0
				end
			end
			else if(@TableName='FITABLE031')
			begin 
				if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE031 a  inner join FITABLE032 p on a.COLUMN01=p.COLUMN12 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join SATABLE012 b on 'J'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE031 a  inner join FITABLE032 p on a.COLUMN01=p.COLUMN12 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join SATABLE012 b on 'J'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN16) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID and isnull(a.COLUMNA13,0)=0 
				end
				else if exists (SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE031 a  inner join FITABLE032 p on a.COLUMN01=p.COLUMN12 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join PUTABLE015 b on 'J'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0)
				begin
				SELECT  a.COLUMN02 COLUMN05
				FROM FITABLE031 a  inner join FITABLE032 p on a.COLUMN01=p.COLUMN12 and a.COLUMNA03=p.COLUMNA03 and isnull(p.COLUMNA13,0)=0 inner join PUTABLE015 b on 'J'+cast(p.COLUMN02 as nvarchar(250)) in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and b.COLUMNA03=p.COLUMNA03 and isnull(b.COLUMNA13,0)=0
				where   a.COLUMN02=@TransactionID  and isnull(a.COLUMNA13,0)=0
				end
			end
			else if(@TableName='SATABLE013')
			begin
			SELECT a.COLUMN02 COLUMN05 FROM SATABLE013 a left outer join SATABLE015 b on a.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN18) s) and isnull                    (b.COLUMNA13,0)=0 where   a.COLUMN02=@TransactionID 
			end
			else if(@TableName='SATABLE015')
			begin
			if exists (SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE015 a  inner join SATABLE009 b on a.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN28) s ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID)
				begin
			SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE015 a  inner join SATABLE009 b on a.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN28) s ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
				end
			else if exists (SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE015 a  inner join SATABLE005 b on a.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN34) s ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID)
				begin
			    SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE015 a  inner join SATABLE005 b on a.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',b.COLUMN34) s ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
				end
			end
			else if(@TableName='MATABLE023')
			begin
			if exists (SELECT a.COLUMN02 COLUMN05 FROM MATABLE025 a left outer join MATABLE023 b on b.COLUMN02 in (a.COLUMN04) and isnull(b.COLUMNA13,0)=0 where   b.COLUMN02=@TransactionID  and a.COLUMN08=1)
				begin
			SELECT a.COLUMN02 COLUMN05 FROM MATABLE025 a left outer join MATABLE023 b on b.COLUMN02 in (a.COLUMN04) and isnull(b.COLUMNA13,0)=0 where   b.COLUMN02=@TransactionID  and a.COLUMN08=1 and isnull(a.columna13,0)=0
			end
			else
			begin
			SELECT a.COLUMN02 COLUMN05 FROM MATABLE026 a left outer join MATABLE023 b on b.COLUMN02 in (a.COLUMN06) and isnull(b.COLUMNA13,0)=0 where   b.COLUMN02=@TransactionID and isnull(a.columna13,0)=0
			end
			end
			else
			begin 
				SELECT * FROM  (values (null) ) as T(COLUMN05)
			end
		END
	END




GO
