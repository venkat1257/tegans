USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_DirectDebit_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_POR_TP_DirectDebit_LINE_DATA]
( @BillID int)
AS
BEGIN
--EMPHCS1644 rajasekhar reddy patakota 02/04/2016 RK Stock Details and current Inventory Changes
		declare @Query1 nvarchar(max)					
select p.COLUMN03,p.COLUMN05,p.COLUMN07,p.COLUMN25,p.COLUMN26,p.COLUMN32,p.COLUMN09,cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))COLUMN24,cast((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))+((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01))) as decimal(18,2))COLUMN11,p.COLUMN17,p.avlCOLUMN07,p.aiCOLUMN07,p.aCOLUMN11,cast((cast((p.COLUMN07*p.COLUMN09)as decimal(18,2))-cast(p.disc as decimal(18,2)))*(cast(p.taxper as decimal(18,2))*0.01) as decimal(18,2) ) aCOLUMN24,p.Item,p.COLUMN37,p.UPC into #temp6 from(					
SELECT  
     iif(im.column06!='',im.column06,b.COLUMN04) COLUMN03, b.COLUMN05 COLUMN05,iif(im.column06!='',sum(isnull(im.COLUMN07,0)),(isnull(b.column09,0)-sum(isnull(sl.column07,0)))) COLUMN07,iif(im.column06!='',im.COLUMN08,b.COLUMN19) COLUMN26,b.COLUMN18 COLUMN25,b.COLUMN11 COLUMN09,
	((isnull(b.column09,0)-sum(isnull(sl.column07,0)))*isnull(b.COLUMN11,0)) COLUMN24, cast((((cast(isnull(m.column17,0) as decimal(18,2))*(0.01)))*(iif(b.COLUMN31=22556,cast(b.COLUMN12 as decimal(18,2))-(cast(b.COLUMN12 as decimal(18,2))*cast(b.COLUMN32 as decimal(18,2))*0.01), cast(b.COLUMN12 as decimal(18,2))-cast(b.COLUMN32 as decimal(18,2)))))
+cast(b.COLUMN12 as decimal(18,2)) as decimal(18,2)) COLUMN11,b.COLUMN32,cast((((cast(isnull(m.column17,0) as decimal(18,2))*(0.01)))*((iif(b.COLUMN31=22556,cast(b.COLUMN12 as decimal(18,2))-(cast(b.COLUMN12 as decimal(18,2))*cast(b.COLUMN32 as decimal(18,2))*0.01), cast(b.COLUMN12 as decimal(18,2))-cast(b.COLUMN32 as decimal(18,2)))))) as decimal(18,2)) aCOLUMN24,
	isnull(m.column17,0) aCOLUMN11,b.COLUMN27 COLUMN17,fi.COLUMN04 avlCOLUMN07,i.COLUMN48 aiCOLUMN07,m.column17 taxper,0 disc,iif((i.COLUMN66=1 or i.COLUMN66='True'),'Y','N') lottrack,i.COLUMN04 Item,b.COLUMN35 COLUMN37,b.COLUMN06 'UPC'
	FROM PUTABLE005 a inner join PUTABLE006 b on a.COLUMN01=b.COLUMN13 and isnull(b.COLUMNA13,0)=0
	left join PUTABLE001 sh on  sh.column48=a.COLUMN02 left join PUTABLE002 sl on  sl.column19=sh.COLUMN01 and sl.column03=b.column04 and isnull(sl.COLUMNA13,0)=0
	left join MATABLE013 m on  m.column02=b.COLUMN18
	left join MATABLE007 i on  i.column02=b.COLUMN04
	left join FITABLE038 im on  im.column06=b.COLUMN04 and im.column05=b.column01 and im.columnA03=b.columnA03 and im.columnA02=b.columnA02 and isnull(im.COLUMNA13,0)=0
	left join FITABLE010 fi on  fi.column03=(iif(im.column06!='',im.column06,b.COLUMN04)) and fi.column13=b.COLUMNA02 and isnull(fi.COLUMN19,0)=(iif(im.column06!='',im.COLUMN08,b.COLUMN19)) 
	and isnull(fi.COLUMN21,0)=(case when (cast(isnull(b.COLUMN35,'') as nvarchar(250))!='' and isnull(b.COLUMN35,0)!=0) then b.COLUMN35 else isnull(a.COLUMN29,0) end) and isnull(fi.COLUMN22,0)=isnull(b.COLUMN27,0) 
	and isnull(fi.COLUMN23,0)=(case when (cast(isnull(a.COLUMN26,'') as nvarchar(250))!='' and isnull(a.COLUMN26,0)!=0) then a.COLUMN26 else isnull(a.COLUMN26,0) end)
	--and isnull(fi.COLUMN23,0)=isnull(a.COLUMN26,0)
	WHERE  a.COLUMN02=@BillID and isnull(a.COLUMNA13,0)=0
	 group by b.COLUMN04,im.COLUMN06,im.COLUMN08,b.COLUMN05,b.column09,b.COLUMN19,m.column17,b.COLUMN18,b.COLUMN11,b.column19,b.COLUMN27,fi.COLUMN04,i.COLUMN48,b.COLUMN32,b.COLUMN12,b.COLUMN31,i.COLUMN66,i.column04,b.COLUMN35,b.COLUMN06
	having (isnull(b.column09,0)-sum(isnull(sl.column07,0)))>0) p
set @Query1='select * from #temp6'
execute(@Query1)
END


GO
