USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PRTABLE008]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PRO_TP_PRTABLE008]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)

AS

BEGIN
begin try
set @COLUMN11=(select iif((@COLUMN11!='' and @COLUMN11>0),@COLUMN11,(select max(column02) from fitable037 where column07=1 and column08=1)))
IF @Direction = 'Insert'
BEGIN
declare @DecimalPositions int
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
set @COLUMN07=round(cast(@COLUMN07 as decimal(18,7)),@DecimalPositions)
set @COLUMN06=round(cast(@COLUMN06 as decimal(18,7)),@DecimalPositions)
SELECT @COLUMN02=NEXT VALUE FOR DBO.PRTABLE008_SequenceNo
--set @COLUMN10= (SELECT MAX(COLUMN01) FROM PRTABLE007)
set @COLUMNA02=(select COLUMN11 from PRTABLE007 where COLUMN01=@COLUMN10) 
set @COLUMN08= (SELECT isnull(COLUMN04,0) from MATABLE024 where COLUMN07=@COLUMN03 and COLUMN06='Purchase' and COLUMNA03=@COLUMNA03 )
insert into PRTABLE008 
(
   COLUMN02,  COLUMN03,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN10,COLUMN11,  
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN10,@COLUMN11,
   @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   @COLUMND09, @COLUMND10
)  

declare @Sourceou nvarchar(250)=null,@Memo nvarchar(250)=null,@SAvgCost nvarchar(250)=null,
@SQOH nvarchar(250)=null,@SAvlQty nvarchar(250)=null,@Project nvarchar(250)=null
DECLARE @Projectactualcost DECIMAL(18,2), @Amount DECIMAL(18,2) 
declare @InewID int,@ItmpnewID1 int ,@AID int,@internalid nvarchar(250)=null,@chk bit ,@Customer nvarchar(250)=null,@Cunsumptionno nvarchar(250)=null,@date nvarchar(250)=null
declare @SBAL DECIMAL(18,2)
declare @CBAL DECIMAL(18,2)
declare @lineiid nvarchar(250)=null
declare @DT date
declare @number1 int
declare @CUST int
declare @PRICE DECIMAL(18,2)



set @chk=(select column48 from matable007 where column02=@COLUMN03)
set @Cunsumptionno= (SELECT COLUMN04 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@COLUMN02)
set @date= (SELECT COLUMN09 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Customer= (SELECT COLUMN06 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Memo= (SELECT COLUMN13 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Sourceou= (SELECT COLUMN11 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @SQOH= (SELECT COLUMN04 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)
set @SAvlQty= (SELECT COLUMN08 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)
set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)
set @SAvgCost= (cast(@SAvgCost as DECIMAL(18,2))*round(cast(@COLUMN06 as DECIMAL(18,7)),@DecimalPositions))
 
set @Amount=(round(cast(@COLUMN06 as DECIMAL(18,2)),@DecimalPositions)*cast(@COLUMN08 as DECIMAL(18,2)))
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))+cast(@Amount as DECIMAL(18,2))) where column02=@Project

if(round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)>0)
begin
if(@chk=1)
begin
update FITABLE010 set COLUMN04=(round(cast( @SQOH as DECIMAL(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)), COLUMN08=(round(cast( @SAvlQty as decimal(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)),
COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)as decimal(18,2))-((round(cast(@COLUMN06 as decimal(18,7)),@DecimalPositions))*(round(cast(@COLUMN08 as decimal(18,7)),@DecimalPositions))))
where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11
set @SQOH=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)as decimal(18,7)),@DecimalPositions));
if(round(cast(@SQOH as decimal(18,7)),@DecimalPositions)=0)
begin
UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11
end
else
begin
UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)as decimal(18,2))/round(cast(@SQOH as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11
end
end
else
begin
			set @AID=cast((select isnull(MAX(COLUMN02),1000) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'Resource Consumption Direct',@date,@internalid,@Customer,CAST(@Amount AS decimal(18,2)),NULL,@Cunsumptionno,1078,@COLUMNA02 , @COLUMNA03,0,@Project)
end
set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end
insert into PUTABLE017 
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06, COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13)
values
( @InewID,'1000', GETDATE() ,@COLUMN10,'Resource Consumption Direct' ,@COLUMN09,@SAvgCost,@SAvgCost,@Sourceou,@Project,@COLUMNA02,@COLUMNA03,1,0)



--set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
--set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);

set @number1=(select max(column01) from PRTABLE007 where COLUMN01=@COLUMN10)                                                                                                                                                                                                                                                                                                                                                                                                                                         
set @DT=(select COLUMN09 from PRTABLE007 where column01=@number1)
set @CUST=(select COLUMN06 from PRTABLE007 where column01=@number1)
set @lineiid=(select max(column01) from PRTABLE008 where column02=@COLUMN02)
set @PRICE=(select column17 from matable007 where column02=@COLUMN03)

--if(@chk=0)
--	begin	
--		if(@AID>0)
--			begin
--				--set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
--			end
--		else
--			begin
--				set @AID=1000;
--			end

--			--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			
--			--values(@AID,@DT,'Resourse Consumption',@lineiid,@CUST,NULL,1053,CAST((CAST(isnull(@COLUMN06,0) AS decimal(18,2))*CAST(isnull(@COLUMN08,0) AS decimal(18,2)))AS decimal(18,2)),CAST((CAST(isnull(@COLUMN06,0) AS decimal(18,2))*CAST(isnull(@COLUMN08,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project)
			
--			end
--else
--	begin	
		
--			if(@AID>0)
--			begin
--				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
--			end
--		else
--			begin
--				set @AID=1000;
--			end
--			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
--			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
--			values(@AID,@DT,'Resourse Consumption Direct',@lineiid,@CUST,NULL,1056,
--			cast(((cast(cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN06,0)as DECIMAL(18,2))as DECIMAL(18,2))))AS decimal(18,2))
--			,@CBAL+cast(((cast(cast(isnull(@PRICE,0) as DECIMAL(18,2))*
--			cast(isnull(@COLUMN06,0)as DECIMAL(18,2))as DECIMAL(18,2))))AS decimal(18,2)),
--			@COLUMNA02 , @COLUMNA03,0,@Project)
--	end




end
set @ReturnValue = 1
END

 ELSE IF @Direction = 'Select'

BEGIN

select * from PRTABLE008

END 

 

ELSE IF @Direction = 'Update'

BEGIN

if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
  set @COLUMNA02=( CASE WHEN (@COLUMN10!= '' and @COLUMN10 is not null ) THEN (select COLUMN11 from PRTABLE007 
   where COLUMN01=@COLUMN10) else @COLUMNA02  END )
   set @COLUMN08= (SELECT isnull(COLUMN04,0) from MATABLE024 where COLUMN07=@COLUMN03 and COLUMN06='Purchase' and COLUMNA03=@COLUMNA03)
UPDATE PRTABLE008 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN10=@COLUMN10,  COLUMN11=@COLUMN11,   
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,            
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03, 
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08, 
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
WHERE COLUMN02 = @COLUMN02
   --EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
set @Memo= (SELECT COLUMN13 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Project= (SELECT COLUMN08 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @Sourceou= (SELECT COLUMN11 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @SQOH= (SELECT COLUMN04 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)
set @SAvlQty= (SELECT COLUMN08 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)
set @SAvgCost= (SELECT COLUMN17 FROM FITABLE010 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)
set @SAvgCost= (cast(@SAvgCost as DECIMAL(18,2))*round(cast(@COLUMN06 as DECIMAL(18,7)),@DecimalPositions))
--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
set @Cunsumptionno= (SELECT COLUMN04 FROM PRTABLE007 WHERE COLUMN01=(@COLUMN10))
set @internalid=  (select COLUMN01 from PRTABLE008 where COLUMN02=@COLUMN02)
--EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
set @chk=(select column48 from matable007 where column02=@COLUMN03)

set @Amount=(round(cast(@COLUMN06 as DECIMAL(18,7)),@DecimalPositions)*cast(@COLUMN08 as DECIMAL(18,2)))
set @Projectactualcost=(select column09 from PRTABLE001 where column02=@Project)
update PRTABLE001 set column09=(cast(isnull(@Projectactualcost,0) as DECIMAL(18,2))+cast(@Amount as DECIMAL(18,2))) where column02=@Project

if(round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)>0)
--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
begin
if(@chk=1)
begin
update FITABLE010 set COLUMN04=(round(cast( @SQOH as DECIMAL(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)), COLUMN08=(round(cast( @SAvlQty as decimal(18,7)),@DecimalPositions)-round(cast( @COLUMN06 as DECIMAL(18,7)),@DecimalPositions)),
COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)as decimal(18,2))-((round(cast(@COLUMN06 as decimal(18,7)),@DecimalPositions))*(cast(@COLUMN08 as decimal(18,2)))))
where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11
set @SQOH=(round( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)as decimal(18,7)),@DecimalPositions));
if(round(cast(@SQOH as decimal(18,7)),@DecimalPositions)=0)
begin
UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11
end
else
begin
UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11)as decimal(18,2))/round(cast(@SQOH as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=(@COLUMN03) and COLUMN13=@Sourceou and COLUMN19=@COLUMN11
end
--EMPHCS1185 rajasekhar reddy patakota 28/09/2015 Service items calculation in Resource Consumprion
end
else
begin
--EMPHCS1218 rajasekhar reddy patakota 29/09/2015 Project - at the time of resources consumption for service items , system is looking for aval qty
		UPDATE FITABLE036 SET COLUMN07=CAST(@Amount AS decimal(18,2)),COLUMNA13=@COLUMNA13
		WHERE COLUMN05=cast(@internalid as nvarchar(250)) and COLUMN03='Resource Consumption Direct' AND COLUMN09=cast(@Cunsumptionno as nvarchar(250)) AND COLUMN10=1078 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
end
set @ItmpnewID1=(select MAX(COLUMN02) from PUTABLE017)
if(@ItmpnewID1>0)
begin
set @InewID=cast((select MAX(COLUMN02) from PUTABLE017) as int)+1
end
else
begin
set @InewID=1000
end
insert into PUTABLE017 
( COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06, COLUMN09,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13)
values
( @InewID,'1000', GETDATE() ,@COLUMN10,'Resource Consumption Direct' ,@COLUMN09,@SAvgCost,@SAvgCost,@Sourceou,@Project,@COLUMNA02,@COLUMNA03,1,0)
end


--EMPHCS1252 gnaneshwar on 7/10/2015  Inserting Data Into Cost Of Sales When Resourse Consumption Created in Project
set @PRICE=(select column17 from matable007 where column02=@COLUMN05)
--EMPHCS730	Account Owner Condition for Income Register by srinivas 7/21/2015

--set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
--if(@chk=0)
--	begin
	DECLARE @id nvarchar(250)
	DECLARE @MaxRownum INT
	DECLARE @Item decimal(18,2),
	@number int,
	@BAL decimal(18,2)=0,
	@AMOUNT1 decimal(18,2)=0,@lineID nvarchar(250),@lineID1 nvarchar(250)
		  DECLARE @Initialrow INT=1

	set @lineID1=(select column01 from PRTABLE008 where column02=(cast(@COLUMN02 as int)) )
	--SET @AMOUNT=(SELECT isnull(COLUMN09,0) FROM FITABLE025 WHERE COLUMN05=@lineID1 AND COLUMN08=1056  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
	   set @number = (select COLUMN01 from  PRTABLE008 WHERE COLUMN02=@COLUMN02)
	     --EMPHCS1167	Service items calculation by srinivas22/09/2015
 -- if(@chk=1)
 -- begin
 -- if(cast(isnull(@AMOUNT,0) as decimal(18,2))>0)
	--BEGIN

	--set @lineID=(select max(column01) from PRTABLE008 where column02<(cast(@COLUMN02 as int)) and COLUMNA13=0)
	--set @lineID1=(select column01 from PRTABLE008 where column02=(cast(@COLUMN02 as int)) and COLUMNA13=0)
	----SET @AMOUNT=(SELECT isnull(COLUMN09,0) FROM FITABLE025 WHERE COLUMN05=@lineID1 AND COLUMN08=1056  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	----set @AMOUNT1=(select max(isnull(COLUMN10,0)) from FITABLE025 where COLUMN05<=(@lineID) AND COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	--set @PRICE=(select isnull(column17,0) from matable007 where column02=@COLUMN03 and COLUMNA13=0)

	----UPDATE FITABLE025 SET COLUMN10=cast((@AMOUNT1+cast(((cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN06,0)as DECIMAL(18,2))))AS decimal(18,2)))AS decimal(18,2)),COLUMN09=cast(((cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN06,0)as DECIMAL(18,2))))AS decimal(18,2)),COLUMNA13=@COLUMNA13 WHERE COLUMN05=@lineID1 AND COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	
	----SET @BAL=(SELECT isnull(COLUMN10,0) FROM FITABLE025 WHERE COLUMN05=@lineID1 AND COLUMN08=1056  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	----DECLARE cur3 CURSOR FOR SELECT COLUMN02 from FITABLE025 where cast(COLUMN05 as int)>@lineID1 AND  COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0
	----	  OPEN cur3
	----	  set @Initialrow=(1)
	----	  FETCH NEXT FROM cur3 INTO @id
	----	  SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE025 where cast(COLUMN05 as int)>@lineID1 AND  COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0)
	----	     WHILE @Initialrow <= @MaxRownum
	----	     BEGIN 
	----	         set @Item=(select COLUMN09 from FITABLE025 where COLUMN02=@id)
	----		     UPDATE FITABLE025 SET COLUMN10=cast(isnull(@Item,0) as decimal(18,2))+cast(isnull(@BAL,0) as decimal(18,2))
	----				WHERE COLUMN02=@id
	----			 SET @BAL=(SELECT isnull(COLUMN10,0) FROM FITABLE025 WHERE COLUMN02=@id and COLUMNA13=0)
	----			FETCH NEXT FROM cur3 INTO @id
	----	         SET @Initialrow = @Initialrow + 1 
	----		END
	----	CLOSE cur3 
	--end	
 -- else
	--begin	
	--set @CUST=(select column06 from PRTABLE007 where column01=@number)
	
	----set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and COLUMNA13=0) ;
	----set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
	--set @lineiid=(select max(column01) from PRTABLE008 where column02=@COLUMN02)
		
	--	--if(@AID>0)
	--	--	begin
	--	--		set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
	--	--	end
	--	--else
	--	--	begin
	--	--		set @AID=1000;
	--	--	end
	--	--	insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
	--	--	--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
	--	--	values(@AID,getdate(),'Resourse Consumption Direct',@lineiid,@CUST,NULL,1056,cast(cast(cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN06,0)as DECIMAL(18,2))as DECIMAL(18,2))AS decimal(18,2)),@CBAL+cast(cast(cast(isnull(@PRICE,0) as DECIMAL(18,2))*cast(isnull(@COLUMN06,0)as DECIMAL(18,2))as DECIMAL(18,2))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0)
	--end
	--     --EMPHCS1167	Service items calculation by srinivas22/09/2015
	--end
	--else
	--begin
	----SET @AMOUNT=(SELECT isnull(COLUMN09,0) FROM FITABLE025 WHERE COLUMN05=@lineID1 AND COLUMN08=1053  AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
	----if(cast(isnull(@AMOUNT,0) as decimal(18,2))>0)
	----begin
	----UPDATE FITABLE025 SET COLUMN10=CAST((CAST(isnull(@COLUMN06,0) AS decimal(18,2))*CAST(isnull(@COLUMN08,0) AS decimal(18,2)))AS decimal(18,2)),COLUMN09=CAST((CAST(isnull(@COLUMN06,0) AS decimal(18,2))*CAST(isnull(@COLUMN08,0) AS decimal(18,2)))AS decimal(18,2)),COLUMNA13=@COLUMNA13
	----WHERE COLUMN05=@lineID1 AND COLUMN08=1053 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	----end
	----else
	----begin
	----		set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
	----		--insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
	----		----EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
	----		--values(@AID,getdate(),'Resourse Consumption',@lineID1,@CUST,NULL,1053,CAST((CAST(isnull(@COLUMN06,0) AS decimal(18,2))*CAST(isnull(@COLUMN08,0) AS decimal(18,2)))AS decimal(18,2)),CAST((CAST(isnull(@COLUMN06,0) AS decimal(18,2))*CAST(isnull(@COLUMN08,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0)
	----end
	--end


set @ReturnValue = 1
END

 

ELSE IF @Direction = 'Delete'

BEGIN

UPDATE PRTABLE008 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02

END

end try
begin catch
declare @tempSTR nvarchar(max)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PRTABLE008_RCDirect.txt',0
return 0
end catch
end




GO
