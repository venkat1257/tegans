USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_SATABLE006]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAR_TP_SATABLE006]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	@COLUMN29   NVARCHAR(250)=NULL,  @COLUMN30   NVARCHAR(250)=NULL,  @COLUMN31   NVARCHAR(250)=NULL,
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	@COLUMN32   NVARCHAR(250)=NULL,  @COLUMN35   NVARCHAR(250)=NULL,  @COLUMN36   nvarchar(250)=null,
	--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	@COLUMN37   nvarchar(250)=null,  @COLUMN38   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null, 
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250)=null,       @ReturnValue nvarchar(250)=null 
)

AS
BEGIN
begin try
 --EMPHCS gnaneshwar 17/8/2015 uom condition checking in sales order
	  set @COLUMN27=(select iif((@COLUMN27!='' and @COLUMN27>0),@COLUMN27,(select max(column02) from fitable037 where column07=1 and column08=1)))
	  SELECT @COLUMNB01= COLUMN04,@COLUMNB02= COLUMN03 FROM SATABLE005 WHERE COLUMN01=@COLUMN19
IF @Direction = 'Insert'
BEGIN
declare @FRMID int
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE006_SequenceNo
set @FRMID= (SELECT COLUMN03 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN19= (SELECT COLUMN01 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN20= (SELECT COLUMN24 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN21= (SELECT COLUMN25 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN22= (SELECT COLUMN26 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN23= (SELECT COLUMN27 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
declare @payeetyp nvarchar(250)
set @payeetyp=(select isnull(COLUMN51,'22335') from SATABLE005 where COLUMN01=@COLUMN19)
--EMPHCS  gnaneshwar 11/8/2015 uom condition checking in credit memo
set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0 ) THEN (select COLUMN24 from SATABLE005
  where COLUMN01=@COLUMN19) else @COLUMNA02  END )
insert into SATABLE006 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   --EMPHCS922 gnaneshwar 17/8/2015 uom condition checking in credit memo
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32, COLUMN35,   COLUMN36,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN37,  COLUMN38,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS922 gnaneshwar 17/8/2015 uom condition checking in credit memo
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN27,  @COLUMN07,  @COLUMN30,  @COLUMN31,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   @COLUMN32,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMNA01, @COLUMNA02, @COLUMNA03,
   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10,
   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08,
   @COLUMND09, @COLUMND10
)

declare @RefundQty DECIMAL(18,2)
declare @RefID int
declare @Refreceipt int
declare @TQty DECIMAL(18,2)
declare @RRQty DECIMAL(18,2),@AMNT DECIMAL(18,2)
declare @PID int
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
declare @Result nvarchar(250),@Amtin nvarchar(250),@ltaxamt DECIMAL(18,2)
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
declare @date nvarchar(250),@SOID nvarchar(250),@SONum nvarchar(250),@InvoiceNo nvarchar(250),@InvoiceId nvarchar(250),@RIQty DECIMAL(18,2),@SIQty DECIMAL(18,2)
set @date=(select COLUMN06 from SATABLE005 where COLUMN01=@COLUMN19);
set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@COLUMN19);
set @InvoiceNo=(select COLUMN49 from SATABLE005 where COLUMN01=@COLUMN19);
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @Amtin=(select COLUMN50 from SATABLE005 where COLUMN01=@COLUMN19);
set @InvoiceId=(select COLUMN01 from SATABLE009 where COLUMN02=@InvoiceNo);
set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
set @SONum=(select COLUMN04 from SATABLE005 where COLUMN04=(select COLUMN11 from PUTABLE001 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0 );
set @SOID=(select COLUMN02 from SATABLE005 where COLUMN04=(select COLUMN11 from PUTABLE001 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
set @RefundQty=(select isnull(COLUMN13,0) from PUTABLE002 where COLUMN02=@COLUMN36 and isnull(COLUMNA13,0)=0)
end
else
begin
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
SET @COLUMN17=iif(@COLUMN17='',0,isnull(@COLUMN17,0))
set @RefundQty=isnull((select COLUMN13 from PUTABLE002 where COLUMN19 in (@PID) and COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=ISNULL(@COLUMN27,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@COLUMN17,0) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 ),0);
end
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin	
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where  COLUMN02=@COLUMN36 and isnull(COLUMNA13,0)=0
end
else
begin
UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=ISNULL(@COLUMN27,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@COLUMN17,0) and COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
end
SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE COLUMN19 in (@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 )
set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM SATABLE006 WHERE COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN33=CAST(@Refreceipt AS NVARCHAR(250))) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RIQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE COLUMN19 in(@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @SIQty=(SELECT sum(isnull(COLUMN08,0)) FROM PUTABLE004 WHERE COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN02=@Refreceipt) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
end
else
begin
set @TQty=(SELECT sum(isnull(COLUMN10,0)) FROM SATABLE010 WHERE COLUMN15 in(@InvoiceId) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN49=@InvoiceNo) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @SIQty=(@TQty)
end
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where  COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0
end
else
begin
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where COLUMN19=@COLUMN19 and COLUMN03=@COLUMN03 and isnull(COLUMN27,0)=ISNULL(@COLUMN27,0)  and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@COLUMN17,0) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
end

if(@SIQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=109)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=108)
end
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@COLUMN19)

if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin
UPDATE PUTABLE003 set COLUMN18=@Result where COLUMN02=(@Refreceipt)
IF(@TQty=@RIQty)
begin
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=104)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=105)
end
end
ELSE 
BEGIN
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=102)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=103)
end
end
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@PID)
END

if((@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0') and (@InvoiceNo='' or isnull(@InvoiceNo,0)=0 or isnull(@InvoiceNo,0)='0'))
begin
UPDATE SATABLE005 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=109) where COLUMN01=(@COLUMN19)
end
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
if exists(select column01 from SATABLE006 where column19=@column19  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
					begin 
declare @Type nvarchar(250)
 --set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@COLUMN19);
Declare @CustID int, @memo nvarchar(250), @Num nvarchar(250), @dueDT date, @newID int, @tmpnewID int , @Totamt DECIMAL(18,2),
@Creditamt DECIMAL(18,2),@Tax nvarchar(250), @TaxAG nvarchar(250), @TAXRATE nvarchar(250),@TaxColumn17 nvarchar(250),
@HeaderTax nvarchar(250), @HeaderTaxAG nvarchar(250), @HeaderTAXRATE nvarchar(250),@HeaderTaxColumn17 nvarchar(250)
set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@COLUMN19)
set @memo = (select COLUMN09 from SATABLE005 where COLUMN01=@COLUMN19)
set @Num = (select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19)
set @dueDT = (select COLUMN08  from SATABLE005 where COLUMN01=@COLUMN19)
declare @chk bit
declare @AID int
declare @SBAL DECIMAL(18,2)
--declare @PRICE DECIMAL(18,2)
declare @CBAL DECIMAL(18,2)
declare @number1 int
declare @DT date
declare @PRICE DECIMAL(18,2)
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
 declare @lineiid nvarchar(250)=null, @Project nvarchar(250)=null,@ProjectID nvarchar(250)
 declare @CUST int
 --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @number1=(select max(column01) from SATABLE005 where column01=@COLUMN19 )
set @DT=(select COLUMN06 from SATABLE005 where column01=@number1)
set @chk=(select column48 from matable007 where column02=@COLUMN03)
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0) ;
set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0) ;
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
set @PRICE=(select column17 from matable007 where column02=@COLUMN03)
set @lineiid=(select max(column01) from SATABLE006 where column02=@COLUMN02)
set @CUST=(select column05 from SATABLE005 where column01=@number1)
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
set @Project=(select column35 from SATABLE005 where column01=@number1)
set @ProjectID=(select column35 from SATABLE005 where column01=@COLUMN19)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)

if(@chk=0)
	begin	
		if(@AID>0)
			begin
			--Sales of Product Income storing each row wise at line level by srinivas 7/21/2015
			--delete from FITABLE025 where column05=@number1	 and column08=1052
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
			declare @ACID int,@ACTYPE int
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),0)!=0,(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1053))
					SET @ACTYPE=(SELECT top 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN25,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			values(@AID,@DT,'Credit Memo',@lineiid,@CUST,@memo,@ACID,CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@SBAL+CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project)
						
	end
else
	begin	
  --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
  declare @Qty_On_Hand decimal(18,2),@PRATE decimal(18,2),@AvgPrice	decimal(18,2), @Qty	decimal(18,2),@lotno nvarchar(250),@Location nvarchar(250)
  set @lotno=(select COLUMN17 from SATABLE006 where COLUMN02=@COLUMN02)
  set @Location=(select COLUMN48 from SATABLE005 where column01=@number1)
  set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
  set @Location = (case when (cast(isnull(@COLUMN38,'') as nvarchar(250))!='' and @COLUMN38!=0) then @COLUMN38 else @Location end)
  set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN27,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@ProjectID,0) and COLUMN24=@COLUMN04);
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
  if(@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0')
			 begin	--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
if exists(select COLUMN03 from FITABLE010  where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)
begin
  set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04);
  --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  set @PRATE=(isnull(@AvgPrice,0))
  --IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
  set @Qty=cast(@COLUMN07 as decimal(18,2));
  set @Qty_On_Hand=( @Qty_On_Hand + @Qty);
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04) as decimal(18,2))+(cast(@COLUMN07 as decimal(18,2))*cast(@PRATE as decimal(18,2))))
 ,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT'
  WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=isnull(@ProjectID,0) and COLUMN24=@COLUMN04
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)as decimal(18,2)));
  if(cast(@Qty_On_Hand as decimal(18,2))=0)
  begin
  UPDATE FITABLE010 SET COLUMN12=0,COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04
  end
  else
  begin
  UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)),COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='INSERT' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04
  end
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
 end
else
begin
declare @newIID int,@tmpnewIID1 int,@ASSET decimal(18,2) 
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
SET @AvgPrice=(ISNULL(@COLUMN09,0))
SET @ASSET=(CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03,COLUMN23,COLUMNA11,COLUMNA07,COLUMNB01,COLUMNB02,COLUMNB03,COLUMN24
)
values
(
  @newIID,@COLUMN03,@COLUMN07,@COLUMN07, @ASSET,@AvgPrice,@COLUMN27,  @lotno, @location,@COLUMN20,@COLUMNA02,@COLUMNA03,@ProjectID,@COLUMNA08,@COLUMNA07,@COLUMNB01,@COLUMNB02,'INSERT',@COLUMN04 )
END
declare @VendorID nvarchar(250),@IAmt decimal(18,2)
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN27,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@ProjectID,0) and COLUMN24=@COLUMN04);
SET @IAmt=(cast(@COLUMN07 as decimal(18,2))*cast(@AvgPrice as decimal(18,2)))
SET @VendorID=(select COLUMN05 from SATABLE005 where COLUMN01=@number1)
SET @Memo=(select COLUMN09 from SATABLE005 where COLUMN01=@number1)
SET @date=(select COLUMN06 from SATABLE005 where COLUMN01=@number1)
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE017) as int)+1
if(@payeetyp!='22305')
begin
  insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
   @newID,'1000',@date ,@number1,'Credit Memo', @VendorID,'Inventory Received',@Memo,@IAmt,@IAmt,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lineiid, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
end
else if(@payeetyp='22305')
begin
  insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
   @newID,'1000',@date ,@number1,'Credit Memo', @VendorID,'Inventory Received',@Memo,@COLUMN25,@COLUMN25,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lineiid, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
			SET @ACTYPE=(SELECT top 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @date,   @COLUMN08 = @payeetyp,  @COLUMN09 = @VendorID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'Credit Memo',@date,@lineiid,@VendorID,@COLUMN25,NULL,@Num,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @date,   @COLUMN08 = @payeetyp,  @COLUMN09 = @VendorID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12)
			values(@AID,@date,'Credit Memo',@lineiid,@VendorID,@MEMO,@ACID,CAST(isnull(@COLUMN25,0) AS decimal(18,2)),CAST(isnull(@COLUMN25,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project)
		    END
end

end
		if(@AID>0)
			begin
			--Sales of Product Income storing each row wise at line level by srinivas 7/21/2015
			--delete from FITABLE025 where column05=@number1	 and column08=1052
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
			--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
			if(@payeetyp!=22305)
			begin
SET @ACID=(SELECT IIF(isnull((SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),0)!=0,(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1052))
					SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
					SET @AMNT = CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))+CAST(isnull(@COLUMN10,0) AS decimal(18,2)))AS decimal(18,2))
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @AMNT,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			values(@AID,@DT,'Credit Memo',@lineiid,@CUST,@memo,@ACID,CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))+CAST(isnull(@COLUMN10,0) AS decimal(18,2)))AS decimal(18,2)),@SBAL+CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project)
			end
			--Changed by Raj Patakota on 07/17/2015 to handle decimal
			if(@AID>0)
			begin
				set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
			end
		else
			begin
				set @AID=1000;
			end
			set @PRICE=(select column17 from matable007 where column02=@COLUMN03)
			--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			if(@payeetyp!=22305)
			begin
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1056))
			SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
					SET @AMNT = cast(((cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN07,0)as DECIMAL(18,2))as DECIMAL(18,2)))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2))
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @AMNT,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12,COLUMND05, COLUMND06)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			values(@AID,@DT,'Credit Memo',@lineiid,@CUST,@memo,@ACID,cast(((cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN07,0)as DECIMAL(18,2))as DECIMAL(18,2)))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@CBAL+cast(((cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN07,0)as DECIMAL(18,2))as DECIMAL(18,2)))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03,@lineiid)
			end
	end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
	--set @Tax= (select COLUMN26 from SATABLE006 where COLUMN02=@COLUMN02)
	--set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
	--set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
	--set @HeaderTax= (select COLUMN31 from SATABLE005 where COLUMN01=@COLUMN19)
 --  set @HeaderTaxAG= (select column16 from MATABLE013 where COLUMN02=@HeaderTax)
 -- set @HeaderTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@HeaderTax)
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		declare @subtotalamt decimal(18,2),@rate decimal(18,2)
		if(cast(isnull(@COLUMN32,0) as decimal(18,2))>0)
		begin
		set @rate=isnull(@COLUMN32,0)
		end
		else
		begin
		set @rate=isnull(@COLUMN09,0)
		end
    set @subtotalamt=(cast(@COLUMN07 as decimal(18,2))*(cast(@rate as decimal(18,2))))
declare @mode nvarchar(250),@TaxString nvarchar(250)=NULL,@TAXRATEM decimal(18,2)

set @Tax= (select COLUMN26 from SATABLE006 where COLUMN02=@COLUMN02)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN12,@TTYPE INT = 0,@HTTYPE INT = 0,@ReverseCharged bit,@CharAcc nvarchar(250),@CharAcc1 nvarchar(250)
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN26,0) from SATABLE006 where COLUMN02=@COLUMN02
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end

	set @headerTax= (select COLUMN31 from SATABLE005 where COLUMN01=@COLUMN19)
	select @TaxAG=column16, @TAXRATE=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	select @headerTaxAG=column16,@headerTAXRATE=COLUMN07,@HTTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@headerTax
		if(@payeetyp=22305)
begin
set @mode='INPUT '
set @TAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
set @HeaderTAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@HeaderTax)
end
else
begin
set @mode='OUTPUT '
end
if(@SONum=(select COLUMN11 from PUTABLE001 where COLUMN02=@RefID))
BEGIN
if exists(select 1 from SATABLE009 where COLUMN06=@SOID and COLUMNA03=@COLUMNA03)
begin
set @ReverseCharged=(select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@COLUMN19);
if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
BEGIN
	set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1134 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=1134 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = '1134',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1134,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num,@Project)
	set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)	
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1142 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=1142 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = '1142',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,        @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	values(ISNULL(@tmpnewID,1000),'Credit Memo',@DT,@memo,@CUST,0,@ltaxamt,  @Num,1142,@Project,@COLUMNA02, @COLUMNA03)
END
else if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TTYPE=0 OR @TTYPE=22383))
		BEGIN
		  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin
SET @CharAcc=(SELECT COLUMN02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
SET @CharAcc=(SELECT COLUMN02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
ELSE if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22344)
	BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		--if(@Amtin='22713') 
		--begin 
		--set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
		--end	
		set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
		insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
		values(@tmpnewID,@DT,'Credit Memo',@Num,@CUST,@number1,@TaxColumn17,0,@ltaxamt,@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03) 
	end
ELSE if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22402)
	BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE034)+1
		insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
		values(ISNULL(@tmpnewID,1000),'Credit Memo',@DT,@memo,@CUST,0,@ltaxamt,  @Num,@TaxColumn17,@Project,@COLUMNA02, @COLUMNA03) 
	end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0)
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@HeaderTAXRATE as decimal(18,2))/ (100 + cast(@HeaderTAXRATE as decimal(18,2))))))	
	--end	
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin

		set @CharAcc=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
	set @CharAcc=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
end
END
else
BEGIN
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @ReverseCharged=(select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@COLUMN19);
if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
BEGIN
	set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1134 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = '22383', @COLUMN11 = '1134',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1134,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num,@Project)
	set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)	
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1142 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = '22402', @COLUMN11 = '1142',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	values(ISNULL(@tmpnewID,1000),'Credit Memo',@DT,@memo,@CUST,0,@ltaxamt,  @Num,1142,@Project,@COLUMNA02, @COLUMNA03)
END
else if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TTYPE=0 OR @TTYPE=22383))
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin
	set @CharAcc = (select max(COLUMN02) from FITABLE001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
	set @CharAcc = (select max(COLUMN02) from FITABLE001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
ELSE if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22344)
	BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		--if(@Amtin='22713') 
		--begin 
		--set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
		--end	
		set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
		insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
		values(@tmpnewID,@DT,'Credit Memo',@Num,@CUST,@number1,@TaxColumn17,0,@ltaxamt,@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03) 
	end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
set @HeaderTax= (select COLUMN31 from SATABLE005 where COLUMN01=@COLUMN19)
   set @HeaderTaxAG= (select column16 from MATABLE013 where COLUMN02=@HeaderTax)
  set @HeaderTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@HeaderTax)
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0)
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@HeaderTAXRATE as decimal(18,2))/ (100 + cast(@HeaderTAXRATE as decimal(18,2))))))	
	--end	
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin
	set @CharAcc =  (select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
	set @CharAcc =  (select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
END
		  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		BEGIN
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
	--  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
		--if(@payeetyp=22305)
		--begin
		--    Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		--		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@TaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
		--end
		--else
		--begin
		--    Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		--		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@TaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
		--end
		end
		if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		BEGIN
	set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
	set @tmpnewID = ((@tmpnewID)+1)
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2)))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@HeaderTAXRATE as decimal(18,2))/ (100 + cast(@HeaderTAXRATE as decimal(18,2))))))	
	--end	
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
--if(@payeetyp=22305)
--begin
--	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
--	values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@headerTaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
--end
--else
--begin
--	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
--	values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@headerTaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
--end
		end
END		
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
declare @salesclaim bit,@actualdiscount nvarchar(250),@expecteddiscount nvarchar(250),@claimdiscount nvarchar(250),@claimtype nvarchar(250),
@claimamt nvarchar(250),@increaseamt nvarchar(250),@fixedamt decimal(18,2)
set @fixedamt=(select iif(exists(select COLUMN11 from MATABLE023 where column02=@claimtype and column05='22831'),(select isnull(COLUMN11,0) COLUMN11 from MATABLE023 where column02=@claimtype and column05='22831'),0));
set @expecteddiscount=(select COLUMN07 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN03 and COLUMN03=@CustID and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @claimtype=(select COLUMN06 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN03 and COLUMN03=@CustID and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @claimtype=(select COLUMN05 from MATABLE023 where COLUMN02=@claimtype and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @actualdiscount=(select COLUMN06 from MATABLE023 where column02=@COLUMN37 and column05='22831');
if((cast(isnull(@actualdiscount,0) as decimal(18,2))>cast(isnull(@expecteddiscount,0) as decimal(18,2)))  and cast(isnull(@actualdiscount,0) as decimal(18,2))>0)
begin
set @salesclaim=(select COLUMN04 from CONTABLE031 where COLUMN03='Sales Claim Required' and COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=isnull(@COLUMNA02,0) or COLUMNA02 is null) and  isnull(column04,0)=1 and isnull(columna13,0)=0)
if(@salesclaim=1 or @salesclaim='True')
begin
set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))-cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--IF(@claimtype='22777')
--begin
--set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))+cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--end
set @rate=(isnull(@COLUMN32,0));
set @claimamt=((cast(isnull(@COLUMN09,0) as decimal(18,2))*(cast(isnull(@claimdiscount,0) as decimal(18,2))*0.01))*cast(isnull(@COLUMN07,0) as decimal(18,2)));
if(@fixedamt>0.00)
begin
set @claimamt=((cast(isnull(@claimdiscount,0) as decimal(18,2)))*cast(isnull(@COLUMN07,0) as decimal(18,2)));
end
set @CharAcc =  (select MAX(COLUMN07) from FITABLE001 WHERE COLUMN02='1129' AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Markdown Register',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @CharAcc, @COLUMN11 = '1129',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @claimamt,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11,COLUMN13,COLUMNA02, COLUMNA03)
values(@AID,@DT,'Markdown Register',@lineiid,@CustID,@memo,'1129',0,@claimamt,@claimamt,@COLUMN03, @COLUMNA02, @COLUMNA03)

set @CharAcc =  (select MAX(COLUMN07) from FITABLE001 WHERE COLUMN02='1128' AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Sales Claim Register',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @CharAcc, @COLUMN11 = '1128',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @claimamt,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE034) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03)
values(@AID,'Sales Claim Register',@DT,@memo,@CustID,0,@claimamt,@Num,'1128',@COLUMNA02, @COLUMNA03)
end
end
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
IF(CAST(isnull(@COLUMN10,0) AS decimal(18,2))>0)
BEGIN
IF ((select COLUMN05 from MATABLE023 where COLUMN02= @COLUMN37 and COLUMNA03=@COLUMNA03) != '22831' )
begin 

set @CharAcc =  (select MAX(COLUMN07) from FITABLE001 WHERE COLUMN02='1049' AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @CharAcc, @COLUMN11 = '1049',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN10,       @COLUMN15 = 0,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
values(@tmpnewID,@DT,'Credit Memo',@Num,@CUST,@number1,1049,0,CAST(isnull(@COLUMN10,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03) 
END
END
set @ReturnValue = 1
end
else
begin
return 0
end
END

 ELSE IF @Direction = 'Select'
BEGIN
select * from SATABLE006
END 

ELSE IF @Direction = 'Update'
BEGIN
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
--if exists( SELECT 1 FROM SATABLE006 WHERE COLUMN03=@COLUMN03 and COLUMN19=@COLUMN19)
--begin
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
set @COLUMN19= (SELECT COLUMN01 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN20= (SELECT COLUMN24 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN21= (SELECT COLUMN25 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN22= (SELECT COLUMN26 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @COLUMN23= (SELECT COLUMN27 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
set @payeetyp=(select COLUMN51 from SATABLE005 where COLUMN01=@COLUMN19)
  set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= '0' ) THEN (select COLUMN24 from SATABLE005 
  where COLUMN01=@COLUMN19) else @COLUMNA02  END )
  --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @FRMID= (SELECT COLUMN03 FROM SATABLE005 WHERE COLUMN01=@COLUMN19)
if not exists( SELECT 1 FROM SATABLE006 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
	begin
	 exec [CheckDirectory] 'step1','usp_SAR_TP_SATABLE006.txt',0
	select @COLUMN02=NEXT VALUE FOR DBO.SATABLE006_SequenceNo
	insert into SATABLE006 
	(
	   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
	   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
	   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,    COLUMN32,  COLUMN36, 
	   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
	   COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, 
	   COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, 
	   COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
	)
	values
	( 
	   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
	   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
	   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN27,  @COLUMN07,  @COLUMN30,  @COLUMN31, @COLUMN32,  @COLUMN36, 
	   @COLUMNA01, @COLUMNA02, @COLUMNA03,
	   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 
	   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10,
	   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08,
	   @COLUMND09, @COLUMND10
	)
	 exec [CheckDirectory] 'step2','usp_SAR_TP_SATABLE006.txt',0
	end
	else
	begin
	UPDATE SATABLE006 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,  
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,  
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26, 
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,
   --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
   COLUMN32=@COLUMN32,    COLUMN35=@COLUMN35,    COLUMN36=@COLUMN36,    COLUMN37=@COLUMN37,    COLUMN38=@COLUMN38,
   COLUMNA01=@COLUMNA01,  
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05, 
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03, 
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08, 
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
	WHERE COLUMN02 = @COLUMN02
	end
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
set @date=(select COLUMN06 from SATABLE005 where COLUMN01=@COLUMN19);
set @number1=(select max(column01) from SATABLE005  where COLUMN01=@COLUMN19)
set @Refreceipt=(select COLUMN33 from SATABLE005 where COLUMN01=@COLUMN19);
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
set @InvoiceNo=(select COLUMN49 from SATABLE005 where COLUMN01=@COLUMN19);
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @Amtin=(select COLUMN50 from SATABLE005 where COLUMN01=@COLUMN19);
set @InvoiceId=(select COLUMN01 from SATABLE009 where COLUMN02=@InvoiceNo);
set @RefID=(select COLUMN06 from PUTABLE003 where COLUMN02=@Refreceipt);
set @PID=(select COLUMN01 from PUTABLE001 where COLUMN02=@RefID);
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
set @SONum=(select COLUMN04 from SATABLE005 where COLUMN04=(select COLUMN11 from PUTABLE001 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @SOID=(select COLUMN02 from SATABLE005 where COLUMN04=(select COLUMN11 from PUTABLE001 where COLUMN02=@RefID) and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @COLUMN17=iif(@COLUMN17='',0,isnull(@COLUMN17,0))
set @ProjectID=(select column35 from SATABLE005 where column01=@COLUMN19)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
if(isnull(@COLUMN36,0)>0)
begin
set @RefundQty=(select isnull(COLUMN13,0) from PUTABLE002 where  COLUMN02=@COLUMN36 and isnull(COLUMNA13,0)=0)
end
else
begin
set @RefundQty=isnull((select COLUMN13 from PUTABLE002 where COLUMN19 in (@PID) and COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=ISNULL(@COLUMN27,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@COLUMN17,0)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 ),0);
end
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin	
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN36,0)>0)
begin
UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where   COLUMN02=@COLUMN36 and isnull(COLUMNA13,0)=0
end
else
begin
UPDATE PUTABLE002 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where COLUMN03=@COLUMN03 and isnull(COLUMN26,0)=ISNULL(@COLUMN27,0)  and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@COLUMN17,0) and COLUMN19 in (@PID)  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
end
SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMN19 in (@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03 )
set @RRQty=(SELECT sum(isnull(COLUMN07,0)) FROM SATABLE006 WHERE COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN33=CAST(@Refreceipt AS NVARCHAR(250))) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RIQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE COLUMN19 in(@PID) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @SIQty=(SELECT sum(isnull(COLUMN08,0)) FROM PUTABLE004 WHERE COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN02=@Refreceipt) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
end
else
begin
set @TQty=(SELECT sum(isnull(COLUMN10,0)) FROM SATABLE010 WHERE COLUMN15 in(@InvoiceId) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @RRQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in(select COLUMN01 from SATABLE005 where COLUMN49=@InvoiceNo) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
set @SIQty=(@TQty)
end
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if(isnull(@COLUMN36,0)>0)
begin
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where  COLUMN36=@COLUMN36 and isnull(COLUMNA13,0)=0
end
else
begin
UPDATE SATABLE006 SET   COLUMN13=(CAST(@RefundQty AS DECIMAL(18,2))+ CAST(@COLUMN07 AS DECIMAL(18,2))) where COLUMN19=@COLUMN19 and COLUMN03=@COLUMN03  and isnull(COLUMN27,0)=ISNULL(@COLUMN27,0) and iif(COLUMN17='',0,isnull(COLUMN17,0))=isnull(@COLUMN17,0) and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03
end

if(@SIQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=109)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=108)
end
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN01=(@COLUMN19)

if(@Refreceipt!='' and isnull(@Refreceipt,0)!=0 and isnull(@Refreceipt,0)!='0')
begin
UPDATE PUTABLE003 set COLUMN18=@Result where COLUMN02=(@Refreceipt)
IF(@TQty=@RIQty)
begin
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=104)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=105)
end
end
ELSE 
BEGIN
if(@TQty=@RRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
end
else if(@RRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=102)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=103)
end
end
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(@PID)
END

if((@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0') and (@InvoiceNo='' or isnull(@InvoiceNo,0)=0 or isnull(@InvoiceNo,0)='0'))
begin
UPDATE SATABLE005 set COLUMN16=(select COLUMN04 from CONTABLE025 where COLUMN02=109) where COLUMN01=(@COLUMN19)
end
if exists(select column01 from SATABLE006 where column19=@column19  and isnull(COLUMNA13,0)=0   and COLUMNA02=@COLUMNA02  and COLUMNA03=@COLUMNA03)
begin 
set @lineiid=(select max(column01) from SATABLE006 where column02=@COLUMN02)
 --set @COLUMN04=(select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from SATABLE005 where COLUMN01=@COLUMN19);
set @CustID = (select COLUMN05 from SATABLE005 where COLUMN01=@COLUMN19)
set @memo = (select COLUMN09 from SATABLE005 where COLUMN01=@COLUMN19)
set @Num = (select COLUMN04 from SATABLE005 where COLUMN01=@COLUMN19)
set @dueDT = (select COLUMN08  from SATABLE005 where COLUMN01=@COLUMN19)
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
set @DT=(select COLUMN06 from SATABLE005 where column01=@COLUMN19)
set @chk=(select column48 from matable007 where column02=@COLUMN03)
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1052 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0) ;
set @CBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1056 AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0) ;
set @AID=(select ISNULL(MAX(COLUMN02),0) from FITABLE025);
set @PRICE=(select column17 from matable007 where column02=@COLUMN03)
set @CUST=(select column05 from SATABLE005 where column01=@COLUMN19)
--EMPHCS1308 rajasekhar reddy patakota 23/10/2015 Credit Memo Edit and Delete calcuaions
set @Project=(select column35 from SATABLE005 where column01=@COLUMN19)
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		if(cast(isnull(@COLUMN32,0) as decimal(18,2))>0)
		begin
		set @rate=isnull(@COLUMN32,0)
		end
		else
		begin
		set @rate=isnull(@COLUMN09,0)
		end
    set @subtotalamt=(cast(@COLUMN07 as decimal(18,2))*(cast(@rate as decimal(18,2))))
if(@chk=0)
	begin	
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),0)!=0,(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1053))
					SET @ACTYPE=(SELECT top 1 COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @COLUMN04, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @COLUMN25,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE025) as int)+1;
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			values(@AID,@DT,'Credit Memo',@lineiid,@CUST,@memo,@ACID,CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@SBAL+CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project)

	end
else
	begin	
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  set @lotno=(select COLUMN17 from SATABLE006 where COLUMN02=@COLUMN02)
  set @Location=(select COLUMN48 from SATABLE005 where column01=@number1)
  set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
  set @Location = (case when (cast(isnull(@COLUMN38,'') as nvarchar(250))!='' and @COLUMN38!=0) then @COLUMN38 else @Location end)
  set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
  set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN27,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0)  and COLUMN24=@COLUMN04
  AND isnull(COLUMN23,0)=isnull(@ProjectID,0));	
--EMPHCS1543 rajasekhar reddy patakota 29/01/2016 Direct Credit and Debit Memo creations for returns
	if(@Refreceipt='' or isnull(@Refreceipt,0)=0 or isnull(@Refreceipt,0)='0')
			 begin
			 --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  if exists(select COLUMN03 from FITABLE010  where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)
begin 
  set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04);
  --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  set @PRATE=(isnull(@AvgPrice,0))
  --IF(@PRATE=0)BEGIN SET @PRATE=(@AvgPrice)END
  set @Qty=cast(@COLUMN07 as decimal(18,2));
  set @Qty_On_Hand=( @Qty_On_Hand + @Qty);
  UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN08= @Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)as decimal(18,2))+(cast(@COLUMN07 as decimal(18,2))*cast(@PRATE as decimal(18,2)))),
  COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE'	 
  WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04
  set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where  COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)as decimal(18,2)));
  if(cast(@Qty_On_Hand as decimal(18,2))=0)
  begin
  UPDATE FITABLE010 SET COLUMN12=0, COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04
  end
  else
  begin
  UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)), COLUMNA11=@COLUMNA08,COLUMNA07=@COLUMNA07,COLUMNB01=@COLUMNB01,COLUMNB02=@COLUMNB02,COLUMNB03='UPDATE' WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN27 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and COLUMN24=@COLUMN04
  end
  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
end 
else
begin
set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewIID1>0)
		begin
set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
		begin
set @newIID=1000
end
SET @AvgPrice=(ISNULL(@COLUMN09,0))
SET @ASSET=(CAST(ISNULL(@COLUMN07,0)AS DECIMAL(18,2))*CAST(ISNULL(@AvgPrice,0)AS DECIMAL(18,2)))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN19,COLUMN22,COLUMN21,COLUMN13,COLUMNA02,COLUMNA03,COLUMN23,COLUMNA11,COLUMNA07,COLUMNB01,COLUMNB02,COLUMNB03,COLUMN24
)
values
(
  @newIID,@COLUMN03,@COLUMN07,@COLUMN07, @ASSET,@AvgPrice,@COLUMN27,  @lotno, @location,@COLUMN20,@COLUMNA02,@COLUMNA03,@ProjectID,@COLUMNA08,@COLUMNA07,@COLUMNB01,@COLUMNB02,'INSERT',@COLUMN04 )
END
SET @VendorID=(select COLUMN05 from SATABLE005 where COLUMN01=@number1)
SET @Memo=(select COLUMN09 from SATABLE005 where COLUMN01=@number1)
SET @date=(select COLUMN06 from SATABLE005 where COLUMN01=@number1)
set @newID=cast((select MAX(isnull(COLUMN02,999)) from PUTABLE017) as int)+1
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND isnull(COLUMN19,0)=isnull(@COLUMN27,0) AND isnull(COLUMN21,0)=isnull(@Location,0) AND isnull(COLUMN22,0)=isnull(@lotno,0) AND isnull(COLUMN23,0)=isnull(@ProjectID,0) and COLUMN24=@COLUMN04);
SET @IAmt=(cast(@COLUMN07 as decimal(18,2))*cast(@AvgPrice as decimal(18,2)))
  if(@payeetyp!='22305')
  BEGIN
  insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
   @newID,'1000',@date ,@number1,'Credit Memo', @VendorID,'Inventory Received',@Memo,@IAmt,@IAmt,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lineiid, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
END
ELSE if(@payeetyp='22305')
  BEGIN
  insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  
   @newID,'1000',@date ,@number1,'Credit Memo', @VendorID,'Inventory Received',@Memo,@COLUMN25,@COLUMN25,@COLUMNA02,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN03, @lineiid, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @date,   @COLUMN08 = @payeetyp,  @COLUMN09 = @VendorID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'Credit Memo',@date,@lineiid,@VendorID,@COLUMN25,NULL,@Num,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @date,   @COLUMN08 = @payeetyp,  @COLUMN09 = @VendorID,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN25,       @COLUMN15 = 0,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12)
			values(@AID,@date,'Credit Memo',@lineiid,@VendorID,@MEMO,@ACID,CAST(isnull(@COLUMN25,0) AS decimal(18,2)),CAST(isnull(@COLUMN25,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project)
		    END
END
end

			set @AID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE025) as int)+1;
			if(@payeetyp!=22305)
			begin
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),0)!=0,(SELECT COLUMN52 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN52!=0),1052))
			SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
					SET @AMNT = CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))+CAST(isnull(@COLUMN10,0) AS decimal(18,2)))AS decimal(18,2))
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @AMNT,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			values(@AID,@DT,'Credit Memo',@lineiid,@CUST,@memo,@ACID,CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))+CAST(isnull(@COLUMN10,0) AS decimal(18,2)))AS decimal(18,2)),@SBAL+CAST((CAST(isnull(@COLUMN25,0) AS decimal(18,2))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project)
			end
			--Changed by Raj Patakota on 07/17/2015 to handle decimal
		
			set @AID=cast((select MAX(isnull(COLUMN02,999)) from FITABLE025) as int)+1;
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			if(@payeetyp!=22305)
			begin
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN03 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1056))
			SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03)
					SET @AMNT = cast(((cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN07,0)as DECIMAL(18,2))as DECIMAL(18,2)))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2))
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @AMNT,     @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN11 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN12,COLUMND05, COLUMND06)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			values(@AID,@DT,'Credit Memo',@lineiid,@CUST,@memo,@ACID,cast(((cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN07,0)as DECIMAL(18,2))as DECIMAL(18,2)))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@CBAL+cast(((cast(cast(isnull(@AvgPrice,0) as DECIMAL(18,2))*cast(isnull(@COLUMN07,0)as DECIMAL(18,2))as DECIMAL(18,2)))-CAST(isnull(0,0) AS decimal(18,2)))AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03,@lineiid)
			end
	end
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
	--set @Tax= (select COLUMN26 from SATABLE006 where COLUMN02=@COLUMN02)
	--set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
	--set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
set @Tax= (select COLUMN26 from SATABLE006 where COLUMN02=@COLUMN02)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN17,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
 set @cnt  = 0 set @vochercnt  = 1 set @AMOUNTM =@COLUMN12;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN26,0) from SATABLE006 where COLUMN02=@COLUMN02
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end

	set @headerTax= (select COLUMN31 from SATABLE005 where COLUMN01=@COLUMN19)
	select @TaxAG=column16, @TAXRATE=COLUMN07,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	select @headerTaxAG=column16,@headerTAXRATE=COLUMN07,@HTTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@headerTax
		 if(@payeetyp=22305)
begin
set @mode='INPUT '
set @TAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
set @HeaderTAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@HeaderTax)
end
else
begin
set @mode='OUTPUT '
end
		    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		set @ReverseCharged=(select isnull(COLUMN70,0) from SATABLE005 where COLUMN01=@COLUMN19);
		if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		BEGIN
			set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
			--if(@Amtin='22713') 
			--begin 
		 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
			--end	
			set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1134 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=1134 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = '1134',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=((select isnull(max(column02),1000) from FITABLE026)+1)	
				Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
				values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1134,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Num,@Project)
			set @tmpnewID=((select isnull(max(column02),1000) from FITABLE034)+1)	
			set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1142 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			SET @ACTYPE=(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=1142 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @ACTYPE, @COLUMN11 = '1142',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,        @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
			values(ISNULL(@tmpnewID,1000),'Credit Memo',@DT,@memo,@CUST,0,@ltaxamt,  @Num,1142,@Project,@COLUMNA02, @COLUMNA03)
		END
		ELSE if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TTYPE=0 OR @TTYPE=22383))
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
  set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin
	SET @CharAcc=(SELECT COLUMN02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
	SET @CharAcc=(SELECT COLUMN02 FROM FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
					exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@Tax,@TaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
		ELSE if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22344)
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		--if(@Amtin='22713') 
		--begin 
		--set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
		--end	
		set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
		insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
		values(@tmpnewID,@DT,'Credit Memo',@Num,@CUST,@number1,@TaxColumn17,0,@ltaxamt,@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03) 
	end
		ELSE if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TTYPE=22402)
			BEGIN
		    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
				set @TaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
				set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
				exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE034)+1
				insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
				values(ISNULL(@tmpnewID,1000),'Credit Memo',@DT,@memo,@CUST,0,@ltaxamt,  @Num,@TaxColumn17,@Project,@COLUMNA02, @COLUMNA03) 
		end
		set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
		set @tmpnewID = ((@tmpnewID)+1)
		set @HeaderTax= (select COLUMN31 from SATABLE005 where COLUMN01=@COLUMN19)
		   set @HeaderTaxAG= (select column16 from MATABLE013 where COLUMN02=@HeaderTax)
		  set @HeaderTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@HeaderTax)
		if(@SONum=(select COLUMN11 from PUTABLE001 where COLUMN02=@RefID))
BEGIN
if exists(select 1 from SATABLE009 where COLUMN06=@SOID and COLUMNA03=@COLUMNA03)
begin
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0)
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@HeaderTAXRATE as decimal(18,2))/ (100 + cast(@HeaderTAXRATE as decimal(18,2))))))	
	--end	
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin
	set @CharAcc=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
	set @CharAcc=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
end
END
else
BEGIN
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0)
		BEGIN
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@HeaderTAXRATE as decimal(18,2))/ (100 + cast(@HeaderTAXRATE as decimal(18,2))))))	
	--end	
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
if(@payeetyp=22305)
begin
	set @CharAcc=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
else
begin
	set @CharAcc=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@HeaderTaxColumn17 AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @TTYPE, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN25, @COLUMN17 = @HeaderTAXRATE, @COLUMN18 = @HeaderTaxAG, @COLUMN19 = @HeaderTaxColumn17, @COLUMN20 = @COLUMN03,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,@HeaderTax,@HeaderTaxAG,@ltaxamt,@COLUMN03,@COLUMN25,@HeaderTAXRATE,@HeaderTaxColumn17,@COLUMNA02, @COLUMNA03,@Num,@Project)
end
		end
END
  --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		BEGIN
set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
set @tmpnewID = ((@tmpnewID)+1)
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@TAXRATE as decimal(18,2))/ (100 + cast(@TAXRATE as decimal(18,2))))))	
	--end	
		--if(@payeetyp=22305)
		--begin
		--    Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		--		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@TaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
		--end
		--else
		--begin
		--   Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		--		values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@TaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
		--end
		end
		if(isnull(cast(@COLUMN25 as decimal(18,2)),0)>0 and isnull(cast(@HeaderTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		BEGIN
	set @tmpnewID = (select isnull(max(COLUMN02),999)  from FITABLE026)
	set @tmpnewID = ((@tmpnewID)+1)
    set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*(cast(@HeaderTAXRATE as decimal(18,2))*0.01))
    --EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions	
	--if(@Amtin='22713') 
	--begin 
 --   set @ltaxamt=(cast(@COLUMN25 as decimal(18,2))*((cast(@HeaderTAXRATE as decimal(18,2))/ (100 + cast(@HeaderTAXRATE as decimal(18,2))))))	
	--end	
  set @HeaderTaxColumn17=(@mode+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@HeaderTaxAG)+CAST(@HeaderTAXRATE AS NVARCHAR(250))+'%')
--if(@payeetyp=22305)
--begin
--	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
--	values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@headerTaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
--end
--else
--begin
--	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN12,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
--	values(@tmpnewID,@DT,'Credit Memo',@number1,@CUST,@memo,1121,@headerTaxAG,(@ltaxamt)*(0.005),@COLUMN03,@COLUMN25,0.5,'Output SBC@0.5%',@COLUMNA02, @COLUMNA03,@Num,@Project)
--end
		end
END
end
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @expecteddiscount=(select COLUMN07 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN03 and COLUMN03=@CustID and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @fixedamt=(select iif(exists(select COLUMN11 from MATABLE023 where column02=@claimtype and column05='22831'),(select isnull(COLUMN11,0) COLUMN11 from MATABLE023 where column02=@claimtype and column05='22831'),0));
set @claimtype=(select COLUMN06 from MATABLE026 where (isnull(COLUMN04,0)=isnull(@COLUMNA02,0) or isnull(COLUMN04,0)=0) and COLUMN05=@COLUMN03 and COLUMN03=@CustID and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @claimtype=(select COLUMN05 from MATABLE023 where COLUMN02=@claimtype and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0);
set @actualdiscount=(select COLUMN06 from MATABLE023 where column02=@COLUMN37 and column05='22831');
if((cast(isnull(@actualdiscount,0) as decimal(18,2))>cast(isnull(@expecteddiscount,0) as decimal(18,2)))  and cast(isnull(@actualdiscount,0) as decimal(18,2))>0)
begin
set @salesclaim=(select COLUMN04 from CONTABLE031 where COLUMN03='Sales Claim Required' and COLUMNA03=@COLUMNA03 and  (isnull(COLUMNA02,0)=isnull(@COLUMNA02,0) or COLUMNA02 is null) and  isnull(column04,0)=1 and isnull(columna13,0)=0)
if(@salesclaim=1 or @salesclaim='True')
begin
set @AID=cast((select MAX(COLUMN02) from FITABLE025) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))-cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--IF(@claimtype='22777')
--begin
--set @claimdiscount=(cast(isnull(@actualdiscount,0) as decimal(18,2))+cast(isnull(@expecteddiscount,0) as decimal(18,2)));
--end
set @rate=(isnull(@COLUMN32,0));
set @claimamt=((cast(isnull(@COLUMN09,0) as decimal(18,2))*(cast(isnull(@claimdiscount,0) as decimal(18,2))*0.01))*cast(isnull(@COLUMN07,0) as decimal(18,2)));
if(@fixedamt>0.00)
begin
set @claimamt=((cast(isnull(@claimdiscount,0) as decimal(18,2)))*cast(isnull(@COLUMN07,0) as decimal(18,2)));
end
set @CharAcc =  (select MAX(COLUMN07) from FITABLE001 WHERE COLUMN02='1129' AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Markdown Register',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @CharAcc, @COLUMN11 = '1129',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @claimamt,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
Insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,  COLUMN11,COLUMN13,COLUMNA02, COLUMNA03)
values(@AID,@DT,'Markdown Register',@lineiid,@CustID,@memo,'1129',0,@claimamt,@claimamt,@COLUMN03, @COLUMNA02, @COLUMNA03)
set @CharAcc =  (select MAX(COLUMN07) from FITABLE001 WHERE COLUMN02='1128' AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Sales Claim Register',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @CharAcc, @COLUMN11 = '1128',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @claimamt,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=cast((select MAX(COLUMN02) from FITABLE034) as int)+1;
if(@AID is null or @AID='')set @AID=(1000)
Insert into FITABLE034(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10, COLUMNA02, COLUMNA03)
values(@AID,'Sales Claim Register',@DT,@memo,@CustID,0,@claimamt,@Num,'1128',@COLUMNA02, @COLUMNA03)
end
end
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
IF(CAST(isnull(@COLUMN10,0) AS decimal(18,2))>0)
BEGIN
IF ((select COLUMN05 from MATABLE023 where COLUMN02= @COLUMN37 and COLUMNA03=@COLUMNA03) != '22831' )
begin 
set @CharAcc =  (select MAX(COLUMN07) from FITABLE001 WHERE COLUMN02='1049' AND COLUMNA03=@COLUMNA03)
		
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Credit Memo',  @COLUMN04 = @Num, @COLUMN05 = @COLUMN19,  @COLUMN06 = @lineiid,
		@COLUMN07 = @DT,   @COLUMN08 = @payeetyp,  @COLUMN09 = @CUST,    @COLUMN10 = @CharAcc, @COLUMN11 = '1049',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @COLUMN10,       @COLUMN15 = 0,      @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @tmpnewID=(select MAX(ISNULL(COLUMN02,999)) from FITABLE036)+1
insert into FITABLE036(COLUMN02,COLUMN04,COLUMN03,COLUMN09,COLUMN06,COLUMN05,COLUMN10,COLUMN07 ,COLUMN08 ,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11,COLUMN12)
values(@tmpnewID,@DT,'Credit Memo',@Num,@CUST,@number1,1049,0,CAST(isnull(@COLUMN10,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,0,@Project,@COLUMN03) 
END
END
set @ReturnValue = 1
--end
END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE SATABLE006 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END
end try
begin catch
declare @tempSTR nvarchar(max)=''
set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_EXCEPTION_SATABLE006.txt',0

if not exists(select column01 from SATABLE006 where column19=@column19)
					begin
					delete SATABLE005 where column01=@COLUMN19
					return 0
					end
					return 0
return 0
end catch
end



















GO
