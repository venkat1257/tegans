USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetJobOrderProcessingDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetJobOrderProcessingDetails]
(
	@ReportName  nvarchar(250)= null,
	@FrDate    nvarchar(250)= null,
	@ToDate      nvarchar(250)= null,
	@Joborder      nvarchar(250)= null,
	@OPUnit nvarchar(250)= null,
	@AcOwner nvarchar(250)= null,
	@OperatingUnit      nvarchar(250)= null,
	@whereStr nvarchar(1000)=null,
	@Query1 nvarchar(max)=null,
	@DateF nvarchar(250)=null
)
AS
	BEGIN
	if @OperatingUnit!=''
	begin
	set @whereStr=' where p.column19 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
	end
	if @Joborder!=''
	begin
	if(@whereStr='' or @whereStr is null)
	begin
	set @whereStr=' where p.COLUMN06 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Joborder+''') s)'
	end
	else
	begin
	set @whereStr=@whereStr+' and p.COLUMN06 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Joborder+''') s)'
	end
	end	
	if(@whereStr='' or @whereStr is null)
	begin
	set @whereStr=' where 1=1'
	end
	set @Query1=
	'SELECT p.[COLUMN02 ] AS ID ,o.[COLUMN03 ] AS [OPUNIT],p.[COLUMN04 ] AS [Production#],format(p.COLUMN11,''' + @DateF + ''') Date,
	jo.[COLUMN04] AS [Job Order#],ji.[COLUMN04] AS [Issue#],j.[COLUMN05] AS [Jobber],cs.[COLUMN04] AS [Current Status],
	st.[COLUMN04] AS [Submitted To],m.COLUMN04 [Item],l.COLUMN05 [Desc],(isnull(l.COLUMN06,0)) [ExpQty],
	(isnull(l.COLUMN07,0)) [ActQty],l.COLUMN08 [Remarks],eb.COLUMN09 Executed,ht.COLUMN09 Handedover from SATABLE019 p 
	inner join SATABLE020 l on l.COLUMN09=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0 and (l.COLUMNA02=p.COLUMNA02 or l.COLUMNA02 is null) 
	left join SATABLE005 jo on jo.COLUMN02=p.column06 and jo.COLUMNA03=p.COLUMNA03 and isnull(jo.COLUMNA13,0)=0 and (jo.COLUMNA02=p.COLUMNA02 or jo.COLUMNA02 is null) 
	left join SATABLE007 ji on ji.COLUMN02=p.column05 and ji.COLUMNA03=p.COLUMNA03 and isnull(ji.COLUMNA13,0)=0 and (ji.COLUMNA02=p.COLUMNA02 or ji.COLUMNA02 is null) 
	left join SATABLE001 j on j.COLUMN02=p.COLUMN07 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and (j.COLUMNA02=p.COLUMNA02 or j.COLUMNA02 is null) 
	left join MATABLE030 cs on cs.COLUMN02=l.COLUMN11 and cs.COLUMNA03=l.COLUMNA03 and isnull(cs.COLUMNA13,0)=0 and (cs.COLUMNA02=l.COLUMNA02 or cs.COLUMNA02 is null) 
	left join MATABLE030 st on st.COLUMN02=l.COLUMN12 and st.COLUMNA03=l.COLUMNA03 and isnull(st.COLUMNA13,0)=0 and (st.COLUMNA02=l.COLUMNA02 or st.COLUMNA02 is null) 
	left join MATABLE007 m on m.COLUMN02=l.column04 and m.COLUMNA03=l.COLUMNA03 and isnull(m.COLUMNA13,0)=0 and (m.COLUMNA02=l.COLUMNA02 or m.COLUMNA02 is null)
	left join MATABLE010 eb on eb.COLUMN02=p.COLUMN15 and eb.COLUMNA03=p.COLUMNA03 and isnull(eb.COLUMNA13,0)=0 and (eb.COLUMNA02=p.COLUMNA02 or eb.COLUMNA02 is null) 
	left join MATABLE010 ht on ht.COLUMN02=p.COLUMN16 and ht.COLUMNA03=p.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 and (ht.COLUMNA02=p.COLUMNA02 or ht.COLUMNA02 is null) 
	left join CONTABLE007 o on o.COLUMN02=p.column19 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 and (o.COLUMNA02=p.COLUMNA02 or o.COLUMNA02 is null)
	'+ @whereStr +' and isnull(p.COLUMNA13,0)=0 and p.COLUMNA03=''' + @AcOwner + ''' AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND  p.COLUMN03=1626 
	--group by  p.COLUMN02,p.COLUMN04,p.COLUMN11,jo.COLUMN04,ji.COLUMN04,j.COLUMN05,cs.COLUMN04,st.COLUMN04,m.COLUMN04,
	--l.COLUMN05,l.COLUMN08,o.COLUMN03 
	 order by  convert(datetime,p.COLUMN11,103) desc '
exec (@Query1) 
END
GO
