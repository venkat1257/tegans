USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetReports]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetReports]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(max)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@DateF nvarchar(250)=null)
as 
begin
---EMPHCS907	 After deleting invoice - Taxes are not cleared  By Raj.Jr 11/8/2015
IF  @ReportName='TransactionReport'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Type!='')
begin
select  max(COLUMN06) COLUMN06,max(COLUMN07) COLUMN07,max(COLUMN13) COLUMN13,max(COLUMN14) COLUMN14,COLUMN03,isnull(sum(COLUMN08),0) as COLUMN08,isnull(sum(COLUMN09),0) as COLUMN09,
                isnull(sum(COLUMN10),0) as COLUMN10,isnull(sum(COLUMN11),0) as COLUMN11,isnull(sum(COLUMN12),0) as COLUMN12,
                isnull(sum(COLUMN21),0) as COLUMN21,isnull(sum(COLUMN22),0) as COLUMN22,isnull(sum(COLUMN23),0) 
                as COLUMN23 from putable013 where isnull((COLUMNA13),0)=0 and COLUMN15 between @FromDate and @ToDate and COLUMN03=@Type AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner group by column03
end
else if(@FromDate!='' and @ToDate!='')
begin
select  max(COLUMN06) COLUMN06,max(COLUMN07) COLUMN07,max(COLUMN13) COLUMN13,max(COLUMN14) COLUMN14,COLUMN03,isnull(sum(COLUMN08),0) as COLUMN08,isnull(sum(COLUMN09),0) as COLUMN09,
                isnull(sum(COLUMN10),0) as COLUMN10,isnull(sum(COLUMN11),0) as COLUMN11,isnull(sum(COLUMN12),0) as COLUMN12,
                isnull(sum(COLUMN21),0) as COLUMN21,isnull(sum(COLUMN22),0) as COLUMN22,isnull(sum(COLUMN23),0) 
                as COLUMN23 from putable013 where isnull((COLUMNA13),0)=0 and COLUMN15 between @FromDate and @ToDate  AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner group by column03
end
else if( @Type!='')
begin
select  max(COLUMN06) COLUMN06,max(COLUMN07) COLUMN07,max(COLUMN13) COLUMN13,max(COLUMN14) COLUMN14,COLUMN03,isnull(sum(COLUMN08),0) as COLUMN08,isnull(sum(COLUMN09),0) as COLUMN09,
                isnull(sum(COLUMN10),0) as COLUMN10,isnull(sum(COLUMN11),0) as COLUMN11,isnull(sum(COLUMN12),0) as COLUMN12,
                isnull(sum(COLUMN21),0) as COLUMN21,isnull(sum(COLUMN22),0) as COLUMN22,isnull(sum(COLUMN23),0) 
                as COLUMN23 from putable013 where isnull((COLUMNA13),0)=0 and COLUMN03=@Type AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner group by column03
end
END

else IF  @ReportName='JobberPaymentDues'
BEGIN
if(@Jobber='' and @OperatingUnit='')
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, p.COLUMN19 date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%' and c.COLUMN05 > 0  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end

else if(@Jobber!='' and @OperatingUnit!='')
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, p.COLUMN19 date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%' and c.COLUMN05 > 0 and p.COLUMN14 = @OperatingUnit and p.COLUMN05=@Jobber AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end
else if(@Jobber!='' )
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, p.COLUMN19 date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%' and c.COLUMN05 > 0 and  p.COLUMN05=@Jobber AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end
else if(  @OperatingUnit!='')
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, p.COLUMN19 date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%' and c.COLUMN05 > 0 and p.COLUMN14 = @OperatingUnit  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end
END

else IF  @ReportName='StockOfJobber'
BEGIN
if(@Jobber='' and @OperatingUnit='')
begin
SELECT  (SELECT COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = p.COLUMN24)) AS operatingunit,(SELECT  COLUMN05 FROM SATABLE001 WHERE  (COLUMN02 = p.COLUMN05)) AS jobber, ad.COLUMN06 AS addr,(SELECT  COLUMN04  FROM  MATABLE007 WHERE  (COLUMN02 = f.COLUMN03)) AS item, f.COLUMN07, f.COLUMN11 AS total, p.COLUMN04 AS joid, p.COLUMN06 AS date, f.COLUMN09 AS price FROM  SATABLE005 AS p INNER JOIN SATABLE006 AS f ON f.COLUMN19 = p.COLUMN01 INNER JOIN SATABLE003 AS ad ON ad.COLUMN02 = p.COLUMN05 WHERE isnull((p.COLUMNA13),0)=0 and  (p.COLUMN04 LIKE 'JO%') AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end

else if(@Jobber!='' and @OperatingUnit!='')
begin
SELECT  (SELECT COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = p.COLUMN24)) AS operatingunit,(SELECT  COLUMN05 FROM SATABLE001 WHERE  (COLUMN02 = p.COLUMN05)) AS jobber, ad.COLUMN06 AS addr,(SELECT  COLUMN04  FROM  MATABLE007 WHERE  (COLUMN02 = f.COLUMN03)) AS item, f.COLUMN07, f.COLUMN11 AS total, p.COLUMN04 AS joid, p.COLUMN06 AS date, f.COLUMN09 AS price FROM  SATABLE005 AS p INNER JOIN SATABLE006 AS f ON f.COLUMN19 = p.COLUMN01 INNER JOIN SATABLE003 AS ad ON ad.COLUMN02 = p.COLUMN05 WHERE  isnull((p.COLUMNA13),0)=0 and (p.COLUMN04 LIKE 'JO%') and p.COLUMN24 = @OperatingUnit and p.COLUMN05=@Jobber AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@Jobber!='' )
begin
SELECT  (SELECT COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = p.COLUMN24)) AS operatingunit,(SELECT  COLUMN05 FROM SATABLE001 WHERE  (COLUMN02 = p.COLUMN05)) AS jobber, ad.COLUMN06 AS addr,(SELECT  COLUMN04  FROM  MATABLE007 WHERE  (COLUMN02 = f.COLUMN03)) AS item, f.COLUMN07, f.COLUMN11 AS total, p.COLUMN04 AS joid, p.COLUMN06 AS date, f.COLUMN09 AS price FROM  SATABLE005 AS p INNER JOIN SATABLE006 AS f ON f.COLUMN19 = p.COLUMN01 INNER JOIN SATABLE003 AS ad ON ad.COLUMN02 = p.COLUMN05 WHERE isnull((p.COLUMNA13),0)=0 and  (p.COLUMN04 LIKE 'JO%') and  p.COLUMN05=@Jobber AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(  @OperatingUnit!='')
begin
SELECT  (SELECT COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = p.COLUMN24)) AS operatingunit,(SELECT  COLUMN05 FROM SATABLE001 WHERE  (COLUMN02 = p.COLUMN05)) AS jobber, ad.COLUMN06 AS addr,(SELECT  COLUMN04  FROM  MATABLE007 WHERE  (COLUMN02 = f.COLUMN03)) AS item, f.COLUMN07, f.COLUMN11 AS total, p.COLUMN04 AS joid, p.COLUMN06 AS date, f.COLUMN09 AS price FROM  SATABLE005 AS p INNER JOIN SATABLE006 AS f ON f.COLUMN19 = p.COLUMN01 INNER JOIN SATABLE003 AS ad ON ad.COLUMN02 = p.COLUMN05 WHERE  isnull((p.COLUMNA13),0)=0 and (p.COLUMN04 LIKE 'JO%') and p.COLUMN24 = @OperatingUnit  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
END



else IF  @ReportName='InventoryByLoc'
BEGIN
if(@Item='' and @OperatingUnit='')
begin
SELECT (SELECT  COLUMN03 FROM    CONTABLE007 WHERE        (COLUMN02 = p.COLUMN13)) AS operatingunit,(SELECT  COLUMN04 FROM  MATABLE007 WHERE    (COLUMN02 = p.COLUMN03)) AS column03, COLUMN04, COLUMN06, COLUMN05, COLUMN18, COLUMN12, COLUMN17 FROM   FITABLE010 AS p WHERE isnull((p.COLUMNA13),0)=0 and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
	end

else if(@Item!='' and @OperatingUnit!='')
begin
SELECT (SELECT  COLUMN03 FROM    CONTABLE007 WHERE        (COLUMN02 = p.COLUMN13)) AS operatingunit,(SELECT  COLUMN04 FROM  MATABLE007 WHERE    (COLUMN02 = p.COLUMN03)) AS column03, COLUMN04, COLUMN06, COLUMN05, COLUMN18, COLUMN12, COLUMN17 FROM   FITABLE010 AS p where isnull((p.COLUMNA13),0)=0 and  p.COLUMN13 = @OperatingUnit and p.COLUMN03=@Item  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
	end
else if(@Item!='' )
begin
SELECT (SELECT  COLUMN03 FROM    CONTABLE007 WHERE        (COLUMN02 = p.COLUMN13)) AS operatingunit,(SELECT  COLUMN04 FROM  MATABLE007 WHERE    (COLUMN02 = p.COLUMN03)) AS column03, COLUMN04, COLUMN06, COLUMN05, COLUMN18, COLUMN12, COLUMN17 FROM   FITABLE010 AS p where isnull((p.COLUMNA13),0)=0 and  p.COLUMN03=@Item AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
	end
	else if(  @OperatingUnit!='')
begin
   select c7.COLUMN03 AS operatingunit, m7.COLUMN04  AS column03,
   f10.COLUMN04, f10.COLUMN06, f10.COLUMN05, f10.COLUMN18, f10.COLUMN12, f10.COLUMN17
     FROM   FITABLE010 f10 inner join  MATABLE007 m7 on f10.COLUMN03=m7.COLUMN02
    left outer join  CONTABLE007 c7 on c7.COLUMN02=f10.COLUMN13 
	 
     where f10.COLUMN13 = @OperatingUnit  AND f10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND f10.COLUMNA03=@AcOwner and m7.COLUMNA13=0
	end
END

ELSE IF @ReportName='InventoryByItem'
BEGIN

(select (select COLUMN03 from CONTABLE007 where COLUMN02=p1.COLUMN24) ou,f.COLUMN04 item,f.COLUMN06 upc,
p1.COLUMN04 tno,p1.COLUMN06 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=p1.COLUMN05) vendor ,
0 openbal,p2.COLUMN07 qr,0 qi,p2.COLUMN09 price,p2.COLUMN24 amt
from PUTABLE001 p1 inner join PUTABLE002 p2 on p2.COLUMN19=p1.COLUMN01
inner join MATABLE007 f on f.COLUMN02=p2.COLUMN03 and f.column48=1 where isnull((p1.COLUMNA13),0)=0 
AND p1.COLUMN06 BETWEEN @FrDate AND @ToDate  AND p1.COLUMNA02=@OperatingUnit and
 p1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p1.COLUMNA03=@AcOwner )

 union

(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and f.column48=1 where isnull((b.COLUMNA13),0)=0  
AND b.COLUMN09 BETWEEN @FrDate AND @ToDate  AND b.COLUMNA02=@OperatingUnit and
 b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner )

 union

 (select (select COLUMN03 from CONTABLE007 where COLUMN02=p5.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
p5.COLUMN04 tno,p5.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=p5.COLUMN05) vendor ,
0 openbal,p6.COLUMN09 qr,0 qi,p6.COLUMN11 price,p6.COLUMN12 amt
from PUTABLE005 p5 inner join PUTABLE006 p6 on p6.COLUMN13=p5.COLUMN01
inner join MATABLE007 f on f.COLUMN02=p6.COLUMN04 and f.column48=1 where isnull((p5.COLUMNA13),0)=0 and  p5.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 

AND p5.COLUMN08 BETWEEN @FrDate AND @ToDate  AND p5.COLUMNA02=@OperatingUnit  AND  p5.COLUMNA03=@AcOwner )

 union
(select (select COLUMN03 from CONTABLE007 where COLUMN02=s5.COLUMN13) ou,g.COLUMN04 item,g.COLUMN06 upc,
s5.COLUMN04 tno,s5.COLUMN06 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = s5.COLUMN05 ) vendor,
0 openbal, 0 qr,s6.COLUMN09 qi ,s6.COLUMN12 price,s6.COLUMN13 amt
from SATABLE005 s5 inner join SATABLE006 s6 on s6.COLUMN19=s5.COLUMN01
inner join MATABLE007 g on g.COLUMN02=s6.COLUMN03 and g.column48=1 where isnull((s5.COLUMNA13),0)=0 and  s5.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND
 s5.COLUMN09 BETWEEN @FrDate AND @ToDate  AND s5.COLUMNA02=@OperatingUnit   and  s5.COLUMNA03=@AcOwner)
 
 union

(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and g.column48=1 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND
 a.COLUMN09 BETWEEN @FrDate AND @ToDate  AND a.COLUMNA02=@OperatingUnit   and  a.COLUMNA03=@AcOwner)
 union

 (select (select COLUMN03 from CONTABLE007 where COLUMN02=s9.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
s9.COLUMN04 tno,s9.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = s9.COLUMN05 ) vendor,
0 openbal, 0 qr,s10.COLUMN10 qi ,s10.COLUMN13 price,s10.COLUMN14 amt
from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01
inner join MATABLE007 g on g.COLUMN02=s10.COLUMN05 and g.column48=1 where isnull((s9.COLUMNA13),0)=0 and  s9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND 
 s9.COLUMN08 BETWEEN @FrDate AND @ToDate  AND s9.COLUMNA02=@OperatingUnit and s9.COLUMNA03=@AcOwner)
 union

(select (select COLUMN03 from CONTABLE007 where COLUMN02 = max(g.COLUMN37)) ou,g.COLUMN04 item,g.COLUMN06 upc,
 null tno,null tdate, null vendor,
(sum(h.COLUMN08)-sum(d.COLUMN09)) openbal,0 qr,0 qi ,0 price,(sum(cast(h.COLUMN11 as decimal))-sum(d.COLUMN13)) amt
from SATABLE008 d  inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and g.column48=1
inner join PUTABLE004 h on h.COLUMN03=g.COLUMN02 where isnull((d.COLUMNA13),0)=0 and  d.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  d.COLUMNA03=@AcOwner group by g.COLUMN04,g.COLUMN06 ) 

END

else IF  @ReportName='JobberPayment'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Jobber!='' and @OperatingUnit!='')
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, FORMAT( p.COLUMN19, @DateF) date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%'and p.COLUMN19 between @FromDate and @ToDate and p.COLUMN05=@Jobber and  p.COLUMN14=@OperatingUnit AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @Jobber!='' )
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, FORMAT( p.COLUMN19, @DateF) date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%'and p.COLUMN19 between @FromDate and @ToDate and p.COLUMN05 =@Jobber AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end
	else if(@FromDate!='' and @ToDate!=''  and @OperatingUnit!='')
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, FORMAT( p.COLUMN19, @DateF) date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%'and p.COLUMN19 between @FromDate and @ToDate  and p.COLUMN14=@OperatingUnit AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner
order by p.COLUMN19 desc
end
	else if(@FromDate!='' and @ToDate!='')
begin
select (select column03 from CONTABLE007 where COLUMN02 = p.COLUMN14) operatingunit , (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN05 ) jobber , p.COLUMN04 jooberid , c.COLUMN04 total,c.COLUMN06 payment,c.COLUMN05 due, FORMAT( p.COLUMN19, @DateF) date from SATABLE011 p join SATABLE012 c on p.COLUMN01=c.COLUMN08      where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 like'JP%'and p.COLUMN19 between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND p.COLUMNA03=@AcOwner 
order by p.COLUMN19 desc
end
END



ELSE IF  @ReportName='PayableReport'
BEGIN
--EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
if((@FromDate!='' and @ToDate!='' and @Type!='' and @Vendor!='' and @OperatingUnit!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
GROUP BY putable016.COLUMN07
union all
select  COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02 ) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end

else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Vendor!='' and @OperatingUnit!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
GROUP BY putable016.COLUMN07
union all
select  COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02 ) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
--else if((@FromDate!='' and @ToDate!='' and @Type!=''and @OperatingUnit!=''   and @Project!=''  ))
--begin
----EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
--select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
--NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,NULL COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18 from putable016
--where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
--and COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--union all
--select  COLUMN02,
--(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,cast(COLUMN04 as date) as COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18 from putable016
--where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner and COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--order by COLUMN02 desc
--end
else if((@FromDate!='' and @ToDate!='' and @Type!=''and @OperatingUnit!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
GROUP BY putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner   group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
--EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
else if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!=''   and @Project!=''  ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
GROUP BY putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
GROUP BY putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
--EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!=''  and @Project!='' )
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=@OPUnit ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07)  COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
GROUP BY putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner   group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' )
begin
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
;With MyTable AS
(
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner 

union all
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=@OperatingUnit ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
GROUP BY putable016.COLUMN07
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''  and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) group by putable016.COLUMNA02
, putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02    AND  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner group by putable016.COLUMNA02
, putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate  AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02   AND  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s))  AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  group by putable016.COLUMNA02
, putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) 
 AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
 union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate --and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate AND  COLUMNA03=@AcOwner group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02   AND  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s))  AND  COLUMNA03=@AcOwner  group by putable016.COLUMNA02
, putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate and  (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) 
 AND  COLUMNA03=@AcOwner
 union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
(select column04 from FITABLE001 where COLUMN02=fitable020.COLUMN06 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,
COLUMN04 COLUMN09,'ADVANCE PAYMENT' COLUMN06,FORMAT(COLUMN05, @DateF) as COLUMN11 ,
(case when FITABLE020.COLUMN11=22492 then (select column06 from MATABLE010 where COLUMN02=fitable020.COLUMN07)  
when FITABLE020.COLUMN11=22305 or FITABLE020.COLUMN11=22334 then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) 
 when FITABLE020.COLUMN11=22335  then (select column05 from SATABLE002 where COLUMN02=fitable020.COLUMN07) end) 
 COLUMN07,COLUMN16 COLUMN10, 0 COLUMN12,COLUMN18 COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,COLUMN08 COLUMN18,COLUMN05 Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05 between @FromDate and @ToDate --and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate AND  COLUMNA03=@AcOwner group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if(@FromDate!='' and @ToDate!='' and @Project!='')
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02 AND COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  group by putable016.COLUMNA02
, putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04 , @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  AND  COLUMN20 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate AND  COLUMNA03=@AcOwner  group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if(@FromDate!='' and @ToDate!='')
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select  NULL COLUMN02,(select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable016.COLUMNA02 AND COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,(select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) COLUMN07,NULL COLUMN10,isnull(sum(COLUMN12),0) COLUMN12,isnull(sum(COLUMN13),0) COLUMN13,isnull(sum(COLUMN12)-sum(COLUMN13),0) COLUMN14,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  group by putable016.COLUMNA02
, putable016.COLUMN07
union all
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
union all
select NULL COLUMN02,(select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as OperatingUnit,
NULL COLUMN08,NULL COLUMN09,NULL COLUMN06,NULL COLUMN11 ,
(select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) COLUMN07,NULL COLUMN10, 0 COLUMN12,sum(COLUMN18) COLUMN13,0 COLUMN14,NULL COLUMN15,NULL COLUMN16,NULL COLUMN17,'Opening Balance' COLUMN18,NULL Dtt from fitable020
where COLUMN03=1363 and isnull((COLUMNA13),0)=0 and COLUMN05<@FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  group by  fitable020.COLUMNA02,fitable020.COLUMN07
)
Select *
From MyTable
Order by Dtt desc
end
else if(@Type!='' and @Vendor!='')
begin
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT( COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) AND  COLUMNA03=@AcOwner
order by Dtt desc
end
else if(@Type!='' and @OperatingUnit!='')
begin
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT( COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner order by Dtt desc
end
else if(@OperatingUnit!='' and @Vendor!='')
begin
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT( COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) AND  COLUMNA03=@AcOwner order by Dtt desc
end
else if(@Type!='' or @Vendor!='' or @OperatingUnit!='')
begin
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT(COLUMN04 , @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN04 between @FromDate and @ToDate or (COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)) or COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) or COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)  or COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner order by Dtt desc
end
else if(@Type!='' and @Vendor!='' and @OperatingUnit!='')
begin
select  COLUMN02,
(select COLUMN03 from CONTABLE007 where COLUMN02=putable016.COLUMNA02) as OperatingUnit,(select column04 from FITABLE001 where COLUMN02=putable016.COLUMN08 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) COLUMN08,COLUMN09,COLUMN06,FORMAT( COLUMN04, @DateF) as COLUMN11 ,(case when putable016.COLUMN06='EXPENSE' then (select column06 from MATABLE010 where COLUMN02=putable016.COLUMN07) else (select column05 from SATABLE001 where COLUMN02=putable016.COLUMN07) end) COLUMN07,COLUMN10,COLUMN12,COLUMN13,COLUMN14,(select column05 from SATABLE002 where COLUMN02=putable016.COLUMN15) COLUMN15,(select column06 from MATABLE010 where COLUMN02=putable016.COLUMN16) COLUMN16,COLUMN17,COLUMN18,COLUMN04 Dtt from putable016
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by Dtt desc
end
end


ELSE IF  @ReportName='ReceivableReport'
BEGIN
if((@FromDate!='' and @ToDate!='' and @Type!='' and @Customer!='' and @OperatingUnit!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
group by putable018.COLUMN07
 union all
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
--select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE025.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN04 COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE025.COLUMN06) COLUMN07,COLUMN03 COLUMN04, NULL COLUMN05,NULL COLUMN09,COLUMN09 COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08 from FITABLE025 
--where isnull((COLUMNA13),0)=0 and   COLUMN03 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMN12 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
--union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02

, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end

else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Customer!='' and @OperatingUnit!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
group by putable018.COLUMN07
 union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036  
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group  by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)     group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
union all
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
select COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end)  COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0   and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0   and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)     group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt  desc
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @OperatingUnit!='' and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
group by putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)   and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt  desc
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @OperatingUnit!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
group by putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)     group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0   and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0   and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)   and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)     group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''  and @Project!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 <@FromDate  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
group by putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where isnull((COLUMNA13),0)=0 and  COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where  isnull((COLUMNA13),0)=0 AND column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
group by putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where isnull((COLUMNA13),0)=0 and  COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where   isnull((COLUMNA13),0)=0 AND column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0   and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)     group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
order by Dtt  desc
end

else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' and @Project!='' )
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
group by putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)and   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt  desc
end

else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and COLUMN04 <@FromDate   
and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
group by putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner)and isnull(COLUMN10,0)=1049 group by FITABLE036.COLUMNA02,COLUMN09,COLUMN06,COLUMN04
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0   and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN04 < @FromDate and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)     group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt  desc
end

else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @Project!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN04 <@FromDate AND  COLUMNA03=@AcOwner and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) GROUP BY putable018.COLUMNA02
, putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' ))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and  COLUMN04 <@FromDate AND  COLUMNA03=@AcOwner GROUP BY putable018.COLUMNA02
, putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner  and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate     group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt  desc
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @Project!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  COLUMN04 <@FromDate and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) GROUP BY putable018.COLUMNA02
, putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  and COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner  and COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  AND  COLUMN11 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  AND  COLUMN18 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02, FITABLE036.COLUMN06

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s) and COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end

else if((@FromDate!='' and @ToDate!=''  and @Customer!=''))
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner ) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and  COLUMN04 <@FromDate and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   
AND  COLUMNA03=@AcOwner GROUP BY putable018.COLUMNA02
, putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0   and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner and COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)     group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end

else if(@Type!='' and @Customer!='' and @OperatingUnit!='')
begin
;With MyTable AS
(
select NULL COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07, COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'ADVANCE PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,FORMAT(COLUMN05,@DateF) COLUMN04, COLUMN04 COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,COLUMN11 COLUMN08,COLUMN05 Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05 between  @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   group by FITABLE023.COLUMNA02,COLUMN08,COLUMN05,COLUMN04,COLUMN11,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt  desc
end

else if(@FromDate!='' and @ToDate!='')
begin
;With MyTable AS
(
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
select NULL COLUMN02,  (select distinct(COLUMN03) from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,
NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,NULL COLUMN04 ,NULL COLUMN05,isnull(sum(COLUMN09),0) COLUMN09,isnull(sum(COLUMN11),0) COLUMN11,isnull(sum(COLUMN09)-sum(COLUMN11),0) COLUMN12,
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from putable018 where isnull((COLUMNA13),0)=0 and  COLUMN04 <@FromDate AND  COLUMNA03=@AcOwner   group by putable018.COLUMNA02
, putable018.COLUMN07
 --EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select COLUMN02, (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04,(case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0  and   COLUMN04 < @FromDate  AND  COLUMNA03=@AcOwner and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0  and COLUMN04 < @FromDate AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02
, FITABLE036.COLUMN06

union all
select NULL COLUMN02,  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,NULL COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) COLUMN07,NULL COLUMN04, NULL COLUMN05,NULL COLUMN09,sum(cast(isnull(COLUMN19,0)as decimal(18,2))) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Opening Balance' COLUMN08,NULL Dtt from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN05< @FromDate AND  COLUMNA03=@AcOwner    group by FITABLE023.COLUMNA02,FITABLE023.COLUMN08
)
Select *
From MyTable
Order by Dtt desc
end
else if(@Type!='' and @Customer!='' )
begin
;With MyTable AS
(
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
select   (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT'  and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   AND  COLUMNA03=@AcOwner
)
Select *
From MyTable
Order by Dtt  desc
end

else if(@Type!='' and @OperatingUnit!='' )
begin
;With MyTable AS
(
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
select   (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all 
select  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
)
Select *
From MyTable
Order by Dtt  desc
end

else if(@Customer!='' and @OperatingUnit!='' )
begin
;With MyTable AS
(
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
select  (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
AND  COLUMNA03=@AcOwner
)
Select *
From MyTable
Order by Dtt  desc
end

else if(@Type!='' or @Customer!='' or @OperatingUnit!='')
begin
;With MyTable AS
(
--EMPHCS974 Account receivable report is giving compilation error done by Srinivas 28/8/2015
select   (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE036.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,'PAYMENT' COLUMN06,NULL COLUMN16,(select column05 from SATABLE002 where COLUMN02=FITABLE036.COLUMN06) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, COLUMN09 COLUMN05,NULL COLUMN09,SUM(COLUMN07) COLUMN11,NULL COLUMN12,NULL COLUMN14,NULL COLUMN15,'Discount' COLUMN08,COLUMN04 Dtt from FITABLE036 
where isnull((COLUMNA13),0)=0 and   COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and COLUMN03='PAYMENT' and COLUMN06 in 
(select COLUMN07 from PUTABLE018 where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) or column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  or COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner) and isnull(COLUMN10,0)=1049  group by FITABLE036.COLUMNA02,COLUMN06,COLUMN04,COLUMN09
--EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
union all
select  (select COLUMN03 from CONTABLE007 where COLUMN02=putable018.COLUMNA02) as OperatingUnit,COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN07,FORMAT(COLUMN04,@DateF) COLUMN04, (case when COLUMN06='JOURNAL ENTRY' then (select COLUMN04 from FITABLE031 where COLUMN01=putable018.COLUMN05) else COLUMN05 end) COLUMN05,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08,COLUMN04 Dtt from putable018 
where isnull((COLUMNA13),0)=0 and  COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) or column07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  or COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) AND  COLUMNA03=@AcOwner
)
Select *
From MyTable
Order by Dtt  desc
end
END

ELSE IF  @ReportName='PaymentReport'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Type!='')
begin
 SELECT b.COLUMN04, a.COLUMN06,(select column05 from SATABLE001 where COLUMN02=a.COLUMN07) COLUMN07, a.COLUMN10, a.COLUMN11, a.COLUMN12  FROM dbo.PUTABLE019 a  inner JOIN FITABLE001  b ON 
 b.COLUMN02 = a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN04 between  @FromDate and @ToDate and a.COLUMN06=@Type AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner
order by a.COLUMN04 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
 SELECT b.COLUMN04, a.COLUMN06,(select column05 from SATABLE001 where COLUMN02=a.COLUMN07) COLUMN07, a.COLUMN10, a.COLUMN11, a.COLUMN12  FROM dbo.PUTABLE019 a  inner JOIN FITABLE001  b ON 
 b.COLUMN02 = a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN04 between  @FromDate and @ToDate AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner
order by a.COLUMN04 desc
end
else if( @Type!='')
begin
 SELECT b.COLUMN04, a.COLUMN06,(select column05 from SATABLE001 where COLUMN02=a.COLUMN07) COLUMN07, a.COLUMN10, a.COLUMN11, a.COLUMN12 FROM dbo.PUTABLE019 a  inner JOIN FITABLE001  b ON 
 b.COLUMN02 = a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN06=@Type AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner
order by a.COLUMN04 desc
end
END


ELSE IF  @ReportName='POSummary'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Vendor!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,a.COLUMN04 pono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT( a.COLUMN06, @DateF) dt ,a.COLUMN03 fid  FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Vendor  AND a.COLUMN24= @OperatingUnit AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,a.COLUMN04 pono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT( a.COLUMN06, @DateF) dt ,a.COLUMN03 fid  FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Vendor AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner 
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,a.COLUMN04 pono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT( a.COLUMN06, @DateF) dt ,a.COLUMN03 fid  FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Vendor!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,a.COLUMN04 pono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT( a.COLUMN06, @DateF) dt ,a.COLUMN03 fid  FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN05= @Vendor AND a.COLUMN24= @OperatingUnit AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Vendor!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,a.COLUMN04 pono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT( a.COLUMN06, @DateF) dt  ,a.COLUMN03 fid  FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN05= @Vendor or a.COLUMN24= @OperatingUnit AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,a.COLUMN04 pono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT( a.COLUMN06, @DateF) dt  ,a.COLUMN03 fid FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
end



ELSE IF  @ReportName='PendingBills'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Status!='' and @BillNO!='' and @Vendor!='')
begin
SELECT p.COLUMN04 as COLUMN04, p.COLUMN08, f.column05 as COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN12, p.COLUMN14, p.COLUMN13 FROM PUTABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05  WHERE(p.COLUMN13 <> 'CLOSE') AND (p.COLUMN13 <> 'FULLY PAID') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN08 between @FromDate and @ToDate and p.COLUMN05= @Vendor and p.COLUMN02=@BillNO and p.COLUMN13=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!=''  and @BillNO!='' and @Vendor!='')
begin
SELECT p.COLUMN04 as COLUMN04, p.COLUMN08, f.column05 as COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN12, p.COLUMN14, p.COLUMN13 FROM PUTABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05  WHERE(p.COLUMN13 <> 'CLOSE') AND (p.COLUMN13 <> 'FULLY PAID') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN08 between @FromDate and @ToDate and p.COLUMN05= @Vendor and p.COLUMN02=@BillNO  AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @Status!='' )
begin
SELECT p.COLUMN04 as COLUMN04, p.COLUMN08, f.column05 as COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN12, p.COLUMN14, p.COLUMN13 FROM PUTABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05  WHERE(p.COLUMN13 <> 'CLOSE') AND (p.COLUMN13 <> 'FULLY PAID') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN08 between @FromDate and @ToDate and  p.COLUMN13=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='')
begin
SELECT p.COLUMN04 as COLUMN04, p.COLUMN08, f.column05 as COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN12, p.COLUMN14, p.COLUMN13 FROM PUTABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05  WHERE(p.COLUMN13 <> 'CLOSE') AND (p.COLUMN13 <> 'FULLY PAID') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN08 between @FromDate and @ToDate and p.COLUMN05= @Vendor AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!=''   and @BillNO!=''  )
begin
SELECT p.COLUMN04 as COLUMN04, p.COLUMN08, f.column05 as COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN12, p.COLUMN14, p.COLUMN13 FROM PUTABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05  WHERE(p.COLUMN13 <> 'CLOSE') AND (p.COLUMN13 <> 'FULLY PAID') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN08 between @FromDate and @ToDate  and p.COLUMN02=@BillNO  AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT p.COLUMN04 as COLUMN04, p.COLUMN08, f.column05 as COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN12, p.COLUMN14, p.COLUMN13 FROM PUTABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05  WHERE(p.COLUMN13 <> 'CLOSE') AND (p.COLUMN13 <> 'FULLY PAID') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN08 between @FromDate and @ToDate   AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
end
END


ELSE IF  @ReportName='PendingPO'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Status!='' and @PONO!='' and @Vendor!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate and p.COLUMN05= @Vendor and p.COLUMN04=@PONO and p.COLUMN16=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @PONO!='' and @Vendor!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate  and p.COLUMN04=@PONO and p.COLUMN05= @Vendor AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @PONO!='' and @Status!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate and p.COLUMN16=@Status and p.COLUMN04=@PONO AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='' and @Status!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate and p.COLUMN05= @Vendor and p.COLUMN16=@Status  AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Status!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate and p.COLUMN16=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate and p.COLUMN05= @Vendor AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner 
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @PONO!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate  and p.COLUMN04=@PONO AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE') and p.COLUMN04 like 'PO%' and isnull((p.COLUMNA13),0)=0 and  p.COLUMN06 between @FromDate and @ToDate AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN06 desc
end
END


ELSE IF  @ReportName='SOSummary'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='')
begin 
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT(cast( a.COLUMN06 as date),@DateF) dt  ,a.COLUMN03 fid  FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)   AND a.COLUMN24 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT(cast( a.COLUMN06 as date),@DateF) dt  ,a.COLUMN03 fid  FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  AND  COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT(cast( a.COLUMN06 as date),@DateF) dt ,a.COLUMN03 fid  FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Customer!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT(cast( a.COLUMN06 as date),@DateF) dt ,a.COLUMN03 fid  FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  AND a.COLUMN24 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Customer!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT(cast( a.COLUMN06 as date),@DateF) dt ,a.COLUMN03 fid  FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  or a.COLUMN24 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,FORMAT(cast( a.COLUMN06 as date),@DateF) dt ,a.COLUMN03 fid  FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND  COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
end

ELSE IF  @ReportName='VendorPayment'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Vendor!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
(select COLUMN04 from PUTABLE005 where COLUMN02=b.COLUMN03) billno,a.COLUMN18 'date',
b.COLUMN04 totamt,b.COLUMN06 paidamt,b.COLUMN05 dueamt,b.COLUMN07 duedate
FROM PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and 
 a.COLUMN18 between @FromDate and @ToDate AND a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)  AND a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN18 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
(select COLUMN04 from PUTABLE005 where COLUMN02=b.COLUMN03) billno,a.COLUMN18 'date',
b.COLUMN04 totamt,b.COLUMN06 paidamt,b.COLUMN05 dueamt,b.COLUMN07 duedate
FROM PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and 
 a.COLUMN18 between @FromDate and @ToDate AND a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s)  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN18 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
(select COLUMN04 from PUTABLE005 where COLUMN02=b.COLUMN03) billno,a.COLUMN18 'date',
b.COLUMN04 totamt,b.COLUMN06 paidamt,b.COLUMN05 dueamt,b.COLUMN07 duedate
FROM PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and 
 a.COLUMN18 between @FromDate and @ToDate AND a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN18 desc
end
else if( @Vendor!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
(select COLUMN04 from PUTABLE005 where COLUMN02=b.COLUMN03) billno,a.COLUMN18 'date',
b.COLUMN04 totamt,b.COLUMN06 paidamt,b.COLUMN05 dueamt,b.COLUMN07 duedate
FROM PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and 
 a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) AND a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN18 desc
end
else if( @Vendor!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
(select COLUMN04 from PUTABLE005 where COLUMN02=b.COLUMN03) billno,a.COLUMN18 'date',
b.COLUMN04 totamt,b.COLUMN06 paidamt,b.COLUMN05 dueamt,b.COLUMN07 duedate
FROM PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and  
 a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Vendor) s) or a.COLUMN13 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN18 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN13) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
(select COLUMN04 from PUTABLE005 where COLUMN02=b.COLUMN03) billno,a.COLUMN18 'date',
b.COLUMN04 totamt,b.COLUMN06 paidamt,b.COLUMN05 dueamt,b.COLUMN07 duedate
FROM PUTABLE014 a  inner join PUTABLE015 b on b.COLUMN08=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and 
 a.COLUMN18 between @FromDate and @ToDate  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN18 desc
END
end

ELSE IF  @ReportName='CustomerPayment'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='' and @SalesRep!='')
begin 
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01
inner join SATABLE002 c on c.COLUMN02=a.COLUMN05 WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and
a.COLUMN19 between @FromDate and @ToDate AND a.COLUMN05= @Customer  AND a.COLUMN14= @OperatingUnit  AND c.COLUMN22= @SalesRep  
AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='')
begin 
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN19 between @FromDate and @ToDate AND a.COLUMN05= @Customer  AND a.COLUMN14= @OperatingUnit 
AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='' and @SalesRep !='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
 FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01
inner join SATABLE002 c on c.COLUMN02=a.COLUMN05 WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and
 a.COLUMN19 between @FromDate and @ToDate AND a.COLUMN05= @Customer   AND c.COLUMN22= @SalesRep  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!=''   AND @SalesRep!=''   )
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN19 between @FromDate and @ToDate AND a.COLUMN14= @OperatingUnit   AND c.COLUMN22= @SalesRep  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @SalesRep!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01
inner join SATABLE002 c on c.COLUMN02=a.COLUMN05 WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and
 a.COLUMN19 between @FromDate and @ToDate AND c.COLUMN22= @SalesRep  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN19 between @FromDate and @ToDate AND a.COLUMN05= @Customer  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN19 between @FromDate and @ToDate AND a.COLUMN14= @OperatingUnit  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if( @Customer!='' and @OperatingUnit!='' and  @SalesRep!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01
inner join SATABLE002 c on c.COLUMN02=a.COLUMN05 WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and
 a.COLUMN05= @Customer AND a.COLUMN14= @OperatingUnit and c.COLUMN22= @SalesRep AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if(  @OperatingUnit!='' and  @SalesRep!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01
inner join SATABLE002 c on c.COLUMN02=a.COLUMN05 WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and
 a.COLUMN05= @Customer  and c.COLUMN22= @SalesRep AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if( @Customer!=''  and  @SalesRep!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN05= @Customer  and c.COLUMN22= @SalesRep AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if( @Customer!='' and @OperatingUnit!='' )
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN05= @Customer AND a.COLUMN14= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if( @Customer!='' or @OperatingUnit!='' or @SalesRep!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01
inner join SATABLE002 c on c.COLUMN02=a.COLUMN05 WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and
 a.COLUMN05= @Customer or a.COLUMN14= @OperatingUnit or c.COLUMN22= @SalesRep AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT (SELECT  COLUMN03 FROM CONTABLE007 WHERE (COLUMN02 = a.COLUMN14)) AS ou, c.COLUMN05 AS customer,
(SELECT COLUMN06 from  MATABLE010 where COLUMN30='True' and COLUMN02=c.COLUMN22) AS salesrep,
(SELECT COLUMN04 FROM SATABLE009 WHERE (COLUMN02 = b.COLUMN03)) AS billno, 
a.COLUMN19 AS 'date', b.COLUMN04 AS totamt, b.COLUMN06 AS paidamt, b.COLUMN05 AS dueamt, b.COLUMN07 AS duedate
,a.COLUMN04 [NO] ,a.COLUMN03 fid
FROM SATABLE011 AS a INNER JOIN SATABLE012 AS b ON b.COLUMN08 = a.COLUMN01 inner join SATABLE002 c on c.COLUMN02=a.COLUMN05
WHERE isnull((a.COLUMNA13),0)=0 and  a.COLUMN18 = 1002 and a.COLUMN19 between @FromDate and @ToDate  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN19 desc
end
end
ELSE IF  @ReportName='POByItem'
BEGIN
if(@FromDate!='' and @ToDate!='' and @UPC!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 pono,a.COLUMN06  dt, (select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM PUTABLE001 a inner join PUTABLE002 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND b.COLUMN04= @UPC  AND a.COLUMN24= @OperatingUnit  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @UPC!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 pono,a.COLUMN06  dt, (select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM PUTABLE001 a inner join PUTABLE002 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND b.COLUMN04= @UPC AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 pono,a.COLUMN06  dt, (select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM PUTABLE001 a inner join PUTABLE002 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @UPC!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 pono,a.COLUMN06  dt, (select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM PUTABLE001 a inner join PUTABLE002 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1001 and 
 b.COLUMN04= @UPC AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @UPC!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 pono,a.COLUMN06  dt, (select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM PUTABLE001 a inner join PUTABLE002 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1001 and 
 b.COLUMN04= @UPC or a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 pono,a.COLUMN06  dt, (select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM PUTABLE001 a inner join PUTABLE002 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
end

ELSE IF  @ReportName='SOByItem'
BEGIN
if(@FromDate!='' and @ToDate!='' and @UPC!='' and @OperatingUnit!='')
begin 
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 sono,a.COLUMN06  dt, (select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND b.COLUMN04= @UPC  AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @UPC!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 sono,a.COLUMN06  dt, (select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND b.COLUMN04= @UPC AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 sono,a.COLUMN06  dt, (select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @UPC!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 sono,a.COLUMN06  dt, (select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1002 and
 b.COLUMN04= @UPC AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @UPC!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 sono,a.COLUMN06  dt, (select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1002 and
 b.COLUMN04= @UPC or a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN03) item,
b.COLUMN04 upc,a.COLUMN04 sono,a.COLUMN06  dt, (select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
b.COLUMN07 qty,b.COLUMN11 amt 
FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
end

ELSE IF  @ReportName='POHistory'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Vendor!='' and @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Vendor  AND a.COLUMN24= @OperatingUnit and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Vendor  and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit  and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Vendor!='' and @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN05= @Vendor AND a.COLUMN24= @OperatingUnit   and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Vendor!=''  and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN05= @Vendor   and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
  a.COLUMN24= @OperatingUnit   and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate  and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
else if(@FromDate!='' and @ToDate!='' and @Vendor!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Vendor  AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Vendor!='' )
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Vendor AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN06 desc
end
else if( @Vendor!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN05= @Vendor AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN06 between @FromDate and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
else if( @Vendor!='' or @OperatingUnit!='' or @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor, a.COLUMN16 status,a.COLUMN04 pono,
a.COLUMN06 dt,a.COLUMN15 amt FROM PUTABLE001 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1001 and 
 a.COLUMN05= @Vendor or a.COLUMN24= @OperatingUnit  or a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
end

ELSE IF  @ReportName='SOHistory'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='' and @Status!='')
begin 
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,a.COLUMN06 dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Customer  AND a.COLUMN24= @OperatingUnit and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Customer  and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit   and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Customer!='' and @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN05= @Customer AND a.COLUMN24= @OperatingUnit   and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Customer!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN05= @Customer and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(  @OperatingUnit!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
  a.COLUMN24= @OperatingUnit  and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate   and a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='' )
begin 
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Customer  AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN05= @Customer AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Customer!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN05= @Customer AND a.COLUMN24= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if( @Customer!='' or @OperatingUnit!='' or @Status!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN05= @Customer or a.COLUMN24= @OperatingUnit  or a.COLUMN16=@Status AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN24) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,a.COLUMN04 sono,a.COLUMN15 amt,
 a.COLUMN16 status,cast( a.COLUMN06 as date) dt FROM SATABLE005 a where isnull((a.COLUMNA13),0)=0 and  COLUMN29=1002 and
 a.COLUMN06 between @FromDate and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN06 desc
END
end

ELSE IF  @ReportName='ItemOrderDetails'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Item!='' and @OperatingUnit!='')
begin 
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02= a.COLUMN13) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN03) item,b.COLUMN06 qo,b.COLUMN07 qr,
b.COLUMN09 rq,(select COLUMN04 from PUTABLE001 where COLUMN02=a.COLUMN06) pono,a.COLUMN09 dt,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor
 FROM PUTABLE003 a inner join PUTABLE004 b on b.COLUMN12=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and
 a.COLUMN09 between @FromDate and @ToDate AND b.COLUMN03= @Item  AND a.COLUMN13= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
else if(@FromDate!='' and @ToDate!='' and @Item!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02= a.COLUMN13) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN03) item,b.COLUMN06 qo,b.COLUMN07 qr,
b.COLUMN09 rq,(select COLUMN04 from PUTABLE001 where COLUMN02=a.COLUMN06) pono,a.COLUMN09 dt,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor
 FROM PUTABLE003 a inner join PUTABLE004 b on b.COLUMN12=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and
 a.COLUMN09 between @FromDate and @ToDate AND b.COLUMN03= @Item AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02= a.COLUMN13) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN03) item,b.COLUMN06 qo,b.COLUMN07 qr,
b.COLUMN09 rq,(select COLUMN04 from PUTABLE001 where COLUMN02=a.COLUMN06) pono,a.COLUMN09 dt,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor
 FROM PUTABLE003 a inner join PUTABLE004 b on b.COLUMN12=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and
 a.COLUMN09 between @FromDate and @ToDate AND a.COLUMN13= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
else if( @Item!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02= a.COLUMN13) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN03) item,b.COLUMN06 qo,b.COLUMN07 qr,
b.COLUMN09 rq,(select COLUMN04 from PUTABLE001 where COLUMN02=a.COLUMN06) pono,a.COLUMN09 dt,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor
 FROM PUTABLE003 a inner join PUTABLE004 b on b.COLUMN12=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and
 b.COLUMN03= @Item AND a.COLUMN13= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
else if( @Item!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02= a.COLUMN13) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN03) item,b.COLUMN06 qo,b.COLUMN07 qr,
b.COLUMN09 rq,(select COLUMN04 from PUTABLE001 where COLUMN02=a.COLUMN06) pono,a.COLUMN09 dt,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor
 FROM PUTABLE003 a inner join PUTABLE004 b on b.COLUMN12=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and
 b.COLUMN03= @Item or a.COLUMN13= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02= a.COLUMN13) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN03) item,b.COLUMN06 qo,b.COLUMN07 qr,
b.COLUMN09 rq,(select COLUMN04 from PUTABLE001 where COLUMN02=a.COLUMN06) pono,a.COLUMN09 dt,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN05) vendor
 FROM PUTABLE003 a inner join PUTABLE004 b on b.COLUMN12=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN17=1001 and
 a.COLUMN09 between @FromDate and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
END
end

ELSE IF  @ReportName='ItemFulfilment'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Item!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN04) item,b.COLUMN07 qo,b.COLUMN08 qr,
b.COLUMN11 rq,(select COLUMN04 from SATABLE005 where COLUMN02=a.COLUMN06) sono,a.COLUMN08 dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer
 FROM SATABLE007 a  inner join SATABLE008 b on b.COLUMN14=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN20=1002 and 
 a.COLUMN08 between @FromDate and @ToDate AND b.COLUMN04= @Item  AND a.COLUMN15= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @Item!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN04) item,b.COLUMN07 qo,b.COLUMN08 qr,
b.COLUMN11 rq,(select COLUMN04 from SATABLE005 where COLUMN02=a.COLUMN06) sono,a.COLUMN08 dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer
 FROM SATABLE007 a  inner join SATABLE008 b on b.COLUMN14=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN20=1002 and 
 a.COLUMN08 between @FromDate and @ToDate AND b.COLUMN04= @Item AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN04) item,b.COLUMN07 qo,b.COLUMN08 qr,
b.COLUMN11 rq,(select COLUMN04 from SATABLE005 where COLUMN02=a.COLUMN06) sono,a.COLUMN08 dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer
 FROM SATABLE007 a  inner join SATABLE008 b on b.COLUMN14=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN20=1002 and 
 a.COLUMN08 between @FromDate and @ToDate AND a.COLUMN15= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if( @Item!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN04) item,b.COLUMN07 qo,b.COLUMN08 qr,
b.COLUMN11 rq,(select COLUMN04 from SATABLE005 where COLUMN02=a.COLUMN06) sono,a.COLUMN08 dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer
 FROM SATABLE007 a  inner join SATABLE008 b on b.COLUMN14=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN20=1002 and 
 b.COLUMN04= @Item AND a.COLUMN15= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if( @Item!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN04) item,b.COLUMN07 qo,b.COLUMN08 qr,
b.COLUMN11 rq,(select COLUMN04 from SATABLE005 where COLUMN02=a.COLUMN06) sono,a.COLUMN08 dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer
 FROM SATABLE007 a  inner join SATABLE008 b on b.COLUMN14=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN20=1002 and   
 b.COLUMN04= @Item or a.COLUMN15= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,
(select COLUMN04 from MATABLE007 where COLUMN02= b.COLUMN04) item,b.COLUMN07 qo,b.COLUMN08 qr,
b.COLUMN11 rq,(select COLUMN04 from SATABLE005 where COLUMN02=a.COLUMN06) sono,a.COLUMN08 dt,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer
 FROM SATABLE007 a  inner join SATABLE008 b on b.COLUMN14=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN20=1002 and 
 a.COLUMN08 between @FromDate and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
END
end

ELSE IF  @ReportName='VendorPaymentDues'
BEGIN
declare @whereStr nvarchar(max)=null
declare	@Query1 nvarchar(max)=null
if @OperatingUnit!=''
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and VID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end
select * into #VendorPaymentDues from (
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) ou,
(select COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07) vendor,FORMAT(a.COLUMN04,@DateF) pdt,
a.COLUMN09 pbno, a.COLUMN12 total,a.COLUMN13 payment,(case when a.COLUMN06='Credit Memo' then -(a.COLUMN14) else a.COLUMN14 end) due,FORMAT(a.COLUMN11,@DAteF) dt,a.COLUMNA02 OPID,a.COLUMN07 VID
,a.COLUMN06 fname
FROM PUTABLE016 a   where  isnull((a.COLUMNA13),0)=0  AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  a.COLUMNA03=@AcOwner
union all
select (select COLUMN03 from CONTABLE007 where COLUMN02=fitable020.COLUMNA02 ) as ou,
(case when FITABLE020.COLUMN11=22305  then (select column05 from SATABLE001 where COLUMN02=fitable020.COLUMN07) end) vendor,FORMAT(fitable020.COLUMN05,@DateF) pdt,fitable020.COLUMN04 pbno,0.00 total,(fitable020.COLUMN18) payment,0.00 due, null dt,fitable020.COLUMNA02 OPID,fitable020.COLUMN07 VID 
--,'5' fname
,cast(fitable020.COLUMN03 as varchar) fname
 from fitable020 fitable020
where COLUMN03=1363 and FITABLE020.COLUMN11=22305 and isnull((COLUMNA13),0)=0 and COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  COLUMNA03=@AcOwner  
)Query
set @Query1='select * from #VendorPaymentDues'+@whereStr+' order by pdt desc'
exec (@Query1) 
end

ELSE IF  @ReportName='CustomerPaymentDues'
BEGIN
if( @Customer!='' and @OperatingUnit!='')
begin 
--EMPHCS1108	Customerpayment dues changes adding aging columns BY RAJ.Jr 08/09/2015
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=a.COLUMN07) As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=a.COLUMN07)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN07) Party,FORMAT(a.COLUMN04,@DateF) Date,a.COLUMN05 [Bill No.],a.COLUMN09 Amount,a.COLUMN11 Paid,
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [0-30],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [31-45],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [46-60],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [61-90],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [>90],a.COLUMN12 Balance ,a.COLUMN06 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM PUTABLE018 a   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'RECEIPT VOUCHER')  and
a.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)    AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0 group by a.COLUMN07,a.COLUMNA02,a.COLUMN06,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN04,a.COLUMN12 
 union all
 select (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08)) as City,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) Party,FORMAT(COLUMN05,@DateF) Date, COLUMN04 [Bill No.],0.00 Amount,cast(isnull(COLUMN19,0)as decimal(18,2)) Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0.00 Balance ,cast(fitable023.COLUMN03 as varchar) fname from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN08 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)  AND COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)    AND  COLUMNA03=@AcOwner   
union all
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=b.COLUMN06)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=b.COLUMN06) Party,FORMAT(b.COLUMN04,@DateF) Date,
b.COLUMN09 [Bill No.], NULL Amount,b.COLUMN07 Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],NULL Balance  ,b.COLUMN03 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM FITABLE036 b   where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049 AND b.COLUMN06= @Customer  AND b.COLUMNA02= @OperatingUnit   AND  b.COLUMNA03=@AcOwner order by City,Date desc 

end
else if( @Customer!='')
begin 
--EMPHCS1108	Customerpayment dues changes adding aging columns BY RAJ.Jr 08/09/2015
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=a.COLUMN07) As Mobile,
(select COLUMN10 from    SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=a.COLUMN07)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN07) Party,FORMAT(a.COLUMN04,@DateF) Date,a.COLUMN05 [Bill No.],a.COLUMN09 Amount,a.COLUMN11 Paid,
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [0-30],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [31-45],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [46-60],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [61-90],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [>90],a.COLUMN12 Balance,a.COLUMN06 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM PUTABLE018 a   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or  a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER')  and
a.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)      AND  a.COLUMNA03=@AcOwner and isnull((a.COLUMNA13),0)=0
group by a.COLUMN07,a.COLUMNA02,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN04,a.COLUMN12 ,a.COLUMN06
 union all
 select (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08)) as City,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) Party,FORMAT(COLUMN05,@DateF) Date, COLUMN04 [Bill No.],0.00 Amount,cast(isnull(COLUMN19,0)as decimal(18,2)) Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0.00 Balance  ,cast(fitable023.COLUMN03 as varchar) fname from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0   and   COLUMN08= @Customer   AND  COLUMNA03=@AcOwner
 union all
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=b.COLUMN06)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=b.COLUMN06) Party,FORMAT(b.COLUMN04,@DateF) Date,
b.COLUMN09 [Bill No.], NULL Amount,b.COLUMN07 Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],NULL Balance  ,b.COLUMN03 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM FITABLE036 b   where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049  AND b.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Customer) s)    AND  b.COLUMNA03=@AcOwner order by City,Date desc 
 end
 else if(@OperatingUnit!='')
begin 
--EMPHCS1108	Customerpayment dues changes adding aging columns BY RAJ.Jr 08/09/2015
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=a.COLUMN07) As Mobile,
(select COLUMN10 from    SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=a.COLUMN07)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN07) Party,FORMAT(a.COLUMN04,@DateF) Date,a.COLUMN05 [Bill No.],a.COLUMN09 Amount,a.COLUMN11 Paid,
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [0-30],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [31-45],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [46-60],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [61-90],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [>90],a.COLUMN12 Balance,a.COLUMN06 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM PUTABLE018 a   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER')  and
 a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)   AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0  group by a.COLUMN07,a.COLUMNA02,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN04,a.COLUMN12 ,a.COLUMN06
  union all
 select (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08)) as City,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) Party,FORMAT(COLUMN05,@DateF) Date, COLUMN04 [Bill No.],0.00 Amount,cast(isnull(COLUMN19,0)as decimal(18,2)) Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0.00 Balance   ,cast(fitable023.COLUMN03 as varchar) fname from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0    AND COLUMNA02= @OperatingUnit   AND  COLUMNA03=@AcOwner
 union all
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=b.COLUMN06)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=b.COLUMN06) Party,FORMAT(b.COLUMN04,@DateF) Date,
b.COLUMN09 [Bill No.], NULL Amount,b.COLUMN07 Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],NULL Balance  ,b.COLUMN03 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM FITABLE036 b   where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049  AND b.COLUMNA02= @OperatingUnit   AND  b.COLUMNA03=@AcOwner order by City,Date desc 
 end
else if( @Customer='' and @OperatingUnit='')
begin 
--EMPHCS1108	Customerpayment dues changes adding aging columns BY RAJ.Jr 08/09/2015
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=a.COLUMN07) As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=a.COLUMN07)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN07) Party,FORMAT(a.COLUMN04,@DateF) Date,a.COLUMN05 [Bill No.],a.COLUMN09 Amount,a.COLUMN11 Paid,
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [0-30],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [31-45],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [46-60],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [61-90],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [>90],a.COLUMN12 Balance,a.COLUMN06 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM PUTABLE018 a   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER') AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0 group by a.COLUMN07,a.COLUMNA02,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN04,a.COLUMN12 ,a.COLUMN06
  union all
 select (select COLUMN03 from CONTABLE007 where COLUMN02=FITABLE023.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,(select COLUMN11 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08)) as City,(select column05 from SATABLE002 where COLUMN02=FITABLE023.COLUMN08) Party,FORMAT(COLUMN05,@DateF) Date, COLUMN04 [Bill No.],0.00 Amount,cast(isnull(COLUMN19,0)as decimal(18,2)) Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0.00 Balance  ,cast(fitable023.COLUMN03 as varchar) fname from FITABLE023 
where COLUMN03=1386 and isnull((COLUMNA13),0)=0  AND  COLUMNA03=@AcOwner
 union all
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=(select COLUMN16 from SATABLE002 where COLUMN02=b.COLUMN06)) as City,
(select COLUMN05 from SATABLE002 where COLUMN02=b.COLUMN06) Party,FORMAT(b.COLUMN04,@DateF) Date,
b.COLUMN09 [Bill No.], NULL Amount,b.COLUMN07 Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],NULL Balance  
--EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
,b.COLUMN03 fname
--EMPHCS1108  Customerpayment dues changes adding aging columns by srinivas 12/09/2015
 FROM FITABLE036 b   where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049 AND   b.COLUMNA03=@AcOwner order by City,Date desc
end
end

ELSE IF  @ReportName='PendingInvoices'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Status!='' and @BillNO!='' and @Customer!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and  p.COLUMN10 between @FromDate and @ToDate and p.COLUMN05=@Customer and p.COLUMN02=@BillNO and p.COLUMN13=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!=''   and @BillNO!='' and @Customer!='' )
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and p.COLUMN10 between @FromDate and @ToDate and   p.COLUMN02=@BillNO and p.COLUMN05=@Customer AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner   
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!=''   and @BillNO!='' and @Status!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and p.COLUMN10 between @FromDate and @ToDate and   p.COLUMN02=@BillNO and p.COLUMN13=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner   
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!=''   and @Customer!='' and @Status!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and p.COLUMN10 between @FromDate and @ToDate and p.COLUMN05=@Customer and p.COLUMN13=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner   
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!=''  and @BillNO!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and p.COLUMN10 between @FromDate and @ToDate and   p.COLUMN02=@BillNO AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner   
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and p.COLUMN10 between @FromDate and @ToDate   and p.COLUMN05=@Customer
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!='' and @Status!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and p.COLUMN10 between @FromDate and @ToDate and p.COLUMN13=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN10 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'INV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED') and isnull((p.COLUMNA13),0)=0 and p.COLUMN10 between @FromDate and @ToDate AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN10 desc
end
END


ELSE IF  @ReportName='PendingSalesOrders'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Status!='' and @SONO!='' and @Customer!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate and p.column05=@Customer and p.column16=@Status and p.column04=@SONO AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!=''   and @SONO!=''  and @Customer!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate and  p.column04=@SONO and p.column05=@Customer AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!=''   and @SONO!=''  and @Status!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate and  p.column04=@SONO and p.column16=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!=''   and @Customer!=''  and @Status!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate and p.column05=@Customer and p.column16=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @SONO!=''  )
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate and  p.column04=@SONO  AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @Customer!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate and p.column05=@Customer AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @Status!='' )
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate  and p.column16=@Status AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%' and isnull((p.COLUMNA13),0)=0 and p.COLUMN08 between @FromDate and @ToDate AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner
order by p.COLUMN08 desc
end
END

ELSE IF  @ReportName='AccountsLedger'
BEGIN
if(@FromDate!='' and @ToDate!='' and @Type!='')
begin
SELECT a.COLUMN04, a.COLUMN05,(SELECT COLUMN04 from FITABLE001 where column02=a.COLUMN06)as COLUMN06,(SELECT COLUMN04 from matable002 where column02=a.COLUMN07)as COLUMN07, a.COLUMN08, a.COLUMN09, (select sum( column10) from putable019 where column03 = a.column02)as payable, (select sum( column11) from putable019 where column03 = a.column02)as receible FROM FITABLE001 a  where isnull((a.COLUMNA13),0)=0 and a.COLUMN09 between  @FromDate  and @ToDate AND COLUMN07 = @Type AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT a.COLUMN04, a.COLUMN05,(SELECT COLUMN04 from FITABLE001 where column02=a.COLUMN06)as COLUMN06,(SELECT COLUMN04 from matable002 where column02=a.COLUMN07)as COLUMN07, a.COLUMN08, a.COLUMN09, (select sum( column10) from putable019 where column03 = a.column02)as payable, (select sum( column11) from putable019 where column03 = a.column02)as receible FROM FITABLE001 a  where isnull((a.COLUMNA13),0)=0 and a.COLUMN09 between  @FromDate  and @ToDate AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN09 desc
end
END

ELSE IF  @ReportName='OpeningBalance'
BEGIN
SELECT a.COLUMN04 'date',a.COLUMN05 'no',a.COLUMN06 'type',a.COLUMN07 'payee', b.COLUMN04, a.COLUMN09,a.COLUMN10,a.COLUMN11 bal,a.COLUMN12 FROM FITABLE018 a  left outer join  FITABLE001 b on b.COLUMN02=a.COLUMN08
AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND isnull((a.COLUMNA13),0)=0 and  a.COLUMNA03=@AcOwner order by a.COLUMN02 desc 
END

--ELSE IF  @ReportName='InventoryAsset'
--BEGIN
--SELECT		e.COLUMN04 AS accountno,
--			p.COLUMN04 AS 'date',
--			p.COLUMN06 type, 
--			(CASE  
--				WHEN 
--					p.COLUMN06='Item Issue' 
--					then (select COLUMN06 from SATABLE001 where COLUMN02=p.COLUMN07)
--				when	
--					p.COLUMN06='Inventory Adjustment'
--					then null
--					else (select COLUMN05 from SATABLE001 where COLUMN02=p.COLUMN07) 
--			end) payee,
--			p.COLUMN08 AS account, 
--			p.COLUMN09 AS memo,
--			p.COLUMN10 AS inamt,
--			p.COLUMN11 AS decamt,
--			p.COLUMN12 AS balcamt
--			From PUTABLE017 p inner join  
--			FITABLE001 e on e.COLUMN02=p.COLUMN03
--			AND  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  p.COLUMNA03=@AcOwner  order by p.COLUMN02 desc 
--END
ELSE IF  @ReportName='InventoryAsset'
BEGIN
if(@FromDate!='' and @ToDate!=''  and @OperatingUnit!='')
begin
--SELECT   e.COLUMN04 AS accountno,CON07.COLUMN03 as OperatingUnit,p.COLUMN04 AS 'date',p.COLUMN06 type, 
--(CASE  WHEN p.COLUMN06='Item Issue' then a.COLUMN06 else b.COLUMN05 end) payee,
--p.COLUMN08 AS account, p.COLUMN09 AS memo,p.COLUMN10 AS inamt,p.COLUMN11 AS decamt,p.COLUMN12 AS balcamt
-- From PUTABLE017 p 
-- left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
-- left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02
-- left outer join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMN18=CON07.COLUMN02
-- left outer join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and b.COLUMN17=CON07.COLUMN02
   --EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
   declare @temp decimal(18,2)
set @temp=(select isnull(sum(p.column10),0)-isnull(sum(p.column11),0)  from PUTABLE017 p where p.COLUMN04<@FromDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  p.COLUMNA03=@AcOwner and p.COLUMNA02= @OperatingUnit  and p.COLUMNA02 is not null) 
(select NULL accountno,CON07.COLUMN03 OperatingUnit,NULL 'date',NULL 'type',NULL payee,NULL account,'Previous Amount' memo,sum(isnull(p.column10,0)) inamt,sum(isnull(p.column11,0)) decamt,
isnull(sum(p.column10),0)-isnull(sum(p.column11),0) balcamt,max(p.COLUMN02) ab  from PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
where p.COLUMN04<@FromDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  p.COLUMNA03=@AcOwner and p.COLUMNA02= @OperatingUnit  and p.COLUMNA02 is not null group by CON07.COLUMN03) union all
SELECT   e.COLUMN04 AS accountno,CON07.COLUMN03 as OperatingUnit,p.COLUMN04 AS 'date',p.COLUMN06 type, 
(CASE  WHEN p.COLUMN06='Item Issue' then a.COLUMN06 else b.COLUMN05 end) payee,
p.COLUMN08 AS account, p.COLUMN09 AS memo,p.COLUMN10 AS inamt,p.COLUMN11 AS decamt,
 (SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+@temp AS balcamt,p.COLUMN02 ab
 From PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02
 left outer join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMN18=CON07.COLUMN02
 left outer join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and b.COLUMN17=CON07.COLUMN02
  where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  p.COLUMNA03=@AcOwner and p.COLUMNA02= @OperatingUnit  and p.COLUMNA02 is not null order by ab desc
			end
else if(@FromDate!='' and @ToDate!='')
begin
--EMPHCS800 Reports - Inventory asset ,AP and AR ..... calculations going wrong with date filterations done by srinivas 8/5/2015
set @temp=(select isnull(sum(p.column10),0)-isnull(sum(p.column11),0)  from PUTABLE017 p where p.COLUMN04<@FromDate and  p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  p.COLUMNA03=@AcOwner and p.COLUMNA02 is not null) 
(select NULL accountno,CON07.COLUMN03 OperatingUnit,NULL 'date',NULL 'type',NULL payee,NULL account,'Previous Amount' memo,sum(isnull(p.column10,0)) inamt,sum(isnull(p.column11,0)) decamt,
isnull(sum(p.column10),0)-isnull(sum(p.column11),0) balcamt,max(p.COLUMN02) ab  from PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
where p.COLUMN04<@FromDate and p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  p.COLUMNA03=@AcOwner and p.COLUMNA02 is not null group by CON07.COLUMN03) union all
SELECT   e.COLUMN04 AS accountno,CON07.COLUMN03 as OperatingUnit,p.COLUMN04 AS 'date',p.COLUMN06 type, 
(CASE  WHEN p.COLUMN06='Item Issue' then a.COLUMN06 else b.COLUMN05 end) payee,
p.COLUMN08 AS account, p.COLUMN09 AS memo,p.COLUMN10 AS inamt,p.COLUMN11 AS decamt,
 (SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))+@temp AS balcamt,p.COLUMN02 ab
 From PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02
 left outer join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMN18=CON07.COLUMN02
 left outer join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and b.COLUMN17=CON07.COLUMN02
  where isnull((p.COLUMNA13),0)=0 and p.COLUMN04 between @FromDate and @ToDate AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  p.COLUMNA03=@AcOwner and p.COLUMNA02 is not null  order by ab desc 
			
			end
else if( @OperatingUnit!='')
begin

SELECT   e.COLUMN04 AS accountno,CON07.COLUMN03 as OperatingUnit,p.COLUMN04 AS 'date',p.COLUMN06 type, 
(CASE  WHEN p.COLUMN06='Item Issue' then a.COLUMN06 else b.COLUMN05 end) payee,
p.COLUMN08 AS account, p.COLUMN09 AS memo,p.COLUMN10 AS inamt,p.COLUMN11 AS decamt,
 (SUM(isnull(p.COLUMN10,0)) OVER(ORDER BY p.COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
	-SUM(isnull(p.COLUMN11,0)) OVER(ORDER BY p.COLUMN02 asc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) AS balcamt
 From PUTABLE017 p left outer join CONTABLE007 CON07 on p.COLUMNA02=CON07.COLUMN02 
 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN03 and e.COLUMNA02=CON07.COLUMN02
 left outer join  SATABLE001 a on a.COLUMN02=p.COLUMN07 and a.COLUMN18=CON07.COLUMN02
 left outer join  SATABLE002 b on b.COLUMN02=p.COLUMN07 and b.COLUMN17=CON07.COLUMN02
  AND p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND isnull((p.COLUMNA13),0)=0 and p.COLUMNA03=@AcOwner and p.COLUMN13= @OperatingUnit and p.COLUMNA02 is not null  order by p.COLUMN02 desc
end
END
ELSE IF  @ReportName='StockLedger'
--EMPHCS1338	Rename Stock Details as Stock Ledger BY RAj.Jr 15/11/2015
BEGIN
--EMPHCS1005		StockDetails and Stock ledger displaying deleted records  BY RAj.Jr 21/8/2015
--EMPHCS1340 Stock Details : For Invoice and bills not showing values,If no IR and II then it has to consider bill and invoice qty BY RAj.Jr 15/11/2015
--EMPHCS1337	Stock Details : For IR vendor details not coming BY RAj.Jr 15/11/2015
if((@FromDate!='' and @ToDate!=''  and @Item!='' and @OperatingUnit!='' and @Type!=''  and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr, 0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
and a.column06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
and a.column05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
 union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01  and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) --and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Item!='' and @OperatingUnit!='' and @Type!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr, 0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
and a.column06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.COLUMN05=a.COLUMNA02 then d.COLUMN06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
and a.column05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
 UNION ALL 
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) --and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end

else if((@FromDate!='' and @ToDate!=''  and @Item!='' and @OperatingUnit!='' and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
 b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) AND a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s)  and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr, 0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
and a.COLUMN06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal, 0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
and a.COLUMN05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN13,f.COLUMN04)
 UNION ALL 
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN15,f.COLUMN04)
  UNION ALL 
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) --and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Item!='' and @OperatingUnit!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,-d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr, 0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on ( c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
 --group by a.column06,c7.COLUMN04,g.COLUMN04,g.COLUMN06,a.COLUMN04,a.COLUMN08,d.column06,c10.COLUMN04
and a.column06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL 
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal, 0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
and a.column05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)union all
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN13,f.COLUMN04)
 union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) --and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @OperatingUnit!='' and @Type!='' and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate  and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate  and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr, 0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate   and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and
a.COLUMN06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate   and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and
a.COLUMN05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate  and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate 
and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) -- and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate   and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @OperatingUnit!='' and @Type!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate  and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate  and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr,0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate   and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and
a.COLUMN06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate   and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and
a.COLUMN05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate  and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
  UNION ALL 
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN15,f.COLUMN04)
  UNION ALL 
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate 
and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) -- and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate   and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Item!='' and @Type!='' and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0  qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  group by b.COLUMN15,f.COLUMN04)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN14,g.COLUMN04)
UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Item!='' and @Type!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)  group by b.COLUMN15,f.COLUMN04)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.COLUMN14,g.COLUMN04)
UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05 ) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!='' and @Type!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05 )  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate and a.column03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @OperatingUnit!='' and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate  and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate  and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  
and a.COLUMN06 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  
and a.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate  and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate 
and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  -- and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate   and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @OperatingUnit!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate  and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate  and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.COLUMN06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  
and a.COLUMN06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.COLUMN05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate  
and a.COLUMN05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate  and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and b.COLUMN13  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and b.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
and a.COLUMN15  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate 
and a.COLUMN14  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s) 
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  -- and a.columna02=@OperatingUnit 
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate   and a.COLUMN10 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OperatingUnit) s)
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Item!='' and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0  qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND   b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by b.COLUMN15,f.COLUMN04)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.COLUMN14,g.COLUMN04)
Union
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Item!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate and c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate and c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0 qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s) 
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate and  c.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate and  c.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by b.COLUMN15,f.COLUMN04)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate and  d.COLUMN04 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08<@FromDate and  d.COLUMN05 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.COLUMN14,g.COLUMN04)
UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate  and d.COLUMN03 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Item) s)  group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!=''  and @Project!=''))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate 
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr, 0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0 qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05) and c7.column02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate 
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMN21 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate  group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMN26 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMN22 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMN29 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate 
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  d.COLUMN07 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@Project) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
else if((@FromDate!='' and @ToDate!='' ))
begin
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN09 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN10 price,c.COLUMN11 amt,c10.COLUMN04 transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and
b.COLUMN09 between @FromDate and @ToDate 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,f.COLUMN06 upc,
b.COLUMN04 tno,b.COLUMN08 tdate, (select COLUMN05 from SATABLE001 where COLUMN02=b.COLUMN05) vendor ,
0 openbal,c.COLUMN08 qr,0 qi,c.COLUMN11 price,c.COLUMN12 amt,c10.COLUMN04 transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 where  (c.COLUMN03='0' or c.COLUMN03='' or c.COLUMN03 is null) and isnull((b.COLUMNA13),0)=0 and  ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN08 between @FromDate and @ToDate 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN09 qi ,d.COLUMN12 price,d.COLUMN13 amt,c10.COLUMN04 transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate 
)
 UNION ALL
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, (select COLUMN05 from SATABLE002 where COLUMN02 = a.COLUMN05 ) vendor,
0 openbal, 0 qr,d.COLUMN10 qi ,d.COLUMN13 price,d.COLUMN14 amt,c10.COLUMN04 transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate 
)
 UNION ALL
 (select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,case when a.column06!='' then d.column06 else 0 end qr,0 qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.column02=a.column06)  left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column06  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
(select c7.COLUMN03  ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN08 tdate, null vendor,
0 openbal,0 qr,case when a.column05=a.COLUMNA02 then  d.column06 else 0 end qi ,0 price,0 amt,c10.COLUMN04 transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE007 c7 on (c7.Column02=a.column05) left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.column05  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
)UNION ALL
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,g.COLUMN06 upc,
a.COLUMN04 tno,a.COLUMN05 tdate, null vendor,
0 openbal,isnull(d.COLUMN08,0) qr,isnull(d.COLUMN16,0) qi ,0 price,0 amt,c10.COLUMN04 transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 between @FromDate and @ToDate 
)UNION ALL
 ----------------
(select ou,item,upc,tno,tdate,vendor,isnull(sum(openbal),0) as openbal,qr,qi,price,amt,transType from (
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 where isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN09< @FromDate  group by b.COLUMN13,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN15) ou,f.COLUMN04 item,null upc,
null tno,null tdate, null vendor ,
sum(c.COLUMN08) openbal,null qr,null qi,null price,null amt,null transType
from PUTABLE005 b inner join PUTABLE006 c on c.COLUMN13=b.COLUMN01 and isnull(c.COLUMNA13,0)=0 
inner join MATABLE007 f on f.COLUMN02=c.COLUMN04 where  (c.COLUMN03='0' or c.COLUMN03='' or c.column03 is null) and isnull((b.COLUMNA13),0)=0 and  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  b.COLUMNA03=@AcOwner and b.COLUMN08< @FromDate group by b.COLUMN15,f.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN15) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN09) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
group by a.COLUMN15,g.COLUMN04)
  union all
(select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.COLUMN10) openbal, null qr,null qi ,null price,null amt,null transType
from SATABLE009 a inner join SATABLE010 d on d.COLUMN15=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN05 where (d.COLUMN04='0' or d.COLUMN04='' or d.COLUMN04 is null) and isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner 
and a.COLUMN08<@FromDate 
group by a.COLUMN14,g.COLUMN04)
 union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.columna02) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
-sum(d.column06) openbal,null qr,null qi ,null price,null amt,null transType
from PRTABLE003 a inner join PRTABLE004 d on d.COLUMN08=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN08 < @FromDate  
group by a.columna02,g.COLUMN04
) union all
 (select (select COLUMN03 from CONTABLE007 where COLUMN02=a.column10) ou,g.COLUMN04 item,null upc,
null tno,null tdate, null vendor,
isnull(sum(d.COLUMN08),0)-isnull(sum(d.COLUMN16),0) openbal,null qr,null qi ,null price,null amt,null transType
from FITABLE014 a inner join FITABLE015 d on d.COLUMN11=a.COLUMN01 and  isnull((d.COLUMNA13),0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN03 where  isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner and a.COLUMN05 < @FromDate
group by a.column10,g.COLUMN04
)
) p group by ou,item,upc,tno,tdate,vendor,qr,qi,price,amt,transType
)
end
END

ELSE IF  @ReportName='Top10Vendors'
BEGIN
if(@FromDate!='' and @ToDate!='' )
begin
SELECT s1.COLUMN04, s1.COLUMN05,FORMAT(max( s2.COLUMN06),@DateF) date, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE001 AS s1 INNER JOIN PUTABLE001 AS s2 ON s1.COLUMN02 = s2.COLUMN05  where isnull((s1.COLUMNA13),0)=0 and s1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or s1.COLUMNA02 is null AND s1.COLUMNA03=@AcOwner  and s2.COLUMN06 between cast(@FromDate as date) and cast(@ToDate as date)   GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC 
end
else if(@FromDate!='' or @ToDate!=''  )
begin
SELECT s1.COLUMN04, s1.COLUMN05, FORMAT(max( s2.COLUMN06),@DateF) date, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE001 AS s1 INNER JOIN PUTABLE001 AS s2 ON s1.COLUMN02 = s2.COLUMN05  where isnull((s1.COLUMNA13),0)=0 and s1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or s1.COLUMNA02 is null AND s1.COLUMNA03=@AcOwner  and (s2.COLUMN06= @FromDate or s2.COLUMN06=@ToDate)  GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC 
end
end
ELSE IF  @ReportName='Top10Customers'
BEGIN
if(@FromDate!='' and @ToDate!='' )
begin
SELECT  s1.COLUMN04, s1.COLUMN05,FORMAT( max(s2.COLUMN06),@DateF) date, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE002 AS s1 INNER JOIN SATABLE005 AS s2 ON s1.COLUMN02 = s2.COLUMN05  where isnull((s1.COLUMNA13),0)=0 and s1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or s1.COLUMNA02 is null AND s1.COLUMNA03=@AcOwner  and s2.COLUMN06 between @FromDate and @ToDate  GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC 
end
else if(@FromDate!='' or @ToDate!=''  )
begin
SELECT  s1.COLUMN04, s1.COLUMN05,FORMAT( max(s2.COLUMN06),@DateF) date, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE002 AS s1 INNER JOIN SATABLE005 AS s2 ON s1.COLUMN02 = s2.COLUMN05  where isnull((s1.COLUMNA13),0)=0 and s1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or s1.COLUMNA02 is null AND s1.COLUMNA03=@AcOwner  and (s2.COLUMN06 = @FromDate or s2.COLUMN06 = @ToDate)  GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC
end
end
ELSE IF  @ReportName='VendorRegister'
BEGIN
if(@Vendor!='' )
begin
SELECT p.COLUMN04 as COLUMN04 , p.COLUMN05,p. COLUMN06, d.column04 as COLUMN07, f.COLUMN04 as COLUMN08, COLUMN11, COLUMN12 FROM SATABLE001 p left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN07 left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN08  where isnull((p.COLUMNA13),0)=0 and (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or p.COLUMNA02 is null) AND p.COLUMNA03=@AcOwner and p.COLUMN02=@Vendor
end
else if(@Vendor='' )
begin
SELECT p.COLUMN04 as COLUMN04 , p.COLUMN05,p. COLUMN06, d.column04 as COLUMN07, f.COLUMN04 as COLUMN08, COLUMN11, COLUMN12 FROM SATABLE001 p left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN07 left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN08  where isnull((p.COLUMNA13),0)=0 and (p.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or p.COLUMNA02 is null) AND p.COLUMNA03=@AcOwner
end
end





ELSE IF  @ReportName='ProjectPayableReport'
BEGIN
if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!=''and a.COLUMN20=@Project and a.COLUMN04 between @FromDate and @ToDate and   a.COLUMN07=@Vendor and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!=''))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!='' and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate and  a.COLUMN07=@Vendor and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!='' and @OperatingUnit!='' and @Project!='' ))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project and a.COLUMN04 between @FromDate and @ToDate  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' )
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate and  ( a.COLUMN07=@Vendor) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!='' and @Project!='' ))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project and a.COLUMN04 between @FromDate and @ToDate 
 AND  a.COLUMNA03=@AcOwner
 order by a.COLUMN02 desc
end
else if(@Project!='' and @Vendor!='' and @OperatingUnit!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project  and a.COLUMN07=@Vendor  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!='' and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate  and (  a.COLUMNA03=@AcOwner)
order by a.COLUMN02 desc
end
else if(@Project!='' and @Vendor!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project  and a.COLUMN07=@Vendor AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@Project!='' and @OperatingUnit!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner order by a.column02 desc
end
else if(@OperatingUnit!='' and @Vendor!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where  isnull((a.COLUMNA13),0)=0 and a.COLUMN20!='' and a.COLUMNA02=@OperatingUnit and a.COLUMN07=@Vendor AND  a.COLUMNA03=@AcOwner order by a.column02 desc
end
else if((@FromDate!='' or @ToDate!=''  or @Vendor!='' or @OperatingUnit!='' or @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!='' and a.COLUMN20!='' and (a.COLUMN20=@Project or a.COLUMN04 between @FromDate and @ToDate or   a.COLUMN07=@Vendor or a.COLUMNA02=@OperatingUnit) AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end

end


ELSE IF  @ReportName='projectPayableDueDates'
BEGIN
if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!=''and a.COLUMN20=@Project and a.COLUMN04 between @FromDate and @ToDate and   a.COLUMN07=@Vendor and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!='' and @OperatingUnit!=''))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!='' and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate and  a.COLUMN07=@Vendor and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!='' and @OperatingUnit!='' and @Project!='' ))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project and a.COLUMN04 between @FromDate and @ToDate  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='' )
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Vendor!=''))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate and  ( a.COLUMN07=@Vendor) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!='' and @Project!='' ))
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project and a.COLUMN04 between @FromDate and @ToDate 
 AND  a.COLUMNA03=@AcOwner
 order by a.COLUMN02 desc
end
else if(@Project!='' and @Vendor!='' and @OperatingUnit!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project  and a.COLUMN07=@Vendor  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!='' and a.COLUMN20!='' and a.COLUMN04 between @FromDate and @ToDate  and (  a.COLUMNA03=@AcOwner)
order by a.COLUMN02 desc
end
else if(@Project!='' and @Vendor!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project  and a.COLUMN07=@Vendor AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@Project!='' and @OperatingUnit!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and a.COLUMN20=@Project  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner order by a.column02 desc
end
else if(@OperatingUnit!='' and @Vendor!='')
begin
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where  isnull((a.COLUMNA13),0)=0 and a.COLUMN20!='' and a.COLUMNA02=@OperatingUnit and a.COLUMN07=@Vendor AND  a.COLUMNA03=@AcOwner order by a.column02 desc
end
else if((@FromDate!='' or @ToDate!=''  or @Vendor!='' or @OperatingUnit!='' or @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
select (select  COLUMN05 from SATABLE001 where COLUMN02=a.COLUMN07)COLUMN07,a.COLUMN09,a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN08) COLUMN08,a.COLUMN11,a.COLUMN18,a.COLUMN12,a.COLUMN13,a.COLUMN14,(select column05 from SATABLE002 where COLUMN02=a.COLUMN15) COLUMN15,
(select column06 from MATABLE010 where COLUMN02=a.COLUMN16) COLUMN16,a.COLUMN17 ,c.COLUMN05,(case when a.COLUMN06='BILL' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN15)
 when a.COLUMN06='BILL PAYMENT' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN13) end)COLUMN04
from PUTABLE016 a left outer join PUTABLE005 b on b.COLUMN01=a.COLUMN05 and b.COLUMN26!=''
 left outer join PUTABLE014 d on d.COLUMN01=a.COLUMN05 and d.COLUMN23!=''
left outer  join PRTABLE001 c on (c.COLUMN02=d.COLUMN23 or  c.COLUMN02=b.COLUMN26 or c.COLUMN02=a.COLUMN20) 
where isnull((a.COLUMNA13),0)=0 and c.COLUMN02!='' and a.COLUMN20!='' and (a.COLUMN20=@Project or a.COLUMN04 between @FromDate and @ToDate or   a.COLUMN07=@Vendor or a.COLUMNA02=@OperatingUnit) AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end

end


ELSE IF  @ReportName='projectReceivableReport'
BEGIN
if((@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18=@Project  and a.column07= @Customer and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''))
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.column07= @Customer and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @OperatingUnit!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18=@Project  and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18=@Project  and a.COLUMN04 between @FromDate and @ToDate and a.column07= @Customer  AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if( @Customer!='' and @OperatingUnit!='' and @Project!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN18=@Project and  a.column07= @Customer  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--(case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
 where  isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!=''))
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.column07= @Customer and a.COLUMN04 between @FromDate and @ToDate AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Project!=''))
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN04 between @FromDate and @ToDate AND  a.COLUMNA03=@AcOwner  and a.COLUMN18=@Project 
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and b.COLUMN29!='' and a.COLUMN04 between @FromDate and @ToDate and  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if(@Customer!='' and @OperatingUnit!='' )
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
 where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.column07= @Customer and a.COLUMNA02=@OperatingUnit 
AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if( @Customer!=''  and @Project!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN18=@Project and  a.column07= @Customer   AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if( @OperatingUnit!='' and @Project!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN18=@Project   and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' or @ToDate!='' or @Customer!='' or @OperatingUnit!='' or @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and (a.COLUMN18=@Project  or a.column07= @Customer or a.COLUMN04 between @FromDate and @ToDate or a.COLUMNA02=@OperatingUnit) AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
END


ELSE IF  @ReportName='ProjectReceivableDueDates'
BEGIN
if((@FromDate!='' and @ToDate!='' and @Customer!='' and @OperatingUnit!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18=@Project  and a.column07= @Customer and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''))
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.column07= @Customer and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @OperatingUnit!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18=@Project  and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18=@Project  and a.COLUMN04 between @FromDate and @ToDate and a.column07= @Customer  AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if( @Customer!='' and @OperatingUnit!='' and @Project!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN18=@Project and  a.column07= @Customer  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
 where  isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.COLUMN04 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!=''))
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.column07= @Customer and a.COLUMN04 between @FromDate and @ToDate AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Project!=''))
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN04 between @FromDate and @ToDate AND  a.COLUMNA03=@AcOwner  and a.COLUMN18=@Project 
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and b.COLUMN29!='' and a.COLUMN04 between @FromDate and @ToDate and  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
else if(@Customer!='' and @OperatingUnit!='' )
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
 where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and a.column07= @Customer and a.COLUMNA02=@OperatingUnit 
AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if( @Customer!=''  and @Project!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN18=@Project and  a.column07= @Customer   AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if( @OperatingUnit!='' and @Project!='')
begin
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0  and a.COLUMN18=@Project   and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if((@FromDate!='' or @ToDate!='' or @Customer!='' or @OperatingUnit!='' or @Project!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
--EMPHCS1294 rajasekhar reddy patakota 10/10/2015 Receive Payment Edit and delete calculations
select  (select COLUMN03 from contable007 where COLUMN02=a.COLUMNA02)[Opearting Unit], 
--select  (case when a.COLUMN06='Invoice' then (select COLUMN03 from contable007 where COLUMN02=b.COLUMN14)
-- when a.COLUMN06='Payment' then (select COLUMN03 from contable007 where COLUMN02=d.COLUMN14) end)[Opearting Unit], 
 (select COLUMN05 from PRTABLE001 where COLUMN02=a.COLUMN18)[Project],
 a.COLUMN06,(select column04 from FITABLE001 where COLUMN02=a.COLUMN16 and COLUMNA03=a.COLUMNA03) COLUMN16,
(select column05 from SATABLE002 where COLUMN02=a.COLUMN07) COLUMN05, a.COLUMN04, a.COLUMN09,
a.COLUMN11,a.COLUMN12,a.COLUMN14,a.COLUMN15,a.COLUMN08 from putable018 a 
left outer join SATABLE009 b on b.COLUMN04=a.COLUMN05 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and b.COLUMN29!=''
left outer join SATABLE011 d on d.COLUMN04=a.COLUMN05 and d.COLUMNA02=a.COLUMNA02 and d.COLUMNA03=a.COLUMNA03 and d.COLUMN22!=''
where isnull((a.COLUMNA13),0)=0 and a.COLUMN18!='' and (a.COLUMN18=@Project  or a.column07= @Customer or a.COLUMN04 between @FromDate and @ToDate or a.COLUMNA02=@OperatingUnit) AND  a.COLUMNA03=@AcOwner 
order by a.COLUMN02 desc
end
END


ELSE IF  @ReportName='projectBankRegister'
BEGIN
if((@FromDate!='' and @ToDate!='' and @Type!=''  and @OperatingUnit!='' and @Project!=''))
begin
SELECT p.[COLUMN02 ] AS BankRegisterID,s.[COLUMN04 ] AS Bank,cast(p.[COLUMN04 ] as date) AS Date,
		p.[COLUMN05 ] AS BillInvoice,p.[COLUMN06] AS Type,p.[COLUMN09 ] AS Account,p.COLUMN14 AS Cheque,p.[COLUMN10 ] AS PaymentAmount,
		p.[COLUMN11 ] AS DepositAmount,p.[COLUMN12 ] AS BalanceAmount,
		(case when b.COLUMN23!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN23) when d.COLUMN26!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=d.COLUMN26) when f.COLUMN25!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=f.COLUMN25) end)Project,
	    (case when p.COLUMN06='BILL PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) when p.COLUMN06='PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=d.COLUMN15) when p.COLUMN06='EXPENCE' then (select  COLUMN03 from CONTABLE007 where COLUMN02=f.COLUMN10) end)[Operting Unit]
		From	PUTABLE019 p  left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 left outer join PUTABLE014 b on b.COLUMN01=p.COLUMN08
        left outer join PUTABLE005 d on d.COLUMN01=p.COLUMN08 left outer  join FITABLE012 f on f.COLUMN01=p.COLUMN08
where isnull((p.COLUMNA13),0)=0 and (b.COLUMN23!='' or d.COLUMN26!='' or f.COLUMN25!='') and p.column06= @Type and  p.COLUMN04 between @FromDate and @ToDate and p.COLUMNA02=@OperatingUnit AND  p.COLUMNA03=@AcOwner and (b.COLUMN22=@Project or d.COLUMN26=@Project)
order by p.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!='' and @Type!=''  and @OperatingUnit!=''))
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
where isnull((COLUMNA13),0)=0 and column06= @Type  and COLUMN04 between @FromDate and @ToDate  and COLUMNA02=@OperatingUnit AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!='' and @OperatingUnit!=''))
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
where isnull((COLUMNA13),0)=0 and column07= @Customer and COLUMN04 between @FromDate and @ToDate and COLUMNA02=@OperatingUnit AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
 where isnull((COLUMNA13),0)=0 and  COLUMN04 between @FromDate and @ToDate and COLUMNA02=@OperatingUnit AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if(@Type!='' and @Customer!='' and @OperatingUnit!='')
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
 where isnull((COLUMNA13),0)=0 and column06= @Type and column07= @Customer  and COLUMNA02=@OperatingUnit AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!='' and @Type!='' ))
begin
SELECT p.[COLUMN02 ] AS BankRegisterID,s.[COLUMN04 ] AS Bank,cast(p.[COLUMN04 ] as date) AS Date,
		p.[COLUMN05 ] AS BillInvoice,p.[COLUMN06] AS Type,p.[COLUMN09 ] AS Account,p.COLUMN14 AS Cheque,p.[COLUMN10 ] AS PaymentAmount,
		p.[COLUMN11 ] AS DepositAmount,p.[COLUMN12 ] AS BalanceAmount,
		(case when b.COLUMN23!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN23) when d.COLUMN26!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=d.COLUMN26) when f.COLUMN25!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=f.COLUMN25) end)Project,
	    (case when p.COLUMN06='BILL PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) when p.COLUMN06='PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=d.COLUMN15) when p.COLUMN06='EXPENCE' then (select  COLUMN03 from CONTABLE007 where COLUMN02=f.COLUMN10) end)[Operting Unit]
		From	PUTABLE019 p  left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 left outer join PUTABLE014 b on b.COLUMN01=p.COLUMN08
        left outer join PUTABLE005 d on d.COLUMN01=p.COLUMN08 left outer  join FITABLE012 f on f.COLUMN01=p.COLUMN08
where isnull((p.COLUMNA13),0)=0 and (b.COLUMN23!='' or d.COLUMN26!='' or f.COLUMN25!='') and p.column06= @Type and  p.COLUMN04 between @FromDate and @ToDate and p.COLUMNA02=@OperatingUnit AND  p.COLUMNA03=@AcOwner and (b.COLUMN22=@Project or d.COLUMN26=@Project)
order by p.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''  and @Customer!=''))
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
where isnull((COLUMNA13),0)=0 and column07= @Customer and COLUMN04 between @FromDate and @ToDate AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT p.[COLUMN02 ] AS BankRegisterID,s.[COLUMN04 ] AS Bank,cast(p.[COLUMN04 ] as date) AS Date,
		p.[COLUMN05 ] AS BillInvoice,p.[COLUMN06] AS Type,p.[COLUMN09 ] AS Account,p.COLUMN14 AS Cheque,p.[COLUMN10 ] AS PaymentAmount,
		p.[COLUMN11 ] AS DepositAmount,p.[COLUMN12 ] AS BalanceAmount,
		(case when b.COLUMN23!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN23) when d.COLUMN26!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=d.COLUMN26) when f.COLUMN25!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=f.COLUMN25) end)Project,
	    (case when p.COLUMN06='BILL PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) when p.COLUMN06='PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=d.COLUMN15) when p.COLUMN06='EXPENCE' then (select  COLUMN03 from CONTABLE007 where COLUMN02=f.COLUMN10) end)[Operting Unit]
		From	PUTABLE019 p  left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 left outer join PUTABLE014 b on b.COLUMN01=p.COLUMN08
        left outer join PUTABLE005 d on d.COLUMN01=p.COLUMN08 left outer  join FITABLE012 f on f.COLUMN01=p.COLUMN08
where isnull((p.COLUMNA13),0)=0 and (b.COLUMN23!='' or d.COLUMN26!='' or f.COLUMN25!='') and p.COLUMN04 between @FromDate and @ToDate and(   p.COLUMNA03=@AcOwner )
order by p.COLUMN02 desc
end
else if(@Type!='' and @Customer!='' )
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
 where isnull((COLUMNA13),0)=0 and column06= @Type and column07= @Customer  AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if(@Type!='' and @OperatingUnit!='' )
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
 where isnull((COLUMNA13),0)=0 and column06= @Type and COLUMNA02=@OperatingUnit AND  COLUMNA03=@AcOwner 
order by COLUMN02 desc
end
else if(@Customer!='' and @OperatingUnit!='' )
begin
select  COLUMN06,(select column04 from FITABLE001 where COLUMN02=putable018.COLUMN16) COLUMN16,(select column05 from SATABLE002 where COLUMN02=putable018.COLUMN07) COLUMN05, COLUMN04, COLUMN07,COLUMN09,COLUMN11,COLUMN12,COLUMN14,COLUMN15,COLUMN08 from putable018
 where isnull((COLUMNA13),0)=0 and column07= @Customer and COLUMNA02=@OperatingUnit 
AND  COLUMNA03=@AcOwner
order by COLUMN02 desc
end
else if(@Type!='' or @Customer!='' or @OperatingUnit!='' or @Project!='')
begin
SELECT p.[COLUMN02 ] AS BankRegisterID,s.[COLUMN04 ] AS Bank,cast(p.[COLUMN04 ] as date) AS Date,
		p.[COLUMN05 ] AS BillInvoice,p.[COLUMN06] AS Type,p.[COLUMN09 ] AS Account,p.COLUMN14 AS Cheque,p.[COLUMN10 ] AS PaymentAmount,
		p.[COLUMN11 ] AS DepositAmount,p.[COLUMN12 ] AS BalanceAmount,
		(case when b.COLUMN23!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN23) when d.COLUMN26!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=d.COLUMN26) when f.COLUMN25!='' then (select  COLUMN05 from PRTABLE001 where COLUMN02=f.COLUMN25) end)Project,
	    (case when p.COLUMN06='BILL PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMN13) when p.COLUMN06='PAYMENT' then (select  COLUMN03 from CONTABLE007 where COLUMN02=d.COLUMN15) when p.COLUMN06='EXPENCE' then (select  COLUMN03 from CONTABLE007 where COLUMN02=f.COLUMN10) end)[Operting Unit]
		From	PUTABLE019 p  left outer join  FITABLE001 s on s.COLUMN02=p.COLUMN03 left outer join PUTABLE014 b on b.COLUMN01=p.COLUMN08
        left outer join PUTABLE005 d on d.COLUMN01=p.COLUMN08 left outer  join FITABLE012 f on f.COLUMN01=p.COLUMN08
where isnull((p.COLUMNA13),0)=0 and (b.COLUMN23!='' or d.COLUMN26!='' or f.COLUMN25!='') and p.column06= @Type or  p.COLUMN04 between @FromDate and @ToDate or p.COLUMNA02=@OperatingUnit or  p.COLUMNA03=@AcOwner or (b.COLUMN22=@Project or d.COLUMN26=@Project)
order by p.COLUMN02 desc
end
END

ELSE IF  @ReportName='projectExpenses'
BEGIN
--EMPHCS1221 Expense tracking -Amount Paid ,Amount Spent and Amount Deposited.  All expense reprots -COA ,project and regular report BY RAJ.Jr 2/10/2015
if((@FromDate!='' and @ToDate!=''   and @OperatingUnit!='' and @Project!=''))
begin
SELECT (select  COLUMN04 from FITABLE001 where COLUMN02=a.COLUMN03 and COLUMNA03=a.COLUMNA03)COLUMN03,a.COLUMN07 as Date, cast(a.COLUMN04 as decimal(18,2)) as AmountPaid,cast((a.COLUMN04-a.COLUMN09) as decimal(18,2)) as AmountDeposited,(select  COLUMN09 from MATABLE010 where COLUMN02= a.COLUMN10) as Payee, a.COLUMN05, 
(select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN25) COLUMN07,(select  COLUMN03 from CONTABLE007
 where COLUMN02=b.COLUMN10)COLUMN10 FROM dbo.FITABLE021 a inner join FITABLE012 b on b.COLUMN01=a.COLUMN06 and b.COLUMN25!=''
where isnull((a.COLUMNA13),0)=0   and  a.COLUMN07 between @FromDate and @ToDate and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner and (b.COLUMN25=@Project)
order by a.COLUMN02 desc
end
else if((@FromDate!='' and @ToDate!=''   and @OperatingUnit!=''))
begin
--EMPHCS1149	Project Forms Functionality Changes BY RAJ.Jr 15/9/2015
SELECT (select  COLUMN04 from FITABLE001 where COLUMN02=a.COLUMN03 and COLUMNA03=a.COLUMNA03)COLUMN03,a.COLUMN07 as Date, cast(a.COLUMN04 as decimal(18,2)) as AmountPaid,cast((a.COLUMN04-a.COLUMN09) as decimal(18,2)) as AmountDeposited,(select  COLUMN09 from MATABLE010 where COLUMN02= a.COLUMN10) as Payee, a.COLUMN05, 
(select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN25) COLUMN07,(select  COLUMN03 from CONTABLE007
 where COLUMN02=b.COLUMN10)COLUMN10 FROM dbo.FITABLE021 a inner join FITABLE012 b on b.COLUMN01=a.COLUMN06 and b.COLUMN25!=''
where isnull((a.COLUMNA13),0)=0    and a.COLUMN07 between @FromDate and @ToDate  and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner
order by a.COLUMN02 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT (select  COLUMN04 from FITABLE001 where COLUMN02=a.COLUMN03 and COLUMNA03=a.COLUMNA03)COLUMN03,a.COLUMN07 as Date, cast(a.COLUMN04 as decimal(18,2)) as AmountPaid,cast((a.COLUMN04-a.COLUMN09) as decimal(18,2)) as AmountDeposited,(select  COLUMN09 from MATABLE010 where COLUMN02= a.COLUMN10) as Payee, a.COLUMN05, 
(select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN25) COLUMN07,(select  COLUMN03 from CONTABLE007
 where COLUMN02=b.COLUMN10)COLUMN10 FROM dbo.FITABLE021 a inner join FITABLE012 b on b.COLUMN01=a.COLUMN06 and b.COLUMN25!=''
where isnull((a.COLUMNA13),0)=0  and  a.COLUMN07  between @FromDate and @ToDate and  a.COLUMNA03 in (@AcOwner)
order by a.COLUMN02 desc
end
else if( @OperatingUnit!='' and @Project!='')
begin
SELECT (select  COLUMN04 from FITABLE001 where COLUMN02=a.COLUMN03 and COLUMNA03=a.COLUMNA03)COLUMN03,a.COLUMN07 as Date, cast(a.COLUMN04 as decimal(18,2)) as AmountPaid,cast((a.COLUMN04-a.COLUMN09) as decimal(18,2)) as AmountDeposited,(select  COLUMN09 from MATABLE010 where COLUMN02= a.COLUMN10) as Payee, a.COLUMN05, 
(select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN25) COLUMN07,(select  COLUMN03 from CONTABLE007
 where COLUMN02=b.COLUMN10)COLUMN10 FROM dbo.FITABLE021 a inner join FITABLE012 b on b.COLUMN01=a.COLUMN06 and b.COLUMN25!=''
where isnull((a.COLUMNA13),0)=0   and a.COLUMNA02=@OperatingUnit AND  a.COLUMNA03=@AcOwner and b.COLUMN25=@Project
order by a.COLUMN02 desc
end
if((@FromDate!='' or @ToDate!=''   or @OperatingUnit!='' or @Project!=''))
begin
SELECT (select  COLUMN04 from FITABLE001 where COLUMN02=a.COLUMN03 and COLUMNA03=a.COLUMNA03)COLUMN03,a.COLUMN07 as Date, cast(a.COLUMN04 as decimal(18,2)) as AmountPaid,cast((a.COLUMN04-a.COLUMN09) as decimal(18,2)) as AmountDeposited,(select  COLUMN09 from MATABLE010 where COLUMN02= a.COLUMN10) as Payee, a.COLUMN05, 
(select  COLUMN05 from PRTABLE001 where COLUMN02=b.COLUMN25) COLUMN07,(select  COLUMN03 from CONTABLE007
 where COLUMN02=b.COLUMN10)COLUMN10 FROM dbo.FITABLE021 a inner join FITABLE012 b on b.COLUMN01=a.COLUMN06 and b.COLUMN25!=''
where isnull((a.COLUMNA13),0)=0   and  (a.COLUMN07 between @FromDate and @ToDate or a.COLUMNA02=@OperatingUnit or b.COLUMN25=@Project) AND  a.COLUMNA03=@AcOwner and b.COLUMN25!=''
order by a.COLUMN02 desc
end
END
ELSE IF  @ReportName='SOByCustomer'
BEGIN
if(@FromDate!='' and @ToDate!='' and @UPC!='' and @OperatingUnit!='')
begin 
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN05) item,
b.COLUMN03 upc,a.COLUMN04 sono,a.COLUMN08  dt, 
b.COLUMN10 qty,b.COLUMN14 amt 
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and a.COLUMN19=1002 and
 a.COLUMN08 between @FromDate and @ToDate AND b.COLUMN03= @UPC  AND a.COLUMN14= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @UPC!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN05) item,
b.COLUMN03 upc,a.COLUMN04 sono,a.COLUMN08  dt, 
b.COLUMN10 qty,b.COLUMN14 amt 
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and a.COLUMN19=1002 and
 a.COLUMN08 between @FromDate and @ToDate AND b.COLUMN03= @UPC  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN05) item,
b.COLUMN03 upc,a.COLUMN04 sono,a.COLUMN08  dt, 
b.COLUMN10 qty,b.COLUMN14 amt 
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and a.COLUMN19=1002 and
 a.COLUMN08 between @FromDate and @ToDate  AND a.COLUMN14= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if( @UPC!='' and @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN05) item,
b.COLUMN03 upc,a.COLUMN04 sono,a.COLUMN08  dt, 
b.COLUMN10 qty,b.COLUMN14 amt 
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and a.COLUMN19=1002 and
b.COLUMN03= @UPC  AND a.COLUMN14= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if( @UPC!='' or @OperatingUnit!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN05) item,
b.COLUMN03 upc,a.COLUMN04 sono,a.COLUMN08  dt, 
b.COLUMN10 qty,b.COLUMN14 amt 
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and a.COLUMN19=1002 and
 a.COLUMN08 between @FromDate and @ToDate AND b.COLUMN03= @UPC  AND a.COLUMN14= @OperatingUnit AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
end
else if(@FromDate!='' and @ToDate!='')
begin
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMN14) ou,
(select COLUMN05 from SATABLE002 where COLUMN02=a.COLUMN05) customer,
(select COLUMN04 from MATABLE007 where COLUMN02=b.COLUMN05) item,
b.COLUMN03 upc,a.COLUMN04 sono,a.COLUMN08  dt, 
b.COLUMN10 qty,b.COLUMN14 amt 
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 where isnull((a.COLUMNA13),0)=0 and a.COLUMN19=1002 and
 a.COLUMN08 between @FromDate and @ToDate  AND  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  a.COLUMNA03=@AcOwner
order by a.COLUMN08 desc
END
END
--EMPHCS1210	Inventory by UOM Report BY RAJ.Jr 25/9/2015
ELSE IF  @ReportName='InventoryByUOM'
BEGIN
if @OperatingUnit!=''
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Item!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
else
begin
set @whereStr=@whereStr+' and ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end
select * into #InventoryByUOM from (
SELECT m.COLUMN02, F.COLUMN17 AP,m2.COLUMN01,m.COLUMN04, isnull(m2.COLUMN04,'EACH') AS UOM, f.COLUMN04 AS Qty, m.COLUMN17,(case when isnull(sp1.COLUMN04,0)>0 then isnull(sp1.COLUMN04,0) else isnull(m.COLUMN17,0) end) Pprice,
(case when isnull(sp.COLUMN04,0)>0 then isnull(sp.COLUMN04,0) else isnull(m.COLUMN51,0) end) SPrices,sp.COLUMN05 SMRP,f.COLUMN03 as ItemID,isnull(f.COLUMN13,0) OPID,m.COLUMN50 DESCR,PR.COLUMN05 Project FROM FITABLE010   AS f
 LEFT OUTER JOIN MATABLE007 AS m ON  m.COLUMN02 = f.COLUMN03
 left outer join MATABLE024 sp on sp.COLUMN07=m.COLUMN02 and sp.COLUMN06='Sales' and (isnull(sp.COLUMN03,0)=m.COLUMNA02  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03=m.COLUMNA03 and  sp.COLUMNA13=0 
 left outer join MATABLE024 sp1 on sp1.COLUMN07=m.COLUMN02 and sp1.COLUMN06='Purchase' and (isnull(sp1.COLUMN03,0)=m.COLUMNA02  or isnull(sp1.COLUMN03,0)=0) and         sp1.COLUMNA03=m.COLUMNA03 and sp1.COLUMNA13=0 
 LEFT OUTER JOIN MATABLE002 AS m2 ON m2.COLUMN02 = f.COLUMN19  
 LEFT OUTER JOIN PRTABLE001 PR ON PR.COLUMN02 = f.COLUMN23 AND  PR.COLUMNA03 = f.COLUMNA03 AND ISNULL(PR.COLUMNA13,0)=0
where  (f.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)) AND f.COLUMNA03=@AcOwner  and isnull(f.columna13,0)=0
GROUP BY m.COLUMN04, m2.COLUMN04, f.COLUMN04, F.COLUMN17,m.COLUMN17 ,m2.COLUMN04,m2.COLUMN01,m.COLUMN51,sp.COLUMN06,sp.COLUMN04,m.COLUMN02,sp1.COLUMN04,sp.COLUMN05,f.COLUMN03,f.COLUMN13,m.COLUMN50,PR.COLUMN05
having sum(isnull(f.COLUMN04,0))>0)Query
set @Query1='select * from #InventoryByUOM '+@whereStr+' order by  COLUMN01 asc'
exec (@Query1) 
end
END
--case IsNumeric(m.COLUMN04) when 1 then Replicate(''0'', 100 - Len(m.COLUMN04)) + m.COLUMN04 else m.COLUMN04 end,


























GO
