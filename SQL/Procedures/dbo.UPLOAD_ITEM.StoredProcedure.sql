USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[UPLOAD_ITEM]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[UPLOAD_ITEM] 
AS 
BEGIN
declare @AccountTable Table(RowIds int Identity(1,1),ITEM_INTERNAL_ID INT,UPC NVARCHAR(250),TotalValue decimal(18,2),
UOM int,QOH decimal(18,2),PRICE DECIMAL(18,2),LOT NVARCHAR(250),LOC NVARCHAR(250))
declare @REFERANCE nvarchar(50)
declare @DIRECTION nvarchar(50)='Insert'
declare @TABLENAME nvarchar(50)='FITABLE014'
declare @OPUNIT nvarchar(50)
declare @ACOWNER nvarchar(50)
declare @TRANSDATE nvarchar(50)='04/01/2017'
declare @ReferredForm nvarchar(50)='1634'
declare @AdjustmntNo nvarchar(50)
declare @InternalID nvarchar(50)
declare @TotalValue nvarchar(50)
declare @ADJLINENO nvarchar(50)
declare @ITEM nvarchar(50)
declare @UOM nvarchar(50)
declare @UPC nvarchar(50)
declare @QOH nvarchar(50)
declare @TOTVALUE nvarchar(50)
declare @INCREASE nvarchar(50)
declare @DECREASE nvarchar(50)
declare @REMANING nvarchar(50)
declare @CURRENTQTY nvarchar(50)
declare @ESTEMATEDPRICE nvarchar(50)
declare @LOT nvarchar(50)
declare @PRICE nvarchar(50)
declare @LOTNO nvarchar(50)
declare @CREATEDBY nvarchar(50)='1251'
declare @flgAcconts int
declare @cntAccounts int
declare @LOTID nvarchar(50)
declare @OPTID nvarchar(50)
declare @LOCATION nvarchar(50)
set @flgAcconts=1
SET  @OPUNIT=56751
SET @ACOWNER=56608
Insert into @AccountTable select M.COLUMN02, I.UPC,I.QTY*iif(M.COLUMN17=1 or m.column17 is null,i.price,m.column17),m2.column02,I.QTY,iif(M.COLUMN17=1 or m.column17 is null,i.price,m.column17),I.LOT,I.LOCATION from dbo.MATABLE007 M
INNER JOIN ITEM_UPLOAD I ON I.ITEM_NAME=M.COLUMN04 AND M.COLUMNA03=@ACOWNER  and( m.columna02 is null or m.columna02=56751 ) --and i.PRICE=m.column51
left JOIN matable002 m2 ON m2.column03=11119 AND M2.COLUMN04=i.uom and m2.columna03=i.ACCOUNT_OWNER
 where isnull(I.QTY,0)!=0  and i.OPERATING_UNIT=56751  and m.columna13=0 AND CAST(I.ITEM_INTERNAL_ID AS INT) BETWEEN 1 AND 500 --and i.ITEM_INTERNAL_ID>=6001 --and i.ITEM_INTERNAL_ID<=6000 
 --where  DATEDIFF(DAY,Getdate(),expiry_date) <=15
select @cntAccounts=COUNT(1) from @AccountTable
--print 'step 1'
print @cntAccounts
if @cntAccounts >= 1
begin
declare @NO int
while (@flgAcconts <=@cntAccounts)
begin
	set @AdjustmntNo='IA'
	set @LOTNO=NULL
	set @LOTID=0
	SET  @NO =0
      select @TotalValue=TotalValue,@ITEM=ITEM_INTERNAL_ID,@UOM=UOM,@QOH=QOH,@TOTVALUE=TotalValue,@UPC=UPC,@PRICE=PRICE,@LOCATION=LOC
	  from @AccountTable where RowIds=@flgAcconts
	--print 'step 2'
	  set @InternalID=(select isnull(max(column02),0)+1 from FITABLE014)
	  --set @LOTID=(select isnull(max(column02),0)+1 from [FITABLE043])
	  set @OPTID=(select isnull(max(column02),0)+1 from [FITABLE047])
	  set @NO=(@NO+@flgAcconts)
	  set @AdjustmntNo=(@AdjustmntNo+(right('00000'+convert(nvarchar(10),@NO),5)))
	  --set @OPTID=(@OPTID+cast(@NO as nvarchar(250)))		
	  --set @LOTNO=(@LOTNO+cast(@NO as nvarchar(250)))	
	  --lot creation
		--INSERT [dbo].[FITABLE043] ( [COLUMN02], [COLUMN03], [COLUMN04], [COLUMN05], [COLUMN06], [COLUMN07], 
		--[COLUMN08], [COLUMN09], [COLUMN10], [COLUMN11], [COLUMN12], [COLUMNA01], [COLUMNA02], [COLUMNA03], [COLUMNA04], 
		--[COLUMNA05], [COLUMNA06], [COLUMNA07], [COLUMNA08], [COLUMNA09], [COLUMNA10], [COLUMNA11], [COLUMNA12], [COLUMNA13], 
		--[COLUMNB01], [COLUMNB02], [COLUMNB03], [COLUMNB04], [COLUMNB05], [COLUMNB06], [COLUMNB07], [COLUMNB08], [COLUMNB09], 
		--[COLUMNB10], [COLUMNB11], [COLUMNB12], [COLUMND01], [COLUMND02], [COLUMND03], [COLUMND04], [COLUMND05], [COLUMND06], 
		--[COLUMND07], [COLUMND08], [COLUMND09], [COLUMND10]) 
		--VALUES ( @LOTID, 1515, @LOTNO, GETDATE(), GETDATE(), GETDATE()+365, 0, @ITEM, @OPUNIT, NULL, NULL, NULL, @OPUNIT, @ACOWNER, NULL, NULL, GETDATE(), GETDATE(), @CREATEDBY, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
	    
	  --ADJUSTMENT HEADER TABLE
	  --opening balance item table 
--	  INSERT [dbo].[FITABLE047] ([COLUMN02], [COLUMN03], [COLUMN12], [COLUMN17], [COLUMN04], [COLUMN05], [COLUMN06], 
--[COLUMN07], [COLUMN08], [COLUMN09], [COLUMN10], [COLUMN11], [COLUMN13], [COLUMN14], [COLUMN15], [COLUMN16], [COLUMN18], 
--[COLUMN19], [COLUMN20], [COLUMN21], [COLUMN22], [COLUMNA01], [COLUMNA02], [COLUMNA03], [COLUMNA04], [COLUMNA05], [COLUMNA06],
-- [COLUMNA07], [COLUMNA08], [COLUMNA09], [COLUMNA10], [COLUMNA11], [COLUMNA12], [COLUMNA13], [COLUMNB01], [COLUMNB02], 
-- [COLUMNB03], [COLUMNB04], [COLUMNB05], [COLUMNB06], [COLUMNB07], [COLUMNB08], [COLUMNB09], [COLUMNB10], [COLUMNB11], 
-- [COLUMNB12], [COLUMND01], [COLUMND02], [COLUMND03], [COLUMND04], [COLUMND05], [COLUMND06], [COLUMND07], [COLUMND08], 
-- [COLUMND09], [COLUMND10]) VALUES (@OPTID, @ITEM, @TOTVALUE, @PRICE, 
-- @QOH, NULL, 10002, N'OB00001', getdate(), NULL, NULL, NULL, @OPUNIT,
--  NULL, NULL, NULL, NULL, @UOM, NULL, 0, NULL, NULL, @OPUNIT, @ACOWNER, NULL, NULL, getdate(), getdate(), NULL, NULL, NULL, NULL, NULL,
--   NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
--    NULL, NULL, NULL)
	  exec usp_FI_TP_FITABLE014 @InternalID,@ReferredForm,@AdjustmntNo,@TRANSDATE,'1100',@TotalValue,'','',@OPUNIT,NULL,NULL,'','','',@LOCATION,
								NULL,@OPUNIT,@ACOWNER,NULL,NULL,@TRANSDATE,@TRANSDATE,@CREATEDBY,NULL,NULL,NULL,1,0,NULL,NULL,NULL
								,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@DIRECTION,@TABLENAME,'0',''
	--print 'step 3'

	  set @ADJLINENO=(select isnull(max(column02),0)+1 from FITABLE015)
	  set @REFERANCE=(select isnull(max(column01),0) from FITABLE014)
	  set @TABLENAME='FITABLE015'
	  --ADJUSTMENT LINE TABLE
	  exec usp_FI_TP_FITABLE015 @ADJLINENO,@ITEM,@UPC,'',0,0,@QOH,@QOH,@PRICE,@REFERANCE,@OPUNIT,NULL,NULL,'',0,@QOH,@UOM,@QOH,NULL,NULL,@UOM,@LOTID,
								NULL,@OPUNIT,@ACOWNER,NULL,NULL,@TRANSDATE,@TRANSDATE,@CREATEDBY,NULL,NULL,NULL,1,0,NULL,NULL,NULL
								,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@DIRECTION,@TABLENAME,''
	  
      --print 'step 4'
	  set @flgAcconts=@flgAcconts+1
      end 
	  end
	  END
GO
