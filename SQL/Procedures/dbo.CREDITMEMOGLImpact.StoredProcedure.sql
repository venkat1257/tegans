USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CREDITMEMOGLImpact]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CREDITMEMOGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,S5.column09 Memo,sum(p18.COLUMN12) Credit,0 Debit,1 id  from SATABLE005 S5
left outer join putable018 p18 on p18.COLUMN05= S5.COLUMN04 and p18.COLUMN07= S5.COLUMN05 and p18.COLUMNA03= S5.COLUMNA03 and p18.COLUMNA02= S5.COLUMNA02 and p18.COLUMN06='Credit Memo' and isnull(p18.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p18.COLUMN03
where S5.COLUMN02=@ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0
group by f1.COLUMN04,S5.column09
union all
select f1.COLUMN04 Account,S5.column09 Memo,0 Credit,sum(p16.COLUMN14) Debit,2 id  from SATABLE005 S5
left outer join putable016 p16 on p16.COLUMN09= S5.COLUMN04 and p16.COLUMNA03= S5.COLUMNA03 and p16.COLUMNA02= S5.COLUMNA02 and p16.COLUMN06='Credit Memo' and isnull(p16.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p16.COLUMN03
where S5.COLUMN02=@ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0
group by f1.COLUMN04,S5.column09
union all
select f1.COLUMN04 Account,S5.column09 Memo,0 Credit,-sum(f36.COLUMN08) Debit,3 id from SATABLE005 S5
inner join fitable036 f36 on f36.COLUMN09 = S5.COLUMN04 and f36.COLUMNA03= S5.COLUMNA03 and f36.COLUMNA02= S5.COLUMNA02 
and f36.COLUMN03 = 'Credit Memo' and isnull(f36.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f36.COLUMN10
where  S5.COLUMN02= @ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0
group by f1.COLUMN04,S5.column09
--union all
--select f1.COLUMN04 Account,S5.column09 Memo,sum(p17.COLUMN10) Credit,0 Debit,4 id  from SATABLE005 S5
--inner join putable017 p17 on  p17.COLUMN05= S5.COLUMN01 and p17.COLUMNA03= S5.COLUMNA03 and p17.COLUMNA02= S5.COLUMNA02 
--and p17.COLUMN06 = 'Credit Memo' and isnull(p17.COLUMNA13,0)=0
--left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03
--where  S5.COLUMN02= @ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0
--group by f1.COLUMN04,S5.column09
union all
select f1.COLUMN04 Account,S5.column09 Memo,-sum(f25.COLUMN11) Credit,sum(f25.COLUMN09) Debit,5 id from SATABLE005 S5
left outer join SATABLE006 S6 on S6.COLUMN19=S5.COLUMN01 and  S6.COLUMNA03= S5.COLUMNA03 and  S6.COLUMNA02= S5.COLUMNA02
inner join fitable025 f25 on f25.COLUMN05 = S6.COLUMN01 and f25.COLUMNA03= S6.COLUMNA03 and f25.COLUMNA02= S6.COLUMNA02 and f25.COLUMN04 = 'Credit Memo' and isnull(f25.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f25.COLUMN08
where  S5.COLUMN02= @ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0 and f1.COLUMN02!= 1056
group by f1.COLUMN04,S5.column09
UNION ALL
select f1.COLUMN04 Account,S5.column09 Memo,-sum(f34.COLUMN07) Credit,-sum(f34.COLUMN08) Debit,6 id from SATABLE005 S5
left outer join fitable034 f34 on f34.COLUMN09= S5.COLUMN04 and f34.COLUMNA03= S5.COLUMNA03 and f34.COLUMNA02= S5.COLUMNA02 and isnull(f34.COLUMNA13,0)=0 and f34.COLUMN03 = 'Credit Memo'
left outer join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10
where  S5.COLUMN02= @ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0
group by  f1.COLUMN04,S5.column09
UNION ALL
select f1.COLUMN04 Account,S5.column09 Memo,-sum(f34.COLUMN08) Credit,sum(f34.COLUMN07) Debit,7 id from SATABLE005 S5
left outer join fitable035 f34 on f34.COLUMN09= S5.COLUMN04 and f34.COLUMNA03= S5.COLUMNA03 and f34.COLUMNA02= S5.COLUMNA02 and isnull(f34.COLUMNA13,0)=0 and f34.COLUMN03 = 'Credit Memo'
left outer join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10
where  S5.COLUMN02= @ide and S5.COLUMNA03=@AcOwner and f1.COLUMNA03 = S5.COLUMNA03 and isnull(S5.COLUMNA13,0)=0
group by  f1.COLUMN04,S5.column09
union all
select f26.COLUMN17 Account,S5.column09 Memo,-sum(f26.COLUMN12) Credit,sum(f26.COLUMN11) Debit,8 id from SATABLE005 S5
left outer join fitable026 f26 on f26.COLUMN09 = S5.COLUMN04 and  f26.COLUMN06 = S5.COLUMN05 and f26.COLUMNA03= S5.COLUMNA03 
and f26.COLUMNA02= S5.COLUMNA02 and f26.COLUMN04 = 'Credit Memo' and isnull(f26.COLUMNA13,0)=0
left outer join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 
where  S5.COLUMN02= @ide and S5.COLUMNA03=@AcOwner and isnull(S5.COLUMNA13,0)=0
group by f26.COLUMN17,S5.column09
)
select [Account],[Memo],[Credit],[Debit],[id] from MyTable
group by [Account],[Memo],[Credit],[Debit],[id]  order by id asc
end



GO
