USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PQ_Line_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_PQ_Line_DATA]
(
	@Vendor  int =null,
	@item int = null,
	@Status varchar(250) = null,
	@MonthStartDate varchar(250) = null,
	@MonthEndDate varchar(250) = null,
	@OPUnit varchar(250) = null,
	@projectid  int = null,
	@quotationid  int =null,
	@AcOwner int=null
)
AS
BEGIN
set @Status=(select column04 from CONTABLE025 where COLUMN02=65)
set @MonthStartDate=( SELECT DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0))
set @MonthEndDate=( SELECT DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0)))
 if(@projectid!='' and @item!='')
begin
SELECT v.COLUMN05 Vendor, a.COLUMN04 Quotation,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and  a.COLUMN08>=@MonthStartDate  
 and a.COLUMN16=@Status and a.COLUMN36 in(@projectid)  and b.COLUMN03=@item and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) order by b.COLUMN11 asc
end
else if(@vendor!='' and @item!='')
begin
SELECT v.COLUMN05 Vendor,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN04 Quotation, a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and a.COLUMN08>=@MonthStartDate 
and a.COLUMN05=@Vendor and a.COLUMN16=@Status and b.COLUMN03=@item and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) order by b.COLUMN11 asc
end
else if(@vendor!='')
begin
if(@vendor=1)
begin 
SELECT v.COLUMN05 Vendor,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN04 Quotation, a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and a.COLUMN08>=@MonthStartDate 
 and a.COLUMN16=@Status order by b.COLUMN11 asc
end
else
begin
SELECT v.COLUMN05 Vendor,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN04 Quotation, a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  a.COLUMN08>=@MonthStartDate  
 and a.COLUMN16=@Status and a.COLUMN05=@Vendor order by b.COLUMN11 asc
end
end
else if(@quotationid!='')
begin
SELECT v.COLUMN05 Vendor,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN04 Quotation, a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and  a.COLUMN08>=@MonthStartDate 
 and a.COLUMN16=@Status and a.COLUMN02=@quotationid  and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) order by b.COLUMN11 asc
end
else if(@item!='')
begin
if(@item=1)
begin 
SELECT v.COLUMN05 Vendor,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN04 Quotation, a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and  a.COLUMN08>=@MonthStartDate  
 and a.COLUMN16=@Status order by b.COLUMN11 asc
end
else
begin
SELECT v.COLUMN05 Vendor, a.COLUMN04 Quotation,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and  a.COLUMN08>=@MonthStartDate  
 and a.COLUMN16=@Status  and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and b.COLUMN03=@item order by b.COLUMN11 asc
end
end
else if(@projectid!='')
begin
SELECT v.COLUMN05 Vendor, a.COLUMN04 Quotation,m.COLUMN04 Item,b.COLUMN07 Qty, b.COLUMN09 Price, b.COLUMN24 Amount, b.COLUMN10 Discount,
b.COLUMN11 Total,p.COLUMN04 [Payment Terms], a.COLUMN02 QuotationNO, b.COLUMN02 QItemID
FROM PUTABLE001 a   inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 inner join satable001 v on a.COLUMN05=v.column02 
inner join matable007 m on b.COLUMN03=m.column02  left outer join matable002 p on a.COLUMN10=p.COLUMN02  
where  a.COLUMN03=1374 and a.COLUMN35 between @MonthStartDate and @MonthEndDate  and  a.COLUMN08>=@MonthStartDate  
 and a.COLUMN16=@Status and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and a.COLUMN36 in(@projectid) order by b.COLUMN11 asc
end
end

GO
