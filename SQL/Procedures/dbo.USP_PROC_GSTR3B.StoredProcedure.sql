USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTR3B]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_PROC_GSTR3B](@FROMDT NVARCHAR(250),
	@TODT NVARCHAR(250),
	@AcOwner NVARCHAR(25),
	@OPUnit NVARCHAR(250) =NULL,
	@OPERATING NVARCHAR(250) = NULL,
	@SEC NVARCHAR(250) = NULL,
	@FORMATDT NVARCHAR(250) = NULL,@whereStr nvarchar(max)='')
AS
BEGIN
if(@OPUnit!='')
begin
	set @whereStr=@whereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)'
end
if(@OPERATING!='')
begin
	set @whereStr=@whereStr+' and S9.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPERATING+''') s)'
end
if(@whereStr='' or @whereStr is null)
begin
	set @whereStr=' and 1=1'
end
if(@SEC = '1')
BEGIN
exec('select [Nature of Supplies],sum(SUBTOTAL)SUBTOTAL,sum(IGSTAMT)IGSTAMT,sum(CGSTAMT)CGSTAMT,sum(SGSTAMT)SGSTAMT,sum(CESSTAMT)CESSTAMT from(
SELECT ''( a) Outward taxable supplies (other than zero rated, nil rated and exempted)'' [Nature of Supplies],''0'' SUBTOTAL,''0'' IGSTAMT,''0'' CGSTAMT, ''0'' SGSTAMT,''0'' CESSTAMT
UNION ALL
SELECT ''( a) Outward taxable supplies (other than zero rated, nil rated and exempted)'' [Nature of Supplies],cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0)  as decimal(18,2)) SUBTOTAL,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' and (a.COLUMN21!=1000) and  a.COLUMN21!=''NILL'' and a.COLUMN21!=''NIL'' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
--)A group by [Nature of Supplies] having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))>0
UNION ALL
SELECT ''( a) Outward taxable supplies (other than zero rated, nil rated and exempted)'' [Nature of Supplies],-cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)-isnull(a.COLUMN10,0) as decimal(18,2)) SUBTOTAL,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+'''
 --and s9.column67 is not null and s9.column67 != '''' 
 and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13,a.COLUMN10--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
--UNION ALL
--SELECT ''( a) Outward taxable supplies (other than zero rated, nil rated and exempted)'' [Nature of Supplies],cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)as decimal(18,2))SUBTOTAL,
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
--FROM PUTABLE001 S9  
--inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
----inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
--left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
--WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.column70 is not null and s9.column70 != '''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
--group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT ''( a) Outward taxable supplies (other than zero rated, nil rated and exempted)'' [Nature of Supplies],
cast(IIF(S9.COLUMN27 =22713,isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)),0),isnull(cast(s9.COLUMN19 as decimal(18,2)),0)) as decimal(18,2))
SUBTOTAL,
cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM FITABLE023 S9  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(isnull(s9.COLUMN12,0),''-'',''''))) s)) or t.COLUMN02=isnull(s9.COLUMN12,0)) and isnull(cast(s9.COLUMN19 as decimal(18,2)),0)!=0 and t.COLUMNA03=s9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1386 and s9.column05 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+'
AND ISNULL(S9.columna13,0)=0
group by S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S9.COLUMN19,S9.COLUMN27,S9.COLUMN24

)A group by [Nature of Supplies] --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))>0
UNION ALL
select ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],''0'' SUBTOTAL,''0'' IGSTAMT,''0'' CGSTAMT, ''0'' SGSTAMT,''0'' CESSTAMT
UNION ALL
SELECT ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
inner join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND '''+@TODT+''' and isnull(t.COLUMN07,0) = 0 and isnull(S9.COLUMNA13,0)=0 and  s9.columna03 ='+@AcOwner+' '+@whereStr+' 
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,t.COLUMN07--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
--)A group by IGSTAMT,CGSTAMT,SGSTAMT,CESSTAMT having ((IGSTAMT)+(CGSTAMT)+(SGSTAMT)+(CESSTAMT))=0
UNION ALL
--select ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],-cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)as decimal(18,2))SUBTOTAL,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 ='+@AcOwner+' '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
--select ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( b) Outward taxable supplies (zero rated)'' [Nature of Supplies],cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)as decimal(18,2))SUBTOTAL,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 ='+@AcOwner+' '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A group by IGSTAMT,CGSTAMT,SGSTAMT,CESSTAMT having ((IGSTAMT)+(CGSTAMT)+(SGSTAMT)+(CESSTAMT))=0
UNION ALL
select ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],''0'' SUBTOTAL,''0'' IGSTAMT,''0'' CGSTAMT, ''0'' SGSTAMT,''0'' CESSTAMT
UNION ALL
SELECT ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN35,0)as decimal(18,2))SUBTOTAL,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND '''+@TODT+''' and a.column21 in( 1000,0) and isnull(S9.COLUMNA13,0)=0 and s9.columna03 ='+@AcOwner+' '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
--)A having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT)) is NULL
UNION ALL
--select ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],-cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 ='+@AcOwner+' and a.COLUMN26 =1000 '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05

--UNION ALL
----select ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
--SELECT ''( c) Other outward supplies, (Nil rated, exempted)'' [Nature of Supplies],cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)as decimal(18,2))SUBTOTAL,
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
--cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
--FROM PUTABLE001 S9  
--inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
----inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
--left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
--WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 ='+@AcOwner+' '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
--group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT)) is NULL
UNION ALL
select ''( d) Inward supplies (liable to reverse charge)'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( d) Inward supplies (liable to reverse charge)'' [Nature of Supplies],''0'' SUBTOTAL,''0'' IGSTAMT,''0'' CGSTAMT,''0'' SGSTAMT,''0'' CESSTAMT
UNION ALL
SELECT ''( d) Inward supplies (liable to reverse charge)'' [Nature of Supplies],
--cast(isnull(a.COLUMN09,0)*isnull(a.COLUMN11,0)as decimal(18,2))
cast(isnull(a.COLUMN09,0)*isnull(a.COLUMN11,0)-(CASE WHEN ISNULL(a.COLUMN31,0)=22556 THEN (((round(CAST(ISNULL(a.COLUMN09,0) AS decimal(18,7)),0)*CAST(ISNULL(a.COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(a.COLUMN32,0) AS decimal(18,2))/100))) ELSE (cast(ISNULL(a.COLUMN32,0) AS decimal(18,2))) END)as decimal(18,2))
SUBTOTAL,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.COLUMN58 = 1  and s9.columna03 ='+@AcOwner+' '+@whereStr+' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33,a.COLUMN31,a.COLUMN32--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT ''( d) Inward supplies (liable to reverse charge)'' [Nature of Supplies],-cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)as decimal(18,2))SUBTOTAL,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 ='+@AcOwner+' '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0 and isnull(S9.COLUMN73,0)=1
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13
)A having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''( e) Non GST outward supplies'' [Nature of Supplies],isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT ''( e) Non GST outward supplies'' [Nature of Supplies],''0'' SUBTOTAL,''0'' IGSTAMT,''0'' CGSTAMT, ''0'' SGSTAMT,''0'' CESSTAMT
UNION ALL
SELECT ''( e) Non GST outward supplies'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,
0 
CGSTAMT, 
0 SGSTAMT,
0 CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND '''+@TODT+''' and (s9.column64 is null or s9.column64 = '''')  and s9.columna03 ='+@AcOwner+' and isnull(S9.COLUMNA13,0)=0 '+@whereStr+'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
')
END
else if(@SEC = '2')
BEGIN
exec('
select  [Nature of Supplies],max(PLACE),isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT from(
select ''Supplies made to Unregistered Persons'' [Nature of Supplies],''''PLACE,''0''SUBTOTAL,''0''IGSTAMT
UNION ALL
SELECT ''Supplies made to Unregistered Persons'' [Nature of Supplies],(s.column06+''-''+s.column05) PLACE,
 --cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)-isnull(a.COLUMN19,0) as decimal(18,2)) 
 0 SUBTOTAL,
0 IGSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE017 s on S9.COLUMN66=s.COLUMN02 --and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
--left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and (s9.column64 is null or s9.column64 = '''')  and s9.columna03 = '+@AcOwner+' and isnull(S9.COLUMNA13,0)=0 '+   @whereStr +'
--group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S.COLUMN05,S.COLUMN06
UNION ALL
SELECT ''Supplies made to Unregistered Persons'' [Nature of Supplies],(s.column06+''-''+s.column05) PLACE, 0 SUBTOTAL,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE017 s on S9.COLUMN66=s.COLUMN02 --and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and (s9.column64 is null or s9.column64 = '''')  and s9.columna03 = '+@AcOwner+' and isnull(S9.COLUMNA13,0)=0 '+   @whereStr +'
--group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S.COLUMN05,S.COLUMN06
UNION ALL
SELECT ''Supplies made to Unregistered Persons'' [Nature of Supplies],(s.column06+''-''+s.column05) PLACE,
-cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)-isnull(a.COLUMN10,0) as decimal(18,2)) SUBTOTAL,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
inner join MATABLE017 s on S9.COLUMN69=s.COLUMN02 --and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+'''
 --and s9.column67 is not null and s9.column67 != '''' 
 and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0
 group by s.COLUMN06,s.COLUMN05,a.COLUMN07,a.COLUMN09,a.COLUMN10,a.COLUMN32
)A group by [Nature of Supplies] --having (sum(IGSTAMT))!=0
UNION ALL

select [Nature of Supplies],max(PLACE),isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT from(
select ''Supplies made to Composition Taxable Persons'' [Nature of Supplies],''''PLACE,''0''SUBTOTAL,''0''IGSTAMT
UNION ALL
SELECT ''Supplies made to Composition Taxable Persons'' [Nature of Supplies],(s.column06+''-''+s.column05) PLACE, cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE017 s on S9.COLUMN66=s.COLUMN02 --and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and (s9.column65 = 23603) and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
--group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S.COLUMN05,S.COLUMN06
)A group by [Nature of Supplies] --having (sum(IGSTAMT))!=0
UNION ALL

select [Nature of Supplies],max(PLACE),isnull(sum(SUBTOTAL),0)SUBTOTAL,isnull(sum(IGSTAMT),0)IGSTAMT from(
select ''Supplies made to UIN holders'' [Nature of Supplies],''''PLACE,''0''SUBTOTAL,''0''IGSTAMT
UNION ALL
SELECT ''Supplies made to UIN holders'' [Nature of Supplies],(s.column06+''-''+s.column05) PLACE, cast(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)as decimal(18,2))SUBTOTAL,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE017 s on S9.COLUMN66=s.COLUMN02 --and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and (s9.column65 = 23604) and isnull(S9.COLUMNA13,0)=0 and s9.columna03 = '+@AcOwner+' '+   @whereStr +'
--group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35,S.COLUMN05,S.COLUMN06
)A group by [Nature of Supplies] ')

END
else if(@SEC = '3')
BEGIN
exec('
select ''(A) ITC Available (whether in full or part)'' [Nature of Supplies],NULL,NULL,NULL,NULL
UNION ALL
select ''(1) Import of goods'' [Nature of Supplies],0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT from(
select ''(1) Import of goods'' [Nature of Supplies],''0''IGSTAMT,''0''CGSTAMT,''0''SGSTAMT,''0''CESSTAMT
UNION ALL
SELECT ''(1) Import of goods'' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 = ''ITTY001''
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0 and t.COLUMN16 = 23584  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,t.COLUMN16
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''(2) Import of services'' [Nature of Supplies],isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
select ''(2) Import of services'' [Nature of Supplies],''0''IGSTAMT,''0''CGSTAMT,''0''SGSTAMT,''0''CESSTAMT
UNION ALL
SELECT ''(2) Import of services'' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 = ''ITTY009''
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0 and t.COLUMN16 = 23584  and s9.columna03 = '+@AcOwner+' '+   @whereStr +'  and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,t.COLUMN16
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''(3) Inward supplies liable to reverse charge (otherthan 1 & 2 above)'' [Nature of Supplies],isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
select ''(3) Inward supplies liable to reverse charge (otherthan 1 & 2 above)'' [Nature of Supplies],''0''IGSTAMT,''0''CGSTAMT,''0''SGSTAMT,''0''CESSTAMT
UNION ALL
SELECT ''(3) Inward supplies liable to reverse charge (otherthan 1 & 2 above)'' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 != ''ITTY009'' and s.COLUMN05 != ''ITTY001''
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 1  and t.COLUMN16 != 23584 and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,t.COLUMN16
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''(4) Inward supplies from ISD'' [Nature of Supplies],isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
select ''(4) Inward supplies from ISD'' [Nature of Supplies],''0''IGSTAMT,''0''CGSTAMT,''0''SGSTAMT,''0''CESSTAMT
UNION ALL
SELECT ''(4) Inward supplies from ISD'' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 = ''ITTY009''
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and t.COLUMN16 != 23584   and (s9.COLUMN55 != '''' and s9.COLUMN55 is not null)  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05,t.COLUMN16
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''(5) All other ITC'' [Nature of Supplies],isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
select ''(5) All other ITC'' [Nature of Supplies],''0''IGSTAMT,''0''CGSTAMT,''0''SGSTAMT,''0''CESSTAMT
UNION ALL
SELECT ''(5) All other ITC'' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 != ''ITTY009'' --and s.COLUMN05 != ''ITTY001'' 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0 
--and t.COLUMN16 != 23584
 and (s9.COLUMN55 != '''' and s9.COLUMN55 is not null)  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''(5) All other ITC'' [Nature of Supplies],
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.column70 is not null and s9.column70 != '''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0  and isnull(s9.COLUMN73,0) = 0 
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''(B) ITC Reversed'' [Nature of Supplies],NULL,NULL,NULL,NULL
UNION ALL
select ''(1) As per rules 42 & 43 of CGST Rules'' [Nature of Supplies],isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
select ''(1) As per rules 42 & 43 of CGST Rules'' [Nature of Supplies],''0''IGSTAMT,''0''CGSTAMT,''0''SGSTAMT,''0''CESSTAMT
UNION ALL
SELECT ''(1) As per rules 42 & 43 of CGST Rules'' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 != ''ITTY009'' and s.COLUMN05 != ''ITTY001''
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +'  and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0
UNION ALL
select ''(2) Others'' [Nature of Supplies],NULL,NULL,NULL,NULL
UNION ALL
select ''(C) Net ITC Available (A) – (B)'' [Nature of Supplies],isnull(sum(IGSTAMT),0)IGSTAMT,isnull(sum(CGSTAMT),0)CGSTAMT,isnull(sum(SGSTAMT),0)SGSTAMT,isnull(sum(CESSTAMT),0)CESSTAMT from(
SELECT '''' [Nature of Supplies],
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and (s.COLUMN05 = ''ITTY009'' OR s.COLUMN05 = ''ITTY001'')
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0 and (s9.COLUMN55 != '''' and s9.COLUMN55 is not null)   and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT ''(5) All other ITC'' [Nature of Supplies],
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
--inner join SATABLE002 s on S9.COLUMN05=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.column70 is not null and s9.column70 != '''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.COLUMNA13,0)=0  and isnull(s9.COLUMN73,0) = 0 
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32,a.COLUMN10,a.COLUMN13
UNION ALL
SELECT '''' [Nature of Supplies],
-cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,
-cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT, 
-cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,
-cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE007 s on a.COLUMN04=s.COLUMN02 and S9.COLUMNA03=S.COLUMNA03 and isnull(S.COLUMNA13,0)=0 and s.COLUMN05 != ''ITTY001'' and s.COLUMN05 != ''ITTY009''
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A --having (sum(IGSTAMT)+sum(CGSTAMT)+sum(SGSTAMT)+sum(CESSTAMT))!=0 
UNION ALL
select ''(D) Ineligible ITC'' [Nature of Supplies],NULL,NULL,NULL,NULL
UNION ALL
select ''(1) As per section 17(5)'' [Nature of Supplies],NULL,NULL,NULL,NULL
UNION ALL
select ''(2) Others'' [Nature of Supplies],NULL,NULL,NULL,NULL')
END
else if(@SEC = '4')
BEGIN
exec('
--select ''5 Values of exempt, nil-rated and non-GST inward supplies'' [Nature of Supplies],NULL,NULL
--UNION ALL
select  [Nature of Supplies],isnull(sum(AMT1),0)AMT1,isnull(sum(AMT2),0)AMT2 from(
select  ''From a supplier under composition scheme, Exempt and Nil rated supply''[Nature of Supplies],0 AMT1,0 AMT2
UNION ALL
SELECT ''From a supplier under composition scheme, Exempt and Nil rated supply'' [Nature of Supplies],
cast(isnull(a.COLUMN22 ,0) as decimal(18,2)) AMT1,0 AMT2
FROM PUTABLE005 S9  
inner join CONTABLE007 a on S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and cast(S9.COLUMN57 as nvarchar(25))=a.COLUMND01 and isnull(a.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN24 = 0 and s9.COLUMN56 = 23603 and (s9.COLUMN55 != '''' and s9.COLUMN55 is not null)  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
--group by a.COLUMN22--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT ''From a supplier under composition scheme, Exempt and Nil rated supply'' [Nature of Supplies],
0 AMT2,cast(isnull(a.COLUMN22 ,0) as decimal(18,2)) AMT1
FROM PUTABLE005 S9  
inner join CONTABLE007 a on S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and cast(S9.COLUMN57 as nvarchar(25))!=a.COLUMND01 and isnull(a.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN24 = 0 and s9.COLUMN56 = 23603 and (s9.COLUMN55 != '''' and s9.COLUMN55 is not null)  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
--group by a.COLUMN22--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A  group by [Nature of Supplies]
UNION ALL
select  [Nature of Supplies],isnull(sum(AMT1),0)AMT1,isnull(sum(AMT2),0)AMT2 from(
select  ''Non GST supply'' [Nature of Supplies],0 AMT1,0 AMT2
UNION ALL
SELECT ''Non GST supply'' [Nature of Supplies],
cast(isnull(a.COLUMN22 ,0) as decimal(18,2)) AMT1,0 AMT2
FROM PUTABLE005 S9  
inner join CONTABLE007 a on S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and cast(S9.COLUMN57 as nvarchar(25))=a.COLUMND01 and isnull(a.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN24 = 0 and (s9.COLUMN55 = '''' or s9.COLUMN55 is  null)  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
--group by a.COLUMN22--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
UNION ALL
SELECT ''Non GST supply'' [Nature of Supplies],
0 AMT2,cast(isnull(a.COLUMN22 ,0) as decimal(18,2)) AMT1
FROM PUTABLE005 S9  
inner join CONTABLE007 a on S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and cast(S9.COLUMN57 as nvarchar(25))!=a.COLUMND01 and isnull(a.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN24 = 0 and (s9.COLUMN55 = '''' or s9.COLUMN55 is  null)  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
--group by a.COLUMN22--,S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S.COLUMN05
)A group by [Nature of Supplies]  ')
END
else if(@SEC = '5')
BEGIN
exec('
select [Nature of Supplies],SUM(SUBTOTAL) SUBTOTAL,sum(IGSTAMT)IGSTAMT,sum(CGSTAMT)CGSTAMT,sum(SGSTAMT)SGSTAMT,sum(CESSTAMT)CESSTAMT,NULL,NULL,NULL,NULL from(
SELECT ''Integrated Tax'' [Nature of Supplies],cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23584 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and isnull(S9.COLUMNA13,0)=0 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+'''  AND ISNULL(S9.columna13,0)=0 and s9.columna03 = '+@AcOwner+' '+   @whereStr +' 
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35
UNION ALL
SELECT ''Integrated Tax'' [Nature of Supplies],0 SUBTOTAL,
cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23584 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''Integrated Tax'' [Nature of Supplies],cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23584 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''Integrated Tax'' [Nature of Supplies],0 SUBTOTAL,
-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23584 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Integrated Tax'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23584 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Integrated Tax'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23584 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+'''  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Integrated Tax'' [Nature of Supplies],cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23584,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT

FROM FITABLE023 S9  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(isnull(s9.COLUMN12,0),''-'',''''))) s)) or t.COLUMN02=isnull(s9.COLUMN12,0)) and isnull(cast(s9.COLUMN19 as decimal(18,2)),0)!=0 and t.COLUMNA03=s9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1386 and isnull(S9.COLUMNA13,0)=0 and s9.column05 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.columna13,0)=0
group by S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S9.COLUMN19,S9.COLUMN27,S9.COLUMN24
UNION ALL
SELECT ''Central Tax'' [Nature of Supplies],cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT, 0 SGSTAMT,0 CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 inner join MATABLE013 t on t.COLUMN16=23582 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+'''  and s9.columna03 = '+@AcOwner+' '+   @whereStr +'
AND ISNULL(S9.columna13,0)=0
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35
UNION ALL
SELECT ''Central Tax'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23582 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+'''  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Central Tax'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT,0 SGSTAMT,0 CESSTAMT

FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 

inner join MATABLE013 t on t.COLUMN16=23582 and (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
union all
SELECT ''Central Tax'' [Nature of Supplies],cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT

FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 

inner join MATABLE013 t on t.COLUMN16=23582 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''Central Tax'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23582 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Central Tax'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23582 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Central Tax'' [Nature of Supplies],cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,

0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT 
FROM FITABLE023 S9  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(isnull(s9.COLUMN12,0),''-'',''''))) s)) or t.COLUMN02=isnull(s9.COLUMN12,0)) and isnull(cast(s9.COLUMN19 as decimal(18,2)),0)!=0 and t.COLUMNA03=s9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1386 and isnull(S9.COLUMNA13,0)=0 and s9.column05 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.columna13,0)=0
group by S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S9.COLUMN19,S9.COLUMN27,S9.COLUMN24
UNION ALL
SELECT ''State/UT Tax'' [Nature of Supplies],cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23582,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT, 0 SGSTAMT,0 CESSTAMT
FROM SATABLE009 S9 
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 inner join MATABLE013 t on t.COLUMN16=23582 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+'''  and s9.columna03 = '+@AcOwner+' '+   @whereStr +'
and isnull(S9.COLUMNA13,0)=0 group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35
UNION ALL
SELECT ''State/UT Tax'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM SATABLE005 S9  
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23583 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+'''  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''State/UT Tax'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,0CGSTAMT,cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,0 CESSTAMT
---cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 

inner join MATABLE013 t on t.COLUMN16=23583 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''State/UT Tax'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23583 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''State/UT Tax'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23583 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL 
SELECT ''State/UT Tax'' [Nature of Supplies],cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,

0 IGSTAMT,0CGSTAMT,0 SGSTAMT,0 CESSTAMT

FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 

inner join MATABLE013 t on t.COLUMN16=23583 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33

UNION ALL
SELECT ''State/UT Tax'' [Nature of Supplies],cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23583,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0CGSTAMT,0 SGSTAMT,0 CESSAMT
FROM FITABLE023 S9  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(isnull(s9.COLUMN12,0),''-'',''''))) s)) or t.COLUMN02=isnull(s9.COLUMN12,0)) and isnull(cast(s9.COLUMN19 as decimal(18,2)),0)!=0 and t.COLUMNA03=s9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1386 and isnull(S9.COLUMNA13,0)=0 and s9.column05 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.columna13,0)=0
group by S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S9.COLUMN19,S9.COLUMN27,S9.COLUMN24
UNION ALL
SELECT ''Cess'' [Nature of Supplies],cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0CGSTAMT,0 SGSTAMT, 0 CESSTAMT
FROM SATABLE009 S9  
inner join SATABLE010 a on S9.COLUMN01=a.COLUMN15 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23586 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,''-'',''''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1277 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+'''  and isnull(S9.COLUMNA13,0)=0 and s9.columna03 = '+@AcOwner+' '+   @whereStr +'
group by a.COLUMN02,a.COLUMN10,a.COLUMN13,a.COLUMN35
UNION ALL
SELECT ''Cess'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM SATABLE005 S9
inner join SATABLE006 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23586 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN26,''-'',''''))) s)) or t.COLUMN02=a.COLUMN26) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1330 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+'''  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Cess'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,0CGSTAMT,0 SGSTAMT,cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23586 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''Cess'' [Nature of Supplies],cast(((isnull(a.COLUMN09 ,0)*isnull(a.COLUMN33 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE005 S9  
inner join PUTABLE006 a on S9.COLUMN01=a.COLUMN13 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0 
inner join MATABLE013 t on t.COLUMN16=23586 and  (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN18,''-'',''''))) s)) or t.COLUMN02=a.COLUMN18) and isnull(a.COLUMN11,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1273 and s9.column08 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN58 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' and isnull(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN09,a.COLUMN11,a.COLUMN33
UNION ALL
SELECT ''Cess'' [Nature of Supplies],cast((Iif(S9.COLUMN27 =22713,(isnull(cast(s9.COLUMN19-ISNULL(s9.COLUMN24,0) as decimal(18,2)) ,0)),(isnull(cast(s9.COLUMN19 as decimal(18,2)) ,0)))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN07,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM FITABLE023 S9  
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(isnull(s9.COLUMN12,0),''-'',''''))) s)) or t.COLUMN02=isnull(s9.COLUMN12,0)) and isnull(cast(s9.COLUMN19 as decimal(18,2)),0)!=0 and t.COLUMNA03=s9.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1386 and isnull(S9.COLUMNA13,0)=0 and s9.column05 between '''+@FROMDT+''' AND '''+@TODT+''' and s9.columna03 = '+@AcOwner+'  '+@whereStr+' AND ISNULL(S9.columna13,0)=0
group by S9.COLUMN04,S9.COLUMN03,S9.COLUMN08,S9.COLUMN19,S9.COLUMN27,S9.COLUMN24
UNION ALL
SELECT ''Cess'' [Nature of Supplies],-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,0 CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23586 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 1  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32
UNION ALL
SELECT ''Cess'' [Nature of Supplies],0 SUBTOTAL,
0 IGSTAMT,0 CGSTAMT,0 SGSTAMT,-cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((SUM((iif(t.COLUMN16=23586,CAST(isnull(t.COLUMN17,0) as decimal(18,2)),0))))/100)) as decimal(18,2)) CESSTAMT
FROM PUTABLE001 S9  
inner join PUTABLE002 a on S9.COLUMN01=a.COLUMN19 and S9.COLUMNA02=a.COLUMNA02 and S9.COLUMNA03=a.COLUMNA03 and isnull(a.COLUMNA13,0)=0
inner join MATABLE013 t on t.COLUMN16=23586 and   (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN25,''-'',''''))) s)) or t.COLUMN02=a.COLUMN25) and isnull(a.COLUMN09,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
WHERE  s9.column03=1355 and s9.column06 between '''+@FROMDT+''' AND  '''+@TODT+''' and s9.COLUMN73 = 0  and s9.columna03 = '+@AcOwner+' '+   @whereStr +' AND ISNULL(S9.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN07,a.COLUMN09,a.COLUMN32

)A group by [Nature of Supplies] ')
END
else if(@SEC = '6')
BEGIN

select 'TDS' [Nature of Supplies],NULL,NULL,NULL
UNION ALL
select 'TCS' [Nature of Supplies],NULL,NULL,NULL
END
else if(@SEC = 'HEADER')
BEGIN
SELECT COLUMN03 CMP,
COLUMND02 GSTIN,DATEPART(YYYY,@TODT) YY,
LEFT(DATENAME(MONTH,@TODT),10) MM 
FROM CONTABLE008 WHERE COLUMNA03 = @AcOwner AND ISNULL(COLUMNA13,0)=0 ORDER BY COLUMN02 DESC
END
END





GO
