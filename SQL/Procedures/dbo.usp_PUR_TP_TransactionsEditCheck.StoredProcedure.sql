USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_TransactionsEditCheck]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_TransactionsEditCheck]
(
	@TransactionID int = null ,
	@FormId int = null ,
	@TableName nvarchar(250) = null
)

AS
	BEGIN
		if(@TableName='PUTABLE001')
		begin 
		if(@FormId=1374)
		begin 
			SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
			FROM PUTABLE001 a  inner join PUTABLE001 b on a.COLUMN02 in(b.COLUMN34 ) and b.COLUMNA13=0
			where   a.COLUMN02=@TransactionID
		end
		else
		begin
			SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
			FROM PUTABLE001 a  inner join PUTABLE003 b on a.COLUMN02 in(b.COLUMN06 ) and b.COLUMNA13=0
			where   a.COLUMN02=@TransactionID
		end
		end
		else if(@TableName='PUTABLE003')
		begin 
			SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
			FROM PUTABLE003 a  inner join PUTABLE006 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
			where   a.COLUMN02=@TransactionID
		end
		else if(@TableName='PUTABLE005')
		begin 
			SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
			FROM PUTABLE005 a  inner join PUTABLE015 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
			where   a.COLUMN02=@TransactionID
		end
		else if(@TableName='PUTABLE014')
		begin 
			SELECT  a.COLUMN02 COLUMN05
			FROM PUTABLE014 a 
			where   a.COLUMN02=@TransactionID
		end
			else if(@TableName='SATABLE005')
			begin 
			if(@FormId=1376)
			begin 
				SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE005 a  inner join SATABLE005 b on a.COLUMN02 in(b.COLUMN35 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			else
			begin
				SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE005 a  inner join SATABLE007 b on a.COLUMN02 in(b.COLUMN06 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			end
			else if(@TableName='SATABLE007')
			begin 
				SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE007 a  inner join SATABLE010 b on a.COLUMN02 in(b.COLUMN04 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			else if(@TableName='SATABLE009')
			begin 
				SELECT  a.COLUMN02 COLUMN05
			--EMPHCS875 rajasekhar reddy patakota 8/8/2015 Even after deleting Bill, system is not allowing to delete item receipt
				FROM SATABLE009 a  inner join SATABLE012 b on a.COLUMN02 in(b.COLUMN03 ) and b.COLUMNA13=0
				where   a.COLUMN02=@TransactionID
			end
			else if(@TableName='SATABLE011')
			begin 
		if(@FormId!=1288)
			begin 
				SELECT  a.COLUMN02 COLUMN05
				FROM SATABLE011 a 
				where   a.COLUMN02=@TransactionID
			end
			end
		else
		begin 
			SELECT * FROM  (values (null) ) as T(COLUMN05)
		end
	
	END


GO
