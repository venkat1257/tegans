USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GettingClients]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[GettingClients]
( 
		 @CustType  nvarchar(250), 
         @COLUMN02 nvarchar(250)=null,
		 @COLUMN03 nvarchar(250)=null,
		 @COLUMN04 DateTime=null,
		 @COLUMN05 nvarchar(250)=null,
		 @COLUMN06 nvarchar(250)=null,
		 @COLUMN07 nvarchar(250)=null,
		 @COLUMN08 nvarchar(250)=null,
		 @COLUMN09 nvarchar(250)=null,
		 @COLUMN10 nvarchar(250)=null,
		 @COLUMN11 nvarchar(250)=null,
		 @COLUMN12 nvarchar(250)=null,
		 @COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
		 @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
		 @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null, 
		 @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null, 
		 @COLUMNA13  nvarchar(250)=0,     @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
		 @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
		 @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
		 @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
		 @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null, 
		 @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
		 @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
		 @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null   
)
as
Begin
begin try
     IF @CustType = '22368'  
    begin
		select COLUMN11 as COLUMN009, COLUMN05  as COLUMN008 FROM SATABLE002   Where  COLUMN02  in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN02) s) and COLUMNA03 = @COLUMNA03 AND ISNULL(COLUMNA13,0)=0
	 end
	 IF @CustType = '22371'  
    begin
       select COLUMN14 as COLUMN009, COLUMN06  as COLUMN008  FROM MATABLE010   Where  COLUMN02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN02) s) and COLUMNA03 = @COLUMNA03 AND ISNULL(COLUMNA13,0)=0
	 end
	  IF @CustType = '22370'  
    begin        
		 select COLUMN12 as COLUMN009,COLUMN05  as COLUMN008  FROM SATABLE001 Where  COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN02) s)	 and COLUMNA03 = @COLUMNA03 AND ISNULL(COLUMNA13,0)=0
	 end
	   IF @CustType = '22369'  
    begin     
		 select COLUMN12 as COLUMN009,COLUMN05  as COLUMN008  FROM SATABLE001 Where  COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN02) s)	 and COLUMNA03 = @COLUMNA03 AND ISNULL(COLUMNA13,0)=0
	 end
	 end try

	 begin catch
	 end catch
end




GO
