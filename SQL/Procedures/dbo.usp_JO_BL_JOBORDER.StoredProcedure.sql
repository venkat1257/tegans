USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_BL_JOBORDER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_BL_JOBORDER]

(

    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  

	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  

	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,  

	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  

	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 

	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  

	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  

	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,

	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,

	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,

    @COLUMN32   nvarchar(250)=null,  @COLUMN33   nvarchar(250)=null,  @COLUMN34   nvarchar(250)=null,

	@COLUMN35   nvarchar(250)=null,  @COLUMN36   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,

	@COLUMN38   nvarchar(250)=null,  @COLUMN39   nvarchar(250)=null,  @COLUMN40   nvarchar(250)=null,
	--EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
	@COLUMN41   nvarchar(250)=null,  @COLUMN42   nvarchar(250)=null,  @COLUMN50   nvarchar(250)=null,
        @COLUMN51   nvarchar(250)=null,  @COLUMN52   nvarchar(250)=null,  @COLUMN53   nvarchar(250)=null,
	@COLUMN54   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   

	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 

    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,

	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,

	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  

	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  

	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,

    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   

	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  

	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  

	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  

	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
        --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
	@TabelName  nvarchar(250),       @ReturnValue int=null OUTPUT,    @UOMData  XML=null 

)

AS

BEGIN

begin try

   declare @tempSTR nvarchar(max)

--   set @tempSTR=('the values '+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','
--+isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+','+isnull(@COLUMN14,'')+','+isnull(@COLUMN15,'')+','+isnull(@COLUMN16,'')+','+isnull(@COLUMN17,'')+','+isnull(@COLUMN18,'')+','+isnull(@COLUMN19,'')+','+isnull(@COLUMN20,'')+','+isnull(@COLUMN21,'')+','+isnull(@COLUMN22,'')+','+isnull(@COLUMN23,'')+','+isnull(@COLUMN24,'')+','+isnull(@COLUMN25,'')+','+isnull(@COLUMN26,'')+','+isnull(@COLUMN27,'')+','+isnull(@COLUMN28,'')+','+isnull(@COLUMN29,'')+','+isnull(@COLUMN30,'')+','+isnull(@COLUMN31,'')+','+isnull(@COLUMN32,'')+' of  '+isnull(@TabelName,'')+' succeeded at  ');

IF  @TabelName='SATABLE005'

BEGIN

set @COLUMNA02=( CASE WHEN (@COLUMN24!= '' and @COLUMN24 is not null and @COLUMN24!= '0' ) THEN @COLUMN24 else @COLUMNA02  END )

EXEC usp_JO_TP_SATABLE005 

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,

   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  

   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
  
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,  @COLUMN38,  @COLUMN39,  @COLUMN40,  @COLUMN41,
   --EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions	
   @COLUMN42,  @COLUMN50,  @COLUMN51,  @COLUMN52,  @COLUMN53,  @COLUMN54,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, 

   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 

   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 

   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 
   --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
   @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue OUTPUT
SELECT @ReturnValue
END

 

ElSE IF  @TabelName='SATABLE006'

BEGIN

EXEC usp_JO_TP_SATABLE006

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,

   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  
   --EMPHCS922 rajasekhar reddy patakota 11/8/2015 uom condition checking in sales order 
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31, @COLUMN32, 
   @COLUMN33,  @COLUMN34, @COLUMNA01, @COLUMNA02, @COLUMNA03, 

   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 

   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 

   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 

   @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue,@UOMData

END 

ElSE IF  @TabelName='SATABLE003'

BEGIN
--set @COLUMN20= (SELECT COLUMN01 FROM SATABLE005 WHERE COLUMN01=(SELECT MAX(COLUMN01) FROM SATABLE005))

set @COLUMN21= (SELECT COLUMN24 FROM SATABLE005 WHERE COLUMN01=@COLUMN20)
set @COLUMN22= (SELECT COLUMN25 FROM SATABLE005 WHERE COLUMN01=@COLUMN20)
set @COLUMN23= (SELECT COLUMN26 FROM SATABLE005 WHERE COLUMN01=@COLUMN20)
set @COLUMN24= (SELECT COLUMN27 FROM SATABLE005 WHERE COLUMN01=@COLUMN20)

  set @COLUMNA02=( CASE WHEN (@COLUMN20!= '' and @COLUMN20 is not null and @COLUMN20!= '0' ) THEN (select COLUMN24 from SATABLE005 

   where COLUMN01=@COLUMN20) else @COLUMNA02  END )

if  exists(select column01 from SATABLE006 where column19=@COLUMN20)

					begin

EXEC usp_JO_TP_SATABLE003

   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,

   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  'SALESORDER',  @COLUMN20,  @COLUMN21,  

   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMNA01, @COLUMNA02, @COLUMNA03, 

   @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, 

   @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, 

   @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, 

   @COLUMND09, @COLUMND10, @Direction, @TabelName, @ReturnValue

end

else

begin

return 0

end

 END

   exec [CheckDirectory] @tempSTR,'usp_SAL_BL_SALESORDER.txt',0

END TRY

			BEGIN CATCH

			begin

   --set @tempSTR='transaction failed at  ';
   set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
   exec [CheckDirectory] @tempSTR,'usp_Exception_SALESORDER.txt',0

			return 0

			end

			END CATCH

end








GO
