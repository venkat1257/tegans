USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAR_TP_PUTABLE003]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAR_TP_PUTABLE003]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMNA01  varchar(100)=null,   
	@COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   
	@COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  
	@COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  
	@COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  
	@COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  
	@COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null, 
	@COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  
	@COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,  
	@COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  
	@COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  
	@COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  
	@COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       @TabelName  nvarchar(250),       
	--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
	@ReturnValue int=null OUTPUT
)

AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN17=(1003)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE003_SequenceNo
insert into PUTABLE003 
(
  COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
  COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,
  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, 
  COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, 
  COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, 
  COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, 
   @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, 
   @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, 
   @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @ReturnValue =(Select COLUMN01 from PUTABLE003 where COLUMN02=@COLUMN02)
END
 

IF @Direction = 'Select'
BEGIN
select * from PUTABLE003
END 

 
IF @Direction = 'Update'
BEGIN
--EMPHCS1794 rajasekhar reddy patakota 01/08/2016 Formbuild Screen Modifications
set @COLUMN17=(1003)
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @location nvarchar(250),@linelocation nvarchar(250)
set @location =(select COLUMN22 from  PUTABLE003 Where COLUMN02=@COLUMN02 );
set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
UPDATE PUTABLE003 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06,  
   COLUMN07=@COLUMN07,     COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,   
   COLUMN17=@COLUMN17,     COLUMN18=@COLUMN18,     COLUMN19=@COLUMN19,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,  COLUMN22=@COLUMN22,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
   COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02


 declare @POID nvarchar(250),@ItemQty nvarchar(250),@TotalItemQty nvarchar(250),@RemainItemQty nvarchar(250)
 declare @Qty_On_Hand decimal(18,2), @Qty_On_Hand1  decimal(18,2),@Qty_Order  decimal(18,2),@Price  decimal(18,2),
 @Qty_Avl  decimal(18,2),@Qty_Cmtd  decimal(18,2)
      set @POID=(select COLUMN01 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed	  
declare @FRMID nvarchar(250),@FRMType nvarchar(250),@lotno nvarchar(250)
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN02=@COLUMN02)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	set @FRMType=('Return Receipt')
	--Inventory Asset table Updations
	delete from PUTABLE011 where COLUMN02=@POID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN13 and COLUMNA03=@COLUMNA03
	delete from PUTABLE017 where COLUMN05=@POID and COLUMN06=@FRMType and  COLUMN13=@COLUMN13 and COLUMNA03=@COLUMNA03
	  DECLARE @Initialrow nvarchar(250),@id nvarchar(250),@MaxRownum nvarchar(250),@Item nvarchar(250),@uom nvarchar(250),@TrackQty nvarchar(250),
	  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	   @PRATE DECIMAL(18,2),@ASSET DECIMAL(18,2),@AvgPrice DECIMAL(18,2),@RefLineId nvarchar(250),@maxqty decimal(18,2)
	 set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE004 where COLUMN12 in(@POID) and isnull(COLUMNA13,'False')='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE004 where COLUMN12 in(@POID) and isnull(COLUMNA13,'False')='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
	 --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
		     declare @lIID nvarchar(250)
			 set @lIID=(select COLUMN01 from PUTABLE004 WHERE COLUMN02=@id)
             set @Item=(select COLUMN03 from PUTABLE004 where COLUMN02=@id)
	     --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
             set @RefLineId=(select COLUMN26 from PUTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN08,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @Price=(select isnull(COLUMN10,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             SET @PRATE=(SELECT ISNULL(COLUMN17,0) FROM MATABLE007 WHERE COLUMN02=@Item)	 
	     --EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
	     set @uom=(select isnull(COLUMN17,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
	     set @linelocation=(select cast(COLUMN28 as nvarchar(250)) from PUTABLE004 where COLUMN02=@id)
		 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
		 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
		 set @lotno=(select isnull(COLUMN24,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @uom =(case when @uom='' then 0 when isnull(@uom,0)=0 then 0  else @uom end)
	     set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 
			 --Received Qty Updation
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefLineId,0)>0)
				begin
				set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))   where  COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  COLUMNA13=0 and COLUMN26=@RefLineId) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select max(isnull(COLUMN07,0)) from PUTABLE004 where  COLUMNA13=0 and COLUMN26=@RefLineId) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @maxqty=((cast((select max(isnull(COLUMN06,0)) from PUTABLE004 where  COLUMNA13=0 and COLUMN26=@RefLineId) AS decimal(18,2))))
				end
				else
				begin
			    --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			    set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @COLUMN06) and COLUMN03=@Item and isnull(COLUMN26,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and isnull(COLUMNA13,0)=0)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))   where COLUMN03=@Item and isnull(COLUMN26,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and isnull(COLUMNA13,0)=0 and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@COLUMN06))
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  columna13=0 and COLUMN03=@Item and isnull(COLUMN17,0)=@uom and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select max(isnull(COLUMN07,0)) from PUTABLE004 where   columna13=0 and isnull(COLUMN17,0)=@uom and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno and COLUMN03=@Item) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @maxqty=((cast((select max(isnull(COLUMN06,0)) from PUTABLE004 where   columna13=0 and isnull(COLUMN17,0)=@uom and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno and COLUMN03=@Item) AS decimal(18,2))))
				end
				UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from PUTABLE001 where COLUMN02 in (@COLUMN06))
			   	--EMPHCS806 rajasekhar reddy patakota 28/7/2015 partial qty edit is IR not updating remaining qty in IR
				--EMPHCS854 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in item receipt
				set @RemainItemQty=(cast(@maxqty AS decimal(18,2))- CAST(@RemainItemQty AS decimal(18,2)))
				update PUTABLE004 set COLUMN07=@TotalItemQty, COLUMN08=0, COLUMN09=@RemainItemQty where   COLUMN02=@id and COLUMN03=@Item
				
				--Inventory Updation
				--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				if exists(SELECT COLUMN12 FROM FITABLE038 WHERE column03='RETURN RECEIPT' and column04=@COLUMN04 and column05=@lIID and COLUMN06=@Item and isnull(COLUMN08,10000)=@uom and datalength(isnull(COLUMN12,0))>0 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
				begin
				exec usp_PUR_TP_InventoryLOT  @COLUMN04,@lIID,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@location,@lotno
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else
				begin
				--EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
				if exists(select COLUMN03 from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
				 --if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+cast(@ItemQty as decimal(18,2)))
				if(@Qty_Order>0 or @Qty_Order>cast(@ItemQty as decimal(18,2)))
				Begin
				set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				end
				else
				begin
				set @Qty_Order = 0;
				end
				set @Qty_Avl = cast(@Qty_Cmtd as decimal(18,2));
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @PRATE=(@Price)
				set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
				--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
				SET @PRATE=(@AvgPrice)
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN07=@Qty_Order, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))-((cast(@ItemQty as decimal(18,2)))*(cast(@PRATE as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				--EMPHCS884 rajasekhar reddy patakota 8/8/2015 to deleete existing inventory asset records for  IR
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				else
				begin
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				end
				end
				--set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
		     --EMPHCS854 rajasekhar reddy patakota 06/08/2015 Deleted row condition checking in item receipt
			 UPDATE PUTABLE004 SET COLUMNA13=1 WHERE  COLUMN02 = @id and COLUMN03=@Item
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
--if  exists( SELECT 1 FROM PUTABLE013 WHERE COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from PUTABLE001 WHERE COLUMN02 = @COLUMN02))
--begin
--delete from PUTABLE013  where COLUMN04=@COLUMN04 and COLUMN02 in(select COLUMN01 from PUTABLE001 WHERE COLUMN02 = @COLUMN02)
--end

END


else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE003 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
--EMPHCS854 rajasekhar reddy patakota 8/8/2015Deleted row condition checking in item receipt
declare @PID nvarchar(250),@TQty decimal(18,2),@Result nvarchar(250),@BQty decimal(18,2),@RecQty decimal(18,2)
--EMPHCS1474 rajasekhar reddy patakota 30/12/2015 Purchase Returns Latest Issues Fixed
      set @COLUMN04=(select COLUMN04 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN05=(select COLUMN05 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA02=(select COLUMNA02 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMNA03=(select COLUMNA03 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN06=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN13=(select COLUMN13 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @POID=(select COLUMN01 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @PID=(select COLUMN06 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
      set @COLUMN22=(select COLUMN22 from PUTABLE003 WHERE COLUMN02 = @COLUMN02)
set @location=(case when @COLUMN22='' then 0 when isnull(@COLUMN22,0)=0 then 0  else @COLUMN22 end)
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN02=@COLUMN02)
set @FRMID= (SELECT COLUMN06 FROM CONTABLE0010 WHERE COLUMN02=@FRMID)
if (@FRMID=1272)
	begin
	--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	set @FRMType='Item Receipt';
	end
    else if (@FRMID=1329)
	begin
	set @FRMType=('Return Receipt')
	end
	else
	begin
	set @FRMType=('JobOrder Receipt')
	end
	--Inventory Asset table Updations
	delete from PUTABLE011  where COLUMN02=@POID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN13 and COLUMNA03=@COLUMNA03
	delete from  PUTABLE017  where COLUMN05=@POID and COLUMN06=@FRMType and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN13 and COLUMNA03=@COLUMNA03
			 
	  set @Initialrow =(1)
	  DECLARE cur1 CURSOR FOR SELECT COLUMN02 from PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False'
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM PUTABLE004 where COLUMN12 in(@POID) and COLUMNA13='False')
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
             set @Item=(select COLUMN03 from PUTABLE004 where COLUMN02=@id)
			 set @lIID=(select COLUMN01 from PUTABLE004 WHERE COLUMN02=@id)
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
             set @RefLineId=(select COLUMN26 from PUTABLE004 where COLUMN02=@id)
             set @COLUMNA03=(select COLUMNA03 from PUTABLE004 where COLUMN02=@id)
             set @ItemQty=(select isnull(COLUMN08,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @Price=(select isnull(COLUMN10,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             SET @PRATE=(SELECT ISNULL(COLUMN17,0) FROM MATABLE007 WHERE COLUMN02=@Item)	 
			 set @linelocation=(select cast(COLUMN28 as nvarchar(250)) from PUTABLE004 where COLUMN02=@id)
			 set @location=(case when (cast(@linelocation as nvarchar(250))!='' and @linelocation!='0') then @linelocation else @location end)
			 set @location=(case when @location='' then 0 when isnull(@location,0)=0 then 0  else @location end)
			 --EMPHCS1168	After creation of bill , if user deletes item receipt , system is not deducting inventory BY RAJ.Jr 16/9/2015
			 set @uom=(select isnull(COLUMN17,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
	     set @lotno=(select isnull(COLUMN24,0) from PUTABLE004 where COLUMN02=@id and COLUMN03=@Item)
             set @uom =(case when @uom='' then 0 when isnull(@uom,0)=0 then 0  else @uom end)
	     set @lotno=(case when @lotno='' then 0 when isnull(@lotno,0)=0 then 0  else @lotno end)
			 --Received Qty Updation
			 --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
			    if(isnull(@RefLineId,0)>0)
				begin
				set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where  COLUMN02=@RefLineId and isnull(COLUMNA13,0)=0
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where COLUMN26=@RefLineId and isnull(COLUMNA13,0)=0) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select max(isnull(COLUMN07,0)) from PUTABLE004 where COLUMN26=@RefLineId and isnull(COLUMNA13,0)=0) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @maxqty=((cast((select max(isnull(COLUMN06,0)) from PUTABLE004 where  COLUMN26=@RefLineId and isnull(COLUMNA13,0)=0) AS decimal(18,2))))
				end
				else
				begin
				--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
			    set @COLUMN12=(select isnull(COLUMN12,0) from PUTABLE002 where COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02= @COLUMN06) and isnull(COLUMNA13,0)=0 and isnull(COLUMN26,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and COLUMN03=@Item)
				UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2))) where COLUMN03=@Item and isnull(COLUMN26,0)=@uom and iif(COLUMN17='',0,isnull(COLUMN17,0))=@lotno and isnull(COLUMNA13,0)=0 and COLUMN19 in(select COLUMN01 from PUTABLE001 where COLUMN02= (@COLUMN06))
				set @TotalItemQty=((cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where  columna13=0 and COLUMN03=@Item and isnull(COLUMN17,0)=@uom and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @RemainItemQty=((cast((select max(isnull(COLUMN07,0)) from PUTABLE004 where   columna13=0 and isnull(COLUMN17,0)=@uom and COLUMN03=@Item and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno) AS decimal(18,2)))- CAST(@ItemQty AS decimal(18,2)))
				set @maxqty=((cast((select max(isnull(COLUMN06,0)) from PUTABLE004 where   columna13=0 and isnull(COLUMN17,0)=@uom and iif(COLUMN24='',0,isnull(COLUMN24,0))=@lotno and COLUMN03=@Item) AS decimal(18,2))))
				end
				UPDATE PUTABLE013 SET COLUMN09=(CAST(@COLUMN12 AS decimal(18,2))- CAST(@ItemQty AS decimal(18,2)))  
				WHERE COLUMN14=@Item and COLUMN02 in (select COLUMN01 from PUTABLE001 where COLUMN02 in (@COLUMN06))
			   
				set @RemainItemQty=(cast(@maxqty AS decimal(18,2))- CAST(@RemainItemQty AS decimal(18,2)))
				update PUTABLE004 set COLUMN07=@TotalItemQty, COLUMN08=0, COLUMN09=@RemainItemQty where   COLUMN02=@id and COLUMN03=@Item
				--EMPHCS854 rajasekhar reddy patakota 8/8/2015Deleted row condition checking in item receipt
				SET @TQty=(SELECT sum(COLUMN07) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @RecQty=(SELECT sum(COLUMN12) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				SET @BQty=(SELECT sum(COLUMN13) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
				--EMPHCS886 rajasekhar reddy patakota 8/8/2015 PO status is not changing back to Approved after delete
				if(cast(@RecQty as decimal(18,2))=0.00)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=101)
				end
				else IF(@BQty=(0.00))
				begin
				if(@TQty=@RecQty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=104)
				end
				else 
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=102)
				end
				end
				
				ELSE 
				BEGIN
				if(@TQty=@BQty)
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=106)
				end
				else
				begin
				set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=103)
				end
				end
				
				UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @POID))
				
				
             --Inventory Updation
	     --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				if exists(SELECT COLUMN12 FROM FITABLE038 WHERE column03='RETURN RECEIPT' and column04=@COLUMN04 and column05=@lIID and COLUMN06=@Item and isnull(COLUMN08,10000)=@uom and datalength(isnull(COLUMN12,0))>0 and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03)
				begin
				exec usp_PUR_TP_InventoryLOT  @COLUMN04,@lIID,@Item,@ItemQty,@COLUMNA02,@uom,@COLUMNA03,'Update',@location,@lotno
				update FITABLE038 set COLUMNA13=1 where COLUMN04=@COLUMN04 and COLUMN05=@lIID and COLUMN06=@Item and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
				end
				else
				begin
	                    --EMPHCS920 rajasekhar reddy patakota 11/08/2015 uom condition checking in Item receipt
			    set @Qty_On_Hand1=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)))
				--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
				if exists(select COLUMN03 from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)
				 --if(@Qty_On_Hand1>=0)
				 begin
				set @Qty_On_Hand=( cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))- cast(@ItemQty as decimal(18,2)));
				set @Qty_Cmtd=(cast(@Qty_On_Hand as decimal(18,2))- cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2)));
				set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+cast(@ItemQty as decimal(18,2)))
				if(@Qty_Order>0 or @Qty_Order>cast(@ItemQty as decimal(18,2)))
				Begin
				set @Qty_Order = (cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))+ cast(@ItemQty as decimal(18,2)));
				end
				else
				begin
				set @Qty_Order = 0;
				end
				set @Qty_Avl = cast(@Qty_Cmtd as decimal(18,2));
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno);
				--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
				set @PRATE=(@Price)
				--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
				SET @PRATE=(@AvgPrice)
				UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN07=@Qty_Order, COLUMN08=@Qty_On_Hand ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))-((cast(@ItemQty as decimal(18,2)))*(cast(@PRATE as decimal(18,2))))) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				--EMPHCS884 rajasekhar reddy patakota 8/8/2015 to deleete existing inventory asset records for  IR
				--UPDATE FITABLE010 SET COLUMN17=round(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2)),1) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
				  
				if(cast(@Qty_On_Hand as decimal(18,2))=0)
				begin
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				else
				begin
					--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
				set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@Item)
				if(@TrackQty=1)
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13 AND isnull(COLUMN19,0)=@uom  AND isnull(COLUMN21,0)=@location AND isnull(COLUMN22,0)=@lotno
				end
				end
				end
				end
				end
				--set @COLUMN04=(select COLUMN04 from PUTABLE001 where COLUMN02=@COLUMN06)
				--set @COLUMN05=(select COLUMN05 from PUTABLE003 where COLUMN01=@POID)
				--Inventory Asset table Updations
				--update PUTABLE011 set  COLUMNA13=@COLUMNA13 where COLUMN02=@POID and COLUMN04=@COLUMN06 and COLUMN20=@COLUMN13 and COLUMNA03=@COLUMNA03
				--update PUTABLE017  set  COLUMNA13=@COLUMNA13 where COLUMN05=@POID and COLUMN07=@COLUMN05 and COLUMN13=@COLUMN13 and COLUMNA03=@COLUMNA03
			    --set @Qty=(select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@Item AND COLUMN13=@COLUMN13)
		     --UPDATE FITABLE010 SET COLUMN07=cast(@Qty as decimal)-cast(@ItemQty as decimal) WHERE COLUMN03=@Item AND COLUMN13=@COLUMN13
			   UPDATE PUTABLE004 SET COLUMNA13=1 WHERE COLUMN02=@id
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
  CLOSE cur1 
END

end try
begin catch	
declare @tempSTR nvarchar(max)
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_EXCEPTION_SAR_PUTABLE003.txt',0
return 0
end catch
end







GO
