USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JOB_RECEIPT_IN_TP_PUTABLE004]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JOB_RECEIPT_IN_TP_PUTABLE004]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null, 
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,  
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
    @COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null, 
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null, @LotData  XML=null,
	@Result nvarchar(250)=null,      @VendorID nvarchar(250)=null,    @ReceiptType nvarchar(250)=null
)
AS
BEGIN
begin try
set @COLUMN17=(select iif((@COLUMN17!='' and @COLUMN17>0),@COLUMN17,(select max(column02) from fitable037 where column07=1 and column08=1)))
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
--set @COLUMN12=(select max(COLUMN01) from PUTABLE003)
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE004_SequenceNo
set @COLUMN13 =(select COLUMN13 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN14 =(select COLUMN14 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN15 =(select COLUMN15 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN16 =(select COLUMN16 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMNA02=( CASE WHEN (@COLUMN12!= '' and @COLUMN12 is not null and @COLUMN12!= '0' ) THEN
 (select COLUMN13 from PUTABLE003 where COLUMN01=@COLUMN12) else @COLUMNA02  END )

insert into PUTABLE004 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,
   COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN12,
   @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN17,  @COLUMN08,  @COLUMN20,  @COLUMN21, 
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05,     
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02,
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
if exists(select column01 from PUTABLE004 where column12=@COLUMN12)
begin
declare @PID int
declare @TQty decimal(18,2)
declare @RecQty decimal(18,2)
declare @BQty decimal(18,2)
declare @FRMID int
declare @JRStatus nvarchar(250)
SET @PID=(SELECT COLUMN06 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
SET @BQty=(SELECT sum(isnull(COLUMN13,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @JRStatus= (SELECT COLUMN20 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
--EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
declare @ItemLineID int
set @ItemLineID=(select COLUMN26 from PUTABLE004 where COLUMN02=@COLUMN02)
set @COLUMN07=(select sum(isnull(COLUMN12,0)) from PUTABLE002 where columna13=0 and COLUMN19= (select COLUMN01 from PUTABLE001 where COLUMN02= @PID) and COLUMN02=@ItemLineID)
UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN02=@ItemLineID and COLUMN19=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))

--IF(@TQty=(SELECT sum(isnull(COLUMN08,0)) FROM PUTABLE004 WHERE  columna13=0 and COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN06=@PID) ))
--begin
--if(@TQty=@BQty)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=26)
--end
--else if(@BQty=0)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
--end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=43)
--end
--end

--ELSE 
--BEGIN
SET @RecQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
if(@TQty=@RecQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end

--end

UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))
UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=05) where COLUMN01=@COLUMN12
END
--declare @Qty_On_Hand decimal(18,2)

--declare @Qty_Commit decimal(18,2)

--declare @Qty_Order decimal(18,2)

--declare @Qty_AVL decimal(18,2)

--declare @Qty_BACK decimal(18,2)

--declare @Qty decimal(18,2)

--declare @WIP decimal(18,2)

--declare @Memo nvarchar(250)


--if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17)
--begin
--declare @WipQty decimal(18,2)
--set @Qty_BACK =0
--set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17)- cast(@COLUMN08 as decimal(18,2));
--set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@COLUMN03 AND  COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17)
--set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17);
--set @Qty=cast(@COLUMN08 as decimal(18,2));
--set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
--set @Qty_AVL=(@Qty_On_Hand-@Qty_Commit)
--if(@Qty_AVL<0)
--begin
--set @Qty_AVL=0
--set @Qty_BACK =@Qty_Commit
--set @Qty_Commit=0
--end

--UPDATE FITABLE010 SET COLUMN18=(@WIP+cast(@COLUMN08 as decimal(18,2))) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17
--end
--else
--begin
--declare @newIID int
--declare @tmpnewIID1 int 
--set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
--if(@tmpnewIID1>0)
--begin
--set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
--end
--else
--begin
--set @newIID=1000
--end

--set @WIP=0

--insert into FITABLE010 
--(
--  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN18,COLUMN19, COLUMN13,COLUMNA02,COLUMNA03
--)
--values
--(
--  @newIID,@COLUMN03,0,0, 0,0,(@WIP+cast(@COLUMN08 as decimal(18,2))),@COLUMN17,@COLUMN13,@COLUMNA02,@COLUMNA03 
--)
--END


set @ReturnValue = 1

--END
END
IF @Direction = 'Select'
BEGIN
select * from PUTABLE004
END 

IF @Direction = 'Update'
BEGIN
declare @prvQty decimal(18,2)
set @prvQty=(select sum(isnull(COLUMN08,0)) from PUTABLE004 where COLUMN12=@COLUMN12)
set @COLUMN13 =(select COLUMN13 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN14 =(select COLUMN14 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN15 =(select COLUMN15 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
set @COLUMN16 =(select COLUMN16 from  PUTABLE003 Where COLUMN01=@COLUMN12 );
--EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
if(@COLUMN26 is null)
begin
set @COLUMN26 =(select COLUMN26 from  PUTABLE004 Where COLUMN02=@COLUMN02 )
end
set @COLUMNA02=( CASE WHEN (@COLUMN12!= '' and @COLUMN12 is not null and @COLUMN12!= 0 ) THEN (select COLUMN13 from PUTABLE003 
  where COLUMN01=@COLUMN12) else @COLUMNA02  END )
UPDATE PUTABLE004 SET
   COLUMN02=@COLUMN02,     COLUMN03=@COLUMN03,     COLUMN04=@COLUMN04,     COLUMN05=@COLUMN05,     COLUMN06=@COLUMN06, 
   COLUMN08=@COLUMN08,     COLUMN09=@COLUMN09,     COLUMN10=@COLUMN10,     COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,     COLUMN13=@COLUMN13,     COLUMN14=@COLUMN14,     COLUMN15=@COLUMN15,     COLUMN16=@COLUMN16,
   COLUMN17=@COLUMN17,     COLUMN18=@COLUMN17,     COLUMN19=@COLUMN08,     COLUMN20=@COLUMN20,     COLUMN21=@COLUMN21,
   COLUMN22=@COLUMN22,     COLUMN23=@COLUMN23,     COLUMN24=@COLUMN24,     COLUMN25=@COLUMN25,     COLUMN26=@COLUMN26,
   COLUMN27=@COLUMN27,
   COLUMNA01=@COLUMNA01,   COLUMNA02=@COLUMNA02,   COLUMNA03=@COLUMNA03,   COLUMNA04=@COLUMNA04,   COLUMNA05=@COLUMNA05,
     COLUMNA07=@COLUMNA07,   COLUMNA08=@COLUMNA08,   COLUMNA09=@COLUMNA09,   COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,   COLUMNA12=@COLUMNA12,   COLUMNA13=@COLUMNA13,   COLUMNB01=@COLUMNB01,   COLUMNB02=@COLUMNB02,
   COLUMNB03=@COLUMNB03,   COLUMNB04=@COLUMNB04,   COLUMNB05=@COLUMNB05,   COLUMNB06=@COLUMNB06,   COLUMNB07=@COLUMNB07,
   COLUMNB08=@COLUMNB08,   COLUMNB09=@COLUMNB09,   COLUMNB10=@COLUMNB10,   COLUMNB11=@COLUMNB11,   COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,   COLUMND02=@COLUMND02,   COLUMND03=@COLUMND03,   COLUMND04=@COLUMND04,   COLUMND05=@COLUMND05,
   COLUMND06=@COLUMND06,   COLUMND07=@COLUMND07,   COLUMND08=@COLUMND08,   COLUMND09=@COLUMND09,   COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02

 if exists(select column01 from PUTABLE004 where column12=@COLUMN12)
begin

SET @PID=(SELECT COLUMN06 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
SET @TQty=(SELECT sum(isnull(COLUMN07,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
SET @BQty=(SELECT sum(isnull(COLUMN13,0)) FROM PUTABLE002 WHERE columna13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
set @FRMID= (SELECT COLUMN03 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
set @JRStatus= (SELECT COLUMN20 FROM PUTABLE003 WHERE COLUMN01=@COLUMN12)
--EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
set @ItemLineID=(select COLUMN26 from PUTABLE004 where COLUMN02=@COLUMN02)
set @COLUMN07=(select sum(isnull(COLUMN12,0)) from PUTABLE002 where columna13=0 and COLUMN19= (select COLUMN01 from PUTABLE001 where COLUMN02= @PID) and COLUMN02=@ItemLineID)
UPDATE PUTABLE002 SET   COLUMN12=(CAST(@COLUMN08 AS decimal(18,2))+ CAST(@COLUMN07 AS decimal(18,2))) where COLUMN02=@ItemLineID and COLUMN19=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))

--IF(@TQty=(SELECT sum(COLUMN08) FROM PUTABLE004 WHERE  columna13=0 and COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN06=@PID) ))
--begin
--if(@TQty=@BQty)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
--end
--else if(@BQty=0)
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=9)
--end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=41)
--end
--end

--ELSE 
--BEGIN
SET @RecQty=(SELECT sum(isnull(COLUMN12,0)) FROM PUTABLE002 WHERE columna13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID))
if(@TQty=@RecQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=30)
end
else 
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=28)
end
--else
--begin
--set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=06)
--end
--end

UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE003 where COLUMN01= @COLUMN12))
UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=05) where COLUMN01=@COLUMN12




--if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17)
--begin
--declare @PrvQty decimal(18,2)
--set @PrvQty=(SELECT COLUMN08 FROM PUTABLE004 WHERE  columna13=0 and COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN06=@PID) )
--set @Qty_BACK =0
--set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17)- cast(@COLUMN08 as decimal(18,2));
--set @WIP=(select isnull(COLUMN18,0) from FITABLE010 where COLUMN03=@COLUMN03 AND  COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17)-@PrvQty
--set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17);
--set @Qty=cast(@COLUMN08 as decimal(18,2));
--set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
--set @Qty_AVL=(@Qty_On_Hand-@Qty_Commit)
--if(@Qty_AVL<0)
--begin
--set @Qty_AVL=0
--set @Qty_BACK =@Qty_Commit
--set @Qty_Commit=0
--end

--UPDATE FITABLE010 SET COLUMN18=(@WIP+cast(@COLUMN08 as decimal(18,2))) WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN13 and COLUMN19=@COLUMN17
--end
--else
--begin
--set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
--if(@tmpnewIID1>0)
--begin
--set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
--end
--else
--begin
--set @newIID=1000
--end

--set @WIP=0

--insert into FITABLE010 
--(
--  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17,COLUMN18,COLUMN19, COLUMN13,COLUMNA02,COLUMNA03
--)
--values
--(
--  @newIID,@COLUMN03,0,0, 0,0,(@WIP+cast(@COLUMN08 as decimal(18,2))),@COLUMN17,@COLUMN13,@COLUMNA02,@COLUMNA03 
--)
--END


set @ReturnValue = 1

END
END

else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE004 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02


END
end try
begin catch
	if not exists(select column01 from putable004 where column12=@column12)
		begin
			delete from putable003 where column01=@column12
            return 0
		end
return 0
end catch
end









GO
