USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetSalesReports]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[GetSalesReports]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Location    nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
	@Query1 nvarchar(max)=null,
	@Query2 nvarchar(max)=null,
	@DateF nvarchar(250)=null,
@DisplayType      nvarchar(250)= null,
@Category      nvarchar(250)= null)

as 
begin



IF  @ReportName='SOSummary'
BEGIN

if @Type!=''
begin

set @whereStr= ' where Type2 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s)'
end

if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and s5.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and s5.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Project!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @Vendor!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where NameV in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and NameV in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if @Customer!=''
begin

if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where s5.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and s5.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1)'
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
 end
set @Query1=
'select c7.COLUMN03 ou,s2.COLUMN05 customer,s5.COLUMN04 sono,s5.COLUMN15 amt,s5.COLUMN16 status, 
FORMAT(cast( s5.COLUMN06 as date), '''+ @DateF+''') dt  ,s5.COLUMN03 fid

from  SATABLE005 s5 inner join CONTABLE007 c7 on c7.COLUMN02=s5.COLUMN24
left outer join SATABLE002 s2 on s2.COLUMN02=s5.COLUMN05

where isnull((s5.COLUMNA13),0)=0 and  s5.COLUMN29=1002 and
 s5.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +'''  AND  s5.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) 
  AND  s5.COLUMNA03='+@AcOwner+' '+ @whereStr +' order by  Convert(DateTime,s5.COLUMN06,103) desc'

exec (@Query1) 


end


ELSE IF  @ReportName='Top10Customers'
BEGIN

SELECT  s1.COLUMN04, s1.COLUMN05,FORMAT( max(s2.COLUMN06),@DateF) date, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE002 AS s1 INNER JOIN SATABLE005 AS s2 ON s1.COLUMN02 = s2.COLUMN05 
 where isnull((s1.COLUMNA13),0)=0 and s1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or s1.COLUMNA02 is null AND s1.COLUMNA03=@AcOwner  
 and s2.COLUMN06 between @FromDate and @ToDate  GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC 

end


ELSE IF @ReportName='SalesByProductSummary'
	BEGIN
		IF(@OperatingUnit!='')

				 if @OperatingUnit!=''
begin
 set @whereStr= ' and S10.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end 
         if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end

				DECLARE @PERECENT DECIMAL(18,2)
				SET @PERECENT=(SELECT SUM(S10.COLUMN14) FROM SATABLE010 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05 AND S10.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	
				INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				WHERE  S9.COLUMN08 BETWEEN @FromDate and @ToDate AND (S10.COLUMNA02=@OperatingUnit or S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) )
				AND S10.COLUMNA03=@AcOwner AND ISNULL(S10.COLUMNA13,0)=0)
				
				set @Query1=
				'SELECT M7.COLUMN04 ITEM,C7.COLUMN04 [OPERATING UNIT],SUM(S10.COLUMN10) QTY,SUM(S10.COLUMN14) AMOUNT,CAST((100/ '+cast ( @PERECENT as nvarchar(250))+' )*(SUM(S10.COLUMN14)) AS DECIMAL(18,4)) [% OF SALE]
				,CAST(SUM(S10.COLUMN14)/SUM(S10.COLUMN10)AS DECIMAL(18,2)) [AVG PRICE], '+ cast ( @PERECENT as nvarchar(250)) +' TOTAL 
				,M7.COLUMN10 BrandID,m5.COLUMN04 as Brand ,S10.COLUMNA02 OPID   ,1277 fid, null [NO]  FROM SATABLE010 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05 AND S10.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	
				INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+'
				WHERE  
				 S9.COLUMN08 BETWEEN '''+ @FromDate +''' and '''+ @ToDate +'''  
				 AND S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) 
				 AND S10.COLUMNA03= '+@AcOwner+ ''+ @whereStr + ' AND ISNULL(S10.COLUMNA13,0)=0
				 GROUP BY M7.COLUMN04,C7.COLUMN04,m5.COLUMN04,M7.COLUMN10 ,S10.COLUMNA02 HAVING SUM(ISNULL(S10.COLUMN14,0))>0'
				
exec (@Query1) 

		
	END


	ELSE IF @ReportName='SalesByProductDetails'
	BEGIN
		IF(@OperatingUnit!='')

				 if @OperatingUnit!=''
begin
 set @whereStr= ' and S10.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end 
         if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end

		set @Query1=
				'SELECT M7.COLUMN04 ITEM,C7.COLUMN03 [OPERATING UNIT],FORMAT((S9.COLUMN08),'''+ @DateF+''') [DATE],S9.COLUMN08 Dtt,''Invoice''[TRANSACTION TYPE],(S9.COLUMN04) [NO],
				(S2.COLUMN05) [CLIENT],(S9.COLUMN12) [MEMO/DESCREPTION],(S10.COLUMN10) [QTY],(S10.COLUMN13) [RATE],(S10.COLUMN14) AMOUNT  ,(F.COLUMN04) LOT
		
				,M7.COLUMN10 BrandID,m5.COLUMN04 as Brand ,S10.COLUMNA02 OPID  ,S9.COLUMN03 fid  FROM SATABLE010 S10
				INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
				INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
				INNER JOIN SATABLE002 S2 ON S2.COLUMN02=S9.COLUMN05    AND S10.COLUMNA03=S2.COLUMNA03 AND ISNULL(S2.COLUMNA13,0)=0
				INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=S10.COLUMNA02 AND S10.COLUMNA03=C7.COLUMNA03 AND ISNULL(C7.COLUMNA13,0)=0
				left outer JOIN FITABLE043 F ON F.COLUMN02=S10.COLUMN31 AND S10.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0  
				 left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=S10.COLUMN05 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03='+@AcOwner+'
				WHERE  S9.COLUMN08 BETWEEN  '''+ @FromDate +''' and '''+ @ToDate +'''   AND S10.COLUMNA03='+@AcOwner+ '  AND 
				    S10.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND
				ISNULL(S10.COLUMNA13,0)=0 '+ @whereStr + ' order by  Convert(DateTime,S9.COLUMN08,103) desc '
			
exec (@Query1) 

		
	END
	ELSE IF @ReportName='SalesByProductDetailReport'
	BEGIN
	if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND S9.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and S9.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end	
 if @Item!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and S10.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
else
begin
set @whereStr=@whereStr+' and S10.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
end

		 if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end

		set @Query1=
				'SELECT FORMAT((S9.COLUMN08),'''+ @DateF+''') [Date],M7.COLUMN04 Item,(S10.COLUMN10) [QTY],(S10.COLUMN13) [RATE],(S10.COLUMN14) AMOUNT,(S9.COLUMN04) [Invoice NO],S2.COLUMN05 [Customer],sum(S9.COLUMN35+S9.COLUMN36) TransportCharges,(S9.COLUMN34) TrackingNo,(S9.COLUMN40) DriverName,(S9.COLUMN41) TruckNo,(S9.COLUMN42) DriverContactNo,S9.COLUMN05 Customers,S10.COLUMN05 Items,S9.COLUMN03 fid FROM SATABLE010 S10
INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05   AND S10.COLUMNA03=M7.COLUMNA03 AND ISNULL(M7.COLUMNA13,0)=0
INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15   AND S10.COLUMNA03=S9.COLUMNA03 AND S10.COLUMNA02=S9.COLUMNA02 AND ISNULL(S9.COLUMNA13,0)=0
INNER JOIN SATABLE002 S2 ON S2.COLUMN02=S9.COLUMN05    AND S10.COLUMNA03=S2.COLUMNA03 AND ISNULL(S2.COLUMNA13,0)=0
WHERE S9.COLUMN08 between '''+ @FromDate +''' and '''+ @ToDate +''' and S10.COLUMNA03='+@AcOwner+ ' AND
ISNULL(S10.COLUMNA13,0)=0 '+ @whereStr + '
Group by S9.COLUMN08,M7.COLUMN04,S10.COLUMN10,S10.COLUMN13,S10.COLUMN14,S9.COLUMN04,S2.COLUMN05,S9.COLUMN34,S9.COLUMN40,S9.COLUMN41,S9.COLUMN42,S9.COLUMN05,M7.COLUMN02,S10.COLUMN05,S9.COLUMN03  order by  Convert(DateTime,S9.COLUMN08,103) desc '
				
			
exec (@Query1) 

--		select * into #SalesByProductDetail from (
--		select format(S9.COLUMN08, 'dd/MM/yyyy') [Date],M7.COLUMN04 ITEM,(S10.COLUMN10) [QTY],(S10.COLUMN13) [RATE],(S10.COLUMN14) AMOUNT,(S9.COLUMN04) [Invoice NO],(S2.COLUMN05) [Customer],sum(S9.COLUMN35+S9.COLUMN36) TransportCharges,(S9.COLUMN34) TrackingNo,(S9.COLUMN40) DriverName,(S9.COLUMN41) TruckNo,(S9.COLUMN42) DriverContactNo FROM SATABLE010 S10
--INNER JOIN MATABLE007 M7 ON M7.COLUMN02=S10.COLUMN05 
--INNER JOIN SATABLE009 S9 ON S9.COLUMN01=S10.COLUMN15 
--INNER JOIN SATABLE002 S2 ON S2.COLUMN02=S9.COLUMN05 
--    where isnull((S10.COLUMNA13),0)= 0 and S10.COLUMNA03=@AcOwner
--	) Query 

--set @Query1='select S9.COLUMN08,M7.COLUMN04,S10.COLUMN10,S10.COLUMN13,S10.COLUMN14,S9.COLUMN04,S2.COLUMN05,S9.COLUMN34,S9.COLUMN40,S9.COLUMN41,S9.COLUMN42 from #SalesByProductDetail'+@whereStr +' 
--group by S9.COLUMN08,M7.COLUMN04,S10.COLUMN10,S10.COLUMN13,S10.COLUMN14,S9.COLUMN04,S2.COLUMN05,S9.COLUMN34,S9.COLUMN40,S9.COLUMN41,S9.COLUMN42'
--exec (@Query1)
	END



	ELSE IF  @ReportName='SalesByCustomer'
BEGIN

if @OperatingUnit!=''
begin
 set @whereStr= ' AND a.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end

if @Category!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND S2.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
else
begin
set @whereStr=@whereStr+' and S2.COLUMN07  in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' AND 1=1'
end
 --,M7.COLUMN04 itm 
set @Query1=
'SELECT C7.COLUMN03 ou,S2.COLUMN05  customer,a.COLUMN04 sono,FORMAT(a.COLUMN08, '''+ @DateF+''')  dt,a.COLUMN08 Dtt,
 ( C07.COLUMN04 ) TrnsType

  ,a.COLUMN04 num ,  b.COLUMN06 descc,sum(isnull(b.COLUMN10,0)) QTY 
  ,sum(b.COLUMN14) amont,sum(b.COLUMN23) taxamont ,(F.COLUMN04) LOT,M7.COLUMN10 BrandID

  ,m5.COLUMN04 as Brand ,a.COLUMN05 CID,a.COLUMN03 fid
   , a.COLUMN05 c , '+@FromDate+' fd , '+@ToDate+' td ,M2.column04 Category,M7.column50 Descr,M7.column04 itm

FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 and isnull(b.columna13,0)=0 
left outer JOIN FITABLE043 F ON F.COLUMN02=b.COLUMN31 AND b.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0 
 left outer JOIN MATABLE007 M7 ON M7.COLUMN02=b.COLUMN05 AND b.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	 
 LEFT OUTER JOIN CONTABLE007 C7 ON C7.COLUMN02=a.COLUMN14 
 LEFT OUTER JOIN CONTABLE0010 C07 ON C07.COLUMN02=1277
 LEFT OUTER JOIN SATABLE002 S2 ON S2.COLUMN02=a.COLUMN05
  left outer JOIN MATABLE002 M2 ON M2.COLUMN02=S2.COLUMN07 AND M2.COLUMNA03=S2.COLUMNA03  AND ISNULL(M2.COLUMNA13,0)=0	
left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=b.COLUMN05  and isnull(m5.COLUMNA13,0)=0 
 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN19=1002 and
 a.COLUMN08 between '''+ @FromDate +''' and '''+ @ToDate +'''  AND
   a.COLUMNA03='+@AcOwner+ ' AND 
  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) '+ @whereStr + '
group by C7.COLUMN03,S2.COLUMN05,a.COLUMN04,a.COLUMN08, C07.COLUMN04 ,b.COLUMN13, b.COLUMN06,F.COLUMN04 , m5.COLUMN04, M7.COLUMN10 ,a.COLUMN04,a.COLUMN03,a.COLUMN05,

a.COLUMN08,M2.column04,M7.column50,M7.column04 order by  Convert(DateTime,a.COLUMN08,103) desc'


exec (@Query1)


end

	ELSE IF  @ReportName='SalesByCustomerDateWise'
BEGIN

if @OperatingUnit!=''
begin
 set @whereStr= ' AND a.COLUMNA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and M7.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

if @Category!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND S2.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
else
begin
set @whereStr=@whereStr+' and S2.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
end

if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end



if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' AND 1=1'
end
 --,M7.COLUMN04 itm 
set @Query1=
'SELECT C7.COLUMN03 ou,S2.COLUMN05  customer,a.COLUMN04 sono,
(CASE  
 WHEN ('+@DisplayType+'= 23066 ) then (FORMAT(cast(  a.COLUMN08 as date), '''+ @DateF+'''))
else  ''1''  end)dt,

(CASE  
 WHEN ('+@DisplayType+'= 23066 ) then CONVERT(CHAR(4), a.COLUMN08, 100) +''-''+ CONVERT(CHAR(4),  a.COLUMN08, 120)
else  (FORMAT(cast(  a.COLUMN08 as date), '''+ @DateF+'''))  end) D
  
 ,a.COLUMN08 Dtt,
 ( C07.COLUMN04 ) TrnsType,M7.COLUMN04 Itm

  ,a.COLUMN04 num ,  b.COLUMN06 descc 
  ,sum(b.COLUMN14) amont,sum(b.COLUMN23) taxamont ,(F.COLUMN04) LOT,M7.COLUMN10 BrandID

  ,m5.COLUMN04 as Brand ,a.COLUMN05 CID,a.COLUMN03 fid
   , a.COLUMN05 c , '+@FromDate+' fd , '+@ToDate+' td ,sum(isnull(b.COLUMN10,0)) qty
   ,m10.COLUMN09 AttBY,M2.column04 Category,m7.column50 Descr
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 and isnull(b.columna13,0)=0 
left outer JOIN FITABLE043 F ON F.COLUMN02=b.COLUMN31 AND b.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0 
 left outer JOIN MATABLE007 M7 ON M7.COLUMN02=b.COLUMN05 AND b.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	 
 LEFT OUTER JOIN CONTABLE007 C7 ON C7.COLUMN02=a.COLUMN14 
 LEFT OUTER JOIN CONTABLE007 C07 ON C07.COLUMN02=1277
 LEFT OUTER JOIN SATABLE002 S2 ON S2.COLUMN02=a.COLUMN05  AND S2.COLUMNA03=a.COLUMNA03 
 left outer JOIN MATABLE002 M2 ON M2.COLUMN02=S2.COLUMN07 AND M2.COLUMNA03=S2.COLUMNA03  AND ISNULL(M2.COLUMNA13,0)=0	

left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=b.COLUMN05  and isnull(m5.COLUMNA13,0)=0 
left outer join MATABLE010 m10 on m10.COLUMN02=a.COLUMN49 and m10.COLUMNA03=a.COLUMNA03 and (m10.COLUMN30=''True'' or m10.COLUMN30=1) 
 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN19=1002 and
 a.COLUMN08 between '''+ @FromDate +''' and '''+ @ToDate +'''  AND   a.COLUMNA03='+@AcOwner+ ' AND
  b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) '+ @whereStr + '
group by C7.COLUMN03,S2.COLUMN05,a.COLUMN04,M7.COLUMN04, a.COLUMN08, C07.COLUMN04 , b.COLUMN06,F.COLUMN04 , m5.COLUMN04, M7.COLUMN10 ,a.COLUMN04,a.COLUMN03,a.COLUMN05
  ,m10.COLUMN09,M2.column04,M7.column50 order by  Convert(DateTime,a.COLUMN08,103) desc'


exec (@Query1)


end


Else if @ReportName='CreditMemoByCustomerDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and a.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @Category!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND S.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
else
begin
set @whereStr=@whereStr+' and S.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
end
if @Vendor!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
else
begin
set @whereStr=@whereStr+' and a.COLUMN05 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Vendor+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
set @Query1='
SELECT  o.COLUMN03 ou,(case when a.COLUMN51=''22305'' then s1.COLUMN05 else s.COLUMN05 end) customer,FORMAT(cast( a.COLUMN06 as date),'''+ @DateF+''') dt, a.COLUMN06 Dtt,
c.COLUMN04 TrnsType, m.COLUMN04 itm ,a.COLUMN04 Creditnum ,b.COLUMN06 descc,b.COLUMN07 qty ,b.COLUMN09 price  ,b.COLUMN25 amont,a.COLUMN05 CID,a.COLUMN05 VID,a.COLUMN24 OPID,
m5.COLUMN02 BrandID,m5.COLUMN04 Brand ,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,a.COLUMN04 Trans# ,a.COLUMN03 fid ,m1.column02  CID1,m1.column04 Category  FROM SATABLE005 a inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN24 
left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03 left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03 
left outer join CONTABLE0010 c on c.COLUMN02=a.COLUMN03 left outer join MATABLE007 m on m.COLUMN02=b.COLUMN03 left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10
 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0 
left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
left outer join MATABLE002 m1 on m1.column02 = s.column07 and m1.COLUMNA03=s.COLUMNA03 and  isnull(m1.COLUMNA13,0)=0
 where isnull(a.COLUMNA13,0)=0 and a.COLUMN03 in(1330,1603)
 and a.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND a.COLUMNA03='+@AcOwner+ ' 
  '+ @whereStr + '  order by Convert(DateTime,a.COLUMN06,103) desc'

exec (@Query1) 
END


Else IF  @ReportName='CreditMemoByCustomerSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and a.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m.COLUMN10 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Category!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' AND S.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
else
begin
set @whereStr=@whereStr+' and S.COLUMN07 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
set @Query1='
SELECT  o.COLUMN03 ou,(case when a.COLUMN51=''22305'' then s1.COLUMN05 else s.COLUMN05 end) customer,FORMAT(cast( a.COLUMN06 as date),'''+ @DateF+''')dt,a.COLUMN06 Dtt,c.COLUMN04 TrnsType, 
m.COLUMN04 itm ,a.COLUMN04 Creditnum ,b.COLUMN06 descc,b.COLUMN07 qty ,b.COLUMN09 price  ,b.COLUMN25 amont,a.COLUMN05 CID,a.COLUMN24 OPID,m5.COLUMN04 Brand 
, isnull(m.COLUMN10,0) BrandID  ,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,a.COLUMN04 Trans# ,a.COLUMN03 fid   , a.COLUMN05 c , '+@FromDate+' fd , '+@ToDate+' td,m1.column02  CID1,m1.column04 Category  FROM SATABLE005 a 
inner join SATABLE006 b on b.COLUMN19=a.COLUMN01 
left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN24 
left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03
left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03
left outer join CONTABLE0010 c on c.COLUMN02=a.COLUMN03 
left outer join MATABLE007 m on m.COLUMN02=b.COLUMN03
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0  
left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
left outer join MATABLE002 m1 on m1.column02 = s.column07 and m1.COLUMNA03=s.COLUMNA03 and  isnull(m1.COLUMNA13,0)=0
where isnull(a.COLUMNA13,0)=0 and a.COLUMN03 in (1330,1603)
 and a.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s)
 AND a.COLUMNA03='+@AcOwner+ ' '+ @whereStr + '  order by Convert(DateTime,a.COLUMN06,103) desc'

exec (@Query1) 
END

Else IF  @ReportName='CreditMemoByProductDetails'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and a.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
set @Query1=
'SELECT  o.COLUMN03 ou,(case when a.COLUMN51=''22305'' then s1.COLUMN05 else s.COLUMN05 end) customer,FORMAT(cast( a.COLUMN06 as date),'''+ @DateF+''') dt,a.COLUMN06 Dtt,
c.COLUMN04 TrnsType, m.COLUMN04 itm ,L.COLUMN04 Lot,a.COLUMN04 Creditnum ,b.COLUMN06 descc,b.COLUMN07 qty ,b.COLUMN09 price  ,b.COLUMN25 amont,a.COLUMN24 OPID,
m5.COLUMN02 BrandID,m5.COLUMN04 Brand ,c1.COLUMN04 Location,c1.COLUMN02 LocationID,a.COLUMN04 Trans# ,a.COLUMN03 fid FROM SATABLE005 a inner join SATABLE006 b 
on b.COLUMN19=a.COLUMN01 left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN24 left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03 
left outer join SATABLE001 s1 on s1.COLUMN02=a.COLUMN05 and s1.COLUMNA03=a.COLUMNA03 left outer join CONTABLE0010 c on c.COLUMN02=a.COLUMN03 left outer join MATABLE007 m 
on m.COLUMN02=b.COLUMN03 left outer join FITABLE043 L on L.COLUMN02=b.COLUMN17 and L.COLUMNA03=b.COLUMNA03 and isnull(L.COLUMNA13,0)=0 left outer join MATABLE005 m5 
on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0 left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 
and  isnull(c1.COLUMNA13,0)=0
where isnull(a.COLUMNA13,0)=0 and a.COLUMN03=1330 and a.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' and a.COLUMNA02 
in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND a.COLUMNA03='+@AcOwner+ ' '+ @whereStr + '  order by Convert(DateTime,a.COLUMN06,103) desc'

exec (@Query1) 
END

Else if @ReportName='CreditMemoByProductSummary'
BEGIN
if @OperatingUnit!=''
begin
 set @whereStr= ' and a.COLUMN24 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and m5.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and c1.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+ @Location+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' and 1=1'
end
SET @PERECENT=(SELECT SUM(b.COLUMN25) FROM SATABLE006 b
INNER JOIN MATABLE007 M7 ON M7.COLUMN02=b.COLUMN03 
INNER JOIN CONTABLE007 C7 ON C7.COLUMN02=b.COLUMNA02
left outer join FITABLE043 L on L.COLUMN02=b.COLUMN17 
inner join SATABLE005 a on a.COLUMN01=b.COLUMN19 where isnull(b.COLUMNA13,0)=0 and a.COLUMN03=1330 and a.COLUMN06 between @FromDate and @ToDate
 and a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner )

set @Query1='
SELECT  o.COLUMN03 ou, m.COLUMN04 itm ,L.COLUMN04 Lot,sum(b.COLUMN07) qty ,sum(b.COLUMN25) amont,
CAST((100/ '+cast ( @PERECENT as nvarchar(250))+' )*(SUM(b.COLUMN25)) AS DECIMAL(18,4)) [% OF SALE],CAST(SUM(b.COLUMN25)/SUM(b.COLUMN07)AS DECIMAL(18,2)) [AVG PRICE],
 '+cast ( @PERECENT as nvarchar(250))+'  TOTAL,
a.COLUMN24 OPID,m5.COLUMN02 BrandID,m5.COLUMN04 Brand,c1.COLUMN04 Location,c1.COLUMN02 LocationID
,a.COLUMN04 Trans# ,a.COLUMN03 fid FROM SATABLE006 b  
inner join CONTABLE007 o on o.COLUMN02=b.COLUMNA02  
inner join MATABLE007 m on m.COLUMN02=b.COLUMN03 
inner join SATABLE005 a on isnull(a.COLUMNA13,0)=0 and a.COLUMN01=b.COLUMN19 
left outer join MATABLE005 m5 on m5.COLUMN02=m.COLUMN10 and m.COLUMN02=b.COLUMN03 and isnull(m5.COLUMNA13,0)=0
left outer join CONTABLE030 c1 on  c1.COLUMN02=a.COLUMN48 and c1.COLUMNA03=a.COLUMNA03 and  isnull(c1.COLUMNA13,0)=0
left outer join FITABLE043 L on L.COLUMN02=b.COLUMN17
where isnull(b.COLUMNA13,0)=0 and a.COLUMN03=1330 and a.COLUMN06 between '''+ @FromDate +''' and '''+ @ToDate +''' and 
a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) AND a.COLUMNA03='+@AcOwner+ ' '+ @whereStr + ' 
group by o.COLUMN03,m.COLUMN04,L.COLUMN04,a.COLUMN24,m5.COLUMN02,m5.COLUMN04,c1.COLUMN04, c1.COLUMN02,a.COLUMN04  ,a.COLUMN03   HAVING SUM(ISNULL(b.COLUMN25,0))>0 '


exec (@Query1) 
END



ELSE IF  @ReportName='CustomerPaymentDues'
BEGIN

if @OperatingUnit!=''
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1)'
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s) OR BrandID = 1)'
end
end


if @Category!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where cat in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
else
begin
set @whereStr=@whereStr+' and cat in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Category+''') s)'
end
end

if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where 1=1'
end
select * into #CustomerPaymentDues from (

SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,c.COLUMN11 As Mobile,
(select COLUMN10 from    SATABLE003 where COLUMN02=c.COLUMN16) as City,
c.COLUMN05 Party,FORMAT(a.COLUMN04,@DateF) Date,a.COLUMN04 Dtt,(case when a.COLUMN06  = 'JOURNAL ENTRY' then F31.COLUMN04 else a.COLUMN05 end) [Bill No.],a.COLUMN08 Memo,a.COLUMN09 Amount,a.COLUMN11 Paid,
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [0-30],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [31-45],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [46-60],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [61-90],
(CASE WHEN a.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN isnull(SUM(a.COLUMN09), 0) - isnull(SUM(a.COLUMN11), 0) END) AS [>90],a.COLUMN12 Balance,a.COLUMN06 fname
,a.COLUMN07 CID,a.COLUMNA02 OPID 
--,(case
-- when a.COLUMN06='Invoice' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner))

-- when a.COLUMN06='Credit Memo' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))

--end) Brand ,
--(case
-- when a.COLUMN06='Invoice' then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select (COLUMN10) from MATABLE007 where COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner)) 

-- when a.COLUMN06='Credit Memo'  then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))
-- when a.COLUMN06='JOURNAL ENTRY' then (1)
--  when a.COLUMN06='RECEIPT VOUCHER' then (1)
--end) BrandID
 ,m5.COLUMN04 Brand,c.COLUMN37 BrandID,M2.COLUMN04 Category,M2.COLUMN02 cat
 FROM PUTABLE018 a 
 left join SATABLE002 c on c.COLUMN02=a.COLUMN07 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 LEFT JOIN FITABLE031 F31 ON cast(F31.COLUMN01 as nvarchar(250)) = cast(a.COLUMN05 as nvarchar(250)) AND F31.COLUMNA03=a.COLUMNA03 AND F31.COLUMNA02=a.COLUMNA02 and isnull(F31.COLUMNA13,0)=0
 left join MATABLE002 m2  on m2.COLUMN02=c.COLUMN07 and c.COLUMNA03=C.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER' or a.COLUMN06 like 'PAYMENT VOUCHER')  and
 a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)   AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0
  and  a.COLUMN04 between @FromDate and @ToDate   
  group by a.COLUMNA02,c.COLUMN11,c.COLUMN16,c.COLUMN05,a.COLUMN04,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN12,a.COLUMN07 ,a.COLUMN06,m5.COLUMN04,c.COLUMN37,M2.COLUMN04,M2.COLUMN02,a.COLUMN02,a.COLUMN08,F31.COLUMN04

  union all
select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02 and  COLUMNA03=a.COLUMNA03) as OperatingUnit,c.COLUMN11 As Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=c.COLUMN16) as City,c.column05 Party,FORMAT(a.COLUMN05,@DateF) Date,a.COLUMN05 Dtt, a.COLUMN04 [Bill No.],a.COLUMN11 Memo,0.00 Amount,cast(isnull(a.COLUMN19,0)as decimal(18,2)) Paid,
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2))  END) AS [0-30],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2)) END) AS [31-45],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2))  END) AS [46-60],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2)) END) AS [61-90],
(CASE WHEN a.COLUMN05 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN -cast(isnull((a.COLUMN19), 0) as decimal(18,2))  END) AS [>90],
0.00 Balance   ,cast(a.COLUMN03 as varchar) fname 
,a.COLUMN08 CID,a.COLUMNA02 OPID ,m5.COLUMN04 Brand,c.COLUMN37 BrandID,M2.COLUMN04 Category,M2.COLUMN02 cat  from FITABLE023 a
 left join SATABLE002 c on c.COLUMN02=a.COLUMN08 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 left join MATABLE002 m2  on m2.COLUMN02=c.COLUMN07 and c.COLUMNA03=C.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
 where a.COLUMN03=1386 and isnull((a.COLUMNA13),0)=0    AND a.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  a.COLUMNA03=@AcOwner
 and  a.COLUMN05 between @FromDate and @ToDate 

 union all
SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where COLUMN02=c.COLUMN16) as City,
c.COLUMN05 Party,FORMAT(b.COLUMN04,@DateF) Date,b.COLUMN04 Dtt,
b.COLUMN09 [Bill No.],NULL Memo, NULL Amount,b.COLUMN07 Paid,
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 30 AND GETDATE() - 0 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2))  END) AS [0-30],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 45 AND GETDATE() - 30 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2)) END) AS [31-45],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() -60 AND GETDATE() - 45 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2))  END) AS [46-60],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 90 AND GETDATE() - 60 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2)) END) AS [61-90],
(CASE WHEN b.COLUMN04 BETWEEN GETDATE() - 900 AND GETDATE() - 90 THEN -cast(isnull((b.COLUMN07), 0) as decimal(18,2))  END) AS [>90],
NULL Balance  ,b.COLUMN03 fname
,b.COLUMN06 CID,b.COLUMNA02 OPID,m5.COLUMN04 Brand,c.COLUMN37 BrandID ,M2.COLUMN04 Category,M2.COLUMN02 cat
 FROM FITABLE036 b  
 left join SATABLE002 c on c.COLUMN02=b.COLUMN06 and c.COLUMNA03=b.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 left join MATABLE002 m2  on m2.COLUMN02=c.COLUMN07 and c.COLUMNA03=C.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
  where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049  AND b.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  b.COLUMNA03=@AcOwner  
  and  b.COLUMN04 between @FromDate and @ToDate 
  
  --PREVIOUS BALANCE


  union all

  select OperatingUnit,Mobile,City,Party,Date,Dtt,[Bill No.],Memo,
      iif( sum(isnull(Amount,0))>sum(isnull(Paid,0)),sum(isnull(Amount,0))-sum(isnull(Paid,0)),0) Amount,
 
    iif( sum(isnull(Paid,0))>sum(isnull(Amount,0)),sum(isnull(Paid,0))-sum(isnull(Amount,0)),0) Paid,
 
  [0-30],[31-45],[46-60],[61-90],[>90],Balance,fname,CID,OPID,Brand,BrandID,Category,cat from (


SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02) as OperatingUnit,null As Mobile,
(select COLUMN10 from    SATABLE003 where COLUMN02=c.COLUMN16) as City,
c.COLUMN05 Party,null Date,null Dtt, 'PREVIOUS BALANCE'  [Bill No.],a.COLUMN08 Memo,a.COLUMN09 Amount,a.COLUMN11 Paid,
null  [0-30],
null  [31-45],
null  [46-60],
null  [61-90],
null  [>90],0 Balance,null fname
,a.COLUMN07 CID,a.COLUMNA02 OPID 
--,
--iif(@Brand='',
--null,
-- (case
-- when a.COLUMN06='Invoice' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select m5.column04 from MATABLE005 m5 inner join MATABLE007 m7 on m5.COLUMN02=m7.COLUMN10 where m7.COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner))

-- when a.COLUMN06='Credit Memo' then (select column04 from MATABLE005 where COLUMN02 = (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))

--end))
--  Brand ,
--iif(@Brand='',
--(case
-- when a.COLUMN06='Invoice' then (1)
--  when  a.COLUMN06='Payment' then  (1)
--  when a.COLUMN06='Credit Memo' then  (1)
--  when a.COLUMN06='JOURNAL ENTRY' then (1)
--  when a.COLUMN06='RECEIPT VOUCHER' then (1)
-- end),
--(case
-- when a.COLUMN06='Invoice' then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s10.COLUMN05)  from SATABLE009 s9 inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01 where s9.COLUMN04 = a.COLUMN05 and  s9.COLUMNA03=@AcOwner)))

-- when  a.COLUMN06='Payment' then (select (COLUMN10) from MATABLE007 where COLUMN02=
--( SELECT max(s10.COLUMN05) FROM  SATABLE012 s12 inner join SATABLE011 s11 on s11.COLUMN01=s12.COLUMN08 inner join SATABLE009 s9 on s9.COLUMN02=s12.COLUMN03
--inner join SATABLE010 s10 on s10.COLUMN15=s9.COLUMN01  where s11.COLUMN04=a.COLUMN05 and s11.COLUMNA03=@AcOwner)) 

-- when a.COLUMN06='Credit Memo'  then ( (select (COLUMN10) from MATABLE007 where COLUMN02 = 
--( select max(s06.COLUMN03)  from SATABLE005 s5 inner join SATABLE006 s06 on s06.COLUMN19=s5.COLUMN01 where s5.COLUMN04 = a.COLUMN05 and  s5.COLUMNA03=@AcOwner)))
-- when a.COLUMN06='JOURNAL ENTRY' then (1)
--  when a.COLUMN06='RECEIPT VOUCHER' then (1)
--end)) BrandID
 ,m5.COLUMN04 Brand,c.COLUMN37 BrandID,M2.COLUMN04 Category,M2.COLUMN02 cat 
 FROM PUTABLE018 a 
 left join SATABLE002 c on c.COLUMN02=a.COLUMN07 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 left join MATABLE002 m2  on m2.COLUMN02=c.COLUMN07 and c.COLUMNA03=C.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
   where  (a.COLUMN06 like 'Payment' or a.COLUMN06 like 'Invoice' or a.COLUMN06 like 'Credit Memo' or a.COLUMN06 like 'Debit Memo' or a.COLUMN06 like 'RECEIVE PAYMENT' or a.COLUMN06 like 'JOURNAL ENTRY' or a.COLUMN06 like 'Opening Balance' or a.COLUMN06 like 'RECEIPT VOUCHER' or a.COLUMN06 like 'PAYMENT VOUCHER')  and
 a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)   AND  a.COLUMNA03=@AcOwner  and isnull((a.COLUMNA13),0)=0
  and  a.COLUMN04 < @FromDate   
  --group by a.COLUMN07,a.COLUMNA02,a.COLUMN05,a.COLUMN09,a.COLUMN11,a.COLUMN04,a.COLUMN12 ,a.COLUMN06

  
  union all

select (select COLUMN03 from CONTABLE007 where COLUMN02=a.COLUMNA02 and  COLUMNA03=@AcOwner) as OperatingUnit,null As Mobile,
(select COLUMN10 from SATABLE003 where COLUMN02=c.COLUMN16) as City,c.column05 Party,
null Date,null Dtt, 'PREVIOUS BALANCE' [Bill No.],a.COLUMN11 Memo,0 Amount,cast(isnull(a.COLUMN19,0)as decimal(18,2)) Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0 Balance   ,null fname 
,a.COLUMN08 CID,a.COLUMNA02 OPID ,m5.COLUMN04 Brand,c.COLUMN37 BrandID,M2.COLUMN04 Category,M2.COLUMN02 cat   from FITABLE023 a
 left join SATABLE002 c on c.COLUMN02=a.COLUMN08 and c.COLUMNA03=a.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 left join MATABLE002 m2  on m2.COLUMN02=c.COLUMN07 and c.COLUMNA03=C.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
where a.COLUMN03=1386 and isnull((a.COLUMNA13),0)=0    AND a.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  a.COLUMNA03=@AcOwner
 and  a.COLUMN05 < @FromDate  
 union all

SELECT  (select COLUMN03 from CONTABLE007 where COLUMN02=b.COLUMNA02) OperatingUnit,NULL Mobile,(select COLUMN10 from SATABLE003 where 
COLUMN02=c.COLUMN16) as City,c.COLUMN05 Party,
null Date,null Dtt,'PREVIOUS BALANCE' [Bill No.],NULL Memo, 0 Amount,b.COLUMN07 Paid,
NULL [0-30],
NULL [31-45],
NULL [46-60],
NULL [61-90],NULL [>90],0 Balance  ,null fname
,b.COLUMN06 CID,b.COLUMNA02 OPID,m5.COLUMN04 Brand,c.COLUMN37 BrandID,M2.COLUMN04 Category,M2.COLUMN02 cat
 FROM FITABLE036 b 
 left join SATABLE002 c on c.COLUMN02=b.COLUMN06 and c.COLUMNA03=b.COLUMNA03 and isnull(c.COLUMNA13,0)=0
 left join MATABLE005 m5   on m5.COLUMN02=c.COLUMN37 and c.COLUMNA03=m5.COLUMNA03 and isnull(m5.COLUMNA13,0)=0
 left join MATABLE002 m2  on m2.COLUMN02=c.COLUMN07 and c.COLUMNA03=C.COLUMNA03 and isnull(m2.COLUMNA13,0)=0
  where isnull((b.COLUMNA13),0)=0 and  (b.COLUMN03='PAYMENT' ) and isnull(b.COLUMN10,0)=1049  AND b.COLUMNA02  in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  AND  b.COLUMNA03=@AcOwner  
  and  b.COLUMN04 < @FromDate 
 
 ) a group by

 a.OperatingUnit,a.Mobile,a.City,a.Party,a.Date,a.Dtt,a.[Bill No.],a.Memo,a.[0-30],a.[31-45],
 a.[46-60],a.[61-90],a.[>90],a.Balance,a.fname,a.CID,a.OPID,a.Brand,a.BrandID,A.Category,A.cat
 

)Query
set @Query1='select * from #CustomerPaymentDues'+@whereStr +' order by City,Dtt desc'
exec (@Query1) 
END

ELSE IF  @ReportName='SalesInvoiceRK'
BEGIN

if @Customer!=''
begin
 set @whereStr= ' WHERE iid in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' WHERE 1=1'
end
  
--set @Query1=
select * into #SalesInvoiceRK from (SELECT c8.COLUMN03 CoName,''OrderNo,'' PONo,a.COLUMN04 sono,'' SODate,
FORMAT(a.COLUMN08, @DateF) dt,IIF(ISNULL(b.COLUMNB01,'')='',MAX(M21.COLUMN04),b.COLUMNB01) Barcode,''SKUCode,'MENS WEAR' Categorys,m3.COLUMN04 Product,''Style,
m2S.COLUMN04 Typess,'SFS' TypeAbrv,m2c.COLUMN04 Shades,'Piece' Units,m5.COLUMN04 as Brand,(F.COLUMN04) LOT,M23.COLUMN04 Size,
isnull(b.COLUMN10,0) QTY,ISNULL(M24.COLUMN05,0) SMRP,ISNULL(b.COLUMN35,0) WSP,S2.COLUMN05 ConsigneeName,a.COLUMN40 TransporterName,''MRPDIFF,''WSPDIFF,'SS-18' SEASON,''QUALITY,
C7.COLUMN03 ou,a.COLUMN08 Dtt,( C07.COLUMN04 ) TrnsType,a.COLUMN04 num ,b.COLUMN06 descc 
 ,sum(b.COLUMN14) amont,sum(b.COLUMN23) taxamont ,M7.COLUMN10 BrandID ,a.COLUMN05 CID,a.COLUMN03 fid
 , a.COLUMN05 c , @FromDate fd , @ToDate td ,M2.column04 Category,M7.column50 Descr,M7.column04 ITEM,a.COLUMN02 iid,c8.COLUMN12 City,
 (select COLUMN10 from SATABLE003 where COLUMN02=S2.COLUMN16) as Place,M32.COLUMN04 HSNCode,ISNULL(b.COLUMN23,0) taxamt,
 CAST((ISNULL(b.COLUMN10,0)*ISNULL(b.COLUMN35,0))+ISNULL(b.COLUMN23,0) AS DECIMAL(18,2)) grossvalue,B.COLUMN02 lid
FROM SATABLE009 a inner join SATABLE010 b on b.COLUMN15=a.COLUMN01 and isnull(b.columna13,0)=0 
left outer JOIN FITABLE043 F ON F.COLUMN02=b.COLUMN31 AND b.COLUMNA03=F.COLUMNA03 AND ISNULL(F.COLUMNA13,0)=0 
 left outer JOIN MATABLE007 M7 ON M7.COLUMN02=b.COLUMN05 AND b.COLUMNA03=M7.COLUMNA03  AND ISNULL(M7.COLUMNA13,0)=0	 
 LEFT OUTER JOIN CONTABLE007 C7 ON C7.COLUMN02=a.COLUMN14 
 LEFT OUTER JOIN CONTABLE0010 C07 ON C07.COLUMN02=1277
 LEFT OUTER JOIN SATABLE002 S2 ON S2.COLUMN02=a.COLUMN05 and S2.COLUMNA03=a.COLUMNA03 and isnull(S2.COLUMNA13,0)=0
 left outer JOIN MATABLE002 M2 ON M2.COLUMN02=S2.COLUMN07 AND M2.COLUMNA03=S2.COLUMNA03  AND ISNULL(M2.COLUMNA13,0)=0	
left outer join MATABLE005 m5 on m5.COLUMN02=M7.COLUMN10 and M7.COLUMN02=b.COLUMN05  and isnull(m5.COLUMNA13,0)=0 
left outer join contable008 c8 on c8.COLUMNA03 = a.COLUMNA03 AND ISNULL(c8.COLUMNA13,0)=0
left outer join MATABLE002 M23 ON M23.COLUMN02 = b.COLUMN22 
left outer join MATABLE024 M24 ON M24.COLUMN07=b.COLUMN05 AND M24.COLUMNA03=b.COLUMNA03 AND ISNULL(M24.COLUMNA13,0)=0 AND M24.COLUMN06 ='Sales'
left outer join MATABLE032 M32 ON M32.COLUMN02 =M7.COLUMN75
left outer join matable002 m2c on m2c.COLUMN02 = M7.COLUMN42
left outer join matable002 m2S on m2S.COLUMN02 = M7.COLUMN43
left join MATABLE003 m3 on m3.COLUMN02 = M7.COLUMN12 AND  m3.COLUMNA03 = M7.COLUMNA03 AND isnull(m3.COLUMNA13,0)=0
LEFT JOIN MATABLE021 M21 ON M21.COLUMN05= b.COLUMN05 AND M21.COLUMN06 = b.COLUMN22 AND M21.COLUMNA03 = b.COLUMNA03
AND isnull(M21.COLUMNA13,0)=0  
 where isnull((a.COLUMNA13),0)=0 and  a.COLUMN19=1002
 --and a.COLUMN08 between @FromDate and @ToDate  AND
  and a.COLUMNA03=@AcOwner 
  --AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
group by C7.COLUMN03,S2.COLUMN05,a.COLUMN04,a.COLUMN08, C07.COLUMN04 , b.COLUMN06,F.COLUMN04 , m5.COLUMN04, M7.COLUMN10 ,a.COLUMN04,a.COLUMN03,a.COLUMN05,
a.COLUMN08,M2.column04,M7.column50,M7.column04,c8.COLUMN03,b.COLUMNB01,a.column02,M23.COLUMN04,b.COLUMN13,M24.COLUMN05,a.COLUMN40,c8.COLUMN12,S2.COLUMN16,M32.COLUMN04,m2c.COLUMN04,m2S.COLUMN04,b.COLUMN23,b.COLUMN10,b.COLUMN35,m3.COLUMN04,B.COLUMN02)  Query


--exec (@Query1)
set @Query1='select CoName,OrderNo,PONo,sono,SODate,dt,Barcode,SKUCode,Categorys,Product,Style,Typess,TypeAbrv,Shades,Units,Brand,
LOT,Size,QTY,SMRP,WSP,ConsigneeName,TransporterName,MRPDIFF,WSPDIFF,SEASON,QUALITY,
ou,Dtt,TrnsType,num,descc,amont,taxamont ,BrandID ,CID,fid,c,fd,td,Category,Descr,ITEM,iid,City,Place,HSNCode,taxamt,grossvalue,lid
from #SalesInvoiceRK '+@whereStr +' group by CoName,OrderNo,PONo,sono,SODate,dt,Barcode,SKUCode,Categorys,Product,Style,Typess,TypeAbrv,Shades,Units,Brand,
LOT,Size,QTY,SMRP,WSP,ConsigneeName,TransporterName,MRPDIFF,WSPDIFF,SEASON,QUALITY,
ou,Dtt,TrnsType,num,descc,amont,taxamont ,BrandID ,CID,fid,c,fd,td,Category,Descr,ITEM,iid,City,Place,HSNCode,taxamt,grossvalue,lid order by iid,ITEM asc'
exec (@Query1)

END




end





GO
