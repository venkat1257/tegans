USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PUTABLE002]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PUTABLE002]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null, 
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null, 
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null, 
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,
	@COLUMN26   nvarchar(250)=null,  @COLUMN27   nvarchar(250)=null,  @COLUMN28   nvarchar(250)=null,
	--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
	@COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null,
    	@COLUMN32   nvarchar(250)=null,  @COLUMN37   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null, 
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,
    @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null, 
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  nvarchar(100)=null,  @Direction  nvarchar(250),
	@TabelName  nvarchar(250)=null,	 @ReceiptType  nvarchar(250)=null,@Type  nvarchar(250)=null,
	@ReturnValue nvarchar(250)=null, @Qty_On_Hand decimal(18,2)=null, @Qty_On_Hand1 decimal(18,2)=null, 
	@Qty_On_Avl  decimal(18,2)=null, @newID int=null,                 @tmpnewID1 int=null
)
AS
BEGIN
begin try
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
set @COLUMN26=(select iif((@COLUMN26!='' and @COLUMN26>0 ),@COLUMN26,(select max(column02) from fitable037 where column07=1 and column08=1 )))
IF @Direction = 'Insert'
BEGIN 
   --set @COLUMN19 =(select MAX(COLUMN01) from  PUTABLE001); 
   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= '0' ) THEN (select COLUMN24 from PUTABLE001 
   where COLUMN01=@COLUMN19) else @COLUMNA02  END ) 
   set @ReceiptType =(select COLUMN29 from  PUTABLE001 Where COLUMN01=@COLUMN19 ); 
   set @COLUMN20 =(select COLUMN24 from  PUTABLE001 Where COLUMN01=@COLUMN19 ); 
   set @COLUMN21 =(select COLUMN25 from  PUTABLE001 Where COLUMN01=@COLUMN19 );  
   set @COLUMN22 =(select COLUMN26 from  PUTABLE001 Where COLUMN01=@COLUMN19 ); 
   set @COLUMN23 =(select COLUMN27 from  PUTABLE001 Where COLUMN01=@COLUMN19 ); 
   --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
   if(cast(isnull(@COLUMN24,0) as decimal(18,2))>0 and cast(isnull(@COLUMN07,0) as decimal(18,2))>0)
   begin
   set @COLUMN32 =(cast(isnull(@COLUMN24,0) as decimal(18,2))/cast(isnull(@COLUMN07,0) as decimal(18,2)) );
   end
   select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE002_SequenceNo
insert into PUTABLE002  
( 
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12, 
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23, 
   --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
   COLUMN24,  COLUMN25, COLUMN26,   COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN37,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,  
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
   COLUMND09, COLUMND10 
) 
values 
(  
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11, 
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21, 
   --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN26,  @COLUMN07,  @COLUMN29,  @COLUMN30,   @COLUMN31,
   @COLUMN32,  @COLUMN37,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08,  
   @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,  
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03,  
   @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10 
)   
if exists(select column01 from putable002 where column19=@column19) 
		begin 
 set @COLUMN15=(SELECT CAST(GETDATE() AS DATE)); 
 set @COLUMN02=(select COLUMN01 from  PUTABLE001 where COLUMN01=@COLUMN19); 
 set @COLUMN06=(select COLUMN05 from  PUTABLE001 where COLUMN01=@COLUMN19); 
 set @COLUMN04=(select COLUMN04 from  PUTABLE001 where COLUMN01=@COLUMN19); 
 if( @COLUMN04 not like 'PQ%')  
 begin   
IF (@ReceiptType !='1001') 
 begin 
 set @Type=('Sales Return Order') 
 end 
 else 
 begin 
 set @Type=('Purchase Order') 
 end  

insert into PUTABLE013 
( 
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN06,  COLUMN08,  COLUMN13,  COLUMN14,  COLUMN15,
   --COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,   COLUMN07,  COLUMN11,  COLUMN12,    COLUMN14,  COLUMN15, COLUMN16,
   --COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,   COLUMN21,  COLUMN22,  COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, 
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
   COLUMND09, COLUMND10
)
values
(  
   @COLUMN02,  @Type,  @COLUMN04,  @COLUMN06,   @COLUMN07,    @COLUMN14,    @COLUMN03,  @COLUMN15,
   --  @COLUMN03,  @COLUMN04,  @COLUMN05,   @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN15,
   --@COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10,
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07,
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05,
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
declare @Location nvarchar(250), @Lot nvarchar(250),@Project nvarchar(250)
set @Location=(select COLUMN47 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Location = (case when (cast(isnull(@COLUMN37,'') as nvarchar(250))!='' and @COLUMN37!=0) then @COLUMN37 else @Location end)
set @Location=(iif(@Location='',0,isnull(@Location,0)))
set @Project=(select COLUMN36 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project=(iif(@Project='',0,isnull(@Project,0)))
set @Lot=(iif(@COLUMN17='',0,isnull(@COLUMN17,0)))
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
set @Qty_On_Hand1= CAST(ISNULL((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26  AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND  isnull(COLUMN22,0)=@Lot),-1) as decimal(18,2))
set @Qty_On_Avl=CAST((select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot) as decimal(18,2))
--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot)
begin
--if(@Qty_On_Hand1>=0)
--begin
set @Qty_On_Hand= cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot)as decimal(18,2))+ cast(@COLUMN07 as decimal(18,2))
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
declare @TrackQty bit
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
UPDATE FITABLE010 SET COLUMN07 =@Qty_On_Hand WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot
end
--end
end
--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin 
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
insert into FITABLE010 
(
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
  COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22,COLUMN23
)
values
(
  @newID,@COLUMN03,'0','0', @COLUMN07,'0', (select column24 from PUTABLE001 where COLUMN01=@COLUMN19),@COLUMN26,@COLUMNA02,@COLUMNA03,@Location,@Lot,@Project
)
end
END
end
set @ReturnValue = 1
end
else
begin
return 0
end
END

IF @Direction = 'Select'
BEGIN
select * from PUTABLE002
END 

IF @Direction = 'Update'
BEGIN
	DECLARE @IQTY decimal(18,2)
   if  exists( SELECT 1 FROM PUTABLE002 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
   begin
   set @COLUMN19 =(select COLUMN19 from  PUTABLE002 Where COLUMN02=@COLUMN02 );
   end
   SET @IQTY=(select isnull(COLUMN07,0) from  PUTABLE002 Where COLUMN02=@COLUMN02)
   set @COLUMN20 =(select COLUMN24 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN21 =(select COLUMN25 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN22 =(select COLUMN26 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @COLUMN23 =(select COLUMN27 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   set @ReceiptType =(select COLUMN29 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
   --EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @Location=(select COLUMN47 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Location = (case when (cast(isnull(@COLUMN37,'') as nvarchar(250))!='' and @COLUMN37!=0) then @COLUMN37 else @Location end)
set @Location=(iif(@Location='',0,isnull(@Location,0)))
set @Project=(select COLUMN36 from PUTABLE001 where COLUMN01=@COLUMN19)
set @Project=(iif(@Project='',0,isnull(@Project,0)))
set @Lot=(iif(@COLUMN17='',0,isnull(@COLUMN17,0)))
   --EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
   if(cast(isnull(@COLUMN24,0) as decimal(18,2))>0 and cast(isnull(@COLUMN07,0) as decimal(18,2))>0)
   begin
   set @COLUMN32 =(cast(isnull(@COLUMN24,0) as decimal(18,2))/cast(isnull(@COLUMN07,0) as decimal(18,2)) );
   end
if not exists( SELECT 1 FROM PUTABLE002 WHERE COLUMN02=@COLUMN02 and COLUMN19=@COLUMN19)
begin
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE002_SequenceNo
   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0) THEN 
   (select COLUMN24 from PUTABLE001 where COLUMN01=@COLUMN19) else @COLUMNA02  END )
insert into PUTABLE002 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,  COLUMN12,
   COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,
   --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,  COLUMN31,  COLUMN32,  COLUMN37,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09,
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, 
   COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN26,  @COLUMN07,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN37,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, 
   @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, 
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, 
   @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
set @Qty_On_Hand1= CAST(ISNULL((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20  AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot),-1) as decimal(18,2))
set @Qty_On_Avl=CAST((select isnull(COLUMN08,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot) as decimal(18,2))
set @COLUMN04 =(select COLUMN04 from  PUTABLE001 Where COLUMN01=@COLUMN19 )
if( @COLUMN04 not like 'PQ%') 
begin
--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
--EMPHCS1692 rajasekhar reddy patakota 14/04/2016 Multi Lot Concept in Returns and purchase screens
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN03  AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot)
--if(@Qty_On_Hand1>=0)
begin
set @Qty_On_Hand= cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot)as decimal(18,2))+ cast(@COLUMN07 as decimal(18,2))
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
UPDATE FITABLE010 SET COLUMN07 =@Qty_On_Hand WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot
end
end
--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin
set @tmpnewID1=(select MAX(COLUMN02) from FITABLE010)
if(@tmpnewID1>0)
begin
set @newID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
end
else
begin
set @newID=1000
end
	--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
if(@TrackQty=1)
begin
insert into FITABLE010 
--EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
(COLUMN02,COLUMN03,COLUMN04,COLUMN05, COLUMN07, COLUMN08, COLUMN13,COLUMN19,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22,COLUMN23)
values
(@newID,@COLUMN03,'0','0', @COLUMN07,'0', (select column24 from PUTABLE001 where COLUMN01=@COLUMN19),@COLUMN26,@COLUMNA02,@COLUMNA03,@Location,@Lot,@Project  )
end
END
end
end
else 
begin
   set @COLUMNA02=( CASE WHEN (@COLUMN19!= '' and @COLUMN19 is not null and @COLUMN19!= 0 ) THEN 
   (select COLUMN24 from PUTABLE001 where COLUMN01=@COLUMN19) else @COLUMNA02  END )
UPDATE PUTABLE002 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11, 
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16, 
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,  
   --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
   COLUMN22=@COLUMN22,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,    COLUMN26=@COLUMN26,
   COLUMN27=@COLUMN26,    COLUMN28=@COLUMN07,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,    COLUMN31=@COLUMN31,    
   COLUMN32=@COLUMN32,    COLUMN37=@COLUMN37,    
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,
   COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,
   COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,
   COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05, 
   COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,
   COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,
   COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,
   COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10

   WHERE COLUMN02 = @COLUMN02
   
 --  IF(CAST(@IQTY AS DECIMAL)>=CAST(@COLUMN07 AS DECIMAL))
	--	BEGIN
	--		SET @IQTY=(CAST(@IQTY AS DECIMAL)-CAST(@COLUMN07 AS DECIMAL))
	--		SET @COLUMN07=((SELECT isnull(COLUMN07,0) FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20)-CAST(@IQTY AS DECIMAL))
	--	END
	--ELSE IF(CAST(@IQTY AS DECIMAL)<=CAST(@COLUMN07 AS DECIMAL))
	--	BEGIN
	--		SET @IQTY=(CAST(@COLUMN07 AS DECIMAL)-CAST(@IQTY AS DECIMAL))
	--		SET @COLUMN07=((SELECT isnull(COLUMN07,0) FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20)+CAST(@IQTY AS DECIMAL))
	--	END
	    --EMPHCS919 rajasekhar reddy patakota 11/8/2015 uom condition checking in Purchase
	    SET @COLUMN07=(cast((SELECT isnull(COLUMN07,0) FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot)as decimal(18,2))+CAST(@COLUMN07 AS decimal(18,2)))
			--EMPHCS1167	Service items calculation BY RAJ.Jr 22/9/2015
		set @TrackQty=(select COLUMN48 from MATABLE007 where COLUMN02=@COLUMN03)
		if(@TrackQty=1)
		begin
		UPDATE FITABLE010 SET COLUMN07 =@COLUMN07 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20 AND COLUMN19=@COLUMN26 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@Project AND isnull(COLUMN22,0)=@Lot
		end
END

 set @COLUMN15=(SELECT CAST(GETDATE() AS DATE));
 set @COLUMN02=(select COLUMN01 from  PUTABLE001 where COLUMN01=@COLUMN19);
 set @COLUMN06=(select COLUMN05 from  PUTABLE001 where COLUMN01=@COLUMN19);
 set @COLUMN04=(select COLUMN04 from  PUTABLE001 where COLUMN01=@COLUMN19);
 if( @COLUMN04 not like 'PQ%') 
 begin  
IF (@ReceiptType !='1001')
 begin
 set @Type=('Sales Return Order')
 end
 else
 begin
 set @Type=('Purchase Order')
 end
insert into PUTABLE013 
( 
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN06,  COLUMN08,  COLUMN13,  COLUMN14,  COLUMN15,
   --COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,   COLUMN07,  COLUMN11,  COLUMN12,    COLUMN14,  COLUMN15, COLUMN16, 
   --COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,   COLUMN21,  COLUMN22,  COLUMN23,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11,
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09,
   COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08,
   COLUMND09, COLUMND10
)
values
(  
   @COLUMN02,  @Type,  @COLUMN04,  @COLUMN06,   @COLUMN07,    @COLUMN14,    @COLUMN03,  @COLUMN15,
   --  @COLUMN03,  @COLUMN04,  @COLUMN05,   @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,  @COLUMN15,
   --@COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,  @COLUMN22,  @COLUMN23,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, 
   @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, 
   @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05,
   @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
end
end
else IF @Direction = 'Delete'
BEGIN
--EMPHCS867 rajasekhar reddy patakota 8/8/2015 Deleted row condition checking in Purchase Order
-- SET @IQTY=(select COLUMN07 from  PUTABLE002 Where COLUMN02=@COLUMN02 )
--   set @COLUMN19 =(select COLUMN19 from  PUTABLE002 Where COLUMN02=@COLUMN02 );
--   set @COLUMN04=(select COLUMN04 from  PUTABLE001 where COLUMN01=@COLUMN19);
--   set @COLUMN20 =(select COLUMN24 from  PUTABLE001 Where COLUMN01=@COLUMN19 );
--   SET @COLUMN07=((SELECT COLUMN07 FROM FITABLE010 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20)-@IQTY)
--   UPDATE FITABLE010 SET COLUMN07 =@COLUMN07 WHERE COLUMN03=@COLUMN03 AND COLUMN13=@COLUMN20
--update PUTABLE013 set COLUMNA13=@COLUMNA13 where COLUMN04=@COLUMN04 and COLUMN02=@COLUMN19
UPDATE PUTABLE002 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END

end try
begin catch
	declare @tempSTR nvarchar(max)	
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_Exception_PUR_TP_PUTABLE002.txt',0
if not exists(select column01 from putable002 where column19=@column19)
		begin
			delete from putable001 where column01=@column19
		end
return 0
end catch
end






GO
