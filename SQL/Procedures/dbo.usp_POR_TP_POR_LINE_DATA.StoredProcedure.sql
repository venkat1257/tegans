USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_POR_TP_POR_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_POR_TP_POR_LINE_DATA]
(
	@PurchaseOrderID nvarchar(max)=null
)

AS
BEGIN
	SELECT  
	b.COLUMN03, b.COLUMN04,b.COLUMN05,(b.COLUMN12)COLUMN07,b.COLUMN08,b.COLUMN09,cast((b.COLUMN12)*(b.COLUMN09) as decimal(18,2)) COLUMN25 ,cast((((c.COLUMN17)*((b.COLUMN12)*(b.COLUMN09))*0.01)+ (b.COLUMN12)*(b.COLUMN09)) as decimal(18,2)) COLUMN11
	,b.COLUMN25 COLUMN26,b.COLUMN13,b.COLUMN26 COLUMN27,b.COLUMN17,b.column37 COLUMN38	FROM PUTABLE001 a 
	inner join PUTABLE002 b on a.COLUMN01=b.COLUMN19 
	inner join MATABLE013 c on c.COLUMN02=b.COLUMN25 
	--EMPHCS876  rajasekhar reddy patakota 8/8/2015 After clicking on RETURN button from purchase order, system is not considerig deleted lines of that purchase order and showing all lines 
	WHERE  a.COLUMN02 in( @PurchaseOrderID) and b.COLUMNA13=0
END

GO
