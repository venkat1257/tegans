USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JO_TP_SATABLE010]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JO_TP_SATABLE010]

(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,  
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22 nvarchar(250)=null,
	
	@COLUMN23 nvarchar(250)=null,    @COLUMN24   nvarchar(250)=null,  @COLUMN25   nvarchar(250)=null,  
	@COLUMN26 nvarchar(250)=null,    @COLUMN27 nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,   
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250), @Soid nvarchar(250)=null,      
	@ReturnValue nvarchar(250)=null ,@Result nvarchar(250)=null,@Status nvarchar(250)=null
)

AS
BEGIN
begin try

IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
--set @COLUMN15 = (select MAX(COLUMN01) from  SATABLE009);
set @COLUMN16=(select COLUMN14 from SATABLE009 where COLUMN01=@COLUMN15)
set @COLUMN17=(select COLUMN15 from SATABLE009 where COLUMN01=@COLUMN15)
set @COLUMN18=(select COLUMN16 from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
set @COLUMN12=(cast(@COLUMN08 as decimal(18,2))-cast(@COLUMN09 as decimal(18,2)))
set @Soid=(select COLUMN06 from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.SATABLE010_SequenceNo
insert into SATABLE010 
(
   COLUMN02,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16, COLUMN17,  COLUMN18,   COLUMN19,  COLUMN20,  COLUMN21,   COLUMN22,  COLUMN23,
   COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, 
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, 
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08, (CAST(@COLUMN09 AS decimal(18,2))+ CAST(@COLUMN10 AS decimal(18,2))), 
   @COLUMN10,  @COLUMN11,  @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18, @COLUMN19, 
   @COLUMN20,   @COLUMN21,   @COLUMN22,  @COLUMN23, @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,@COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
)  
if exists(select column01 from SATABLE010 where column15=@COLUMN15)
					begin
declare @PID int
declare @SPID int
declare @BillQty decimal(18,2)
declare @PBillQty decimal(18,2)
declare @IQty decimal(18,2)
declare @IRQty decimal(18,2)
declare @PBQty decimal(18,2)
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
declare @Vid nvarchar(250)=null
declare @number int
declare @number1 int
declare @MEMO nvarchar(250)=null
 DECLARE @Tax int
 DECLARE @headerTax int
 DECLARE @TaxAG int 
 DECLARE @headerTaxAG int 
 declare @TAXRATE DECIMAL(18,2) 
 declare @headerTAXRATE DECIMAL(18,2) 
 declare @lineiid nvarchar(250)=null
 declare @DT date
declare @PRICE DECIMAL(18,2)
declare @CUST int

--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
 set @number1=(select max(column01) from SATABLE009 where COLUMN01=@COLUMN15 )
 set @number = (select MAX(COLUMN01) from  SATABLE010 where COLUMN02=@COLUMN02);
 set @Tax= (select COLUMN21 from SATABLE010 where COLUMN01=@number)
 set @headerTax= (select COLUMN23 from SATABLE009 where COLUMN01=@number1)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 set @headerTaxAG= (select column16 from MATABLE013 where COLUMN02=@headerTax)
 set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
 set @headerTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@headerTax)
 set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
 --EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
 set @number=(select max(column01) from SATABLE010 where COLUMN02=@COLUMN02 )
--set @chk=(select column48 from matable007 where column02=@COLUMN05)
set @PRICE=(select column17 from matable007 where column02=@COLUMN05)
set @DT=(select column08 from SATABLE009 where column01=@number1)
set @CUST=(select column05 from SATABLE009 where column01=@number1)
set @MEMO=(select column12 from SATABLE009 where column01=@number1)

SET @PID=(SELECT COLUMN06 FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
SET @SPID=(SELECT datalength(COLUMN06) FROM SATABLE009 WHERE COLUMN01=@COLUMN15)
SET @PBillQty=isnull((SELECT (COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) and COLUMN03=@COLUMN05),0)
UPDATE SATABLE006 SET   COLUMN13=((@PBillQty)+ CAST(@COLUMN10 AS decimal(18,2))) where COLUMN03=@COLUMN05 and COLUMN19=
(select COLUMN01 from SATABLE005 where COLUMN02= (@PID))


SET @BillQty=(SELECT sum(COLUMN13) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID))
SET @IRQty=(SELECT sum(COLUMN12) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID))
SET @IQty=(SELECT sum(COLUMN07) FROM SATABLE006 WHERE COLUMN19 in (select COLUMN01 from SATABLE005 where COLUMN02=@PID) )

if(@SPID=0)
	begin
		declare @Qty_On_Hand decimal(18,2)
		declare @Qty_Commit decimal(18,2)
		declare @Qty_Order decimal(18,2)
		declare @Qty_AVL decimal(18,2)
		declare @Qty_BACK decimal(18,2)
		declare @Qty decimal(18,2)

		begin
			set @Qty_BACK =0
			set @Qty_Commit=(select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN05);
			set @Qty_On_Hand=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN05);
			set @Qty=cast(@COLUMN10 as decimal(18,2));
			set @Qty_On_Hand=( @Qty_On_Hand - @Qty);
			set @Qty_AVL=(@Qty_On_Hand)
		if(@Qty_AVL<0)
			begin
				set @Qty_AVL=0
				set @Qty_BACK =@Qty_Commit
				set @Qty_Commit=0
			end
			UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand, COLUMN05 =@Qty_Commit, COLUMN07 =@Qty_BACK, COLUMN08= @Qty_AVL ,COLUMN12=(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05  AND COLUMN13=@COLUMN16)as decimal(18,2))-(cast(@COLUMN10 as decimal(18,2))*cast(@COLUMN13 as decimal(18,2)))) WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16
			UPDATE FITABLE010 SET COLUMN17=round(cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16)as decimal(18,2))/@Qty_On_Hand,1) WHERE COLUMN03=@COLUMN05 AND COLUMN13=@COLUMN16
		end

	end

if(@PID!='')
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=57)
UPDATE SATABLE005 set COLUMN16=@Result where COLUMN02=((select COLUMN06 from SATABLE009 where COLUMN01= @COLUMN15))
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=51)
UPDATE SATABLE009 set COLUMN13=(@Result) where COLUMN01=@COLUMN15
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=51)
UPDATE SATABLE009 set COLUMN13=@Result where COLUMN01=@COLUMN15
end

UPDATE PUTABLE013 SET COLUMN10=(CAST(@COLUMN10 AS decimal(18,2))+ CAST(@PBillQty AS decimal(18,2)))   WHERE COLUMN14=@COLUMN04 and COLUMN02 = (select COLUMN01 from SATABLE005 where COLUMN02= (select COLUMN05 from SATABLE009 where COLUMN01= @COLUMN15))


--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
--declare @CUST int
Declare @CustID int
--Declare @memo nvarchar(250)
Declare @Num nvarchar(250)
Declare @dueDT date
declare @newID int
declare @tmpnewID int 
declare @Totamt decimal(18,2)
declare @JBamt decimal(18,2) 
--declare @MEMO nvarchar(250)=null
set @CustID = (select COLUMN05 from SATABLE009 where COLUMN01=@COLUMN15)
set @JBamt = (select COLUMN20 from SATABLE009 where COLUMN01=@COLUMN15)
set @memo = (select COLUMN11 from SATABLE009 where COLUMN01=@COLUMN15)
set @Num = (select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15)
set @dueDT = (select COLUMN10  from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
		--if not exists(select COLUMN05  from PUTABLE016 where COLUMN09=@Num AND COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03)
		--BEGIN
		--set @tmpnewID=(select MAX(COLUMN02) from PUTABLE016)
		--if(@tmpnewID>0)
		--begin
		--set @newID=cast((select MAX(COLUMN02) from PUTABLE016) as int)+1
		--end
		--else
		--begin
		--set @newID=100000
		--end
		--insert into PUTABLE016	(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14, COLUMNA02, COLUMNA03)
		--			values(@newID,2000,GETDATE(),@COLUMN15,'JOBER BILL',@CustID,@Num,@Result,@dueDT,@JBamt,null,@JBamt,@COLUMN16,@COLUMNA03) 
		--END
		--else
		--begin
		--set @Totamt=(select COLUMN12 from PUTABLE016  where COLUMN05=@COLUMN15 )
		--update PUTABLE016 set COLUMN04=GETDATE(),COLUMN12=(cast(@Totamt as int)+cast(@JBamt as int)),COLUMN14=(cast(@Totamt as int)+cast(@JBamt as int)) where COLUMN09=@Num
		--end


		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
		BEGIN
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@DT,'JOBBER BILL',@number,@CUST,@MEMO,@Tax,@TaxAG,(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@TAXRATE),@COLUMN05,CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)),@TAXRATE,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15))
		END
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0)
		BEGIN
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
			Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMNA02, COLUMNA03,COLUMN09)
			values(@Vid,@DT,'JBOBBER BILL',@number,@CUST,@MEMO,@headerTax,@headerTaxAG,(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@headerTAXRATE),@COLUMN05,CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)),@headerTAXRATE,@COLUMNA02, @COLUMNA03,(select COLUMN04 from SATABLE009 where COLUMN01=@COLUMN15))
		END

		DECLARE @ACID int
		DECLARE @AID int
			DECLARE @ACTYPE int
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'JBOBBER BILL',@DT,@number,@CUST,@COLUMN14,NULL,@Num,@ACID,@COLUMNA02 , @COLUMNA03,0)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
			set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03,  COLUMN13)
			values(@AID,@DT,'JBOBBER BILL',@number,@CUST,@MEMO,@ACID,CAST(isnull(@COLUMN14,0) AS decimal(18,2)),CAST(isnull(@COLUMN14,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@COLUMN05)
		    END

set @ReturnValue = 1
end
else
begin
return 0
end
END
 

IF @Direction = 'Select'
BEGIN
select * from SATABLE010
END 
 

IF @Direction = 'Update'
BEGIN
set @COLUMN16=(select COLUMN15 from SATABLE009 where COLUMN01=@COLUMN15)
set @COLUMN17=(select COLUMN16 from SATABLE009 where COLUMN01=@COLUMN15)
set @COLUMN18=(select COLUMN17 from SATABLE009 where COLUMN01=@COLUMN15)
set @COLUMN19=(select COLUMN18 from SATABLE009 where COLUMN01=@COLUMN15)
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
set @COLUMN12=(cast(isnull(@COLUMN08,0) as decimal(18,2))-cast(isnull(@COLUMN09,0) as decimal(18,2)))
set @COLUMN09=(select Max(isnull(column09,0)) from satable010 where COLUMN15=@COLUMN15)
SET @COLUMN09=(cast(@column09 as decimal(18,2))-cast((select COLUMN10 from SATABLE010 where COLUMN02=@COLUMN02) as decimal(18,2)))
	 
UPDATE SATABLE010 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,  
   --EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
   COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=(CAST(@COLUMN09 AS decimal(18,2))+ CAST(@COLUMN10 AS decimal(18,2))),     COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
   COLUMN20=@COLUMN20,    COLUMN21=@COLUMN21,    COLUMN22=@COLUMN22,COLUMN23=@COLUMN23, 
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  
    COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  
   COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  
   COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07, 
   COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  
   COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05, 
   COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   
--EMPHCS1052 rajasekhar reddy patakota 29/08/2015 Bill to Jobber Functionality Changes
 set @Tax= (select COLUMN21 from SATABLE010 where COLUMN01=@number)
 set @headerTax= (select COLUMN23 from SATABLE009 where COLUMN01=@COLUMN15)
 set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
 set @headerTaxAG= (select column16 from MATABLE013 where COLUMN02=@headerTax)
 set @TAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@Tax)
 set @headerTAXRATE= (select COLUMN07 from MATABLE013 where COLUMN02=@headerTax)
	--SET @TAXRATE=(SELECT COLUMN16 FROM FITABLE026 WHERE COLUMN05=@number AND COLUMN04='Invoice' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03)
	if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0)
		BEGIN
		--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
		--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
		UPDATE FITABLE026 SET COLUMN08=@Tax,COLUMN10=@TaxAG,COLUMN13=@COLUMN04,COLUMN14=CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)),COLUMN16=@TAXRATE,COLUMN11=(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@TAXRATE) WHERE COLUMN05=@number AND COLUMN04='JOBBER BILL' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03  and COLUMN10=@TaxAG and COLUMN08=@Tax
		END
		if(isnull(cast(@COLUMN14 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0)
		BEGIN
		--EMPHCS729 Tax Amount Calculation In Transaction Forms for decimals by srinivas 7/21/2015
		--EMPHCS897 rajasekhar reddy patakota 10/8/2015 tax transaction table is calculating with out considering Discount amount. Tax should get calcuate on (amount - Discount).
		UPDATE FITABLE026 SET  COLUMN08=@headerTax,COLUMN10=@headerTaxAG,COLUMN13=@COLUMN04,COLUMN14=CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2)),COLUMN16=@headerTAXRATE,COLUMN11=(CAST((CAST(isnull(@COLUMN14,0) AS decimal(18,2))-CAST(isnull(@COLUMN19,0) AS decimal(18,2)))AS decimal(18,2))/100)*(@headerTAXRATE) WHERE COLUMN05=@number AND COLUMN04='JOBBER BILL' AND  COLUMNA02=@COLUMNA02 AND COLUMNA03= @COLUMNA03  and COLUMN10=@headerTaxAG and COLUMN08=@headerTax
		END
		set @number1=(select max(column01) from SATABLE009 where COLUMN01=@COLUMN15 )
		set @number = (select MAX(COLUMN01) from  SATABLE010 where COLUMN02=@COLUMN02);
		set @DT=(select column08 from SATABLE009 where column01=@number1)
		set @CUST=(select column05 from SATABLE009 where column01=@number1)
		set @MEMO=(select column12 from SATABLE009 where column01=@number1)
		
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN05 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			values(@AID,'JBOBBER BILL',@DT,@number,@CUST,@COLUMN14,NULL,@Num,@ACID,@COLUMNA02 , @COLUMNA03,0)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
			set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03,  COLUMN13)
			values(@AID,@DT,'JBOBBER BILL',@number,@CUST,@MEMO,@ACID,CAST(isnull(@COLUMN14,0) AS decimal(18,2)),CAST(isnull(@COLUMN14,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@COLUMN05)
		    END
END
 

else IF @Direction = 'Delete'
BEGIN
UPDATE SATABLE009 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END

end try
begin catch
if not exists(select column01 from SATABLE010 where column15=@COLUMN15)
					begin
					delete SATABLE009 where column01=@COLUMN15
					return 0
					end
					
DECLARE @tempSTR NVARCHAR(MAX)
 set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(250)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(250))))
    exec [CheckDirectory] @tempSTR,'Exception_JO_SATABLE010.txt',0

return 0
end catch
end













GO
