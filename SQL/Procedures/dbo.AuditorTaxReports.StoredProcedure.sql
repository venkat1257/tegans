USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[AuditorTaxReports]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[AuditorTaxReports]
(@ReportName nvarchar(250)=null,@FromDate nvarchar(250)=null,@ToDate nvarchar(250)=null,@OperatingUnit nvarchar(250)=null,
 @OPUnit nvarchar(250)=null,@Type nvarchar(250)=null,@Taxes nvarchar(250)=null,@AcOwner nvarchar(250)=null,
 @whereStr nvarchar(max)=null, @Query1 nvarchar(max)=null,
@Brand nvarchar(250)= null,@DateF nvarchar(250)=null
)
as 
begin
if (LEN(ISNULL(@Taxes,''))>0)
begin
 set @whereStr= ' where taxid in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Taxes+''') s)'
end
--if @FromDate!='' and @ToDate!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where cast(datec as date)  between '''+@FromDate+''' and '''+@ToDate+''''
--end
--else
--begin
--set @whereStr=@whereStr+' and cast(datec as date)   between '''+@FromDate+''' and '''+@ToDate+''''
--end
--end

if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end

if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where ou in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and ou in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if (LEN(ISNULL(@Type,''))>0)
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Type='''+@Type+''''
end
else
begin
set @whereStr=@whereStr+' and Type='''+@Type+''''
end
end
--else if @Type=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where (Type=''Sales'' or Type=''Purchase'')'
--end
--else
--begin
--set @whereStr=@whereStr+' and (Type=''Sales'' or Type=''Purchase'')'
--end
--end
else if (@ReportName!='TDSRegister' and @ReportName!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Report='''+@ReportName+''''
end
else
begin
set @whereStr=@whereStr+' and Report='''+@ReportName+''''
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1  order by rno desc'
end
else
begin
 set @whereStr=@whereStr+' order by rno desc'
end

if(@ReportName='TDSRegister')
begin
select * into #TDSRegistercommon from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT  'Sales' Type,s.column02 col2,s.column01 col1,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,isnull(s.COLUMN22,0) subtotal,l.COLUMN04 tdssec,td.COLUMN07 tdsamt,row_number() over(order by s.COLUMN08) rno ,s.COLUMN03 fid  from 
FITABLE034 td inner join SATABLE009 s on td.column09=s.column04 and isnull(td.COLUMNA13,0)=0 and td.columnA02=s.columnA02 and td.columnA03=s.columnA03 and td.column03='TDS Receivable'
left join MATABLE002 l on l.column02=s.column37 and isnull(l.COLUMNA13,0)=0 left join SATABLE002 c on c.column02=s.column05 and c.columnA03=s.columnA03
--left join MATABLE013 t on t.column02=l.column21
 where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner and  isnull(td.COLUMN07,0)>0 and
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT  'Purchase' Type,s.column02 col2,s.column01 col1,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN22 subtotal,l.COLUMN04 tdssec,td.COLUMN11 tdsamt,row_number() over(order by s.COLUMN08) rno ,s.COLUMN03 fid from 
FITABLE026 td inner join PUTABLE005 s on td.column05=s.column01 and isnull(td.COLUMNA13,0)=0 and td.column09=s.column04 and td.columnA02=s.columnA02 and td.columnA03=s.columnA03 and td.column04='TDS Payable'
left join MATABLE002 l on l.column02=s.column27 and isnull(l.COLUMNA13,0)=0 left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03
--left join MATABLE013 t on t.column02=l.column18 and t.COLUMN16='22401'
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and isnull(td.COLUMN11,0)>0 and
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate ) Query
set @Query1='select * from #TDSRegistercommon'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='SalesTaxRegister')
begin
exec(';With MyTable AS
 (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(iif((isnull(l.column21,0)=0 or l.column21=1000),iif(i.COLUMN05=''ITTY009'',''22401'',0),t.COLUMN16)!=''22401'',''SalesTaxRegister'',''SalesSevicesTaxRegister'') Report,
''Sales'' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column21,0)=0,1000,l.column21)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,'''+@DateF+''') datec,c.COLUMN05 payee,c.COLUMN42 AS GSTIN,
l.COLUMN14 subtotal,0 fsubtotal,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,''Exempted'',t.COLUMN04) tax,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,
l.COLUMN14,cast(((isnull(l.column10,0)*isnull(l.column35,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt, ''''  BrandIDN,0 BrandID,
iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,l.COLUMN14,0) examt,0 fexamt,f.COLUMN04 form,row_number() over(order by s.COLUMN08) rno  ,s.COLUMN03 fid,
Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt,ISNULL(s.COLUMN71,0) ShippingAmt,(ISNULL(s.COLUMN22,0) + ISNULL(s.COLUMN24,0) + ISNULL(s.COLUMN72,0)- ISNULL(s.COLUMN25,0)) STAmt,ISNULL(s.COLUMN56,0) Discount,ISNULL(s.COLUMN68,0) Roundoff,
ISNULL(s.COLUMN20,0) INVTOT,ISNULL(s.COLUMN71,0) ShippingAmt1,ISNULL(s.COLUMN56,0) Discount1,ISNULL(s.COLUMN68,0) Roundoff1,0 Stax,0 Ptax,0 Otax from 
SATABLE009 s inner join SATABLE010 l on l.COLUMNA03='''+@AcOwner+''' and l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.COLUMNA03='''+@AcOwner+''' and c.column02=s.column05 and c.columnA03=s.columnA03 and isnull(c.columnA13,0)=0
inner join MATABLE007 i on i.COLUMNA03='''+@AcOwner+''' and i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull(i.columnA13,0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull(m5.columnA13,0)=0
left join MATABLE002 f on f.COLUMNA03='''+@AcOwner+''' and f.column02=s.COLUMN31 and s.COLUMNA03=f.columnA03 and isnull((f.COLUMNA13),0)=0
left join MATABLE013 t on t.COLUMNA03='''+@AcOwner+''' and ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02 = (replace(l.column21,''-'','''')  ) and (COLUMNA03=l.columnA03) and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column21,0)=0,1000,l.column21))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03='''+@AcOwner+''' and s.COLUMN03 in(1277,1532) and
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and s.column08 between '''+@FromDate+''' and'''+ @ToDate+'''
 UNION ALL
 SELECT ''SalesTaxRegister'' Report,
''Sales'' Type,s.column02 col2,s.column01 col1,(iif(isnull(s.column73,0)=0,1000,s.column73)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,'''+@DateF+''') datec,c.COLUMN05 payee,c.COLUMN42 AS GSTIN,
0 subtotal,0 fsubtotal,iif((iif(isnull(s.column73,0)=0,1000,s.column73))=1000,''Exempted'',t.COLUMN04) tax,
iif((iif(isnull(s.column73,0)=0,1000,s.column73))=1000,
0,cast(((isnull(s.column35,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt, '''' BrandIDN,0 BrandID,
0 examt,0 fexamt,f.COLUMN04 form,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid,
Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt,ISNULL(s.COLUMN71,0) ShippingAmt,(ISNULL(s.COLUMN22,0) + ISNULL(s.COLUMN24,0) + ISNULL(s.COLUMN72,0)- ISNULL(s.COLUMN25,0)) STAmt,ISNULL(s.COLUMN56,0) Discount,ISNULL(s.COLUMN68,0) Roundoff,ISNULL(s.COLUMN20,0) INVTOT,ISNULL(s.COLUMN71,0) ShippingAmt1,ISNULL(s.COLUMN56,0) Discount1,ISNULL(s.COLUMN68,0) Roundoff1,0 Stax,1 Ptax,0 Otax from 
SATABLE009 s 
--inner join SATABLE010 l on l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.COLUMNA03='''+@AcOwner+''' and c.column02=s.column05 and c.columnA03=s.columnA03 and isnull(c.columnA13,0)=0
--inner join MATABLE007 i on i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull(i.columnA13,0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull(m5.columnA13,0)=0
left join MATABLE002 f on f.COLUMNA03='''+@AcOwner+''' and f.column02=s.COLUMN31 and s.COLUMNA03=f.columnA03 and isnull((f.COLUMNA13),0)=0
left join MATABLE013 t on t.COLUMNA03='''+@AcOwner+''' and ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02 = (replace(s.COLUMN73,''-'','''')  ) and (COLUMNA03=s.columnA03) and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.COLUMN73,0)=0,1000,s.COLUMN73))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03='''+@AcOwner+''' and s.COLUMN03 in(1277,1532) and
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and s.column08 between '''+@FromDate+''' and'''+ @ToDate+'''
 UNION ALL
 SELECT ''SalesTaxRegister'' Report,
''Sales'' Type,s.column02 col2,s.column01 col1,(iif(isnull(s.column69,0)=0,1000,s.column69)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,'''+@DateF+''') datec,c.COLUMN05 payee,c.COLUMN42 AS GSTIN,
0 subtotal,0 fsubtotal,iif((iif(isnull(s.column69,0)=0,1000,s.column69))=1000,''Exempted'',t.COLUMN04) tax,
iif((iif(isnull(s.column69,0)=0,1000,s.column69))=1000,
0,cast(((isnull(s.column36,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt, '''' BrandIDN,0 BrandID,
0 examt,0 fexamt,f.COLUMN04 form,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid,
Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt,ISNULL(s.COLUMN71,0) ShippingAmt,(ISNULL(s.COLUMN22,0) + ISNULL(s.COLUMN24,0) + ISNULL(s.COLUMN72,0)- ISNULL(s.COLUMN25,0)) STAmt,ISNULL(s.COLUMN56,0) Discount,ISNULL(s.COLUMN68,0) Roundoff,ISNULL(s.COLUMN20,0) INVTOT,ISNULL(s.COLUMN71,0) ShippingAmt1,ISNULL(s.COLUMN56,0) Discount1,ISNULL(s.COLUMN68,0) Roundoff1,1 Stax,0 Ptax,0 Otax from 
SATABLE009 s 
--inner join SATABLE010 l on l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.COLUMNA03='''+@AcOwner+''' and c.column02=s.column05 and c.columnA03=s.columnA03 and isnull(c.columnA13,0)=0
--inner join MATABLE007 i on i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull(i.columnA13,0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull(m5.columnA13,0)=0
left join MATABLE002 f on f.COLUMNA03='''+@AcOwner+''' and f.column02=s.COLUMN31 and s.COLUMNA03=f.columnA03 and isnull((f.COLUMNA13),0)=0
left join MATABLE013 t on t.COLUMNA03='''+@AcOwner+''' and ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02 = (replace(s.column69,''-'','''')  ) and (COLUMNA03=s.columnA03) and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.column69,0)=0,1000,s.column69))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03='''+@AcOwner+''' and s.COLUMN03 in(1277,1532) and
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and s.column08 between '''+@FromDate+''' and'''+ @ToDate+'''
 UNION ALL
 SELECT ''SalesTaxRegister'' Report,
''Sales'' Type,s.column02 col2,s.column01 col1,(iif(isnull(s.column70,0)=0,1000,s.column70)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,'''+@DateF+''') datec,c.COLUMN05 payee,c.COLUMN42 AS GSTIN,
0 subtotal,0 fsubtotal,iif((iif(isnull(s.column70,0)=0,1000,s.column70))=1000,''Exempted'',t.COLUMN04) tax,
iif((iif(isnull(s.column70,0)=0,1000,s.column70))=1000,
0,cast(((isnull(s.column44,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt, '''' BrandIDN,0 BrandID,
0 examt,0 fexamt,f.COLUMN04 form,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid,
Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt,ISNULL(s.COLUMN71,0) ShippingAmt,(ISNULL(s.COLUMN22,0) + ISNULL(s.COLUMN24,0) + ISNULL(s.COLUMN72,0)- ISNULL(s.COLUMN25,0)) STAmt,ISNULL(s.COLUMN56,0) Discount,ISNULL(s.COLUMN68,0) Roundoff,ISNULL(s.COLUMN20,0) INVTOT,ISNULL(s.COLUMN71,0) ShippingAmt1,ISNULL(s.COLUMN56,0) Discount1,ISNULL(s.COLUMN68,0) Roundoff1,0 Stax,0 Ptax,1 Otax from 
SATABLE009 s 
--inner join SATABLE010 l on l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.COLUMNA03='''+@AcOwner+''' and c.column02=s.column05 and c.columnA03=s.columnA03 and isnull(c.columnA13,0)=0
--inner join MATABLE007 i on i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull(i.columnA13,0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull(m5.columnA13,0)=0
left join MATABLE002 f on f.COLUMNA03='''+@AcOwner+''' and f.column02=s.COLUMN31 and s.COLUMNA03=f.columnA03 and isnull((f.COLUMNA13),0)=0
left join MATABLE013 t on t.COLUMNA03='''+@AcOwner+''' and ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02 = (replace(s.column70,''-'','''')  ) and (COLUMNA03=s.columnA03) and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.column70,0)=0,1000,s.column70))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03='''+@AcOwner+''' and s.COLUMN03 in(1277,1532) and
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and s.column08 between '''+@FromDate+''' and'''+ @ToDate+'''
--  UNION ALL
-- SELECT ''SalesTaxRegister'' Report,
--''Sales'' Type,s.column02 col2,s.column01 col1,0 taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,'''+@DateF+''') datec,c.COLUMN05 payee,c.COLUMN42 AS GSTIN,
--0 subtotal,0 fsubtotal,'''' tax,
--0 taxamt,0 ftaxamt, '''' BrandIDN,0 BrandID,
--0 examt,0 fexamt,f.COLUMN04 form,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid,
--Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt,0 ShippingAmt,(ISNULL(s.COLUMN22,0) + ISNULL(s.COLUMN24,0) + ISNULL(s.COLUMN72,0)- ISNULL(s.COLUMN25,0)) STAmt,ISNULL(s.COLUMN56,0) Discount,ISNULL(s.COLUMN68,0) Roundoff,ISNULL(s.COLUMN20,0) INVTOT from 
--SATABLE009 s 
----inner join SATABLE010 l on l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
--left join SATABLE002 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull(c.columnA13,0)=0
----inner join MATABLE007 i on i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull(i.columnA13,0)=0
----left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull(m5.columnA13,0)=0
--left join MATABLE002 f on f.column02=s.COLUMN31 and s.COLUMNA03=f.columnA03 and isnull((f.COLUMNA13),0)=0
--left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable('','',(select COLUMN05  from MATABLE014 where COLUMN02 = (replace(s.column70,''-'','''')  ) and (COLUMNA03=s.columnA03) and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.column70,0)=0,1000,s.column70))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
--where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03='''+@AcOwner+''' and s.COLUMN03 in(1277,1532) and
-- s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and s.column08 between '''+@FromDate+''' and'''+ @ToDate+'''
  )
SELECT Report,Type,col2,col1,taxid,ou,transno,datec,payee,GSTIN,CAST((subtotal)/iif(rowcnt=0,1,rowcnt) as decimal(18,2))subtotal,0 fsubtotal,tax,taxamt,0 ftaxamt,BrandIDN,BrandID,examt,0 fexamt,form,rno,fid,rowcnt,(Count(*) over (partition by col1 ORDER BY col1))scnt,CAST((ShippingAmt)/iif((Count(*) over (partition by col1 ORDER BY col1))=0,1,(Count(*) over (partition by col1 ORDER BY col1))) as decimal(18,9))ShippingAmt,STAmt,CAST((Discount)/iif((Count(*) over (partition by col1 ORDER BY col1))=0,1,(Count(*) over (partition by col1 ORDER BY col1))) as decimal(18,9)) Discount,CAST((Roundoff)/iif((Count(*) over (partition by col1 ORDER BY col1))=0,1,(Count(*) over (partition by col1 ORDER BY col1))) as decimal(18,9)) Roundoff,INVTOT,ShippingAmt1,Discount1,Roundoff1,Stax,Ptax,Otax from MyTable '+@whereStr+'')
--exec (@Query1) 
end

ELSE if(@ReportName='SalesServicesTaxRegister')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,CAST((Q.subtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))subtotal,0 fsubtotal,Q.tax,Q.taxamt,0 ftaxamt,Q.examt,0 fexamt,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt into #SalesSevicesTaxRegister from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(iif((isnull(l.column21,0)=0 or l.column21=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','SalesTaxRegister','SalesServicesTaxRegister') Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column21,0)=0,1000,l.column21)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,l.COLUMN14 subtotal,0 fsubtotal,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,l.COLUMN14,cast(((isnull(l.column10,0)*isnull(l.column35,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,l.COLUMN14,0) examt,0 fexamt,row_number() over(order by s.COLUMN08) rno ,s.COLUMN03 fid 
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt from 
SATABLE009 s inner join SATABLE010 l on l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
inner join MATABLE007 i on i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column21,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column21,0)=0,1000,l.column21))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner and 
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT 'SalesServicesTaxRegister' Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column21,0)=0,1000,l.column21)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,0 subtotal,0 fsubtotal,cs.COLUMN17 tax,cs.COLUMN11 taxamt,0 ftaxamt,0 examt,0 fexamt,row_number() over(order by s.COLUMN08) rno ,s.COLUMN03 fid
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt  from 
SATABLE009 s inner join SATABLE010 l on l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull(l.column21,1000)!=1000 and isnull(l.COLUMNA13,0)=0 
left join SATABLE002 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
 inner join FITABLE026 cs on cs.column09=s.column04 and cs.column05=l.column01 and cs.column04='INVOICE' and cs.column08 in(1121,1130) and cs.columnA03=s.columnA03 and isnull((cs.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column21,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column21,0)=0,1000,l.column21))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
inner join MATABLE007 i on i.column02=l.column05  and iif((isnull(l.column21,0)=0 or l.column21=1000),iif(i.COLUMN05='ITTY009','009',0),iif(t.COLUMN16='22401','009',0))='009' and isnull((i.COLUMNA13),0)=0
left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
 where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner and 
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(isnull(t.COLUMN16,0)!='22401','SalesTaxRegister','SalesServicesTaxRegister') Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column15,0)=0,1000,l.column15)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column05,@DateF) datec,(case when s.COLUMN20=22335 then c.COLUMN05 when s.COLUMN20=22492 then e.COLUMN05 else v.COLUMN05 end) payee,l.COLUMN05 subtotal,0 fsubtotal,iif((iif(isnull(l.column15,0)=0,1000,l.column15))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column15,0)=0,1000,l.column15))=1000,l.COLUMN05,cast(((isnull(l.column05,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt,iif((iif(isnull(l.column15,0)=0,1000,l.column15))=1000,l.COLUMN05,0) examt,0 fexamt,row_number() over(order by s.COLUMN05) rno ,1526 fid 
,'' BrandIDN,'' BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN05 as datetime)) rowcnt from 
FITABLE023 s inner join FITABLE024 l on l.column09=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.column02=s.column08 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
left join SATABLE001 v on v.column02=s.column08 and v.columnA03=s.columnA03 and isnull((v.COLUMNA13),0)=0
left join MATABLE010 e on e.column02=s.column08 and e.columnA03=s.columnA03 and isnull((e.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column15,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column15,0)=0,1000,l.column15))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1526  and s.COLUMNA03=@AcOwner and 
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column05 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(isnull(t.COLUMN16,0)!='22401','SalesTaxRegister','SalesServicesTaxRegister') Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column15,0)=0,1000,l.column15)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column05,@DateF) datec,(case when s.COLUMN20=22335 then c.COLUMN05 when s.COLUMN20=22492 then e.COLUMN05 else v.COLUMN05 end) payee,0 subtotal,0 fsubtotal,'Cess' tax,cs.COLUMN11 taxamt,0 ftaxamt,0 examt,0 fexamt,row_number() over(order by s.COLUMN05) rno ,1526 fid  
,'' BrandIDN,'' BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN05 as datetime)) rowcnt from 
FITABLE023 s inner join FITABLE024 l on l.column09=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
inner join FITABLE026 cs on cs.column09=s.column04 and cs.column05=s.column01 and cs.column04='RECEIPT VOUCHER' and cs.column08=1121 and cs.columnA03=s.columnA03 and isnull((cs.COLUMNA13),0)=0 
left join SATABLE002 c on c.column02=s.column08 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0 
left join SATABLE001 v on v.column02=s.column08 and v.columnA03=s.columnA03 and isnull((v.COLUMNA13),0)=0 
left join MATABLE010 e on e.column02=s.column08 and e.columnA03=s.columnA03 and isnull((e.COLUMNA13),0)=0 
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column15,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column15,0)=0,1000,l.column15))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0 and isnull(t.COLUMN16,0)='22401'
where  isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1526 and s.COLUMNA03=@AcOwner and isnull(t.COLUMN16,0)='22401' and
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column05 between @FromDate and @ToDate) Q
set @Query1='select * from #SalesSevicesTaxRegister'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='SalesReturnTaxRegister')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,CAST((Q.subtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))subtotal,0 fsubtotal,Q.tax,Q.taxamt,0 ftaxamt,Q.examt,0 fexamt,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt into #SalesReturnTaxRegister from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(iif((isnull(l.column26,0)=0 or l.column26=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','SalesReturnTaxRegister','SalesReturnServicesTaxRegister') Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column26,0)=0,1000,l.column26)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column06,@DateF) datec,(case when s.COLUMN51='22305' then c1.COLUMN05 else c.COLUMN05 end) payee,l.COLUMN25 subtotal,0 fsubtotal,iif((iif(isnull(l.column26,0)=0,1000,l.column26))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column26,0)=0,1000,l.column26))=1000,l.COLUMN25,cast(((isnull(l.column07,0)*isnull(l.column32,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt,iif((iif(isnull(l.column26,0)=0,1000,l.column26))=1000,l.COLUMN25,0) examt,0 fexamt,row_number() over(order by s.COLUMN06) rno ,s.COLUMN03 fid
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN06 as datetime)) rowcnt  from 
SATABLE005 s inner join SATABLE006 l on l.column19=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
left join SATABLE001 c1 on c1.column02=s.column05 and c1.columnA03=s.columnA03 and isnull((c1.COLUMNA13),0)=0
inner join MATABLE007 i on i.column02=l.column03 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column26,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column26,0)=0,1000,l.column26))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and (s.COLUMN03=1330 or s.COLUMN03=1603)  and s.COLUMNA03=@AcOwner and 
 s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column06 between @FromDate and @ToDate
--union all
--SELECT 'SalesReturnTaxRegister' Report,'Sales' Type,l.column02 col2,(iif(isnull(l.column26,0)=0,1000,l.column26)) taxid,s.columna02 ou,s.column04 transno,s.column06 datec,c.COLUMN05 payee,0 subtotal,0 fsubtotal,'Cess' tax,-(cs.COLUMN12) taxamt,0 ftaxamt,0 examt,0 fexamt from 
--SATABLE005 s inner join SATABLE006 l on l.column19=s.column01 and (iif(isnull(l.column26,0)=0,1000,l.column26))!=1000 and isnull(l.COLUMNA13,0)=0 left join SATABLE002 c on c.column02=s.column05 and c.columnA03=s.columnA03
-- inner join FITABLE026 cs on cs.column09=s.column04 and cs.column05=s.column01 and cs.column04='Credit Memo' and cs.column08=1121 and c.columnA03=s.columnA03
--left join MATABLE013 t on t.column02=(iif(isnull(l.column26,0)=0,1000,l.column26)) and isnull(t.columna13,0)=0
-- inner join MATABLE007 i on i.column02=l.column03  and iif((isnull(l.column26,0)=0 or l.column26=1000),iif(i.COLUMN05='ITTY009','009',0),iif(t.COLUMN16='22401','009',0))='009'
--where  isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1330  and s.COLUMNA03=@AcOwner and
-- s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
 ) Q
set @Query1='select * from #SalesReturnTaxRegister'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='PurchaseTaxRegister')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,Q.GSTIN,CAST((Q.subtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))subtotal,CAST((Q.fsubtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))fsubtotal,Q.tax,Q.taxamt,Q.ftaxamt,Q.examt,Q.fexamt,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt,
(Count(*) over (partition by col1 ORDER BY col1)) scnt,
CAST((Q.ShippingAmt)/iif((Count(*) over (partition by col1 ORDER BY col1))=0,1,(Count(*) over (partition by col1 ORDER BY col1))) as decimal(18,9)) ShippingAmt,Q.STAmt,
CAST((Q.Discount)/iif((Count(*) over (partition by col1 ORDER BY col1))=0,1,(Count(*) over (partition by col1 ORDER BY col1))) as decimal(18,9)) Discount,
CAST((Q.Roundoff)/iif((Count(*) over (partition by col1 ORDER BY col1))=0,1,(Count(*) over (partition by col1 ORDER BY col1))) as decimal(18,9)) Roundoff,Q.INVTOT,Q.ShippingAmt1,Q.Discount1,Q.Roundoff1,Q.Stax,Q.Ptax,Q.Otax into #PurchaseTaxRegister from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT  IIF(iif((isnull(l.column18,0)=0 or l.column18=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','PurchaseTaxRegister','PurchaseServicesTaxRegister') Report,'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column18,0)=0,1000,l.column18)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN55 AS GSTIN,
l.COLUMN12 subtotal,l.COLUMN12 fsubtotal,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,cast(((isnull(l.column09,0)*isnull(l.column33,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,cast(((isnull(l.column09,0)*isnull(l.column33,0))*t.COLUMN17/100)as decimal(18,2))) ftaxamt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,0) examt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,0) fexamt,row_number() over(order by s.COLUMN08) rno  ,s.COLUMN03 fid 
, '' BrandIDN,0 BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt ,ISNULL(s.COLUMN62,0) ShippingAmt,(ISNULL(s.COLUMN22,0)+ISNULL(s.COLUMN63,0)+ISNULL(s.COLUMN24,0)-ISNULL(s.COLUMN41,0)) STAmt,0 Discount,ISNULL(s.COLUMN59,0) Roundoff,ISNULL(s.COLUMN14,0) INVTOT,ISNULL(s.COLUMN62,0) ShippingAmt1,0 Discount1,ISNULL(s.COLUMN59,0) Roundoff1,0 'Stax',0 'Ptax',0 'Otax' from 
PUTABLE005 s inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
inner join MATABLE007 i on i.column02=l.column04 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column18,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column18,0)=0,1000,l.column18))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
UNION ALL
SELECT  'PurchaseTaxRegister' Report,'Purchase' Type,s.column02 col2,s.column01 col1,(iif(isnull(s.column64,0)=0,1000,s.column64)) taxid,s.columna02 ou,s.column04 transno,
FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN55 AS GSTIN,
0 subtotal,0 fsubtotal,iif((iif(isnull(s.column64,0)=0,1000,s.column64))=1000,'Exempted',t.COLUMN04) tax,
iif((iif(isnull(s.column64,0)=0,1000,s.column64))=1000,0,cast(((isnull(s.column34,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,
0 ftaxamt,
0 examt,0 fexamt,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid 
,'' BrandIDN,0 BrandID,Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt ,
ISNULL(s.COLUMN62,0) ShippingAmt,(ISNULL(s.COLUMN22,0)+ISNULL(s.COLUMN63,0)+ISNULL(s.COLUMN24,0)-ISNULL(s.COLUMN41,0)) STAmt,0 Discount,ISNULL(s.COLUMN59,0) Roundoff,ISNULL(s.COLUMN14,0) INVTOT,ISNULL(s.COLUMN62,0) ShippingAmt1,0 Discount1,ISNULL(s.COLUMN59,0) Roundoff1,0 'Stax',1 'Ptax',0 'Otax' from 
PUTABLE005 s 
--inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
--inner join MATABLE007 i on i.column02=l.column04 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(s.column64,'-','') and COLUMNA03=s.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.column64,0)=0,1000,s.column64))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
UNION ALL
SELECT  'PurchaseTaxRegister' Report,'Purchase' Type,s.column02 col2,s.column01 col1,(iif(isnull(s.column60,0)=0,1000,s.column60)) taxid,s.columna02 ou,s.column04 transno,
FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN55 AS GSTIN,
0 subtotal,0 fsubtotal,iif((iif(isnull(s.column60,0)=0,1000,s.column60))=1000,'Exempted',t.COLUMN04) tax,
iif((iif(isnull(s.column60,0)=0,1000,s.column60))=1000,0,cast(((isnull(s.column35,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,
0 ftaxamt,
0 examt,0 fexamt,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid 
, '' BrandIDN,0 BrandID,Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt ,ISNULL(s.COLUMN62,0)ShippingAmt,(ISNULL(s.COLUMN22,0)+ISNULL(s.COLUMN63,0)+ISNULL(s.COLUMN24,0)-ISNULL(s.COLUMN41,0)) STAmt,0 Discount,ISNULL(s.COLUMN59,0) Roundoff,ISNULL(s.COLUMN14,0) INVTOT,ISNULL(s.COLUMN62,0) ShippingAmt1,0 Discount1,ISNULL(s.COLUMN59,0) Roundoff1,1 'Stax',0 'Ptax',0 'Otax' from 
PUTABLE005 s 
--inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
--inner join MATABLE007 i on i.column02=l.column04 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(s.column60,'-','') and COLUMNA03=s.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.column60,0)=0,1000,s.column60))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
UNION ALL
SELECT  'PurchaseTaxRegister' Report,'Purchase' Type,s.column02 col2,s.column01 col1,(iif(isnull(s.column61,0)=0,1000,s.column61)) taxid,s.columna02 ou,s.column04 transno,
FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN55 AS GSTIN,
0 subtotal,0 fsubtotal,iif((iif(isnull(s.column61,0)=0,1000,s.column61))=1000,'Exempted',t.COLUMN04) tax,
iif((iif(isnull(s.column61,0)=0,1000,s.column61))=1000,0,cast(((isnull(s.column40,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,
0 ftaxamt,
0 examt,0 fexamt,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid 
, '' BrandIDN,0 BrandID,Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt ,ISNULL(s.COLUMN62,0)ShippingAmt,(ISNULL(s.COLUMN22,0)+ISNULL(s.COLUMN63,0)+ISNULL(s.COLUMN24,0)-ISNULL(s.COLUMN41,0)) STAmt,0 Discount,ISNULL(s.COLUMN59,0) Roundoff,ISNULL(s.COLUMN14,0) INVTOT,ISNULL(s.COLUMN62,0) ShippingAmt1,0 Discount1,ISNULL(s.COLUMN59,0) Roundoff1,0 'Stax',0 'Ptax',1 'Otax' from 
PUTABLE005 s 
--inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
--inner join MATABLE007 i on i.column02=l.column04 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
--left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(s.column61,'-','') and COLUMNA03=s.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(s.column61,0)=0,1000,s.column61))) and t.COLUMNA03=s.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
--UNION ALL
--SELECT  'PurchaseTaxRegister' Report,'Purchase' Type,s.column02 col2,s.column01 col1,0 taxid,s.columna02 ou,s.column04 transno,
--FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN55 AS GSTIN,
--0 subtotal,0 fsubtotal,'' tax,
--0 taxamt,
--0 ftaxamt,
--0 examt,0 fexamt,row_number() over(order by s.COLUMN08) rno,s.COLUMN03 fid 
--, '' BrandIDN,0 BrandID,Count(*) over (partition by s.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt ,0 ShippingAmt,0 STAmt,0 Discount,ISNULL(S.COLUMN59,0) Roundoff from 
--PUTABLE005 s
--left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
--where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
--s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
) Q
set @Query1='select * from #PurchaseTaxRegister'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='PurchaseServicesTaxRegister')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,CAST((Q.subtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))subtotal,CAST((Q.fsubtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))fsubtotal,Q.tax,Q.taxamt,Q.ftaxamt,Q.examt,Q.fexamt,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt into #PurchaseServicesTaxRegister from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT  IIF(iif((isnull(l.column18,0)=0 or l.column18=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','PurchaseTaxRegister','PurchaseServicesTaxRegister') Report,'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column18,0)=0,1000,l.column18)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,l.COLUMN12 subtotal,l.COLUMN12 fsubtotal,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,cast(((isnull(l.column09,0)*isnull(l.column33,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,cast(((isnull(l.column09,0)*isnull(l.column33,0))*t.COLUMN17/100)as decimal(18,2))) ftaxamt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,0) examt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,0) fexamt,row_number() over(order by s.COLUMN08) rno ,s.COLUMN03 fid
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt  from 
PUTABLE005 s inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0 
inner join MATABLE007 i on i.column02=l.column04 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column18,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column18,0)=0,1000,l.column18))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT  'PurchaseServicesTaxRegister' Report,'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column18,0)=0,1000,l.column18)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,0 subtotal,0 fsubtotal,'Cess' tax,cs.COLUMN07 taxamt,cs.COLUMN07 ftaxamt,0 examt,0 fexamt,row_number() over(order by s.COLUMN08) rno ,s.COLUMN03 fid 
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt from 
PUTABLE005 s inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and (iif(isnull(l.column18,0)='0','1000',l.column18))!='1000' and isnull((l.COLUMNA13),0)=0 
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
 inner join FITABLE036 cs on cs.column09=s.column04 and cs.column05=l.column01 and cs.column03='BILL' and cs.column10=1122 and cs.COLUMNA02=s.columnA02 and cs.columnA03=s.columnA03 and isnull(cs.columnA13,0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column18,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column18,0)=0,1000,l.column18))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
inner join MATABLE007 i on i.column02=l.column04  and iif((isnull(l.column18,0)=0 or l.column18=1000),iif(i.COLUMN05='ITTY009','009',0),iif(t.COLUMN16='22401','009',0))='009' and isnull(i.columnA13,0)=0
left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull(m5.columnA13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(isnull(t.COLUMN16,0)!='22401','PurchaseTaxRegister','PurchaseServicesTaxRegister') Report, 'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column14,0)=0,1000,l.column14)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column05,@DateF) datec,(case when s.COLUMN11=22335 then c.COLUMN05 when s.COLUMN11=22492 then e.COLUMN05 else v.COLUMN05 end) payee,l.COLUMN05 subtotal,l.COLUMN05 fsubtotal,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,cast(((isnull(l.column05,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,cast(((isnull(l.column05,0))*t.COLUMN17/100)as decimal(18,2))) ftaxamt,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,0) examt,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,0) fexamt,row_number() over(order by s.COLUMN05) rno ,1525 fid 
,'' BrandIDN,'' BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN05 as datetime)) rowcnt from 
FITABLE020 s inner join FITABLE022 l on l.column09=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.column02=s.column07 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
left join SATABLE001 v on v.column02=s.column07 and v.columnA03=s.columnA03 and isnull((v.COLUMNA13),0)=0
left join MATABLE010 e on e.column02=s.column07 and e.columnA03=s.columnA03 and isnull((e.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column14,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column14,0)=0,1000,l.column14))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1525 and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column05 between @FromDate and @ToDate
 union all
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(isnull(t.COLUMN16,0)!='22401','PurchaseTaxRegister','PurchaseServicesTaxRegister') Report, 'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column14,0)=0,1000,l.column14)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column05,@DateF) datec,(case when s.COLUMN11=22335 then c.COLUMN05 when s.COLUMN11=22492 then e.COLUMN05 else v.COLUMN05 end) payee,0 subtotal,0 fsubtotal,'Cess' tax,cs.COLUMN07 taxamt,cs.COLUMN07 ftaxamt,0 examt,0 fexamt,row_number() over(order by s.COLUMN05) rno ,1525 fid 
,'' BrandIDN,'' BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN05 as datetime)) rowcnt from 
FITABLE020 s inner join FITABLE022 l on l.column09=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
inner join FITABLE036 cs on cs.column09=s.column04 and cs.column05=l.column01 and cs.column03='PAYMENT VOUCHER' and cs.column10=1122 and cs.columnA03=s.columnA03 and cs.columnA02=s.columnA02 and isnull((cs.COLUMNA13),0)=0 
left join SATABLE002 c on c.column02=s.column07 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
left join SATABLE001 v on v.column02=s.column07 and v.columnA03=s.columnA03 and isnull((v.COLUMNA13),0)=0
left join MATABLE010 e on e.column02=s.column07 and e.columnA03=s.columnA03 and isnull((e.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column14,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column14,0)=0,1000,l.column14))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0 and isnull(t.COLUMN16,0)='22401'
where isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1525 and s.COLUMNA03=@AcOwner  and  isnull(t.COLUMN16,0)='22401' and
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column05 between @FromDate and @ToDate) Q
set @Query1='select * from #PurchaseServicesTaxRegister'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='PurchaseReturnTaxRegister')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,CAST((Q.subtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))subtotal,CAST((Q.fsubtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))fsubtotal,Q.tax,Q.taxamt,Q.ftaxamt,Q.examt,Q.fexamt,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt into #PurchaseReturnTaxRegister from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(iif((isnull(l.column25,0)=0 or l.column25=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','PurchaseReturnTaxRegister','PurchaseReturnServicesTaxRegister') Report, 'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column25,0)=0,1000,l.column25)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column06,@DateF) datec,(case when s.COLUMN50='22335' then c2.COLUMN05 else c.COLUMN05 end) payee,l.COLUMN24 subtotal,l.COLUMN24 fsubtotal,iif((iif(isnull(l.column25,0)=0,1000,l.column25))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column25,0)=0,1000,l.column25))=1000,l.COLUMN24,cast(((isnull(l.column07,0)*isnull(l.column32,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,iif((iif(isnull(l.column25,0)=0,1000,l.column25))=1000,l.COLUMN24,cast(((isnull(l.column07,0)*isnull(l.column32,0))*t.COLUMN17/100)as decimal(18,2))) ftaxamt,iif((iif(isnull(l.column25,0)=0,1000,l.column25))=1000,l.COLUMN24,0) examt,iif((iif(isnull(l.column25,0)=0,1000,l.column25))=1000,l.COLUMN24,0) fexamt,row_number() over(order by s.COLUMN06) rno ,s.COLUMN03 fid,m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN06 as datetime)) rowcnt  from 
PUTABLE001 s inner join PUTABLE002 l on l.column19=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
left join SATABLE002 c2 on c2.column02=s.column05 and c2.columnA03=s.columnA03 and isnull((c2.COLUMNA13),0)=0
 inner join MATABLE007 i on i.column02=l.column03 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
 left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column25,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column25,0)=0,1000,l.column25))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1355  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column06 between @FromDate and @ToDate
--union all
--SELECT  'SevicesTaxRegister' Report,'Purchase' Type,l.column02 col2,(iif(isnull(l.column25,0)=0,1000,l.column25)) taxid,s.columna02 ou,s.column04 transno,s.column06 datec,c.COLUMN05 payee,0 subtotal,0 fsubtotal,'Cess' tax,-(cs.COLUMN08) taxamt,-(cs.COLUMN08) ftaxamt,0 examt,0 fexamt from 
--PUTABLE001 s inner join PUTABLE002 l on l.column15=s.column01 and (iif(isnull(l.column25,0)=0,1000,l.column25))!=1000 and isnull((l.COLUMNA13),0)=0 left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03
-- inner join FITABLE036 cs on cs.column09=s.column04 and cs.column05=s.column01 and cs.column03='Debit Memo' and cs.column10=1122 and c.columnA03=s.columnA03
--left join MATABLE013 t on t.column02=(iif(isnull(l.column25,0)=0,1000,l.column25)) and isnull(t.columna13,0)=0
-- inner join MATABLE007 i on i.column02=l.column03  and iif((isnull(l.column25,0)=0 or l.column25=1000),iif(i.COLUMN05='ITTY009','009',0),iif(t.COLUMN16='22401','009',0))='009'
--where isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1355  and s.COLUMNA03=@AcOwner and
--s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)
) Q
set @Query1='select * from #PurchaseReturnTaxRegister'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='PurchaseTaxRegisterDetails')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,Q.GSTIN,Q.subtotal,CAST((Q.fsubtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2))fsubtotal,Q.tax,Q.taxamt,Q.ftaxamt,Q.examt,Q.fexamt,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt into #PurchaseTaxRegisterDetails from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT  IIF(iif((isnull(l.column18,0)=0 or l.column18=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','PurchaseTaxRegisterDetails','PurchaseServicesTaxRegister') Report,'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column18,0)=0,1000,l.column18)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN55 AS GSTIN,
l.COLUMN12 subtotal,l.COLUMN12 fsubtotal,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,cast(((isnull(l.column09,0)*isnull(l.column33,0))*t.COLUMN17/100)as decimal(18,2))) taxamt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,cast(((isnull(l.column09,0)*isnull(l.column33,0))*t.COLUMN17/100)as decimal(18,2))) ftaxamt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,0) examt,iif((iif(isnull(l.column18,0)=0,1000,l.column18))=1000,l.COLUMN12,0) fexamt,row_number() over(order by s.COLUMN08) rno  ,s.COLUMN03 fid 
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt from 
PUTABLE005 s inner join PUTABLE006 l on l.column13=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE001 c on c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0
 inner join MATABLE007 i on i.column02=l.column04 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
left join MATABLE005 m5 on m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column18,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column18,0)=0,1000,l.column18))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner  and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
-- union all
----EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
--SELECT IIF(isnull(t.COLUMN16,0)!='22401','PurchaseTaxRegister','PurchaseServicesTaxRegister') Report, 'Purchase' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column14,0)=0,1000,l.column14)) taxid,s.columna02 ou,s.column04 transno,s.column05 datec,(case when s.COLUMN11=22335 then c.COLUMN05 when s.COLUMN11=22492 then e.COLUMN05 else v.COLUMN05 end) payee,l.COLUMN05 subtotal,l.COLUMN05 fsubtotal,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,l.COLUMN06) taxamt,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,l.COLUMN06) ftaxamt,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,0) examt,iif((iif(isnull(l.column14,0)=0,1000,l.column14))=1000,l.COLUMN05,0) fexamt,row_number() over(order by s.COLUMN05) rno  ,1525 fid from 
--FITABLE020 s inner join FITABLE022 l on l.column09=s.column01 and isnull((l.COLUMNA13),0)=0 
--left join SATABLE002 c on c.column02=s.column07 and c.columnA03=s.columnA03
--left join SATABLE001 v on v.column02=s.column07 and v.columnA03=s.columnA03
--left join MATABLE010 e on e.column02=s.column07 and e.columnA03=s.columnA03
--left join MATABLE013 t on t.column02=(iif(isnull(l.column14,0)=0,1000,l.column14)) and isnull(t.columna13,0)=0
--where isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1525 and s.COLUMNA03=@AcOwner  and 
--s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
) Q
set @Query1='select * from #PurchaseTaxRegisterDetails'+@whereStr
exec (@Query1) 
end
ELSE if(@ReportName='SalesTaxRegisterDetails')
begin
select Q.Report,Q.Type,Q.col2,Q.col1,Q.taxid,Q.ou,Q.transno,Q.datec,Q.payee,Q.GSTIN,Q.subtotal,CAST((Q.subtotal)/iif(Q.rowcnt=0,1,Q.rowcnt) as decimal(18,2)) fsubtotal,Q.tax,Q.taxamt,0 ftaxamt,Q.examt,0 fexamt,Q.form,Q.rno,Q.fid,Q.BrandIDN,Q.BrandID,Q.rowcnt from (
--EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
SELECT IIF(iif((isnull(l.column21,0)=0 or l.column21=1000),iif(i.COLUMN05='ITTY009','22401',0),t.COLUMN16)!='22401','SalesTaxRegisterDetails','SalesSevicesTaxRegister') Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column21,0)=0,1000,l.column21)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column08,@DateF) datec,c.COLUMN05 payee,s.COLUMN64 AS GSTIN,l.COLUMN14 subtotal,0 fsubtotal,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,l.COLUMN14,cast(((isnull(l.column10,0)*isnull(l.column35,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt,iif((iif(isnull(l.column21,0)=0,1000,l.column21))=1000,l.COLUMN14,0) examt,0 fexamt,f.COLUMN04 form,row_number() over(order by s.COLUMN08) rno  ,s.COLUMN03 fid
, m5.COLUMN04  BrandIDN,isnull(i.COLUMN10,0) BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN08 as datetime)) rowcnt from 
SATABLE009 s inner join SATABLE010 l on   s.COLUMNA03=@AcOwner  and s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and  isnull(s.COLUMNA13,0)=0 and s.column08 between @FromDate and @ToDate and l.columnA03=@AcOwner and l.column15=s.column01 and s.COLUMNA02=l.columnA02 and s.COLUMNA03=l.columnA03 and isnull((l.COLUMNA13),0)=0 
left join SATABLE002 c on c.columnA03=@AcOwner and c.column02=s.column05 and c.columnA03=s.columnA03 and isnull((c.COLUMNA13),0)=0 
inner join MATABLE007 i on i.columnA03=@AcOwner and i.column02=l.column05 and s.COLUMNA03=i.columnA03 and isnull((i.COLUMNA13),0)=0
left join MATABLE005 m5 on m5.columnA03=@AcOwner and m5.column02=i.column10 and m5.COLUMNA03=s.columnA03 and isnull((m5.COLUMNA13),0)=0
left join MATABLE002 f on f.columnA03=@AcOwner and f.column03=11150 and f.column02=s.COLUMN31 and s.COLUMNA03=f.columnA03 and isnull((f.COLUMNA13),0)=0
left join MATABLE013 t on t.columnA03=@AcOwner and ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column21,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column21,0)=0,1000,l.column21))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
where  isnull(s.COLUMNA13,0)=0  and s.COLUMNA03=@AcOwner and 
s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column08 between @FromDate and @ToDate
-- union all
----EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
--SELECT IIF(isnull(t.COLUMN16,0)!='22401','SalesTaxRegisterDetails','SalesSevicesTaxRegister') Report,'Sales' Type,l.column02 col2,s.column01 col1,(iif(isnull(l.column15,0)=0,1000,l.column15)) taxid,s.columna02 ou,s.column04 transno,FORMAT(s.column05,@DateF) datec,(case when s.COLUMN20=22335 then c.COLUMN05 when s.COLUMN20=22492 then e.COLUMN05 else v.COLUMN05 end) payee,l.COLUMN05 subtotal,0 fsubtotal,iif((iif(isnull(l.column15,0)=0,1000,l.column15))=1000,'Exempted',t.COLUMN04) tax,iif((iif(isnull(l.column15,0)=0,1000,l.column15))=1000,l.COLUMN05,cast(((isnull(l.column05,0))*t.COLUMN07/100)as decimal(18,2))) taxamt,0 ftaxamt,iif((iif(isnull(l.column15,0)=0,1000,l.column15))=1000,l.COLUMN05,0) examt,0 fexamt,null form,row_number() over(order by s.COLUMN05) rno ,1526 fid
--, '' BrandIDN,'' BrandID,Count(*) over (partition by l.column02 ORDER BY cast(s.COLUMN05 as datetime)) rowcnt from 
--FITABLE023 s inner join FITABLE024 l on l.column09=s.column01 and isnull((l.COLUMNA13),0)=0 
--left join SATABLE002 c on c.column02=s.column08 and c.columnA03=s.columnA03
--left join SATABLE001 v on v.column02=s.column08 and v.columnA03=s.columnA03
--left join MATABLE010 e on e.column02=s.column08 and e.columnA03=s.columnA03
--left join MATABLE013 t on ((t.COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.column15,'-','') and COLUMNA03=l.columnA03 and isnull(COLUMNA13,0)=0)) s)) or t.COLUMN02=(iif(isnull(l.column15,0)=0,1000,l.column15))) and t.COLUMNA03=l.columnA03 and isnull(t.columna13,0)=0
--where  isnull(s.COLUMNA13,0)=0  and s.COLUMN03=1526  and s.COLUMNA03=@AcOwner and 
-- s.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) and s.column05 between @FromDate and @ToDate
 ) Q
--set @Query1='select * from #SalesTaxRegisterDetails'+@whereStr
--exec (@Query1) 
end
end









GO
