USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_V_Address]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_V_Address]
(
	@VendorID int
)

AS
BEGIN
	SELECT a.COLUMN06,a.COLUMN07,a.COLUMN08,a.COLUMN09,
	a.COLUMN10,
	(SELECT COLUMN03 from MATABLE017 where COLUMN02= a.COLUMN11) COLUMN11,
	(SELECT COLUMN03 from MATABLE016 where COLUMN02= a.COLUMN16) COLUMN16,
	
	a.COLUMN12
	FROM dbo.SATABLE003 a
	WHERE a.COLUMN19='Vendor' and a.COLUMN20 = (select COLUMN01 FROM dbo.SATABLE001 WHERE COLUMN02= @vendorId)
END














GO
