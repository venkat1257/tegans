USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PROC_TransCalculations]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_PROC_TransCalculations](@ITEM nvarchar(250)=null,@QTY decimal(18,2)=0,@BPrice decimal(18,2)=0,
@PLEVEL nvarchar(250)=null,@HTax nvarchar(250)=null,@HPLEVEL nvarchar(250)=null,@HDiscount decimal(18,2)=0,@DiscountMode nvarchar(250)=null,@DiscountAmt decimal(18,2)=0,@PCharges  decimal(18,2)=0,@SCharges  decimal(18,2)=0,@OCharges  decimal(18,2)=0,@Inclusive nvarchar(250)=null,@TAX nvarchar(250)=null,@Date nvarchar(250)=null,@FormID nvarchar(250)=null,
@TLTaxAmt  decimal(18,2)=0,@TLineAmt  decimal(18,2)=0,@TDiscAmt  decimal(18,2)=0,@TSCESS  decimal(18,2)=0,@TKCESS  decimal(18,2)=0,@TEDCESS  decimal(18,2)=0,@TSHCESS  decimal(18,2)=0,@RoundOffVal  decimal(18,2)=0,@PackTax nvarchar(250)=null,@ShipTax nvarchar(250)=null,@OtherTax nvarchar(250)=null,@Party nvarchar(250)=null,@Tdsamt decimal(18,2)=0,@HDisper decimal(18,2)=0,@LineDiscPer decimal(18,2)=0,@Screen nvarchar(250)=null,@OPUnit nvarchar(250)=null,@AcOwner nvarchar(250)=null)
as
begin
DECLARE @cnt INT = 1,@taxcnt INT = 0,@rowscnt INT = 0,@RATE decimal(18,2)=0,@PLevelType nvarchar(250)=null,@Group  int =1,@TAXRATE decimal(18,2)=0,@TAXAMT decimal(18,2)=0,@TOTTAXAMT decimal(18,2)=0,@HTAXAMT decimal(18,2)=0,
@SUBTOTAMT decimal(18,2)=0,@TSUBTOTAMT decimal(18,2)=0,@TOTAMT decimal(18,2)=0,@Discount decimal(18,2)=0,@FinalAmt decimal(18,2)=0,@MDiscount decimal(18,2)=0,@TaxAgency nvarchar(250)=null,@TaxApplyon nvarchar(250)=null,@UOM nvarchar(250)=null,@LOT nvarchar(250)=null,@LOC nvarchar(250)=null
,@cessIS decimal(18,2)=0,@cessIK decimal(18,2)=0,@cessed decimal(18,2)=0,@cesssh decimal(18,2)=0,@ECRate decimal(18,2)=0,@SHECRate decimal(18,2)=0,@PTaxAmt  decimal(18,2)=0,@STaxAmt  decimal(18,2)=0,@OTaxAmt  decimal(18,2)=0

--Line PriceLevel Calculations
if(@PLEVEL!='1000' and isnull(@PLEVEL,'')!='')
begin
DECLARE @cache TABLE (Rate decimal(18,2) null,pricetype nvarchar(250) null,Discount decimal(18,2) null)
INSERT @cache EXECUTE dbo.InvoiceHeaderPriceLevelDetails @AcOwner=@AcOwner,@Pricelevel=@PLEVEL,@Type='Line',@Price=@BPrice,@Qty=@QTY,@Date=@Date
SELECT @RATE=Rate,@PLevelType=pricetype FROM @cache
set @rowscnt=(SELECT @@ROWCOUNT)
set @RATE=(iif((cast(@RATE as nvarchar(250))='' or @rowscnt=0),@BPrice,@RATE))
end
else if((cast(@DiscountAmt as nvarchar(250))!='0' and isnull(cast(@DiscountAmt as nvarchar(250)),'')!='') or (cast(@LineDiscPer as nvarchar(250))!='0' and isnull(cast(@LineDiscPer as nvarchar(250)),'')!='' and @Screen='Retail'))
begin
if (@DiscountMode = '22556') set @DiscountAmt = cast(cast((@QTY *@BPrice) as decimal(18,2)) * (@DiscountAmt * (0.01)) as decimal(18,2))
else if (cast(@LineDiscPer as nvarchar(250))!='0' and isnull(cast(@LineDiscPer as nvarchar(250)),'')!='' and @Screen='Retail') set @DiscountAmt = cast(cast((@QTY *@BPrice) as decimal(18,2)) * (@LineDiscPer * (0.01)) as decimal(18,2))
if (@DiscountAmt > 0 and @QTY!=0) set @RATE = cast(@BPrice-((@DiscountAmt) / (@QTY)) as decimal(18,2));
else set @RATE=@BPrice
end
else set @RATE=@BPrice
set @RATE=(iif(isnull(cast(@RATE as nvarchar(250)),'')='','0',@RATE))
set @TOTAMT=(@QTY *@RATE)
set @SUBTOTAMT=(@QTY *@BPrice)
set @Discount=iif((@PLevelType=22778 or @FormID='1273' or (@Screen='Retail' and (@PLEVEL='1000' or isnull(@PLEVEL,'')=''))),cast(@SUBTOTAMT-@TOTAMT as decimal(18,2)),0)
set @MDiscount=iif(@PLevelType=22831,cast(@SUBTOTAMT-@TOTAMT as decimal(18,2)),0)

--Line Level Tax Calculations
if(@TAX!='1000' and isnull(@TAX,'')!='')
begin
SELECT @Group=(select iif((SELECT CHARINDEX('-',@TAX))=1,0,1))
SELECT @TAX=(select replace(@TAX,'-',''))
DECLARE @cache1 TABLE (Rate decimal(18,2) null,Agency nvarchar(250) null,ECRate decimal(18,2) null,SHECRate decimal(18,2) null,Appliedon nvarchar(250) null)
INSERT @cache1 EXECUTE dbo.GetTaxAmount @TaxType=@TAX,@FormID=@FormID,@Party=@Party,@Group=@Group,@AcOwner=@AcOwner
SELECT @TAXRATE=sum(Rate),@TaxAgency=Agency,@TaxApplyon=Appliedon,@ECRate=sum(ECRate),@SHECRate=sum(SHECRate) FROM @cache1 group by Agency,Appliedon
set @TAXRATE=(iif(isnull(cast(@TAXRATE as nvarchar(250)),'')='','0',@TAXRATE))
if (@Inclusive = '22713')
begin
set @RATE=(cast(@RATE-(@RATE* ((@TAXRATE+iif(@TaxAgency=23513,0,@ECRate)+iif(@TaxAgency=23513,0,@SHECRate))/ (100 +(@TAXRATE+iif(@TaxAgency=23513,0,@ECRate)+iif(@TaxAgency=23513,0,@SHECRate)))))as decimal(18,2)))
set @TOTAMT=(@QTY *@RATE)
set @SUBTOTAMT=(@QTY *@BPrice)
end
if(@Group='0')
begin
SET @taxcnt=(SELECT count(*) FROM @cache1)
WHILE @cnt <= @taxcnt
BEGIN
SELECT @TAXRATE=q.Rate,@cnt=q.rno FROM (SELECT *,row_number() over (order by cast(Rate as int))rno FROM @cache1)q where q.rno=@cnt 
set @TOTTAXAMT=cast(@TOTTAXAMT+(@TOTAMT *(@TAXRATE*0.01)) as decimal(18,2))
if(@cnt=@taxcnt)set @TAXAMT=(@TOTTAXAMT)
set @cnt=(@cnt+1)
END
end
else
set @TAXAMT=(@TOTAMT *(@TAXRATE*0.01))
if(@TaxAgency=22401)
begin
if(cast(@Date as date)>'11/14/2015')
set @cessIS=(@TOTAMT *(@ECRate*0.01))
if(cast(@Date as date)>'05/31/2016')
set @cessIK=(@TOTAMT *(@SHECRate*0.01))
end
end

--Total Calculations
set @TDiscAmt=cast((@TDiscAmt +@Discount)as decimal(18,2))
set @TLineAmt=cast((@TLineAmt +@TOTAMT)as decimal(18,2))
set @TSCESS=cast((@TSCESS +@cessIS)as decimal(18,2))
set @TKCESS=cast((@TKCESS +@cessIK)as decimal(18,2))
set @TLTaxAmt=cast((@TLTaxAmt +@TAXAMT)as decimal(18,2))
set @TSUBTOTAMT=(cast(@TLineAmt as decimal(18,2))+cast(@TDiscAmt as decimal(18,2)))
if(@TaxAgency=23513)
begin
set @cessed=cast((@TAXAMT +@cessIS+@cessIK)*(@ECRate*0.01) as decimal(18,2))
set @cesssh=cast((@TAXAMT +@cessIS+@cessIK)*(@SHECRate*0.01) as decimal(18,2))
set @TEDCESS=cast((@TEDCESS +@cessed)as decimal(18,2))
set @TSHCESS=cast((@TSHCESS +@cesssh)as decimal(18,2))
end

--Header Tax Calculations
if(@HTax!='1000' and isnull(@HTax,'')!='')
begin
SELECT @Group=(select iif((SELECT CHARINDEX('-',@HTax))=1,0,1))
SELECT @HTax=(select replace(@HTax,'-',''))
DELETE FROM @cache1
INSERT @cache1 EXECUTE dbo.GetTaxAmount @TaxType=@HTax,@FormID=@FormID,@Party=@Party,@Group=@Group,@AcOwner=@AcOwner
SELECT @TAXRATE=sum(Rate),@TaxAgency=Agency,@TaxApplyon=Appliedon FROM @cache1 group by Agency,Appliedon
set @TAXRATE=(iif(isnull(cast(@TAXRATE as nvarchar(250)),'')='','0',@TAXRATE))

if (@TaxApplyon= 'Tax') set @HTAXAMT = cast((@TLTaxAmt + @TSCESS + @TKCESS + @TEDCESS + @TSHCESS) * (@TAXRATE)as decimal(18,2))
else if (@TaxApplyon= 'SubTotalTax') set @HTAXAMT = cast((@TLineAmt - @TDiscAmt + @TLTaxAmt + @TSCESS + @TKCESS + @TEDCESS + @TSHCESS) * (@TAXRATE)as decimal(18,2))
else
begin
if(@Group='0')
begin
set @TOTTAXAMT=(0)set @TAXRATE=(0)set @cnt=(1)set @taxcnt=(0)
SET @taxcnt=(SELECT count(*) FROM @cache1)
WHILE @cnt <= @taxcnt
BEGIN
SELECT @TAXRATE=q.Rate,@cnt=q.rno FROM (SELECT *,row_number() over (order by cast(Rate as int))rno FROM @cache1)q where q.rno=@cnt 
set @TOTTAXAMT=cast(@TOTTAXAMT+(@TLineAmt *(@TAXRATE*0.01)) as decimal(18,2))
if(@cnt=@taxcnt)set @HTAXAMT=(@TOTTAXAMT)
set @cnt=(@cnt+1)
END
end
else
set @HTAXAMT=(@TLineAmt *(@TAXRATE*0.01))
end
set @TLTaxAmt=cast(cast(@TLTaxAmt as decimal(18,2))+cast(@HTAXAMT as decimal(18,2))as decimal(18,2))
end

set @FinalAmt=cast(cast(@TLineAmt as decimal(18,2))+cast(@PCharges as decimal(18,2))+cast(@SCharges as decimal(18,2))+cast(@OCharges as decimal(18,2))+cast(@TLTaxAmt as decimal(18,2)) +@TSCESS +@TKCESS +@TEDCESS +@TSHCESS as decimal(18,2))
--Header PriceLevel Calculations
if(@HPLEVEL!='1000' and isnull(@HPLEVEL,'')!='')
begin
DELETE FROM @cache
INSERT @cache EXECUTE dbo.InvoiceHeaderPriceLevelDetails @AcOwner=@AcOwner,@Pricelevel=@HPLEVEL,@Type='Header',@Price=@FinalAmt,@Qty=@QTY,@Date=@Date
SELECT @HDiscount=Discount FROM @cache
set @rowscnt=(SELECT @@ROWCOUNT)
set @HDiscount=(iif((cast(@HDiscount as nvarchar(250))='' or @rowscnt=0),'0',@HDiscount))
set @FinalAmt=cast(cast(@FinalAmt as decimal(18,2))-cast(@HDiscount as decimal(18,2)) as decimal(18,2))
end
else if(cast(@HDisper as nvarchar(250))!='0.00' and cast(@HDisper as nvarchar(250))!='0' and isnull(cast(@HDisper as nvarchar(250)),'')!='')
begin
set @HDiscount = (0);
end
else
begin
set @FinalAmt=cast(cast(@FinalAmt as decimal(18,2))-cast(@HDiscount as decimal(18,2)) as decimal(18,2))
end

--Packing Tax calculations
if(@PackTax!='1000' and isnull(@PackTax,'')!='')
begin
SELECT @Group=(select iif((SELECT CHARINDEX('-',@PackTax))=1,0,1))
SELECT @PackTax=(select replace(@PackTax,'-',''))
DELETE FROM @cache1
INSERT @cache1 EXECUTE dbo.GetTaxAmount @TaxType=@PackTax,@FormID=@FormID,@Party=@Party,@Group=@Group,@AcOwner=@AcOwner
SELECT @TAXRATE=sum(Rate),@TaxAgency=Agency,@TaxApplyon=Appliedon FROM @cache1 group by Agency,Appliedon
set @TAXRATE=(iif(isnull(cast(@TAXRATE as nvarchar(250)),'')='','0',@TAXRATE))
if(@Group='0')
begin
set @PTaxAmt=(0)set @TAXRATE=(0)set @cnt=(1)set @taxcnt=(0)
SET @taxcnt=(SELECT count(*) FROM @cache1)
WHILE @cnt <= @taxcnt
BEGIN
SELECT @TAXRATE=q.Rate,@cnt=q.rno FROM (SELECT *,row_number() over (order by cast(Rate as int))rno FROM @cache1)q where q.rno=@cnt 
set @PTaxAmt=cast(@PTaxAmt+(@PCharges *(@TAXRATE*0.01)) as decimal(18,2))
if(@cnt=@taxcnt)set @PTaxAmt=(@PTaxAmt)
set @cnt=(@cnt+1)
END
end
else
set @PTaxAmt=(@PCharges *(@TAXRATE*0.01))
end
--Shipping Tax calculations
if(@ShipTax!='1000' and isnull(@ShipTax,'')!='')
begin
SELECT @Group=(select iif((SELECT CHARINDEX('-',@ShipTax))=1,0,1))
SELECT @ShipTax=(select replace(@ShipTax,'-',''))
DELETE FROM @cache1
INSERT @cache1 EXECUTE dbo.GetTaxAmount @TaxType=@ShipTax,@FormID=@FormID,@Party=@Party,@Group=@Group,@AcOwner=@AcOwner
SELECT @TAXRATE=sum(Rate),@TaxAgency=Agency,@TaxApplyon=Appliedon FROM @cache1 group by Agency,Appliedon
set @TAXRATE=(iif(isnull(cast(@TAXRATE as nvarchar(250)),'')='','0',@TAXRATE))
if(@Group='0')
begin
set @STaxAmt=(0)set @TAXRATE=(0)set @cnt=(1)set @taxcnt=(0)
SET @taxcnt=(SELECT count(*) FROM @cache1)
WHILE @cnt <= @taxcnt
BEGIN
SELECT @TAXRATE=q.Rate,@cnt=q.rno FROM (SELECT *,row_number() over (order by cast(Rate as int))rno FROM @cache1)q where q.rno=@cnt 
set @STaxAmt=cast(@STaxAmt+(@SCharges *(@TAXRATE*0.01)) as decimal(18,2))
if(@cnt=@taxcnt)set @STaxAmt=(@STaxAmt)
set @cnt=(@cnt+1)
END
end
else
set @STaxAmt=(@SCharges *(@TAXRATE*0.01))
end
--Other Tax calculations
if(@OtherTax!='1000' and isnull(@OtherTax,'')!='')
begin
SELECT @Group=(select iif((SELECT CHARINDEX('-',@OtherTax))=1,0,1))
SELECT @OtherTax=(select replace(@OtherTax,'-',''))
DELETE FROM @cache1
INSERT @cache1 EXECUTE dbo.GetTaxAmount @TaxType=@OtherTax,@FormID=@FormID,@Party=@Party,@Group=@Group,@AcOwner=@AcOwner
SELECT @TAXRATE=sum(Rate),@TaxAgency=Agency,@TaxApplyon=Appliedon FROM @cache1 group by Agency,Appliedon
set @TAXRATE=(iif(isnull(cast(@TAXRATE as nvarchar(250)),'')='','0',@TAXRATE))
if(@Group='0')
begin
set @OTaxAmt=(0)set @TAXRATE=(0)set @cnt=(1)set @taxcnt=(0)
SET @taxcnt=(SELECT count(*) FROM @cache1)
WHILE @cnt <= @taxcnt
BEGIN
SELECT @TAXRATE=q.Rate,@cnt=q.rno FROM (SELECT *,row_number() over (order by cast(Rate as int))rno FROM @cache1)q where q.rno=@cnt 
set @OTaxAmt=cast(@OTaxAmt+(@OCharges *(@TAXRATE*0.01)) as decimal(18,2))
if(@cnt=@taxcnt)set @OTaxAmt=(@OTaxAmt)
set @cnt=(@cnt+1)
END
end
else
set @OTaxAmt=(@OCharges *(@TAXRATE*0.01))
end
set @PTaxAmt=cast(cast(@PTaxAmt as decimal(18,2))+cast(@STaxAmt as decimal(18,2))+cast(@OTaxAmt as decimal(18,2)) as decimal(18,2))
set @PCharges=cast(cast(@PCharges as decimal(18,2))+cast(@SCharges as decimal(18,2))+cast(@OCharges as decimal(18,2)) as decimal(18,2))
set @FinalAmt=cast(cast(@FinalAmt as decimal(18,2))+cast(@PTaxAmt as decimal(18,2)) as decimal(18,2))
--Retail Header discount percentage
if(cast(@HDisper as nvarchar(250))!='0.00' and cast(@HDisper as nvarchar(250))!='0' and isnull(cast(@HDisper as nvarchar(250)),'')!='')
begin
set @HDiscount = (cast((cast(@FinalAmt as decimal(18,2))*cast(@HDisper as decimal(18,2))) * (0.01) as decimal(18,2)));
set @FinalAmt=(cast(cast(@FinalAmt as decimal(18,2))-cast(@HDiscount as decimal(18,2)) as decimal(18,2)))
end

--RoundOff Calculations
declare @roundofftype nvarchar(250),@isroundoff bit=0
select @isroundoff=isnull(COLUMN04,0),@roundofftype = COLUMN05 from CONTABLE031 where COLUMNA03=@AcOwner and  (isnull(COLUMNA02,0)=@Opunit or COLUMNA02 is null) and  COLUMN03='Allow Round off' and isnull(column04,0)=1 and isnull(columna13,0)=0
if (@isroundoff = '1')
begin
        if (@roundofftype = 'UW')
		begin
            set @RoundOffVal = cast(CEILING(@FinalAmt) - ((@FinalAmt))as decimal(18,2));
            set @FinalAmt = CEILING(@FinalAmt);
        end
        else if (@roundofftype = 'DW')
		begin
            set @RoundOffVal = cast(FLOOR(@FinalAmt) - ((@FinalAmt))as decimal(18,2));
            set @FinalAmt = FLOOR(@FinalAmt);
        end
        else if (@roundofftype = 'NR')
		begin
            set @RoundOffVal = cast(ROUND(@FinalAmt,0) - ((@FinalAmt))as decimal(18,2));
            set @FinalAmt = ROUND(@FinalAmt,0);
        end
        else 
		begin
            set @FinalAmt = cast((@FinalAmt) + ((@RoundOffVal))as decimal(18,2));
		end
end
else 
begin
     set @FinalAmt = cast((@FinalAmt) + ((@RoundOffVal))as decimal(18,2));
end
--if(@Tdsamt>0)set @FinalAmt = cast((@FinalAmt) - ((@Tdsamt))as decimal(18,2));
select @RATE 'RATE',@SUBTOTAMT 'SUBTOT',@TOTAMT 'AMT',@TAXAMT 'Tax',cast(@TOTAMT + @TAXAMT as decimal(18,2))'LINETOTAMT',@Discount 'Discount',@TSUBTOTAMT 'TSUBTOT',
@FinalAmt 'TAMT',@TLTaxAmt 'TTax',@TDiscAmt 'TDiscount',@HDiscount 'HDiscount',@HTAXAMT 'HTAXAMT',
@cessIS 'SBC',@cessIK 'KKC',@TSCESS 'TOTSBC',@TKCESS 'TOTKKC',@cessed 'ECESS',@cesssh 'SHCESS',@TEDCESS 'TECESS',@TSHCESS 'TSHCESS',@RoundOffVal 'RoundOffVal',@PCharges 'ShipCost',@PTaxAmt 'ShipTax'
end


GO
