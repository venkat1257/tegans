USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CompanyAddress]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CompanyAddress]
(
	@CompanyID int
)
AS
BEGIN
select C8.COLUMN03,C8.COLUMN04,C8.COLUMN07,C8.COLUMN09,C8.COLUMN10,C8.COLUMN11,c.COLUMN03 compCtry,s.COLUMN03 compState,C8.COLUMN12 compCity,C8.COLUMN14 compZip,C8.COLUMND02 from CONTABLE008 C8 left join
MATABLE016 c on c.COLUMN02=C8.COLUMN15 left join MATABLE017 s on s.COLUMN02=C8.COLUMN13 where C8.COLUMN02=@CompanyID
END

GO
