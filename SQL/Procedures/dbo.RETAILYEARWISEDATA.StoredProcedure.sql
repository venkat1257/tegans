USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[RETAILYEARWISEDATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RETAILYEARWISEDATA]
(
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Location    nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@Query2 nvarchar(max)=null,
@DateF nvarchar(250)=null

)
AS
BEGIN
IF  @ReportName='MonthWiseSalesDetails'
BEGIN
if @Customer!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)) '
end
else
begin
set @whereStr=@whereStr+' and (BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)) '
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end
if(@FromDate!='' and @ToDate!='')
		BEGIN
DECLARE @PFRDATE  nvarchar(250)= '01/01/1900'
select top 1 @PFRDATE=column04 from FITABLE048 where COLUMNA03=@AcOwner and column09 = '1' and isnull(columna13,0)=0
END
select * into #SalesRegister from (
select S2.COLUMN05 Customer,S3.COLUMN10 Area,SUM(isnull(S10.COLUMN10,0)) AQty, SUM(isnull(S10.COLUMN14,0)+isnull(S10.COLUMN23,0)) AValue,DATENAME(month, S9.column08) MNAME,M5.COLUMN04 Brand,S9.COLUMN05 CID,S2.COLUMN37 BrandID,
MONTH(S9.COLUMN08) AS Monthnum from SATABLE009 S9
inner join SATABLE010 S10 on S9.COLUMNA03=@AcOwner AND S9.column08 between @FromDate and @ToDate and S10.COLUMNA03=@AcOwner and  S9.column01= S10.column15  and isnull(S10.COLUMNA13,0) = 0
inner JOIN SATABLE002 S2 ON  S2.COLUMNA03 = @AcOwner and S2.COLUMN02 = S9.COLUMN05 AND isnull(S2.COLUMNA13,0) = 0
LEFT OUTER JOIN SATABLE003 S3 ON  S3.COLUMNA03 = @AcOwner and S3.COLUMN20 = S2.COLUMN01 AND isnull(S3.COLUMNA13,0) = 0 AND S3.COLUMN19 ='Customer'
LEFT JOIN MATABLE005 M5 ON M5.COLUMN02 = S2.COLUMN37 AND M5.COLUMNA03=@AcOwner AND isnull(M5.COLUMNA13,0) = 0 
where S9.column08 between @FromDate and @ToDate 
and isnull(S9.COLUMNA13,0) = 0
and S9.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND S9.COLUMNA03=@AcOwner GROUP BY S9.column08,S2.COLUMN05,S3.COLUMN10,M5.COLUMN04,S9.COLUMN05,S2.COLUMN37
--order by DATEPART(MM, S9.column08) 
) Query 

set @Query1='select customer,Area,(AQty) AQty,(AValue) AValue,MNAME,Brand,BrandID,CID,Monthnum from #SalesRegister'+@whereStr +'
 --group by Monthnum,MNAME,customer,Area,Brand,BrandID,CID
 '
exec (@Query1)
end
end
GO
