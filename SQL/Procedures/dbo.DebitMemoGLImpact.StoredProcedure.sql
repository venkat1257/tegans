USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[DebitMemoGLImpact]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DebitMemoGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,p.column09 Memo,sum(p16.COLUMN13) Credit,-sum(p16.COLUMN12) Debit  from PUTABLE001 p
left outer join putable016 p16 on p16.COLUMN09= p.COLUMN04 and p16.COLUMN07= p.COLUMN05 and p16.COLUMNA03= p.COLUMNA03 and p16.COLUMNA02= p.COLUMNA02 and p16.COLUMN06='Debit Memo' and isnull(p16.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p16.COLUMN03
where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04 ,p.column09
--union all
--select f1.COLUMN04 Account,p.column09 Memo,sum(p17.COLUMN10) Credit,sum(p17.COLUMN11) Debit  from PUTABLE001 p
--left outer join putable017 p17 on p17.COLUMN05= p.COLUMN01 and p17.COLUMN07= p.COLUMN05 and p17.COLUMNA03= p.COLUMNA03 and p17.COLUMNA02= p.COLUMNA02 and p17.COLUMN06='Debit Memo' and isnull(p17.COLUMNA13,0)=0
--left outer join FITABLE001 f1 on f1.COLUMN02 = p17.COLUMN03
--where p.COLUMN02=@ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
--group by f1.COLUMN04,p.column09
--union all
--select f26.COLUMN17 Account,p.column09 Memo,-sum(f26.COLUMN12) Credit,sum(f26.COLUMN11) Debit from PUTABLE001 p
--left outer join fitable026 f26 on f26.COLUMN09 =  p.COLUMN04 and  f26.COLUMN06 =  p.COLUMN05 and f26.COLUMNA03=  p.COLUMNA03 
--and f26.COLUMNA02=  p.COLUMNA02 and f26.COLUMN04 = 'Debit Memo' and isnull(f26.COLUMNA13,0)=0
--left outer join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 
--where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and isnull(p.COLUMNA13,0)=0
--group by f26.COLUMN17,p.column09
union all
select f1.COLUMN04 Account,p.column09 Memo,sum(f36.COLUMN07) Credit,-sum(f36.COLUMN08) Debit from PUTABLE001 p
left outer join fitable036 f36 on f36.COLUMN09 = p.COLUMN04  and f36.COLUMNA03= p.COLUMNA03 and f36.COLUMNA02= p.COLUMNA02 and f36.COLUMN03 = 'Debit Memo' and isnull(f36.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f36.COLUMN10 and f1.COLUMNA03 = f36.COLUMNA03
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner  and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04,p.column09
union all
select f1.COLUMN04 Account,p.column09 Memo,sum(f25.COLUMN09) Credit,sum(f25.COLUMN11) Debit from PUTABLE001 p
left outer join fitable025 f25 on f25.COLUMN05 = p.COLUMN01 and f25.COLUMN06 = p.COLUMN05 and f25.COLUMNA03= p.COLUMNA03 and f25.COLUMNA02= p.COLUMNA02 and f25.COLUMN04 = 'Debit Memo' and isnull(f25.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f25.COLUMN08 and f1.COLUMNA03 = f25.COLUMNA03 
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04,p.column09
union all
select f1.COLUMN04 Account,p.column09 Memo,sum(f34.COLUMN07) Credit,-sum(f34.COLUMN08) Debit from PUTABLE001 p
left outer join fitable034 f34 on f34.COLUMN09 = p.COLUMN04 and f34.COLUMN06 = p.COLUMN05 and f34.COLUMNA03= p.COLUMNA03 and f34.COLUMNA02= p.COLUMNA02 and f34.COLUMN03 = 'Debit Memo' and isnull(f34.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10 and f1.COLUMNA03 = f34.COLUMNA03 AND ISNULL(f1.COLUMNA13,0)=0
where  p.COLUMN02= @ide and p.COLUMNA03=@AcOwner and f1.COLUMNA03 = p.COLUMNA03 and isnull(p.COLUMNA13,0)=0
group by f1.COLUMN04,p.column09
)
select [Account],[Memo],[Credit],[Debit] from MyTable
group by [Account],[Memo],[Credit],[Debit]
end


GO
