USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[CheckDirectory]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CheckDirectory] (
@text [nvarchar](max), 
@path [nvarchar](max), 
@append [bit]
)
AS 
begin
declare @Res int

 CREATE TABLE #ResultSet (Directory nvarchar(200))
 INSERT INTO #ResultSet
 EXEC master.dbo.xp_subdirs 'D:\MySmartSCM_DB_Logs\'
 Select @res=count(*)  FROM #ResultSet where Directory = (cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))
 if(@Res=0)
 begin
 declare @Location nvarchar(260)
 declare @cmdpath nvarchar(260)
 set @Location='D:\MySmartSCM_DB_Logs\'+(cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))
 set @cmdpath = 'MD '+ @Location
 exec master.dbo.xp_cmdshell @cmdpath
 drop table #resultset
 end
 
 set @Location='D:\MySmartSCM_DB_Logs\'+(cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))
 CREATE TABLE #ResultSet1 (Directory varchar(200))
 INSERT INTO #ResultSet1
 EXEC master.dbo.xp_subdirs @Location
 Select @res=count(*)  FROM #ResultSet1 where Directory = (cast((select datepart(MONTH,GETDATE()))as nvarchar(250)))
 if(@Res=0)
 begin
 set @Location='D:\MySmartSCM_DB_Logs\'+(cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))+'\'+(cast((select datepart(MONTH,GETDATE()))as nvarchar(250)))
 set @cmdpath = 'MD '+ @Location
 exec master.dbo.xp_cmdshell @cmdpath
 drop table #resultset1
 end

 set @Location='D:\MySmartSCM_DB_Logs\'+(cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))+'\'+(cast((select datepart(MONTH,GETDATE()))as nvarchar(250)))
 CREATE TABLE #ResultSet2 (Directory varchar(200))
 INSERT INTO #ResultSet2
 EXEC master.dbo.xp_subdirs @Location
 Select @res=count(*)  FROM #ResultSet2 where Directory = (cast((select datepart(DAY,GETDATE()))as nvarchar(250)))
 if(@Res=0)
 begin
 set @Location='D:\MySmartSCM_DB_Logs\'+(cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))+'\'+(cast((select datepart(MONTH,GETDATE()))as nvarchar(250)))+'\'+(cast((select datepart(DAY,GETDATE()))as nvarchar(250)))
 set @cmdpath = 'MD '+ @Location
 exec master.dbo.xp_cmdshell @cmdpath
 drop table #resultset2
 end
 set @Location='D:\MySmartSCM_DB_Logs\'+(cast((select datepart(YEAR,GETDATE()))as nvarchar(250)))+'\'+(cast((select datepart(MONTH,GETDATE()))as nvarchar(250)))+'\'+(cast((select datepart(DAY,GETDATE()))as nvarchar(250)))+'\'+@path
 SELECT dbo.WriteTextFile(@text+'  '+(cast((select GETDATE())as nvarchar(250))), @Location, 1)
 end










GO
