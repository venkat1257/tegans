USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[AdvancePaymentGLImpact]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AdvancePaymentGLImpact] 
(
@ide nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@OPUnit nvarchar(250)= null
)
AS
BEGIN
;with MyTable as (
select f1.COLUMN04 Account,F20.column08 Memo,0 Credit,sum(F34.COLUMN07) Debit,1 id  from FITABLE020 F20
left outer join FITABLE034 F34 on F34.COLUMN09= F20.COLUMN04 and F34.COLUMNA03= F20.COLUMNA03 and F34.COLUMNA02= F20.COLUMNA02 and F34.COLUMN03='ADVANCE PAYMENT' and isnull(F34.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = f34.COLUMN10
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F20.column08
union all
select f1.COLUMN04 Account,F20.column08 Memo,sum(p19.COLUMN10) Credit,0 Debit,2 id  from FITABLE020 F20
left outer join putable019 p19 on p19.COLUMN05= F20.COLUMN04 and  p19.COLUMN08= F20.COLUMN01 and p19.COLUMNA03= F20.COLUMNA03 and p19.COLUMNA02= F20.COLUMNA02 and p19.COLUMN06='ADVANCE PAYMENT' and isnull(p19.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = p19.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F20.column08
UNION ALL
select f1.COLUMN04 Account,F20.column08 Memo,sum(F52.COLUMN11) Credit,0 Debit,3 id  from FITABLE020 F20
left outer join FITABLE052 F52 on F52.COLUMN05= F20.COLUMN04  and F52.COLUMNA03= F20.COLUMNA03 and F52.COLUMNA02= F20.COLUMNA02 and F52.COLUMN06='ADVANCE PAYMENT' and isnull(F52.COLUMNA13,0)=0
left outer join FITABLE001 f1 on f1.COLUMN02 = F52.COLUMN03
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner and f1.COLUMNA03 = F20.COLUMNA03 and isnull(F20.COLUMNA13,0)=0
group by f1.COLUMN04,F20.column08
union all
select f26.COLUMN17 Account,F20.column08 Memo,0 Credit,sum(f26.COLUMN11) Debit,4 id  from FITABLE020 F20
left outer join fitable026 f26 on f26.COLUMN09= F20.COLUMN04 and f26.COLUMN05= F20.COLUMN01 and f26.COLUMNA03= F20.COLUMNA03 and f26.COLUMNA02= F20.COLUMNA02 and f26.COLUMN04='ADVANCE PAYMENT' and isnull(f26.COLUMNA13,0)=0
left outer join matable013 m13 on m13.COLUMN02=f26.COLUMN08 and m13.COLUMNA03=f26.COLUMNA03 and isnull(m13.COLUMNA13,0)=0
where F20.COLUMN02=@ide and F20.COLUMNA03=@AcOwner  and isnull(F20.COLUMNA13,0)=0 AND ISNULL(f26.COLUMN11,0)>0
group by f26.COLUMN17,F20.column08
)
select [Account],[Memo],[Credit],[Debit],[id] from MyTable
group by [Account],[Memo],[Credit],[Debit],[id] ORDER BY ID ASC
end



GO
