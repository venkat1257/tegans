USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PUTABLE006]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PUTABLE006]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null, 
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null,
	@COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null,
	--EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	@COLUMN20   nvarchar(250)=null,  @COLUMN21   nvarchar(250)=null,  @COLUMN22   nvarchar(250)=null,
	--EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
	@COLUMN23   nvarchar(250)=null,  @COLUMN24   nvarchar(250)=null,  @COLUMN25  nvarchar(250)=null,
	--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	@COLUMN26  nvarchar(250)=null,   @COLUMN27  nvarchar(250)=null,   @COLUMN28  nvarchar(250)=null,
	--EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
	--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
	@COLUMN29  nvarchar(250)=null,   @COLUMN30  nvarchar(250)=null,   @COLUMN31  nvarchar(250)=null,
	@COLUMN32  nvarchar(250)=null,	 @COLUMN33  nvarchar(250)=null,	  @COLUMN34  nvarchar(250)=null,
	@COLUMN35  nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null, 
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null, 
	@COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null, 
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),       
	--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null, @UOMData  XML=null,
	@Result nvarchar(250)=null,      @Status nvarchar(250)=null,      @Totamt nvarchar(250)=null,     
	@poid nvarchar(250)=null ,       @Quotation nvarchar(250)=null
)
AS
BEGIN
begin try
declare @FiscalYearStartDt nvarchar(250)= '01/01/1900'
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@COLUMNA03 AND COLUMN08='1' order by COLUMN01 desc
--EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
set @COLUMN19=(select iif((@COLUMN19!='' and @COLUMN19>0),@COLUMN19,(select max(column02) from fitable037 where column07=1 and column08=1)))
IF @Direction = 'Insert'
BEGIN
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
--set @COLUMN13 = (select MAX(COLUMN01) from  PUTABLE005);
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE006_SequenceNo
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
set @COLUMN33 = (isnull(@COLUMN33,0))
set @COLUMN14=(select COLUMN15 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMN15=(select COLUMN16 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMN16=(select COLUMN17 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMN17=(select COLUMN18 from PUTABLE005 where COLUMN01=@COLUMN13)
set @poid=(select COLUMN06 from PUTABLE005 where COLUMN01=@COLUMN13)
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
declare @Location nvarchar(250)=null,@ProjectID nvarchar(250)=null
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
declare @Date nvarchar(250)=null,@memo nvarchar(250)=null,@Project nvarchar(250)=null,@DecimalPositions int=null,@DecimalQty decimal(18,7)
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
set @COLUMN09=(round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions))
set @DecimalQty=(@COLUMN09)
set @memo=(select COLUMN12 from PUTABLE005 where COLUMN01=@COLUMN13)
set @Project=(select COLUMN26 from PUTABLE005 where COLUMN01=@COLUMN13)

set @Date=(select COLUMN08 from PUTABLE005 where COLUMN01=@COLUMN13)
set @Location = (select isnull(COLUMN29,0) from  PUTABLE005 WHERE COLUMN01=@COLUMN13)
set @Location = (case when (cast(isnull(@COLUMN35,'') as nvarchar(250))!='' and @COLUMN35!=0) then @COLUMN35 else @Location end)
set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
set @ProjectID=(select isnull(COLUMN26,0) from PUTABLE005 where COLUMN01=@COLUMN13)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
set @COLUMNA02=( CASE WHEN (@COLUMN13!= '' and @COLUMN13 is not null and @COLUMN13!= '0' ) THEN (select COLUMN15 from PUTABLE005 
where COLUMN01=@COLUMN13) else @COLUMNA02  END )
if(@poid!='')
begin
set @COLUMN10 =(round(CAST(@COLUMN10 AS decimal(18,2)),@DecimalPositions)- round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions))
end
--EMPHCS1410 LOGS CREATION BY SRINIVAS
declare @tempSTR nvarchar(max)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Line Values are Intiated for PUTABLE006 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull( @COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 				  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ 'at ')
    )

insert into PUTABLE006 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN09,  COLUMN10,  COLUMN11,
   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   COLUMN21,  COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02,
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12,
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN09,  @COLUMN10,  @COLUMN11, @COLUMN12,
   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN19,  @COLUMN09,  
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,  @COLUMN32,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11,@COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS1410 LOGS CREATION BY SRINIVAS
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Line Values are Created in PUTABLE006 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
declare @Billno nvarchar(250)=null,@Billlineref nvarchar(250)=null,@uomselection nvarchar(250)=null,@chk bit,@lotno nvarchar(250)=null
set @lotno=(case when @column27='' then 0 when isnull(@column27,0)=0 then 0  else @column27 end)
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
set @Billno=(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
DECLARE @newID INT, @VENID INT,@ASSET DECIMAL(18,2),@PRATE DECIMAL(18,2)
--set @project=(select COLUMN26 from PUTABLE005 where COLUMN01=@COLUMN13)
set @Billlineref=(select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
--EMPHCS1167 Service items calculation by srinivas 22/09/2015
			set @chk=(select column48 from matable007 where column02=@COLUMN04)
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'Bill',@Billno,@Billlineref,@COLUMN04,@COLUMN14,@COLUMNA03
end
--EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation in Bill
if(@poid!='')
begin
set @COLUMN08=(round(cast(( select sum(isnull(COLUMN09,0)) from PUTABLE006 WHERE COLUMN03 in(select COLUMN02 from PUTABLE003 where COLUMN06=@poid) and COLUMN04=@COLUMN04 and COLUMNA13=0  ) as decimal(18,7)),@DecimalPositions))
end
else
begin
--set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@COLUMN03 and COLUMN13 in(select COLUMN01 from  PUTABLE005 where COLUMN06 in(@poid)))
set @COLUMN08=(round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions))
end
update PUTABLE006 set COLUMN08=@COLUMN08 where COLUMN02=@COLUMN02
--end
if exists(select column01 from putable006 where column13=@column13)
					begin  
declare @ID1 int
declare @PID1 int
set @COLUMN05=(SELECT CAST(@Date AS DATE));
set @ID1=(select COLUMN01 from  PUTABLE005 where COLUMN01=@COLUMN13);
set @PID1=(select DATALENGTH(COLUMN06)  from  PUTABLE005 where COLUMN01=@COLUMN13);
if(@PID1=0)
begin
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
declare @Qty_On_Hand decimal(18,7),@Qty_On_Hand1 decimal(18,7),@Qty_Order decimal(18,7),@Qty_Avl decimal(18,7),@Qty_Cmtd decimal(18,7),@TotalBillQty decimal(18,7),@TotalBillAmt decimal(18,2),@BillAvgPrice decimal(18,2)
set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
--EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
  where h.COLUMN08>=@FiscalYearStartDt  AND l.COLUMN06=@COLUMN06 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT)
  union all
  select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
  union all
  select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt  AND l.COLUMN04=@COLUMN06 AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT))T)
set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN08>=@FiscalYearStartDt  AND l.COLUMN06=@COLUMN06 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT)
  union all
  select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
  union all
  select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt   AND l.COLUMN04=@COLUMN06 AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT))T)
  if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt  and l.COLUMN05=@COLUMN06 AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))
  begin
  set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt  and l.COLUMN05=@COLUMN06 AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))as decimal(18,7)),@DecimalPositions))as decimal(18,7)),@DecimalPositions)
  set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
  where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@COLUMN06 AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))as decimal(18,2)))as decimal(18,2))
  end
            if(isnull(@TotalBillQty,0)=0)
            begin
			set @BillAvgPrice=(0)
			end
			else
			begin
			set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),@DecimalPositions))
			end
declare @AvgPrice decimal(18,2), @FinalAvgPrice decimal(18,2)
if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Bill' and COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN14 and COLUMNA03=@COLUMNA03) AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)
end
else
begin
set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and Column24=@COLUMN06)
end
set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
 --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
 --EMPHCS1167 Service items calculation by srinivas 22/09/2015
 if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False') and @chk=1)
 begin
 --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
 set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID  and Column24=@COLUMN06)
 --EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
 if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)
 --if(@Qty_On_Hand1>=0)
 begin
 --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
 --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
set @Qty_On_Hand=( round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06 AND isnull(COLUMN22,0)=@lotno)as decimal(18,7)),@DecimalPositions)+ round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions));
set @Qty_Cmtd=(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)- round(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06 AND isnull(COLUMN22,0)=@lotno)as decimal(18,7)),@DecimalPositions));
set @Qty_Order = (round(cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06 AND isnull(COLUMN22,0)=@lotno)as decimal(18,7)),@DecimalPositions))
if(@Qty_Order>0 and @Qty_Order>round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions))
Begin
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
set @Qty_Order = (round(cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06 AND isnull(COLUMN22,0)=@lotno)as decimal(18,7)),@DecimalPositions)- round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions));
end
else
begin
set @Qty_Order = 0;
end
set @Qty_Avl =  round(cast(@Qty_Cmtd as decimal(18,7)),@DecimalPositions);
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@COLUMN11,0))
--IF(@PRATE=0)BEGIN SET @PRATE=(@COLUMN33)END
--EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN08=@Qty_On_Hand  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06
--EMPHCS1410 LOGS CREATION BY SRINIVAS
					set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Inventory Updated in FITABLE010 Table by '+cast(@Qty_On_Hand as nvarchar(20))+' for '+isnull(cast(@COLUMN04 as nvarchar(20)),'')+' '+'' + CHAR(13)+CHAR(10) + ''))
					
set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)as decimal(18,7)),@DecimalPositions));
UPDATE FITABLE010 SET COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)*cast(@BillAvgPrice as decimal(18,2)
) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06
if(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)=0)
begin
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06
end
else
begin
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06 AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID
end
--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
--UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19)as decimal(18,2))/@Qty_On_Hand)as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19
end
--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
else
begin
declare @newIID int
declare @tmpnewIID1 int 
			set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
			if(@tmpnewIID1>0)
					begin
					set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
					end
					else
							begin
									set @newIID=1000
									end
									--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
									--set @lotno=(select case when isnull(@lotno,0)=0 then NULL else replace('',null,null) end)
									--EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
									--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1410 LOGS CREATION BY SRINIVAS							
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Values are Intiated for FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newIID as nvarchar(250)) +','+  isnull(@COLUMN04,'')+','+  isnull(@COLUMN09,'')+','+  isnull(@COLUMN12,'')+','+  isnull(@COLUMN11,'')+','+  cast((select COLUMN15 from PUTABLE005 where COLUMN01=@COLUMN13)as nvarchar(250))+','+ @COLUMN19+','+  cast(@Location  as nvarchar(250))+','+ cast( @lotno as nvarchar(250))+','+ isnull( @COLUMNA02,'')+','+
   @COLUMNA03 +  ' at ')
    )
    --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@BillAvgPrice,0))
--IF(@PRATE=0)BEGIN SET @PRATE=(@COLUMN33)END
SET @ASSET=(round(CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,7)),@DecimalPositions)*ISNULL(@PRATE,0))
insert into FITABLE010 
(
  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22,COLUMN23,COLUMN24
)
values
(
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
  @newIID,@COLUMN04,@COLUMN09,@COLUMN09, @ASSET,@BillAvgPrice , (select COLUMN15 from PUTABLE005 where COLUMN01=@COLUMN13 ),@COLUMN19,@COLUMNA02,@COLUMNA03,@Location,@lotno,@ProjectID,@COLUMN06)
 --EMPHCS1410	Logs Creation by srinivas
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Intiated Values are Created in FITABLE010 Table qty by '+cast(@COLUMN09 as nvarchar(20))+' for '+cast(@COLUMN04 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
    )
  --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
END
end
else
begin
--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
exec usp_PUR_TP_InventoryUOM  @Billno,@Billlineref,@COLUMN04,@COLUMN09,@COLUMN14,@COLUMN19,@COLUMNA03,'Insert',@Location,@lotno
end
--DECLARE @newID INT, @VENID INT,@ASSET DECIMAL(18,2),@PRATE DECIMAL(18,2)
if(@chk=1)
Begin
--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
SET @PRATE=(ISNULL(@BillAvgPrice,0))
SET @PRATE=(ISNULL(@COLUMN11,0))
--IF(@PRATE=0)BEGIN SET @PRATE=(@COLUMN33)END
SET @ASSET=(round(CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,7)),@DecimalPositions)*ISNULL(@PRATE,0))
Declare @Amtin nvarchar(250)
set @Amtin=(select COLUMN43 from PUTABLE005 where COLUMN01=@COLUMN13)
--EMPHCS1785 rajasekhar reddy patakota 21/07/2016 Rate Calculation with inclusive of tax in Purchase
IF(@Amtin=22713)BEGIN 
if(cast(isnull(@COLUMN32,0)as decimal(18,2))>0)
begin
SET @ASSET=(@ASSET-cast(isnull(@COLUMN20,0) as decimal(18,2)))
end
else begin SET @ASSET=(@COLUMN12) end
END
set @newID=cast((select ISNULL(MAX(COLUMN02),1000) from PUTABLE017) as int)+1
set @VENID=(select COLUMN05 from PUTABLE005 where COLUMN01=@COLUMN13)

--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Intiated for PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +', 1000 '+  cast(GETDATE() as nvarchar(250))+','+ isnull(@COLUMN13,'')+', BILL ,'+  cast(@VENID as nvarchar(250))+', 1000 ,'+ isnull( @COLUMN12,'')+','+  isnull(@COLUMN12,'')+','+ isnull(@COLUMNA01,'')+','+ isnull(@COLUMNA02,'')+','+ 
   isnull(@COLUMNA03,'')+','+ isnull(@COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+ isnull(@COLUMNA06,'')+','+ 			
   isnull(@COLUMNA07,'')+','+isnull(@COLUMNA08,'')+','+ isnull(@COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+ isnull(@COLUMNA11,'')+','+ isnull(@COLUMNA12,'')+','+ isnull(@COLUMNA13,'')+','+ isnull(@COLUMNB01,'')+','+ isnull(@COLUMNB02,'')+','+ isnull(@COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull(@COLUMNB05,'')+','+ isnull(@COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+ isnull(@COLUMNB08,'')+','+ isnull(@COLUMNB09,'')+','+ isnull(@COLUMNB10,'')+','+ isnull(@COLUMNB11,'')+','+ isnull(@COLUMNB12,'')+','+ isnull(@COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull(@COLUMND03,'')+','+ isnull(@COLUMND04,'')+','+isnull(@COLUMND05,'')+','+ isnull(@COLUMND06,'')+','+ isnull(@COLUMND07,'')+','+ isnull(@COLUMND08,'')+','+ isnull(@COLUMND09,'')+','+ isnull(@COLUMND10,'')+''+ 'at ')
    )
insert into PUTABLE017 
( 
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
(  --EMPHCS1578	Inventory Assert report price is taking purchase price insested price in Bill Price BY RAJ.JR
   @newID,'1000', @Date ,@COLUMN13,'BILL', @VENID,'1000',@memo,@ASSET,@ASSET,@COLUMN14,@Project,
   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN04, @Billlineref, @COLUMND07,
   @COLUMND08, @COLUMND09, @COLUMND10
)
	--EMPHCS1410	Logs Creation by srinivas	
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Intiated Values are Created in PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
    --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
begin
EXEC usp_TP_InventoryAssetUpdate @COLUMN13,@Billno,null,@COLUMN04,@COLUMN19,@Location,@lotno,@BillAvgPrice,@COLUMN14,@COLUMNA03
EXEC usp_TP_ProdIncomeUpdate @COLUMN13,@Billno,null,@COLUMN04,@COLUMN19,@Location,@lotno,@BillAvgPrice,@COLUMN14,@COLUMNA03
end
END
end
insert into PUTABLE011 
( 
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05, COLUMN07,COLUMN08,COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,   COLUMN19,  COLUMN20,  COLUMN21,  COLUMN22,  COLUMN23,COLUMNA02,COLUMNA03
)
values
(  
   @ID1,  'Bill',  @COLUMN04,   @COLUMN05  ,@COLUMN07 ,@COLUMN09 , '3','3', @COLUMN11,  @COLUMN12,@ID1,  @COLUMN14,  @COLUMN15,  @COLUMN16 ,@COLUMN17,@COLUMNA02,@COLUMNA03
)  
declare @PID int,@BIID int,@BINO nvarchar(250),@BillQty decimal(18,2),@PBillQty decimal(18,2),@IQty decimal(18,2),@IRQty decimal(18,2),@PIQty decimal(18,2),@PBQty decimal(18,2)
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
,@dueDT date,@tmpnewID int ,@billID nvarchar(250),@Vid nvarchar(250)=null,@internalid nvarchar(250)=null,@Taxappliedon nvarchar(250)=null,@ExciseTotTax decimal(18,2)=null,@TotTaxAmt decimal(18,2)
--EMPHCS805 Tax register is not working correctly. Bill contains 14.5 with one record and 2% with two lines , tax register is getting saved three records with 14.5 and 2 records with 2% done by 26/7/2015
, @Tax int,@TaxAG int,@TAXRATE decimal(18,2) ,@DT DATE , @headerTax int,@headerTaxAG int , @headerTAXRATE DECIMAL(18,2),@TaxNo int 
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @internalid= (select max(column01) from PUTABLE005 where COLUMN01=@COLUMN13)
 --set @Tax= (select COLUMN18 from PUTABLE006 where COLUMN02=@COLUMN02)
 ----EMPHCS851 Invoice - Header level tax is not updating in Tax Register,also check in bill BY Raj.Jr 6/8/2015
  --set @headerTax= (select COLUMN23 from PUTABLE005 where COLUMN01=@internalid)
  --set @Taxappliedon= (select COLUMN19 from MATABLE013 where COLUMN02=@headerTax)
  -- set @headerTaxAG= (select isnull(column16,0) from MATABLE013 where COLUMN02=@headerTax)
  --set @headerTAXRATE= (select isnull(column17,0) from MATABLE013 where COLUMN02=@headerTax)
 set @DT= (select COLUMN08 from PUTABLE005 where COLUMN01=@internalid)
 set @venID= (select COLUMN05 from PUTABLE005 where COLUMN01=@internalid)
 set @MEMO= (select COLUMN12 from PUTABLE005 where COLUMN01=@internalid)
-- set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
--set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
-- set @TAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
-- set @TaxNo=(select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
SET @PID=(SELECT COLUMN06 FROM PUTABLE005 WHERE COLUMN01=@COLUMN13)
if(@PID!='')
begin
--EMPHCS855 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in bill
--EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
if(isnull(@COLUMN34,0)>0)
begin
SET @PBillQty=(SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN02=@COLUMN34)
UPDATE PUTABLE002 SET   COLUMN13=(round(CAST(@PBillQty AS decimal(18,7)),@DecimalPositions)+ round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)) where  COLUMNA13=0 and COLUMN02=@COLUMN34
end
else 
begin
SET @PBillQty=isnull((SELECT (COLUMN13) FROM PUTABLE002 WHERE COLUMNA13=0 and  COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) and COLUMN03=@COLUMN04 and isnull(COLUMN26,0)=isnull(@COLUMN19,0) and isnull(COLUMN17,0)=isnull(@COLUMN27,0)),0)
UPDATE PUTABLE002 SET   COLUMN13=(round(CAST(@PBillQty AS decimal(18,7)),@DecimalPositions)+ round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)) where COLUMNA13=0 and COLUMN03=@COLUMN04  and isnull(COLUMN26,0)=isnull(@COLUMN19,0) and isnull(COLUMN17,0)=isnull(@COLUMN27,0) and COLUMN19=(select COLUMN01 from PUTABLE001 where COLUMN02=  (select COLUMN06 from PUTABLE005 where COLUMN01= @COLUMN13))
end
UPDATE PUTABLE013 SET COLUMN10=(round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)+ round(CAST(@PBillQty AS decimal(18,7)),@DecimalPositions))   WHERE COLUMN14=@COLUMN04 and COLUMN02 = (select COLUMN01 from PUTABLE001 where COLUMN02= @PID)
SET @BillQty=isnull((SELECT sum(COLUMN13) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID)),0)
SET @IRQty=isnull((SELECT sum(COLUMN12) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID)),0)
SET @IQty=isnull((SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) ),0)
IF(@BillQty=(@IQty))
begin
if(@IRQty=@BillQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
end
else if(@IRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=13)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=14)
end
end
ELSE
BEGIN
if(@IQty=@IRQty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=41)
end
else if(@IRQty=0)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=11)
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=06)
end
end
UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=13) where COLUMN01=@COLUMN13
UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (select COLUMN06 from PUTABLE005 where COLUMN01= @COLUMN13))
--EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
declare @billedqty decimal(18,2),@receiptqty decimal(18,2),@AID int
set @billedqty=(select sum(isnull(COLUMN09,0)) from PUTABLE006 where COLUMN03=@COLUMN03 and COLUMNA13=0)
set @receiptqty=(select sum(isnull(COLUMN08,0)) from PUTABLE004 where columna13=0 and  COLUMN12 in(select COLUMN01 from  PUTABLE003 where COLUMN02 in(@COLUMN03) ))
if(@billedqty=@receiptqty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=83) where COLUMN02=@COLUMN03
end
else if(@billedqty<@receiptqty)
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=82) where COLUMN02=@COLUMN03
end
end
else
begin
set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=13)
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
set @BIID=(select max(column01) from PUTABLE005 where COLUMN01=@COLUMN13)
set @venID=(select COLUMN05 from PUTABLE005 where column01=@BIID)
set @BINO=(select COLUMN04 from PUTABLE005 where column01=@BIID)
set @dueDT = (select COLUMN11  from PUTABLE005 where COLUMN01=@COLUMN13)
set @MEMO= (select COLUMN12 from PUTABLE005 where COLUMN01=@COLUMN13)
UPDATE PUTABLE005 set COLUMN13=@Result where COLUMN01=@COLUMN13
insert into PUTABLE013 
(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN06,  COLUMN08,COLUMN09,COLUMN10,   COLUMN14,  COLUMN15,COLUMNA02,COLUMNA03)
values
(@BIID,  'BILL',  @BINO,  @venID,   @COLUMN09,@COLUMN09,@COLUMN09,      @COLUMN04,  @Date,@COLUMNA02,@COLUMNA03)
end 
set @Quotation=(select COLUMN18 from PUTABLE005 where COLUMN01=@COLUMN13)
if(@Quotation like 'PQ%')
begin 
update  putable001 set COLUMN16=(select column04 from CONTABLE025 where COLUMN02=66) where column04=@Quotation
end
--EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
declare @ldisamt decimal(18,2),@ltaxamt decimal(18,2),@disval decimal(18,2),@SBAL decimal(18,2)
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
set @ldisamt=(isnull(@COLUMN12,0))
--IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
--begin
--IF(ISNULL(@COLUMN31,0)=22556)
--BEGIN SET @ldisamt=(CAST(@COLUMN12 AS decimal(18,2))-(CAST(@COLUMN12 AS decimal(18,2))*CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100)) END
--else
--BEGIN SET @ldisamt=(CAST(@COLUMN12 AS decimal(18,2))-CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
--end	


IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
begin
IF(ISNULL(@COLUMN31,0)=22556)
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
BEGIN SET @disval=(((round(CAST(ISNULL(@COLUMN09,0) AS decimal(18,7)),@DecimalPositions)*CAST(ISNULL(@COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100))) END
else
BEGIN SET @disval=(cast(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1118 and COLUMNA03=@COLUMNA03)
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @Billlineref,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22405', @COLUMN11 = '1118',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @disval,       @COLUMN15 = '0',       @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Purchase Discount Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(isnull(@AID,'') as nvarchar(250))+',' + cast( ISNULL(@DT,'')as nvarchar(250))+','+',BILL,'+ cast(ISNULL(@BIID,'')as nvarchar(250))+','+cast( ISNULL(@venID,'')as nvarchar(250))+','+ cast( ISNULL(null,'')as nvarchar(250))+','+ '1118'+','+ cast( ISNULL(@disval,'')as nvarchar(250))+','+cast(ISNULL((@SBAL+@disval),'')as nvarchar(250))+','+ cast( ISNULL(@COLUMNA02,'')as nvarchar(250))+ ','+cast(ISNULL(@COLUMNA03,'')as nvarchar(250))+ ','+cast(ISNULL(@project,'')as nvarchar(250))+ ' ')
					)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12, COLUMN13)
			values(@AID,@DT,'BILL',@BIID,@venID,@MEMO,'1118',@disval,@SBAL+cast(isnull(@disval,0) as decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project,@COLUMN04)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Purchase Discount Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''))
end

		--EMPHCS1167 Service items calculation by srinivas 22/09/2015
		if(@chk=0)
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
		    set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
			set @Amtin=(select COLUMN43 from PUTABLE005 where COLUMN01=@COLUMN13)
			declare @ActAmt decimal(18,2)
			 SET @ActAmt=(@COLUMN12) 			
			IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
			begin
			IF(ISNULL(@COLUMN31,0)=22556)
			BEGIN SET @disval=(((round(CAST(ISNULL(@COLUMN09,0) AS decimal(18,7)),@DecimalPositions)*CAST(ISNULL(@COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100))) END
			else
			BEGIN SET @disval=(cast(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
            SET @ActAmt=((cast(isnull(@ActAmt,0) as decimal(18,2)))+cast(isnull(@disval,0) as decimal(18,2)))
			END
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Discount  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			DECLARE @ACID int
			DECLARE @ACTYPE int
					SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1078))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22344', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ActAmt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			values(@AID,'BILL',@Date,@internalid,@venID,@ActAmt,NULL,@BINO,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22406', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN12,       @COLUMN15 = '0',       @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12, COLUMN13)
			values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@ACID,CAST(isnull(@COLUMN12,0) AS decimal(18,2)),CAST(isnull(@COLUMN12,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project,@COLUMN04)
		    END
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Discount  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		end
		else if(@chk=1)
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
		    set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
			set @Amtin=(select COLUMN43 from PUTABLE005 where COLUMN01=@COLUMN13)
			 SET @ActAmt=(@COLUMN12) 			
			IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
			begin
			IF(ISNULL(@COLUMN31,0)=22556)
			BEGIN SET @disval=(((round(CAST(ISNULL(@COLUMN09,0) AS decimal(18,7)),@DecimalPositions)*CAST(ISNULL(@COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100))) END
			else
			BEGIN SET @disval=(cast(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
            SET @ActAmt=((cast(isnull(@ActAmt,0) as decimal(18,2)))+cast(isnull(@disval,0) as decimal(18,2)))
			END
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHASE  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22344', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ActAmt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ActAmt,NULL,@BINO,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22406', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN12,       @COLUMN15 = '0',       @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12, COLUMN13)
			values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@ACID,CAST(isnull(@COLUMN12,0) AS decimal(18,2)),CAST(isnull(@COLUMN12,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project,@COLUMN04)
		    END
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHAGE  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		end
declare @TaxString nvarchar(250)=NULL,@TAXRATEM DECIMAL(18,2)
set @internalid= (select max(column01) from PUTABLE005 where COLUMN01=@COLUMN13)
declare @ReverseCharged bit,@ReverseItemid int,@CharAcc nvarchar(250),@CharAcc1 nvarchar(250)
set @ReverseItemid = (select COLUMN04 from PUTABLE006 where COLUMN02=@COLUMN02)
set @ReverseCharged = (select isnull(COLUMN58,0) from PUTABLE005 where COLUMN01=@internalid)
set @Tax= (select COLUMN18 from PUTABLE006 where COLUMN02=@COLUMN02)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
DECLARE @cnt INT = 0,@vochercnt INT = 1,@AMOUNTM decimal(18,2)=@COLUMN12,@TTYPE INT = 0,@HTTYPE INT = 0;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN18,0) from PUTABLE006 where COLUMN02=@COLUMN02
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end

	set @headerTax= (select COLUMN23 from PUTABLE005 where COLUMN01=@internalid)
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	select @Taxappliedon=COLUMN19,@headerTaxAG=column16,@headerTAXRATE=COLUMN17,@HTTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@headerTax
        set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02) 
		if((@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0') and @TTYPE!=22344 and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		begin
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))

exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = '22402', @COLUMN11 = @CharAcc1,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@DT,@MEMO,@venID,@ltaxamt,0,  @Billno,@CharAcc1,@project,@COLUMNA02, @COLUMNA03)		
		--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10, COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		--values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@CharAcc1,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Billno,@project)
end
end
		else if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and @TTYPE!=22344 and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		BEGIN
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1141 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
	set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = '22402', @COLUMN11 = 1141,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	values(ISNULL(@Vid,1000),'BILL',@DT,@MEMO,@venID,@ltaxamt,0,  @Billno,1141,@project,@COLUMNA02, @COLUMNA03)
	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1135 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = '22383', @COLUMN11 = '1135',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
	values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,1135,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Billno,@project)
END
		ELSE if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG!=22400 AND @TaxAG!=23050 ) and (@TTYPE=0 OR @TTYPE=22383))
	    BEGIN
		--EMPHCS729	Tax Amount Calculation In Transaction Forms for decimal values by srinivas 7/21/2015
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		declare @TaxColumn17 nvarchar(250),@TTYPE1 int
		--EMPHCS991	COA - bill and invoice tax details are showing in both input and output account  BY RAJ.Jr 28/8/2015
	--EMPHCS1147 Deleted tax amount in bill ,appearing in chart of accounts i.e., not deleting by srinivas 15/09/2015
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		select top 1 @CharAcc = column02,@TTYPE1=column07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
		--EMPHCS1410	Logs Creation by srinivas			
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 line Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',INVOICE,'+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@Tax as nvarchar(250))+','+  cast(@TaxAG as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),'') +  ' at ')
    )	
    set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Tax,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Created In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		END
		ELSE if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG!=22400 AND @TaxAG!=23050 ) and @TTYPE=22344)
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		select top 1 @TaxColumn17=COLUMN02,@TTYPE1=column07 from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03
		set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE036)+1)
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
						values(@Vid,'BILL',@DT,@internalid,@venID,@ltaxamt,NULL,@BINO,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0,@project)
		END
		ELSE if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG!=22400 AND @TaxAG!=23050 )  and @TTYPE=22402)
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		select top 1 @TaxColumn17=COLUMN02,@TTYPE1=column07 from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03
		set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)
		insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
		values(ISNULL(@Vid,1000),'BILL',@DT,@MEMO,@venID,@ltaxamt,0,  @Billno,@TaxColumn17,@project,@COLUMNA02, @COLUMNA03)
		END

		--set @CharAcc6 = (select COLUMN04 from FITABLE001 where COLUMN02=1140)
		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
		DECLARE @DISCTYPE INT,@DISCAMNT DECIMAL(18,2) 
		--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		SET @DISCAMNT=(CAST(@COLUMN12 AS decimal(18,2)))
		--IF(ISNULL(@COLUMN31,0)=22556)
		--BEGIN SET @DISCAMNT=(CAST(@COLUMN12 AS decimal(18,2))-(CAST(@COLUMN12 AS decimal(18,2))*CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100)) END
		--ELSE 
		--BEGIN SET @DISCAMNT=(CAST(@COLUMN12 AS decimal(18,2))-CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND (@TaxAG=22400 OR @TaxAG=23050))
		begin
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+cast(@TAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			set @TotTaxAmt=(isnull((@DISCAMNT)*((@TAXRATE)*(0.01)),0))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @TotTaxAmt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((@TAXRATE)*(0.01)),0) AS decimal(18,2)),NULL,@BINO,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0,@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		end
		
		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
		set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Tax,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
		declare @KKC bit,@SBC bit,@KKCRate decimal(18,2),@SBCRate decimal(18,2),@Servicename nvarchar(250)
		set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@Tax)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@Tax)
		if(cast(@Date as date)>'11/14/2015')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			set @TotTaxAmt=(CAST(isnull((@DISCAMNT)*((@SBCRate)*(0.01)),0) AS decimal(18,2)))
			set @TaxColumn17=('INPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
				exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @TotTaxAmt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @SBCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			--EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((@SBCRate)*(0.01)),0) AS decimal(18,2)),NULL,@BINO,@Servicename,@COLUMNA02 , @COLUMNA03,0,@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		if(cast(@Date as date)>'05/31/2016')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1131'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			set @TotTaxAmt=isnull((@DISCAMNT)*((@KKCRate)*(0.01)),0)
			set @TaxColumn17=('INPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    --insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			----EMPHCS898 rajasekhar reddy patakota 10/8/2015 in sales of product income table should store by considering discount
			--values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((0.5)*(0.01)),0) AS decimal(18,2)),NULL,@BINO,1131,@COLUMNA02 , @COLUMNA03,0)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @TotTaxAmt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @KKCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@Servicename,NULL,CAST(isnull((@DISCAMNT)*((@KKCRate)*(0.01)),0) AS decimal(18,2)),@COLUMN04,@COLUMN12,@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,@BINO,@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end
		
		declare @EDCESS bit,@SHECESS bit,@EDCRate decimal(18,2),@SHECRate decimal(18,2),@Excisename nvarchar(250),@lexcisetaxamt decimal(18,2)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ))
		BEGIN
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@Tax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@Tax)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	    set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN04,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN04,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		END

		if(@Taxappliedon='Tax')
	    begin
		set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
		
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0  and (@headerTaxAG!=22400 AND @headerTaxAG!=23050))
		BEGIN
		--EMPHCS729	Tax Amount Calculation In Transaction Forms for decimal values by srinivas 7/21/2015
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
		--EMPHCS991	COA - bill and invoice tax details are showing in both input and output account  BY RAJ.Jr 28/8/2015
	--EMPHCS1147 Deleted tax amount in bill ,appearing in chart of accounts i.e., not deleting by srinivas 15/09/2015
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+CAST(@headerTAXRATE AS NVARCHAR(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		--EMPHCS1410	Logs Creation by srinivas
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',BILL,'+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@headerTax as nvarchar(250))+','+  cast(@headerTaxAG as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+    isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),'') +  ' at ')
    )
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @headerTAXRATE, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@headerTax,@headerTaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@headerTAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Created In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		END

		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND (@headerTaxAG=22400 OR @headerTaxAG=23050))
		begin
			set @CharAcc=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+CAST(@headerTAXRATE AS NVARCHAR(250))+'%')
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+cast(@headerTAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @headerTAXRATE, @COLUMN18 = @headerTaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ltaxamt,NULL,@BINO,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0,@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

		end

		--EMPHCS1457	SWACHA BHARATH CESS AND INPUT CST CHANGES BY SRINIVAS 26/12/2015
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		begin
	    set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		if(cast(@Date as date)>'11/14/2015')
		begin
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
		    set @TaxColumn17=('INPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
				exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @SBCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ltaxamt,NULL,@BINO,@Servicename,@COLUMNA02 , @COLUMNA03,0,@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		if(cast(@Date as date)>'05/31/2016')
		begin
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
			set @TaxColumn17=('INPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @KKCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1131'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			--insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			--values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((0.5)*(0.01)),0) AS decimal(18,2)),NULL,@BINO,1131,@COLUMNA02 , @COLUMNA03,0)
			insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,'BILL',@Date,@internalid,@venID,@MEMO,@Servicename,NULL,@ltaxamt,@COLUMN04,@COLUMN12,@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,@BINO,@project)
		--EMPHCS1410	Logs Creation by srinivas
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end
				
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ))
		BEGIN
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@headerTax)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		set @ExciseTotTax=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ExciseTotTax,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@headerTaxAG,@ExciseTotTax,@COLUMN04,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		set @ltaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@headerTaxAG,@ltaxamt,@COLUMN04,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		END
END
		set @ReturnValue = 1
end
else 
begin
return 0
end
END

IF @Direction = 'Select'
BEGIN
select * from PUTABLE006
END 

IF @Direction = 'Update'
BEGIN
--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
set @COLUMNA12=(1)
set @COLUMNA13=(0)
set @COLUMN33 = (isnull(@COLUMN33,0))
set @COLUMN14=(select COLUMN15 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMN15=(select COLUMN16 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMN16=(select COLUMN17 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMN17=(select COLUMN18 from PUTABLE005 where COLUMN01=@COLUMN13)
set @Date=(select COLUMN08 from PUTABLE005 where COLUMN01=@COLUMN13)
set @memo=(select COLUMN12 from PUTABLE005 where COLUMN01=@COLUMN13)
set @BINO=(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13)
set @Project=(select COLUMN26 from PUTABLE005 where COLUMN01=@COLUMN13)
set @COLUMNA02=( CASE WHEN (@COLUMN13!= '' and @COLUMN13 is not null and @COLUMN13!= '0' ) THEN (select COLUMN15 from PUTABLE005 
where COLUMN01=@COLUMN13) else @COLUMNA02  END )
if exists(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
begin
set @DecimalPositions=(select isnull(COLUMN05,0) from CONTABLE031 where COLUMNA03=@COLUMNA03 and COLUMN03='No of Decimal Places required for qty feild' and isnull(column04,0)=1 and isnull(columna13,0)=0)
end
else
begin set @DecimalPositions=(2) end
set @COLUMN09=(round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions))
--EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation in Bill
if(@PID!='')
	  begin
	  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
      if(isnull(@COLUMN34,0)>0)
		begin
		set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where COLUMNA13=0 and COLUMN34=@COLUMN34)
		end
		else 
		begin
		set @COLUMN10=(select isnull(COLUMN10,0) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@COLUMN03 and COLUMN02=@COLUMN02)
		end
set @COLUMN10=(round(CAST(@COLUMN10 AS decimal(18,7)),@DecimalPositions)- round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)); 
end
--EMPHCS1167 Service items calculation by srinivas 22/09/2015
			set @chk=(select column48 from matable007 where column02=@COLUMN04)
if not exists( SELECT 1 FROM PUTABLE006 WHERE COLUMN02=@COLUMN02 and COLUMN13=@COLUMN13)
begin 
--EMPHCS1770 rajasekhar reddy patakota 27/06/2016 Identity Column setup in sales and purchase transactions
select @COLUMN02=NEXT VALUE FOR DBO.PUTABLE006_SequenceNo
--EMPHCS1410 LOGS CREATION BY SRINIVAS
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Line Values are Intiated for PUTABLE006 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+isnull(  @COLUMN11,'')+','+
   isnull(@COLUMN12 ,'')+','+isnull(  @COLUMN13,'')+','+isnull(  @COLUMN14,'')+','+isnull( @COLUMN15,'')+','+isnull(  @COLUMN16,'')+','+isnull(  @COLUMN17,'')+','+isnull(  @COLUMN18,'')+','+isnull(  @COLUMN19,'')+','+isnull(  @COLUMN20,'')+','+isnull(  @COLUMN21,'')+','+ 
   isnull(@COLUMN22 ,'')+','+isnull(  @COLUMN23,'')+','+isnull(  @COLUMN24,'')+','+isnull( @COLUMN25,'')+','+isnull(  @COLUMN26,'')+','+isnull(  @COLUMN27,'')+','+isnull(  @COLUMN28,'')+','+isnull(  @COLUMN29,'')+','+isnull(  @COLUMN30,'')+','+isnull(  @COLUMN31,'')+','+
   isnull(@COLUMN32 ,'')+','+isnull( @COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+ 				  
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ 'at ')
    )

insert into PUTABLE006 
(
   COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN09,  COLUMN10,  COLUMN11,
   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
   COLUMN12,  COLUMN13,  COLUMN14,  COLUMN15,  COLUMN16,  COLUMN17,  COLUMN18,  COLUMN19,  COLUMN20,  
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   COLUMN21,  COLUMN22,  COLUMN23,  COLUMN24,  COLUMN25,  COLUMN26,  COLUMN27,  COLUMN28,  COLUMN29,  COLUMN30,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN31,  COLUMN32,  COLUMN33,  COLUMN34,  COLUMN35,  COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, 
   COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02,
   COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12,
   COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN09,  @COLUMN10,  @COLUMN11, @COLUMN12,
   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN19,  @COLUMN09,  
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,  @COLUMN32,
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, 
   @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11,@COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, 
   @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, 
   @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
--EMPHCS1410 LOGS CREATION BY SRINIVAS
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Line Values are Created in PUTABLE006 Table '+'' + CHAR(13)+CHAR(10) + '')
    )

--EMPHCS1118 rajasekhar reddy 13/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
--set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
--set @Billno=(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13)
--set @Billlineref=(select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
--if(@uomselection=1 or @uomselection='1' or @uomselection='True')
--begin
--if(@UOMData is null)
--begin
--update FITABLE038 set COLUMNA13=0 where COLUMN04=@Billno and COLUMN06=@COLUMN04 and COLUMN05=@Billlineref and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
--end
--else
--begin
--EXEC usp_PUR_TP_InsertUOM @UOMData,'Bill',@Billno,@Billlineref,@COLUMN04,@COLUMN14,@COLUMNA03
--end
--end
end
else
begin
	  UPDATE PUTABLE006 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,
   --COLUMN07=@COLUMN07, 
   COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
   COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,  
   COLUMN17=@COLUMN17,    COLUMN18=@COLUMN18,    COLUMN19=@COLUMN19,    COLUMN20=@COLUMN20,
   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
   --EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
   COLUMN21=@COLUMN19,    COLUMN22=@COLUMN09,    COLUMN23=@COLUMN23,    COLUMN24=@COLUMN24,    COLUMN25=@COLUMN25,  
   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
   COLUMN26=@COLUMN26,    COLUMN27=@COLUMN27,    COLUMN28=@COLUMN28,    COLUMN29=@COLUMN29,    COLUMN30=@COLUMN30,
   --EMPHCS1427 rajasekhar reddy patakota 17/12/2015 Adding Discount and type of discount Field In Bill
   --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
   COLUMN31=@COLUMN31,    COLUMN32=@COLUMN32,    COLUMN33=@COLUMN33,    COLUMN34=@COLUMN34,    COLUMN35=@COLUMN35,
   COLUMNA01=@COLUMNA01,  COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05,  
    COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10, 
   COLUMNA11=@COLUMNA11,  COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02, 
   COLUMNB03=@COLUMNB03,  COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07, 
   COLUMNB08=@COLUMNB08,  COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12, 
   COLUMND01=@COLUMND01,  COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05, 
   COLUMND06=@COLUMND06,  COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
   --EMPHCS1410 LOGS CREATION BY SRINIVAS
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Bill Line Values are Updated for PUTABLE006 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
      end
if exists(SELECT COLUMN04 FROM MATABLE021 where COLUMN12 in(@COLUMN02) and COLUMN05 in(@COLUMN04) and COLUMNA03=@COLUMNA03)
begin
UPDATE MATABLE021 SET COLUMNA13=0 where COLUMN12 in(@COLUMN02) and COLUMN05 in(@COLUMN04) and COLUMNA03=@COLUMNA03
end
		 
      --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
set @uomselection=(select COLUMN65 from MATABLE007 where COLUMN02=@COLUMN04)
set @Billno=(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13)
set @Billlineref=(select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
set @Location = (select isnull(COLUMN29,0) from  PUTABLE005 WHERE COLUMN01=@COLUMN13)
set @Location = (case when (cast(isnull(@COLUMN35,'') as nvarchar(250))!='' and @COLUMN35!=0) then @COLUMN35 else @Location end)
set @Location=(case when @Location='' then 0 when isnull(@Location,0)=0 then 0  else @Location end)
set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
set @lotno=(case when @column27='' then 0 when isnull(@column27,0)=0 then 0  else @column27 end)
set @DT= (select COLUMN08 from PUTABLE005 where COLUMN01=@COLUMN13)
set @venID=(select COLUMN05 from PUTABLE005 where column01=@COLUMN13)
set @MEMO= (select COLUMN12 from PUTABLE005 where COLUMN01=@COLUMN13)
set @project=(select COLUMN26 from PUTABLE005 where COLUMN01=@COLUMN13)
set @ProjectID = (select isnull(COLUMN26,0) from  PUTABLE005 WHERE COLUMN01=@COLUMN13)
set @ProjectID=(case when @ProjectID='' then 0 when isnull(@ProjectID,0)=0 then 0  else @ProjectID end)
IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
begin
IF(ISNULL(@COLUMN31,0)=22556)
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
BEGIN SET @disval=(((round(CAST(ISNULL(@COLUMN09,0) AS decimal(18,7)),@DecimalPositions)*CAST(ISNULL(@COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100))) END
else
BEGIN SET @disval=(cast(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @Billlineref,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22405', @COLUMN11 = '1118',
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @disval,       @COLUMN15 = '0',       @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @SBAL=(select ISNULL(MAX(COLUMN10),0) from FITABLE025 WHERE COLUMN08=1118 and COLUMNA03=@COLUMNA03) 
set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Purchase Discount Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''+
						'The Values are '+
						cast(isnull(@AID,'') as nvarchar(250))+',' + cast( ISNULL(@DT,'')as nvarchar(250))+','+',BILL,'+ cast(ISNULL(@COLUMN13,'')as nvarchar(250))+','+cast( ISNULL(@venID,'')as nvarchar(250))+','+ cast( ISNULL(null,'')as nvarchar(250))+','+ '1118'+','+ cast( ISNULL(@disval,'')as nvarchar(250))+','+cast(ISNULL((@SBAL+@disval),'')as nvarchar(250))+','+ cast( ISNULL(@COLUMNA02,'')as nvarchar(250))+ ','+cast(ISNULL(@COLUMNA03,'')as nvarchar(250))+ ','+cast(ISNULL(@project,'')as nvarchar(250))+ ' ')
					)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12, COLUMN13)
			values(@AID,@DT,'BILL',@COLUMN13,@venID,@MEMO,'1118',@disval,@SBAL+cast(isnull(@disval,0) as decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project,@COLUMN04)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Purchase Discount Income  Values are Intiated for FITABLE025 Table '+'' + CHAR(13)+CHAR(10) + ''))
end

if(@uomselection=1 or @uomselection='1' or @uomselection='True')
begin
if(@UOMData is null)
begin
update FITABLE038 set COLUMNA13=0 where COLUMN04=@Billno and COLUMN06=@COLUMN04 and COLUMN05=@Billlineref and COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
end
else
begin
EXEC usp_PUR_TP_InsertUOM @UOMData,'Bill',@Billno,@Billlineref,@COLUMN04,@COLUMN14,@COLUMNA03
end
end
	  set @PID=(select COLUMN06  from  PUTABLE005 where COLUMN01 in(@COLUMN13))
	  --EMPHCS781 rajasekhar reddy patakota 28/7/2015 Remaining Qty Calculation in Bill
	  if(@PID!='')
	  begin
	  --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
      if(isnull(@COLUMN34,0)>0)
		begin
		set @COLUMN08=(round(cast(( select sum(isnull(COLUMN09,0)) from PUTABLE006 WHERE COLUMNA13=0 and COLUMN34=@COLUMN34 and COLUMN03 in(select COLUMN02 from PUTABLE003 where COLUMN06=@PID) and COLUMN04=@COLUMN04 and COLUMNA13=0  ) as decimal(18,7)),@DecimalPositions))
	    set @COLUMN10=(round(cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where COLUMNA13=0 and COLUMN26=@COLUMN34 and COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN02=@COLUMN03) and COLUMN03=@COLUMN04 and COLUMNA13=0) as decimal(18,7)),@DecimalPositions)-(round(cast(( select sum(isnull(COLUMN09,0)) from PUTABLE006 WHERE COLUMN03 in(select COLUMN02 from PUTABLE003 where COLUMN02=@COLUMN03) and COLUMN34=@COLUMN34 and COLUMN04=@COLUMN04 and COLUMNA13=0  ) as decimal(18,7)),@DecimalPositions)))
	    end			   
		else 		   
		begin		   
		set @COLUMN08=(round(cast(( select sum(isnull(COLUMN09,0)) from PUTABLE006 WHERE COLUMN03 in(select COLUMN02 from PUTABLE003 where COLUMN06=@PID) and COLUMN04=@COLUMN04 and COLUMNA13=0  ) as decimal(18,7)),@DecimalPositions))
	    set @COLUMN10=(round(cast((select sum(isnull(COLUMN08,0)) from PUTABLE004 where COLUMN12 in(select COLUMN01 from PUTABLE003 where COLUMN02=@COLUMN03) and COLUMN03=@COLUMN04 and COLUMNA13=0) as decimal(18,7)),@DecimalPositions)-(round(cast(( select sum(isnull(COLUMN09,0)) from PUTABLE006 WHERE COLUMN03 in(select COLUMN02 from PUTABLE003 where COLUMN02=@COLUMN03) and COLUMN04=@COLUMN04 and COLUMNA13=0  ) as decimal(18,7)),@DecimalPositions)))
	  end
	  end
	  else
	  begin
	  set @COLUMN08=(round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions))
	  --set @COLUMN08=(select max(isnull(COLUMN08,0)) from PUTABLE006 where  COLUMN04=@COLUMN04 and COLUMN03=@COLUMN03 and COLUMN02 in(@COLUMN02))
	  end
	  update PUTABLE006 set COLUMN08=@COLUMN08,COLUMN10=@COLUMN10 where COLUMN02=@COLUMN02
      
	  
	  if(@PID<>'')
      begin
      --EMPHCS855 rajasekhar reddy patakota 05/08/2015 Deleted row condition checking in bill
      --EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
      if(isnull(@COLUMN34,0)>0)
	  begin
	  SET @PBillQty=(SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN02=@COLUMN34)
	  UPDATE PUTABLE002 SET   COLUMN13=(round(CAST(@PBillQty AS decimal(18,7)),@DecimalPositions)+ round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)) where  COLUMNA13=0 and COLUMN02=@COLUMN34
	  end
	  else 
	  begin
      SET @PBillQty=isnull((SELECT isnull(COLUMN13,0) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) and COLUMN03=@COLUMN04  and isnull(COLUMN26,0)=isnull(@COLUMN19,0) and isnull(COLUMN17,0)=isnull(@COLUMN27,0)),0)
      UPDATE PUTABLE002 SET   COLUMN13=(round(CAST(@PBillQty AS decimal(18,7)),@DecimalPositions)+ round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)) where COLUMN03=@COLUMN04  and isnull(COLUMN26,0)=isnull(@COLUMN19,0) and isnull(COLUMN17,0)=isnull(@COLUMN27,0) and COLUMN19=(select COLUMN01 from PUTABLE001 where COLUMN02=  (@PID))
      end
	  UPDATE PUTABLE013 SET COLUMN10=(round(CAST(@COLUMN09 AS decimal(18,7)),@DecimalPositions)+ round(CAST(@PBillQty AS decimal(18,7)),@DecimalPositions))   WHERE COLUMN14=@COLUMN04 and COLUMN02 = (select COLUMN01 from PUTABLE001 where COLUMN02= @PID)
      SET @BillQty=isnull((SELECT sum(COLUMN13) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID)),0)
      SET @IRQty=isnull((SELECT sum(COLUMN12) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID)),0)
      SET @IQty=isnull((SELECT sum(COLUMN07) FROM PUTABLE002 WHERE COLUMNA13=0 and COLUMN19 in (select COLUMN01 from PUTABLE001 where COLUMN02=@PID) ),0)
      IF(@BillQty=(@IQty))
      begin
      if(@IRQty=@BillQty)
      begin
      set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
      end
      else if(@IRQty=0)
      begin
      set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=13)
      end
      else
      begin
      set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=14)
      end
      end
      ELSE
      BEGIN
      if(@IQty=@IRQty)
      begin
      set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=41)
      end
      else if(@IRQty=0)
      begin
      set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=11)
      end
      else
      begin
      set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=06)
      end
      end
      UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=13) where COLUMN01=@COLUMN13
      UPDATE PUTABLE001 set COLUMN16=@Result where COLUMN01=(select COLUMN01 from PUTABLE001 where COLUMN02= (@PID))
	  --EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Billed Quantity Calculation
	  set @billedqty=(select sum(isnull(COLUMN09,0)) from PUTABLE006 where COLUMN03=@COLUMN03 and COLUMNA13=0)
	  set @receiptqty=(select sum(isnull(COLUMN08,0)) from PUTABLE004 where columna13=0 and  COLUMN12 in(select COLUMN01 from  PUTABLE003 where COLUMN02 in(@COLUMN03)))
	  if(@billedqty=@receiptqty)
	  begin
	  set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
	  UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=83) where COLUMN02=@COLUMN03
	  end
	  else if(@billedqty<@receiptqty)
	  begin
	  set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=10)
	  UPDATE PUTABLE003 set COLUMN18=(select COLUMN04 from CONTABLE025 where COLUMN02=82) where COLUMN02=@COLUMN03
	  end
      end
	  else 
	  begin
	  --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
	  
			set @TotalBillQty=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN09,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0 
			--EMPHCS1836 rajasekhar reddy patakota 21/11/2016 Bacth level Pricing setup in all transactions
			where h.COLUMN08>=@FiscalYearStartDt and l.COLUMN06=@COLUMN06  AND l.COLUMN04=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT)
			union all
			select isnull(sum(isnull(l.COLUMN04,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
			union all
			select isnull(sum(isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0) Total from FITABLE015 l  inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
			where h.COLUMN05>=@FiscalYearStartDt  and l.COLUMN04=@COLUMN06   AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT))T)
			set @TotalBillAmt=(SELECT SUM(ISNULL(TOTAL,0)) FROM ((select sum(isnull(l.COLUMN12,0)) Total from PUTABLE006 l inner join PUTABLE005 h on h.COLUMN01=l.COLUMN13 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN08>=@FiscalYearStartDt and l.COLUMN06=@COLUMN06 AND l.COLUMN04=@COLUMN04 and iif(l.COLUMN03='',0,isnull(l.COLUMN03,0))=0 AND isnull(h.COLUMN15,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN27,0)=@lotno AND (case when (cast(l.COLUMN35 as nvarchar(250))!='' and isnull(l.COLUMN35,0)!=0) then l.COLUMN35 else iif(h.COLUMN29='',0,isnull(h.COLUMN29,0)) end)=@Location and isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT)
			union all
			select isnull(sum(isnull(l.COLUMN12,0)),0) Total from FITABLE047 l where  l.COLUMN08>=@FiscalYearStartDt and l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN19,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0)
  union all
  select isnull((isnull(l.COLUMN08,0)+isnull(l.COLUMN16,0)),0)*(isnull((isnull(l.COLUMN10,0)),0)) Total from FITABLE015 l   inner join FITABLE014 h on h.COLUMN01=l.COLUMN11 and isnull(h.COLUMNA13,0)=0 
  where h.COLUMN05>=@FiscalYearStartDt   and l.COLUMN04=@Column04 AND l.COLUMN03=@COLUMN04 AND isnull(l.COLUMN22,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMNA02,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMNA03,0)=@COLUMNA03 and isnull(l.COLUMNA13,0)=0 AND isnull(l.COLUMN23,0)=@lotno and isnull(l.COLUMNA13,0)=0  AND (case when (cast(h.COLUMN16 as nvarchar(250))!='' and isnull(h.COLUMN16,0)!='0') then h.COLUMN16 else 0 end)=@Location AND isnull(h.COLUMN26,0)=CAST(@ProjectID AS INT))T)
			if exists(select l.COLUMN08 from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@COLUMN06  AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))
			begin
			set @TotalBillQty=round(cast((@TotalBillQty+round(cast((select sum(isnull(l.COLUMN08,0)) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@COLUMN06  AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))as decimal(18,2)),@DecimalPositions))as decimal(18,2)),@DecimalPositions)
			set @TotalBillAmt=cast((@TotalBillAmt+cast((select sum(cast(isnull(iif(cast(l.COLUMN11 as nvarchar(250))='','0',l.COLUMN11),0) as decimal(18,2))) from PUTABLE004 l inner join PUTABLE003 h on h.COLUMN01=l.COLUMN12 and isnull(l.COLUMNA13,0)=0
			where h.COLUMN09>=@FiscalYearStartDt and l.COLUMN05=@COLUMN06  AND l.COLUMN03=@COLUMN04 AND isnull(h.COLUMN13,0)=isnull(@COLUMN14,0) AND isnull(l.COLUMN17,0)=isnull(@COLUMN19,0) AND isnull(l.COLUMN24,0)=@lotno AND (case when (cast(l.COLUMN28 as nvarchar(250))!='' and isnull(l.COLUMN28,0)!=0) then l.COLUMN28 else iif(h.COLUMN22='',0,isnull(h.COLUMN22,0)) end)=@Location AND isnull(h.COLUMNA13,0)=0 AND isnull(h.COLUMN21,0)=CAST(@ProjectID AS INT))as decimal(18,2)))as decimal(18,2))
			end
			if(isnull(@TotalBillQty,0)=0)
                        begin
			set @BillAvgPrice=(0)
			end
			else
			begin
			set @BillAvgPrice=(cast(isnull(@TotalBillAmt,0) as decimal(18,2))/round(cast(isnull(@TotalBillQty,0) as decimal(18,7)),@DecimalPositions))
			end
			if(@uomselection=1 or @uomselection='1' or @uomselection='True')
			begin
			set @AvgPrice= (SELECT avg(isnull(COLUMN17,0)) FROM FITABLE010 WHERE  COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19 in(SELECT COLUMN08 FROM FITABLE038 WHERE COLUMN03='Bill' and COLUMN04=@Billno and COLUMN05=@Billlineref and COLUMN06=@COLUMN04 and COLUMNA02=@COLUMN14 and COLUMNA03=@COLUMNA03) AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)
			end
			else
			begin
			set @AvgPrice=(select isnull(COLUMN17,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)
			end
			set @FinalAvgPrice=(@BillAvgPrice-@AvgPrice)
	  --EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
	   --EMPHCS1141	rajasekhar reddy 14/09/2015	Inventory not updating for Bill & Invoice
	   --EMPHCS1167 Service items calculation by srinivas 22/09/2015
	   if((@uomselection=0 or @uomselection='0' or @uomselection='' or @uomselection is null or @uomselection='False') and @chk=1)
      begin
	  --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
           --declare @Qty_On_Hand decimal(18,2),@Qty_On_Hand1 decimal(18,2),@Qty_Order decimal(18,2),@Qty_Avl decimal(18,2),@Qty_Cmtd decimal(18,2)
	   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
            set @Qty_On_Hand1=(select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)
			--EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
if exists(select COLUMN03 from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)
			--if(@Qty_On_Hand1>=0)
            begin
	    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	    --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
           set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)as decimal(18,7)),@DecimalPositions)+round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions));
           set @Qty_Cmtd=(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)- round(cast((select isnull(COLUMN05,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)as decimal(18,7)),@DecimalPositions));
           set @Qty_Order = (round(cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)as decimal(18,7)),@DecimalPositions))
           if(@Qty_Order>0 or @Qty_Order>round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions))
           Begin
           set @Qty_Order = (round(cast((select isnull(COLUMN07,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)as decimal(18,7)),@DecimalPositions)+ round(cast(@COLUMN09 as decimal(18,7)),@DecimalPositions));
           end
           else
           begin
           set @Qty_Order = 0;
           end
           set @Qty_Avl =  round(cast(isnull(@Qty_Cmtd,0) as decimal(18,7)),@DecimalPositions);
	   --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
		SET @PRATE=(ISNULL(@COLUMN11,0))
		--IF(@PRATE=0)BEGIN SET @PRATE=(@COLUMN33)END
	   --EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
	   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	   --EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
	   --EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
	   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
           UPDATE FITABLE010 SET COLUMN04 =@Qty_On_Hand,  COLUMN08=@Qty_On_Hand  WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06
	--EMPHCS1410 LOGS CREATION BY SRINIVAS
			 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Intiated Values are Created in FITABLE010 Table qty by '+cast(@Qty_On_Hand as nvarchar(20))+' for '+cast(@COLUMN04 as nvarchar(20))+''+'' + CHAR(13)+CHAR(10) + '')
    )
			set @Qty_On_Hand=(round(cast((select isnull(COLUMN04,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06)as decimal(18,7)),@DecimalPositions));
			UPDATE FITABLE010 SET COLUMN12=cast(round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)*cast(@BillAvgPrice as decimal(18,2)
) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06
				if(round(cast(@Qty_On_Hand as decimal(18,2)),@DecimalPositions)=0)
				begin
				--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
				UPDATE FITABLE010 SET COLUMN12=0 WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06
				end
				else
				begin
				UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN23,0)=@ProjectID and Column24=@COLUMN06 AND isnull(COLUMN22,0)=@lotno)as decimal(18,2))/round(cast(@Qty_On_Hand as decimal(18,7)),@DecimalPositions)) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19 AND isnull(COLUMN21,0)=@Location AND isnull(COLUMN22,0)=@lotno AND isnull(COLUMN23,0)=@ProjectID
				end
			--EMPHCS798 rajasekhar reddy patakota 30/7/2015 Total price and Average price should get fixed for inventory
           --UPDATE FITABLE010 SET COLUMN17=cast((cast((select isnull(COLUMN12,0) from FITABLE010 where COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19)as decimal(18,2))/cast(@Qty_On_Hand as decimal(18,2))) as decimal(18,2)) WHERE COLUMN03=@COLUMN04 AND COLUMN13=@COLUMN14 AND COLUMN19=@COLUMN19
           end
		   --EMPHCS1222 Allowing back order qty while item issue and invoicing based on system rule BY RAJ.Jr 6/10/2015
		   else
			begin
						set @tmpnewIID1=(select MAX(COLUMN02) from FITABLE010)
						if(@tmpnewIID1>0)
								begin
								set @newIID=cast((select MAX(COLUMN02) from FITABLE010) as int)+1
								end
								else
										begin
												set @newIID=1000
												end
												--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
									--set @lotno=(select case when isnull(@lotno,0)=0 then NULL else replace('',null,null) end)
												--EMPHCS915 rajasekhar reddy patakota 11/08/2015 uom condition checking in bill
												--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
	--EMPHCS1410 LOGS CREATION BY SRINIVAS											
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Values are Intiated for FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newIID as nvarchar(250)) +','+  isnull(@COLUMN04,'')+','+  isnull(@COLUMN09,'')+','+  isnull(@COLUMN12,'')+','+  isnull(@COLUMN11,'')+','+  cast((select COLUMN15 from PUTABLE005 where COLUMN01=@COLUMN13)as nvarchar(250))+','+ @COLUMN19+','+  cast(@Location  as nvarchar(250))+','+ cast( @lotno as nvarchar(250))+','+ isnull( @COLUMNA02,'')+','+
   @COLUMNA03 +  ' at ')
    )
	--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@BillAvgPrice,0))
--SET @PRATE=(ISNULL(@COLUMN11,0))
SET @ASSET=(round(CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,7)),@DecimalPositions)*ISNULL(@PRATE,0))
			insert into FITABLE010 
			(
			  COLUMN02,COLUMN03,COLUMN04,COLUMN08, COLUMN12,COLUMN17, COLUMN13,COLUMN19,COLUMNA02,COLUMNA03,COLUMN21,COLUMN22,COLUMN23,COLUMN24
			)
			values
			(
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			  @newIID,@COLUMN04,@COLUMN09,@COLUMN09, @ASSET,@BillAvgPrice , (select COLUMN15 from PUTABLE005 where COLUMN01=@COLUMN13 ),@COLUMN19,@COLUMNA02,@COLUMNA03,@Location,@lotno,@ProjectID,@COLUMN06)
		--EMPHCS1410 LOGS CREATION BY SRINIVAS
			 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Intiated Values are Created in FITABLE010 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
			END
			--EMPHCS1118 rajasekhar reddy 09/09/2015 Inventory Calculations For Multi Uom  in bill and Invoice
			END
			else
			begin
			--EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			--EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			exec usp_PUR_TP_InventoryUOM @Billno,@Billlineref,@COLUMN04,@COLUMN09,@COLUMN14,@COLUMN19,@COLUMNA03,'Insert',@Location,@lotno
			end
			--EMPHCS791 rajasekhar reddy patakota 28/7/2015 Inventory Asset Calculation In Bill With out po in edit
			if(@chk=1)
Begin
--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
SET @PRATE=(ISNULL(@BillAvgPrice,0))
SET @PRATE=(ISNULL(@COLUMN11,0))
--IF(@PRATE=0)BEGIN SET @PRATE=(@COLUMN33)END
SET @ASSET=(round(CAST(ISNULL(@COLUMN09,0)AS DECIMAL(18,7)),@DecimalPositions)*ISNULL(@PRATE,0))
set @Amtin=(select COLUMN43 from PUTABLE005 where COLUMN01=@COLUMN13)
--EMPHCS1785 rajasekhar reddy patakota 21/07/2016 Rate Calculation with inclusive of tax in Purchase
IF(@Amtin=22713)BEGIN 
if(cast(isnull(@COLUMN32,0)as decimal(18,2))>0)
begin
SET @ASSET=(@ASSET-cast(isnull(@COLUMN20,0) as decimal(18,2)))
end
else begin SET @ASSET=(@COLUMN12) end
END
			set @newID=cast((select ISNULL(MAX(COLUMN02),1000) from PUTABLE017) as int)+1
			set @VENID=(select COLUMN05 from PUTABLE005 where COLUMN01=@COLUMN13)
			
--EMPHCS1410	Logs Creation by srinivas
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Values are Intiated for PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@newID as nvarchar(250)) +', 1000 '+  cast(GETDATE() as nvarchar(15))+','+ isnull(@COLUMN13,'')+', BILL ,'+  cast(@VENID as nvarchar(250))+', 1000 ,'+ isnull( @COLUMN12,'')+','+  isnull(@COLUMN12,'')+','+ isnull(@COLUMNA01,'')+','+ isnull(@COLUMNA02,'')+','+ 
   isnull(@COLUMNA03,'')+','+ isnull(@COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+ isnull(@COLUMNA06,'')+','+ 			
   isnull(@COLUMNA07,'')+','+isnull(@COLUMNA08,'')+','+ isnull(@COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+ isnull(@COLUMNA11,'')+','+ isnull(@COLUMNA12,'')+','+ isnull(@COLUMNA13,'')+','+ isnull(@COLUMNB01,'')+','+ isnull(@COLUMNB02,'')+','+ isnull(@COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull(@COLUMNB05,'')+','+ isnull(@COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+ isnull(@COLUMNB08,'')+','+ isnull(@COLUMNB09,'')+','+ isnull(@COLUMNB10,'')+','+ isnull(@COLUMNB11,'')+','+ isnull(@COLUMNB12,'')+','+ isnull(@COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull(@COLUMND03,'')+','+ isnull(@COLUMND04,'')+','+isnull(@COLUMND05,'')+','+ isnull(@COLUMND06,'')+','+ isnull(@COLUMND07,'')+','+ isnull(@COLUMND08,'')+','+ isnull(@COLUMND09,'')+','+ isnull(@COLUMND10,'')+''+ 'at ')
    )
			insert into PUTABLE017 
			( 
			--EMPHCS1588 rajasekhar reddy patakota 04/03/2016 Average Price Calculations in all transactions
			   COLUMN02,  COLUMN03,  COLUMN04, COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13,COLUMN14,
			   COLUMNA01, COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08,
			   COLUMNA09, COLUMNA10, COLUMNA11, COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, COLUMNB09, COLUMNB10, COLUMNB11,
			   COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, COLUMND07, COLUMND08, COLUMND09, COLUMND10
			)
			values
			(  --EMPHCS1578	Inventory Assert report price is taking purchase price insested price in Bill Price BY RAJ.JR
			   @newID,'1000',@Date ,@COLUMN13,'BILL', @VENID,'1000',@memo,@ASSET,@ASSET,@COLUMN14,@Project,
			   @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04,
			   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05,
			   --EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			   @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMN04, @Billlineref, @COLUMND07,
			   @COLUMND08, @COLUMND09, @COLUMND10
			) 
			--EMPHCS1662 rajasekhar reddy patakota 08/04/2016 Average Price implementation Through out system
			if(@FinalAvgPrice!=0 or @FinalAvgPrice!=0.00)
			begin
			EXEC usp_TP_InventoryAssetUpdate @COLUMN13,@Billno,null,@COLUMN04,@COLUMN19,@Location,@lotno,@BillAvgPrice,@COLUMN14,@COLUMNA03
			EXEC usp_TP_ProdIncomeUpdate @COLUMN13,@Billno,null,@COLUMN04,@COLUMN19,@Location,@lotno,@BillAvgPrice,@COLUMN14,@COLUMNA03
			end
		end	
	--EMPHCS1410	Logs Creation by srinivas	
   set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Inventory Asset Intiated Values are Created in PUTABLE017 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		   --update PUTABLE017  set COLUMN10=@COLUMN12, COLUMN12=@COLUMN12 where COLUMN05=@COLUMN13 and COLUMN13=@COLUMN14
		   update PUTABLE013  set COLUMN08=@COLUMN09,COLUMN09=@COLUMN09, COLUMN10=@COLUMN09 where COLUMN02=@COLUMN13 and COLUMN14=@COLUMN04
		  
           set @Result=(select COLUMN04 from CONTABLE025 where COLUMN02=13)
           UPDATE PUTABLE005 set COLUMN13=@Result where COLUMN01=@COLUMN13
           end
      
		Declare @BICOL2 int,@BIAMT decimal(18,2),@PAMT decimal(18,2)
		SET @BICOL2=(SELECT COLUMN02 FROM PUTABLE005 WHERE COLUMN01=@COLUMN13 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		if exists(select COLUMN02 from PUTABLE015 where COLUMN03=@BICOL2 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		begin
		SET @BIAMT=(SELECT isnull(COLUMN14,0) FROM PUTABLE005 WHERE COLUMN01=@COLUMN13 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		SET @PAMT=(SELECT SUM(cast(iif(isnull(cast(COLUMN16 as nvarchar(250)),'')='','0',COLUMN16)as decimal(18,2))+cast(iif(isnull(cast(COLUMN06 as nvarchar(250)),'')='','0',COLUMN06)as decimal(18,2))) 
		FROM PUTABLE015 where COLUMN03=@BICOL2 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		if(@BIAMT=@PAMT)
		UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=12) where COLUMN01=@COLUMN13 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
		else
		UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=11) where COLUMN01=@COLUMN13 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
		end
		else
		begin
		UPDATE PUTABLE005 set COLUMN13=(select COLUMN04 from CONTABLE025 where COLUMN02=13) where COLUMN01=@COLUMN13 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
		end

	  update  PUTABLE011 set COLUMN07=@COLUMN07,COLUMN08=@COLUMN09,COLUMN17=@COLUMN11,COLUMN18=@COLUMN12
	  where COLUMN04=@COLUMN04 and COLUMN19=@COLUMN13
   ----EMPHCS851 Invoice - Header level tax is not updating in Tax Register,also check in bill BY Raj.Jr 6/8/2015
	set @internalid= (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
	set @BIID=(select column13 from PUTABLE006 where COLUMN02=@COLUMN02)
 --   set @Tax= (select COLUMN18 from PUTABLE006 where COLUMN02=@COLUMN02)
	--set @TaxAG= (select column16 from MATABLE013 where COLUMN02=@Tax)
	--set @TAXRATE= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
	--set @TaxNo= (select COLUMN17 from MATABLE013 where COLUMN02=@Tax)
	--set @headerTax= (select COLUMN23 from PUTABLE005 where COLUMN01=@BIID)
 --  set @headerTaxAG= (select isnull(column16,0) from MATABLE013 where COLUMN02=@headerTax)
 --   set @Taxappliedon= (select COLUMN19 from MATABLE013 where COLUMN02=@headerTax)
 -- set @headerTAXRATE= (select isnull(column17,0) from MATABLE013 where COLUMN02=@headerTax)
  --EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
set @ldisamt=(@COLUMN12)
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
--IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
--begin
--IF(ISNULL(@COLUMN31,0)=22556)
--BEGIN SET @ldisamt=(CAST(@COLUMN12 AS decimal(18,2))-(CAST(@COLUMN12 AS decimal(18,2))*CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100)) END
--else
--BEGIN SET @ldisamt=(CAST(@COLUMN12 AS decimal(18,2))-CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
--end  
          
	  set @DT= (select COLUMN08 from PUTABLE005 where COLUMN01=@BIID)
      set @venID=(select cast(COLUMN05 as int) from PUTABLE005 where column01=@BIID)
      set @MEMO= (select COLUMN12 from PUTABLE005 where COLUMN01=@BIID)
      set @project=(select COLUMN26 from PUTABLE005 where COLUMN01=@BIID)
set @ldisamt=(@COLUMN12)
--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
--IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
--begin
--IF(ISNULL(@COLUMN31,0)=22556)
--BEGIN SET @ldisamt=(CAST(@COLUMN12 AS decimal(18,2))-(CAST(@COLUMN12 AS decimal(18,2))*CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100)) END
--else
--BEGIN SET @ldisamt=(CAST(@COLUMN12 AS decimal(18,2))-CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
--end	
--set @internalid= (select max(column01) from PUTABLE005 where COLUMN01=@COLUMN13)
		if(@chk=0)
		begin
			set @Amtin=(select COLUMN43 from PUTABLE005 where COLUMN01=@BIID)
			SET @ActAmt=(@COLUMN12) 		
			IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
			begin
			IF(ISNULL(@COLUMN31,0)=22556)
			BEGIN SET @disval=(((round(CAST(ISNULL(@COLUMN09,0) AS decimal(18,7)),@DecimalPositions)*CAST(ISNULL(@COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100))) END
			else
			BEGIN SET @disval=(cast(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
            SET @ActAmt=((cast(isnull(@ActAmt,0) as decimal(18,2)))+cast(isnull(@disval,0) as decimal(18,2)))
			END
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Discount  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			SET @ACID=(SELECT IIF(EXISTS(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),(SELECT COLUMN59 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1078))
			SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22344', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ActAmt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ActAmt,NULL,@Billno,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
			END
			ELSE IF(@ACTYPE=22406)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22406', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN12,       @COLUMN15 = '0',       @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12, COLUMN13)
			values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@ACID,CAST(isnull(@COLUMN12,0) AS decimal(18,2)),CAST(isnull(@COLUMN12,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project,@COLUMN04)
		    END
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Discount Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		end
		
		else if(@chk=1)
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
		    set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
			set @Amtin=(select COLUMN43 from PUTABLE005 where COLUMN01=@COLUMN13)
			 SET @ActAmt=(@COLUMN12) 			
			IF(cast(ISNULL(@COLUMN32,0) as decimal(18,2))>0)
			begin
			IF(ISNULL(@COLUMN31,0)=22556)
			BEGIN SET @disval=(((round(CAST(ISNULL(@COLUMN09,0) AS decimal(18,7)),@DecimalPositions)*CAST(ISNULL(@COLUMN11,0) AS decimal(18,2)))*(CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100))) END
			else
			BEGIN SET @disval=(cast(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
            SET @ActAmt=((cast(isnull(@ActAmt,0) as decimal(18,2)))+cast(isnull(@disval,0) as decimal(18,2)))
			END
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHASE  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
					SET @ACID=(SELECT IIF(isnull((SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),0)!=0,(SELECT COLUMN74 FROM MATABLE007 WHERE COLUMN02=@COLUMN04 AND COLUMNA03=@COLUMNA03 AND COLUMN59!=0),1133))
					SET @ACTYPE=(SELECT IIF(EXISTS(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),(SELECT COLUMN07 FROM FITABLE001 WHERE COLUMN02=@ACID AND COLUMNA03=@COLUMNA03),22344))
			IF(@ACTYPE=22344)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22344', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = 0,       @COLUMN15 = @ActAmt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ActAmt,NULL,@BINO,@ACID,@COLUMNA02 , @COLUMNA03,0,@project)
		    END
			ELSE IF(@ACTYPE=22406)
			BEGIN
 exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @VENID,    @COLUMN10 = '22406', @COLUMN11 = @ACID,
		@COLUMN12 = @MEMO,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN12,       @COLUMN15 = '0',       @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
			set @AID=((select MAX(isnull(COLUMN02,999)) from FITABLE025)+1)
			insert into FITABLE025(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMN12, COLUMN13)
			values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@ACID,CAST(isnull(@COLUMN12,0) AS decimal(18,2)),CAST(isnull(@COLUMN12,0) AS decimal(18,2)),@COLUMNA02 , @COLUMNA03,@project,@COLUMN04)
		    END
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'PURCHAGE  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
      set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
set @Tax= (select COLUMN18 from PUTABLE006 where COLUMN02=@COLUMN02)
 if(@Tax<0)
	begin
		select @TaxString=COLUMN05,@TAXRATEM=isnull(COLUMN16,0) from MATABLE014 where COLUMN02=(Select REPLACE(@Tax,'-',''))
	end
 
set @cnt  = 0 set @vochercnt = 1 set @AMOUNTM=@COLUMN12;
WHILE @cnt < @vochercnt
BEGIN
	select @Tax=ISNULL(COLUMN18,0) from PUTABLE006 where COLUMN02=@COLUMN02
		if(@Tax<0) 
	begin
		if(@TaxString IS NULL) set @vochercnt=0; 
		set @Tax=(Select Substring(@TaxString,0,CharIndex(',',@TaxString)))--getting first record
		if((Select len(Substring(@TaxString,0,CharIndex(',',@TaxString))))=0)
		begin
			set @TaxString=(@TaxString)--only one record i.e first record
			set @Tax=(@TaxString)
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
			set @vochercnt=0;
		end
		else
		begin
			set @TaxString=(Select REPLACE(@TaxString,CAST(@Tax AS NVARCHAR(25))+',', ''))--removing first record
			--set @TaxString=(Select REPLACE(@TaxString,@Tax, ''))--removing first record
			if(@vochercnt!=0)
			begin
			set @vochercnt=(@vochercnt+1)
			end
			select @TaxAG=column16, @TAXRATE=COLUMN17,@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
			--if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and @TAXRATEM>0)
			--select @COLUMN12= (@AMOUNTM/@TAXRATEM)*@TAXRATE,@ldisamt = (@AMOUNTM/@TAXRATEM)*@TAXRATE
		end
	end
	else
	begin
		set @vochercnt=0
	end
	set @headerTax= (select COLUMN23 from PUTABLE005 where COLUMN01=@BIID)
	select @TaxAG=column16, @TAXRATE=COLUMN17,@TaxNo=COLUMN17,@TTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@Tax
	select @Taxappliedon=COLUMN19,@headerTaxAG=column16,@headerTAXRATE=COLUMN17,@HTTYPE=ISNULL(COLUMN25,22383) from MATABLE013 where COLUMN02=@headerTax
	set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
	set @ReverseItemid = (select COLUMN04 from PUTABLE006 where COLUMN02=@COLUMN02)
	set @ReverseCharged = (select isnull(COLUMN58,0) from PUTABLE005 where COLUMN01=@BIID)
		
		if((@ReverseCharged=0 or @ReverseCharged='False' or @ReverseCharged='0') and @TTYPE!=22344 and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		begin
if(@TaxAG=23582) set @CharAcc1=(1136)
else if(@TaxAG=23584) set @CharAcc1=(1137)
else if(@TaxAG=23583) set @CharAcc1=(1138)
else if(@TaxAG=23585) set @CharAcc1=(1139)
else if(@TaxAG=23586) set @CharAcc1=(1140)
if(@CharAcc1 is not null )
begin
   set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
   select top 1 @CharAcc = COLUMN04,@TTYPE1 = column07 from FITABLE001 where COLUMN02=@CharAcc1 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
   set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
   set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
   exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @CharAcc1,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
   insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
   values(ISNULL(@Vid,1000),'BILL',@DT,@MEMO,@venID,@ltaxamt,0,  @Billno,@CharAcc1,@project,@COLUMNA02, @COLUMNA03)	
		--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		--values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@CharAcc1,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Billno,@project)		
end
end
		else if((@ReverseCharged=1 or @ReverseCharged='True' or @ReverseCharged='1') and @TTYPE!=22344 and (@TaxAG=23582 or @TaxAG=23584 or @TaxAG=23583 or @TaxAG=23585 or @TaxAG=23586))
		BEGIN
   select top 1 @CharAcc = COLUMN04,@TTYPE1 = column07 from FITABLE001 where COLUMN02=1141 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0
	set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)	
	set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = 1141,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
	values(ISNULL(@Vid,1000),'BILL',@DT,@MEMO,@venID,@ltaxamt,0,  @Billno,1141,@project,@COLUMNA02, @COLUMNA03)
	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
	set @CharAcc = (select max(COLUMN04) from FITABLE001 where COLUMN02=1135 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = '22383', @COLUMN11 = '1135',
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = @ltaxamt,       @COLUMN15 = 0,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
	values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,1135,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@CharAcc,@COLUMNA02, @COLUMNA03,@Billno,@project)
END
		else if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG!=22400 AND @TaxAG!=23050 AND @TaxAG!=23582 AND @TaxAG!=23583 AND @TaxAG!=23584 AND @TaxAG!=23585 AND @TaxAG!=23586)  and(@TTYPE=0 OR @TTYPE=22383))
		BEGIN
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',BILL,'+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@Tax as nvarchar(250))+','+  cast(@TaxAG as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),'') +  ' at ')
    )
    set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
	set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
	select top 1 @CharAcc = column02,@TTYPE1 = column07 from fitable001 where column04 = @TaxColumn17 and columna03 = @COLUMNA03
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @CharAcc,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
	set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Tax,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Created In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		END
		ELSE if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG!=22400 AND @TaxAG!=23050 ) and @TTYPE=22344)
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		select top 1 @TaxColumn17 = COLUMN02,@TTYPE1 = column07 from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03
		set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE036)+1)
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13,COLUMN11)
						values(@Vid,'BILL',@DT,@internalid,@venID,@ltaxamt,NULL,@BINO,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0,@project)
		END
		ELSE if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG!=22400 AND @TaxAG!=23050 )  and @TTYPE=22402)
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		select top 1 @TaxColumn17 = COLUMN02,@TTYPE1 = COLUMN07 from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03
		set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @Billno, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE034)+1)
		insert into FITABLE034 (COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,COLUMN10,COLUMN12,COLUMNA02, COLUMNA03)
		values(ISNULL(@Vid,1000),'BILL',@DT,@MEMO,@venID,@ltaxamt,0,  @Billno,@TaxColumn17,@project,@COLUMNA02, @COLUMNA03)
		END
		--END
		--EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
		SET @DISCAMNT=(CAST(@COLUMN12 AS decimal(18,2)))
		--IF(ISNULL(@COLUMN31,0)=22556)
		--BEGIN SET @DISCAMNT=(CAST(@COLUMN12 AS decimal(18,2))-(CAST(@COLUMN12 AS decimal(18,2))*CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))/100)) END
		--ELSE 
		--BEGIN SET @DISCAMNT=(CAST(@COLUMN12 AS decimal(18,2))-CAST(ISNULL(@COLUMN32,0) AS decimal(18,2))) END
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND (@TaxAG=22400 OR @TaxAG=23050))
		begin
			set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
			select top 1 @TaxColumn17 = column02,@TTYPE1 = COLUMN07 from fitable001 where column04 like('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+cast(@TAXRATE as nvarchar(20))+'%') and columna03=@columna03
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			set @TotTaxAmt= (isnull((@DISCAMNT)*((@TAXRATE)*(0.01)),0))
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE1, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @TotTaxAmt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((CAST(@DISCAMNT AS decimal(18,2))/100)*(@TAXRATE),0) AS decimal(18,2)),NULL,@Billno,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0,@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 AND @TaxAG=22401)
		begin
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@TaxAG)+CAST(@TAXRATE AS NVARCHAR(250))+'%')
		set @CharAcc = @TaxColumn17
		set @TaxColumn17=(select MAX(COLUMN02) from FITABLE001 WHERE COLUMN04=@TaxColumn17 AND COLUMNA03=@COLUMNA03)
		set @ltaxamt=(cast(@ldisamt as decimal(18,2))*(cast(@TAXRATE as decimal(18,2))*0.01))
		set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @TAXRATE, @COLUMN18 = @TaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Tax,@TaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@TAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@Tax)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@Tax)
		if(cast(@Date as date)>'11/14/2015')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			set @TotTaxAmt=(CAST(isnull((@DISCAMNT)*((@SBCRate)*(0.01)),0) AS decimal(18,2)))
			set @TaxColumn17=('INPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @TotTaxAmt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @SBCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((@SBCRate)*(0.01)),0) AS decimal(18,2)),NULL,@Billno,@Servicename,@COLUMNA02 , @COLUMNA03,0,@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		if(cast(@Date as date)>'05/31/2016')
		begin
			set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@TAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1131'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@TAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			--insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			--values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((0.5)*(0.01)),0) AS decimal(18,2)),NULL,@Billno,1131,@COLUMNA02 , @COLUMNA03,0)
			set @TotTaxAmt=isnull((@DISCAMNT)*((@KKCRate)*(0.01)),0)
			set @TaxColumn17=('INPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
			set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @TotTaxAmt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @KKCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@Servicename,NULL,CAST(isnull((@DISCAMNT)*((@KKCRate)*(0.01)),0) AS decimal(18,2)),@COLUMN04,@COLUMN12,@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Billno,@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end

		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ))
		BEGIN
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@Tax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@Tax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@Tax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@Tax)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	    set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN04,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@TAXRATE as decimal(18,2)),0)>0 and (@TaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@DISCAMNT,0) as decimal(18,2))*(cast(isnull(@TAXRATE,0) as decimal(18,2))*0.01))
		set @ltaxamt=(cast(isnull(@lexcisetaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    set @TotTaxAmt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))+CAST(isnull(@ltaxamt,0) AS decimal(18,2)))
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @TaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@TaxAG,@ltaxamt,@COLUMN04,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		END
		
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(isnull(@TotTaxAmt,0) as decimal(18,2)))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@headerTAXRATE as decimal(18,2))*0.01))
	    end

		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0  and (@headerTaxAG!=22400 AND @headerTaxAG!=23050))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)
		set @TaxColumn17=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+CAST(@headerTAXRATE AS NVARCHAR(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities  Values are Intiated For FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@Vid as nvarchar(250)) +','+  cast(@DT as nvarchar(250))+',BILL,'+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast(@MEMO as nvarchar(250))+','+  cast(@headerTax as nvarchar(250))+','+  cast(@headerTaxAG as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +','+ isnull((select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),'') +  ' at ')
    )
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @headerTAXRATE, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@headerTax,@headerTaxAG,@ltaxamt,@COLUMN04,@COLUMN12,@headerTAXRATE,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Current Liabilities Intiated Values are Created In FITABLE026 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		END 

		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND (@headerTaxAG=22400 OR @headerTaxAG=23050))
		begin
			set @CharAcc=('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+CAST(@headerTAXRATE AS NVARCHAR(250))+'%')
			set @TaxColumn17=(select max(column02) from fitable001 where column04 like('INPUT '+(select MATABLE002.COLUMN04 from MATABLE002 WHERE MATABLE002.COLUMN02=@headerTaxAG)+cast(@headerTAXRATE as nvarchar(20))+'%') and columna03=@columna03)
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @TaxColumn17,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @headerTAXRATE, @COLUMN18 = @headerTaxAG, @COLUMN19 = @CharAcc, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ltaxamt,NULL,@Billno,@TaxColumn17,@COLUMNA02 , @COLUMNA03,0,@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT CST  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
		end

		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 AND @headerTaxAG=22401)
		begin
	    set @SBC=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @SBCRate=  (select isnull(COLUMN22,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKC=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTAXRATE)
		set @KKCRate=  (select isnull(COLUMN23,0.5) from MATABLE013 where COLUMN02=@headerTAXRATE)
		if(cast(@Date as date)>'11/14/2015')
		begin
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(isnull(@TotTaxAmt,0) as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(isnull(@TotTaxAmt,0) as decimal(18,2)))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@SBCRate as decimal(18,2))*0.01))
	    end
		    set @TaxColumn17=('INPUT SBC@'+cast(cast(@SBCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)		    
			set @AID=cast((select MAX(COLUMN02) from FITABLE036) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1078'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @SBCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13, COLUMN11)
			values(@AID,'BILL',@Date,@internalid,@venID,@ltaxamt,NULL,@Billno,@Servicename,@COLUMNA02 , @COLUMNA03,0,@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT SBC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		if(cast(@Date as date)>'05/31/2016')
		begin
		if(@Taxappliedon='Tax')
	    begin
        set @ltaxamt=(cast(@TotTaxAmt as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else if(@Taxappliedon='SubTotalTax')
	    begin
        set @ltaxamt=((cast(@DISCAMNT as decimal(18,2))+cast(@TotTaxAmt as decimal(18,2)))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
	    else
	    begin
        set @ltaxamt=(cast(@DISCAMNT as decimal(18,2))*(cast(@KKCRate as decimal(18,2))*0.01))
	    end
		    set @TaxColumn17=('INPUT KKC@'+cast(cast(@KKCRate as decimal(18,2)) as nvarchar(250))+'%')
		    set @Servicename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)		    
			exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @Date,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Servicename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @COLUMN12, @COLUMN17 = @KKCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @AID=cast((select MAX(COLUMN02) from FITABLE026) as int)+1;
			set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values are Intiated For FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(@AID as nvarchar(250)) +',BILL,'+  cast(@Date as nvarchar(250))+','+  cast(@internalid as nvarchar(250))+','+  cast(@venID as nvarchar(250))+','+  cast((CAST(@COLUMN12 AS decimal(18,2))/100)*(@headerTAXRATE) as nvarchar(250))+','+  cast(@BINO as nvarchar(250))+',1131'+  cast(@COLUMNA02 as nvarchar(250))+','
   + isnull(@COLUMN04,'')+','+  cast(@COLUMN12 as nvarchar(250))+','+  cast(@headerTAXRATE as nvarchar(250))+','+  isnull(@COLUMNA02,'')+','+
   isnull(@COLUMNA03,'') +' at ')
    )
			--insert into FITABLE036(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09 ,COLUMN10,COLUMNA02, COLUMNA03, COLUMNA13)
			--values(@AID,'BILL',@Date,@internalid,@venID,CAST(isnull((@DISCAMNT)*((0.5)*(0.01)),0) AS decimal(18,2)),NULL,@Billno,1131,@COLUMNA02 , @COLUMNA03,0)
			insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@AID,@Date,'BILL',@internalid,@venID,@MEMO,@Servicename,NULL,@ltaxamt,@COLUMN04,@COLUMN12,@KKCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,@Billno,@project)
		set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET5***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'INPUT KKC  Values Intiated Values are Created In FITABLE036 Table '+'' + CHAR(13)+CHAR(10) + '')
    )
	end
		end
		
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ))
		BEGIN
		set @EDCESS=  (select COLUMN20 from MATABLE013 where COLUMN02=@headerTax)
		set @EDCRate=  (select COLUMN22 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECESS=  (select COLUMN21 from MATABLE013 where COLUMN02=@headerTax)
		set @SHECRate=  (select COLUMN23 from MATABLE013 where COLUMN02=@headerTax)
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@EDCESS as bit)=1 and isnull(@EDCRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT EC@'+cast(cast(@EDCRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		set @ExciseTotTax=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@EDCRate,0) as decimal(18,2))*0.01))
	    exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ExciseTotTax,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @EDCRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		 Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@headerTaxAG,@ExciseTotTax,@COLUMN04,@lexcisetaxamt,@EDCRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		if(isnull(cast(@COLUMN12 as decimal(18,2)),0)>0 and isnull(cast(@headerTAXRATE as decimal(18,2)),0)>0 and (@headerTaxAG=23513 ) and (cast(@SHECESS as bit)=1 and isnull(@SHECRate,0)>0 ))
		BEGIN
		set @internalid=  (select COLUMN01 from PUTABLE006 where COLUMN02=@COLUMN02)
		set @TaxColumn17=('INPUT SHEC@'+cast(cast(@SHECRate as decimal(18,2)) as nvarchar(250))+'%')
		set @Excisename=(select top(1)COLUMN02 from fitable001 where COLUMN04=@TaxColumn17 and COLUMNA03=@COLUMNA03 and isnull(COLUMNA13,0)=0)
		set @lexcisetaxamt=(cast(isnull(@ltaxamt,0) as decimal(18,2)))
		set @ExciseTotTax=(cast(isnull(@ltaxamt,0) as decimal(18,2))*(cast(isnull(@SHECRate,0) as decimal(18,2))*0.01))
	    set @Vid=((select isnull(max(column02),1000) from FITABLE026)+1)	
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'BILL',  @COLUMN04 = @BINO, @COLUMN05 = @COLUMN13,  @COLUMN06 = @internalid,
		@COLUMN07 = @DT,   @COLUMN08 = '22305',  @COLUMN09 = @venID,    @COLUMN10 = @TTYPE, @COLUMN11 = @Excisename,
		@COLUMN12 = @MEMO,   @COLUMN13 = @project,	@COLUMN14 = 0,       @COLUMN15 = @ltaxamt,      @COLUMN16 = @lexcisetaxamt, @COLUMN17 = @SHECRate, @COLUMN18 = @headerTaxAG, @COLUMN19 = @TaxColumn17, @COLUMN20 = @COLUMN04,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
		Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN10,  COLUMN11,COLUMN13, COLUMN14, COLUMN16,COLUMN17,COLUMNA02, COLUMNA03,COLUMN09,COLUMN19)
		values(@Vid,@DT,'BILL',@internalid,@venID,@MEMO,@Excisename,@headerTaxAG,@ExciseTotTax,@COLUMN04,@lexcisetaxamt,@SHECRate,@TaxColumn17,@COLUMNA02, @COLUMNA03,(select COLUMN04 from PUTABLE005 where COLUMN01=@COLUMN13),@project)
	    END
		END
END
END
else IF @Direction = 'Delete'
BEGIN
UPDATE PUTABLE006 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02

END
--EMPHCS1410 LOGS CREATION BY SRINIVAS
exec [CheckDirectory] @tempSTR,'usp_PUR_TP_PUTABLE006.txt',0
end try
			begin catch
			SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
--EMPHCS1410 LOGS CREATION BY SRINIVAS		
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'Exception_PUTABLE006.txt',0
				if not exists(select column01 from putable006 where column13=@column13)
					begin
						delete from putable005 where column01=@column13
						return 0
			end
return 0
end catch
end









GO
