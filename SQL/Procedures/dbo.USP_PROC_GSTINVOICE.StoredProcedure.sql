USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_PROC_GSTINVOICE]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_PROC_GSTINVOICE](@SONO nvarchar(250)=null,@TYPE nvarchar(250)=null,@frDT nvarchar(250)=null) 
AS
BEGIN
DECLARE @AcOwner int,@Formid int
SET @AcOwner = (SELECT COLUMNA03 FROM SATABLE009 WHERE COLUMN02 = @SONO)
SET @Formid = (SELECT COLUMN03 FROM SATABLE009 WHERE COLUMN02 = @SONO)
if(@TYPE='HEADER')
begin
if(@AcOwner=56664)
Begin
	select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,
S9.COLUMN10, S9.COLUMN24 as COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,

S9.COLUMN43 as wayBill,ht.COLUMN07 as HTamnt,
c.COLUMN04 as company  , c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN34 as TrackingNo,
(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0)) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,
f.COLUMN04 as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1,
 c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,
 s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,
 s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,
 ft.COLUMN04 as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,opu.COLUMN30 as OPUVATNO,
 opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,
 CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , 
 S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+S9.COLUMN24-(iif(S9.COLUMNA03=56637,isnull(S9.COLUMN25,0)-isnull(S9.COLUMN55,0),0))) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,s5.COLUMN04 as reference,
 S9.COLUMN41 TruckNo,S9.COLUMN46,s.COLUMN42 CustGSTIN,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,S9.COLUMN07 BillingAddress,
 S9.COLUMN67 RevCharge,c.COLUMND02 CGSTIN,MC.COLUMN03 compCtry,MS.COLUMN03 compState,S9.COLUMN09 SalesRep,m5.COLUMN04 brand,S9.COLUMN12 memo,m10.COLUMN33 as [signature],c.COLUMN07 as Custmail,s7.column04 dcno,s7.column08 Date1,S9.COLUMN34 dispatchno,c.COLUMN02 CompanyID, c.COLUMN07 companymail ,M17.COLUMN05 StateCodep   from SATABLE009 S9 left join SATABLE002 s on s.column02=S9.COLUMN05 and isnull(s.columna13,0)=0
 inner join SATABLE010 s10 on (S9.column04=@SONO OR S9.column02=@SONO) AND s10.column15=S9.COLUMN01 and s10.columna02 = s9.columna02 and s10.columna03 = s9.columna03 and isnull(s10.columna13,0)=0
 left join matable002 mot on mot.column02=S9.column32 left join matable002 ft on ft.column02=S9.column30 
 left join matable002 st on st.column02=s.column43 
 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND (c.COLUMNA02=s9.COLUMNA02 or c.COLUMNA02 is null) AND ISNULL(c.COLUMNA13,0)=0 AND ISNULL(c.COLUMN05,0)=0 
 left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    
 left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    
 left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 
 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 
 left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 
 left JOIN MATABLE016 MC  ON MC.COLUMN02 = c.COLUMN15 
 left JOIN MATABLE017 MS  ON MS.COLUMN02 = c.COLUMN13 
 left outer join MATABLE005 m5 on m5.COLUMN02=s.COLUMN37
 left outer join MATABLE010 m10 on m10.COLUMN02=S9.COLUMNA08
 left outer join satable007 s7 on s7.column02 = S10.column04 and  s7.column06 = S9.column06 and s7.columnA03 = S9.columnA03 and isnull(s7.columnA13,0)=0
 where (S9.column04=@SONO OR S9.column02=@SONO) AND  ISNULL(S9.COLUMNA13,0)=0
END
else if (@AcOwner=56852)
BEGIN
select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,
S9.COLUMN10, S9.COLUMN24 as COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,
ht.COLUMN07 as HTamnt,
c.COLUMN04 as company,c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN12 as TrackingNo,
IIF(@AcOwner=56728,isnull(S9.COLUMN25,0),(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0))) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,
IIF(@AcOwner in (56647,56828),S9.COLUMN40,f.COLUMN04) as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1,
 c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,
 s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,
 s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,
 iif(S9.COLUMNA03 IN (56647,56860),s9.COLUMN43,ft.COLUMN04) as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,
 iif(S9.COLUMNA03 IN (56571,56836,56837),msp.COLUMN04,opu.COLUMN30)as OPUVATNO,
 opu.COLUMN38 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,
 CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , 
 S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+S9.COLUMN24-(iif(S9.COLUMNA03=56637,isnull(S9.COLUMN25,0)-isnull(S9.COLUMN55,0),0))) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,iif(@AcOwner=56606,S9.COLUMN09,s5.COLUMN04) as reference,
 S9.COLUMN41 TruckNo,S9.COLUMN46,s.COLUMN42 CustGSTIN,s.COLUMN06 TransportName,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,S9.COLUMN07 BillingAddress,
 iif(S9.COLUMNA03 IN (56909),CAST(m10.COLUMN06 AS NVARCHAR(250)),CAST(S9.COLUMN67 AS NVARCHAR(250))) RevCharge,c.COLUMND02 CGSTIN,MC.COLUMN03 compCtry,MS.COLUMN03 compState,S9.COLUMN09 SalesRep,m5.COLUMN04 brand,iif(S9.COLUMNA03 IN (56852),FORMAT(S9.COLUMN08,'MM/dd/yyyy'),S9.COLUMN12) memo,m10.COLUMN33 as [signature],c.COLUMN07 as Custmail,s7.column04 dcno,s7.column08 Date1,S9.COLUMN09 dispatchno,c.COLUMN02 CompanyID, c.COLUMN07 companymail, c.COLUMND03 compGST,c.COLUMN36 DLNO,c.COLUMN30 as STNO,isnull(S9.COLUMN56,0) Discount1,(isnull(S9.COLUMN20,0)-isnull(S9.COLUMN24,0)-isnull(S9.COLUMN72,0)) as Tax,c.COLUMN09 as companyaddress,S15.COLUMN04 SalesQuotation,S9.COLUMN65 GSTTYPE,iif(S9.COLUMNA03 IN (56852),FORMAT(S9.COLUMN10,'MM/dd/yyyy'),S9.COLUMN43) as wayBill,S9.COLUMN22 GSTSUBTOTAL,
 M17.COLUMN05 StateCodep,s.column06 cusmemo,c.COLUMN36 Durgno from SATABLE009 S9 
 left join SATABLE002 s on s.column02=S9.COLUMN05 and isnull(s.columna13,0)=0
 left join matable002 mot on mot.column02=S9.column33 left join matable002 ft on ft.column02=S9.column30 
 left join matable002 st on st.column02=s.column43 
 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND ISNULL(c.COLUMNA13,0)=0 AND ISNULL(c.COLUMN05,0)=0 
 left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    
 left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    
 left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 
 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 
 left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 
 left JOIN MATABLE016 MC  ON MC.COLUMN02 = c.COLUMN15 
 left JOIN MATABLE017 MS  ON MS.COLUMN02 = c.COLUMN13 
 left outer join MATABLE005 m5 on m5.COLUMN02=s.COLUMN37
 left outer join MATABLE010 m10 on m10.COLUMN02=S9.COLUMNA08 and m10.columnA03 = S9.columnA03 and isnull(m10.columnA13,0)=0
 left outer join satable007 s7 on s7.column06 = S9.column06 and s7.columnA03 = S9.columnA03 and isnull(s7.columnA13,0)=0
 left outer join SATABLE015 S15 ON S15.COLUMN02 = S9.column28 and S15.columnA03 = S9.columnA03 and isnull(S15.columnA13,0)=0
 left join matable002 msp on msp.column02=S9.COLUMN32
 where (S9.column04=@SONO OR S9.column02=@SONO) AND  ISNULL(S9.COLUMNA13,0)=0
END
Else
Begin
	select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,
S9.COLUMN10, S9.COLUMN24 as COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,
ht.COLUMN07 as HTamnt,
c.COLUMN04 as company,c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN34 as TrackingNo,
IIF(@AcOwner=56728,isnull(S9.COLUMN25,0),(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0))) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,
IIF(@AcOwner in (56647,56828),S9.COLUMN40,f.COLUMN04) as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1,
 c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,
 s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,
 s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,
 iif(S9.COLUMNA03 IN (56647,56860),s9.COLUMN43,ft.COLUMN04) as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,
 iif(S9.COLUMNA03 IN (56571,56836,56837),msp.COLUMN04,opu.COLUMN30)as OPUVATNO,
 opu.COLUMN38 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,
 CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , 
 S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+S9.COLUMN24-(iif(S9.COLUMNA03=56637,isnull(S9.COLUMN25,0)-isnull(S9.COLUMN55,0),0))) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,iif(@AcOwner=56606,S9.COLUMN09,s5.COLUMN04) as reference,
 S9.COLUMN41 TruckNo,S9.COLUMN46,s.COLUMN42 CustGSTIN,s.COLUMN06 TransportName,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,S9.COLUMN07 BillingAddress,
 iif(S9.COLUMNA03 IN (56909),CAST(m10.COLUMN06 AS NVARCHAR(250)),CAST(S9.COLUMN67 AS NVARCHAR(250))) RevCharge,c.COLUMND02 CGSTIN,MC.COLUMN03 compCtry,MS.COLUMN03 compState,S9.COLUMN09 SalesRep,m5.COLUMN04 brand,S9.COLUMN12 memo,m10.COLUMN33 as [signature],c.COLUMN07 as Custmail,s7.column04 dcno,s7.column08 Date1,S9.COLUMN09 dispatchno,c.COLUMN02 CompanyID, c.COLUMN07 companymail, c.COLUMND03 compGST,c.COLUMN36 DLNO,c.COLUMN30 as STNO,isnull(S9.COLUMN56,0) Discount1,(isnull(S9.COLUMN20,0)-isnull(S9.COLUMN24,0)-isnull(S9.COLUMN72,0)) as Tax,c.COLUMN09 as companyaddress,S15.COLUMN04 SalesQuotation,S9.COLUMN65 GSTTYPE,S9.COLUMN43 as wayBill,S9.COLUMN22 GSTSUBTOTAL,
 M17.COLUMN05 StateCodep,s.column06 cusmemo,c.COLUMN36 Durgno from SATABLE009 S9 
 left join SATABLE002 s on s.column02=S9.COLUMN05 and isnull(s.columna13,0)=0
 left join matable002 mot on mot.column02=S9.column33 left join matable002 ft on ft.column02=S9.column30 
 left join matable002 st on st.column02=s.column43 
 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND ISNULL(c.COLUMNA13,0)=0 AND ISNULL(c.COLUMN05,0)=0 
 left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    
 left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    
 left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 
 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 
 left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 
 left JOIN MATABLE016 MC  ON MC.COLUMN02 = c.COLUMN15 
 left JOIN MATABLE017 MS  ON MS.COLUMN02 = c.COLUMN13 
 left outer join MATABLE005 m5 on m5.COLUMN02=s.COLUMN37
 left outer join MATABLE010 m10 on m10.COLUMN02=S9.COLUMNA08 and m10.columnA03 = S9.columnA03 and isnull(m10.columnA13,0)=0
 left outer join satable007 s7 on s7.column06 = S9.column06 and s7.columnA03 = S9.columnA03 and isnull(s7.columnA13,0)=0
 left outer join SATABLE015 S15 ON S15.COLUMN02 = S9.column28 and S15.columnA03 = S9.columnA03 and isnull(S15.columnA13,0)=0
 left join matable002 msp on msp.column02=S9.COLUMN32
 where (S9.column04=@SONO OR S9.column02=@SONO) AND  ISNULL(S9.COLUMNA13,0)=0
End
end
ELSE if(@TYPE='LINE')
begin
if(@AcOwner=56597)
begin
SELECT P.item,P.upc,SUM(P.quantity)quantity,SUM(P.FREE)FREE,P.units,P.serial,P.[desc],CONVERT(varchar,CAST(SUM(P.rate) AS money), 1)rate,CONVERT(varchar,CAST((P.MRP) AS money), 1) MRP,CONVERT(varchar, CAST(SUM(P.lineamt) AS money), 1)lineamt,SUM(P.Discount)Discount,SUM(P.val)val,SUM(P.amt)amt,P.UOM,P.[Description],
SUM(P.CGST)CGST,SUM(P.SGST)SGST,SUM(P.IGST)IGST,SUM(P.CGSTAMT)CGSTAMT,SUM(P.SGSTAMT)SGSTAMT,SUM(P.IGSTAMT)IGSTAMT,SUM(P.Tamnt)Tamnt,SUM(P.HTamnt)HTamnt,SUM(P.lTamnt)lTamnt,P.names,SUM(P.LHtaxAmnt)LHtaxAmnt,P.SACCode,P.HSNCode,P.SerItem,P.Lot,P.Expiry
 FROM(
select it.column04 item,it.column06 upc,IIF(isnull(a.COLUMN13,0)=0,0,isnull(a.COLUMN10,0)) quantity,IIF(isnull(a.COLUMN13,0)=0,isnull(a.COLUMN10,0),0) FREE,
a.COLUMN22 units,a.COLUMN04 serial,iif(isnull(f43.COLUMN14,0)>0,f43.COLUMN14,s.COLUMN05)MRP,
a.COLUMN06 'desc',CAST(iif(isnull(a.COLUMN35 ,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35 ,0)) AS decimal(18,2)) rate,CAST(isnull(a.COLUMN10,0)*iif(isnull(a.COLUMN35 ,0)=0,isnull(a.COLUMN13,0),isnull(a.COLUMN35 ,0)) AS decimal(18,2)) lineamt,
CAST(isnull(a.COLUMN19,0) AS decimal(18,2)) Discount,sum(isnull(t.COLUMN07,0)) val,--t.COLUMN04 name,
cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0))-(isnull(a.COLUMN19 ,0))as decimal(18,2))  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,a.COLUMN02,F43.COLUMN04 Lot,max(format(F43.COLUMN05,'dd/MM/yyyy')) Expiry from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
--inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,F43.COLUMN04,F43.COLUMN05,f43.COLUMN14,s.COLUMN05)P
GROUP BY P.item,P.upc,P.units,P.serial,P.[desc],P.UOM,P.[Description],P.names,P.SACCode,P.HSNCode,P.SerItem,P.Lot,P.Expiry,P.MRP
end
else if(@AcOwner=56647)
begin
SELECT P.item,P.upc,SUM(P.quantity)quantity,SUM(P.FREE)FREE,P.units,P.serial,P.[desc],CONVERT(varchar,CAST(SUM(P.rate) AS money), 1)rate,CONVERT(varchar,CAST((P.MRP) AS money), 1) MRP,CONVERT(varchar, CAST(SUM(P.lineamt) AS money), 1)lineamt,SUM(P.Discount)Discount,SUM(P.val)val,SUM(P.amt)amt,P.UOM,P.[Description],
SUM(P.CGST)CGST,SUM(P.SGST)SGST,SUM(P.IGST)IGST,SUM(P.CGSTAMT)CGSTAMT,SUM(P.SGSTAMT)SGSTAMT,SUM(P.IGSTAMT)IGSTAMT,SUM(P.Tamnt)Tamnt,SUM(P.HTamnt)HTamnt,SUM(P.lTamnt)lTamnt,P.names,SUM(P.LHtaxAmnt)LHtaxAmnt,P.SACCode,P.HSNCode,P.SerItem,P.Lot,P.Expiry MFGDATE,P.ITEMBRAND,
P.MFGDATE Expiry,P.PPRICE,P.PLDiscount
 FROM(
select it.column04 item,it.column06 upc,IIF(isnull(a.COLUMN13,0)=0,0,isnull(a.COLUMN10,0)) quantity,IIF(isnull(a.COLUMN13,0)=0,isnull(a.COLUMN10,0),0) FREE,
a.COLUMN22 units,a.COLUMN04 serial,iif(isnull(f43.COLUMN14,0)>0,f43.COLUMN14,s.COLUMN05)MRP,
a.COLUMN06 'desc',CAST(isnull(a.COLUMN13,0) AS decimal(18,2)) rate,isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) lineamt,
CAST(isnull(a.COLUMN19,0) AS decimal(18,2)) Discount,sum(isnull(t.COLUMN07,0)) val,--t.COLUMN04 name,
cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0))-(isnull(a.COLUMN19 ,0))as decimal(18,2))  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,a.COLUMN02,F43.COLUMN04 Lot,max(format(F43.COLUMN05,'MM/yyyy')) Expiry,
M5.COLUMN04 ITEMBRAND,max(format(F43.COLUMN06,'MM/yyyy')) MFGDATE,iif(isnull(f43.COLUMN13,0)>0,f43.COLUMN13,M24.COLUMN05) PPRICE,CAST(isnull(pl.COLUMN06,0) AS DECIMAL(18,0)) PLDiscount
 from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
--inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
left join MATABLE024 M24 on M24.column07=a.COLUMN05 and  M24.column06='Purchase' and M24.COLUMNA03=a.COLUMNA03 and isnull(M24.COLUMNA13,0)=0
LEFT JOIN MATABLE005 M5 ON M5.COLUMN02=it.COLUMN10 AND M5.COLUMNA03=it.COLUMNA03 AND ISNULL(M5.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,F43.COLUMN04,F43.COLUMN05,f43.COLUMN14,s.COLUMN05,M5.COLUMN04,M24.COLUMN05,pl.COLUMN06,f43.COLUMN13)P
GROUP BY P.item,P.upc,P.units,P.serial,P.[desc],P.UOM,P.[Description],P.names,P.SACCode,P.HSNCode,P.SerItem,P.Lot,P.Expiry,P.MRP,
P.ITEMBRAND,P.MFGDATE,P.PPRICE,P.PLDiscount
end
else if(@AcOwner in(56832))
begin
SELECT P.item,P.upc,SUM(P.quantity)quantity,SUM(P.FREE)FREE,P.units,P.serial,P.[desc],CONVERT(varchar,CAST(SUM(P.rate) AS money), 1)rate,CONVERT(varchar,CAST((P.MRP) AS money), 1) MRP,CONVERT(varchar, CAST(SUM(P.lineamt) AS money), 1)lineamt,SUM(P.Discount)Discount,SUM(P.val)val,SUM(P.amt)amt,P.UOM,P.[Description],
SUM(P.CGST)CGST,SUM(P.SGST)SGST,SUM(P.IGST)IGST,SUM(P.CGSTAMT)CGSTAMT,SUM(P.SGSTAMT)SGSTAMT,SUM(P.IGSTAMT)IGSTAMT,SUM(P.Tamnt)Tamnt,SUM(P.HTamnt)HTamnt,SUM(P.lTamnt)lTamnt,P.names,SUM(P.LHtaxAmnt)LHtaxAmnt,P.SACCode,P.HSNCode,P.SerItem,P.Lot,P.Expiry MFGDATE,P.ITEMBRAND,
P.MFGDATE Expiry,P.PPRICE,P.PLDiscount,P.PriceToRetail,CONVERT(varchar,CAST(SUM(P.PTS) AS money), 1) PTS
 FROM(
select it.column04 item,it.column06 upc,IIF(isnull(a.COLUMN13,0)=0,0,isnull(a.COLUMN10,0)) quantity,IIF(isnull(a.COLUMN13,0)=0,isnull(a.COLUMN10,0),0) FREE,
a.COLUMN22 units,a.COLUMN04 serial,iif(isnull(f43.COLUMN14,0)>0,f43.COLUMN14,s.COLUMN05)MRP,
a.COLUMN06 'desc',CAST(isnull(a.COLUMN13,0) AS decimal(18,2)) rate,isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) lineamt,
CAST(isnull(a.COLUMN19,0) AS decimal(18,2)) Discount,sum(isnull(t.COLUMN07,0)) val,--t.COLUMN04 name,
cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0))-(isnull(a.COLUMN19 ,0))as decimal(18,2))  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,a.COLUMN02,F43.COLUMN04 Lot,max(format(F43.COLUMN05,'MM/yyyy')) Expiry,
M5.COLUMN04 ITEMBRAND,max(format(F43.COLUMN06,'MM/yyyy')) MFGDATE,M24.COLUMN05 PPRICE,CAST(isnull(pl.COLUMN06,0) AS DECIMAL(18,0)) PLDiscount,
a.COLUMN35 PriceToRetail,CAST(isnull(a.COLUMN35,0) AS decimal(18,2)) PTS
 from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
--inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and isnull(a.COLUMN13,0)!=0 and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
left join MATABLE024 M24 on M24.column07=a.COLUMN05 and  M24.column06='Purchase' and M24.COLUMNA03=a.COLUMNA03 and isnull(M24.COLUMNA13,0)=0
LEFT JOIN MATABLE005 M5 ON M5.COLUMN02=it.COLUMN10 AND M5.COLUMNA03=it.COLUMNA03 AND ISNULL(M5.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,F43.COLUMN04,F43.COLUMN05,f43.COLUMN14,s.COLUMN05,M5.COLUMN04,M24.COLUMN05,pl.COLUMN06)P
GROUP BY P.item,P.upc,P.units,P.serial,P.[desc],P.UOM,P.[Description],P.names,P.SACCode,P.HSNCode,P.SerItem,P.Lot,P.Expiry,P.MRP,
P.ITEMBRAND,P.MFGDATE,P.PPRICE,P.PLDiscount,P.PriceToRetail
end
else
if(@AcOwner in(56571,56837,56836) and  (@Formid=1277))
BEGIN
select SUM(f.quantity)quantity,CONVERT(varchar, CAST((f.rate) AS money), 1)rate,CONVERT(varchar, CAST(SUM(lineamt) AS money), 1) lineamt ,
SUM(f.Discount)Discount,SUM(f.Val)Val,CONVERT(varchar, CAST(SUM(f.amt) AS money), 1)amt,f.UOM,(f.CGST)CGST,(f.SGST)SGST,(f.IGST)IGST,CAST(SUM(f.lineamt)*((f.CGST)/100) AS DECIMAL(18,2))CGSTAMT,
CAST(SUM(f.lineamt)*((f.SGST)/100) AS DECIMAL(18,2))SGSTAMT,CAST(SUM(f.lineamt)*((f.IGST)/100) AS DECIMAL(18,2))IGSTAMT,CAST(SUM(f.lineamt)*(SUM(f.VAL)/100) AS DECIMAL(18,2))Tamnt,SUM(f.HTamnt)HTamnt,SUM(f.lTamnt)lTamnt,f.names,SUM(f.LHtaxAmnt)LHtaxAmnt,f.SACCode,f.HSNCode,f.SerItem,f.itemfamily,f.ILDis
from ( select  (isnull(a.COLUMN10,0)) quantity,
CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) as decimal(18,2)) rate,CAST( (isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0)) as decimal(18,2)) lineamt,
(CAST(isnull(a.COLUMN19,0)as decimal(18,2))) Discount,SUM(CAST((isnull(t.COLUMN07,0))as decimal(18,2)))  val,
cast( (isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))- (CAST(isnull(a.COLUMN19,0)as decimal(18,2))) as decimal(18,2))  amt,u.COLUMN04 UOM  ,
SUM(iif(t.COLUMN16=23582,CAST((isnull(t.COLUMN07,0))as decimal(18,2)),0)) CGST, SUM(iif(t.COLUMN16=23583,CAST((isnull(t.COLUMN07,0))as decimal(18,2)),0)) SGST, SUM(iif(t.COLUMN16=23584,CAST((isnull(t.COLUMN07,0))as decimal(18,2)),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(SUM(CAST( (isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(SUM(CAST( (isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(SUM(CAST( (isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(SUM((CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(cast(isnull(ht.COLUMN07,0) as decimal(18,2))) as HTamnt,(cast( ((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* ((cast(isnull(ht.COLUMN07,0) as decimal(18,2))))/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (SUM(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,M3.COLUMN04 itemfamily,isnull(pl.COLUMN06,0) ILDis from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by a.COLUMN02,a.COLUMN10,pl.COLUMN05,b.COLUMN46,a.COLUMN35,a.COLUMN13,a.COLUMN19,u.COLUMN04,ht.COLUMN16,ht.COLUMN07,
M33.COLUMN04,M32.COLUMN04,it.COLUMN48,M3.COLUMN04,pl.COLUMN06
) f
group by f.UOM,f.SACCode,f.HSNCode,f.SerItem,f.itemfamily,f.names,f.rate,f.SGST,f.IGST,f.CGST,f.ILDis
order by f.itemfamily
END
else if(@AcOwner=56590)
BEGIN
select * from(select a.COLUMN06 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46) f order by f.COLUMN02
END
else if(@AcOwner in (56671,56672,56673,56728,56578,56899,56731,56732,56788,56807,56816,56925))
BEGIN
select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56671,56672,56673,56578,56899,56731,56732,56788,56807,56816,56925),ISNULL(CAST(a.COLUMN13 AS money),0),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,CAST(isnull(pl.COLUMN06,0) AS DECIMAL(18,0)) PLDiscount from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,pl.COLUMN06) f
END
else if(@AcOwner in (56860))
BEGIN
select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56671,56672,56673),ISNULL(CAST(a.COLUMN13 AS money),0),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,CAST(isnull(pl.COLUMN06,0) AS DECIMAL(18,0)) PLDiscount from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,pl.COLUMN06) f
END
else if(@AcOwner = 56688)
BEGIN
select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56688),ISNULL(CAST(a.COLUMN13 AS money),0),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,CAST(isnull(pl.COLUMN06,0) AS DECIMAL(18,2)) PLDiscount from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,pl.COLUMN06) f
END

else if (@AcOwner in(56794,56897,56939,56940))
Begin

select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56667,56668,56669,56697,56714,56719,56577),CAST(ISNULL(a.COLUMN13,0) AS money),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,M5.COLUMN04 mft,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, sum(isnull(iif(t.COLUMN16=23586,t.COLUMN07,0),0)) CESS, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23586,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CESSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate, F43.COLUMN04 Lot, iif(a.COLUMNA03 in(56897),max(format(F43.COLUMN05,'MM/yyyy')),max(format(F43.COLUMN05,'dd/MM/yyyy'))) Expiry,M24.COLUMN05 SMRP from  SATABLE010 a 



inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0
 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE005 M5 ON  M5.COLUMN02 = it.COLUMN10
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03
LEFT JOIN MATABLE024 M24 on M24.column07=it.COLUMN02 AND M24.COLUMNA03=it.COLUMNA03 and isnull(M24.COLUMNA13,0)=0 AND M24.COLUMN06 ='Sales'
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,M5.COLUMN04,M24.COLUMN05,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,F43.COLUMN05) f order by f.COLUMN02
END
ELSE IF(@AcOwner in(56909))
BEGIN
select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56667,56668,56669,56697,56714,56719,56577),CAST(ISNULL(a.COLUMN13,0) AS money),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,
CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,
--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,F43.COLUMN04 Lot,br.column04 Brand,
--ROUND(ISNULL(s.COLUMN05,0)-cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)/CAST(isnull(a.COLUMN10 ,0)as decimal(18,2)) )as decimal(18,2)),2) 
iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05) MRP,
--cast((iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,ISNULL(s.COLUMN05,0)-cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2))/CAST(isnull(a.COLUMN10 ,0)as decimal(18,2)))-CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))*isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) as DECIMAL(18,2))  
CAST((iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05) - isnull(a.COLUMN13,0))*isnull(a.COLUMN10,0) AS DECIMAL(18,2)) DisAmt
FROM SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE005 br on br.column02=it.COLUMN10 and it.columna03=br.columna03 and isnull(br.columna13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03 and isnull(F43.COLUMNA13,0)=0
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,br.column04,F43.COLUMN14,s.COLUMN05,a.COLUMN23) f order by f.COLUMN02
END
ELSE IF (@AcOwner in(56928,56640))
BEGIN
select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56667,56668,56669,56697,56714,56719,56577),CAST(ISNULL(a.COLUMN13,0) AS money),
CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull((a.COLUMN13-(a.COLUMN13*pl.COLUMN06*0.01))

,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,

CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull((a.COLUMN13-(a.COLUMN13*pl.COLUMN06*0.01))

,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)) as decimal(18,2)) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull((a.COLUMN13-(a.COLUMN13*pl.COLUMN06*0.01))

,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)) as decimal(18,2)) AS money)-iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull(a.COLUMN23,0),0), 1) amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,F43.COLUMN04 Lot,br.column04 Brand,iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05) MRP,
ROUND(cast((iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05)-CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))*isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0)-IIF(b.COLUMN46=22713 AND ISNULL(A.COLUMN23,0)>0,ISNULL(A.COLUMN23,0),0) as DECIMAL(18,2)),0)  DisAmt
,max(format(F43.COLUMN05,'dd/MM/yyyy')) Expiry
 from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE005 br on br.column02=it.COLUMN10 and it.columna03=br.columna03 and isnull(br.columna13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03 and isnull(F43.COLUMNA13,0)=0
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,br.column04,F43.COLUMN14,s.COLUMN05,a.COLUMN23,pl.COLUMN06) f order by f.COLUMN02
END
ELSE IF (@AcOwner in(56645))
 BEGIN
select * from(select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56667,56668,56669,56697,56714,56719,56577),CAST(ISNULL(a.COLUMN13,0) AS money),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,F43.COLUMN04 Lot,br.column04 Brand,iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05) MRP,
ROUND(cast((iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05)-CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))*isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0)-IIF(b.COLUMN46=22713 AND ISNULL(A.COLUMN23,0)>0,ISNULL(A.COLUMN23,0),0) as DECIMAL(18,2)),0)  DisAmt
,max(format(F43.COLUMN05,'dd/MM/yyyy')) Expiry
 from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE005 br on br.column02=it.COLUMN10 and it.columna03=br.columna03 and isnull(br.columna13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03 and isnull(F43.COLUMNA13,0)=0
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,br.column04,F43.COLUMN14,s.COLUMN05,a.COLUMN23) f order by f.COLUMN02
END
ELSE 
BEGIN
select * from(select it.column04 item,it.column06 upc,iif(a.COLUMNA03 in(56645),isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0),isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0)) quantity,a.COLUMN22 units,a.COLUMN04 serial,
a.COLUMN06 'desc',CONVERT(varchar,iif(a.COLUMNA03 in(56667,56668,56669,56697,56714,56719,56577),CAST(ISNULL(a.COLUMN13,0) AS money),CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,
isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,--t.COLUMN04 name,
CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,
--sum(isnull(vt.COLUMN07,0))  cVAT,sum(isnull(cst.COLUMN07,0)) cCST,sum(isnull(cgt.COLUMN07,0)) CGST,sum(isnull(sgt.COLUMN07,0)) SGST,sum(isnull(igt.COLUMN07,0)) IGST,
sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,
(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , 
(select column04 from MATABLE002 where column02=ht.COLUMN16) names,
((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,
M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,F43.COLUMN04 Lot,br.column04 Brand,iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05) MRP,
ROUND(cast((iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05)-CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))*isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0)-IIF(b.COLUMN46=22713 AND ISNULL(A.COLUMN23,0)>0,ISNULL(A.COLUMN23,0),0) as DECIMAL(18,2)),0)  DisAmt
,max(format(F43.COLUMN05,'dd/MM/yyyy')) Expiry
 from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE005 br on br.column02=it.COLUMN10 and it.columna03=br.columna03 and isnull(br.columna13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03 and isnull(F43.COLUMNA13,0)=0
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,br.column04,F43.COLUMN14,s.COLUMN05,a.COLUMN23) f order by f.COLUMN02
END
END
ELSE if(@TYPE='TOTAL')
BEGIN

SET @AcOwner = (SELECT COLUMNA03 FROM SATABLE009 WHERE COLUMN02 = @SONO)
if(@AcOwner in(56671,56672,56673,56728,56860,56578,56899,56731,56732,56788,56807,56816,56925))
begin
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt,sum(subtotamt1) subtotamt1 from(
select (IIF(isnull(a.COLUMN13,0)=0,0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0))) qty,
(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN35,0))) as decimal(18,2))) subtotamt,cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0)) as decimal(18,2)) subtotamt1,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 

cast(((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-((CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2))))) as decimal(18,2)) amt
from  SATABLE010 a 
inner join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46)f
END
else if(@AcOwner in(56939,56940))
begin
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt,sum(subtotamt1) subtotamt1,sum(f.cesstax)cesstax from(
select (IIF(isnull(a.COLUMN13,0)=0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0),isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0))) qty,
(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN35,0))) as decimal(18,2))) subtotamt,cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0)) as decimal(18,2)) subtotamt1,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23586,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cesstax, 
cast(((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-((CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2))))) as decimal(18,2)) amt
from  SATABLE010 a 
left join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46)f
END
else if(@AcOwner in (56688,56794))
begin
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt,sum(subtotamt1) subtotamt1 from(
select (IIF(isnull(a.COLUMN13,0)=0,0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0))) qty,
(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt,cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0)) as decimal(18,2)) subtotamt1,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-((CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2))))) as decimal(18,2)) amt
from  SATABLE010 a 
inner join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46)f
END
else if(@AcOwner in (56928,56640))
BEGIN
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt from(
select (IIF(isnull(a.COLUMN13,0)=0,0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0))) qty,
cast((isnull(a.COLUMN10 ,0)*CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull((a.COLUMN13-(a.COLUMN13*pl.COLUMN06*0.01))

,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money)) as decimal(18,2)) subtotamt,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast((isnull(a.COLUMN10 ,0)*CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull((a.COLUMN13-(a.COLUMN13*pl.COLUMN06*0.01))

,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))-iif((pl.COLUMN05=22831 or b.COLUMN46=22713),
isnull(a.COLUMN23,0),0)  as decimal(18,2)) amt
from  SATABLE010 a 
inner join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46,pl.COLUMN06,a.COLUMN23)f
END
else if(@AcOwner in (56571))
BEGIN
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt from(
select (IIF(isnull(a.COLUMN13,0)=0,0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0))) qty,cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0)) as decimal(18,2)) subtotamt,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-((CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2))))) as decimal(18,2)) amt
from  SATABLE010 a 
inner join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46
union all 


select 0 qty,0 subtotamt,
cast((isnull(b.COLUMN36 ,0)*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast((isnull(b.COLUMN36 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast((isnull(b.COLUMN36 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast((isnull(b.COLUMN36 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
0 amt
from  SATABLE009 b

left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03
and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN69,'-',''))) s)) or t.COLUMN02=b.COLUMN69) 
and t.COLUMNA03=b.COLUMNA03 and isnull(t.COLUMNA13,0)=0   
where  b.column02=@SONO  and isnull(b.COLUMNA13,0)=0 
group by b.COLUMN46,b.column36,b.COLUMN69,t.column07


union all 

select 0  qty,0 subtotamt,
cast((isnull(b.COLUMN35 ,0)*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast((isnull(b.COLUMN35 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast((isnull(b.COLUMN35 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast((isnull(b.COLUMN35 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
0 amt
from  SATABLE009 b

left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03
and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN73,'-',''))) s)) or t.COLUMN02=b.COLUMN73) 
and t.COLUMNA03=b.COLUMNA03 and isnull(t.COLUMNA13,0)=0   
where  b.column02=@SONO  and isnull(b.COLUMNA13,0)=0 
group by b.COLUMN46,b.column35,b.COLUMN73,t.column07

union all 

select 0 qty,0 subtotamt,
cast((isnull(b.COLUMN44 ,0)*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast((isnull(b.COLUMN44 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast((isnull(b.COLUMN44 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast((isnull(b.COLUMN44 ,0)*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
0 amt
from  SATABLE009 b
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03
and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN70,'-',''))) s)) or t.COLUMN02=b.COLUMN70) 
and t.COLUMNA03=b.COLUMNA03 and isnull(t.COLUMNA13,0)=0   
where  b.column02=@SONO  and isnull(b.COLUMNA13,0)=0 group by b.COLUMN46,b.column44,b.COLUMN70,t.column07


)f
END
else if(@AcOwner in (56645)) 
BEGIN
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt from(
select (IIF(isnull(a.COLUMN13,0)=0,0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0))) qty,cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0)) as decimal(18,2)) subtotamt,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-((CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2))))) as decimal(18,2)) amt
from  SATABLE010 a 
inner join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46)f
END
else BEGIN
select sum(f.qty)qty,sum(f.subtotamt)subtotamt,sum(f.totaltax)totaltax,sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.amt)amt from(
select (IIF(isnull(a.COLUMN13,0)=0,0,isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0))) qty,cast((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN13 ,0)) as decimal(18,2)) subtotamt,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*(sum(CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax, 
cast(((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-((CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2))))) as decimal(18,2)) amt
from  SATABLE010 a 
inner join SATABLE009 b on b.column01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN13,a.COLUMN35,a.COLUMN19,a.COLUMN02,pl.COLUMN05,b.COLUMN46)f
END
END
ELSE if(@TYPE='LineTax')
begin
SET @AcOwner = (SELECT COLUMNA03 FROM SATABLE009 WHERE COLUMN02 = @SONO)
if(@AcOwner in(56688))
begin
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
lt.COLUMN16 TaxType,SUM(isnull(iif(lt.COLUMN16=23582,lt.COLUMN07,0),0)) CGST,
SUM(isnull(iif(lt.COLUMN16=23583,lt.COLUMN07,0),0)) SGST,
SUM(isnull(iif(lt.COLUMN16=23584,lt.COLUMN07,0),0)) IGST,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(lt.COLUMN16=23582,lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(lt.COLUMN16=23583,lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,
 cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(lt.COLUMN16=23584,lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
(CAST(isnull(b.COLUMN20,0) as decimal(18,2))) lineamt from   SATABLE010 a inner join SATABLE009 b on  b.COLUMN01=a.COLUMN15   
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where b.column02=@SONO  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 ,a.COLUMN10,a.COLUMN35,b.COLUMN20
END 
ELSE IF (@AcOwner = 56909)
BEGIN 
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
lt.COLUMN16 TaxType,(CAST(isnull(a.COLUMN14,0) as decimal(18,2))) lineamt from   SATABLE010 a inner join SATABLE009 b on  b.COLUMN01=a.COLUMN15   
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16,a.COLUMN14
END
ELSE IF (@AcOwner = 56571)
BEGIN 
SELECT f.linetaxes as linetaxes,sum(f.linetaxamt) as linetaxamt,f.headertaxes as headertaxes,f.headertaxamt as headertaxamt,sum(f.linetotaltax)linetotaltax,f.TaxType as TaxType from(
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
lt.COLUMN16 TaxType from   SATABLE010 a inner join SATABLE009 b on  b.COLUMN01=a.COLUMN15   
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 
--shipping and pacing,other taxes and amount
union all
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
(cast(((isnull(b.COLUMN36 ,0)*(isnull(lt.COLUMN07,0))/100)) as decimal(18,2)))
linetotaltax,lt.COLUMN16 TaxType from  SATABLE009 b
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03
and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN69,'-',''))) s)) or lt.COLUMN02=b.COLUMN69) 
and lt.COLUMNA03=b.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and b.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 ,b.COLUMN36

union all 
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
(cast(((isnull(b.COLUMN44 ,0)*(isnull(lt.COLUMN07,0))/100)) as decimal(18,2)))linetotaltax,lt.COLUMN16 TaxType from 
 SATABLE009 b
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03
and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN70,'-',''))) s)) or lt.COLUMN02=b.COLUMN70) 
and lt.COLUMNA03=b.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and b.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 ,b.COLUMN44

union all 
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
(cast(((isnull(b.COLUMN35 ,0)*(isnull(lt.COLUMN07,0))/100)) as decimal(18,2)))linetotaltax,lt.COLUMN16 TaxType from  SATABLE009 b
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03
and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN73,'-',''))) s)) or lt.COLUMN02=b.COLUMN73) 
and lt.COLUMNA03=b.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and b.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 ,b.COLUMN35
union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   
where column02=@SONO and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 
union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   
where column02=@SONO and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 
union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   
where column02=@SONO and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 )f
group by f.linetaxes,f.headertaxes,f.headertaxamt,f.TaxType
END
ELSE 
BEGIN
select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,
sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,
lt.COLUMN16 TaxType from   SATABLE010 a inner join SATABLE009 b on  b.COLUMN01=a.COLUMN15   
left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    
where  b.column02=@SONO  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 
union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, 
cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    
inner join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and 
lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=b.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(b.COLUMN23,'-',''))) s)) or ht.COLUMN02=b.COLUMN23) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0   
 where  b.column02=@SONO and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(isnull(a.COLUMN21,0))  and a.COLUMNA13=0  
 group by lt.column04,lt.column07,ht.column04,ht.column07,lt.COLUMN16,ht.COLUMN16 
 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   
 where column02=@SONO and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 
 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   
 where column02=@SONO and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 
 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   
 where column02=@SONO and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 
END
END
ELSE if(@TYPE='HSNCODE')
begin
--select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(f.lineamt) lineamt,f.SerItem as SerItem from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt,isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,it.COLUMN48 SerItem from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 group by M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST
SET @AcOwner = (SELECT COLUMNA03 FROM SATABLE009 WHERE COLUMN02 = @SONO)
if(@AcOwner=56577)
BEGIN
select (f.CGST) CGST,(f.SGST) SGST,(f.IGST) IGST,SUM(f.CGSTAMT) CGSTAMT,SUM(f.SGSTAMT) SGSTAMT,SUM(f.IGSTAMT) IGSTAMT,SUM(f.lineamt) lineamt,f.SerItem as SerItem from (
select a.COLUMN02,SUM(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,
SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,
SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,
 cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
 --(CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) 
 --(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) as decimal(18,2))) 
 (CAST(isnull(a.COLUMN14,0) as decimal(18,2))) lineamt,
 isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,
 0 SerItem from  SATABLE010 a 
 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 
group by a.COLUMN02,M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN05,a.COLUMN35,a.COLUMN13,it.COLUMN48,a.COLUMN14 
) f group by (f.CGST),(f.SGST),(f.IGST),f.SerItem
END
ELSE
BEGIN
if(@AcOwner in (56688,56619))
BEGIN
select (f.CGST) CGST,(f.SGST) SGST,(f.IGST) IGST,SUM(f.CGSTAMT) CGSTAMT,SUM(f.SGSTAMT) SGSTAMT,SUM(f.IGSTAMT) IGSTAMT,SUM(f.lineamt) lineamt,f.SerItem as SerItem,f.GSTRate  as GSTRate from (
select a.COLUMN02,SUM(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,
SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,
SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,
 cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
 --(CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) 
 --(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) as decimal(18,2))) 
 (CAST(isnull(a.COLUMN14,0) as decimal(18,2))) lineamt,
 isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,
 it.COLUMN48 SerItem from  SATABLE010 a 
 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 and isnull(t.COLUMN07,0)>0 
group by a.COLUMN02,M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN05,a.COLUMN35,a.COLUMN13,it.COLUMN48,a.COLUMN14 
) f group by (f.CGST),(f.SGST),(f.IGST),f.SerItem,f.GSTRate
END
ELSE if (@AcOwner in(56790,56832))
BEGIN
select (f.CGST) CGST,(f.SGST) SGST,(f.IGST) IGST,SUM(f.CGSTAMT) CGSTAMT,SUM(f.SGSTAMT) SGSTAMT,SUM(f.IGSTAMT) IGSTAMT,SUM(f.lineamt) lineamt from (
select a.COLUMN02,SUM(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,
SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,
SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,
 cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
 --(CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) 
 --(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) as decimal(18,2))) 
 (CAST(isnull(a.COLUMN14,0) as decimal(18,2))) lineamt,
 isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,
 it.COLUMN48 SerItem from  SATABLE010 a 
 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 
group by a.COLUMN02,M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN05,a.COLUMN35,a.COLUMN13,it.COLUMN48,a.COLUMN14 
) f group by (f.CGST),(f.SGST),(f.IGST)
END
ELSE if(@AcOwner in (56909))
BEGIN
--select Round(CAST(SUM(f.HDisAmt) AS DECIMAL(18,2)),0) HDisAmt from (
--select (
--iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05)-CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))*isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0)-IIF(b.COLUMN46=22713 AND ISNULL(A.COLUMN23,0)>0,ISNULL(A.COLUMN23,0),0),
--cast((iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,ISNULL(s.COLUMN05,0)-cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2))/CAST(isnull(a.COLUMN10 ,0)as decimal(18,2)))-CAST(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money))*isnull(CAST(a.COLUMN10 AS DECIMAL(18,2)),0) as DECIMAL(18,2)) HDisAmt
--from  SATABLE010 a 
--inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
--LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
--LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03 and isnull(F43.COLUMNA13,0)=0
--left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
--where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
--group by a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,
--a.COLUMN10,a.COLUMN35,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,F43.COLUMN14,s.COLUMN05,a.COLUMN23) f
select Round(CAST(SUM(f.HDisAmt) AS DECIMAL(18,2)),2) HDisAmt  from 
(
select (iif(isnull(F43.COLUMN14,0)>0,F43.COLUMN14,s.COLUMN05) - isnull(a.COLUMN13,0))*isnull(a.COLUMN10,0)  HDisAmt FROM  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 
inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE005 br on br.column02=it.COLUMN10 and it.columna03=br.columna03 and isnull(br.columna13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0
LEFT JOIN FITABLE043 F43 ON F43.COLUMN02 = a.COLUMN31 AND F43.COLUMN09 = a.COLUMN05 AND F43.COLUMNA03 = a.COLUMNA03 and isnull(F43.COLUMNA13,0)=0
left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0
where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0
group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,
a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,F43.COLUMN04,br.column04,F43.COLUMN14,s.COLUMN05,a.COLUMN23) f
END
ELSE
BEGIN
select (f.CGST) CGST,(f.SGST) SGST,(f.IGST) IGST,SUM(f.CGSTAMT) CGSTAMT,SUM(f.SGSTAMT) SGSTAMT,SUM(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,SUM(f.lineamt) lineamt,f.SerItem as SerItem from (
select a.COLUMN02,SUM(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,
SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,
SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,
 cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, 
 --(CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) 
 --(CAST(isnull(a.COLUMN10,0)*(iif((b.COLUMN46=22713),isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) as decimal(18,2))) 
 (CAST(isnull(a.COLUMN14,0) as decimal(18,2))) lineamt,
 isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,
 it.COLUMN48 SerItem from  SATABLE010 a 
 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 
group by a.COLUMN02,M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN05,a.COLUMN35,a.COLUMN13,it.COLUMN48,a.COLUMN14 
) f group by (f.CGST),(f.SGST),(f.IGST),f.SACCode,f.HSNCode,f.SerItem
END
END
END
ELSE if(@TYPE='ITEMFAMILY')
BEGIN
SELECT '' itemfamily
END
ELSE if(@TYPE='GROUPTAX')
if(@AcOwner in (56798,56832))
Begin
select (f.CGST) CGST,SUM(f.CGSTAMT) CGSTAMT,f.taxname taxname,SUM(f.lineamt) lineamt from (
select a.COLUMN02,SUM(isnull(iif(t.COLUMN16 in(23582,23583,23584),t.COLUMN07,0),0)) CGST,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16 in(23582,23583,23584),t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
iif(sgt.COLUMN04!='',sgt.COLUMN04,st.COLUMN04) taxname,(CAST(isnull(a.COLUMN14,0) as decimal(18,2))) lineamt from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
left join MATABLE013 M13 ON M13.COLUMN02 = a.COLUMN21 and M13.COLUMNA03=a.COLUMNA03 and isnull(M13.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
left join MATABLE013 st on st.COLUMN02=a.COLUMN21 and (st.COLUMNA03=a.COLUMNA03 or isnull(st.COLUMNA03,0)=0) and isnull(st.COLUMNA13,0)=0
left join MATABLE014 sgt on sgt.COLUMN02=replace(a.COLUMN21,'-','') and sgt.COLUMNA03=a.COLUMNA03 and isnull(sgt.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 
group by a.COLUMN02,M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN05,a.COLUMN35,a.COLUMN13,it.COLUMN48,a.COLUMN14,sgt.COLUMN04,
st.COLUMN04
) f group by (f.CGST),f.taxname
END
BEGIN
select (f.CGST) CGST,SUM(f.CGSTAMT) CGSTAMT,f.taxname taxname from (
select a.COLUMN02,SUM(isnull(iif(t.COLUMN16 in(23582,23583,23584),t.COLUMN07,0),0)) CGST,
cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*sum((CAST((isnull(iif(t.COLUMN16 in(23582,23583,23584),t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, 
iif(sgt.COLUMN04!='',sgt.COLUMN04,st.COLUMN04) taxname from  SATABLE010 a 
inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 
left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 
left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 
left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 
left join MATABLE013 M13 ON M13.COLUMN02 = a.COLUMN21 and M13.COLUMNA03=a.COLUMNA03 and isnull(M13.COLUMNA13,0)=0 
LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 
LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 
left join MATABLE013 st on st.COLUMN02=a.COLUMN21 and (st.COLUMNA03=a.COLUMNA03 or isnull(st.COLUMNA03,0)=0) and isnull(st.COLUMNA13,0)=0
left join MATABLE014 sgt on sgt.COLUMN02=replace(a.COLUMN21,'-','') and sgt.COLUMNA03=a.COLUMNA03 and isnull(sgt.COLUMNA13,0)=0
LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02=@SONO  and isnull(a.COLUMNA13,0)=0 
group by a.COLUMN02,M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN05,a.COLUMN35,a.COLUMN13,it.COLUMN48,a.COLUMN14,sgt.COLUMN04,
st.COLUMN04
) f group by (f.CGST),f.taxname
END
END


























GO
