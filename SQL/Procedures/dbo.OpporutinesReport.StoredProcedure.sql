USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[OpporutinesReport]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[OpporutinesReport]
(

@ReportName     nvarchar(250)= null,
@Opportunity    nvarchar(250)= null,
@Operatingunit  nvarchar(250)= null,
@SalesPerson 	nvarchar(250)= null,
@Priority       nvarchar(250)= null,
@Status        nvarchar(250)= null,
@Date          nvarchar(250)= null,
@EscaltedTo    nvarchar(250)= null,
@Milestone       nvarchar(250)= null,
@NextFollowup    nvarchar(250)= null,
@OPUnit    nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null

)
as 
begin
if (@OperatingUnit!='' or @OperatingUnit!=null)
begin
 set @whereStr= ' where OPUnit in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s) '
end
if (@SalesPerson!='' or @SalesPerson!=null)
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where SRep in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SalesPerson+''') s) '
end
else 
begin
set @whereStr= @whereStr+' and SRep in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SalesPerson+''') s) '
end
end
if (@Priority!='' or @Priority!=null)
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where prioritty in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Priority+''') s) '
end
else 
begin
set @whereStr=@whereStr+' and prioritty in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Priority+''') s) '
end
end
if (@Status!='' or @Status!=null)
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where statuss in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Status+''') s) '
end
else 
begin
set @whereStr=@whereStr+' and statuss in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Status+''') s) '
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
end

select * into #OpporutinesReport from (select c.COLUMN04 OU,s.COLUMN04 Opportunity ,format(s.COLUMN07, 'dd/MM/yyyy') [Date]
,M.COLUMN09 
SalesPerson, m10.COLUMN09 [EscaltedTo],s1.COLUMN05 Customer,M2.COLUMN04 Milestone,M1.COLUMN04 Priority,format(s.COLUMN30,'dd/MM/yyyy') [NextFollowup],m3.COLUMN04 Status,s.COLUMN11 OPUnit,s.COLUMN15 SRep,s.COLUMN22 prioritty,s.COLUMN06 statuss from SATABLE013 s
left outer join CONTABLE007 c on c.COLUMN02 = s.COLUMN11 AND c.COLUMNA03=s.COLUMNA03 AND ISNULL(c.COLUMNA13,0)=0 
left outer join SATABLE002 s1 On s1.COLUMN02= s.COLUMN05 AND s1.COLUMNA03=s.COLUMNA03 AND ISNULL(s1.COLUMNA13,0)=0 
left outer join MATABLE010 m On M.COLUMN02= s.COLUMN15 AND s1.COLUMNA03=s.COLUMNA03 AND ISNULL(M.COLUMNA13,0)=0 
left outer join MATABLE010 m10 On M10.COLUMN02= s.COLUMN31 AND s1.COLUMNA03=s.COLUMNA03 AND ISNULL(M10.COLUMNA13,0)=0 
left outer join MATABLE002 m1 on M1.COLUMN02=s.COLUMN22 AND ISNULL(M1.COLUMNA13,0)=0
left outer join MATABLE002 m2 on M2.COLUMN02=s.COLUMN23 AND ISNULL(M2.COLUMNA13,0)=0 
left outer join MATABLE002 m3 on M3.COLUMN02=s.COLUMN06 AND ISNULL(M3.COLUMNA13,0)=0
where isnull((s.COLUMNA13),0)= 0 and s.COLUMNA03=@AcOwner and s.COLUMNA02 in 
(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)

) Query 

set @Query1='select OU,Opportunity,Date,SalesPerson,EscaltedTo,Customer, Milestone,Priority,NextFollowup,Status,OPUnit,SRep,prioritty,statuss from #OpporutinesReport'+@whereStr +' group by OU,Opportunity,Date,SalesPerson,EscaltedTo,Customer, Milestone,Priority,NextFollowup,Status,OPUnit,SRep,prioritty,statuss '
exec (@Query1)
end
GO
