USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_FI_TP_FITABLE046]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_FI_TP_FITABLE046]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,   
	@COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null,  
	@COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null,  
	@COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null,  
	@COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null,  
	@COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null,  
	@COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
    @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,   
	@COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null,  
	@COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null,  
	@COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null,  
	@COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250),
	@TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null 
)
AS
BEGIN
begin try
IF @Direction = 'Insert'
BEGIN
--EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
--set @COLUMN09= (select max(column01) from FITABLE045)
SELECT @COLUMN02=NEXT VALUE FOR DBO.FITABLE046_SequenceNo
set @COLUMNA02=( CASE WHEN (@COLUMN09!= '' and @COLUMN09 is not null and @COLUMN09!=0 ) THEN (select COLUMN16 from FITABLE045 
where COLUMN01=@COLUMN09) else @COLUMNA02  END )
set @COLUMN06=(cast(isnull(@COLUMN06,0) as DECIMAL(18,2))-((cast(isnull(@COLUMN07,0) as DECIMAL(18,2))+cast(isnull(@COLUMN08,0 )as DECIMAL(18,2)))))
declare @tempSTR nvarchar(max)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Payment Line Values are Intiated for FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+isnull( @COLUMNA06,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
insert into FITABLE046 
(
   COLUMN02,  COLUMN03,  COLUMN05,  COLUMN06,  COLUMN07,  COLUMN08,  COLUMN09,  COLUMN10,  COLUMN11,
   COLUMNA02, COLUMNA03, COLUMNA04, COLUMNA05, COLUMNA06, COLUMNA07, COLUMNA08, COLUMNA09, COLUMNA10, COLUMNA11, 
   COLUMNA12, COLUMNA13, COLUMNB01, COLUMNB02, COLUMNB03, COLUMNB04, COLUMNB05, COLUMNB06, COLUMNB07, COLUMNB08, 
   COLUMNB09, COLUMNB10, COLUMNB11, COLUMNB12, COLUMND01, COLUMND02, COLUMND03, COLUMND04, COLUMND05, COLUMND06, 
   COLUMND07, COLUMND08, COLUMND09, COLUMND10
)
values
( 
   @COLUMN02,  @COLUMN03,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMNA02, @COLUMNA03, @COLUMNA04, @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, 
   @COLUMNA12, @COLUMNA13, @COLUMNB01, @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, 
   @COLUMNB09, @COLUMNB10, @COLUMNB11, @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, 
   @COLUMND07, @COLUMND08, @COLUMND09, @COLUMND10
) 
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Payment Line Intiated Values are Created in FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )
declare @pid int, @Balance decimal(18,2), @IncAmt nvarchar(250), @TYPEID nvarchar(250), @ID nvarchar(250)
declare @Project nvarchar(250), @Totbal decimal(18,2), @DATE nvarchar(250), @vendor nvarchar(250), @memo nvarchar(250),
 @TYPE nvarchar(250),@AccType NVARCHAR(250),@NUMBER NVARCHAR(250), @Remainbal decimal(18,2), @cretAmnt decimal(18,2),@INTERNAL NVARCHAR(250)
set @TYPEID= (select COLUMN04 from FITABLE045 where column01=@COLUMN09)
set @Project= (select COLUMN09 from FITABLE045 where column01=@COLUMN09)
set @NUMBER= (select COLUMN01 from FITABLE045 where column01=@COLUMN09)
set @vendor= (select column06 from FITABLE045 where column01=@COLUMN09)
set @memo= (select column13 from FITABLE045 where column01=@COLUMN09)
set @DATE=(select column05 from FITABLE045 where column01=@COLUMN09)
set @Totbal=(select column19 from FITABLE045 where column01=@COLUMN09)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Expense Payment Updated Data in FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+'1.Header Internal ID' +','+cast(isnull(@NUMBER,'') as nvarchar(250))+','+'2.Payment Amount'+','+ cast(isnull(@COLUMN07,'') as nvarchar(250))+','+  '3.Employee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(@COLUMN07!= '0' and @COLUMN07!= '')
	begin
	update  FITABLE064 set COLUMN15=cast(@COLUMN07 as decimal(18,2))+cast(COLUMN15 as decimal(18,2)) where COLUMN11=2000 and COLUMN05 in(@NUMBER) and COLUMN03='EXPENSE'  and COLUMN04=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	update  PUTABLE016 set COLUMN13=cast(@COLUMN07 as decimal(18,2))+cast(COLUMN13 as decimal(18,2)),COLUMN14=cast(@COLUMN07 as decimal(18,2))+cast(COLUMN14 as decimal(18,2)) where COLUMN03=2000 and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	if(cast(@COLUMN06 as decimal(18,2))=0 or cast(@COLUMN06 as decimal(18,2))=0.00)
	begin
	update  PUTABLE016 set COLUMN10='CLOSE' where COLUMN03=2000 and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	end
	DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1 
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor AND FITABLE020.COLUMN16='OPEN' AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN11) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id!='' and @id is not null)
		     BEGIN 
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363'  AND FITABLE020.COLUMN07=@vendor AND FITABLE020.COLUMN16='OPEN' and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN11) s)
			   AND ISNULL(FITABLE020.COLUMNA13,0)=0 )
			   WHILE @Initialrow <= @MaxRownum
			   BEGIN
		        if(@COLUMN07!='0' and cast(@COLUMN07 as decimal(18,2))>0)
			  begin
				set @cretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 		 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 'Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN07 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(0), COLUMN16='CLOSE'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					set @COLUMN07=(cast(@COLUMN07 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN07 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
					update FITABLE020 set column18=(cast(isnull(column18,0) as DECIMAL(18,2))-cast(isnull(@COLUMN07,0) as DECIMAL(18,2))), 
					COLUMN16='OPEN'  where column02= @id and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 set @COLUMN07='0'
				 end
				end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1 
				END
				CLOSE curA 
				deallocate curA
			END
		END
		declare @Vid1 nvarchar(250)
select @COLUMN07=column07,@INTERNAL=column01 from FITABLE046 where column02=@COLUMN02
	if(cast(isnull(@COLUMN07,0) as decimal(18,2))> 0)
	begin
	exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Expense',  @COLUMN04 = @TYPEID, @COLUMN05 = @NUMBER,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @DATE,   @COLUMN08 = '22492',  @COLUMN09 = @vendor,    @COLUMN10 = '22402', @COLUMN11 = '1120',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN07,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		set @Vid1=((select isnull(max(column02),999) from FITABLE034)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@Vid1,999)as nvarchar(250)) +','+ isnull(cast(@DATE as nvarchar(250)),'')+','+ 'ADVANCE PAYMENT'+','+ isnull(cast(@NUMBER as nvarchar(250)),'')+','+ isnull(cast(@vendor as nvarchar(250)),'')+','+ '1120'+','+isnull( cast(@TYPEID as nvarchar(250)),'')+','+isnull( cast(@COLUMN07 as nvarchar(250)),'')+','+  isnull(cast(@COLUMN07 as nvarchar(250)),'')+','+'Advances Paid' +','+isnull(cast(@Project as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
		--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
		-- values(@Vid1,@DATE,'Expense',@NUMBER,@vendor,@memo,'1120',@TYPEID,@COLUMN07,@COLUMN07,'Advances Paid',@Project,@COLUMNA02, @COLUMNA03)
	insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,   COLUMN08,COLUMN07,   COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@Vid1,999),  'Expense',  @DATE,  @memo,  @vendor,  @COLUMN07,0,@TYPEID,'1120',@Project,
	@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''))	
					end	
set @ReturnValue = 1

END

ELSE IF @Direction = 'Select'
BEGIN
select * from FITABLE046
END 

ELSE IF @Direction = 'Update'
BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN09!= '' and @COLUMN09 is not null and @COLUMN09!=0 ) THEN (select COLUMN16 from FITABLE045 
where COLUMN01=@COLUMN09) else @COLUMNA02  END )
declare @padv decimal(18,2), @pamt decimal(18,2)
set @padv=(select COLUMN07 from FITABLE046 where COLUMN02=@COLUMN02);
set @pamt=(select COLUMN08 from FITABLE046 where COLUMN02=@COLUMN02);
set @COLUMN06=(select COLUMN06 from FITABLE046 where COLUMN02=@COLUMN02);
set @COLUMN11=(select COLUMN11 from FITABLE046 where COLUMN02=@COLUMN02);
--set @COLUMN06=(cast(@COLUMN05 as DECIMAL(18,2))-(cast(isnull(@COLUMN07,0) as DECIMAL(18,2))+(cast(isnull(@padv,0) as DECIMAL(18,2))-cast(isnull(@COLUMN08,0 )as DECIMAL(18,2)))+cast(isnull(@pamt,0 )as DECIMAL(18,2))));
if(@COLUMN06!= '' and @COLUMN06 is not null)
begin
set @COLUMN06=(cast(@COLUMN06 as DECIMAL(18,2))+(@pamt-cast(@COLUMN08 as DECIMAL(18,2))+(@padv-cast(isnull(@COLUMN07,0 )as DECIMAL(18,2)))));
end
--set @COLUMN06=(cast(isnull(@COLUMN06,0) as DECIMAL(18,2))-((cast(isnull(@COLUMN07,0) as DECIMAL(18,2))+cast(isnull(@COLUMN08,0 )as DECIMAL(18,2)))))
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Expense Payment Line Values are Intiated for FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   isnull(@COLUMN02 ,'')+','+isnull(  @COLUMN03,'')+','+isnull(  @COLUMN04,'')+','+isnull( @COLUMN05,'')+','+isnull(  @COLUMN06,'')+','+isnull(  @COLUMN07,'')+','+isnull(  @COLUMN08,'')+','+isnull(  @COLUMN09,'')+','+isnull(  @COLUMN10,'')+','+
   isnull(@COLUMNA01,'')+','+isnull( @COLUMNA02,'')+','+isnull( @COLUMNA03,'')+','+isnull( @COLUMNA04,'')+','+isnull( @COLUMNA05,'')+','+					 
   isnull(@COLUMNA07,'')+','+isnull( @COLUMNA08,'')+','+isnull( @COLUMNA09,'')+','+isnull(@COLUMNA10,'')+','+isnull( @COLUMNA11,'')+','+isnull( @COLUMNA12,'')+','+isnull( @COLUMNA13,'')+','+isnull( @COLUMNB01,'')+','+isnull( @COLUMNB02,'')+','+isnull( @COLUMNB03,'')+','+ 
   isnull(@COLUMNB04,'')+','+isnull( @COLUMNB05,'')+','+isnull( @COLUMNB06,'')+','+isnull(@COLUMNB07,'')+','+isnull( @COLUMNB08,'')+','+isnull( @COLUMNB09,'')+','+isnull( @COLUMNB10,'')+','+isnull( @COLUMNB11,'')+','+isnull( @COLUMNB12,'')+','+isnull( @COLUMND01,'')+','+ 
   isnull(@COLUMND02,'')+','+isnull( @COLUMND03,'')+','+isnull( @COLUMND04,'')+','+isnull(@COLUMND05,'')+','+isnull( @COLUMND06,'')+','+isnull( @COLUMND07,'')+','+isnull( @COLUMND08,'')+','+isnull( @COLUMND09,'')+','+isnull( @COLUMND10,'')+''+ ' ')
   )
UPDATE FITABLE046 SET
   COLUMN02=@COLUMN02,    COLUMN03=@COLUMN03,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06,    COLUMN07=@COLUMN07,
   COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,     COLUMNA01=@COLUMNA01, 
   COLUMNA02=@COLUMNA02,  COLUMNA03=@COLUMNA03,  COLUMNA04=@COLUMNA04,  COLUMNA05=@COLUMNA05, 
   COLUMNA07=@COLUMNA07,  COLUMNA08=@COLUMNA08,  COLUMNA09=@COLUMNA09,  COLUMNA10=@COLUMNA10,  COLUMNA11=@COLUMNA11,
   COLUMNA12=@COLUMNA12,  COLUMNA13=@COLUMNA13,  COLUMNB01=@COLUMNB01,  COLUMNB02=@COLUMNB02,  COLUMNB03=@COLUMNB03,
   COLUMNB04=@COLUMNB04,  COLUMNB05=@COLUMNB05,  COLUMNB06=@COLUMNB06,  COLUMNB07=@COLUMNB07,  COLUMNB08=@COLUMNB08,
   COLUMNB09=@COLUMNB09,  COLUMNB10=@COLUMNB10,  COLUMNB11=@COLUMNB11,  COLUMNB12=@COLUMNB12,  COLUMND01=@COLUMND01, 
   COLUMND02=@COLUMND02,  COLUMND03=@COLUMND03,  COLUMND04=@COLUMND04,  COLUMND05=@COLUMND05,  COLUMND06=@COLUMND06,
   COLUMND07=@COLUMND07,  COLUMND08=@COLUMND08,  COLUMND09=@COLUMND09,  COLUMND10=@COLUMND10
   WHERE COLUMN02 = @COLUMN02
 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET1***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Expense Payment Line Intiated Values are Created in FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+' ')
   )

set @TYPEID= (select COLUMN04 from FITABLE045 where column01=@COLUMN09)
set @Project= (select COLUMN09 from FITABLE045 where column01=@COLUMN09)
set @NUMBER= (select COLUMN01 from FITABLE045 where column01=@COLUMN09)
set @vendor= (select column06 from FITABLE045 where column01=@COLUMN09)
set @memo= (select column13 from FITABLE045 where column01=@COLUMN09)
set @DATE=(select column05 from FITABLE045 where column01=@COLUMN09)
set @Totbal=(select column19 from FITABLE045 where column01=@COLUMN09)

set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET2***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Line Update:Expense Payment Updated Data in FITABLE046 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+'1.Header Internal ID' +','+cast(isnull(@NUMBER,'') as nvarchar(250))+','+'2.Payment Amount'+','+ cast(isnull(@COLUMN07,'') as nvarchar(250))+','+  '3.Employee'+','+ cast(isnull(@vendor,'') as nvarchar(250))+','+  cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(@COLUMN07!= '0' and @COLUMN07!= '')
	begin
	update  FITABLE064 set COLUMN15=cast(@COLUMN07 as decimal(18,2))+cast(COLUMN15 as decimal(18,2)) where COLUMN11=2000 and COLUMN05 in(@NUMBER) and COLUMN03='EXPENSE'  and COLUMN04=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
    update  PUTABLE016 set COLUMN13=cast(@COLUMN07 as decimal(18,2))+cast(COLUMN13 as decimal(18,2)),COLUMN14=cast(@COLUMN07 as decimal(18,2))+cast(COLUMN14 as decimal(18,2)) where COLUMN03=2000 and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	if(cast(@COLUMN06 as decimal(18,2))=0 or cast(@COLUMN06 as decimal(18,2))=0.00)
	begin
	update  PUTABLE016 set COLUMN10='CLOSE' where COLUMN03=2000 and COLUMN05 in(@NUMBER) and COLUMN06='EXPENSE'  and COLUMN09=@TYPEID and COLUMNA02=@COLUMNA02 AND COLUMNA03=@COLUMNA03
	end
	set @Initialrow=(1)
			  DECLARE curA CURSOR FOR select  FITABLE020.COLUMN02 from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363' AND FITABLE020.COLUMN07=@vendor AND FITABLE020.COLUMN16='OPEN' AND ISNULL(FITABLE020.COLUMNA13,0)=0 and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN11) s)
			  order by FITABLE020.column02 asc
		  OPEN curA
		  FETCH NEXT FROM curA INTO @id
		  if(@id!='' and @id is not null)
		     BEGIN 
			  set @MaxRownum=(select  COUNT(*) from FITABLE020 
			  INNER JOIN FITABLE022 ON FITABLE022.COLUMN09=FITABLE020.COLUMN01 AND ISNULL(FITABLE022.COLUMNA13,0)=0 
			  WHERE  FITABLE020.COLUMNA03=@COLUMNA03 AND FITABLE020.COLUMNA02=@COLUMNA02 AND FITABLE020.COLUMN03='1363'  AND FITABLE020.COLUMN07=@vendor AND FITABLE020.COLUMN16='OPEN' and FITABLE020.COLUMN02 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@COLUMN11) s)
			   AND ISNULL(FITABLE020.COLUMNA13,0)=0 )
			   WHILE @Initialrow <= @MaxRownum
			   BEGIN
		        if(@COLUMN07!='0' and cast(@COLUMN07 as decimal(18,2))>0)
			  begin
				set @cretAmnt=(select column18 from FITABLE020 where column02= @id  
				 and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02 and isnull(COLUMNA13,0)=0)
				 		 set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET3***********************'+   '' + CHAR(13)+CHAR(10) + '' +
				 'Line Update:Advance Amount in FITABLE020 Table '+'' + CHAR(13)+CHAR(10) + ''+
				 'The Values are '+'1.Advance Amount' +','+cast(isnull(@cretAmnt,'') as nvarchar(250))+','+'2.Id'+','+ cast(isnull(@id,'') as nvarchar(250))+','+ cast(isnull(@COLUMNA02,'') as nvarchar(250))+','+cast(isnull(@COLUMNA03,'') as nvarchar(250)) +  '  '))
	if(cast(@COLUMN07 as DECIMAL(18,2))>=@cretAmnt)
				begin
					update FITABLE020 set column18=(0), COLUMN16='CLOSE'  where column02= @id  
					and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
					set @COLUMN07=(cast(@COLUMN07 as decimal(18,2))-@cretAmnt)
				enD
				 else if(cast(@COLUMN07 as DECIMAL(18,2))<@cretAmnt)				 
				 begin
					update FITABLE020 set column18=(cast(isnull(column18,0) as DECIMAL(18,2))-cast(isnull(@COLUMN07,0) as DECIMAL(18,2))), 
					COLUMN16='OPEN'  where column02= @id and COLUMNA03=@COLUMNA03 and COLUMNA02=@COLUMNA02   and isnull(COLUMNA13,0)=0
				 set @COLUMN07='0'
				 end
				end
				FETCH NEXT FROM curA INTO @id
				SET @Initialrow = @Initialrow + 1 
				END
				CLOSE curA 
				deallocate curA
			END
		END
select @COLUMN07=column07,@INTERNAL=column01 from FITABLE046 where column02=@COLUMN02
	if(cast(isnull(@COLUMN07,0) as decimal(18,2))> 0)
	begin
	--EMPHCS1431 rajasekhar reddy patakota 23/12/2015 Advance Payment and Advance Received COA Changes
		exec [usp_FIN_TP_FITABLE064] @COLUMN03 = 'Expense',  @COLUMN04 = @TYPEID, @COLUMN05 = @NUMBER,  @COLUMN06 = @INTERNAL,
		@COLUMN07 = @DATE,   @COLUMN08 = '22492',  @COLUMN09 = @vendor,    @COLUMN10 = '22402', @COLUMN11 = '1120',
		@COLUMN12 = @memo,   @COLUMN13 = @Project,	@COLUMN14 = @COLUMN07,       @COLUMN15 = 0,  @COLUMNA02  = @COLUMNA02,
		@COLUMNA03  = @COLUMNA03, 		          @COLUMNA06  = @COLUMNA06,              	 @COLUMNA08  = @COLUMNA08,   
		@COLUMNA09  = @COLUMNA09,				  @COLUMNA12  = '1',                         @COLUMNA13  = '0'
set @Vid1=((select isnull(max(column02),999) from FITABLE034)+1)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''+
   'The Values are '+
   cast(isnull(@Vid1,999)as nvarchar(250)) +','+ isnull(cast(@DATE as nvarchar(250)),'')+','+ 'ADVANCE PAYMENT'+','+ isnull(cast(@NUMBER as nvarchar(250)),'')+','+ isnull(cast(@vendor as nvarchar(250)),'')+','+ '1120'+','+isnull( cast(@TYPEID as nvarchar(250)),'')+','+isnull( cast(@COLUMN07 as nvarchar(250)),'')+','+  isnull(cast(@COLUMN07 as nvarchar(250)),'')+','+'Advances Paid' +','+isnull(cast(@Project as nvarchar(250)),'') +','+isnull(cast(@COLUMNA02 as nvarchar(250)),'') +','+isnull(cast(@COLUMNA03 as nvarchar(250)),'') +  '  '))
		--Insert into FITABLE026(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN12,  COLUMN14, COLUMN17, COLUMN19, COLUMNA02, COLUMNA03)
		-- values(@Vid1,@DATE,'Expense',@NUMBER,@vendor,@memo,'1120',@TYPEID,@COLUMN07,@COLUMN07,'Advances Paid',@Project,@COLUMNA02, @COLUMNA03)
		insert into FITABLE034 
	(COLUMN02,  COLUMN03,  COLUMN04,  COLUMN05,  COLUMN06,   COLUMN08,COLUMN07,   COLUMN09,COLUMN10,COLUMN12,
	COLUMNA02, COLUMNA03)
	values
	( ISNULL(@Vid1,999),  'Expense',  @DATE,  @memo,  @vendor,  @COLUMN07,0,@TYPEID,'1120',@Project,
	@COLUMNA02, @COLUMNA03)
set @tempSTR=(isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10)+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************TARGET4***********************'+   '' + CHAR(13)+CHAR(10) + '' +
					'Advance paid Values are Intiated for FITABLE034 Table '+'' + CHAR(13)+CHAR(10) + ''))	
					end	
set @ReturnValue = 1

END

ELSE IF @Direction = 'Delete'
BEGIN
UPDATE FITABLE046 SET COLUMNA13=@COLUMNA13 WHERE COLUMN02 = @COLUMN02
END
 exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE046.txt',0
end try
begin catch
SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
		set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
    exec [CheckDirectory] @tempSTR,'usp_FI_TP_FITABLE046.txt',0
end catch
end





GO
