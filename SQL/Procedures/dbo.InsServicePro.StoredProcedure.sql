USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[InsServicePro]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE proc [dbo].[InsServicePro]
 (
         @COLUMN02 bigint=null,
		 @COLUMN03 nvarchar(250)=null,
		 @COLUMN04 nvarchar(250)=null,
		 @COLUMN05 int=null,
		 @COLUMN06 nvarchar(250)=null,
		 @COLUMN07 nvarchar(250)=null,
		 @COLUMN08 nvarchar(250)=null,
		 @COLUMN09 nvarchar(250)=null,
		 @COLUMN10 nvarchar(250)=null,
		 @COLUMN11 nvarchar(250)=null,
		 @COLUMN12 nvarchar(250)=null,
		 @COLUMN13 date=null,
		 @COLUMN14 date=null,
		 @COLUMN15 nvarchar(250)=null,
		 @COLUMN16 int=null,
		 @column17 nvarchar(250)=null,
		 @column18 nvarchar(250)=null,
		 @column19 nvarchar(250)=null,
		 @column20 nvarchar(250)=null,
		 @COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
		 @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null, 
		 @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null,   @COLUMNA09  nvarchar(250)=null, 
		 @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null,   @COLUMNA12  nvarchar(250)=null, 
		 @COLUMNA13  nvarchar(250)=0,     @COLUMNB01  nvarchar(250)=null,  @COLUMNB02  nvarchar(250)=null, 
		 @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  @COLUMNB05  nvarchar(250)=null, 
		 @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  @COLUMNB08  nvarchar(250)=null,
		 @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null,  @COLUMNB11  varchar(100)=null,  
		 @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  @COLUMND02  nvarchar(250)=null, 
		 @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  @COLUMND05  nvarchar(250)=null, 
		 @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  @COLUMND08  nvarchar(250)=null, 
		 @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   @Direction  nvarchar(250)  		 			
 )
 As
 Begin
 begin try
  IF @Direction = 'GetSpList'  
    begin
	 Select COLUMN02,COLUMN03 as 'ServiceProvider',COLUMN04 as 'Description',COLUMN05 as 'Priority',COLUMN06 as 'UserName',COLUMN07 as 'PassWord',COLUMN08 as 'SenderId',COLUMN09 as 'SendSMSUrl',COLUMN10 as 'SentReportUrl',COLUMN11 as 'ChechBalanceUrl',COLUMN12 as 'MobileNo',COLUMN13 as 'FromDate',COLUMN14 as 'ToDate',COLUMN15 as 'Message',COLUMN12 as 'MessagegType',COLUMN17 as 'AuthenticationKey',COLUMN18 as 'APIKey',COLUMN19 as 'PortNo' from SETABLE023 where COLUMNA03=@COLUMNA03 and COLUMNA13=@COLUMNA13
    end
 IF @Direction = 'Insert'
    begin
	declare @priority int=0
set @priority = (select count(*) COLUMN05 from SETABLE023 WHERE COLUMN05 = @COLUMN05)
	 if(@priority >1) 
         begin
		 update SETABLE023 set COLUMN05=0 where COLUMN05 = @COLUMN05 and COLUMNA03=@COLUMNA03
		 end

		Insert into SETABLE023 (COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMN17,COLUMN18,COLUMN19,COLUMN20,COLUMNA03,COLUMNA13 )
	       values ( @COLUMN03, @COLUMN04, @COLUMN05, @COLUMN06, @COLUMN07, @COLUMN08, @COLUMN09, @COLUMN10, @COLUMN11, @COLUMN12, @COLUMN13, @COLUMN14, @COLUMN15, @COLUMN16, @COLUMN17, @COLUMN18, @COLUMN19,@COLUMN20,@COLUMNA03,@COLUMNA13)
    end
IF @Direction = 'UpdateGet'  
    begin
	 select COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN11,COLUMN12,COLUMN13,COLUMN14,COLUMN15,COLUMN16,COLUMN17,COLUMN18,COLUMN19
            from SETABLE023 where COLUMN02=@COLUMN02 and COLUMNA03=@COLUMNA03 

    end
 IF @Direction = 'UpdatePost'
    begin 
	 set @priority = (select count(*) COLUMN05 from SETABLE023 WHERE COLUMN05 = @COLUMN05)
	 if(@priority >1) 
         begin
		 update SETABLE023 set COLUMN05=0 where COLUMN05 = @COLUMN05 and COLUMNA03=@COLUMNA03
		 end
	   update SETABLE023 set
       COLUMN03=@COLUMN03,    COLUMN04=@COLUMN04,    COLUMN05=@COLUMN05,    COLUMN06=@COLUMN06, 
       COLUMN07=@COLUMN07,    COLUMN08=@COLUMN08,    COLUMN09=@COLUMN09,    COLUMN10=@COLUMN10,    COLUMN11=@COLUMN11,   
       COLUMN12=@COLUMN12,    COLUMN13=@COLUMN13,    COLUMN14=@COLUMN14,    COLUMN15=@COLUMN15,    COLUMN16=@COLUMN16,
	   COLUMN17=@column17,    COLUMN18= @column18,    COLUMN19=@column19
       where COLUMNA03=@COLUMNA03 and COLUMN02=@COLUMN02
	end
 else IF @Direction = 'Delete'
    begin

	update SETABLE023 set COLUMNA13=1 where COLUMN02=@COLUMN02

	end 

end try

begin catch
    
end catch

  end


GO
