USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[BarCodeWiseStock]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[BarCodeWiseStock] 
(
@Item nvarchar(250)=null,
@UPC nvarchar(250)=null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@DateF nvarchar(250)=null
)
as
begin
declare @whereStr nvarchar(1000)=null
declare @Query1 nvarchar(max)=null 
if @Item!='' 
begin
 set @whereStr= 'and p.ItemId in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr='and p.UPC ='''+@UPC+''''
end
else
begin
set @whereStr=@whereStr+' and p.UPC ='''+@UPC+''''
end
end
exec ('
SELECT o.COLUMN03 OperatingUnit,p.UPC,p.Item,p.ItemId,u.COLUMN04 uom,sum(p.BQTY)''BillQty'',sum(p.IQTY) ''InvoiceQty'',sum(p.RQTY) ''ReturnQty'',sum(p.AQTY) ''AdjustmentQty'',sum(p.QTY) ''ClosingQty'',(pp.COLUMN04) ''PurchasePrice'',(sp.COLUMN04) ''SalesPrice''
 from
(
select ph.COLUMN15 OperatingUnit,a.column04 ''UPC'',i.column04 ''Item'',pl.column04 ''ItemId'',pl.COLUMN19 ''uom'',count(*) ''BQTY'',0 ''IQTY'',0 ''RQTY'',0 ''AQTY'',count(*) ''QTY'' ,0 ''QuantityOnHand'',0 ''PurchasePrice'', 0 ''SalesPrice''
from matable021 a inner join putable006 pl on pl.column02=a.column12 and pl.columnA03=a.columnA03 and isnull(pl.columnA13,0)=0 
inner join putable005 ph on ph.column01=pl.column13 and pl.columnA03=ph.columnA03 and  pl.columnA02=ph.columnA02 and isnull(ph.columnA13,0)=0 
inner join matable007 i on i.column02=pl.column04 and pl.columnA03=i.columnA03 and isnull(i.columnA13,0)=0 where a.columnA03='+@AcOwner+' and isnull(a.columnA13,0)=0
group by ph.COLUMN15,a.column04,i.column04,pl.column04,pl.COLUMN19
union all
select ph.COLUMN14 OperatingUnit,pl.columnB01 ''UPC'',i.column04 ''Item'',pl.column05 ''ItemId'',pl.COLUMN22 ''uom'',0 ''BQTY'',(sum(isnull(pl.column10,0))) ''IQTY'',0 ''RQTY'',0 ''AQTY'',-(sum(isnull(pl.column10,0))) ''QTY'',0 ''QuantityOnHand'',0 ''PurchasePrice'', 0 ''SalesPrice''
from satable010 pl inner join satable009 ph on ph.column01=pl.column15 and pl.columnA03=ph.columnA03 and pl.columnA02=ph.columnA02 and  isnull(ph.columnA13,0)=0 
inner join matable007 i on i.column02=pl.column05 and pl.columnA03=i.columnA03 and isnull(i.columnA13,0)=0 where pl.columnA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and pl.columnA03='+@AcOwner+' and isnull(pl.columnA13,0)=0
group by ph.COLUMN14,pl.columnB01,i.column04,pl.column05,pl.COLUMN22
union all
select ph.COLUMN24 OperatingUnit,pl.columnB01 ''UPC'',i.column04 ''Item'',pl.column03 ''ItemId'',pl.COLUMN27 ''uom'',0 ''BQTY'',0 ''IQTY'',sum(isnull(pl.column07,0)) ''RQTY'',0 ''AQTY'',sum(isnull(pl.column07,0)) ''QTY'' ,0 ''QuantityOnHand'',0 ''PurchasePrice'', 0 ''SalesPrice''
from satable006 pl inner join satable005 ph on ph.column01=pl.column19 and pl.columnA03=ph.columnA03 and pl.columnA02=ph.columnA02 and isnull(ph.columnA13,0)=0 
inner join matable007 i on i.column02=pl.column03 and pl.columnA03=i.columnA03 and isnull(i.columnA13,0)=0 where pl.columnA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and pl.columnA03='+@AcOwner+' and isnull(pl.columnA13,0)=0
group by ph.COLUMN24,pl.columnB01,i.column04,pl.column03,pl.COLUMN27
union all
select ph.COLUMN24 OperatingUnit,pl.columnB01 ''UPC'',i.column04 ''Item'',pl.column03 ''ItemId'',pl.COLUMN26 ''uom'',0 ''BQTY'',0 ''IQTY'',-(sum(isnull(pl.column07,0))) ''RQTY'',0 ''AQTY'',-(sum(isnull(pl.column07,0))) ''QTY'' ,0 ''QuantityOnHand'',0 ''PurchasePrice'', 0 ''SalesPrice''
from putable002 pl inner join putable001 ph on ph.column01=pl.column19 and pl.columnA03=ph.columnA03 and pl.columnA02=ph.columnA02 and isnull(ph.columnA13,0)=0 
inner join matable007 i on i.column02=pl.column03 and pl.columnA03=i.columnA03 and isnull(i.columnA13,0)=0 where pl.columnA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and pl.columnA03='+@AcOwner+' and isnull(pl.columnA13,0)=0
group by ph.COLUMN24,pl.columnB01,i.column04,pl.column03,pl.COLUMN26
union all
select ph.COLUMN10 OperatingUnit,pl.column04 ''UPC'',i.column04 ''Item'',pl.column03 ''ItemId'',pl.COLUMN22 ''uom'',0 ''BQTY'',0 ''IQTY'',0 ''RQTY'',sum(isnull(pl.COLUMN08,0)-isnull(pl.COLUMN16,0)) ''AQTY'',
sum(isnull(pl.COLUMN08,0)-isnull(pl.COLUMN16,0))''QTY'' ,0 ''QuantityOnHand'',0 ''PurchasePrice'', 0 ''SalesPrice'' 
from FITABLE015 pl inner join FITABLE014 ph on ph.column01=pl.column11 and pl.columnA03=ph.columnA03 and pl.columnA02=ph.columnA02 and isnull(ph.columnA13,0)=0 
inner join matable007 i on i.column02=pl.column03 and pl.columnA03=i.columnA03 and isnull(i.columnA13,0)=0 where pl.columnA02 in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPUnit+''') s) and pl.columnA03='+@AcOwner+' and isnull(pl.columnA13,0)=0
group by ph.COLUMN10,pl.column04,i.column04,pl.column03,pl.COLUMN22) p 
left join MATABLE024 sp on sp.COLUMN07=p.ItemId and sp.COLUMN06=''Sales'' and (isnull(sp.COLUMN03,0)='+@OPUnit+'  or isnull(sp.COLUMN03,0)=0) and sp.COLUMNA03='+@AcOwner+' and sp.COLUMNA13=0 
left join MATABLE024 pp on pp.COLUMN07=p.ItemId and pp.COLUMN06=''Purchase'' and (isnull(pp.COLUMN03,0)='+@OPUnit+'  or isnull(pp.COLUMN03,0)=0) and pp.COLUMNA03='+@AcOwner+' and pp.COLUMNA13=0 
left join MATABLE002 u on u.COLUMN02=p.uom and u.COLUMNA03='+@AcOwner+' and isnull(u.COLUMNA13,0)=0 		
left join CONTABLE007 o on o.COLUMN02=p.OperatingUnit and o.COLUMNA03='+@AcOwner+' and isnull(o.COLUMNA13,0)=0 		
where p.UPC!='''' and p.UPC!=''0'' '+@whereStr+'  group by o.COLUMN03,p.UPC,p.Item,p.ItemId,u.COLUMN04,o.COLUMN03,pp.COLUMN04,sp.COLUMN04')
end
GO
