USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_EX_TP_PAYMENT_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_EX_TP_PAYMENT_LINE_DATA]
(@ExpenseID nvarchar(250)=null,@Employee nvarchar(250)=null, @AcOwner int=null, @OPUNIT int=null)
AS
BEGIN

declare @DueAmnt decimal(18,2), @PayAmnt decimal(18,2),@FrDate date = '1/1/2012',@ToDate date = '1/1/2200'
 select top 1 @FrDate=column04,@ToDate=column05 from FITABLE048 where COLUMNA03= @AcOwner and column09 = '1' and isnull(columna13,0)=0 
if(@ExpenseID !=0 and @ExpenseID !='')
begin
set @DueAmnt=(select isnull(min(COLUMN06),0) from FITABLE046 where COLUMN03=@ExpenseID and isnull(COLUMNA13,0)=0)
if(@DueAmnt>0)
begin  
select b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN24),0) as COLUMN05, isnull(min(a.COLUMN06),0) as COLUMN06
from FITABLE046 a 
	inner join FITABLE045 c on c.COLUMN01=a.COLUMN09
	inner join FITABLE012 b on a.COLUMN03=b.COLUMN02 and isnull(b.COLUMNA13,'False')='False' AND B.COLUMN09 <= @ToDate
	inner join FITABLE021 d on b.COLUMN01=d.COLUMN06
	 WHERE  a.COLUMN03=@ExpenseID and isnull(a.COLUMNA13,'False')='False' group by b.COLUMN02
end 
else if(@DueAmnt=(0.00) and (select isnull(max(COLUMN05),0) from FITABLE046 where COLUMN03=@ExpenseID and isnull(COLUMNA13,0)=0)>0)
begin  
select b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN24),0) as COLUMN05, 0 as COLUMN06
from FITABLE046 a 
	inner join FITABLE045 c on c.COLUMN01=a.COLUMN09
	inner join FITABLE012 b on a.COLUMN03=b.COLUMN02 and isnull(b.COLUMNA13,'False')='False' AND B.COLUMN09 <= @ToDate
	inner join FITABLE021 d on b.COLUMN01=d.COLUMN06
	 WHERE  a.COLUMN03=@ExpenseID and isnull(a.COLUMNA13,'False')='False' group by b.COLUMN02
end
else
begin
select b.COLUMN02 as COLUMN03, isnull(max(b.COLUMN24),0) as COLUMN05, isnull(max(b.COLUMN24),0) as COLUMN06
from FITABLE012 b 
	--inner join FITABLE045 c on c.COLUMN01=a.COLUMN09
	--inner join FITABLE012 b on a.COLUMN03=b.COLUMN02
	inner join FITABLE021 d on b.COLUMN01=d.COLUMN06
	 WHERE  b.COLUMN02=@ExpenseID and isnull(b.COLUMNA13,'False')='False' AND B.COLUMN09 <= @ToDate group by b.COLUMN02
end
ENd
else
begin
set @DueAmnt=(select (isnull(sum(COLUMN07),0)+isnull(sum(COLUMN08),0)) AS COLUMN05 from FITABLE046 WHERE  COLUMN09 in(select isnull((COLUMN01),0) 
from FITABLE045 where COLUMN06 in(@Employee)  and isnull(columna13,0)=0)  and isnull(columna13,0)=0 )
if exists(select COLUMN06 from FITABLE045 where COLUMN06 in(@Employee) and isnull(COLUMNA13,0)=0)
begin
select a.COLUMN02 as COLUMN03, isnull(max(a.COLUMN24),0) as COLUMN05,isnull(max(a.COLUMN24),0)-isnull(sum(b.COLUMN07),0)-isnull(sum(b.COLUMN08),0) as COLUMN06
from FITABLE012 a 
	--inner join FITABLE021 d on a.COLUMN01=d.COLUMN06 
	left join FITABLE046 b on b.COLUMN03=a.COLUMN02 and isnull(b.columna13,0)=0
	left join FITABLE045 c on c.COLUMN01=b.COLUMN09 and c.COLUMN06 in(@Employee)  and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN16 in(@Employee) and  isnull(a.COLUMNA13,'False')='False' AND A.COLUMN09 <= @ToDate   group by a.COLUMN02 having (isnull(max(a.COLUMN24),0)-isnull(sum(b.COLUMN07),0)-isnull(sum(b.COLUMN08),0))>0
END
ELSE
begin
select a.COLUMN02 as COLUMN03, isnull(max(a.COLUMN24),0) as COLUMN05,isnull(max(a.COLUMN24),0)-isnull(min(b.COLUMN06),0) as COLUMN06
from FITABLE012 a 
	inner join FITABLE021 d on a.COLUMN01=d.COLUMN06
	left join FITABLE046 b on b.COLUMN03=a.COLUMN02 and isnull(b.columna13,0)=0
	left join FITABLE045 c on c.COLUMN01=b.COLUMN09 and c.COLUMN06=a.COLUMN16  and isnull(c.columna13,0)=0
	 WHERE  a.COLUMN16 in(@Employee) and isnull(a.COLUMNA13,'False')='False' AND A.COLUMN09 <= @ToDate  group by a.COLUMN02 having (isnull(max(a.COLUMN24),0)-isnull(min(b.COLUMN06),0))>0
ENd
END
END  

GO
