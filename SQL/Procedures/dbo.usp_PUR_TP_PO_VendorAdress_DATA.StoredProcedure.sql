USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PO_VendorAdress_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TP_PO_VendorAdress_DATA]
(
	@vendorId int,@accntId int ,@Opunit nvarchar(250)=null
)

AS
BEGIN


	SELECT a.COLUMNA03,a.COLUMN01,a.COLUMN06,a.COLUMN07,a.COLUMN08,a.COLUMN09,a.COLUMN10,a.COLUMN11,a.COLUMN12,a.COLUMN16,a.COLUMN17,a.COLUMN18,
	a.COLUMN21,a.COLUMN22,a.COLUMN23,a.COLUMN24,v.COLUMN14 as HostId,v.COLUMN15 as HostIntId,v.COLUMN16 as HostExtId,v.COLUMN18 vOU,v.COLUMN29,v.COLUMN30,v.COLUMN32,v.COLUMN31,
	v.COLUMN33 CGRY,v.COLUMN34 GSTIN,iif(o.COLUMN16!='',o.COLUMN16,v.COLUMN35) STCODE,CONCAT(sc.COLUMN06,'-',sc.COLUMN03) STCODENAME
	FROM dbo.SATABLE003 a
	LEFT JOIN dbo.CONTABLE007 o on o.COLUMN02 =ISNULL(a.COLUMNA02,0) and ISNULL(o.COLUMNA13,0)=0
	inner JOIN dbo.SATABLE001 v on v.COLUMN02 = @vendorId and ISNULL(v.COLUMNA13,0)=0
	LEFT JOIN dbo.MATABLE017 sc on sc.COLUMN02 = v.COLUMN35 and ISNULL(sc.COLUMNA13,0)=0
	WHERE a.COLUMN19='Vendor' and a.COLUMN20 = (select COLUMN01 FROM dbo.SATABLE001 WHERE COLUMN02= @vendorId) and a.COLUMNA03=@accntId
END














GO
