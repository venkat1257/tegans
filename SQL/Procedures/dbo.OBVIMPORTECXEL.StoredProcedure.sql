USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[OBVIMPORTECXEL]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OBVIMPORTECXEL]
(
@Vendor nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null
)
AS
BEGIN
select f.column02 as AID,f.column04 AR,s.column02 as ID from SATABLE001 s  
left outer join FITABLE001 f on f.COLUMNA03=@AcOwner and f.column02=2000 and isnull(f.COLUMNA13,0) = 0
where s.column05=@Vendor and s.COLUMNA03=@AcOwner
end
GO
