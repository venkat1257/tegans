USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_JOBBINGLEDGER]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[USP_REPORTS_JOBBINGLEDGER](
@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
 )
as
begin
if @Item!=''
begin
 set @whereStr= ' where ItemID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Item+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
end

select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc

if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s) '
end
else
begin
set @whereStr=@whereStr+' and upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end

select * into #JobbingLedgerReport from (
(select o.COLUMN03 ou,f.COLUMN04 item,f.COLUMN06 upc,c.COLUMN17 uomid,(case when c.COLUMN17=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24)) Lot,b.COLUMN04 tno,FORMAT(b.COLUMN09,@DateF) tdate,b.COLUMN09 Dtt,  s1.COLUMN05  customer ,0 openbal,iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,c.COLUMN08) qr,0 qi,c.COLUMN10 price,((iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,c.COLUMN08))*(c.COLUMN10)) amt,c10.COLUMN04 transType,c.COLUMN03 ItemID,b.COLUMN13 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN21 Project ,b.COLUMN03  fid,(case when f.COLUMN10=0 then null else f.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,b.COLUMN22 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24) LotID
from PUTABLE003 b 
inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner  
left join CONTABLE0010 c10 on c10.COLUMN02=b.COLUMN03 
left join MATABLE002 m on m.COLUMN02=c.COLUMN17 and m.COLUMNA03=c.COLUMNA03  
left join CONTABLE030 l on l.COLUMN02=b.COLUMN22 and l.COLUMNA02=b.COLUMN13 and l.COLUMNA03=b.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=c.COLUMN24 and bm.COLUMNA02=c.COLUMNA02 and bm.COLUMNA03=c.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN03 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=b.COLUMN13  and o.COLUMNA03=b.COLUMNA03
left outer join SATABLE002 s1 on s1.COLUMN02=b.COLUMN05 and s1.COLUMNA03=b.COLUMNA03
where isnull((b.COLUMNA13),0)=0 and ( b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s)  or b.columna02 is null) AND  b.COLUMNA03=@AcOwner and b.COLUMN09 between @FromDate and @ToDate and b.COLUMN09 >=@FiscalYearStartDt and b.COLUMN03=1587
)
 UNION ALL
(select o.COLUMN03 ou,g.COLUMN04 item,g.COLUMN06 upc,d.COLUMN19 uomid,(case when d.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else ( m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24)) Lot,a.COLUMN04 tno,FORMAT(a.COLUMN08,@DateF) tdate,a.COLUMN08 Dtt,s.COLUMN05 customer,0 openbal, 0 qr,iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,d.COLUMN09) qi,d.COLUMN12 price,-((iif(isnull(mu.COLUMN12,0)!=0,mu.COLUMN07,d.COLUMN09))*(d.COLUMN12)) amt,c10.COLUMN04 transType,d.COLUMN04 ItemID,a.COLUMN15 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN22 Project
,a.COLUMN03 fid,(case when g.COLUMN10=0 then null else g.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,a.COLUMN23 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24) LotID
from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
left join CONTABLE0010 c10 on c10.COLUMN02=a.COLUMN03 
left join MATABLE002 m on m.COLUMN02=d.COLUMN19 and m.COLUMNA03=d.COLUMNA03   
left join CONTABLE030 l on l.COLUMN02=a.COLUMN23 and l.COLUMNA02=a.COLUMN15 and l.COLUMNA03=a.COLUMNA03 left join FITABLE043 bm on bm.COLUMN02=d.COLUMN24 and bm.COLUMNA02=d.COLUMNA02 and bm.COLUMNA03=d.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=@AcOwner
left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN04 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN15  and o.COLUMNA03=a.COLUMNA03
left outer join SATABLE002 s on s.COLUMN02=a.COLUMN05 and s.COLUMNA03=a.COLUMNA03
 where isnull((a.COLUMNA13),0)=0  AND a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) 
 AND  a.COLUMNA03=@AcOwner and a.COLUMN08 between @FromDate and @ToDate and a.COLUMN08 >=@FiscalYearStartDt and a.COLUMN03=1588
)
 
 UNION ALL
 ----------------
(select ou,item,upc,uomid,uom,Location,Lot,tno,tdate,Dtt,customer,isnull(sum(openbal),0) as openbal,qr,qi,null price,null amt,transType,ItemID,OPID,0 TransTypeID,null Project
,fid,BrandID,Brand, LocID,LotID from (
(select o.COLUMN03 ou,f.COLUMN04 item,f.COLUMN06 upc,c.COLUMN17 uomid,(case when c.COLUMN17=10000 then (select column05 from fitable037 where column02=10000) else (m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24)) Lot,null tno,null tdate,NULL Dtt, null customer ,iif(isnull(mu.COLUMN12,0)!=0,sum(isnull(mu.column07,0)),sum(isnull(c.column08,0))) openbal,null qr,null qi,sum(isnull(c.column10,0)) price,((iif(isnull(mu.COLUMN12,0)!=0,sum(isnull(mu.column07,0)),sum(isnull(c.column08,0))))*(sum(isnull(c.column10,0)))) amt,null transType,c.COLUMN03 ItemID,b.COLUMN13 OPID,(case when @Type='' then 0 else b.COLUMN03 end) TransTypeID,b.COLUMN21 Project,null  fid,(case when f.COLUMN10=0 then null else f.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,b.COLUMN22 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,c.COLUMN24) LotID from PUTABLE003 b inner join PUTABLE004 c on c.COLUMN12=b.COLUMN01 and isnull(c.COLUMNA13,0)=0
inner join MATABLE007 f on f.COLUMN02=c.COLUMN03 and isnull(f.COLUMNA13,0)=0 and f.COLUMN48='True' and f.COLUMNA03=@AcOwner 
left join MATABLE002 m on m.COLUMN02=c.COLUMN17 and m.COLUMNA03=c.COLUMNA03  
left join CONTABLE030 l on l.COLUMN02=b.COLUMN22 and l.COLUMNA02=b.COLUMN13 and l.COLUMNA03=b.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=c.COLUMN24 and bm.COLUMNA02=c.COLUMNA02 and bm.COLUMNA03=c.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=f.COLUMN10 and f.COLUMN02=c.COLUMN03 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=c.COLUMNA03
left join FITABLE038 mu on mu.COLUMN04=b.COLUMN04 and mu.COLUMN05=c.COLUMN01 and mu.COLUMN06=c.COLUMN03 and mu.COLUMNA02=b.COLUMNA02 and mu.COLUMNA03=b.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=b.COLUMN13  and o.COLUMNA03=b.COLUMNA03
where isnull((b.COLUMNA13),0)=0 and b.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND b.COLUMNA03=@AcOwner and (b.COLUMN09< @FromDate) 
and b.COLUMN09 >=@FiscalYearStartDt and b.COLUMN03=1587 and ( (@Type!=null and b.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) group by f.COLUMN10,c.COLUMN24,f.COLUMN06,m5.COLUMN04,b.COLUMN13,f.COLUMN04,c.COLUMN03,b.COLUMN03,b.COLUMN21,c.COLUMN17,m.COLUMN04,l.COLUMN04,f.COLUMN17,mu.COLUMN12,b.COLUMN22,o.COLUMN03)
union all
(select o.COLUMN03 ou,g.COLUMN04 item,g.COLUMN06 upc,d.COLUMN19 uomid,(case when d.COLUMN19=10000 then (select column05 from fitable037 where column02=10000) else (  m.COLUMN04) end) uom,l.COLUMN04 Location,(select COLUMN04 from FITABLE043 where column02=iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24)) Lot,null tno,null tdate,NULL Dtt, null customer,
-(iif(isnull(mu.COLUMN12,0)!=0,sum(isnull(mu.column07,0)),sum(isnull(d.column09,0)))) openbal, null qr,null qi ,sum(isnull(d.column12,0)) price,-(((iif(isnull(mu.COLUMN12,0)!=0,sum(isnull(mu.column07,0)),sum(isnull(d.column09,0)))))*(sum(isnull(d.column12,0)))) amt,null transType,d.COLUMN04 ItemID,a.COLUMN15 OPID,(case when @Type='' then 0 else a.COLUMN03 end) TransTypeID,a.COLUMN22 Project,null fid,(case when g.COLUMN10=0 then null else g.COLUMN10 end) BrandID,m5.COLUMN04 as Brand,a.COLUMN23 LocID,iif(isnull(mu.COLUMN12,0)!=0, mu.COLUMN12,d.COLUMN24) LotID from SATABLE007 a inner join SATABLE008 d on d.COLUMN14=a.COLUMN01 and isnull(d.COLUMNA13,0)=0
inner join MATABLE007 g on g.COLUMN02=d.COLUMN04 and isnull(g.COLUMNA13,0)=0 and g.COLUMN48='True' and g.COLUMNA03=@AcOwner 
left join MATABLE002 m on m.COLUMN02=d.COLUMN19 and m.COLUMNA03=d.COLUMNA03   
left join CONTABLE030 l on l.COLUMN02=a.COLUMN23 and l.COLUMNA02=a.COLUMN15 and l.COLUMNA03=a.COLUMNA03 
left join FITABLE043 bm on bm.COLUMN02=d.COLUMN24 and bm.COLUMNA02=d.COLUMNA02 and bm.COLUMNA03=d.COLUMNA03
left outer join MATABLE005 m5 on m5.COLUMN02=g.COLUMN10 and g.COLUMN02=d.COLUMN04 and isnull(m5.COLUMNA13,0)=0  and m5.COLUMNA03=d.COLUMNA03
left join FITABLE038 mu on mu.COLUMN04=a.COLUMN04 and mu.COLUMN05=d.COLUMN01 and mu.COLUMN06=d.COLUMN04 and mu.COLUMNA02=a.COLUMNA02 and mu.COLUMNA03=a.COLUMNA03 and isnull(mu.COLUMNA13,0)=0
left outer join CONTABLE007 o on o.COLUMN02=a.COLUMN15  and o.COLUMNA03=a.COLUMNA03
where isnull((a.COLUMNA13),0)=0 and  a.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND a.COLUMNA03=@AcOwner and a.COLUMN08< @FromDate 
and a.COLUMN08 >=@FiscalYearStartDt and a.COLUMN03=1588 and ( (@Type!=null and a.COLUMN03 in (SELECT ListValue FROM dbo.FN_ListToTable(',',@Type) s)) or(1=1)) group by g.COLUMN10,g.COLUMN06,d.COLUMN24,m5.COLUMN04,a.COLUMN15,g.COLUMN04,d.COLUMN04,a.COLUMN03,a.COLUMN22,d.COLUMN19,m.COLUMN04,l.COLUMN04,mu.COLUMN12,a.COLUMN23,o.COLUMN03)
  )p group by ou,item,upc,uomid,uom,Location,LocID,Lot,tno,tdate,Dtt,customer,qr,qi,transType,ItemID,OPID,fid,BrandID,Brand, LocID,LotID)) Query
if(@Type=null)
begin
set @Query1='select *,F10.COLUMN17 AvgPrice from #JobbingLedgerReport left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID AND F10.COLUMN13=OPID AND F10.COLUMNA02=OPID AND F10.COLUMN19=UOMID AND iif(F10.COLUMN21='',0,isnull(F10.COLUMN21,0))=iif(LocID='',0,isnull(LocID,0)) AND iif(F10.COLUMN22='',0,isnull(F10.COLUMN22,0))=iif(LotID='',0,isnull(LotID,0)) and isnull(F10.COLUMNA13,0)=0 '+@whereStr+' order by Dtt desc' 
end
else
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
end
else
begin
set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID=0 )'
end
set @Query1='select *,F10.COLUMN17 AvgPrice from #JobbingLedgerReport
			 left JOIN   FITABLE010 F10 ON F10.COLUMN03=ITEMID AND F10.COLUMN13=OPID AND F10.COLUMNA02=OPID AND F10.COLUMN19=UOMID AND isnull(F10.COLUMN21,0)=isnull(LocID,0) AND isnull(F10.COLUMN22,0)=isnull(LotID,0) and isnull(F10.COLUMNA13,0)=0  '+@whereStr+' order by Dtt desc'  
end
exec (@Query1) 
end


GO
