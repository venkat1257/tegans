USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_InsertUOM]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_InsertUOM]
@UOMData XML,@Childtable nvarchar(250)=null,@TransactionNo nvarchar(250)=null,
@TransactionRefNo nvarchar(250)=null,@ItemName nvarchar(250)=null,@OperatingUnit nvarchar(250)=null, @AccountOwner nvarchar(250)=null
AS
BEGIN
      SET NOCOUNT ON; 
	  declare @Quantity nvarchar(250), @Price nvarchar(250),@col2 int,@id int,@item int
	  set @col2=(isnull((select max(cast(COLUMN02 as int)) from FITABLE038),999))
	  set @col2=((@col2)+1)
	  --EMPHCS1117 rajasekhar reddy 09/09/2015 Multiple Uom Condition in bill and Invoice
	  delete from FITABLE038 where column03=@Childtable and COLUMN04=@TransactionNo and COLUMN05=@TransactionRefNo and COLUMN06=@ItemName and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner
	  if not exists( SELECT COLUMN01 from FITABLE038 where column03=@Childtable and COLUMN04=@TransactionNo and COLUMN05=@TransactionRefNo and COLUMN06=@ItemName and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner)
      begin
	  INSERT INTO FITABLE038(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMN09,COLUMNA02,COLUMNA03,COLUMNA12,COLUMNA13) 
	  SELECT
      @col2 AS Id,@Childtable,@TransactionNo,@TransactionRefNo,@ItemName,
      UOM.details.value('(Quantity)[1]','varchar(100)') as Quantity,
      UOM.details.value('(UnitIds)[1]','varchar(100)') as Units,
      UOM.details.value('(Price)[1]','varchar(100)') as Price,
	  @OperatingUnit,@AccountOwner,1,0
      from @UOMData.nodes('/ROOT/Column') as UOM(details)
	  
	  DECLARE @MaxRownum INT
      DECLARE @Initialrow INT=1
	  DECLARE cur1 CURSOR FOR SELECT COLUMN01 from FITABLE038 where column02=@col2
      OPEN cur1
	  FETCH NEXT FROM cur1 INTO @id
      SET @MaxRownum = (SELECT COUNT(*) FROM FITABLE038 where column02=@col2)
         WHILE @Initialrow <= @MaxRownum
         BEGIN 
		 if(@Initialrow=1)
		 begin
             set @item=(SELECT COLUMN01 FROM FITABLE038 WHERE COLUMN01=@id)
		 end
		 else
		 begin
             set @Item=(cast((select max(cast(COLUMN02 as int)) from FITABLE038) as int)+1)
		     UPDATE FITABLE038 SET COLUMN02=cast(@Item as nvarchar(250)) WHERE COLUMN01=@id
		 end
			 FETCH NEXT FROM cur1 INTO @id
             SET @Initialrow = @Initialrow + 1 
			 END
        CLOSE cur1 
	--EMPHCS1117 rajasekhar reddy 09/09/2015 Multiple Uom Condition in bill and Invoice
        deallocate cur1 
		end
		else 
		begin
		;WITH XmlData AS 
		(
		SELECT
		  UOM.details.value('(Quantity)[1]','varchar(100)') as Quantity,
		  UOM.details.value('(UnitIds)[1]','varchar(100)') as Units,
		  UOM.details.value('(Price)[1]','varchar(100)') as Price
		FROM 
		    @UOMData.nodes('/ROOT/Column') AS UOM(details)
		)
	
        UPDATE FITABLE038 SET 
        COLUMN07 = x.Quantity,
        COLUMN08 = x.Units,
        COLUMN09 = x.Price,
        COLUMNA02=@OperatingUnit,COLUMNA03=@AccountOwner,COLUMNA12=1,COLUMNA13=0
	    from XmlData x
	  --update FITABLE038  set 
	  --COLUMN07=(SELECT UOM.details.value('(Quantity)[1]','varchar(100)') as Quantity from @UOMData.nodes('/ROOT/Column') as UOM(details)),
	  --COLUMN08=(SELECT UOM.details.value('(UnitIds)[1]','varchar(100)') as Units from @UOMData.nodes('/ROOT/Column') as UOM(details)),
	  --COLUMN09=(SELECT  UOM.details.value('(Price)[1]','varchar(100)') as Price from @UOMData.nodes('/ROOT/Column') as UOM(details)),
	    where COLUMN03=@Childtable and COLUMN08 in(x.Units) and COLUMN04=@TransactionNo and COLUMN05=@TransactionRefNo and COLUMN06=@ItemName and COLUMNA02=@OperatingUnit and COLUMNA03=@AccountOwner
		end
END

GO
