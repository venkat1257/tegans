USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[SendSMSList]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[SendSMSList]
(
         @Direction  nvarchar(250), 		 
         @COLUMN02 bigint=null,
		 @COLUMN03 nvarchar(250)=null,
		 @COLUMN04 DateTime=null,
		 @COLUMN05 nvarchar(250)=null,
		 @COLUMN06 nvarchar(250)=null,
		 @COLUMN07 nvarchar(250)=null,
		 @COLUMN08 nvarchar(250)=null,
		 @COLUMN09 nvarchar(250)=null,
		 @COLUMN10 nvarchar(250)=null,
		 @COLUMN11 nvarchar(250)=null,
		 @COLUMN12 nvarchar(250)=null,
		 @COLUMN13 date=null,
		 @COLUMN14 date=null,
		 @COLUMN15 nvarchar(250)=null,
		 @COLUMN16 int=null,
		 @column17 nvarchar(250)=null,
		 @column18 nvarchar(250)=null,
		 @column19 nvarchar(250)=null,
		 @column20 nvarchar(250)=null,
		 @COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null,   @COLUMNA03  varchar(100)=null,  
		 @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null,   @COLUMNA06  nvarchar(250)=null
		
)
as
begin
   begin try
  --  IF @Direction = 'GetSmsList'  
  --  begin
	 --Select COLUMN02,COLUMN03 as 'CustomertName',COLUMN04 as 'Date',COLUMN05 as 'PhoneNumbers',COLUMN06 as 'Message' from SETABLE024 where COLUMNA02=@COLUMNA02 and COLUMNA03=@COLUMNA03
  --  end

   IF @Direction = 'Add'  
    begin

     select @COLUMN02=NEXT VALUE FOR DBO.SETABLE024_SequenceNo
     Insert into SETABLE024(COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08,COLUMNA02,COLUMNA03 ) 
	 values(@COLUMN02,@COLUMN03,@COLUMN04,@COLUMN05,@COLUMN06,@COLUMN07,@COLUMN08,@COLUMNA02,@COLUMNA03)

	 DECLARE @cnt INT = 0,@CMPcnt INT = 1,@TEMP NVARCHAR(250)=NULL,@COLUMNA3 NVARCHAR(250)=@COLUMNA03,@ROLE BIGINT,@ID INT,@MULTI NVARCHAR(250)
DECLARE @REF BIGINT,@COLUMN2 INT,@ID1 NVARCHAR(25),@newID NVARCHAR(250)=NULL,@Internalid nvarchar(250)=NULL
SET @MULTI = @COLUMN08
SET @Internalid = (SELECT MAX(COLUMN01) FROM SETABLE024 WHERE COLUMN08=@COLUMN08 AND COLUMN02 =@COLUMN02 AND COLUMNA03 =@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
WHILE @cnt < @CMPcnt
	BEGIN
		IF(@COLUMN08 IS NULL OR @COLUMN08 = '' OR @COLUMN08 = '0' OR LEN(@COLUMN08)=0) 
		BEGIN 
			set @CMPcnt=0;  
		END
		ELSE 
			BEGIN
				set @TEMP=(Select Substring(@COLUMN08,0,CharIndex(',',@COLUMN08)))
				IF(@TEMP IS NOT NULL  AND @TEMP != '' and LEN(@TEMP)>0) SET @COLUMN10=(@TEMP)
				if((Select len(Substring(@COLUMN08,0,CharIndex(',',@COLUMN08))))=0)
				begin
					set @COLUMN08=(@COLUMN08)
					set @COLUMN10=(@COLUMN08)
					set @CMPcnt=0;
				end
				else
				begin
					set @COLUMN08=(Select REPLACE(@COLUMN08,CAST(@COLUMN10 AS NVARCHAR(25))+',', ''))
					if(@CMPcnt!=0)
					begin
					set @CMPcnt=(@CMPcnt+1)
					end
				end
			end
	    IF @COLUMN07 = '22368'  
        begin
	  SET @COLUMN03 = (SELECT COLUMN05 FROM SATABLE002 WHERE COLUMN02 = @COLUMN10 AND COLUMNA03=@COLUMNA03  AND ISNULL(COLUMNA13,0)=0)
	  SET @COLUMN05 = (SELECT COLUMN11 FROM SATABLE002 WHERE COLUMN02 = @COLUMN10 AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	  END
	  ELSE IF @COLUMN07 = '22369'  
    begin
	 SET  @COLUMN03 = (SELECT COLUMN05 FROM SATABLE001 WHERE COLUMN02 = @COLUMN10 AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	 SET @COLUMN05 = (SELECT COLUMN12 FROM SATABLE001 WHERE COLUMN02 = @COLUMN10  AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	END
	ELSE IF @COLUMN07 = '22370'
    begin
	 SET  @COLUMN03 = (SELECT COLUMN05 FROM SATABLE001 WHERE COLUMN02 = @COLUMN10  AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	 SET @COLUMN05 = (SELECT COLUMN12 FROM SATABLE001 WHERE COLUMN02 = @COLUMN10   AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	END
	ELSE IF @COLUMN07 = '22371'  
    begin
	 SET  @COLUMN03 = (SELECT COLUMN06 FROM MATABLE010 WHERE COLUMN02 = @COLUMN10 AND COLUMNA03=@COLUMNA03  AND ISNULL(COLUMNA13,0)=0)
	 SET @COLUMN05 = (SELECT COLUMN14 FROM MATABLE010 WHERE COLUMN02 = @COLUMN10  AND COLUMNA03=@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
	END
	Insert into SETABLE025(COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN09,COLUMN10,COLUMNA02,COLUMNA03 ) 
	  values(@COLUMN03,@COLUMN04,@COLUMN05,@COLUMN06,@COLUMN07,@Internalid,@COLUMN10,@COLUMNA02,@COLUMNA03)


	 end
	 end
  IF @Direction = 'Delete'  
    begin
    update SETABLE024 set COLUMNA13=1 where COLUMN02= @COLUMN10 and COLUMNA03 =@COLUMNA03

	--update SETABLE023 set COLUMNA13=1 where COLUMN02=@COLUMN02
	 end

	 IF @Direction = 'UpdateSentMsg'  
    begin
    --Select COLUMN03 as 'CustomertName',COLUMN04 as 'Date',COLUMN05 as 'PhoneNumbers',COLUMN06 as 'Message' from SETABLE024 

	select COLUMND07,COLUMN07,COLUMN08,COLUMN06 from SETABLE024  where COLUMN02= @COLUMN02

	
	 end
	If @direction='ResendMessage'
	 begin
update setable024 set COLUMN06=@COLUMN06 where COLUMN02=@COLUMN02 and ISNULL(COLUMNA13, 0) = 0
SET @Internalid = (SELECT COLUMN01 FROM SETABLE024 WHERE COLUMN02=@COLUMN02 AND COLUMNA03 =@COLUMNA03 AND ISNULL(COLUMNA13,0)=0)
UPDATE SETABLE025 SET COLUMN06 = @COLUMN06 where COLUMN09=@Internalid and ISNULL(COLUMNA13, 0) = 0

END
   end try

   begin catch
   declare @tempSTR nvarchar(max)
			SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
    set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(500))))
    exec [CheckDirectory] @tempSTR,'setable025.txt',0
return 0
end catch
   
end


GO
