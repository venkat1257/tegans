USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetCommissionReport]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetCommissionReport]
(
@SalesRef    nvarchar(250)= null,

@FromDate    nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@OPUnitStatus      int= null,
@AcOwner nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@DateF nvarchar(250)=null)
as 
BEGIN
---EMPHCS907	 After deleting invoice - Taxes are not cleared  By Raj.Jr 11/8/2015

if(@SalesRef!='' and @FromDate!='' and @ToDate!='')
BEGIN
select FORMAT(b.COLUMN19,@DateF)COLUMN19,c.COLUMN05 COLUMN30,b.COLUMN13, (select column06 from matable010 where column02=c.COLUMN22) salesref,(select column31 from matable010 where column02=c.COLUMN22) comm, 
 CAST((isnull(b.COLUMN13,0)*(isnull(c.COLUMN23,0)*0.01)) AS DECIMAL(18,0)) COLUMN31 from  SATABLE002 c
inner join  SATABLE011 b on c.COLUMN02=b.COLUMN05
where isnull((c.COLUMNA13),0)=0 and c.COLUMN22=@SalesRef and b.COLUMN19 between @FromDate and  @ToDate AND c.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  c.COLUMNA03=@AcOwner
order by b.COLUMN19	desc
end
else if(@SalesRef!='' and @FromDate!='' and @ToDate!='')
BEGIN
select FORMAT(b.COLUMN19,@DateF)COLUMN19,c.COLUMN05 COLUMN30,b.COLUMN13, (select column06 from matable010 where column02=c.COLUMN22) salesref,(select column31 from matable010 where column02=c.COLUMN22) comm, 
 CAST((isnull(b.COLUMN13,0)*(isnull(c.COLUMN23,0)*0.01)) AS DECIMAL(18,0)) COLUMN31 from  SATABLE002 c
inner join  SATABLE011 b on c.COLUMN02=b.COLUMN05
where isnull((c.COLUMNA13),0)=0 and c.COLUMN22=@SalesRef  and b.COLUMN19 between @FromDate and  @ToDate AND c.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  c.COLUMNA03=@AcOwner
order by b.COLUMN19	desc
end

else if(@SalesRef!='')
BEGIN
select FORMAT(b.COLUMN19,@DateF)COLUMN19,c.COLUMN05 COLUMN30,b.COLUMN13, (select column06 from matable010 where column02=c.COLUMN22) salesref,(select column31 from matable010 where column02=c.COLUMN22) comm, 
CAST((isnull(b.COLUMN13,0)*(isnull(c.COLUMN23,0)*0.01)) AS DECIMAL(18,0)) COLUMN31 from  SATABLE002 c
inner join  SATABLE011 b on c.COLUMN02=b.COLUMN05 AND c.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  c.COLUMNA03=@AcOwner
where isnull((c.COLUMNA13),0)=0 and c.COLUMN22=@SalesRef 
order by b.COLUMN19	desc
end

else if(@FromDate!='' and @ToDate!='')
BEGIN
select FORMAT(b.COLUMN19,@DateF)COLUMN19,c.COLUMN05 COLUMN30,b.COLUMN13, (select column06 from matable010 where column02=c.COLUMN22) salesref,(select column31 from matable010 where column02=c.COLUMN22) comm, 
 CAST((isnull(b.COLUMN13,0)*(isnull(c.COLUMN23,0)*0.01)) AS DECIMAL(18,0)) COLUMN31 from  SATABLE002 c
inner join  SATABLE011 b on c.COLUMN02=b.COLUMN05
where isnull((c.COLUMNA13),0)=0 and b.COLUMN19 between  @FromDate and  @ToDate AND c.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) AND  c.COLUMNA03=@AcOwner
order by b.COLUMN19	desc
end
END






GO
