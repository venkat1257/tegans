USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[GetTaxAmount]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[GetTaxAmount]
(
@TaxType  nvarchar(250)= null,@FormID  int,@Party int=null,@Group  int =null,@AcOwner  int =null)
as 
begin
--EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
--EMPHCS1071 rajasekhar reddy patakota 01/09/2015 Issue check - line level data is not getting displayed
--EMPHCS1149 rajasekhar reddy patakota 15/08/2015 Project Forms Functionality Changes
if(@Group is not null and @Group=0)
Begin
if(@FormID=1251  or @FormID=1272 or @FormID=1273  or @FormID=1353  or @FormID=1355 or @FormID=1363 or @FormID=1380)
	select (isnull(cast(COLUMN17 as decimal(18,2)),0))+(isnull(cast(COLUMN22 as decimal(18,2)),0))+(isnull(cast(COLUMN23 as decimal(18,2)),0)) COLUMN07,0 COLUMN16,0 COLUMN22,0 COLUMN23,'SubTotal' COLUMN19  from MATABLE013 
	where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=@TaxType and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)) s) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
else
	select (isnull(cast(COLUMN07 as decimal(18,2)),0))+(isnull(cast(COLUMN22 as decimal(18,2)),0))+(isnull(cast(COLUMN23 as decimal(18,2)),0)) COLUMN07,0 COLUMN16,0 COLUMN22,0 COLUMN23,'SubTotal' COLUMN19  from MATABLE013 
	where COLUMN02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=@TaxType and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0)) s) and COLUMNA03=@AcOwner and ISNULL(COLUMNA13,0)=0
End
else
Begin
if(@Party='' or @Party is null)
begin
set @Party=0
end
if(@FormID=1251  or @FormID=1273  or @FormID=1353  or @FormID=1355 or @FormID=1363 or @FormID=1380)
begin
if(@Party=22335)
begin
select  isnull(COLUMN07,0) COLUMN07,isnull(COLUMN16,0) COLUMN16,iif(isnull(COLUMN20,0)=1,isnull(COLUMN22,0),0) COLUMN22,iif(isnull(COLUMN21,0)=1,isnull(COLUMN23,0),0) COLUMN23,COLUMN19  from MATABLE013 where COLUMN02=@TaxType
end
else
begin
select  isnull(COLUMN17,0) COLUMN07,isnull(COLUMN16,0) COLUMN16,iif(isnull(COLUMN20,0)=1,isnull(COLUMN22,0),0) COLUMN22,iif(isnull(COLUMN21,0)=1,isnull(COLUMN23,0),0) COLUMN23,COLUMN19  from MATABLE013 where COLUMN02=@TaxType
end 
end
else
begin
if(@Party=22305)
begin
select  isnull(COLUMN17,0) COLUMN07,isnull(COLUMN16,0) COLUMN16,iif(isnull(COLUMN20,0)=1,isnull(COLUMN22,0),0) COLUMN22,iif(isnull(COLUMN21,0)=1,isnull(COLUMN23,0),0) COLUMN23,COLUMN19  from MATABLE013 where COLUMN02=@TaxType
end
else
begin
select  isnull(COLUMN07,0) COLUMN07,isnull(COLUMN16,0) COLUMN16,iif(isnull(COLUMN20,0)=1,isnull(COLUMN22,0),0) COLUMN22,iif(isnull(COLUMN21,0)=1,isnull(COLUMN23,0),0) COLUMN23,COLUMN19  from MATABLE013 where COLUMN02=@TaxType
end  
end
end

end





GO
