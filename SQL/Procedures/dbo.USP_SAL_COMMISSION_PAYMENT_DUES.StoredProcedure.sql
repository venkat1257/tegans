USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_SAL_COMMISSION_PAYMENT_DUES]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_SAL_COMMISSION_PAYMENT_DUES]
(
@CUSTOMER nvarchar(250)=NULL,
@SALESREP nvarchar(250)=NULL,
@OPERATING nvarchar(250)=NULL,
@FRDATE date=NULL,
@OPUnit nvarchar(250)= null,
@OPUnitStatus      int= null,
@AcOwner nvarchar(250)= null,
@TODATE date=NULL,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@DateF nvarchar(250)=null,
@Category nvarchar(250)= null
)
AS	
BEGIN
if(@OPERATING!='')
begin
set @whereStr= ' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OPERATING+''') s)'
end
if(@CUSTOMER!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@CUSTOMER+''') s)'
end
else
begin
set @whereStr=@whereStr+' and CID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@CUSTOMER+''') s)'
end
end
if(@SALESREP!='')
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where REPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SALESREP+''') s)'
end
else
begin
set @whereStr=@whereStr+' and REPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@SALESREP+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end
 select * into #CommissionPaymentDuesReport from (
SELECT CONTABLE007.COLUMN03,(case when FITABLE027.COLUMN04='COMMISSION PAYMENT' then MATABLE010.COLUMN06 else SATABLE002.COLUMN05 end) COLUMN05, MATABLE010.COLUMN06,
(cast(FITABLE027.COLUMN12 as decimal(18,2))) as COLUMN12,FITABLE027.COLUMN07,FITABLE027.COLUMN09,FORMAT(FITABLE027.COLUMN03,@DateF) date, (cast(isnull(FITABLE027.COLUMN11,0) as decimal(18,2)))+(cast(isnull(FITABLE027.COLUMN18,0) as decimal(18,2))) as COLUMN11, FITABLE027.COLUMN13,FITABLE027.COLUMN15, FITABLE027.COLUMNA02 OPID,FITABLE027.COLUMN14 REPID,FITABLE027.COLUMN06 CID
FROM FITABLE027 left JOIN SATABLE002 on SATABLE002.COLUMN02 = FITABLE027.COLUMN06 
inner join MATABLE010 ON MATABLE010.COLUMN02 = FITABLE027.COLUMN14
INNER JOIN CONTABLE007 ON   CONTABLE007.COLUMN02 =FITABLE027.COLUMNA02
where FITABLE027.COLUMN03 BETWEEN @FRDATE AND @TODATE  AND FITABLE027.COLUMNA03=@AcOwner 
)Query
set @Query1='select * from #CommissionPaymentDuesReport'+@whereStr+' order by date desc'
exec (@Query1) 
END



GO
