USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_PAYBILL_REP_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PUR_TP_PAYBILL_REP_DATA]
(
--EMPHCS1027 Vendor payment screen while paying to sales rep , first it should get total due for sales rep. After slecting operating unit , system should get based on that operating unit  BY RAJ.Jr 3/9/2015
    @OpUnit int,
	@SalesRepID int
)
AS
BEGIN
	declare @Amnt decimal(18,2)
	declare @Due decimal(18,2)
	declare @Pay decimal(18,2)
	declare @Advance decimal(18,2)
	if(@OpUnit!='')
	begin
	set @Amnt=(select sum(isnull(COLUMN12,0)) from FITABLE027 where COLUMN14=@SalesRepID and COLUMNA02=@OpUnit)
	set @Pay=(select sum(isnull(COLUMN11,0)) from FITABLE027 where	column14=@SalesRepID and COLUMNA02=@OpUnit)
	set @Advance=(select sum(isnull(COLUMN18,0)) from FITABLE027 where	column14=@SalesRepID and COLUMNA02=@OpUnit)
	set @Due=@Amnt-(@Pay+@Advance)
	select 0 as COLUMN03,@Amnt COLUMN04,@Due COLUMN05 
	end
	else
	begin
	set @Amnt=(select sum(isnull(COLUMN12,0)) from FITABLE027 where COLUMN14=@SalesRepID)
	set @Pay=(select sum(isnull(COLUMN11,0)) from FITABLE027 where	column14=@SalesRepID)
	set @Advance=(select sum(isnull(COLUMN18,0)) from FITABLE027 where	column14=@SalesRepID)
	set @Due=@Amnt-(@Pay+@Advance)
	select 0 as COLUMN03,@Amnt COLUMN04,@Due COLUMN05 
	end

END



GO
