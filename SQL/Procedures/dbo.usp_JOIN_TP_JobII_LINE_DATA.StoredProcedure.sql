USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_JOIN_TP_JobII_LINE_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_JOIN_TP_JobII_LINE_DATA]
(
	@SalesOrderID int,
	@ReceiptID int
)

AS
BEGIN
	if(@ReceiptID!=0)
	begin
	set @SalesOrderID=(select column06 from putable003 where column02=@ReceiptID)
	end
	select  a.COLUMN03 as COLUMN04,a.COLUMN05 as COLUMN05,a.COLUMN15 as COLUMN06 , a.COLUMN07 as COLUMN07,
	 isnull(a.COLUMN12,0) as COLUMN08 ,'' as COLUMN09,
	 --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	 (case when (b.COLUMN65=1 or b.COLUMN65='True') then
	 isnull((select sum(COLUMN08) from FITABLE010 where COLUMN03 in (a.COLUMN03)  AND COLUMN13=A.COLUMN20 AND isnull(COLUMN19,0)=isnull(a.COLUMN27,0)),0) else 
	 isnull((select COLUMN04 from FITABLE010 where COLUMN03 in(a.COLUMN03)  AND COLUMN13=A.COLUMN20  AND isnull(COLUMN19,0)=isnull(a.COLUMN27,0) and isnull(COLUMN22,0)=isnull(a.COLUMN17,0) and isnull(COLUMN21,0)=isnull(s.COLUMN48,0)),0)end) as COLUMN10,
	  (isnull(CAST(a.COLUMN12 as decimal(18,2)),0)-isnull(CAST(a.COLUMNB12 as decimal(18,2)),0)) as COLUMN11,  a.COLUMN09 as COLUMN12,
	  --cast((isnull(CAST(a.COLUMN07 as decimal(18,2)),0)-isnull(CAST(a.COLUMN12 as decimal(18,2)),0))*CAST(a.COLUMN32 as decimal(18,2)) as decimal(18,2)) as COLUMN13,
	  --EMPHCS1388 rajasekhar reddy patakota 23/11/2015 Multi Uom Setup for sales and stock transfer screens
	  a.COLUMN27 COLUMN19,a.COLUMN17 COLUMN24,b.COLUMN48 COLUMN48,b.COLUMN65,a.COLUMN09 COLUMN25,cast(a.COLUMN09*a.COLUMN12 
--EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
as decimal(18,2)) as COLUMN13,a.COLUMN02 COLUMN26,a.COLUMN02 LineID
	FROM dbo.PUTABLE002 a 
	inner join PUTABLE001 s on s.COLUMN02=@SalesOrderID
	inner join MATABLE007 b on a.COLUMN03=b.COLUMN02
	WHERE a.COLUMN19 in (select COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02=@SalesOrderID) and ((isnull(CAST(a.COLUMN12 as decimal(18,2)),0)-isnull(CAST(a.COLUMNB12 as decimal(18,2)),0)))>0 and a.COLUMNA13=0
END





GO
