USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[USP_REPORTS_JobberIN]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   procedure [dbo].[USP_REPORTS_JobberIN]
(

@ReportName  nvarchar(250)= null,
@FromDate    nvarchar(250)= null,
@ToDate      nvarchar(250)= null,
@FrDate      nvarchar(250)= null,
@Type        nvarchar(250)= null,
@Brand        nvarchar(250)= null,
@Vendor      nvarchar(250)= null,
@Customer      nvarchar(250)= null,
@BillNO      nvarchar(250)= null,
@Jobber      nvarchar(250)= null,
@SONO      nvarchar(250)= null,
@PONO      nvarchar(250)= null,
@Status      nvarchar(250)= null,
@Item      nvarchar(250)= null,
@UPC      nvarchar(250)= null,
@OPUnit nvarchar(250)= null,
@AcOwner nvarchar(250)= null,
@SalesRep nvarchar(250)= null,
@OperatingUnit      nvarchar(250)= null,
@Operating      nvarchar(250)= null,
@OPUnitStatus      int= null,
@Project      nvarchar(250)= null,
--EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
@Location      nvarchar(250)= null,
@whereStr nvarchar(1000)=null,
@Query1 nvarchar(max)=null,
@FiscalYearStartDt nvarchar(250)= '01/01/1900',
@DateF nvarchar(250)=null
)
as 
begin
if @Customer!=''
begin
 set @whereStr= ' where Custid in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Customer+''') s)'
end
if @OperatingUnit!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
else
begin
set @whereStr=@whereStr+' and OPID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@OperatingUnit+''') s)'
end
end
if @Brand!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
end
else
begin
set @whereStr=@whereStr+' and BrandID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Brand+''') s)'
--EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
end
end
if @Location!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
else
begin
set @whereStr=@whereStr+' and LocID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Location+''') s)'
end
end
--if @Type!=''
--begin
--if(@whereStr='' or @whereStr is null)
--begin
--set @whereStr=' where (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
--end
--else
--begin
--set @whereStr=@whereStr+' and (TransTypeID in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Type+''') s) or TransTypeID is null )'
--end
--end
select top 1 @FiscalYearStartDt=column04 from FITABLE048 where COLUMNA03=@AcOwner order by COLUMN01 desc

if @Project!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
else
begin
set @whereStr=@whereStr+' and Project in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@Project+''') s)'
end
end
if @UPC!=''
begin
if(@whereStr='' or @whereStr is null)
begin
set @whereStr=' where upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s) '
end
else
begin
set @whereStr=@whereStr+' and upc in (SELECT ListValue FROM dbo.FN_ListToTable('','','''+@UPC+''') s)'
end
end
if(@whereStr='' or @whereStr is null)
begin
 set @whereStr=' where 1=1'
 end

 if(@ReportName='JobberIN')
 begin
select * into #ReceivableReport from 
(


  select c7.COLUMN03 ou ,S2.COLUMN05 Customer , p1.COLUMN04 COLUMN04 , FORMAT(p1.COLUMN06,@DateF) dt , p1.COLUMN06 Dtt,
   m7.COLUMN04 item ,  p2.COLUMN04 upc,(isnull(p2.COLUMN07,0)) qty,(isnull(p2.COLUMN12,0)) orderRQty,
    sum(isnull(s8.COLUMN09,0)) IssueQty,
   (case when (p2.COLUMN27)=10000 then (select column05 from fitable037 where column02=10000) 
else (select COLUMN04 from MATABLE002 where column02=(p2.COLUMN27)) end) uom,
  m13.COLUMN04 tax,
 p2.COLUMN09 price, p2.COLUMN24 amount, sum(isnull(p2.COLUMN11,0)) Tamount ,
 p1.COLUMNA02 OPID, p1.COLUMN05 Custid
 from PUTABLE001 p1 inner join  PUTABLE002 p2 on p2.COLUMN19 =p1.COLUMN01 and p2.COLUMNA03 =p1.COLUMNA03  and isnull(p2.COLUMNA13,0)=0
 --left  join PUTABLE003 p3 on p3.COLUMN06= p1.COLUMN02  and p3.COLUMNA03 =p1.COLUMNA03 
 --left  join PUTABLE004 p4 on p4.COLUMN12=p3.COLUMN01 and p4.COLUMN26= p2.COLUMN02  and isnull(p4.COLUMNA13,0)=0
 left join SATABLE007 s7 on s7.COLUMN06 = p1.COLUMN02     and s7.COLUMNA03 =p1.COLUMNA03  and isnull(s7.COLUMNA13,0)=0
 left join SATABLE008 s8 on s8.COLUMN14 =s7.COLUMN01 and   s8.COLUMN26= p2.COLUMN02   and isnull(s8.COLUMNA13,0)=0
 left  join MATABLE007 m7 on m7.COLUMN02=p2.COLUMN03 and isnull(m7.COLUMNA13,0)=0   and  m7.COLUMNA03=p2.COLUMNA03
 left join CONTABLE007 c7 on c7.COLUMN02=p1.COLUMN24  and isnull(c7.COLUMNA13,0)=0 and  c7.COLUMNA03=p2.COLUMNA03
 left join MATABLE013 m13 on m13.COLUMN02=p2.COLUMN25 and isnull(m13.COLUMNA13,0)=0 and  m13.COLUMNA03=p2.COLUMNA03
 LEFT JOIN SATABLE002 S2 ON  S2.COLUMN02=p1.COLUMN05 where  isnull((p1.COLUMNA13),0)=0 and  p1.COLUMN03=1586 and
( p1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or p1.columna02 is null) AND  p1.COLUMNA03=@AcOwner and
p1.COLUMN06 between @FromDate and @ToDate and p1.COLUMN06 >=@FiscalYearStartDt
group by c7.COLUMN03,S2.COLUMN05, p1.COLUMN04,p1.COLUMN06,m7.COLUMN04,p2.COLUMN04,p2.COLUMN27,
m13.COLUMN04 ,p2.COLUMN09,p2.COLUMN24, p1.COLUMNA02,p1.COLUMN05,p2.COLUMN07,p2.COLUMN12
) Query

set @Query1='select * from #ReceivableReport '+@whereStr+'order by Dtt desc '
exec (@Query1) 
end
--else if(@ReportName='JobingLedger')
--begin
--select * into #JobingLedger from 
--(


--  select c7.COLUMN03 ou ,S2.COLUMN05 Customer , p1.COLUMN04 COLUMN04 , FORMAT(p1.COLUMN06,@DateF) dt , 
--   m7.COLUMN04 item ,  p2.COLUMN04 upc, p2.COLUMN07 qty,p4.COLUMN06 orderRQty,
--    s8.COLUMN09 IssueQty,
--   (case when (p2.COLUMN27)=10000 then (select column05 from fitable037 where column02=10000) 
--else (select COLUMN04 from MATABLE002 where column02=(p2.COLUMN27)) end) uom,
--  m13.COLUMN04 tax,
-- p2.COLUMN09 price, p2.COLUMN24 amount, p2.COLUMN11 Tamount ,
-- p1.COLUMNA02 OPID, p1.COLUMN05 Custid
-- from PUTABLE001 p1 inner join  PUTABLE002 p2 on p2.COLUMN19 =p1.COLUMN01 and p2.COLUMNA03 =p1.COLUMNA03 
-- left outer  join PUTABLE003 p3 on p3.COLUMN06= p1.COLUMN02  and p3.COLUMNA03 =p1.COLUMNA03 
-- left outer  join PUTABLE004 p4 on p4.COLUMN12=p3.COLUMN01 and p4.COLUMN03= p2.COLUMN03 
-- left outer join SATABLE007 s7 on s7.COLUMN06 = p1.COLUMN02   
-- left outer join SATABLE008 s8 on s8.COLUMN14 =s7.COLUMN01 and   s8.COLUMN04= p2.COLUMN03 
--  inner join MATABLE007 m7 on m7.COLUMN02=p2.COLUMN03 and isnull(m7.COLUMNA13,0)=0 
--  and  m7.COLUMNA03=@AcOwner
--  left outer join CONTABLE007 c7 on c7.COLUMN02=p1.COLUMN24 
--  left outer join MATABLE013 m13 on m13.COLUMN02=p2.COLUMN25
--  LEFT OUTER JOIN SATABLE002 S2 ON  S2.COLUMN02=p1.COLUMN05 where  isnull((p1.COLUMNA13),0)=0 and  p1.COLUMN03=1586 and
--( p1.COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',@OPUnit) s) or p1.columna02 is null) AND  p1.COLUMNA03=@AcOwner and
--p1.COLUMN06 between @FromDate and @ToDate and p1.COLUMN06 >=@FiscalYearStartDt


--) Query

--set @Query1='select * from #JobingLedger '+@whereStr+'order by dt desc '
--exec (@Query1) 
--end

end



GO
