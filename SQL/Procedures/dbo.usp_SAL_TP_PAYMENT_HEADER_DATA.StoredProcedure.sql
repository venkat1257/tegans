USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_SAL_TP_PAYMENT_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SAL_TP_PAYMENT_HEADER_DATA]
(
----//EMPHCS1136 customer name is not getting for  payment by srinivas 13/09/2015
-- EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
	@SalesOrderID nvarchar(250),@Form nvarchar(250),@AcOwner int=null
)

AS
BEGIN
if(@Form='0')
begin  
-- EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
   SELECT  COLUMN02 , COLUMN05,COLUMN06 ,COLUMN09,  COLUMN11 ,COLUMN12, 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN13,COLUMN14, COLUMN15,COLUMN16,COLUMN17,COLUMN19,COLUMN20,COLUMN21 ,COLUMN29 ,COLUMN39 Location,COLUMN06 AddressC
   ,(case when SATABLE009.COLUMN03 = 1287 then (select COLUMN05 from SATABLE001 where COLUMN02=SATABLE009.COLUMN05)  
		   ELSE (select COLUMN05 from SATABLE002 where COLUMN02=SATABLE009.COLUMN05)
  end)  cName
   FROM SATABLE009 WHERE  COLUMN06= cast( @SalesOrderID as nvarchar) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
   end
   else if(@SalesOrderID='0')
   begin
    SELECT  COLUMN02 , COLUMN05,COLUMN06 ,COLUMN09,  COLUMN11 ,COLUMN12, 
    --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN13,COLUMN14, COLUMN15,COLUMN16,COLUMN17,COLUMN19,COLUMN20,COLUMN21 ,COLUMN29  ,COLUMN39 Location,COLUMN06 AddressC
   ,(case when SATABLE009.COLUMN03 = 1287 then (select COLUMN05 from SATABLE001 where COLUMN02=SATABLE009.COLUMN05)  
		   ELSE (select COLUMN05 from SATABLE002 where COLUMN02=SATABLE009.COLUMN05)
  end)  cName
   FROM SATABLE009 WHERE  COLUMN02=@Form  and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
   end 
   else 
   begin
   SELECT  COLUMN02 , COLUMN05,COLUMN06 ,COLUMN09,  COLUMN11 ,COLUMN12, 
   --EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
   COLUMN13,COLUMN14, COLUMN15,COLUMN16,COLUMN17,COLUMN19,COLUMN20,COLUMN21,COLUMN29 ,COLUMN39 Location,COLUMN06 AddressC
   ,(case when SATABLE009.COLUMN03 = 1287 then (select COLUMN05 from SATABLE001 where COLUMN02=SATABLE009.COLUMN05)  
		   ELSE (select COLUMN05 from SATABLE002 where COLUMN02=SATABLE009.COLUMN05)
  end)  cName
   FROM SATABLE009 WHERE (  COLUMN02=@Form) and COLUMNA03=@AcOwner and isnull(COLUMNA13,0)=0
END
end









GO
