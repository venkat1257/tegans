USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[barcodestracking]    Script Date: 8/31/2020 6:41:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[barcodestracking]
(
@Id nvarchar(250) = NULL,				 @FormId nvarchar(250) = NULL,			@Trans_Id nvarchar(250) = NULL,	@Qty nvarchar(250) = NULL,
@Item nvarchar(250) = NULL,      @Barcode nvarchar(250) = NULL,			@Isactive nvarchar(250) = NULL, 	

		   
@COLUMNA02 INT = NULL,		   @COLUMNA03 INT = NULL,			@COLUMNA06  DATETIME = NULL,     @COLUMNA07  DATETIME = NULL,  @COLUMNA08 INT = NULL,
@COLUMNA12 INT = NULL,         @COLUMNA13 INT = NULL,           @DIRECTION  NVARCHAR(250),
@TabelName nvarchar(250) = NULL, @ReturnValue nvarchar(250) = NULL 
)
AS
BEGIN
BEGIN TRY
IF(@DIRECTION = 'INSERT')
BEGIN
if not exists(select Id from activebarcodes where FormId=@FormId and Trans_Id=@Trans_Id and Barcode=@Barcode and COLUMNA03=@COLUMNA03 and isnull(Isactive,0)=1 and isnull(COLUMNA13,0)=0)
begin
	INSERT INTO activebarcodes 
	(FormId, Trans_Id, Item, Barcode, Isactive, Qty,
	COLUMNA02,COLUMNA03,COLUMNA06,COLUMNA07,COLUMNA08,COLUMNA12,COLUMNA13)
	VALUES
	(@FormId, @Trans_Id, @Item, @Barcode, @Isactive,@Qty,
	@COLUMNA02,@COLUMNA03,@COLUMNA06,@COLUMNA07,@COLUMNA08,@COLUMNA12,@COLUMNA13)
end

END
ELSE IF(@DIRECTION = 'SELECT')
BEGIN
	SELECT b.Id,i.COLUMN04 'Item',format(GETDATE(),'dd.MM.yy') 'PACKDATE',format(GETDATE()+3,'dd.MM.yy') 'EXPIRY',cast(s.COLUMN04 as decimal(18,2)) 'MRP',b.Qty,b.Barcode 'UPC',u.COLUMN04 'UOM',st.COLUMN04 'Style'
	 FROM activebarcodes b
	INNER JOIN MATABLE007 i on b.Item = i.COLUMN02 and b.COLUMNA03 = i.COLUMNA03 and isnull(i.COLUMNA13, 0) = 0
	LEFT JOIN MATABLE002 u on u.COLUMN03 = 11119 and u.COLUMN02 = i.COLUMN63 and isnull(u.COLUMNA13, 0) = 0  
	LEFT JOIN MATABLE002 st on st.COLUMN03 = 11126 and st.COLUMN02 = i.COLUMN43 and isnull(st.COLUMNA13, 0) = 0  
	INNER JOIN MATABLE024 s on s.COLUMN06 = 'Sales' and s.COLUMN07 = i.COLUMN02 and s.COLUMNA03 = i.COLUMNA03 and isnull(s.COLUMNA13, 0) = 0  
	WHERE b.COLUMNA03 = @COLUMNA03 and  isnull(b.COLUMNA02,0) = isnull(@COLUMNA02,0) and isnull(b.Isactive,0)=1 and isnull(b.COLUMNA13,0)=0
END
--ELSE IF(@DIRECTION = 'Barcode')
--BEGIN
--	SELECT p.COLUMN02 Id,i.COLUMN04 'Item',format(GETDATE(),'dd.MM.yy') 'PACKDATE',format(GETDATE()+3,'dd.MM.yy') 'EXPIRY',cast(s.COLUMN04 as decimal(18,2)) 'MRP',P.Qty,i.COLUMN06 'UPC' FROM PRTABLE011 p
--	INNER JOIN MATABLE007 i on p.COLUMN07 = i.COLUMN02 and p.COLUMNA03 = i.COLUMNA03 and isnull(i.COLUMNA13, 0) = 0
--	INNER JOIN MATABLE024 s on s.COLUMN06 = 'Sales' and s.COLUMN07 = i.COLUMN02 and s.COLUMNA03 = i.COLUMNA03 and isnull(s.COLUMNA13, 0) = 0  
--	WHERE p.COLUMNA03 = @COLUMNA03 and isnull(p.COLUMN02,0)=@Id and isnull(p.COLUMNA13,0)=0
--END
ELSE IF(@DIRECTION = 'Update')
BEGIN
UPDATE activebarcodes SET Isactive =0 ,COLUMNA08 =@COLUMNA08 WHERE Id=@Id
END
ELSE IF(@DIRECTION = 'Delete')
BEGIN
UPDATE activebarcodes SET COLUMNA13 =1 ,COLUMNA08 =@COLUMNA08 WHERE Id=@Id --,COLUMNA07 =@COLUMNA07
END

END TRY
BEGIN CATCH
DECLARE @tempSTR NVARCHAR(450)
	set  @tempSTR=((isnull(@tempSTR,''+' ' + CHAR(13)+CHAR(10))+'')+(' ' + CHAR(13)+CHAR(10) + ''+  '***********************Exception***********************'+   '' + CHAR(13)+CHAR(10) + '' +
   'Exception  Occured at Line#'+cast((select  ERROR_LINE()) as nvarchar(20)) +'' + CHAR(13)+CHAR(10) + ''+cast((select  ERROR_MESSAGE())as nvarchar(200))))
   exec [CheckDirectory] @tempSTR,'EXCEPTION_activebarcode.txt',0
	
END CATCH
END


GO
