USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[QuickSearch]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[QuickSearch]
(
	@Item  nvarchar(250)= null,
	@UPC    nvarchar(250)= null,
	@VendorPart nvarchar(250)= null,
	@ItemDesc nvarchar(max)= null,
	@AcOwner nvarchar(250)= null
	)
as 
begin
if(@Item!='')
begin
select m.column04 as Item,m.COLUMN50 ItemDesc,m.column06 as UPC, iif(f.COLUMN19='10000','EACH',m1.column04) as Units,c1.column04 as Location,f1.column04 as Lot#,isnull(convert(DOUBLE PRECISION,f.column04),0) as QtyAvailable,  o.column03 as OperatingUnit,m.column46 as Image from MATABLE007 m
	left join FITABLE010 f on m.COLUMN02=f.COLUMN03 and f.COLUMNA03=m.COLUMNA03   and isnull (f.COLUMNA13,0) = 0
	left join MATABLE002 m1 on f.COLUMN19=m1.COLUMN02 and m1.COLUMNA03=m.COLUMNA03  and isnull (m1.COLUMNA13,0) = 0
	left join FITABLE043 f1 on f.COLUMN22 = f1.COLUMN02 and f1.COLUMNA03=f.COLUMNA03  and isnull (f1.COLUMNA13,0) = 0
	left join conTABLE007 o on o.COLUMN02 = f.COLUMNA02 and o.COLUMNA03=m.COLUMNA03  and isnull (o.COLUMNA13,0) = 0
	left join CONTABLE030 c1 on c1.COLUMN02 = f.COLUMN21 and c1.COLUMNA03=f.COLUMNA03 and isnull (c1.COLUMNA13,0) = 0
	where m.COLUMN02=@Item and m.COLUMNA03=@AcOwner and isnull (m.COLUMNA13,0) = 0
end	
else if(@UPC!='')
begin
select m.column04 as Item,m.COLUMN50 ItemDesc,m.column06 as UPC, iif(f.COLUMN19='10000','EACH',m1.column04) as Units,c1.column04 as Location,f1.column04 as Lot#,isnull(convert(DOUBLE PRECISION,f.column04),0) as QtyAvailable,  o.column03 as OperatingUnit,m.column46 as Image from MATABLE007 m
	left join FITABLE010 f on m.COLUMN02=f.COLUMN03 and f.COLUMNA03=m.COLUMNA03   and isnull (f.COLUMNA13,0) = 0
	left join MATABLE002 m1 on f.COLUMN19=m1.COLUMN02 and m1.COLUMNA03=m.COLUMNA03  and isnull (m1.COLUMNA13,0) = 0
	left join FITABLE043 f1 on f.COLUMN22 = f1.COLUMN02 and f1.COLUMNA03=f.COLUMNA03  and isnull (f1.COLUMNA13,0) = 0
	left join conTABLE007 o on o.COLUMN02 = f.COLUMNA02 and o.COLUMNA03=m.COLUMNA03  and isnull (o.COLUMNA13,0) = 0
	left join CONTABLE030 c1 on c1.COLUMN02 = f.COLUMN21 and c1.COLUMNA03=f.COLUMNA03 and isnull (c1.COLUMNA13,0) = 0
	where m.COLUMN06=@UPC and m.COLUMNA03=@AcOwner  and isnull (m.COLUMNA13,0) = 0
end
else if(@VendorPart!='')
begin
select m.column04 as Item,m.COLUMN50 ItemDesc,m.column06 as UPC, iif(f.COLUMN19='10000','EACH',m1.column04) as Units,c1.column04 as Location,f1.column04 as Lot#,isnull(convert(DOUBLE PRECISION,f.column04),0) as QtyAvailable,  o.column03 as OperatingUnit,m.column46 as Image from MATABLE007 m
	left join FITABLE010 f on m.COLUMN02=f.COLUMN03 and f.COLUMNA03=m.COLUMNA03   and isnull (f.COLUMNA13,0) = 0
	left join MATABLE002 m1 on f.COLUMN19=m1.COLUMN02 and m1.COLUMNA03=m.COLUMNA03  and isnull (m1.COLUMNA13,0) = 0
	left join FITABLE043 f1 on f.COLUMN22 = f1.COLUMN02 and f1.COLUMNA03=f.COLUMNA03  and isnull (f1.COLUMNA13,0) = 0
	left join conTABLE007 o on o.COLUMN02 = f.COLUMNA02 and o.COLUMNA03=m.COLUMNA03  and isnull (o.COLUMNA13,0) = 0
	left join CONTABLE030 c1 on c1.COLUMN02 = f.COLUMN21 and c1.COLUMNA03=f.COLUMNA03 and isnull (c1.COLUMNA13,0) = 0
	where m.COLUMN09=@VendorPart and m.COLUMNA03=@AcOwner and isnull (m.COLUMNA13,0) = 0
end
else if(@ItemDesc!='')
begin
select m.column04 as Item,m.COLUMN50 ItemDesc,m.column06 as UPC, iif(f.COLUMN19='10000','EACH',m1.column04) as Units,c1.column04 as Location,f1.column04 as Lot#,isnull(convert(DOUBLE PRECISION,f.column04),0) as QtyAvailable,  o.column03 as OperatingUnit,m.column46 as Image from MATABLE007 m
	left join FITABLE010 f on m.COLUMN02=f.COLUMN03 and f.COLUMNA03=m.COLUMNA03   and isnull (f.COLUMNA13,0) = 0
	left join MATABLE002 m1 on f.COLUMN19=m1.COLUMN02 and m1.COLUMNA03=m.COLUMNA03  and isnull (m1.COLUMNA13,0) = 0
	left join FITABLE043 f1 on f.COLUMN22 = f1.COLUMN02 and f1.COLUMNA03=f.COLUMNA03  and isnull (f1.COLUMNA13,0) = 0
	left join conTABLE007 o on o.COLUMN02 = f.COLUMNA02 and o.COLUMNA03=m.COLUMNA03  and isnull (o.COLUMNA13,0) = 0
	left join CONTABLE030 c1 on c1.COLUMN02 = f.COLUMN21 and c1.COLUMNA03=f.COLUMNA03 and isnull (c1.COLUMNA13,0) = 0
	where m.COLUMN50=@ItemDesc and m.COLUMNA03=@AcOwner and isnull (m.COLUMNA13,0) = 0
end
end





GO
