USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TP_IAItemDetails]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_PUR_TP_IAItemDetails]
(
	@ItemID nvarchar(250)=NULL,
	@uom nvarchar(250)=NULL,
	--EMPHCS1379 rajasekhar reddy 17/11/2015 UPC Scan in Stock Transfer and Inventory Adjustment
	@OPUnit nvarchar(250)=NULL,
	@AcOwner nvarchar(250)=NULL,
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
	@location nvarchar(250)=NULL,
	@lotno  nvarchar(250)=NULL,
	@Project  nvarchar(250)=NULL
)

AS
BEGIN  
		IF EXISTS(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		begin
		set @ItemID=(SELECT COLUMN02 FROM MATABLE007 WHERE COLUMN06=cast(@ItemID as nvarchar(250)) and COLUMNA03=@AcOwner and (COLUMNA02=@OPUnit or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 )
		end
	select MATABLE007.COLUMN02,MATABLE007.COLUMN06,MATABLE007.COLUMN50 COLUMN09,isnull((FITABLE010.COLUMN17),(MATABLE024.COLUMN04)) COLUMN17,
	isnull(( FITABLE010.COLUMN12),0) COLUMN12,
	isnull(( FITABLE010.COLUMN08),0) COLUMN08,(case when (@uom!='' and @uom is not null) then @uom else  MATABLE007.COLUMN63 end)COLUMN63 from MATABLE007
	left join FITABLE010 FITABLE010 on  FITABLE010.COLUMN13=@OPUnit and FITABLE010.COLUMNA03=@AcOwner and FITABLE010.COLUMN03=MATABLE007.COLUMN02 and FITABLE010.COLUMN19=(case when (@uom!='' and @uom is not null) then @uom when MATABLE007.COLUMN63='' then 10000 else  isnull(MATABLE007.COLUMN63,10000) end)  and
	--EMPHCS1544	adding Location in stockTransfer and adding Location ,lot in Inventory adjestment By Raj.Jr
	 isnull(FITABLE010.COLUMN21,0)=isnull(@location,0) and (isnull(FITABLE010.COLUMN22,0)=@lotno or isnull(FITABLE010.COLUMN22,0)=0)
	 and (isnull(FITABLE010.COLUMN23,0)=@Project or isnull(FITABLE010.COLUMN23,0)=0)
	left join MATABLE024 MATABLE024 on  MATABLE024.COLUMNA02=@OPUnit and MATABLE024.COLUMNA03=@AcOwner and MATABLE024.COLUMN07=MATABLE007.COLUMN02 and MATABLE024.COLUMN06='purchase'
	 WHERE  MATABLE007.COLUMN47='False' and MATABLE007.COLUMNA13=0 and MATABLE007.COLUMNA03=@AcOwner and  (MATABLE007.COLUMN02=@ItemID or MATABLE007.COLUMN06=@ItemID)
END



GO
