USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_CON_BL_LOCATION]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CON_BL_LOCATION]
(
    @COLUMN02   nvarchar(250),       @COLUMN03   nvarchar(250)=null,  @COLUMN04   nvarchar(250)=null,  
	@COLUMN05   nvarchar(250)=null,  @COLUMN06   nvarchar(250)=null,  @COLUMN07   nvarchar(250)=null,  
	@COLUMN08   nvarchar(250)=null,  @COLUMN09   nvarchar(250)=null,  @COLUMN10   nvarchar(250)=null,
	@COLUMN11   nvarchar(250)=null,  @COLUMN12   nvarchar(250)=null,  @COLUMN13   nvarchar(250)=null,  
	@COLUMN14   nvarchar(250)=null,  @COLUMN15   nvarchar(250)=null,  @COLUMN16   nvarchar(250)=null, 
    @COLUMN17   nvarchar(250)=null,  @COLUMN18   nvarchar(250)=null,  @COLUMN19   nvarchar(250)=null, 
	@COLUMN20  nvarchar(250)=null,   @COLUMN21  nvarchar(250)=null,   @COLUMN22  nvarchar(250)=null, 
	@COLUMN23  nvarchar(250)=null,   @COLUMN24  nvarchar(250)=null,	  @COLUMN25  nvarchar(250)=null,
	@COLUMN26  nvarchar(250)=null,   @COLUMN27  nvarchar(250)=null,   @COLUMN28  nvarchar(250)=null,  
    @COLUMN29   nvarchar(250)=null,  @COLUMN30   nvarchar(250)=null,  @COLUMN31   nvarchar(250)=null, 
	@COLUMN32  nvarchar(250)=null,   @COLUMN33  nvarchar(250)=null,   @COLUMN34  nvarchar(250)=null, 
	@COLUMN35  nvarchar(250)=null,   @COLUMN36  nvarchar(250)=null,	  @COLUMN37  nvarchar(250)=null, 
	  @COLUMN39  nvarchar(250)=null,	  @COLUMN40  nvarchar(250)=null,
	@COLUMNA01  varchar(100)=null,   @COLUMNA02  varchar(100)=null, 
	@COLUMNA03  varchar(100)=null,   @COLUMNA04  varchar(100)=null,   @COLUMNA05  varchar(100)=null, 
	@COLUMNA06  nvarchar(250)=null,  @COLUMNA07  nvarchar(250)=null,  @COLUMNA08  varchar(100)=null, 
	@COLUMNA09  nvarchar(250)=null,  @COLUMNA10  nvarchar(250)=null,  @COLUMNA11  varchar(100)=null, 
	@COLUMNA12  nvarchar(250)=null,  @COLUMNA13  nvarchar(250)=null,  @COLUMNB01  nvarchar(250)=null, 
	@COLUMNB02  nvarchar(250)=null,  @COLUMNB03  nvarchar(250)=null,  @COLUMNB04  nvarchar(250)=null,  
	@COLUMNB05  nvarchar(250)=null,  @COLUMNB06  nvarchar(250)=null,  @COLUMNB07  nvarchar(250)=null,  
	@COLUMNB08  nvarchar(250)=null,  @COLUMNB09  nvarchar(250)=null,  @COLUMNB10  nvarchar(250)=null, 
	@COLUMNB11  varchar(100)=null,   @COLUMNB12  varchar(100)=null,   @COLUMND01  nvarchar(250)=null,  
	@COLUMND02  nvarchar(250)=null,  @COLUMND03  nvarchar(250)=null,  @COLUMND04  nvarchar(250)=null,  
	@COLUMND05  nvarchar(250)=null,  @COLUMND06  nvarchar(250)=null,  @COLUMND07  nvarchar(250)=null,  
	@COLUMND08  nvarchar(250)=null,  @COLUMND09  nvarchar(250)=null,  @COLUMND10  varchar(100)=null,   
	@Direction  nvarchar(250),       @TabelName  nvarchar(250),       @ReturnValue nvarchar(250)=null
)
AS
BEGIN
begin try
   declare @tempSTR nvarchar(max)
   set @tempSTR=('the values '+isnull(@COLUMN03,'')+','+isnull(@COLUMN04,'')+','+isnull(@COLUMN05,'')+','+isnull(@COLUMN06,'')+','+isnull(@COLUMN07,'')+','+isnull(@COLUMN08,'')+','+isnull(@COLUMN09,'')+','+isnull(@COLUMN10,'')+','+isnull(@COLUMN11,'')+','
   +isnull(@COLUMN12,'')+','+isnull(@COLUMN13,'')+','+isnull(@COLUMN14,'')+','+isnull(@COLUMN15,'')+','+isnull(@COLUMN16,'')+','+isnull(@COLUMN17,'')+','+isnull(@COLUMN18,'')+','+isnull(@COLUMN19,'')+','+isnull(@COLUMN20,'')+','+isnull(@COLUMN21,'')+','+
   +isnull(@COLUMN22,'')+','+isnull(@COLUMN23,'')+','+isnull(@COLUMN24,'')+','+isnull(@COLUMN25,'')+','+isnull(@COLUMN26,'')+','+isnull(@COLUMN27,'')+','+isnull(@COLUMN28,'')+','+isnull(@COLUMN29,'')+','+isnull(@COLUMN30,'')+','+isnull(@COLUMN31,'')+','+
   isnull(@COLUMN32,'')+','+isnull(@COLUMN33,'')+','+isnull(@COLUMN34,'')+','+isnull(@COLUMN35,'')+','+isnull(@COLUMN36,'')+','+isnull(@COLUMN37,'')+' of  '+isnull(@TabelName,'')+' succeeded at  ');

IF  @TabelName='CONTABLE030'
BEGIN
set @COLUMNA02=( CASE WHEN (@COLUMN11!= '' and @COLUMN11 is not null and @COLUMN11!= '0') THEN @COLUMN11 else @COLUMNA02  END )
EXEC usp_CON_TP_CONTABLE030 
   @COLUMN02,  @COLUMN03,  @COLUMN04,  @COLUMN05,  @COLUMN06,  @COLUMN07,  @COLUMN08,  @COLUMN09,  @COLUMN10,  @COLUMN11,
   @COLUMN12,  @COLUMN13,  @COLUMN14,  @COLUMN15,  @COLUMN16,  @COLUMN17,  @COLUMN18,  @COLUMN19,  @COLUMN20,  @COLUMN21,
   @COLUMN22,  @COLUMN23,  @COLUMN24,  @COLUMN25,  @COLUMN26,  @COLUMN27,  @COLUMN28,  @COLUMN29,  @COLUMN30,  @COLUMN31,
   @COLUMN32,  @COLUMN33,  @COLUMN34,  @COLUMN35,  @COLUMN36,  @COLUMN37,   @COLUMN39,  @COLUMN40,  @COLUMNA01, @COLUMNA02, @COLUMNA03, @COLUMNA04, 
   @COLUMNA05, @COLUMNA06, @COLUMNA07, @COLUMNA08, @COLUMNA09, @COLUMNA10, @COLUMNA11, @COLUMNA12, @COLUMNA13, @COLUMNB01, 
   @COLUMNB02, @COLUMNB03, @COLUMNB04, @COLUMNB05, @COLUMNB06, @COLUMNB07, @COLUMNB08, @COLUMNB09, @COLUMNB10, @COLUMNB11, 
   @COLUMNB12, @COLUMND01, @COLUMND02, @COLUMND03, @COLUMND04, @COLUMND05, @COLUMND06, @COLUMND07, @COLUMND08, @COLUMND09,
   @COLUMND10, @Direction, @TabelName, @ReturnValue
END 
   exec [CheckDirectory] @tempSTR,'usp_CON_BL_LOCATION.txt',0
END TRY
			BEGIN CATCH	 
			SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() AS ErrorState,
        ERROR_PROCEDURE() AS ErrorProcedure,
        ERROR_LINE() AS ErrorLine,
        ERROR_MESSAGE() AS ErrorMessage
			begin
   set @tempSTR='transaction failed at  ';
   exec [CheckDirectory] @tempSTR,'usp_CON_BL_LOCATION.txt',0
			return 0
			end
			END CATCH
end




GO
