USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PUR_TR_Address1]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PUR_TR_Address1]
(
	@PurchaseOrderID int
)

AS
BEGIN


	SELECT a.COLUMN05, a.COLUMN06,a.COLUMN07,a.COLUMN08,a.COLUMN09,a.COLUMN10,a.COLUMN11
		
	FROM dbo.PUTABLE009 a
	LEFT JOIN dbo.PUTABLE001 po on po.COLUMN01 = a.COLUMN19
	WHERE a.COLUMN19 = (select COLUMN01 FROM dbo.PUTABLE001 WHERE COLUMN02= @PurchaseOrderID)
END












GO
