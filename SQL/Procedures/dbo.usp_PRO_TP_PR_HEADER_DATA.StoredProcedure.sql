USE [Tegans]
GO
/****** Object:  StoredProcedure [dbo].[usp_PRO_TP_PR_HEADER_DATA]    Script Date: 8/31/2020 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_PRO_TP_PR_HEADER_DATA]
(
	@ProjectID int = null,
	@MileStoneID int = null,
	@Ref varchar(250) = null
)

AS
BEGIN
if(@Ref!='')
begin 
	SELECT  a.COLUMN02 Project,b.COLUMN02 MileStone,a.COLUMN06 Customer,a.COLUMN19 'Operating Unit',a.COLUMN18 Deparment
	,S2.COLUMN05 cName
	FROM PRTABLE001 a  inner join PRTABLE002 b on a.COLUMN01=b.COLUMN15 
	inner join SATABLE002 S2 ON S2.COLUMN02=a.COLUMN06
	where   a.COLUMN02=@ProjectID 
end
else if(@MileStoneID!='')
begin 
	SELECT  b.COLUMN02,b.COLUMN04
	FROM PRTABLE001 a  inner join PRTABLE002 b on a.COLUMN01=b.COLUMN15 
	where   a.COLUMN02=@MileStoneID 
end
else
begin 
	SELECT  a.COLUMN02 Project,null MileStone ,a.COLUMN06 Customer,a.COLUMN19 'Operating Unit',a.COLUMN18 Deparment
	,S2.COLUMN05 cName
	FROM PRTABLE001 a   
	inner join SATABLE002 S2 ON S2.COLUMN02=a.COLUMN06
	where   a.COLUMN02=@ProjectID 
end
END











GO
