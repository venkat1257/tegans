﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using eBizSuiteAppBAL.Fombuilding;
using eBizSuiteAppDAL.classes;
using eBizSuiteAppUI.Models;
using eBizSuiteAppModel.Table;
using System.Data.SqlClient;
//using DataTablesServerSide.Models;
using System.Configuration;
using System.Data;
using WebMatrix.Data;
using System.Web.Mvc;

namespace DataTablesServerSide
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetTableData(string FormName,string ViewSort,string SortByStatus,string ItemType)
        {
            try
            {
                var echo = int.Parse(HttpContext.Current.Request.Params["sEcho"]);
                var displayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
                var displayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
                var sortOrder = HttpContext.Current.Request.Params["sSortDir_0"].ToString(CultureInfo.CurrentCulture);
                var roleId = HttpContext.Current.Request.Params["roleId"].ToString(CultureInfo.CurrentCulture);
                var searchdata = HttpContext.Current.Request.Params["sSearch"];
                //var records = GetRecordsFromDatabaseWithFilter().ToList();
                if (FormName == "")
                    FormName = Convert.ToString(HttpContext.Current.Session["FormName"]);
                var FormId = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).FirstOrDefault().COLUMN02.ToString();
                var idi=ViewSort;
                if ((FormId == "1532" || FormId == "1603" || FormId == "1604") && idi == "") idi = Convert.ToString(HttpContext.Current.Session["idi"]);
                if (FormId == "")
                    FormId = Convert.ToString(HttpContext.Current.Session["id"]);
                if (ItemType == "")
                    ItemType = Convert.ToString(HttpContext.Current.Session["ItemType"]);
                if (SortByStatus == "")
                    SortByStatus = Convert.ToString(HttpContext.Current.Session["SortByStatus"]);
                if (ViewSort == "")
                    ViewSort = Convert.ToString(HttpContext.Current.Session["ViewSort"]);
                var infospace = " | "; var mis = 0; var NextForm = "";
                if (FormId == "1277") NextForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1278).FirstOrDefault().COLUMN04.ToString();
                else if (FormId == "1275") NextForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1276).FirstOrDefault().COLUMN04.ToString();
                else if (FormId == "1656") NextForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1657).FirstOrDefault().COLUMN04.ToString();
                else if (FormId == "1276") NextForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1277).FirstOrDefault().COLUMN04.ToString();
                else if (FormId == "1273") NextForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1274).FirstOrDefault().COLUMN04.ToString();
                else if (FormId == "1532") NextForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1603).FirstOrDefault().COLUMN04.ToString();
                else NextForm = Convert.ToString(HttpContext.Current.Session["PForms"]);
                List<int> permission = HttpContext.Current.Session["Permission"] as List<int>;
                string PoNo = "ID", PreFormID = "", FromDate = "", ToDate = "";
                object[] sp = new object[7];
                sp[0] = (FormId);
                sp[1] = (HttpContext.Current.Session["OPUnit"]);
                sp[2] = (HttpContext.Current.Session["AcOwner"]);
                sp[3] = (searchdata);
                if (FormId == "1261")
                    sp[4] = (ItemType);
                else
                    sp[4] = (SortByStatus);
                sp[5] = (HttpContext.Current.Session["FormatDate"]);
                sp[6] = (ViewSort);
                var db = Database.Open("sqlcon");
                var records = db.Query("Exec USP_PROC_FormBuildInfo @FormId=@0,@OPUnit=@1,@AcOwner=@2,@SearchData=@3,@FilterByStatus=@4,@dateF=@5,@SortBy=@6", sp);
                var sb = new StringBuilder();
                sb.Append(@"{" + "\"sEcho\": " + echo + ",");
                sb.Append("\"recordsTotal\": " + records.Count() + ",");
                sb.Append("\"recordsFiltered\": " + records.Count() + ",");
                sb.Append("\"iTotalRecords\": " + records.Count() + ",");
                sb.Append("\"iTotalDisplayRecords\": " + records.Count() + ",");
                sb.Append("\"aaData\": [");
                if (!records.Any())
                {
                    sb.Append("]}");
                    return sb.ToString();
                }

                //var orderedResults = sortOrder == "asc"
                //                     ? records.OrderBy(o => o.ID)
                //                     :  records.OrderByDescending(o => o.ID);
                var orderedResults = records;
                var itemsToSkip = displayStart;
                var pagedResults = orderedResults.Skip(itemsToSkip).Take(displayLength).ToList();
                var hasMoreRecords = false;

                foreach (var result in pagedResults)
                {
                    if (hasMoreRecords)
                    {
                        sb.Append(",");
                    }

                    sb.Append("["); var links = "";
                    for (int i = 0; i < result.Columns.Count; i++)
                    {
                        var coldata =Convert.ToString(result[result.Columns[i]]);
                        if (coldata != "" && coldata !=null)
                            coldata = coldata.Replace("\t", " ").Replace("\n", " ").Replace("\r", " ").Replace("\"", " ");
                        if (result.Columns[i] == "ID")
                        {
                            if (FormId.ToString() == "1532")
                            {
                                links = "<div class='ch'>";
                                if (permission.Contains(22933))
                                {
                                    links = links + "<a href='/PointOfSales/PointOfSalesEdit?idi=" + idi + "&FormName=" + FormName + "&ide=" + result.ID + "' style=''>Edit</a> " + infospace + "";
                                    links = links + "<a ide=" + result.ID + " id='btnDelete' class='posDelete' href='#'>Delete</a> " + infospace + "";
                                }
                                if (permission.Contains(22934))
                                {
                                    links = links + "<a href='/PointOfSales/PointOfSalesView?idi=" + idi + "&FormName=" + FormName + "&ide=" + result.ID + "' style=''>View</a> " + infospace + "" +
                                     "<a  href='/ReturnReceipt/ReturnReceiptNew?idi=3139&FormName=" + NextForm + "&ide=" + result.ID + "'>Return</a>";
                                }
                                links = links + "</div>";
                            }
                            else if (FormId.ToString() == "1603")
                            {
                                links = "<div class='ch'>";
                                if (permission.Contains(22933))
                                { links = links + "<a href='/ReturnReceipt/ReturnReceiptEdit?idi=" + idi + "&FormName=" + FormName + "&ide=" + result.ID + "' style=''>Edit</a> " + infospace + ""; }
                                if (permission.Contains(22934))
                                {
                                    links = links + "<a href='/ReturnReceipt/ReturnReceiptView?idi=" + idi + "&FormName=" + FormName + "&ide=" + result.ID + "' style=''>View</a>";
                                }
                                links = links + "</div>";
                            }
                            else if (FormId.ToString() == "1604")
                            {
                                links = "<div class='ch'>";
                                if (permission.Contains(22933))
                                { links = links + "<a href='/ServiceReceipt/ServiceReceiptEdit?idi=" + idi + "&FormName=" + FormName + "&ide=" + result.ID + "' style=''>Edit</a> " + infospace + ""; }
                                if (permission.Contains(22934))
                                {
                                    links = links + "<a href='/ServiceReceipt/ServiceReceiptView?idi=" + idi + "&FormName=" + FormName + "&ide=" + result.ID + "' style=''>View</a>";
                                }
                                links = links + "</div>";
                            }
                            else if (FormId.ToString() == "1623")
                            {
                                links = "<div class='ch'><input id='Qty' text='Qty' value='1' style='width:20px;margin-right:5px;' /> <a href=''  id='btnGenerate' itemid=" + result.ID + " role=" + result.AliasID + " title='" + result["UPC/Barcode"] + "'>Print</a></div>";
                            }
                            else
                            {
                                if (FormId.ToString() == "1404")
                                {
                                    links = "<div class='ch'>";
                                    if (permission.Contains(22933))
                                    {
                                        links = links + "<a href='/FormBuilding/Edit?ide=" + result.ID + "&FormName=" + FormName + "' style=''>Edit</a> " + infospace + "";
                                    }
                                    links = links + "<a href='/FormBuilding/Detailes?ide=" + result.ID + "&FormName=" + FormName + "' style=''>View</a></div>";
                                }
                                if (FormId.ToString() == "1669")
                                {
                                    links = "<div class='ch'>";
                                    //if (permission.Contains(22933))
                                    //{
                                    //    links = links + "<a href='/FormBuilding/Edit?ide=" + result.ID + "&FormName=" + FormName + "' style=''>Edit</a> " + infospace + "";
                                    //}
                                    links = links + "<a href='/FormBuilding/Detailes?ide=" + result.ID + "&FormName=" + FormName + "' style=''>View</a></div>";
                                }
                                else
                                {
                                    links = links + "<div class='ch'>";
                                    if (permission.Contains(22933))
                                    {
                                        links = links + "<a href='/FormBuilding/Edit?ide=" + result.ID + "&FormName=" + FormName + "' style=''>Edit</a> " + infospace + "";
                                    }
                                    if (FormId.ToString() == "1523")
                                    {
                                        links = links + "<a href='/FormBuilding/Detailes?ide=" + result.ID + "&FormName='Sales Order'' style=''>View</a>";
                                    }
                                    else
                                    {
                                        links = links + "<a href='/FormBuilding/Detailes?ide=" + result.ID + "&FormName=" + FormName + "' style=''>View</a>";
                                    }
                                    if (NextForm != null && NextForm != "" && permission.Contains(22935))
                                    {
                                        links = links + "" + infospace + " <a class='' id='retLnk' href='FormBuild?FormName=" + NextForm + "&ide=" + result[PoNo] + "&Prevfi=" + PreFormID + "'>" + NextForm + "</a> ";
                                        if (FormId.ToString() == "1375")
                                        {
                                            var ofm = "Sales Order"; var refe = "oppConvert";
                                            links = links + "" + infospace + " <a class='' id='retLnk' href='FormBuild?FormName=" + ofm + "&ide=" + result[PoNo] + "&Prevfi=" + PreFormID + "&Ref=" + refe + "'>" + ofm + "</a>";
                                        }
                                    }
                                    if (FormId.ToString() == "1250")
                                    {
                                        links = links + " " + infospace + " <a href='/FormBuilding/AccountsDisplay?ide=" + result.ID + "&typ = " + result.Typ + "&FromDate = " + FromDate + "&ToDate = " + ToDate + "&Name = " + result["Account Name"] + "'>GLView</a> ";
                                    }
                                    if (FormId.ToString() == "1527")
                                    {
                                        links = links + "" + infospace + "<a href='/FormBuilding/ClearWriteCheque?ide=" + result.ID + "&FormName = " + FormName + "'>Clear</a>";
                                    }
                                    links = links + "</div>";
                                }
                            }
                            sb.Append("\"" + links + "\",");
                        }
                        else if (result.Columns[i] == "AliasID" && FormId.ToString() == "1623") { }
                        else if (i == result.Columns.Count - 1)
                            sb.Append("\"" + coldata + "\"");
                        else
                            sb.Append("\"" + coldata + "\",");
                    }
                    //sb.ToString().TrimEnd(',');
                    //sb.Append("\"<img class='image-details' src='content/details_open.png' runat='server' height='16' width='16' alt='View Details'/>\"");
                    sb.Append("]");
                    hasMoreRecords = true;
                }
                sb.Append("]}");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                //return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
                return "";
            }
        }

        public void GetExceptionMsg(Exception ex)
        {
            var msg = "Failed........";
            msg = msg + " Due to " + ex.Message.ToString().Replace("\r\n", " ");
            var linenumber = ex.StackTrace.Substring(ex.StackTrace.Length - 5, 5);
            Session["MessageFrom"] = msg + " at line no : " + linenumber;
            Session["SuccessMessageFrom"] = "fail";
        }
    }
}
