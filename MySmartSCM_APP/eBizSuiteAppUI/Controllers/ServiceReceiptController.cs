﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class ServiceReceiptController : Controller
    {
        //
        // GET: /ServiceReceipt/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        CommonController taxlst = new CommonController();
        public ActionResult ServiceCreate()
        {
            try
            {
                Session["POSGridData"] = null;
                //List<SelectListItem> Customer = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                //DataTable dtdata = new DataTable();
                //cmddl.Fill(dtdata);
                //for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                //{
                //    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' and  COLUMN05='ITTY009' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Customer1 = new List<SelectListItem>();
                SqlDataAdapter cmddld = new SqlDataAdapter("select COLUMN02,COLUMN05,COLUMN04 from SATABLE002  where  (COLUMN40='True' or COLUMN40=1) and COLUMN05 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN05 ", cn);
                DataTable dtdatad = new DataTable();
                cmddld.Fill(dtdatad);
                SqlDataAdapter cmddl1 = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  COLUMN05 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN05 ", cn);
                DataTable dtdata1 = new DataTable();
                cmddl1.Fill(dtdata1);
                var DefaultFlag = ""; var DefaultFlagval = "";
                if (dtdatad.Rows.Count > 0) DefaultFlagval = dtdatad.Rows[0]["COLUMN02"].ToString();
                for (int dd = 0; dd < dtdata1.Rows.Count; dd++)
                {
                    if (DefaultFlagval == dtdata1.Rows[dd]["COLUMN02"].ToString())
                        DefaultFlag = dtdata1.Rows[dd]["COLUMN02"].ToString();
                    Customer1.Add(new SelectListItem { Value = dtdata1.Rows[dd]["COLUMN02"].ToString(), Text = dtdata1.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer1, "Value", "Text", selectedValue: DefaultFlag);
                List<SelectListItem> Amtin = new List<SelectListItem>();
                SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dta = new DataTable();
                cmda.Fill(dta);
                for (int dd = 0; dd < dta.Rows.Count; dd++)
                {
                    Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: Session["TaxInclusive"]);
                //DateFormat();
                ViewBag.DateFormat = Session["FormatDate"];
                List<SelectListItem> Attendedby = new List<SelectListItem>();
                SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                DataTable dtat = new DataTable();
                cmdat.Fill(dtat);
                var salesrep = "";
                for (int dd = 0; dd < dtat.Rows.Count; dd++)
                {
                    if (Convert.ToString(Session["eid"]) == dtat.Rows[dd]["COLUMN02"].ToString())
                        salesrep = dtat.Rows[dd]["COLUMN02"].ToString();
                    Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: salesrep);
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult ServiceDiscountTypes(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> DiscountType = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where COLUMN03=11158 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    DiscountType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["DiscountType"] = new SelectList(DiscountType, "Value", "Text");
                return PartialView("ServiceDiscountTypes");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult ServiceTaxInformation(string ItemId, string Customer, string FormName, string Qty, string Price, string Date)
        {
            try
            {
                List<SelectListItem> TaxType = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013	where (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0 and column16 in(22401,23582,23584,23583,23585,23586)	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                //DataTable dtdata = new DataTable();
                //cmddl.Fill(dtdata);
                //for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                //{
                //    TaxType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                //}
                TaxType = taxlst.TaxList(ItemId, "", Customer, "Sales", Price, "Tax", Date);
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                return PartialView("ServiceTaxInformation");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetServiceItemDetails(string ItemID)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("ServiceItemdetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string htmlstring = null, htmlstring1 = null;
                for (int g = 0; g < dt.Rows.Count; g++)
                {
                    string dvalue = Convert.ToString(dt.Rows[g][dt.Columns[7].ColumnName]);
                    string dddata = "";
                    List<SelectListItem> UOM = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        UOM.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["UOM"] = new SelectList(UOM, "Value", "Text", selectedValue: dvalue).ToString();
                    dddata += "<option value=''>--Select--</option>";
                    for (int d = 0; d < UOM.Count; d++)
                    {
                        if (UOM[d].Value == dvalue)
                            dddata += "<option value=" + UOM[d].Value + " selected>" + UOM[d].Text + "</option>";
                        else
                            dddata += "<option value=" + UOM[d].Value + ">" + UOM[d].Text + "</option>";
                    }
                     htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dItemId>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dItemName>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><select   class ='gridddl edit-mode'  name='" + dt.Columns[2].ColumnName + "'   id=dynamicuom  >" + dddata + "</select></td>";
                     //htmlstring += "<td><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dQuantity>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dPrice>" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dAmount>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                     htmlstring += "<td style='display:none;'><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "'  style='display:none;'><span id=dTaxamt style='display:none;'>" + dt.Rows[g][dt.Columns[8].ColumnName] + "</span></td>";
                     htmlstring += "<td style='display:none;'><div class=initshow><span id=ddiscount style='display:none;'>0</span></div><input type='text' id='Discount' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
                     htmlstring += "<td style='display:none;'><div class=initshow><span id=ddisper style='display:none;'>0</span></div><input type='text' id='disper1' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
                     htmlstring += "<td style='display:none;'><span id=dservicetax style='display:none;'>" + dt.Rows[g][dt.Columns[8].ColumnName] + "</span><span id=dtds style='display:none;'></span><span id=dcessper style='display:none;'>0.005</span><span id=dcess style='display:none;'>0.005</span></td>";
                     htmlstring += "<td style='display:none;'><input type='text' id='chkrow' class='txtgridclass edit-mode'  name='chkrow'    value=0  style='display:none;'><span id=saveUnitId style='display:none'>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span></td></tr>";
                 }
                return Json(new { Data = htmlstring }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("ServiceReceiptInfo", "ServiceReceipt", new { idi = idi, FormName = custform });
            }
        }
        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono = "", TransNo = "";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (frmIDchk == 1604)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "SER";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,4,len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                else if (frmIDchk == 1278)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "CP";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                pacmd.ExecuteNonQuery();
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }
        [HttpPost]
        public ActionResult Saveform(string Qty, string SubTotal, string Type, string Discount, string TaxAmt, string TotAmt, string PayAmt, string PayAcc, string Customer, string TDSamt, string Cessamt, string cesstax1, string CreatedDate, string roundoffadjamt, string Amtin, string Attendedby)
        {
            try
            {
                int OutParam = 0;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string OrderType = "1002";
                string Date = DateTime.Now.ToString();
                if (CreatedDate == "") CreatedDate = Date;
                else { CreatedDate = DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = "";
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE009 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString(); int r = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1604");
                    TransNo = taxlst.TransactionNoGenerate(Convert.ToString(Session["FormName"]), 1604, OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Customer);
                    Cmd.Parameters.AddWithValue("@COLUMN19", OrderType);
                    Cmd.Parameters.AddWithValue("@COLUMN08", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN10", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN14", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN23", "1000");
                    Cmd.Parameters.AddWithValue("@COLUMN24", TaxAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN22", SubTotal);
                    Cmd.Parameters.AddWithValue("@COLUMN20", TotAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN25", Discount);
                    Cmd.Parameters.AddWithValue("@COLUMN38", TDSamt);
                    Cmd.Parameters.AddWithValue("@COLUMN45", Cessamt);
                    Cmd.Parameters.AddWithValue("@COLUMN48", cesstax1);
                    Cmd.Parameters.AddWithValue("@COLUMN46", Amtin);
                    Cmd.Parameters.AddWithValue("@COLUMN68", roundoffadjamt);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Attendedby);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cnc.Open(); Cmd.CommandTimeout = 0;
                    r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                }
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1604).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                if (r > 0)
                {
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                }
                int l = 0;
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE010 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string Item = itemdata.Tables[0].Rows[i]["ItemID"].ToString();
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["Qty"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                    string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["Amount"].ToString();
                    string lDiscount = itemdata.Tables[0].Rows[i]["Discount"].ToString();
                    string TaxType = itemdata.Tables[0].Rows[i]["TaxType"].ToString();
                    string lTaxAmt = itemdata.Tables[0].Rows[i]["TaxAmt"].ToString();
                    if (lTaxAmt == "" || lTaxAmt == null) lTaxAmt = "0";
                    if (Units == "0" || Units == "" || Units == null) Units = "10000";
                    if (Rate == "0" || Rate == "" || Rate == null) Rate = Price;
                    SqlCommand cmddi = new SqlCommand("select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=" + Item + " and COLUMN19=" + Units + " and COLUMN13 in(" + OpUnit + ") and COLUMNA03=" + AOwner + " ", cn);
                    SqlDataAdapter daai = new SqlDataAdapter(cmddi);
                    DataTable dtti = new DataTable();
                    daai.Fill(dtti);
                    string Avlqty = dtti.Rows[0][0].ToString();
                    if (Avlqty == "" || Avlqty == null) Avlqty = "0";
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN15", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Item);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", ItemName);
                        Cmdl.Parameters.AddWithValue("@COLUMN22", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", lQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN21", TaxType);
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN35", Rate);
                        Cmdl.Parameters.AddWithValue("@COLUMN14", Amount);
                        Cmdl.Parameters.AddWithValue("@COLUMN23", lTaxAmt);
                        Cmdl.Parameters.AddWithValue("@COLUMN19", lDiscount);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", Avlqty);
                        Cmdl.Parameters.AddWithValue("@COLUMN30", "22557");
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE010");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open(); Cmdl.CommandTimeout = 0;
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                int lP = 0;
                SqlCommand cmddc = new SqlCommand("select  COLUMN02 from SATABLE009 where column02=" + HCol2 + " ", cn);
                SqlDataAdapter daac = new SqlDataAdapter(cmddc);
                DataTable dttc = new DataTable();
                daac.Fill(dttc);
                //if (PayAcc != "0" && PayAmt != "0" && PayAcc != "" && PayAmt != "")
                //{
                if (dttc.Rows.Count > 0)
                {
                    //Payment
                    SqlCommand cmdp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE011 ", cn);
                    SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                    DataTable dtp = new DataTable();
                    dap.Fill(dtp);
                    string HColp = dtp.Rows[0][0].ToString();
                    HColp = (Convert.ToInt32(HColp) + 1).ToString();
                    int pm = 0;
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand CmdP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                        CmdP.CommandType = CommandType.StoredProcedure;
                        CmdP.Parameters.AddWithValue("@COLUMN02", HColp);
                        CmdP.Parameters.AddWithValue("@COLUMN03", "1278");
                        TransNo = taxlst.TransactionNoGenerate(Convert.ToString(Session["FormName"]), 1278, OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMN04", TransNo);
                        CmdP.Parameters.AddWithValue("@COLUMN05", Customer);
                        CmdP.Parameters.AddWithValue("@COLUMN18", OrderType);
                        CmdP.Parameters.AddWithValue("@COLUMN19", CreatedDate);
                        CmdP.Parameters.AddWithValue("@COLUMN07", HCol2);
                        CmdP.Parameters.AddWithValue("@COLUMN08", PayAcc);
                        CmdP.Parameters.AddWithValue("@COLUMN14", OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMN13", PayAmt);
                        CmdP.Parameters.AddWithValue("@COLUMN10", Type);
                        CmdP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        CmdP.Parameters.AddWithValue("@COLUMNA06", Date);
                        CmdP.Parameters.AddWithValue("@COLUMNA07", Date);
                        CmdP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        CmdP.Parameters.AddWithValue("@COLUMNA12", 1);
                        CmdP.Parameters.AddWithValue("@COLUMNA13", 0);
                        CmdP.Parameters.AddWithValue("@Direction", insert);
                        CmdP.Parameters.AddWithValue("@TabelName", "SATABLE011");
                        CmdP.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cnc.Open();
                        CmdP.CommandTimeout = 0;
                        pm = CmdP.ExecuteNonQuery();
                        OutParam = Convert.ToInt32(CmdP.Parameters["@ReturnValue"].Value);
                    }
                    if (pm > 0)
                    {
                        custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1278).FirstOrDefault().COLUMN04.ToString();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                            if (Oper == null)
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                            if (listTM == null)
                            {
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                            }
                        }
                        if (listTM != null)
                        {
                            MYTABLE002 product = listTM;
                            if (listTM.COLUMN09 == "")
                                product.COLUMN09 = (1).ToString();
                            else
                                product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                            act1.SaveChanges();
                        }
                    }
                    //Line                
                    SqlCommand cmddlp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE012 ", cn);
                    SqlDataAdapter daalp = new SqlDataAdapter(cmddlp);
                    DataTable dttlp = new DataTable();
                    daalp.Fill(dttlp);
                    string LColp = dttlp.Rows[0][0].ToString();
                    LColp = (Convert.ToInt32(LColp) + 1).ToString();
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand CmdlP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                        CmdlP.CommandType = CommandType.StoredProcedure;
                        CmdlP.Parameters.AddWithValue("@COLUMN08", OutParam);
                        CmdlP.Parameters.AddWithValue("@COLUMN02", LColp);
                        CmdlP.Parameters.AddWithValue("@COLUMN03", HCol2);
                        CmdlP.Parameters.AddWithValue("@COLUMN04", TotAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN05", TotAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN06", PayAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN14", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMN13", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMN09", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        CmdlP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        CmdlP.Parameters.AddWithValue("@COLUMNA06", Date);
                        CmdlP.Parameters.AddWithValue("@COLUMNA07", Date);
                        CmdlP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        CmdlP.Parameters.AddWithValue("@COLUMNA12", 1);
                        CmdlP.Parameters.AddWithValue("@COLUMNA13", 0);
                        CmdlP.Parameters.AddWithValue("@Direction", insert);
                        CmdlP.Parameters.AddWithValue("@TabelName", "SATABLE012");
                        CmdlP.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        CmdlP.CommandTimeout = 0;
                        lP = CmdlP.ExecuteNonQuery();
                    }
                }
                //}
                //cn.Close();
                //return Json(new { Data = lP, InvoiceNo = HCol2 }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("PointOfSalesInfo", "PointOfSales");
                //return View("PointOfSalesInfo");
                Session["POSGridData"] = null;
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                if (r > 0)
                {
                    var msg = string.Empty;
                    msg = "Service Receipt Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                idi = "3138";
                return Json(new { Data = l, InvoiceNo = HCol2, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }



        [HttpPost]
        public ActionResult EditSaveform(string Qty, string SubTotal, string Type, string Discount, string TaxAmt, string TotAmt, string PayAmt, string PayAcc, string Customer, string TDSamt, string Cessamt, string cesstax1, string ide, string CreatedDate, string roundoffadjamt, string Amtin, string Attendedby)
        {
            try
            {
                int OutParam = 0;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string OrderType = "1002";
                string Date = DateTime.Now.ToString();
                if (CreatedDate == "") CreatedDate = Date;
                else { CreatedDate = DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null) OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update", TransNo = "", IHeaderIID = "", PHeaderIID = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  COLUMN01,COLUMN04,COLUMN08 from SATABLE009 where column02=" + ide + " ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                IHeaderIID = dtt.Rows[0][0].ToString();
                TransNo = dtt.Rows[0]["COLUMN04"].ToString();
                string TDate = Convert.ToDateTime(dtt.Rows[0]["COLUMN08"].ToString()).ToShortDateString();
                string HCol2 = ide; int r = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1604");
                    Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Customer);
                    Cmd.Parameters.AddWithValue("@COLUMN19", OrderType);
                    Cmd.Parameters.AddWithValue("@COLUMN08", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN10", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN14", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN23", "1000");
                    Cmd.Parameters.AddWithValue("@COLUMN24", TaxAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN22", SubTotal);
                    Cmd.Parameters.AddWithValue("@COLUMN20", TotAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN25", Discount);
                    Cmd.Parameters.AddWithValue("@COLUMN38", TDSamt);
                    Cmd.Parameters.AddWithValue("@COLUMN45", Cessamt);
                    Cmd.Parameters.AddWithValue("@COLUMN48", cesstax1);
                    Cmd.Parameters.AddWithValue("@COLUMN68", roundoffadjamt);
                    Cmd.Parameters.AddWithValue("@COLUMN46", Amtin);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Attendedby);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cnc.Open();
                    Cmd.CommandTimeout = 0;
                    r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                }
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1604).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();

                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    //SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE010 ", cn);
                    //SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    //DataTable dttl = new DataTable();
                    //daal.Fill(dttl);
                    //string LCol2 = dttl.Rows[0][0].ToString();
                    //LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string LCol2 = itemdata.Tables[0].Rows[i]["rowid"].ToString();
                    string Item = itemdata.Tables[0].Rows[i]["ItemID"].ToString();
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["Qty"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                    string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["Amount"].ToString();
                    string lDiscount = itemdata.Tables[0].Rows[i]["Discount"].ToString();
                    string TaxType = itemdata.Tables[0].Rows[i]["TaxType"].ToString();
                    string lTaxAmt = itemdata.Tables[0].Rows[i]["TaxAmt"].ToString();
                    if (lTaxAmt == "" || lTaxAmt == null) lTaxAmt = "0";
                    if (Units == "0" || Units == "" || Units == null) Units = "10000";
                    if (Rate == "0" || Rate == "" || Rate == null) Rate = Price;
                    SqlCommand cmddi = new SqlCommand("select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=" + Item + " and COLUMN19=" + Units + " and COLUMN13 in(" + OpUnit + ") and COLUMNA03=" + AOwner + " ", cn);
                    SqlDataAdapter daai = new SqlDataAdapter(cmddi);
                    DataTable dtti = new DataTable();
                    daai.Fill(dtti);
                    string Avlqty = dtti.Rows[0][0].ToString();
                    if (Avlqty == "" || Avlqty == null) Avlqty = "0";
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN15", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Item);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", ItemName);
                        Cmdl.Parameters.AddWithValue("@COLUMN22", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", lQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN21", TaxType);
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN35", Rate);
                        Cmdl.Parameters.AddWithValue("@COLUMN14", Amount);
                        Cmdl.Parameters.AddWithValue("@COLUMN23", lTaxAmt);
                        Cmdl.Parameters.AddWithValue("@COLUMN19", lDiscount);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", Avlqty);
                        Cmdl.Parameters.AddWithValue("@COLUMN30", "22557");
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE010");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        Cmdl.CommandTimeout = 0;
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                int lP = 0;
                SqlCommand cmddc = new SqlCommand("select  l.COLUMN02 LID,l.COLUMN08,h.COLUMN04,h.COLUMN02 HID from SATABLE012 l inner join SATABLE011 h on h.COLUMN01=l.COLUMN08 and h.COLUMNA02=l.COLUMNA02 and h.COLUMNA03=l.COLUMNA03 and isnull(h.COLUMNA13,0)=0 where l.column03=" + ide + "  and l.COLUMNA03=" + AOwner + " and isnull(l.COLUMNA13,0)=0", cn);
                SqlDataAdapter daac = new SqlDataAdapter(cmddc);
                DataTable dttc = new DataTable();
                daac.Fill(dttc);
                if (dttc.Rows.Count > 0)
                {
                    OutParam = Convert.ToInt32(dttc.Rows[0]["COLUMN08"].ToString());
                    string HColp = dttc.Rows[0]["HID"].ToString();
                    TransNo = dttc.Rows[0]["COLUMN04"].ToString();
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand CmdP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                        CmdP.CommandType = CommandType.StoredProcedure;
                        CmdP.Parameters.AddWithValue("@COLUMN02", HColp);
                        CmdP.Parameters.AddWithValue("@COLUMN03", "1278");
                        CmdP.Parameters.AddWithValue("@COLUMN04", TransNo);
                        CmdP.Parameters.AddWithValue("@COLUMN05", Customer);
                        CmdP.Parameters.AddWithValue("@COLUMN18", OrderType);
                        CmdP.Parameters.AddWithValue("@COLUMN19", CreatedDate);
                        CmdP.Parameters.AddWithValue("@COLUMN07", HCol2);
                        CmdP.Parameters.AddWithValue("@COLUMN08", PayAcc);
                        CmdP.Parameters.AddWithValue("@COLUMN14", OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMN13", PayAmt);
                        CmdP.Parameters.AddWithValue("@COLUMN10", Type);
                        CmdP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        CmdP.Parameters.AddWithValue("@COLUMNA06", Date);
                        CmdP.Parameters.AddWithValue("@COLUMNA07", Date);
                        CmdP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        CmdP.Parameters.AddWithValue("@COLUMNA12", 1);
                        CmdP.Parameters.AddWithValue("@COLUMNA13", 0);
                        CmdP.Parameters.AddWithValue("@Direction", insert);
                        CmdP.Parameters.AddWithValue("@TabelName", "SATABLE011");
                        cnc.Open();
                        CmdP.CommandTimeout = 0;
                        int pm = CmdP.ExecuteNonQuery();
                    }
                    string LColp = dttc.Rows[0]["LID"].ToString();
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand CmdlP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                        CmdlP.CommandType = CommandType.StoredProcedure;
                        CmdlP.Parameters.AddWithValue("@COLUMN08", OutParam);
                        CmdlP.Parameters.AddWithValue("@COLUMN02", LColp);
                        CmdlP.Parameters.AddWithValue("@COLUMN03", HCol2);
                        CmdlP.Parameters.AddWithValue("@COLUMN04", TotAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN05", 0);
                        CmdlP.Parameters.AddWithValue("@COLUMN06", PayAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN14", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMN13", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMN09", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        CmdlP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        CmdlP.Parameters.AddWithValue("@COLUMNA06", Date);
                        CmdlP.Parameters.AddWithValue("@COLUMNA07", Date);
                        CmdlP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        CmdlP.Parameters.AddWithValue("@COLUMNA12", 1);
                        CmdlP.Parameters.AddWithValue("@COLUMNA13", 0);
                        CmdlP.Parameters.AddWithValue("@Direction", insert);
                        CmdlP.Parameters.AddWithValue("@TabelName", "SATABLE012");
                        CmdlP.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        CmdlP.CommandTimeout = 0;
                        lP = CmdlP.ExecuteNonQuery();
                    }
                }
                Session["POSGridData"] = null;
                //return Json(new { Data = lP, InvoiceNo = HCol2 }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("PointOfSalesInfo", "PointOfSales");
                //return View("PointOfSalesInfo");
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                if (r > 0)
                {
                    var msg = string.Empty;
                    msg = "Service Receipt Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                idi = "3138";
                return Json(new { Data = l, InvoiceNo = HCol2, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        
        public ActionResult ServiceReceiptInfo()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var frmid = 1604;
                Session["id"] = frmid;
                var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmid).ToList().FirstOrDefault().COLUMN04;
                Session["FormName"] = FormName;
                Session["idi"] = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName && a.COLUMNA03 == acID).ToList().FirstOrDefault().COLUMN02;
                object[] sp = new object[4];
                sp[0] = (frmid);
                sp[1] = (Session["OPUnit"]);
                sp[2] = (Session["AcOwner"]);
                sp[3] = ("Info");
                var GData = dbs.Query("Exec USP_PROC_FormBuildInfo @FormId=@0,@OPUnit=@1,@AcOwner=@2,@Type=@3", sp);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                ViewBag.Columns = GData.FirstOrDefault().Columns;
                ViewBag.Grid = GData;
                DateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public void DateFormat()
        {
            string str = "select isnull(COLUMN04,'dd/MM/yyyy')COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
            SqlDataAdapter daf = new SqlDataAdapter(str, cn);
            DataTable dtf = new DataTable();
            daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
            if (dtf.Rows.Count > 0)
            {
                DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                Session["DateFormat"] = DateFormat;
                Session["FormatDate"] = DateFormat;
                ViewBag.DateFormat = DateFormat;
            }
            if (JQDateFormat != "")
            {
                Session["DateFormat"] = DateFormat;
                Session["FormatDate"] = DateFormat;
                Session["ReportDate"] = JQDateFormat;
            }
            else
            {
                Session["DateFormat"] = "dd/MM/yyyy";
                Session["FormatDate"] = "dd/MM/yyyy";
                Session["ReportDate"] = "dd/mm/yy";
                ViewBag.DateFormat = "dd/MM/yyyy";
            }
        }

        public ActionResult ServiceReceiptEdit()
        {
            Session["FormName"] = "Service Receipt";
            Session["ide"] = Request["ide"];
            Session["POSGridData"] = null;
            try
            {
                SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(l.COLUMN02) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN10) [Qty], h.COLUMN22 [SubTotal], h.COLUMN25 Discount, h.COLUMN24 Tax, h.COLUMN38 Tds,  h.COLUMN45 Cess,(isnull(h.COLUMN20,0)) Total, (isnull(p.COLUMN06,0)) Payment, (isnull(h.COLUMN20,0)- (isnull(p.COLUMN06,0))) Dueamt,ph.COLUMN08 Account,ph.COLUMN09 cardNo ,ph.COLUMN10 PayType,h.COLUMN08 CreatedDate,isnull(h.COLUMN68,0)roundoffadjamt,h.COLUMN46 Amtin,h.COLUMNA08 Attendedby FROM SATABLE009 h inner join SATABLE010 l on l.COLUMN15=h.COLUMN01   " +
                " left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 left JOIN SATABLE011 ph  ON ph.COLUMN01 =p.COLUMN08 where h.column02=" + @Request["ide"] + " group by h.COLUMN05,h.COLUMN22,h.COLUMN25,h.COLUMN24,h.COLUMN20,h.COLUMN38,h.COLUMN45,c.COLUMN05,p.COLUMN06,ph.COLUMN08 ,ph.COLUMN09,ph.COLUMN10,h.COLUMN08,h.COLUMN68,h.COLUMN46,h.COLUMNA08 ", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                System.Collections.IEnumerable rows = dtv.Rows;
                ViewBag.pitemsdata = rows;

                SqlDataAdapter cmdCnt = new SqlDataAdapter("select count(*) [No of Items] from SATABLE011 where column07= " + @Request["ide"] + " ", cn);
                DataTable dtCnt = new DataTable();
                cmdCnt.Fill(dtCnt);
                int i = Convert.ToInt32(dtCnt.Rows[0]["No of Items"].ToString());

                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  and  COLUMN05='ITTY009'  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");

                List<SelectListItem> UOM = new List<SelectListItem>();
                SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdataU = new DataTable();
                cmddlU.Fill(dtdataU);
                for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                {
                    UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["UOM"] = new SelectList(UOM, "Value", "Text");


                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    rNoofitems = "",
                    rTotalItems = "",
                    rCustomerno = "",
                    rCustomer = "",
                    rSubTotal = "",
                    rDiscount = "",
                    rTax = "",
                    rTotal = "",
                    rPayment = "",
                    rDueamt = "",
                    rAccount = "",
                    TDSAmount = "",
                    rPamt = "",
                    CESSAmount = "",
                    Date = ""
                });
                if (dtv.Rows.Count > 0)
                {
                    all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                    all1[0].rTotalItems = dtv.Rows[0]["Qty"].ToString();
                    all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                    all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                    all1[0].rSubTotal = dtv.Rows[0]["SubTotal"].ToString();
                    all1[0].rDiscount = dtv.Rows[0]["Discount"].ToString();
                    all1[0].rTax = dtv.Rows[0]["Tax"].ToString();
                    all1[0].TDSAmount = dtv.Rows[0]["Tds"].ToString();
                    all1[0].CESSAmount = dtv.Rows[0]["Cess"].ToString();
                    all1[0].rTotal = dtv.Rows[0]["Total"].ToString();
                    all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                    string payamt = dtv.Rows[0]["Payment"].ToString();
                    if (payamt == "" || payamt == "null") payamt = "0";
                    string dueamt = dtv.Rows[0]["Dueamt"].ToString();
                    if (dueamt == "" || dueamt == "null") dueamt = "0";
                    all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                    all1[0].rPayment = (Convert.ToDecimal(payamt) + Convert.ToDecimal(dueamt)).ToString();
                    all1[0].rAccount = dtv.Rows[0]["Account"].ToString();
                    all1[0].Date = dtv.Rows[0]["CreatedDate"].ToString();
                    all1[0].amttype = dtv.Rows[0]["Amtin"].ToString();
                }
                List<SelectListItem> Amtin = new List<SelectListItem>();
                SqlDataAdapter cmdai = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtai = new DataTable();
                cmdai.Fill(dtai);
                for (int dd = 0; dd < dtai.Rows.Count; dd++)
                {
                    Amtin.Add(new SelectListItem { Value = dtai.Rows[dd]["COLUMN02"].ToString(), Text = dtai.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: all1[0].amttype);

                if (i >= 1)
                {
                    SqlDataAdapter cmdc = new SqlDataAdapter(" select isnull(max(column04),0), isnull(sum(column06),0) SumPaid,( isnull(max(column04),0) - isnull(sum(column06),0) ) Dueamt from SATABLE012 where column03= " + @Request["ide"] + "  ", cn);
                    DataTable dtc = new DataTable();
                    cmdc.Fill(dtc);
                    all1[0].rPamt = dtc.Rows[0]["SumPaid"].ToString();
                    all1[0].rDueamt = dtc.Rows[0]["Dueamt"].ToString();
                }
                //DateFormat();Session["DateFormat"] = DateFormat;
                ViewBag.DateFormat = Session["FormatDate"];
                List<SelectListItem> Attendedby = new List<SelectListItem>();
                SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                DataTable dtat = new DataTable();
                cmdat.Fill(dtat);
                for (int dd = 0; dd < dtat.Rows.Count; dd++)
                {
                    Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: dtv.Rows[0]["Attendedby"].ToString());
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult ServiceReceiptView()
        {
            Session["FormName"] = "Service Receipt";
            Session["ide"] = Request["ide"];
            try
            {
                SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(DISTINCT l.COLUMN02) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN10) [Qty], h.COLUMN22 [SubTotal], h.COLUMN25 Discount, h.COLUMN24 Tax, h.COLUMN38 Tds,  h.COLUMN45 Cess,(isnull(h.COLUMN20,0)) Total, (isnull(p.COLUMN06,0)) Payment, (isnull(h.COLUMN20,0)- (isnull(p.COLUMN06,0))) Dueamt,ph.COLUMN08 Account,ph.COLUMN09 cardNo ,ph.COLUMN10 PayType,h.COLUMN08 CreatedDate,isnull(h.COLUMN68,0)roundoffadjamt,h.COLUMN46 Amtin,h.COLUMNA08 Attendedby FROM SATABLE009 h inner join SATABLE010 l on l.COLUMN15=h.COLUMN01   " +
                " left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 left JOIN SATABLE011 ph  ON ph.COLUMN01 =p.COLUMN08 where h.column02=" + @Request["ide"] + " group by h.COLUMN05,h.COLUMN22,h.COLUMN25,h.COLUMN24,h.COLUMN20,h.COLUMN38,h.COLUMN45,c.COLUMN05,p.COLUMN06,ph.COLUMN08 ,ph.COLUMN09,ph.COLUMN10,h.COLUMN08,h.COLUMN68,h.COLUMN46,h.COLUMNA08 ", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                System.Collections.IEnumerable rows = dtv.Rows;
                ViewBag.pitemsdata = rows;

                SqlDataAdapter cmdCnt = new SqlDataAdapter("select count(*) [No of Items] from SATABLE011 where column07= " + @Request["ide"] + " ", cn);
                DataTable dtCnt = new DataTable();
                cmdCnt.Fill(dtCnt);
                int i = Convert.ToInt32(dtCnt.Rows[0]["No of Items"].ToString());

                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    rNoofitems = "",
                    rTotalItems = "",
                    rCustomerno = "",
                    rCustomer = "",
                    rSubTotal = "",
                    rDiscount = "",
                    rTax = "",
                    rTotal = "",
                    rPayment = "",
                    rDueamt = "",
                    rAccount = "",
                    TDSAmount = "",
                    CESSAmount = "",
                    Date = ""
                });
                if (dtv.Rows.Count > 0)
                {
                    all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                    all1[0].rTotalItems = dtv.Rows[0]["Qty"].ToString();
                    all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                    all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                    all1[0].rSubTotal = dtv.Rows[0]["SubTotal"].ToString();
                    all1[0].rDiscount = dtv.Rows[0]["Discount"].ToString();
                    all1[0].rTax = dtv.Rows[0]["Tax"].ToString();
                    all1[0].TDSAmount = dtv.Rows[0]["Tds"].ToString();
                    all1[0].CESSAmount = dtv.Rows[0]["Cess"].ToString();
                    all1[0].rTotal = dtv.Rows[0]["Total"].ToString();
                    string payamt = dtv.Rows[0]["Payment"].ToString();
                    if (payamt == "" || payamt == "null") payamt = "0";
                    string dueamt = dtv.Rows[0]["Dueamt"].ToString();
                    if (dueamt == "" || dueamt == "null") dueamt = "0";
                    all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                    all1[0].rPayment = (Convert.ToDecimal(payamt) + Convert.ToDecimal(dueamt)).ToString();
                    all1[0].rAccount = dtv.Rows[0]["Account"].ToString();
                    all1[0].Date = dtv.Rows[0]["CreatedDate"].ToString();
                    all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                    all1[0].amttype = dtv.Rows[0]["Amtin"].ToString();
                }
                List<SelectListItem> Amtin = new List<SelectListItem>();
                SqlDataAdapter cmdai = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtai = new DataTable();
                cmdai.Fill(dtai);
                for (int dd = 0; dd < dtai.Rows.Count; dd++)
                {
                    Amtin.Add(new SelectListItem { Value = dtai.Rows[dd]["COLUMN02"].ToString(), Text = dtai.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: all1[0].amttype);
                if (i > 1)
                {
                    SqlDataAdapter cmdc = new SqlDataAdapter(" select isnull(max(column04),0), isnull(sum(column06),0) SumPaid,( isnull(max(column04),0) - isnull(sum(column06),0) ) Dueamt from SATABLE012 where column03= " + @Request["ide"] + "  ", cn);
                    DataTable dtc = new DataTable();
                    cmdc.Fill(dtc);

                    all1[0].rDueamt = dtc.Rows[0]["Dueamt"].ToString();
                }
                //DateFormat();
                ViewBag.DateFormat = Session["FormatDate"];
                List<SelectListItem> Attendedby = new List<SelectListItem>();
                SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                DataTable dtat = new DataTable();
                cmdat.Fill(dtat);
                for (int dd = 0; dd < dtat.Rows.Count; dd++)
                {
                    Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: dtv.Rows[0]["Attendedby"].ToString());
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult ServiceReceiptDelete()
        {
            try
            {
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from SATABLE009 where COLUMN02=" + HCol2 + "", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                string Date = DateTime.Now.ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OPunit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02 from SATABLE011 where COLUMN07=" + HCol2 + "", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                int c = 0;
                for (int p = 0; p < dt1.Rows.Count; p++)
                {
                    int HColc = Convert.ToInt32(dt1.Rows[p][0].ToString());
                    if (HColc > 0)
                    {
                        SqlCommand Cmdc = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
                        Cmdc.CommandType = CommandType.StoredProcedure;
                        Cmdc.Parameters.AddWithValue("@COLUMN02", HColc);
                        Cmdc.Parameters.AddWithValue("@COLUMNA02", OPunit);
                        Cmdc.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdc.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdc.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdc.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdc.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdc.Parameters.AddWithValue("@COLUMNA13", 1);
                        Cmdc.Parameters.AddWithValue("@Direction", insert);
                        Cmdc.Parameters.AddWithValue("@TabelName", "SATABLE011");
                        Cmdc.Parameters.AddWithValue("@ReturnValue", "");
                        cn.Open();
                        c = Cmdc.ExecuteNonQuery();
                        cn.Close();
                    }
                }
                if (c > 0)
                {
                    var msg = string.Empty;
                    msg = "Service Receipt Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return RedirectToAction("ServiceReceiptInfo", "ServiceReceipt", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult PaymentInformationEdit(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {
                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationEdit");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult PaymentInformationView(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {
                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationEdit");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult EditSave(string totamt, string payamt, string totamta, string payacc, string Qty, string SubTotal, string Discount, string TaxAmt, string Customer, string Type, string CardNo)
        {
            try
            {
                int OutParam = 0;
                int id = Convert.ToInt32(Session["IDE"]);

                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null) OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = "";
                cn.Open();
                //Payment
                SqlCommand cmdp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE011 ", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                string HColp = dtp.Rows[0][0].ToString();
                HColp = (Convert.ToInt32(HColp) + 1).ToString();

                SqlCommand CmdP = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
                CmdP.CommandType = CommandType.StoredProcedure;
                CmdP.Parameters.AddWithValue("@COLUMN02", HColp);
                CmdP.Parameters.AddWithValue("@COLUMN03", "1278");
                TransNo = taxlst.TransactionNoGenerate(Convert.ToString(Session["FormName"]), 1278, OpUnit);
                CmdP.Parameters.AddWithValue("@COLUMN04", TransNo);
                CmdP.Parameters.AddWithValue("@COLUMN05", Customer);
                //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                CmdP.Parameters.AddWithValue("@COLUMN06", "");
                CmdP.Parameters.AddWithValue("@COLUMN18", 1002);
                CmdP.Parameters.AddWithValue("@COLUMN19", Date);
                CmdP.Parameters.AddWithValue("@COLUMN07", id);
                CmdP.Parameters.AddWithValue("@COLUMN08", payacc);
                CmdP.Parameters.AddWithValue("@COLUMN14", OpUnit);
                CmdP.Parameters.AddWithValue("@COLUMN13", payamt);
                CmdP.Parameters.AddWithValue("@COLUMN10", Type);
                CmdP.Parameters.AddWithValue("@COLUMN09", CardNo);
                CmdP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                CmdP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                CmdP.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdP.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdP.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdP.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdP.Parameters.AddWithValue("@Direction", insert);
                CmdP.Parameters.AddWithValue("@TabelName", "SATABLE011");
                CmdP.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int pm = CmdP.ExecuteNonQuery();
                OutParam = Convert.ToInt32(CmdP.Parameters["@ReturnValue"].Value);

                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1278).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();


                if (pm > 0)
                {
                    custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1278).FirstOrDefault().COLUMN04.ToString();
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                }
                int lP = 0;
                SqlCommand cmddlp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE012 ", cn);
                SqlDataAdapter daalp = new SqlDataAdapter(cmddlp);
                DataTable dttlp = new DataTable();
                daalp.Fill(dttlp);
                string LColp = dttlp.Rows[0][0].ToString();
                LColp = (Convert.ToInt32(LColp) + 1).ToString();
                SqlCommand CmdlP = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
                CmdlP.CommandType = CommandType.StoredProcedure;
                CmdlP.Parameters.AddWithValue("@COLUMN08", OutParam);
                CmdlP.Parameters.AddWithValue("@COLUMN02", LColp);
                CmdlP.Parameters.AddWithValue("@COLUMN03", id);
                CmdlP.Parameters.AddWithValue("@COLUMN04", totamt);
                CmdlP.Parameters.AddWithValue("@COLUMN05", totamta);
                CmdlP.Parameters.AddWithValue("@COLUMN06", payamt);
                CmdlP.Parameters.AddWithValue("@COLUMN14", "0");
                CmdlP.Parameters.AddWithValue("@COLUMN13", "0");
                CmdlP.Parameters.AddWithValue("@COLUMN09", "0");
                CmdlP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                CmdlP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                CmdlP.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdlP.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdlP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdlP.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdlP.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdlP.Parameters.AddWithValue("@Direction", insert);
                CmdlP.Parameters.AddWithValue("@TabelName", "SATABLE012");
                CmdlP.Parameters.AddWithValue("@ReturnValue", "");
                lP = CmdlP.ExecuteNonQuery();

                cn.Close();
                //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                if (pm > 0)
                {
                    //    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    //    if (msgMaster != null)
                    //    {
                    //        msg = msgMaster.COLUMN03;
                    //    }
                    //    else
                    msg = "Sales Receipt Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //SqlCommand cmd = new SqlCommand("update SATABLE012  set column05 =" + totamt + " ,COLUMN06= " + totamta + " FROM SATABLE009 h inner join SATABLE010 l  on l.COLUMN15=h.COLUMN01    left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 where  h.columnA03 = "+Convert.ToInt32(Session["AcOwner"])+" and  h.column02=" + id + "", cn);
                //cn.Open();
                //cmd.ExecuteNonQuery();
                //cn.Close();

                return Json(new { Data = 1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult getBalanceAmount(string Account)
        {
            try
            {
                string bal = "0";
                SqlCommand cmd = new SqlCommand("Select isnull(column10,0) from fitable001 where column02=" + Account + " AND    (" + Session["OPUnitWithNull"] + " or columna02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    bal = dt1.Rows[0][0].ToString();
                }
                return Json(new { Balance = bal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult GetTaxAmount(string TaxType, string TaxCal, string FormName, int grp = -1)
        {
            try
            {
                if (Convert.ToInt32(TaxType) < 0) { grp = 0; TaxType = TaxType.Replace("-", ""); }
                string fid = "1275";
                SqlCommand cmd = new SqlCommand("GetTaxAmount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TaxType", TaxType));
                cmd.Parameters.Add(new SqlParameter("@FormID", fid));
                cmd.Parameters.Add(new SqlParameter("@Group", grp));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1); var Tax = "";var Agency = "";
                if (dt1.Rows.Count > 0)
                {
                    Tax = dt1.Rows[0]["COLUMN07"].ToString();
                    Agency = dt1.Rows[0]["COLUMN16"].ToString();
                }
                return Json(new { Tax = Tax, Agency = Agency }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        [HttpPost]
        public ActionResult POSSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["POSGridData"] = ds;
                //var saveform = Request.QueryString["FormName"];
                //var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == saveform).ToList();
                //var fname = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var frmID = fnamedata.Select(q => q.COLUMN02).FirstOrDefault();
                //var customfrmID = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var frmType = fnamedata.Select(q => q.COLUMN05).FirstOrDefault().ToString();
                //var MasterForm = fnamedata.Select(q => q.COLUMN06).FirstOrDefault();
                //if (frmType == "Custom")
                //{
                //    frmID = Convert.ToInt32(MasterForm);
                //    saveform = dc.CONTABLE0010.Where(q => q.COLUMN02 == MasterForm).FirstOrDefault().COLUMN04;
                //}
                //Session["FormName"] = saveform;
                //Session["id"] = fname;
                //eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                //lg.CreateFile(Server.MapPath("~/"), saveform + "_" + Session["UserName"].ToString() + "", " \r\n" + "***********************TARGET1***********************" + " \r\n" + " \r\n" + " Line Data Received .");

                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult ItemFullGridFill(string Query, string FormName, int RowNo, string Type)
        {
            try
            {
                Session["NoGridRows"] = "";
                string type = "Insert", formname = "";
                var acID = (int?)Session["AcOwner"];
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                SqlCommand cmd = new SqlCommand(Query, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int endrow = RowNo + 99;
                if (Type == "Next") endrow = RowNo + 99;
                else if (Type == "Prev") endrow = RowNo - 99;
                string ORDER = " ORDER BY p.COLUMNA06 desc ";
                var last = Query.Trim().Length;
                int m = Query.IndexOf("ORDER BY"); if (m > 0) { ORDER = Query.Substring(m).ToString(); Query = Query.Replace(ORDER, "").ToString(); }
                Query = Query.Replace("SELECT", "SELECT ROW_NUMBER() OVER (" + ORDER + ") AS 'RowNumber',");
                if (Type == "Next") Query = "select * from ( " + Query + " ) q where q.RowNumber BETWEEN " + RowNo + " and " + endrow + "";
                else if (Type == "Prev") Query = "select * from ( " + Query + " ) q where q.RowNumber BETWEEN " + endrow + " and " + RowNo + "";
                var GData = dbs.Query(Query);
                List<WebGridColumn> gcol = new List<WebGridColumn>();
                List<string> colNM = new List<string>();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    colNM.Add(dt.Columns[i].ColumnName);
                }
                gcol.Add(new WebGridColumn()
                {
                    Header = "",
                    Format = (item) => new HtmlString("<div class='ch'><a href='/ServiceReceipt/ServiceReceiptEdit?ide=" + item.ID + "&FormName=" + FormName + " &idi=" + 3138 + " ' style=''>Edit</a> | <a href='/ServiceReceipt/ServiceReceiptView?ide=" + item.ID + "&FormName=" + FormName + " &idi=" + 3138 + " '' style=''>View</a> </div>")
                });
                foreach (string dtr in colNM)
                {
                    if (dtr != "ID")
                    {
                        gcol.Add(new WebGridColumn()
                        {
                            ColumnName = dtr,
                            Header = dtr
                        });
                    }
                }
                var grid1 = new WebGrid(null, canPage: false, canSort: false);
                grid1 = new WebGrid(GData, canPage: false, canSort: false);
                var htmlstring1 = grid1.GetHtml(
                htmlAttributes: new { id = "grdfData", style = "500px" },
                columns: gcol);
                return Json(new { grid = htmlstring1.ToHtmlString(), rowscnt = dt.Rows.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

    }
}
