﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI
{
    public class QuickSearchController : Controller
    {
        //
        // GET: /Default1/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Create()
        {
            try
            {
                List<SelectListItem> UPC = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN06,COLUMN06  from MATABLE007 where COLUMN06 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null )  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    UPC.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN06"].ToString(), Text = dtdata.Rows[dd]["COLUMN06"].ToString() });
                }
                ViewData["UPC"] = new SelectList(UPC, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Image = new List<SelectListItem>();
                SqlDataAdapter cmdi1 = new SqlDataAdapter("select COLUMN02,COLUMN46 from MATABLE007  Where COLUMN46 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti1 = new DataTable();
                cmdi1.Fill(dti1);
                for (int dd = 0; dd < dti1.Rows.Count; dd++)
                {
                    Image.Add(new SelectListItem { Value = dti1.Rows[dd]["COLUMN02"].ToString(), Text = dti1.Rows[dd]["COLUMN46"].ToString() });
                }
                ViewBag.Search = "";
                //ViewData["Image"] = new SelectList(Image, "Value", "Text");
                List<SelectListItem> VendorPart = new List<SelectListItem>();
                SqlDataAdapter cmddl1 = new SqlDataAdapter("select COLUMN09  from MATABLE007 where COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null )  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata1 = new DataTable();
                cmddl1.Fill(dtdata1);
                for (int dd = 0; dd < dtdata1.Rows.Count; dd++)
                {
                    VendorPart.Add(new SelectListItem { Value = dtdata1.Rows[dd]["COLUMN09"].ToString(), Text = dtdata1.Rows[dd]["COLUMN09"].ToString() });
                }
                ViewData["VendorPart"] = new SelectList(VendorPart, "Value", "Text");
                //EMPHCS1852 Item Desc Added in Item Search(Quick) Header and Grid Level
				List<SelectListItem> ItemDesc = new List<SelectListItem>();
                SqlDataAdapter cmddl2 = new SqlDataAdapter("select COLUMN50 from MATABLE007 where COLUMN50 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null )  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata2 = new DataTable();
                cmddl2.Fill(dtdata2);
                for (int dd = 0; dd < dtdata2.Rows.Count; dd++)
                {
                    ItemDesc.Add(new SelectListItem { Value = dtdata2.Rows[dd]["COLUMN50"].ToString(), Text = dtdata2.Rows[dd]["COLUMN50"].ToString() });
                }
                ViewData["ItemDesc"] = new SelectList(ItemDesc, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
      


        
        public ActionResult GetUPCDetails(string UPCID)
        {

            var OPUnit = Session["OPUnit"];
            var AcOwner = Session["AcOwner"];
            SqlCommand cmd = new SqlCommand("select column06 from MATABLE007 and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0) UPCID = dt.Rows[0][0].ToString();
            else { UPCID = "0"; }
            return Json(new { Data = UPCID }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetVendorPart(string VendorPart)
        {

            var OPUnit = Session["OPUnit"];
            var AcOwner = Session["AcOwner"];
            SqlCommand cmd1 = new SqlCommand("select column09 from MATABLE007 and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            if (dt1.Rows.Count > 0) VendorPart = dt1.Rows[0][0].ToString();
            else { VendorPart = "0"; }
            return Json(new { Data = VendorPart }, JsonRequestBehavior.AllowGet);

        }
		//EMPHCS1852 Item Desc Added in Item Search(Quick) Header and Grid Level
        public ActionResult GetItemDesc(string ItemDesc)
        {

            var OPUnit = Session["OPUnit"];
            var AcOwner = Session["AcOwner"];
            SqlCommand cmd1 = new SqlCommand("select column50 from MATABLE007 and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            if (dt1.Rows.Count > 0) ItemDesc = dt1.Rows[0][0].ToString();
            else { ItemDesc = "0"; }
            return Json(new { Data = ItemDesc }, JsonRequestBehavior.AllowGet);

        }
//EMPHCS1852 Item Desc Added in Item Search(Quick) Header and Grid Level
        public ActionResult GetItemDetails(String Item, string UPC, string VendorPart, string ItemDesc)
        {
            try
        {
            var OPunit = Session["OPunit"];
            var AcOwner = Session["AcOwner"];
            SqlCommand cmd = new SqlCommand("QuickSearch", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Item", Item);
             cmd.Parameters.AddWithValue("@UPC", UPC);
             cmd.Parameters.AddWithValue("@VendorPart", VendorPart);
             cmd.Parameters.AddWithValue("@ItemDesc", ItemDesc);
            //cmd.Parameters.AddWithValue("@UOM", UOM);
            //cmd.Parameters.AddWithValue("@Lot", Lot);
            //cmd.Parameters.AddWithValue("@QtyAval", QtyAval);
            //cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
            cmd.Parameters.AddWithValue("@AcOwner", AcOwner);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            ViewBag.Search = "";
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToString(dt.Rows[0]["Image"]) != "")
                    ViewBag.Search = "/Content/Upload/Images/Item/Item/" + Convert.ToString(dt.Rows[0]["Image"]);
            }
            string htmlstring = null, htmlstring1 = null;
            for (int g = 0; g < dt.Rows.Count; g++)
            {
                //string dvalue = Convert.ToString(dt.Rows[g][dt.Columns[7].ColumnName]);
                //string dddata = "";
                //List<SelectListItem> UOM = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                //DataTable dtdata = new DataTable();
                //cmddl.Fill(dtdata);
                //for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                //{
                //    UOM.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["UOM"] = new SelectList(UOM, "Value", "Text", selectedValue: dvalue).ToString();
                //dddata += "<option value=''>--Select--</option>";
                //for (int d = 0; d < UOM.Count; d++)
                //{
                //    if (UOM[d].Value == dvalue)
                //        dddata += "<option value=" + UOM[d].Value + " selected>" + UOM[d].Text + "</option>";
                //    else
                //        dddata += "<option value=" + UOM[d].Value + ">" + UOM[d].Text + "</option>";
               // }
			   //EMPHCS1852 Item Desc Added in Item Search(Quick) Header and Grid Level
                htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dItem>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dItemDesc>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dUPC>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dLocation>" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dLot#>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dQty Available>" + dt.Rows[g][dt.Columns[6].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dOperating Unit>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[7].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[7].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[7].ColumnName] + "' ></td></tr>";
            }
            return Json(new { Data = htmlstring, Data1 = ViewBag.Search }, JsonRequestBehavior.AllowGet);


        }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }

    }
}       

