﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class SetupController : Controller
    {
        public ActionResult SetupFinancialYear()
        {
            try
            {
                DateFormat();
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlDataAdapter da = new SqlDataAdapter("SELECT 'FY'+CAST(IIF(DATEPART(yyyy,GETDATE())!=DATEPART(yyyy,DATEADD(DAY,-1,DATEADD(YEAR,1,GETDATE()))),CAST(DATEPART(yyyy,GETDATE())AS NVARCHAR)+'-'+CAST(DATEPART(yyyy,GETDATE())+1 AS NVARCHAR),CAST(DATEPART(yyyy,GETDATE())AS NVARCHAR))AS NVARCHAR)", cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                ViewBag.TransNo = dt.Rows[0][0];
                List<SelectListItem> SubPeriod = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where  COLUMN03=11200 and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    SubPeriod.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString(), Selected = true });
                }
                ViewData["SubPeriod"] = new SelectList(SubPeriod, "Value", "Text", "23150");
                return View();
            }
            catch { return RedirectToAction("Info", "EmployeeMaster"); }
        }

        public ActionResult CloseStock()
        {
            try
            {
                DateFormat();
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlDataAdapter da = new SqlDataAdapter("SELECT 'CS'+RIGHT('00000' + CAST((IIF(EXISTS(SELECT COLUMN01 FROM FITABLE054 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0),(SELECT COUNT(COLUMN01) +1 FROM FITABLE054 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0), 1))AS VARCHAR(5)),5 )", cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                ViewBag.TransNo = dt.Rows[0][0];
                return View();
            }
            catch { return RedirectToAction("Info", "EmployeeMaster"); }
        }

        public ActionResult CloseAccount()
        {
            try
            {
                DateFormat();
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlDataAdapter da = new SqlDataAdapter("SELECT 'CA'+RIGHT('00000' + CAST((IIF(EXISTS(SELECT COLUMN01 FROM FITABLE055 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0),(SELECT COUNT(COLUMN01) +1 FROM FITABLE055 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0), 1))AS VARCHAR(5)),5 )", cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                ViewBag.TransNo = dt.Rows[0][0];
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Party"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Item, "Value", "Text");

                return View();
            }
            catch { return RedirectToAction("Info", "EmployeeMaster"); }
        }

        public ActionResult CloseFinancialYear()
        {

            try
            {
                DateFormat();
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlDataAdapter da = new SqlDataAdapter("SELECT 'CFY'+RIGHT('00000' + CAST((IIF(EXISTS(SELECT COLUMN01 FROM FITABLE056 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0),(SELECT COUNT(COLUMN01) +1 FROM FITABLE056 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0), 1))AS VARCHAR(5)),5 )", cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                ViewBag.TransNo = dt.Rows[0][0];
                return View();
            }
            catch { return RedirectToAction("Info", "EmployeeMaster"); }
        }

        public ActionResult SettingFiscalYear()
        {

            try
            {
                DateFormat();
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlDataAdapter da = new SqlDataAdapter("SELECT 'SFY'+RIGHT('00000' + CAST((IIF(EXISTS(SELECT COLUMN01 FROM FITABLE048 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0),(SELECT COUNT(COLUMN01) +1 FROM FITABLE048 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0), 1))AS VARCHAR(5)),5 )", cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                ViewBag.TransNo = dt.Rows[0][0];
                List<SelectListItem> FYear = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE053  where isnull(COLUMNA13,'False')='False' AND COLUMNA03=" + Session["AcOwner"] + "  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    FYear.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString(), Selected = true });
                }
                ViewData["FinancialYear"] = new SelectList(FYear, "Value", "Text");
                return View();
            }
            catch { return RedirectToAction("Info", "EmployeeMaster"); }
        }

        public ActionResult SettingFiscalYearInfo()
        {
            try
            {
                DateFormat();
                Session["id"] = "0"; 
                Session["FormName"] = "SettingFiscalYearInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN03 ] AS [Trans#],FORMAT(p.[COLUMN07 ],'" + Session["DateFormat"] + "') AS [Date],s.[COLUMN04 ] AS [Financial Year],FORMAT(p.COLUMN04,'" + Session["DateFormat"] + "') AS [Year Start Date],FORMAT(p.COLUMN05,'" + Session["DateFormat"] + "')  AS [Year End Date],p.COLUMN08 [Lock Financial Year],iif(p.[COLUMN09]=1,'Yes','No') AS [Is Active] From FITABLE048 p " +
                             " left outer join  FITABLE053 s on s.COLUMN02=p.COLUMN06 where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " ORDER BY p.COLUMN03 desc";
                alCol.AddRange(new List<string> { "ID", "Trans#", "Date", "Financial Year", "Year Start Date", "Year End Date", "Lock Financial Year", "Is Active" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN03", "COLUMN07", "COLUMN04", "COLUMN04", "COLUMN05", "COLUMN08", "COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SettingFiscalYearEdit(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["FormName"] = "SettingFiscalYearEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN03 ] AS [Trans],FORMAT(p.[COLUMN07 ],'" + Session["DateFormat"] + "') AS [Date],s.[COLUMN04 ] AS [FinancialYear],p.[COLUMN06 ] AS [FinancialYearID],FORMAT(p.COLUMN04,'" + Session["DateFormat"] + "') AS [YearStartDate],FORMAT(p.COLUMN05,'" + Session["DateFormat"] + "')  AS [YearEndDate],p.COLUMN08  AS [IsActive] From FITABLE048 p " +
                             " left outer join  FITABLE053 s on s.COLUMN02=p.COLUMN06 where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND P.COLUMN02='" + ide + "' ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.FYear = GData.FirstOrDefault().FinancialYear;
                se.FYearID = GData.FirstOrDefault().FinancialYearID;
                se.YSDate = GData.FirstOrDefault().YearStartDate;
                se.YEDate = GData.FirstOrDefault().YearEndDate;
                se.Lock = GData.FirstOrDefault().IsActive;
                List<SelectListItem> FYear = new List<SelectListItem>();
                FYear.Add(new SelectListItem { Value = se.FYearID.ToString(), Text = se.FYear.ToString(), Selected = true });
                ViewData["FinancialYear"] = new SelectList(FYear, "Value", "Text");
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SettingFiscalYearView(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "SettingFiscalYearEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN03 ] AS [Trans],FORMAT(p.[COLUMN07 ],'" + Session["DateFormat"] + "') AS [Date],s.[COLUMN04 ] AS [FinancialYear],p.[COLUMN06 ] AS [FinancialYearID],FORMAT(p.COLUMN04,'" + Session["DateFormat"] + "') AS [YearStartDate],FORMAT(p.COLUMN05,'" + Session["DateFormat"] + "')  AS [YearEndDate],p.COLUMN08  AS [IsActive] From FITABLE048 p " +
                             " left outer join  FITABLE053 s on s.COLUMN02=p.COLUMN06 where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND P.COLUMN02='" + ide + "' ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.FYear = GData.FirstOrDefault().FinancialYear;
                se.FYearID = GData.FirstOrDefault().FinancialYearID;
                se.YSDate = GData.FirstOrDefault().YearStartDate;
                se.YEDate = GData.FirstOrDefault().YearEndDate;
                se.Lock = GData.FirstOrDefault().IsActive;
                List<SelectListItem> FYear = new List<SelectListItem>();
                FYear.Add(new SelectListItem { Value = se.FYearID.ToString(), Text = se.FYear.ToString(), Selected = true });
                ViewData["FinancialYear"] = new SelectList(FYear, "Value", "Text");
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SetupFinancialYearInfo()
        {
            try
            {
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseFinancialYearInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans#],FORMAT(p.[COLUMN10 ],'" + Session["DateFormat"] + "') AS [Date],s.[COLUMN04 ] AS [Sub Period],p.[COLUMN06 ] AS [No of Periods],FORMAT(p.COLUMN07,'" + Session["DateFormat"] + "') AS [Year Start Date],FORMAT(p.COLUMN08,'" + Session["DateFormat"] + "')  AS [Year End Date],iif(p.COLUMN09=1,'Yes','No') [Is Active] From FITABLE053 p left outer join  MATABLE002 s on s.COLUMN02=p.COLUMN05 where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " ORDER BY p.COLUMN03 desc";
                alCol.AddRange(new List<string> { "ID", "Trans#", "Date", "Sub Period", "No of Periods", "Year Start Date", "Year End Date", "Is Active" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN10", "COLUMN04", "COLUMN06", "COLUMN07", "COLUMN08", "COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SetupFinancialYearEdit(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["FormName"] = "CloseFinancialYearInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans],Format(p.[COLUMN10 ],'" + Session["DateFormat"] + "') AS [Date],s.[COLUMN04 ] AS [SubPeriod],p.[COLUMN05 ] AS [SubPeriodID],p.[COLUMN06 ] AS [NoofPeriods],FORMAT(p.COLUMN07,'" + Session["DateFormat"] + "') AS [YearStartDate],FORMAT(p.COLUMN08,'" + Session["DateFormat"] + "')  AS [YearEndDate],iif(p.COLUMN09=1,'Yes','No') [IsActive] From FITABLE053 p left outer join  MATABLE002 s on s.COLUMN02=p.COLUMN05 where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND p.COLUMN02=" + ide + " ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.SubPeriodID = Convert.ToString(GData.FirstOrDefault().SubPeriodID);
                se.SubPeriod = GData.FirstOrDefault().SubPeriod;
                se.NoofPeriods = Convert.ToString(GData.FirstOrDefault().NoofPeriods);
                se.YSDate = GData.FirstOrDefault().YearStartDate;
                se.YEDate = GData.FirstOrDefault().YearEndDate;
                se.IsActive = GData.FirstOrDefault().IsActive;
                List<SelectListItem> SubPeriod = new List<SelectListItem>();
                SubPeriod.Add(new SelectListItem { Value = se.SubPeriodID.ToString(), Text = se.SubPeriod.ToString(), Selected = true });
                ViewData["SubPeriod"] = new SelectList(SubPeriod, "Value", "Text");
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SetupFinancialYearView(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseFinancialYearInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans],Format(p.[COLUMN10 ],'" + Session["DateFormat"] + "') AS [Date],s.[COLUMN04 ] AS [SubPeriod],p.[COLUMN05 ] AS [SubPeriodID],p.[COLUMN06 ] AS [NoofPeriods],FORMAT(p.COLUMN07,'" + Session["DateFormat"] + "') AS [YearStartDate],FORMAT(p.COLUMN08,'" + Session["DateFormat"] + "')  AS [YearEndDate],iif(p.COLUMN09=1,'Yes','No') [IsActive] From FITABLE053 p left outer join  MATABLE002 s on s.COLUMN02=p.COLUMN05 where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND p.COLUMN02=" + ide + " ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.SubPeriodID = Convert.ToString(GData.FirstOrDefault().SubPeriodID);
                se.SubPeriod = GData.FirstOrDefault().SubPeriod;
                se.NoofPeriods = Convert.ToString(GData.FirstOrDefault().NoofPeriods);
                se.YSDate = GData.FirstOrDefault().YearStartDate;
                se.YEDate = GData.FirstOrDefault().YearEndDate;
                se.IsActive = GData.FirstOrDefault().IsActive;
                List<SelectListItem> SubPeriod = new List<SelectListItem>();
                SubPeriod.Add(new SelectListItem { Value = se.SubPeriodID.ToString(), Text = se.SubPeriod.ToString(), Selected = true });
                ViewData["SubPeriod"] = new SelectList(SubPeriod, "Value", "Text");
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseFinancialYearInfo()
        {
            try
            {
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseFinancialYearInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans#],FORMAT(p.[COLUMN10 ],'" + Session["DateFormat"] + "') AS [Date],FORMAT(p.COLUMN05,'" + Session["DateFormat"] + "') AS [Year Start Date],FORMAT(p.COLUMN06,'" + Session["DateFormat"] + "')  AS [Year End Date],iif(p.COLUMN07=1,'Yes','No') [Is Stock Closed],iif(p.[COLUMN08]=1,'Yes','No') AS [Is Account Closed],p.[COLUMN09] AS [Lock Financial Year] From FITABLE056 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " ORDER BY p.COLUMN04 desc";
                alCol.AddRange(new List<string> { "ID", "Trans#", "Date", "Year Start Date", "Year End Date", "Is Stock Closed", "Is Account Closed", "Lock Financial Year" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN10", "COLUMN05", "COLUMN06", "COLUMN07", "COLUMN08", "COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseFinancialYearEdit(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["FormName"] = "CloseFinancialYearEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans],FORMAT(p.[COLUMN10 ],'" + Session["DateFormat"] + "') AS [Date],FORMAT(p.COLUMN05,'" + Session["DateFormat"] + "') AS [YearStartDate],FORMAT(p.COLUMN06,'" + Session["DateFormat"] + "')  AS [YearEndDate],iif(p.COLUMN07=1,'Yes','No') [IsStockClosed],iif(p.[COLUMN08]=1,'Yes','No') AS [IsAccountClosed],p.[COLUMN09] AS [LockFinancialYear] From FITABLE056 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND p.COLUMN02=" + ide + "  ORDER BY p.COLUMN04 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.Stock = GData.FirstOrDefault().IsStockClosed;
                se.Account = GData.FirstOrDefault().IsAccountClosed;
                se.YSDate = GData.FirstOrDefault().YearStartDate;
                se.YEDate = GData.FirstOrDefault().YearEndDate;
                se.Lock = GData.FirstOrDefault().LockFinancialYear;
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseFinancialYearView(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseFinancialYearView";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans],FORMAT(p.[COLUMN10 ],'" + Session["DateFormat"] + "') AS [Date],FORMAT(p.COLUMN05,'" + Session["DateFormat"] + "') AS [YearStartDate],FORMAT(p.COLUMN06,'" + Session["DateFormat"] + "')  AS [YearEndDate],iif(p.COLUMN07=1,'Yes','No') [IsStockClosed],iif(p.[COLUMN08]=1,'Yes','No') AS [IsAccountClosed],p.[COLUMN09] AS [LockFinancialYear] From FITABLE056 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND p.COLUMN02=" + ide + "  ORDER BY p.COLUMN04 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.Stock = GData.FirstOrDefault().IsStockClosed;
                se.Account = GData.FirstOrDefault().IsAccountClosed;
                se.YSDate = GData.FirstOrDefault().YearStartDate;
                se.YEDate = GData.FirstOrDefault().YearEndDate;
                se.Lock = GData.FirstOrDefault().LockFinancialYear;
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseAccountInfo()
        {
            try
            {
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseAccountInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Trans#],FORMAT(p.[COLUMN06 ],'" + Session["DateFormat"] + "') AS [Date],p.COLUMN07 AS [Fiscal Year],p.COLUMN04 AS [Is Inventory Closed]  From FITABLE055 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " ORDER BY p.COLUMN03 desc";
                alCol.AddRange(new List<string> { "ID", "Trans#", "Date", "Fiscal Year", "Is Inventory Closed" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN05", "COLUMN06", "COLUMN04" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseAccountEdit(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["FormName"] = "CloseStockEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Trans],FORMAT(p.[COLUMN06 ],'" + Session["DateFormat"] + "') AS [Date],p.COLUMN07 AS [FiscalYear],p.COLUMN04 AS [IsInventoryClosed]  From FITABLE055 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " and p.column02=" + Request["ide"] + " ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.FYear = GData.FirstOrDefault().FiscalYear;
                se.Stock = Convert.ToString(GData.FirstOrDefault().IsInventoryClosed);
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseAccountView(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseStockEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Trans],FORMAT(p.[COLUMN06 ],'" + Session["DateFormat"] + "') AS [Date],p.COLUMN07 AS [FiscalYear],p.COLUMN04 AS [IsInventoryClosed]  From FITABLE055 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " and p.column02=" + Request["ide"] + " ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.FYear = GData.FirstOrDefault().FiscalYear;
                se.Stock = Convert.ToString(GData.FirstOrDefault().IsInventoryClosed);
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseStockInfo()
        {
            try
            {
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseStockInfo";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans#],FORMAT(p.[COLUMN05 ],'" + Session["DateFormat"] + "') AS [Date],p.COLUMN06 AS [Fiscal Year]  From FITABLE054 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " ORDER BY p.COLUMN03 desc";
                alCol.AddRange(new List<string> { "ID", "Trans#", "Date", "Fiscal Year" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN05", "COLUMN06" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseStockEdit(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["FormName"] = "CloseStockEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans],FORMAT(p.[COLUMN05 ],'" + Session["DateFormat"] + "') AS [Date],p.COLUMN06 AS [FiscalYear],p.COLUMN07 AS [Memo]  From FITABLE054 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND column02=" + ide + " ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.FYear = GData.FirstOrDefault().FiscalYear;
                se.Memo = GData.FirstOrDefault().Memo;
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CloseStockView(string ide)
        {
            try
            {
                SetupEntities se = new SetupEntities();
                DateFormat();
                Session["id"] = "0";
                Session["FormName"] = "CloseStockEdit";
                int? acID = (int?)Session["AcOwner"];
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Trans],FORMAT(p.[COLUMN05 ],'" + Session["DateFormat"] + "') AS [Date],p.COLUMN06 AS [FiscalYear],p.COLUMN07 AS [Memo]  From FITABLE054 p where isnull(p.COLUMNA13,'False')='False'  and p.COLUMNA03=" + acID + " AND column02=" + ide + " ORDER BY p.COLUMN03 desc";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                se.Trans = GData.FirstOrDefault().Trans;
                se.Date = GData.FirstOrDefault().Date;
                se.FYear = GData.FirstOrDefault().FiscalYear;
                se.Memo = GData.FirstOrDefault().Memo;
                return View(se);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult NewRow()
        {
            try
            {
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                Customer = Customer.OrderBy(x => x.Text).ToList();
                List<SelectListItem> Account = new List<SelectListItem>();
                SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dta = new DataTable();
                cmda.Fill(dta);
                for (int dd = 0; dd < dta.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                }
                Account = Account.OrderBy(x => x.Text).ToList();
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                string dddataA = "";
                dddataA += "<option value='0'>--Select--</option>";
                for (int d = 0; d < Account.Count; d++)
                {
                    dddataA += "<option value=" + Account[d].Value + " >" + Account[d].Text + "</option>";
                }
                string dddataP = "";
                dddataP += "<option value='0'>--Select--</option>";
                for (int d = 0; d < Customer.Count; d++)
                {
                    dddataP += "<option value=" + Customer[d].Value + " >" + Customer[d].Text + "</option>";
                }
                string dddataI = "";
                dddataI += "<option value='0'>--Select--</option>";
                for (int d = 0; d < Item.Count; d++)
                {
                    dddataI += "<option value=" + Item[d].Value + " >" + Item[d].Text + "</option>";
                }
                string htmlstring = "";
                htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></td>";
                htmlstring += "<td><select class='gridddl' id='Account' name='Account'>" + dddataA + "</select></td>";
                htmlstring += "<td><select class='gridddl' id='Party' name='Party'>" + dddataP + "</select></td>";
                htmlstring += "<td><input type='text' id='Actual' class='txtgridclass'  pattern=''    itemid='Actual' name='Actual'    value='' ></td>";
                htmlstring += "<td><input type='text' id='Increase' class='txtgridclass'  pattern=''    itemid='Increase' name='Increase'    value='' ></td>";
                htmlstring += "<td><input type='text' id='Decrease' class='txtgridclass'  pattern=''    itemid='Decrease' name='Decrease'    value='' ></td>";
                htmlstring += "<td><input type='text' id='Final Balance' class='txtgridclass'  pattern=''    itemid='Final Balance' name='Final Balance'    value='' ></td></tr>";

                return this.Json(new { Data = htmlstring }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GridBind()
        {
            try
            {
                string htmlstring = "";
                FinancialYearChecking();
                DateFormat();
                if (fYear != "" && fYear != fYearS)
                {
                    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter("SELECT 'FY'+RIGHT('00000' + CAST((IIF(EXISTS(SELECT COLUMN01 FROM FITABLE048 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0),(SELECT COUNT(COLUMN01) +1 FROM FITABLE048 WHERE COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0), 1))AS VARCHAR(5)),5 )", cn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    ViewBag.TransNo = dt.Rows[0][0];
                    SqlCommand cmd = new SqlCommand("USP_REPORTS_CLOSESTOCK", cn);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                    cmd.Parameters.AddWithValue("@OPUnit", Convert.ToString(Session["OPUnit"]));
                    cmd.Parameters.AddWithValue("@Project", "");
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FromDate", endDate);
                    cmd.Parameters.AddWithValue("@ToDate", endDate);
                    cmd.Parameters.AddWithValue("@Type", "");
                    cmd.Parameters.AddWithValue("@OperatingUnit", Convert.ToString(Session["OPUnit"]));
                    cmd.Parameters.AddWithValue("@DateF", Session["DateFormat"].ToString());
                    SqlDataAdapter daS = new SqlDataAdapter(cmd);
                    DataTable dtS = new DataTable();
                    daS.Fill(dtS);
                    string dddataI = "";
                    string dddataU = "";
                    string dddataL = "";
                    string dddataT = "";
                    string dddataO = "";
                    dddataI += "<option value='0'>--Select--</option>";
                    dddataU += "<option value='0'>--Select--</option>";
                    dddataL += "<option value='0'>--Select--</option>";
                    dddataT += "<option value='0'>--Select--</option>";
                    dddataO += "<option value='0'>--Select--</option>";
                    for (int i = 0; i < dtS.Rows.Count; i++)
                    {
                        dddataI = "<option value=" + dtS.Rows[i]["ItemID"] + " >" + dtS.Rows[i]["Item"] + "</option>";
                        dddataU = "<option value=" + dtS.Rows[i]["uomid"] + " >" + dtS.Rows[i]["uom"] + "</option>";
                        dddataL = "<option value=" + dtS.Rows[i]["LocID"] + " >" + dtS.Rows[i]["Location"] + "</option>";
                        dddataT = "<option value=" + dtS.Rows[i]["LotID"] + " >" + dtS.Rows[i]["Lot"] + "</option>";
                        dddataO = "<option value=" + dtS.Rows[i]["OPID"] + " >" + dtS.Rows[i]["OU"] + "</option>";
                        htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></td>";
                        htmlstring += "<td><select class='gridddl' id='Item' name='Item' style='min-width: 150px'>" + dddataI + "</select></td>";
                        htmlstring += "<td><select class='gridddl' id='UOM' name='UOM' style='min-width: 120px'>" + dddataU + "</select></td>";
                        htmlstring += "<td><select class='gridddl' id='Location' name='Location' style='min-width: 120px'>" + dddataL + "</select></td>";
                        htmlstring += "<td><select class='gridddl' id='Lot' name='Lot' style='min-width: 120px'>" + dddataT + "</select></td>";
                        htmlstring += "<td><select class='gridddl' id='Operating Unit' name='Operating Unit' style='min-width: 120px'>" + dddataO + "</select></td>";
                        htmlstring += "<td><input type='text' id='Actual' class='txtgridclass'  pattern=''    itemid='Actual' name='Actual' readonly='true'     value=' " + dtS.Rows[i]["closing"] + "' ></td>";
                        htmlstring += "<td><input type='text' id='Increase' class='txtgridclass'  pattern=''    itemid='Increase' name='Increase' readonly='true'  value='" + dtS.Rows[i]["AvgPrice"] + "' ></td>";
                        htmlstring += "<td><input type='text' id='Decrease' class='txtgridclass'  pattern=''    itemid='Decrease' name='Decrease' readonly='true'  value='" + dtS.Rows[i]["asset"] + "' ></td>";
                        htmlstring += "<td><input type='text' id='Final Balance' class='txtgridclass'  pattern=''    itemid='Final Balance' readonly='true'  name='Final Balance'    value='" + dtS.Rows[i]["closing"] + "' ></td>";
                        htmlstring += "<td><input type='text' id='Note' class='txtgridclass'  pattern=''    itemid='Note' name='Note'    value='' ></td></tr>";
                    }
                }
                var startD = DateTime.ParseExact(startDATE.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                return this.Json(new { Data = htmlstring, startD = startD, fYear = fYear, fYearS = fYearS }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GridAccountBind()
        {
            try
            {
                string htmlstring = "";
                FinancialYearChecking();
                DateFormat();
                if (fYear != "" && fYear != fYearA)
                {
                    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                    SqlCommand cmd = new SqlCommand("USP_FIN_ACCOUNTSCLOSE", cn);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OPUnit", Convert.ToString(Session["OPUnit"]));
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    SqlDataAdapter daS = new SqlDataAdapter(cmd);
                    DataTable dtS = new DataTable();
                    daS.Fill(dtS);
                    string dddataA = "";
                    string dddataP = "";
                    string dddataO = "";
                    //string dddataL = "";
                    //string dddataT = "";
                    dddataA += "<option value='0'>--Select--</option>";
                    dddataP += "<option value='0'>--Select--</option>";
                    dddataO += "<option value='0'>--Select--</option>";
                    //dddataL += "<option value='0'>--Select--</option>";
                    //dddataT += "<option value='0'>--Select--</option>";
                    for (int i = 0; i < dtS.Rows.Count; i++)
                    {
                        dddataA = "<option value=" + dtS.Rows[i]["ID"] + " >" + dtS.Rows[i]["Account Name"] + "</option>";
                        dddataP = "<option value=" + dtS.Rows[i]["PartyId"] + " >" + dtS.Rows[i]["Party"] + "</option>";
                        dddataO = "<option value=" + dtS.Rows[i]["OUId"] + " >" + dtS.Rows[i]["OU"] + "</option>";
                        //dddataL = "<option value=" + dtS.Rows[i]["LocID"] + " >" + dtS.Rows[i]["Location"] + "</option>";
                        //dddataT = "<option value=" + dtS.Rows[i]["LotID"] + " >" + dtS.Rows[i]["Lot"] + "</option>";
                        htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></td>";
                        htmlstring += "<td><select class='gridddl' id='Account' name='Account' style='min-width: 150px'>" + dddataA + "</select></td>";
                        htmlstring += "<td><select class='gridddl' id='Party' name='Party' style='min-width: 150px'>" + dddataP + "</select></td>";
                        htmlstring += "<td><select class='gridddl' id='Operating Unit' name='Party' style='min-width: 120px'>" + dddataO + "</select></td>";
                        htmlstring += "<td><input type='text' id='Actual' class='txtgridclass'  pattern=''    itemid='Actual' name='Actual' readonly='true'   value=' " + dtS.Rows[i]["Balance On Hand"] + "' ></td>";
                        //htmlstring += "<td><input type='text' id='Increase' class='txtgridclass'  pattern=''    itemid='Increase' name='Increase'    value='0' ></td>";
                        //htmlstring += "<td><input type='text' id='Decrease' class='txtgridclass'  pattern=''    itemid='Decrease' name='Decrease'    value='0' ></td>";
                        // htmlstring += "<td><input type='text' id='Final Balance' class='txtgridclass'  pattern=''    itemid='Final Balance'  readonly='true'  name='Final Balance'    value='" + dtS.Rows[i]["Balance On Hand"] + "' ></td>";
                        htmlstring += "<td><input type='text' id='Last Payment Made' class='txtgridclass'  pattern=''    itemid='Last Payment Made' name='Last Payment Made'    value='' ></td>";
                        htmlstring += "<td><input type='text' id='Last Transaction Made' class='txtgridclass'  pattern=''    itemid='Last Transaction Made' name='Last Transaction Made'    value='' ></td>";
                        htmlstring += "<td><input type='text' id='Note' class='txtgridclass'  pattern=''    itemid='Note' name='Note'    value='' ></td></tr>";
                    }
                }
                var startD = DateTime.ParseExact(startDATE.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                return this.Json(new { Data = htmlstring, startD = startD, fYear = fYear, fYearA = fYearA }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GridFilter(string sub, string sdt)
        {
            try
            {
                string dt = DateTime.ParseExact(sdt, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                DateTime sDT = Convert.ToDateTime(dt);
                DateTime eDT = Convert.ToDateTime(dt);
                var EDT = DateTime.ParseExact(sDT.AddMonths(12).AddDays(-1).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                string htmlstring = "", periodString = "";
                int j = 0, mt = 0;
                var trans = "FY" + sDT.Year.ToString();
                if (sDT.Year != sDT.AddMonths(12).AddDays(-1).Year)
                {
                    trans = "FY" + sDT.Year.ToString() + "-" + sDT.AddMonths(12).AddDays(-1).ToString("yy");
                }
                if (sub == "23151") { j = 4; periodString = "Quarter "; mt = 3; }
                else if (sub == "23152") { j = 12; periodString = "Month "; mt = 1; }
                else if (sub == "23150") { j = 1; periodString = "Year "; mt = 12; }
                for (int i = 0; i < j; i++)
                {
                    htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></td>";
                    htmlstring += "<td><input type='text' id='Name' class='txtgridclass'  pattern=''    itemid='Name' name='Name'    value='" + periodString + (i + 1).ToString() + "' ></td>";
                    htmlstring += "<td><input type='text' id='From Date' class='txtgridclass'  pattern=''    itemid='From Date' name='From Date' readonly='true'    value='" + DateTime.ParseExact(sDT.AddMonths(mt * i).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture) + "' ></td>";
                    htmlstring += "<td><input type='text' id='To Date' class='txtgridclass'  pattern=''    itemid='To Date' name='To Date' readonly='true'    value='" + DateTime.ParseExact(sDT.AddMonths(mt * (i + 1)).AddDays(-1).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture) + "' ></td></tr>";
                }

                return this.Json(new { Data = htmlstring, EDT = EDT, trans = trans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public void DateFormat()
        {

            Session["id"] = "1";
			SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
            SqlDataAdapter daf = new SqlDataAdapter(str, cn);
            DataTable dtf = new DataTable();
            daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
            if (dtf.Rows.Count > 0)
            {
                DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                Session["DateFormat"] = DateFormat;
                ViewBag.DateFormat = DateFormat;
            }
            if (JQDateFormat != "")
            {
                Session["DateFormat"] = DateFormat;
                Session["ReportDate"] = JQDateFormat;
            }
            else
            {
                Session["DateFormat"] = "dd/MM/yyyy";
                Session["ReportDate"] = "dd/mm/yy";
                ViewBag.DateFormat = "dd/MM/yyyy";
            }
        }

        public ActionResult SaveSetupFinancialYear(string FinancialYear, string Date, string SubPeriod, string NoofPeriods, string YearStartDate, string YearEndDate, string IsActive, string insert)
        {
            try
            {
                string OutParam = null;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["SFYGridData"];
                string DateT = DateTime.Now.ToString();
                YearStartDate = DateTime.ParseExact(YearStartDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                YearEndDate = DateTime.ParseExact(YearEndDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                Date = DateTime.ParseExact(Date, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                //string insert = "Insert";
                int l = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("USP_FIN_BL_SETUPFINANCIALYEAR", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", Request["ide"]);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "0");
                    Cmd.Parameters.AddWithValue("@COLUMN04", FinancialYear);
                    Cmd.Parameters.AddWithValue("@COLUMN05", SubPeriod);
                    Cmd.Parameters.AddWithValue("@COLUMN06", NoofPeriods);
                    Cmd.Parameters.AddWithValue("@COLUMN07", YearStartDate);
                    Cmd.Parameters.AddWithValue("@COLUMN08", YearEndDate);
                    Cmd.Parameters.AddWithValue("@COLUMN09", IsActive);
                    Cmd.Parameters.AddWithValue("@COLUMN10", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TABLENAME", "FITABLE053");
                    Cmd.Parameters.Add("@OUTPARAM", SqlDbType.NVarChar, 250).Direction = ParameterDirection.Output;
                    cnc.Open();
                    int r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToString(Cmd.Parameters["@OUTPARAM"].Value);
                }
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("USP_FIN_BL_SETUPFINANCIALYEAR", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", Convert.ToString(itemdata.Tables[0].Rows[i][3]));
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Convert.ToString(itemdata.Tables[0].Rows[i][0]));
                        Cmdl.Parameters.AddWithValue("@COLUMN04", DateTime.ParseExact(Convert.ToString(itemdata.Tables[0].Rows[i][1]), Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        Cmdl.Parameters.AddWithValue("@COLUMN05", DateTime.ParseExact(Convert.ToString(itemdata.Tables[0].Rows[i][2]), Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        Cmdl.Parameters.AddWithValue("@COLUMN06", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", DateT);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", DateT);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TABLENAME", "FITABLE059");
                        cnc.Open();
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                Session["FYLN"] = FinancialYear;
                if (l > 0)
                {
                    var msg = string.Empty;
                    msg = "Setup Financial Year Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SaveCloseStock(string FinancialYear, string Date, string FYtxt, string insert, string memo)
        {
            try
            {
                string OutParam = null;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["SFYGridData"];
                string DateT = DateTime.Now.ToString();
                Date = DateTime.ParseExact(Date, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                //string insert = "Insert";
                int l = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("USP_FIN_BL_CLOSESTOCK", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", Request["ide"]);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "0");
                    Cmd.Parameters.AddWithValue("@COLUMN04", FinancialYear);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN06", FYtxt);
                    Cmd.Parameters.AddWithValue("@COLUMN07", memo);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TABLENAME", "FITABLE054");
                    Cmd.Parameters.Add("@OUTPARAM", SqlDbType.NVarChar, 250).Direction = ParameterDirection.Output;
                    cnc.Open();
                    int r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToString(Cmd.Parameters["@OUTPARAM"].Value);
                }
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("USP_FIN_BL_CLOSESTOCK", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", Convert.ToString(itemdata.Tables[0].Rows[i][10]));
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Convert.ToString(itemdata.Tables[0].Rows[i][0]));
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Convert.ToString(itemdata.Tables[0].Rows[i][1]));
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Convert.ToString(itemdata.Tables[0].Rows[i][2]));
                        Cmdl.Parameters.AddWithValue("@COLUMN06", Convert.ToString(itemdata.Tables[0].Rows[i][3]));
                        Cmdl.Parameters.AddWithValue("@COLUMN07", Convert.ToString(itemdata.Tables[0].Rows[i][5]));
                        Cmdl.Parameters.AddWithValue("@COLUMN08", Convert.ToString(itemdata.Tables[0].Rows[i][6]));
                        Cmdl.Parameters.AddWithValue("@COLUMN09", Convert.ToString(itemdata.Tables[0].Rows[i][7]));
                        Cmdl.Parameters.AddWithValue("@COLUMN11", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", Convert.ToString(itemdata.Tables[0].Rows[i][8]));
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Convert.ToString(itemdata.Tables[0].Rows[i][9]));
                        Cmdl.Parameters.AddWithValue("@COLUMN12", Convert.ToString(itemdata.Tables[0].Rows[i][4]));
                        Cmdl.Parameters.AddWithValue("@COLUMNa02", Convert.ToString(itemdata.Tables[0].Rows[i][4]));
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", DateT);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", DateT);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TABLENAME", "FITABLE057");
                        cnc.Open();
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                if (l > 0)
                {
                    var msg = string.Empty;
                    msg = "Close Stock Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SaveCloseAccount(string FinancialYear, string Date, string IsActive, string FYtxt, string insert)
        {
            try
            {
                string OutParam = null;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["SFYGridData"];
                string DateT = DateTime.Now.ToString();
                Date = DateTime.ParseExact(Date, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                //string insert = "Insert";
                int l = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("USP_FIN_BL_CLOSEACCOUNT", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", Request["ide"]);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "0");
                    Cmd.Parameters.AddWithValue("@COLUMN04", IsActive);
                    Cmd.Parameters.AddWithValue("@COLUMN05", FinancialYear);
                    Cmd.Parameters.AddWithValue("@COLUMN06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN07", FYtxt);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TABLENAME", "FITABLE055");
                    Cmd.Parameters.Add("@OUTPARAM", SqlDbType.NVarChar, 250).Direction = ParameterDirection.Output;
                    cnc.Open();
                    int r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToString(Cmd.Parameters["@OUTPARAM"].Value);
                }
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("USP_FIN_BL_CLOSEACCOUNT", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", Convert.ToString(itemdata.Tables[0].Rows[i][10]));
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Convert.ToString(itemdata.Tables[0].Rows[i][0]));
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Convert.ToString(itemdata.Tables[0].Rows[i][1]));
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Convert.ToString(itemdata.Tables[0].Rows[i][3]));
                        Cmdl.Parameters.AddWithValue("@COLUMN06", Convert.ToString(itemdata.Tables[0].Rows[i][4]));
                        Cmdl.Parameters.AddWithValue("@COLUMN07", Convert.ToString(itemdata.Tables[0].Rows[i][5]));
                        Cmdl.Parameters.AddWithValue("@COLUMN08", Convert.ToString(itemdata.Tables[0].Rows[i][6]));
                        Cmdl.Parameters.AddWithValue("@COLUMN09", Convert.ToString(itemdata.Tables[0].Rows[i][7]));
                        Cmdl.Parameters.AddWithValue("@COLUMN10", Convert.ToString(itemdata.Tables[0].Rows[i][8]));
                        Cmdl.Parameters.AddWithValue("@COLUMN11", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN12", Convert.ToString(itemdata.Tables[0].Rows[i][2]));
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Convert.ToString(itemdata.Tables[0].Rows[i][9]));
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", Convert.ToString(itemdata.Tables[0].Rows[i][2]));
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TABLENAME", "FITABLE058");
                        cnc.Open();
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                if (l > 0)
                {
                    var msg = string.Empty;
                    msg = "Close Account Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SaveCloseFinancialYear(string FinancialYear, string Date, string YearStartDate, string YearEndDate, string IsStockClosed, string IsAccountClosed, string LockFinancialYear, string insert)
        {
            try
            {
                string DateT = DateTime.Now.ToString();
                YearStartDate = DateTime.ParseExact(YearStartDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                YearEndDate = DateTime.ParseExact(YearEndDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                Date = DateTime.ParseExact(Date, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                //string insert = "Insert";
                int l = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("USP_FIN_FITABLE056", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", Request["ide"]);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "0");
                    Cmd.Parameters.AddWithValue("@COLUMN04", FinancialYear);
                    Cmd.Parameters.AddWithValue("@COLUMN05", YearStartDate);
                    Cmd.Parameters.AddWithValue("@COLUMN06", YearEndDate);
                    Cmd.Parameters.AddWithValue("@COLUMN07", IsStockClosed);
                    Cmd.Parameters.AddWithValue("@COLUMN08", IsAccountClosed);
                    Cmd.Parameters.AddWithValue("@COLUMN09", LockFinancialYear);
                    Cmd.Parameters.AddWithValue("@COLUMN10", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    cnc.Open();
                    l = Cmd.ExecuteNonQuery();
                }
                if (l > 0)
                {
                    var msg = string.Empty;
                    msg = "Close Financial Year Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SaveSettingFiscalYear(string FinancialYearN, string Date, string FinancialYear, string YearStartDate, string YearEndDate, string IsActive, string insert, string overide)
        {
            try
            {
                Session["id"] = "0";
                string DateT = DateTime.Now.ToString();
                YearStartDate = DateTime.ParseExact(YearStartDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                YearEndDate = DateTime.ParseExact(YearEndDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                Date = DateTime.ParseExact(Date, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                //string insert = "Insert";
                int l = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("USP_FIN_FITABLE048", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", Request["ide"]);
                    Cmd.Parameters.AddWithValue("@COLUMN03", FinancialYearN);
                    Cmd.Parameters.AddWithValue("@COLUMN04", YearStartDate);
                    Cmd.Parameters.AddWithValue("@COLUMN05", YearEndDate);
                    Cmd.Parameters.AddWithValue("@COLUMN06", FinancialYear);
                    Cmd.Parameters.AddWithValue("@COLUMN07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN08", IsActive);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", DateT);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@overide", overide);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    cnc.Open();
                    l = Cmd.ExecuteNonQuery();
                }
                Session["FYLN"] = FinancialYearN;
                if (l > 0)
                {
                    var msg = string.Empty;
                    msg = "Setting Financial Year Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult SFYSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["SFYGridData"] = ds;
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult GetSFY(string id)
        {
            try
            {
                DateFormat();
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlDataAdapter da = new SqlDataAdapter("SELECT COLUMN07,COLUMN08 FROM FITABLE053 WHERE COLUMN02=" + id + " AND COLUMNA03=" + Session["AcOwner"] + " AND ISNULL(COLUMNA13,0)=0", cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                var sdt = DateTime.ParseExact(Convert.ToDateTime(dt.Rows[0][0]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                var edt = DateTime.ParseExact(Convert.ToDateTime(dt.Rows[0][1]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                return this.Json(new { sdt = sdt, edt = edt }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public DateTime startDATE = DateTime.Now, endDate = DateTime.Now;
        public string fYear = "";
        public string fYearS = "";
        public string fYearA = "";
        public ActionResult FinancialYearChecking()
        {
            try
            {
                //USP_FIN_FINANCIALYEARCHECKINGDateFormat();
                var sdt = "";
                var edt = "";
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlCommand cmd = new SqlCommand("USP_FIN_FINANCIALYEARCHECKING", cn);
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                SqlDataAdapter daS = new SqlDataAdapter(cmd);
                DataTable dtS = new DataTable();
                daS.Fill(dtS);
                if (dtS.Rows.Count > 0)
                {
                    startDATE = Convert.ToDateTime(dtS.Rows[0][0]);
                    endDate = Convert.ToDateTime(dtS.Rows[0][1]);
                    sdt = DateTime.ParseExact(Convert.ToDateTime(dtS.Rows[0][0]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                    edt = DateTime.ParseExact(Convert.ToDateTime(dtS.Rows[0][1]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                    fYear = Convert.ToString(dtS.Rows[0][2]);
                    fYearS = Convert.ToString(dtS.Rows[0][3]);
                    fYearA = Convert.ToString(dtS.Rows[0][4]);
                }
                Session["FYLN"] = dtS.Rows[0][2];
                return this.Json(new { sdt = sdt, edt = edt }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GetDate(string date, string fyID)
        {
            try
            {
                //USP_FIN_FINANCIALYEARCHECKINGDateFormat();
                var sdt = "";
                var edt = "";
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlCommand cmd = new SqlCommand("INFO_DATE", cn);
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@fid", Session["id"]);
                SqlDataAdapter daS = new SqlDataAdapter(cmd);
                DataTable dtS = new DataTable();
                daS.Fill(dtS);
                if (dtS.Rows.Count > 0)
                {
                    var currentTime = new DateTime();
                    DateTime ToDate = DateTime.Now, FromDate = new DateTime(ToDate.Year, ToDate.Month, 1);
                    if (ToDate < Convert.ToDateTime(dtS.Rows[0][1]))
                    {
                        sdt = DateTime.ParseExact(Convert.ToDateTime(FromDate).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                        edt = DateTime.ParseExact(Convert.ToDateTime(ToDate).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);

                        Session["FDate"] = sdt;
                        Session["TDate"] = edt;
                    }
                    else
                    {
                    sdt = DateTime.ParseExact(Convert.ToDateTime(dtS.Rows[0][0]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                    edt = DateTime.ParseExact(Convert.ToDateTime(dtS.Rows[0][1]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);

                    Session["FDate"] = edt;
                    Session["TDate"] = edt;
                    }

                    startDATE = Convert.ToDateTime(dtS.Rows[0][0]);
                    endDate = Convert.ToDateTime(dtS.Rows[0][1]);
                    sdt = DateTime.ParseExact(Convert.ToDateTime(dtS.Rows[0][0]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                    edt = DateTime.ParseExact(Convert.ToDateTime(dtS.Rows[0][1]).ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                }
                Session["FYLN"] = dtS.Rows[0][2];
                return this.Json(new { sdt = sdt, edt = edt }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CheckAccountsClose(int fyc)
        {
            try
            {
                var chkFY = 0;
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlCommand cmd = new SqlCommand("CHECK_FY_UPDATE", cn);
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ACOWNER", Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@FYID", fyc);
                SqlDataAdapter daS = new SqlDataAdapter(cmd);
                DataTable dtS = new DataTable();
                daS.Fill(dtS);
                if (dtS.Rows.Count > 0)
                {
                    chkFY = Convert.ToInt32(dtS.Rows[0][0]);
                }
                return this.Json(new { chkFY = chkFY }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
    }
}
