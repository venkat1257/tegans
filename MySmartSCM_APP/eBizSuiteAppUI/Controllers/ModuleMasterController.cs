﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Xml;
using WebMatrix.Data;
using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using eBizSuiteDAL.classes;
using System.Data.SqlClient;
using System.Configuration;
namespace eBizSuiteProduct.Controllers
{
    public class ModuleMasterController : Controller
    {

        private eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;

        string[] theader;

        string[] theadertext;

        public ActionResult Index(string ShowInactives)
        {
            try
            {
                Session["FormIDACCDN"]=Request.QueryString["idi"];
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/Modulemaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/Modulemaster/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:177px;font-size:10px;''  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:177px;font-size:10px;'' />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;

                ViewBag.columns = theader;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                var query1 = "Select * from CONTABLE001  where COLUMNA13='False'";
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                return View("Index", db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                return Content("Form Editing Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult CustomForm()
        {
            try
            {

                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var ttid = tid.COLUMN02;
                List<CONTABLE006> all = new List<CONTABLE006>();
                var alll = db.CONTABLE006.Where(a => a.COLUMN04 == ttid && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                all = alll.Where(a => a.COLUMN06 != null).ToList();
                var Tab = all.Where(a => a.COLUMN11 == "Tab" && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                var Ta = Tab.Select(a => a.COLUMN12).Distinct().ToList();
                var type = all.Select(b => b.COLUMN11).Distinct();
                var sn = all.Select(b => b.COLUMN12).Distinct();
                var Section = all.Where(b => b.COLUMN11 != "Tab" && b.COLUMN11 != "Item Level" && b.COLUMN06 != null && b.COLUMN06 != "").ToList();
                var sec = Section.Select(b => b.COLUMN12).Distinct();
                //var roles = db.CONTABLE012.Where(a => a.COLUMN04 != null).ToList();
                //ViewBag.Roles = roles;
                ViewBag.Type = type;
                ViewBag.Tabs = sn;
                ViewBag.TabMaster = Ta;
                ViewBag.Section = sec;
                return View(all);
            }
            catch (Exception ex)
            {
                return Content("Form Editing Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public void Normal()
        {

            List<WebGridColumn> cols = new List<WebGridColumn>();
            var col2 = "COLUMN02";
            cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN02",
                Header = "Action",
                Format = (item) => new HtmlString("<a href=/Modulemaster/Edit/" + item[col2] + " >Edit</a>|<a href=/Modulemaster/Details/" + item[col2] + " >View</a>")
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN02",
                Header = "Module ID"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN03",
                Header = "Name"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN04",
                Header = "Description"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN05",
                Header = "Subsidary"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN06",
                Header = "Operating Unit"
            });
            ViewBag.cols = cols;

        }

        [HttpGet]
        public ActionResult Sort(string ShowInactives)
        {
            try
            {

                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;

                return View("Index", data);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult Details(int id = 0)
        {
            try
            {

                CONTABLE001 CONTABLE001 = db.CONTABLE001.Find(id);
                ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA13 == false).ToList();
                var viewlistou = db.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                ViewBag.ou = viewlistou;
                if (CONTABLE001 == null)
                {
                    return HttpNotFound();
                }
                return View(CONTABLE001);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult Create()
        {
            CONTABLE001 all = new CONTABLE001();
            var col2 = db.CONTABLE001.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
            all.COLUMN02 = (col2.COLUMN02 + 1);
            var ac = Convert.ToInt32(Session["AcOwner"]);
            ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA13 == false).ToList();
            var viewlistou = db.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
            ViewBag.ou = viewlistou;
            return View(all);
        }

        [HttpPost]
        public ActionResult Create(CONTABLE001 CONTABLE001, string Create, string Save)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    CONTABLE001.COLUMNA13 = false;
                    CONTABLE001.COLUMNA12 = true;
                    CONTABLE001.COLUMNA06 = DateTime.Now;
                    CONTABLE001.COLUMNA07 = DateTime.Now;
                    db.CONTABLE001.Add(CONTABLE001);
                    db.SaveChanges();
                    
                }
                if (Create != null)
                {
                    var msg = string.Empty;
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1236 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Module master Successfully Created.......... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    return RedirectToAction("Index");
                }
                else
                    return RedirectToAction("Create");

            }
             catch (Exception ex)
             {
                 var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1236 && q.COLUMN05 == 0).FirstOrDefault();
                 var msg = string.Empty;
                 if (msgMaster != null)
                 {
                     msg = msgMaster.COLUMN03;
                 }
                 else
                 {
                     msg = "Module master Creation Failed .........";
                 }
                 Session["MessageFrom"] = msg;
                 Session["SuccessMessageFrom"] = "fail";
                 return View();
             } 
                return View(CONTABLE001);
         
            //catch (Exception ex)
            //{
            //    return Content("Form Creation Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            //}
        }

        public ActionResult Edit(int id = 0)
        {
            CONTABLE001 CONTABLE001 = db.CONTABLE001.Find(id);
            var viewlistou = db.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
            ViewBag.ou = viewlistou;
            ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA13 == false).ToList();

            if (CONTABLE001 == null)
            {
                return HttpNotFound();
            }
            return View(CONTABLE001);
        }

        [HttpPost]
        public ActionResult Edit(CONTABLE001 CONTABLE001)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    CONTABLE001.COLUMNA07 = DateTime.Now;

                    CONTABLE001.COLUMNA12 = true;

                    CONTABLE001.COLUMNA13 = false;
                   
                    db.Entry(CONTABLE001).State = EntityState.Modified;
                    db.SaveChanges();
                    var msg = string.Empty;
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1236 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Module master Successfully Modified.......... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    return RedirectToAction("Index");
                }

                return View(CONTABLE001);
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1236 && q.COLUMN05 == 3).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Module master Modification Failed .........";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                return View();
            }

        }

        public ActionResult Delete(int id = 0)
        {
            try
            {
                CONTABLE001 contable007 = db.CONTABLE001.Find(id);

                var y = (from x in db.CONTABLE001 where x.COLUMN02 == id select x).First();
                y.COLUMNA12 = false;
                y.COLUMNA13 = true;
                //db.CONTABLE001.Remove(y);
                db.SaveChanges();
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1236 && q.COLUMN05 == 4).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Module Master Successfully Deleted.......... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
              
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1236 && q.COLUMN05 == 5).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Module Master Deletion Failed .........";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                return View();
            }

        }

        public void ExportCSV()
        {

            StringWriter sw = new StringWriter();

            var y = db.CONTABLE001.OrderBy(q => q.COLUMN02).ToList();

            sw.WriteLine("\"Module ID\",\"Name\",\"Description\",\"Subsidary\",\"Operating Unit\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
            Response.ContentType = "text/csv";

            foreach (var line in y)
            {

                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"",
                                           line.COLUMN02,
                                           line.COLUMN03,
                                           line.COLUMN04,
                                           line.COLUMN05,
                                           line.COLUMN06
                                           ));

            }

            Response.Write(sw.ToString());

            Response.End();


        }

        public void ExportPdf()
        {
            List<CONTABLE001> all = new List<CONTABLE001>();
            all = db.CONTABLE001.ToList();
            var data = from e in db.CONTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public void Exportword()
        {
            List<CONTABLE001> all = new List<CONTABLE001>();
            all = db.CONTABLE001.ToList();
            var data = from e in db.CONTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
            Response.ContentType = "application/vnd.ms-word ";
            Response.Charset = string.Empty;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public void ExportExcel()
        {
            List<CONTABLE001> all = new List<CONTABLE001>();
            all = db.CONTABLE001.ToList();
            var data = from e in db.CONTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            gv.FooterRow.Visible = false;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter ht = new HtmlTextWriter(sw);
            gv.RenderControl(ht);
            Response.Write(sw.ToString());
            Response.End();
            gv.FooterRow.Visible = true;

        }

        public ActionResult Export(string items)
        {
            List<CONTABLE001> all = new List<CONTABLE001>();
            all = db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
            var ttid = tid.COLUMN02;
            var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (items == "PDF")
            {
                ExportPdf();
            }
            else if (items == "Excel")
            {
                ExportExcel();
            }
            else if (items == "Word")
            {
                Exportword();
            }
            else if (items == "CSV")
            {
                ExportCSV();
            }
            return View("Index", db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList());
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();

                var ModuleID = Request["ModuleID"];
                var Name = Request["Name"];




                if (Name != null && Name != "" && Name != string.Empty)
                {
                    Name = Name + "%";
                }
                else
                    Name = Request["Name"];






                if (ModuleID != null && ModuleID != "" && ModuleID != string.Empty)
                {
                    ModuleID = ModuleID + "%";
                }
                else
                    ModuleID = Request["ModuleID"];

                var query = "SELECT * FROM CONTABLE001  WHERE COLUMN03 like '" + Name + "' or  or COLUMN02 LIKE '" + ModuleID + "'";

                var query1 = db.CONTABLE001.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.cols = Session["cols"];
                ViewBag.Inlinecols = Session["Inlinecols"];
                ViewBag.gdata = gdata;

                return View("Index", gdata);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult UpdateInline(string list)
        {
            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            string qStr = null;
            var tblID = db.CONTABLE004.Where(q => q.COLUMN04 == "CONTABLE001").Select(q => new { q.COLUMN02 });
            var ID = tblID.Select(a => a.COLUMN02).First();
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
            {
                for (int x = 0; x < ds.Tables[0].Columns.Count; x++)
                {
                    var colname = ds.Tables[0].Columns[x].ColumnName;
                    var items = db.CONTABLE005.Where(q => q.COLUMN03 == ID && q.COLUMN04 == colname).FirstOrDefault();
                    if (items.COLUMN04 == ds.Tables[0].Columns[x].ColumnName)
                    {
                        if (qStr == null)
                        {
                            qStr += "UPDATE CONTABLE001 SET " + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "'";
                        }
                        else if (x == ds.Tables[0].Columns.Count - 1)
                        {
                            qStr += "," + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "' WHERE COLUMN02=" + ds.Tables[0].Rows[j]["COLUMN02"] + "";
                        }
                        else
                        {
                            qStr += "," + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "'";
                        }
                    }
                }
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand(qStr, cn);
                cn.Open();
                cmd1.ExecuteNonQuery();
                cn.Close();
                qStr = null;
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
            ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
            if (ShowInactives != null)
            {
                var all = db.CONTABLE001.ToList();
                //return View(all);
                return View("Index", db.CONTABLE001.ToList());
            }
            if (Session["Data"] != null)
            {
                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;
                return View("Index", data);

            }
            return View("Index", db.CONTABLE001.Where(a => a.COLUMNA12 == true).ToList());
        }

        string tthdata, tthtext;

        string itthdata, itthtext;

        public ActionResult View1(string items, string sort, string inactive, string style)
        {
            List<CONTABLE001> all = new List<CONTABLE001>();
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
            var ttid = tid.COLUMN02;
            var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (style == "Normal")
            {
                all = all.Where(a => a.COLUMNA12 == true).ToList();
                var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var Attid = Atid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var indata = db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE001 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE001 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();

                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th class=webgrid-header>Action</th>";
                        tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                    }
                }
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td class=webgrid-row-style><a href=/Modulemaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/Modulemaster/Details/" + itm[Inline] + " >View</a></td>";
                            itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px;font-size:10px;  /></td>";
                            tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                        }
                        else
                        {
                            itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:80px;font-size:10px;  /></td>";
                            tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                        }
                    }
                    tthdata = "<tr>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                ViewBag.columns = theader;
                var htmlString = "<table id=tbldata style=width:100%><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata style=width:100%><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                return View("Index", db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

            }
            else
            {
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                if (sort == "Recently Created")
                {
                    all = db.CONTABLE001.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    all = db.CONTABLE001.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                else
                {
                    all = db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList();

                }
                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var Attid = Atid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/Modulemaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/Modulemaster/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Index", db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList());
        }






        public ActionResult ListView()
        {
            return View(db.CONTABLE001.ToList());
        }

        public ActionResult New()
        {
            return View();
        }







        public ActionResult Style(string items, string inactive)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            CONTABLE001 lv = new CONTABLE001();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
            var tbtid = tbid.COLUMN02;
            var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
            ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
            var data = from e in db.CONTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
            List<CONTABLE001> tbldata = new List<CONTABLE001>();
            tbldata = db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList();
            if (items == "Normal")
            {
                var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
                var Attid = Atid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == Attid).ToList();
                var indata = db.CONTABLE001.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE001 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE001 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th class=webgrid-header>Action</th>";
                        tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                    }
                }
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td class=webgrid-row-style><a href=/Modulemaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/Modulemaster/Details/" + itm[Inline] + " >Details</a></td>";
                            tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   '' /></td>";
                        }
                        else
                        {
                            tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    ''  /></td>";
                        }


                    }
                    tthdata = "<tr>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                ViewBag.columns = theader;
                var htmlString = "<table id=tbldata style=width:100%><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata style=width:100%><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
            }
            return View("Index", db.CONTABLE001.Where(a => a.COLUMNA13 == false).ToList());
        }





        public ActionResult CustomView()
        {
            List<CONTABLE005> all = new List<CONTABLE005>();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            //all = from e in db.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
            var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
            all = db.CONTABLE005.SqlQuery(query).ToList();

            return View(all);
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {


            int col2 = 0;
            var tbldata = db.CONTABLE004.Where(a => a.COLUMN05 == "tbl_subsidary").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = db.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = db.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    db.CONTABLE013.Add(dd);
                    db.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        db.CONTABLE013.Add(dd);
                        db.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);

        }






        public ActionResult NewField()
        {

            //var table2= Convert.ToString();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
            var ttid = Convert.ToInt32(tid.COLUMN02);
            var SecNames = db.CONTABLE006.Where(a => a.COLUMN12 != null && a.COLUMN04 == ttid).ToList();
            var dsecnames = SecNames.Select(a => a.COLUMN12).Distinct();
            ViewBag.SecNames = dsecnames;
            return View();
        }

        [HttpPost]
        public ActionResult NewField(FormCollection fc)
        {
            CONTABLE006 fm = new CONTABLE006();

            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE001").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            var query = "SELECT  top( 1 ) * FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04=COLUMN05  ";
            var data = db.CONTABLE005.SqlQuery(query).ToList();
            var tblid = data.Select(a => a.COLUMN03).FirstOrDefault();
            var colname = data.Select(a => a.COLUMN04).FirstOrDefault();
            var alicolname = data.Select(a => a.COLUMN05).FirstOrDefault();
            var con6data = db.CONTABLE006.Where(a => a.COLUMN03 == ttid).FirstOrDefault();
            fm.COLUMN03 = con6data.COLUMN03;
            fm.COLUMN04 = Convert.ToInt32(tblid);
            fm.COLUMN05 = Convert.ToString(colname);
            fm.COLUMN06 = Request["Label_Name"];
            fm.COLUMN07 = "Y";
            fm.COLUMN08 = Request["Mandatory"];
            fm.COLUMN10 = Request["Control_Type"];
            fm.COLUMN11 = Request["Section_Type"];
            fm.COLUMN12 = Request["Section_Name"];
            var pr = con6data.COLUMN13;
            fm.COLUMN13 = (pr + 1).ToString();
            db.CONTABLE006.Add(fm);
            db.SaveChanges();
            return View("Index");
        }






        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}