﻿using eBizSuiteAppModel.Table;
using eBizSuiteAppUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;
using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Data.SqlClient;

namespace MeBizSuiteAppUI.Controllers
{
    public class PaymentTermsController : Controller
    {
        //
        // GET: /PaymentTerms/
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        CommonController cmn = new CommonController();
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";



        public ActionResult Info()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Terms],p.[COLUMN05 ] AS [Standard], p.[COLUMN06] AS [Net Due],p.[COLUMN07] AS [Discount Amount],p.[COLUMN08] AS [Discount Paid], p.[COLUMN09] AS [Active Flag]  From MATABLE022 p   where isnull(p.COLUMNA13,'False')='False' and " + OPUnit + " and p.COLUMNA03=" + acID + " or p.COLUMNA03 is null order by p.[COLUMN02 ] desc  ";
                alCol.AddRange(new List<string> { "ID", "Terms", "Standard", "Net Due", "Discount Amount", "Discount Paid", "Active Flag" });
                actCol.AddRange(new List<string> { "COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "p.COLUMN07", "p.COLUMN08", "p.COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                Session["FormName"] = "Payment Terms";
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult Create()
        {
            try
            {
                LOVDetailsModel obj = new LOVDetailsModel();
                MATABLE022 all = new MATABLE022();

                return View("Create", all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult Create(MATABLE022 collection, string Save, string SaveNew)
        {
            try
            {
                //MATABLE022 lovDetails = new MATABLE022();
                //EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
                List<MATABLE022> objList = act.MATABLE022.OrderBy(s => s.COLUMN02).ToList();
                if (objList.Count > 0)
                {
                    collection.COLUMN02 = objList[objList.Count - 1].COLUMN02 + 1;
                }
                else
                    collection.COLUMN02 = 10000;

                collection.COLUMNA01 = null;
                var ac = Convert.ToInt32(Session["AcOwner"]);

                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    collection.COLUMNA02 = null;
                else
                    collection.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);

                collection.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                if (Session["LogedEmployeeID"] != null)
                    collection.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                else
                    collection.COLUMNA04 = null;
                collection.COLUMNA05 = null;
                collection.COLUMNA06 = DateTime.Now;
                collection.COLUMNA07 = null;
                collection.COLUMNA09 = DateTime.Now;
                collection.COLUMNA10 = null;
                collection.COLUMNA11 = null;
                collection.COLUMNA12 = true;
                collection.COLUMNA13 = false;
                act.MATABLE022.Add(collection);
                act.SaveChanges();
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1580 && q.COLUMN05 == 1).FirstOrDefault();
                Session["FormName"] = "Payment Terms";
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Payment Terms Successfully Created... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1580", "110014051", "Update"); }
                if (Save != null)
                {
                    return RedirectToAction("Info");
                }
                return RedirectToAction("Info");
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1580 && q.COLUMN05 == 0).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Payment Terms  Created Failed .........";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }


        }


        [HttpGet]
        public ActionResult Edit(int ide)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                MATABLE022 MATABLE022 = act.MATABLE022.Find(ide);
                Session["editid"] = ide;
                MATABLE022 edit = new MATABLE022();
                var data = activity.MATABLE022.Find(ide);
                edit.COLUMN04 = data.COLUMN04;
                edit.COLUMN05 = data.COLUMN05;
                edit.COLUMN06 = data.COLUMN06;
                edit.COLUMN07 = data.COLUMN07;
                edit.COLUMN08 = data.COLUMN08;
                edit.COLUMN09 = data.COLUMN09;
                //var col2 = edit.COLUMN02;
                //Session["time"] = activity.MATABLE022.Find(col2).COLUMNA06;
                Session["FormName"] = "Payment Terms";
                return View("Edit", edit);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        [HttpPost]
        public ActionResult Edit(MATABLE022 Info)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int editid = Convert.ToInt32(Session["editid"]);
                    Session["editid"] = editid;
                    var edit = act.MATABLE022.Where(a => a.COLUMN02 == editid).FirstOrDefault();
                    edit.COLUMNA07 = DateTime.Now;
                    //Info.COLUMNA06 = Convert.ToDateTime(Session["time"]);
                    edit.COLUMNA12 = true;
                    edit.COLUMNA13 = false;
                    edit.COLUMN03 = Info.COLUMN03;
                    edit.COLUMN04 = Info.COLUMN04;
                    edit.COLUMN05 = Info.COLUMN05;
                    edit.COLUMN06 = Info.COLUMN06;
                    edit.COLUMN07 = Info.COLUMN07;
                    edit.COLUMN08 = Info.COLUMN08;
                    edit.COLUMN09 = Info.COLUMN09;

                    act.Entry(edit).State = EntityState.Modified;
                    act.SaveChanges();
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1580 && q.COLUMN05 == 2).FirstOrDefault();
                    Session["FormName"] = "Payment Terms";
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "PaymentTerms Successfully Updated... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    if (Convert.ToString(Session["RediesCache"]) == "1")
                    { int d = cmn.UpdateRedisCacheStatus("1580", "110014051", "Update"); }
                    return RedirectToAction("Info", "PaymentTerms");
                }
                return View(Info);
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1580 && q.COLUMN05 == 3).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "PaymentTerms Update Failed... ";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        [HttpGet]
        public ActionResult View(int ide)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                Session["editid"] = ide;
                MATABLE022 edit = new MATABLE022();
                var data = activity.MATABLE022.Find(ide);
                edit.COLUMN04 = data.COLUMN04;
                edit.COLUMN05 = data.COLUMN05;
                edit.COLUMN06 = data.COLUMN06;
                edit.COLUMN07 = data.COLUMN07;
                edit.COLUMN08 = data.COLUMN08;
                edit.COLUMN09 = data.COLUMN09;
                //var col2 = edit.COLUMN02;
                //Session["time"] = activity.MATABLE022.Find(col2).COLUMNA06;
                Session["FormName"] = "Payment Terms";
                return View("View", edit);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }



        public ActionResult Delete(MATABLE022 Info)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    int editid = Convert.ToInt32(Session["editid"]);
                    var edit = act.MATABLE022.Where(a => a.COLUMN02 == editid).FirstOrDefault();
                    edit.COLUMNA07 = DateTime.Now;
                    //Info.COLUMNA06 = Convert.ToDateTime(Session["time"]);
                    edit.COLUMNA12 = false;
                    edit.COLUMNA13 = true;

                    act.Entry(edit).State = EntityState.Modified;
                    act.SaveChanges();
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1580 && q.COLUMN05 == 4).FirstOrDefault();
                    Session["FormName"] = "Payment Terms";
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "PaymentTerms Successfully Deleted... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    if (Convert.ToString(Session["RediesCache"]) == "1")
                    { int d = cmn.UpdateRedisCacheStatus("1580", "110014051", "Update"); }
                    return RedirectToAction("Info", "PaymentTerms");
                }
                return View(Info);
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1580 && q.COLUMN05 == 3).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "PaymentTerms Update Failed... ";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult GetPT(int PT)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();

                MATABLE022 edit = new MATABLE022();
                var data = activity.MATABLE022.Find(PT);
                var PTv = data.COLUMN06;
                return Json(new { PT = PTv });

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "PaymentTerms", new { FormName = Session["FormName"] });
            }
        }
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        public ActionResult PTNameCheck(string PT)
        {
            try
            {
                string PTCheck1 = "";
                if (PT == "")
                    PT = "NULL";


                if (PT != "NULL")
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd = new SqlCommand("select count(column01) from MATABLE022 where column04='" + PT + "' and  ( COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='false'", cn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    cn.Close();

                    if (dt1.Rows.Count > 0)
                    {
                        if (PT != null)
                            PTCheck1 = dt1.Rows[0][0].ToString();

                    }
                }
                else
                    PTCheck1 = "0";

                return Json(new { PTCheck = PTCheck1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "PaymentTerms", new { FormName = Session["FormName"] });
            }
        }

    }
}
