﻿using eBizSuiteDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.Data;
using System.Web.UI;
using System.Net;
using eBizSuiteAppModel.Table;
using System.Configuration;

namespace eBizSuiteProduct.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        public ActionResult Index()
        {
            return View();
        }

        // GET: /Account/AdminRegister

        public ActionResult AdminRegister()
        {
            return View();
        }

        //POST

        [HttpPost]
        public ActionResult AdminRegister(tbl_AdminRegistration register)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    RegisterManage userManager = new RegisterManage();
                    if (!userManager.IsUserLoginIDExist(register.UserID))
                    {

                        userManager.Add(register);

                        FormsAuthentication.SetAuthCookie(register.UserName, false);
                        return RedirectToAction("LogOn", "Admin");
                        //return RedirectToAction("Purchase", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "LogID already taken");
                    }
                }
            }
            catch
            {
                return View(register);
            }

            return View(register);
        }

        public ActionResult Login()
        {
            Session["UserName"] = null;
            return View();
        }
        //POST
        [HttpPost]
        public ActionResult Login(tbl_EmployeeMaster regUser)
        {
            System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
            string browser = browse.Browser;
            Session["browser"] = browser;

            string strHost = "";
            strHost = System.Net.Dns.GetHostName();
            IPHostEntry ipAdd = System.Net.Dns.GetHostEntry(strHost);
            IPAddress[] addr = ipAdd.AddressList;
            string IPAddress = addr[addr.Length - 2].ToString();


            if (ModelState.IsValid)
            {
                eBizSuiteTableEntities dbUser = new eBizSuiteTableEntities();
                eBizSuiteTableEntities dbUserSA = new eBizSuiteTableEntities();



                var v = dbUser.tbl_EmployeeMaster.Where(a => a.Email.Equals(regUser.Email) && a.Password.Equals(regUser.Password)).FirstOrDefault();
                if (v != null)
                {
                    ViewBag.UserID = Session["LogedEmployeeID"] = v.EmployeeID.ToString();
                    Session["LogedeMailId"] = v.Email.ToString();

                    int empID = Convert.ToInt32(Session["LogedEmployeeID"]);




                    //string sr=null;

                    var db = Database.Open("sqlcon");
                    var query = "select EmployeeID from tbl_EmployeeMaster where Email='" + v.Email.ToString() + "'";
                    Session["id"] = db.Query(query).ToString();
                    Session["UserName"] = v.EmployeeName;
                    var chk = dbUserSA.tbl_AdminSecurityAnswer.Where(q => q.EmployeeID == v.EmployeeID).FirstOrDefault();



                    if (chk == null)
                    {


                        return RedirectToAction("AdminSecurityQuestion", "Admin");
                    }
                    else
                    {
                        eBizSuiteTableEntities dbe = new eBizSuiteTableEntities();
                        tbl_AdminLoginInformation sysLogInfo = new tbl_AdminLoginInformation();

                        var chk1 = dbe.tbl_AdminLoginInformation.Where(a => a.IPAddress == IPAddress && a.Browser == browser && a.UserID == v.EmployeeID).FirstOrDefault();
                        // var chk2 = dbe.tbl_AdminLoginInformation.Where(a => a.Browser == browser).FirstOrDefault();


                        //checking && chk2!=null

                        if (chk1 != null)
                        {
                            return RedirectToAction("EmployeeMasterDetailes", "EmployeeMaster");
                        }
                        else
                        {
                            eBizSuiteTableEntities obj = new eBizSuiteTableEntities();
                            ViewBag.a = obj.tbl_AdminSecurityQuestion.Select(a => a.Question);


                            Session["error01"] = "Please Answer Security Question !";
                            // return View("SecurityAnswerCheck");

                            return RedirectToAction("SecurityAnswerCheck", "Admin");

                        }

                    }



                }

                else
                {
                    ViewBag.error = "The email or password you entered is incorrect.";
                }

            }
            return View(regUser);



        }

        // GET: /Admin/AdminLogOn

        public ActionResult LogOn()
        {
            return View("LogOn");
        }

        //POST
        [HttpPost]
        public ActionResult LogOn(tbl_AdminLogOn regUser)
        {
            if (ModelState.IsValid)
            {
                eBizSuiteTableEntities dbUser = new eBizSuiteTableEntities();

                var v = dbUser.tbl_AdminLogOn.Where(a => a.UserName.Equals(regUser.UserName) && a.Password.Equals(regUser.Password)).FirstOrDefault();
                if (v != null)
                {
                    ViewBag.UserID = Session["LogedUserID"] = v.UserID.ToString();
                    Session["LogedUserFullName"] = v.UserName.ToString();
                    return RedirectToAction("AdminSecurityQuestion", "Admin");
                }

            }
            return View(regUser);

        }


        //GET AdminSecurityQuestion
        public ActionResult AdminSecurityQuestion()
        {
            eBizSuiteTableEntities obj = new eBizSuiteTableEntities();
            ViewBag.a = obj.tbl_AdminSecurityQuestion.Select(a => a.Question);
            ViewBag.UserID = Convert.ToInt32(Session["LogedUserID"]);
            return View("AdminSecurityQuestion");

        }

        [HttpPost]
        public ActionResult AdminSecurityQuestion(tbl_AdminSecurityAnswer user)
        {
            eBizSuiteTableEntities dbe = new eBizSuiteTableEntities();


            try
            {

                if (ModelState.IsValid)
                {
                    int UserID = ViewBag.UserID = Convert.ToInt32(Session["LogedEmployeeID"]);

                    tbl_AdminLoginInformation sysLogInfo = new tbl_AdminLoginInformation();

                    sysLogInfo.UserID = Convert.ToInt32(UserID);

                    sysLogInfo.Date = DateTime.Now.Date;

                    sysLogInfo.Time = DateTime.Now.TimeOfDay;

                    string strHost = "";
                    strHost = System.Net.Dns.GetHostName();
                    IPHostEntry ipAdd = System.Net.Dns.GetHostEntry(strHost);
                    IPAddress[] addr = ipAdd.AddressList;
                    sysLogInfo.IPAddress = addr[addr.Length - 2].ToString();

                    System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
                    string browser = browse.Browser;

                    sysLogInfo.Browser = browser;

                    sysLogInfo.EmailID = Session["LogedeMailId"].ToString();
                    sysLogInfo.Location = "India";
                    dbe.tbl_AdminLoginInformation.Add(sysLogInfo);
                    dbe.SaveChanges();


                    AdminSesurityAnswerManage userManager = new AdminSesurityAnswerManage();
                    if (!userManager.IsUserLoginIDExist(UserID))
                    {
                        //userManager.Add(user, UserID);
                        FormsAuthentication.SetAuthCookie(user.SeqQue1, false);
                        //chage path to code
                        return RedirectToAction("EmployeeMasterDetailes", "EmployeeMaster");

                    }
                    else
                    {
                        ModelState.AddModelError("", "LogID already taken");
                    }
                }
                else
                {

                    eBizSuiteTableEntities obj1 = new eBizSuiteTableEntities();
                    ViewBag.a = obj1.tbl_AdminSecurityQuestion.Select(a => a.Question);
                    ViewBag.UserID = Convert.ToInt32(Session["LogedUserID"]);
                    return View("AdminSecurityQuestion");
                }
            }
            catch
            {
                return View(user);
            }

            return View(user);
        }

        //get Security Answer Check

        public ActionResult SecurityAnswerCheck()
        {
            eBizSuiteTableEntities obj = new eBizSuiteTableEntities();
            ViewBag.a = obj.tbl_AdminSecurityQuestion.Select(a => a.Question);
            ViewBag.errorLogin = Session["error01"];
            return View("SecurityAnswerCheck");
        }

        [HttpPost]
        public ActionResult SecurityAnswerCheck(tbl_AdminSecurityAnswer user)
        {
            int id = Convert.ToInt32(Session["LogedEmployeeID"]);

            eBizSuiteTableEntities dbUserSA = new eBizSuiteTableEntities();
            var v = dbUserSA.tbl_AdminSecurityAnswer.Where(a => a.SeqQue1.Equals(user.SeqQue1) && a.SecAns1.Equals(user.SecAns1) && a.EmployeeID.Equals(id)).FirstOrDefault();
            if (v != null)
            {
                return RedirectToAction("EmployeeMasterDetailes", "EmployeeMaster");
            }
            else
            {
                eBizSuiteTableEntities obj1 = new eBizSuiteTableEntities();
                ViewBag.a = obj1.tbl_AdminSecurityQuestion.Select(a => a.Question);
                ViewBag.UserID = Convert.ToInt32(Session["LogedUserID"]);
                ViewBag.errorCheck = "You are Not Authorized!";
                return View("SecurityAnswerCheck");
            }
            //return View(user);


        }

    }
}
