﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeBizSuiteAppUI.Controllers
{
    public class AutoFieldConfigController : Controller
    {
        //
        // GET: /AutoFieldConfig/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Create()
        {
		//EMPHCS1207  rajasekhar reddy patakota 25/09/2015 Auto Feild Configuration Setup For Location and adding success messages
            try
            {
                var custform = Request.Url.Query.ToString(); custform = custform.Replace("?", ""); custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int l = custform.IndexOf("&"); if (l > 0) custform = custform.Substring(0, l); custform = custform.Replace("&", "");
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == custform).FirstOrDefault();
                Session["FormName"] = fnamedata.COLUMN04;
                Session["id"] = fnamedata.COLUMN02;
                //EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
                SqlCommand cmd = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                SqlCommand cmd1 = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN26' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                //EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
                SqlCommand cmdl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1273 and s.COLUMN05 = 'COLUMN29' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                //EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
				SqlCommand cmdw = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1277 and s.COLUMN05 = 'COLUMN28' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter daw = new SqlDataAdapter(cmdw);
                DataTable dtw = new DataTable();
                daw.Fill(dtw);
				//EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
                SqlCommand cmdd = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1277 and s.COLUMN05 = 'COLUMN30' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);
				//EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
                SqlCommand cmdb = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1273 and s.COLUMN05 = 'COLUMN27' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                DataTable dtb = new DataTable();
                dab.Fill(dtb);
                ViewBag.projectval = false; ViewBag.uomval = false; ViewBag.locationval = false; ViewBag.weightval = false; ViewBag.discountval = false; ViewBag.lotval = false;
                if (dt.Rows.Count > 0) { if (dt.Rows[0][0].ToString() == "Y") ViewBag.projectval = true; }
                if (dt1.Rows.Count > 0) { if (dt1.Rows[0][0].ToString() == "Y") ViewBag.uomval = true; }
                if (dtl.Rows.Count > 0) { if (dtl.Rows[0][0].ToString() == "Y") ViewBag.locationval = true; }
				//EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
                if (dtw.Rows.Count > 0) { if (dtw.Rows[0][0].ToString() == "Y") ViewBag.weightval = true; }
				//EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
                if (dtd.Rows.Count > 0) { if (dtd.Rows[0][0].ToString() == "Y") ViewBag.discountval = true; }
				//EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
                if (dtb.Rows.Count > 0) { if (dtb.Rows[0][0].ToString() == "Y") ViewBag.lotval = true; }
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1387).FirstOrDefault().COLUMN04;
                return View();
            }
			//EMPHCS1207  rajasekhar reddy patakota 25/09/2015 Auto Feild Configuration Setup For Location and adding success messages
            catch (Exception ex)
            {
                Session["MessageFrom"] = "Failed Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpPost]
        public ActionResult Create(FormCollection fc)
        {
		//EMPHCS1207  rajasekhar reddy patakota 25/09/2015 Auto Feild Configuration Setup For Location and adding success messages
            try
            {
                var custform = Request.Url.Query.ToString(); custform = custform.Replace("?", ""); custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int l = custform.IndexOf("&"); if (l > 0) custform = custform.Substring(0, l); custform = custform.Replace("&", "");
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == custform).FirstOrDefault();
                Session["FormName"] = fnamedata.COLUMN04;
                Session["id"] = fnamedata.COLUMN02;
                var chkval = fc["Project"];
                var UOM = fc["UOM"];
                //EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
                var Location = fc["Location"];
				//EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
                var Weight = fc["Weight"];
				//EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
                var Discount = fc["Discount"];
				//EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
                var Lot = fc["Lot"];
                SqlCommand Cmd = new SqlCommand("AutoFieldConfig", cn);
                cn.Open();
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@chkval", chkval);
                Cmd.Parameters.AddWithValue("@UOM", UOM);
                Cmd.Parameters.AddWithValue("@Location", Location);
				//EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
                Cmd.Parameters.AddWithValue("@Weight", Weight);
				//EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
                Cmd.Parameters.AddWithValue("@Discount", Discount);
				//EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
                Cmd.Parameters.AddWithValue("@Lot", Lot);
                //EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
                Cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                Cmd.Parameters.AddWithValue("@OPUnit", Session["OPUnit1"]);
                int i = Cmd.ExecuteNonQuery();
                cn.Close();
                //EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
                SqlCommand cmd = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                SqlCommand cmd1 = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN26' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                //EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
                SqlCommand cmdl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1273 and s.COLUMN05 = 'COLUMN29' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
				//EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
                SqlCommand cmdw = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1277 and s.COLUMN05 = 'COLUMN28' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter daw = new SqlDataAdapter(cmdw);
                DataTable dtw = new DataTable();
                daw.Fill(dtw);
				//EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
                SqlCommand cmdd = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1277 and s.COLUMN05 = 'COLUMN30' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);
				//EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
                SqlCommand cmdb = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1273 and s.COLUMN05 = 'COLUMN27' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                DataTable dtb = new DataTable();
                dab.Fill(dtb);
                ViewBag.projectval = false; ViewBag.uomval = false; ViewBag.locationval = false; ViewBag.weightval = false; ViewBag.discountval = false; ViewBag.lotval = false;
                if (dt.Rows.Count > 0) { if (dt.Rows[0][0].ToString() == "Y") ViewBag.projectval = true; }
                if (dt1.Rows.Count > 0) { if (dt1.Rows[0][0].ToString() == "Y") ViewBag.uomval = true; }
                if (dtl.Rows.Count > 0) { if (dtl.Rows[0][0].ToString() == "Y") ViewBag.locationval = true; }
				//EMPHCS1213 rajasekhar reddy patakota 26/09/2015 Weight convertion for bill and invoice
                if (dtw.Rows.Count > 0) { if (dtw.Rows[0][0].ToString() == "Y") ViewBag.weightval = true; }
				//EMPHCS1293 rajasekhar reddy patakota 10/10/2015 Mode Of Discount Adding in Invoice Screen
                if (dtd.Rows.Count > 0) { if (dtd.Rows[0][0].ToString() == "Y") ViewBag.discountval = true; }
				//EMPHCS1330 Rajasekhar reddy patakota 10/11/2015 Auto Feild Configuration Setup For Lot Management
                if (dtb.Rows.Count > 0) { if (dtb.Rows[0][0].ToString() == "Y") ViewBag.lotval = true; }
				//EMPHCS1207  rajasekhar reddy patakota 25/09/2015 Auto Feild Configuration Setup For Location and adding success messages
                if (i > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1387 && q.COLUMN05 == 2).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Data Updated............ ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1387 && q.COLUMN05 == 3).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Data Updation Failed.......... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("Info", "EmployeeMaster");
            }
            catch(Exception ex)
            {
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1387 && q.COLUMN05 == 3).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Data Updation Failed ";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

    }
}
