﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Xml;
using WebMatrix.Data;
using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using System.Configuration;
namespace eBizSuiteProduct.Controllers
{
    public class AccountOwnerController : Controller
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        //
        // GET: /AccountOwner/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CustomForm()
        {
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            List<CONTABLE006> all = new List<CONTABLE006>();
            var alll = dbContext.CONTABLE006.Where(a => a.COLUMN04 == ttid && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
            all = alll.Where(a => a.COLUMN06 != null).ToList();
            var Tab = all.Where(a => a.COLUMN11 == "Tab" && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
            var Ta = Tab.Select(a => a.COLUMN12).Distinct().ToList();
            var type = all.Select(b => b.COLUMN11).Distinct();
            var sn = all.Select(b => b.COLUMN12).Distinct();
            var Section = all.Where(b => b.COLUMN11 != "Tab" && b.COLUMN11 != "Item Level" && b.COLUMN06 != null && b.COLUMN06 != "").ToList();
            var sec = Section.Select(b => b.COLUMN12).Distinct();
            var roles = dbContext.CONTABLE012.Where(a => a.COLUMN04 != null).ToList();
            ViewBag.Roles = roles;
            ViewBag.Type = type;
            ViewBag.Tabs = sn;
            ViewBag.TabMaster = Ta;
            ViewBag.Section = sec;
            return View(all);
        }
        public ActionResult NewField()
        {
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            var SecNames = dbContext.CONTABLE006.Where(a => a.COLUMN12 != null&& a.COLUMN04==ttid).ToList();
            var dsecnames=SecNames.Select(a => a.COLUMN12).Distinct();
            ViewBag.SecNames = dsecnames;
            return View();
        }

        [HttpPost]
        public ActionResult NewField(FormCollection fc)
        {
            CONTABLE006 fm = new CONTABLE006();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            var query = "SELECT  top( 1 ) * FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04=COLUMN05  ";
            var data = dbContext.CONTABLE005.SqlQuery(query).ToList();
            var tblid = data.Select(a => a.COLUMN03).FirstOrDefault();
            var colname = data.Select(a => a.COLUMN04).FirstOrDefault();
            var alicolname = data.Select(a => a.COLUMN05).FirstOrDefault();
            var con6data = dbContext.CONTABLE006.Where(a => a.COLUMN03 == ttid).FirstOrDefault();
            fm.COLUMN03 = con6data.COLUMN03;
            fm.COLUMN04 = tblid;
            fm.COLUMN05 = colname;
            fm.COLUMN06 = Request["Label_Name"];
            fm.COLUMN07 = "Y";
            fm.COLUMN08 = Request["Mandatory"];
            fm.COLUMN10 = Request["Control_Type"];
            fm.COLUMN11 = Request["Section_Type"];
            fm.COLUMN12 = Request["Section_Name"];
            var pr=con6data.COLUMN13;
            fm.COLUMN13 = (pr + 1).ToString();
            dbContext.CONTABLE006.Add(fm);
            dbContext.SaveChanges();
            return View("Info");
        }
        //public static class GroupDropListExtensions
        //{
        //    public static MvcHtmlString GroupDropList(this HtmlHelper helper, string name, IEnumerable<GroupDropListItem> data, string SelectedValue, object htmlAttributes)
        //    {
        //        if (data == null && helper.ViewData != null)
        //            data = helper.ViewData.Eval(name) as IEnumerable<GroupDropListItem>;
        //        if (data == null) return MvcHtmlString.Create(string.Empty);

        //        var select = new TagBuilder("select");
        //        select.AddCssClass("nochosen");
        //        if (htmlAttributes != null)
        //            select.MergeAttributes(new RouteValueDictionary(htmlAttributes));

        //        select.GenerateId(name);

        //        var optgroupHtml = new StringBuilder();
        //        var groups = data.ToList();
        //        foreach (var group in data)
        //        {
        //            var groupTag = new TagBuilder("optgroup");
        //            groupTag.Attributes.Add("label", helper.Encode(group.Name));
        //            var optHtml = new StringBuilder();
        //            foreach (var item in group.Items)
        //            {
        //                var option = new TagBuilder("option");
        //                option.Attributes.Add("value", helper.Encode(item.Value));
        //                if (SelectedValue != null && item.Value == SelectedValue)
        //                    option.Attributes.Add("selected", "selected");
        //                option.InnerHtml = helper.Encode(item.Text);
        //                optHtml.AppendLine(option.ToString(TagRenderMode.Normal));
        //            }
        //            groupTag.InnerHtml = optHtml.ToString();
        //            optgroupHtml.AppendLine(groupTag.ToString(TagRenderMode.Normal));
        //        }
        //        select.InnerHtml = optgroupHtml.ToString();
        //        return MvcHtmlString.Create(select.ToString(TagRenderMode.Normal));
        //    }
        //}
      
        string[] theader;
        string[] theadertext;
        [HttpGet]
        public ActionResult Info( string ShowInactives)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
            var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
            theader = new string[Acolslist.Count];
            theadertext = new string[Acolslist.Count];
            for (int a = 0; a < Acolslist.Count; a++)
            {
                var acol = Acolslist[a].ToString();
                var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();
               
                var tblcol = columndata.COLUMN04;
                var dcol = Ccolslist[a].ToString();
                if (a == 0)
                {
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = "Action",
                        Format = (item) => new HtmlString("<a href=/AccountOwner/Edit/" + item[tblcol] + " >Edit</a>|<a href=/AccountOwner/Detailes/" + item[tblcol] + " >View</a>")

                    }); 
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol
                    }); 
                    Inlinecols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol,
                        Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px;font-size:10px  />")
                    }); 
                }
                else
                {
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol
                    });
                    Inlinecols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol,
                        Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:80px;font-size:10px />")
                    });
                } theader[a] = tblcol;theadertext[a] = dcol;
               
            }
            ViewBag.cols = cols;
            ViewBag.Inlinecols = Inlinecols;
            ViewBag.columns = cols;
            ViewBag.itemscol = theader;
            ViewBag.theadertext = theadertext;
            var query1 = "Select * from CONTABLE002 where COLUMNA13='False'";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(query1);
            ViewBag.itemsdata = books;
            Session["cols"] = cols;
            Session["Inlinecols"] = Inlinecols;
            return View("Info", dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false).ToList());
        }
        [HttpGet]
        public ActionResult Sort( string ShowInactives)
        {  
                var data=Session["Data"];
                var item =Session["sortselected"] ;
                ViewBag.sortselected = item;
                //Session["Data"] = null;
                //Session["sortselected"] = null;
                return View("Info",data );
        }
        public ActionResult Create()
        {
            CONTABLE002 all = new CONTABLE002();
            Session["DDDynamicItems"] = "";
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var ParentOf = dbContext.CONTABLE009.ToList();
            ViewBag.ParentOf = ParentOf;
            var col2 = dbContext.CONTABLE002.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
            all.COLUMN02 = (col2.COLUMN02 + 1);
            var country = activity.MATABLE016.ToList();
            ViewBag.country = country;
            var state = activity.MATABLE017.ToList();
            ViewBag.state = state; int? ac = null; int? ou = null; ac = Convert.ToInt32(Session["AcOwner"]);
            var Company = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMNA02 == null).ToList();
            ViewBag.Company = Company;
            return View(all);
        }

        [HttpPost]
        public ActionResult Create(CONTABLE002 info, string Save, string SaveNew)
        {
            info.COLUMNA06 = DateTime.Now;
            info.COLUMNA07 = DateTime.Now;
            info.COLUMNA12 = true;
            info.COLUMNA13 = false;
            dbContext.CONTABLE002.Add(info);
            dbContext.SaveChanges();
            //Column02 Auto Generation
            string COLOUMN02;int  val1;
            SqlCommand cmdd = new SqlCommand("select  Max(COLUMN02) from MATABLE010 ", cn);
            SqlDataAdapter daa = new SqlDataAdapter(cmdd); 
            DataTable dtt = new DataTable();
            daa.Fill(dtt);
            var firstcol = dtt.Rows[0][0].ToString();
            if (firstcol == null || firstcol == "")
            {
                COLOUMN02=1000.ToString();
            }
            else
            {
                val1 = Convert.ToInt32(dtt.Rows[0][0].ToString());
                COLOUMN02=(val1 + 1).ToString();
            }
            //Transaction No Generation
            eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
            var FormName = dbContext.CONTABLE0010.Where(a => a.COLUMN02 == 1260).FirstOrDefault().COLUMN04.ToString();
            var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == FormName && a.COLUMN11 == "default").FirstOrDefault();
            if (listTM == null)
            {
                listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == FormName).FirstOrDefault();
            }
            var Prefix = "";
            var sNum = "";
            string fino, pono,TransactionNo=null;
            int c = 0; 
            SqlCommand pacmd = new SqlCommand();
            if (listTM != null)
            {
                Prefix = listTM.COLUMN06.ToString();
                sNum = listTM.COLUMN07.ToString();
                c = Prefix.Length + 1;
                fino = Prefix;
                pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM MATABLE010  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM MATABLE010  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "') ORDER BY COLUMN02 DESC ", cn);
            }
            else
            {
                    fino = "EM";
                    pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM MATABLE010  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM MATABLE010  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "')  ORDER BY COLUMN02 DESC", cn);
            } 
            SqlDataAdapter pada = new SqlDataAdapter(pacmd);
            DataTable padt = new DataTable();
            pada.Fill(padt);
            if (padt.Rows.Count > 0)
            {
                if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                {
                    pono = fino + 1000.ToString();
                    TransactionNo = (pono);
                }
                else
                {
                    pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString()) + 1);
                    TransactionNo = pono;
                }
            }
            cn.Open();
            SqlCommand Cmd = new SqlCommand("usp_MAS_BL_EmployeeMaster", cn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddWithValue("@COLUMN02", COLOUMN02);
            Cmd.Parameters.AddWithValue("@COLUMN03", 1260);
            Cmd.Parameters.AddWithValue("@COLUMN04", TransactionNo);
            Cmd.Parameters.AddWithValue("@COLUMN06", info.COLUMN03);
            Cmd.Parameters.AddWithValue("@COLUMN13", info.COLUMN08);
            Cmd.Parameters.AddWithValue("@COLUMN20", true);
            Cmd.Parameters.AddWithValue("@COLUMN21", info.COLUMN09);
            Cmd.Parameters.AddWithValue("@COLUMNA03", info.COLUMN02);
            Cmd.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
            Cmd.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
            Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
            Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
            Cmd.Parameters.AddWithValue("@COLUMNA08", info.COLUMN02);
            Cmd.Parameters.AddWithValue("@Direction", "Insert");
            Cmd.Parameters.AddWithValue("@TabelName", "MATABLE010");
            Cmd.Parameters.AddWithValue("@ReturnValue", "");
            int r = Cmd.ExecuteNonQuery();
            //Column02 Auto Generation
            cmdd = new SqlCommand("select  Max(COLUMN02) from SATABLE003 ", cn);
            daa = new SqlDataAdapter(cmdd);
            dtt = new DataTable();
            daa.Fill(dtt);
            firstcol = dtt.Rows[0][0].ToString();
            if (firstcol == null || firstcol == "")
            {
                COLOUMN02 = 1000.ToString();
            }
            else
            {
                val1 = Convert.ToInt32(dtt.Rows[0][0].ToString());
                COLOUMN02 = (val1 + 1).ToString();
            }
            Cmd = new SqlCommand("usp_MAS_BL_EmployeeMaster", cn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddWithValue("@COLUMN02", COLOUMN02);
            Cmd.Parameters.AddWithValue("@COLUMN05", info.COLUMN10);
            Cmd.Parameters.AddWithValue("@COLUMN06", info.COLUMN11);
            Cmd.Parameters.AddWithValue("@COLUMN07", info.COLUMN12);
            Cmd.Parameters.AddWithValue("@COLUMN08", info.COLUMN13);
            Cmd.Parameters.AddWithValue("@COLUMN16", info.COLUMN17);
            Cmd.Parameters.AddWithValue("@COLUMN11", info.COLUMN15);
            Cmd.Parameters.AddWithValue("@COLUMN10", info.COLUMN14);
            Cmd.Parameters.AddWithValue("@COLUMN12", info.COLUMN16);
            Cmd.Parameters.AddWithValue("@COLUMNA03", info.COLUMN02);
            Cmd.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
            Cmd.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
            Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
            Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
            Cmd.Parameters.AddWithValue("@COLUMNA08", info.COLUMN02);
            Cmd.Parameters.AddWithValue("@Direction", "Insert");
            Cmd.Parameters.AddWithValue("@TabelName", "SATABLE003");
            Cmd.Parameters.AddWithValue("@ReturnValue", "");
            r = Cmd.ExecuteNonQuery();
            cn.Close();
            if (Save != null)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1241 && q.COLUMN05 == 1).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Account Owner Successfully Created.......... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                return RedirectToAction("Info");
            }
            return RedirectToAction("Create");
        }
        public ActionResult ListView()
        {
            return View(dbContext.CONTABLE002.ToList());
        } 

        public ActionResult New()
        {
            return View();
        } 
        [HttpGet]
        public ActionResult Edit(int id)
        {
            int? ac = null; int? ou = null;ac = Convert.ToInt32(Session["AcOwner"]);
            CONTABLE002 edit = dbContext.CONTABLE002.Find(id);
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var ParentOf = dbContext.CONTABLE009.ToList();
            ViewBag.ParentOf = ParentOf;
            var country = activity.MATABLE016.ToList();
            ViewBag.country = country;
            var state = activity.MATABLE017.ToList();
            ViewBag.state = state;
            var Company = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMNA02 == null).ToList();
            ViewBag.Company = Company;
            var col2 = edit.COLUMN02;
                Session["time"]= dbContext.CONTABLE002.Find(col2).COLUMNA06;
               return View(edit);
        }

        [HttpPost]
        public ActionResult Edit(CONTABLE002 info)
        {
            if (ModelState.IsValid)
            {
                dbContext.Entry(info).State = EntityState.Modified;
                info.COLUMNA07 = DateTime.Now; info.COLUMNA12 = true; info.COLUMNA13 = false;
                info.COLUMNA06 = Convert.ToDateTime(Session["time"]);
                dbContext.SaveChanges();
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1241 && q.COLUMN05 == 2).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Account Owner Successfully Modified.......... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                return RedirectToAction("Info");
            }
            return View(info);
        }

        public ActionResult SaveNew(CONTABLE002 info)
        {
            if (ModelState.IsValid)
            {
                dbContext.Entry(info).State = EntityState.Modified;
                dbContext.SaveChanges();
                return RedirectToAction("Edit");
            }
            return View(info);
        }

        public ActionResult Detailes(int id)
        {
            var ac = Convert.ToInt32(Session["AcOwner"]);
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var ParentOf = dbContext.CONTABLE009.ToList();
            ViewBag.ParentOf = ParentOf;
            var country = activity.MATABLE016.ToList();
            ViewBag.country = country;
            var state = activity.MATABLE017.ToList();
            ViewBag.state = state;
            var Company = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac).ToList();
            ViewBag.Company = Company;
            CONTABLE002 Detaile = dbContext.CONTABLE002.Find(id);

            return View(Detaile);
        }
       [HttpGet]
        public ActionResult Delete(int id)
        {
            CONTABLE002 del = dbContext.CONTABLE002.Find(id);

            var y = (from x in dbContext.CONTABLE002 where x.COLUMN02 == id select x).First();
            y.COLUMNA13 = true;
            //dbContext.CONTABLE002.Remove(y);
            dbContext.SaveChanges();
            var msg = string.Empty;
            var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1241 && q.COLUMN05 == 4).FirstOrDefault();
            if (msgMaster != null)
            {
                msg = msgMaster.COLUMN03;
            }
            else
            {
                msg = "Account Owner Successfully Deleted.......... ";
            }
            Session["MessageFrom"] = msg;
            Session["SuccessMessageFrom"] = "Success";
            return RedirectToAction("Info");
        }
       [HttpPost]
       public ActionResult UpdateInline(string list)
       {
           XmlDocument obj = new XmlDocument();
           obj.LoadXml(list);
           DataSet ds = new DataSet();
           ds.ReadXml(new XmlNodeReader(obj));
           string qStr = null;
           string tName = Session["Table"].ToString();
           int frmID = Convert.ToInt32(Session["id"]);
           var tblID = dbContext.CONTABLE004.Where(q => q.COLUMN04 == "CONTABLE002").Select(q => new { q.COLUMN02 });
           var ID = tblID.Select(a => a.COLUMN02).First();
           var clName = dbContext.CONTABLE005.Where(q => q.COLUMN03 == ID && q.COLUMN04 != "COLUMN01" && (q.COLUMN04.Length) < 9).Select(q => new { q.COLUMN04, q.COLUMN05 });
           for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
           {
               int x = 0;
               foreach (var items in clName)
               {
                   if (x < ds.Tables[0].Columns.Count)
                   {
                       if (items.COLUMN05 == ds.Tables[0].Columns[x].ColumnName)
                       {
                           if (qStr == null)
                           {
                               qStr += "UPDATE CONTABLE002 SET " + items.COLUMN05 + " ='" + ds.Tables[0].Rows[j][items.COLUMN05] + "'";
                           }
                           else if (x == ds.Tables[0].Columns.Count - 1)
                           {
                               qStr += "," + items.COLUMN05 + " ='" + ds.Tables[0].Rows[j][items.COLUMN05] + "' WHERE COLUMN02=" + ds.Tables[0].Rows[j]["ID"] + "";
                           }
                           else
                           {
                               qStr += "," + items.COLUMN05 + " ='" + ds.Tables[0].Rows[j][items.COLUMN05] + "'";
                           }
                       }
                       x++;
                   }
               }
               SqlConnection cn = new SqlConnection(sqlcon);
               SqlCommand cmd1 = new SqlCommand(qStr, cn);
               cn.Open();
               cmd1.ExecuteNonQuery();
               cn.Close();
               qStr = null;
           }
           return RedirectToAction("Info");
       }
       public ActionResult Search()
       {
           return View();
       }
        [HttpPost]
       public ActionResult Search(FormCollection fc)
       {
           var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
           var ttid = tid.COLUMN02;
           var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
           ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
           var Name = Request["Name"];
           var Company = Request["Company"];
           var Email = Request["Email"];
           var State = Request["State"];

           if (Name != null && Name != "" && Name != string.Empty)
           {
               Name = Name + "%";
           }
           else
               Name = Request["Name"];


           if (Company != null && Company != "" && Company != string.Empty)
           {
               Company = Company + "%";
           }
           else
               Company = Request["Company"];


           if (Email != null && Email != "" && Email != string.Empty)
           {
               Email = Email + "%";
           }
           else
               Email = Request["Email"];

           if (State != null && State != "" && State != string.Empty)
           {
               State = State + "%";
           }
           else
               State = Request["State"];

           var query = "SELECT * FROM CONTABLE002  WHERE COLUMN03 like '" + Name + "' or COLUMN04 LIKE '" + Company + "' or COLUMN08 LIKE '" + Email + "'  or COLUMN23 LIKE '" + State + "'";

           var query1 = dbContext.CONTABLE002.SqlQuery(query);
           var gdata = from e in query1 select   e ;
           ViewBag.cols =Session["cols"] ;
           ViewBag.Inlinecols = Session["Inlinecols"]; ViewBag.columns = Session["Inlinecols"];
            return View("Info",gdata); 
       }
        public ActionResult QuickSort( string item)
        {
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
            List<CONTABLE002> all = new List<CONTABLE002>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            if (item == "Recently Created")
            {
                all = dbContext.CONTABLE002.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA12 == true).ToList();
                grid = new WebGrid(all, canPage: false, canSort: false);
            }
            else if (item == "Recently Modified")
            {
                all = dbContext.CONTABLE002.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA12 == true).ToList();
                all = all.Where(a => a.COLUMNA07 != null).ToList();
                grid = new WebGrid(all, canPage: false, canSort: false);
            }
            else
            {
                all = dbContext.CONTABLE002.Where(a => a.COLUMNA12 == true).ToList();
                grid = new WebGrid(all, canPage: false, canSort: false);
            }
                              var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                              headerStyle: "webgrid-header",
                              footerStyle: "webgrid-footer",
                              alternatingRowStyle: "webgrid-alternating-row",
                              rowStyle: "webgrid-row-style",
                              htmlAttributes: new { id = "DataTable" },
                              columns:
                                  grid.Columns
                                  (grid.Column("Action", "Action", format: (items) => new HtmlString("<a href='/AccountOwner/Edit/" + items.COLUMN02 + "'>Edit</a>|<a href='/AccountOwner/Detailes/" + items.COLUMN02 + "'>View</a>")),
                                  grid.Column("COLUMN02", "AccountOwnerID"),
                                  grid.Column("COLUMN03", "Name"),
                                  grid.Column("COLUMN04", "Company"),
                                  grid.Column("COLUMN07", "BusinessType"),
                                  grid.Column("COLUMN08", "Email"),
                                  grid.Column("COLUMN10", "Attention"),
                                  grid.Column("COLUMN23", "RState"),
                                  grid.Column("COLUMN24", "RZip"),
                                  grid.Column("COLUMN26", "Info1"),
                                  grid.Column("COLUMN27", "Info2")
                                  ));
                                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                                headerStyle: "webgrid-header",
                                footerStyle: "webgrid-footer",
                                alternatingRowStyle: "webgrid-alternating-row",
                                rowStyle: "webgrid-row-style",
                                htmlAttributes: new { id = "grdData" },
                                columns:
                                    grid.Columns
                                    (
                                    //grid.Column(col1, col1, format: (item1) => new HtmlString("<input type='text' id=" + col1 + " name=" + col1 + "    Value=" + item1[col1] + "   />")),
                                    //grid.Column(col2, col2, format: (item1) => new HtmlString("<input type='text' id=" + col2 + " name=" + col2 + "    Value=" + item1[col2] + "   />")),
                                    //grid.Column(col4, col4, format: (item1) => new HtmlString("<input type='text' id=" + col4 + " name=" + col4 + "     Value=" + item1[col4] + " />")),
                                    //grid.Column(col5, col5, format: (item1) => new HtmlString("<input type='text' id=" + col5 + " name=" + col5 + "     Value=" + item1[col5] + " />")))
                                    //
                                    ));
            return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InActive(string ShowInactives)
        {
            List<CONTABLE002> all = new List<CONTABLE002>();
            var grid = new WebGrid(null, canPage: false, canSort: false); 
            if (ShowInactives != null)
            {
                all = dbContext.CONTABLE002.ToList();
                var data = from e in dbContext.CONTABLE002.AsEnumerable() select new { AccountOwnerID = e.COLUMN02, Name = e.COLUMN03, Company = e.COLUMN04, BusinessType = e.COLUMN07, Email = e.COLUMN08, Attention = e.COLUMN10, RState = e.COLUMN23, RZip = e.COLUMN24, Info1 = e.COLUMN26, Info2=e.COLUMN27 };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            else
            {
                all = dbContext.CONTABLE002.Where(a => a.COLUMNA12 == true).ToList();
                var data = from e in dbContext.CONTABLE002.AsEnumerable() where (e.COLUMNA12==true) select new { AccountOwnerID = e.COLUMN02, Name = e.COLUMN03, Company = e.COLUMN04, BusinessType = e.COLUMN07, Email = e.COLUMN08, Attention = e.COLUMN10, RState = e.COLUMN23, RZip = e.COLUMN24, Info1 = e.COLUMN26, Info2 = e.COLUMN27 };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
          headerStyle: "webgrid-header",
          footerStyle: "webgrid-footer",
          alternatingRowStyle: "webgrid-alternating-row",
          rowStyle: "webgrid-row-style",
                              htmlAttributes: new { id = "DataTable" },
                              columns:
                                  grid.Columns
                                  (grid.Column("Action", "Action", format: (item) => new HtmlString("<a href='/AccountOwner/Edit/" + item.AccountOwnerID + "'>Edit</a>|<a href='/AccountOwner/Detailes/" + item.AccountOwnerID + "'>View</a>")),
                                  grid.Column("AccountOwnerID", "AccountOwnerID"),
                                  grid.Column("Name", "Name"),
                                  grid.Column("Company", "Company"),
                                  grid.Column("BusinessType", "BusinessType"),
                                  grid.Column("Email", "Email"),
                                  grid.Column("Attention", "Attention"),
                                  grid.Column("RState", "RState"),
                                  grid.Column("RZip", "RZip"),
                                  grid.Column("Info1", "Info1"),
                                  grid.Column("Info2", "Info2")
                                  ));
                return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        }
        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult Style(string items, string inactive, string view, string sort)
        {  
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            List<CONTABLE002> all = new List<CONTABLE002>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var tbtid = tbid.COLUMN02;
            var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
            ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
            var data = from e in dbContext.CONTABLE002.AsEnumerable() select new { AccountOwnerID = e.COLUMN02, Name = e.COLUMN03, Company = e.COLUMN04, Description = e.COLUMN05, InActive = e.COLUMN06, BusinessType = e.COLUMN07, Email = e.COLUMN08, Password = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };   
            List<CONTABLE002> tbldata = new List<CONTABLE002>();
            tbldata=dbContext.CONTABLE002.Where(a=>a.COLUMNA13==false).ToList();
            if (items == "Normal")
            {
                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                var indata = dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE002 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE002 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1); 
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th class=webgrid-header>Action</th>";
                        tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                    }
                } int r = 0;string rowColor="";
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        r = r + 1; 
                        
                        if (r % 2 == 0)
                        {
                            rowColor = "alt";
                        }
                        else
                        {
                            rowColor = "alt0";
                        }
    
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td class=webgrid-row-style><a href=/AccountOwner/Edit/" + itm[Inline] + " >Edit</a>|<a href=/AccountOwner/Detailes/" + itm[Inline] + " >View</a></td>";
                            tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true  style=width:80px;font-size:10px  /></td>";
                        }
                        else
                        {
                            tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:80px;font-size:10px  /></td>";
                        }


                    }
                    tthdata = "<tr class="+@rowColor+" style='color:black'>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                 grid = new WebGrid(null, canPage: false, canSort: false);

                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/AccountOwner/Edit/" + item[tblcol] + " >Edit</a>|<a href=/AccountOwner/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px;font-size:10px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:80px;font-size:10px/>")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Info", dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false).ToList());
        }
   
        public ActionResult CustomView()
        {
            List<CONTABLE005> all = new List<CONTABLE005>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            //all = from e in dbContext.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
            var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
            all = dbContext.CONTABLE005.SqlQuery(query).ToList();
            
            return View(all);
        }
        
        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
            int col2 = 0;
            var tbldata = dbContext.CONTABLE004.Where(a => a.COLUMN05 == "Account_Owner").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = dbContext.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString(); 
                if (i == 0)
                {
                    var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    dbContext.CONTABLE013.Add(dd);
                    dbContext.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                            var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                            dd.COLUMN02 = (vid.COLUMN02) + 1;
                            dd.COLUMN03 = ViewName;
                            dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                            dd.COLUMN05 = Convert.ToInt32(formid);
                            dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                            dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                            dbContext.CONTABLE013.Add(dd);
                            dbContext.SaveChanges();
                    }
                   
                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult View(string items, string sort, string inactive, string style)
        {
            List<CONTABLE002> all = new List<CONTABLE002>();
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (style == "Normal")
            {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var indata = dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1="";
                    if (inactive != null)
                    {
                        query1 = "Select * from CONTABLE002 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from CONTABLE002 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString(); 
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th class=webgrid-header>Action</th>";
                            tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                            itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                            itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td class=webgrid-row-style><a href=/AccountOwner/Edit/" + itm[Inline] + " >Edit</a>|<a href=/AccountOwner/Detailes/" + itm[Inline] + " >View</a></td>";
                                itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px;font-size:10px  /></td>";
                                tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            }
                            else
                            {
                                itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:80px;font-size:10px /></td>";
                                tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            }
                        }
                        tthdata = "<tr>"+  tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                    return View("Info", dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12==true).ToList());
                
            }
            else
            {
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                

                if (sort == "Recently Created")
                {
                    all = dbContext.CONTABLE002.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    all = dbContext.CONTABLE002.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                else
                {
                    all = dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false).ToList();

                }
                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/AccountOwner/Edit/" + item[tblcol] + " >Edit</a>|<a href=/AccountOwner/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px;font-size:10px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:80px;font-size:10px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            } 
            return View("Info", dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false).ToList());
        }
        public void ExportPdf()
        {
            List<CONTABLE002> all = new List<CONTABLE002>();
            all = dbContext.CONTABLE002.ToList();
            var data = from e in dbContext.CONTABLE002.AsEnumerable() select new { AccountOwnerID = e.COLUMN02, Name = e.COLUMN03, Company = e.COLUMN04, Description = e.COLUMN05, InActive = e.COLUMN06, BusinessType = e.COLUMN07, Email = e.COLUMN08, Password = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        public void Exportword()
        {
            List<CONTABLE002> all = new List<CONTABLE002>();
            all = dbContext.CONTABLE002.ToList();
            var data = from e in dbContext.CONTABLE002.AsEnumerable() select new { AccountOwnerID = e.COLUMN02, Name = e.COLUMN03, Company = e.COLUMN04, Description = e.COLUMN05, InActive = e.COLUMN06, BusinessType = e.COLUMN07, Email = e.COLUMN08, Password = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
            Response.ContentType = "application/vnd.ms-word ";
            Response.Charset = string.Empty;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        public void ExportCSV()
        {
            StringWriter sw = new StringWriter();

            var y = dbContext.CONTABLE002.OrderBy(q => q.COLUMN02).ToList();

            sw.WriteLine("\"AccountOwnerID\",\"Name\",\"Company\",\"Description\",\"Inactive\",\"Business_Type\",\"Email\",\"Password\",\"Attention\",\"Addressee\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Country\",\"Attention\",\"Addressee\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Country\",\"Info1\",\"Info2\",\"Info3\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
            Response.ContentType = "text/csv";

            foreach (var line in y)
            {

                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\",\"{19}\",\"{20}\",\"{21}\",\"{22}\",\"{23}\",\"{24}\",\"{25}\",\"{26}\"",
                                           line.COLUMN02,
                                           line.COLUMN03,
                                           line.COLUMN04,
                                           line.COLUMN05,
                                           line.COLUMN06,
                                           line.COLUMN07,
                                           line.COLUMN08,
                                           line.COLUMN09,
                                           line.COLUMN10,
                                           line.COLUMN11,
                                           line.COLUMN12,
                                           line.COLUMN13,
                                           line.COLUMN14,
                                           line.COLUMN15,
                                           line.COLUMN16,
                                           line.COLUMN17,
                                           line.COLUMN18,
                                           line.COLUMN19,
                                           line.COLUMN20,
                                           line.COLUMN21,
                                           line.COLUMN22,
                                           line.COLUMN23,
                                           line.COLUMN24,
                                           line.COLUMN25,
                                           line.COLUMN26,
                                           line.COLUMN27,
                                           line.COLUMN28));

            }

            Response.Write(sw.ToString());

            Response.End();

        }
        public void ExportExcel()
        {
            List<CONTABLE002> all = new List<CONTABLE002>();
            all = dbContext.CONTABLE002.ToList();
            var data = from e in dbContext.CONTABLE002.AsEnumerable() select new { AccountOwnerID = e.COLUMN02, Name = e.COLUMN03, Company = e.COLUMN04, Description = e.COLUMN05, InActive = e.COLUMN06, BusinessType = e.COLUMN07, Email = e.COLUMN08, Password = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            gv.FooterRow.Visible = false;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter ht = new HtmlTextWriter(sw);
            gv.RenderControl(ht);
            Response.Write(sw.ToString());
            Response.End();
            gv.FooterRow.Visible = true;

        }
        public ActionResult Export(string items)
        {  
            List<CONTABLE002> all = new List<CONTABLE002>();
            all = dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false).ToList();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE002").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (items == "PDF")
            {
                ExportPdf();
            }
            else if (items == "Excel")
            {
                ExportExcel();
            }
            else if (items == "Word")
            {
                Exportword();
            }
            else if (items == "CSV")
            {
                ExportCSV();
            }
            return View("Info",dbContext.CONTABLE002.Where(a => a.COLUMNA13 == false).ToList());
        }
    }
}                                         

                                           

                                           

                                          

                                          

                                         

                                           

                                           