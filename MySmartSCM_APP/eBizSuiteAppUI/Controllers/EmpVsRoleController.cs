﻿using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;
using System.Configuration;
namespace MvcApplication1.Controllers
{
    public class EmpVsRoleController : Controller
    {

        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        //
        // GET: /OperatingUnit/

        string[] theader;
        string[] theadertext;

        public ActionResult Index(string ShowInactives)
        {
            try
            {

                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/EmpVsRole/Edit/" + item[tblcol] + " >Edit</a>|<a href=/EmpVsRole/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   ''  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  '' />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;
                ViewBag.columns = theader;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                var ac = Convert.ToInt32(Session["AcOwner"]); string ounit = null;
                if (ac != 56567)
                    ounit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                else
                    ounit = "  p.COLUMNA02 is null ";
                var query1 = "Select p.COLUMN02,e.COLUMN06 COLUMN03,r.COLUMN04,d.COLUMN04 COLUMN06,o.COLUMN03 COLUMN05,p.COLUMN07 from CONTABLE027 p left outer join MATABLE002 d on d.COLUMN02=p.COLUMN06 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN05 left outer join CONTABLE012 r on r.COLUMN02=p.COLUMN04 left outer join MATABLE010 e on e.COLUMN02=p.COLUMN03 where p.COLUMNA13='False'    AND p.COLUMN03='" + Session["eid"] + "' AND " + ounit + " AND p.COLUMNA03='" + Session["AcOwner"] + "'";
                var db2 = WebMatrix.Data.Database.Open("sqlcon");
                var books = db2.Query(query1);
                var acO = Convert.ToInt32(Session["AcOwner"]);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                return View("Index");
            }
            catch (Exception ex)
            {
                return Content("Form Editing Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }
        //
        // GET: /OperatingUnit/Details/5

        public ActionResult CustomForm()
        {
            try
            {

                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var ttid = tid.COLUMN02;
                List<CONTABLE006> all = new List<CONTABLE006>();
                var alll = db.CONTABLE006.Where(a => a.COLUMN04 == ttid && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                all = alll.Where(a => a.COLUMN06 != null).ToList();
                var Tab = all.Where(a => a.COLUMN11 == "Tab" && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                var Ta = Tab.Select(a => a.COLUMN12).Distinct().ToList();
                var type = all.Select(b => b.COLUMN11).Distinct();
                var sn = all.Select(b => b.COLUMN12).Distinct();
                var Section = all.Where(b => b.COLUMN11 != "Tab" && b.COLUMN11 != "Item Level" && b.COLUMN06 != null && b.COLUMN06 != "").ToList();
                var sec = Section.Select(b => b.COLUMN12).Distinct();
                //var roles = db.CONTABLE012.Where(a => a.COLUMN04 != null).ToList();
                //ViewBag.Roles = roles;
                ViewBag.Type = type;
                ViewBag.Tabs = sn;
                ViewBag.TabMaster = Ta;
                ViewBag.Section = sec;
                return View(all);
            }
            catch (Exception ex)
            {
                return Content("Form Editing Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public void Normal()
        {

            List<WebGridColumn> cols = new List<WebGridColumn>();
            var col2 = "COLUMN02";
            cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN02",
                Header = "Action",
                Format = (item) => new HtmlString("<a href=/OperatingUnit/Edit/" + item[col2] + " >Edit</a>|<a href=/OperatingUnit/Details/" + item[col2] + " >View</a>")
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN02",
                Header = "OperatingUnit ID"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN03",
                Header = "Name"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN04",
                Header = "Description"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN05",
                Header = "Logo"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN08",
                Header = "Parent Of"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN10",
                Header = "Attention"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN23",
                Header = "RState"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN24",
                Header = "RZip"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN26",
                Header = "Info1"
            }); cols.Add(new WebGridColumn()
            {
                ColumnName = "COLUMN27",
                Header = "Info2"
            });
            ViewBag.cols = cols;

        }

        [HttpGet]
        public ActionResult Sort(string ShowInactives)
        {
            try
            {

                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;

                return View("Index", data);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        //
        // GET: /OperatingUnit/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {

                CONTABLE027 CONTABLE027 = db.CONTABLE027.Find(id);
                ViewBag.r = db.CONTABLE012.ToList();
                ViewBag.o = db.CONTABLE007.ToList();
                ViewBag.e = db.MATABLE010.ToList();
                ViewBag.d = db.MATABLE002.Where(q => q.COLUMN03 == 11117).ToList();
                if (CONTABLE027 == null)
                {
                    return HttpNotFound();
                }
                return View(CONTABLE027);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        //
        // GET: /OperatingUnit/Create

        public ActionResult Create()
        {
            
                var ac = Convert.ToInt32(Session["AcOwner"]);
                string eid = (Session["OPUnit"]).ToString(); int? ou = null;
                //if (ac != 56567)
                //    ou = Convert.ToInt32(Session["OPUnit"].ToString());
                if (ac == 56567)
                {
                    ou = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(Convert.ToString( Session["OPUnitstatus"])))
                        ou = null;
                    else
                        ou = Convert.ToInt32(Session["OPUnit"]);
                }
                eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
                if (ou == null)
                {
                    ViewBag.r = dbContext.CONTABLE012.Where(q => q.COLUMN06 == null).ToList();
                    ViewBag.o = dbContext.CONTABLE007.Where(q => q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == null && q.COLUMNA03 == ac).ToList();
                    ViewBag.e = dbContext.MATABLE010.Where(q => q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == null && q.COLUMNA03 == ac).ToList();
                    ViewBag.d = dbContext.MATABLE002.Where(q => q.COLUMN03 == 11117 && q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == null && q.COLUMNA03 == ac).ToList();
                }
                else
                {
                    ViewBag.r = dbContext.CONTABLE012.Where(q => eid.Contains(q.COLUMN06)).ToList();
                    ViewBag.o = dbContext.CONTABLE007.Where(q => q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == ou && q.COLUMNA03 == ac).ToList();
                    ViewBag.e = dbContext.MATABLE010.Where(q => q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == ou && q.COLUMNA03 == ac).ToList();
                    ViewBag.d = dbContext.MATABLE002.Where(q => q.COLUMN03 == 11117 && q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == ou && q.COLUMNA03 == ac).ToList();
                }
                //}
                Session["DDDynamicItems"] = "";
                CONTABLE027 all = new CONTABLE027();
                var col2 = db.CONTABLE027.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
                var col1 = col2;
                if (col2 == null || col2 == 0)
                { col1 = 50000; all.COLUMN02 = 50000; }
                else
                {
                    all.COLUMN02 = (Convert.ToInt32(col1) + 1);
                }
                return View(all);
        
        }

        //
        // POST: /OperatingUnit/Create

        [HttpPost]
        public ActionResult Create(CONTABLE027 CONTABLE027, string Create, string Save)
        {

            if (ModelState.IsValid)
            {
                CONTABLE027.COLUMNA13 = false;
                CONTABLE027.COLUMNA12 = true;
                var ac = Convert.ToInt32(Session["AcOwner"]);
                if (ac == 56567)
                {
                    CONTABLE027.COLUMNA02 = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                        CONTABLE027.COLUMNA02 = null;
                    else
                        CONTABLE027.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                }
                CONTABLE027.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                CONTABLE027.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"]);
                CONTABLE027.COLUMNA06 = DateTime.Now;
                CONTABLE027.COLUMNA07 = DateTime.Now;
                db.CONTABLE027.Add(CONTABLE027);
                //db.SaveChanges();
                var emp = CONTABLE027.COLUMN03;
                var acID = db.MATABLE010.Where(q => q.COLUMN02 == emp).Select(q => q.COLUMNA03).FirstOrDefault();
                var mnu = db.CONTABLE003.Where(q => q.COLUMNA03 == acID).ToList();
                var cID = db.CONTABLE012.Where(q => q.COLUMN02 == CONTABLE027.COLUMN04).Select(q => q.COLUMN05).FirstOrDefault();
                var menu = db.CONTABLE003.Where(q => q.COLUMN04 == cID).ToList();
                var acOwnr = menu.Select(q => q.COLUMNA03).FirstOrDefault();
                if (menu != null && acOwnr == null)
                {
                    foreach (CONTABLE003 product in menu)
                    {
                        product.COLUMNA03 = acID;
                    }
                }
                else if (menu != null && acOwnr != acID)
                {
                    foreach (CONTABLE003 product in menu)
                    {
                        product.COLUMNA03 = acID;
                        db.CONTABLE003.Add(product);
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();

                int eID = Convert.ToInt32(Session["eid"]);
                var centerIDR = db.CONTABLE027.Where(a => a.COLUMN03 == eID).Select(q => q.COLUMN04).ToList();
                var rls = db.CONTABLE012.Where(a => centerIDR.Contains(a.COLUMN02)).ToList().OrderByDescending(q => q.COLUMN02);
                SelectList Country = new SelectList(rls, "COLUMN05", "COLUMN03"); 
                Session["roles"] = Country;
                ViewData["roleM"] = Session["roles"];
                if (Session["DDDynamicItems"] == "DDDynamicItems")
                {
                    Session["DDDynamicItems"] = null;
                    return RedirectToAction("FormBuild", "FormBuilding", new { FormName = Session["FormName"] });
                }
                if (Save != null)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Create");

            }



            return View(CONTABLE027);

        }
        //
        // GET: /OperatingUnit/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CONTABLE027 CONTABLE027 = db.CONTABLE027.Find(id);
            var ac = Convert.ToInt32(Session["AcOwner"]);
            var eid = (Session["OPUnit"]).ToString(); int? ou = null;
            if (ac != 56567)
                ou = Convert.ToInt32(Session["OPUnit"].ToString());

            //if (ac != 56567)
            //    ou = Convert.ToInt32(Session["OPUnit"].ToString());
            if (ac == 56567)
            {
                ou = null;
            }
            else
            {
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    ou = null;
                else
                    ou = Convert.ToInt32(Session["OPUnit"]);
            }
            eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
            ViewBag.r = dbContext.CONTABLE012.Where(q => eid.Contains(q.COLUMN06)).ToList();
            ViewBag.o = dbContext.CONTABLE007.Where(q => q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == ou && q.COLUMNA03 == ac).ToList();
            ViewBag.e = dbContext.MATABLE010.Where(q => q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == ou && q.COLUMNA03 == ac).ToList();
            ViewBag.d = dbContext.MATABLE002.Where(q => q.COLUMN03 == 11117 && q.COLUMNA13 == false && q.COLUMN03 != null && q.COLUMNA02 == ou && q.COLUMNA03 == ac).ToList();


            var tmp = db.CONTABLE027.Where(a => a.COLUMN05 != null && a.COLUMN02 == id).ToList();
            var tmp1 = tmp.Select(a => a.COLUMN05).SingleOrDefault();
            TempData["abc"] = tmp1;

            if (CONTABLE027 == null)
            {
                return HttpNotFound();
            }
            return View(CONTABLE027);
        }

        //
        // POST: /OperatingUnit/Edit/5
        [HttpPost]
        public ActionResult Edit(CONTABLE027 CONTABLE027)
        {
            try
            {

                if (ModelState.IsValid)
                {


                    //if (CONTABLE027.COLUMN05 == null)
                    //{

                    var image = Request.Files[0] as HttpPostedFileBase;
                    if (image != null)
                    {
                        if (Request.Files.Count > 0)
                        {
                            //var image = Request.Files[0] as HttpPostedFileBase;
                            if (image.ContentLength > 0)
                            {

                                string fileName = Path.GetFileName(image.FileName);
                                var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                                image.SaveAs(path);
                                //CONTABLE027.COLUMN05 = fileName;

                                //db.CONTABLE027.Add(CONTABLE027);
                                //db.SaveChanges();

                            }

                        }
                    }
                    //}
                    else
                    {
                        //  CONTABLE027.COLUMN05 = Convert.ToString(TempData["abc"]);
                        db.CONTABLE027.Add(CONTABLE027);
                        //db.Entry(CONTABLE027).State = EntityState.Modified;
                        //db.SaveChanges();
                    }

                    CONTABLE027.COLUMNA07 = DateTime.Now;

                    CONTABLE027.COLUMNA12 = true;

                    CONTABLE027.COLUMNA13 = false;
                    var ac = Convert.ToInt32(Session["AcOwner"]);
                    if (ac == 56567)
                    {
                        CONTABLE027.COLUMNA02 = null;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            CONTABLE027.COLUMNA02 = null;
                        else
                            CONTABLE027.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                    }
                    CONTABLE027.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);

                    db.Entry(CONTABLE027).State = EntityState.Modified;
                    db.SaveChanges();


                    return RedirectToAction("Index");
                }
                return View(CONTABLE027);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        //
        // GET: /OperatingUnit/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                CONTABLE027 CONTABLE027 = db.CONTABLE027.Find(id);

                var y = (from x in db.CONTABLE027 where x.COLUMN02 == id select x).First();
                y.COLUMNA12 = false;
                y.COLUMNA13 = true;

                //db.CONTABLE027.Remove(y);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }

        }

        public void ExportCSV()
        {

            StringWriter sw = new StringWriter();

            var y = db.CONTABLE027.OrderBy(q => q.COLUMN02).ToList();

            sw.WriteLine("\"OperatingUnit ID\",\"Name\",\"Description\",\"Logo\",\"Inactive\",\"Make Inventary Available\",\"Parent Of\",\"Subsidary\",\"Attention\",\"Return Addressee\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Country\",\"Return Attention\",\"Return Addressee\",\"Return Address1\",\"Return Address2\",\"Return City\",\"Return State\",\"Return Zip\",\"Return Country\",\"Info1\",\"Info2\",\"Info3\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
            Response.ContentType = "text/csv";

            foreach (var line in y)
            {

                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\",\"{19}\",\"{20}\",\"{21}\",\"{22}\",\"{23}\",\"{24}\",\"{25}\",\"{26}\"",
                                           line.COLUMN02,
                                           line.COLUMN03,
                                           line.COLUMN04,
                                           line.COLUMN05,
                                           line.COLUMN07
                    //,line.COLUMN08
                    //line.COLUMN09,
                    //line.COLUMN10,
                    //line.COLUMN11,
                    //line.COLUMN12,
                    //line.COLUMN13,
                    //line.COLUMN14,
                    //line.COLUMN15,
                    //line.COLUMN16,
                    //line.COLUMN17,
                    //line.COLUMN18,
                    //line.COLUMN19,
                    //line.COLUMN20,
                    //line.COLUMN21,
                    //line.COLUMN22,
                    //line.COLUMN23,
                    //line.COLUMN24,
                    //line.COLUMN25, line.COLUMN26, line.COLUMN27, line.COLUMN28, line.COLUMN29
                                           ));

            }

            Response.Write(sw.ToString());

            Response.End();


        }

        public void ExportPdf()
        {
            List<CONTABLE027> all = new List<CONTABLE027>();
            all = db.CONTABLE027.ToList();
            var data = from e in db.CONTABLE027.AsEnumerable()
                       select new
                       {
                           OperatingUnitID = e.COLUMN02,
                           Name = e.COLUMN03,
                           Description = e.COLUMN04,
                           Logo1 = e.COLUMN05,
                           Logo2 = e.COLUMN06,
                           InActive = e.COLUMN07
                           //ParentOf = e.COLUMN09, Subsidary = e.COLUMN10, Attention = e.COLUMN11, Addressee = e.COLUMN12, Address1 = e.COLUMN13, Address2 = e.COLUMN14, City = e.COLUMN15, State = e.COLUMN16, Zip = e.COLUMN17, Country = e.COLUMN18, RAttention = e.COLUMN19, RAddressee = e.COLUMN20, RAddress1 = e.COLUMN21, RAddress2 = e.COLUMN22, RCity = e.COLUMN23, RState = e.COLUMN24, RZip = e.COLUMN25, RCountry = e.COLUMN26, Info1 = e.COLUMN27, Info2 = e.COLUMN28, Info3 = e.COLUMN29 
                       };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        public void Exportword()
        {
            List<CONTABLE027> all = new List<CONTABLE027>();
            all = db.CONTABLE027.ToList();
            var data = from e in db.CONTABLE027.AsEnumerable()
                       select new
                       {
                           OperatingUnitID = e.COLUMN02,
                           Name = e.COLUMN03,
                           Description = e.COLUMN04,
                           Logo1 = e.COLUMN05,
                           Logo2 = e.COLUMN06,
                           InActive = e.COLUMN07
                           //, MakeInventaryAvailable = e.COLUMN08 , ParentOf = e.COLUMN09, Subsidary = e.COLUMN10, Attention = e.COLUMN11, Addressee = e.COLUMN12, Address1 = e.COLUMN13, Address2 = e.COLUMN14, City = e.COLUMN15, State = e.COLUMN16, Zip = e.COLUMN17, Country = e.COLUMN18, RAttention = e.COLUMN19, RAddressee = e.COLUMN20, RAddress1 = e.COLUMN21, RAddress2 = e.COLUMN22, RCity = e.COLUMN23, RState = e.COLUMN24, RZip = e.COLUMN25, RCountry = e.COLUMN26, Info1 = e.COLUMN27, Info2 = e.COLUMN28, Info3 = e.COLUMN29 
                       };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
            Response.ContentType = "application/vnd.ms-word ";
            Response.Charset = string.Empty;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public void ExportExcel()
        {
            List<CONTABLE027> all = new List<CONTABLE027>();
            all = db.CONTABLE027.ToList();
            var data = from e in db.CONTABLE027.AsEnumerable()
                       select new
                       {
                           OperatingUnitID = e.COLUMN02,
                           Name = e.COLUMN03,
                           Description = e.COLUMN04,
                           Logo1 = e.COLUMN05,
                           Logo2 = e.COLUMN06,
                           InActive = e.COLUMN07
                           //, ParentOf = e.COLUMN09, Subsidary = e.COLUMN10, Attention = e.COLUMN11, Addressee = e.COLUMN12, Address1 = e.COLUMN13, Address2 = e.COLUMN14, City = e.COLUMN15, State = e.COLUMN16, Zip = e.COLUMN17, Country = e.COLUMN18, RAttention = e.COLUMN19, RAddressee = e.COLUMN20, RAddress1 = e.COLUMN21, RAddress2 = e.COLUMN22, RCity = e.COLUMN23, RState = e.COLUMN24, RZip = e.COLUMN25, RCountry = e.COLUMN26, Info1 = e.COLUMN27, Info2 = e.COLUMN28, Info3 = e.COLUMN29 
                       };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            gv.FooterRow.Visible = false;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter ht = new HtmlTextWriter(sw);
            gv.RenderControl(ht);
            Response.Write(sw.ToString());
            Response.End();
            gv.FooterRow.Visible = true;

        }

        public ActionResult Export(string items)
        {
            List<CONTABLE027> all = new List<CONTABLE027>();
            all = db.CONTABLE027.Where(a => a.COLUMNA13 == false).ToList();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
            var ttid = tid.COLUMN02;
            var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (items == "PDF")
            {
                ExportPdf();
            }
            else if (items == "Excel")
            {
                ExportExcel();
            }
            else if (items == "Word")
            {
                Exportword();
            }
            else if (items == "CSV")
            {
                ExportCSV();
            }
            return View("Index", db.CONTABLE027.Where(a => a.COLUMNA13 == false).ToList());
        }

        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();

                var OperatingUnitID = Request["OperatingUnitID"];
                var Name = Request["Name"];
                var Attention = Request["Attention"];



                if (Name != null && Name != "" && Name != string.Empty)
                {
                    Name = Name + "%";
                }
                else
                    Name = Request["Name"];


                if (Attention != null && Attention != "" && Attention != string.Empty)
                {
                    Attention = Attention + "%";
                }
                else
                    Attention = Request["Attention"];



                if (OperatingUnitID != null && OperatingUnitID != "" && OperatingUnitID != string.Empty)
                {
                    OperatingUnitID = OperatingUnitID + "%";
                }
                else
                    OperatingUnitID = Request["OperatingUnitID"];

                var query = "SELECT * FROM CONTABLE027  WHERE COLUMN03 like '" + Name + "' or COLUMN10 LIKE '" + Attention + "'  or COLUMN02 LIKE '" + OperatingUnitID + "'";

                var query1 = db.CONTABLE027.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.cols = Session["cols"];
                ViewBag.Inlinecols = Session["Inlinecols"];
                ViewBag.gdata = gdata;

                return View("Index", gdata);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult UpdateInline(string list)
        {
            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            string qStr = null;
            var tblID = db.CONTABLE004.Where(q => q.COLUMN04 == "CONTABLE027").Select(q => new { q.COLUMN02 });
            var ID = tblID.Select(a => a.COLUMN02).First();
            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
            {
                for (int x = 0; x < ds.Tables[0].Columns.Count; x++)
                {
                    var colname = ds.Tables[0].Columns[x].ColumnName;
                    var items = db.CONTABLE005.Where(q => q.COLUMN03 == ID && q.COLUMN04 == colname).FirstOrDefault();
                    if (items.COLUMN04 == ds.Tables[0].Columns[x].ColumnName)
                    {
                        if (qStr == null)
                        {
                            qStr += "UPDATE CONTABLE027 SET " + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "'";
                        }
                        else if (x == ds.Tables[0].Columns.Count - 1)
                        {
                            qStr += "," + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "' WHERE COLUMN02=" + ds.Tables[0].Rows[j]["COLUMN02"] + "";
                        }
                        else
                        {
                            qStr += "," + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "'";
                        }
                    }
                }
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand(qStr, cn);
                cn.Open();
                cmd1.ExecuteNonQuery();
                cn.Close();
                qStr = null;
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
            ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
            if (ShowInactives != null)
            {
                var all = db.CONTABLE027.ToList();
                //return View(all);
                return View("Index", db.CONTABLE027.ToList());
            }
            if (Session["Data"] != null)
            {
                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;
                return View("Index", data);

            }
            return View("Index", db.CONTABLE027.Where(a => a.COLUMNA12 == true).ToList());
        }

        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult View1(string items, string sort, string inactive, string style)
        {
            List<CONTABLE027> all = new List<CONTABLE027>();
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
            var ttid = tid.COLUMN02;
            var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (style == "Normal")
            {
                all = all.Where(a => a.COLUMNA12 == true).ToList();
                var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var Attid = Atid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var indata = db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE027 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE027 where COLUMNA13='False' ";
                }
                var db2 = WebMatrix.Data.Database.Open("sqlcon");
                var books = db2.Query(query1);
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();

                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th>Action</th>";
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                }
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td><a href=/OperatingUnit/Edit/" + itm[Inline] + " >Edit</a>|<a href=/OperatingUnit/Details/" + itm[Inline] + " >View</a></td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                        }
                        else
                        {
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                        }
                    }
                    tthdata = "<tr>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                ViewBag.columns = theader;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                return View("Index", db.CONTABLE027.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

            }
            else
            {
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                if (sort == "Recently Created")
                {
                    all = db.CONTABLE027.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    all = db.CONTABLE027.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                else
                {
                    all = db.CONTABLE027.Where(a => a.COLUMNA13 == false).ToList();

                }
                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var Attid = Atid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/OperatingUnit/Edit/" + item[tblcol] + " >Edit</a>|<a href=/OperatingUnit/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.columns = theader;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Index", db.CONTABLE027.Where(a => a.COLUMNA13 == false).ToList());
        }

        public ActionResult ListView()
        {
            return View(db.CONTABLE027.ToList());
        }

        public ActionResult New()
        {
            return View();
        }

        public ActionResult QuickSort(string item)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var ttid = tid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                List<CONTABLE027> all = new List<CONTABLE027>();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                if (item == "Recently Created")
                {
                    all = db.CONTABLE027.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA12 == true).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                else if (item == "Recently Modified")
                {
                    all = db.CONTABLE027.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA12 == true).ToList();
                    all = all.Where(a => a.COLUMNA07 != null).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                else
                {
                    all = db.CONTABLE027.Where(a => a.COLUMNA12 == true).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
              headerStyle: "webgrid-header",
              footerStyle: "webgrid-footer",
              alternatingRowStyle: "webgrid-alternating-row",
              rowStyle: "webgrid-row-style",
                                  htmlAttributes: new { id = "DataTable" },
                                  columns:
                                      grid.Columns
                                      (grid.Column("Action", "Action", format: (items) => new HtmlString("<a href='/OperatingUnit/Edit/" + items.COLUMN02 + "'>Edit</a>|<a href='/OperatingUnit/Details/" + items.COLUMN02 + "'>View</a>")),
                                      grid.Column("COLUMN02", "OperatingUnit ID"),
                                      grid.Column("COLUMN03", "Name"),
                                      grid.Column("COLUMN04", "Description"),
                                      grid.Column("COLUMN09", "Parent Of"),

                                      grid.Column("COLUMN10", "Attention"),
                                      grid.Column("COLUMN23", "RState"),
                                      grid.Column("COLUMN24", "RZip"),
                                      grid.Column("COLUMN26", "Info1"),
                                      grid.Column("COLUMN27", "Info2")
                                      ));
                return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public JsonResult InActive(string ShowInactives)
        {

            List<CONTABLE027> all = new List<CONTABLE027>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            if (ShowInactives != null)
            {
                all = db.CONTABLE027.ToList();
                var data = from e in db.CONTABLE027.AsEnumerable()
                           select new
                           {
                               SubsidaryID = e.COLUMN02,
                               Name = e.COLUMN03,
                               Description = e.COLUMN04,
                               Inactive = e.COLUMN07
                               //, ParentOf = e.COLUMN09, Attention = e.COLUMN10, RState = e.COLUMN20, RZip = e.COLUMN21, Info1 = e.COLUMN22, Info2 = e.COLUMN23 
                           };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            else
            {
                all = db.CONTABLE027.Where(a => a.COLUMNA12 == true).ToList();
                var data = from e in db.CONTABLE027.AsEnumerable()
                           where (e.COLUMNA12 == true)
                           select new
                           {
                               SubsidaryID = e.COLUMN02,
                               Name = e.COLUMN03,
                               Description = e.COLUMN04,
                               Inactive = e.COLUMN07
                               //, ParentOf = e.COLUMN09, Attention = e.COLUMN10, RState = e.COLUMN20, RZip = e.COLUMN21, Info1 = e.COLUMN22, Info2 = e.COLUMN23 
                           };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
      headerStyle: "webgrid-header",
      footerStyle: "webgrid-footer",
      alternatingRowStyle: "webgrid-alternating-row",
      rowStyle: "webgrid-row-style",
                          htmlAttributes: new { id = "DataTable" },
                          columns:
                              grid.Columns
                              (grid.Column("Action", "Action", format: (item) => new HtmlString("<a href='/OperatingUnit/Edit/" + item.SubsidaryID + "'>Edit</a>|<a href='/OperatingUnit/Details/" + item.SubsidaryID + "'>View</a>")),
                              grid.Column("SubsidaryID", "SubsidaryID"),
                              grid.Column("Name", "Name"),
                              grid.Column("Description", "Description"),
                              grid.Column("Inactive", "Inactive"),

                              grid.Column("Attention", "Attention"),
                              grid.Column("RState", "RState"),
                              grid.Column("RZip", "RZip"),
                              grid.Column("Info1", "Info1"),
                              grid.Column("Info2", "Info2")
                              ));
            return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Style(string items, string inactive)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            CONTABLE027 lv = new CONTABLE027();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
            var tbtid = tbid.COLUMN02;
            var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
            ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
            var data = from e in db.CONTABLE027.AsEnumerable()
                       select new
                       {
                           OperatingUnitID = e.COLUMN02,
                           Name = e.COLUMN03,
                           Description = e.COLUMN04,
                           Logo1 = e.COLUMN05,
                           Logo2 = e.COLUMN06,
                           InActive = e.COLUMN07
                           //, ParentOf = e.COLUMN09, Subsidary = e.COLUMN10, Attention = e.COLUMN11, Addressee = e.COLUMN12, Address1 = e.COLUMN13, Address2 = e.COLUMN14, City = e.COLUMN15, State = e.COLUMN16, Zip = e.COLUMN17, Country = e.COLUMN18, RAttention = e.COLUMN19, RAddressee = e.COLUMN20, RAddress1 = e.COLUMN21, RAddress2 = e.COLUMN22, RCity = e.COLUMN23, RState = e.COLUMN24, RZip = e.COLUMN25, RCountry = e.COLUMN26, Info1 = e.COLUMN27, Info2 = e.COLUMN28, Info3 = e.COLUMN29
                       };
            List<CONTABLE027> tbldata = new List<CONTABLE027>();
            tbldata = db.CONTABLE027.Where(a => a.COLUMNA13 == false).ToList();
            if (items == "Normal")
            {
                var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
                var Attid = Atid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == Attid).ToList();
                var indata = db.CONTABLE027.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE027 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE027 where COLUMNA13='False' ";
                }
                var db2 = WebMatrix.Data.Database.Open("sqlcon");
                var books = db2.Query(query1);
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th>Action</th>";
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                }
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td><a href=/OperatingUnit/Edit/" + itm[Inline] + " >Edit</a>|<a href=/OperatingUnit/Details/" + itm[Inline] + " >Details</a></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   '' /></td>";
                        }
                        else
                        {
                            tthdata += "<td>" + itm[Inline] + "</td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    ''  /></td>";
                        }


                    }
                    tthdata = "<tr>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                ViewBag.columns = theader;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
            }
            return View("Index", db.CONTABLE027.Where(a => a.COLUMNA13 == false).ToList());
        }

        //public ActionResult Style(string items)
        //{

        //    List<CONTABLE027> all = new List<CONTABLE027>();
        //    var data = from e in db.CONTABLE027.AsEnumerable() where (e.COLUMNA12 == true) select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Inactive = e.COLUMN07, ParentOf = e.COLUMN09, Attention = e.COLUMN10, RState = e.COLUMN20, RZip = e.COLUMN21, Info1 = e.COLUMN22, Info2 = e.COLUMN23 };

        //    return View("Index", data);
        //}

        public ActionResult CustomView()
        {
            List<CONTABLE005> all = new List<CONTABLE005>();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            //all = from e in db.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
            var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
            all = db.CONTABLE005.SqlQuery(query).ToList();

            return View(all);
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {


            int col2 = 0;
            var tbldata = db.CONTABLE004.Where(a => a.COLUMN05 == "tbl_subsidary").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = db.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = db.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    db.CONTABLE013.Add(dd);
                    db.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        db.CONTABLE013.Add(dd);
                        db.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);

        }

        public ActionResult NewField()
        {

            //var table2= Convert.ToString();
            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
            var ttid = Convert.ToInt32(tid.COLUMN02);
            var SecNames = db.CONTABLE006.Where(a => a.COLUMN12 != null && a.COLUMN04 == ttid).ToList();
            var dsecnames = SecNames.Select(a => a.COLUMN12).Distinct();
            ViewBag.SecNames = dsecnames;
            return View();
        }

        [HttpPost]
        public ActionResult NewField(FormCollection fc)
        {
            CONTABLE006 fm = new CONTABLE006();

            var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE027").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            var query = "SELECT  top( 1 ) * FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04=COLUMN05  ";
            var data = db.CONTABLE005.SqlQuery(query).ToList();
            var tblid = data.Select(a => a.COLUMN03).FirstOrDefault();
            var colname = data.Select(a => a.COLUMN04).FirstOrDefault();
            var alicolname = data.Select(a => a.COLUMN05).FirstOrDefault();
            var con6data = db.CONTABLE006.Where(a => a.COLUMN03 == ttid).FirstOrDefault();
            fm.COLUMN03 = con6data.COLUMN03;
            fm.COLUMN04 = Convert.ToInt32(tblid);
            fm.COLUMN05 = Convert.ToString(colname);
            fm.COLUMN06 = Request["Label_Name"];
            fm.COLUMN07 = "Y";
            fm.COLUMN08 = Request["Mandatory"];
            fm.COLUMN10 = Request["Control_Type"];
            fm.COLUMN11 = Request["Section_Type"];
            fm.COLUMN12 = Request["Section_Name"];
            var pr = con6data.COLUMN13;
            fm.COLUMN13 = (pr + 1).ToString();
            db.CONTABLE006.Add(fm);
            db.SaveChanges();
            return View("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}