﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using MvcMenuMaster.Models;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using System.IO;
using System.ComponentModel.DataAnnotations;
using eBizSuiteUI.Controllers;
using eBizSuiteAppModel.Table;
using WebMatrix.Data;
using System.Web.Helpers;
using MeBizSuiteAppUI.Controllers;
using System.Web.UI;
using iTextSharp.text;
using System.Text;
using eBizSuiteAppDAL.classes;
using eBizSuiteAppUI.Models;
using Microsoft.Reporting.WebForms;
using System.Diagnostics;
using GemBox.Document;
using GemBox.Document.Tables;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using eBizSuiteAppBAL.Fombuilding;
using System.Data.SqlTypes;
using System.Globalization;


namespace MeBizSuiteAppUI.Controllers.Manufacture
{
    public class JobOrderReceiptController : Controller
    {
        //
        // GET: /JobOrderReceipt/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Index()
        {
            return View();
        }
        //display data into webgrid from db for all dynamic forms 
        public ActionResult Info(string FormName, string idi, string Item)
        {
            try
            {
                FormBuildingEntities entity = new FormBuildingEntities();
                FormbuildingInfo info = new FormbuildingInfo();
                Session["DDDynamicItems"] = null;
                if (Session["Reload"] == "Reload" || Session["Reload"] == null)
                {
                    if (Item != null)
                    {
                        ViewBag.GridStyle = Item;
                    }
                    if (FormName != null)
                    {
                        Session["FormName"] = FormName;
                        Session["FormNameContr"] = FormName.Replace(" ", "");
                    }
                    else if (Session["FormName"] != null)
                        FormName = Session["FormName"].ToString();

                    else
                        return RedirectToAction("Logon", "Account");
                    Session["FormName"] = Request["FormName"];
                    if (idi != null)
                    {
                        Session["IDI"] = idi;
                    }
                    else if (Session["IDI"] != null)
                        idi = Session["IDI"].ToString();
                    Session["Accordianid"] = idi;
                    int moduleid = Convert.ToInt32(idi);
                    var acID = (int?)Session["AcOwner"];
                    var cenID = (int?)Session["cenid"];
                    var center = dc.CONTABLE003.Where(a => a.COLUMN02 == moduleid && (a.COLUMN05 == FormName || a.COLUMN10 == FormName) && a.COLUMNA03 == acID && a.COLUMN04 == cenID).ToList();
                    if (center.Count == 0)
                        center = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName && a.COLUMNA03 == acID && a.COLUMN04 == cenID).ToList();
                    var centerid = center.Where(a => a.COLUMN04 != 101).FirstOrDefault();
                    Session["cenid"] = centerid.COLUMN04;
                    Session["FormIDACCRDN1"] = centerid.COLUMN02;
                    string sformnae = "";
                    if (Session["SFormid"] != null) { sformnae = Session["SFormid"].ToString(); }
                    if (sformnae != FormName) { Session["SortByStatus"] = null; }
                    eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                    var nM = Request["FormName"].ToString();
                    var fid = db.CONTABLE0010.Where(q => q.COLUMN04 == nM).FirstOrDefault();
                    var frmID = fid.COLUMN02;
                    Session["id"] = frmID;
                    if (frmID == 1377)
                    {
                        idi = null;
                    }
                    //set redirection path for static path
                    entity = info.ReturnReport(frmID);
                    if (!string.IsNullOrEmpty(entity.View) && !string.IsNullOrEmpty(entity.Controller))
                    {
                        return RedirectToAction(entity.View, entity.Controller, new { FormName = FormName, idi = idi });
                    }
                    //set form link in webgrid for associated forms 
                    entity = info.ReturnLinks(frmID);
                    Session["PForms"] = entity.PForms;
                    ViewBag.PoNo = entity.PoNo;
                    var viewlist = dc.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN05 == frmID).ToList();
                    ViewBag.views = viewlist.Select(a => a.COLUMN03).Distinct().ToList();
                    List<CONTABLE006> con6 = new List<CONTABLE006>();
                    List<CONTABLE013> con13 = new List<CONTABLE013>();
                    var flds = con6;
                    var fldsV = con13;
                    var tblID = 0;
                    string strQry = null;
                    List<string> actCol = new List<string>();
                    List<string> alCol = new List<string>();
                    //query generation to display form wise details in webgrid
                    if (fldsV.Count == 0)
                    {
                        if (Convert.ToString(Session["SFormid"]) != FormName) { Session["SortByStatus"] = null; Session["filterStatus"] = null; } else { Session["filterStatus"] = 1; }
                        //Order Selection Screen
                        entity = info.GetInfo(frmID, Session["ViewStyle"], Session["SortByStatus"],
                                                Session["ViewSort"], Session["OPUnit"].ToString(), Session["AcOwner"].ToString(),
                                                cn, Session["ItemType"], idi, Session["customer"], Session["item"], Session["Family"], Session["Group"], Session["Brand"], Session["State"], Session["City"], Session["DateFrm"], Session["DateTo"], Session["OPUnit"], Session["UPC"], null, null, null, null, null);
                        strQry = entity.strQry;
                        actCol = entity.actCol;
                        alCol = entity.alCol;

                        ViewBag.statusdata = entity.statusdata;
                        ViewData["ItemType"] = new SelectList(entity.Country, "Value", "Text");
                        ViewBag.ItemType = new SelectList(entity.Country, "Value", "Text");

                    }

                    var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(connectionString, providerName);

                    ViewBag.sortselected = Session["SortByStatus"];
                    var sql = strQry;
                    var GData = dbs.Query(sql);
                    var grid = new WebGrid(GData, canPage: false, canSort: false);
                    List<WebGridColumn> cols = new List<WebGridColumn>();
                    int index = 0;
                    foreach (var column in actCol)
                    {
                        cols.Add(grid.Column(alCol[index], alCol[index]));
                        index++;
                    }
                    ViewBag.Columns = cols;
                    if (Session["SDT"] != null)
                    {
                        ViewBag.Grid = Session["SDT"];
                        Session["SDT"] = null;
                    }
                    else
                        ViewBag.Grid = GData;
                    return View("~/Views/Manufacture/JobOrderReceipt/Info.cshtml", new { FormName = Session["FormName"] });
                }
                else
                {
                    Session["Reload"] = null;
                    return Content("");
                }
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult NewForm(string FormName, string idi)
        {

            try
            {
                string idd; int alco = 0;
                //var fid="";
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                List<CONTABLE006> all = new List<CONTABLE006>();
                List<FormBuildClass> all1 = new List<FormBuildClass>();
                List<CONTABLE006> colmap = new List<CONTABLE006>();
                List<CONTABLE0010> fm = new List<CONTABLE0010>();
                if (idi == null)
                {
                    if (FormName != null)
                    {
                        Session["FormName"] = FormName;
                    }
                    else
                    {
                        if (Session["FormName"] != null)
                            FormName = Session["FormName"].ToString();
                        else
                            return RedirectToAction("Logon", "Account");
                    }
					//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                    var id1 =Convert.ToString(Session["Accordianid"]);
                    if (id1 != "")
                    {
                        int moduleid = Convert.ToInt32(id1);
                        var acID = (int?)Session["AcOwner"];
                        var cenID = (int?)Session["cenid"];
                        var center = dc.CONTABLE003.Where(a => a.COLUMN02 == moduleid && a.COLUMN05 == FormName && a.COLUMNA03 == acID && a.COLUMN04 == cenID).ToList();
                        if (center.Count == 0)
                        {
                            center = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName && a.COLUMNA03 == acID && a.COLUMN04 == cenID).ToList();
                            var centerid = center.Where(a => a.COLUMN04 != 101).FirstOrDefault();
                            Session["cenid"] = centerid.COLUMN04;
                            Session["FormIDACCRDN1"] = centerid.COLUMN02;
                        }
                    }
                    int ctblid = 0;
                    var fid = db.CONTABLE0010.Where(q => q.COLUMN04 == FormName).OrderBy(a => a.COLUMN02);
                    var frmID = fid.Select(q => q.COLUMN02).FirstOrDefault();
                    Session["id"] = frmID;
                    var fdata = dc.CONTABLE006.Where(a => a.COLUMN03 == frmID).OrderBy(a => a.COLUMN11);
                    all = fdata.Where(a => a.COLUMN06 != null).ToList();
                    var Ta = all.Where(a => (a.COLUMN11 == "Item Level" || a.COLUMN11 == "Tab" || a.COLUMN11 == "Table") && a.COLUMN07 != "N").ToList();
                   
                    Ta = Ta.Where(a => a.COLUMN06 != null).ToList();
                    var Tabs = Ta.Select(b => b.COLUMN12).Distinct();
                    var itemslist = Ta.Where(b => b.COLUMN11 == "Item Level").OrderBy(q => q.COLUMN02).ToList();
                    var itemscol = itemslist.Where(b => b.COLUMN06 != null).OrderBy(q => q.COLUMN02).ToList();
                    var dynamicddldata = fdata.Where(a => a.COLUMN14 != null && a.COLUMN15 != null).ToList();
                    
                    for (int d = 0; d < dynamicddldata.Count; d++)
                    {
                        var firstname = dynamicddldata[d].COLUMN06; ViewData[firstname] = "";
                        if (dynamicddldata[d].COLUMN14.ToString() == "Control Value")
                        {
                            List<SelectListItem> Country = new List<SelectListItem>();
                            int ddata = Convert.ToInt32(dynamicddldata[d].COLUMN15.ToString());
                            List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                            var acOW = Convert.ToInt32(Session["AcOwner"]);
                            List<MATABLE002> dropdata = new List<MATABLE002>();
                            //if (ddata == 11130 || ddata == 11131 || ddata == 11114 || ddata == 11115 || ddata == 11127 ||
                            //    ddata == 11124 || ddata == 11125 || ddata == 11126 || ddata == 11122 || ddata == 11123 ||
                            //    ddata == 11132 || ddata == 11128 || ddata == 11133 || ddata == 11134 || ddata == 11135 ||
                            //    ddata == 11143 )
                            //    dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata).ToList();
                            //else}
                            dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                            if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                                dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                            else
                                dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString())).ToList();
                            for (int dd = 0; dd < dropdata.Count; dd++)
                            {
                                Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
                            }
                            ViewData[firstname] = new SelectList(Country, "Value", "Text");
                        }
                        else if (dynamicddldata[d].COLUMN14.ToString() == "Master Value")
                        {
                            List<SelectListItem> Country = new List<SelectListItem>();
                            if (frmID == 1272 && dynamicddldata[d].COLUMN05 == "COLUMN03")
                            {
                                SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN03 from PUTABLE002  where  " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                DataTable dtdata2 = new DataTable();
                                cmdd2.Fill(dtdata2); string dcol = "";
                                for (int dd2 = 0; dd2 < dtdata2.Rows.Count; dd2++)
                                {
                                    if (dd2 == 0)
                                        dcol = dtdata2.Rows[dd2][0].ToString();
                                    else
                                        dcol += " or COLUMN02=" + dtdata2.Rows[dd2][0].ToString();
                                }
                                int ddata = Convert.ToInt32(dynamicddldata[d].COLUMN15.ToString());
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                var tblName = tblddl.Select(q => q.COLUMN04).First();
                                if (dcol != "")
                                {
                                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN02=" + dcol + "   AND   " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                }
                                ViewData[firstname] = new SelectList(Country, "Value", "Text");
                            }
                            
                            
                            else if (frmID == 1283 || frmID == 1284 || frmID == 1286 || frmID == 1287 || frmID == 1288)
                            {
                                int ddata = Convert.ToInt32(dynamicddldata[d].COLUMN15.ToString());
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                var tblName = tblddl.Select(q => q.COLUMN04).First(); SqlDataAdapter cmddl = new SqlDataAdapter();
                                if (tblName == "SATABLE009")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN19 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (tblName == "PRTABLE001")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                else if (tblName == "MATABLE009")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN06 ='true' or COLUMN06 ='1' and COLUMN11='False' and COLUMNA13='False'", cn);
                                else if (tblName == "MATABLE011")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN06 ='true' or COLUMN06 ='1' and COLUMN12='False' and COLUMNA13='False'", cn);
                                else if (tblName == "SATABLE005" && frmID == 1286)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "    where  COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (tblName == "SATABLE005" || tblName == "PUTABLE001")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (frmID == 1283 && dynamicddldata[d].COLUMN15 == 110008817 && dynamicddldata[d].COLUMN05 == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where  ("  + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (frmID == 1286 && dynamicddldata[d].COLUMN15 == 110008817 && dynamicddldata[d].COLUMN05 == "COLUMN29")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where ISNULL(COLUMNA13,0) = 0  AND ISNULL(COLUMN78,0) = 1 and COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False'", cn);
                                else if (frmID == 1286 && dynamicddldata[d].COLUMN15 == 110008817 && dynamicddldata[d].COLUMN05 == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False'", cn);
                            
                                else if (tblName == "SATABLE001")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                                else if (tblName == "MATABLE016")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                else if (tblName == "MATABLE017")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                else if (tblName == "CONTABLE007")
									//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where   ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                else if (tblName == "MATABLE007")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                else if (tblName == "SATABLE019")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and (COLUMN13='' or isnull(COLUMN13,0)=0) and isnull(COLUMNA13,'False')='False'", cn);
                                else
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);

                                DataTable dtdata = new DataTable();
                                cmddl.Fill(dtdata);
                                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                {
                                    Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                }
                                ViewData[firstname] = new SelectList(Country, "Value", "Text");
                            }
                            else
                            {
                                int ddata = Convert.ToInt32(dynamicddldata[d].COLUMN15.ToString());
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                var tblName = tblddl.Select(q => q.COLUMN04).First(); SqlDataAdapter cmddl = new SqlDataAdapter();
                                if (tblName == "MATABLE010" && (frmID == 1265 || frmID == 1367))
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (tblName == "PRTABLE001")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                else if (tblName == "PUTABLE001" && frmID == 1329)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE011 where COLUMN04='Return Order' )   AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (tblName == "PUTABLE003" && frmID == 1330)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN17 in (Select COLUMN02 from MATABLE011 where COLUMN04='Return Order' )  AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (tblName == "SATABLE005" && frmID == 1354)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='Return Order' ) and COLUMN04 like 'PR%'  AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (tblName == "SATABLE007" && frmID == 1355)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN04='Return Order' ) and COLUMN04 like 'RI%'  AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                else if (tblName == "MATABLE010" && (frmID == 1260 || frmID == 1415))
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (tblName == "MATABLE010" && frmID == 1293)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + "  Where  COLUMN30 !=1   AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (tblName == "SATABLE001" && (frmID == 1363))
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  Where (COLUMN22=22285 or COLUMN22=null or COLUMN22='') and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMN24='False' and COLUMNA13='False'", cn);
                                else if (tblName == "SATABLE001")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                                else if (tblName == "SATABLE002")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN25='False' and COLUMNA13='False'", cn);
                                else if (tblName == "MATABLE008")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                else if (tblName == "MATABLE007")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN47='False' and COLUMNA13='False'", cn);
                                else if (tblName == "FITABLE001" && (frmID == 1293) && dynamicddldata[d].COLUMN05.ToString() == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07=22344 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and COLUMN12='false' and COLUMNA13='false'", cn);
                                else if (tblName == "FITABLE001" && (frmID == 1261))
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07 IN (22344,22385,22384,22405,22406) AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and COLUMN12='false' and COLUMNA13='false'", cn);
                                else if (tblName == "FITABLE001" && (frmID == 1363 || frmID == 1386) && dynamicddldata[d].COLUMN05.ToString() == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07!=22266 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and COLUMN12='false' and COLUMNA13='false'", cn);
                                else if (tblName == "FITABLE001" && (frmID == 1358 || frmID == 1363 || frmID == 1386 || frmID == 1293))
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07=22266 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and COLUMN12='false' and COLUMNA13='false'", cn);
                                else if (tblName == "MATABLE013")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN06 ='true' or COLUMN06 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and COLUMN15='false' and COLUMNA13='false'", cn);
                                //cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND " + Session["OPUnitWithNull"] + " AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)and COLUMN15='False' and COLUMNA13='False'", cn);
                                else if (tblName == "MATABLE011" || tblName == "MATABLE009")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 as COLUMN04 from " + tblName + "  ", cn);
                                else if (tblName == "MATABLE016")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                else if (tblName == "MATABLE017")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                else if (tblName == "FITABLE020")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN21 as COLUMN04 from " + tblName + " where  COLUMN16!=(select column04 from contable025 where column02=63) and  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                //else if (tblName == "FITABLE001" && frmID == 1293 && dynamicddldata[d].COLUMN05 == "COLUMN03")
                                //    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  " +Session["OPUnitWithNull"]+ "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN07=22344", cn);
                                else if (tblName == "CONTABLE007" && (frmID == 1260 || frmID == 1415))
									//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!=''  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                else if (tblName == "CONTABLE007")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                else if (tblName == "MATABLE003")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='false' AND COLUMN12='false'", cn);
                                else if (tblName == "MATABLE004")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='false' AND COLUMN12=0", cn);
                                else if (tblName == "MATABLE005")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='false' AND COLUMN12=0", cn);
                                else if (tblName == "CONTABLE009")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='false' AND COLUMN07='false'", cn);


                                else
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='false'", cn);

                                DataTable dtdata = new DataTable();
                                cmddl.Fill(dtdata);
                                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                {
                                    Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                }
                                ViewData[firstname] = new SelectList(Country, "Value", "Text");
                            }
                        }
                        else if (dynamicddldata[d].COLUMN14.ToString() == "GridView")
                        {
                            var sql = "";
                            int ddata = Convert.ToInt32(dynamicddldata[d].COLUMN15.ToString());
                            var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                            var tbleID = Convert.ToInt32(Session["TBLID"]);
                            var modID = tblddl.Where(q => q.COLUMN02 == ddata).Select(q => q.COLUMN03).First();
                            var tblName = tblddl.Where(q => q.COLUMN03 == modID && q.COLUMN02 == ddata).Select(q => q.COLUMN04).First();
                            var tblNameID = tblddl.Select(q => q.COLUMN02).First();
                            var formID = dc.CONTABLE006.Where(a => a.COLUMN04 == tblNameID && a.COLUMN07 == "Y").OrderBy(q => q.COLUMN01).Take(4).ToList();
                            List<string> colNM = new List<string>();
                            List<string> TDataC = new List<string>();
                            if (formID.Count == 0)
                            {
                                //var colLST = dc.CONTABLE005.Where(a=>a.COLUMN03==ddata).Select(q => q.COLUMN05).ToList();
                                //foreach (string dtr in colLST)
                                //{
                                //    colNM.Add(dtr);
                                //}
                                colNM.Add("Item"); colNM.Add("Qty OnHand"); colNM.Add("Qty OnOrder"); colNM.Add("Qty Committed"); colNM.Add("Qty Available"); colNM.Add("Qty Backordered"); colNM.Add("Qty Intransit");
                                ViewBag.EFormName = "";
                                sql = "select  (select column04 from MATABLE007 where COLUMN02=FITABLE010.COLUMN03) Item,COLUMN04 as [Qty OnHand],COLUMN05 as [Qty OnOrder],COLUMN06 as [Qty Committed],COLUMN07 as [Qty Available],COLUMN08  as [Qty Backordered],COLUMN09  as [Qty Intransit] from " + tblName + "   Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'";
                            }
                            else
                            {
                                var frmIDE = formID.Select(q => q.COLUMN03).First();
                                var colLST = formID.Select(q => q.COLUMN06).ToList();
                                var formName = db.CONTABLE0010.Where(q => q.COLUMN02 == frmIDE);
                                var formNameE = formName.Select(q => q.COLUMN04).First();
                                foreach (string dtr in colLST)
                                {
                                    if (frmIDE == 1363 || frmIDE == 1386)
                                    {
                                        if (dtr != "Tax")
                                            colNM.Add(dtr);
                                    }
                                    else
                                    {
                                        colNM.Add(dtr);
                                    }
                                }
                                ViewBag.EFormName = formNameE;
                               
                                    sql = "select COLUMN02 AA ,COLUMN03 as [" + colNM[0] + "],COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN06 as [" + colNM[3] + "] from " + tblName + "   Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'";
                                TDataC.Add("COLUMN02");
                            }
                            //SqlDataAdapter cmddlC = new SqlDataAdapter("select COLUMN02 ,COLUMN03 as [" + colNM[0] + "],COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN06 as [" + colNM[3] + "] from " + tblName + "", cn);
                            //DataTable dtdataC = new DataTable();
                            //cmddlC.Fill(dtdataC);
                            //List<DataRow> eData = new List<DataRow>();
                            //foreach (DataRow dtr in dtdataC.Rows)
                            //{
                            //    eData.Add(dtr);
                            //}
                            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                            var providerName = "System.Data.SqlClient";
                            var dbs = Database.OpenConnectionString(connectionString, providerName);
                            var GData = dbs.Query(sql);
                            var grid = new WebGrid(GData, canPage: false, canSort: false);
                            foreach (string dtr in colNM)
                            {

                                TDataC.Add(dtr);
                            }
                            ViewBag.GridDynamicData = GData;
                            ViewBag.GridDynamicDataC = TDataC;
                            ViewBag.TableIDE = ddata;
                        }
                    }


                    ArrayList cols = new ArrayList();
                    ArrayList JOIRcols = new ArrayList();
                    for (int col = 0; col < itemscol.Count; col++)
                    {
                        ctblid = Convert.ToInt32(itemscol[col].COLUMN04);
                        var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                        var ctname = ctbldata.COLUMN04;
                        var cid = (ctname + itemscol[col].COLUMN05);
                        cols.Add(cid);
                    }
                    if (frmID == 1286)
                    {
                        itemscol = itemscol.Where(a => a.COLUMN04 == 110010817 && a.COLUMN03 == 1286).ToList();
                        for (int col = 0; col < itemscol.Count; col++)
                        {
                            ctblid = Convert.ToInt32(itemscol[col].COLUMN04);
                            var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                            var ctname = ctbldata.COLUMN04;
                            var cid = (ctname + itemscol[col].COLUMN05);
                            JOIRcols.Add(cid);
                        }
                    }
                    if (frmID == 1286)
                        ViewBag.NewCols = JOIRcols;
                    else
                        ViewBag.NewCols = cols;
                    var result = new List<dynamic>();
                    if (itemslist.Count > 0)
                    {
                        var obj = (IDictionary<string, object>)new System.Dynamic.ExpandoObject();
                        foreach (var row1 in itemslist)
                        {
                            ctblid = Convert.ToInt32(row1.COLUMN04);
                            var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                            var ctname = ctbldata.COLUMN04;
                            row1.COLUMN06 = (ctname + row1.COLUMN05);
                            obj.Add(row1.COLUMN06, row1.COLUMN06);
                        }
                        result.Add(obj);
                    }

                    var fddl = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).Select(q => new { q.COLUMN06 });
                    var fid6 = fddl.Select(w => w.COLUMN06).First();
                    var ddl = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName || a.COLUMN06 == fid6 || a.COLUMN02 == frmID);
                    ViewBag.ddl = ddl.Select(a => a.COLUMN04).ToList();
                    ViewBag.itemslist = result;
                    ViewBag.itemscol = cols;
                    ViewBag.Tabs = Tabs;
                    var FName = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).First();
                    FormName = FName.COLUMN04.ToString();
                    ViewBag.selected = FormName;
                    Session["FormName"] = FormName;
                    var tbl = fdata.OrderBy(a => a.COLUMN13).Select(a => a.COLUMN04).Distinct().ToList();
                    
                    int? tabindex = 0; string tabname = "";
                    for (int irow = 0; irow < all.Count; irow++)
                    {
                        for (int i = 0; i < tbl.Count; i++)
                        {
                            int tblid = Convert.ToInt32(tbl[i]); SqlCommand acmd = new SqlCommand();
                            if (frmID == 1330 || frmID == 1286 || frmID == 1322)
                            {
                                acmd = new SqlCommand(
                                   "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                                 "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                                 "where CONTABLE005.COLUMN03 =" + tblid + " and CONTABLE006.COLUMN04 =" + tblid + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04  and CONTABLE006.COLUMN11!='Grid Level' and CONTABLE006.COLUMN03=" + frmID + " ORDER BY CAST(CONTABLE006.COLUMN13 AS INT)  ", cn);
                            }
                            
                            cn.Open();
                            SqlDataAdapter ada = new SqlDataAdapter(acmd);
                            DataTable adt = new DataTable();
                            ada.Fill(adt);
                            acmd.ExecuteNonQuery();
                            cn.Close();
                            for (int row = 0; row < adt.Rows.Count; row++)
                            {
                                all1.Add(new MeBizSuiteAppUI.Controllers.FormBuildClass { Field_Name = "", Label_Name = "", Action = "", Mandatory = "", Default_Value = "", Control_Type = "", Section_Type = "", Section_Name = "", Form_Id = null, Table_Id = null, Data_Type = "", Tab_Index = null });
                                SqlCommand pacmd = new SqlCommand(); string pono = ""; string fino = "";

                                ctblid = Convert.ToInt32(adt.Rows[row][9]);
                                var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                                var ctname = ctbldata.COLUMN04;
                                all1[alco + row].Field_Name = (ctname + adt.Rows[row][0].ToString());
                                all1[alco + row].Label_Name = (adt.Rows[row][1].ToString());
                                all1[alco + row].Default_Value = (adt.Rows[row][4].ToString());
                                all1[alco + row].Action = (adt.Rows[row][2].ToString());
                                all1[alco + row].Mandatory = (adt.Rows[row][3].ToString());
                                all1[alco + row].Control_Type = (adt.Rows[row][5].ToString());
                                all1[alco + row].Section_Type = (adt.Rows[row][6].ToString());
                                all1[alco + row].Section_Name = (adt.Rows[row][7].ToString());
                                all1[alco + row].Form_Id = Convert.ToInt32((adt.Rows[row][8].ToString()));
                                all1[alco + row].Table_Id = Convert.ToInt32((adt.Rows[row][9].ToString()));
                                all1[alco + row].Data_Type = (adt.Rows[row][10].ToString());
                                all1[alco + row].Source_Type = (adt.Rows[row][11].ToString());
                                all1[alco + row].Source_Value = (adt.Rows[row][12].ToString());
                                all1[alco + row].Tab_Index = Convert.ToInt32((adt.Rows[row]["COLUMN13"].ToString()));
                                //if (alco + row == 0)
                                //{
                                //    all1[alco + row].Tab_Index = tabindex;
                                //}
                                //else
                                //{
                                //    if (tabname != adt.Rows[row][7].ToString())
                                //    {
                                //        tabindex = tabindex + 1;
                                //        tabname = adt.Rows[row][7].ToString();
                                //    } all1[alco + row].Tab_Index = tabindex;
                                //}
                            }
                            alco += adt.Rows.Count;
                            var tname = dc.CONTABLE004.Where(a => a.COLUMN02 == tblid).First();
                            var tblname = tname.COLUMN04;
                            SqlCommand cmd = new SqlCommand("select  column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tblname + "'", cn);
                            SqlCommand cmdd = new SqlCommand("select  * from " + tblname + "   ", cn);
                            //where Column02=" + idi + "
                            cn.Open();
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                            DataTable dt = new DataTable();
                            DataTable dtd = new DataTable();
                            da.Fill(dt);
                            dad.Fill(dtd);
                            cn.Close();

                            string col = "COLUMN0" + irow;
                            int b = 01, d = 01;
                            var bcol = "COLUMNB0"; var dcol = "COLUMND0"; var scol = "COLUMN0";
                            irow = irow + (dtd.Columns.Count);
                        }
                    }
                    if (frmID == 1330 || frmID == 1285 || frmID == 1286)
                    {
                        all1 = (from p in all1
                                where p.Label_Name != null
                                orderby (p.Tab_Index)
                                select p).ToList();
                    }
                }
                else
                {
                }
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                
                return View("~/Views/Manufacture/JobOrderReceipt/NewForm.cshtml", all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("~/Views/Manufacture/JobOrderReceipt/Info.cshtml", "JobOrderReceipt", new { FormName = Session["FormName"] });
            }
        }

        SqlCommand Cmd;
        string[] customerparam;
        string[] customerparam1;
        [HttpPost]
        public ActionResult NewForm(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
			    //EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                int OutParam = 0;
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["GridData"];
                int saveformid = Convert.ToInt32(Session["id"]);
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN02 == saveformid).ToList();
                var fname = fnamedata.Select(a => a.COLUMN04).FirstOrDefault();
                Session["FormName"] = fname;
                var formid = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var ftid=formid.COLUMN02; 
                var transno = 0;
                var errormsg = "";
                var fd = dc.CONTABLE006.Where(a => a.COLUMN07 == "Y" && a.COLUMN03 == saveformid);
                var tbl = fd.Select(a => a.COLUMN04).Distinct().ToList();
                var distLocations = (from li in fd.AsEnumerable()
                                     orderby li.COLUMN01
                                     select new { Location = li.COLUMN04 }).Distinct().ToList();
                tbl = distLocations.Select(a => a.Location).ToList();

                for (int t = 0; t < tbl.Count; t++)
                {
                    int tblid = Convert.ToInt32(tbl[t]);
                    var tbldlist = dc.CONTABLE004.Where(a => a.COLUMN02 == tblid);
                    var table = dc.CONTABLE006.Where(a => a.COLUMN04 == tblid);
                    var tname = tbldlist.FirstOrDefault().COLUMN04;
                    string savedata = "Select a.COLUMN06,a.COLUMN05,a.COLUMN11,a.COLUMN12,b.COLUMN06, a.COLUMN07, a.COLUMN08,a.COLUMN09, a.COLUMN10,a.COLUMN03,a.COLUMN04,a.COLUMN14,a.COLUMN15 From CONTABLE006 a " +
                          "Inner Join CONTABLE005 b on a.COLUMN04 = b.COLUMN03 " +
                          "where b.COLUMN03 =" + tblid + " and a.COLUMN04 =" + tblid + " and a.COLUMN05 = b.COLUMN04 and a.COLUMN03=" + formid + " and a.column07='Y'  and a.column11!='Grid Level'  ";
                    if (formid == 1285)
                    {
                        savedata = "Select a.COLUMN06,a.COLUMN05,a.COLUMN11,a.COLUMN12,b.COLUMN06, a.COLUMN07, a.COLUMN08,a.COLUMN09, a.COLUMN10,a.COLUMN03,a.COLUMN04,a.COLUMN14,a.COLUMN15 From CONTABLE006 a " +
                            "Inner Join CONTABLE005 b on a.COLUMN04 = b.COLUMN03 " +
                            "where b.COLUMN03 =" + tblid + " and a.COLUMN04 =" + tblid + " and a.COLUMN05 = b.COLUMN04 and a.COLUMN03=" + formid + " and a.column07='Y'    ";
                    }

                    SqlCommand acmd = new SqlCommand("select  column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tname + "'", cn);
                    SqlCommand cmd = new SqlCommand(savedata, cn);
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    SqlDataAdapter ada = new SqlDataAdapter(acmd);
                    DataTable dt = new DataTable();
                    DataTable adt = new DataTable();
                    da.Fill(dt);
                    ada.Fill(adt);
                    cn.Close();
                    SqlCommand cmddt = new SqlCommand("select  COLUMN06 from CONTABLE005 where COLUMN03='" + tblid + "' and COLUMN04!='COLUMN02' and COLUMN04!='COLUMN01'", cn);
                    cn.Open();
                    SqlDataAdapter dadt = new SqlDataAdapter(cmddt);
                    DataTable dtdt = new DataTable();
                    dadt.Fill(dtdt);
                    cn.Close();
                    int val1 = 0;
                    StringBuilder value = new StringBuilder();
                    string values = null;
                    SqlCommand cmdd = new SqlCommand();
                    SqlDataAdapter daa = null;
                    DataTable dtt = null; var itemlevel = dt.Rows[0][2].ToString();
                    if (formid == 1285 || formid == 1359)
                        itemlevel = dt.Rows[1][2].ToString();

                    if (itemlevel == "Item Level")
                    {
                        for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                        {
                            if ((formid == 1278 || formid == 1274) && (tbl[t] == 110010831 || tbl[t] == 110013417) && i > 0)
                            {
                                cn.Open();
                                string paymentstable; var fino = "";
                                if (formid == 1278)
                                {
                                    Cmd = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
                                    paymentstable = "SATABLE011";
                                    fino = "CP";
                                }
                                else
                                {
                                    Cmd = new SqlCommand("usp_PUR_BL_PAYBILL", cn);
                                    paymentstable = "PUTABLE014";
                                    fino = "PB";
                                }
                                Cmd.CommandType = CommandType.StoredProcedure;
                                Cmd.Parameters.Clear();
                                for (int p = 0; p < customerparam.Length; p++)
                                {
                                    Cmd.Parameters.AddWithValue("@" + customerparam1[p] + "", customerparam[p].ToString());
                                }
                                SqlCommand cmdi = new SqlCommand("select max(COLUMN02) from " + paymentstable + "", cn);
                                SqlDataAdapter dai = new SqlDataAdapter(cmdi);
                                DataTable dti = new DataTable();
                                dai.Fill(dti);
                                int COLUMN02 = Convert.ToInt32(dti.Rows[0][0].ToString());
                                //Transaction No Generation
                                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == fname && a.COLUMN11 == "default" && a.COLUMNA02 == Oper && a.COLUMNA03 == acOW).FirstOrDefault();
                                if (listTM == null)
                                {
                                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == fname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                                }
                                var Prefix = ""; var TransactionNo = "";
                                var sNum = "";
                                int c = 0;
                                SqlCommand pacmd = new SqlCommand();
                                if (listTM != null)
                                {
                                    Prefix = listTM.COLUMN06.ToString();
                                    sNum = listTM.COLUMN07.ToString();
                                    c = Prefix.Length + 1;
                                }
                                else
                                {
                                    Prefix = null;
                                } if (Prefix != null)
                                {
                                    fino = Prefix;
                                    pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM " + paymentstable + "  Where " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM " + paymentstable + "  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                                }
                                else
                                {
                                    pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM " + paymentstable + "  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM " + paymentstable + "  Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                                }
                                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                                DataTable padt = new DataTable();
                                pada.Fill(padt);
                                pacmd.CommandTimeout = 0;
                                pacmd.ExecuteNonQuery();
                                if (padt.Rows.Count > 0)
                                {
                                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                                    {
                                        TransactionNo = fino + 1000.ToString();
                                    }
                                    else
                                    {
                                        TransactionNo = fino + (Convert.ToInt32(padt.Rows[0][0].ToString()) + 1);
                                    }
                                }
                                else
                                {
                                    TransactionNo = fino + 1000.ToString();
                                }

                                Cmd.Parameters["@COLUMN02"].Value = COLUMN02 + 1;
                                Cmd.Parameters["@COLUMN04"].Value = TransactionNo;
                                string insert1 = "Insert";
                                Cmd.Parameters.AddWithValue("@Direction", insert1);
                                Cmd.Parameters.AddWithValue("@TabelName", paymentstable);
                                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                                Cmd.CommandTimeout = 0;
                                int m = Cmd.ExecuteNonQuery();
                                cn.Close();
                            }
                            if (formid == 1286 && tblid == 110008817)
                            {
                                break;
                            }
                            for (int k = 0; k < dt.Rows.Count; k++)
                            {
                                var colname = dt.Rows[k][1].ToString(); colname = tname + colname;
                                if (k == 0)
                                {
                                    cmdd = new SqlCommand("select  Max(COLUMN02) from " + tname + " ", cn);
                                    daa = new SqlDataAdapter(cmdd);
                                    cn.Open();
                                    dtt = new DataTable();
                                    daa.Fill(dtt);
                                    cn.Close();
                                    values = adt.Rows[1][0].ToString();
                                    var firstcol = dtt.Rows[0][0].ToString();
                                    if (firstcol == null || firstcol == "")
                                    {
                                        value.Append(1000).ToString();
                                    }
                                    else
                                    {
                                        val1 = Convert.ToInt32(dtt.Rows[0][0].ToString());
                                        value.Append(((val1 + 1)).ToString());
                                    }
                                    values += "`" + dt.Rows[k][1].ToString();
                                    if (dt.Rows[k][0].ToString() == "Refered Form")
                                        value.Append("`" + formid);
                                    else if (dt.Rows[k][1].ToString() == "COLUMN05" && formid == 1378 && tblid == 110010871)
                                    {
                                        value.Append("`" + ((val1 + 1)).ToString());
                                    }

                                    else if ((formid == 1286) && itemdata.Tables[0].Columns.Contains("UPC"))
                                    {
                                        Cmd.Parameters.AddWithValue("@COLUMNB04", itemdata.Tables[0].Rows[i]["UPC"].ToString());
                                    }
                                    else
                                    {
                                        var coldata = itemdata.Tables[0].Rows[i][colname].ToString();
                                        value.Append("`" + coldata);
                                    }
                                }
                                else
                                {
                                    var coldata = itemdata.Tables[0].Rows[i][colname].ToString();
                                    values += "`" + dt.Rows[k][1].ToString();
                                    if (coldata == "" || coldata == null)
                                    {
                                        if (dt.Rows[k][4].ToString() == "NVARCHAR" || dt.Rows[k][4].ToString() == "nvarchar" || dt.Rows[k][4].ToString() == "DATETIME" || dt.Rows[k][4].ToString() == "datetime" || dt.Rows[k][4].ToString() == "DATE" || dt.Rows[k][4].ToString() == "date")
                                        { value.Append("`" + DBNull.Value); }
                                        else
                                        { coldata = "0"; value.Append("`" + coldata); }
                                    }
                                    else
                                    {
                                        if (dt.Rows[k][4].ToString() == "image" || dt.Rows[k][4].ToString() == "IMAGE")
                                        {
                                            var image = Request.Files[0] as HttpPostedFileBase;
                                            if (image != null)
                                            {
                                                if (Request.Files.Count > 0)
                                                {
                                                    if (image.ContentLength > 0)
                                                    {
                                                        LogWriter logs = new LogWriter();
                                                        string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                                        var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == fname).FirstOrDefault().COLUMN06.ToString();
                                                        int moduleid = Convert.ToInt32(moduleid1.ToString());
                                                        string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                                        string fileName = Path.GetFileName(image.FileName);
                                                        string path1 = logs.ImageFile(fname, modulename);
                                                        //imagepath = imagepath + "Images" + "/" + fileName;
                                                        string path = Path.Combine(Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + fname + ""), fileName);
                                                        image.SaveAs(path);
                                                        value.Append("`" + fileName);
                                                    }
                                                    else
                                                    {
                                                        value.Append("`" + DBNull.Value);
                                                    }
                                                }
                                            }
                                        }
                                        else { value.Append("`" + coldata); }

                                    }
                                }
                            }
							//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                            if (itemdata.Tables[0].Columns.Contains("PUTABLE004COLUMN26"))
                            {
                                if (formid == 1286)
                                    values += "`" + "COLUMN26";
                                value.Append("`" + itemdata.Tables[0].Rows[i]["PUTABLE004COLUMN26"].ToString());
                            }
							//EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                            values += "`" + "COLUMN12";value.Append("`" + OutParam);
                            values += "`" + "COLUMNA03" + "`" + "COLUMNA06" + "`" + "COLUMNA07" + "`" + "COLUMNA12" + "`" + "COLUMNA13" + "`" + "COLUMNA08";
                            value.Append("`" + Session["AcOwner"] + "`" + DateTime.Now + "`" + DateTime.Now + "`" + "1" + "`" + "0" + "`" + Session["eid"]);
                            SqlCommand cmd1 = new SqlCommand("insert into " + tname + "(" + values + ") values(" + value + ")", cn);
                            cn.Open();
                            string[] param = value.ToString().Split('`');
                            string[] param1 = values.Split('`');

                            if (formid == 1286)
                            {
                                Cmd = new SqlCommand("usp_JO_BL_JO_RECEIPT", cn);
                            }
                            
                            Cmd.CommandType = CommandType.StoredProcedure;
                            for (int p = 0; p < param.Length; p++)
                            {
                                Cmd.Parameters.AddWithValue("@" + param1[p] + "", param[p].ToString());
                            }

                            string insert = "Insert";
                            Cmd.Parameters.AddWithValue("@Direction", insert);
                            Cmd.Parameters.AddWithValue("@TabelName", tname);
                            Cmd.Parameters.AddWithValue("@ReturnValue", "");
                            Cmd.CommandTimeout = 0;
                            int r = Cmd.ExecuteNonQuery();
                            cn.Close();
                            value = new StringBuilder();
                            values = null;
                            if (r > 0)
                            {
                                eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                                lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " succeeded at ");

                                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 1).FirstOrDefault();
                                var msg = string.Empty;
                                if (msgMaster != null)
                                {
                                    msg = msgMaster.COLUMN03;
                                }
                                else
                                    msg = "Successfully Created..... ";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "Success";
                            }
                            else
                            {
                                eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                                lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " has failed at ");

                                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 0).FirstOrDefault();
                                var msg = string.Empty;
                                if (msgMaster != null)
                                {
                                    msg = msgMaster.COLUMN03;
                                }
                                else
                                    msg = "Data Creation Failed....... ";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "fail";
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                cmdd = new SqlCommand("select  Max(COLUMN02) from " + tname + " ", cn);
                                daa = new SqlDataAdapter(cmdd);
                                cn.Open();
                                dtt = new DataTable();
                                daa.Fill(dtt);
                                cn.Close();
                                values = adt.Rows[1][0].ToString();
                                var firstcol = dtt.Rows[0][0].ToString();
                                if (firstcol == null || firstcol == "")
                                {
                                    value.Append(10000).ToString();
                                }
                                else
                                {
                                    val1 = Convert.ToInt32(dtt.Rows[0][0].ToString());
                                    value.Append(((val1 + 1)).ToString());
                                }
                                var str = col[dt.Rows[i][0].ToString()];
                                values += "`" + dt.Rows[i][1].ToString();
                                var colname = dt.Rows[i][1].ToString();
                                var variables = dc.CONTABLE005.Where(a => a.COLUMN03 == tblid && a.COLUMN04 == colname).FirstOrDefault();

                                if (variables.COLUMN05 == "Refered Form")
                                {
                                    value.Append("`" + formid);
                                }
                                else if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT" || variables.COLUMN06 == "image" || variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL" || variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT" || variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                {
                                    if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            value.Append("`1");
                                        }
                                    }
                                    else if (variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`" + DBNull.Value);
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`" + DBNull.Value + "");
                                            else
                                            {
                                                var dateVal = DateTime.ParseExact(col[dt.Rows[i][0].ToString()], Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                                                value.Append("`" + dateVal);
                                            }
                                        }
                                    }
                                    else if (variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "image")
                                    {
                                        var image = Request.Files[0] as HttpPostedFileBase;
                                        if (image != null)
                                        {
                                            if (Request.Files.Count > 0)
                                            {
                                                if (image.ContentLength > 0)
                                                {
                                                    LogWriter logs = new LogWriter();
                                                    string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                                    var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == fname).FirstOrDefault().COLUMN06.ToString();
                                                    int moduleid = Convert.ToInt32(moduleid1.ToString());
                                                    string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                                    string fileName = Path.GetFileName(image.FileName);
                                                    string path1 = logs.ImageFile(fname, modulename);
                                                    //imagepath = imagepath + "Images" + "/" + fileName;
                                                    string path = Path.Combine(Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + fname + ""), fileName);
                                                    image.SaveAs(path);
                                                    value.Append("`" + fileName);
                                                }
                                                else
                                                {
                                                    value.Append("`" + DBNull.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string cname = dt.Rows[i][0].ToString();
                                    if (str == null)
                                    {
                                        if (cname == "Refered Form")
                                            value.Append("`" + formid);
                                        else
                                            value.Append("`" + DBNull.Value);
                                    }
                                    else
                                    {
                                        var colval = col[dt.Rows[i][0].ToString()];
                                        if (colval == "")
                                            value.Append("`" + DBNull.Value);
                                        else
                                            value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                    }
                                }
                            }
                            else
                            {
                                if (formid == 1251 || formid == 1275)
                                {
                                    var poval = col[dt.Rows[1][0].ToString()]; var pochk = "";
                                    if (formid == 1251)
                                    {
                                        cmdd = new SqlCommand("select * from  PUTABLE001 where COLUMN04 ='" + poval + "' and COLUMN04 !='' AND   " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                        pochk = "PurchaseOrder Already Existed With This PONO";
                                    }
                                    else
                                    {
                                        cmdd = new SqlCommand("select * from  SATABLE005 where COLUMN04 ='" + poval + "' and COLUMN04 !='' AND   " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                        pochk = "SalesOrder Already Existed With This SONO";
                                    }
                                    daa = new SqlDataAdapter(cmdd);
                                    cn.Open();
                                    dtt = new DataTable();
                                    daa.Fill(dtt);
                                    cn.Close();
                                    if (dtt.Rows.Count > 0)
                                    {
                                        try
                                        {
                                            throw new Exception(message: pochk);
                                        }
                                        catch (Exception ex)
                                        {
                                            Session["MessageFrom"] = ex.Message;
                                            Session["SuccessMessageFrom"] = "fail";
                                        }
                                        return RedirectToAction("FormBuild", new { FormName = Session["FormName"] });
                                    }
                                }
                                var str = col[dt.Rows[i][0].ToString()];
                                values += "`" + dt.Rows[i][1].ToString();
                                var colname = dt.Rows[i][1].ToString();
                                var variables = dc.CONTABLE005.Where(a => a.COLUMN03 == tblid && a.COLUMN04 == colname).FirstOrDefault();
                                if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT" || variables.COLUMN06 == "image" || variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL" || variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT" || variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                {
                                    if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            value.Append("`1");
                                        }
                                    }
                                    else if (variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`" + DBNull.Value);
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`" + DBNull.Value + "");
                                            else
                                            {
                                                var dateVal = DateTime.ParseExact(col[dt.Rows[i][0].ToString()], Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                                                value.Append("`" + dateVal);
                                            }
                                        }
                                    }
                                    else if (variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "image")
                                    {
                                        var image = Request.Files[0] as HttpPostedFileBase;
                                        if (image != null)
                                        {
                                            if (Request.Files.Count > 0)
                                            {
                                                if (image.ContentLength > 0)
                                                {
                                                    LogWriter logs = new LogWriter();
                                                    string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                                    var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == fname).FirstOrDefault().COLUMN06.ToString();
                                                    int moduleid = Convert.ToInt32(moduleid1.ToString());
                                                    string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                                    string fileName = Path.GetFileName(image.FileName);
                                                    string path1 = logs.ImageFile(fname, modulename);
                                                    //imagepath = imagepath + "Images" + "/" + fileName;
                                                    string path = Path.Combine(Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + fname + ""), fileName);
                                                    image.SaveAs(path);
                                                    value.Append("`" + fileName);
                                                }
                                                else
                                                {
                                                    value.Append("`" + DBNull.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (str == null)
                                    {
                                        value.Append("`" + DBNull.Value);
                                    }
                                    else
                                    {
                                        var colval = col[dt.Rows[i][0].ToString()];
                                        if (colval == "")
                                            value.Append("`" + DBNull.Value);
                                        else
                                            value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                    }
                                }
                            }
                        }
                        values += "`" + "COLUMNA03" + "`" + "COLUMNA06" + "`" + "COLUMNA07" + "`" + "COLUMNA12" + "`" + "COLUMNA13" + "`" + "COLUMNA08";
                        value.Append("`" + Session["AcOwner"] + "`" + DateTime.Now + "`" + DateTime.Now + "`" + "1" + "`" + "0" + "`" + Session["eid"]);
                        SqlCommand cmd1 = new SqlCommand("insert into " + tname + "(" + values + ") values(" + value + ")", cn);
                        cn.Open();
                        string[] param = value.ToString().Split('`');
                        string[] param1 = values.Split('`');
                        customerparam = value.ToString().Split('`');
                        customerparam1 = values.Split('`');
                        
                        if (formid == 1272 || formid == 1286)
                        {
                            Cmd = new SqlCommand("usp_JO_BL_JO_RECEIPT", cn);
                        }
                       
                        Cmd.CommandType = CommandType.StoredProcedure;
                        for (int p = 0; p < param.Length; p++)
                        {
                            Cmd.Parameters.AddWithValue("@" + param1[p] + "", param[p].ToString());
                        }

                        string insert = "Insert";
                        Cmd.Parameters.AddWithValue("@Direction", insert);
                        Cmd.Parameters.AddWithValue("@TabelName", tname);
						//EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                        //Cmd.Parameters.AddWithValue("@ReturnValue", "");
                        Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;

                        Cmd.CommandTimeout = 0;
                        int r = Cmd.ExecuteNonQuery();
                        OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                        //int r = 0;
                        cn.Close();
                        if (r > 0)
                        {
                            eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                            //7/19/2015 auto generation update for null operating unit by srinivas
                            Oper = Convert.ToInt32(Session["OPUnit1"]);
                            if (Oper == 0)
                                Oper = null;
                            var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == fname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                            if (Oper == null)
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == fname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                            if (listTM == null)
                            {
                                listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == fname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                                if (listTM == null)
                                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == fname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                            }
                            if (listTM != null)
                            {
                                if (transno != 1)
                                {
                                    transno = 1;
                                    MYTABLE002 product = listTM;
                                    if (listTM.COLUMN07 == "") listTM.COLUMN07 = "1";
                                    if (listTM.COLUMN09 == "")
                                        product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN07)).ToString();
                                    else
                                        product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                                    act1.SaveChanges();
                                }
                            }
                            eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                            lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " has succeeded at ");

                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 1).FirstOrDefault();
                            var msg = string.Empty;
                            if (msgMaster != null)
                            {
                                msg = msgMaster.COLUMN03;
                            }
                            else
                                msg = "Successfully Created........ ";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "Success";
                        }
                        else
                        {
                            eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                            lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " has failed at ");

                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 0).FirstOrDefault();
                            var msg = string.Empty;
                            if (msgMaster != null)
                            {
                                msg = msgMaster.COLUMN03;
                            }
                            else
                                msg = "Data Creation Failed.......... ";
                            Session["MessageFrom"] = msg;
                            t = tbl.Count();
                            Session["SuccessMessageFrom"] = "fail";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                lg.CreateFile(Server.MapPath("~/"), Session["FormName"].ToString() + "_" + Session["UserName"].ToString() + "", Session["FormName"].ToString() + " Creation failed for " + ex.Message.ToString() + " at ");

                int saveformid = Convert.ToInt32(Session["id"]);
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 0).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Failed............ ";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });

            }
            if (Session["DDDynamicItems"] == "DDDynamicItems")
            {
                Session["FormName"] = Session["DFormNameD"];
                Session["DDDynamicItems"] = null;
                Session["TBLID"] = Session["DTBLIDD"];
                Session["id"] = Session["DidD"];
                return RedirectToAction("NewForm", "JobOrderReceipt", new { FormName = Session["FormName"] });
            }
            Session["GridData"] = null;
            return RedirectToAction("Info", "JobOrderReceipt", new { FormName = Session["FormName"] });
        }


        public ActionResult Edit(string ide, string FormName, string tblIDE)
        {
            try
            {
                List<CONTABLE006> all = new List<CONTABLE006>();
                List<FormBuildClass> all1 = new List<FormBuildClass>();
                List<CONTABLE0010> fm = new List<CONTABLE0010>();
                if (ide != null)
                {
                    Session["IDE"] = ide;
                }
                else
                {
                    ide = Session["IDE"].ToString();
                }

                if (FormName != null)
                {
                    Session["FormName"] = FormName;
                }
                else
                {
                    if (Session["FormName"] != null)
                        FormName = Session["FormName"].ToString();
                    else
                        return RedirectToAction("Logon", "Account");
                }
                var formid = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).FirstOrDefault();
                Session["id"] = formid.COLUMN02;
                if (formid.COLUMN02 == 1251)
                {
                    Session["PForms"] = "Item Receipt"; ViewBag.PoNo = "ID";
                }
               
                else if (formid.COLUMN02 == 1286) { Session["PForms"] = "Bill to Jobber"; ViewBag.PoNo = "JoReciept#"; }
                
                else { Session["PForms"] = null; ViewBag.PoNo = 0; }

                var fdata = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02 && a.COLUMN07 == "Y").OrderBy(a => a.COLUMN11);
                all = fdata.Where(a => a.COLUMN06 != null).ToList();
                var Ta = all.Where(a => (a.COLUMN11 == "Item Level" || a.COLUMN11 == "Tab" || a.COLUMN11 == "Table") && a.COLUMN07 != "N").ToList();
                Ta = Ta.Where(a => a.COLUMN06 != null).ToList();
                
                var Tabs = Ta.Select(b => b.COLUMN12).Distinct();
                var itemslist = Ta.Where(b => b.COLUMN11 == "Item Level" && b.COLUMN07 != "N").OrderBy(q => q.COLUMN02).ToList();
                var itemscol = itemslist.Where(b => b.COLUMN06 != null).OrderBy(q => q.COLUMN02).ToList();

                var fddl = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).Select(q => new { q.COLUMN06 });
                var fid6 = fddl.Select(w => w.COLUMN06).First();
                var ddl = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName || a.COLUMN06 == fid6 || a.COLUMN02 == formid.COLUMN02);
                ViewBag.ddl = ddl.Select(a => a.COLUMN04).ToList();
                var tbl = fdata.Select(a => a.COLUMN04).Distinct().ToList();
                var distLocations = (from li in fdata.AsEnumerable()
                                     orderby li.COLUMN01
                                     select new { Location = li.COLUMN04 }).Distinct().ToList();
                tbl = distLocations.Select(a => a.Location).ToList();
                int alco = 0;
                int countryID = 0;
                for (int t = 0; t < tbl.Count; t++)
                {
                    if (formid.COLUMN02 == 1286 && t == 2) break;
                    int tblid = Convert.ToInt32(tbl[t]);
                    var tbldlist = dc.CONTABLE004.Where(a => a.COLUMN02 == tblid);
                    var table = dc.CONTABLE006.Where(a => a.COLUMN04 == tblid);
                    var tname = tbldlist.FirstOrDefault().COLUMN04; SqlCommand cmd = new SqlCommand();
                    string tblids = "";
                    for (int j = 1; j < tbl.Count; j++)
                    {
                        if (j == 1)
                            tblids = tbl[j].ToString();
                        else
                            tblids += "," + tbl[j].ToString();
                    }
                    if (formid.COLUMN02 == 1286)
                    {
                        if (t == 0)
                        {
                            cmd = new SqlCommand(
                               "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                             "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                             "where CONTABLE005.COLUMN03 =" + tblid + " and CONTABLE006.COLUMN04 =" + tblid + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN11!='Grid Level'  and   CONTABLE006.COLUMN03=" + formid.COLUMN02 + " and CONTABLE006.COLUMN07 !='N' ORDER BY CAST(CONTABLE006.COLUMN13 AS INT)  ", cn);
                        }
                        else
                        {
                            cmd = new SqlCommand(
                               "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                             "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                             "where CONTABLE005.COLUMN03 in(" + tblids + ") and CONTABLE006.COLUMN04 in(" + tblids + ") and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN11!='Grid Level'  and   CONTABLE006.COLUMN03=" + formid.COLUMN02 + " and CONTABLE006.COLUMN07 !='N' ORDER BY CAST(CONTABLE006.COLUMN13 AS INT)  ", cn);
                        }
                    }
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    cmd.ExecuteNonQuery();
                    string colms = null;
                    string acolms = null;
                    string pid = null; string pidcolumn = null;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {int tbllid=Convert.ToInt32(dt.Rows[i]["COLUMN04"]);
                    var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == tbllid).FirstOrDefault();
                        var ctname = ctbldata.COLUMN04;
                        var ccolid = (ctname + dt.Rows[i]["COLUMN05"]);
                        if (colms == null)
                        {
                            colms += dt.Rows[i]["COLUMN05"];
                            acolms += dt.Rows[i]["COLUMN05"] + " as " + "[" + ccolid + "]";
                        }
                        else
                        {
                            colms += "," + dt.Rows[i]["COLUMN05"];
                            acolms += "," + dt.Rows[i]["COLUMN05"] + " as " + "[" + ccolid + "]";
                        }
                    }

                    SqlCommand cmdp = new SqlCommand();
                    if (tbl[t].Equals(110010837) || tbl[t].Equals(110008817) || tbl[t].Equals(110010848))
                    {
                        cmdp = new SqlCommand("SELECT COLUMN04 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN03 =" + tblid + " and column04 <> 'column01'  ", cn);

                    }
                    else
                        cmdp = new SqlCommand("SELECT COLUMN04 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN05 LIKE '%Parent_Internal_ID%'  and COLUMN03 =" + tblid + " ", cn);
                    SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                    DataTable dtp = new DataTable();
                    dap.Fill(dtp);
                    var strqry = "";
                    if (dtp.Rows.Count > 0 || tbl[t].Equals(110010837) || tbl[t].Equals(110008817) || tbl[t].Equals(110010848))
                    {
                        pidcolumn = dtp.Rows[0][0].ToString();
                        //if (tname != "MATABLE007" && formid.COLUMN02 == 1286)
                        if ((tbl[t] == 110010837 || tbl[t].Equals(110008817) || tbl[t].Equals(110010848)) && tname != "MATABLE007")
                            pid = Session["IDE"].ToString();

                        else
                            pid = Session["pid"].ToString();

                        var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                        var providerName = "System.Data.SqlClient";
                        var dbs = Database.OpenConnectionString(connectionString, providerName);
                        
                        var strqry2 = pid;
                        if (formid.COLUMN02 == 1286)
                        {
                            strqry = "select  p.COLUMN03 as [PUTABLE004COLUMN03],p.COLUMN05 as [PUTABLE004COLUMN05],p.COLUMN17 as [PUTABLE004COLUMN17],m.COLUMN50 [MATABLE007COLUMN04],COLUMN42 as [MATABLE007COLUMN42],COLUMN43 as [MATABLE007COLUMN43],COLUMN44 as [MATABLE007COLUMN44],m.COLUMN10 as [MATABLE007COLUMN10], p.COLUMN06 as [PUTABLE004COLUMN06], p.COLUMN08 as [PUTABLE004COLUMN08]," +
                                     "p.COLUMN09 as [PUTABLE004COLUMN09],p.COLUMN23 as [PUTABLE004COLUMN23], p.COLUMN10 as [PUTABLE004COLUMN10], p.COLUMN11 as [PUTABLE004COLUMN11]," +
                                     "p.COLUMN12 as [PUTABLE004COLUMN12],p.COLUMN13 as [PUTABLE004COLUMN13],p.COLUMN14 as [PUTABLE004COLUMN14]," +
                                     "p.COLUMN15 as [PUTABLE004COLUMN15],p.COLUMN16 as [PUTABLE004COLUMN16],p.COLUMN26 as [PUTABLE004COLUMN26],p.COLUMN24 as [PUTABLE004COLUMN24], " +
                                    " p.COLUMN29 as [PUTABLE004COLUMN29], p.COLUMN30 as [PUTABLE004COLUMN30], p.COLUMN31 as [PUTABLE004COLUMN31], p.COLUMN32 as [PUTABLE004COLUMN32],p.COLUMN33 as [PUTABLE004COLUMN33], p.COLUMN34 as [PUTABLE004COLUMN34], p.COLUMN35 as [PUTABLE004COLUMN35], p.COLUMN36 as [PUTABLE004COLUMN36], p.COLUMN37 as [PUTABLE004COLUMN37]" +
                                     " from PUTABLE004 p,MATABLE007 m where p.COLUMN12=" + strqry2 + "  and m.COLUMN02=p.COLUMN03  ";
                        }
                        var sql = strqry;
                        var GData = dbs.Query(sql);
                       
                        ViewBag.itemsdata = GData;
                        //EMPHCS755	Item master - type of price deletion is not working. even though deleted, it is showing again Done By SRINIVAS 7/23/2015
                        SqlCommand gcmd = new SqlCommand();
                        if (formid.COLUMN02 == 1286 && t >= 1)
                            gcmd = new SqlCommand(strqry, cn);
                        else
                            gcmd = new SqlCommand("select  " + acolms + " from " + tname + " where " + pidcolumn + "=" + pid + "  and columna13=0  ", cn);
                        SqlDataAdapter gda = new SqlDataAdapter(gcmd);
                        DataTable gdt = new DataTable();
                        gda.Fill(gdt);
                        //EMPHCS942 rajasekhar reddy patakota 14/8/2015 After creating multiple line and after editing / saving , Invoice is getting shown with only one line (for same item with different uom)
                        SqlCommand cmdd1 = new SqlCommand("select  COLUMN02 from " + tname + " where  " + pidcolumn + "=" + pid + "  and columna13=0  ", cn);
                        SqlDataAdapter daa = new SqlDataAdapter(cmdd1);
                        DataTable dtt = new DataTable();
                        daa.Fill(dtt);
                        ArrayList uniquecols = new ArrayList();
                        for (int j = 0; j < dtt.Rows.Count; j++) { uniquecols.Add(dtt.Rows[j][0].ToString()); }
                        ArrayList icols = new ArrayList();
                        ArrayList bcols = new ArrayList();
                        ArrayList PTcols = new ArrayList();
                        ArrayList Tcols = new ArrayList();
                        ArrayList TPcols = new ArrayList();
                        ArrayList TPcols1 = new ArrayList();
						//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                        ArrayList Refcols = new ArrayList();
                        ArrayList lotcols = new ArrayList();
                        ArrayList MofMarging = new ArrayList();

                        for (int i = 0; i < gdt.Rows.Count; i++)
                        {
                            if (formid.COLUMN02 == 1286)
                            {
                                icols.Add(gdt.Rows[i]["PUTABLE004COLUMN03"].ToString());
                                bcols.Add(gdt.Rows[i]["PUTABLE004COLUMN17"].ToString());
                                PTcols.Add(gdt.Rows[i]["PUTABLE004COLUMN37"].ToString());
                                Tcols.Add(gdt.Rows[i]["MATABLE007COLUMN43"].ToString()); 
                                TPcols.Add(gdt.Rows[i]["PUTABLE004COLUMN29"].ToString());
                                TPcols1.Add(gdt.Rows[i]["MATABLE007COLUMN10"].ToString());
                                Refcols.Add(gdt.Rows[i]["PUTABLE004COLUMN26"].ToString());
                                lotcols.Add(gdt.Rows[i]["PUTABLE004COLUMN24"].ToString());
                                MofMarging.Add(gdt.Rows[i]["PUTABLE004COLUMN35"].ToString());

                            }
                            
                        }
                        ViewBag.gitemsdata = icols;
                        ViewBag.bgitemsdata = bcols;
                        ViewBag.PTitemsdata = PTcols;
                        ViewBag.Titemsdata = Tcols;
                        ViewBag.TPitemsdata = TPcols;
                        ViewBag.TPitemsdata1 = TPcols1;
						//EMPHCS1051 rajasekhar reddy patakota 27/8/2015 Job Oder Receipt Functionality Changes
                        ViewBag.uniquecols = uniquecols;
                        ViewBag.Refcols = Refcols;
                        ViewBag.lotcols = lotcols;
                        ViewBag.MofMarging = MofMarging;
                    }
                    else
                    {
                        pid = ide;
                        pidcolumn = "COLUMN02";
                        if (t != 0 && (formid.COLUMN02 == 1252 || formid.COLUMN02 == 1265 || formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                        {
                            pid = Session["pid"].ToString();
                        }
                        else if (formid.COLUMN02 == 1285)
                        {
                            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                            var providerName = "System.Data.SqlClient";
                            var dbs = Database.OpenConnectionString(connectionString, providerName);
                            var sql = "select  " + acolms + " from " + tname + " where " + pidcolumn + "=" + pid + "  and COLUMNA13='False' ";
                            var GData = dbs.Query(sql);
                            ViewBag.itemsdata = GData;
                        }
                        else if (tname == "PUTABLE009")
                        {
                            pid = Session["pid"].ToString();
                            pidcolumn = "COLUMN19";
                        }
                        else if (tname == "SATABLE003")
                        {
                            pid = Session["pid"].ToString();
                            pidcolumn = "COLUMN20";
                        }
                    }

                    SqlCommand cmdd = new SqlCommand();
                    if (tname == "SATABLE003" && formid.COLUMN02 == 1252)
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='Vendor' and columna03=" + Session["AcOwner"] + " ", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='Employee' and columna03=" + Session["AcOwner"] + " ", cn);
                    else if (tname == "SATABLE003" && formid.COLUMN02 == 1265)
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='Customer' and columna03=" + Session["AcOwner"] + " ", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1275 || formid.COLUMN02 == 1283))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='SALESORDER' and columna03=" + Session["AcOwner"] + "", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1378))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='PROJECT' and columna03=" + Session["AcOwner"] + " ", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1380))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='PROJECTWORKORDER' and columna03=" + Session["AcOwner"] + " ", cn);
                    else if (formid.COLUMN02 == 1286 && t >= 1)
                        cmdd = new SqlCommand(strqry, cn);
                    else
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + "  and columna03=" + Session["AcOwner"] + " ", cn);
                    SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                    DataTable dtd = new DataTable();
                    dad.Fill(dtd);
                    if (dtd.Rows.Count > 0)
                    {
                        if (tname == "SATABLE003")
                        {
                            if (dtd.Rows[0]["COLUMN16"] != "")
                            {
                                countryID = Convert.ToInt32(dtd.Rows[0]["COLUMN16"]);
                            }
                        }
                        else if (tname == "PUTABLE009")
                        {
                            if (dtd.Rows[0]["COLUMN15"] != "")
                            {
                                countryID = Convert.ToInt32(dtd.Rows[0]["COLUMN15"]);
                            }
                        }

                    }
                    cn.Close();
                    SqlCommand cmdfp = new SqlCommand();

                    if (t == 0 && (formid.COLUMN02 == 1252 || formid.COLUMN02 == 1265 || formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                    {
                        if (formid.COLUMN02 == 1252)
                        {
                            cmdfp = new SqlCommand("select COLUMN17 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid.COLUMN02 == 1265)
                        {
                            cmdfp = new SqlCommand("select COLUMN16 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid.COLUMN02 == 1260)
                        {
                            cmdfp = new SqlCommand("select COLUMN25 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid.COLUMN02 == 1415)
                        {
                            cmdfp = new SqlCommand("select COLUMN25 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        SqlDataAdapter dafp = new SqlDataAdapter(cmdfp);
                        DataTable dtfp = new DataTable();
                        dafp.Fill(dtfp);
                        string ppid = dtfp.Rows[0][0].ToString();
                        Session["pid"] = ppid;
                    }
                    else if (t == 0)
                    {
                        cmdfp = new SqlCommand("select COLUMN01 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        SqlDataAdapter dafp = new SqlDataAdapter(cmdfp);
                        DataTable dtfp = new DataTable();
                        dafp.Fill(dtfp);
                        string ppid = dtfp.Rows[0][0].ToString();
                        Session["pid"] = ppid;
                    }
                    for (int row1 = 0; row1 < dtd.Rows.Count; row1++)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {

                            all1.Add(new MeBizSuiteAppUI.Controllers.FormBuildClass { Field_Name = "", Label_Name = "", Action = "", Mandatory = "", Default_Value = "", Control_Type = "", Section_Type = "", Section_Name = "", Form_Id = null, Table_Id = null, Data_Type = "" });
                            int ctblid = Convert.ToInt32(dt.Rows[row][9]);
                            var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                            var ctname = ctbldata.COLUMN04;
                            all1[alco + row].Field_Name = (ctname + dt.Rows[row][0].ToString());
                            //all1[alco + row].Field_Name = (dt.Rows[row][1].ToString());
                            all1[alco + row].Label_Name = (dt.Rows[row][1].ToString());
                            all1[alco + row].Action = (dt.Rows[row][2].ToString());
                            all1[alco + row].Mandatory = (dt.Rows[row][3].ToString());
                            if (dt.Rows[row][10].ToString() == "image" || dt.Rows[row][10].ToString() == "IMAGE")
                            {
                                string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName).FirstOrDefault().COLUMN06.ToString();
                                int moduleid = Convert.ToInt32(moduleid1.ToString());
                                string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                string path = "/" + modulename + "/" + FormName + "/" + dtd.Rows[row1][row].ToString();
                                all1[alco + row].Default_Value = (path);
                            }
                            else
                                all1[alco + row].Default_Value = (dtd.Rows[row1][row].ToString());

                            all1[alco + row].Control_Type = (dt.Rows[row][5].ToString());
                            all1[alco + row].Section_Type = (dt.Rows[row][6].ToString());
                            all1[alco + row].Section_Name = (dt.Rows[row][7].ToString());
                            all1[alco + row].Form_Id = Convert.ToInt32((dt.Rows[row][8].ToString()));
                            all1[alco + row].Table_Id = Convert.ToInt32((dt.Rows[row][9].ToString()));
                            all1[alco + row].Data_Type = (dt.Rows[row][10].ToString());
                            all1[alco + row].Tab_Index = Convert.ToInt32((dt.Rows[row]["COLUMN13"].ToString()));
                            var firstname = all1[alco + row].Label_Name; ViewData[firstname] = "";
                            var dynamicddldata = fdata.Where(a => a.COLUMN14 != null && a.COLUMN15 != null).ToList();
                            var dval = dtd.Rows[row1][row].ToString();
                            var ddata = dt.Rows[row]["COLUMN14"].ToString();
                            if (ddata == "Control Value")
                            {
                                int ddatavalue = Convert.ToInt32(dt.Rows[row]["COLUMN15"].ToString());
                                List<SelectListItem> Country = new List<SelectListItem>();
                                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                                var acOW = Convert.ToInt32(Session["AcOwner"]);
                                List<MATABLE002> dropdata = new List<MATABLE002>();
                               
                                dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddatavalue && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                                    dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                              for (int dd = 0; dd < dropdata.Count; dd++)
                                {
                                    Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
                                }
                                ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                            }
                            else if (ddata == "Master Value")
                            {
                                int ddatavalue = Convert.ToInt32(dt.Rows[row]["COLUMN15"].ToString());
                                List<SelectListItem> Country = new List<SelectListItem>();
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddatavalue).OrderBy(q => q.COLUMN02);
                                var tblName = tblddl.Select(q => q.COLUMN04).First();
                                if (formid.COLUMN02 == 1272 && dt.Rows[row][0] == "COLUMN03")
                                {
                                    SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN03 from PUTABLE002   where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata2 = new DataTable();
                                    cmdd2.Fill(dtdata2); string dcol = "";
                                    for (int dd2 = 0; dd2 < dtdata2.Rows.Count; dd2++)
                                    {
                                        if (dd2 == 0)
                                            dcol = dtdata2.Rows[dd2][0].ToString();
                                        else
                                            dcol += " or COLUMN02=" + dtdata2.Rows[dd2][0].ToString();
                                    }
                                    if (dcol != "")
                                    {
                                        SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN02=" + dcol + " ", cn);
                                        DataTable dtdata = new DataTable();
                                        cmddl.Fill(dtdata);
                                        for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                        {
                                            Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                        }
                                    }
                                    ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                                }
                               
                                else if (formid.COLUMN02 == 1283 || formid.COLUMN02 == 1284 || formid.COLUMN02 == 1286 || formid.COLUMN02 == 1287 || formid.COLUMN02 == 1288)
                                {
                                    SqlDataAdapter cmddl = new SqlDataAdapter();
                                    if (tblName == "SATABLE009")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN19 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "PRTABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                    else if (tblName == "MATABLE009")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN06 ='true' or COLUMN06 ='1'", cn);
                                    else if (tblName == "MATABLE011")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN06 ='true' or COLUMN06 ='1'", cn);
                                    else if (tblName == "SATABLE005" && formid.COLUMN02 == 1286)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "    where  COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "SATABLE005" || tblName == "PUTABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (formid.COLUMN02 == 1283 && tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN47 = 'FALSE'", cn);

                                    else if (formid.COLUMN02 == 1286 && tblName == "MATABLE007" && dt.Rows[row]["COLUMN05"].ToString() == "COLUMN29")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where    ISNULL(COLUMNA13,0) = 0  AND ISNULL(COLUMN78,0) = 1 and COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False'", cn);
                                    else if (formid.COLUMN02 == 1286 && tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN47 = 'FALSE'", cn);

                                    else if (tblName == "SATABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "MATABLE016")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE017")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "CONTABLE007")
										//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "SATABLE019")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and (COLUMN13='' or isnull(COLUMN13,0)=0) and isnull(COLUMNA13,'False')='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);

                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                                }
                                else
                                {
                                    SqlDataAdapter cmddl = new SqlDataAdapter();
                                    if (tblName == "MATABLE010" && formid.COLUMN02 == 1265)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + " where COLUMN30='True'  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (tblName == "PRTABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                    else if (tblName == "PUTABLE001" && formid.COLUMN02 == 1329)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE011 where COLUMN04='Return Order' )   AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "PUTABLE003" && formid.COLUMN02 == 1330)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN17 in (Select COLUMN02 from MATABLE011 where COLUMN04='Return Order' )  AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "SATABLE005" && formid.COLUMN02 == 1354)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='Return Order' ) and COLUMN04 like 'PR%'  AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "SATABLE007" && formid.COLUMN02 == 1355)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN04='Return Order' ) and COLUMN04 like 'RI%'  AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "MATABLE010" && (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (tblName == "MATABLE010" && formid.COLUMN02 == 1293)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + "  Where  COLUMN30 !=1   AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (tblName == "SATABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "SATABLE002")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN25='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "MATABLE008")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE013")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)and COLUMN15='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "MATABLE011" || tblName == "MATABLE009")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 as COLUMN04 from " + tblName + "  ", cn);
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "FITABLE001" && (formid.COLUMN02 == 1261))
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07 IN (22344,22385,22384,22405,22406) AND  (COLUMNA02 in(" + Session["OPUnit"] + ") OR COLUMNA02 IS NULL) AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) ", cn);
                                    else if (tblName == "FITABLE001" && (formid.COLUMN02 == 1363 || formid.COLUMN02 == 1386) && dt.Rows[row]["COLUMN05"].ToString() == "COLUMN03")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07!=22266 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL)", cn);
                                    else if (tblName == "FITABLE001" && (formid.COLUMN02 == 1293) && dt.Rows[row][0].ToString() == "COLUMN03")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07=22344 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL)", cn);
                                    else if (tblName == "FITABLE001" && (formid.COLUMN02 == 1358 || formid.COLUMN02 == 1363 || formid.COLUMN02 == 1386 || formid.COLUMN02 == 1293))
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07=22266 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL)", cn);
                                    else if (tblName == "MATABLE016")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE017")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "CONTABLE007" && (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
										//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!=''  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                    else if (tblName == "CONTABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                    else if (tblName == "FITABLE020")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN21 as COLUMN04 from " + tblName + " where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    //else if (tblName == "FITABLE001" && formid.COLUMN02 == 1293 && dtd.Rows[row]["COLUMN05"].ToString() == "COLUMN03")
                                    //    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  " +Session["OPUnitWithNull"]+ "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN07=22344", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'", cn);

                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                                }
                            }
                            else if (ddata == "GridView")
                            {
                                var sql = "";
                                List<string> colNM = new List<string>();
                                List<string> TDataC = new List<string>();
                                int ddatavalue = Convert.ToInt32(dt.Rows[row]["COLUMN15"].ToString());
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddatavalue).OrderBy(q => q.COLUMN02);
                                var tbleID = Convert.ToInt32(Session["TBLID"]);
                                var modID = tblddl.Where(q => q.COLUMN02 == ddatavalue).Select(q => q.COLUMN03).First();
                                var tblName = tblddl.Where(q => q.COLUMN03 == modID && q.COLUMN02 == ddatavalue).Select(q => q.COLUMN04).First();
                                var tblNameID = tblddl.Select(q => q.COLUMN02).First();
                                var formID = dc.CONTABLE006.Where(a => a.COLUMN04 == tblNameID && a.COLUMN07 == "Y").OrderBy(q => q.COLUMN01).Take(4).ToList();
                                SqlCommand cmdit = new SqlCommand("select column02 from  FITABLE010 where column03=" + ide + "", cn);
                                SqlDataAdapter dait = new SqlDataAdapter(cmdit);
                                DataTable dtit = new DataTable();
                                dait.Fill(dtit);
                                if (formID.Count == 0)
                                {
                                    
                                    colNM.Add("Operating Unit"); colNM.Add("Qty OnHand"); colNM.Add("Qty Committed"); colNM.Add("Qty Backordered"); colNM.Add("Qty OnOrder"); colNM.Add("Qty Available"); colNM.Add("Qty Intransit"); colNM.Add("Qty WIP"); colNM.Add("Total Price"); colNM.Add("Avg Price");
                                    ViewBag.EFormName = "";
                                    if (dtit.Rows.Count > 0)
                                    {
                                        sql = "select  (select column03 from CONTABLE007 where COLUMN02=(select COLUMN37 from matable007 where COLUMN02=FITABLE010.COLUMN03))  [Operating Unit],isnull(COLUMN04,0) as [Qty OnHand],isnull(COLUMN05,0) as [Qty Committed],isnull(COLUMN06,0) as [Qty Backordered],isnull(COLUMN07,0) as [Qty OnOrder],isnull(COLUMN08,0)  as [Qty Available],isnull(COLUMN09,0)  as [Qty Intransit],isnull(COLUMN18,0)  as [Qty WIP],isnull(COLUMN12,0)  as [Total Price],isnull(COLUMN17,0)  as [Avg Price] from FITABLE010 where COLUMN03=" + ide + "";
                                    }
                                    else
                                    {
                                        sql = "select  (select column03 from CONTABLE007 where COLUMN02=matable007.COLUMN37)  [Operating Unit],0 as [Qty OnHand],0 as [Qty Committed],0  as [Qty Backordered],0 as [Qty OnOrder],0 as [Qty Available],0  as [Qty Intransit],0 as [Qty WIP],0  as [Total Price],0  as [Avg Price] from matable007 where column02=" + ide + "";
                                    }
                                }
                                else
                                {
                                    var frmIDE = formID.Select(q => q.COLUMN03).First();
                                    var colLST = formID.Select(q => q.COLUMN06).ToList();
                                    var formName = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmIDE);
                                    var formNameE = formName.Select(q => q.COLUMN04).First();
                                    foreach (string dtr in colLST)
                                    {
                                        if (frmIDE == 1363)
                                        {
                                            if (dtr != "Tax")
                                                colNM.Add(dtr);
                                        }
                                        else
                                        {
                                            colNM.Add(dtr);
                                        }
                                    }
                                    ViewBag.EFormName = formNameE; if (frmIDE == 1363)
                                    {
                                        sql = "select COLUMN02 AA ,COLUMN03 as Account,COLUMN04 as Description,COLUMN05 as Amount from FITABLE022   Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN07=0 union all select " +
                                "0, null,null,0 where NOT EXISTS (select COLUMN02 AA ,COLUMN03 as Account,COLUMN04 as Description,COLUMN05 as Amount from FITABLE022   Where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN07=0)";
                                    }
                                    else
                                    {
                                        sql = "select COLUMN02 AA ,COLUMN03 as [" + colNM[0] + "],COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN06 as [" + colNM[3] + "] from " + tblName + "";
                                    } TDataC.Add("COLUMN02");
                                }
                               
                                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                                var providerName = "System.Data.SqlClient";
                                var dbs = Database.OpenConnectionString(connectionString, providerName);
                                var GData = dbs.Query(sql);
                                var grid = new WebGrid(GData, canPage: false, canSort: false);
                                foreach (string dtr in colNM)
                                {

                                    TDataC.Add(dtr);
                                }
                                ViewBag.GridDynamicData = GData;
                                ViewBag.GridDynamicDataC = TDataC;
                                ViewBag.TableIDE = ddata;
                            }
                        }
                        alco += dt.Rows.Count;
                        if (tname == "FITABLE013") { break; } break;
                    }
                }
                var itemlist = all.Where(a => a.COLUMN11 == "Item Level").ToList();
                var litemlist = itemlist.Select(a => a.COLUMN06).ToList();
                ViewBag.itemscol = litemlist;
                var result1 = new List<dynamic>();
                if (litemlist.Count > 0)
                {
                    var obj = (IDictionary<string, object>)new System.Dynamic.ExpandoObject();
                    foreach (var row1 in litemlist)
                    {
                        obj.Add(row1, row1);
                    }
                    result1.Add(obj);
                }
                ArrayList cols = new ArrayList();
                ArrayList JOIRcols = new ArrayList();
                for (int col = 0; col < itemscol.Count; col++)
                {

                    int ctblid = Convert.ToInt32(itemscol[col].COLUMN04);
                    var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                    var ctname = ctbldata.COLUMN04;
                    var cid = (ctname + itemscol[col].COLUMN05);
                    cols.Add(cid);
                }
                if (formid.COLUMN02 == 1286)
                {
                    itemscol = itemscol.Where(a => a.COLUMN04 == 110010817 && a.COLUMN03 == 1286).ToList();
                    for (int col = 0; col < itemscol.Count; col++)
                    {
                        var ctblid = Convert.ToInt32(itemscol[col].COLUMN04);
                        var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                        var ctname = ctbldata.COLUMN04;
                        var cid = (ctname + itemscol[col].COLUMN05);
                        JOIRcols.Add(cid);
                    }
                }
                if (formid.COLUMN02 == 1286)
                    ViewBag.NewCols = JOIRcols;
                else
                    ViewBag.NewCols = cols;
                ViewBag.Tabs = Tabs;
                ViewBag.itemslist = result1;
                if (formid.COLUMN02 == 1330 || formid.COLUMN02 == 1285 )
                {
                    all1 = (from p in all1
                            where p.Label_Name != null
                            orderby (p.Tab_Index)
                            select p).ToList();
                }
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                ViewBag.DateFormat = DateFormat;
                return View("~/Views/Manufacture/JobOrderReceipt/Edit.cshtml", all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult Edit(FormCollection col, HttpPostedFileBase hb)
        {
            try

            {
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["GridData"];
                int saveformid = Convert.ToInt32(Session["id"]);
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN02 == saveformid).ToList();
                var fname = fnamedata.Select(a => a.COLUMN04).FirstOrDefault();
                Session["FormName"] = fname;
                var formid = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var ftid=formid.COLUMN02;
                int rqty = 0; int bqty = 0; var errormage = "";

                //if (formid == 1251 || formid == 1275)
                //{
                //    for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                //    {
                //        if (formid == 1251)
                //        {
                //            if (itemdata.Tables[0].Rows[i]["PUTABLE002COLUMN12"] != "")
                //                rqty += Convert.ToInt32(itemdata.Tables[0].Rows[i]["PUTABLE002COLUMN12"]);
                //            if (itemdata.Tables[0].Rows[i]["PUTABLE002COLUMN13"] != "")
                //                bqty += Convert.ToInt32(itemdata.Tables[0].Rows[i]["PUTABLE002COLUMN13"]);
                //        }
                //        else
                //        {
                //            if (itemdata.Tables[0].Rows[i]["SATABLE006COLUMN12"] != "")
                //                rqty += Convert.ToInt32(itemdata.Tables[0].Rows[i]["SATABLE006COLUMN12"]);
                //            if (itemdata.Tables[0].Rows[i]["SATABLE006COLUMN13"] != "")
                //                bqty += Convert.ToInt32(itemdata.Tables[0].Rows[i]["SATABLE006COLUMN13"]);
                //        }
                //    }
                //    if (rqty != 0 || bqty != 0)
                //    {
                //        try
                //        {
                //            throw new Exception(message: "Edit Operation Can't Be Performed For This Transaction");
                //        }
                //        catch (Exception ex)
                //        {
                //            Session["MessageFrom"] = ex.Message;
                //            Session["SuccessMessageFrom"] = "fail";
                //        }
                //        return RedirectToAction("Info", new { FormName = Session["FormName"] });
                //    }
                //}
                var fd = dc.CONTABLE006.Where(a => a.COLUMN07 == "Y" && a.COLUMN03 == saveformid);
                var tbl = fd.Select(a => a.COLUMN04).Distinct().ToList();
                var distLocations = (from li in fd.AsEnumerable()
                                     orderby li.COLUMN01
                                     select new { Location = li.COLUMN04 }).Distinct().ToList();
                tbl = distLocations.Select(a => a.Location).ToList();
                for (int t = 0; t < tbl.Count; t++)
                {
                    int tblid = Convert.ToInt32(tbl[t]);
                    var tbldlist = dc.CONTABLE004.Where(a => a.COLUMN02 == tblid);
                    var table = dc.CONTABLE006.Where(a => a.COLUMN04 == tblid && a.COLUMN03 == saveformid);
                    var tname = tbldlist.FirstOrDefault().COLUMN04;
                    SqlCommand acmd = new SqlCommand("select  column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tname + "'", cn);
                    SqlCommand cmd = new SqlCommand("select  COLUMN06,COLUMN05,COLUMN11,COLUMN12  from CONTABLE006 where COLUMN04=" + tblid + " and COLUMN03=" + saveformid + " and COLUMN07='Y'  and column11!='Grid Level' ", cn);
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    SqlDataAdapter ada = new SqlDataAdapter(acmd);
                    DataTable dt = new DataTable();
                    DataTable adt = new DataTable();
                    da.Fill(dt);
                    ada.Fill(adt);
                    cn.Close();
                    SqlCommand cmddt = new SqlCommand("select  COLUMN06 from CONTABLE005 where COLUMN03='" + tblid + "' and COLUMN04!='COLUMN02' and COLUMN04!='COLUMN01'", cn);
                    cn.Open();
                    SqlDataAdapter dadt = new SqlDataAdapter(cmddt);
                    DataTable dtdt = new DataTable();
                    dadt.Fill(dtdt);
                    cn.Close();

                    //
                    var pidcolumn = "";
                    var pid = "";
                    SqlCommand cmdp = new SqlCommand("SELECT COLUMN04 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN05 LIKE '%Parent_Internal_ID%'  and COLUMN03 =" + tblid + " ", cn);
                    SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                    DataTable dtp = new DataTable();
                    dap.Fill(dtp);

                    if (dtp.Rows.Count > 0)
                    {
                        pidcolumn = dtp.Rows[0][0].ToString();
                        pid = Session["pid"].ToString();
                    }
                    else
                    {
                        pid = Session["IDE"].ToString();
                        pidcolumn = "COLUMN02";
                        if (t != 0 && (formid == 1252 || formid == 1265 || formid == 1260 || formid == 1415))
                        {
                            pid = Session["pid"].ToString();
                        }
                        else if (tname == "PUTABLE009")
                        {
                            pid = Session["pid"].ToString();
                            pidcolumn = "COLUMN19";
                        }
                        else if (tname == "SATABLE003")
                        {
                            pid = Session["pid"].ToString();
                            pidcolumn = "COLUMN20";
                        }
                    }

                    SqlCommand cmdfp = new SqlCommand();

                    if (t == 0 && (formid == 1252 || formid == 1265 || formid == 1260 || formid == 1415))
                    {
                        if (formid == 1252)
                        {
                            cmdfp = new SqlCommand("select COLUMN17 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid == 1265)
                        {
                            cmdfp = new SqlCommand("select COLUMN16 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid == 1260 || formid == 1415)
                        {
                            cmdfp = new SqlCommand("select COLUMN25 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        SqlDataAdapter dafp = new SqlDataAdapter(cmdfp);
                        DataTable dtfp = new DataTable();
                        dafp.Fill(dtfp);
                        string ppid = dtfp.Rows[0][0].ToString();
                        Session["pid"] = ppid;
                    }
                    else if (t == 0)
                    {
                        cmdfp = new SqlCommand("select COLUMN01 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        SqlDataAdapter dafp = new SqlDataAdapter(cmdfp);
                        DataTable dtfp = new DataTable();
                        dafp.Fill(dtfp);
                        string ppid = dtfp.Rows[0][0].ToString();
                        Session["pid"] = ppid;
                    }
                    //
                    int val1 = 0;
                    StringBuilder value = new StringBuilder();
                    string values = null;
                    SqlCommand cmdd = new SqlCommand();
                    SqlDataAdapter daa = null;
                    DataTable dtt = null; var itemlevel = dt.Rows[0][2].ToString();
                    cmdd = new SqlCommand("select  COLUMN02 from " + tname + " where  " + pidcolumn + "=" + pid + " ", cn);

                    daa = new SqlDataAdapter(cmdd);
                    cn.Open();
                    dtt = new DataTable();
                    daa.Fill(dtt);
                    cn.Close();
                    if (formid == 1286 && tblid == 110008817)
                    {
                        break;
                    }
                    var firstcol = dtt.Rows[0][0].ToString();
                    if (itemlevel == "Item Level")
                    {
                        //EMPHCS679 Rajasekhar patakota on 17/7/2015.While Updating Item Without Price it Should allow 
                        if (itemdata.Tables.Count > 0)
                        {
                            for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                            {
								//EMPHCS1051 rajasekhar reddy patakota 27/8/2015 Job Oder Receipt Functionality Changes
                                for (int k = 0; k < itemdata.Tables[0].Columns.Count-1; k++)
                                {
                                    var colname = itemdata.Tables[0].Columns[k].ColumnName;
                                    var coldata = itemdata.Tables[0].Rows[i][colname].ToString();
                                    if (k == 0)
                                    {
                                        values = "COLUMN02";

                                        if (i < dtt.Rows.Count)
                                        {
											//EMPHCS1051 rajasekhar reddy patakota 27/8/2015 Job Oder Receipt Functionality Changes
                                            if (itemdata.Tables[0].Rows[i]["checkRow"].ToString() == null || itemdata.Tables[0].Rows[i]["checkRow"].ToString() == "")
                                            {
                                                SqlCommand cmddn = new SqlCommand("select  max(COLUMN02) from " + tname + "", cn);
                                                SqlDataAdapter daan = new SqlDataAdapter(cmddn);
                                                DataTable dttn = new DataTable();
                                                daan.Fill(dttn);
                                                val1 = Convert.ToInt32(dttn.Rows[0][0].ToString());
                                                value.Append((val1 + 1).ToString());
                                            }
                                            else
                                            {
                                                val1 = Convert.ToInt32(itemdata.Tables[0].Rows[i]["checkRow"].ToString());
                                                value.Append((val1).ToString());
                                            }
                                        }
                                        else
                                        {
                                            if (tname == "FITABLE013")
                                            { value.Append(dtt.Rows[0][0].ToString()); }
                                            else
                                            {
                                                SqlCommand cmddn = new SqlCommand("select  max(COLUMN02) from " + tname + "", cn);
                                                SqlDataAdapter daan = new SqlDataAdapter(cmddn);
                                                cn.Open();
                                                DataTable dttn = new DataTable();
                                                daan.Fill(dttn);
                                                cn.Close();
                                                val1 = Convert.ToInt32(dttn.Rows[0][0].ToString());
                                                value.Append((val1 + 1).ToString());
                                            }
                                        }
                                        values += "`" + dt.Rows[k][1].ToString();
                                        value.Append("`" + coldata);
                                    }
                                    else
                                    {
										//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                        if (itemdata.Tables[0].Columns.Contains("PUTABLE004COLUMN26") && colname=="PUTABLE004COLUMN26")
                                        {
                                            if (formid == 1286)
                                                values += "`" + "COLUMN26";
                                            value.Append("`" + itemdata.Tables[0].Rows[i]["PUTABLE004COLUMN26"].ToString());
                                        }
                                        else
                                        {
                                            values += "`" + dt.Rows[k][1].ToString();
                                            if (coldata == "")
                                            {
                                                coldata = "0"; value.Append("`" + coldata);
                                            }
                                            else
                                            { value.Append("`" + coldata); }
                                        }
                                    }
                                } if (tname == "FITABLE013")
                                { }
                                else
                                {
                                    if (pidcolumn != null)
                                    {
                                        values += "`" + pidcolumn;
                                        value.Append("`" + pid);
                                    }
                                }
                                values += "`" + "COLUMNA03" + "`" + "COLUMNA06" + "`" + "COLUMNA07" + "`" + "COLUMNA12" + "`" + "COLUMNA13" + "`" + "COLUMNA11" + "`" + "COLUMNA08";
                                value.Append("`" + Session["AcOwner"] + "`" + DateTime.Now + "`" + DateTime.Now + "`" + "1" + "`" + "0" + "`" + Session["eid"] + "`" + Session["eid"]);
                                SqlCommand cmd1 = new SqlCommand("insert into " + tname + "(" + values + ") values(" + value + ")", cn);
                                cn.Open();
                                string[] param = value.ToString().Split('`');
                                string[] param1 = values.Split('`');
                                if ( formid == 1286)
                                {
                                    Cmd = new SqlCommand("usp_JO_BL_JO_RECEIPT", cn);
                                }
                                
                                Cmd.CommandType = CommandType.StoredProcedure;
                                for (int p = 0; p < param.Length; p++)
                                {
                                    Cmd.Parameters.AddWithValue("@" + param1[p] + "", param[p].ToString());
                                }
                                string update = "Update";
                                Cmd.Parameters.AddWithValue("@Direction", update);
                                Cmd.Parameters.AddWithValue("@TabelName", tname);
                                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                                Cmd.CommandTimeout = 0;
                                int r = Cmd.ExecuteNonQuery();
                                cn.Close();
                                value = new StringBuilder();
                                values = null;
                                if (r > 0)
                                {
                                    eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                                    lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " edit succeeded at ");

                                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 2).FirstOrDefault();
                                    var msg = string.Empty;
                                    if (msgMaster != null)
                                    {
                                        msg = msgMaster.COLUMN03;
                                    }
                                    else
                                        msg = "Data Updated............ ";
                                    Session["MessageFrom"] = msg;
                                    Session["SuccessMessageFrom"] = "Success";
                                }
                                else
                                {
                                    eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                                    lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", fname + " update  " + tname + " failed for the  values " + value + " at the Time ");

                                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 3).FirstOrDefault();
                                    var msg = string.Empty;
                                    if (msgMaster != null)
                                    {
                                        msg = msgMaster.COLUMN03;
                                    }
                                    else
                                        msg = "Data Updation Failed.......... ";
                                    Session["MessageFrom"] = msg;
                                    Session["SuccessMessageFrom"] = "fail";
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                if (firstcol != null || firstcol != "")
                                {
                                    val1 = Convert.ToInt32(dtt.Rows[0][0].ToString());
                                    value.Append((val1).ToString());
                                }
                                var str = col[dt.Rows[i][0].ToString()];
                                values = "COLUMN02";
                                values += "`" + dt.Rows[i][1].ToString();
                                var colname = dt.Rows[i][1].ToString();
                                var variables = dc.CONTABLE005.Where(a => a.COLUMN03 == tblid && a.COLUMN04 == colname).FirstOrDefault();
                                if (variables.COLUMN05 == "Refered Form")
                                {
                                    value.Append("`" + formid);
                                }
                                else if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT" || variables.COLUMN06 == "image" || variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL" || variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT" || variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                {
                                    if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            value.Append("`1");
                                        }
                                    }
                                    else if (variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`" + DBNull.Value);
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`" + DBNull.Value + "");
                                            else
                                            {
                                                var dateVal = DateTime.ParseExact(col[dt.Rows[i][0].ToString()], Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                                                value.Append("`" + dateVal);
                                            }
                                        }
                                    }
                                    else if (variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "image")
                                    {
                                        var image = Request.Files[0] as HttpPostedFileBase;
                                        if (image != null)
                                        {
                                            if (Request.Files.Count > 0)
                                            {
                                                if (image.ContentLength > 0)
                                                {
                                                    if (image.ContentLength > 0)
                                                    {
                                                        LogWriter logs = new LogWriter();
                                                        string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                                        var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == fname).FirstOrDefault().COLUMN06.ToString();
                                                        int moduleid = Convert.ToInt32(moduleid1.ToString());
                                                        string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                                        string fileName = Path.GetFileName(image.FileName);
                                                        string path1 = logs.ImageFile(fname, modulename);
                                                        //imagepath = imagepath + "Images" + "/" + fileName;
                                                        string path = Path.Combine(Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + fname + ""), fileName);
                                                        image.SaveAs(path);
                                                        value.Append("`" + fileName);
                                                    }
                                                }
                                                else
                                                {
                                                    value.Append("`" + DBNull.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string cname = dt.Rows[i][0].ToString();
                                    if (str == null)
                                    {
                                        if (cname == "Refered Form")
                                            value.Append("`" + formid);
                                        else
                                            value.Append("`" + DBNull.Value);
                                    }
                                    else
                                    {
                                        var colval = col[dt.Rows[i][0].ToString()];
                                        if (colval == "")
                                            value.Append("`" + DBNull.Value);
                                        else
                                            value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                    }
                                }
                            }
                            else
                            {
                                var str = col[dt.Rows[i][0].ToString()];
                                values += "`" + dt.Rows[i][1].ToString();
                                var colname = dt.Rows[i][1].ToString();
                                var variables = dc.CONTABLE005.Where(a => a.COLUMN03 == tblid && a.COLUMN04 == colname).FirstOrDefault();
                                if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT" || variables.COLUMN06 == "image" || variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL" || variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT" || variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                {
                                    if (variables.COLUMN06 == "bit" || variables.COLUMN06 == "BIT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            value.Append("`1");
                                        }
                                    }
                                    else if (variables.COLUMN06 == "DATETIME" || variables.COLUMN06 == "DATE" || variables.COLUMN06 == "datetime" || variables.COLUMN06 == "date")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`" + DBNull.Value);
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`" + DBNull.Value + "");
                                            else
                                            {
                                                var dateVal = DateTime.ParseExact(col[dt.Rows[i][0].ToString()], Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                                                value.Append("`" + dateVal);
                                            }
                                        }
                                    }
                                    else if (variables.COLUMN06 == "int" || variables.COLUMN06 == "INT" || variables.COLUMN06 == "bigint" || variables.COLUMN06 == "BIGINT")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "decimal" || variables.COLUMN06 == "DECIMAL")
                                    {
                                        if (str == null)
                                        {
                                            value.Append("`0");
                                        }
                                        else
                                        {
                                            if (str == "")
                                                value.Append("`0");
                                            else
                                                value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                        }
                                    }
                                    else if (variables.COLUMN06 == "image")
                                    {
                                        var image = Request.Files[0] as HttpPostedFileBase;
                                        if (image != null)
                                        {
                                            if (Request.Files.Count > 0)
                                            {
                                                if (image.ContentLength > 0)
                                                {
                                                    if (image.ContentLength > 0)
                                                    {
                                                        LogWriter logs = new LogWriter();
                                                        string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                                        var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == fname).FirstOrDefault().COLUMN06.ToString();
                                                        int moduleid = Convert.ToInt32(moduleid1.ToString());
                                                        string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                                        string fileName = Path.GetFileName(image.FileName);
                                                        string path1 = logs.ImageFile(fname, modulename);
                                                        //imagepath = imagepath + "Images" + "/" + fileName;
                                                        string path = Path.Combine(Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + fname + ""), fileName);
                                                        image.SaveAs(path);
                                                        value.Append("`" + fileName);
                                                    }
                                                }
                                                else
                                                {
                                                    value.Append("`" + DBNull.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (str == null)
                                    {
                                        value.Append("`" + DBNull.Value);
                                    }
                                    else
                                    {
                                        value.Append("`" + col[dt.Rows[i][0].ToString()]);
                                    }
                                }
                            }
                        }
                        if (t != 0 && (formid == 1251 || formid == 1275 || formid == 1283))
                        {
                            values += "`" + pidcolumn;
                            value.Append("`" + pid);
                        }
                        values += "`" + "COLUMNA03" + "`" + "COLUMNA06" + "`" + "COLUMNA07" + "`" + "COLUMNA12" + "`" + "COLUMNA13" + "`" + "COLUMNA11" + "`" + "COLUMNA08";
                        value.Append("`" + Session["AcOwner"] + "`" + DateTime.Now + "`" + DateTime.Now + "`" + "1" + "`" + "0" + "`" + Session["eid"] + "`" + Session["eid"]);
                        //SqlCommand cmd1 = new SqlCommand("insert into " + tname + "(" + values + ") values(" + value + ")", cn);
                        cn.Open();
                        string[] param = value.ToString().Split('`');
                        string[] param1 = values.Split('`');
                        if (formid == 1286)
                        {
                            Cmd = new SqlCommand("usp_JO_BL_JO_RECEIPT", cn);
                        }
                        
                        Cmd.CommandType = CommandType.StoredProcedure;
                        for (int p = 0; p < param.Length; p++)
                        {
                            Cmd.Parameters.AddWithValue("@" + param1[p] + "", param[p].ToString());
                        }
                        string update = "Update";
                        Cmd.Parameters.AddWithValue("@Direction", update);
                        Cmd.Parameters.AddWithValue("@TabelName", tname);
                        Cmd.Parameters.AddWithValue("@ReturnValue", "");
                        Cmd.CommandTimeout = 0;
                        int r = Cmd.ExecuteNonQuery();
                        cn.Close();
                        if (r > 0)
                        {
                            eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                            lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " edit succeeded at ");
                            if (formid == 1260 || formid == 1415)
                            {
                                var ou = dc.CONTABLE006.Where(a => (a.COLUMN03 == 1260 || a.COLUMN03 == 1415) && a.COLUMN04 == 110008617 && a.COLUMN05 == "COLUMN26").FirstOrDefault().COLUMN06;
                                //EMPHCS690 Sessions Reloading while employee update done by Rajasekhar Jr 7/18/2015
                                if (col[ou] != null && col[ou] != "")
                                {
                                    Session["OPUnitstatus"] = 1;
                                    Session["OPUnit"] = col[ou];
                                    Session["OPUnit1"] = col[ou];
                                    Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") ) ";
                                }
                                else
                                {
                                    Session["OPUnitstatus"] = null;
                                    Session["OPUnit1"] = null;
                                    int acid = Convert.ToInt32(Session["AcOwner"]);
                                    var accountowner = dc.CONTABLE007.Where(a => a.COLUMNA03 == acid).ToList();

                                    StringBuilder sb = new StringBuilder();
                                    for (int i = 0; i < accountowner.Count; i++)
                                    {
                                        sb.Append(accountowner[i].COLUMN02);
                                        sb.Append(",");
                                    }
                                    string accountownerids = sb.ToString();
                                    Session["OPUnit"] = accountownerids.TrimEnd(',');
                                    if (Session["OPUnit"] == "")
                                        Session["OPUnit"] = "''";
                                    Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                                }
                            }
                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 2).FirstOrDefault();
                            var msg = string.Empty;
                            if (msgMaster != null)
                            {
                                msg = msgMaster.COLUMN03;
                            }
                            else
                                msg = "Data Updated............ ";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "Success";
                        }
                        else
                        {
                            eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                            lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " the  values " + value + " of form " + fname + " and Table " + tname + " edit failed at ");

                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 3).FirstOrDefault();
                            var msg = string.Empty;
                            if (msgMaster != null)
                            {
                                msg = msgMaster.COLUMN03;
                            }
                            else
                                msg = "Data Updation Failed.......... ";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "fail";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                lg.CreateFile(Server.MapPath("~/"), Session["FormName"].ToString() + "_" + Session["UserName"].ToString() + "", Session["FormName"].ToString() + " updation failed for  " + ex.Message.ToString() + " at  ");

                int saveformid = Convert.ToInt32(Session["id"]);
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 3).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Data Updation Failed ";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "JobOrderReceipt", new { FormName = Session["FormName"] });
            }
          // Exception Handling By Venkat
            return RedirectToAction("Info", new { FormName = Session["FormName"] });
        }
        public ActionResult Detailes(string ide, string FormName)
        {
            try
            {
                List<CONTABLE006> all = new List<CONTABLE006>();
                List<FormBuildClass> all1 = new List<FormBuildClass>();
                List<CONTABLE0010> fm = new List<CONTABLE0010>();
                if (ide != null)
                {
                    Session["IDE"] = ide;
                }
                else
                {
                    ide = Session["IDE"].ToString();
                }

                if (FormName != null)
                {
                    Session["FormName"] = FormName;
                }
                else
                {
                    if (Session["FormName"] != null)
                        FormName = Session["FormName"].ToString();
                    else
                        return RedirectToAction("Logon", "Account");
                }
                var formid = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).FirstOrDefault();
                //EMPHCS747 rajasekhar reddy patakota 25/7/2015 Invoice view is showing wrong item details
                Session["id"] = formid.COLUMN02;
                if (formid.COLUMN02 == 1251)
                {
                    Session["PForms"] = "Item Receipt"; ViewBag.PoNo = "ID";
                }
                
                else if (formid.COLUMN02 == 1286) { Session["PForms"] = "Bill to Jobber"; ViewBag.PoNo = "JoReciept#"; }
                
                else { Session["PForms"] = null; ViewBag.PoNo = 0; }
               
                var fdata = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02 && a.COLUMN07 == "Y").OrderBy(a => a.COLUMN11);
                all = fdata.Where(a => a.COLUMN06 != null).ToList();
                var Ta = all.Where(a => (a.COLUMN11 == "Item Level" || a.COLUMN11 == "Tab" || a.COLUMN11 == "Grid Level" || a.COLUMN11 == "Table") && a.COLUMN07 != "N").ToList();
                Ta = Ta.Where(a => a.COLUMN06 != null).ToList();
                if (formid.COLUMN02 == 1261)
                    Ta = Ta.Where(a => a.COLUMN06 != null).OrderBy(a => int.Parse(a.COLUMN13)).ToList();
                if (formid.COLUMN02 == 1285 || formid.COLUMN02 == 1274 || formid.COLUMN02 == 1273 || formid.COLUMN02 == 1277)
                {
                    Ta = all.Where(a => (a.COLUMN11 == "Item Level" || a.COLUMN11 == "Tab" || a.COLUMN11 == "Grid Level") && a.COLUMN07 != "N").OrderByDescending(a => a.COLUMN11).ToList();
                }
                var Tabs = Ta.Select(b => b.COLUMN12).Distinct();
                var itemslist = Ta.Where(b => b.COLUMN11 == "Item Level" && b.COLUMN07 != "N").ToList();
                var itemscol = itemslist.Where(b => b.COLUMN06 != null).ToList();

                var fddl = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).Select(q => new { q.COLUMN06 });
				//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                ViewBag.RefForm = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1275).FirstOrDefault().COLUMN04;
                var fid6 = fddl.Select(w => w.COLUMN06).First();
                var ddl = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName || a.COLUMN06 == fid6 || a.COLUMN02 == formid.COLUMN02);
                ViewBag.ddl = ddl.Select(a => a.COLUMN04).ToList();
                var tbl = fdata.Select(a => a.COLUMN04).Distinct().ToList();
                var distLocations = (from li in fdata.AsEnumerable()
                                     orderby li.COLUMN01
                                     select new { Location = li.COLUMN04 }).Distinct().ToList();
                tbl = distLocations.Select(a => a.Location).ToList();
                int alco = 0;
                int countryID = 0;
                for (int t = 0; t < tbl.Count; t++)
                {
                    if (formid.COLUMN02 == 1286 && t == 2) break;
                    int tblid = Convert.ToInt32(tbl[t]);
                    var tbldlist = dc.CONTABLE004.Where(a => a.COLUMN02 == tblid);
                    var table = dc.CONTABLE006.Where(a => a.COLUMN04 == tblid);
                    var tname = tbldlist.FirstOrDefault().COLUMN04; SqlCommand cmd = new SqlCommand();
                    string tblids = "";
                    for (int j = 1; j < tbl.Count; j++)
                    {
                        if (j == 1)
                            tblids = tbl[j].ToString();
                        else
                            tblids += "," + tbl[j].ToString();
                    }
                    if (formid.COLUMN02 == 1286)
                    {
                        if (t == 0)
                        {
                            cmd = new SqlCommand(
                               "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                             "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                             "where CONTABLE005.COLUMN03 =" + tblid + " and CONTABLE006.COLUMN04 =" + tblid + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN11!='Grid Level'  and   CONTABLE006.COLUMN03=" + formid.COLUMN02 + " and CONTABLE006.COLUMN07 !='N' ORDER BY CAST(CONTABLE006.COLUMN13 AS INT)  ", cn);
                        }
                        else
                        {
                            cmd = new SqlCommand(
                               "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                             "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                             "where CONTABLE005.COLUMN03 in(" + tblids + ") and CONTABLE006.COLUMN04 in(" + tblids + ") and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN11!='Grid Level'  and   CONTABLE006.COLUMN03=" + formid.COLUMN02 + " and CONTABLE006.COLUMN07 !='N' ORDER BY CAST(CONTABLE006.COLUMN13 AS INT)  ", cn);
                        }
                    }
                   cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    cmd.ExecuteNonQuery();
                    string colms = null;
                    string acolms = null;
                    string pid = null; string pidcolumn = null;
                   //EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
					 string joins = null; var ctname1 = "";
                    var CN = "";
                    var CNchk = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int tbllid = Convert.ToInt32(dt.Rows[i]["COLUMN04"]);
                        var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == tbllid).FirstOrDefault();
                        var ctname = ctbldata.COLUMN04;

                        var ccolid ="";
                        if (((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN04") && (Convert.ToString(dt.Rows[i]["COLUMN06"]) == "Item Desc")) || (Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN42") || (Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN43") || (Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN10" && Convert.ToString(dt.Rows[i]["COLUMN06"]) == "Brand") || (Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN44"))
                            ccolid = ("MATABLE007" + dt.Rows[i]["COLUMN05"]);
                            else
                           ccolid = (ctname + dt.Rows[i]["COLUMN05"]);
                        if (colms == null)
                        {
                            if (Convert.ToString(dt.Rows[i]["COLUMN14"]) == "Master Value" && Convert.ToInt32(dt.Rows[i]["COLUMN15"]) != 110000014)
                            {

                                int tid1 = Convert.ToInt32(dt.Rows[i]["COLUMN15"]);
                                var ctbldata11 = dc.CONTABLE004.Where(a => a.COLUMN02 == tid1).FirstOrDefault();
                                ctname1 = ctbldata11.COLUMN04;
                                if (ctname1 == "CONTABLE007" || ctname1 == "MATABLE007" || ctname1 == "PUTABLE001" || ctname1 == "PUTABLE003" || ctname1 == "MATABLE011"
                                 || ctname1 == "CONTABLE030" || ctname1 == "MATABLE013" || ctname1 == "FITABLE043" || ctname1 == "SATABLE005" || ctname1 == "SATABLE009"

                                    )
                                {
                                    CN = " B" + i + ".COLUMN04";
                                    CNchk = " B" + i + ".COLUMN02";
                                }
                                else if (ctname1 == "PRTABLE001" || ctname1 == "SATABLE001" || ctname1 == "SATABLE002" || ctname1 == "PUTABLE009" || ctname1 == "FITABLE037"
                                     || ctname1 == "FITABLE040"
                                    )
                                {
                                    CN = " B" + i + ".COLUMN05";
                                    CNchk = " B" + i + ".COLUMN02";
                                }
                                else
                                {
                                    CN = " B" + i + ".COLUMN04";
                                    CNchk = " B" + i + ".COLUMN02";
                                }

                                colms += dt.Rows[i]["COLUMN05"];
                                acolms += CN + " as " + "[" + ccolid + "]";
                                if ((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN10") && (Convert.ToString(dt.Rows[i]["COLUMN06"]) == "Brand") )
                                joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= B1." + dt.Rows[i]["COLUMN05"] + " AND  B" + i + ".COLUMNA03=B1.COLUMNA03";
                                else
                                joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= A." + dt.Rows[i]["COLUMN05"] + " AND  B" + i + ".COLUMNA03=A.COLUMNA03";
                                
                            }
                            else if (Convert.ToString(dt.Rows[i]["COLUMN14"]) == "Control Value")
                            {
                                ctname1 = "MATABLE002";
                                CN = " B" + i + ".COLUMN04";
                                CNchk = " B" + i + ".COLUMN02";
                                colms += dt.Rows[i]["COLUMN05"];
                                acolms += CN + " as " + "[" + ccolid + "]";
                                if ((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN42")||(Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN43")||(Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN44"))
                                    joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= B1." + dt.Rows[i]["COLUMN05"];
                                else
                                joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= A." + dt.Rows[i]["COLUMN05"];
                            }
                            else
                            {

                                colms += dt.Rows[i]["COLUMN05"];
                                if ((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN04") && (Convert.ToString(dt.Rows[i]["COLUMN06"]) == "Item Desc"))
                                    acolms += "," + "B1." + dt.Rows[i]["COLUMN05"] + " as " + "[" + ccolid + "]";
                                else
                                acolms += "A." + dt.Rows[i]["COLUMN05"] + " as " + "[" + ccolid + "]";

                            }
                        }
                        else
                        {
//EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
					
                            if (Convert.ToString(dt.Rows[i]["COLUMN14"]) == "Master Value")
                            {

                                int tid = Convert.ToInt32(dt.Rows[i]["COLUMN15"]);
                                var ctbldata1 = dc.CONTABLE004.Where(a => a.COLUMN02 == tid).FirstOrDefault();
                                ctname1 = ctbldata1.COLUMN04;
                                if (ctname1 == "CONTABLE007" || ctname1 == "MATABLE007" || ctname1 == "PUTABLE001" || ctname1 == "PUTABLE003" || ctname1 == "MATABLE011"
                                 || ctname1 == "CONTABLE030" || ctname1 == "MATABLE013" || ctname1 == "FITABLE043"
                                    )
                                {
                                    CN = " B" + i + ".COLUMN04";
                                    CNchk = " B" + i + ".COLUMN02";
                                }
                                else if (ctname1 == "PRTABLE001" || ctname1 == "SATABLE001" || ctname1 == "SATABLE002" || ctname1 == "PUTABLE009" || ctname1 == "FITABLE037"
                                     || ctname1 == "FITABLE040"
                                     )
                                {
                                    CN = " B" + i + ".COLUMN05";
                                    CNchk = " B" + i + ".COLUMN02";
                                }
                                else
                                {
                                    CN = " B" + i + ".COLUMN04";
                                    CNchk = " B" + i + ".COLUMN02";
                                }

                                colms += "," + dt.Rows[i]["COLUMN05"];
                                acolms += "," + CN + " as " + "[" + ccolid + "]";
                                 if ((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN10") && (Convert.ToString(dt.Rows[i]["COLUMN06"]) == "Brand") )
                                joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= B1." + dt.Rows[i]["COLUMN05"] + " AND  B" + i + ".COLUMNA03=B1.COLUMNA03";
                                else
                                joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= A." + dt.Rows[i]["COLUMN05"] + " AND  B" + i + ".COLUMNA03=A.COLUMNA03";
                            }
                            else if (Convert.ToString(dt.Rows[i]["COLUMN14"]) == "Control Value")
                            {
                                ctname1 = "MATABLE002";
                                CN = " B" + i + ".COLUMN04";
                                CNchk = " B" + i + ".COLUMN02";
                                colms += "," + dt.Rows[i]["COLUMN05"];
                                acolms += "," + CN + " as " + "[" + ccolid + "]";
                                if ((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN42") || (Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN43") || (Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN44"))
                                    joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= B1." + dt.Rows[i]["COLUMN05"];
                                else
                                    joins += " left outer join " + ctname1 + " B" + i + " on" + CNchk + "= A." + dt.Rows[i]["COLUMN05"];
                            }
                            else
                            {
                                colms += "," + dt.Rows[i]["COLUMN05"];
                                if ((Convert.ToString(dt.Rows[i]["COLUMN05"]) == "COLUMN04") && (Convert.ToString(dt.Rows[i]["COLUMN06"]) == "Item Desc"))
                                    acolms += "," + "B1." + dt.Rows[i]["COLUMN05"] + " as " + "[" + ccolid + "]";
                                else
                                acolms += "," + " A." + dt.Rows[i]["COLUMN05"] + " as " + "[" + ccolid + "]";
                            }
                        }
                    }

                    SqlCommand cmdp = new SqlCommand();
                    if (tbl[t].Equals(110010837) || tbl[t].Equals(110008817) || tbl[t].Equals(110010848))
                    {
                        cmdp = new SqlCommand("SELECT COLUMN04 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN03 =" + tblid + " and column04 <> 'column01'  ", cn);

                    }
                    else
                        cmdp = new SqlCommand("SELECT COLUMN04 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN05 LIKE '%Parent_Internal_ID%'  and COLUMN03 =" + tblid + " ", cn);
                    SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                    DataTable dtp = new DataTable();
                    dap.Fill(dtp);
                    var strqry = "";

                    if (dtp.Rows.Count > 0 || tbl[t].Equals(110010837) || tbl[t].Equals(110008817) || tbl[t].Equals(110010848))
                    {
                        pidcolumn = dtp.Rows[0][0].ToString();
                        //if (tname != "MATABLE007" && formid.COLUMN02 == 1286)
                        if ((tbl[t] == 110010837 || tbl[t].Equals(110008817) || tbl[t].Equals(110010848)) && tname != "MATABLE007")
                            pid = Session["IDE"].ToString();

                        else
                            pid = Session["pid"].ToString();

                        var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                        var providerName = "System.Data.SqlClient";
                        var dbs = Database.OpenConnectionString(connectionString, providerName);

                        var strqry2 = pid;
                        // //EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
                        if (formid.COLUMN02 == 1286)
                        {
                            strqry = "select m.COLUMN04  as [PUTABLE004COLUMN03],P.COLUMN05  as [PUTABLE004COLUMN05], B2.COLUMN04 as [PUTABLE004COLUMN17],p.COLUMN06 as [PUTABLE004COLUMN06], p.COLUMN08 as [PUTABLE004COLUMN08]," +
                        "p.COLUMN09 as [PUTABLE004COLUMN09], p.COLUMN10 as [PUTABLE004COLUMN10], p.COLUMN11 as [PUTABLE004COLUMN11],p.COLUMN24 as [PUTABLE004COLUMN24],p.COLUMN23 as [PUTABLE004COLUMN23]," +
                        "p.COLUMN12 as [PUTABLE004COLUMN12],p.COLUMN13 as [PUTABLE004COLUMN13],p.COLUMN14 as [PUTABLE004COLUMN14]," +
                        "p.COLUMN15 as [PUTABLE004COLUMN15],p.COLUMN16 as [PUTABLE004COLUMN16],m.COLUMN50 [MATABLE007COLUMN04],m.COLUMN42 as [MATABLE007COLUMN42],m.COLUMN43 as [MATABLE007COLUMN43]," +
                        " mf.COLUMN04 as [PUTABLE004COLUMN29], p.COLUMN30 as [PUTABLE004COLUMN30], p.COLUMN31 as [PUTABLE004COLUMN31], p.COLUMN32 as [PUTABLE004COLUMN32],p.COLUMN33 as [PUTABLE004COLUMN33], p.COLUMN34 as [PUTABLE004COLUMN34], b17.Column04 as [PUTABLE004COLUMN35], p.COLUMN36 as [PUTABLE004COLUMN36]," +
                       "m.COLUMN44 as [MATABLE007COLUMN44],m.COLUMN10 as [MATABLE007COLUMN10],m21.COLUMN04 as [PUTABLE004COLUMN37] from PUTABLE004 p LEFT JOIN MATABLE007 m ON m.COLUMN02=p.COLUMN03 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 LEFT JOIN MATABLE007 mf ON mf.COLUMN02=p.COLUMN29 and mf.COLUMNA03=p.COLUMNA03 and isnull(mf.COLUMNA13,0)=0  left join MATABLE002 B2 on b2.Column02=p.COLUMN17 and b2.Column03 =11119 left join MATABLE002 b17  on b17.Column02=p.Column35 Left join MATABLE002 m21 on m21.column02 = p.COLUMN37 and m21.Column03 =11124 and isnull(m21.COLUMNA13,0)=0    where p.COLUMN12=" + strqry2 + "  and isnull(p.COLUMNA13,0)=0";
                        }
                        // strqry = "select  " + acolms + " from " + tname + " A " + joins + " where A.COLUMN12 =" + strqry2 + " and  A.COLUMNA03=" + Session["AcOwner"] + "  and A.COLUMNA13='False' ";
                        var sql = strqry;
                        var GData = dbs.Query(sql);

                        ViewBag.itemsdata = GData;

                        SqlCommand gcmd = new SqlCommand();
                        //if (formid.COLUMN02 == 1286 && t >= 1)
                            gcmd = new SqlCommand(strqry, cn);
                       // else
                           // gcmd = new SqlCommand("select  " + acolms + " from " + tname + " where " + pidcolumn + "=" + pid + "  ", cn);
                        SqlDataAdapter gda = new SqlDataAdapter(gcmd);
                        DataTable gdt = new DataTable();
                        gda.Fill(gdt);
                        ArrayList icols = new ArrayList();
                        ArrayList bcols = new ArrayList();
                        ArrayList PTcols = new ArrayList();
                        ArrayList Tcols = new ArrayList();
                        ArrayList TPcols = new ArrayList();
                        ArrayList TPcols1 = new ArrayList();
                        ArrayList lotcols = new ArrayList();
                        ArrayList MofMarging = new ArrayList();
                        for (int i = 0; i < gdt.Rows.Count; i++)
                        {
                            if (formid.COLUMN02 == 1286)
                            {
                                icols.Add(gdt.Rows[i]["PUTABLE004COLUMN03"].ToString());
                                bcols.Add(gdt.Rows[i]["PUTABLE004COLUMN17"].ToString());
                                PTcols.Add(gdt.Rows[i]["PUTABLE004COLUMN37"].ToString());
                                Tcols.Add(gdt.Rows[i]["MATABLE007COLUMN43"].ToString());
                                TPcols.Add(gdt.Rows[i]["PUTABLE004COLUMN29"].ToString());
                                TPcols1.Add(gdt.Rows[i]["MATABLE007COLUMN10"].ToString());
                                lotcols.Add(gdt.Rows[i]["PUTABLE004COLUMN24"].ToString());
                                MofMarging.Add(gdt.Rows[i]["PUTABLE004COLUMN35"].ToString());

                            }

                        }
                        ViewBag.gitemsdata = icols;
                        ViewBag.bgitemsdata = bcols;
                        ViewBag.PTitemsdata = PTcols;
                        ViewBag.Titemsdata = Tcols;
                        ViewBag.TPitemsdata = TPcols;
                        ViewBag.TPitemsdata1 = TPcols1;
                        ViewBag.lotcols = lotcols;
                        ViewBag.MofMarging = MofMarging;
                    }
                    else
                    {
                        pidcolumn = "COLUMN02";
                        pid = ide; var collname = "";
                        if (tname == "PUTABLE001")
                            collname = "COLUMN02";
                        else if (tname == "PUTABLE003")
                            collname = "COLUMN05";
                        else if (tname == "PUTABLE005")
                            collname = "COLUMN05";

                        if (collname != null && collname != "")
                        {
                            SqlCommand cmddi = new SqlCommand("select   " + collname + "  from  " + tname + "  where COLUMN02=" + pid + "  ", cn);
                            SqlDataAdapter dadi = new SqlDataAdapter(cmddi);
                            DataTable dtdi = new DataTable();
                            dadi.Fill(dtdi);
                            if (dtdi.Rows.Count > 0)
                            {
                                Session["POID"] = dtdi.Rows[0][0].ToString();
                            }
                        }
                        if (t != 0 && (formid.COLUMN02 == 1252 || formid.COLUMN02 == 1265 || formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                        {
                            pid = Session["pid"].ToString();
                        }
                        else if (formid.COLUMN02 == 1285)
                        {
                            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                            var providerName = "System.Data.SqlClient";
                            var dbs = Database.OpenConnectionString(connectionString, providerName);
                            var sql = "select  " + acolms + " from " + tname + " where " + pidcolumn + "=" + pid + "  and COLUMNA13='False' ";
                            var GData = dbs.Query(sql);
                            ViewBag.itemsdata = GData;
                        }
                        else if (tname == "PUTABLE009")
                        {
                            pid = Session["pid"].ToString();
                            pidcolumn = "COLUMN19";
                        }
                        else if (tname == "SATABLE003")
                        {
                            pid = Session["pid"].ToString();
                            pidcolumn = "COLUMN20";
                        }
                    }

                    SqlCommand cmdd = new SqlCommand();
                    if (tname == "SATABLE003" && formid.COLUMN02 == 1252)
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='Vendor'  and columna03=" + Session["AcOwner"] + "", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='Employee' and columna03=" + Session["AcOwner"] + " ", cn);
                    else if (tname == "SATABLE003" && formid.COLUMN02 == 1265)
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='Customer'  and columna03=" + Session["AcOwner"] + "", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1275 || formid.COLUMN02 == 1283))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='SALESORDER'  and columna03=" + Session["AcOwner"] + "", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1378))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='PROJECT'  and columna03=" + Session["AcOwner"] + "", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1380))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='PROJECTWORKORDER'  and columna03=" + Session["AcOwner"] + "", cn);
                    else if (tname == "SATABLE003" && (formid.COLUMN02 == 1388))
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + " and column19 ='CONTACTS'  and columna03=" + Session["AcOwner"] + "", cn);
                    else if (formid.COLUMN02 == 1286 && t >= 1)
                        cmdd = new SqlCommand(strqry, cn);
                    else
                        cmdd = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + "  and columna03=" + Session["AcOwner"] + " ", cn);
                    SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                    DataTable dtd = new DataTable();
                    dad.Fill(dtd);
                    cn.Close();
                    SqlCommand cmdfp = new SqlCommand();

                    if (t == 0 && (formid.COLUMN02 == 1252 || formid.COLUMN02 == 1265 || formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                    {
                        if (formid.COLUMN02 == 1252)
                        {
                            cmdfp = new SqlCommand("select COLUMN17 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid.COLUMN02 == 1265)
                        {
                            cmdfp = new SqlCommand("select COLUMN16 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else if (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415)
                        {
                            cmdfp = new SqlCommand("select COLUMN25 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        }
                        else
                        {
                            cmdfp = new SqlCommand("select  " + colms + " from " + tname + " where " + pidcolumn + "=" + pid + "  ", cn);
                        }
                        SqlDataAdapter dafp = new SqlDataAdapter(cmdfp);
                        DataTable dtfp = new DataTable();
                        dafp.Fill(dtfp);
                        string ppid = dtfp.Rows[0][0].ToString();
                        Session["pid"] = ppid;
                    }
                    else if (t == 0)
                    {
                        cmdfp = new SqlCommand("select COLUMN01 from " + tname + " where " + pidcolumn + "=" + pid + "", cn);
                        SqlDataAdapter dafp = new SqlDataAdapter(cmdfp);
                        DataTable dtfp = new DataTable();
                        dafp.Fill(dtfp);
                        string ppid = dtfp.Rows[0][0].ToString();
                        Session["pid"] = ppid;
                    }
                    for (int row1 = 0; row1 < dtd.Rows.Count; row1++)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {
                            all1.Add(new MeBizSuiteAppUI.Controllers.FormBuildClass { Field_Name = "", Label_Name = "", Action = "", Mandatory = "", Default_Value = "", Control_Type = "", Section_Type = "", Section_Name = "", Form_Id = null, Table_Id = null, Data_Type = "" });
                            int ctblid = Convert.ToInt32(dt.Rows[row][9]);
                            var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                            var ctname = ctbldata.COLUMN04;
                            all1[alco + row].Field_Name = (ctname + dt.Rows[row][0].ToString());
                            all1[alco + row].Label_Name = (dt.Rows[row][1].ToString());
                            all1[alco + row].Action = (dt.Rows[row][2].ToString());
                            all1[alco + row].Mandatory = (dt.Rows[row][3].ToString());
                            if (dt.Rows[row][10].ToString() == "image" || dt.Rows[row][10].ToString() == "IMAGE")
                            {
                                string imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
                                var moduleid1 = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName).FirstOrDefault().COLUMN06.ToString();
                                int moduleid = Convert.ToInt32(moduleid1.ToString());
                                string modulename = dc.CONTABLE003.Where(a => a.COLUMN06 == moduleid).FirstOrDefault().COLUMN05.ToString();
                                string path = "/" + modulename + "/" + FormName + "/" + dtd.Rows[row1][row].ToString();
                                all1[alco + row].Default_Value = (path);
                            }
                            else
                                all1[alco + row].Default_Value = (dtd.Rows[row1][row].ToString());

                            all1[alco + row].Control_Type = (dt.Rows[row][5].ToString());
                            all1[alco + row].Section_Type = (dt.Rows[row][6].ToString());
                            all1[alco + row].Section_Name = (dt.Rows[row][7].ToString());
                            all1[alco + row].Form_Id = Convert.ToInt32((dt.Rows[row][8].ToString()));
                            all1[alco + row].Table_Id = Convert.ToInt32((dt.Rows[row][9].ToString()));
                            all1[alco + row].Data_Type = (dt.Rows[row][10].ToString());
                            all1[alco + row].Tab_Index = Convert.ToInt32((dt.Rows[row]["COLUMN13"].ToString()));
                            var firstname = dt.Rows[row][1].ToString(); ViewData[firstname] = "";
                            var dval = dtd.Rows[row1][row].ToString();
                            var ddata = dt.Rows[row]["COLUMN14"].ToString();
                            if (formid.COLUMN02 == 1275 && tbl[t] == 110010823 && dt.Rows[row][0].ToString() == "COLUMN04")
                            {
                                SqlCommand cmdr = new SqlCommand("select COLUMN11 from PUTABLE001 where COLUMN11='" + dtd.Rows[row1][row].ToString() + "'  and   " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' ", cn);
                                SqlDataAdapter dar = new SqlDataAdapter(cmdr);
                                DataTable dtr = new DataTable();
                                dar.Fill(dtr);
                                if (dtr.Rows.Count > 0)
                                    Session["SalesReturn"] = dtr.Rows[0][0].ToString();
                                else
                                    Session["SalesReturn"] = null;
                            }
                            if (ddata == "Control Value")
                            {
                                int ddatavalue = Convert.ToInt32(dt.Rows[row]["COLUMN15"].ToString());
                                List<SelectListItem> Country = new List<SelectListItem>();
                                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                                var acOW = Convert.ToInt32(Session["AcOwner"]);
                                List<MATABLE002> dropdata = new List<MATABLE002>();
                                //if (ddatavalue == 11130 || ddatavalue == 11131 || ddatavalue == 11114 || ddatavalue == 11115 || ddatavalue == 11127 || ddatavalue == 11124 || ddatavalue == 11125 ||
                                //ddatavalue == 11126 || ddatavalue == 11122 || ddatavalue == 11123 || ddatavalue == 11132 || ddatavalue == 11128 || ddatavalue == 11133 || ddatavalue == 11134 || ddatavalue == 11135)
                                //    dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddatavalue).ToList();
                                //else
                                //{}
                                dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddatavalue && (a.COLUMNA03 == acOW || a.COLUMNA03 == null)).ToList();
                                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                                    dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                                //else
                                //    dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString())).ToList();
                                for (int dd = 0; dd < dropdata.Count; dd++)
                                {
                                    Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
                                   //EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
					 if (dval == dropdata[dd].COLUMN02.ToString())
                                        all1[alco + row].Field_Text = dropdata[dd].COLUMN04;
                                }
                                ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                            }
                            else if (ddata == "Master Value")
                            {
                                int ddatavalue = Convert.ToInt32(dt.Rows[row]["COLUMN15"].ToString());
                                List<SelectListItem> Country = new List<SelectListItem>();
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddatavalue).OrderBy(q => q.COLUMN02);
                                var tblName = tblddl.Select(q => q.COLUMN04).First();
                                if (formid.COLUMN02 == 1272 && dt.Rows[row][0] == "COLUMN03")
                                {
                                    SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN03 from PUTABLE002", cn);
                                    DataTable dtdata2 = new DataTable();
                                    cmdd2.Fill(dtdata2); string dcol = "";
                                    for (int dd2 = 0; dd2 < dtdata2.Rows.Count; dd2++)
                                    {
                                        if (dd2 == 0)
                                            dcol = dtdata2.Rows[dd2][0].ToString();
                                        else
                                            dcol += " or COLUMN02=" + dtdata2.Rows[dd2][0].ToString();
                                    }
                                    if (dcol != "")
                                    {
                                        SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN02=" + dcol + " ", cn);
                                        DataTable dtdata = new DataTable();
                                        cmddl.Fill(dtdata);
                                        for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                        {
                                            Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                            //EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
					if (dval == dtdata.Rows[dd]["COLUMN02"].ToString())
                                                all1[alco + row].Field_Text = dtdata.Rows[dd]["COLUMN04"].ToString();
                                        }
                                    }
                                    ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                                }

                                else if (formid.COLUMN02 == 1283 || formid.COLUMN02 == 1284 || formid.COLUMN02 == 1286 || formid.COLUMN02 == 1287 || formid.COLUMN02 == 1288)
                                {
                                    SqlDataAdapter cmddl = new SqlDataAdapter();
                                    if (tblName == "SATABLE009")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN19 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "PRTABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                    else if (tblName == "MATABLE009")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN06 ='true' or COLUMN06 ='1'", cn);
                                    else if (tblName == "MATABLE011")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN06 ='true' or COLUMN06 ='1'", cn);
                                    else if (tblName == "SATABLE005" && formid.COLUMN02 == 1286)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "    where  COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "SATABLE005" || tblName == "PUTABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN04='JobOrder' )  And  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (formid.COLUMN02 == 1283 && tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (formid.COLUMN02 == 1286 && tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);

                                    else if (tblName == "SATABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "MATABLE016")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE017")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "CONTABLE007")
										//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else if (tblName == "SATABLE019")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and (COLUMN13='' or isnull(COLUMN13,0)=0) and isnull(COLUMNA13,'False')='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);

                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                        //EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
					if (dval == dtdata.Rows[dd]["COLUMN02"].ToString())
                                            all1[alco + row].Field_Text = dtdata.Rows[dd]["COLUMN04"].ToString();
                                    }
                                    ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                                }
                                else
                                {
                                    SqlDataAdapter cmddl = new SqlDataAdapter();
                                    if (tblName == "SATABLE002" && (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1415))
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where COLUMN22='True'", cn);
                                    else if (tblName == "PRTABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                    else if (tblName == "PUTABLE001" && formid.COLUMN02 == 1329)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN29 in (Select COLUMN02 from MATABLE011 where COLUMN04='Return Order' ) order by Column02 desc", cn);
                                    else if (tblName == "PUTABLE003" && formid.COLUMN02 == 1330)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN17 in (Select COLUMN02 from MATABLE011 where COLUMN04='Return Order' ) order by Column02 desc", cn);
                                    else if (tblName == "SATABLE002" || tblName == "SATABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE016")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE017")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                    else if (tblName == "MATABLE010" && (formid.COLUMN02 == 1260 || formid.COLUMN02 == 1265 || formid.COLUMN02 == 1415))
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 from " + tblName + " ", cn);
                                    else if (tblName == "FITABLE020")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN21 as COLUMN04 from " + tblName + " where  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (tblName == "CONTABLE007")
										//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + " where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "", cn);
                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dval);
                                }
                            }
                            else if (ddata == "GridView")
                            {
                                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                                var providerName = "System.Data.SqlClient";
                                var sql = "";
                                List<string> colNM = new List<string>();
                                List<string> TDataC = new List<string>();
                                int ddatavalue = Convert.ToInt32(dt.Rows[row]["COLUMN15"].ToString());
                                var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddatavalue).OrderBy(q => q.COLUMN02);
                                var tbleID = Convert.ToInt32(Session["TBLID"]);
                                var modID = tblddl.Where(q => q.COLUMN02 == ddatavalue).Select(q => q.COLUMN03).First();
                                var tblName = tblddl.Where(q => q.COLUMN03 == modID && q.COLUMN02 == ddatavalue).Select(q => q.COLUMN04).First();
                                var tblNameID = tblddl.Select(q => q.COLUMN02).First();
                                var formID = dc.CONTABLE006.Where(a => a.COLUMN04 == tblNameID && a.COLUMN07 == "Y").OrderBy(q => q.COLUMN01).Take(4).ToList();
                                SqlCommand cmdit = new SqlCommand("select column02 from  FITABLE010 where column03=" + ide + "", cn);
                                SqlDataAdapter dait = new SqlDataAdapter(cmdit);
                                DataTable dtit = new DataTable();
                                dait.Fill(dtit);
                                if (formID.Count == 0)
                                {
                                    //var colLST = dc.CONTABLE005.Where(a=>a.COLUMN03==ddata).Select(q => q.COLUMN05).ToList();
                                    //foreach (string dtr in colLST)
                                    //{
                                    //    colNM.Add(dtr);
                                    //}
                                    //EMPHCS772 - gnaneshwar  26/7/2015  Hiding Commit QTy field from item master inventory view

                                    colNM.Add("Operating Unit"); colNM.Add("Qty OnHand"); colNM.Add("Qty Backordered"); colNM.Add("Qty OnOrder"); colNM.Add("Qty Available"); colNM.Add("Qty Intransit"); colNM.Add("Qty WIP"); colNM.Add("Total Price"); colNM.Add("Avg Price");
                                    ViewBag.EFormName = "";
                                    if (dtit.Rows.Count > 0)
                                    {
                                        sql = "select  (select column03 from CONTABLE007 where COLUMN02=(FITABLE010.COLUMN13))  [Operating Unit],isnull(COLUMN04,0) as [Qty OnHand],isnull(COLUMN06,0) as [Qty Backordered],isnull(COLUMN07,0) as [Qty OnOrder],isnull(COLUMN08,0)  as [Qty Available],isnull(COLUMN09,0)  as [Qty Intransit],isnull(COLUMN18,0)  as [Qty WIP],isnull(COLUMN12,0)  as [Total Price],isnull(COLUMN17,0)  as [Avg Price] from FITABLE010 where COLUMN03=" + ide + "";
                                    }
                                    else
                                    {
                                        sql = "select  (select column03 from CONTABLE007 where COLUMN02=matable007.COLUMN37)  [Operating Unit],0 as [Qty OnHand],0  as [Qty Backordered],0 as [Qty OnOrder],0 as [Qty Available],0  as [Qty Intransit],0 as [Qty WIP],0  as [Total Price],0  as [Avg Price] from matable007 where column02=" + ide + "";
                                    }
                                }
                                else
                                {
                                    var frmIDE = formID.Select(q => q.COLUMN03).First();
                                    var colLST = formID.Select(q => q.COLUMN06).ToList();
                                    var formName = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmIDE);
                                    var formNameE = formName.Select(q => q.COLUMN04).First();
                                    foreach (string dtr in colLST)
                                    {
                                        colNM.Add(dtr);
                                    }
                                    ViewBag.EFormName = formNameE;
                                    var ptbl = "PUTABLE004";
                                    if (tname == "MATABLE007" && tblName == "PUTABLE004")
                                    { ptbl = "PUTABLE004"; pid = ide; }
                                    else if (tname == "SATABLE009")
                                        ptbl = "SATABLE009";
                                    else if (tname == "SATABLE008")
                                        ptbl = "SATABLE007";
                                    else if (tname == "PUTABLE004")
                                        ptbl = "PUTABLE003";
                                    else if (tname == "FITABLE015")
                                        ptbl = "FITABLE014";

                                    var billsql = "  select COLUMN02 from " + ptbl + " where COLUMN02 =" + pid + "";
                                    var billdb = Database.OpenConnectionString(connectionString, providerName);
                                    var billGData = billdb.Query(billsql);
                                    var billid = billGData.FirstOrDefault();
                                    if (billid == null)
                                    {
                                        ViewBag.EFormName = "";
                                        sql = "select top(1) 0 ID ,0 as [" + colNM[0] + "],0 as [" + colNM[1] + "],0 as [" + colNM[2] + "],0 as [" + colNM[3] + "] from " + tblName + " ";
                                    }
                                    else if (formid.COLUMN02 == 1252)
                                    {
                                        billid = billid[0]; colNM[0] = "Mobile#"; colNM[3] = "Designation";
                                        sql = "select COLUMN02 AA ,COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN12 as Designation,COLUMN16 as Mobile# from " + tblName + " where COLUMN11=" + billid + " and COLUMN10='22305'";
                                    }
                                    else if (formid.COLUMN02 == 1265)
                                    {
                                        billid = billid[0]; colNM[0] = "Mobile#"; colNM[3] = "Designation";
                                        sql = "select COLUMN02 AA ,COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN12 as Designation,COLUMN16 as Mobile# from " + tblName + " where COLUMN11=" + billid + " and COLUMN10='22335'";
                                    }
                                    else
                                    {
                                        billid = billid[0];
                                        sql = "select COLUMN02 AA ,COLUMN03 as [" + colNM[0] + "],COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN06 as [" + colNM[3] + "] from " + tblName + " where COLUMN03=" + billid + "";
                                    } TDataC.Add("COLUMN02");
                                }
                                
                                var dbs = Database.OpenConnectionString(connectionString, providerName);
                                var GData = dbs.Query(sql);
                                var grid = new WebGrid(GData, canPage: false, canSort: false);
                                foreach (string dtr in colNM)
                                {

                                    TDataC.Add(dtr);
                                }
                                ViewBag.GridDynamicData = GData;
                                ViewBag.GridDynamicDataC = TDataC;
                                ViewBag.TableIDE = ddata;
                            }
                        }
                        alco += dt.Rows.Count;
                       
                        break;
                    }
                }
                var itemlist = all.Where(a => a.COLUMN11 == "Item Level").ToList();
                var litemlist = itemlist.Select(a => a.COLUMN06).ToList();
                ViewBag.itemscol = litemlist;
                var result1 = new List<dynamic>();
                if (litemlist.Count > 0)
                {
                    var obj = (IDictionary<string, object>)new System.Dynamic.ExpandoObject();
                    foreach (var row1 in litemlist)
                    {
                        obj.Add(row1, row1);
                    }
                    result1.Add(obj);
                }
                ViewBag.Tabs = Tabs;
                ViewBag.itemslist = result1;
                if (formid.COLUMN02 == 1330 || formid.COLUMN02 == 1285 || formid.COLUMN02 == 1286)
                {
                    all1 = (from p in all1
                            where p.Label_Name != null
                            orderby (p.Tab_Index)
                            select p).ToList();
                }
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                
                return View("~/Views/Manufacture/JobOrderReceipt/Detailes.cshtml", all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult GetItemDetails(string ItemID)
        {
            try
            {
                //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                string OPUnit = null; string OPUnitstatus = "0";
                string AcOwner = Session["AcOwner"].ToString();
                if (Session["OPUnitstatus"] != null)
                    OPUnitstatus = Session["OPUnitstatus"].ToString();
                if (OPUnitstatus == "1")
                    OPUnit = Session["OPUnit"].ToString();
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_PUR_TP_ItemDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                cmd.Parameters.Add(new SqlParameter("@OPUnit", OPUnit));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Close();
                var item = "";
                var upc = "";
                var itemdesc = "";
                var color = "";
                var style = "";
                var size  = "";
                var brand = "";
                
                if (dt.Rows.Count > 0)
                {
                    item = dt.Rows[0]["COLUMN02"].ToString();
                    upc = dt.Rows[0]["COLUMN06"].ToString();
                    itemdesc = dt.Rows[0]["COLUMN09"].ToString();
                    color = dt.Rows[0]["COLUMN42"].ToString();
                    style = dt.Rows[0]["COLUMN43"].ToString();
                    size  = dt.Rows[0]["COLUMN44"].ToString();
                    brand = dt.Rows[0]["COLUMN45"].ToString();
                }
                else
                {
                    item = "";
                    upc = "";
                    itemdesc = "";
                    color = "";
                    style = "";
                    size = "";
                    brand = "";

                }
                return Json(new
                {
                    Data = item,
                    Data1 = upc,
                    Data2 = itemdesc,
                    Data3 = color,
                    Data4 = style,
                    Data5 = size,
                    Data6 = brand

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult JOGetItemDetails(string ItemID,string JO)
        {
            try
            {
                //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                string OPUnit = null; string OPUnitstatus = "0";
                string AcOwner = Session["AcOwner"].ToString();
                if (Session["OPUnitstatus"] != null)
                    OPUnitstatus = Session["OPUnitstatus"].ToString();
                if (OPUnitstatus == "1")
                    OPUnit = Session["OPUnit"].ToString();
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_PUR_TP_JOItemDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                cmd.Parameters.Add(new SqlParameter("@JO", JO));
                //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                cmd.Parameters.Add(new SqlParameter("@OPUnit", OPUnit));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Close();
                var item = "";
                var upc = "";
                var itemdesc = "";
                var color = "";
                var style = "";
                var size  = "";
                var brand = "";
                var price = "";
                var Distprice = "";
                var UOM = "";
                var FG = "";
                if (dt.Rows.Count > 0)
                {
                    item = dt.Rows[0]["COLUMN02"].ToString();
                    upc = dt.Rows[0]["COLUMN06"].ToString();
                    itemdesc = dt.Rows[0]["COLUMN09"].ToString();
                    color = dt.Rows[0]["COLUMN42"].ToString();
                    style = dt.Rows[0]["COLUMN43"].ToString();
                    size  = dt.Rows[0]["COLUMN44"].ToString();
                    brand = dt.Rows[0]["COLUMN10"].ToString();
                    price = dt.Rows[0]["COLUMN17"].ToString();
                    Distprice = dt.Rows[0]["COLUMN45"].ToString();
                    UOM = dt.Rows[0]["COLUMN63"].ToString();
                    FG = dt.Rows[0]["FG"].ToString();
                }
                else
                {
                    item = "";
                    upc = "";
                    itemdesc = "";
                    color = "";
                    style = "";
                    size = "";
                    brand = "";
                    price = "";
                    Distprice = "";
                    UOM = "";
                    FG = "";
                }
                return Json(new
                {
                    Data = item,
                    Data1 = upc,
                    Data2 = itemdesc,
                    Data3 = color,
                    Data4 = style,
                    Data5 = size,
                    Data6 = brand,
                    Data7 = price,
                    Data8 = Distprice,
                    Data9 = UOM,
                    Data10 = FG

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
		//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
        public List<CONTABLE006> getAutoFeildDetails(DataTable adt)
        {
            List<CONTABLE006> itemdata = adt.AsEnumerable().Select(x => new CONTABLE006
            {
                COLUMN05 = ((x["COLUMN05"].ToString() == null || x["COLUMN05"].ToString() == "") ? null : (string)(x["COLUMN05"])),
                COLUMN06 = ((x["COLUMN06"].ToString() == null || x["COLUMN06"].ToString() == "") ? null : (string)(x["COLUMN06"])),
                COLUMN07 = ((x["COLUMN07"].ToString() == null || x["COLUMN07"].ToString() == "") ? null : (string)(x["COLUMN07"])),
                COLUMN08 = ((x["COLUMN08"].ToString() == null || x["COLUMN08"].ToString() == "") ? null : (string)(x["COLUMN08"])),
                COLUMN09 = ((x["COLUMN09"].ToString() == null || x["COLUMN09"].ToString() == "") ? null : (string)(x["COLUMN09"])),
                COLUMN10 = ((x["COLUMN10"].ToString() == null || x["COLUMN10"].ToString() == "") ? null : (string)(x["COLUMN10"])),
                COLUMN11 = ((x["COLUMN11"].ToString() == null || x["COLUMN11"].ToString() == "") ? null : (string)(x["COLUMN11"])),
                COLUMN12 = ((x["COLUMN12"].ToString() == null || x["COLUMN12"].ToString() == "") ? null : (string)(x["COLUMN12"])),
                COLUMN03 = ((x["COLUMN03"].ToString() == null || x["COLUMN03"].ToString() == "") ? null : (int?)(x["COLUMN03"])),
                COLUMN04 = ((x["COLUMN04"].ToString() == null || x["COLUMN04"].ToString() == "") ? null : (int?)(x["COLUMN04"])),
                COLUMN13 = ((x["COLUMN13"].ToString() == null || x["COLUMN13"].ToString() == "") ? null : (string)(x["COLUMN13"])),
                COLUMN14 = ((x["COLUMN14"].ToString() == null || x["COLUMN14"].ToString() == "") ? null : (string)(x["COLUMN14"])),
                COLUMN15 = ((x["COLUMN15"].ToString() == null || x["COLUMN15"].ToString() == "") ? null : (int?)(x["COLUMN15"]))
            }).ToList();
            return itemdata;
        }
        public ActionResult getProductiontoJORDetails(string ProductionID)
        {
            try
            {
                var JOID = 0;var JIID = "";
                SqlCommand cmdpo = new SqlCommand();
                cmdpo = new SqlCommand("select column02,column04,column05,column06 from SAtable019 where column02='" + ProductionID + "'", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0]["column06"] != "")
                        JOID = Convert.ToInt32(dtpo.Rows[0]["column06"]);
                    JIID = dtpo.Rows[0]["column05"].ToString();
                }
                AddressMaster obj = new AddressMaster();
                var addressText = obj.GetAdrs(JOID);
                var Jobber = "";
                var JobOrder = "";
                var Reference = "";
                var Memo = "";
                var Operating_Unit = "";
                int frmid = (int)Session["id"];
                SqlCommand cmdj = new SqlCommand("usp_JO_TP_ProductionJOR_HeaderDetails", cn);
                cmdj.CommandType = CommandType.StoredProcedure;
                cmdj.Parameters.Add(new SqlParameter("@ProductionID", ProductionID));
                cmdj.Parameters.Add(new SqlParameter("@JoborderID", JOID));
                SqlDataAdapter daj = new SqlDataAdapter(cmdj);
                DataTable dtj = new DataTable();
                daj.Fill(dtj);
                if (dtj.Rows.Count > 0)
                {
                    Jobber = dtj.Rows[0]["COLUMN05"].ToString();
                    JobOrder = dtj.Rows[0]["COLUMN06"].ToString();
                    Reference = dtj.Rows[0]["COLUMN10"].ToString();
                    Memo = dtj.Rows[0]["COLUMN12"].ToString();
                    Operating_Unit = dtj.Rows[0]["COLUMN13"].ToString();
                }
                else
                {
                    Jobber = "";
                    JobOrder = "";
                    Reference = "";
                    Memo = "";
                    Operating_Unit = "";
                }
                SqlCommand cmd = new SqlCommand("usp_JO_TP_ProductionJOR_LineDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ProductionID", ProductionID));
                cmd.Parameters.Add(new SqlParameter("@JoborderID", JOID));
                cmd.Parameters.Add(new SqlParameter("@JoborderIssueID", JIID));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmd.Parameters.Add(new SqlParameter("@Opunit", Session["OPUnit1"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                var formid = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmid).FirstOrDefault();
                var data = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02).ToList();
                var itemdata = data.Where(a => a.COLUMN11 == "Item Level" && a.COLUMN07 != "N").ToList();
                var tblid = itemdata.Select(a => a.COLUMN04).Distinct().ToList();
                var tblids= "";
                for (int j = 0; j < tblid.Count; j++)
                {
                    if (j == 0)
                        tblids = tblid[j].ToString();
                    else
                        tblids += "," + tblid[j].ToString();
                }
                SqlCommand acmd = new SqlCommand();
                    acmd = new SqlCommand(
                             "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN08,CONTABLE006.COLUMN13,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15 From CONTABLE006  " +
                           "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                           " left join SETABLE011 s on s.COLUMN04 in( " + tblids + ") and CONTABLE006.COLUMN05 =s.COLUMN05 and s.COLUMN06 = " + formid.COLUMN02 + "  " +
                           " left join SETABLE012 s1 on s.COLUMN04 = s1.COLUMN04  and s1.COLUMN05 =s.COLUMN05 and s.COLUMN06 = s1.COLUMN06 and s1.COLUMNA03 = " + Session["AcOwner"] + " " +
                           "where CONTABLE005.COLUMN03 in( " + tblids + ") and CONTABLE006.COLUMN04 in( " + tblids + ") and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level'  and  iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)='Y'   ORDER BY CAST(CONTABLE006.COLUMN13 AS INT) ", cn);
                SqlDataAdapter ada = new SqlDataAdapter(acmd);
                DataTable adt = new DataTable();
                ada.Fill(adt);
                itemdata = getAutoFeildDetails(adt);
                var result = new List<dynamic>();
                if (itemdata.Count > 0)
                {
                    var obj1 = (IDictionary<string, object>)new System.Dynamic.ExpandoObject();
                    foreach (var row1 in itemdata)
                    {
                        obj1.Add(row1.COLUMN06, row1.COLUMN06);
                    }
                    result.Add(obj1);
                }
                string[] Inlinecolumns = new string[itemdata.Count];
                string htmlstring = ""; string line = ""; string dvalue = ""; int gfrow = 0;
                List<WebGridColumn> gcol = new List<WebGridColumn>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var coo = 0; int itcount = 0;
                    for (int g = 1; g <= itemdata.Count; g++)
                    {
                        if (i == 0) { gfrow = 0; } else { gfrow = 1; }
                        for (int q = coo; q < itemdata.Count; q++)
                        {
                            if (dt.Columns.Contains(itemdata[q].COLUMN05))
                                itemdata[q].COLUMN09 = dt.Rows[i][itemdata[q].COLUMN05].ToString();
                            int ctblid = Convert.ToInt32(itemdata[q].COLUMN04);
                            var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                            var ctname = ctbldata.COLUMN04;
                            var cid = (ctname + itemdata[q].COLUMN05);
                            string id = (from DataRow dr in adt.Rows
                                         where (string)dr["COLUMN05"] == itemdata[q].COLUMN05 && (int)dr["COLUMN04"] == (itemdata[q].COLUMN04)
                                         select (string)dr["DataType"]).FirstOrDefault();
                            string giid = "a" + cid;
                            if (gfrow == 0)
                            {
                                if (q == 0)
                                {
                                    var LineID = dt.Rows[i]["LineID"].ToString(); if (LineID == "") LineID = "0";
                                    string hiddenlineid = "<input type='input'  id='LineID'  value=" + LineID + " style='display:none'  />";
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = "",
                                        Header = "",
                                        Format = (item) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'    value='' checked='true'   />" + hiddenlineid + "")
                                    });
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = "UPC",
                                        Header = "UPC",
                                        Format = (item) => new HtmlString("<input id='UPCSearch' class='gridWH' pattern='' itemid='' name='' value='' type='text'>")
                                    });
                                }
                                if (itemdata[q].COLUMN10 == "TextBox")
                                {
                                    string hiddenlineid = "", ahiddenlineid = "a" + cid + "";
                                    if (cid == "PUTABLE004COLUMN08") hiddenlineid = "<input type='input'  id='" + ahiddenlineid + "'  value=" + itemdata[q].COLUMN09 + " style='display:none'  />";
                                   if (id == "INT" || id == "int" || id == "BIT" || id == "bit" || id == "DECIMAL" || id == "decimal")
                                    {
                                            gcol.Add(new WebGridColumn()
                                            {
                                                ColumnName = itemdata[q].COLUMN06,
                                                Header = itemdata[q].COLUMN06,
                                                Format = (item) => new HtmlString("<input  type='text'  onchange='itemChange(this);' value='" + itemdata[q].COLUMN09 + "'  id='" + cid + "' class='txtintgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' name='" + itemdata[q].COLUMN06 + "'   />"+hiddenlineid+"")
                                            });
                                    }
                                    else
                                    {
                                        gcol.Add(new WebGridColumn()
                                        {
                                            ColumnName = itemdata[q].COLUMN06,
                                            Header = itemdata[q].COLUMN06,
                                            Format = (item) => new HtmlString("<input  type='text'  onchange='itemChange(this);' value='" + itemdata[q].COLUMN09 + "'  id='" + cid + "' class='txtgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' name='" + itemdata[q].COLUMN06 + "'   />"+hiddenlineid+"")

                                        });
                                    }
                                }
                                else if (itemdata[q].COLUMN10 == "DropDownList")
                                {
                                        List<SelectListItem> Country = new List<SelectListItem>();
                                        var firstname = itemdata[q].COLUMN06; ViewData[firstname] = "";
                                        var acOW = Convert.ToInt32(Session["AcOwner"]);
                                        if (itemdata[q].COLUMN14 == "Control Value")
                                        {
                                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
                                            var dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata && (a.COLUMNA03 == acOW || a.COLUMNA03 == null)).ToList();
                                            for (int dd = 0; dd < dropdata.Count; dd++)
                                            {
                                                Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
                                            }
                                            Country = Country.OrderBy(x => x.Text).ToList();
                                            Country.Insert(0, (new SelectListItem { Value = "0", Text = "--Select--" }));
                                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: itemdata[q].COLUMN09);
                                        }
                                        else if (itemdata[q].COLUMN14 == "Master Value")
                                        {
                                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
                                            var tblddl = dc.CONTABLE004.Where(r => r.COLUMN02 == ddata).OrderBy(r => r.COLUMN02);
                                            var tblName = tblddl.Select(r => r.COLUMN04).First();
                                            SqlDataAdapter cmddl = new SqlDataAdapter();
                                            if (tblName == "MATABLE007")
                                                cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'   AND COLUMN02='" + dt.Rows[i]["COLUMN03"] + "' ", cn);
                                            else
                                                cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                                            DataTable dtdata = new DataTable();
                                            cmddl.Fill(dtdata);
                                            for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                            {
                                                Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                            }
                                            Country = Country.OrderBy(x => x.Text).ToList();
                                            Country.Insert(0, (new SelectListItem { Value = "0", Text = "--Select--" }));
                                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: itemdata[q].COLUMN09);
                                        }
                                        string dddata = "";
                                        for (int d = 0; d < Country.Count; d++)
                                        {
                                            if (Country[d].Value == itemdata[q].COLUMN09 && itemdata[q].COLUMN09 != "")
                                                dddata += "<option value=" + Country[d].Value + " selected>" + Country[d].Text + "</option>";
                                            else
                                                dddata += "<option value=" + Country[d].Value + ">" + Country[d].Text + "</option>";
                                        }
                                        gcol.Add(new WebGridColumn()
                                        {
                                            ColumnName = itemdata[q].COLUMN06,
                                            Header = itemdata[q].COLUMN06,
                                            Format = (item) => new HtmlString("<select  class =gridddl name='" + itemdata[q].COLUMN06 + "' itemid=" + id + "    id='" + cid + "'  >" + dddata + "</select>")

                                        });
                                }
                                else if (itemdata[q].COLUMN10 == "TextArea")
                                {
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = itemdata[q].COLUMN06,
                                        Header = itemdata[q].COLUMN06,
                                        Format = (item) => new HtmlString("<textarea  id=" + cid + " class='txtgridclass'      pattern='" + itemdata[q].COLUMN08 + "'   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    >" + itemdata[q].COLUMN09 + "</textarea> ")
                                    });
                                }
                                else if (itemdata[q].COLUMN10 == "Image")
                                {
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = itemdata[q].COLUMN06,
                                        Header = itemdata[q].COLUMN06,
                                        Format = (item) => new HtmlString("<input type='file' id=" + cid + "   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    value=" + itemdata[q].COLUMN09 + " />")
                                    });
                                }
                                else if (itemdata[q].COLUMN10 == "CheckBox")
                                {
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = itemdata[q].COLUMN06,
                                        Header = itemdata[q].COLUMN06,
                                        Format = (item) => new HtmlString("<input  id=" + cid + " class='chkclass' itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "  type='checkbox'    value=" + itemdata[q].COLUMN09 + "  />")
                                    });
                                }
                                else if (itemdata[q].COLUMN10 == "RadioButton")
                                {
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = itemdata[q].COLUMN06,
                                        Header = itemdata[q].COLUMN06,
                                        Format = (item) => new HtmlString("<input class='rdoclass' itemid=" + id + "  id=" + cid + " name=" + itemdata[q].COLUMN06 + " type='radio'    value=" + itemdata[q].COLUMN09 + " />")
                                    });
                                }
                                else if (itemdata[q].COLUMN10 == "DatePicker")
                                {
                                    gcol.Add(new WebGridColumn()
                                    {
                                        ColumnName = itemdata[q].COLUMN06,
                                        Header = itemdata[q].COLUMN06,
                                        Format = (item) => new HtmlString("<input   name=" + itemdata[q].COLUMN06 + "   id=" + cid + "  type='text' readonly='true' class=date    value=" + itemdata[q].COLUMN09 + " />")
                                    });
                                }
                            }
                            else
                            {
                                if (itemdata[q].COLUMN10 == "TextBox")
                                {
                                    string hiddenlineid = "", ahiddenlineid = "a" + cid + "";
                                    if (cid == "PUTABLE004COLUMN08") hiddenlineid = "<input type='input'  id='" + ahiddenlineid + "'  value=" + itemdata[q].COLUMN09 + " style='display:none'  />";
                                    if (id == "INT" || id == "int" || id == "BIT" || id == "bit" || id == "DECIMAL" || id == "decimal")
                                    {
                                        Inlinecolumns[q] = "<td  ><input type='text' id='" + cid + "' class='txtintgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' onchange='itemChange(this);' name='" + itemdata[q].COLUMN06 + "'    value='" + itemdata[q].COLUMN09 + "'   />"+hiddenlineid+"</td>";
                                    }
                                    else
                                    {
                                        Inlinecolumns[q] = "<td  ><input type='text' id='" + cid + "' class='txtgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' onchange='itemChange(this);' name='" + itemdata[q].COLUMN06 + "'    value='" + itemdata[q].COLUMN09 + "'   />" + hiddenlineid + "</td>";
                                    }
                                }
                                else if (itemdata[q].COLUMN10 == "TextArea")
                                {
                                    Inlinecolumns[q] = "<td><textarea  id=" + cid + " class='txtgridclass'      pattern='" + itemdata[q].COLUMN08 + "'   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    >" + itemdata[q].COLUMN09 + "</textarea> </td>";

                                }
                                else if (itemdata[q].COLUMN10 == "Image")
                                {
                                    Inlinecolumns[q] = "<td><input type='file' id=" + cid + "   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    value=" + itemdata[q].COLUMN09 + "></td>";

                                }
                                else if (itemdata[q].COLUMN10 == "CheckBox")
                                {
                                    Inlinecolumns[q] = "<td><input  id=" + cid + " class='chkclass' itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "  type='checkbox'    value=" + itemdata[q].COLUMN09 + "  ></td>";

                                }
                                else if (itemdata[q].COLUMN10 == "DropDownList")
                                {
                                        List<SelectListItem> Country = new List<SelectListItem>();
                                        var firstname = itemdata[q].COLUMN06; ViewData[firstname] = "";
                                        var acOW = Convert.ToInt32(Session["AcOwner"]);
                                        if (itemdata[q].COLUMN14 == "Control Value")
                                        {
                                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
                                            var dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata && (a.COLUMNA03 == acOW || a.COLUMNA03 == null)).ToList();
                                            for (int dd = 0; dd < dropdata.Count; dd++)
                                            {
                                                Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
                                            }
                                            Country = Country.OrderBy(x => x.Text).ToList();
                                            Country.Insert(0, (new SelectListItem { Value = "0", Text = "--Select--" }));
                                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dvalue);
                                        }
                                        else if (itemdata[q].COLUMN14 == "Master Value")
                                        {
                                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
                                            var tblddl = dc.CONTABLE004.Where(r => r.COLUMN02 == ddata).OrderBy(r => r.COLUMN02);
                                            var tblName = tblddl.Select(r => r.COLUMN04).First();
                                            SqlDataAdapter cmddl = new SqlDataAdapter();
                                            if (tblName == "MATABLE007")
                                                cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'    AND COLUMN02='" + dt.Rows[i]["COLUMN03"] + "' ", cn);
                                            else
                                                cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                                            DataTable dtdata = new DataTable();
                                            cmddl.Fill(dtdata);
                                            for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                            {
                                                Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                            }
                                            Country = Country.OrderBy(x => x.Text).ToList();
                                            Country.Insert(0, (new SelectListItem { Value = "0", Text = "--Select--" }));
                                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dvalue).ToString();
                                        }
                                        string dddata = "";
                                        for (int d = 0; d < Country.Count; d++)
                                        {
                                            if (Country[d].Value == itemdata[q].COLUMN09 && itemdata[q].COLUMN09 != "")
                                                dddata += "<option value=" + Country[d].Value + " selected>" + Country[d].Text + "</option>";
                                            else
                                                dddata += "<option value=" + Country[d].Value + ">" + Country[d].Text + "</option>";
                                        }
                                        Inlinecolumns[q] = "<td  >" + "<select   class =gridddl  name='" + itemdata[q].COLUMN06 + "' itemid=" + id + "    id='" + cid + "'  >" + dddata + "</select>" + "</td>";
                                 
                                }
                                else if (itemdata[q].COLUMN10 == "RadioButton")
                                {
                                    Inlinecolumns[q] = "<td><input class='rdoclass' itemid=" + id + "  id=" + cid + " name=" + itemdata[q].COLUMN06 + " type='radio'    value=" + itemdata[q].COLUMN09 + " ></td>";
                                }
                                else if (itemdata[q].COLUMN10 == "DatePicker")
                                {
                                    Inlinecolumns[q] = "<td   ><input   name=" + itemdata[q].COLUMN06 + "  id=" + cid + "  type='text' readonly='true' class=date    value=" + itemdata[q].COLUMN09 + " ></td>";
                                }
                                if (q == 0)
                                {
                                    var LineID = dt.Rows[i]["LineID"].ToString(); if (LineID == "") LineID = "0";
                                    htmlstring += "<tr  ><td  ><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'    value='' checked='true'  /><input type='input'  id='LineID'  value=" + LineID + " style='display:none'  /></td><td><input id='UPCSearch' class='gridWH' pattern='' itemid='' name='' value='' type='text'></td> " + Inlinecolumns[q];
                                }
                                else if (q == itemdata.Count - 1)
                                {
                                    htmlstring += Inlinecolumns[q] + "</tr>";
                                }
                                else
                                {
                                    htmlstring += Inlinecolumns[q];
                                }
                            }
                            coo = coo + 1;
                            if (dt.Columns.Contains(itemdata[q].COLUMN05))
                            {
                                itemdata[q].COLUMN09 = dt.Rows[0][itemdata[q].COLUMN05].ToString();
                            }
                            break;
                        }
                    }
                }
                var grid1 = new WebGrid(null, canPage: false, canSort: false);
                grid1 = new WebGrid(result, canPage: false, canSort: false);
                var htmlstring1 = grid1.GetHtml(tableStyle: "webgrid-table",
                headerStyle: "webgrid-header",
                footerStyle: "webgrid-footer",
                alternatingRowStyle: "webgrid-alternating-row",
                rowStyle: "webgrid-row-style",
                htmlAttributes: new { id = "grdData" },
                columns: gcol);
                return Json(new
                {
                    grid = htmlstring1.ToHtmlString(),
                    remain = htmlstring,
                    Data = addressText,
                    Data1 = Jobber,
                    Data2 = JobOrder,
                    Data3 = Reference,
                    Data4 = Memo,
                    Data5 = Operating_Unit
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //jo Receipt Details
        public ActionResult getJOReceiptDetails(string JOID)
        {
            try
            {
                string JOI = JOID; Session["PForms"] = "Receipt(Out)";
                int PurchaseOrderID = 0;
                cn.Open();
                SqlCommand cmdpo = new SqlCommand();
                if (Convert.ToString( Session["PForms"] )== "Receipt(Out)")
                {
                    cmdpo = new SqlCommand("select column06,column02 from SAtable007 where (column04='" + JOID + "' or column02='" + JOID + "' or column06='" + JOID + "') AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                    //Session["PForms"] = null;
                }
                else
                    cmdpo = new SqlCommand("select column06,0 from SAtable007 where column04='" + JOID + "' or column06='" + JOID + "'", cn);

                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo); if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0] != "")
                    {
                        PurchaseOrderID = Convert.ToInt32(dtpo.Rows[0][0]);
                    }
                }
                AddressMaster obj = new AddressMaster();
                var addressText = obj.GetAdrs(PurchaseOrderID);
                var Reference = "";
                var Payment_Terms = "";
                var Payment_Mode = "";
                var Memo = "";
                var Operating_Unit = "";
                var Subsidary = "";
                var Classification = "";
                var Department = "";
                var Vendor = "";
                var OrederType = "";
                var Status = "";
                var JOIssue = "";
                var JO = "";
				//EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                var Project = "";
				   var cName = "";
                int frmid = (int)Session["id"];
                SqlCommand cmdj = new SqlCommand("usp_MAN_TP_JOIR_HEADER_DATA", cn);
                cmdj.CommandType = CommandType.StoredProcedure;
                cmdj.Parameters.Add(new SqlParameter("@SalesOrderID", PurchaseOrderID));
                cmdj.Parameters.Add(new SqlParameter("@JOI", JOI));
                SqlDataAdapter daj = new SqlDataAdapter(cmdj);
                DataTable dtj = new DataTable();
                daj.Fill(dtj);
                cn.Close();
                if (dtj.Rows.Count > 0)
                {
                    OrederType = dtj.Rows[0]["COLUMN20"].ToString();
                    Vendor = dtj.Rows[0]["COLUMN05"].ToString();
                    cName = dtj.Rows[0]["cName"].ToString();
                    Reference = dtj.Rows[0]["COLUMN09"].ToString();
                    Payment_Terms = dtj.Rows[0]["COLUMN12"].ToString();
                    Payment_Mode = dtj.Rows[0]["COLUMN30"].ToString();
                    Memo = dtj.Rows[0]["COLUMN13"].ToString();
                    Status = dtj.Rows[0]["COLUMN14"].ToString();
                    Operating_Unit = dtj.Rows[0]["COLUMN15"].ToString();
                    Subsidary = dtj.Rows[0]["COLUMN16"].ToString();
                    Classification = dtj.Rows[0]["COLUMN17"].ToString();
                    Department = dtj.Rows[0]["COLUMN18"].ToString();
                    JOIssue = dtj.Rows[0]["JOI"].ToString();
                    JO = dtj.Rows[0]["JO"].ToString();
                    Project = dtj.Rows[0]["COLUMN21"].ToString();
                }
                else
                {
                    OrederType = ""; Status = "";
                    Reference = "";
                    Payment_Terms = "";
                    Memo = "";
                    Operating_Unit = "";
                    Subsidary = "";
                    Classification = "";
                    Department = "";
                    Vendor = "";
                    Project = "";
					 cName = "";
                }

                SqlCommand cmd1 = new SqlCommand("usp_MAN_TP_JOIR_LINE_DATA", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@SalesOrderID", PurchaseOrderID));
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da1.Fill(dt);

                List<string> gcol = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    gcol.Add(dt.Rows[i]["COLUMN29"].ToString());
                }
              
                return Json(new
                {
                    pid = JO,
                    Data = addressText,
                    Data1 = JOIssue,
                    Data2 = Payment_Terms,
                    Data3 = Memo,
                    Data4 = Operating_Unit,
                    Data5 = Subsidary,
                    Data6 = Classification,
                    Data7 = Department,
                    Data14 = Vendor,
                    Data9 = Status,
                    Data8 = OrederType,
                    Data10 = Payment_Mode,
                    Data11 = Project,
                    cName = cName,
                    UPC = gcol
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

       //EMPHCS1806: Adding GRN DROPDOWN To jobOrderIN,JobOrderIssueIN,JobOrderReceiptIN by gnaneshwar on 27/8/2016
	    public ActionResult JORddlSelectGrn(string JOID)
        {
            try
            {
                string JOI = JOID; Session["PForms"] = "Job Order Receipt IN";
                int PurchaseOrderID = 0;
                cn.Open();
                SqlCommand cmdpo = new SqlCommand();

                cmdpo = new SqlCommand("select  column02 from PUTABLE001 where (COLUMN11='" + JOID + "') AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                   
               
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo); if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0] != "")
                    {
                        PurchaseOrderID = Convert.ToInt32(dtpo.Rows[0][0]);
                    }
                }
                
                return Json(new
                {
                    pid = PurchaseOrderID

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }



        public ActionResult AddNewTab()
        {
            try
            {
                var nM = Request["FormName"].ToString();
                var fd = dc.CONTABLE0010.Where(a => a.COLUMN04 == nM);
                var fid = fd.First().COLUMN02;
                var tblid = dc.CONTABLE006.Where(q => q.COLUMN03 == fid).Select(a => a.COLUMN04).FirstOrDefault();
                ViewBag.Source_Master = dc.CONTABLE004.OrderBy(q => q.COLUMN02).ToList();
                int ttid = Convert.ToInt32(tblid);
                var exCol = dc.CONTABLE005.Where(q => q.COLUMN03 == ttid && q.COLUMN04 == q.COLUMN05).ToList();
                var exColList = exCol.Select(w => w.COLUMN04).Distinct();
                ViewBag.ExColNames = exColList;
                return View("~/Views/Manufacture/JobOrderReceipt/AddNewTab.cshtml");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult AddNewTab(FormCollection fc)
        {
            try
            {
                var fd = dc.CONTABLE0010.Where(a => a.COLUMN04 == Session["FormName"]);
                var fid = fd.First().COLUMN02;
                var tblid = dc.CONTABLE006.Where(q => q.COLUMN03 == fid).Select(a => a.COLUMN04).FirstOrDefault();
                int tblID = Convert.ToInt32(tblid);
                int frmID = fid;
                var colName = fc["ColumnName"];
                var uData = dc.CONTABLE005.Where(q => q.COLUMN03 == tblID && q.COLUMN04 == colName).First();
                var uDataCount = dc.CONTABLE006.Where(q => q.COLUMN04 == tblID && q.COLUMN03 == frmID).ToList();
                if (uData != null)
                {
                    uData.COLUMN05 = fc["LabelName"];
                    uData.COLUMNA07 =
                    uData.COLUMNA10 = DateTime.Now.Date;
                }
                CONTABLE006 fm = new CONTABLE006();
                List<CONTABLE006> objList = dc.CONTABLE006.OrderBy(s => s.COLUMN02).ToList();
                if (objList.Count > 0)
                {
                    fm.COLUMN02 = objList[objList.Count - 1].COLUMN02 + 1;
                }
                else
                    fm.COLUMN02 = 50000;
                fm.COLUMN03 = frmID;
                fm.COLUMN04 = tblID;
                fm.COLUMN05 = fc["ColumnName"];
                fm.COLUMN06 = fc["LabelName"];
                fm.COLUMN07 = "Y";
                fm.COLUMN08 = "Y";
                fm.COLUMN09 = null;
                fm.COLUMN10 = fc["Control_Type"];
                fm.COLUMN11 = fc["Section_Type"];
                fm.COLUMN12 = fc["Section_Name"];
                fm.COLUMN13 = uDataCount.Count.ToString();
                fm.COLUMN14 = "GridView";
                fm.COLUMN15 = Convert.ToInt32(fc["Data_Source"]);
                fm.COLUMNA07 =
                fm.COLUMNA10 = DateTime.Now.Date;
                dc.CONTABLE006.Add(fm);
                dc.SaveChanges();
                eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                lg.CreateFile(Server.MapPath("~/"), Session["FormName"].ToString() + "_" + Session["UserName"].ToString() + "", Session["FormName"].ToString() + " new tab   " + fc["LabelName"] + " added at ");

                return RedirectToAction("AddNewTab");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }




        //Check Deletion Eligibility
        public ActionResult DeletionEligibility(int TransactionID, int FormId)
        {
            try
            {
                var TableData = dc.CONTABLE006.Where(a => a.COLUMN03 == FormId).ToList();
                var TableID = TableData.Select(a => a.COLUMN04).FirstOrDefault();
                var TableName = dc.CONTABLE004.Where(a => a.COLUMN02 == TableID).Select(a => a.COLUMN04).FirstOrDefault();
                Cmd = new SqlCommand("usp_PRO_TP_PR_ProjectDeleteCheck", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@TransactionID", TransactionID);
                Cmd.Parameters.AddWithValue("@TableName", TableName);
                Cmd.Parameters.AddWithValue("@FormId", FormId);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                DataTable dt = new DataTable();
                da.Fill(dt); string val = null;
                if (dt.Rows.Count > 0)
                    val = dt.Rows[0][0].ToString();
                return Json(new { Data1 = val }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "JobOrderRecipt", new { FormName = Session["FormName"] });
            }
        }



        public ActionResult Delete()
        {
            try
            {
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                List<CONTABLE006> all = new List<CONTABLE006>();
                var fname = Request["FormName"];
                var formdata = dc.CONTABLE0010.Where(a => a.COLUMN04 == fname).FirstOrDefault();
                ViewBag.tmpFIDEX = formdata.COLUMN02;
                var fdata = dc.CONTABLE006.Where(a => a.COLUMN03 == formdata.COLUMN02).ToList();
                var tbldata = fdata.Select(a => a.COLUMN04).Distinct().ToList();
                //for (int i = 0; i < tbldata.Count; i++)
                //{
                int tblid = tbldata[0].Value;
                var tname = db.CONTABLE004.Where(a => a.COLUMN02 == tblid).Select(a => a.COLUMN04).FirstOrDefault();
                int rid = Convert.ToInt32(Session["IDE"]);
                //var tName = Session["Table"].ToString();
                //SqlCommand cmd = new SqlCommand("UPDATE " + tName + " set COLUMNA13= 1 where COLUMN02=" + Convert.ToInt32(Session["IDE"]) + " ", cn);
                if (formdata.COLUMN02 == 1251)
                {
                    Cmd = new SqlCommand("usp_PUR_BL_PURCHASE", cn);
                }
                else if (formdata.COLUMN02 == 1286)
                {
                    Cmd = new SqlCommand("usp_JO_BL_JO_RECEIPT", cn);
                }
                else if (formdata.COLUMN02 == 1274)
                {
                    Cmd = new SqlCommand("usp_PUR_BL_PAYBILL", cn);
                }

                else if (formdata.COLUMN02 == 1414)
                {
                    Cmd = new SqlCommand("USP_FI_BL_JOURNALLEDGER", cn);
                }
                else if (formdata.COLUMN02 == 1273)
                {
                    Cmd = new SqlCommand("[usp_BIL_BL_BILL]", cn);
                }
                //else if (formdata.COLUMN02 == 1414)
                //{
                //    Cmd = new SqlCommand("USP_FI_BL_JOURNALLEDGER", cn);
                //}
                else if (formdata.COLUMN02 == 1272)
                {
                    Cmd = new SqlCommand("usp_PUR_BL_ITEM_RECEIPT", cn);
                }
                else if (formdata.COLUMN02 == 1261)
                {
                    Cmd = new SqlCommand("usp_MAS_BL_ItemMaster", cn);
                }
                else if (formdata.COLUMN02 == 1378)
                {
                    Cmd = new SqlCommand("usp_PRO_BL_Project", cn);
                }
                else if (formdata.COLUMN02 == 1380)
                {
                    Cmd = new SqlCommand("usp_PRO_BL_ProjectWorkOrder", cn);
                }
                else if (formdata.COLUMN02 == 1388)
                {
                    Cmd = new SqlCommand("[usp_MAS_BL_CONTACTS]", cn);
                }

                else if (formdata.COLUMN02 == 1404)
                {
                    Cmd = new SqlCommand("usp_MAS_BL_DAILYVISITREPORT", cn);
                }
                else
                {
                    var tName = Session["Table"].ToString();
                    SqlCommand cmd = new SqlCommand("UPDATE " + tName + " set COLUMNA13= 1 where COLUMN02=" + Convert.ToInt32(Session["IDE"]) + " ", cn);
                    cn.Open();
                    int r = cmd.ExecuteNonQuery();
                    cn.Close();
                    if (r > 0)
                    {
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == formdata.COLUMN02 && q.COLUMN05 == 4).FirstOrDefault();
                        var msg = string.Empty;
                        if (msgMaster != null)
                        {
                            msg = msgMaster.COLUMN03;
                        }
                        else
                            msg = "Record Successfully Deleted.......... ";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    else
                    {
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == formdata.COLUMN02 && q.COLUMN05 == 5).FirstOrDefault();
                        var msg = string.Empty;
                        if (msgMaster != null)
                        {
                            msg = msgMaster.COLUMN03;
                        }
                        else
                            msg = "Record Deletion Failed .........";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "fail";
                    }
                    return RedirectToAction("Info", new { FormName = Request["FormName"] });
                }
                cn.Open();
                Cmd.CommandType = CommandType.StoredProcedure;
                string delete = "Delete";
                Cmd.Parameters.AddWithValue("@COLUMN02", rid);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", delete);
                Cmd.Parameters.AddWithValue("@TabelName", tname);
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                int rr = Cmd.ExecuteNonQuery();
                cn.Close();
                if (rr > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == formdata.COLUMN02 && q.COLUMN05 == 4).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Row Deleted.......... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == formdata.COLUMN02 && q.COLUMN05 == 5).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Deletion Failed .........";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
            }
            //}
            catch (Exception ex)
            {
                int saveformid = ViewBag.tmpFIDEX;
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == saveformid && q.COLUMN05 == 5).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Deletion Failed........... ";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";

				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });

            }

            return RedirectToAction("Info", new { FormName = Request["FormName"] });
        }


      //EMPHCS1572 - working on multi units sizes in job order by GNANESHWAR ON 18/2/2016
	    public ActionResult GetJobItemDetails(string ItemID, string uom, string opunit, string JO, string lotno)
        {
            try
            {
                
                string AcOwner = Session["AcOwner"].ToString();

                
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_PUR_TP_JobItemDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                cmd.Parameters.Add(new SqlParameter("@uom", uom));
                cmd.Parameters.Add(new SqlParameter("@OPUnit", opunit));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
               cmd.Parameters.Add(new SqlParameter("@JO", JO));
           
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Close();
               
                var qty = "";
                
                if (dt.Rows.Count > 0)
                {
                    qty = dt.Rows[0]["FG"].ToString();
                   
                }
                else
                {
                    
                    qty = "";
                    
                }

               
                return Json(new
                {
                   
                    Data1 = qty


                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
		//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
        public ActionResult RemainingQTY(string FormID, string JOID, string JOISSue, string Item, string uom, string ProdID)
        {
            try
            {

                string AcOwner = Session["AcOwner"].ToString();
                if (ProdID != "" && ProdID != null) ProdID = " and a.COLUMN23='" + ProdID + "'";
                else ProdID = " and a.COLUMN10='" + JOISSue + "'";
                string str = "select count(*) count,sum(b.COLUMN08) as AcQty from PUTABLE004 b left outer join PUTABLE003 a on a.COLUMN01=b.COLUMN12 and a.COLUMN03=" + FormID + " " + ProdID + " and a.COLUMN06=" + JOID + " where  b.COLUMN03=" + Item + " and b.COLUMN17='" + uom + "' and isnull(b.COLUMNA13,0)=0 and b.COLUMNA03=" + Session["AcOwner"] + "";
                SqlDataAdapter da = new SqlDataAdapter(str,cn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                var qty = "";
                var count = "";
                if (dt.Rows.Count > 0)
                {
                    qty = dt.Rows[0]["AcQty"].ToString();
                    count = dt.Rows[0]["count"].ToString();
                }
                return Json(new
                {
                    Data1 = qty,
                    Data2=count

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult SetDateFormat()
        {
            try
            {
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter da = new SqlDataAdapter(str, cn);
                DataTable dt = new DataTable();
                da.Fill(dt); string DateFormat = null;
                if (dt.Rows.Count > 0)
                {
                    DateFormat = dt.Rows[0][0].ToString();
                    Session["FormatDate"] = dt.Rows[0][0].ToString();
                }
                if (dt.Rows[0][1].ToString() != "")
                    Session["ReportDate"] = dt.Rows[0][1].ToString();
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                }
                return Json(new { Data2 = DateFormat }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
    }
}
