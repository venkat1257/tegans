﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBizSuiteAppModel.Table;
using System.Web.Helpers;
using eBizSuiteUI.Models;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;

namespace MeBizSuiteAppUI.Controllers
{
    public class QueryGeneratorController : Controller
    {
        //
        // GET: /QueryGenerator/

        private eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        //
        // GET: /Table_Mapping/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult TreeView()
        {
            //var dataContext = new eBizSuiteEDM.eBizSuiteEntities();

            //var tables = from e in dataContext.Table_Mapping.OrderBy(a=>a.Table_Alias).ToList()
            //             select new
            //             {
            //                 id = e.TID,
            //                 Name =  e.Table_Alias
            //             };

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public class DynaNode
        {
            public string title { get; set; }
            public bool isFolder { get; set; }
            public bool isLazy { get; set; }
            public string key { get; set; }
            public string parentId { get; set; }
        }

        //private List<Table_Mapping> GetTreeView()
        //{
        //    //List<tbl_TreeView> objTreeList = objContext.tbl_TreeViews.OrderBy(s => s.ParentId).ToList();
        //    List<Table_Mapping> objTreeList = db.Table_Mapping.OrderBy(s => s.Table_Alias).ToList();
        //    //List<Table_Mapping> objList = new List<Table_Mapping>();
        //    //foreach (var item in objTreeList)
        //    //{
        //    //    DynaNode objTree = new DynaNode();
        //    //    objTree.key = item.TID.ToString();
        //    //    objTree.title = item.Table_Alias;
        //    //    objTree.isLazy = true;
        //    //    objTree.isFolder = false;
        //    //    objTree.parentId = item.TID.ToString();
        //    //    objList.Add(objTree);
        //    //}

        //    return objTreeList;

        //}

        public int id;

        public ActionResult GetAllNode(int? tid)
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            QueryGeneratorModel obj = new QueryGeneratorModel();
            obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN06 == null && q.COLUMN04 == 101).ToList();
            ViewBag.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0).ToList().Distinct();
            if (!tid.HasValue)
            {
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == 0).ToList();
            }
            else
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == tid).ToList();
            List<CONTABLE004> objTreeList = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
            ViewBag.Column = objTreeList;
            return View("GetAllNode", obj);
        }

        [HttpPost]
        public ActionResult Filter(string table_Alias)
        {
            QueryGeneratorModel obj = new QueryGeneratorModel();
            var tId = db.CONTABLE004.Where(c => c.COLUMN05 == table_Alias);
            var Tid = tId.FirstOrDefault().COLUMN02;
            obj.tbl = db.CONTABLE005.Where(a => a.COLUMN02 == Tid).OrderBy(q=>q.COLUMN04).ToList();
            //if (objColumn.Count > 0)
            //{
            //    ViewBag.Column = objColumn;
            //    //Session["id"] = Tid;
            //    //tid = (int?)Session["id"];
            //    //List<eBizSuiteAppModel.Table.Column_Mapping> objColumn = db.Column_Mapping.Where(a => a.TID == tid).ToList();
            //    List<Table_Mapping> objTreeList = db.Table_Mapping.OrderBy(s => s.Table_Alias).ToList();
            //    ViewBag.Column = objTreeList;
            //    return PartialView("GetAllNode", objColumn);
            //    //return RedirectToAction("GetAllNode");
            //}
            //else
            //{ ViewBag.Column = objColumn = null; }

            return PartialView("GetAllNode", obj);
        }

        private List<DynaNode> GetTreeView(int? mid)
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            List<CONTABLE004> objTreeList = db.CONTABLE004.Where(s => s.COLUMN03 == mid).ToList();
            List<DynaNode> objList = new List<DynaNode>();
            foreach (var item in objTreeList)
            {
                DynaNode objTree = new DynaNode();
                objTree.key = item.COLUMN02.ToString();
                objTree.title = item.COLUMN04 + " ( " + item.COLUMN05 + " ) ";
                objTree.isLazy = true;
                //var objChild = objTreeList.FindAll(s => s.ParentId == item.BRANCHId);
                //if (objChild.Count > 0)
                //{
                //    objTree.isFolder = true;
                //}
                //else
                objTree.isFolder = true;
                //objTree.parentId = "0";
                objList.Add(objTree);
            }

            return objList;
        }

        public JsonResult GetTables(int? id)
        {
            if (id != null)
            {
                ViewBag.selected = Session["FID"] = id;
                //eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                //var mid = db.Form_Master.Where(a => a.F_Id == id);
                //var mId = mid.FirstOrDefault().Module_Id;
                List<DynaNode> objList = GetTreeView(id);
                return Json(objList, JsonRequestBehavior.AllowGet);
            }
            else
                if (Session["FID"] != null)
                {
                    int i = Convert.ToInt32(Session["FID"]);
                    //eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                    //var mid = db.Form_Master.Where(a => a.F_Id == i);
                    //var mId = mid.FirstOrDefault().Module_Id;
                    List<DynaNode> objList = GetTreeView(i);
                    return Json(objList,JsonRequestBehavior.AllowGet);
                    ;
                }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // [HttpPost]
        public ActionResult CreateTree(int? lst)
        {
            //ViewBag.selected = fid;
            if (lst != null)
            {
                Session["TID"] = lst;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                //tid = (int?)Session["id"];
                QueryGeneratorModel obj = new QueryGeneratorModel();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN04 == 101).ToList();
                //obj.lst = db.Form_Master.Where(q => q.Module_Id == mid).ToList();
                obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
                ViewBag.lst = obj.lst;
                //var list =  int.Parse(lst);
                var col = db.CONTABLE004;
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == lst).OrderBy(q => q.COLUMN04).ToList();
                //obj.tbl = (from t in db.Column_Mapping join c in db.Table_Mapping on t.TID equals c.TID where list.Contains(t.TID) select t).ToList();
                ViewBag.Table = obj.tbl;
                return PartialView("GetAllNode", obj);
                //return Json(obj, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string st = "0";
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                //tid = (int?)Session["id"];
                QueryGeneratorModel obj = new QueryGeneratorModel();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN04 == 101).ToList();
                obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == 0).ToList();
                obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
                ViewBag.lst = obj.lst;
                var list = st.Split(',').Select(n => int.Parse(n)).ToList();
                obj.tbl = db.CONTABLE005.Where(a => list.Contains(a.COLUMN03)).ToList();
                //obj.tbl = db.Column_Mapping.Where(a => list.Contains(a.TID)).ToList();
                //obj.tbl = (from t in db.Column_Mapping where list.Contains(t.TID) select t).ToList();
                //obj.tbl = (from t in db.Column_Mapping join c in db.Table_Mapping on t.TID equals c.TID where list.Contains(t.TID) select new {c.Table_Alias,t }).ToList();
                ViewBag.Table = obj.tbl;
                return PartialView("GetAllNode", obj);
                //return Json(obj, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CreateTable(string str)
        {
            try
            {
                int? res = null;
                using (SqlConnection con = new SqlConnection(sqlcon))
                {
                    SqlCommand cmd = new SqlCommand(str, con);
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }

                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GenerateTable(int Module_ID, string Module_Name)
        {
            string result = null;
            List<CONTABLE004> objTreeList = db.CONTABLE004.OrderBy(q => q.COLUMN04).Where(s => s.COLUMN03 == Module_ID).ToList();
            if (objTreeList.Count > 0)
            {
                string str = objTreeList[objTreeList.Count - 1].COLUMN04.ToString();
                string sub = str.Substring(0, str.Count() - 1);
                int nstr = Convert.ToInt32(str.Last().ToString());
                result = sub + (nstr + 1).ToString();
            }
            else
            {
                string ss = Module_Name.Substring(0, 2);
                result = ss.ToUpper() + "TABLE001";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult Index()
        //{
        //    var table_mapping = db.CONTABLE004;
        //    return View(table_mapping.ToList());
        //}

        //
        // GET: /Table_Mapping/Details/5

        public ActionResult Details(int id)
        {
            CONTABLE004 table_mapping = db.CONTABLE004.Find(id);
            if (table_mapping == null)
            {
                return HttpNotFound();
            }
            return View(table_mapping);
        }

        //
        // GET: /Table_Mapping/Create
        //static Column_Mapping mod = new Column_Mapping();

        public ActionResult Create()
        {
            QueryGeneratorModel obj = new QueryGeneratorModel();
            obj.lstMD = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0).ToList();
            //eBizSuiteEDMModel obj1 = new eBizSuiteEDMModel();
            IList<eBizSuiteUI.Models.Column_Mapping> collist = new List<eBizSuiteUI.Models.Column_Mapping>();
            IList<eBizSuiteUI.Models.Column_Mapping1> collistBD = new List<eBizSuiteUI.Models.Column_Mapping1>();
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN01", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN02", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN03", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN04", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN05", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN06", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN07", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN08", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN09", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            collist.Add(new eBizSuiteUI.Models.Column_Mapping { Column_Name = "COLUMN10", Data_Type = "", Alias_Column_Name = "", IsNullable = "", Size = "" });
            obj.list = collist;
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA01", Data_Type = "INT", Alias_Column_Name = "Company_ID", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA02", Data_Type = "INT", Alias_Column_Name = "Operating_Unit_ID", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA03", Data_Type = "INT", Alias_Column_Name = "Account_Owner_ID", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA04", Data_Type = "INT", Alias_Column_Name = "Classification_ID", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA05", Data_Type = "INT", Alias_Column_Name = "Department_ID", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA06", Data_Type = "DATETIME", Alias_Column_Name = "LCreated_Date", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA07", Data_Type = "DATETIME", Alias_Column_Name = "Lmodified_Date", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA08", Data_Type = "INT", Alias_Column_Name = "Created_By", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA09", Data_Type = "DATETIME", Alias_Column_Name = "GCreated_Date", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA10", Data_Type = "DATETIME", Alias_Column_Name = "Gmodified_Date", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA11", Data_Type = "INT", Alias_Column_Name = "Modified_By", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA12", Data_Type = "BIT", Alias_Column_Name = "IsActive", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNA13", Data_Type = "BIT", Alias_Column_Name = "IsDeleted", IsNullable = "", Size = "0" });

            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB01", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMNB01", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB02", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMNB02", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB03", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMNB03", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB04", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMNB04", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB05", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMNB05", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB06", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMNB06", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB07", Data_Type = "DATETIME", Alias_Column_Name = "COLUMNB07", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB08", Data_Type = "DATETIME", Alias_Column_Name = "COLUMNB08", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB09", Data_Type = "DECIMAL", Alias_Column_Name = "COLUMNB09", IsNullable = "", Size = "(18,0)" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB10", Data_Type = "DECIMAL", Alias_Column_Name = "COLUMNB10", IsNullable = "", Size = "(18,0)" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB11", Data_Type = "INT", Alias_Column_Name = "COLUMNB11", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMNB12", Data_Type = "INT", Alias_Column_Name = "COLUMNB12", IsNullable = "", Size = "0" });

            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND01", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMND01", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND02", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMND02", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND03", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMND03", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND04", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMND04", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND05", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMND05", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND06", Data_Type = "NVARCHAR", Alias_Column_Name = "COLUMND06", IsNullable = "", Size = "250" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND07", Data_Type = "DATETIME", Alias_Column_Name = "COLUMND07", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND08", Data_Type = "DATETIME", Alias_Column_Name = "COLUMND08", IsNullable = "", Size = "0" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND09", Data_Type = "DECIMAL", Alias_Column_Name = "COLUMND09", IsNullable = "", Size = "(18,0)" });
            collistBD.Add(new eBizSuiteUI.Models.Column_Mapping1 { Column_Name = "COLUMND10", Data_Type = "INT", Alias_Column_Name = "COLUMND10", IsNullable = "", Size = "0" });
            obj.lstBD = collistBD;

            return View(obj);
        }

        //
        // POST: /Table_Mapping/Create

        //[HttpPost]
        //public ActionResult Create(string lst)
        //{
        //    XmlDocument xmldoc = new XmlDocument();
        //    xmldoc.LoadXml(lst.ToString());
        //    DataSet ds = new DataSet();
        //    ds.ReadXml(new XmlNodeReader(xmldoc));
        //    DataRow dr = ds.Tables[0].NewRow();
        //    ds.Tables[0].Rows.Add(dr);
        //    eBizSuiteEDMModel obj = new eBizSuiteEDMModel();
        //    IList<eBizSuiteUI.Models.Column_Mapping> collist = new List<eBizSuiteUI.Models.Column_Mapping>();
        //    obj.lst = collist;
        //    return View(obj);
        //}

        public JsonResult SaveData(string lst, string Module_ID, string Actual_Table, string Table_Alias)
        {
            var x = db.CONTABLE004.Where(q => q.COLUMN04 == Actual_Table).ToList();
            if (x.Count > 0)
            {
                return this.Json("Table Name Existed...", JsonRequestBehavior.AllowGet);
            }
            else
            {
                CONTABLE004 tb = new CONTABLE004();
                eBizSuiteAppModel.Table.CONTABLE005 cb;
                List<CONTABLE004> objList = db.CONTABLE004.OrderBy(s => s.COLUMN01).ToList();
                if (objList.Count > 0)
                {
                    tb.COLUMN02 = objList[objList.Count - 1].COLUMN02 + 200;
                }
                else
                    tb.COLUMN02 = 10000;
                tb.COLUMN04 = Actual_Table;
                tb.COLUMN05 = Table_Alias;
                if (Module_ID != "")
                    tb.COLUMN03 = Convert.ToInt32(Module_ID);
                else
                    tb.COLUMN03 = null;
                tb.COLUMNA01 = null;
                tb.COLUMNA02 = null;
                if (Session["LogedEmployeeID"] != null)
                {
                    tb.COLUMNA03 = tb.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                }
                else
                    tb.COLUMNA03 = tb.COLUMNA08 = null;
                tb.COLUMNA04 = null;
                tb.COLUMNA05 = null;
                tb.COLUMNA06 = DateTime.Now;
                tb.COLUMNA07 = null;
                tb.COLUMNA09 = DateTime.Now;
                tb.COLUMNA10 = null;
                tb.COLUMNA11 = null;
                tb.COLUMNA12 = true;
                tb.COLUMNA13 = false;
                db.CONTABLE004.Add(tb);
                ViewBag.TID = new SelectList(db.CONTABLE005, "Col_Id", "Actual_Column", tb.COLUMN02);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(lst.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(xmldoc));
                if (ds.Tables.Count > 0)
                {
                    for (int cnt = 0; cnt < ds.Tables[0].Rows.Count; cnt++)
                    {
                        cb = new eBizSuiteAppModel.Table.CONTABLE005();
                        string s = ds.Tables[0].Rows[cnt]["IsNullable"].ToString();
                        cb.COLUMN02 = tb.COLUMN02 + cnt;
                        cb.COLUMN03 = tb.COLUMN02;
                        cb.COLUMN04 = ds.Tables[0].Rows[cnt]["Column_Name"].ToString();
                        cb.COLUMN05 = ds.Tables[0].Rows[cnt]["Alias_Column_Name"].ToString();
                        cb.COLUMN06 = ds.Tables[0].Rows[cnt]["Data_Type"].ToString();
                        if (s == "1")
                        {
                            cb.COLUMN07 = true;
                        }
                        else
                            cb.COLUMN07 = false;
                        cb.COLUMN08 = ds.Tables[0].Rows[cnt]["Size"].ToString();
                        //cb.Table_Mapping = null;
                        cb.COLUMNA01 = null;
                        cb.COLUMNA02 = null;
                        if (Session["LogedEmployeeID"] != null)
                        {
                            cb.COLUMNA03 = cb.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                        }
                        else
                            cb.COLUMNA03 = cb.COLUMNA08 = null;
                        cb.COLUMNA04 = null;
                        cb.COLUMNA05 = null;
                        cb.COLUMNA06 = DateTime.Now;
                        cb.COLUMNA07 = null;
                        cb.COLUMNA09 = DateTime.Now;
                        cb.COLUMNA10 = null;
                        cb.COLUMNA11 = null;
                        cb.COLUMNA12 = true;
                        cb.COLUMNA13 = false;
                        db.CONTABLE005.Add(cb);
                        db.SaveChanges();
                    }

                    return this.Json("Table and Columns adding succeeded...", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json("Table and Columns adding failed...", JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult AddRow(string lst)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(xmldoc));
            DataRow dr = ds.Tables[0].NewRow();
            ds.Tables[0].Rows.Add(dr);
            eBizSuiteEDMModel obj = new eBizSuiteEDMModel();
            IList<eBizSuiteUI.Models.Column_Mapping> collist = new List<eBizSuiteUI.Models.Column_Mapping>();
            obj.lst = collist;
            return View(obj);
        }
        //
        // GET: /Table_Mapping/Edit/5

        public ActionResult Edit(int id)
        {
            CONTABLE004 table_mapping = db.CONTABLE004.Find(id);
            if (table_mapping == null)
            {
                return HttpNotFound();
            }
            ViewBag.TID = new SelectList(db.Column_Mapping, "Col_Id", "Actual_Column", table_mapping.COLUMN02);
            return View(table_mapping);
        }

        //
        // POST: /Table_Mapping/Edit/5

        [HttpPost]
        public ActionResult Edit(CONTABLE004 table_mapping)
        {
            if (ModelState.IsValid)
            {
                // db.Entry(table_mapping).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TID = new SelectList(db.CONTABLE004, "Col_Id", "Actual_Column", table_mapping.COLUMN02);
            return View(table_mapping);
        }

        //
        // GET: /Table_Mapping/Delete/5

        public ActionResult Delete(int id)
        {
            CONTABLE004 table_mapping = db.CONTABLE004.Find(id);
            if (table_mapping == null)
            {
                return HttpNotFound();
            }
            return View(table_mapping);
        }

        //
        // POST: /Table_Mapping/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CONTABLE004 table_mapping = db.CONTABLE004.Find(id);
            db.CONTABLE004.Remove(table_mapping);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}
