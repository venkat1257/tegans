﻿using eBizSuite.Models;
using eBizSuite.Models.User;
using eBizSuite.Models.UserManage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using WebMatrix.Data;
using eBizSuiteAppModel.Table;
using System.Configuration;
namespace eBizSuite.Controllers
{
    public class UsersController : Controller
    {
        eBizSuiteTableEntities entities = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        ModelServices1 obj = new ModelServices1();

        //
        // GET: /Users/

        public ActionResult Register()
        {
            return View("Register");
        }

        [HttpPost]
        public ActionResult Register(ApplicationOwnerReg user)
        {

            try
            {
                if (ModelState.IsValid)
                {


                    ApplOwnerRegManage userManager = new ApplOwnerRegManage();
                    if (!userManager.IsUserLoginIDExist(user.ApplicationOwnerId))
                    {

                        userManager.Add(user);

                        FormsAuthentication.SetAuthCookie(user.AppOwnerUserName, false);
                        return RedirectToAction("AppOwnerInfo1", "Users");
                        //return RedirectToAction("Purchase", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "LogID already taken");
                    }
                }
            }
            catch
            {
                return View(user);
            }

            return View(user);
        }

        [HttpGet]
        public ActionResult AppOwnerInfo1()
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_AppOwnerMaster";

            ViewBag.grid = db.Query(query);
            return View();
        }

        [HttpGet]
        public ActionResult EmpReg()
        {
            return View("EmpReg");
        }

        [HttpPost]
        public ActionResult EmpReg(EmployeMaster user)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    EmployeMasterManage userManager = new EmployeMasterManage();
                    if (!userManager.IsUserLoginIDExist(user.EmployeeID))
                    {

                        userManager.Add(user);

                        FormsAuthentication.SetAuthCookie(user.EmployeeName, false);
                        return RedirectToAction("EmployeeMaster", "Users");
                        //return RedirectToAction("Purchase", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "LogID already taken");
                    }
                }
            }
            catch
            {
                return View(user);
            }

            return View(user);
        }

        [HttpGet]
        public ActionResult EmployeeMaster(string id)
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_EmployeeMaster";
            if (!string.IsNullOrEmpty(id))
            {
                query = query + " where EmployeeID=" + id;
            }
            ViewBag.grid = db.Query(query);
            return View();
        }



        [HttpGet]
        public ActionResult EditEmploye(int id)
        {
            var data = obj.GetEmployeeDetail(id);

            EmployeMaster emp = new EmployeMaster();
            emp.EmployeeID = data.EmployeeID;
            emp.EmployeeName = data.EmployeeName;
            emp.CompanyName = data.CompanyName;
            emp.TypeOfBusines = data.TypeOfBusines;
            emp.IsActive = Convert.ToBoolean(data.IsActive);
            emp.Email = data.Email;

            return View("EditEmploye", emp);


        }


        [HttpPost]
        public ActionResult UpdateEmployee(EmployeMaster register)
        {
            tbl_EmployeeMaster sysRegister = new tbl_EmployeeMaster();
            //sysRegister.UserID = register.UserID;
            sysRegister.EmployeeID = register.EmployeeID;
            sysRegister.EmployeeName = register.EmployeeName;
            sysRegister.CompanyName = register.CompanyName;
            sysRegister.TypeOfBusines = register.TypeOfBusines;
            sysRegister.IsActive = Convert.ToString(register.IsActive);
            sysRegister.Email = register.Email;
            if (register.Password == true)
            {
                sysRegister.Password = "eBizSuite";
            }
            else { sysRegister.Password = "NULL"; }

            bool IsSuccess = obj.UpdateEmployee(sysRegister);
            if (IsSuccess)
            {
                TempData["OperStatus"] = "Employee updated succeessfully";
                ModelState.Clear();
                return RedirectToAction("EmployeeMaster", "Users");
            }

            return View();

        }

        public ActionResult Delete(int id)
        {
            bool check = obj.DeleteEmployee(id);

            var data = obj.GetEmployee();
            ViewBag.grid = data;
            return View("EmployeeMaster");

        }

        public ActionResult DeleteSelected(string[] assignChkBx)
        {
            //Delete Selected 
            int[] id = null;
            if (assignChkBx != null)
            {
                id = new int[assignChkBx.Length];
                int j = 0;
                foreach (string i in assignChkBx)
                {
                    int.TryParse(i, out id[j++]);
                }
            }

            if (id != null && id.Length > 0)
            {
                List<tbl_EmployeeMaster> allSelected = new List<tbl_EmployeeMaster>();
                using (eBizSuiteTableEntities dc = new eBizSuiteTableEntities())
                {
                    allSelected = dc.tbl_EmployeeMaster.Where(a => id.Contains(a.EmployeeID)).ToList();
                    foreach (var i in allSelected)
                    {
                        dc.tbl_EmployeeMaster.Remove(i);
                    }
                    dc.SaveChanges();
                }
            }

            return RedirectToAction("EmployeeMaster");
        }

        [HttpPost]
        public ActionResult DeleteSelected1(string[] assignChkBx)
        {
            //Delete Selected 
            int[] id = null;
            if (assignChkBx != null)
            {
                id = new int[assignChkBx.Length];
                int j = 0;
                foreach (string i in assignChkBx)
                {
                    int.TryParse(i, out id[j++]);
                }
            }

            if (id != null && id.Length > 0)
            {
                List<tbl_EmployeeMaster> allSelected = new List<tbl_EmployeeMaster>();
                using (eBizSuiteTableEntities dc = new eBizSuiteTableEntities())
                {
                    allSelected = dc.tbl_EmployeeMaster.Where(a => id.Contains(a.EmployeeID)).ToList();
                    foreach (var i in allSelected)
                    {
                        dc.tbl_EmployeeMaster.Remove(i);
                    }
                    dc.SaveChanges();
                }
            }

            return RedirectToAction("EmployeeMaster");
        }





        [HttpPost]
        public ActionResult UpdateInline(string list)
        {
            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                var c = dc.tbl_EmployeeMaster.Where(a => a.EmployeeID == eid).FirstOrDefault();

                c.EmployeeName = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                c.CompanyName = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                c.Email = ds.Tables[0].Rows[i].ItemArray[3].ToString();

                dc.SaveChanges();
            }

               return View("EmployeeMaster");

            

        }


        //AddVanceSEARCH

         public ActionResult Search()
        {
            return View("Search");
        }

        [HttpPost]
        [ActionName("Search")]
        public ActionResult Searchpost()
        {
            var db = Database.Open("sqlcon");
            //var quer = "select distinct EmployeeID from tblEmployeMaster ORDER BY EmployeeID";
            //var EmpID = db.Query(quer);
            //var query = "SELECT DISTINCT CompanyName FROM tblEmployeMaster ORDER BY CompanyName";
            //var Companys = db.Query(query);

            var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID like @0 or EmployeeName LIKE @1 or CompanyName LIKE @2  ";

            // query = "SELECT * FROM tblEmployeMaster  WHERE EmployeeID like '" + Request["EmployeeID"] + "%' or EmployeeName LIKE '" + Request["EmployeeName"] + "%' or CompanyName LIKE '" + Request["CompanyName"] + "%'  ";
            var EmployeeID = "";
            var EmployeeName = "";
            var CompanyName = "";
            if (Request["EmployeeID"] != string.Empty)
            {
                EmployeeID = Request["EmployeeID"] + "%";
            }
            else
                EmployeeID = Request["EmployeeID"];
            if (Request["EmployeeName"] != string.Empty)
            {
                EmployeeName = Request["EmployeeName"] + "%";
            }
            else
                EmployeeName = Request["EmployeeName"];
            if (Request["CompanyName"] != string.Empty)
            {
                CompanyName = Request["CompanyName"] + "%";
            }
            else
                CompanyName = Request["CompanyName"];

            ViewBag.grid = db.Query(query, EmployeeID, EmployeeName, CompanyName);

            ViewBag.columns = new[] { "EmployeeID", "EmployeeName", "CompanyName", "TypeOfBusines", "Email" };


            return View("EmployeeMaster");
        }

        


        //Search

        [HttpPost]
        [ActionName("AdvSearch1")]
        public ActionResult AdvSearchPost()
        {
            var db = Database.Open("sqlcon");

            var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID like @0 or EmployeeName LIKE @1  ";

            
            var EmployeeID = "";
            var EmployeeName = "";
            
            if (Request["EmployeeID"] != string.Empty)
            {
                EmployeeID = Request["EmployeeID"] + "%";
            }
            else
                EmployeeID = Request["EmployeeID"];
            if (Request["EmployeeID"] != string.Empty)
            {
                EmployeeName = Request["EmployeeID"] + "%";
            }
            else
                EmployeeName = Request["EmployeeID"];


            ViewBag.grid = db.Query(query, EmployeeID, EmployeeName);

            ViewBag.columns = new[] { "EmployeeID", "EmployeeName"};


            return View("AdvSearchPost");
        }

        public ActionResult AdvSearch1()
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_EmployeeMaster";

            ViewBag.grid = db.Query(query);

            return View();
        }


         [HttpGet]
        public ActionResult SearchEmp()
        {
            //var connectionString = "data source=programmer5-pc;Initial Catalog=EMPHORADBDB;integrated security=true";

            //var providerName = "System.Data.SqlClient";

            //var db = Database.OpenConnectionString(connectionString, providerName);
            //var query = "select * from tblEmployeMaster";

            //ViewBag.grid = db.Query(query);
            ViewBag.grid = Session["data"];
            ViewBag.columns = Session["col"];

            return View();
        }



        //[HttpPost]
        //[ActionName("SearchEmp")]
        //public ActionResult SearchEmployee(int assignChkBx)
        //{
        //    var connectionString = "data source=programmer5-pc;Initial Catalog=EMPHORADBDB;integrated security=true";

        //    var providerName = "System.Data.SqlClient";

        //    var db = Database.OpenConnectionString(connectionString, providerName);
        //    var query = "SELECT * FROM tblEmployeMaster  WHERE EmployeeID =" + assignChkBx + "";

        //    ViewBag.grid = db.Query(query);

        //    ViewBag.columns = new[] { "EmployeeID", "EmployeeName", "CompanyName", "TypeOfBusines", "Email" };

        //    //return RedirectToAction("EmployeeMaster");
        //    return RedirectToAction("EmployeeMaster", new { id = assignChkBx }); // here p
        //    //return View("EmployeeMaster");


            
        //}


         [HttpPost]
         [ActionName("SearchEmployee")]
         public ActionResult SearchEmployee(int assignChkBx)
         {
             var db = Database.Open("sqlcon");
             var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID =" + assignChkBx + "";

             ViewBag.grid = db.Query(query);

             ViewBag.columns = new[] { "EmployeeID", "EmployeeName", "CompanyName", "TypeOfBusines", "Email" };


             return View("EmployeeMaster");



         }



        //[HttpPost]
        //[ActionName("FilterData")]
        public ActionResult FilterData(int id)
        {
            //var connectionString = "data source=programmer5-pc;Initial Catalog=EMPHORADBDB;integrated security=true";

            //var providerName = "System.Data.SqlClient";

            //var db = Database.OpenConnectionString(connectionString, providerName);
            //var query = "SELECT * FROM tblEmployeMaster  WHERE EmployeeID =" + id + "";
            
            var grid = entities.tbl_EmployeeMaster.Where(a => a.EmployeeID == id);
            ViewBag.grid = grid;

            


            return PartialView("EmployeeMaster");



        }




        [HttpGet]
        public ActionResult SearchEmployee()
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_EmployeeMaster";
            //ViewBag.grid = Session["griddata"];
            //if (ViewBag.grid == null)
            //{
                ViewBag.grid = db.Query(query);

            //}
            return PartialView();
        }



        [HttpGet]
        [ActionName("SearchEmployePost")]
        public ActionResult SearchEmployePost(int id)
        {
            var db = Database.Open("sqlcon");

            var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID like @0 or EmployeeName LIKE @1  ";

            
            int EmployeeID = id;
            var EmployeeName = "";

            //if (EmployeeID != string.Empty)
            //{
            //    EmployeeID = Request["EmployeeID"] + "%";
            //}
            //else
            //    EmployeeID = Request["EmployeeID"];
            //if (EmployeeName != string.Empty)
            //{
            //    EmployeeName = Request["EmployeeID"] + "%";
            //}
            //else
            //    EmployeeName = Request["EmployeeID"];

            var grid = entities.tbl_EmployeeMaster.Where(a=>a.EmployeeID==EmployeeID);
            //ViewBag.grid = db.Execute(query, EmployeeID, EmployeeName);
            ViewBag.grid = grid;
            Session["griddata"] = grid;
            Session["data"] = ViewBag.grid;
            //ViewBag.columns = new[] { "EmployeeID", "EmployeeName"};
            //Session["col"] = ViewBag.columns;

            return PartialView("SearchEmployee");
        }

        [HttpGet]
        public ActionResult SEmp()
        {
            ViewBag.grid = Session["data"];
            ViewBag.columns = Session["col"];
            return View();
        }



        
    }
}
