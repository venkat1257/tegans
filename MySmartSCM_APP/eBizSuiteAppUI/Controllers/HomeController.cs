﻿using eBizSuiteAppModel.Table;
using eBizSuiteUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace MeBizSuiteAppUI.Controllers
{
    public class HomeController : Controller
    {
        public JsonResult SaveData(string lst)
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            CONTABLE006 cb;
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(xmldoc));
            if (ds.Tables.Count > 0)
            {
                for (int cnt = 0; cnt < ds.Tables[0].Rows.Count; cnt++)
                {
                    int i = Convert.ToInt32(Session["FID"]);
                    // var count = db.Field_Master.Where(a => a.Form_Id ==i ).Count();
                    cb = new CONTABLE006();
                    string astr = ds.Tables[0].Rows[cnt]["Action"].ToString();
                    string mstr = ds.Tables[0].Rows[cnt]["Mandatory"].ToString();
                    List<CONTABLE006> objList = db.CONTABLE006.OrderBy(s => s.COLUMN02).ToList();
                    if (objList.Count > 0)
                    {
                        cb.COLUMN02 = objList[objList.Count - 1].COLUMN02 + i + 1;
                    }
                    else
                        cb.COLUMN02 = 50000;
                    cb.COLUMN05 = ds.Tables[0].Rows[cnt]["Field_Name"].ToString();
                    cb.COLUMN06 = ds.Tables[0].Rows[cnt]["Label_Name"].ToString();
                    cb.COLUMN09 = ds.Tables[0].Rows[cnt]["Default_Value"].ToString();
                    cb.COLUMN10 = ds.Tables[0].Rows[cnt]["Control_Type"].ToString();
                    cb.COLUMN07 = ds.Tables[0].Rows[cnt]["Action"].ToString();
                    cb.COLUMN08 = ds.Tables[0].Rows[cnt]["Mandatory"].ToString();
                    cb.COLUMN11 = ds.Tables[0].Rows[cnt]["Section_Type"].ToString();
                    cb.COLUMN12 = ds.Tables[0].Rows[cnt]["Section_Name"].ToString();
                    cb.COLUMN13 = (cnt + 1).ToString();
                    if (ds.Tables[0].Rows[cnt]["Source_Type"].ToString() != "")
                    {
                        cb.COLUMN14 = ds.Tables[0].Rows[cnt]["Source_Type"].ToString();
                    }
                    else
                        cb.COLUMN14 = null;
                    if (ds.Tables[0].Rows[cnt]["Source_Value"].ToString() != "null")
                    {
                        cb.COLUMN15 = Convert.ToInt32(ds.Tables[0].Rows[cnt]["Source_Value"].ToString());
                    }
                    else
                        cb.COLUMN15 = null;
                    //cb.Form_Master = null;
                    cb.COLUMN03 = Convert.ToInt32(Session["FID"]);
                    cb.COLUMN04 = Convert.ToInt32(ds.Tables[0].Rows[cnt]["TID"].ToString());
                    cb.COLUMNA01 = null;
                    cb.COLUMNA02 = null;
                    if (Session["LogedEmployeeID"] != null)
                    {
                        cb.COLUMNA03 = cb.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                    }
                    else
                        cb.COLUMNA03 = cb.COLUMNA08 = null;
                    cb.COLUMNA04 = null;
                    cb.COLUMNA05 = null;
                    cb.COLUMNA06 = DateTime.Now;
                    cb.COLUMNA07 = null;
                    cb.COLUMNA09 = DateTime.Now;
                    cb.COLUMNA10 = null;
                    cb.COLUMNA11 = null;
                    cb.COLUMNA12 = true;
                    cb.COLUMNA13 = false;
                    db.CONTABLE006.Add(cb);
                    db.SaveChanges();
                }

                var fID = Convert.ToInt32(Session["FID"]);
                var fm = db.CONTABLE0010.Where(q => q.COLUMN02 == fID).FirstOrDefault();
                if (fm != null)
                {
                    fm.COLUMN07 = "1";
                    db.SaveChanges();

                }
                return this.Json("Fields adding Succeeded...", JsonRequestBehavior.AllowGet);
            }
            else

                return this.Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(int? tid)
        {
            Session["FID"] = Session["TID"] = null;
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            //tid = (int?)Session["ID"];
            FieldMasterModel obj = new FieldMasterModel();
            obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN06 == null && q.COLUMN04 == 101).ToList();
            obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == 0).ToList();
            obj.Source = db.MATABLE01.OrderBy(q => q.COLUMN02).ToList();
            obj.Source_Master = db.CONTABLE004.OrderBy(q => q.COLUMN02).ToList();
            obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
            ViewBag.Column = obj.tree;
            obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == tid).ToList();
            return View(obj);
        }

        public ActionResult Create(string lst, int mid, int fid)
        {
            ViewBag.selected = fid;
            if (lst != "")
            {
                Session["TID"] = lst;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                //tid = (int?)Session["id"];
                FieldMasterModel obj = new FieldMasterModel();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN06 == null && q.COLUMN04 == 101).ToList();
                obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == mid).ToList();
                obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
                obj.Source = db.MATABLE01.OrderBy(q => q.COLUMN02).ToList();
                obj.Source_Master = db.CONTABLE004.OrderBy(q => q.COLUMN02).ToList();
                ViewBag.lst = obj.lst;
                var list = lst.Split(',').Select(n => int.Parse(n)).ToList();
                var col = db.Table_Mapping;
                obj.tbl = db.CONTABLE005.Where(a => list.Contains(a.COLUMN03)).ToList();
                var abc = (from t in db.CONTABLE005
                           join c in db.CONTABLE004 on t.COLUMN03 equals c.COLUMN02
                           where list.Contains(t.COLUMN03) && t.COLUMN04.Length < 9 && t.COLUMN04 != "COLUMN01" && t.COLUMN04 != "COLUMN02"
                           select new
                           {
                               COLUMN04 = t.COLUMN04,
                               COLUMN03 = t.COLUMN03,
                               COLUMN05 = t.COLUMN05,
                               table_name = c.COLUMN04
                           }
                ).ToList();
                ViewBag.Table = abc;
                return PartialView("Index", obj);
            }
            else
            {
                string st = "0";
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                //tid = (int?)Session["id"];
                FieldMasterModel obj = new FieldMasterModel();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN04 == 101).ToList();
                obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == mid).ToList();
                obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
                ViewBag.lst = obj.lst;
                var list = st.Split(',').Select(n => int.Parse(n)).ToList();
                obj.tbl = db.CONTABLE005.Where(a => list.Contains(a.COLUMN03)).ToList();
                var abc = (from t in db.CONTABLE005
                           join c in db.CONTABLE004 on t.COLUMN03 equals c.COLUMN02
                           where list.Contains(t.COLUMN03)
                           select new
                           {
                               COLUMN04 = t.COLUMN04,
                               COLUMN03 = t.COLUMN03,
                               COLUMN05 = t.COLUMN05,
                               table_name = c.COLUMN04
                           }
                ).ToList();
                ViewBag.Table = abc;
                return PartialView("Index", obj);
            }
        }

        private List<DynaNode> GetTreeView(int? mid)
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            List<CONTABLE004> objTreeList = db.CONTABLE004.Where(s => s.COLUMN03 == mid).ToList();
            List<DynaNode> objList = new List<DynaNode>();
            foreach (var item in objTreeList)
            {
                DynaNode objTree = new DynaNode();
                objTree.key = item.COLUMN02.ToString();
                objTree.title = item.COLUMN04 + " ( " + item.COLUMN05 + " ) ";
                objTree.isLazy = true;
                //var objChild = objTreeList.FindAll(s => s.ParentId == item.BRANCHId);
                //if (objChild.Count > 0)
                //{
                //    objTree.isFolder = true;
                //}
                //else
                objTree.isFolder = true;
                //objTree.parentId = "0";
                objList.Add(objTree);
            }

            return objList;
        }

        public JsonResult GetAllNode(int? id)
        {
            if (id != null)
            {
                ViewBag.selected = Session["FID"] = id;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                var mid = db.CONTABLE0010.Where(a => a.COLUMN02 == id);
                var mId = mid.FirstOrDefault().COLUMN03;
                List<DynaNode> objList = GetTreeView(mId);
                return Json(objList, JsonRequestBehavior.AllowGet);
            }
            else
                if (Session["FID"] != null)
                {
                    int i = Convert.ToInt32(Session["FID"]);
                    eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                    var mid = db.CONTABLE0010.Where(a => a.COLUMN02 == i);
                    var mId = mid.FirstOrDefault().COLUMN03;
                    List<DynaNode> objList = GetTreeView(mId);
                    return Json(objList, JsonRequestBehavior.AllowGet);
                    ;
                }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetForms(int? id)
        {
            if (id != null)
            {
                FieldMasterModel obj = new FieldMasterModel();
                Session["MID"] = id;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN06 == null && q.COLUMN04 == 101).ToList();
                obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == id && q.COLUMN07 == "0").ToList();
                obj.Source = db.MATABLE01.OrderBy(q => q.COLUMN02).ToList();
                obj.Source_Master = db.CONTABLE004.OrderBy(q => q.COLUMN02).ToList();
                obj.tree = db.CONTABLE004.Where(s => s.COLUMN02 == 0).ToList();
                ViewBag.Column = obj.tree;
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == 0).ToList();
                return PartialView("index", obj);
            }
            else
            //if (Session["MID"] != null)
            {
                int i = Convert.ToInt32(Session["MID"]);
                FieldMasterModel obj = new FieldMasterModel();
                Session["MID"] = id;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN06 == null && q.COLUMN04 == 101).ToList();
                obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == i).ToList();
                obj.Source = db.MATABLE01.OrderBy(q => q.COLUMN02).ToList();
                obj.Source_Master = db.CONTABLE004.OrderBy(q => q.COLUMN02).ToList();
                obj.tree = db.CONTABLE004.Where(s => s.COLUMN02 == 0).ToList();
                ViewBag.Column = obj.tree;
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == 0).ToList();
                return PartialView("index", obj);
            }
            //return Json("", JsonRequestBehavior.AllowGet);
        }

        public class DynaNode
        {
            public string title { get; set; }
            public bool isFolder { get; set; }
            public bool isLazy { get; set; }
            public string key { get; set; }
            public string parentId { get; set; }
        }

    }
}
