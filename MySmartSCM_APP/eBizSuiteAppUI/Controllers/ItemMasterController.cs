﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBizSuiteAppModel.Table;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Helpers;
using WebMatrix.Data;
using eBizSuiteAppBAL.Fombuilding;
using System.Collections;
using System.IO;
using System.Text;
using eBizSuiteAppDAL.classes;
using System.Xml;

namespace MeBizSuiteAppUI.Controllers
{
    public class ItemMasterController : Controller
    {
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        CommonController cmn = new CommonController();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        string[] theader;
        string[] theadertext;
        public ActionResult Index()
        {
            try
            {
                FormBuildingEntities entity = new FormBuildingEntities();
                FormbuildingInfo info = new FormbuildingInfo();
                Session["DDDynamicItems"] = null;


                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);

                ViewBag.sortselected = Session["SortByStatus"];
                //EMPHCS1011 rajasekhar reddy patakota 25/08/2015 Customisation of Views
                ViewBag.viewselected = Session["ViewStyle"];
                var sql = "Select p.[COLUMN02 ] AS ID, p.COLUMN41 as Item#,p.COLUMN04 as 'Item Name',m.COLUMN03 as Type from MATABLE007 p left outer join MATABLE008 m on m.COLUMN02=p.COLUMN05 where p.COLUMNA03=" + Session["AcOwner"] + " and (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) and isnull(p.COLUMNA13,0)=0 order by cast(p.COLUMN02 as int) desc ";
                var GData = dbs.Query(sql);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in grid.ColumnNames)
                {
                    cols.Add(grid.Column(column, column));
                }
                ViewBag.Columns = cols;
                ViewBag.Grid = GData;
                return View("Index", activity.MATABLE007.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        [HttpGet]
        public ActionResult Create(string FormName, string idi)
        {
            try
            {
                string str = "ITEM0";
                Session["LineData"] = null;
                FormName = "Item";
                string idd; int alco = 0;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                List<CONTABLE006> all2 = new List<CONTABLE006>();
                List<FormBuildClass> all1 = new List<FormBuildClass>();
                List<CONTABLE006> colmap = new List<CONTABLE006>();
                List<CONTABLE0010> fm = new List<CONTABLE0010>();
                //if (idi == null)
                //{
                if (FormName != null)
                {
                    Session["FormName"] = FormName;
                }
                else
                {
                    if (Session["FormName"] != null)
                        FormName = Session["FormName"].ToString();
                    else
                        return RedirectToAction("Logon", "Account");
                }
                //var id1 = Session["Accordianid"].ToString();
                int moduleid = Convert.ToInt32(2796);
                var acID = (int?)Session["AcOwner"];
                var cenID = (int?)Session["cenid"];
                var fid = db.CONTABLE0010.Where(q => q.COLUMN04 == FormName).OrderBy(a => a.COLUMN02);
                var frmID = fid.Select(q => q.COLUMN02).FirstOrDefault();
                var frmType = fid.Select(q => q.COLUMN05).FirstOrDefault().ToString();
                var MasterForm = fid.Select(q => q.COLUMN06).FirstOrDefault();
                var center = db.CONTABLE003.Where(a => a.COLUMN02 == moduleid && a.COLUMN05 == FormName && a.COLUMNA03 == acID && a.COLUMN04 == cenID).ToList();
                FormName = fid.Select(q => q.COLUMN04).FirstOrDefault().ToString();
                if (center.Count == 0)
                {
                    center = db.CONTABLE003.Where(a => a.COLUMN05 == FormName && a.COLUMNA03 == acID && a.COLUMN04 == cenID).ToList();
                    var centerid = center.Where(a => a.COLUMN04 != 101).FirstOrDefault();
                    Session["cenid"] = centerid.COLUMN04;
                    Session["FormIDACCRDN1"] = centerid.COLUMN02;
                }
                Session["ofid"] = frmID;
                int ctblid = 0;
                FormBuildingEntities entity = new FormBuildingEntities();
                FormbuildingInfo info = new FormbuildingInfo();
                if (Request["ide"] != "null")
                {
                    entity = info.FormBuildReturnLinks(frmID);
                    Session["PForms"] = entity.PForms;
                }
                else Session["PForms"] = null;
                Session["id"] = frmID;
                var fdata = db.CONTABLE006.Where(a => a.COLUMN03 == frmID).OrderBy(a => a.COLUMN11);
                all2 = fdata.Where(a => a.COLUMN06 != null).ToList();
                var Ta = all2.Where(a => (a.COLUMN11 == "Item Level" || a.COLUMN11 == "Tab" || a.COLUMN11 == "Table") && a.COLUMN07 != "N").ToList();
                if (frmID == 1285 || frmID == 1274 || frmID == 1278 || frmID == 1528 || frmID == 1288 || frmID == 1525 || frmID == 1526)
                {
                    Ta = all2.Where(a => (a.COLUMN11 == "Item Level" || a.COLUMN11 == "Tab" || a.COLUMN11 == "Grid Level") && a.COLUMN07 != "N").OrderByDescending(a => a.COLUMN11).ToList();
                }
                Ta = Ta.Where(a => a.COLUMN06 != null).ToList();
                var Tabs = Ta.Select(b => b.COLUMN12).Distinct();
                if (frmID == 1261) Tabs = Ta.OrderBy(b => b.COLUMN13).Select(b => b.COLUMN12).Distinct();
                var itemslist = Ta.Where(b => b.COLUMN11 == "Item Level").OrderBy(q => q.COLUMN02).ToList();
                var itemscol = itemslist.Where(b => b.COLUMN06 != null).OrderBy(q => q.COLUMN02).ToList();
                ArrayList cols = new ArrayList();
                ArrayList JOIRcols = new ArrayList();
                for (int col = 0; col < itemscol.Count; col++)
                {
                    ctblid = Convert.ToInt32(itemscol[col].COLUMN04);
                    var ctbldata = db.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                    var ctname = ctbldata.COLUMN04;
                    var cid = (ctname + itemscol[col].COLUMN05);
                    cols.Add(cid);
                }
                ViewBag.NewCols = cols;
                var result = new List<dynamic>();
                if (itemslist.Count > 0)
                {
                    var obj = (IDictionary<string, object>)new System.Dynamic.ExpandoObject();
                    foreach (var row1 in itemslist)
                    {
                        ctblid = Convert.ToInt32(row1.COLUMN04);
                        var ctbldata = db.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                        var ctname = ctbldata.COLUMN04;
                        row1.COLUMN06 = (ctname + row1.COLUMN05);
                        obj.Add(row1.COLUMN06, row1.COLUMN06);
                    }
                    result.Add(obj);
                }

                var fddl = db.CONTABLE0010.Where(a => a.COLUMN04 == FormName).Select(q => new { q.COLUMN06 });
                var fid6 = fddl.Select(w => w.COLUMN06).First();
                var ddl = db.CONTABLE0010.Where(a => (a.COLUMN04 == FormName || a.COLUMN06 == fid6 || a.COLUMN02 == frmID) && (a.COLUMNA03 == acID || a.COLUMNA03.Equals(null))).OrderByDescending(a => a.COLUMN02);
                ViewBag.ddl = ddl.Select(a => a.COLUMN04).ToList();
                ViewBag.itemslist = result;
                ViewBag.itemscol = cols;
                ViewBag.Tabs = Tabs;
                var FName = db.CONTABLE0010.Where(a => a.COLUMN02 == frmID).First();
                FormName = FName.COLUMN04.ToString();
                ViewBag.selected = FormName;
                var tbl = fdata.OrderBy(a => a.COLUMN13).Select(a => a.COLUMN04).Distinct().ToList();
                Session["tbl"] = tbl.Count;

                int? tabindex = 0; string tabname = "";
                for (int i = 0; i < tbl.Count; i++)
                {
                    int tblid;
                    var tbl1 = fdata.OrderBy(a => a.COLUMN13).Select(a => a.COLUMN04).Distinct().ToList();
                    tblid = Convert.ToInt32(tbl1[i]);
                    SqlCommand acmd = new SqlCommand();
                    acmd = new SqlCommand(
                                                 "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                                               "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                                               " left join SETABLE011 s on s.COLUMN04 = " + tblid + " and CONTABLE006.COLUMN05 =s.COLUMN05 and s.COLUMN06 = " + frmID + "  " +
                                               " left join SETABLE012 s1 on s.COLUMN04 = s1.COLUMN04  and s1.COLUMN05 =s.COLUMN05 and s.COLUMN06 = s1.COLUMN06 and s1.COLUMNA03 = " + Session["AcOwner"] + " " +
                                               "where CONTABLE005.COLUMN03 =" + tblid + " and CONTABLE006.COLUMN04 =" + tblid + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04  and CONTABLE006.COLUMN11!='Grid Level' and CONTABLE006.COLUMN03=" + frmID + "  and  iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)='Y'  ", con);
                    con.Open();
                    SqlDataAdapter ada = new SqlDataAdapter(acmd);
                    DataTable adt = new DataTable();
                    ada.Fill(adt);
                    acmd.ExecuteNonQuery();
                    con.Close();

                }




                Session["DDDynamicItems"] = "";
                var ac = Convert.ToInt32(Session["AcOwner"]); //int? ou = null;
                var Vendor = activity.SATABLE001.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMN22 == "22285" || a.COLUMN22 == "" || a.COLUMN22 == "NULL")).ToList();
                ViewBag.Vendor = Vendor;
                var operatingunit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
                ViewBag.OPUnit = operatingunit;
                var Department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
                ViewBag.Department = Department;
                var UOM = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11119).ToList();
                ViewBag.UOM = UOM;
                var Transactions = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11142).ToList();
                ViewBag.Transactions = Transactions;
                var Pricetype = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11128).ToList();
                ViewBag.Pricetype = Pricetype;
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> family = new List<SelectListItem>();
                SqlConnection cn1 = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE003  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["family"] = new SelectList(family, "Value", "Text");
                List<SelectListItem> group = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE004  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    group.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["group"] = new SelectList(group, "Value", "Text");
                List<SelectListItem> type = new List<SelectListItem>();
                SqlConnection cn3 = new SqlConnection(sqlcon);
                SqlCommand cmd3 = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE008   where ISNULL(COLUMNA13,0)='0' ", cn);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                DataTable dt3 = new DataTable();
                da3.Fill(dt3);
                for (int dd = 0; dd < dt3.Rows.Count; dd++)
                {
                    type.Add(new SelectListItem { Value = dt3.Rows[dd]["COLUMN02"].ToString(), Text = dt3.Rows[dd]["COLUMN03"].ToString() });
                }
                ViewData["type"] = new SelectList(type, "Value", "Text");
                List<SelectListItem> ExpAcc = new List<SelectListItem>();
                SqlConnection cn4 = new SqlConnection(sqlcon);
                SqlCommand cmd4 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN07=22344", cn);
                SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
                DataTable dt4 = new DataTable();
                da4.Fill(dt4);
                for (int dd = 0; dd < dt4.Rows.Count; dd++)
                {
                    ExpAcc.Add(new SelectListItem { Value = dt4.Rows[dd]["COLUMN02"].ToString(), Text = dt4.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["ExpAcc"] = new SelectList(ExpAcc, "Value", "Text");
                List<SelectListItem> incomeAcc = new List<SelectListItem>();
                SqlConnection cn5 = new SqlConnection(sqlcon);
                SqlCommand cmd5 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN07 in (22344,22385,22384,22405,22406)", cn);
                SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                DataTable dt5 = new DataTable();
                da5.Fill(dt5);
                for (int dd = 0; dd < dt5.Rows.Count; dd++)
                {
                    incomeAcc.Add(new SelectListItem { Value = dt5.Rows[dd]["COLUMN02"].ToString(), Text = dt5.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["incomeAcc"] = new SelectList(incomeAcc, "Value", "Text");
                List<SelectListItem> Tax = new List<SelectListItem>();
                SqlConnection cn7 = new SqlConnection(sqlcon);
                SqlCommand cmd7 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False' and COLUMN06='True'", cn);
                SqlDataAdapter da7 = new SqlDataAdapter(cmd7);
                DataTable dt7 = new DataTable();
                da7.Fill(dt7);
                for (int dd = 0; dd < dt7.Rows.Count; dd++)
                {
                    Tax.Add(new SelectListItem { Value = dt7.Rows[dd]["COLUMN02"].ToString(), Text = dt7.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Tax"] = new SelectList(Tax, "Value", "Text");
                List<SelectListItem> PurchTax = new List<SelectListItem>();
                SqlConnection cn6 = new SqlConnection(sqlcon);
                SqlCommand cmd6 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False' and COLUMN13='True'", cn);
                SqlDataAdapter da6 = new SqlDataAdapter(cmd6);
                DataTable dt6 = new DataTable();
                da6.Fill(dt6);
                for (int dd = 0; dd < dt6.Rows.Count; dd++)
                {
                    PurchTax.Add(new SelectListItem { Value = dt6.Rows[dd]["COLUMN02"].ToString(), Text = dt6.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["PurchTax"] = new SelectList(PurchTax, "Value", "Text");
                List<SelectListItem> RawMaterial = new List<SelectListItem>();
                SqlCommand cmdr = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter dar = new SqlDataAdapter(cmdr);
                DataTable dtr = new DataTable();
                dar.Fill(dtr);
                for (int dd = 0; dd < dtr.Rows.Count; dd++)
                {
                    RawMaterial.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["RawMaterial"] = new SelectList(RawMaterial, "Value", "Text");
                MATABLE007 all = new MATABLE007();
                var col2 = activity.MATABLE007.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
                var col1 = col2;
                if (col2 == null || col2 == "0")
                { col1 = "56567"; all.COLUMN02 = "56567"; }
                else
                {
                    all.COLUMN02 = ((col1) + 1);
                }
                ViewBag.itemslist = col2;
                var sql = "select top 1 COLUMN03,COLUMN04 from FITABLE013 where COLUMNA03=" + ac;
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(sql);
                ViewBag.itemsdata = books;
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == 1261).ToList();
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 15/09/2015
                Session["AUTO"] = Oper;
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                //var customfrmID = 1261;
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1261 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN03 == 1261 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    //if (Tranlist.Count > 0)
                    //    listTM = null;
                    //else
                    //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 20/09/2015
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1261 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN03 == 1261 && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN03 == 1261 && a.COLUMNA03 == acOW).ToList();
                        //if (Tranlist.Count > 0)
                        //    listTM = null;
                        //else
                        //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 20/09/2015
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1261 && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1261 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var fino = "";
                var pono = "";
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                if (listTM != null)
                {
                    fino = Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                int? opunit = Convert.ToInt32(Session["OPUnit1"]);
                //EMPHCS1010 rajasekhar reddy patakota 25/08/2015 Autogeneration number with custom form
                SqlCommand pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM MATABLE007  Where COLUMN03='" + 1261 + "' and COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA02='" + opunit + "'   AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE002  Where COLUMN03='" + 1261 + "' and  COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA02='" + opunit + "'  AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'   ORDER BY COLUMN02 DESC", cn);
                cn.Open();
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                cn.Close();
                var paddingno = ""; var paddingnolength = ""; var item = ""; if (listTM != null)
                {
                    if (padt.Rows.Count > 0)
                    {
                        if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                        {
                            if (sNum == "") sNum = "00001";
                            else sNum = sNum;
                            pono = fino + sNum + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                            item = (pono).TrimEnd();
                        }
                        else
                        {
                            if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                            {
                                paddingnolength = sNum.Length.ToString();
                                paddingno = "00000";
                                pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                                Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                            }
                            else if (CurrentNo != "" && CurrentNo != null)
                            {
                                paddingnolength = CurrentNo.Length.ToString();
                                paddingno = "00000";
                                pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                            }
                            else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                            {
                                sNum = (Convert.ToInt32(sNum) + 0).ToString();
                                paddingnolength = sNum.Length.ToString();
                                paddingno = "00000";
                                pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(sNum));
                            }
                            else
                            {
                                sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                                paddingnolength = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString().Length.ToString();
                                paddingno = "00000";
                                pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                            }
                            item = pono.TrimEnd();
                            Session["INO"] = item;
                        }
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            paddingnolength = sNum.Length.ToString();
                            paddingno = "00000";
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            paddingnolength = CurrentNo.Length.ToString();
                            paddingno = "00000";
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            paddingnolength = sNum.Length.ToString();
                            paddingno = "00000";
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            paddingnolength = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString().Length.ToString();
                            paddingno = "00000";
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        item = (pono.TrimEnd());
                        Session["INO"] = item;
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    paddingnolength = sNum.Length.ToString();
                    paddingno = "00000";
                    if (padt.Rows.Count > 0)
                    { CurrentNo = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, ""))).ToString(paddingno); }
                    if (listTM != null)
                    {
                        if (CurrentNo == "") CurrentNo = "00001";
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    item = (pono.TrimEnd());
                    Session["INO"] = item;
                }
                //}
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }

        }

        [HttpGet]
        public ActionResult CreateOnfly(string FormName, string idi)
        {
            try
            {
                var ac = Convert.ToInt32(Session["AcOwner"]);
                var Vendor = activity.SATABLE001.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMN22 == "22285" || a.COLUMN22 == "" || a.COLUMN22 == "NULL")).ToList();
                ViewBag.Vendor = Vendor;
                var UOM = db.MATABLE002.Where(a => a.COLUMNA13 == false && (a.COLUMNA03 == ac || a.COLUMNA03 == null) && a.COLUMN03 == 11119).ToList();
                ViewBag.UOM = UOM;
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> family = new List<SelectListItem>();
                SqlConnection cn1 = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE003  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                }
                family = family.OrderBy(x => x.Text).ToList();
                ViewData["family"] = new SelectList(family, "Value", "Text");
                List<SelectListItem> group = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE004  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    group.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                group = group.OrderBy(x => x.Text).ToList();
                ViewData["group"] = new SelectList(group, "Value", "Text");
                List<SelectListItem> type = new List<SelectListItem>();
                SqlConnection cn3 = new SqlConnection(sqlcon);
                SqlCommand cmd3 = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE008 where COLUMN02 in ('ITTY001','ITTY009')", cn);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                DataTable dt3 = new DataTable();
                da3.Fill(dt3);
                for (int dd = 0; dd < dt3.Rows.Count; dd++)
                {
                    if (dt3.Rows[dd]["COLUMN02"].ToString() == "ITTY001")
                        type.Add(new SelectListItem { Value = dt3.Rows[dd]["COLUMN02"].ToString(), Text = dt3.Rows[dd]["COLUMN03"].ToString(), Selected = true });
                    else
                        type.Add(new SelectListItem { Value = dt3.Rows[dd]["COLUMN02"].ToString(), Text = dt3.Rows[dd]["COLUMN03"].ToString() });
                }
                type = type.OrderBy(x => x.Text).ToList();
                ViewData["type"] = new SelectList(type, "Value", "Text");
                List<SelectListItem> Tax = new List<SelectListItem>();
                SqlConnection cn7 = new SqlConnection(sqlcon);
                SqlCommand cmd7 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False' and COLUMN06='True'	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter da7 = new SqlDataAdapter(cmd7);
                DataTable dt7 = new DataTable();
                da7.Fill(dt7);
                for (int dd = 0; dd < dt7.Rows.Count; dd++)
                {
                    Tax.Add(new SelectListItem { Value = dt7.Rows[dd]["COLUMN02"].ToString(), Text = dt7.Rows[dd]["COLUMN04"].ToString() });
                }
                Tax = Tax.OrderBy(x => x.Text).ToList();
                ViewData["Tax"] = new SelectList(Tax, "Value", "Text");
                List<SelectListItem> PurchTax = new List<SelectListItem>();
                SqlConnection cn6 = new SqlConnection(sqlcon);
                SqlCommand cmd6 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False' and COLUMN13='True' 	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter da6 = new SqlDataAdapter(cmd6);
                DataTable dt6 = new DataTable();
                da6.Fill(dt6);
                for (int dd = 0; dd < dt6.Rows.Count; dd++)
                {
                    PurchTax.Add(new SelectListItem { Value = dt6.Rows[dd]["COLUMN02"].ToString(), Text = dt6.Rows[dd]["COLUMN04"].ToString() });
                }
                PurchTax = PurchTax.OrderBy(x => x.Text).ToList();
                ViewData["PurchTax"] = new SelectList(PurchTax, "Value", "Text");
                List<SelectListItem> HSN = new List<SelectListItem>();
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE032  Where  (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,0)=0 and COLUMN04!='' ", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    HSN.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                HSN = HSN.OrderBy(x => x.Text).ToList();
                ViewData["HSN"] = new SelectList(HSN, "Value", "Text");
                return View("Create");
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult line(string list)
        {
            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            Session["LineData"] = ds;
            return null;
        }
        [HttpPost]
        public ActionResult Create(FormCollection col)
        {
            try
            {

                int OutParam = 0;
                string OrderType = "1261";
                string Date = DateTime.Now.ToString();

                string OpUnit = Convert.ToString(Session["OPUnit1"]);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE007 ", con);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                con.Open();
                SqlCommand Cmd = new SqlCommand("usp_MAS_BL_ItemMaster", con);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1261");
                TransNo = GetTransactionNo(1261);
                Cmd.Parameters.AddWithValue("@COLUMN41", col["COLUMN41"]);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["COLUMN04"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["COLUMN05"]);
                Cmd.Parameters.AddWithValue("@COLUMN06", col["COLUMN06"]);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["COLUMN08"]);
                Cmd.Parameters.AddWithValue("@COLUMN45", col["COLUMN45"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", col["COLUMN10"]);
                Cmd.Parameters.AddWithValue("@COLUMN12", col["COLUMN12"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["COLUMN11"]);
                //EMPHCS1781 Retail Creation of Customer and Item Master by gnaneshwar on 9/7/2016
                Cmd.Parameters.AddWithValue("@COLUMN46", col["COLUMN46"]);
                Cmd.Parameters.AddWithValue("@COLUMN63", col["COLUMN63"]);
                var inactive = Convert.ToString(col["COLUMN47"]);
                if (inactive == "on" || inactive == "True" || inactive == "1") inactive = "1";
                else inactive = "0";
                Cmd.Parameters.AddWithValue("@COLUMN47", inactive);
                var QOH = Convert.ToString(col["COLUMN48"]);
                if (QOH == "on" || QOH == "True" || QOH == "1") QOH = "1";
                Cmd.Parameters.AddWithValue("@COLUMN48", QOH);
                var multiunit = Convert.ToString(col["COLUMN65"]);
                if (multiunit == "on" || multiunit == "True" || multiunit == "1") multiunit = "1";
                Cmd.Parameters.AddWithValue("@COLUMN65", multiunit);
                var poitem = Convert.ToString(col["COLUMN56"]);
                if (poitem == "on" || poitem == "True" || poitem == "1") poitem = "1";
                Cmd.Parameters.AddWithValue("@COLUMN56", poitem);
                var soitem = Convert.ToString(col["COLUMN49"]);
                if (soitem == "on" || soitem == "True" || soitem == "1") soitem = "1";
                Cmd.Parameters.AddWithValue("@COLUMN49", soitem);
                var lot = Convert.ToString(col["COLUMN66"]);
                if (lot == "on" || lot == "True" || lot == "1") lot = "1";
                Cmd.Parameters.AddWithValue("@COLUMN66", lot);
                Cmd.Parameters.AddWithValue("@COLUMN40", col["COLUMN40"]);
                var ouselected = Convert.ToString(col["COLUMN37"]);
                if (ouselected == "" || ouselected == "0") ouselected = null;
                Cmd.Parameters.AddWithValue("@COLUMN37", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMN57", col["COLUMN57"]);
                Cmd.Parameters.AddWithValue("@COLUMN59", col["COLUMN59"]);

                Cmd.Parameters.AddWithValue("@COLUMN61", col["COLUMN61"]);
                if (col["COLUMN17"] == "")
                    col["COLUMN17"] = null;
                Cmd.Parameters.AddWithValue("@COLUMN17", col["COLUMN17"]);
                Cmd.Parameters.AddWithValue("@COLUMN50", col["COLUMN50"]);

                Cmd.Parameters.AddWithValue("@COLUMN51", col["COLUMN51"]);
                Cmd.Parameters.AddWithValue("@COLUMN52", col["COLUMN52"]);
                Cmd.Parameters.AddWithValue("@COLUMN54", col["COLUMN54"]);
                Cmd.Parameters.AddWithValue("@COLUMN64", col["COLUMN64"]);
                Cmd.Parameters.AddWithValue("@COLUMN67", col["COLUMN67"]);

                Cmd.Parameters.AddWithValue("@COLUMNA02", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "MATABLE007");
                //EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                con.Close();

                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);

                var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1261).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();
                if (r > 0)
                {
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN11 == custfname && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //Line
                if (Convert.ToString(Session["CustomerPricing"]) == "1" || Convert.ToString(Session["CustomerPricing"]) == "True")
                    Pricinggridsave(HCol2, ouselected, AOwner, Date, insert);
                if (OutParam > 0)
                    ItemPricingSaveMethod(OutParam, "");
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemSOMATABLE007"] = "";
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemPOMATABLE007"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1261", "110008817", "Update"); }
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Index", "ItemMaster", new { idi = idi, FormName = custform });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public void Pricinggridsave(string HCol2, string ouselected, string AOwner, string Date, string insert)
        {
            DataSet itemdata = new DataSet();
            itemdata = (DataSet)Session["LineData"];
            if (itemdata.Tables.Count > 0)
            {
                SqlCommand cmddl = new SqlCommand("select  max(COLUMN01) from MATABLE007 where COLUMN02=" + HCol2 + "", con);
                SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                DataTable dttl = new DataTable();
                daal.Fill(dttl);
                var HColid ="";
                if (insert == "Insert") HColid = dttl.Rows[0][0].ToString();
                else HColid = HCol2;
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    string PriceType = itemdata.Tables[0].Rows[i]["COLUMN03"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["COLUMN04"].ToString();
                    SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_ItemMaster", con);
                    con.Open();
                    Cmdl.CommandType = CommandType.StoredProcedure;
                    Cmdl.Parameters.AddWithValue("@COLUMN02", HColid);
                    Cmdl.Parameters.AddWithValue("@COLUMN03", PriceType);
                    Cmdl.Parameters.AddWithValue("@COLUMN04", Price);
                    Cmdl.Parameters.AddWithValue("@COLUMNA02", ouselected);
                    Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmdl.Parameters.AddWithValue("@Direction", insert);
                    Cmdl.Parameters.AddWithValue("@TabelName", "FITABLE013");
                    Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                    int o = Cmdl.ExecuteNonQuery();
                    con.Close();
                }
            }
            Session["LineData"] = null;
        }

        [HttpPost]
		//EMPHCS1853 rajasekhar reddy patakota 27/01/2017 In Service Receipt on item selection default Tax calculations
        public ActionResult ItemPricingSaveMethod(int ItemIID, string OnflyItemType)
        {
            try
            {
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["PurchaseGridData"];
                DataSet itemdata1 = new DataSet();
                itemdata1 = (DataSet)Session["SalesGridData"];
                DataSet itemdata2 = new DataSet();
                itemdata2 = (DataSet)Session["PricingGridData"];
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert";
                SqlCommand cmddl = new SqlCommand("select  max(COLUMN02) from MATABLE007 where COLUMN01=" + ItemIID + "", con);
                SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                DataTable dttl = new DataTable();
                daal.Fill(dttl);
                string LCol2 = dttl.Rows[0][0].ToString();
                if (itemdata.Tables.Count > 0)
                {
                    for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                    {
                        SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE024", con);
                        SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                        DataTable dtt = new DataTable();
                        daa.Fill(dtt);
                        string HCol2 = dtt.Rows[0][0].ToString();
                        if (HCol2 == "") HCol2 = "999";
                        HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                        string Opunit = itemdata.Tables[0].Rows[i]["Opunit"].ToString();
                        string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                        string MRP = itemdata.Tables[0].Rows[i]["MRP"].ToString();
                        SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_PriceDetails", con);
                        con.Open();
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", HCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Opunit);
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", MRP);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", "Purchase");
                        Cmdl.Parameters.AddWithValue("@COLUMN07", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE024");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        int l = Cmdl.ExecuteNonQuery();
                        con.Close();
                    }
                }
                if (itemdata1.Tables.Count > 0)
                {
                    for (int i = 0; i < itemdata1.Tables[0].Rows.Count; i++)
                    {
                        SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE024", con);
                        SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                        DataTable dtt = new DataTable();
                        daa.Fill(dtt);
                        string HCol2 = dtt.Rows[0][0].ToString();
                        HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                        string Opunit = itemdata1.Tables[0].Rows[i]["SOpunit"].ToString();
                        string Price = itemdata1.Tables[0].Rows[i]["SPrice"].ToString();
                        string MRP = itemdata1.Tables[0].Rows[i]["SMRP"].ToString();
                        SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_PriceDetails", con);
                        con.Open();
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", HCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Opunit);
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", MRP);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", "Sales");
                        Cmdl.Parameters.AddWithValue("@COLUMN07", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE024");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        int l = Cmdl.ExecuteNonQuery();
                        con.Close();
                    }
                }
                if (itemdata2.Tables.Count > 0)
                {
                    for (int i = 0; i < itemdata2.Tables[0].Rows.Count; i++)
                    {
                        SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE025", con);
                        SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                        DataTable dtt = new DataTable();
                        daa.Fill(dtt);
                        string HCol2 = dtt.Rows[0][0].ToString();
                        if (HCol2 == "" || HCol2 == null) HCol2 = "999";
                        HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                        string Priceid = itemdata2.Tables[0].Rows[i]["Priceid"].ToString();
                        string Type = itemdata2.Tables[0].Rows[i]["Type"].ToString();
                        string PriceLevel = itemdata2.Tables[0].Rows[i]["PriceLevel"].ToString();
                        string Discount = itemdata2.Tables[0].Rows[i]["Discount"].ToString();
                        string Select = itemdata2.Tables[0].Rows[i]["Selected"].ToString();
                        SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_ItemvsPriceLevel", con);
                        con.Open();
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", HCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Priceid);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Type);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", PriceLevel);
                        Cmdl.Parameters.AddWithValue("@COLUMN07", Discount);
                        Cmdl.Parameters.AddWithValue("@COLUMN08", Select);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE025");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        int l = Cmdl.ExecuteNonQuery();
                        con.Close();
                    }
                }
                Session["PurchaseGridData"] = null;
                Session["SalesGridData"] = null;
                Session["PricingGridData"] = null;
                var idi = "";
                List<SelectListItem> ItemL = new List<SelectListItem>();
                if (Session["DDDynamicItems"] == "DDDynamicItems")
                {
                    OpUnit = Convert.ToString(Session["OPUnit"]);
					//EMPHCS1853 rajasekhar reddy patakota 27/01/2017 In Service Receipt on item selection default Tax calculations
                    string sql = "select COLUMN02,COLUMN04 COLUMN03 from MATABLE007  where  COLUMN04 !='' AND COLUMN05='ITTY001' and ( COLUMNA02 in(" + OpUnit + ") or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc";
                    if (OnflyItemType == "ServiceItem") sql = "select COLUMN02,COLUMN04 COLUMN03 from MATABLE007  where COLUMN04 !='' AND COLUMN05='ITTY009' and  ( COLUMNA02 in(" + OpUnit + ") or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc";
                    SqlDataAdapter cmdd = new SqlDataAdapter(sql, con);
                    DataTable dtdata = new DataTable();
                    cmdd.Fill(dtdata);
                    ViewData["Item"] = new SelectList(ItemL, "Value", "Text");
                    Session["DDDynamicItemsData"] = null;
                    ItemL.Add(new SelectListItem { Value = "", Text = "" });
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        if (dtdata.Rows[dd]["COLUMN02"].ToString() == LCol2)
                            ItemL.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString(), Selected = true });
                        else
                            ItemL.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString() });
                    }
                    Session["DDDynamicItems"] = null;
                    Session["DDDynamicItemsData"] = ItemL;
					//EMPHCS1853 rajasekhar reddy patakota 27/01/2017 In Service Receipt on item selection default Tax calculations
                    return Json(new { item = ItemL, OnflyItemType = OnflyItemType }, JsonRequestBehavior.AllowGet);
                }
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Index", "ItemMaster", new { idi = idi, FormName = custform });
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        [HttpPost]
        public ActionResult PurcingItemEditSave(string Eid)
        {
            try
            {
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["PurchaseGridData"];
                DataSet itemdata1 = new DataSet();
                itemdata1 = (DataSet)Session["SalesGridData"];
                DataSet itemdata2 = new DataSet();
                itemdata2 = (DataSet)Session["PricingGridData"];
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update";
                if (Eid != "" && Eid != null)
                {
                    SqlCommand cmddl = new SqlCommand("select  max(COLUMN02) from MATABLE007 where COLUMN02=" + Eid + "", con);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    Eid = LCol2;
                    SqlCommand cmdi = new SqlCommand("UPDATE MATABLE024 SET COLUMNA13=1 WHERE COLUMN07 in(" + Eid + ") and COLUMNA03=" + AOwner + "", con);
                    con.Open();
                    cmdi.ExecuteNonQuery();
                    con.Close();
                    if (itemdata.Tables.Count > 0)
                    {
                        for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                        {
                            SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE024", con);
                            SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                            DataTable dtt = new DataTable();
                            daa.Fill(dtt);
                            string HCol2 = dtt.Rows[0][0].ToString();
                            if (HCol2 == "") HCol2 = "999";
                            HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                            string Opunit = itemdata.Tables[0].Rows[i]["Opunit"].ToString();
                            string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                            string MRP = itemdata.Tables[0].Rows[i]["MRP"].ToString();
                            string PId = itemdata.Tables[0].Rows[i]["PId"].ToString();
                            if (PId == "" || PId == "0") PId = HCol2;
                            SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_PriceDetails", con);
                            con.Open();
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", PId);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", Opunit);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", Price);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", MRP);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", "Purchase");
                            Cmdl.Parameters.AddWithValue("@COLUMN07", LCol2);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE024");
                            Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                            int l = Cmdl.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    if (itemdata1.Tables.Count > 0)
                    {
                        for (int i = 0; i < itemdata1.Tables[0].Rows.Count; i++)
                        {
                            SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE024", con);
                            SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                            DataTable dtt = new DataTable();
                            daa.Fill(dtt);
                            string HCol2 = dtt.Rows[0][0].ToString();
                            HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                            string Opunit = itemdata1.Tables[0].Rows[i]["SOpunit"].ToString();
                            string Price = itemdata1.Tables[0].Rows[i]["SPrice"].ToString();
                            string MRP = itemdata1.Tables[0].Rows[i]["SMRP"].ToString();
                            string SId = itemdata1.Tables[0].Rows[i]["SId"].ToString();
                            if (SId == "" || SId == "0") SId = HCol2;
                            SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_PriceDetails", con);
                            con.Open();
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", SId);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", Opunit);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", Price);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", MRP);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", "Sales");
                            Cmdl.Parameters.AddWithValue("@COLUMN07", LCol2);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE024");
                            Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                            int l = Cmdl.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    if (itemdata2.Tables.Count > 0)
                    {
                        for (int i = 0; i < itemdata2.Tables[0].Rows.Count; i++)
                        {
                            SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE025", con);
                            SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                            DataTable dtt = new DataTable();
                            daa.Fill(dtt);
                            string HCol2 = dtt.Rows[0][0].ToString();
                            if (HCol2 == "" || HCol2 == null) HCol2 = "999";
                            HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                            string Priceid = itemdata2.Tables[0].Rows[i]["Priceid"].ToString();
                            string Type = itemdata2.Tables[0].Rows[i]["Type"].ToString();
                            string PriceLevel = itemdata2.Tables[0].Rows[i]["PriceLevel"].ToString();
                            string Discount = itemdata2.Tables[0].Rows[i]["Discount"].ToString();
                            string Select = itemdata2.Tables[0].Rows[i]["Selected"].ToString();
                            string Pid = itemdata2.Tables[0].Rows[i]["Pid"].ToString();
                            if (Priceid == "0") { insert = "Insert"; Priceid = HCol2; } else insert = "Update";
                            SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_ItemvsPriceLevel", con);
                            con.Open();
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", Priceid);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", LCol2);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", Pid);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", Type);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", PriceLevel);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", Discount);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", Select);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE025");
                            Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                            int l = Cmdl.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    Session["PurchaseGridData"] = null;
                    Session["SalesGridData"] = null;
                    Session["PricingGridData"] = null;
                    var idi = "";
                    var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                    return RedirectToAction("Index", "ItemMaster", new { idi = idi, FormName = custform });
                }
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public string GetTransactionNo(int frmID)
        {
            try
            {
                var acID = (int?)Session["AcOwner"];
                var OPUnit = Session["OPUnit1"];
                var eid = (int?)Session["eid"];
                SqlCommand cmd = new SqlCommand("usp_Proc_TransNoGenerate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Formid", frmID);
                //cmd.Parameters.AddWithValue("@TblID", tblID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", acID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string item = "", trancol = "", Override = "";
                if (dt.Rows.Count > 0 && dt.Columns.Contains("PO"))
                    return dt.Rows[0]["PO"].ToString();
                else
                {
                    eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                    var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                    var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                    int frmIDchk = Convert.ToInt32(frmIDc);
                    int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                    if (Oper == 0)
                        Oper = null;
                    var acOW = Convert.ToInt32(Session["AcOwner"]);
                    var custfname = db.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                    var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                    var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                    if (Oper == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                        if (Oper == null)
                        {
                            Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                        }
                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    var Prefix = "";
                    var Endno = "";
                    var Sufix = " ";
                    var CurrentNo = "";
                    var sNum = "";
                    int c = 0;
                    string fino = "", pono = "", TransNo = "";
                    SqlCommand pacmd = new SqlCommand();
                    if (listTM != null)
                    {
                        Prefix = listTM.COLUMN06.ToString();
                        Sufix = listTM.COLUMN10.ToString();
                        sNum = listTM.COLUMN07.ToString();
                        CurrentNo = listTM.COLUMN09.ToString();
                        ViewBag.Override = listTM.COLUMN12.ToString();
                        //ViewBag.ColumnName = all1[alco + row].Field_Name;
                        c = Prefix.Length + 1;
                        if (string.IsNullOrEmpty(Sufix))
                            Sufix = " ";
                    }
                    else
                    {
                        Prefix = null;
                    }
                    frmIDchk = Convert.ToInt32(frmID);

                    if (frmIDchk == 1277)
                    {
                        if (Prefix != null)
                        {
                            fino = Prefix;
                            pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                        else
                        {
                            fino = "IV";
                            pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                    }
                    else if (frmIDchk == 1278)
                    {
                        if (Prefix != null)
                        {
                            fino = Prefix;
                            pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                        else
                        {
                            fino = "CP";
                            pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                    }
                    else if (frmIDchk == 1261)
                    {
                        if (Prefix != null)
                        {
                            fino = Prefix;
                            pacmd = new SqlCommand("Select  substring(COLUMN05," + c + ",len(COLUMN05)) PO FROM MATABLE007  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM MATABLE007  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN05 LIKE '" + fino + "%') AND COLUMN05 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                        else
                        {
                            fino = "CP";
                            pacmd = new SqlCommand("Select  substring(COLUMN05,3,len(COLUMN05)) PO FROM MATABLE007  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM MATABLE007  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN05 LIKE '" + fino + "%') AND COLUMN05 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                    }
                    else if (frmIDchk == 1330)
                    {
                        if (Prefix != null)
                        {
                            fino = Prefix;
                            pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE005  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE005  Where  COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                        else
                        {
                            fino = "CM";
                            pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE005  Where  COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE005  Where  COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", con);
                        }
                    }
                    SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                    DataTable padt = new DataTable();
                    pada.Fill(padt);
                    pacmd.ExecuteNonQuery();
                    var paddingno = "00000"; //var paddingnolength = "";
                    int tranrowcount = padt.Rows.Count;
                    if (tranrowcount > 0)
                    {
                        if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                        {
                            if (sNum == "") sNum = "00001";
                            else sNum = sNum;
                            pono = fino + sNum + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                            TransNo = (pono).TrimEnd();
                        }
                        else
                        {
                            if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                            {
                                pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                                Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                            }
                            else if (CurrentNo != "" && CurrentNo != null)
                            {
                                pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                            }
                            else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                            {
                                sNum = (Convert.ToInt32(sNum) + 0).ToString();
                                pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(sNum));
                            }
                            else
                            {
                                sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                                pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                            }
                            TransNo = pono.TrimEnd();
                        }
                    }
                    else
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        if (CurrentNo == "") CurrentNo = "00001";
                        sNum = (Convert.ToInt32(sNum) + 0).ToString();
                        if (listTM != null)
                        {
                            if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                            else
                            {
                                CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                            }
                        }
                        if (CurrentNo != "" && CurrentNo != null)
                        { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                        else
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                        TransNo = (pono.TrimEnd());
                    }
                    return TransNo;
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }
        [HttpGet]
        public ActionResult Edit(string ide)
        {

            Session["editid"] = ide;
            Session["LineData"] = null;
            List<SelectListItem> Country = new List<SelectListItem>();
            List<SelectListItem> Country1 = new List<SelectListItem>();
            List<SelectListItem> Country2 = new List<SelectListItem>();
            List<SelectListItem> Country3 = new List<SelectListItem>();
            List<SelectListItem> Country4 = new List<SelectListItem>();
            List<SelectListItem> Country5 = new List<SelectListItem>();
            List<SelectListItem> Country6 = new List<SelectListItem>();
            var ac = Convert.ToInt32(Session["AcOwner"]);
            int? op = null;
            string opstatus = Convert.ToString(Session["OPUnitstatus"]);
            if (opstatus == null || opstatus == "") op = null;
            else op = Convert.ToInt32(Session["OPUnit"]);
            var edit = activity.MATABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMNA02 == op || a.COLUMNA02 == null) && a.COLUMN02 == ide).ToList();
            var sql2 = "select COLUMN02,COLUMN03,COLUMN04 from FITABLE013 where COLUMNA03=" + ac + " and COLUMN02=" + edit[0].COLUMN02 + " and isnull(COLUMNA13,0)=0";
            SqlCommand cm = new SqlCommand(sql2, con);
            SqlDataAdapter d = new SqlDataAdapter(cm);
            DataTable t1 = new DataTable();
            d.Fill(t1);
            Session["DDDynamicItems"] = "";
            var Vendor = activity.SATABLE001.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMN22 == "22285" || a.COLUMN22 == "" || a.COLUMN22 == "NULL")).ToList();
            for (int dd = 0; dd < Vendor.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = Vendor[dd].COLUMN02.ToString(), Text = Vendor[dd].COLUMN05 });
            }
            ViewBag.dval14 = Convert.ToString(edit[0].COLUMN08);
            ViewBag.Vendor = new SelectList(Country, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN08));
            var operatingunit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
            for (int dd = 0; dd < operatingunit.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = operatingunit[dd].COLUMN02.ToString(), Text = operatingunit[dd].COLUMN03 });
            }
            ViewBag.dval13 =Convert.ToString(edit[0].COLUMN37);
            ViewBag.OPUnit = new SelectList(Country2, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN37));
            var Department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
            for (int dd = 0; dd < Department.Count; dd++)
            {
                Country3.Add(new SelectListItem { Value = Department[dd].COLUMN02.ToString(), Text = Department[dd].COLUMN04 });
            }
            ViewBag.dval12 =Convert.ToString(edit[0].COLUMN40);
            ViewBag.Department = new SelectList(Country3, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN40));
            var UOM = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11119).ToList();
            for (int dd = 0; dd < UOM.Count; dd++)
            {
                Country4.Add(new SelectListItem { Value = UOM[dd].COLUMN02.ToString(), Text = UOM[dd].COLUMN04 });
            }
            ViewBag.dval11 =Convert.ToString(edit[0].COLUMN63);
            ViewBag.UOM = new SelectList(Country4, "Value", "Text", selectedValue:Convert.ToString( edit[0].COLUMN63));
            var Transactions = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11142).ToList();
            if (Transactions.Count > 0)
            {
                for (int dd = 0; dd < Transactions.Count; dd++)
                {
                    Country5.Add(new SelectListItem { Value = Transactions[dd].COLUMN02.ToString(), Text = Transactions[dd].COLUMN04 });
                }
                ViewBag.dval10 = Convert.ToString(edit[0].COLUMN07);
                ViewBag.Transactions = new SelectList(Country5, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN07));
            }
            var Pricetype = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11128).ToList();
            if (Pricetype.Count > 0)
            {
                for (int dd = 0; dd < Pricetype.Count; dd++)
                {
                    Country6.Add(new SelectListItem { Value = Pricetype[dd].COLUMN02.ToString(), Text = Pricetype[dd].COLUMN04 });
                }
                if (t1.Rows.Count > 0)
                {
                    ViewBag.dval9 = t1.Rows[0]["COLUMN03"].ToString();
                    ViewBag.Pricetype = new SelectList(Country6, "Value", "Text", selectedValue: t1.Rows[0]["COLUMN03"].ToString());
                }
                else { ViewBag.dval9 = null; ViewBag.Pricetype = null; }
            }
            
            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
           
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval8 = Convert.ToString(edit[0].COLUMN10);
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN10));
            List<SelectListItem> family = new List<SelectListItem>();
            SqlConnection cn1 = new SqlConnection(sqlcon);
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE003  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval7 = Convert.ToString(edit[0].COLUMN12);
            ViewData["family"] = new SelectList(family, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN12));
            List<SelectListItem> group = new List<SelectListItem>();
            SqlConnection cn2 = new SqlConnection(sqlcon);
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE004  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                group.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval6 = Convert.ToString(edit[0].COLUMN11);
            ViewData["group"] = new SelectList(group, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN11));
            List<SelectListItem> type = new List<SelectListItem>();
            SqlConnection cn3 = new SqlConnection(sqlcon);
            SqlCommand cmd3 = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE008   where ISNULL(COLUMNA13,0)='0' ", cn);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            for (int dd = 0; dd < dt3.Rows.Count; dd++)
            {
                type.Add(new SelectListItem { Value = dt3.Rows[dd]["COLUMN02"].ToString(), Text = dt3.Rows[dd]["COLUMN03"].ToString() });
            }
            ViewBag.dval5 = Convert.ToString(edit[0].COLUMN05);
            ViewData["type"] = new SelectList(type, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN05));
            List<SelectListItem> ExpAcc = new List<SelectListItem>();
            SqlConnection cn4 = new SqlConnection(sqlcon);
            SqlCommand cmd4 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN07=22344", cn);
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            DataTable dt4 = new DataTable();
            da4.Fill(dt4);
            for (int dd = 0; dd < dt4.Rows.Count; dd++)
            {
                ExpAcc.Add(new SelectListItem { Value = dt4.Rows[dd]["COLUMN02"].ToString(), Text = dt4.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval4 = Convert.ToString(edit[0].COLUMN59);
            ViewData["ExpAcc"] = new SelectList(ExpAcc, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN59));
            List<SelectListItem> incomeAcc = new List<SelectListItem>();
            SqlConnection cn5 = new SqlConnection(sqlcon);
            SqlCommand cmd5 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN07=22405", cn);
            SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
            DataTable dt5 = new DataTable();
            da5.Fill(dt5);
            for (int dd = 0; dd < dt5.Rows.Count; dd++)
            {
                incomeAcc.Add(new SelectListItem { Value = dt5.Rows[dd]["COLUMN02"].ToString(), Text = dt5.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval3 = Convert.ToString(edit[0].COLUMN52);
            ViewData["incomeAcc"] = new SelectList(incomeAcc, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN52));
            List<SelectListItem> Tax = new List<SelectListItem>();
            SqlConnection cn7 = new SqlConnection(sqlcon);
            SqlCommand cmd7 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)   and isnull(COLUMNA13,'False')='False' and COLUMN06='True'", cn);
            SqlDataAdapter da7 = new SqlDataAdapter(cmd7);
            DataTable dt7 = new DataTable();
            da7.Fill(dt7);
            for (int dd = 0; dd < dt7.Rows.Count; dd++)
            {
                Tax.Add(new SelectListItem { Value = dt7.Rows[dd]["COLUMN02"].ToString(), Text = dt7.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval2 = Convert.ToString(edit[0].COLUMN54);
            ViewData["Tax"] = new SelectList(Tax, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN54));
            List<SelectListItem> PurchTax = new List<SelectListItem>();
            SqlConnection cn6 = new SqlConnection(sqlcon);
            SqlCommand cmd6 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)   and isnull(COLUMNA13,'False')='False' and COLUMN13='True'", cn);
            SqlDataAdapter da6 = new SqlDataAdapter(cmd6);
            DataTable dt6 = new DataTable();
            da6.Fill(dt6);
            for (int dd = 0; dd < dt6.Rows.Count; dd++)
            {
                PurchTax.Add(new SelectListItem { Value = dt6.Rows[dd]["COLUMN02"].ToString(), Text = dt6.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval1 = Convert.ToString(edit[0].COLUMN61);
            ViewBag.dvalm = Convert.ToString(edit[0].COLUMN64);
            ViewData["PurchTax"] = new SelectList(PurchTax, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN61));
            List<SelectListItem> RawMaterial = new List<SelectListItem>();
            SqlCommand cmdr = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dar = new SqlDataAdapter(cmdr);
            DataTable dtr = new DataTable();
            dar.Fill(dtr);
            for (int dd = 0; dd < dtr.Rows.Count; dd++)
            {
                RawMaterial.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dvalraw = Convert.ToString(edit[0].COLUMN67);
            ViewData["RawMaterial"] = new SelectList(RawMaterial, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN67));
            MATABLE007 all = new MATABLE007();
            if (t1.Rows.Count > 0)
                Session["price"] = t1.Rows[0]["COLUMN04"].ToString();
            else Session["price"] = 0;
            all.COLUMN04 = Convert.ToString(edit[0].COLUMN04);
            all.COLUMN41 = Convert.ToString(edit[0].COLUMN41);
            all.COLUMN06 = Convert.ToString(edit[0].COLUMN06);
            if (edit[0].COLUMN56 == true) ViewBag.chk1 = "checked";
            if (edit[0].COLUMN47 == true) ViewBag.chk2 = "checked";
            if (edit[0].column65 == true) ViewBag.chk3 = "checked";
            if (edit[0].COLUMN48 == true) ViewBag.chk4 = "checked";
            if (edit[0].COLUMN49 == true) ViewBag.chk5 = "checked";
            if (edit[0].COLUMN66 == true) ViewBag.chk6 = "checked";
            all.COLUMN57 = Convert.ToString(edit[0].COLUMN57);
            all.COLUMN17 = edit[0].COLUMN17;
            all.COLUMN50 = Convert.ToString(edit[0].COLUMN50);
            all.COLUMN51 = edit[0].COLUMN51;

            var sql = "select COLUMN02,COLUMN03,COLUMN04 from FITABLE013 where COLUMNA03=" + ac + " and COLUMN02 in(" + edit[0].COLUMN02 + ") and isnull(COLUMNA13,0)=0";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(sql);
            ViewBag.itemsdata = books;
            return View(all);
        }

//EMPHCS1781 Retail Creation of Customer and Item Master by gnaneshwar on 9/7/2016
        [HttpPost]
        public ActionResult Edit(FormCollection col)
        {
            try
            {
                int editid = Convert.ToInt32(Session["editid"]);
                int OutParam = 0;
                string OrderType = "1261";
                string Date = DateTime.Now.ToString();

                string OpUnit = Convert.ToString(Session["OPUnit1"]);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update", TransNo = ""; int l = 0;
                //SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE007 ", con);
                //SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                //DataTable dtt = new DataTable();
                //daa.Fill(dtt);
                //string HCol2 = dtt.Rows[0][0].ToString();
                //HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                con.Open();
                SqlCommand Cmd = new SqlCommand("usp_MAS_BL_ItemMaster", con);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", editid);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1261");
                TransNo = GetTransactionNo(1261);
                Cmd.Parameters.AddWithValue("@COLUMN41", col["COLUMN41"]);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["COLUMN04"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["COLUMN05"]);
                Cmd.Parameters.AddWithValue("@COLUMN06", col["COLUMN06"]);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["COLUMN08"]);
                Cmd.Parameters.AddWithValue("@COLUMN45", col["COLUMN45"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", col["COLUMN10"]);
                Cmd.Parameters.AddWithValue("@COLUMN12", col["COLUMN12"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["COLUMN11"]);
                Cmd.Parameters.AddWithValue("@COLUMN46", col["COLUMN46"]);
                //Cmd.Parameters.AddWithValue("@COLUMN47", col["COLUMN47"]);
                var inactive = Convert.ToString(col["COLUMN47"]);
                if (inactive == "on" || inactive == "True" || inactive == "1") inactive = "1";
                else inactive = "0";
                Cmd.Parameters.AddWithValue("@COLUMN47", inactive);
                var QOH = Convert.ToString(col["COLUMN48"]);
                if (QOH == "on" || QOH == "True" || QOH == "1") QOH = "1";
                Cmd.Parameters.AddWithValue("@COLUMN48", QOH);
                var multiunit = Convert.ToString(col["COLUMN65"]);
                if (multiunit == "on" || multiunit == "True" || multiunit == "1") multiunit = "1";
                Cmd.Parameters.AddWithValue("@COLUMN65", multiunit);
                var poitem = Convert.ToString(col["COLUMN56"]);
                if (poitem == "on" || poitem == "True" || poitem == "1") poitem = "1";
                Cmd.Parameters.AddWithValue("@COLUMN56", poitem);
                var soitem = Convert.ToString(col["COLUMN49"]);
                if (soitem == "on" || soitem == "True" || soitem == "1") soitem = "1";
                Cmd.Parameters.AddWithValue("@COLUMN49", soitem);
                var lot = Convert.ToString(col["COLUMN66"]);
                if (lot == "on" || lot == "True" || lot == "1") lot = "1";
                Cmd.Parameters.AddWithValue("@COLUMN66", lot);
                var ouselected = Convert.ToString(col["COLUMN37"]);
                if (ouselected == "" || ouselected == "0") ouselected = null;
                Cmd.Parameters.AddWithValue("@COLUMN63", col["COLUMN63"]);
                //Cmd.Parameters.AddWithValue("@COLUMN48", col["COLUMN48"]);
                Cmd.Parameters.AddWithValue("@COLUMN40", col["COLUMN40"]);
                Cmd.Parameters.AddWithValue("@COLUMN37", ouselected);
                //Cmd.Parameters.AddWithValue("@COLUMN56", col["COLUMN56"]);
                Cmd.Parameters.AddWithValue("@COLUMN57", col["COLUMN57"]);
                Cmd.Parameters.AddWithValue("@COLUMN59", col["COLUMN59"]);

                Cmd.Parameters.AddWithValue("@COLUMN61", col["COLUMN61"]);
                Cmd.Parameters.AddWithValue("@COLUMN17", col["COLUMN17"]);
                //Cmd.Parameters.AddWithValue("@COLUMN49", col["COLUMN49"]);
                Cmd.Parameters.AddWithValue("@COLUMN50", col["COLUMN50"]);

                Cmd.Parameters.AddWithValue("@COLUMN51", col["COLUMN51"]);
                Cmd.Parameters.AddWithValue("@COLUMN52", col["COLUMN52"]);
                Cmd.Parameters.AddWithValue("@COLUMN54", col["COLUMN54"]);
                Cmd.Parameters.AddWithValue("@COLUMN64", col["COLUMN64"]);
                //Cmd.Parameters.AddWithValue("@COLUMN66", col["COLUMN66"]);
                Cmd.Parameters.AddWithValue("@COLUMN67", col["COLUMN67"]);

                Cmd.Parameters.AddWithValue("@COLUMNA02", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "MATABLE007");
                Cmd.Parameters.Add("@ReturnValue", "");
                // Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                //OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                con.Close();

                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);

                var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1261).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();
                if (r > 0)
                {

                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 2).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //Line
                var ItemEditid = Convert.ToString(editid);
                if (Convert.ToString(Session["CustomerPricing"]) == "1" || Convert.ToString(Session["CustomerPricing"]) == "True")
                    Pricinggridsave(ItemEditid, ouselected, AOwner, Date, insert);
                if (ItemEditid != "" && ItemEditid != null)
                    PurcingItemEditSave(ItemEditid);
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemSOMATABLE007"] = "";
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemPOMATABLE007"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1261", "110008817", "Update"); }
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Index", "ItemMaster", new { idi = idi, FormName = custform });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }



        [HttpGet]
        public ActionResult Detailes(string ide)
        {

            Session["editid"] = ide;
            List<SelectListItem> Country = new List<SelectListItem>();
            List<SelectListItem> Country1 = new List<SelectListItem>();
            List<SelectListItem> Country2 = new List<SelectListItem>();
            List<SelectListItem> Country3 = new List<SelectListItem>();
            List<SelectListItem> Country4 = new List<SelectListItem>();
            List<SelectListItem> Country5 = new List<SelectListItem>();
            List<SelectListItem> Country6 = new List<SelectListItem>();
            var ac = Convert.ToInt32(Session["AcOwner"]);
            int? op = null;
            string opstatus = Convert.ToString(Session["OPUnitstatus"]);
            if (opstatus == null || opstatus == "") op = null;
            else op = Convert.ToInt32(Session["OPUnit"]);
            var edit = activity.MATABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMNA02 == op || a.COLUMNA02 == null) && a.COLUMN02 == ide).ToList();
            var sql2 = "select COLUMN02,COLUMN03,COLUMN04 from FITABLE013 where COLUMNA03=" + ac + " and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) and isnull(COLUMNA13,0)=0";
            SqlCommand cm = new SqlCommand(sql2, con);
            SqlDataAdapter d = new SqlDataAdapter(cm);
            DataTable t1 = new DataTable();
            d.Fill(t1);
            Session["DDDynamicItems"] = "";
            var Vendor = activity.SATABLE001.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMN22 == "22285" || a.COLUMN22 == "" || a.COLUMN22 == "NULL")).ToList();
            for (int dd = 0; dd < Vendor.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = Vendor[dd].COLUMN02.ToString(), Text = Vendor[dd].COLUMN05 });
            }
            ViewBag.dval14 = Convert.ToString(edit[0].COLUMN08);
            ViewBag.Vendor = new SelectList(Country, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN08));
            var operatingunit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
            for (int dd = 0; dd < operatingunit.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = operatingunit[dd].COLUMN02.ToString(), Text = operatingunit[dd].COLUMN03 });
            }
            ViewBag.dval13 = Convert.ToString(edit[0].COLUMN37);
            ViewBag.OPUnit = new SelectList(Country2, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN37));
            var Department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
            for (int dd = 0; dd < Department.Count; dd++)
            {
                Country3.Add(new SelectListItem { Value = Department[dd].COLUMN02.ToString(), Text = Department[dd].COLUMN04 });
            }
            ViewBag.dval12 = Convert.ToString(edit[0].COLUMN40);
            ViewBag.Department = new SelectList(Country3, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN40));
            var UOM = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11119).ToList();
            for (int dd = 0; dd < UOM.Count; dd++)
            {
                Country4.Add(new SelectListItem { Value = UOM[dd].COLUMN02.ToString(), Text = UOM[dd].COLUMN04 });
            }
            ViewBag.dval11 = Convert.ToString(edit[0].COLUMN63);
            ViewBag.UOM = new SelectList(Country4, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN63));
            var Transactions = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11142).ToList();
            if (Transactions.Count > 0)
            {
                for (int dd = 0; dd < Transactions.Count; dd++)
                {
                    Country5.Add(new SelectListItem { Value = Transactions[dd].COLUMN02.ToString(), Text = Transactions[dd].COLUMN04 });
                }
                ViewBag.dval10 = Convert.ToString(edit[0].COLUMN07);
                ViewBag.Transactions = new SelectList(Country5, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN07));
            }
            var Pricetype = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11128).ToList();
            if (Pricetype.Count > 0)
            {
                for (int dd = 0; dd < Pricetype.Count; dd++)
                {
                    Country6.Add(new SelectListItem { Value = Pricetype[dd].COLUMN02.ToString(), Text = Pricetype[dd].COLUMN04 });
                }
                ViewBag.dval9 = t1.Rows[0]["COLUMN03"].ToString();
                ViewBag.Pricetype = new SelectList(Country6, "Value", "Text", selectedValue: t1.Rows[0]["COLUMN03"].ToString());
            }

            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval8 = Convert.ToString(edit[0].COLUMN10);
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN10));
            List<SelectListItem> family = new List<SelectListItem>();
            SqlConnection cn1 = new SqlConnection(sqlcon);
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE003  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval7 = Convert.ToString(edit[0].COLUMN12);
            ViewData["family"] = new SelectList(family, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN12));
            List<SelectListItem> group = new List<SelectListItem>();
            SqlConnection cn2 = new SqlConnection(sqlcon);
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE004  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                group.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval6 = Convert.ToString(edit[0].COLUMN11);
            ViewData["group"] = new SelectList(group, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN11));
            List<SelectListItem> type = new List<SelectListItem>();
            SqlConnection cn3 = new SqlConnection(sqlcon);
            SqlCommand cmd3 = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE008   where ISNULL(COLUMNA13,0)='0' ", cn);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            for (int dd = 0; dd < dt3.Rows.Count; dd++)
            {
                type.Add(new SelectListItem { Value = dt3.Rows[dd]["COLUMN02"].ToString(), Text = dt3.Rows[dd]["COLUMN03"].ToString() });
            }
            ViewBag.dval5 = Convert.ToString(edit[0].COLUMN05);
            ViewData["type"] = new SelectList(type, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN05));
            List<SelectListItem> ExpAcc = new List<SelectListItem>();
            SqlConnection cn4 = new SqlConnection(sqlcon);
            SqlCommand cmd4 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN07=22344", cn);
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            DataTable dt4 = new DataTable();
            da4.Fill(dt4);
            for (int dd = 0; dd < dt4.Rows.Count; dd++)
            {
                ExpAcc.Add(new SelectListItem { Value = dt4.Rows[dd]["COLUMN02"].ToString(), Text = dt4.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval4 = Convert.ToString(edit[0].COLUMN59);
            ViewData["ExpAcc"] = new SelectList(ExpAcc, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN59));
            List<SelectListItem> incomeAcc = new List<SelectListItem>();
            SqlConnection cn5 = new SqlConnection(sqlcon);
            SqlCommand cmd5 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN07=22405", cn);
            SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
            DataTable dt5 = new DataTable();
            da5.Fill(dt5);
            for (int dd = 0; dd < dt5.Rows.Count; dd++)
            {
                incomeAcc.Add(new SelectListItem { Value = dt5.Rows[dd]["COLUMN02"].ToString(), Text = dt5.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval3 = Convert.ToString(edit[0].COLUMN52);
            ViewData["incomeAcc"] = new SelectList(incomeAcc, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN52));
            List<SelectListItem> Tax = new List<SelectListItem>();
            SqlConnection cn7 = new SqlConnection(sqlcon);
            SqlCommand cmd7 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)   and isnull(COLUMNA13,'False')='False' and COLUMN06='True'", cn);
            SqlDataAdapter da7 = new SqlDataAdapter(cmd7);
            DataTable dt7 = new DataTable();
            da7.Fill(dt7);
            for (int dd = 0; dd < dt7.Rows.Count; dd++)
            {
                Tax.Add(new SelectListItem { Value = dt7.Rows[dd]["COLUMN02"].ToString(), Text = dt7.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval2 = Convert.ToString(edit[0].COLUMN54);
            ViewData["Tax"] = new SelectList(Tax, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN54));
            List<SelectListItem> PurchTax = new List<SelectListItem>();
            SqlConnection cn6 = new SqlConnection(sqlcon);
            SqlCommand cmd6 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)   and isnull(COLUMNA13,'False')='False' and COLUMN13='True'", cn);
            SqlDataAdapter da6 = new SqlDataAdapter(cmd6);
            DataTable dt6 = new DataTable();
            da6.Fill(dt6);
            for (int dd = 0; dd < dt6.Rows.Count; dd++)
            {
                PurchTax.Add(new SelectListItem { Value = dt6.Rows[dd]["COLUMN02"].ToString(), Text = dt6.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dval1 = Convert.ToString(edit[0].COLUMN61);
            ViewBag.dvalm = Convert.ToString(edit[0].COLUMN64);
            ViewData["PurchTax"] = new SelectList(PurchTax, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN61));
            List<SelectListItem> RawMaterial = new List<SelectListItem>();
            SqlCommand cmdr = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dar = new SqlDataAdapter(cmdr);
            DataTable dtr = new DataTable();
            dar.Fill(dtr);
            for (int dd = 0; dd < dtr.Rows.Count; dd++)
            {
                RawMaterial.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewBag.dvalraw = Convert.ToString(edit[0].COLUMN67);
            ViewData["RawMaterial"] = new SelectList(RawMaterial, "Value", "Text", selectedValue: Convert.ToString(edit[0].COLUMN67));
            MATABLE007 all = new MATABLE007();
            Session["price"] = t1.Rows[0]["COLUMN04"].ToString();
            all.COLUMN04 = Convert.ToString(edit[0].COLUMN04);
            all.COLUMN41 = Convert.ToString(edit[0].COLUMN41);
            all.COLUMN06 = Convert.ToString(edit[0].COLUMN06);
            if (edit[0].COLUMN56 == true) ViewBag.chk1 = "checked";
            if (edit[0].COLUMN47 == true) ViewBag.chk2 = "checked";
            if (edit[0].column65 == true) ViewBag.chk3 = "checked";
            if (edit[0].COLUMN48 == true) ViewBag.chk4 = "checked";
            if (edit[0].COLUMN49 == true) ViewBag.chk5 = "checked";
            if (edit[0].COLUMN66 == true) ViewBag.chk6 = "checked";
            all.COLUMN57 = Convert.ToString(edit[0].COLUMN57);
            all.COLUMN17 = edit[0].COLUMN17;
            all.COLUMN50 = Convert.ToString(edit[0].COLUMN50);
            all.COLUMN51 = edit[0].COLUMN51;

            var sql = "select COLUMN02,COLUMN03,COLUMN04 from FITABLE013 where COLUMNA03=" + ac + " and COLUMN02 in(" + edit[0].COLUMN02 + ") and isnull(COLUMNA13,0)=0";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(sql);
            ViewBag.itemsdata = books;
            return View(all);
        }



        public ActionResult Delete()
        {
            try
            {
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                con.Open();
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from MATABLE007 where COLUMN02 =" + HCol2 + "", con);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                string Date = DateTime.Now.ToString();

                SqlCommand Cmd = new SqlCommand("usp_MAS_BL_ItemMaster", con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OPunit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Convert.ToInt32(Session["eid"]));
                Cmd.Parameters.AddWithValue("@COLUMNA12", 0);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "MATABLE007");
                Cmd.Parameters.Add("@ReturnValue", "");
                // Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                if (r > 0)
                {
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 4).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }

                con.Close();
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemSOMATABLE007"] = "";
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemPOMATABLE007"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1261", "110008817", "Update"); }
                return RedirectToAction("Index", "ItemMaster", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        [HttpPost]
        public ActionResult InsertOnflySave(FormCollection col)
        {
            try
            {

                int OutParam = 0;
                string OrderType = "1261";
                string Date = DateTime.Now.ToString();

                var OpUnit = DBNull.Value;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE007 ", con);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_MAS_BL_ItemMaster", con);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1261");
                TransNo = GetTransactionNo(1261);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["COLUMN05"]);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["COLUMN04"]);
                Cmd.Parameters.AddWithValue("@COLUMN63", col["COLUMN63"]);
                Cmd.Parameters.AddWithValue("@COLUMN06", col["COLUMN06"]);
                Cmd.Parameters.AddWithValue("@COLUMN50", col["COLUMN50"]);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["COLUMN08"]);
                Cmd.Parameters.AddWithValue("@COLUMN09", col["COLUMN09"]);
                Cmd.Parameters.AddWithValue("@COLUMN61", col["COLUMN61"]);
                Cmd.Parameters.AddWithValue("@COLUMN54", col["COLUMN54"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", col["COLUMN10"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["COLUMN11"]);
                Cmd.Parameters.AddWithValue("@COLUMN12", col["COLUMN12"]);
                Cmd.Parameters.AddWithValue("@COLUMN46", col["COLUMN46"]);
                Cmd.Parameters.AddWithValue("@COLUMN75", col["COLUMN75"]);
                Cmd.Parameters.AddWithValue("@COLUMN41", TransNo);
                string Income = "1052";
                string Purchase = "1133";
                string Expense = "1056";
                if (col["COLUMN05"] == "ITTY001")
                    Cmd.Parameters.AddWithValue("@COLUMN48", "1");
                else
                {
                    Cmd.Parameters.AddWithValue("@COLUMN48", "0");
                    Income = "1053"; Purchase = "1133"; Expense = "1078";
                }
                Cmd.Parameters.AddWithValue("@COLUMN52", Income);
                Cmd.Parameters.AddWithValue("@COLUMN74", Purchase);
                Cmd.Parameters.AddWithValue("@COLUMN59", Expense);

                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "MATABLE007");
                Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                int r = Cmd.ExecuteNonQuery();
                OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                con.Close();

                SqlCommand cmddl = new SqlCommand("select  max(COLUMN02) from MATABLE007 where COLUMN01=" + OutParam + "", con);
                SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                DataTable dttl = new DataTable();
                daal.Fill(dttl);
                string LCol2 = dttl.Rows[0][0].ToString();


                SqlCommand CmdPL = new SqlCommand("usp_MAS_BL_ItemMaster", con);
                CmdPL.CommandType = CommandType.StoredProcedure;
                CmdPL.Parameters.AddWithValue("@COLUMN02", OutParam);
                CmdPL.Parameters.AddWithValue("@COLUMN03", null);
                CmdPL.Parameters.AddWithValue("@COLUMN04", 0);
                CmdPL.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                CmdPL.Parameters.AddWithValue("@COLUMNA03", AOwner);
                CmdPL.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdPL.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdPL.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdPL.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdPL.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdPL.Parameters.AddWithValue("@Direction", insert);
                CmdPL.Parameters.AddWithValue("@TabelName", "FITABLE013");
                CmdPL.Parameters.AddWithValue("@ReturnValue", "");
                con.Open();
                int lp = CmdPL.ExecuteNonQuery();
                con.Close();

                SqlCommand cmddp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE024", con);
                SqlDataAdapter daap = new SqlDataAdapter(cmddp);
                DataTable dttp = new DataTable();
                daap.Fill(dttp);
                string HCol2p = dttp.Rows[0][0].ToString();
                if (HCol2p == "" || HCol2p == null) HCol2p = "999";
                HCol2p = (Convert.ToInt32(HCol2p) + 1).ToString();
                string Price = col["PCOLUMN04"].ToString();
                string MRP = col["PCOLUMN05"].ToString();
                if (Price == "")
                    Price = "0";
                if (MRP == "")
                    MRP = "0";


                SqlCommand Cmdl = new SqlCommand("usp_MAS_BL_PriceDetails", con);
                Cmdl.CommandType = CommandType.StoredProcedure;
                Cmdl.Parameters.AddWithValue("@COLUMN02", HCol2p);
                Cmdl.Parameters.AddWithValue("@COLUMN03", OpUnit);
                Cmdl.Parameters.AddWithValue("@COLUMN04", Price);
                Cmdl.Parameters.AddWithValue("@COLUMN05", MRP);
                Cmdl.Parameters.AddWithValue("@COLUMN06", "Purchase");
                Cmdl.Parameters.AddWithValue("@COLUMN07", LCol2);
                Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmdl.Parameters.AddWithValue("@Direction", insert);
                Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE024");
                Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                con.Open();
                int l2 = Cmdl.ExecuteNonQuery();
                con.Close();


                SqlCommand cmdds = new SqlCommand("select  Max(isnull(COLUMN02,999)) from MATABLE024", con);
                SqlDataAdapter daas = new SqlDataAdapter(cmdds);
                DataTable dtts = new DataTable();
                daas.Fill(dtts);
                string HCol2s = dttp.Rows[0][0].ToString();
                if (HCol2s == "" || HCol2s == null) HCol2s = "999";
                HCol2s = (Convert.ToInt32(HCol2s) + 1).ToString();
                string sType = col["COLUMN54"].ToString();
                string sPrice = col["SCOLUMN04"].ToString();
                string sMRP = col["SCOLUMN05"].ToString();
                if (sPrice == "")
                    sPrice = "0";
                if (sMRP == "")
                    sMRP = "0";


                SqlCommand CmdS = new SqlCommand("usp_MAS_BL_PriceDetails", con);
                CmdS.CommandType = CommandType.StoredProcedure;
                CmdS.Parameters.AddWithValue("@COLUMN02", HCol2s);
                CmdS.Parameters.AddWithValue("@COLUMN03", OpUnit);
                CmdS.Parameters.AddWithValue("@COLUMN04", sPrice);
                CmdS.Parameters.AddWithValue("@COLUMN05", sMRP);
                CmdS.Parameters.AddWithValue("@COLUMN06", "Sales");
                CmdS.Parameters.AddWithValue("@COLUMN07", LCol2);
                CmdS.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                CmdS.Parameters.AddWithValue("@COLUMNA03", AOwner);
                CmdS.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdS.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdS.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdS.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdS.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdS.Parameters.AddWithValue("@Direction", insert);
                CmdS.Parameters.AddWithValue("@TabelName", "MATABLE024");
                CmdS.Parameters.AddWithValue("@ReturnValue", "");
                con.Open();
                int l1 = CmdS.ExecuteNonQuery();
                con.Close();

                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);

                var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1261).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();
                if (r > 0)
                {
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN11 == custfname && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //Line


                var idi = "";
                //var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemSOMATABLE007"] = "";
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + "ItemPOMATABLE007"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1261", "110008817", "Update"); }
                var desc = col["COLUMN50"].ToString();
                var Sprice = col["SCOLUMN04"].ToString();
                var Pprice = col["PCOLUMN04"].ToString();
                //EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                var uom = col["COLUMN63"].ToString();

                List<SelectListItem> Item = new List<SelectListItem>();
                cmdd = new SqlCommand("select COLUMN02,COLUMN04 COLUMN03,COLUMN01 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' and COLUMN04!=''", con);
                daa = new SqlDataAdapter(cmdd);
                dtt = new DataTable();
                daa.Fill(dtt); var ItemId = "0";
                Item.Add(new SelectListItem { Value = "0", Text = "" });
                for (int dd = 0; dd < dtt.Rows.Count; dd++)
                {
                    if (OutParam == Convert.ToInt32(dtt.Rows[dd]["COLUMN01"].ToString()))
                    {
                        ItemId = dtt.Rows[dd]["COLUMN02"].ToString();
                        Item.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN03"].ToString(), Selected = true });
                    }
                    else
                        Item.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN03"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                return Json(new { item = Item, ItemId = ItemId, sType = sType, sPrice = sPrice, Pprice = Pprice, uom = uom, desc = desc, FormName = "Item" }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index", "ItemMaster", new { idi = idi, FormName = custform });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult NameCheckExist(string ItemName, string Upc, string DupCheck)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select count(column01) from matable007 where isnull(COLUMNA13,0)=0 and COLUMNA03='" + Session["AcOwner"] + "' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) " + DupCheck + "", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string ItemName1 = "0";
                if (dt.Rows.Count > 0)
                {
                    if (ItemName != null)
                        ItemName1 = dt.Rows[0][0].ToString();
                }
                return Json(new { ItemName = ItemName1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
    }
}

