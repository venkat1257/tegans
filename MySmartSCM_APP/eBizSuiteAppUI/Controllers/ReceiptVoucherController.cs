﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class ReceiptVoucherController : Controller
    {
        //
        // GET: /ReceiptVoucher/
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        [HttpGet]
        public ActionResult Create(string Reconcileid, string BankId, string Amt, string Memo)
        {
            var ac = Convert.ToInt32(Session["AcOwner"]);
            List<SelectListItem> customer = new List<SelectListItem>();
            SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002 where COLUMNA03=" + Session["AcOwner"] + " and (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) and isnull(COLUMNA13,0)=0 ", cn);
            SqlDataAdapter dac = new SqlDataAdapter(cmdc);
            DataTable dtc = new DataTable();
            dac.Fill(dtc);
            for (int dd = 0; dd < dtc.Rows.Count; dd++)
            {
                customer.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
            }
            customer = customer.OrderBy(x => x.Text).ToList();
            ViewData["Payee"] = new SelectList(customer, "Value", "Text");
            List<SelectListItem> Bank = new List<SelectListItem>();
            SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001 where COLUMN07=22266 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0", cn);
            DataTable dtdata = new DataTable();
            cmddl.Fill(dtdata);
            for (int dd = 0; dd < dtdata.Rows.Count; dd++)
            {
                Bank.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
            }
            Bank = Bank.OrderBy(x => x.Text).ToList();
            ViewData["Bank"] = new SelectList(Bank, "Value", "Text", selectedValue: BankId);
            List<SelectListItem> OPUnit = new List<SelectListItem>();
            SqlDataAdapter cmdd = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from CONTABLE007 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
            DataTable dtd = new DataTable();
            cmdd.Fill(dtd);
            for (int dd = 0; dd < dtd.Rows.Count; dd++)
            {
                OPUnit.Add(new SelectListItem { Value = dtd.Rows[dd]["COLUMN02"].ToString(), Text = dtd.Rows[dd]["COLUMN04"].ToString() });
            }
            OPUnit = OPUnit.OrderBy(x => x.Text).ToList();
            ViewData["OPUnit"] = new SelectList(OPUnit, "Value", "Text", Session["OPUnit1"]);
            List<SelectListItem> COA = new List<SelectListItem>();
            SqlDataAdapter cmdca = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0", cn);
            DataTable dtca = new DataTable();
            cmdca.Fill(dtca);
            for (int dd = 0; dd < dtca.Rows.Count; dd++)
            {
                COA.Add(new SelectListItem { Value = dtca.Rows[dd]["COLUMN02"].ToString(), Text = dtca.Rows[dd]["COLUMN04"].ToString() });
            }
            COA = COA.OrderBy(x => x.Text).ToList();
            ViewData["Account"] = new SelectList(COA, "Value", "Text", selectedValue: 3000);
            var TransNo=GetTransactionNo(1526);
            Dateformat();
            ViewBag.TransNo = TransNo;
            ViewBag.Reconcileid = Reconcileid;
            ViewBag.Amt = Amt;
            ViewBag.Memo = Memo;
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection col)
        {
            try
            {
                int OutParam = 0;
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (col["OPUnit"] != "") Oper = Convert.ToInt32(col["OPUnit"]);
                string insert = "Insert", TransNo = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from FITABLE023 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_FI_BL_ReceiptVoucher", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", 1526);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Payment"]);
                var Tdate = col["Date"]; String Amount = col["Amount"];
                if (Tdate != "") Tdate = DateTime.ParseExact(Tdate, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                else Tdate = Date;
                if (Amount == "") Amount = "0";
                Cmd.Parameters.AddWithValue("@COLUMN05", Tdate);
                Cmd.Parameters.AddWithValue("@COLUMN20", "22335");
                Cmd.Parameters.AddWithValue("@COLUMN08", col["Payee"]);
                Cmd.Parameters.AddWithValue("@COLUMN09", col["Bank"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", Amount);
                Cmd.Parameters.AddWithValue("@COLUMN16", Oper);
                Cmd.Parameters.AddWithValue("@COLUMNA02", Oper);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Convert.ToInt32(Session["eid"]));
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "FITABLE023");
                Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                cn.Close();
                if (OutParam > 0)
                {
                    SqlCommand cmdA = new SqlCommand("select  Max(isnull(COLUMN02,999)) from FITABLE024 ", cn);
                    SqlDataAdapter daaA = new SqlDataAdapter(cmdA);
                    DataTable dttA = new DataTable();
                    daaA.Fill(dttA);
                    string HCol2A = dttA.Rows[0][0].ToString();
                    if (HCol2A == "") HCol2A = "999";
                    HCol2A = (Convert.ToInt32(HCol2A) + 1).ToString();
                    SqlCommand CmdA = new SqlCommand("usp_FI_BL_ReceiptVoucher", cn);
                    CmdA.CommandType = CommandType.StoredProcedure;
                    CmdA.Parameters.AddWithValue("@COLUMN02", HCol2A);
                    CmdA.Parameters.AddWithValue("@COLUMN03", col["Account"]);
                    CmdA.Parameters.AddWithValue("@COLUMN04", col["Memo"]);
                    CmdA.Parameters.AddWithValue("@COLUMN05", Amount);
                    CmdA.Parameters.AddWithValue("@COLUMN15", 1000);
                    CmdA.Parameters.AddWithValue("@COLUMN06", 0);
                    CmdA.Parameters.AddWithValue("@COLUMN16", Amount);
                    CmdA.Parameters.AddWithValue("@COLUMN17", 0);
                    CmdA.Parameters.AddWithValue("@COLUMN18", Amount);
                    CmdA.Parameters.AddWithValue("@COLUMN09", OutParam);
                    CmdA.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    CmdA.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    CmdA.Parameters.AddWithValue("@COLUMNA06", Date);
                    CmdA.Parameters.AddWithValue("@COLUMNA07", Date);
                    CmdA.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    CmdA.Parameters.AddWithValue("@COLUMNA12", 1);
                    CmdA.Parameters.AddWithValue("@COLUMNA13", 0);
                    CmdA.Parameters.AddWithValue("@Direction", insert);
                    CmdA.Parameters.AddWithValue("@TabelName", "FITABLE024");
                    CmdA.Parameters.AddWithValue("@ReturnValue", "");
                    cn.Open();
                    int r1 = CmdA.ExecuteNonQuery();
                    if (r > 0)
                    {
                        eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();

                        if (Oper == null || Oper == 0)
                            Oper = Convert.ToInt32(Session["OPUnit1"]);
                        if (Oper == 0)
                            Oper = null;
                        int? acOW = Convert.ToInt32(Session["AcOwner"]);

                        var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1526).FirstOrDefault().COLUMN04.ToString();
                        var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();

                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();
                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                            if (Oper == null)
                                listTM = act1.MYTABLE002.Where(q => q.COLUMN11 == custfname && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();

                            if (listTM == null)
                            {
                                listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(null)).FirstOrDefault();
                            }
                        }
                        if (listTM != null)
                        {
                            MYTABLE002 product = listTM;
                            if (listTM.COLUMN07 == "") listTM.COLUMN07 = "1";
                            if (listTM.COLUMN09 == "")
                                product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN07)).ToString();
                            else
                                product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                            act1.SaveChanges();
                        }

                        int? acID = (int?)Session["AcOwner"];
                        string OPUnit = Session["OPUnit"].ToString();
                        int OPUnitStatus = 1;
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                        {
                            OPUnit = OPUnit.Replace(",null", ",");
                            OPUnit = OPUnit.TrimEnd(',');
                            OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                        }
                        string Reconcilid = col["Reconcileid"];
                        var BankId = col["Bank"].ToString();
                        SqlCommand cmds = new SqlCommand("[usp_FIN_REPORT_BANKRECONCILATION]", cn);
                        cmds.CommandType = CommandType.StoredProcedure;
                        cmds.Parameters.AddWithValue("@Reconcilid", Reconcilid);
                        cmds.Parameters.AddWithValue("@Bank", BankId);
                        cmds.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmds.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmds.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter das = new SqlDataAdapter(cmds);
                        DataTable dt = new DataTable();
                        das.Fill(dt);
                        var sysresult = new List<dynamic>();
                        var sysdepositlst = new List<dynamic>();
                        var syswithdrawlst = new List<dynamic>();
                        List<WebGridColumn> syscols = new List<WebGridColumn>();
                        List<WebGridColumn> sysdepcols = new List<WebGridColumn>();
                        List<WebGridColumn> syswithcols = new List<WebGridColumn>();
                        foreach (DataRow row in dt.Rows)
                        {
                            var obj = (IDictionary<string, object>)new ExpandoObject();
                            var deposit = (IDictionary<string, object>)new ExpandoObject();
                            var withdraw = (IDictionary<string, object>)new ExpandoObject();
                            foreach (DataColumn col1 in dt.Columns)
                            {
                                obj.Add(col1.ColumnName, row[col1.ColumnName]);
                                if (row["BankType"].ToString() == "Receivable") deposit.Add(col1.ColumnName, row[col1.ColumnName]);
                                else if (row["BankType"].ToString() == "Payable") withdraw.Add(col1.ColumnName, row[col1.ColumnName]);
                            }
                            sysresult.Add(obj);
                            if (row["BankType"].ToString() == "Receivable") sysdepositlst.Add(deposit);
                            else if (row["BankType"].ToString() == "Payable") syswithdrawlst.Add(withdraw);
                        }
                        var gridsys = new WebGrid(sysresult, canPage: false, canSort: false);
                        var gridsysdep = new WebGrid(sysdepositlst, canPage: false, canSort: false);
                        sysdepcols.Add(gridsys.Column("Checkrow", "", format: (item) =>
                        {
                            if (item.Reconcilid == 23133)
                            {
                                return new HtmlString(string.Format("<input  id='checksysdepRow' name='chkd' TYPE='checkbox' disabled='disabled'/><input id='bankdepid' type='text' val='" + item.TransID + "' style='display:none'/><input id='bankdeptno'  type='text' val='" + item.Trans + "' style='display:none'/><input id='sysdepamt' type='text' val='0' style='display:none'/><input id='sysdepbankid' type='text' val='" + item.BankID + "' style='display:none'/>"));
                            }
                            else
                            { return new HtmlString(string.Format("<input  id='checksysdepRow' name='chkd' TYPE='checkbox' /><input id='bankdepid' type='text' val='" + item.TransID + "' style='display:none'/><input id='bankdeptno' type='text' val='" + item.Trans + "' style='display:none'/><input id='sysdepamt' type='text' val='" + item.DepositAmount + "' style='display:none'/><input id='sysdepbankid' type='text' val='" + item.BankID + "' style='display:none'/>")); }
                        }));
                        sysdepcols.Add(gridsys.Column("Date", "Date"));
                        sysdepcols.Add(gridsys.Column("Trans", "Trans#"));
                        sysdepcols.Add(gridsys.Column("Type", "TransType"));
                        sysdepcols.Add(gridsys.Column("Memo", "Memo"));
                        sysdepcols.Add(gridsys.Column("DepositAmount", "Amount"));
                        var htmlstringsysdep = gridsysdep.GetHtml(mode: WebGridPagerModes.All,
                                         htmlAttributes: new { id = "grdsysdepData" },
                                         columns: sysdepcols);
                        var gridsyswith = new WebGrid(syswithdrawlst, canPage: false, canSort: false);
                        syswithcols.Add(gridsys.Column("Checkrow", "", format: (item) =>
                        {
                            if (item.Reconcilid == 23133)
                            {
                                return new HtmlString(string.Format("<input  id='checksyswithRow'   name='chkw' TYPE='checkbox' disabled='disabled'/><input id='syswithid' type='text' val='" + item.TransID + "' style='display:none'/><input id='syswithtno' type='text' val='" + item.Trans + "' style='display:none'/><input id='syswithamt' type='text' val='0' style='display:none'/><input id='syswithbankid' type='text' val='" + item.BankID + "' style='display:none'/>"));
                            }
                            else
                            { return new HtmlString(string.Format("<input  id='checksyswithRow'   name='chkw' TYPE='checkbox'  /><input id='syswithid' type='text' val='" + item.TransID + "' style='display:none'/><input id='syswithtno' type='text' val='" + item.Trans + "' style='display:none'/><input id='syswithamt' type='text' val='" + item.PaymentAmount + "' style='display:none'/><input id='syswithbankid' type='text' val='" + item.BankID + "' style='display:none'/>")); }
                        }));
                        syswithcols.Add(gridsys.Column("Date", "Date"));
                        syswithcols.Add(gridsys.Column("Trans", "Trans#"));
                        syswithcols.Add(gridsys.Column("Type", "TransType"));
                        syswithcols.Add(gridsys.Column("Memo", "Memo"));
                        syswithcols.Add(gridsys.Column("PaymentAmount", "Amount"));
                        var htmlstringsyswith = gridsyswith.GetHtml(mode: WebGridPagerModes.All,
                                         htmlAttributes: new { id = "grdsyswithData" },
                                         columns: syswithcols);
                        return Json(new { Data3 = htmlstringsysdep.ToHtmlString(), Data4 = htmlstringsyswith.ToHtmlString() }, JsonRequestBehavior.AllowGet);


                        var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1526 && q.COLUMN05 == 1).FirstOrDefault();
                        var msg = string.Empty;
                        if (msgMaster != null)
                        {
                            msg = msgMaster.COLUMN03;
                        }
                        else
                            msg = "Successfully Created..... ";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                }
                return RedirectToAction("Info", "BankReconsilation", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "BankReconsilation", new { FormName = Session["FormName"] });
            }
        }

        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = db.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono = "", TransNo = "";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (Prefix != null)
                {
                    fino = Prefix;
                    pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM FITABLE020  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM FITABLE020  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                }
                else
                {
                    fino = "RV";
                    pacmd = new SqlCommand("Select substring(COLUMN04,3,len(COLUMN04)) PO FROM FITABLE023  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM FITABLE023  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo) + Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(CurrentNo) + Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }
        public void Dateformat()
        {
            try
            {
                Session["FormName"] = db.CONTABLE0010.Where(a => a.COLUMN02 == 1526).FirstOrDefault().COLUMN04;
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter dad = new SqlDataAdapter(str, cn);
                DataTable dtd = new DataTable();
                dad.Fill(dtd); string DateFormat = null;
                if (dtd.Rows.Count > 0)
                {
                    DateFormat = dtd.Rows[0][0].ToString();
                    Session["FormatDate"] = dtd.Rows[0][0].ToString();
                }
                if (dtd.Rows[0][1].ToString() != "")
                    Session["ReportDate"] = dtd.Rows[0][1].ToString();
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //return RedirectToAction("Info", "BankReconsilation", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult GetAccountAmount(string ID)
        {
            try
            {
                var amt = "";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var dropdata = "[PROC_CHECKBALANCE]";
                SqlCommand cmd = new SqlCommand(dropdata, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ACID", ID);
                cmd.Parameters.AddWithValue("@OPUNIT", Session["OPUnit"]);
                cmd.Parameters.AddWithValue("@ACOWNER", Session["AcOwner"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                    amt = dt1.Rows[0][0].ToString();
                return Json(new { Amount = amt }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "BankReconsilation", new { FormName = Session["FormName"] });
            }
        }

    }
}
