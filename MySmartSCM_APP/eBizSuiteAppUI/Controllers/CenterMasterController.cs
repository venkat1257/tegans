﻿
using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using eBizSuiteDAL.classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;

using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;


namespace eBizSuiteProduct.Controllers
{
    public class CenterMasterController : Controller
    {


        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        CenterMasterManage userManager = new CenterMasterManage();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        public ActionResult Index()
        {
            return View();
        }
        string[] theader;
        string[] theadertext;
        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
            var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
            theader = new string[Acolslist.Count];
            theadertext = new string[Acolslist.Count];
            for (int a = 0; a < Acolslist.Count; a++)
            {
                var acol = Acolslist[a].ToString();
                var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                var tblcol = columndata.COLUMN04;
                var dcol = Ccolslist[a].ToString();
                if (a == 0)
                {
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = "Action",
                        Format = (item) => new HtmlString("<a href=/CenterMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CenterMaster/Detailes/" + item[tblcol] + " >View</a>")

                    });
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol
                    });
                    Inlinecols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol,
                        Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                    });
                }
                else
                {
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol
                    });
                    Inlinecols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol,
                        Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                    });
                } theader[a] = tblcol; theadertext[a] = dcol;

            }
            ViewBag.cols = cols;
            ViewBag.Inlinecols = Inlinecols;
            ViewBag.columns = Inlinecols;
            ViewBag.itemscol = theader;
            ViewBag.theadertext = theadertext;
            var query1 = "Select * from CONTABLE011 where COLUMNA13=0  and " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(query1);
            ViewBag.itemsdata = books;
            Session["cols"] = cols;
            Session["Inlinecols"] = Inlinecols;
            var eid = (Session["OPUnit"]).ToString();
            var cen = dbContext.CONTABLE012.Where(q => eid.Contains(q.COLUMN06)).Select(q => q.COLUMN05).ToList();
            return View("Info", dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false && cen.Contains(a.COLUMN02)).ToList());
        }
        //c
        [HttpGet]
        public ActionResult Sort(string ShowInactives)
        {
            var data = Session["Data"];
            var item = Session["sortselected"];
            ViewBag.sortselected = item;
            //Session["Data"] = null;
            //Session["sortselected"] = null;
            return View("Info", data);

        }


        private List<DynaNode> GetTreeView()
        {
            List<CONTABLE003> objTreeList = new List<CONTABLE003>();
            //var ac = Convert.ToInt32(Session["AcOwner"]);
            //var cen = Convert.ToInt32(Session["cenid"]);
            //if (ac == 56567)
             objTreeList = dbContext.CONTABLE003.Where(q => q.COLUMN04 == 101).OrderBy(s => s.COLUMN06).ToList();
            //else
            //    objTreeList = dbContext.CONTABLE003.Where(q => q.COLUMN04 == cen && q.COLUMNA03==ac).OrderBy(s => s.COLUMN06).ToList();
            List<DynaNode> objList = new List<DynaNode>();
            foreach (var item in objTreeList)
            {
                DynaNode objTree = new DynaNode();
                objTree.key = item.COLUMN02.ToString();
                objTree.title = item.COLUMN05;
                objTree.isLazy = true;
                var objChild = objTreeList.FindAll(s => s.COLUMN06 == item.COLUMN02);
                if (objChild.Count > 0)
                {
                    objTree.isFolder = true;
                }
                else
                    objTree.isFolder = false;
                objTree.parentId = item.COLUMN06.ToString();
                objList.Add(objTree);
            }

            return objList;

        }
        public JsonResult GetAllNode()
        {
            List<DynaNode> objList = GetTreeView();
            return Json(objList, JsonRequestBehavior.AllowGet);
        }
        public class DynaNode
        {
            public string title { get; set; }
            public bool isFolder { get; set; }
            public bool isLazy { get; set; }
            public string key { get; set; }
            public string parentId { get; set; }
        }

        [HttpGet]
        public ActionResult Create()
        {
            eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
            // var op = Convert.ToInt32(Session["OPUnitWithNull"]);
            var ac = Convert.ToInt32(Session["AcOwner"]);
            //int acID=0;
            //if (Session["AcOwner"] != null)
            //{
            //    acID = Convert.ToInt32(Session["AcOwner"]);
            //    ViewBag.o = dbContext.CONTABLE007.Where(q=>q.COLUMNA03==acID).ToList();
            //    ViewBag.s = dbContext.CONTABLE009.Where(q=>q.COLUMNA03==acID).ToList();
            //}
            //else
            //{
            ViewBag.o = dbContext.CONTABLE007.Where(q => q.COLUMNA03 == ac).ToList();
            ViewBag.s = dbContext.CONTABLE009.Where(q => q.COLUMNA03 == ac).ToList();
            //}
            CONTABLE011 all = new CONTABLE011();
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            var col2 = dbContext.CONTABLE011.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
            all.COLUMN02 = (col2.COLUMN02 + 1);
            if (ac != 56567)
            ViewBag.accountowner = db.CONTABLE002.OrderBy(q => q.COLUMN02).Where(q => q.COLUMN06 == true && q.COLUMN02 == ac).ToList();
            else
                ViewBag.accountowner = db.CONTABLE002.OrderBy(q => q.COLUMN02).Where(q => q.COLUMN06 == true ).ToList();
            return View(all);
        }



        [HttpPost]
        public ActionResult Create(FormCollection fc, string hdnbranchid1, int Center_ID, string Name, string Description, string Operating_unit, string AccountOwner, string Subsidary, string hdnbranchid20, string Save, string SaveNew)
        {
            //Center Creation
            CONTABLE011 all = new CONTABLE011();
            all.COLUMN02 = Center_ID;
            all.COLUMN03 = Name;
            all.COLUMN04 = Description;
            all.COLUMN05 = fc["COLUMN05"];
            all.COLUMN06 = fc["COLUMN06"];
            int? AccountOwnerId = null, Subsidari = null;
            int OUnit = 0;
            if (fc["COLUMN05"] != null && fc["COLUMN05"] != "")
            {
                OUnit = Convert.ToInt32(fc["COLUMN05"]);
            }
            if (fc["COLUMN06"] != null && fc["COLUMN06"] != "")
            {
                Subsidari = Convert.ToInt32(fc["COLUMN06"]);
            }
            if (fc["COLUMN07"] != null && fc["COLUMN07"] != "")
            {
                all.COLUMN07 = fc["COLUMN07"];
                AccountOwnerId = Convert.ToInt32(fc["COLUMN07"]);
            }
            else
            {
                all.COLUMN07 = Session["AcOwner"].ToString();
                AccountOwnerId = Convert.ToInt32(Session["AcOwner"].ToString());
            }

            all.COLUMNA06 = DateTime.Now;
            all.COLUMNA07 = DateTime.Now;
            all.COLUMNA12 = true;
            all.COLUMNA13 = false;
            dbContext.CONTABLE011.Add(all);
            dbContext.SaveChanges();
            //Menu Creation
            CONTABLE003 mm = new CONTABLE003();
            List<CONTABLE003> lst = dbContext.CONTABLE003.OrderBy(q => q.COLUMN02).ToList();
            List<string> listValues = hdnbranchid1.Split(',').ToList();
            List<string> listKeys = hdnbranchid20.Split(',').ToList();

            for (int i = 0; i < listValues.Count; i++)
            {
                var k = Convert.ToInt32(listKeys[i].ToString());
                var mid = dbContext.CONTABLE003.Where(q => q.COLUMN02 == k).First();

                mm.COLUMN02 = mid.COLUMN02;
                mm.COLUMN03 = mid.COLUMN03;
                mm.COLUMN04 = Center_ID;
                mm.COLUMN05 = mid.COLUMN05;
                mm.COLUMN06 = mid.COLUMN06;
                mm.COLUMN07 = mid.COLUMN07;
                mm.COLUMN08 = "Y";
                mm.COLUMN09 = "Y";
                mm.COLUMN10 = mid.COLUMN10;
                mm.COLUMN11 = mid.COLUMN11;
                mm.COLUMN12 = mid.COLUMN12;
                mm.COLUMNA03 = AccountOwnerId;
                mm.COLUMNA14 = Subsidari.ToString();

                mm.COLUMNA06 = DateTime.Now;
                mm.COLUMNA07 = DateTime.Now;
                mm.COLUMNA12 = true;
                mm.COLUMNA13 = false;
                dbContext.CONTABLE003.Add(mm);
                dbContext.SaveChanges();
            }
            if (fc["COLUMN07"] != null && fc["COLUMN07"] != "")
            {
                //Role Creation
                int idval = 0;
                CONTABLE012 infor = new CONTABLE012();
                var col2 = dbContext.CONTABLE012.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
                var col1 = col2;
                if (col2 == null || col2 == 0)
                { col1 = 11111; infor.COLUMN02 = 5656501; }
                else
                {
                    infor.COLUMN02 = (Convert.ToInt32(col1) + 1);
                }
                infor.COLUMN03 = "Administrator";
                infor.COLUMN04 = "Administrator";
                infor.COLUMN05 = Center_ID;
                infor.COLUMN06 = OUnit.ToString();
                infor.COLUMNA06 = DateTime.Now;
                infor.COLUMNA07 = DateTime.Now;
                infor.COLUMNA12 = true;
                infor.COLUMNA13 = false;
                dbContext.CONTABLE012.Add(infor);
                dbContext.SaveChanges();
                //Emp Vs Role Creation
                CONTABLE027 infoer = new CONTABLE027();
                var coler = dbContext.CONTABLE027.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
                var coler1 = coler;
                if (coler == null || coler == 0)
                { coler1 = 50000; infoer.COLUMN02 = 50000; }
                else
                {
                    infoer.COLUMN02 = (Convert.ToInt32(coler1) + 1);
                }
                var acID = AccountOwnerId;
                var acownerdetails = dbContext.CONTABLE002.Where(a => a.COLUMN02 == acID).FirstOrDefault();
                var empid = dbContext.MATABLE010.Where(a => a.COLUMN13 == acownerdetails.COLUMN08 && a.COLUMN21 == acownerdetails.COLUMN09).FirstOrDefault().COLUMN02;
                infoer.COLUMN03 = Convert.ToInt32(empid);
                infoer.COLUMN04 = infor.COLUMN02;
                infoer.COLUMN05 = OUnit;
                infoer.COLUMNA13 = false;
                infoer.COLUMNA12 = true;
                //infoer.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                //infoer.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                infoer.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"]);
                infoer.COLUMNA06 = DateTime.Now;
                infoer.COLUMNA07 = DateTime.Now;
                dbContext.CONTABLE027.Add(infoer);
                dbContext.SaveChanges();
            }
            //var emp = infoer.COLUMN03;
            //var acID = Convert.ToInt32(fc["COLUMN07"]);
            //var acID = dbContext.MATABLE010.Where(q => q.COLUMN02 == emp).Select(q => q.COLUMNA03).FirstOrDefault();
            //var mnu = dbContext.CONTABLE003.Where(q => q.COLUMNA03 == acID).ToList();
            //var cID = dbContext.CONTABLE012.Where(q => q.COLUMN02 == infoer.COLUMN04).Select(q => q.COLUMN05).FirstOrDefault();
            //var menu = dbContext.CONTABLE003.Where(q => q.COLUMN04 == cID).ToList();
            //var acOwnr = menu.Select(q => q.COLUMNA03).FirstOrDefault();
            //if (menu != null && acOwnr == null)
            //{
            //    foreach (CONTABLE003 product in menu)
            //    {
            //        product.COLUMNA03 = acID;
            //    }
            //}
            //else if (menu != null && acOwnr != acID)
            //{
            //    foreach (CONTABLE003 product in menu)
            //    {
            //        product.COLUMNA03 = acID;
            //        dbContext.CONTABLE003.Add(product);
            //        dbContext.SaveChanges();
            //    }
            //}
            //dbContext.SaveChanges();

            if (Save != null)
            {
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
                var ttid = tid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
                return RedirectToAction("Info");
            }
            return RedirectToAction("Create");


        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var data = userManager.GetEmployeeDetail(id);
            ViewBag.accountowner = dbContext.CONTABLE002.OrderBy(q => q.COLUMN02).Where(q => q.COLUMN06 == true).ToList();
            CONTABLE011 emp = new CONTABLE011();
            emp.COLUMN02 = data.COLUMN02;
            emp.COLUMN03 = data.COLUMN03;
            emp.COLUMN04 = data.COLUMN04;
            emp.COLUMN05 = data.COLUMN05;
            emp.COLUMN06 = data.COLUMN06;

            var str = dbContext.CONTABLE003.Where(a => a.COLUMN04 == id).Select(q => q.COLUMN03).ToArray();
            var str1 = dbContext.CONTABLE003.Where(a => a.COLUMN04 == id).Select(q => q.COLUMN05).ToArray();


            var strID1 = dbContext.CONTABLE003.Where(a => str.Contains(a.COLUMN03) && a.COLUMN06 == null).Select(q => q.COLUMN02).ToArray();
            var strID = dbContext.CONTABLE003.Where(a => str1.Contains(a.COLUMN05) && a.COLUMN04 == 101).Select(q => q.COLUMN02).ToArray();


            int num = strID.Length + strID1.Length;
            string[] info = new string[num];
            for (int l = 0; l < strID1.Length; l++)
            {
                info[l] = strID1[l].ToString();
            }
            for (int l = strID1.Length; l < num; l++)
            {
                info[l] = strID[l - strID1.Length].ToString();
            }




            CONTABLE003 mm1 = new CONTABLE003();

            string nSg = "";
            foreach (string sg in info)
            {
                if (nSg == "")
                {
                    nSg = sg.ToString();
                }
                else
                    nSg = nSg + "," + sg.ToString();
            }
            ViewBag.array = nSg;



            return View("Edit", emp);


        }

        ////c
        //[HttpPost]
        //public ActionResult Create(CONTABLE011 info, string Save, string SaveNew)
        //{
        //    info.COLUMNA06 = DateTime.Now;
        //    info.COLUMNA07 = DateTime.Now;
        //    dbContext.CONTABLE011.Add(info);
        //    dbContext.SaveChanges();
        //    if (Save != null)
        //    {
        //        return RedirectToAction("Info");
        //    }
        //    return RedirectToAction("Create");
        //}




        //c
        //[HttpGet]
        //public ActionResult Edit(int id)
        //{
        //    CONTABLE011 edit = dbContext.CONTABLE011.Find(id);
        //    var col2 = edit.COLUMN02;
        //    Session["time"] = dbContext.CONTABLE011.Find(col2).COLUMNA06;
        //    return View(edit);
        //}


        //[HttpGet]
        //public ActionResult Edit(int id)
        //{
        //    var data = userManager.GetEmployeeDetail(id);
        //    CONTABLE011 emp = new CONTABLE011();
        //    emp.COLUMN02 = data.COLUMN02;
        //    emp.COLUMN03 = data.COLUMN03;
        //    emp.COLUMN04 = data.COLUMN04;
        //    emp.COLUMN05 = data.COLUMN05;
        //    emp.COLUMN06 = data.COLUMN06;

        //    var str = dbContext.CONTABLE003.Where(a => a.COLUMN04 == id).Select(q => q.COLUMN03 ).ToArray();
        //    var str1 = dbContext.CONTABLE003.Where(a => a.COLUMN04 == id).Select(q => q.COLUMN05).ToArray();


        //    var strID1 = dbContext.CONTABLE003.Where(a => str.Contains(a.COLUMN03) && a.COLUMN06 == null).Select(q => q.COLUMN02).ToArray();
        //    var strID = dbContext.CONTABLE003.Where(a => str1.Contains(a.COLUMN05) && a.COLUMN04 == 101).Select(q => q.COLUMN02).ToArray();



        //    int num = strID.Length + strID1.Length;
        //        string[] info = new string[num];
        //        for (int l = 0; l < strID1.Length; l++)
        //        {
        //            info[l] = strID1[l].ToString();
        //        }
        //        for (int l = strID1.Length; l < num; l++)
        //    {
        //        info[l] = strID[l - strID1.Length].ToString();
        //    }




        //    CONTABLE003 mm1 = new CONTABLE003();

        //    string nSg = "";
        //    foreach (string sg in info)
        //    {
        //        if (nSg == "")
        //        {
        //            nSg = sg.ToString();
        //        }
        //        else
        //            nSg = nSg + "," + sg.ToString();
        //    }
        //    ViewBag.array = nSg;



        //    return View("Edit", emp);


        //}

        //c
        //c
        //c
        [HttpPost]
        public ActionResult Edit(CONTABLE011 register, string hdnbranchid1, string hdnbranchid20)
        {
            CONTABLE011 sysRegister = new CONTABLE011();

            sysRegister.COLUMN02 = register.COLUMN02;
            sysRegister.COLUMN03 = register.COLUMN03;
            sysRegister.COLUMN04 = register.COLUMN04;
            sysRegister.COLUMN05 = register.COLUMN05;
            sysRegister.COLUMN06 = register.COLUMN06;
            sysRegister.COLUMNA07 = DateTime.Now;

            bool IsSuccess = userManager.UpdateCenterMaster(sysRegister);

            CONTABLE003 mm = new CONTABLE003();
            var lst = dbContext.CONTABLE003.Where(m => m.COLUMN04 == register.COLUMN02).FirstOrDefault();
            List<CONTABLE003> lst1 = dbContext.CONTABLE003.OrderBy(q => q.COLUMN02).ToList();

            List<string> listValues = hdnbranchid1.Split(',').ToList();
            List<string> listKeys = hdnbranchid20.Split(',').ToList();

            var strID2 = dbContext.CONTABLE003.Where(a => listValues.Contains(a.COLUMN05) && a.COLUMN06 != null).Select(q => q.COLUMN05).Distinct().ToArray();



            for (int i = 0; i < strID2.Length; i++)
            {
                string k = (strID2[i].ToString());
                var mid = dbContext.CONTABLE003.Where(q => q.COLUMN04 == register.COLUMN02).ToList();
                var objCheck = (from m in mid where m.COLUMN05 == k select m).FirstOrDefault();
                if (objCheck != null)
                {

                    lst.COLUMN03 = objCheck.COLUMN03;
                    lst.COLUMN04 = register.COLUMN02;
                    lst.COLUMN05 = strID2[i].ToString();
                    lst.COLUMN06 = objCheck.COLUMN06;
                    lst.COLUMN07 = objCheck.COLUMN07;
                    lst.COLUMN08 = "Y";
                    lst.COLUMN09 = "Y";
                    lst.COLUMN10 = objCheck.COLUMN10;
                    lst.COLUMN11 = objCheck.COLUMN11;

                    lst.COLUMNA07 = DateTime.Now;
                    //mm.COLUMNA02 = Operating_unit;
                    mm.COLUMNA14 = register.COLUMN06;
                    //dbContext.CONTABLE003.Add(mm);
                    dbContext.SaveChanges();
                }
                else
                {
                    string k1 = (strID2[i].ToString());
                    var mid1 = dbContext.CONTABLE003.Where(q => q.COLUMN05 == k1).First();

                    mm.COLUMN02 = lst1[lst1.Count - 1].COLUMN02 + i + 1;
                    mm.COLUMN03 = mid1.COLUMN03;
                    mm.COLUMN04 = register.COLUMN02;
                    mm.COLUMN05 = strID2[i].ToString();
                    mm.COLUMN06 = mid1.COLUMN06;
                    mm.COLUMN07 = mid1.COLUMN07;
                    mm.COLUMN08 = "Y";
                    mm.COLUMN09 = "Y";
                    mm.COLUMN10 = mid1.COLUMN10;
                    mm.COLUMN11 = mid1.COLUMN11;
                    mm.COLUMNA06 = DateTime.Now;
                    mm.COLUMNA07 = DateTime.Now;
                    //mm.COLUMNA02 = Operating_unit;
                    mm.COLUMNA14 = register.COLUMN06;
                    dbContext.CONTABLE003.Add(mm);
                    dbContext.SaveChanges();
                }
            }

            return RedirectToAction("Info");

        }


        public ActionResult Detailes(int id)
        {
            CONTABLE011 Details = dbContext.CONTABLE011.Find(id);
            ViewBag.accountowner = dbContext.CONTABLE002.OrderBy(q => q.COLUMN02).Where(q => q.COLUMN06 == true).ToList();



            var str = dbContext.CONTABLE003.Where(a => a.COLUMN04 == id).Select(q => q.COLUMN03).ToArray();
            var str1 = dbContext.CONTABLE003.Where(a => a.COLUMN04 == id).Select(q => q.COLUMN05).ToArray();

            int num = str1.Length;
            string[] info = new string[num]; string form = null;
            for (int l = 0; l < num; l++)
            {
                info[l] = str1[l].ToString();
                if (form == null)
                {
                    form += info[l];
                }
                else
                {
                    form += ',' + info[l];
                }
            }
            ViewBag.form = form;


            //var strID1 = dbContext.CONTABLE003.Where(a => str.Contains(a.COLUMN03) && a.COLUMN06 == null).Select(q => q.COLUMN02).ToArray();
            //var strID = dbContext.CONTABLE003.Where(a => str1.Contains(a.COLUMN05) && a.COLUMN04 == 101).Select(q => q.COLUMN02).ToArray();



            //int num = strID.Length + strID1.Length;
            //string[] info = new string[num];
            //for (int l = 0; l < strID1.Length; l++)
            //{
            //    info[l] = strID1[l].ToString();
            //}
            //for (int l = strID1.Length; l < num; l++)
            //{
            //    info[l] = strID[l - strID1.Length].ToString();
            //}




            CONTABLE003 mm1 = new CONTABLE003();

            //string nSg = "";
            //foreach (string sg in info)
            //{
            //    if (nSg == "")
            //    {
            //        nSg = sg.ToString();
            //    }
            //    else
            //        nSg = nSg + "," + sg.ToString();
            //}
            //ViewBag.array = nSg;

            return View(Details);

        }

        //c
        [HttpGet]
        public ActionResult Delete(int id)
        {
            CONTABLE011 del = dbContext.CONTABLE011.Find(id);

            var y = (from x in dbContext.CONTABLE011 where x.COLUMN02 == id select x).First();
            string s = "False";
            y.COLUMNA12 = Convert.ToBoolean(s);
            y.COLUMNA13 = true;

            y.COLUMNA07 = DateTime.Now;
            //dbContext.CONTABLE011.Remove(y);
            dbContext.SaveChanges();
            return RedirectToAction("Info");
        }
        //c
        [HttpPost]
        public ActionResult UpdateInline(string list)
        {

            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                var c = dbContext.CONTABLE011.Where(a => a.COLUMN02 == eid).FirstOrDefault();
                c.COLUMN03 = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                c.COLUMN05 = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                c.COLUMN06 = ds.Tables[0].Rows[i].ItemArray[4].ToString();

                c.COLUMNA07 = DateTime.Now;

                dbContext.SaveChanges();
            }

            return RedirectToAction("Info");
        }



        //c
        [HttpGet]
        public ActionResult Search()
        {
            return View("Search");
        }
        //c
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            var CenterID = Request["Center_ID"];
            var Name = Request["Name"];
            var Description = Request["Description"];


            if (CenterID != null && CenterID != "" && CenterID != string.Empty)
            {
                CenterID = CenterID + "%";
            }
            else
                CenterID = Request["CenterID"];


            if (Name != null && Name != "" && Name != string.Empty)
            {
                Name = Name + "%";
            }
            else
                Name = Request["Name"];


            if (Description != null && Description != "" && Description != string.Empty)
            {
                Description = Description + "%";
            }
            else
                Description = Request["Description"];


            var query = "SELECT * FROM CONTABLE011  WHERE COLUMN02 like '" + CenterID + "' or COLUMN03 LIKE '" + Name + "' or COLUMN04 LIKE '" + Description + "' and COLUMNA12 = 'True'";



            var query1 = dbContext.CONTABLE011.SqlQuery(query);
            var gdata = from e in query1 select e;
            ViewBag.cols = Session["cols"];
            ViewBag.Inlinecols = Session["Inlinecols"];
            ViewBag.columns = Session["Inlinecols"];

            //var query1 = dbContext.CONTABLE011.SqlQuery(query);
            //var gdata = from e in query1 select e;
            //ViewBag.gdata = gdata;
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();

            //var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
            //ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();

            return View("Info", gdata);
        }


        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult Style(string items, string inactive, string view, string sort)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            List<CONTABLE011> all = new List<CONTABLE011>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
            var tbtid = tbid.COLUMN02;
            var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
            ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
            var data = from e in dbContext.CONTABLE011.AsEnumerable() select new { Center_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Operating_Unit = e.COLUMN05, Subsidary = e.COLUMN06 };
            List<CONTABLE011> tbldata = new List<CONTABLE011>();
            tbldata = dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false).ToList();
            if (items == "Normal")
            {
                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                var indata = dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE011 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE011 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th>Action</th>";
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                } int r = 0; string rowColor = "";
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        r = r + 1;

                        if (r % 2 == 0)
                        {
                            rowColor = "alt";
                        }
                        else
                        {
                            rowColor = "alt0";
                        }

                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td><a href=/CenterMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/CenterMaster/Detailes/" + itm[Inline] + " >View</a></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                        }
                        else
                        {
                            tthdata += "<td>" + itm[Inline] + "</td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                        }


                    }
                    tthdata = "<tr class=" + @rowColor + " style='color:black'>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                grid = new WebGrid(null, canPage: false, canSort: false);

                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/CenterMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CenterMaster/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Info", dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false).ToList());
        }


        public ActionResult CustomView()
        {
            List<CONTABLE005> all = new List<CONTABLE005>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            //all = from e in dbContext.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
            var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
            all = dbContext.CONTABLE005.SqlQuery(query).ToList();

            return View(all);
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
            int col2 = 0;
            var tbldata = dbContext.CONTABLE004.Where(a => a.COLUMN05 == "Center_Master").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = dbContext.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    dbContext.CONTABLE013.Add(dd);
                    dbContext.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        dbContext.CONTABLE013.Add(dd);
                        dbContext.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        }


        public ActionResult View(string items, string sort, string inactive, string style)
        {
            List<CONTABLE011> all = new List<CONTABLE011>();
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
            var ttid = tid.COLUMN02;
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (style == "Normal")
            {
                all = all.Where(a => a.COLUMNA12 == true).ToList();
                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var indata = dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE011 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE011 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th>Action</th>";
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                }
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td><a href=/CenterMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/CenterMaster/Detailes/" + itm[Inline] + " >View</a></td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                        }
                        else
                        {
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                        }
                    }
                    tthdata = "<tr>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                return View("Info", dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

            }
            else
            {
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                if (sort == "Recently Created")
                {
                    all = dbContext.CONTABLE011.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    all = dbContext.CONTABLE011.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                else
                {
                    all = dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false).ToList();

                }
                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/CenterMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CenterMaster/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Info", dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false).ToList());
        }



        public void ExportPdf()
        {
            List<CONTABLE011> all = new List<CONTABLE011>();
            all = dbContext.CONTABLE011.ToList();
            var data = from e in dbContext.CONTABLE011.AsEnumerable() select new { Center_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Operating_Unit = e.COLUMN05, Subsidary = e.COLUMN06 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        public void Exportword()
        {
            List<CONTABLE011> all = new List<CONTABLE011>();
            all = dbContext.CONTABLE011.ToList();
            var data = from e in dbContext.CONTABLE011.AsEnumerable() select new { Center_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Operating_Unit = e.COLUMN05, Subsidary = e.COLUMN06 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
            Response.ContentType = "application/vnd.ms-word ";
            Response.Charset = string.Empty;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        public void ExportCSV()
        {
            StringWriter sw = new StringWriter();

            var y = dbContext.CONTABLE011.OrderBy(q => q.COLUMN02).ToList();

            sw.WriteLine("\"Center_ID\",\"Name\",\"Description\",\"Operating_Unit\",\"Subsidary\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
            Response.ContentType = "text/csv";

            foreach (var line in y)
            {

                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"",
                                           line.COLUMN02,
                                           line.COLUMN03,
                                           line.COLUMN04,
                                           line.COLUMN05,
                                           line.COLUMN06));

            }

            Response.Write(sw.ToString());

            Response.End();

        }
        public void ExportExcel()
        {
            List<CONTABLE011> all = new List<CONTABLE011>();
            all = dbContext.CONTABLE011.ToList();
            var data = from e in dbContext.CONTABLE011.AsEnumerable() select new { Center_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Operating_Unit = e.COLUMN05, Subsidary = e.COLUMN06 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            gv.FooterRow.Visible = false;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter ht = new HtmlTextWriter(sw);
            gv.RenderControl(ht);
            Response.Write(sw.ToString());
            Response.End();
            gv.FooterRow.Visible = true;

        }
        public ActionResult Export(string items)
        {
            List<CONTABLE011> all = new List<CONTABLE011>();
            all = dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false).ToList();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE011").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (items == "PDF")
            {
                ExportPdf();
            }
            else if (items == "Excel")
            {
                ExportExcel();
            }
            else if (items == "Word")
            {
                Exportword();
            }
            else if (items == "CSV")
            {
                ExportCSV();
            }
            return View("Info", dbContext.CONTABLE011.Where(a => a.COLUMNA13 == false).ToList());
        }

    }
}