﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class ReturnReceiptController : Controller
    {
        //
        // GET: /ReturnReceipt/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        CommonController taxlst = new CommonController();

        public ActionResult Create()
        {
            try
            {
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult ReturnReceiptNew(int ide, string FormName)
        {
            try
            {
                Session["FormName"] = "Return Receipt"; 
                List<SelectListItem> Attendedby = new List<SelectListItem>();
                SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                DataTable dtat = new DataTable();
                cmdat.Fill(dtat);
                var salesrep = "";
                for (int dd = 0; dd < dtat.Rows.Count; dd++)
                {
                    if (Convert.ToString(Session["eid"]) == dtat.Rows[dd]["COLUMN02"].ToString())
                        salesrep = dtat.Rows[dd]["COLUMN02"].ToString();
                    Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: salesrep);
                string lot = "N";
                SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN31'  and column04=110010828 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                DataTable dtlt = new DataTable();
                dalt.Fill(dtlt);
                if (dtlt.Rows.Count > 0)
                    lot = dtlt.Rows[0][0].ToString();
                Session["LotTrack"] = lot;
                if (ide != 0)
                {
                    SqlDataAdapter cmdvChk = new SqlDataAdapter("SELECT   count(*) [NoofItems],sum(COLUMN15) [PAmount] FROM SATABLE005 " +
                    " where column11='" + ide + "' and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  and isnull(COLUMNA13,0)=0  AND COLUMNA03='" + Session["AcOwner"] + "' ", cn);
                    DataTable dtvChk = new DataTable();
                    cmdvChk.Fill(dtvChk);
                    int RowChk = Convert.ToInt32(dtvChk.Rows[0]["NoofItems"].ToString());


                    SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(*) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN10) [Qty], h.COLUMN22 [SubTotal], h.COLUMN25 Discount, h.COLUMN24 Tax, (isnull(h.COLUMN20,0)) Total, (isnull(p.COLUMN06,0)) Payment, (isnull(h.COLUMN20,0)- (isnull(p.COLUMN06,0))) Dueamt,ph.COLUMN08 Account, h.COLUMN02 [invoiceno],isnull(h.COLUMN68,0) roundoffadjamt,h.COLUMN46 amtin, h.COLUMN56 HDiscount FROM SATABLE009 h inner join SATABLE010 l on l.COLUMN15=h.COLUMN01  and isnull(l.COLUMNA13,0)=0  " +
                    " left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 and isnull(p.COLUMNA13,0)=0 left JOIN SATABLE011 ph  ON ph.COLUMN01 =p.COLUMN08 where h.column02=" + ide + " group by h.COLUMN05,h.COLUMN22,h.COLUMN25,h.COLUMN24,h.COLUMN20,c.COLUMN05,p.COLUMN06,ph.COLUMN08, h.COLUMN02,h.COLUMN68,h.COLUMN46,h.COLUMN56 ", cn);
                    DataTable dtv = new DataTable();
                    cmdv.Fill(dtv);
                    decimal TotalAmount = Convert.ToDecimal(dtv.Rows[0]["Total"].ToString());
                    

                   
                    

                    List<SelectListItem> SalesReceipt = new List<SelectListItem>();
                    SqlDataAdapter cmddls = new SqlDataAdapter("select COLUMN02, COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'  AND  COLUMN03=1532   order by  COLUMN02 desc", cn);
                    DataTable dtdatas = new DataTable();
                    cmddls.Fill(dtdatas);
                    for (int dd = 0; dd < dtdatas.Rows.Count; dd++)
                    {
                        SalesReceipt.Add(new SelectListItem { Value = dtdatas.Rows[dd]["COLUMN02"].ToString(), Text = dtdatas.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["SalesReceipt"] = new SelectList(SalesReceipt, "Value", "Text", selectedValue: dtv.Rows[0]["invoiceno"].ToString());


                    List<SelectListItem> Customer = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());

                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");

                 
                    List<SelectListItem> UOM = new List<SelectListItem>();
                    SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdataU = new DataTable();
                    cmddlU.Fill(dtdataU);
                    for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                    {
                        UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["UOM"] = new SelectList(UOM, "Value", "Text");
                    List<SelectListItem> Amtin = new List<SelectListItem>();
                    SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dta = new DataTable();
                    cmda.Fill(dta);
                    for (int dd = 0; dd < dta.Rows.Count; dd++)
                    {
                        Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: dtv.Rows[0]["amtin"].ToString());
                    List<POPrint> all1 = new List<POPrint>();
                    all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                    {
                        rNoofitems = "",
                        rTotalItems = "",
                        rCustomerno = "",
                        rCustomer = "",
                        rSubTotal = "",
                        rDiscount = "",
                        rTax = "",
                        rTotal = "",
                        rPayment = "",
                        rDueamt = "",
                        rAccount = "",
                        ide = ""
                    });

                    SqlDataAdapter cmdL = new SqlDataAdapter("SELECT ((isnull(l.COLUMN10,0))-sum(isnull(s6.column07,0))) Qty, CAST(CAST(((isnull(l.COLUMN10,0))-sum(isnull(s6.column07,0))) AS  decimal(18,2))* (iif(isnull(l.COLUMN35,0)=0,l.COLUMN13,l.COLUMN35)) AS decimal(18,2)) [SubTotal]  , CAST(cast (((isnull(l.COLUMN10,0)-sum(isnull(s6.column07,0)))* iif(isnull(l.COLUMN35,0)=0,l.COLUMN13,l.COLUMN35))-(cast(iif(cast(l.COLUMN19 as nvarchar(250))='','0',isnull(l.COLUMN19,'0')) as decimal(18,2))-cast(isnull(s6.COLUMN10,0) AS  decimal(18,2))) AS  decimal(18,2)) *((cast(sum(isnull(m13.COLUMN07,0)) as decimal(18,2)))*0.01 )AS decimal(18,2))  Tax, (cast(iif(cast(l.COLUMN19 as nvarchar(250))='','0',isnull(l.COLUMN19,'0')) as decimal(18,2))-isnull(s6.COLUMN10,0)) Discount  FROM " +
      "SATABLE009 h inner join SATABLE010 l on l.COLUMN15=h.COLUMN01  and isnull(l.COLUMNA13,0)=0  LEFT  join SATABLE006 s6 on s6.COLUMN36=l.COLUMN02 and isnull(s6.COLUMNA13,0)=0  left join MATABLE013 m13 on  (m13.column02 in(SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(l.COLUMN21,'-',''))) s) or m13.COLUMN02=l.COLUMN21)  where h.column02= " + ide + " GROUP BY l.COLUMN02 ,l.COLUMN10,l.COLUMN13,l.COLUMN35,l.COLUMN19,s6.COLUMN10   having (isnull(l.COLUMN10,0)-sum(isnull(s6.column07,0)))>0 ", cn);
                    DataTable dtL = new DataTable();
                    cmdL.Fill(dtL);
                    decimal qtyL = 0, subtotalL = 0, taxL = 0, disL = 0, paymentL = 0;
                    for (int i = 0; i < dtL.Rows.Count; i++)
                    {
                        qtyL += Convert.ToDecimal(dtL.Rows[i]["Qty"].ToString());
                        subtotalL += Convert.ToDecimal(dtL.Rows[i]["SubTotal"].ToString());
                        taxL += Convert.ToDecimal(dtL.Rows[i]["Tax"].ToString());
                        disL += Convert.ToDecimal(dtL.Rows[i]["Discount"].ToString());

                    }
                    paymentL = (subtotalL + taxL - disL);
                    if (RowChk > 0)
                    {
                        decimal PaidAmount = Convert.ToDecimal(dtvChk.Rows[0]["PAmount"].ToString());
                        if (PaidAmount >= TotalAmount)
                        {
                            return View();
                        }
                    }

                    all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                    all1[0].rTotalItems = qtyL.ToString();
                    all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                    all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                    all1[0].DiscountAmount = dtv.Rows[0]["HDiscount"].ToString();
                    all1[0].rSubTotal = subtotalL.ToString();
                    all1[0].rDiscount = disL.ToString();
                    all1[0].rTax = taxL.ToString();
                    all1[0].rTotal = paymentL.ToString();
                    all1[0].ide = ide.ToString();
                    string payamt = paymentL.ToString();
                    if (payamt == "" || payamt == "null") payamt = "0";
                    string dueamt = dtv.Rows[0]["Dueamt"].ToString();
                    if (dueamt == "" || dueamt == "null") dueamt = "0";
                    all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                    all1[0].rPayment = (Convert.ToDecimal(payamt) + Convert.ToDecimal(dueamt)).ToString();
                    all1[0].rAccount = dtv.Rows[0]["Account"].ToString();
                    all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                    all1[0].amttype = dtv.Rows[0]["amtin"].ToString();
                    //DateFormat();
                    return View(all1);
                }
                else
                {
                    List<SelectListItem> Customer = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                    List<SelectListItem> Amtin = new List<SelectListItem>();
                    SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dta = new DataTable();
                    cmda.Fill(dta);
                    for (int dd = 0; dd < dta.Rows.Count; dd++)
                    {
                        Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: Session["TaxInclusive"]);
                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");
                    List<SelectListItem> SalesReceipt = new List<SelectListItem>();
                    SqlDataAdapter cmddls = new SqlDataAdapter("select COLUMN02, COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'   AND  COLUMN03=1532 order by  COLUMN02 desc ", cn);
                    DataTable dtdatas = new DataTable();
                    cmddls.Fill(dtdatas);
                    for (int dd = 0; dd < dtdatas.Rows.Count; dd++)
                    {
                        SalesReceipt.Add(new SelectListItem { Value = dtdatas.Rows[dd]["COLUMN02"].ToString(), Text = dtdatas.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["SalesReceipt"] = new SelectList(SalesReceipt, "Value", "Text");
                    //DateFormat();
                    return View();
                }
            }

            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public void DateFormat()
        {
            string str = "select isnull(COLUMN04,'dd/MM/yyyy')COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
            SqlDataAdapter daf = new SqlDataAdapter(str, cn);
            DataTable dtf = new DataTable();
            daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
            if (dtf.Rows.Count > 0)
            {
                DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                Session["DateFormat"] = DateFormat;
                Session["FormatDate"] = DateFormat;
                ViewBag.DateFormat = DateFormat;
            }
            if (JQDateFormat != "")
            {
                Session["DateFormat"] = DateFormat;
                Session["FormatDate"] = DateFormat;
                Session["ReportDate"] = JQDateFormat;
            }
            else
            {
                Session["DateFormat"] = "dd/MM/yyyy";
                Session["FormatDate"] = "dd/MM/yyyy";
                Session["ReportDate"] = "dd/mm/yy";
                ViewBag.DateFormat = "dd/MM/yyyy";
            }
        }
        [HttpPost]
        public ActionResult POSSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["POSGridData"] = ds;
                //var saveform = Request.QueryString["FormName"];
                //var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == saveform).ToList();
                //var fname = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var frmID = fnamedata.Select(q => q.COLUMN02).FirstOrDefault();
                //var customfrmID = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var frmType = fnamedata.Select(q => q.COLUMN05).FirstOrDefault().ToString();
                //var MasterForm = fnamedata.Select(q => q.COLUMN06).FirstOrDefault();
                //if (frmType == "Custom")
                //{
                //    frmID = Convert.ToInt32(MasterForm);
                //    saveform = dc.CONTABLE0010.Where(q => q.COLUMN02 == MasterForm).FirstOrDefault().COLUMN04;
                //}
                //Session["FormName"] = saveform;
                //Session["id"] = fname;
                //eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                //lg.CreateFile(Server.MapPath("~/"), saveform + "_" + Session["UserName"].ToString() + "", " \r\n" + "***********************TARGET1***********************" + " \r\n" + " \r\n" + " Line Data Received .");

                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        [HttpPost]
        public ActionResult Saveform(string Qty, string SubTotal, string Discount, string HeaderDiscount, string TaxAmt, string Type, string TotAmt, string PayAmt, string PayAcc, string Customer, string SRecept, string Amtin, string roundoffadjamt, string Attendedby)
        {
            try
            {
                Session["FormName"] = "Return Receipt";
                int OutParam = 0;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string OrderType = "1002";
                string Date = DateTime.Now.ToShortDateString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = ""; int l = 0;
                Session["FormName"] = "Return Receipt";
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE005 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString(); int r = 0;
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1603");
                    TransNo = taxlst.TransactionNoGenerate(Convert.ToString(Session["FormName"]), 1603, OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Customer);
                    //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                    Cmd.Parameters.AddWithValue("@COLUMN06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN19", "");
                    Cmd.Parameters.AddWithValue("@COLUMN08", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN51", PayAcc);
                    Cmd.Parameters.AddWithValue("@COLUMN09", Type);
                    Cmd.Parameters.AddWithValue("@COLUMN10", "");
                    Cmd.Parameters.AddWithValue("@COLUMN11", SRecept);
                    Cmd.Parameters.AddWithValue("@COLUMN14", Discount);
                    Cmd.Parameters.AddWithValue("@COLUMN60", HeaderDiscount);
                    Cmd.Parameters.AddWithValue("@COLUMN23", "");
                    Cmd.Parameters.AddWithValue("@COLUMN32", TaxAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN12", SubTotal);
                    Cmd.Parameters.AddWithValue("@COLUMN15", PayAmt);
                    //Cmd.Parameters.AddWithValue("@COLUMN25", Discount);
                    Cmd.Parameters.AddWithValue("@COLUMN24", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN71", roundoffadjamt);
                    Cmd.Parameters.AddWithValue("@COLUMN50", Amtin);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Attendedby);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE005");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cnc.Open();
                    Cmd.CommandTimeout = 0;
                    r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                }
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1603).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                if (r > 0)
                {
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                }
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {

                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE006 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string Item = itemdata.Tables[0].Rows[i]["ItemID"].ToString();
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["Qty"].ToString();
                    string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                    string InclusiveTotal = itemdata.Tables[0].Rows[i]["InclusiveTotal"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["Amount"].ToString();
                    string BatchId = itemdata.Tables[0].Rows[i]["BatchId"].ToString();
                    if (BatchId == "0" || BatchId == "" || BatchId == null) BatchId = "0";
                    string lDiscount = itemdata.Tables[0].Rows[i]["Discount"].ToString();
                    string lTaxType = itemdata.Tables[0].Rows[i]["TaxType"].ToString();
                    string lTaxAmt = itemdata.Tables[0].Rows[i]["TaxAmt"].ToString();
                    string rowid = itemdata.Tables[0].Rows[i]["rowid"].ToString();

                    if (Units == "0" || Units == "" || Units == null) Units = "10000";

                    SqlCommand cmddi = new SqlCommand("select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=" + Item + " and COLUMN19=" + Units + " and COLUMN13 in(" + OpUnit + ") and COLUMNA03=" + AOwner + " ", cn);
                    SqlDataAdapter daai = new SqlDataAdapter(cmddi);
                    DataTable dtti = new DataTable();
                    daai.Fill(dtti);
                    string Avlqty = dtti.Rows[0][0].ToString();
                    if (Avlqty == "" || Avlqty == null) Avlqty = "0";

                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;

                        Cmdl.Parameters.AddWithValue("@COLUMN19", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Item);
                        Cmdl.Parameters.AddWithValue("@COLUMN07", lQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN27", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN09", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN32", Rate);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", Amount);
                        Cmdl.Parameters.AddWithValue("@COLUMN25", InclusiveTotal);

                        Cmdl.Parameters.AddWithValue("@COLUMN26", lTaxType);
                        //Cmdl.Parameters.AddWithValue("@COLUMN32", lTaxAmt);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", lDiscount);
                        Cmdl.Parameters.AddWithValue("@COLUMN36", rowid);
                        Cmdl.Parameters.AddWithValue("@COLUMN17", BatchId);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eiReturnReceiptNewd"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE006");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        Cmdl.CommandTimeout = 0;
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1329 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                // return RedirectToAction("ReturnReceiptInfo", "ReturnReceipt", new { idi = idi, FormName = custform });
                //return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("ReturnReceiptInfo", "ReturnReceipt", new { idi = idi, FormName = custform });
                return Json(new { Data = l, InvoiceNo = HCol2, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
		

        public ActionResult ReturnReceiptInfo()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var frmid = 1603;
                Session["id"] = frmid;
                var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmid).ToList().FirstOrDefault().COLUMN04;
                Session["FormName"] = FormName;
                Session["idi"] = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName && a.COLUMNA03 == acID).ToList().FirstOrDefault().COLUMN02;
                object[] sp = new object[4];
                sp[0] = (frmid);
                sp[1] = (Session["OPUnit"]);
                sp[2] = (Session["AcOwner"]);
                sp[3] = ("Info");
                var GData = dbs.Query("Exec USP_PROC_FormBuildInfo @FormId=@0,@OPUnit=@1,@AcOwner=@2,@Type=@3", sp);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                ViewBag.Columns = GData.FirstOrDefault().Columns;
                ViewBag.Grid = GData;
                DateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult NewTransaction()
        {
            try
            {
                ViewBag.RetailTransNo = taxlst.TransactionNoGenerate(Convert.ToString(Session["FormName"]), 1603, Convert.ToString(Session["OpUnit1"]));
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Units = new List<SelectListItem>();
                SqlDataAdapter cmdu = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11119 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtu = new DataTable();
                cmdu.Fill(dtu);
                for (int dd = 0; dd < dtu.Rows.Count; dd++)
                {
                    Units.Add(new SelectListItem { Value = dtu.Rows[dd]["COLUMN02"].ToString(), Text = dtu.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Units"] = new SelectList(Units, "Value", "Text");
                List<SelectListItem> Lot = new List<SelectListItem>();
                SqlDataAdapter cmdl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE043  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtl = new DataTable();
                cmdl.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Lot.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Lot"] = new SelectList(Lot, "Value", "Text");
                List<SelectListItem> TaxType = new List<SelectListItem>();
                SqlDataAdapter cmdt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                DataTable dtt = new DataTable();
                cmdt.Fill(dtt);
                for (int dd = 0; dd < dtt.Rows.Count; dd++)
                {
                    TaxType.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                List<SelectListItem> Receipt = new List<SelectListItem>();
                SqlDataAdapter cmdr = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE009  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and  isnull(COLUMNA13,'False')='False' order by column02 desc", cn);
                DataTable dtr = new DataTable();
                cmdr.Fill(dtr);
                for (int dd = 0; dd < dtr.Rows.Count; dd++)
                {
                    Receipt.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Receipt#"] = new SelectList(Receipt, "Value", "Text");
                List<SelectListItem> ReceiptType = new List<SelectListItem>();
                SqlDataAdapter cmdrt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE011  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMN12,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtrt = new DataTable();
                cmdrt.Fill(dtrt);
                for (int dd = 0; dd < dtrt.Rows.Count; dd++)
                {
                    ReceiptType.Add(new SelectListItem { Value = dtrt.Rows[dd]["COLUMN02"].ToString(), Text = dtrt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Receipt Type"] = new SelectList(ReceiptType, "Value", "Text");
                List<SelectListItem> PaymentTerms = new List<SelectListItem>();
                SqlDataAdapter cmdp = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11122 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtp = new DataTable();
                cmdp.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    PaymentTerms.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Payment Terms"] = new SelectList(PaymentTerms, "Value", "Text");
                List<SelectListItem> PaymentMode = new List<SelectListItem>();
                SqlDataAdapter cmdm = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11123 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtm = new DataTable();
                cmdm.Fill(dtm);
                for (int dd = 0; dd < dtm.Rows.Count; dd++)
                {
                    PaymentMode.Add(new SelectListItem { Value = dtm.Rows[dd]["COLUMN02"].ToString(), Text = dtm.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Payment Mode"] = new SelectList(PaymentMode, "Value", "Text");
                List<SelectListItem> OperatingUnit = new List<SelectListItem>();
                SqlDataAdapter cmdo = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from CONTABLE007  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMN07,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dto = new DataTable();
                cmdo.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    OperatingUnit.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Operating Unit"] = new SelectList(OperatingUnit, "Value", "Text");
                List<SelectListItem> Department = new List<SelectListItem>();
                SqlDataAdapter cmdd = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11117 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtd = new DataTable();
                cmdd.Fill(dtd);
                for (int dd = 0; dd < dtd.Rows.Count; dd++)
                {
                    Department.Add(new SelectListItem { Value = dtd.Rows[dd]["COLUMN02"].ToString(), Text = dtd.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Department"] = new SelectList(Department, "Value", "Text");
                List<SelectListItem> Project = new List<SelectListItem>();
                SqlDataAdapter cmdpr = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtpr = new DataTable();
                cmdpr.Fill(dtpr);
                for (int dd = 0; dd < dtpr.Rows.Count; dd++)
                {
                    Project.Add(new SelectListItem { Value = dtpr.Rows[dd]["COLUMN02"].ToString(), Text = dtpr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Project#"] = new SelectList(Project, "Value", "Text");
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlDataAdapter cmdc = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from MATABLE016  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtc = new DataTable();
                cmdc.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Country"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> State = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from MATABLE017  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    State.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["State"] = new SelectList(State, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }



        public ActionResult ReturnReceiptEdit(int ide, string FormName)
        {
            try
            {
                Session["FormName"] = "Return Receipt";
                string lot = "N";
                SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN31'  and column04=110010828 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                DataTable dtlt = new DataTable();
                dalt.Fill(dtlt);
                if (dtlt.Rows.Count > 0)
                    lot = dtlt.Rows[0][0].ToString();
                Session["LotTrack"] = lot;
                if (ide != 0)
                {
                    SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(*) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN07) [Qty], h.COLUMN12 [SubTotal], (isnull(h.COLUMN14,0)+isnull(h.COLUMN60,0)) Discount, h.COLUMN32 Tax,(( isnull(h.COLUMN12,0) + isnull(h.COLUMN32,0) ) -  (isnull(h.COLUMN14,0)+isnull(h.COLUMN60,0))+isnull(h.COLUMN71,0)) Total, (isnull(h.COLUMN15,0)) Payment, ((( isnull(h.COLUMN12,0) + isnull(h.COLUMN32,0) ) -  (isnull(h.COLUMN14,0)+isnull(h.COLUMN60,0))) - (isnull(h.COLUMN15,0))+ isnull(h.COLUMN71,0)) Dueamt,h.COLUMN51 Account, h.COLUMN11 [invoiceno] ,h.COLUMN09 PayType,isnull(h.COLUMN71,0) roundoffadjamt ,h.COLUMN50 amtin,isnull(h.COLUMN60,0) HDiscount,h.COLUMNA08 Attendedby  " +
                    "  FROM SATABLE005 h inner join SATABLE006 l on l.COLUMN19=h.COLUMN01 and isnull(l.COLUMNA13,0)=0 LEFT JOIN SATABLE009 S9 ON S9.COLUMN02= h.COLUMN11 AND S9.COLUMNA03= h.COLUMNA03  left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 where h.column02=" + ide + " and h.COLUMNA03='" + Session["AcOwner"] + "'  and isnull(h.COLUMNA13,'False')='False'   group by h.COLUMN05,h.COLUMN12,h.COLUMN32,h.COLUMN14,h.COLUMN60,h.COLUMN15,h.COLUMN20,c.COLUMN05, h.COLUMN11 ,S9.COLUMN20 ,h.COLUMN51,h.COLUMN09,h.COLUMN71,h.COLUMN50,h.COLUMNA08", cn);
                    DataTable dtv = new DataTable();
                    cmdv.Fill(dtv);
                    System.Collections.IEnumerable rows = dtv.Rows;
                    ViewBag.pitemsdata = rows;

                    List<SelectListItem> SalesReceipt = new List<SelectListItem>();
                    SqlDataAdapter cmddls = new SqlDataAdapter("select COLUMN02, COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'   order by  COLUMN02 desc", cn);
                    DataTable dtdatas = new DataTable();
                    cmddls.Fill(dtdatas);
                    for (int dd = 0; dd < dtdatas.Rows.Count; dd++)
                    {
                        SalesReceipt.Add(new SelectListItem { Value = dtdatas.Rows[dd]["COLUMN02"].ToString(), Text = dtdatas.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["SalesReceipt"] = new SelectList(SalesReceipt, "Value", "Text", selectedValue: dtv.Rows[0]["invoiceno"].ToString());


                    List<SelectListItem> Customer = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());

                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");

                    List<SelectListItem> UOM = new List<SelectListItem>();
                    SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdataU = new DataTable();
                    cmddlU.Fill(dtdataU);
                    for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                    {
                        UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["UOM"] = new SelectList(UOM, "Value", "Text");

                    List<POPrint> all1 = new List<POPrint>();
                    all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                    {
                        rNoofitems = "",
                        rTotalItems = "",
                        rCustomerno = "",
                        rCustomer = "",
                        rSubTotal = "",
                        rDiscount = "",
                        rTax = "",
                        rTotal = "",
                        rPayment = "",
                        rDueamt = "",
                        rAccount = "",
                        ide = ""
                    });
                    all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                    all1[0].rTotalItems = dtv.Rows[0]["Qty"].ToString();
                    all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                    all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                    all1[0].rSubTotal = dtv.Rows[0]["SubTotal"].ToString();
                    all1[0].rDiscount = dtv.Rows[0]["Discount"].ToString();
                    all1[0].DiscountAmount = dtv.Rows[0]["HDiscount"].ToString();
                    all1[0].rTax = dtv.Rows[0]["Tax"].ToString();
                    all1[0].rTotal = dtv.Rows[0]["Total"].ToString();
                    all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                    all1[0].amttype = dtv.Rows[0]["amtin"].ToString();
                    all1[0].ide = ide.ToString();
                    string payamt = dtv.Rows[0]["Payment"].ToString();
                    if (payamt == "" || payamt == "null") payamt = "0";
                    string dueamt = dtv.Rows[0]["Dueamt"].ToString();
                    if (dueamt == "" || dueamt == "null") dueamt = "0";
                    all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                    all1[0].rPamt = payamt;
                    all1[0].rPayment = (Convert.ToDecimal(payamt) + Convert.ToDecimal(dueamt)).ToString();
                    all1[0].rAccount = dtv.Rows[0]["Account"].ToString();
                    List<SelectListItem> Amtin = new List<SelectListItem>();
                    SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dta = new DataTable();
                    cmda.Fill(dta);
                    for (int dd = 0; dd < dta.Rows.Count; dd++)
                    {
                        Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: all1[0].amttype); 
                    List<SelectListItem> Attendedby = new List<SelectListItem>();
                    SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                    DataTable dtat = new DataTable();
                    cmdat.Fill(dtat);
                    for (int dd = 0; dd < dtat.Rows.Count; dd++)
                    {
                        Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: dtv.Rows[0]["Attendedby"].ToString());
                    return View(all1);
                }
                else
                {
                    List<SelectListItem> Customer = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");
                    List<SelectListItem> SalesReceipt = new List<SelectListItem>();
                    SqlDataAdapter cmddls = new SqlDataAdapter("select COLUMN02, COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdatas = new DataTable();
                    cmddls.Fill(dtdatas);
                    for (int dd = 0; dd < dtdatas.Rows.Count; dd++)
                    {
                        SalesReceipt.Add(new SelectListItem { Value = dtdatas.Rows[dd]["COLUMN02"].ToString(), Text = dtdatas.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["SalesReceipt"] = new SelectList(SalesReceipt, "Value", "Text");
                    List<SelectListItem> Amtin = new List<SelectListItem>();
                    SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dta = new DataTable();
                    cmda.Fill(dta);
                    for (int dd = 0; dd < dta.Rows.Count; dd++)
                    {
                        Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text");
                    List<SelectListItem> Attendedby = new List<SelectListItem>();
                    SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                    DataTable dtat = new DataTable();
                    cmdat.Fill(dtat);
                    var salesrep = "";
                    for (int dd = 0; dd < dtat.Rows.Count; dd++)
                    {
                        if (Convert.ToString(Session["eid"]) == dtat.Rows[dd]["COLUMN02"].ToString())
                            salesrep = dtat.Rows[dd]["COLUMN02"].ToString();
                        Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: salesrep);
                    return View();
                }
            }

            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        [HttpPost]
        public ActionResult EditSaveform(string Qty, string SubTotal, string Discount, string HeaderDiscount, string TaxAmt, string TotAmt, string PayAmt, string PayAcc, string Customer, string SRecept, string ide, string Type, string Amtin, string roundoffadjamt, string Attendedby)
        {
            try
            {
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string OrderType = "1002";
                string Date = DateTime.Now.ToShortDateString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update", TransNo = "", IHeaderIID = "", PHeaderIID = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  COLUMN01,COLUMN04,COLUMN06 from SATABLE005 where column02=" + ide + " ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                IHeaderIID = dtt.Rows[0][0].ToString();
                TransNo = dtt.Rows[0]["COLUMN04"].ToString();
                string TDate = Convert.ToDateTime(dtt.Rows[0]["COLUMN06"].ToString()).ToShortDateString();
                string HCol2 = ide; int r = 0;
                //HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;

                    Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1603");
                    Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Customer);
                    //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                    Cmd.Parameters.AddWithValue("@COLUMN06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN19", "");
                    Cmd.Parameters.AddWithValue("@COLUMN08", Date);
                    Cmd.Parameters.AddWithValue("@COLUMN10", "");
                    Cmd.Parameters.AddWithValue("@COLUMN11", SRecept);
                    Cmd.Parameters.AddWithValue("@COLUMN14", Discount);
                    Cmd.Parameters.AddWithValue("@COLUMN60", HeaderDiscount);
                    Cmd.Parameters.AddWithValue("@COLUMN51", PayAcc);
                    Cmd.Parameters.AddWithValue("@COLUMN09", Type);
                    Cmd.Parameters.AddWithValue("@COLUMN23", "");
                    Cmd.Parameters.AddWithValue("@COLUMN32", TaxAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN12", SubTotal);
                    Cmd.Parameters.AddWithValue("@COLUMN15", PayAmt);
                    //Cmd.Parameters.AddWithValue("@COLUMN25", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN24", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN71", roundoffadjamt);
                    Cmd.Parameters.AddWithValue("@COLUMN50", Amtin);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Attendedby);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE005");
                    Cmd.Parameters.AddWithValue("@ReturnValue", "");
                    cnc.Open();
                    Cmd.CommandTimeout = 0;
                    r = Cmd.ExecuteNonQuery();
                }
                //Line
                if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1329 && q.COLUMN05 == 2).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE006 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string rowid = itemdata.Tables[0].Rows[i]["rowid"].ToString();
                    if (rowid == "0" || rowid == "") rowid = LCol2;
                    string Item = itemdata.Tables[0].Rows[i]["ItemID"].ToString();
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    string UPC = itemdata.Tables[0].Rows[i]["UPC"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["Qty"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                    string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                    string InclusiveTotal = itemdata.Tables[0].Rows[i]["InclusiveTotal"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["Amount"].ToString();
                    string lDiscount = itemdata.Tables[0].Rows[i]["Discount"].ToString();
                    string TaxType = itemdata.Tables[0].Rows[i]["TaxType"].ToString();
                    string lTaxAmt = itemdata.Tables[0].Rows[i]["TaxAmt"].ToString();
                    if (Units == "0" || Units == "" || Units == null) Units = "10000";
                    string BatchId = itemdata.Tables[0].Rows[i]["BatchId"].ToString();
                    if (BatchId == "0" || BatchId == "" || BatchId == null) BatchId = "0";
                    SqlCommand cmddi = new SqlCommand("select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=" + Item + " and COLUMN19=" + Units + " and COLUMN13 in(" + OpUnit + ") and COLUMNA03=" + AOwner + " ", cn);
                    SqlDataAdapter daai = new SqlDataAdapter(cmddi);
                    DataTable dtti = new DataTable();
                    daai.Fill(dtti);
                    string Avlqty = dtti.Rows[0][0].ToString();
                    if (Avlqty == "" || Avlqty == null) Avlqty = "0";
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", rowid);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", Item);
                        Cmdl.Parameters.AddWithValue("@COLUMN04", UPC);
                        Cmdl.Parameters.AddWithValue("@COLUMN07", lQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN27", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN09", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN32", Rate);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", Amount);
                        Cmdl.Parameters.AddWithValue("@COLUMN25", InclusiveTotal);

                        Cmdl.Parameters.AddWithValue("@COLUMN26", TaxType);
                        //Cmdl.Parameters.AddWithValue("@COLUMN32", lTaxAmt);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", lDiscount);
                        Cmdl.Parameters.AddWithValue("@COLUMN17", BatchId);
                        Cmdl.Parameters.AddWithValue("@COLUMN19", IHeaderIID);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE006");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        Cmdl.CommandTimeout = 0;
                        l = Cmdl.ExecuteNonQuery();
                    }
                } Session["POSGridData"] = null;
                int lP = 0;

                cn.Close();
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return Json(new { Data = l, InvoiceNo = HCol2, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("PointOfSalesInfo", "PointOfSales");
                //return View("PointOfSalesInfo");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        


        public ActionResult ReturnReceiptView(int ide, string FormName)
        {
            try
            {
                Session["FormName"] = "Return Receipt";

                if (ide != 0)
                {
                    SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(*) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN07) [Qty], h.COLUMN12 [SubTotal], (isnull(h.COLUMN14,0)+isnull(h.COLUMN60,0)) Discount, h.COLUMN32 Tax,(( isnull(h.COLUMN12,0) + isnull(h.COLUMN32,0) ) -  (isnull(h.COLUMN14,0)+isnull(h.COLUMN60,0))+isnull(h.COLUMN71,0)) Total, (isnull(h.COLUMN15,0)) Payment, ((( isnull(h.COLUMN12,0) + isnull(h.COLUMN32,0) ) -  (isnull(h.COLUMN14,0)+isnull(h.COLUMN60,0))) - (isnull(h.COLUMN15,0))+isnull(h.COLUMN71,0)) Dueamt,h.COLUMN51 Account, h.COLUMN11 [invoiceno] ,h.COLUMN09 PayType,isnull(h.COLUMN71,0) roundoffadjamt,h.COLUMN50 amtin,isnull(h.COLUMN60,0) HDiscount,h.COLUMNA08 Attendedby  " +
                        "  FROM SATABLE005 h inner join SATABLE006 l on l.COLUMN19=h.COLUMN01  LEFT JOIN SATABLE009 S9 ON S9.COLUMN02= h.COLUMN11 AND S9.COLUMNA03= h.COLUMNA03  left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 where h.column02=" + ide + " and h.COLUMNA03='" + Session["AcOwner"] + "'  and isnull(h.COLUMNA13,'False')='False'   group by h.COLUMN05,h.COLUMN12,h.COLUMN32,h.COLUMN14,h.COLUMN60,h.COLUMN15,h.COLUMN20,c.COLUMN05, h.COLUMN11 ,S9.COLUMN20 ,h.COLUMN51,h.COLUMN09,h.COLUMN71,h.COLUMN50,h.COLUMNA08", cn);

                    DataTable dtv = new DataTable();
                    cmdv.Fill(dtv);
                    System.Collections.IEnumerable rows = dtv.Rows;
                    ViewBag.pitemsdata = rows;

                    List<SelectListItem> SalesReceipt = new List<SelectListItem>();
                    SqlDataAdapter cmddls = new SqlDataAdapter("select COLUMN02, COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdatas = new DataTable();
                    cmddls.Fill(dtdatas);
                    for (int dd = 0; dd < dtdatas.Rows.Count; dd++)
                    {
                        SalesReceipt.Add(new SelectListItem { Value = dtdatas.Rows[dd]["COLUMN02"].ToString(), Text = dtdatas.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["SalesReceipt"] = new SelectList(SalesReceipt, "Value", "Text", selectedValue: dtv.Rows[0]["invoiceno"].ToString());


                    List<SelectListItem> Customer = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());

                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");

                    List<POPrint> all1 = new List<POPrint>();
                    all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                    {
                        rNoofitems = "",
                        rTotalItems = "",
                        rCustomerno = "",
                        rCustomer = "",
                        rSubTotal = "",
                        rDiscount = "",
                        rTax = "",
                        rTotal = "",
                        rPayment = "",
                        rDueamt = "",
                        rAccount = "",
                        ide = ""
                    });
                    all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                    all1[0].rTotalItems = dtv.Rows[0]["Qty"].ToString();
                    all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                    all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                    all1[0].rSubTotal = dtv.Rows[0]["SubTotal"].ToString();
                    all1[0].rDiscount = dtv.Rows[0]["Discount"].ToString();
                    all1[0].DiscountAmount = dtv.Rows[0]["HDiscount"].ToString();
                    all1[0].rTax = dtv.Rows[0]["Tax"].ToString();
                    all1[0].rTotal = dtv.Rows[0]["Total"].ToString();
                    all1[0].ide = ide.ToString();
                    string payamt = dtv.Rows[0]["Payment"].ToString();
                    if (payamt == "" || payamt == "null") payamt = "0";
                    string dueamt = dtv.Rows[0]["Dueamt"].ToString();
                    if (dueamt == "" || dueamt == "null") dueamt = "0";
                    all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                    all1[0].rPayment = (Convert.ToDecimal(payamt) + Convert.ToDecimal(dueamt)).ToString();
                    all1[0].rAccount = dtv.Rows[0]["Account"].ToString();
                    all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                    all1[0].amttype = dtv.Rows[0]["amtin"].ToString();
                    List<SelectListItem> Amtin = new List<SelectListItem>();
                    SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dta = new DataTable();
                    cmda.Fill(dta);
                    for (int dd = 0; dd < dta.Rows.Count; dd++)
                    {
                        Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: all1[0].amttype);
                    List<SelectListItem> Attendedby = new List<SelectListItem>();
                    SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                    DataTable dtat = new DataTable();
                    cmdat.Fill(dtat);
                    for (int dd = 0; dd < dtat.Rows.Count; dd++)
                    {
                        Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: dtv.Rows[0]["Attendedby"].ToString());
                    //DateFormat();
                    return View(all1);
                }
                else
                {
                    List<SelectListItem> Customer = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                    List<SelectListItem> Amtin = new List<SelectListItem>();
                    SqlDataAdapter cmda = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                    DataTable dta = new DataTable();
                    cmda.Fill(dta);
                    for (int dd = 0; dd < dta.Rows.Count; dd++)
                    {
                        Amtin.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: Session["TaxInclusive"]);
                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");
                    List<SelectListItem> SalesReceipt = new List<SelectListItem>();
                    SqlDataAdapter cmddls = new SqlDataAdapter("select COLUMN02, COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdatas = new DataTable();
                    cmddls.Fill(dtdatas);
                    for (int dd = 0; dd < dtdatas.Rows.Count; dd++)
                    {
                        SalesReceipt.Add(new SelectListItem { Value = dtdatas.Rows[dd]["COLUMN02"].ToString(), Text = dtdatas.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["SalesReceipt"] = new SelectList(SalesReceipt, "Value", "Text");
                    List<SelectListItem> Attendedby = new List<SelectListItem>();
                    SqlDataAdapter cmdat = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                    DataTable dtat = new DataTable();
                    cmdat.Fill(dtat);
                    var salesrep = "";
                    for (int dd = 0; dd < dtat.Rows.Count; dd++)
                    {
                        if (Convert.ToString(Session["eid"]) == dtat.Rows[dd]["COLUMN02"].ToString())
                            salesrep = dtat.Rows[dd]["COLUMN02"].ToString();
                        Attendedby.Add(new SelectListItem { Value = dtat.Rows[dd]["COLUMN02"].ToString(), Text = dtat.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: salesrep);
                    //DateFormat();
                    return View();
                }
            }

            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult ReturnReceiptView0()
        {
            try
            {
                var ReceiptID = Request["ide"];
                SqlCommand cmd = new SqlCommand("usp_SAR_TP_RR_HEADER_DETAILS", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReceiptID", ReceiptID));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                SqlCommand cmd1 = new SqlCommand("usp_SAR_TP_RR_LINE_DETAILS", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@ReceiptID", ReceiptID));
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da1.Fill(dt);
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var strqry = "SELECT b.COLUMN03 Item, b.COLUMN04 UPC,b.COLUMN05 'Item Desc',b.COLUMN07 Quantity,b.COLUMN27 Units,b.COLUMN08 'Shipping Instructions',b.COLUMN26 Tax,b.COLUMN09 Price,b.COLUMN25 Amount,b.COLUMN11 Total,(cast(((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*cast(isnull(b.COLUMN25,0) as decimal(18,2))) as decimal(18,2))) aCOLUMN25,isnull(m.column07,0) aCOLUMN26 FROM SATABLE005 a inner join SATABLE006 b on a.COLUMN01=b.COLUMN19 and isnull(b.COLUMNA13,0)=0 left join MATABLE013 m on  m.column02=b.COLUMN21 WHERE  a.COLUMN02= "+ReceiptID+" and isnull(a.COLUMNA13,0)=0 ";
                var sql = strqry;
                var GData = dbs.Query(sql);
                ViewBag.itemsdata = GData;
                ArrayList icols = new ArrayList();
                ArrayList ucols = new ArrayList();
                ArrayList tcols = new ArrayList();
                 foreach(var row in GData){
                     icols.Add(row.Item);
                     ucols.Add(row.Units);
                     tcols.Add(row.Tax);
                }
                 ViewBag.iitemsdata = icols;
                 ViewBag.uitemsdata = ucols;
                 ViewBag.titemsdata = tcols;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    CreditMemo="",
                    HCustomer = "",
                    OrderStatus="",   
                    Approval="",   
                    Date = "",
                    ReceiptType="",   
                    HReceipt = "",
                    Terms = "",
                    HPaymentMode = "",
                    Operating_Unit = "",
                    HDepartment = "",
                    Reference = "",
                    HTaxType = "",
                    Notes = "",
                    HProject = "",
                    Subtotal = "",
                    DiscountAmount = "",
                    TaxAmount = "",
                    TotalAmount = "",
                    Addressee = "",
                    Address1 = "",
                    Address2 = "",
                    HCountry = "",
                    HState = "",
                    City = "",
                    Zip = "",
                    Billing = "",
                    Shipping = "",
                    location = ""
                });
                if (dt1.Rows.Count > 0)
                {
                    all1[0].CreditMemo=dt1.Rows[0]["COLUMN04"].ToString();
                    all1[0].HCustomer =dt1.Rows[0]["COLUMN05"].ToString();
                    all1[0].OrderStatus=dt1.Rows[0]["COLUMN16"].ToString();
                    all1[0].Approval=dt1.Rows[0]["COLUMN07"].ToString();
                    all1[0].Date = dt1.Rows[0]["COLUMN06"].ToString();
                    all1[0].ReceiptType=dt1.Rows[0]["COLUMN29"].ToString();
                    all1[0].HReceipt =dt1.Rows[0]["COLUMN33"].ToString();
                    all1[0].Terms = dt1.Rows[0]["COLUMN10"].ToString();
                    all1[0].HPaymentMode =dt1.Rows[0]["COLUMN30"].ToString();
                    all1[0].Operating_Unit =dt1.Rows[0]["COLUMN24"].ToString();
                    all1[0].HDepartment = dt1.Rows[0]["COLUMN27"].ToString();
                    all1[0].Reference = dt1.Rows[0]["COLUMN11"].ToString();
                    all1[0].HTaxType = dt1.Rows[0]["COLUMN31"].ToString();
                    all1[0].Notes = dt1.Rows[0]["COLUMN09"].ToString();
                    all1[0].HProject = dt1.Rows[0]["COLUMN35"].ToString();
                    all1[0].Subtotal = dt1.Rows[0]["COLUMN12"].ToString();
                    //all1[0].DiscountAmount = dt1.Rows[0]["COLUMN33"].ToString();
                    all1[0].TaxAmount = dt1.Rows[0]["COLUMN32"].ToString();
                    all1[0].TotalAmount =dt1.Rows[0]["COLUMN15"].ToString();
                    all1[0].Addressee = dt1.Rows[0]["addresse"].ToString();
                    all1[0].Address1 = dt1.Rows[0]["add1"].ToString();
                    all1[0].Address2 = dt1.Rows[0]["add2"].ToString();
                    all1[0].HCountry = dt1.Rows[0]["country"].ToString();
                    all1[0].HState = dt1.Rows[0]["state"].ToString();
                    all1[0].City = dt1.Rows[0]["city"].ToString();
                    all1[0].Zip = dt1.Rows[0]["zip"].ToString();
                    all1[0].Billing = dt1.Rows[0]["bill"].ToString();
                    all1[0].Shipping = dt1.Rows[0]["shipp"].ToString();
                    //all1[0].location = dt1.Rows[0]["COLUMN33"].ToString();
                }

                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text",selectedValue:all1[0].HCustomer);
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                ViewBag.Item = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Units = new List<SelectListItem>();
                SqlDataAdapter cmdu = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11119 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtu = new DataTable();
                cmdu.Fill(dtu);
                for (int dd = 0; dd < dtu.Rows.Count; dd++)
                {
                    Units.Add(new SelectListItem { Value = dtu.Rows[dd]["COLUMN02"].ToString(), Text = dtu.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Units"] = new SelectList(Units, "Value", "Text");
                List<SelectListItem> Lot = new List<SelectListItem>();
                SqlDataAdapter cmdl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE043  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtl = new DataTable();
                cmdl.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Lot.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Lot"] = new SelectList(Lot, "Value", "Text");
                List<SelectListItem> HTaxType = new List<SelectListItem>();
                SqlDataAdapter cmdht = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                DataTable dtht = new DataTable();
                cmdht.Fill(dtht);
                for (int dd = 0; dd < dtht.Rows.Count; dd++)
                {
                    HTaxType.Add(new SelectListItem { Value = dtht.Rows[dd]["COLUMN02"].ToString(), Text = dtht.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Tax Type"] = new SelectList(HTaxType, "Value", "Text", selectedValue: all1[0].HTaxType);
                List<SelectListItem> TaxType = new List<SelectListItem>();
                SqlDataAdapter cmdt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                DataTable dtt = new DataTable();
                cmdt.Fill(dtt);
                for (int dd = 0; dd < dtt.Rows.Count; dd++)
                {
                    TaxType.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                List<SelectListItem> Receipt = new List<SelectListItem>();
                SqlDataAdapter cmdr = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE009  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and  isnull(COLUMNA13,'False')='False' order by column02 desc", cn);
                DataTable dtr = new DataTable();
                cmdr.Fill(dtr);
                for (int dd = 0; dd < dtr.Rows.Count; dd++)
                {
                    Receipt.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Receipt#"] = new SelectList(Receipt, "Value", "Text", selectedValue: all1[0].HReceipt);
                List<SelectListItem> ReceiptType = new List<SelectListItem>();
                SqlDataAdapter cmdrt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE011  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMN12,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtrt = new DataTable();
                cmdrt.Fill(dtrt);
                for (int dd = 0; dd < dtrt.Rows.Count; dd++)
                {
                    ReceiptType.Add(new SelectListItem { Value = dtrt.Rows[dd]["COLUMN02"].ToString(), Text = dtrt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Receipt Type"] = new SelectList(ReceiptType, "Value", "Text", selectedValue: all1[0].ReceiptType);
                List<SelectListItem> PaymentTerms = new List<SelectListItem>();
                SqlDataAdapter cmdp = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11122 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtp = new DataTable();
                cmdp.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    PaymentTerms.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Payment Terms"] = new SelectList(PaymentTerms, "Value", "Text", selectedValue: all1[0].Terms);
                List<SelectListItem> PaymentMode = new List<SelectListItem>();
                SqlDataAdapter cmdm = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11123 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtm = new DataTable();
                cmdm.Fill(dtm);
                for (int dd = 0; dd < dtm.Rows.Count; dd++)
                {
                    PaymentMode.Add(new SelectListItem { Value = dtm.Rows[dd]["COLUMN02"].ToString(), Text = dtm.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Payment Mode"] = new SelectList(PaymentMode, "Value", "Text", selectedValue: all1[0].PaymentMode);
                List<SelectListItem> OperatingUnit = new List<SelectListItem>();
                SqlDataAdapter cmdo = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from CONTABLE007  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMN07,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dto = new DataTable();
                cmdo.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    OperatingUnit.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Operating Unit"] = new SelectList(OperatingUnit, "Value", "Text", selectedValue: all1[0].Operating_Unit);
                List<SelectListItem> Department = new List<SelectListItem>();
                SqlDataAdapter cmdd = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11117 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtd = new DataTable();
                cmdd.Fill(dtd);
                for (int dd = 0; dd < dtd.Rows.Count; dd++)
                {
                    Department.Add(new SelectListItem { Value = dtd.Rows[dd]["COLUMN02"].ToString(), Text = dtd.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Department"] = new SelectList(Department, "Value", "Text", selectedValue: all1[0].HDepartment);
                List<SelectListItem> Project = new List<SelectListItem>();
                SqlDataAdapter cmdpr = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtpr = new DataTable();
                cmdpr.Fill(dtpr);
                for (int dd = 0; dd < dtpr.Rows.Count; dd++)
                {
                    Project.Add(new SelectListItem { Value = dtpr.Rows[dd]["COLUMN02"].ToString(), Text = dtpr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Project#"] = new SelectList(Project, "Value", "Text", selectedValue: all1[0].HProject);
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlDataAdapter cmdc = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from MATABLE016  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtc = new DataTable();
                cmdc.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Country"] = new SelectList(Country, "Value", "Text", selectedValue: all1[0].HCountry);
                List<SelectListItem> State = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from MATABLE017  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    State.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["State"] = new SelectList(State, "Value", "Text", selectedValue: all1[0].HState);
                DateFormat();
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult ReturnReceiptEdito()
        {
            try
            {
                var ReceiptID = Request["ide"];
                SqlCommand cmd = new SqlCommand("usp_SAR_TP_RR_HEADER_DETAILS", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReceiptID", ReceiptID));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                SqlCommand cmd1 = new SqlCommand("usp_SAR_TP_RR_LINE_DETAILS", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@ReceiptID", ReceiptID));
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da1.Fill(dt);
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var strqry = "SELECT b.COLUMN03 Item, b.COLUMN04 UPC,b.COLUMN05 'Item Desc',b.COLUMN07 Quantity,b.COLUMN27 Units,b.COLUMN08 'Shipping Instructions',b.COLUMN26 Tax,b.COLUMN09 Price,b.COLUMN25 Amount,b.COLUMN11 Total,b.COLUMN02 ID,(cast(((cast(isnull(m.column07,0) as decimal(18,2))*(0.01))*cast(isnull(b.COLUMN25,0) as decimal(18,2))) as decimal(18,2))) aCOLUMN25,isnull(m.column07,0) aCOLUMN26 FROM SATABLE005 a inner join SATABLE006 b on a.COLUMN01=b.COLUMN19 and isnull(b.COLUMNA13,0)=0 left join MATABLE013 m on  m.column02=b.COLUMN21 WHERE  a.COLUMN02= " + ReceiptID + " and isnull(a.COLUMNA13,0)=0 ";
                var sql = strqry;
                var GData = dbs.Query(sql);
                ViewBag.itemsdata = GData;
                ArrayList icols = new ArrayList();
                ArrayList ucols = new ArrayList();
                ArrayList tcols = new ArrayList();
                foreach (var row in GData)
                {
                    icols.Add(row.Item);
                    ucols.Add(row.Units);
                    tcols.Add(row.Tax);
                }
                ViewBag.iitemsdata = icols;
                ViewBag.uitemsdata = ucols;
                ViewBag.titemsdata = tcols;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    CreditMemo = "",
                    HCustomer = "",
                    OrderStatus = "",
                    Approval = "",
                    Date = "",
                    ReceiptType = "",
                    HReceipt = "",
                    Terms = "",
                    HPaymentMode = "",
                    Operating_Unit = "",
                    HDepartment = "",
                    Reference = "",
                    HTaxType = "",
                    Notes = "",
                    HProject = "",
                    Subtotal = "",
                    DiscountAmount = "",
                    TaxAmount = "",
                    TotalAmount = "",
                    Addressee = "",
                    Address1 = "",
                    Address2 = "",
                    HCountry = "",
                    HState = "",
                    City = "",
                    Zip = "",
                    Billing = "",
                    Shipping = "",
                    location = ""
                });
                if (dt1.Rows.Count > 0)
                {
                    all1[0].CreditMemo = dt1.Rows[0]["COLUMN04"].ToString();
                    all1[0].HCustomer = dt1.Rows[0]["COLUMN05"].ToString();
                    all1[0].OrderStatus = dt1.Rows[0]["COLUMN16"].ToString();
                    all1[0].Approval = dt1.Rows[0]["COLUMN07"].ToString();
                    all1[0].Date = dt1.Rows[0]["COLUMN06"].ToString();
                    all1[0].ReceiptType = dt1.Rows[0]["COLUMN29"].ToString();
                    all1[0].HReceipt = dt1.Rows[0]["COLUMN33"].ToString();
                    all1[0].Terms = dt1.Rows[0]["COLUMN10"].ToString();
                    all1[0].HPaymentMode = dt1.Rows[0]["COLUMN30"].ToString();
                    all1[0].Operating_Unit = dt1.Rows[0]["COLUMN24"].ToString();
                    all1[0].HDepartment = dt1.Rows[0]["COLUMN27"].ToString();
                    all1[0].Reference = dt1.Rows[0]["COLUMN11"].ToString();
                    all1[0].HTaxType = dt1.Rows[0]["COLUMN31"].ToString();
                    all1[0].Notes = dt1.Rows[0]["COLUMN09"].ToString();
                    all1[0].HProject = dt1.Rows[0]["COLUMN35"].ToString();
                    all1[0].Subtotal = dt1.Rows[0]["COLUMN12"].ToString();
                    //all1[0].DiscountAmount = dt1.Rows[0]["COLUMN33"].ToString();
                    all1[0].TaxAmount = dt1.Rows[0]["COLUMN32"].ToString();
                    all1[0].TotalAmount = dt1.Rows[0]["COLUMN15"].ToString();
                    all1[0].Addressee = dt1.Rows[0]["addresse"].ToString();
                    all1[0].Address1 = dt1.Rows[0]["add1"].ToString();
                    all1[0].Address2 = dt1.Rows[0]["add2"].ToString();
                    all1[0].HCountry = dt1.Rows[0]["country"].ToString();
                    all1[0].HState = dt1.Rows[0]["state"].ToString();
                    all1[0].City = dt1.Rows[0]["city"].ToString();
                    all1[0].Zip = dt1.Rows[0]["zip"].ToString();
                    all1[0].Billing = dt1.Rows[0]["bill"].ToString();
                    all1[0].Shipping = dt1.Rows[0]["shipp"].ToString();
                    //all1[0].location = dt1.Rows[0]["COLUMN33"].ToString();
                }

                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: all1[0].HCustomer);
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where   COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                ViewBag.Item = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Units = new List<SelectListItem>();
                SqlDataAdapter cmdu = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11119 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtu = new DataTable();
                cmdu.Fill(dtu);
                for (int dd = 0; dd < dtu.Rows.Count; dd++)
                {
                    Units.Add(new SelectListItem { Value = dtu.Rows[dd]["COLUMN02"].ToString(), Text = dtu.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Units"] = new SelectList(Units, "Value", "Text");
                List<SelectListItem> Lot = new List<SelectListItem>();
                SqlDataAdapter cmdl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE043  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtl = new DataTable();
                cmdl.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Lot.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Lot"] = new SelectList(Lot, "Value", "Text");
                List<SelectListItem> HTaxType = new List<SelectListItem>();
                SqlDataAdapter cmdht = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                DataTable dtht = new DataTable();
                cmdht.Fill(dtht);
                for (int dd = 0; dd < dtht.Rows.Count; dd++)
                {
                    HTaxType.Add(new SelectListItem { Value = dtht.Rows[dd]["COLUMN02"].ToString(), Text = dtht.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Tax Type"] = new SelectList(HTaxType, "Value", "Text", selectedValue: all1[0].HTaxType);
                List<SelectListItem> TaxType = new List<SelectListItem>();
                SqlDataAdapter cmdt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                DataTable dtt = new DataTable();
                cmdt.Fill(dtt);
                for (int dd = 0; dd < dtt.Rows.Count; dd++)
                {
                    TaxType.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                List<SelectListItem> Receipt = new List<SelectListItem>();
                SqlDataAdapter cmdr = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE009  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and  isnull(COLUMNA13,'False')='False' order by column02 desc", cn);
                DataTable dtr = new DataTable();
                cmdr.Fill(dtr);
                for (int dd = 0; dd < dtr.Rows.Count; dd++)
                {
                    Receipt.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Receipt#"] = new SelectList(Receipt, "Value", "Text", selectedValue: all1[0].HReceipt);
                List<SelectListItem> ReceiptType = new List<SelectListItem>();
                SqlDataAdapter cmdrt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE011  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMN12,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtrt = new DataTable();
                cmdrt.Fill(dtrt);
                for (int dd = 0; dd < dtrt.Rows.Count; dd++)
                {
                    ReceiptType.Add(new SelectListItem { Value = dtrt.Rows[dd]["COLUMN02"].ToString(), Text = dtrt.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Receipt Type"] = new SelectList(ReceiptType, "Value", "Text", selectedValue: all1[0].ReceiptType);
                List<SelectListItem> PaymentTerms = new List<SelectListItem>();
                SqlDataAdapter cmdp = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11122 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtp = new DataTable();
                cmdp.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    PaymentTerms.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Payment Terms"] = new SelectList(PaymentTerms, "Value", "Text", selectedValue: all1[0].Terms);
                List<SelectListItem> PaymentMode = new List<SelectListItem>();
                SqlDataAdapter cmdm = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11123 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtm = new DataTable();
                cmdm.Fill(dtm);
                for (int dd = 0; dd < dtm.Rows.Count; dd++)
                {
                    PaymentMode.Add(new SelectListItem { Value = dtm.Rows[dd]["COLUMN02"].ToString(), Text = dtm.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Payment Mode"] = new SelectList(PaymentMode, "Value", "Text", selectedValue: all1[0].PaymentMode);
                List<SelectListItem> OperatingUnit = new List<SelectListItem>();
                SqlDataAdapter cmdo = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from CONTABLE007  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMN07,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dto = new DataTable();
                cmdo.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    OperatingUnit.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Operating Unit"] = new SelectList(OperatingUnit, "Value", "Text", selectedValue: all1[0].Operating_Unit);
                List<SelectListItem> Department = new List<SelectListItem>();
                SqlDataAdapter cmdd = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  Where COLUMN03 =11117 and COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtd = new DataTable();
                cmdd.Fill(dtd);
                for (int dd = 0; dd < dtd.Rows.Count; dd++)
                {
                    Department.Add(new SelectListItem { Value = dtd.Rows[dd]["COLUMN02"].ToString(), Text = dtd.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Department"] = new SelectList(Department, "Value", "Text", selectedValue: all1[0].HDepartment);
                List<SelectListItem> Project = new List<SelectListItem>();
                SqlDataAdapter cmdpr = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  Where COLUMN04 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtpr = new DataTable();
                cmdpr.Fill(dtpr);
                for (int dd = 0; dd < dtpr.Rows.Count; dd++)
                {
                    Project.Add(new SelectListItem { Value = dtpr.Rows[dd]["COLUMN02"].ToString(), Text = dtpr.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Project#"] = new SelectList(Project, "Value", "Text", selectedValue: all1[0].HProject);
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlDataAdapter cmdc = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from MATABLE016  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtc = new DataTable();
                cmdc.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Country"] = new SelectList(Country, "Value", "Text", selectedValue: all1[0].HCountry);
                List<SelectListItem> State = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN03 COLUMN04 from MATABLE017  Where COLUMN03 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    State.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["State"] = new SelectList(State, "Value", "Text", selectedValue: all1[0].HState);
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        [HttpPost]
        public ActionResult ReturnReceiptEdit(FormCollection fc)
        {
            try
            {
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["RRGridData"];
                string AOwner = Convert.ToString(Session["AcOwner"]);
                var Date = DateTime.Now.ToShortDateString();
                string insert = "Update", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                cn.Open();
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  Max(COLUMN01) from SATABLE005 where COLUMN02="+HCol2+"", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol1 = dtt.Rows[0][0].ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1603");
                Cmd.Parameters.AddWithValue("@COLUMN04", fc[0]);
                Cmd.Parameters.AddWithValue("@COLUMN05", fc[1]);
                Cmd.Parameters.AddWithValue("@COLUMN16", fc[2]);
                Cmd.Parameters.AddWithValue("@COLUMN06", fc[3]);
                Cmd.Parameters.AddWithValue("@COLUMN33", fc[4]);
                Cmd.Parameters.AddWithValue("@COLUMN29", fc[5]);
                Cmd.Parameters.AddWithValue("@COLUMN10", fc[6]);
                Cmd.Parameters.AddWithValue("@COLUMN30", fc[7]);
                Cmd.Parameters.AddWithValue("@COLUMN24", fc[8]);
                Cmd.Parameters.AddWithValue("@COLUMN27", fc[9]);
                Cmd.Parameters.AddWithValue("@COLUMN11", fc[10]);
                Cmd.Parameters.AddWithValue("@COLUMN31", fc[11]);
                Cmd.Parameters.AddWithValue("@COLUMN09", fc[12]);
                Cmd.Parameters.AddWithValue("@COLUMN35", fc[13]);
                Cmd.Parameters.AddWithValue("@COLUMN12", fc[14]);
                Cmd.Parameters.AddWithValue("@COLUMN32", fc[15]);
                Cmd.Parameters.AddWithValue("@COLUMN15", fc[17]);
                Cmd.Parameters.AddWithValue("@COLUMN50", fc["Amtin"]);
                Cmd.Parameters.AddWithValue("@COLUMNA02", fc[8]);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE005");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                int r = Cmd.ExecuteNonQuery();
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE006 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string rowid = itemdata.Tables[0].Rows[i]["rowid"].ToString();
                    if (rowid == "0") rowid = LCol2;
                    string Item = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN03"].ToString();
                    string Upc = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN04"].ToString();
                    string Itemdesc = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN05"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN07"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN27"].ToString();
                    string Instructions = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN08"].ToString();
                    string TaxType = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN26"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN09"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN25"].ToString();
                    string Total = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN11"].ToString();
                    if (Units == "0" || Units == "" || Units == null) Units = "10000";
                    if (TaxType == "0" || TaxType == "" || TaxType == null) TaxType = "1000";
                    SqlCommand Cmdl = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                    Cmdl.CommandType = CommandType.StoredProcedure;
                    Cmdl.Parameters.AddWithValue("@COLUMN02", rowid);
                    Cmdl.Parameters.AddWithValue("@COLUMN03", Item);
                    Cmdl.Parameters.AddWithValue("@COLUMN04", Upc);
                    Cmdl.Parameters.AddWithValue("@COLUMN05", Itemdesc);
                    Cmdl.Parameters.AddWithValue("@COLUMN07", lQty);
                    Cmdl.Parameters.AddWithValue("@COLUMN27", Units);
                    Cmdl.Parameters.AddWithValue("@COLUMN08", Instructions);
                    Cmdl.Parameters.AddWithValue("@COLUMN26", TaxType);
                    Cmdl.Parameters.AddWithValue("@COLUMN09", Price);
                    Cmdl.Parameters.AddWithValue("@COLUMN25", Amount);
                    Cmdl.Parameters.AddWithValue("@COLUMN11", Total);
                    Cmdl.Parameters.AddWithValue("@COLUMN19", HCol1);
                    Cmdl.Parameters.AddWithValue("@COLUMNA02", fc[8]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmdl.Parameters.AddWithValue("@Direction", insert);
                    Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE006");
                    Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                    l = Cmdl.ExecuteNonQuery();
                }
                SqlCommand cmd1 = new SqlCommand("select  Max(COLUMN02) from SATABLE003 where COLUMN20=" + HCol1 + " and COLUMN19='SALESORDER'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                int HColc =Convert.ToInt32( dt1.Rows[0][0].ToString());
                if (HColc > 0)
                {
                    SqlCommand Cmdc = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                    Cmdc.CommandType = CommandType.StoredProcedure;
                    Cmdc.Parameters.AddWithValue("@COLUMN02", HColc);
                    Cmdc.Parameters.AddWithValue("@COLUMN06", fc["Addressee"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN07", fc["Address1"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN08", fc["Address2"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN16", fc["Country"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN11", fc["State"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN10", fc["City"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN12", fc["Zip"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN17", fc["Billing"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN18", fc["Shipping"]);
                    Cmdc.Parameters.AddWithValue("@COLUMN19", "SALESORDER");
                    Cmdc.Parameters.AddWithValue("@COLUMN20", HCol1);
                    Cmdc.Parameters.AddWithValue("@COLUMN21", fc[8]);
                    Cmdc.Parameters.AddWithValue("@COLUMNA02", fc[8]);
                    Cmdc.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdc.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdc.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdc.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdc.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdc.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmdc.Parameters.AddWithValue("@Direction", insert);
                    Cmdc.Parameters.AddWithValue("@TabelName", "SATABLE003");
                    Cmdc.Parameters.AddWithValue("@ReturnValue", "");
                    int c = Cmdc.ExecuteNonQuery();
                }
                if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1329 && q.COLUMN05 == 2).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                cn.Close();
                Session["RRGridData"] = null;
                return RedirectToAction("ReturnReceiptInfo", "ReturnReceipt", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult ReturnReceiptDelete()
        {
            try
            {
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                cn.Open();
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from SATABLE005 where COLUMN02="+HCol2+"", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                string Date = DateTime.Now.ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OPunit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE005");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                int r = Cmd.ExecuteNonQuery();
                //SqlCommand cmd1 = new SqlCommand("select  Max(COLUMN02) from SATABLE003 where COLUMN20=" + HCol1 + " and COLUMN19='SALESORDER'", cn);
                //SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                //DataTable dt1 = new DataTable();
                //da1.Fill(dt1);
                //int HColc =Convert.ToInt32( dt1.Rows[0][0].ToString());
                //if (HColc > 0)
                //{
                //    SqlCommand Cmdc = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                //    Cmdc.CommandType = CommandType.StoredProcedure;
                //    Cmdc.Parameters.AddWithValue("@COLUMN02", HColc);
                //    Cmdc.Parameters.AddWithValue("@COLUMN20", HCol1);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA02", OPunit);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA03", AOwner);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA06", Date);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA07", Date);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA12", 1);
                //    Cmdc.Parameters.AddWithValue("@COLUMNA13", 1);
                //    Cmdc.Parameters.AddWithValue("@Direction", insert);
                //    Cmdc.Parameters.AddWithValue("@TabelName", "SATABLE003");
                //    Cmdc.Parameters.AddWithValue("@ReturnValue", "");
                //    int c = Cmdc.ExecuteNonQuery();
                //}
                if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1329 && q.COLUMN05 == 4).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                cn.Close();
                return RedirectToAction("ReturnReceiptInfo", "ReturnReceipt", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono = "", TransNo = "";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (frmIDchk == 1277)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "IV";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                else if (frmIDchk == 1278)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "CP";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                else if (frmIDchk == 1603)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE005  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE005  Where  COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "RR";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE005  Where  COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE005  Where  COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                pacmd.ExecuteNonQuery();
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }

        public ActionResult getCreditMemoDetails(int ReceiptID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("usp_SAR_TP_RR_HEADER_DATA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ReceiptID", ReceiptID));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                SqlCommand cmd1 = new SqlCommand("usp_SAR_TP_RR_LINE_DATA", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@ReceiptID", ReceiptID));
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da1.Fill(dt);
                var Customer = "";
                var Date = "";
                var Receipt = "";
                var Terms = "";
                var PaymentMode = "";
                var Operating_Unit = "";
                var Department = "";
                var Reference = "";
                var TaxType = "";
                var Notes = "";
                var Project = "";
                var Subtotal = "";
                var DiscountAmount = "";
                var TaxAmount = "";
                var TotalAmount = "";
                var Addressee = "";
                var Address1 = "";
                var Address2 = "";
                var Country = "";
                var State = "";
                var City = "";
                var Zip = "";
                var Billing = "";
                var Shipping = "";
                var location = "";
                List<Dictionary<string, object>> JSONString=null; 
                if (dt1.Rows.Count > 0)
                {
                    Customer = dt1.Rows[0]["Customer"].ToString();
                    Terms = dt1.Rows[0]["Terms"].ToString();
                    PaymentMode = dt1.Rows[0]["Mode"].ToString();
                    Operating_Unit = dt1.Rows[0]["ou"].ToString();
                    Department = dt1.Rows[0]["dept"].ToString();
                    Reference = dt1.Rows[0]["Ref"].ToString();
                    TaxType = dt1.Rows[0]["taxtype"].ToString();
                    Notes = dt1.Rows[0]["notes"].ToString();
                    Project = dt1.Rows[0]["project"].ToString();
                    Subtotal = dt1.Rows[0]["subtotal"].ToString();
                    TaxAmount = dt1.Rows[0]["taxamt"].ToString();
                    TotalAmount = dt1.Rows[0]["totamt"].ToString();
                    Addressee = dt1.Rows[0]["addresse"].ToString();
                    Address1 = dt1.Rows[0]["add1"].ToString();
                    Address2 = dt1.Rows[0]["add2"].ToString();
                    Country = dt1.Rows[0]["country"].ToString();
                    State = dt1.Rows[0]["state"].ToString();
                    City = dt1.Rows[0]["city"].ToString();
                    Zip = dt1.Rows[0]["zip"].ToString();
                    Billing = dt1.Rows[0]["bill"].ToString();
                    Shipping = dt1.Rows[0]["shipp"].ToString();
                }
                if (dt.Rows.Count > 0)
                    JSONString = GetTableRows(dt);
                return Json(new
                {
                    Data1 = Customer,
                    Data2 = Terms,
                    Data3 = PaymentMode,
                    Data4 = Operating_Unit,
                    Data5 = Department,
                    Data6 = Reference,
                    Data7 = TaxType,
                    Data8 = Notes,
                    Data9 = Project,
                    Data10 = Subtotal,
                    Data11 = TaxAmount,
                    Data12 = TotalAmount,
                    Data13 = Addressee,
                    Data14 = Address1,
                    Data15 = Address2,
                    Data16 = Country,
                    Data17 = State,
                    Data18 = City,
                    Data19 = Zip,
                    Data20 = Billing,
                    Data21 = Shipping,
                    Data30 = JSONString
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public List<Dictionary<string, object>> GetTableRows(DataTable dtData)
        {
            try
            {
                List<Dictionary<string, object>>
                lstRows = new List<Dictionary<string, object>>();
                Dictionary<string, object> dictRow = null;
                foreach (DataRow dr in dtData.Rows)
                {
                    dictRow = new Dictionary<string, object>();
                    foreach (DataColumn col in dtData.Columns)
                    {
                        dictRow.Add(col.ColumnName, dr[col]);
                    }
                    lstRows.Add(dictRow);
                }
                return lstRows;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }

        [HttpPost]
        public ActionResult RRSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["RRGridData"] = ds;
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        [HttpPost]
        public ActionResult NewTransaction(FormCollection fc)
        {
            try
            {
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["RRGridData"];
                string AOwner = Convert.ToString(Session["AcOwner"]);
                var Date = DateTime.Now.ToShortDateString();
                string insert = "Insert", TransNo = "";int l = 0;
                cn.Open();
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE005 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1603");
                Cmd.Parameters.AddWithValue("@COLUMN04", fc[0]);
                Cmd.Parameters.AddWithValue("@COLUMN05", fc[1]);
                Cmd.Parameters.AddWithValue("@COLUMN16", fc[2]);
                Cmd.Parameters.AddWithValue("@COLUMN06", fc[3]);
                Cmd.Parameters.AddWithValue("@COLUMN33", fc[4]);
                Cmd.Parameters.AddWithValue("@COLUMN29", fc[5]);
                Cmd.Parameters.AddWithValue("@COLUMN10", fc[6]);
                Cmd.Parameters.AddWithValue("@COLUMN30", fc[7]);
                Cmd.Parameters.AddWithValue("@COLUMN24", fc[8]);
                Cmd.Parameters.AddWithValue("@COLUMN27", fc[9]);
                Cmd.Parameters.AddWithValue("@COLUMN11", fc[10]);
                Cmd.Parameters.AddWithValue("@COLUMN31", fc[11]);
                Cmd.Parameters.AddWithValue("@COLUMN09", fc[12]);
                Cmd.Parameters.AddWithValue("@COLUMN35", fc[13]);
                Cmd.Parameters.AddWithValue("@COLUMN12", fc[14]);
                Cmd.Parameters.AddWithValue("@COLUMN32", fc[15]);
                Cmd.Parameters.AddWithValue("@COLUMN15", fc[17]);
                Cmd.Parameters.AddWithValue("@COLUMN50", fc["Amtin"]);
                Cmd.Parameters.AddWithValue("@COLUMNA02", fc[8]);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE005");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                int r = Cmd.ExecuteNonQuery();
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1603).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                if (r > 0)
                {
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                }
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE006 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string Item = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN03"].ToString();
                    string Upc = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN04"].ToString();
                    string Itemdesc = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN05"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN07"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN27"].ToString();
                    string Instructions = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN08"].ToString();
                    string TaxType = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN26"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN09"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN25"].ToString();
                    string Total = itemdata.Tables[0].Rows[i]["SATABLE006COLUMN11"].ToString();
                    if (Units == "0" || Units == "" || Units == null) Units = "10000";
                    if (TaxType == "0" || TaxType == "" || TaxType == null) TaxType = "1000";
                    SqlCommand Cmdl = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                    Cmdl.CommandType = CommandType.StoredProcedure;
                    Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                    Cmdl.Parameters.AddWithValue("@COLUMN03", Item);
                    Cmdl.Parameters.AddWithValue("@COLUMN04", Upc);
                    Cmdl.Parameters.AddWithValue("@COLUMN05", Itemdesc);
                    Cmdl.Parameters.AddWithValue("@COLUMN07", lQty);
                    Cmdl.Parameters.AddWithValue("@COLUMN27", Units);
                    Cmdl.Parameters.AddWithValue("@COLUMN08", Instructions);
                    Cmdl.Parameters.AddWithValue("@COLUMN26", TaxType);
                    Cmdl.Parameters.AddWithValue("@COLUMN09", Price);
                    Cmdl.Parameters.AddWithValue("@COLUMN25", Amount);
                    Cmdl.Parameters.AddWithValue("@COLUMN11", Total);
                    Cmdl.Parameters.AddWithValue("@COLUMNA02", fc[8]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmdl.Parameters.AddWithValue("@Direction", insert);
                    Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE006");
                    Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                    l = Cmdl.ExecuteNonQuery();
                }
                SqlCommand cmdc = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE003 ", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                string HColc = dtc.Rows[0][0].ToString();
                HColc = (Convert.ToInt32(HColc) + 1).ToString(); 
                SqlCommand cmd1 = new SqlCommand("select  Max(COLUMN01) from SATABLE005 where COLUMN02=" + HCol2 + "", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                string HCol1 = dt1.Rows[0][0].ToString();
                SqlCommand Cmdc = new SqlCommand("usp_SAR_BL_RR_RETURNRECEIPT", cn);
                Cmdc.CommandType = CommandType.StoredProcedure;
                Cmdc.Parameters.AddWithValue("@COLUMN02", HColc);
                Cmdc.Parameters.AddWithValue("@COLUMN06", fc["Addressee"]);
                Cmdc.Parameters.AddWithValue("@COLUMN07", fc["Address1"]);
                Cmdc.Parameters.AddWithValue("@COLUMN08", fc["Address2"]);
                Cmdc.Parameters.AddWithValue("@COLUMN16", fc["Country"]);
                Cmdc.Parameters.AddWithValue("@COLUMN11", fc["State"]);
                Cmdc.Parameters.AddWithValue("@COLUMN10", fc["City"]);
                Cmdc.Parameters.AddWithValue("@COLUMN12", fc["Zip"]);
                Cmdc.Parameters.AddWithValue("@COLUMN17", fc["Billing"]);
                Cmdc.Parameters.AddWithValue("@COLUMN18", fc["Shipping"]);
                Cmdc.Parameters.AddWithValue("@COLUMN19", "SALESORDER");
                Cmdc.Parameters.AddWithValue("@COLUMN20", HCol1);
                Cmdc.Parameters.AddWithValue("@COLUMN21", fc[8]);
                Cmdc.Parameters.AddWithValue("@COLUMNA02", fc[8]);
                Cmdc.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmdc.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmdc.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmdc.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmdc.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmdc.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmdc.Parameters.AddWithValue("@Direction", insert);
                Cmdc.Parameters.AddWithValue("@TabelName", "SATABLE003");
                Cmdc.Parameters.AddWithValue("@ReturnValue", "");
                int c = Cmdc.ExecuteNonQuery();
                cn.Close();
                var idi = "";
                Session["RRGridData"] = null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("ReturnReceiptInfo", "ReturnReceipt", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult GetTaxAmount1(string TaxType, string TaxCal)
        {
            try
            {
                string fid = "1275";
                SqlCommand cmd = new SqlCommand("GetTaxAmount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TaxType", TaxType));
                cmd.Parameters.Add(new SqlParameter("@FormID", fid));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                var Tax = "";
                var Agency = "";
                if (dt1.Rows.Count > 0)
                {
                    Tax = dt1.Rows[0]["COLUMN07"].ToString();
                    Agency = dt1.Rows[0]["COLUMN16"].ToString();
                }
                return Json(new { Tax = Tax, TaxCal = TaxCal,Agency = Agency }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult DiscountTypes(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> DiscountType = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where COLUMN03=11158 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    DiscountType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["DiscountType"] = new SelectList(DiscountType, "Value", "Text");
                return PartialView("DiscountTypes");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult TaxInformation(string ItemId, string Customer, string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> TaxType = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013	where (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                //DataTable dtdata = new DataTable();
                //cmddl.Fill(dtdata);
                //for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                //{
                //    TaxType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                //}
                TaxType = taxlst.TaxList(ItemId, "", Customer, "Sales", Price, "Tax","");
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                return PartialView("TaxInformation");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetTaxAmount(string TaxType, string TaxCal, string FormName, int grp = -1)
        {
            try
            {
                if (Convert.ToInt32(TaxType) < 0) { grp = 0; TaxType = TaxType.Replace("-", ""); }
                string fid = "1603";
                SqlCommand cmd = new SqlCommand("GetTaxAmount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TaxType", TaxType));
                cmd.Parameters.Add(new SqlParameter("@FormID", fid));
                cmd.Parameters.Add(new SqlParameter("@Group", grp));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1); var Tax = ""; var Agency = "";
                if (dt1.Rows.Count > 0)
                {
                    Tax = dt1.Rows[0]["COLUMN07"].ToString();
                    Agency = dt1.Rows[0]["COLUMN16"].ToString();
                }
                return Json(new { Tax = Tax, Agency = Agency }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        public ActionResult GetCustomerDetails(string CustomerId)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("select column02 from SATABLE002 where COLUMN11 like '" + CustomerId + "%' and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0) CustomerId = dt.Rows[0][0].ToString();
                return Json(new { Data = CustomerId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetItemDetails(string ItemID)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("POSItemdetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string htmlstring = null, htmlstring1 = null;
                for (int g = 0; g < dt.Rows.Count; g++)
                {
                    string dvalue = Convert.ToString(dt.Rows[g][dt.Columns[7].ColumnName]);
                    string dddata = "";
                    List<SelectListItem> UOM = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        UOM.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    ViewData["UOM"] = new SelectList(UOM, "Value", "Text", selectedValue: dvalue).ToString();
                    dddata += "<option value=''>--Select--</option>";
                    for (int d = 0; d < UOM.Count; d++)
                    {
                        if (UOM[d].Value == dvalue)
                            dddata += "<option value=" + UOM[d].Value + " selected>" + UOM[d].Text + "</option>";
                        else
                            dddata += "<option value=" + UOM[d].Value + ">" + UOM[d].Text + "</option>";
                    }
                    htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dItemId>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dItemName>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><select   class ='gridddl edit-mode'  name='" + dt.Columns[2].ColumnName + "'   id=dynamicuom  >" + dddata + "</select></td>";
                    //htmlstring += "<td><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dQuantity>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dPrice>" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dAmount>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                    htmlstring += "<td style='display:none;'><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "'  style='display:none;'><span id=dTaxamt style='display:none;'>0</span></td>";
                    htmlstring += "<td style='display:none;'><div class=initshow><span id=ddiscount style='display:none;'>0</span></div><input type='text' id='Discount' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
                    htmlstring += "<td style='display:none;'><div class=initshow><span id=ddisper style='display:none;'>0</span></div><input type='text' id='disper1' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
                    htmlstring += "<td style='display:none;'><input type='text' id='chkrow' class='txtgridclass edit-mode'  name='chkrow'    value=0  style='display:none;'><span id=saveUnitId style='display:none'>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span><span id=itemBatchId style='display:none'>0</span><span id=lotExpDate style='display:none'></span><span id=LocationId style='display:none'>0</span></td></tr>";
                }
                return Json(new { Data = htmlstring }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult ItemQChangesChk(string ItemID, string SName)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("ItemQChangesChk", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                cmd.Parameters.AddWithValue("@SName", SName);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                string dvalue = Convert.ToString(dt.Rows[0]["AQty"]);

                    return Json(new { Data = dvalue }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }





        public ActionResult PaymentInformationNewCash(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {
                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
                //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and isnull(COLUMN14,0)=1 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationNewCash");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }



        public ActionResult PaymentInformationEditSave(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {

                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
                //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and isnull(COLUMN14,0)=1 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationEditSave");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
       


    }
}
