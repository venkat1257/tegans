﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeBizSuiteAppUI.Controllers
{
    public class FormBuildClass
    {
	//	EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
        public string Field_Text { get; set; }
        public string Field_Name { get; set; }
        public string Label_Name { get; set; }
        public string Action { get; set; }
        public string Mandatory { get; set; }
        public string Default_Value { get; set; }
        public string Control_Type { get; set; }
        public string Section_Type { get; set; }
        public string Section_Name { get; set; }
        public string Source_Type { get; set; }
        public string Source_Value { get; set; }
        public Nullable<int> Form_Id { get; set; }
        public Nullable<int> Table_Id { get; set; }
        public Nullable<int> Tab_Index { get; set; }

        public string Data_Type { get; set; }
    }
}