﻿using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class UserPreferencesController : Controller
    {


        string[] theader;
        string[] theadertext;

        //
        // GET: /UserPreferences/
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        public ActionResult Index(string ShowInactives)
        {
            try
            {

                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN04 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/UserPreferences/Edit/" + item[tblcol] + " >Edit</a>|<a href=/UserPreferences/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   ''  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  '' />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;
                ViewBag.columns = theader;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                var query1 = "Select * from MYTABLE001 where COLUMNA13='False'";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var db2 = WebMatrix.Data.Database.OpenConnectionString(connectionString, providerName);
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                return View("Index", db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }



        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                MYTABLE001 all = new MYTABLE001();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var taxNames = "SELECT column05, column02 FROM SETABLE007 ";
                var GData1 = dbs.Query(taxNames);
                ViewBag.TAXES = GData1;
                var id = Convert.ToInt32(Session["eid"]);
                eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
                SETABLE008 SETABLE008 = new SETABLE008();
                var lstids = act.SETABLE008.Where(r => r.COLUMN07 == id).ToList();
                var kpiids = lstids.Select(a => a.COLUMN04).ToList();
                if (kpiids != null)
                {
                    ViewBag.kpiids = kpiids;
                }
                else
                {
                    ViewBag.kpiids = kpiids;
                }

                SETABLE009 SETABLE009 = new SETABLE009();
                var integration = act.SETABLE009.Where(r => r.COLUMN06 == id).ToList();
                var sms = integration.Select(a => a.COLUMN04).FirstOrDefault();
                var email = integration.Select(a => a.COLUMN05).FirstOrDefault();
                var dropbox = integration.Select(a => a.COLUMN07).FirstOrDefault();
                ViewBag.sms = sms;
                ViewBag.email = email;
                ViewBag.dropbox = dropbox;

                MYTABLE001 MYTABLE001 = new MYTABLE001();
                var lst = act.MYTABLE001.Where(q => q.COLUMN09 == id).ToList();
                foreach (MYTABLE001 lists in lst)
                {
                    ViewBag.DateFormat = lists.COLUMN10;
                    ViewBag.LanguagePreference = lists.COLUMN05;
                    ViewBag.ColorTheme = lists.COLUMN06;
					//EMPHCS710 Rajasekhar Patakota 20/7/2015 User Preferences Set Selected Home Page
                    ViewBag.HomePageSelect = lists.COLUMN07;
                    ViewBag.GridSize = lists.COLUMN08;
                }
                if (lst == null)
                {
                    ViewBag.DateFormat = "mm/dd/yy";
                    ViewBag.LanguagePreference = "English";
                    ViewBag.ColorTheme = "Blue";
                    ViewBag.HomePage = "ManageActs";
                    ViewBag.GridSize = "10";
                }

                List<SelectListItem> dateFormate = new List<SelectListItem>();
              
                dateFormate.Add(new SelectListItem() { Text = "MM/dd/yy", Value = "mm/dd/y", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "MMM dd yyyy hh:mm AM/PM", Value = "M dd yy hh:mm AM/PM", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "MM/dd/yyyy", Value = "mm/dd/yy", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "yy.MM.dd", Value = "y.mm.dd", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "dd/MM/yy", Value = "dd/mm/yy", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "dd.MM.yy", Value = "dd.mm.y", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "dd-MM-yy", Value = "dd-mm-y", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "dd MMM yy", Value = "dd M y", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "MMM dd, yy", Value = "M dd, y", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "yyyy.MM.dd", Value = "yy.mm.dd", Selected = false });

                dateFormate.Add(new SelectListItem() { Text = "dd.MM.yyyy", Value = "dd.mm.yy", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "dd-MM-yyyy", Value = "dd-mm-yy", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "dd/MM/yyyy", Value = "dd/mm/yy", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "dd MMM yyyy", Value = "dd M yy", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "MMM dd, yyyy", Value = "M dd, yy", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "MMM dd yyyy hh:mm:ss:ms AM/PM", Value = "M dd yy hh:mm:ss:ms AM/PM", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "MM-dd-yy", Value = "mm-dd-y", Selected = false });
                dateFormate.Add(new SelectListItem() { Text = "MM-dd-yyyy", Value = "mm-dd-yy", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "MMM dd yyyy hh:mi:ss:mmm AM/PM", Value = "M dd yy hh:mi:ss:mmm AM/PM", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "MMM dd yy hh:mm:ss:ms AM/PM", Value = "M dd y hh:mm:ss:ms AM/PM", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "dd MMM yyyy hh:mm:ss:Ms", Value = "dd M yy hh:mm:ss:Ms", Selected = false });
                //dateFormate.Add(new SelectListItem() { Text = "yyyy-MM-dd hh:mm:ss", Value = "yy-mm-dd hh:mm:ss", Selected = false });
                ViewBag.dateFormate = dateFormate.ToList();

                List<SelectListItem> languages = new List<SelectListItem>();
               
                languages.Add(new SelectListItem() { Text = "English", Value = "English", Selected = false });
                languages.Add(new SelectListItem() { Text = "French", Value = "French", Selected = false });
                languages.Add(new SelectListItem() { Text = "German", Value = "German", Selected = false });
                languages.Add(new SelectListItem() { Text = "Spanish", Value = "Spanish", Selected = false });
                ViewBag.languages = languages;

                List<SelectListItem> ColorTheem = new List<SelectListItem>();
                
                ColorTheem.Add(new SelectListItem() { Text = "Blue", Value = "Blue", Selected = false });
                ColorTheem.Add(new SelectListItem() { Text = "Brown", Value = "Brown", Selected = false });
                ColorTheem.Add(new SelectListItem() { Text = "Green", Value = "Green", Selected = false });
                ColorTheem.Add(new SelectListItem() { Text = "Pink", Value = "Pink", Selected = false });
                ColorTheem.Add(new SelectListItem() { Text = "Purple", Value = "Purple", Selected = false });
                ViewBag.ColorTheem = ColorTheem;

                List<SelectListItem> HomePage = new List<SelectListItem>();
               
                HomePage.Add(new SelectListItem() { Text = "KPIs", Value = "KPIs", Selected = false });
                HomePage.Add(new SelectListItem() { Text = "ManageActs", Value = "ManageActs", Selected = false });
                ViewBag.HomePage = HomePage;

                List<SelectListItem> RecordsPerGrid = new List<SelectListItem>();
               
                RecordsPerGrid.Add(new SelectListItem() { Text = "10", Value = "10", Selected = false });
                RecordsPerGrid.Add(new SelectListItem() { Text = "15", Value = "15", Selected = false });
                RecordsPerGrid.Add(new SelectListItem() { Text = "20", Value = "20", Selected = false });
                RecordsPerGrid.Add(new SelectListItem() { Text = "25", Value = "25", Selected = false });
                ViewBag.RecordsPerGrid = RecordsPerGrid;

                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        //
        // POST: /UserPreferences/Create

        [HttpPost]
        public ActionResult Create(IEnumerable<int> coursePrices, string Create, string Save, string smss, string emails, string dropBoxs, string DateFormat, string DateFormatText, string LanguagePreferances, string ColorTheem, string HomePage, string RecordsPerGrid)
        {
            try
            {
                var id = Convert.ToInt32(Session["eid"]);

                var results = (from o in db.MYTABLE001 where o.COLUMN09 == id select o).Any();
                //var results = (from c in db.Customers where c.CustomerID == customerToFind  select c).SingleOrDefault()
                if (DateFormatText == "--Select--")
                    DateFormatText = DateFormat;
                if (results == true)
                {
                    eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
                    MYTABLE001 MYTABLE001 = new MYTABLE001();
                    var lst = act.MYTABLE001.Where(q => q.COLUMN09 == id).ToList();

                    foreach (MYTABLE001 product in lst)
                    {

                        product.COLUMN04 = DateFormatText;
                        product.COLUMN10 = DateFormat;
                        product.COLUMN05 = LanguagePreferances;
                        product.COLUMN06 = ColorTheem;
                        product.COLUMN07 = HomePage;
                        product.COLUMN08 = RecordsPerGrid;
                        product.COLUMNA13 = false;
                        product.COLUMNA12 = true;

                        product.COLUMN09 = Convert.ToInt32(Session["eid"]);
                        product.COLUMNA06 = DateTime.Now;
                        product.COLUMNA07 = DateTime.Now;


                        act.SaveChanges();
                    }
                    SETABLE008 SETABLE008 = new SETABLE008();
                    var lst1 = act.SETABLE008.Where(q => q.COLUMN07 == id).ToList();

                    int j = 0;
                    foreach (var product in lst1)
                    {
                        for (int i = j; i < coursePrices.Count(); i++)
                        {
                            product.COLUMN04 = (coursePrices).ElementAt(i);

                            product.COLUMN07 = Convert.ToInt32(Session["eid"]); j = j + 1;
                            break;
                        }
                        act.SaveChanges();
                    }
					//EMPHCS1068 rajasekhar reddy patakota 29/08/2015 Adding new info in kpis
                    if (coursePrices.Count() > lst1.Count) {
                        for (int i = lst1.Count; i < coursePrices.Count(); i++)
                        {
                            SETABLE008 SETABLE008New = new SETABLE008();
                            var cols2 = act.SETABLE008.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                            if (cols2 == null)
                            {
                                SETABLE008New.COLUMN02 = 1000;
                            }
                            else
                            {
                                SETABLE008New.COLUMN02 = (cols2.COLUMN02 + 1);
                            }
                            SETABLE008New.COLUMN04 = (coursePrices).ElementAt(i);

                            SETABLE008New.COLUMN07 = Convert.ToInt32(Session["eid"]);
                            act.SETABLE008.Add(SETABLE008New);
                            act.SaveChanges();
                        }
                    }
                    if (lst1.Count == 0)
                    {
                        if (coursePrices != null)
                        {
                            for (int i = 0; i < coursePrices.Count(); i++)
                            {
                                SETABLE008 SETABLE008New = new SETABLE008();

                                var cols2 = act.SETABLE008.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                                if (cols2 == null)
                                {
                                    SETABLE008New.COLUMN02 = 1000;
                                }
                                else
                                {
                                    SETABLE008New.COLUMN02 = (cols2.COLUMN02 + 1);
                                }
                                SETABLE008New.COLUMN04 = (coursePrices).ElementAt(i);

                                SETABLE008New.COLUMN07 = Convert.ToInt32(Session["eid"]);
                                act.SETABLE008.Add(SETABLE008New);
                                act.SaveChanges();
                            }
                        }
                    }

                    MYTABLE001 SETABLE009 = new MYTABLE001();
                    var lst2 = act.SETABLE009.Where(q => q.COLUMN06 == id).ToList();

                    foreach (var product in lst2)
                    {
                        product.COLUMN04 = Convert.ToBoolean(smss);
                        product.COLUMN05 = Convert.ToBoolean(emails);
                        product.COLUMN07 = Convert.ToBoolean(dropBoxs);
                        product.COLUMN06 = Convert.ToInt32(Session["eid"]);
                        act.SaveChanges();

                    }


                    Session["MessageFrom"] = "User Preference Data Updated..... ";
                    Session["SuccessMessageFrom"] = "Success";

                }
                else
                {
                    eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
                    MYTABLE001 MYTABLE001 = new MYTABLE001();

                    var col2 = act.MYTABLE001.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (col2 == null)
                    {
                        MYTABLE001.COLUMN02 = 1000;
                    }
                    else
                    {
                        MYTABLE001.COLUMN02 = (col2.COLUMN02 + 1);
                    }


                    MYTABLE001.COLUMN04 = DateFormatText;
                    MYTABLE001.COLUMN10 = DateFormat;
                    MYTABLE001.COLUMN05 = LanguagePreferances;
                    MYTABLE001.COLUMN06 = ColorTheem;
                    MYTABLE001.COLUMN07 = HomePage;
                    MYTABLE001.COLUMN08 = RecordsPerGrid;
                    MYTABLE001.COLUMNA13 = false;
                    MYTABLE001.COLUMNA12 = true;

                    MYTABLE001.COLUMN09 = Convert.ToInt32(Session["eid"]);
                    MYTABLE001.COLUMNA06 = DateTime.Now;
                    MYTABLE001.COLUMNA07 = DateTime.Now;
                    act.MYTABLE001.Add(MYTABLE001);


                    //act.SaveChanges();
                    if (coursePrices != null)
                    {
                        for (int i = 0; i < coursePrices.Count(); i++)
                        {
                            SETABLE008 SETABLE008 = new SETABLE008();

                            var cols2 = act.SETABLE008.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                            if (cols2 == null)
                            {
                                SETABLE008.COLUMN02 = 1000;
                            }
                            else
                            {
                                SETABLE008.COLUMN02 = (cols2.COLUMN02 + 1);
                            }
                            SETABLE008.COLUMN04 = (coursePrices).ElementAt(i);

                            SETABLE008.COLUMN07 = Convert.ToInt32(Session["eid"]);
                            act.SETABLE008.Add(SETABLE008);
                            act.SaveChanges();
                        }
                    }
                    SETABLE009 SETABLE009 = new SETABLE009();

                    var cols3 = act.SETABLE009.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (cols3 == null)
                    {
                        SETABLE009.COLUMN02 = 1000;
                    }
                    else
                    {
                        SETABLE009.COLUMN02 = (cols3.COLUMN02 + 1);
                    }

                    SETABLE009.COLUMN04 = Convert.ToBoolean(smss);
                    SETABLE009.COLUMN05 = Convert.ToBoolean(emails);
                    SETABLE009.COLUMN07 = Convert.ToBoolean(dropBoxs);
                    SETABLE009.COLUMN06 = Convert.ToInt32(Session["eid"]);
                    act.SETABLE009.Add(SETABLE009);
                    act.SaveChanges();

                    if (Session["DDDynamicItems"] == "DDDynamicItems")
                    {
                        Session["DDDynamicItems"] = null;
                        return RedirectToAction("FormBuild", "FormBuilding", new { FormName = Session["FormName"] });
                    }


                    Session["MessageFrom"] = "User Preference Successfully Created..... ";
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult CustomForm()
        {
            try
            {

                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                List<CONTABLE006> all = new List<CONTABLE006>();
                var alll = db.CONTABLE006.Where(a => a.COLUMN04 == ttid && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                all = alll.Where(a => a.COLUMN06 != null).ToList();
                var Tab = all.Where(a => a.COLUMN11 == "Tab" && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                var Ta = Tab.Select(a => a.COLUMN12).Distinct().ToList();
                var type = all.Select(b => b.COLUMN11).Distinct();
                var sn = all.Select(b => b.COLUMN12).Distinct();
                var Section = all.Where(b => b.COLUMN11 != "Tab" && b.COLUMN11 != "Item Level" && b.COLUMN06 != null && b.COLUMN06 != "").ToList();
                var sec = Section.Select(b => b.COLUMN12).Distinct();
                //var roles = db.CONTABLE012.Where(a => a.COLUMN04 != null).ToList();
                //ViewBag.Roles = roles;
                ViewBag.Type = type;
                ViewBag.Tabs = sn;
                ViewBag.TabMaster = Ta;
                ViewBag.Section = sec;
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public void Normal()
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                var col2 = "COLUMN02";
                cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN02",
                    Header = "Action",
                    Format = (item) => new HtmlString("<a href=/UserPreferences/Edit/" + item[col2] + " >Edit</a>|<a href=/UserPreferences/Details/" + item[col2] + " >View</a>")
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN02",
                    Header = "Module ID"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN03",
                    Header = "Name"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN04",
                    Header = "Description"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN05",
                    Header = "Subsidary"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN06",
                    Header = "Operating Unit"
                });
                ViewBag.cols = cols;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        [HttpGet]
        public ActionResult Sort(string ShowInactives)
        {
            try
            {

                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;

                return View("Index", data);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult Details(int id = 0)
        {
            try
            {

                MYTABLE001 MYTABLE001 = db.MYTABLE001.Find(id);
                if (MYTABLE001 == null)
                {
                    return HttpNotFound();
                }
                return View(MYTABLE001);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult Edit(int id = 0)
        {
            MYTABLE001 MYTABLE001 = db.MYTABLE001.Find(id);

            try
            {

                if (MYTABLE001 == null)
                {
                    return HttpNotFound();
                }
                return View(MYTABLE001);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult Edit(MYTABLE001 MYTABLE001)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    MYTABLE001.COLUMNA07 = DateTime.Now;

                    MYTABLE001.COLUMNA12 = true;

                    MYTABLE001.COLUMNA13 = false;


                    db.Entry(MYTABLE001).State = EntityState.Modified;
                    db.SaveChanges();


                }

                return View(MYTABLE001);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }

        }

        public ActionResult Delete(int id = 0)
        {
            try
            {
                MYTABLE001 contable007 = db.MYTABLE001.Find(id);

                var y = (from x in db.MYTABLE001 where x.COLUMN02 == id select x).First();
                y.COLUMNA12 = false;

                //db.MYTABLE001.Remove(y);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }

        }

        public void ExportCSV()
        {
            try
            {
                StringWriter sw = new StringWriter();

                var y = db.MYTABLE001.OrderBy(q => q.COLUMN02).ToList();

                sw.WriteLine("\"Module ID\",\"Name\",\"Description\",\"Subsidary\",\"Operating Unit\"");

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
                Response.ContentType = "text/csv";

                foreach (var line in y)
                {

                    sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"",
                                               line.COLUMN02,
                                               line.COLUMN03,
                                               line.COLUMN04,
                                               line.COLUMN05,
                                               line.COLUMN06
                                               ));

                }

                Response.Write(sw.ToString());

                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public void ExportPdf()
        {
            try
            {
                List<MYTABLE001> all = new List<MYTABLE001>();
                all = db.MYTABLE001.ToList();
                var data = from e in db.MYTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gv.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public void Exportword()
        {
            try
            {
                List<MYTABLE001> all = new List<MYTABLE001>();
                all = db.MYTABLE001.ToList();
                var data = from e in db.MYTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
                Response.ContentType = "application/vnd.ms-word ";
                Response.Charset = string.Empty;
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public void ExportExcel()
        {
            try
            {
                List<MYTABLE001> all = new List<MYTABLE001>();
                all = db.MYTABLE001.ToList();
                var data = from e in db.MYTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                gv.FooterRow.Visible = false;
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter ht = new HtmlTextWriter(sw);
                gv.RenderControl(ht);
                Response.Write(sw.ToString());
                Response.End();
                gv.FooterRow.Visible = true;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult Export(string items)
        {
            try
            {
                List<MYTABLE001> all = new List<MYTABLE001>();
                all = db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (items == "PDF")
                {
                    ExportPdf();
                }
                else if (items == "Excel")
                {
                    ExportExcel();
                }
                else if (items == "Word")
                {
                    Exportword();
                }
                else if (items == "CSV")
                {
                    ExportCSV();
                }
                return View("Index", db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();

                var ModuleID = Request["ModuleID"];
                var Name = Request["Name"];




                if (Name != null && Name != "" && Name != string.Empty)
                {
                    Name = Name + "%";
                }
                else
                    Name = Request["Name"];






                if (ModuleID != null && ModuleID != "" && ModuleID != string.Empty)
                {
                    ModuleID = ModuleID + "%";
                }
                else
                    ModuleID = Request["ModuleID"];

                var query = "SELECT * FROM MYTABLE001  WHERE COLUMN03 like '" + Name + "' or  or COLUMN02 LIKE '" + ModuleID + "'";

                var query1 = db.MYTABLE001.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.cols = Session["cols"];
                ViewBag.Inlinecols = Session["Inlinecols"];
                ViewBag.gdata = gdata;

                return View("Index", gdata);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult UpdateInline(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list);
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                string qStr = null;
                var tblID = db.CONTABLE004.Where(q => q.COLUMN04 == "MYTABLE001").Select(q => new { q.COLUMN02 });
                var ID = tblID.Select(a => a.COLUMN02).First();
                for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                {
                    for (int x = 0; x < ds.Tables[0].Columns.Count; x++)
                    {
                        var colname = ds.Tables[0].Columns[x].ColumnName;
                        var items = db.CONTABLE005.Where(q => q.COLUMN03 == ID && q.COLUMN04 == colname).FirstOrDefault();
                        if (items.COLUMN04 == ds.Tables[0].Columns[x].ColumnName)
                        {
                            if (qStr == null)
                            {
                                qStr += "UPDATE MYTABLE001 SET " + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "'";
                            }
                            else if (x == ds.Tables[0].Columns.Count - 1)
                            {
                                qStr += "," + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "' WHERE COLUMN02=" + ds.Tables[0].Rows[j]["COLUMN02"] + "";
                            }
                            else
                            {
                                qStr += "," + items.COLUMN04 + " ='" + ds.Tables[0].Rows[j][items.COLUMN04] + "'";
                            }
                        }
                    }
                    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                    SqlCommand cmd1 = new SqlCommand(qStr, cn);
                    cn.Open();
                    cmd1.ExecuteNonQuery();
                    cn.Close();
                    qStr = null;
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            try
            {
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
                if (ShowInactives != null)
                {
                    var all = db.MYTABLE001.ToList();
                    //return View(all);
                    return View("Index", db.MYTABLE001.ToList());
                }
                if (Session["Data"] != null)
                {
                    var data = Session["Data"];
                    var item = Session["sortselected"];
                    ViewBag.sortselected = item;
                    return View("Index", data);

                }
                return View("Index", db.MYTABLE001.Where(a => a.COLUMNA12 == true).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        string tthdata, tthtext;

        string itthdata, itthtext;

        public ActionResult View1(string items, string sort, string inactive, string style)
        {
            try
            {
                List<MYTABLE001> all = new List<MYTABLE001>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (style == "Normal")
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var indata = db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from MYTABLE001 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from MYTABLE001 where COLUMNA13='False' ";
                    }
                    var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    var providerName = "System.Data.SqlClient";
                    var db2 = WebMatrix.Data.Database.OpenConnectionString(connectionString, providerName);
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();

                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th class=webgrid-header>Action</th>";
                            tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                            itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                            itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td class=webgrid-row-style><a href=/UserPreferences/Edit/" + itm[Inline] + " >Edit</a>|<a href=/UserPreferences/Details/" + itm[Inline] + " >View</a></td>";
                                itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px;font-size:10px;  /></td>";
                                tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            }
                            else
                            {
                                itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:80px;font-size:10px;  /></td>";
                                tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                            }
                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    ViewBag.columns = theader;
                    var htmlString = "<table id=tbldata style=width:100%><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata style=width:100%><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                    return View("Index", db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

                }
                else
                {
                    var grid = new WebGrid(null, canPage: false, canSort: false);
                    var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                    var tbtid = tbid.COLUMN02;
                    var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                    ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                    if (sort == "Recently Created")
                    {
                        all = db.MYTABLE001.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        all = db.MYTABLE001.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else
                    {
                        all = db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList();

                    }
                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/UserPreferences/Edit/" + item[tblcol] + " >Edit</a>|<a href=/UserPreferences/Details/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Index", db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }






        public ActionResult ListView()
        {
            return View(db.MYTABLE001.ToList());
        }

        public ActionResult New()
        {
            return View();
        }

        public ActionResult Style(string items, string inactive)
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                MYTABLE001 lv = new MYTABLE001();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                var data = from e in db.MYTABLE001.AsEnumerable() select new { ModuleID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Subsidary = e.COLUMN05, OperatingUnit = e.COLUMN06 };
                List<MYTABLE001> tbldata = new List<MYTABLE001>();
                tbldata = db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList();
                if (items == "Normal")
                {
                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == Attid).ToList();
                    var indata = db.MYTABLE001.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from MYTABLE001 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from MYTABLE001 where COLUMNA13='False' ";
                    }
                    var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    var providerName = "System.Data.SqlClient";
                    var db2 = WebMatrix.Data.Database.OpenConnectionString(connectionString, providerName);
                    var books = db2.Query(query1);
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th class=webgrid-header>Action</th>";
                            tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                            itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th class=webgrid-header>" + dcol + "</th>";
                            itthtext += "<th class=webgrid-header>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td class=webgrid-row-style><a href=/UserPreferences/Edit/" + itm[Inline] + " >Edit</a>|<a href=/UserPreferences/Details/" + itm[Inline] + " >Details</a></td>";
                                tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                                itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   '' /></td>";
                            }
                            else
                            {
                                tthdata += "<td class=webgrid-row-style>" + itm[Inline] + "</td>";
                                itthdata += "<td class=webgrid-row-style><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    ''  /></td>";
                            }


                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    ViewBag.columns = theader;
                    var htmlString = "<table id=tbldata style=width:100%><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata style=width:100%><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                }
                return View("Index", db.MYTABLE001.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult CustomView()
        {
            try
            {
                List<CONTABLE005> all = new List<CONTABLE005>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                //all = from e in db.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
                var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
                all = db.CONTABLE005.SqlQuery(query).ToList();

                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {


            int col2 = 0;
            var tbldata = db.CONTABLE004.Where(a => a.COLUMN05 == "tbl_subsidary").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = db.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = db.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    db.CONTABLE013.Add(dd);
                    db.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        db.CONTABLE013.Add(dd);
                        db.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);

        }






        public ActionResult NewField()
        {
            try
            {
                //var table2= Convert.ToString();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = Convert.ToInt32(tid.COLUMN02);
                var SecNames = db.CONTABLE006.Where(a => a.COLUMN12 != null && a.COLUMN04 == ttid).ToList();
                var dsecnames = SecNames.Select(a => a.COLUMN12).Distinct();
                ViewBag.SecNames = dsecnames;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult NewField(FormCollection fc)
        {
            CONTABLE006 fm = new CONTABLE006();
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MYTABLE001").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                var query = "SELECT  top( 1 ) * FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04=COLUMN05  ";
                var data = db.CONTABLE005.SqlQuery(query).ToList();
                var tblid = data.Select(a => a.COLUMN03).FirstOrDefault();
                var colname = data.Select(a => a.COLUMN04).FirstOrDefault();
                var alicolname = data.Select(a => a.COLUMN05).FirstOrDefault();
                var con6data = db.CONTABLE006.Where(a => a.COLUMN03 == ttid).FirstOrDefault();
                fm.COLUMN03 = con6data.COLUMN03;
                fm.COLUMN04 = Convert.ToInt32(tblid);
                fm.COLUMN05 = Convert.ToString(colname);
                fm.COLUMN06 = Request["Label_Name"];
                fm.COLUMN07 = "Y";
                fm.COLUMN08 = Request["Mandatory"];
                fm.COLUMN10 = Request["Control_Type"];
                fm.COLUMN11 = Request["Section_Type"];
                fm.COLUMN12 = Request["Section_Name"];
                var pr = con6data.COLUMN13;
                fm.COLUMN13 = (pr + 1).ToString();
                db.CONTABLE006.Add(fm);
                db.SaveChanges();
                return View("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "UserPreferences", new { FormName = Session["FormName"] });
            }
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
