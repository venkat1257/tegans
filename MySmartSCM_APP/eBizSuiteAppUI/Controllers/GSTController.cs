﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;
using System.Globalization;
using System.Dynamic;
using eBizSuiteAppModel;


namespace MeBizSuiteAppUI.Controllers
{
    public class GSTController : Controller
    {
        //
        // GET: /GST/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();

        public void Dateformat()
        {
            try
            {


                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter dad = new SqlDataAdapter(str, cn);
                DataTable dtd = new DataTable();
                dad.Fill(dtd); string DateFormat = null;
                if (dtd.Rows.Count > 0)
                {
                    DateFormat = dtd.Rows[0][0].ToString();
                    Session["FormatDate"] = dtd.Rows[0][0].ToString();
                }
                if (dtd.Rows[0][1].ToString() != "")
                    Session["ReportDate"] = dtd.Rows[0][1].ToString();
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                }
            }

            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";

            }
        }
        public ActionResult Create()
        {
            try
            {
                SqlCommand cmd1 = new SqlCommand("usp_Proc_InputTaxCreditDetails", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmd1.Parameters.Add(new SqlParameter("@OPUnit", Session["OPUnit1"]));
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da1.Fill(dt);
                List<SelectListItem> Customer = new List<SelectListItem>();
                List<SelectListItem> OperatingUnit = new List<SelectListItem>();
                List<SelectListItem> TaxType = new List<SelectListItem>();
                List<SelectListItem> Vendor = new List<SelectListItem>();
                List<SelectListItem> Bill = new List<SelectListItem>();
                List<SelectListItem> Status = new List<SelectListItem>();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToString(dr["StateCode"]) != "" && Convert.ToString(dr["COLUMN02"]) != "")
                            Customer.Add(new SelectListItem { Value = Convert.ToString(dr["COLUMN02"]), Text = Convert.ToString(dr["StateCode"]) });
                        else if (Convert.ToString(dr["OPUnit"]) != "" && Convert.ToString(dr["COLUMN02"]) != "")
                            OperatingUnit.Add(new SelectListItem { Value = Convert.ToString(dr["COLUMN02"]), Text = Convert.ToString(dr["OPUnit"]) });
                        else if (Convert.ToString(dr["TaxType"]) != "" && Convert.ToString(dr["COLUMN02"]) != "")
                            TaxType.Add(new SelectListItem { Value = Convert.ToString(dr["COLUMN02"]), Text = Convert.ToString(dr["TaxType"]) });
                        else if (Convert.ToString(dr["Vendor"]) != "" && Convert.ToString(dr["COLUMN02"]) != "")
                            Vendor.Add(new SelectListItem { Value = Convert.ToString(dr["COLUMN02"]), Text = Convert.ToString(dr["Vendor"]) });
                        else if (Convert.ToString(dr["Bill"]) != "" && Convert.ToString(dr["COLUMN02"]) != "")
                            Bill.Add(new SelectListItem { Value = Convert.ToString(dr["COLUMN02"]), Text = Convert.ToString(dr["Bill"]) });
                        else if (Convert.ToString(dr["Status"]) != "" && Convert.ToString(dr["COLUMN02"]) != "")
                            Status.Add(new SelectListItem { Value = Convert.ToString(dr["COLUMN02"]), Text = Convert.ToString(dr["Status"]) });
                    }
                    Customer = Customer.OrderBy(x => x.Text).ToList();
                    ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                    OperatingUnit = OperatingUnit.OrderBy(x => x.Text).ToList();
                    ViewData["OperatingUnit"] = new SelectList(OperatingUnit, "Value", "Text");
                    TaxType = TaxType.OrderBy(x => x.Text).ToList();
                    ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                    Vendor = Vendor.OrderBy(x => x.Text).ToList();
                    ViewData["Vendor"] = new SelectList(Vendor, "Value", "Text");
                    Bill = Bill.OrderByDescending(x => x.Value).ToList();
                    ViewData["Bill"] = new SelectList(Bill, "Value", "Text");
                    Status = Status.OrderBy(x => x.Text).ToList();
                    ViewData["Status"] = new SelectList(Status, "Value", "Text");
                }
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                var currentTime = new DateTime();
                DateTime ToDate = DateTime.Now, FromDate = new DateTime(ToDate.Year, ToDate.Month, 1);
                var TD = FromDate.ToString(Session["FormatDate"].ToString());
                ViewBag.FromDate = TD; ViewBag.ToDate = frmDT;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult GetCustomerDetails(String Bill, String Vendor, String OperatingUnit, String FromDate, String Date, String TaxType)
        {
            try
            {
                var OPunit = Session["OPunit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("GSTSearch", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Bill", Bill);
                cmd.Parameters.AddWithValue("@Vendor", Vendor);
                cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                cmd.Parameters.AddWithValue("@TaxType", TaxType);
                cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FromDate, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                cmd.Parameters.AddWithValue("@Date", DateTime.ParseExact(Date, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"]);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string htmlstring = null, htmlstring1 = null;
                for (int g = 0; g < dt.Rows.Count; g++)
                {
                    //htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dCheckRow>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='checkbox' id='" + dt.Columns[0].ColumnName + "' class='chkclass'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                    htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dBill>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dVendor>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dDate>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
                    //htmlstring += "<td><div><span id=dSub Total></span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dSub Total>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dGroupwise >" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dRate >" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dTax Amount>" + dt.Rows[g][dt.Columns[6].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dItc Taken#>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[7].ColumnName + "' class='txtgridclass gridtxtarealength edit-mode'  name='" + dt.Columns[7].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[7].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dItc Ref#>" + dt.Rows[g][dt.Columns[8].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[8].ColumnName + "' class='txtgridclass gridtxtarealength'  name='" + dt.Columns[8].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[8].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dRemarks >" + dt.Rows[g][dt.Columns[9].ColumnName] + "</span></div><textarea id='" + dt.Columns[9].ColumnName + "' class='txtgridclass gridtxtarealength'  name='" + dt.Columns[9].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[9].ColumnName] + "' ></textarea></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dCheckBox>" + dt.Rows[g][dt.Columns[10].ColumnName] + "</span></div><input type='checkbox' id='" + dt.Columns[10].ColumnName + "' class='chkclass'  name='" + dt.Columns[10].ColumnName + "' checked='checked'><input  type='text' style='display:none'  id='id' value='" + dt.Rows[g][dt.Columns[11].ColumnName] + "'  /><input  type='text' style='display:none'  id='TAX' value='" + dt.Rows[g][dt.Columns[12].ColumnName] + "'  /><input  type='text' style='display:none'  id='NAME' value='" + dt.Rows[g][dt.Columns[13].ColumnName] + "'/><input  type='text' style='display:none'  id='VendorID' value='" + dt.Rows[g][dt.Columns[14].ColumnName] + "'/></td></tr>";
                }
                return Json(new { Data = htmlstring, Data1 = ViewBag.Search }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }

        SqlCommand Cmd;
        [HttpPost]
        public ActionResult GSTGridSave(string list, FormCollection col)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["GSTGridSave"] = ds;
                Session["onfly"] = null;
                int OutParam = 0;
                string OrderType = "10000000";
                OrderType = (Convert.ToInt32(OrderType) + 1).ToString();
                var Date = DateTime.Now;
                var OpUnit = Session["OPUnit1"];
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = ""; int r = 0;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["GSTGridSave"];
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {

                    string Bill = itemdata.Tables[0].Rows[i]["Bill"].ToString();
                    SqlCommand cmdd = new SqlCommand("select COLUMN02 from PUTABLE005 WHERE COLUMN04 = '" + Bill + "'", con);
                    SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                    DataTable dtt = new DataTable();
                    daa.Fill(dtt);
                    string HCol2 = dtt.Rows[0]["COLUMN02"].ToString();
                    string Vendor = itemdata.Tables[0].Rows[i]["Vendor"].ToString();
                    string Tax = itemdata.Tables[0].Rows[i]["Tax"].ToString();
                    string ItcTaken = itemdata.Tables[0].Rows[i]["ItcTaken"].ToString();
                    if (ItcTaken == "")
                    {
                        ItcTaken = itemdata.Tables[0].Rows[i]["Tax"].ToString();
                    }
                    string ItcRef = itemdata.Tables[0].Rows[i]["ItcRef"].ToString();
                    string Remarks = itemdata.Tables[0].Rows[i]["Remarks"].ToString();
                    string Dates = itemdata.Tables[0].Rows[i]["Date"].ToString();
                    string Groupwise = itemdata.Tables[0].Rows[i]["Groupwise"].ToString();
                    string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                    string SubAmount = itemdata.Tables[0].Rows[i]["SubAmount"].ToString();
                    string id = itemdata.Tables[0].Rows[i]["id"].ToString();
                    string TAX = itemdata.Tables[0].Rows[i]["TAX"].ToString();
                    string NAME = itemdata.Tables[0].Rows[i]["NAME"].ToString();
                    string VendorID = itemdata.Tables[0].Rows[i]["VendorID"].ToString();
                    string Status = "";
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmd = new SqlCommand("usp_PUR_TP_PUTABLE024", cnc);
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                        Cmd.Parameters.AddWithValue("@COLUMN03", Bill);
                        Cmd.Parameters.AddWithValue("@COLUMN04", Vendor);
                        Cmd.Parameters.AddWithValue("@COLUMN05", ItcTaken);
                        Cmd.Parameters.AddWithValue("@COLUMN06", ItcRef);
                        Cmd.Parameters.AddWithValue("@COLUMN07", Remarks);
                        Cmd.Parameters.AddWithValue("@COLUMN08", Status);
                        Cmd.Parameters.AddWithValue("@COLUMN09", VendorID);
                        Cmd.Parameters.AddWithValue("@COLUMN11", DateTime.ParseExact(Dates, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        Cmd.Parameters.AddWithValue("@COLUMN10", Tax);
                        Cmd.Parameters.AddWithValue("@COLUMN12", Groupwise);
                        Cmd.Parameters.AddWithValue("@COLUMN13", Rate);
                        Cmd.Parameters.AddWithValue("@COLUMN14", SubAmount);
                        Cmd.Parameters.AddWithValue("@COLUMN15", id);
                        Cmd.Parameters.AddWithValue("@COLUMN16", TAX);
                        Cmd.Parameters.AddWithValue("@COLUMN17", NAME);
                        Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmd.Parameters.AddWithValue("@Direction", insert);
                        Cmd.Parameters.AddWithValue("@TabelName", "PUTABLE024");
                        cnc.Open();
                        r = Cmd.ExecuteNonQuery();
                    }
                }
                if (r > 0)
                {
                    var msg = string.Empty;
                    msg = "Input Tax Credit Process Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                return Json("");
                return RedirectToAction("GSTGridSave", "GST");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";

                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

    }
}
