﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using MvcMenuMaster.Models;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using System.IO;
using System.ComponentModel.DataAnnotations;
using eBizSuiteUI.Controllers;
using eBizSuiteAppModel.Table;
using WebMatrix.Data;
using System.Web.Helpers;
using MeBizSuiteAppUI.Controllers;
using System.Web.UI;
using iTextSharp.text;
using System.Text;
using eBizSuiteAppDAL.classes;
using eBizSuiteAppUI.Models;
using Microsoft.Reporting.WebForms;
using System.Diagnostics;
using GemBox.Document;
using GemBox.Document.Tables;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using eBizSuiteAppBAL.Fombuilding;
using System.Data.SqlTypes;
using System.Xml.Linq;
using System.Globalization;

namespace MeBizSuiteAppUI.Controllers
{
    public class CustomerController : Controller
    {
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        CommonController cmn = new CommonController();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        //
        // GET: /Customer/
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        string[] theader;
        string[] theadertext;
        //EMPHCS1781 Retail Creation of Customer and Item Master by gnaneshwar on 9/7/2016
		public ActionResult Index()
        {
            try
            {
                FormBuildingEntities entity = new FormBuildingEntities();
                FormbuildingInfo info = new FormbuildingInfo();
                Session["DDDynamicItems"] = null;
                Session["FormName"] = "Customer";

                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);

                ViewBag.sortselected = Session["SortByStatus"];
                //EMPHCS1011 rajasekhar reddy patakota 25/08/2015 Customisation of Views
                ViewBag.viewselected = Session["ViewStyle"];
                var sql = "Select s2.[COLUMN02 ] AS ID, s2.COLUMN04 as Customer#,s2.COLUMN05 as 'Customer Name',s2.COLUMN10 as Email,s2.COLUMN11 as MobileNo from SATABLE002 s2   where s2.COLUMNA03=" + Session["AcOwner"] + " and (s2.COLUMNA02 in(" + Session["OPUnit"] + ") or s2.COLUMNA02 is null) and isnull(s2.COLUMNA13,0)=0 order by s2.COLUMN02 desc";
                var GData = dbs.Query(sql);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in grid.ColumnNames)
                {
                    cols.Add(grid.Column(column, column));
                }
                ViewBag.Columns = cols;
                ViewBag.Grid = GData;
                return View("Index", db.CONTABLE007.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult Index1()
        {
            try
            {

                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE007").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN04 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/OperatingUnit/Edit/" + item[tblcol] + " >Edit</a>|<a href=/OperatingUnit/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   ''  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  '' />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;
                ViewBag.columns = theader;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                var ac = Convert.ToInt32(Session["AcOwner"]); string ounit = null;
                if (ac != 56567)
                    ounit = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                else
                    ounit = "  COLUMNA02 is null ";
                var query1 = "Select * from SATABLE002 where COLUMN07='False' and isnull(COLUMNA13,'False')='False' and " + ounit + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                return View("Index", db.CONTABLE007.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
            
            
            //return View();
            
           
        }
        [HttpGet]
        public ActionResult Insert() // Calling when we first hit controller.
        {
		// Exception Handling By Venkat
            try
            {
            Session["DDDynamicItems"] = "";
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var ac = Convert.ToInt32(Session["AcOwner"]); //int? ou = null;

            var OPeratingUnit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
            ViewBag.OPUnit = OPeratingUnit;
            List<SelectListItem> PriceLevel = new List<SelectListItem>();
            SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE023  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            for (int dd = 0; dd < dtp.Rows.Count; dd++)
            {
                PriceLevel.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN04"].ToString() });
            }
            PriceLevel = PriceLevel.OrderBy(x => x.Text).ToList();
            ViewData["PriceLevel"] = new SelectList(PriceLevel, "Value", "Text");
            List<SelectListItem> Item = new List<SelectListItem>();
            SqlCommand cmdr = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dar = new SqlDataAdapter(cmdr);
            DataTable dtr = new DataTable();
            dar.Fill(dtr);
            for (int dd = 0; dd < dtr.Rows.Count; dd++)
            {
                Item.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
            }
            Item = Item.OrderBy(x => x.Text).ToList();
            ViewData["Item"] = new SelectList(Item, "Value", "Text");
            var sql = "select top 1 null COLUMN04,null COLUMN05,null COLUMN06,null COLUMN07,null COLUMN08 from MATABLE026";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(sql);
            ViewBag.itemsdata = books;
            var Category = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11120).ToList();
            ViewBag.Category = Category;
            var subCategory = activity.MATABLE002.Where(a => a.COLUMNA13 == false  && a.COLUMN03 == 11121).ToList();
            ViewBag.subCategory = subCategory;
            var Terms = activity.MATABLE002.Where(a => a.COLUMNA13 == false  && a.COLUMN03 == 11122).ToList();
            ViewBag.Terms = Terms;
            var Mode = activity.MATABLE002.Where(a => a.COLUMNA13 == false  && a.COLUMN03 == 11123).ToList();
            ViewBag.Mode = Mode;
            var country = activity.MATABLE016.ToList();
            ViewBag.country = country;
            var department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
            ViewBag.department = department;
            var state = activity.MATABLE017.ToList();
            ViewBag.state = state;

            var SalesRep = db.MATABLE010.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN30 == true).ToList();
            ViewBag.SalesRep = SalesRep;
            var Type = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11130).ToList();
            ViewBag.Type = Type;
            var Section = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11153).ToList();
            ViewBag.Section = Section;

            ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA03 == ac).ToList();
            SATABLE002 all = new SATABLE002();
            var col2 = activity.SATABLE002.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
            var col1 = col2;
            eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
            var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).ToList();
            int? Oper = Convert.ToInt32(Session["OPUnit1"]);
            //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 15/09/2015
            Session["AUTO"] = Oper;
            if (Oper == 0)
                Oper = null;
            var acOW = Convert.ToInt32(Session["AcOwner"]);
            var fid = 1265;
            var customfrmID =1265;
            var listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
            var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
            if (Oper == null)
            {
                //if (Tranlist.Count > 0)
                //    listTM = null;
                //else
                //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 20/09/2015
                listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
            }
            if (listTM == null)
            {
                listTM = act1.MYTABLE002.Where(p => p.COLUMN03 == 1265 && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                if (Oper == null)
                {
                    Tranlist = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMNA03 == acOW).ToList();
                    //if (Tranlist.Count > 0)
                    //    listTM = null;
                    //else
                    //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 20/09/2015
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                }
            }
            var fino = "";
            var pono = "";
            var Prefix = "";
            var Endno = "";
            var Sufix = " ";
            var CurrentNo = "";
            var sNum = "";
            int c = 0;
            if (listTM != null)
            {
                fino= Prefix = listTM.COLUMN06.ToString();
                Sufix = listTM.COLUMN10.ToString();
                sNum = listTM.COLUMN07.ToString();
                CurrentNo = listTM.COLUMN09.ToString();
                c = Prefix.Length + 1;
                if (string.IsNullOrEmpty(Sufix))
                    Sufix = " ";
            }
            else
            {
                Prefix = null;
            }
                int? opunit = Convert.ToInt32(Session["OPUnit1"]);
                        //EMPHCS1010 rajasekhar reddy patakota 25/08/2015 Autogeneration number with custom form
                SqlCommand pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE002  Where COLUMN03='" + 1265 + "' and COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA02='" + opunit + "'   AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE002  Where COLUMN03='" + 1265 + "' and  COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA02='" + opunit + "'  AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'   ORDER BY COLUMN02 DESC", cn);
                cn.Open();
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                cn.Close();
                var paddingno = ""; var paddingnolength = ""; var item = ""; if (listTM != null)
                {
                    if (padt.Rows.Count > 0)
                    {
                        if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                        {
                            if (sNum == "") sNum = "00001";
                            else sNum = sNum;
                            pono = fino + sNum + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                            item = (pono).TrimEnd();
                        }
                        else
                        {
                            if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                            {
                                paddingnolength = sNum.Length.ToString();
                                    paddingno = "00000";
                                pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                                Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                            }
                            else if (CurrentNo != "" && CurrentNo != null)
                            {
                                paddingnolength = CurrentNo.Length.ToString();
                                    paddingno = "00000";
                                pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                            }
                            else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                            {
                                sNum = (Convert.ToInt32(sNum) + 0).ToString();
                                paddingnolength = sNum.Length.ToString();
                                    paddingno = "00000";
                                pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(sNum));
                            }
                            else
                            {
                                sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                                paddingnolength = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString().Length.ToString();
                                    paddingno = "00000";
                                pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                                Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                            }
                            item = pono.TrimEnd();
                            Session["NO"] = item;
                        }
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            paddingnolength = sNum.Length.ToString();
                                paddingno = "00000";
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            paddingnolength = CurrentNo.Length.ToString();
                                paddingno = "00000";
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            paddingnolength = sNum.Length.ToString();
                                paddingno = "00000";
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            paddingnolength = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString().Length.ToString();
                                paddingno = "00000";
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        item = (pono.TrimEnd());
                        Session["NO"] = item;
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    paddingnolength = sNum.Length.ToString();
                        paddingno = "00000";
                    if (padt.Rows.Count > 0)
                    { CurrentNo = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, ""))).ToString(paddingno); }
                    if (listTM != null)
                    {
                        if (CurrentNo == "") CurrentNo = "00001";
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    item = (pono.TrimEnd());
                    Session["NO"] = item;
                }
                    
            return View(all);
            
        }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult InsertOnflySave(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
				//EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
				//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                int OutParam = 0; string TransNo = "";
                Session["onfly"] = null;
                var ac = Convert.ToInt32(Session["AcOwner"]);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                cn.Open();
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE002 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string insert = "Insert";
                string Date = DateTime.Now.ToString();
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1265");
				//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                TransNo = GetTransactionNo(1265);
                Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Customer"]);
                Cmd.Parameters.AddWithValue("@COLUMN06", col["Memo"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["Phoneno"]);
                Cmd.Parameters.AddWithValue("@COLUMN17", Oper);
                Cmd.Parameters.AddWithValue("@COLUMN25", 0);
                Cmd.Parameters.AddWithValue("@COLUMN40", 0);
                Cmd.Parameters.AddWithValue("@COLUMNA02", Oper);
                Cmd.Parameters.AddWithValue("@COLUMNA03", ac);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE002");
				//EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
				//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                SqlCommand cmdA = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE003 ", cn);
                SqlDataAdapter daaA = new SqlDataAdapter(cmdA);
                DataTable dttA = new DataTable();
                daaA.Fill(dttA);
                string HCol2A = dttA.Rows[0][0].ToString();
                HCol2A = (Convert.ToInt32(HCol2A) + 1).ToString();

                SqlCommand CmdA = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                CmdA.CommandType = CommandType.StoredProcedure;
                CmdA.Parameters.AddWithValue("@COLUMN02", HCol2A);
                CmdA.Parameters.AddWithValue("@COLUMN03", 1259);
                CmdA.Parameters.AddWithValue("@COLUMN06", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN07", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN08", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN16", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN11", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN10", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN12", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN20", OutParam);
                CmdA.Parameters.AddWithValue("@COLUMN17", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN18", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMNA02", Oper);
                CmdA.Parameters.AddWithValue("@COLUMNA03", ac);
                CmdA.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdA.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdA.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdA.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdA.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdA.Parameters.AddWithValue("@Direction", insert);
                CmdA.Parameters.AddWithValue("@TabelName", "SATABLE003");
                CmdA.Parameters.AddWithValue("@ReturnValue", "");
                int r1 = CmdA.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
				    //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                    eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                    Oper = Convert.ToInt32(Session["AUTO"]);
                    if (Oper == null || Oper == 0)
                        Oper = Convert.ToInt32(Session["OPUnit1"]);
                    if (Oper == 0)
                        Oper = null;
                    int? acOW = Convert.ToInt32(Session["AcOwner"]);

                    var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).FirstOrDefault().COLUMN04.ToString();
                    var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();

                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN11 == custfname && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }
                    //var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1265 && q.COLUMN05 == 1).FirstOrDefault();
                    //var msg = string.Empty;
                    //if (msgMaster != null)
                    //{
                    //    msg = msgMaster.COLUMN03;
                    //}
                    //else
                    //    msg = "Successfully Created........ ";
                    //Session["MessageFrom"] = msg;
                    //Session["SuccessMessageFrom"] = "Success";
                    List<SelectListItem> vendor = new List<SelectListItem>();
                    if (Session["DDDynamicItems"] == "DDDynamicItems")
                    {
                        SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN01,COLUMN02,COLUMN05 COLUMN03 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                        DataTable dtdata = new DataTable();
                        cmddl.Fill(dtdata);
                        ViewData["Customer"] = new SelectList(vendor, "Value", "Text");
                        for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                        {
                            if (OutParam == Convert.ToInt32(dtdata.Rows[dd]["COLUMN01"].ToString()))
                                vendor.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString(), Selected = true });
                            else
                                vendor.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString() });
                        }
                        Session["DDDynamicItems"] = null;
                    }
                    HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + " SATABLE002"] = "";
                    if (Convert.ToString(Session["RediesCache"]) == "1")
                    { int d = cmn.UpdateRedisCacheStatus("1265", "110007017", "Update"); }
                    return Json(new { item = vendor }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1265 && q.COLUMN05 == 0).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Data Creation Failed.......... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
            }
            return RedirectToAction("PointOfSalesInfo", "PointOfSales", new { FormName = Session["FormName"] });
        }
		//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = db.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono = "", TransNo = "";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (Prefix != null)
                {
                    fino = Prefix;
                    pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE002  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE002  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                }
                else
                {
                    fino = "CUST";
                    pacmd = new SqlCommand("Select  substring(COLUMN04,4,len(COLUMN04)) PO FROM SATABLE002  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE002  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                pacmd.ExecuteNonQuery();
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }
        [HttpGet]
        public ActionResult InsertOnfly() // Calling when we first hit controller.
        {
            Session["DDDynamicItems"] = "DDDynamicItems";
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var ac = Convert.ToInt32(Session["AcOwner"]); //int? ou = null;

            //var OPeratingUnit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
            //ViewBag.OPUnit = OPeratingUnit;
            //var Category = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11120).ToList();
            //ViewBag.Category = Category;
            //var subCategory = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11121).ToList();
            //ViewBag.subCategory = subCategory;
            //var Terms = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11122).ToList();
            //ViewBag.Terms = Terms;
            //var Mode = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11123).ToList();
            //ViewBag.Mode = Mode;
            //var country = activity.MATABLE016.ToList();
            //ViewBag.country = country;
            //var department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
            //ViewBag.department = department;
            //var state = activity.MATABLE017.ToList();
            //ViewBag.state = state;

            //var SalesRep = db.MATABLE010.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN30 == true).ToList();
            //ViewBag.SalesRep = SalesRep;
            //var Type = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11130).ToList();
            //ViewBag.Type = Type;
            //var Section = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11153).ToList();
            //ViewBag.Section = Section;

            //ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA03 == ac).ToList();
            //SATABLE002 all = new SATABLE002();
            //var col2 = activity.SATABLE002.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
            //var col1 = col2;
            //eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
            //var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).ToList();
            //int? Oper = Convert.ToInt32(Session["OPUnit1"]);
            ////EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 15/09/2015
            //Session["AUTO"] = Oper;
            //if (Oper == 0)
            //    Oper = null;
            //var acOW = Convert.ToInt32(Session["AcOwner"]);
            //var fid = 1265;
            //var customfrmID = 1265;
            //var listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
            //var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
            //if (Oper == null)
            //{
            //    //if (Tranlist.Count > 0)
            //    //    listTM = null;
            //    //else
            //    //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 20/09/2015
            //    listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
            //}
            //if (listTM == null)
            //{
            //    listTM = act1.MYTABLE002.Where(p => p.COLUMN03 == 1265 && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
            //    if (Oper == null)
            //    {
            //        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMNA03 == acOW).ToList();
            //        //if (Tranlist.Count > 0)
            //        //    listTM = null;
            //        //else
            //        //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 20/09/2015
            //        listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
            //    }
            //    if (listTM == null)
            //    {
            //        listTM = act1.MYTABLE002.Where(a => a.COLUMN03 == 1265 && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
            //    }
            //}
            //var fino = "";
            //var pono = "";
            //var Prefix = "";
            //var Endno = "";
            //var Sufix = " ";
            //var CurrentNo = "";
            //var sNum = "";
            //int c = 0;
            //if (listTM != null)
            //{
            //    fino = Prefix = listTM.COLUMN06.ToString();
            //    Sufix = listTM.COLUMN10.ToString();
            //    sNum = listTM.COLUMN07.ToString();
            //    CurrentNo = listTM.COLUMN09.ToString();
            //    c = Prefix.Length + 1;
            //    if (string.IsNullOrEmpty(Sufix))
            //        Sufix = " ";
            //}
            //else
            //{
            //    Prefix = null;
            //}
            //int? opunit = Convert.ToInt32(Session["OPUnit1"]);
            ////EMPHCS1010 rajasekhar reddy patakota 25/08/2015 Autogeneration number with custom form
            //SqlCommand pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE002  Where COLUMN03='" + 1265 + "' and COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA02='" + opunit + "'   AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE002  Where COLUMN03='" + 1265 + "' and  COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA02='" + opunit + "'  AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'   ORDER BY COLUMN02 DESC", cn);
            //cn.Open();
            //SqlDataAdapter pada = new SqlDataAdapter(pacmd);
            //DataTable padt = new DataTable();
            //pada.Fill(padt);
            //cn.Close();
            //var paddingno = ""; var paddingnolength = ""; var item = ""; if (listTM != null)
            //{
            //    if (padt.Rows.Count > 0)
            //    {
            //        if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
            //        {
            //            if (sNum == "") sNum = "00001";
            //            else sNum = sNum;
            //            pono = fino + sNum + Sufix;
            //            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
            //            item = (pono).TrimEnd();
            //        }
            //        else
            //        {
            //            if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
            //            {
            //                paddingnolength = sNum.Length.ToString();
            //                paddingno = "00000";
            //                pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
            //                Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
            //            }
            //            else if (CurrentNo != "" && CurrentNo != null)
            //            {
            //                paddingnolength = CurrentNo.Length.ToString();
            //                paddingno = "00000";
            //                pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
            //                Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
            //            }
            //            else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
            //            {
            //                sNum = (Convert.ToInt32(sNum) + 0).ToString();
            //                paddingnolength = sNum.Length.ToString();
            //                paddingno = "00000";
            //                pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
            //                Session["CurrentNo"] = (Convert.ToInt32(sNum));
            //            }
            //            else
            //            {
            //                sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
            //                paddingnolength = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString().Length.ToString();
            //                paddingno = "00000";
            //                pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
            //                Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
            //            }
            //            item = pono.TrimEnd();
            //            Session["NO"] = item;
            //        }
            //    }
            //    else
            //    {
            //        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
            //        {
            //            paddingnolength = sNum.Length.ToString();
            //            paddingno = "00000";
            //            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
            //            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
            //        }
            //        else if (CurrentNo != "" && CurrentNo != null)
            //        {
            //            paddingnolength = CurrentNo.Length.ToString();
            //            paddingno = "00000";
            //            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
            //            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
            //        }
            //        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
            //        {
            //            sNum = (Convert.ToInt32(sNum) + 0).ToString();
            //            paddingnolength = sNum.Length.ToString();
            //            paddingno = "00000";
            //            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
            //            Session["CurrentNo"] = (Convert.ToInt32(sNum));
            //        }
            //        else
            //        {
            //            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
            //            paddingnolength = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString().Length.ToString();
            //            paddingno = "00000";
            //            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
            //            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
            //        }
            //        item = (pono.TrimEnd());
            //        Session["NO"] = item;
            //    }
            //}
            //else
            //{
            //    if (sNum == "") sNum = "00001";
            //    else sNum = sNum;
            //    sNum = (Convert.ToInt32(sNum) + 0).ToString();
            //    paddingnolength = sNum.Length.ToString();
            //    paddingno = "00000";
            //    if (padt.Rows.Count > 0)
            //    { CurrentNo = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, ""))).ToString(paddingno); }
            //    if (listTM != null)
            //    {
            //        if (CurrentNo == "") CurrentNo = "00001";
            //        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
            //        else
            //        {
            //            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
            //        }
            //    }
            //    if (CurrentNo != "" && CurrentNo != null)
            //    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
            //    else
            //        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
            //    Session["CurrentNo"] = (Convert.ToInt32(sNum));
            //    item = (pono.TrimEnd());
            //    Session["NO"] = item;
            //}

            return PartialView("CustomerOnfly");

        }

        [HttpPost]
        public ActionResult PricingGrid(string list)
        {
            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            Session["PricingGrid"] = ds;
            return null;
        }

//EMPHCS1781 Retail Creation of Customer and Item Master by gnaneshwar on 9/7/2016
        [HttpPost]
        public ActionResult Insert(FormCollection col) // Calling on http post (on Submit)
        {
            try
            {

                int OutParam = 0;
                string OrderType = "1265";
                string Date = DateTime.Now.ToString();

                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert", TransNo = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE002 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                cn.Open();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", 1265);
               
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Customer#"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Name"]);
                Cmd.Parameters.AddWithValue("@COLUMN06", col["Memo"]);
                Cmd.Parameters.AddWithValue("@COLUMN07", col["Category"]);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["SubCategory"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", col["Email"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["Phone"]);
                Cmd.Parameters.AddWithValue("@COLUMN12", col["Fax"]);

                var InactiveFlag = Convert.ToString(col["COLUMN25"]);

                if (InactiveFlag == "on" || InactiveFlag == "True" || InactiveFlag == "1") InactiveFlag = "1";
                else InactiveFlag = "0";
                Cmd.Parameters.AddWithValue("@COLUMN25", InactiveFlag);

                var DefaultFlag = Convert.ToString(col["COLUMN40"]);

                if (DefaultFlag == "on" || DefaultFlag == "True" || DefaultFlag == "1") DefaultFlag = "1";
                else DefaultFlag = "0";
                Cmd.Parameters.AddWithValue("@COLUMN40", DefaultFlag);

                var ouselected = Convert.ToString(col["OperatingUnit"]);
                if (ouselected == "" || ouselected == "0") ouselected = null;
                Cmd.Parameters.AddWithValue("@COLUMN17", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMN30", col["PaymentTerms"]);
                Cmd.Parameters.AddWithValue("@COLUMN31", col["PaymentMode"]);
                Cmd.Parameters.AddWithValue("@COLUMN20", col["Department"]);
                Cmd.Parameters.AddWithValue("@COLUMN22", col["SalesRep"]);
                decimal com;
                if (col["Commission"] == "")
                    com = 0;
                else
                    com = Convert.ToDecimal(col["Commission"]);
                Cmd.Parameters.AddWithValue("@COLUMN23", com);
                
                Cmd.Parameters.AddWithValue("@COLUMN24", col["Type"]);


                Cmd.Parameters.AddWithValue("@COLUMNA02", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Convert.ToInt32(Session["eid"]));
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE002");
				//EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                // Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                //OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                SqlCommand cmdA = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE003 ", cn);
                SqlDataAdapter daaA = new SqlDataAdapter(cmdA);
                DataTable dttA = new DataTable();
                daaA.Fill(dttA);
                string HCol2A = dttA.Rows[0][0].ToString();
                HCol2A = (Convert.ToInt32(HCol2A) + 1).ToString();
               
                SqlCommand CmdA = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                CmdA.CommandType = CommandType.StoredProcedure;
                CmdA.Parameters.AddWithValue("@COLUMN02", HCol2A);
                CmdA.Parameters.AddWithValue("@COLUMN03", 1259);

                CmdA.Parameters.AddWithValue("@COLUMN06", col["Addressee"]);
                CmdA.Parameters.AddWithValue("@COLUMN07", col["Address1"]);
                CmdA.Parameters.AddWithValue("@COLUMN08", col["Address2"]);
                CmdA.Parameters.AddWithValue("@COLUMN16", col["Country"]);
                CmdA.Parameters.AddWithValue("@COLUMN11", col["State"]);
                CmdA.Parameters.AddWithValue("@COLUMN10", col["City"]);
                CmdA.Parameters.AddWithValue("@COLUMN12", col["Zip"]);
				//EMPHCS1790 rajasekhar reddy patakota 28/07/2016 Identity Column setup Through out application
                CmdA.Parameters.AddWithValue("@COLUMN20", OutParam);
                int b;
                if (col["Billing"] == "" || col["Billing"] == "false")
                    b = 0;
                else
                    b = 1;
                CmdA.Parameters.AddWithValue("@COLUMN17", b);
                int c;
                if (col["Shipping"] == "" || col["Shipping"] == "false")
                    c = 0;
                else
                    c = 1;
                CmdA.Parameters.AddWithValue("@COLUMN18", b);



                CmdA.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                CmdA.Parameters.AddWithValue("@COLUMNA03", AOwner);
                CmdA.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdA.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdA.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdA.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdA.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdA.Parameters.AddWithValue("@Direction", insert);
                CmdA.Parameters.AddWithValue("@TabelName", "SATABLE003");
                CmdA.Parameters.AddWithValue("@ReturnValue", "");
                int r1 = CmdA.ExecuteNonQuery();
                if (r > 0)
                {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);

                var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();
                
                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN11 == custfname && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }

                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1265 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //Line


                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application

                cn.Close();
                if (OutParam > 0)
                {
                    DataSet itemdata = new DataSet();
                    itemdata = (DataSet)Session["PricingGrid"];
                    if (itemdata.Tables.Count > 0)
                    {
                        SqlCommand cmddl = new SqlCommand("select  max(COLUMN02) from SATABLE002 where COLUMN01=" + OutParam + "", cn);
                        SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                        DataTable dttl = new DataTable();
                        daal.Fill(dttl);
                        var HColid = dttl.Rows[0][0].ToString();
                        SqlCommand cmddpl = new SqlCommand("select  max(isnull(COLUMN02,999)) from MATABLE026", cn);
                        SqlDataAdapter daapl = new SqlDataAdapter(cmddpl);
                        DataTable dttpl = new DataTable();
                        daapl.Fill(dttpl);
                        var PLColid = dttpl.Rows[0][0].ToString();
                        PLColid = (Convert.ToInt32(PLColid) + 1).ToString();
                        for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                        {
                            string OU = itemdata.Tables[0].Rows[i]["COLUMN04"].ToString();
                            string Item = itemdata.Tables[0].Rows[i]["COLUMN05"].ToString();
                            string Pricelevel = itemdata.Tables[0].Rows[i]["COLUMN06"].ToString();
                            string Discount = itemdata.Tables[0].Rows[i]["COLUMN07"].ToString();
                            string Amount = itemdata.Tables[0].Rows[i]["COLUMN08"].ToString();
                            SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                            cn.Open();
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", PLColid);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", HColid);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", OU);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", Item);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", Pricelevel);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", Discount);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", Amount);
                            Cmdl.Parameters.AddWithValue("@COLUMN09", OutParam);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", ouselected);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE026");
                            Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                            int o = Cmdl.ExecuteNonQuery();
                            cn.Close();
                        }
                    }
                    Session["PricingGrid"] = null;
                }
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + " SATABLE002"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1265", "110007017", "Update"); }
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Index", "Customer", new { idi = idi, FormName = custform });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }


        [HttpGet]
        public ActionResult Edit(string ide)
        {

            Session["editid"] = ide;
            Session["DDDynamicItems"] = "";
            var ac = Convert.ToInt32(Session["AcOwner"]);
            int? op = null;
            string opstatus = Convert.ToString(Session["OPUnitstatus"]);
            if (opstatus == null || opstatus == "") op = null;
            else op = Convert.ToInt32(Session["OPUnit"]);
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var edit = activity.SATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMNA02 == op || a.COLUMNA02 == null) && a.COLUMN02 == ide).ToList();

            SqlCommand cmd1 = new SqlCommand("select * from SATABLE003  Where   (" + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)   AND COLUMNA03='" + ac + "' and COLUMN20='" + edit[0].COLUMN01 + "' and COLUMN19='Customer' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            
            var OPeratingUnit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
            ViewBag.OPUnit = OPeratingUnit;
            List<SelectListItem> PriceLevel = new List<SelectListItem>();
            SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE023  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            for (int dd = 0; dd < dtp.Rows.Count; dd++)
            {
                PriceLevel.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN04"].ToString() });
            }
            PriceLevel = PriceLevel.OrderBy(x => x.Text).ToList();
            ViewData["PriceLevel"] = new SelectList(PriceLevel, "Value", "Text", selectedValue: dtp.Rows[0]["COLUMN02"].ToString());
            List<SelectListItem> Item = new List<SelectListItem>();
            SqlCommand cmdr = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dar = new SqlDataAdapter(cmdr);
            DataTable dtr = new DataTable();
            dar.Fill(dtr);
            for (int dd = 0; dd < dtr.Rows.Count; dd++)
            {
                Item.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
            }
            Item = Item.OrderBy(x => x.Text).ToList();
            ViewBag.Item = new SelectList(Item, "Value", "Text", selectedValue: dtr.Rows[0]["COLUMN04"].ToString());
            var sql = "select COLUMN02,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08 from MATABLE026 where COLUMN03 in(" + ide + ") AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(sql);
            ViewBag.itemsdata = books;
            ViewBag.dvalOU = edit[0].COLUMN17;

            var Category = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11120).ToList();
            ViewBag.Category = Category;
            ViewBag.dvalCatg = edit[0].COLUMN07; 

            var subCategory = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11121).ToList();
            ViewBag.subCategory = subCategory;
            ViewBag.dvalsubC = edit[0].COLUMN08; 
            var Terms = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11122).ToList();
            ViewBag.Terms = Terms;
            ViewBag.dvalT = edit[0].COLUMN30; 
            var Mode = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11123).ToList();
            ViewBag.Mode = Mode;
            ViewBag.dvalmo = edit[0].COLUMN31; 
            var country = activity.MATABLE016.ToList();
            ViewBag.country = country;

            ViewBag.dvalC = Convert.ToString(dt1.Rows[0]["COLUMN16"]); 
            //ViewData["country"] = new SelectList(country, "COLUMN02", "COLUMN03", selectedValue: dt1.Rows[0]["COLUMN16"].ToString());
            var department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
            ViewBag.department = department;
            ViewBag.dvalDept = edit[0].COLUMN20; 
            var state = activity.MATABLE017.ToList();
            ViewBag.state = state;
            ViewBag.dvalS = dt1.Rows[0]["COLUMN11"].ToString(); 

            var SalesRep = db.MATABLE010.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN30 == true).ToList();
            ViewBag.SalesRep = SalesRep;
            ViewBag.dvalSR = edit[0].COLUMN22;
            var Type = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11130).ToList();
            ViewBag.Type = Type;
            ViewBag.dvalty = edit[0].COLUMN24;
            var Section = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11153).ToList();
            ViewBag.Section = Section;

            ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA03 == ac).ToList();
            eBizSuiteAppBAL.Fombuilding.CustInsert all = new eBizSuiteAppBAL.Fombuilding.CustInsert();
            var col2 = activity.SATABLE002.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
            var col1 = col2;
            eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
            var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).ToList();
            int? Oper = null;
            opstatus = Convert.ToString(Session["OPUnitstatus"]);
            if (opstatus == null || opstatus == "") op = null;
            else op = Convert.ToInt32(Session["OPUnit"]);
            //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 15/09/2015
            Session["AUTO"] = Oper;
            if (Oper == 0)
                Oper = null;
            var acOW = Convert.ToInt32(Session["AcOwner"]);
            var fid = 1265;
            var customfrmID = 1265;

            all.COLUMN04 =Convert.ToString(edit[0].COLUMN04);
            all.COLUMN05 =Convert.ToString(edit[0].COLUMN05);
            all.COLUMN06 =Convert.ToString(edit[0].COLUMN06);
            all.COLUMN10 =Convert.ToString(edit[0].COLUMN10);
            all.COLUMN11 =Convert.ToString(edit[0].COLUMN11);
            all.COLUMN12 =Convert.ToString(edit[0].COLUMN12);
            all.COLUMN23 = edit[0].COLUMN23;
            //all.COLUMN40 = edit[0].COLUMN40;
            //all.COLUMN25 = edit[0].COLUMN25;
            if (edit[0].COLUMN25 == true) ViewBag.COLUMN25 = "checked";
            if (edit[0].COLUMN40 == true) ViewBag.COLUMN40 = "checked";
            if (dt1.Rows[0]["COLUMN17"].ToString() == "true") ViewBag.chkB = "checked";
            if (dt1.Rows[0]["COLUMN18"].ToString() == "true") ViewBag.chkS = "checked";

            all.SCOLUMN06 = dt1.Rows[0]["COLUMN06"].ToString();
            all.SCOLUMN07 = dt1.Rows[0]["COLUMN07"].ToString();
            all.SCOLUMN08 = dt1.Rows[0]["COLUMN08"].ToString();
            all.SCOLUMN16 = dt1.Rows[0]["COLUMN16"].ToString();
            all.SCOLUMN11 = dt1.Rows[0]["COLUMN11"].ToString();
            all.SCOLUMN10 = dt1.Rows[0]["COLUMN10"].ToString();
            all.SCOLUMN12 = dt1.Rows[0]["COLUMN12"].ToString();
            all.SCOLUMN17 = dt1.Rows[0]["COLUMN17"].ToString();
            all.SCOLUMN18 = dt1.Rows[0]["COLUMN18"].ToString();

            return View(all);

        }


//EMPHCS1781 Retail Creation of Customer and Item Master by gnaneshwar on 9/7/2016
        [HttpGet]
        public ActionResult View(string ide)
        {

            Session["editid"] = ide;
            Session["DDDynamicItems"] = "";
            var ac = Convert.ToInt32(Session["AcOwner"]);
            int? op = null;
            string opstatus = Convert.ToString(Session["OPUnitstatus"]);
            if (opstatus == null || opstatus == "") op = null;
            else op = Convert.ToInt32(Session["OPUnit"]);
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var edit = activity.SATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && (a.COLUMNA02 == op || a.COLUMNA02 == null) && a.COLUMN02 == ide).ToList();

            SqlCommand cmd1 = new SqlCommand("select * from SATABLE003  Where   (" + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)   AND COLUMNA03='" + ac + "' and COLUMN20='" + edit[0].COLUMN01 + "' and COLUMN19='Customer' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);

            var OPeratingUnit = db.CONTABLE007.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN07 == false).ToList();
            ViewBag.OPUnit = OPeratingUnit;
            List<SelectListItem> PriceLevel = new List<SelectListItem>();
            SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE023  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            for (int dd = 0; dd < dtp.Rows.Count; dd++)
            {
                PriceLevel.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN04"].ToString() });
            }
            PriceLevel = PriceLevel.OrderBy(x => x.Text).ToList();
            ViewData["PriceLevel"] = new SelectList(PriceLevel, "Value", "Text", selectedValue: dtp.Rows[0]["COLUMN02"].ToString());
            List<SelectListItem> Item = new List<SelectListItem>();
            SqlCommand cmdr = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dar = new SqlDataAdapter(cmdr);
            DataTable dtr = new DataTable();
            dar.Fill(dtr);
            for (int dd = 0; dd < dtr.Rows.Count; dd++)
            {
                Item.Add(new SelectListItem { Value = dtr.Rows[dd]["COLUMN02"].ToString(), Text = dtr.Rows[dd]["COLUMN04"].ToString() });
            }
            Item = Item.OrderBy(x => x.Text).ToList();
            ViewBag.Item = new SelectList(Item, "Value", "Text", selectedValue: dtr.Rows[0]["COLUMN04"].ToString());
            var sql = "select COLUMN02,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMN08 from MATABLE026 where COLUMN03 in(" + ide + ") AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(sql);
            ViewBag.itemsdata = books;
            ViewBag.dvalOU = edit[0].COLUMN17;

            var Category = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11120).ToList();
            ViewBag.Category = Category;
            ViewBag.dvalCatg = edit[0].COLUMN07;

            var subCategory = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11121).ToList();
            ViewBag.subCategory = subCategory;
            ViewBag.dvalsubC = edit[0].COLUMN08;
            var Terms = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11122).ToList();
            ViewBag.Terms = Terms;
            ViewBag.dvalT = edit[0].COLUMN30;
            var Mode = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11123).ToList();
            ViewBag.Mode = Mode;
            ViewBag.dvalmo = edit[0].COLUMN31;
            var country = activity.MATABLE016.ToList();
            ViewBag.country = country;

            ViewBag.dvalC = Convert.ToString(dt1.Rows[0]["COLUMN16"]);
            //ViewData["country"] = new SelectList(country, "COLUMN02", "COLUMN03", selectedValue: dt1.Rows[0]["COLUMN16"].ToString());
            var department = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN03 == 11117).ToList();
            ViewBag.department = department;
            ViewBag.dvalDept = edit[0].COLUMN20;
            var state = activity.MATABLE017.ToList();
            ViewBag.state = state;
            ViewBag.dvalS = dt1.Rows[0]["COLUMN11"].ToString();

            var SalesRep = db.MATABLE010.Where(a => a.COLUMNA13 == false && a.COLUMNA03 == ac && a.COLUMN30 == true).ToList();
            ViewBag.SalesRep = SalesRep;
            ViewBag.dvalSR = edit[0].COLUMN22;
            var Type = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11130).ToList();
            ViewBag.Type = Type;
            ViewBag.dvalty = edit[0].COLUMN24;
            var Section = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11153).ToList();
            ViewBag.Section = Section;

            ViewBag.s = db.CONTABLE009.Where(q => q.COLUMNA03 == ac).ToList();
            eBizSuiteAppBAL.Fombuilding.CustInsert all = new eBizSuiteAppBAL.Fombuilding.CustInsert();
            var col2 = activity.SATABLE002.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
            var col1 = col2;
            eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
            var idc = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).ToList();
            int? Oper = null;
            opstatus = Convert.ToString(Session["OPUnitstatus"]);
            if (opstatus == null || opstatus == "") op = null;
            else op = Convert.ToInt32(Session["OPUnit"]);
            //EMPHCS1144 TRANSACTION NUMBER GENERATION CHANGES BY SRINIVAS 15/09/2015
            Session["AUTO"] = Oper;
            if (Oper == 0)
                Oper = null;
            var acOW = Convert.ToInt32(Session["AcOwner"]);
            var fid = 1265;
            var customfrmID = 1265;

            all.COLUMN04 = Convert.ToString(edit[0].COLUMN04);
            all.COLUMN05 = Convert.ToString(edit[0].COLUMN05);
            all.COLUMN06 = Convert.ToString(edit[0].COLUMN06);
            all.COLUMN10 = Convert.ToString(edit[0].COLUMN10);
            all.COLUMN11 = Convert.ToString(edit[0].COLUMN11);
            all.COLUMN12 = Convert.ToString(edit[0].COLUMN12);
            all.COLUMN23 = edit[0].COLUMN23;
           
            if (edit[0].COLUMN25 == true) ViewBag.COLUMN25 = "checked";
            if (edit[0].COLUMN40 == true) ViewBag.COLUMN40 = "checked";
            if (dt1.Rows[0]["COLUMN17"].ToString() == "true") ViewBag.chkB = "checked";
            if (dt1.Rows[0]["COLUMN18"].ToString() == "true") ViewBag.chkS = "checked";

            all.SCOLUMN06 = dt1.Rows[0]["COLUMN06"].ToString();
            all.SCOLUMN07 = dt1.Rows[0]["COLUMN07"].ToString();
            all.SCOLUMN08 = dt1.Rows[0]["COLUMN08"].ToString();
            all.SCOLUMN16 = dt1.Rows[0]["COLUMN16"].ToString();
            all.SCOLUMN11 = dt1.Rows[0]["COLUMN11"].ToString();
            all.SCOLUMN10 = dt1.Rows[0]["COLUMN10"].ToString();
            all.SCOLUMN12 = dt1.Rows[0]["COLUMN12"].ToString();
            all.SCOLUMN17 = dt1.Rows[0]["COLUMN17"].ToString();
            all.SCOLUMN18 = dt1.Rows[0]["COLUMN18"].ToString();

            return View(all);

        }

        [HttpPost]
        public ActionResult Edit(FormCollection col) // Calling on http post (on Submit)
        {
            try
            {
                int editid = Convert.ToInt32(Session["editid"]);
                int OutParam = 0;
                string OrderType = "1265";
                string Date = DateTime.Now.ToString();

                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update", TransNo = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from SATABLE002 where COLUMN02=" + editid + "", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                cn.Open();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", editid);
                Cmd.Parameters.AddWithValue("@COLUMN03", 1265);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["COLUMN04"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["COLUMN05"]);
                Cmd.Parameters.AddWithValue("@COLUMN06", col["COLUMN06"]);
                Cmd.Parameters.AddWithValue("@COLUMN07", col["COLUMN07"]);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["COLUMN08"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", col["COLUMN10"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["COLUMN11"]);
                Cmd.Parameters.AddWithValue("@COLUMN12", col["COLUMN12"]);


                var InactiveFlag = Convert.ToString(col["COLUMN25"]);

                if (InactiveFlag == "on" || InactiveFlag == "True" || InactiveFlag == "1") InactiveFlag = "1";
                else InactiveFlag = "0";
                Cmd.Parameters.AddWithValue("@COLUMN25", InactiveFlag);

                var DefaultFlag = Convert.ToString(col["COLUMN40"]);

                if (DefaultFlag == "on" || DefaultFlag == "True" || DefaultFlag == "1") DefaultFlag = "1";
                else DefaultFlag = "0";
                Cmd.Parameters.AddWithValue("@COLUMN40", DefaultFlag);

                var ouselected = Convert.ToString(col["COLUMN17"]);
                if (ouselected == "" || ouselected == "0") ouselected = null;
                Cmd.Parameters.AddWithValue("@COLUMN17", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMN30", col["COLUMN30"]);
                Cmd.Parameters.AddWithValue("@COLUMN31", col["COLUMN31"]);
                Cmd.Parameters.AddWithValue("@COLUMN20", col["COLUMN20"]);
                Cmd.Parameters.AddWithValue("@COLUMN22", col["COLUMN22"]);
                decimal com;
                if (col["COLUMN23"] == "")
                    com = 0;
                else
                    com = Convert.ToDecimal(col["COLUMN23"]);
                Cmd.Parameters.AddWithValue("@COLUMN23", com);

                Cmd.Parameters.AddWithValue("@COLUMN24", col["COLUMN24"]);


                Cmd.Parameters.AddWithValue("@COLUMNA02", ouselected);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Convert.ToInt32(Session["eid"]));
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE002");
                Cmd.Parameters.Add("@ReturnValue", "");
                // Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                //OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                //SqlCommand cmdA = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE003 ", cn);
                //SqlDataAdapter daaA = new SqlDataAdapter(cmdA);
                //DataTable dttA = new DataTable();
                //daaA.Fill(dttA);
                //string HCol2A = dttA.Rows[0][0].ToString();
                //HCol2A = (Convert.ToInt32(HCol2A) + 1).ToString();
                SqlCommand cmd1 = new SqlCommand("select  Max(COLUMN02) from SATABLE003 where COLUMN20=" + HCol1 + " and  COLUMN19='Customer' and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                int HColc = Convert.ToInt32(dt1.Rows[0][0].ToString());
                if (HColc > 0)
                {

                    SqlCommand CmdA = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                    CmdA.CommandType = CommandType.StoredProcedure;
                    CmdA.Parameters.AddWithValue("@COLUMN02", HColc);
                    CmdA.Parameters.AddWithValue("@COLUMN06", col["SCOLUMN06"]);
                    CmdA.Parameters.AddWithValue("@COLUMN07", col["SCOLUMN07"]);
                    CmdA.Parameters.AddWithValue("@COLUMN08", col["SCOLUMN08"]);
                    CmdA.Parameters.AddWithValue("@COLUMN16", col["SCOLUMN16"]);
                    CmdA.Parameters.AddWithValue("@COLUMN11", col["SCOLUMN11"]);
                    CmdA.Parameters.AddWithValue("@COLUMN10", col["SCOLUMN10"]);
                    CmdA.Parameters.AddWithValue("@COLUMN12", col["SCOLUMN12"]);
                    CmdA.Parameters.AddWithValue("@COLUMN20", HCol1);
                    int b;
                    if (col["SCOLUMN17"] == "" || col["SCOLUMN17"] == "false")
                        b = 0;
                    else
                        b = 1;
                    CmdA.Parameters.AddWithValue("@COLUMN17", b);
                    int c;
                    if (col["SCOLUMN18"] == "" || col["SCOLUMN18"] == "false")
                        c = 0;
                    else
                        c = 1;
                    CmdA.Parameters.AddWithValue("@COLUMN18", c);
                    CmdA.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    CmdA.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    CmdA.Parameters.AddWithValue("@COLUMNA06", Date);
                    CmdA.Parameters.AddWithValue("@COLUMNA07", Date);
                    CmdA.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    CmdA.Parameters.AddWithValue("@COLUMNA12", 1);
                    CmdA.Parameters.AddWithValue("@COLUMNA13", 0);
                    CmdA.Parameters.AddWithValue("@Direction", insert);
                    CmdA.Parameters.AddWithValue("@TabelName", "SATABLE003");
                    CmdA.Parameters.Add("@ReturnValue", "");
                    int r1 = CmdA.ExecuteNonQuery();
                }
                // Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                if (r > 0)
                {

                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1265 && q.COLUMN05 == 2).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                cn.Close();
                if (editid > 0)
                {
                    DataSet itemdata = new DataSet();
                    itemdata = (DataSet)Session["PricingGrid"];
                    if (itemdata.Tables.Count > 0)
                    {
                        SqlCommand cmddl = new SqlCommand("select  max(COLUMN01) from SATABLE002 where COLUMN02=" + editid + "", cn);
                        SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                        DataTable dttl = new DataTable();
                        daal.Fill(dttl);
                        var HColid = dttl.Rows[0][0].ToString();
                        for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                        {
                            string Id = itemdata.Tables[0].Rows[i]["COLUMN02"].ToString();
                            string OU = itemdata.Tables[0].Rows[i]["COLUMN04"].ToString();
                            string Item = itemdata.Tables[0].Rows[i]["COLUMN05"].ToString();
                            string Pricelevel = itemdata.Tables[0].Rows[i]["COLUMN06"].ToString();
                            string Discount = itemdata.Tables[0].Rows[i]["COLUMN07"].ToString();
                            string Amount = itemdata.Tables[0].Rows[i]["COLUMN08"].ToString();
                            SqlCommand cmddpl = new SqlCommand();
                            if (Id != "0")
                                cmddpl = new SqlCommand("select  max(isnull(COLUMN02,999)) from MATABLE026 where COLUMN02=" + Id + "", cn);
                            else
                                cmddpl = new SqlCommand("select  max(isnull(COLUMN02,999)) from MATABLE026", cn);
                            SqlDataAdapter daapl = new SqlDataAdapter(cmddpl);
                            DataTable dttpl = new DataTable();
                            daapl.Fill(dttpl);
                            var PLColid = dttpl.Rows[0][0].ToString();
                            if (Id == "0")
                                PLColid = (Convert.ToInt32(PLColid) + 1).ToString();
                            SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                            cn.Open();
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", PLColid);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", editid);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", OU);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", Item);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", Pricelevel);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", Discount);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", Amount);
                            Cmdl.Parameters.AddWithValue("@COLUMN09", HColid);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", ouselected);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE026");
                            Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                            int o = Cmdl.ExecuteNonQuery();
                            cn.Close();
                        }
                    }
                    Session["PricingGrid"] = null;
                }
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + " SATABLE002"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1265", "110007017", "Update"); }
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Index", "Customer", new { idi = idi, FormName = custform });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }




        public ActionResult Delete()
        {
            try
            {
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                cn.Open();
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from SATABLE002 where COLUMN02 =" + HCol2 + "", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                string Date = DateTime.Now.ToString();

                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OPunit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Convert.ToInt32(Session["eid"]));
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE002");
                Cmd.Parameters.Add("@ReturnValue", "");
                // Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                //OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                //SqlCommand cmdA = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE003 ", cn);
                //SqlDataAdapter daaA = new SqlDataAdapter(cmdA);
                //DataTable dttA = new DataTable();
                //daaA.Fill(dttA);
                //string HCol2A = dttA.Rows[0][0].ToString();
                //HCol2A = (Convert.ToInt32(HCol2A) + 1).ToString();
                SqlCommand cmd1 = new SqlCommand("select  Max(COLUMN02) from SATABLE003 where COLUMN20 =" + HCol1 + "", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                int HColc = Convert.ToInt32(dt1.Rows[0][0].ToString());
                if (HColc > 0)
                {
                    SqlCommand CmdA = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                    CmdA.CommandType = CommandType.StoredProcedure;
                    CmdA.Parameters.AddWithValue("@COLUMN02", HColc);
                    CmdA.Parameters.AddWithValue("@COLUMNA02", OPunit);
                    CmdA.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    CmdA.Parameters.AddWithValue("@COLUMNA06", Date);
                    CmdA.Parameters.AddWithValue("@COLUMNA07", Date);
                    CmdA.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    CmdA.Parameters.AddWithValue("@COLUMNA12", 1);
                    CmdA.Parameters.AddWithValue("@COLUMNA13", 1);
                    CmdA.Parameters.AddWithValue("@Direction", insert);
                    CmdA.Parameters.AddWithValue("@TabelName", "SATABLE003");
                    CmdA.Parameters.Add("@ReturnValue", "");
                    int r1 = CmdA.ExecuteNonQuery();
                }
                if (r > 0)
                {
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1265 && q.COLUMN05 == 4).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                cn.Close();
                HttpContext.Cache[Session["AcOwner"] + " " + Session["OPUnit"] + " SATABLE002"] = "";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1265", "110007017", "Update"); }
                return RedirectToAction("Index", "Customer", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
		//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
        [HttpGet]
        public ActionResult Create(string FormName, string idi)
        {
            Session["DDDynamicItems"] = "";
            var ac = Convert.ToInt32(Session["AcOwner"]);         
            
            List<SelectListItem> country = new List<SelectListItem>();
            SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE016 ", con);
            SqlDataAdapter dac = new SqlDataAdapter(cmdc);
            DataTable dtc = new DataTable();
            dac.Fill(dtc);
            for (int dd = 0; dd < dtc.Rows.Count; dd++)
            {
                country.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN03"].ToString() });
            }

            country = country.OrderBy(x => x.Text).ToList();
            ViewData["country"] = new SelectList(country, "Value", "Text");

            List<SelectListItem> state = new List<SelectListItem>();
            SqlCommand cmds = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE017 ", con);
            SqlDataAdapter das = new SqlDataAdapter(cmds);
            DataTable dts = new DataTable();
            das.Fill(dts);
            for (int dd = 0; dd < dts.Rows.Count; dd++)
            {
                state.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN03"].ToString() });
            }

            state = state.OrderBy(x => x.Text).ToList();
            ViewData["state"] = new SelectList(state, "Value", "Text");

            List<SelectListItem> CategoryS = new List<SelectListItem>();
            SqlCommand cmdsc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' and COLUMN03 = 11120", con);
            SqlDataAdapter dasc = new SqlDataAdapter(cmdsc);
            DataTable dtsc = new DataTable();
            dasc.Fill(dtsc);
            for (int dd = 0; dd < dtsc.Rows.Count; dd++)
            {
                CategoryS.Add(new SelectListItem { Value = dtsc.Rows[dd]["COLUMN02"].ToString(), Text = dtsc.Rows[dd]["COLUMN04"].ToString() });
            }

            CategoryS = CategoryS.OrderBy(x => x.Text).ToList();
            ViewData["CategoryS"] = new SelectList(CategoryS, "Value", "Text");

            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
			//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
            //List<SelectListItem> family = new List<SelectListItem>();
            //SqlConnection cn1 = new SqlConnection(sqlcon);
            //SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE003  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            //SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            //DataTable dt1 = new DataTable();
            //da1.Fill(dt1);
            //for (int dd = 0; dd < dt1.Rows.Count; dd++)
            //{
            //    family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            //}
            //ViewData["family"] = new SelectList(family, "Value", "Text");

            //List<SelectListItem> group = new List<SelectListItem>();
            //SqlConnection cn2 = new SqlConnection(sqlcon);
            //SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE004  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
            //SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            //DataTable dt2 = new DataTable();
            //da2.Fill(dt2);
            //for (int dd = 0; dd < dt2.Rows.Count; dd++)
            //{
            //    group.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            //}
            //ViewData["group"] = new SelectList(group, "Value", "Text");

            //List<SelectListItem> type = new List<SelectListItem>();
            //SqlConnection cn3 = new SqlConnection(sqlcon);
            //SqlCommand cmd3 = new SqlCommand("select COLUMN02,COLUMN03 from MATABLE008 ", cn);
            //SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            //DataTable dt3 = new DataTable();
            //da3.Fill(dt3);
            //for (int dd = 0; dd < dt3.Rows.Count; dd++)
            //{
            //    type.Add(new SelectListItem { Value = dt3.Rows[dd]["COLUMN02"].ToString(), Text = dt3.Rows[dd]["COLUMN03"].ToString() });
            //}
            //ViewData["type"] = new SelectList(type, "Value", "Text");


            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection col)
        {
            try
            {

                int OutParam = 0;
                string OrderType = "1265";
                string Date = DateTime.Now.ToString();

                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string AOwner = Convert.ToString(Session["AcOwner"]);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                string insert = "Insert", TransNo = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE002 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                cn.Open();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", 1265);
                TransNo = GetTransactionNo(1265);
                Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["COLUMN05"]);
                Cmd.Parameters.AddWithValue("@COLUMN37", col["COLUMN37"]);
                Cmd.Parameters.AddWithValue("@COLUMN07", col["COLUMN07"]);
                Cmd.Parameters.AddWithValue("@COLUMN11", col["COLUMN11"]);
                Cmd.Parameters.AddWithValue("@COLUMN12", col["COLUMN12"]);
                Cmd.Parameters.AddWithValue("@COLUMN10", col["COLUMN10"]);

                Cmd.Parameters.AddWithValue("@COLUMN17", Oper);


                 var InactiveFlag = Convert.ToString(col["COLUMN25"]);
                if (InactiveFlag == "on" || InactiveFlag == "True" || InactiveFlag == "1") InactiveFlag = "1";
                else InactiveFlag = "0";
                Cmd.Parameters.AddWithValue("@COLUMN25", InactiveFlag);

                 var DefaultFlag = Convert.ToString(col["COLUMN40"]);
                if (DefaultFlag == "on" || DefaultFlag == "True" || DefaultFlag == "1") DefaultFlag = "1";
                else DefaultFlag = "0";
                Cmd.Parameters.AddWithValue("@COLUMN40", DefaultFlag);

              
                Cmd.Parameters.AddWithValue("@COLUMNA02", Oper);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Convert.ToInt32(Session["eid"]));
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE002");

                Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                int r = Cmd.ExecuteNonQuery();
                OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                SqlCommand cmdA = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE003 ", cn);
                SqlDataAdapter daaA = new SqlDataAdapter(cmdA);
                DataTable dttA = new DataTable();
                daaA.Fill(dttA);
                string HCol2A = dttA.Rows[0][0].ToString();
                HCol2A = (Convert.ToInt32(HCol2A) + 1).ToString();

                SqlCommand CmdA = new SqlCommand("usp_SAL_BL_CustomerMaster", cn);
                CmdA.CommandType = CommandType.StoredProcedure;
                CmdA.Parameters.AddWithValue("@COLUMN02", HCol2A);
                CmdA.Parameters.AddWithValue("@COLUMN03", 1259);
				//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                CmdA.Parameters.AddWithValue("@COLUMN06", col["Contact"]);
                CmdA.Parameters.AddWithValue("@COLUMN07", col["Address1"]);
                CmdA.Parameters.AddWithValue("@COLUMN08", col["Address2"]);
                CmdA.Parameters.AddWithValue("@COLUMN16", col["Country"]);
                CmdA.Parameters.AddWithValue("@COLUMN11", col["State"]);
                CmdA.Parameters.AddWithValue("@COLUMN10", col["City"]);
                CmdA.Parameters.AddWithValue("@COLUMN12", col["Zip"]);

                CmdA.Parameters.AddWithValue("@COLUMN20", OutParam);
                CmdA.Parameters.AddWithValue("@COLUMN17", DBNull.Value);
                CmdA.Parameters.AddWithValue("@COLUMN18", DBNull.Value);                

                CmdA.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                CmdA.Parameters.AddWithValue("@COLUMNA03", AOwner);
                CmdA.Parameters.AddWithValue("@COLUMNA06", Date);
                CmdA.Parameters.AddWithValue("@COLUMNA07", Date);
                CmdA.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                CmdA.Parameters.AddWithValue("@COLUMNA12", 1);
                CmdA.Parameters.AddWithValue("@COLUMNA13", 0);
                CmdA.Parameters.AddWithValue("@Direction", insert);
                CmdA.Parameters.AddWithValue("@TabelName", "SATABLE003");
                CmdA.Parameters.AddWithValue("@ReturnValue", "");
                int r1 = CmdA.ExecuteNonQuery();
                if (r > 0)
                {
                    eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                  
                    if (Oper == null || Oper == 0)
                        Oper = Convert.ToInt32(Session["OPUnit1"]);
                    if (Oper == 0)
                        Oper = null;
                    int? acOW = Convert.ToInt32(Session["AcOwner"]);

                    var custfname = db.CONTABLE0010.Where(q => q.COLUMN02 == 1265).FirstOrDefault().COLUMN04.ToString();
                    var listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02 == Oper).FirstOrDefault();

                    if (Oper == null)
                        listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN11 == custfname && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(Oper)).FirstOrDefault();

                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(q => q.COLUMN05 == custfname && q.COLUMN11 == "default" && q.COLUMNA03 == acOW && q.COLUMNA02.Equals(null)).FirstOrDefault();
                        }
                    }
                    if (listTM != null)
                    {
                        MYTABLE002 product = listTM;
                        if (listTM.COLUMN09 == "")
                            product.COLUMN09 = (1).ToString();
                        else
                            product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                        act1.SaveChanges();
                    }

                    List<SelectListItem> vendor = new List<SelectListItem>();
					//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN03,COLUMN01 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' and COLUMN05!=''", cn);
                        DataTable dtdata = new DataTable();
                        cmddl.Fill(dtdata);
                        ViewData["Customer"] = new SelectList(vendor, "Value", "Text");
                        for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                        {
                            if (OutParam ==Convert.ToInt32(dtdata.Rows[dd]["COLUMN01"].ToString()))
                                vendor.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString(), Selected = true });
                            else
                                vendor.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString() });
                        } vendor = vendor.OrderBy(x => x.Text).ToList();
                        Session["DDDynamicItems"] = null;
                   
                    return Json(new { item = vendor }, JsonRequestBehavior.AllowGet);

                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1265 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("FormBuild", "FormBuilding", new { idi = idi, FormName = custform });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult NameCheck(string ItemName, string VenName, string CustName)
        {
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                if (ItemName != null)
                    cmd = new SqlCommand("select count(column01) from matable007 where column04='" + ItemName + "' AND COLUMNA13='false' and COLUMNA03='" + Session["AcOwner"] + "'", cn);
                else if (VenName != null)
                    cmd = new SqlCommand("select count(column01) from SATABLE001 where column05='" + VenName + "' AND COLUMNA13='false' and COLUMNA03='" + Session["AcOwner"] + "'", cn);
                else if (CustName != null)
                    cmd = new SqlCommand("select count(column01) from SATABLE002 where column05='" + CustName + "' AND COLUMNA13='false' and COLUMNA03='" + Session["AcOwner"] + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                cn.Close();
                string ItemName1 = "";
                string VenName1 = "";
                string CustName1 = "";
                if (dt1.Rows.Count > 0)
                {
                    if (ItemName != null)
                        ItemName1 = dt1.Rows[0][0].ToString();
                    else if (VenName != null)
                        VenName1 = dt1.Rows[0][0].ToString();
                    else if (CustName != null)
                        CustName1 = dt1.Rows[0][0].ToString();
                }
                return Json(new { ItemName = ItemName1, VenName = VenName1, CustName = CustName1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

    }
}
