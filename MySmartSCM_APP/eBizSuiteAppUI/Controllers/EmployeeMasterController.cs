﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using WebMatrix.Data;
using eBizSuiteAppModel.Table;
using eBizSuiteDAL.classes;
using System.Web.Helpers;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;

namespace eBizSuiteProduct.Controllers
{
    public class EmployeeMasterController : Controller
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        EmployeeMasterManage userManager = new EmployeeMasterManage();
        //
        // GET: /EmployeeMaster/

        public ActionResult Index()
        {
            return View();
        }

        //get rolemaster
        [HttpGet]
        public ActionResult Info()
        {
            var acO = Convert.ToInt32(Session["AcOwner"]);
            ViewData["roleM"] = Session["roles"];
            var eID = Convert.ToInt32(Session["eid"]);
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var bulletin = activity.SETABLE003.Where(q => q.COLUMN06 <= DateTime.Now && q.COLUMN07 >= DateTime.Now).ToList();
            var taskMgt = activity.SETABLE002.Where(q => q.COLUMN09 <= DateTime.Now && q.COLUMN10 >= DateTime.Now && (q.COLUMN07 == eID || q.COLUMN06 == eID)).ToList();
            var scTask = activity.SETABLE001.Where(q => q.COLUMN07 <= DateTime.Now && q.COLUMN08 >= DateTime.Now && q.COLUMNA08 == eID).ToList();
            var recent = activity.PUTABLE013.Where(e => e.COLUMNA03 == acO).OrderByDescending(u => u.COLUMN02).Take(5).ToList();
            //var reTrans = activity..Where(q => q.COLUMN07 <= DateTime.Now && q.COLUMN08 >= DateTime.Now && q.COLUMNA08 == eID).ToList();
            ViewBag.itemsdata = bulletin;
            ViewBag.taskdata = taskMgt;
            ViewBag.schdata = scTask;
            ViewBag.recent = recent;
            var empID = Convert.ToInt32(Session["eid"]);
            var kpiID = activity.SETABLE008.Where(a => a.COLUMN07 == empID).Select(a => a.COLUMN04).ToList();
            ViewBag.ids = kpiID.Count();
            ViewBag.idsVal = kpiID;
			//EMPHCS711 Rajasekhar Patakota 20/7/2015 KpIs Setup For Home Page
            var chartnamelist =(activity.SETABLE007.Where(a => (kpiID).Contains(a.COLUMN02)).Select(a => a.COLUMN07).ToList());
            var chartids = activity.SETABLE007.Where(a => (kpiID).Contains(a.COLUMN02)).ToList();
            List<string> chartname = new List<string>();
            for (int i = 0; i < chartnamelist.Count; i++)
            {
                int chartnameid = Convert.ToInt32(chartnamelist[i]);
                chartname.Add( dbContext.MATABLE002.Where(a =>a.COLUMN02==chartnameid).FirstOrDefault().COLUMN04);
            }
            ViewBag.kpilist = activity.SETABLE008.Where(a => a.COLUMN07 == empID).Select(a => a.COLUMN04).ToList(); 
            ViewBag.chartname = chartname;
            var homePage = activity.MYTABLE001.Where(a => a.COLUMN09 == empID).OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN07).FirstOrDefault();
            var colorTheme = activity.MYTABLE001.Where(a => a.COLUMN09 == empID).OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN06).FirstOrDefault();
            ViewBag.hPage = homePage;
            ViewBag.cTheme = colorTheme;
            Session["cThem"] = colorTheme;
            return View();
        }
		//EMPHCS711 Rajasekhar Patakota 20/7/2015 KpIs Setup For Home Page
        public JsonResult MyCharts1(int id)
        {
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            var query = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN06).FirstOrDefault().ToString();
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";
            var db = Database.OpenConnectionString(connectionString, providerName);
            //EMPHCS711  Rajasekhar Patakota 20/7/2015 KpIs Setup For Home Page
            string OPUnitstatus =Convert.ToString(Session["OPUnitstatus"]);string opunit = null;
            if (OPUnitstatus == "1")
            { opunit ="p.COLUMNA02="+ Session["OPUnit"].ToString(); }
            else { opunit =" ( p.COLUMNA02 IN ("+ Session["OPUnit"].ToString()+") OR p.COLUMNA02 IS NULL ) ";  }
            string acowner = Session["AcOwner"].ToString();
            query = query.ToString().Replace("opunit", opunit);
            query = query.ToString().Replace("acowner", acowner);
            var data = db.Query(query).ToList();
            var query1 = from o in data.AsEnumerable()
                         select new { o.name,o.amt};
            var title = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN05).FirstOrDefault().ToString();
            var charttype = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN07).FirstOrDefault().ToString();
            int chartnameid = Convert.ToInt32(charttype);
            charttype = dbContext.MATABLE002.Where(a => a.COLUMN02 == chartnameid).FirstOrDefault().COLUMN04;
            return Json(new { query1, title,charttype }, JsonRequestBehavior.AllowGet);
        }
       public ActionResult MyCharts(int id)
        {
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            //var empID = Convert.ToInt32(Session["LogedEmployeeID"]);
            var kpiID = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN05).FirstOrDefault().ToString();
            //for (int i = 0; i < kpiID.Count(); i++)
            //{
            //    int id = Convert.ToInt32(kpiID[i]);
            var title = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN05).FirstOrDefault().ToString();
            var x = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN08).FirstOrDefault().ToString();
            var y = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN09).FirstOrDefault().ToString();
            var TypeOfGraph = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN07).FirstOrDefault().ToString();
            var query = activity.SETABLE007.Where(a => a.COLUMN02 == id).Select(a => a.COLUMN06).FirstOrDefault().ToString();
            int typeGraphID = Convert.ToInt32(TypeOfGraph);
            var chartName = dbContext.MATABLE002.Where(a => a.COLUMN02 == typeGraphID).Select(a => a.COLUMN04).FirstOrDefault().ToString().ToLower();
			//EMPHCS711 Rajasekhar Patakota 20/7/2015 KpIs Setup For Home Page
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";
            var db = Database.OpenConnectionString(connectionString, providerName);
            var data = db.Query(query);

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Yellow);
            myChart.AddLegend(title: title, name: null);
            //myChart.AddTitle(kpiID.ToUpper());
            myChart.SetXAxis(title: x, min: 0, max: 0.0 / 0.0);
            myChart.SetYAxis(title: y, min: 0, max: 0.0 / 0.0);
            myChart.AddSeries(
        name: chartName, 
        chartType: chartName,
        axisLabel: y,
        xValue: data,
        xField: x,
        yValues: data
        , yFields: y
        ).Write(format: "gif");
            //}

            return null;
        }

        public ActionResult MyChart()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) PUTABLE001.COLUMN04 as name,sum(PUTABLE015.COLUMN06) as vendor FROM PUTABLE001  inner JOIN PUTABLE014 ON PUTABLE014.COLUMN06=PUTABLE001.COLUMN02 inner JOIN  PUTABLE015 on PUTABLE015.COLUMN08=PUTABLE014.COLUMN01 group by PUTABLE001.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Vanilla3D)
                .AddSeries(
                name: "Purchage Order", legend: "PO vs Payments",
chartType: "pie",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write();
            return null;
        }

        public ActionResult MyChart1()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) PUTABLE001.COLUMN04 as name,sum(PUTABLE015.COLUMN06) as vendor FROM PUTABLE001  inner JOIN PUTABLE014 ON PUTABLE014.COLUMN06=PUTABLE001.COLUMN02 inner JOIN  PUTABLE015 on PUTABLE015.COLUMN08=PUTABLE014.COLUMN01 group by PUTABLE001.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Green)
                .AddSeries(
name: "Purchage Order",
chartType: "bar",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write();
            return null;
        }

        public ActionResult MyChart2()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) PUTABLE001.COLUMN04 as name,sum(PUTABLE015.COLUMN06) as vendor FROM PUTABLE001  inner JOIN PUTABLE014 ON PUTABLE014.COLUMN06=PUTABLE001.COLUMN02 inner JOIN  PUTABLE015 on PUTABLE015.COLUMN08=PUTABLE014.COLUMN01 group by PUTABLE001.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Green)
                .AddSeries(
name: "Purchage Order",
chartType: "column",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write(format: "gif");
            return null;
        }

        public ActionResult MyChart3()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) PUTABLE001.COLUMN04 as name,sum(PUTABLE015.COLUMN06) as vendor FROM PUTABLE001  inner JOIN PUTABLE014 ON PUTABLE014.COLUMN06=PUTABLE001.COLUMN02 inner JOIN  PUTABLE015 on PUTABLE015.COLUMN08=PUTABLE014.COLUMN01 group by PUTABLE001.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Yellow)
                .AddSeries(
name: "Purchage Order",
chartType: "area",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write();
            return null;
        }

        public ActionResult MyChart4()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) SATABLE005.COLUMN04 as name,sum(SATABLE012.COLUMN06) as vendor FROM SATABLE005  inner JOIN SATABLE011 ON SATABLE011.COLUMN06=SATABLE005.COLUMN02 and SATABLE005.COLUMN04 like 'so%' inner JOIN  SATABLE012 on SATABLE012.COLUMN08=SATABLE011.COLUMN01 group by SATABLE005.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Vanilla3D)
                .AddSeries(
name: "Purchage Order",
chartType: "pie",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write();
            return null;
        }

        public ActionResult MyChart5()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) SATABLE005.COLUMN04 as name,sum(SATABLE012.COLUMN06) as vendor FROM SATABLE005  inner JOIN SATABLE011 ON SATABLE011.COLUMN06=SATABLE005.COLUMN02 and SATABLE005.COLUMN04 like 'so%' inner JOIN  SATABLE012 on SATABLE012.COLUMN08=SATABLE011.COLUMN01 group by SATABLE005.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Yellow)
                .AddSeries(
name: "Purchage Order",
chartType: "column",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write(format: "gif");
            return null;
        }

        public ActionResult MyChart6()
        {
            var db = WebMatrix.Data.Database.Open("Con");
            var data = db.Query("SELECT TOP(10) SATABLE005.COLUMN04 as name,sum(SATABLE012.COLUMN06) as vendor FROM SATABLE005  inner JOIN SATABLE011 ON SATABLE011.COLUMN06=SATABLE005.COLUMN02 and SATABLE005.COLUMN04 like 'so%' inner JOIN  SATABLE012 on SATABLE012.COLUMN08=SATABLE011.COLUMN01 group by SATABLE005.COLUMN04");

            var myChart = new Chart(width: 500,
                                height: 240,
                                theme: ChartTheme.Yellow)
                .AddSeries(
name: "Purchage Order",
chartType: "area",
axisLabel: "name",
xValue: data,
xField: "name",
yValues: data,
yFields: "vendor").Write();
            return null;
        }

        //create
        [HttpGet]
        public ActionResult NewEmployeeMaster()
        {
            return View();
        }
        //create
        [HttpPost]
        public ActionResult NewEmployeeMaster(tbl_EmployeeMaster user)
        {
            try
            {
                //if (ModelState.IsValid)
                //{



                    if (!userManager.IsUserLoginIDExist(user.EmployeeID))
                    {

                        userManager.Add(user);

                        FormsAuthentication.SetAuthCookie(user.EmployeeName, false);
                        return RedirectToAction("EmployeeMasterDetailes", "EmployeeMaster");

                    }
                    else
                    {
                        ModelState.AddModelError("", "EmployeeId already taken");
                    }
               // }
            }
            catch
            {
                return View(user);
            }

            return View(user);
        }



        //edit
        [HttpGet]
        public ActionResult EditEmployeeMaster(int id)
        {
            var data = userManager.GetEmployeeDetail(id);
            tbl_EmployeeMaster emp = new tbl_EmployeeMaster();
            emp.EmployeeID = data.EmployeeID;
            emp.EmployeeName = data.EmployeeName;
            emp.CompanyName = data.CompanyName;


            return View("EditEmployeeMaster", emp);


        }

        ////update
        [HttpPost]
        public ActionResult UpdateEmployeeMaster(tbl_EmployeeMaster register)
        {
            tbl_EmployeeMaster sysRegister = new tbl_EmployeeMaster();

            sysRegister.EmployeeID = register.EmployeeID;
            sysRegister.EmployeeName = register.EmployeeName;
            sysRegister.CompanyName = register.CompanyName;


            bool IsSuccess = userManager.UpdateEmployeeMaster(sysRegister);


            if (IsSuccess)
            {
                TempData["OperStatus"] = "Employee updated succeessfully";
                ModelState.Clear();
                return RedirectToAction("EmployeeMasterDetailes", "EmployeeMaster");
            }

            return View();

        }


        ////delete single row
        public ActionResult DeleteEmployeeMaster(int id)
        {
            bool check = userManager.DeleteEmployeeMaster(id);

            var data = userManager.GetEmployeeMaster();
            ViewBag.grid = data;
            return PartialView("EmployeeMasterDetailes", dbContext.tbl_EmployeeMaster.ToList());

        }

        //delete multiple selections
        [HttpPost]
        public ActionResult DeleteMultiple(string[] assignChkBx)
        {
            //Delete Selected 
            int[] id = null;
            if (assignChkBx != null)
            {
                id = new int[assignChkBx.Length];
                int j = 0;
                foreach (string i in assignChkBx)
                {
                    int.TryParse(i, out id[j++]);
                }
            }

            if (id != null && id.Length > 0)
            {
                List<tbl_EmployeeMaster> allSelected = new List<tbl_EmployeeMaster>();
                eBizSuiteTableEntities dc = new eBizSuiteTableEntities();

                allSelected = dc.tbl_EmployeeMaster.Where(a => id.Contains(a.EmployeeID)).ToList();
                foreach (var i in allSelected)
                {
                    dc.tbl_EmployeeMaster.Remove(i);
                }
                dc.SaveChanges();

            }

            return PartialView("EmployeeMasterDetailes");

            //return RedirectToAction("EmployeeMasterDetailes");
        }


        ////Update InLine
        [HttpPost]
        public ActionResult UpdateInline(string list)
        {

            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                var c = dc.tbl_EmployeeMaster.Where(a => a.EmployeeID == eid).FirstOrDefault();

                c.EmployeeName = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                c.CompanyName = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                dc.SaveChanges();

            }
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();

            var providerName = "System.Data.SqlClient";

            var db = Database.OpenConnectionString(connectionString, providerName);
            var query = "select * from tbl_EmployeeMaster";
            ViewBag.grid = db.Query(query);

            return PartialView("EmployeeMasterDetailes", dbContext.tbl_EmployeeMaster.ToList());
        }


        //1 search RoleMaster
        [HttpGet]
        public ActionResult SearchEmployeeMaster()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();

            var providerName = "System.Data.SqlClient";

            var db = Database.OpenConnectionString(connectionString, providerName);
            var query = "select * from tbl_EmployeeMaster";

            ViewBag.grid = db.Query(query);

            //dbContext.tbl_RoleMaster.ToList()
            return PartialView();
        }

        //2 search RoleMaster
        [HttpGet]
        [ActionName("SearchEmployePost")]
        public ActionResult SearchEmployePost(string id)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();

            var providerName = "System.Data.SqlClient";

            var db = Database.OpenConnectionString(connectionString, providerName);

            var EmployeeID = id;
            var EmployeeName = "";

            if (EmployeeID != null)
            {
                EmployeeID = id + "%";
            }
            else
                EmployeeID = id;

            var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID like '" + EmployeeID + "' or EmployeeName LIKE '" + EmployeeName + "'  ";

            var grid = dbContext.tbl_EmployeeMaster.SqlQuery(query);

            ViewBag.grid = grid;
            Session["griddata"] = grid;
            Session["data"] = ViewBag.grid;

            return PartialView("SearchEmployeeMaster");
        }


        [HttpPost]
        [ActionName("SearchEmployeeMaster")]
        public ActionResult SearchEmployeeMaster(int assignChkBx)
        {

            var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID =" + assignChkBx + "";

            var query1 = dbContext.tbl_EmployeeMaster.SqlQuery(query);

            return View("EmployeeMasterDetailes", query1);
        }


        //Advance Search
        public ActionResult AdvanceSearch()
        {
            return View("AdvanceSearch");
        }
        [HttpPost]
        [ActionName("AdvanceSearch")]
        public ActionResult Searchpost()
        {

            var EmployeeID = "";
            var EmployeeName = "";
            var CompanyName = "";

            if (Request["EmployeeID"] != string.Empty)
            {
                EmployeeID = Request["EmployeeID"] + "%";
            }
            else
                EmployeeID = Request["EmployeeID"];


            if (Request["EmployeeName"] != string.Empty)
            {
                EmployeeName = Request["EmployeeName"] + "%";
            }
            else
                EmployeeName = Request["EmployeeName"];


            if (Request["CompanyName"] != string.Empty)
            {
                CompanyName = Request["CompanyName"] + "%";
            }
            else
                CompanyName = Request["CompanyName"];


            var query = "SELECT * FROM tbl_EmployeeMaster  WHERE EmployeeID like '" + EmployeeID + "' or EmployeeName LIKE '" + EmployeeName + "' or CompanyName LIKE '" + CompanyName + "'  ";

            var query1 = dbContext.tbl_EmployeeMaster.SqlQuery(query);

            return View("EmployeeMasterDetailes", query1);
        }

    }
}
