﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using eBizSuiteAppBAL.Fombuilding;
using eBizSuiteAppDAL.classes;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MeBizSuiteAppUI.Controllers
{
    public class GSTReturnsController : Controller
    {
        //
        // GET: /GSTReturns/

        public ActionResult GSTR1()
        {
            try
            {
                DataAccess das = new DataAccess();
                das.Dateformat();
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                var currentTime = new DateTime();
                DateTime ToDate = DateTime.Now, FromDate = new DateTime(ToDate.Year, ToDate.Month, 1);
                var TD = FromDate.ToString(Session["FormatDate"].ToString());
                string FD = DateTime.ParseExact(frmDT.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                ViewBag.FromDate = TD; ViewBag.ToDate = frmDT;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GSTR2()
        {
            try
            {
                DataAccess das = new DataAccess();
                das.Dateformat();
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                var currentTime = new DateTime();
                DateTime ToDate = DateTime.Now, FromDate = new DateTime(ToDate.Year, ToDate.Month, 1);
                var TD = FromDate.ToString(Session["FormatDate"].ToString());
                string FD = DateTime.ParseExact(frmDT.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                ViewBag.FromDate = TD; ViewBag.ToDate = frmDT;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GSTR3()
        {
            try
            {
                DataAccess das = new DataAccess();
                das.Dateformat();
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                var currentTime = new DateTime();
                DateTime ToDate = DateTime.Now, FromDate = new DateTime(ToDate.Year, ToDate.Month, 1);
                var TD = FromDate.ToString(Session["FormatDate"].ToString());
                string FD = DateTime.ParseExact(frmDT.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                ViewBag.FromDate = TD; ViewBag.ToDate = frmDT;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GETGSTR1(string frDT, string toDT)
        {
            try
            {
                DataAccess das = new DataAccess();
                string htmlstring = "<thead></thead><tbody></tbody>";
                string html = das.GetHeader(Request["ref"], Request["sub"]);
                if(html!="")
                {
                    htmlstring = html + "<tbody>";
                    DataTable dt = new DataTable();
                    var fDT = DateTime.ParseExact(frDT, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                    var tDT = DateTime.ParseExact(toDT, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                    dt = das.GetGSTR1(fDT, tDT, Request["sub"], Request["GSTIN"]);
                    if (dt.Rows.Count > 0)
                    {
                        htmlstring += das.GetBody(dt, Request["ref"],Request["sub"]);
                        //for (int g = 0; g < dt.Rows.Count; g++)
                        //{
                        //    if (Request["ref"] != "3")
                        //    {
                        //        htmlstring += "<tr><td></td>";
                        //        if (Request["sub"] == "3") htmlstring += "<td></td>";
                        //        if (Request["sub"] == "2" || Request["sub"] == "4") htmlstring += "<td></td><td></td>";
                        //        htmlstring += "<td>" + dt.Rows[g][0] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                        //        if (Request["sub"] != "3" && Request["sub"] != "4")
                        //        {
                        //            htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                        //            htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                        //            htmlstring += "<td>" + dt.Rows[g][10] + "</td>";
                        //            htmlstring += "<td>" + dt.Rows[g][11] + "</td>";
                        //            htmlstring += "<td></td>";
                        //            htmlstring += "<td></td>";
                        //        } if (Request["sub"] == "4")
                        //        {
                        //            htmlstring += "<td></td>";
                        //        }
                        //        htmlstring += "<td></td>";
                        //        if (Request["ref"] == "2")
                        //        {
                        //            htmlstring += "<td></td>";
                        //            htmlstring += "<td></td>";
                        //            htmlstring += "<td></td>";
                        //            htmlstring += "<td></td>";
                        //        }
                        //        htmlstring += "<td></td></tr>";
                        //    }
                        //    else if (Request["ref"] == "3")
                        //    {
                        //        htmlstring += "<tr><td>" + dt.Rows[g][4] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                        //        htmlstring += "<td>" + dt.Rows[g][7] + "</td></tr>";
                        //    }
                        //}

                    }
                    htmlstring = htmlstring + "</tbody>";
                }
                return Json(new { Data = htmlstring }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public FileStreamResult ExportPdf(string frDT, string toDT, string Data)
        {
            try
            {
                if (Data != null) Session["pdfDATA"] = null;
                string filePath = "";
                string data = "<table style='width:100%;border-collapse:collapse;border-width:thin'; border='1'>" + Data + "</table>";
                if (Session["pdfDATA"] == null)
                    Session["pdfDATA"] = data;
                else
                    data = Session["pdfDATA"].ToString();
                string pdfData = String.Format("<html><head>{0}</head><body style='font-size:11px'><div style='text-align:center'><div>Government of India/State</div><div>Department of</div><div><b>Form GSTR-1</b></div><div><b>DETAILS OF OUTWARD SUPPLIES</b></div><div><b>1.GSTIN:</b></div><div><b>2.Name of the Taxable Person:</b></div><div><b>3.Aggregate Turnover of the Taxable Person in the previous FY:</b></div><div><b>4.Period: Month: Year:</b></div><div><b>5.Taxable outward supplies to a registered person</b></div></div>{1}</body></html>", "<style>table{border-collapse:collapse;font-size:11px}</style>", data);
                var pdfbytes = System.Text.Encoding.UTF8.GetBytes(pdfData);
                var input = new MemoryStream(pdfbytes);
                var output = new MemoryStream();
                var document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                var writer = PdfWriter.GetInstance(document, Response.OutputStream);
                //writer.PageEvent = new HeaderF();
                writer.CloseStream = false;
                document.Open();
                var xmlworker = iTextSharp.tool.xml.XMLWorkerHelper.GetInstance();
                xmlworker.ParseXHtml(writer, document, input, System.Text.Encoding.UTF8);
                document.Close();
                output.Position = 0;
                return new FileStreamResult(output, "application/pdf");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }

        public void ExportExcel(string frDT, string toDT, string Data)
        {
            try
            {
                if (Data != null) Session["exlDATA"] = null;
                string data = "<table style='width:100%;border-collapse:collapse;border-width:thin'; border='1'>" + Data + "</table>";
                if (Session["exlDATA"] == null)
                    Session["exlDATA"] = data;
                else
                    data = Session["exlDATA"].ToString();
                data = "<html><body style='font-size:11px'><div style='text-align:center;style='font-size:11px''><div>Government of India/State</div><div>Department of</div><div><b>Form GSTR-1</b></div><div><b>DETAILS OF OUTWARD SUPPLIES</b></div><div><b>1.GSTIN:</b></div><div><b>2.Name of the Taxable Person:</b></div><div><b>3.Aggregate Turnover of the Taxable Person in the previous FY:</b></div><div><b>4.Period: Month: Year:</b></div><div><b>5.Taxable outward supplies to a registered person</b></div></div>" + data + "</body></html>";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "GSTReturns.xls"));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/ms-excel";
                Response.Write(data);
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }

        }

        public ActionResult GSTR3B()
        {
            try
            {
                DataAccess das = new DataAccess();
                das.Dateformat();
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                var currentTime = new DateTime();
                DateTime ToDate = DateTime.Now, FromDate = new DateTime(ToDate.Year, ToDate.Month, 1);
                var TD = FromDate.ToString(Session["FormatDate"].ToString());
                string FD = DateTime.ParseExact(frmDT.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                ViewBag.FromDate = TD; ViewBag.ToDate = frmDT;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult GETGSTR3B(string frDT, string toDT)
        {
            try
            {
                DataAccess das = new DataAccess();
                string htmlstring = "<thead></thead><tbody></tbody>";
                string htmlstring1 = "<thead></thead><tbody></tbody>";
                string htmlstring2 = "<thead></thead><tbody></tbody>";
                string htmlstring3 = "<thead></thead><tbody></tbody>";
                string htmlstring4 = "<thead></thead><tbody></tbody>";
                string htmlstring5 = "<thead></thead><tbody></tbody>";
                string html = "";// das.GetHeader(Request["ref"], Request["sub"]);
                //if(html!="")
                //{
                var fDT = DateTime.ParseExact(frDT, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                var tDT = DateTime.ParseExact(toDT, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);

                for (int i = 1; i < 7; i++)
                {
                    DataTable dt = new DataTable();
                    dt = das.GetGSTR3B(fDT, tDT, i);
                    if (i == 1)
                    {
                        htmlstring = "<tbody>";
                        if (dt.Rows.Count > 0)
                        {
                            htmlstring += das.GetBody3B(dt, "1", "0");
                        }
                        htmlstring = htmlstring + "</tbody>";
                    }
                    else if (i == 2)
                    {
                        htmlstring1 = "<tbody>";
                        if (dt.Rows.Count > 0)
                        {
                            htmlstring1 += das.GetBody3B(dt, "2", "0");
                        }
                        htmlstring1 = htmlstring1 + "</tbody>";
                    }
                    else if (i == 3)
                    {
                        htmlstring2 = "<tbody>";
                        if (dt.Rows.Count > 0)
                        {
                            htmlstring2 += das.GetBody3B(dt, "3", "0");
                        }
                        htmlstring2 = htmlstring2 + "</tbody>";
                    }
                    else if (i == 4)
                    {
                        htmlstring3 = "<tbody>";
                        if (dt.Rows.Count > 0)
                        {
                            htmlstring3 += das.GetBody3B(dt, "4", "0");
                        }
                        htmlstring3 = htmlstring3 + "</tbody>";
                    }
                    else if (i == 5)
                    {
                        htmlstring4 = "<tbody>";
                        if (dt.Rows.Count > 0)
                        {
                            htmlstring4 += das.GetBody3B(dt, "5", "0");
                        }
                        htmlstring4 = htmlstring4 + "</tbody>";
                    }
                    else if (i == 6)
                    {
                        htmlstring5 = "<tbody>";
                        if (dt.Rows.Count > 0)
                        {
                            htmlstring5 += das.GetBody3B(dt, "6", "0");
                        }
                        htmlstring5 = htmlstring5 + "</tbody>";
                    }
                }
                DataTable dtH = new DataTable();
                SqlConnection cn = new SqlConnection(eBizSuiteAppDAL.classes.Connection.Con);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTR3B", cn);
                cmd.Parameters.AddWithValue("@FROMDT", fDT);
                cmd.Parameters.AddWithValue("@TODT", tDT);
                cmd.Parameters.AddWithValue("@OPERATING", System.Web.HttpContext.Current.Session["OPunit"]);
                cmd.Parameters.AddWithValue("@OPUNIT", System.Web.HttpContext.Current.Session["OPunit"]);
                cmd.Parameters.AddWithValue("@AcOwner", System.Web.HttpContext.Current.Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@FORMATDT", System.Web.HttpContext.Current.Session["FormatDate"]);
                cmd.Parameters.AddWithValue("@SEC", "HEADER");
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                dad.Fill(dtH);
                    Session["YY"] =  Session["MM"] =  Session["CMP"] =  Session["GSTIN"] = null;
                if (dtH.Rows.Count > 0)
                {
                    Session["YY"] = dtH.Rows[0]["YY"];
                    Session["MM"] = dtH.Rows[0]["MM"];
                    Session["CMP"] = dtH.Rows[0]["CMP"];
                    Session["GSTIN"] = dtH.Rows[0]["GSTIN"];
                }
                return Json(new { Data = htmlstring, Data1 = htmlstring1, Data2 = htmlstring2, Data3 = htmlstring3, Data4 = htmlstring4, Data5 = htmlstring5 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public void ExportExcel3B(string frDT, string toDT, string Data)
        {
            try
            {
                if (Data != null) Session["exlDATA"] = null;
                string data = "<table style='width:100%;border-collapse:collapse;border-width:thin'; border='1'>" + Data + "</table>";
                if (Session["exlDATA"] == null)
                    Session["exlDATA"] = data;
                else
                    data = Session["exlDATA"].ToString();
                data = "<html><body style='font-size:11px >" + data + "</body></html>";
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "GSTReturns.xls"));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/ms-excel";
                Response.Write(data);
                Response.End();
                //var workbook = new XLWorkbook();
                //var worksheet1 = workbook.Worksheets.Add(PassYourDatatable1);
                //var worksheet2 = workbook.Worksheets.Add(PassYourDatatable2);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }

        }

        public FileStreamResult ExportPdf3B(string frDT, string toDT, string Data)
        {
            try
            {
                if (Data != null) Session["pdfDATA"] = null;
                string filePath = "";
                string data = "<table style='width:100%;border-collapse:collapse;border-width:thin'; border='1'>" + Data + "</table>";
                if (Session["pdfDATA"] == null)
                    Session["pdfDATA"] = data;
                else
                    data = Session["pdfDATA"].ToString();
                string pdfData = String.Format("<html><head>{0}</head><body style='font-size:11px'>{1}</body></html>", "<style>table{border-collapse:collapse;font-size:11px}</style>", data);
                var pdfbytes = System.Text.Encoding.UTF8.GetBytes(pdfData);
                var input = new MemoryStream(pdfbytes);
                var output = new MemoryStream();
                var document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                var writer = PdfWriter.GetInstance(document, Response.OutputStream);
                //writer.PageEvent = new HeaderF();
                writer.CloseStream = false;
                document.Open();
                var xmlworker = iTextSharp.tool.xml.XMLWorkerHelper.GetInstance();
                xmlworker.ParseXHtml(writer, document, input, System.Text.Encoding.UTF8);
                document.Close();
                output.Position = 0;
                return new FileStreamResult(output, "application/pdf");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }

        public void getDataSetExportToExcel(string frDT, string toDT)
        {
            try
            {
            var fDT = DateTime.ParseExact(Convert.ToString(Request["fd"]), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
            var tDT = DateTime.ParseExact(Convert.ToString(Request["td"]), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);

            DataAccess das = new DataAccess();
            using (XLWorkbook wb = new XLWorkbook())
            {
                var sheetName = new string[] { "b2b", "b2cl", "b2cs", "cdnr", "cdnur", "exp", "at", "atadj", "exemp", "hsn", "docs", "master" };
                var clName = new string[] { "Summary For B2B(4)", "Summary For B2CL(5)", "Summary For B2CS(7)", "Summary For CDNR(9B)", "Summary For CDNUR(9B)", "Summary For EXP(6)", "Summary For Advance Received (11B) ", "Summary For Advance Adjusted (11B) ", "Summary For Nil rated, exempted and non GST outward supplies (8)", "Summary For HSN(12)", "Summary of documents issued during the tax period (13)", "master" };
                for (int i = 0; i < 12; i++)
                {
                    DataTable dt = new DataTable();
                    dt = das.GetGSTR1(fDT, tDT, Convert.ToString(i + 100), null);
                    var ws = wb.Worksheets.Add(dt, sheetName[i]);
                    ws.Style.Font.FontName = "Times New Roman";
                    ws.Tables.FirstOrDefault().ShowAutoFilter = false;
                    if (i != 11)
                    {
                        var row1 = wb.Worksheet(i + 1).Row(3);
                        row1.Cells(1, (row1.LastCellUsed().Address).ColumnNumber).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 204, 153);
                        var row2 = wb.Worksheet(i + 1).Row(1);
                        foreach (IXLCell cl in row2.Cells())
                        {
                        if (Convert.ToString(cl.Value) == "_" || Convert.ToString(cl.Value) == "__" || Convert.ToString(cl.Value) == "___" || Convert.ToString(cl.Value) == "____" || Convert.ToString(cl.Value) == "_____" || Convert.ToString(cl.Value) == "______" || Convert.ToString(cl.Value) == "_______" || Convert.ToString(cl.Value) == "________" || Convert.ToString(cl.Value) == "_________" || Convert.ToString(cl.Value) == "__________" || Convert.ToString(cl.Value) == "___________" || Convert.ToString(cl.Value) == "____________" || Convert.ToString(cl.Value) == "_____________") {
                            cl.Style.Font.FontColor = XLColor.FromArgb(65, 125, 193);
                        }
                        }
                        row2.InsertRowsAbove(1);
                        var row = wb.Worksheet(i + 1).Row(1);
                        row.Cells(1, 3).Style.Fill.BackgroundColor = XLColor.FromArgb(65, 125, 193);
                        row.Cell(1).Value = clName[i];
                        row.Style.Font.Bold = true;
                        row.Style.Font.FontColor = XLColor.White;
                        wb.Worksheet(i + 1).Row(3).Cells(1, (row1.LastCellUsed().Address).ColumnNumber).SetDataType(XLCellValues.Number);
                        int lC = wb.Worksheet(i + 1).Column(4).CellsUsed().Count()+10;
                        if (i + 100 == 100)
                        {
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(4).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(9).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(9).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(10).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(10).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(11).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(11).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 101)
                        {
                            wb.Worksheet(i + 1).Column(3).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(3).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(6).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(6).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(7).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(7).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 102)
                        {
                            wb.Worksheet(i + 1).Column(3).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(3).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 103 || i + 100 == 104)
                        {
                            wb.Worksheet(i + 1).Column(9).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(9).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(10).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(10).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(11).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(11).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(12).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(12).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 105)
                        {
                            wb.Worksheet(i + 1).Column(9).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(9).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(8).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(8).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 106 || i + 100 == 107 || i + 100 == 108)
                        {
                            wb.Worksheet(i + 1).Column(2).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(2).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(3).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(3).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 109)
                        {
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(4).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(6).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(6).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(7).Cells(5, lC).Style.NumberFormat.Format="0.00";
                            wb.Worksheet(i + 1).Column(7).Cells(5,lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(8).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(8).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(9).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(9).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(10).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(10).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                        else if (i + 100 == 110)
                        {
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(5).Cells(5, lC).SetDataType(XLCellValues.Number);
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).Style.NumberFormat.Format = "0.00";
                            wb.Worksheet(i + 1).Column(4).Cells(5, lC).SetDataType(XLCellValues.Number);
                        }
                    }
                }
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= GSTR1.xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
            }
        }

    }
}

