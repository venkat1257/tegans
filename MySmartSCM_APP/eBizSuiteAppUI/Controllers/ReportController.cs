﻿using eBizSuiteAppModel.Table;
using eBizSuiteUI.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class ReportController : Controller
    {
        private eBizSuiteTableEntities db = new eBizSuiteTableEntities();

        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;

        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();

        string providerName = "System.Data.SqlClient";

        public ActionResult Index(int? tid)
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            QueryGeneratorModel obj = new QueryGeneratorModel();
            obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN04 == 101 && q.COLUMN06 == null).ToList();
            ViewBag.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN06 == null).ToList().Distinct();
            if (!tid.HasValue)
            {
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == 0).ToList();
            }
            else
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == tid).ToList();
            List<CONTABLE004> objTreeList = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
            ViewBag.Column = objTreeList;
            ViewBag.theadertext = Session["1"];
            ViewBag.itemsdata = Session["2"];
            //Session["1"] = Session["2"] = null;
            return View(obj);
        }

        public JsonResult TreeView()
        {
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public class DynaNode
        {
            public string title { get; set; }
            public bool isFolder { get; set; }
            public bool isLazy { get; set; }
            public string key { get; set; }
            public string parentId { get; set; }
        }

        public int id;

        [HttpPost]
        public ActionResult Filter(string table_Alias)
        {
            QueryGeneratorModel obj = new QueryGeneratorModel();
            var tId = db.CONTABLE004.Where(c => c.COLUMN05 == table_Alias);
            var Tid = tId.FirstOrDefault().COLUMN02;
            obj.tbl = db.CONTABLE005.Where(a => a.COLUMN02 == Tid).OrderBy(q => q.COLUMN04).ToList();

            return PartialView("GetAllNode", obj);
        }

        private List<DynaNode> GetTreeView(int? mid)
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            List<CONTABLE004> objTreeList = db.CONTABLE004.Where(s => s.COLUMN03 == mid).ToList();
            List<DynaNode> objList = new List<DynaNode>();
            foreach (var item in objTreeList)
            {
                DynaNode objTree = new DynaNode();
                objTree.key = item.COLUMN02.ToString();
                objTree.title = item.COLUMN04 + " ( " + item.COLUMN05 + " ) ";
                objTree.isLazy = true;
                objTree.isFolder = true;
                objList.Add(objTree);
            }

            return objList;
        }

        public JsonResult GetTables(int? id)
        {
            if (id != null)
            {
                ViewBag.selected = Session["FID"] = id;
                List<DynaNode> objList = GetTreeView(id);
                return Json(objList, JsonRequestBehavior.AllowGet);
            }
            else
                if (Session["FID"] != null)
                {
                    int i = Convert.ToInt32(Session["FID"]);
                    List<DynaNode> objList = GetTreeView(i);
                    return Json(objList, JsonRequestBehavior.AllowGet);
                    ;
                }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // [HttpPost]
        public ActionResult CreateTree(int? lst)
        {
            if (lst != null)
            {
                Session["TID"] = lst;
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                QueryGeneratorModel obj = new QueryGeneratorModel();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN04 == 101 && q.COLUMN06 == null).ToList();
                obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
                ViewBag.lst = obj.lst;
                var col = db.CONTABLE004;
                obj.tbl = db.CONTABLE005.Where(a => a.COLUMN03 == lst && a.COLUMN04.Length < 9).OrderBy(q => q.COLUMN04).ToList();
                ViewBag.Table = obj.tbl;
                return PartialView("Index", obj);
            }
            else
            {
                string st = "0";
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                QueryGeneratorModel obj = new QueryGeneratorModel();
                obj.lstM = db.CONTABLE003.OrderBy(a => a.COLUMN03).Where(q => q.COLUMN07 == 0 && q.COLUMN04 == 101 && q.COLUMN06 == null).ToList();
                obj.lst = db.CONTABLE0010.Where(q => q.COLUMN03 == 0).ToList();
                obj.tree = db.CONTABLE004.OrderBy(s => s.COLUMN05).ToList();
                ViewBag.lst = obj.lst;
                var list = st.Split(',').Select(n => int.Parse(n)).ToList();
                obj.tbl = db.CONTABLE005.Where(a => list.Contains(a.COLUMN03)).ToList();
                ViewBag.Table = obj.tbl;
                return PartialView("Index", obj);
            }
        }

        [HttpPost]
        public ActionResult CreateTable(string str)
        {
            try
            {
                int? res = null;
                using (SqlConnection con = new SqlConnection(sqlcon))
                {
                    SqlCommand cmd = new SqlCommand(str, con);
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }

                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReportBuild(string query)
        {
            try
            {
                var query1 = query;
                Session["ReportQuery"] = query;
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                string[] theadertext;
                theadertext = new string[books.ToList().ElementAt(0).Columns.Count];
                for (int a = 0; a < books.ToList().ElementAt(0).Columns.Count; a++)
                {
                    theadertext[a] = books.ToList().ElementAt(0).Columns[a];

                }
                string tmp = query.Substring(query.LastIndexOf(' ') - 10, 10);
                Session["TMP"] = tmp;
                Session["1"] = theadertext;
                Session["2"] = books;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return Content("Form Editing Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult RefreshReport()
        {
            try
            {
                string query = Session["ReportQuery"].ToString();
                var query1 = query;
                var db2 = WebMatrix.Data.Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                string[] theadertext;
                theadertext = new string[books.ToList().ElementAt(0).Columns.Count];
                for (int a = 0; a < books.ToList().ElementAt(0).Columns.Count; a++)
                {
                    theadertext[a] = books.ToList().ElementAt(0).Columns[a];

                }
                string tmp = query.Substring(query.LastIndexOf(' ') - 10, 10);
                Session["TMP"] = tmp;
                Session["1"] = theadertext;
                Session["2"] = books;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return Content("Form Editing Failed <a onclick='history.go(-1); return false;' href=''> Back</a>");
            }
        }

        public ActionResult SetSess()
        {
            Session["1"] = null;
            Session["2"] = null;
            return RedirectToAction("Index");
        }

        public ActionResult TruncateTable()
        {
            try
            {
                if (Session["TMP"] != null)
                {
                    var query1 = "truncate table " + Session["TMP"].ToString() + "";
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    Session["MessageFrom"] = "" + Session["TMP"].ToString() + " Table Data Cleared.....";
                    Session["SuccessMessageFrom"] = "Success";
                }
                //
                //
                //
                //
                //
                //ViewBag.itemsdata = books;
                //string[] theadertext;
                //theadertext = new string[books.ToList().ElementAt(0).Columns.Count];
                //for (int a = 0; a < books.ToList().ElementAt(0).Columns.Count; a++)
                //{
                //    theadertext[a] = books.ToList().ElementAt(0).Columns[a];

                //}
            }
            catch (Exception ex)
            {
                Session["MessageFrom"] = "" + Session["TMP"].ToString() + " Object not found.....";
                Session["SuccessMessageFrom"] = "Fail";
            }
            Session["1"] = null;
            Session["2"] = null;
            return RedirectToAction("Index");
        }

        public FileContentResult DisplayTransactionReport()
        {
            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath("~/Content/TransactionReport.rdlc");
            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            var dbs = Database.Open("sqlcon");
            var sql = "select  COLUMN03,isnull(sum(COLUMN08),0) as COLUMN08,isnull(sum(COLUMN09),0) as COLUMN09," +
                "isnull(sum(COLUMN10),0) as COLUMN10,isnull(sum(COLUMN11),0) as COLUMN11,isnull(sum(COLUMN12),0) as COLUMN12," +
                "isnull(sum(COLUMN21),0) as COLUMN21,isnull(sum(COLUMN22),0) as COLUMN22,isnull(sum(COLUMN23),0) " +
            "as COLUMN23 from putable013  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "' group by column03 ";
            var customerfilterList = dbs.Query(sql);

            reportDataSource.Value = customerfilterList;
            localReport.DataSources.Add(reportDataSource);
            string reportType = "Image";
            string mimeType;
            string encoding;
            string fileNameExtension;
            //The DeviceInfo settings should be changed based on the reportType            
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>jpeg</OutputFormat>" +
                "  <PageWidth>11.5in</PageWidth>" +
                "  <PageHeight>5in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>1in</MarginLeft>" +
                "  <MarginRight>1in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            return File(renderedBytes, "image/jpeg");
        }

        public ActionResult TransactionReport()
        {
            return View();
        }

        public ActionResult DayReport()
        {
            try
            {
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult OpeningBalance()
        {
            return View();
        }

        public ActionResult InventoryAsset()
        {
            try
            {
                SqlConnection cn = new SqlConnection(sqlcon);
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                var idx = Country1.FindIndex(x => x.Value == "");
                if (idx != -1)
                {
                    var item = Country1[idx];
                    Country1.RemoveAt(idx);
                    Country1.Insert(0, item);
                }
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                var idx1 = Countryp.FindIndex(x => x.Value == "");
                if (idx1 != -1)
                {
                    var item1 = Countryp[idx];
                    Countryp.RemoveAt(idx1);
                    Countryp.Insert(0, item1);
                }
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult EmployeeReport()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN06 as COLUMN04 from MATABLE010 where COLUMN30='True'  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["SalesRef"] = new SelectList(Country, "Value", "Text");
            SetDateFormat();
            return View();
        }

        [HttpGet]
        //EMPHCS1338	Rename Stock Details as Stock Ledger BY RAJ.jr
        public ActionResult StockLedger()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Customer.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");




                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ServiceStockLedger()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN05"].ToString() });
                }
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult StockLedgerKK()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult InventoryReport()
        {
            try
            {
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                //List<SelectListItem> Family = new List<SelectListItem>();
                //SqlCommand cmd1 = new SqlCommand("select COLUMN04,COLUMN02  from MATABLE003  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                //SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                //DataTable dt1 = new DataTable();
                //da1.Fill(dt1);
                //for (int dd = 0; dd < dt1.Rows.Count; dd++)
                //{
                //    Family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["Family"] = new SelectList(Family, "Value", "Text");
                //List<SelectListItem> Group = new List<SelectListItem>();
                //SqlCommand cmdg = new SqlCommand("select COLUMN04,COLUMN02  from MATABLE004  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                //SqlDataAdapter dag = new SqlDataAdapter(cmdg);
                //DataTable dtg = new DataTable();
                //dag.Fill(dtg);
                //for (int dd = 0; dd < dtg.Rows.Count; dd++)
                //{
                //    Group.Add(new SelectListItem { Value = dtg.Rows[dd]["COLUMN02"].ToString(), Text = dtg.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["Group"] = new SelectList(Group, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }





        [HttpGet]
        //EMPHCS1338	Rename Stock Details as Stock Ledger BY RAJ.jr
        public ActionResult StockRegister()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult StockRegisterKK()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

//EMPHCS1810 : Issues Resolving VamanTex Reports,Prints by GNANESHWAR ON 3/9/2016
        [HttpGet]
        public ActionResult StockDetailes()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult AccountspayableLocation()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult AccountspayableDuedates()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ProjectReceivableAccountReport()
        {
            try
            {
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ProjectReceivableAccountDueDates()
        {
            try
            {
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }


        [HttpGet]
        public ActionResult ProjectBankReport()
        {
            try
            {
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ProjectExpenses()
        {
            try
            {
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult PayableAccountReport()
        {
            try
            {
                List<SelectListItem> CountryT = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmdT = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11131 and isnull(COLUMNA13,0)=0 ", cn);
                SqlDataAdapter daT = new SqlDataAdapter(cmdT);
                DataTable dtT = new DataTable();
                daT.Fill(dtT);
                for (int dd = 0; dd < dtT.Rows.Count; dd++)
                {
                    CountryT.Add(new SelectListItem { Value = dtT.Rows[dd]["COLUMN02"].ToString(), Text = dtT.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                CountryT = CountryT.OrderBy(x => x.Text).ToList();
                ViewData["PartyType"] = new SelectList(CountryT, "Value", "Text");
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");

                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult PayableAccountReport(FormCollection frm)
        {
            try
            {
                if (frm["frmDT"] == null || frm["toDT"] == null)
                {
                    ViewBag.frmDT = "";
                    ViewBag.toDT = "";
                    ViewBag.img = null;
                    return View();
                }
                else
                {
                    LocalReport localReport = new LocalReport();
                    localReport.ReportPath = Server.MapPath("~/Content/AccountPayable.rdl");
                    ReportDataSource reportDataSource = new ReportDataSource();
                    reportDataSource.Name = "DataSet1";
                    var dbs = Database.Open("sqlcon");
                    var sql = "select  COLUMN06,COLUMN07,COLUMN10," +
                    "COLUMN12,COLUMN13," +
                    "COLUMN14 from putable016 where COLUMN04 between '" + frm["frmDT"] + "' and '" + frm["toDT"] + "'  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                    var customerfilterList = dbs.Query(sql);

                    reportDataSource.Value = customerfilterList;
                    localReport.DataSources.Add(reportDataSource);
                    string reportType = "Image";
                    string mimeType;
                    string encoding;
                    string fileNameExtension;
                    //The DeviceInfo settings should be changed based on the reportType            
                    //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
                    string deviceInfo = "<DeviceInfo>" +
                        "  <OutputFormat>jpeg</OutputFormat>" +
                        "  <PageWidth>11in</PageWidth>" +
                        "  <PageHeight>29,7in</PageHeight>" +
                        "  <MarginTop>0.5in</MarginTop>" +
                        "  <MarginLeft>1in</MarginLeft>" +
                        "  <MarginRight>1in</MarginRight>" +
                        "  <MarginBottom>0.5in</MarginBottom>" +
                        "</DeviceInfo>";
                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;
                    renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
                    ViewBag.frmDT = frm["frmDT"];
                    ViewBag.toDT = frm["toDT"];
                    ViewBag.img = renderedBytes;
                    //return File(renderedBytes, "image/jpeg");
                    SetDateFormat();
                    return View();
                }
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ReceivableAccountReport()
        {
            try
            {

                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");

                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult ReceivableAccountReport(FormCollection frm)
        {
            try
            {
                if (frm["frmDT"] == null || frm["toDT"] == null)
                {
                    ViewBag.frmDT = "";
                    ViewBag.toDT = "";
                    ViewBag.img = null;
                    return View();
                }
                else
                {
                    LocalReport localReport = new LocalReport();
                    localReport.ReportPath = Server.MapPath("~/Content/ReceivableAccountReport.rdlc");
                    ReportDataSource reportDataSource = new ReportDataSource();
                    reportDataSource.Name = "ReceivableDataSet";
                    var dbs = Database.Open("sqlcon");
                    var sql = "select  COLUMN06, COLUMN04 ,COLUMN07,COLUMN09," +
                        "COLUMN11,COLUMN12 " +
                        " from putable018 where COLUMN04 between '" + frm["frmDT"] + "' and '" + frm["toDT"] + "'  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                    var customerfilterList = dbs.Query(sql);

                    reportDataSource.Value = customerfilterList;
                    localReport.DataSources.Add(reportDataSource);
                    string reportType = "Image";
                    string mimeType;
                    string encoding;
                    string fileNameExtension;
                    //The DeviceInfo settings should be changed based on the reportType            
                    //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
                    string deviceInfo = "<DeviceInfo>" +
                        "  <OutputFormat>jpeg</OutputFormat>" +
                        "  <PageWidth>11in</PageWidth>" +
                        "  <PageHeight>29,7in</PageHeight>" +
                        "  <MarginTop>0.5in</MarginTop>" +
                        "  <MarginLeft>1in</MarginLeft>" +
                        "  <MarginRight>1in</MarginRight>" +
                        "  <MarginBottom>0.5in</MarginBottom>" +
                        "</DeviceInfo>";
                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;
                    renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
                    ViewBag.frmDT = frm["frmDT"];
                    ViewBag.toDT = frm["toDT"];
                    ViewBag.img = renderedBytes;
                    //return File(renderedBytes, "image/jpeg");
                    SetDateFormat();
                    return View();
                }
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpPost]
        public ActionResult PaymentReport(FormCollection frm)
        {
            if (frm["frmDT"] == null || frm["toDT"] == null)
            {
                ViewBag.frmDT = "";
                ViewBag.toDT = "";
                ViewBag.img = null;
                return View();
            }
            else
            {
                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath("~/Content/PaymentReport.rdlc");
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "PaymentDataSet";
                var dbs = Database.Open("sqlcon");
                var sql = "select  COLUMN06,COLUMN07,COLUMN10," +
               "COLUMN11,COLUMN12 " +
               " from putable019 where COLUMN04 between '" + frm["frmDT"] + "' and '" + frm["toDT"] + "'  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                var customerfilterList = dbs.Query(sql);

                reportDataSource.Value = customerfilterList;
                localReport.DataSources.Add(reportDataSource);
                string reportType = "Image";
                string mimeType;
                string encoding;
                string fileNameExtension;
                //The DeviceInfo settings should be changed based on the reportType            
                //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
                string deviceInfo = "<DeviceInfo>" +
                    "  <OutputFormat>jpeg</OutputFormat>" +
                    "  <PageWidth>11in</PageWidth>" +
                    "  <PageHeight>29,7in</PageHeight>" +
                    "  <MarginTop>0.5in</MarginTop>" +
                    "  <MarginLeft>1in</MarginLeft>" +
                    "  <MarginRight>1in</MarginRight>" +
                    "  <MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";
                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;
                renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
                ViewBag.frmDT = frm["frmDT"];
                ViewBag.toDT = frm["toDT"];
                ViewBag.img = renderedBytes;
                //return File(renderedBytes, "image/jpeg");
                SetDateFormat();
                return View();
            }
        }

        public ActionResult PurchaseOrderSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SalesOrderSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult JobberIN()
        {
            try
            {
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlConnection cni = new SqlConnection(sqlcon);
                SqlCommand cmdi = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cni);
                SqlDataAdapter dai = new SqlDataAdapter(cmdi);
                DataTable dti = new DataTable();
                dai.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Item, "Value", "Text");

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }


        public ActionResult VendorPayment()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult CustomerPayment()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Customer"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Country2 = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("SELECT COLUMN02,COLUMN06 COLUMN04 from  MATABLE010 where COLUMN30='True' and " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country2 = Country2.OrderBy(x => x.Text).ToList();
            ViewData["SalesRep"] = new SelectList(Country2, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult POByItem()
        {
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();
        }

        public ActionResult SOByItem()
        {
            try
            {
                SqlConnection cn = new SqlConnection(sqlcon);
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult SOByCustomer()
        {
            try
            {
                SqlConnection cn = new SqlConnection(sqlcon);
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult POHistory()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Country2 = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select distinct COLUMN16 COLUMN04 from PUTABLE001 where COLUMN29=1001  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN04"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country2 = Country2.OrderBy(x => x.Text).ToList();
            ViewData["Status"] = new SelectList(Country2, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult SOHistory()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Customer"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Country2 = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select distinct COLUMN16 COLUMN04 from SATABLE005 where COLUMN29=1002  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN04"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country2 = Country2.OrderBy(x => x.Text).ToList();
            ViewData["Status"] = new SelectList(Country2, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult ItemOrderDetails()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Item"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult ItemFulfilment()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Item"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }
        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
        public ActionResult VendorPaymentDues()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country2, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CustomerPaymentDues()
        {
            try
            {

                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> CountryT = new List<SelectListItem>();
                SqlConnection cnT = new SqlConnection(sqlcon);
                SqlCommand cmdT = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cnT);
                SqlDataAdapter daT = new SqlDataAdapter(cmdT);
                DataTable dtT = new DataTable();
                daT.Fill(dtT);
                for (int dd = 0; dd < dtT.Rows.Count; dd++)
                {
                    CountryT.Add(new SelectListItem { Value = dtT.Rows[dd]["COLUMN02"].ToString(), Text = dtT.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                CountryT = CountryT.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(CountryT, "Value", "Text");

                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
        public ActionResult JobOrderProcessing()
        {
            try
            {
                SqlConnection cn = new SqlConnection(sqlcon);
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> JobOrder = new List<SelectListItem>();
                SqlDataAdapter cmdj = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtj = new DataTable();
                cmdj.Fill(dtj);
                for (int dd = 0; dd < dtj.Rows.Count; dd++)
                {
                    JobOrder.Add(new SelectListItem { Value = dtj.Rows[dd]["COLUMN02"].ToString(), Text = dtj.Rows[dd]["COLUMN04"].ToString() });
                }
                //JobOrder = JobOrder.OrderBy(x => x.Text).ToList();
                ViewData["JobOrder"] = new SelectList(JobOrder, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult Top10Customers()
        {
            try
            {
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CustomerRegister()
        {
            SetDateFormat();
            return View();

        }

        [HttpGet]
        public ActionResult PendingInvoices()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Customer"] = new SelectList(Country, "Value", "Text");

            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from SATABLE009 WHERE COLUMN13 != 'CLOSE'  AND COLUMN13 != 'AMOUNT FULLY RECEIVED' and column04 like'INV%'  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["BillNo"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }

        [HttpGet]
        public ActionResult PendingSalesOrders()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Customer"] = new SelectList(Country, "Value", "Text");

            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from SATABLE005 WHERE(COLUMN16 != 'CLOSE') and column04 like 'S%'  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN04"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["SONO"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult AccountsLedger()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02, COLUMN04  from MATABLE002  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Type"] = new SelectList(Country, "Value", "Text");
            SetDateFormat();
            return View();

        }

        [HttpGet]
        //EMPHCS1339	Reame Stock Ledger as Current Inventory BY RAj.Jr
        public ActionResult CurrentInventory()
        {
            try
            {
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                //List<SelectListItem> Family = new List<SelectListItem>();
                //SqlCommand cmd1 = new SqlCommand("select COLUMN04,COLUMN02  from MATABLE003  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                //SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                //DataTable dt1 = new DataTable();
                //da1.Fill(dt1);
                //for (int dd = 0; dd < dt1.Rows.Count; dd++)
                //{
                //    Family.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["Family"] = new SelectList(Family, "Value", "Text");
                //List<SelectListItem> Group = new List<SelectListItem>();
                //SqlCommand cmdg = new SqlCommand("select COLUMN04,COLUMN02  from MATABLE004  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                //SqlDataAdapter dag = new SqlDataAdapter(cmdg);
                //DataTable dtg = new DataTable();
                //dag.Fill(dtg);
                //for (int dd = 0; dd < dtg.Rows.Count; dd++)
                //{
                //    Group.Add(new SelectListItem { Value = dtg.Rows[dd]["COLUMN02"].ToString(), Text = dtg.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["Group"] = new SelectList(Group, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or  isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult Payment()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult Invoice()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult ItemIssue()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult SalesOrder()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult PayBill()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult Bill()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult ItemReceipt()
        {
            SetDateFormat();
            return View();

        }

        public ActionResult PurchaseOrder()
        {
            SetDateFormat();
            return View();

        }

        [HttpGet]
        public ActionResult PendingPurchaseOrders()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).Take(1).ToList();
            ViewData["Vendor"] = new SelectList(Country, "Value", "Text");

            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from PUTABLE001 WHERE(COLUMN16 != 'CLOSE')  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN04"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["PONO"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult ExpenseTracking()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Employee = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 where COLUMN09!=''   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' AND isnull(COLUMN32,'False')='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Employee.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
                }

                Employee = Employee.OrderBy(x => x.Text).ToList();
                ViewData["Employee"] = new SelectList(Employee, "Value", "Text");


                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmde = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001 where  column07=22344 and  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                SqlDataAdapter dae = new SqlDataAdapter(cmde);
                DataTable dte = new DataTable();
                dae.Fill(dte);
                for (int dd = 0; dd < dte.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dte.Rows[dd]["COLUMN02"].ToString(), Text = dte.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Account"] = new SelectList(Country2, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult CustomerVsSalesRep()
        {
            List<SelectListItem> cust = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            cust = cust.OrderBy(x => x.Text).ToList();
            ViewData["cust"] = new SelectList(cust, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> sales = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            sales = sales.OrderBy(x => x.Text).ToList();
            ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult CommissionPayment()
        {
            List<SelectListItem> cust = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            cust = cust.OrderBy(x => x.Text).ToList();
            ViewData["cust"] = new SelectList(cust, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> sales = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            sales = sales.OrderBy(x => x.Text).ToList();
            ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult CommissionDues()
        {
            List<SelectListItem> cust = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            cust = cust.OrderBy(x => x.Text).ToList();
            ViewData["cust"] = new SelectList(cust, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> sales = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1  AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            sales = sales.OrderBy(x => x.Text).ToList();
            ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult CommissionPaymentDues()
        {
            try
            {
                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");


                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> sales = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1 AND" + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                sales = sales.OrderBy(x => x.Text).ToList();
                ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult PendingBills()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Vendor"] = new SelectList(Country, "Value", "Text");

            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from PUTABLE005 WHERE(COLUMN13!= 'FULLY BILLED/FULLY RECEIVED') AND (COLUMN13 != 'FULLY PAID')   AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["BillNo"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();

        }

        [HttpGet]
        public ActionResult JobberPayment()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }


        [HttpGet]
        public ActionResult JobberPaymentDues()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult StockOfJobber()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult InventoryByLoc()
        {
            List<SelectListItem> Item = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Item.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Item = Item.OrderBy(x => x.Text).ToList();
            ViewData["Item"] = new SelectList(Item, "Value", "Text");

            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();
        }

        public ActionResult VendorRegister()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Vendor"] = new SelectList(Country, "Value", "Text");
            SetDateFormat();
            return View();

        }

        public ActionResult Top10Vendors()
        {
            try
            {
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult JobberReport()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where COLUMN22=22286  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Country, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult JobberReport(FormCollection frm)
        {
            try
            {
                if (frm["frmDT"] == null || frm["toDT"] == null)
                {
                    ViewBag.frmDT = "";
                    ViewBag.toDT = "";
                    ViewBag.img = null;
                    return View();
                }
                else
                {
                    LocalReport localReport = new LocalReport();
                    localReport.ReportPath = Server.MapPath("~/Content/JobberReport.rdlc");
                    ReportDataSource reportDataSource = new ReportDataSource();
                    reportDataSource.Name = "DataSet1";
                    var dbs = Database.Open("sqlcon");
                    var sql = "select  COLUMN06,COLUMN07,COLUMN10," +
                    "COLUMN12,COLUMN13," +
                    "COLUMN14 from putable016 where COLUMN04 between '" + frm["frmDT"] + "' and '" + frm["toDT"] + "'   AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                    var customerfilterList = dbs.Query(sql);

                    reportDataSource.Value = customerfilterList;
                    localReport.DataSources.Add(reportDataSource);
                    string reportType = "Image";
                    string mimeType;
                    string encoding;
                    string fileNameExtension;
                    //The DeviceInfo settings should be changed based on the reportType            
                    //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
                    string deviceInfo = "<DeviceInfo>" +
                        "  <OutputFormat>jpeg</OutputFormat>" +
                        "  <PageWidth>11in</PageWidth>" +
                        "  <PageHeight>29,7in</PageHeight>" +
                        "  <MarginTop>0.5in</MarginTop>" +
                        "  <MarginLeft>1in</MarginLeft>" +
                        "  <MarginRight>1in</MarginRight>" +
                        "  <MarginBottom>0.5in</MarginBottom>" +
                        "</DeviceInfo>";
                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;
                    renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
                    ViewBag.frmDT = frm["frmDT"];
                    ViewBag.toDT = frm["toDT"];
                    ViewBag.img = renderedBytes;
                    //return File(renderedBytes, "image/jpeg");
                    SetDateFormat();
                    return View();
                }
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult BankRegister()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04  from FITABLE001    WHERE (COLUMN07=22266 OR COLUMN07=22329) AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN12='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Bank"] = new SelectList(Country, "Value", "Text");
                //EMPHCS1214	Bank register balance must be cumulative and also operating unit must be added . RECEIPT must be added BY RAJ.Jr 30/9/2015
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");

                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult BankRegister(FormCollection frm)
        {
            if (frm["frmDT"] == null || frm["toDT"] == null)
            {
                ViewBag.frmDT = "";
                ViewBag.toDT = "";
                ViewBag.img = null;
                return View();
            }
            else
            {
                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath("~/Content/BankReport.rdlc");
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";
                var dbs = Database.Open("sqlcon");
                var sql = "select  COLUMN06,COLUMN07,COLUMN10," +
                "COLUMN12,COLUMN13," +
                "COLUMN14 from putable016 where COLUMN04 between '" + frm["frmDT"] + "' and '" + frm["toDT"] + "'   AND " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                var customerfilterList = dbs.Query(sql);

                reportDataSource.Value = customerfilterList;
                localReport.DataSources.Add(reportDataSource);
                string reportType = "Image";
                string mimeType;
                string encoding;
                string fileNameExtension;
                //The DeviceInfo settings should be changed based on the reportType            
                //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
                string deviceInfo = "<DeviceInfo>" +
                    "  <OutputFormat>jpeg</OutputFormat>" +
                    "  <PageWidth>11in</PageWidth>" +
                    "  <PageHeight>29,7in</PageHeight>" +
                    "  <MarginTop>0.5in</MarginTop>" +
                    "  <MarginLeft>1in</MarginLeft>" +
                    "  <MarginRight>1in</MarginRight>" +
                    "  <MarginBottom>0.5in</MarginBottom>" +
                    "</DeviceInfo>";
                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;
                renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
                ViewBag.frmDT = frm["frmDT"];
                ViewBag.toDT = frm["toDT"];
                ViewBag.img = renderedBytes;
                //return File(renderedBytes, "image/jpeg");
                SetDateFormat();
                return View();
            }
        }
		//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
        [HttpGet]
        public ActionResult CashRegister()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04  from FITABLE001    WHERE (COLUMN07=22409) AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Bank"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Countryp = new List<SelectListItem>();
            SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMNA13,'False')='False'", cn);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            for (int dd = 0; dd < dtp.Rows.Count; dd++)
            {
                Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
            }
            Countryp = Countryp.OrderBy(x => x.Text).ToList();
            ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
            SetDateFormat();
            return View();
        }

        //EMPHCS1302 BY GNANESHWAR ON 14/10/2015 Craeting Report InvertoryByItem
        public ActionResult InventoryByItem()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Item"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();
        }



        [HttpGet]
        public ActionResult ARAgeing()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
        [HttpGet]
        public ActionResult ARAgeingNew()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1706 InventoyByUOM and AR Aging Repot Chages ading Location And Sales rep by GNANESHWAR ON 22/4/2016
                List<SelectListItem> sales = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1 AND" + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
                }
                ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        //EMPHCS1309 BY GNANESHWAR ON 28/10/2015	AR aging  And Customer Payment Reports Chages on due amount
        public ActionResult ARAgeingDetailes()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> sales = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1 AND" + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                sales = sales.OrderBy(x => x.Text).ToList();
                ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1615 rajasekhar reddy patakota 16/03/2016 ARAging Details Report Changes for rk
        [HttpGet]
        public ActionResult ARAgeingDetailesNew()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> sales = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1 AND" + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                sales = sales.OrderBy(x => x.Text).ToList();
                ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
                //EMPHCS1805 rajasekhar reddy patakota 27/08/2016 Customer Wise Brand filter in aging reports
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult APAgeing()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult BalanceSheet()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult GeneralLedger()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1332	Adding type,Account filter in General Ledger Report BY RAj.Jr 9/11/2015
                List<SelectListItem> Country = new List<SelectListItem>();
                //EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  Where  COLUMN03=11115 and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and  isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001 where  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Account"] = new SelectList(Country2, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                List<SelectListItem> Countryv = new List<SelectListItem>();
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Countryv.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryv = Countryv.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Countryv, "Value", "Text");
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult TrialBalance()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                List<SelectListItem> cust = new List<SelectListItem>();

                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");
                List<SelectListItem> Country = new List<SelectListItem>();

                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country, "Value", "Text");

                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001 where  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Account"] = new SelectList(Country2, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ProfitAndLoss()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]

        //EMPHCS1285 gnaneshwar on 7/10/2015  Creating Profitandloss By Project Report																																															
        public ActionResult ProfitAndLossByProject()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dt1 = new DataTable();
                DataTable dtp = new DataTable();
                da1.Fill(dt1);
                dap.Fill(dtp);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["ProjectName"] = new SelectList(Countryp, "Value", "Text");
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " and isnull(s.COLUMNA13,0)=0 ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult VatInfo()
        {
            try
            {
                //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult GSTF()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult CustomerDiscount()
        {
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + "or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();
        }
        //EMPHCS1160	VAT Reports BY RAJ.Jr 01/10/2015
        [HttpGet]
        public ActionResult VATComputation()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1210	Inventory by UOM Report BY RAJ.Jr 25/9/2015
        [HttpGet]
        public ActionResult InventoryByUOM()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //List<SelectListItem> Country2 = new List<SelectListItem>();
                //SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11119 and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                //SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                //DataTable dt2 = new DataTable();
                //da2.Fill(dt2);
                //for (int dd = 0; dd < dt2.Rows.Count; dd++)
                //{
                //    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["UOM"] = new SelectList(Country2, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        //EMPHCS1580 rajasekhar reddy patakota 27/02/2016 Adding New Auditor Tax Reports
        public ActionResult ProductsTaxRegister()
        {
            try
            {
                //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
                GetTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult SevicesTaxRegister()
        {
            try
            {
                GetServiceTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult PurchaseTaxRegister()
        {
            try
            {
                GetTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult PurchaseTaxRegisterDetails()
        {
            try
            {
                GetTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult SalesTaxRegisterDetails()
        {
            try
            {
                GetTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult PurchaseSevicesTaxRegister()
        {
            try
            {
                GetServiceTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult SalesReturnTaxRegister()
        {
            try
            {
                GetTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult PurchaseReturnTaxRegister()
        {
            try
            {
                GetTaxregisterData();
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public void GetTaxregisterData()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Country2 = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013 where COLUMN16!='22401' AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and COLUMN15='false' and COLUMNA13='false'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country2 = Country2.OrderBy(x => x.Text).ToList();
            ViewData["Taxes"] = new SelectList(Country2, "Value", "Text");
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )  AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["TransDate"] = new SelectList(Country, "Value", "Text");


            //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cnB = new SqlConnection(sqlcon);
            SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
            SqlDataAdapter daB = new SqlDataAdapter(cmdB);
            DataTable dtB = new DataTable();
            daB.Fill(dtB);
            for (int dd = 0; dd < dtB.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
            }
            Brand = Brand.OrderBy(x => x.Text).ToList();
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
        }
        //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
        public void GetServiceTaxregisterData()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Country2 = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013 where COLUMN16='22401' AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and COLUMN15='false' and COLUMNA13='false'", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country2 = Country2.OrderBy(x => x.Text).ToList();
            ViewData["Taxes"] = new SelectList(Country2, "Value", "Text");
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )  AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["TransDate"] = new SelectList(Country, "Value", "Text");


            //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cnB = new SqlConnection(sqlcon);
            SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
            SqlDataAdapter daB = new SqlDataAdapter(cmdB);
            DataTable dtB = new DataTable();
            daB.Fill(dtB);
            for (int dd = 0; dd < dtB.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
            }
            Brand = Brand.OrderBy(x => x.Text).ToList();
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
        }
        [HttpGet]
        public ActionResult TDSRegister()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE013 where COLUMN16='22401' AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null) and COLUMN15='false' and COLUMNA13='false'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Taxes"] = new SelectList(Country2, "Value", "Text");
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )  AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1551	Sales and Purchase summary and detail Reports By SRINI
        [HttpGet]
        public ActionResult SalesByProductSummary()
        {
            try
            {
                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult SalesByProductDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult SalesByProductDetailReport()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlConnection cn1 = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  where  COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn1);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1556	PurchaseBySuppliersDetails & PurchaseBySuppliersSummary Reports BY RAJ.Jr
        [HttpGet]
        public ActionResult PurchasesBySuppliersDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country2, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }



        public ActionResult PurchaseRegister()
        {
            try
            {
                //List<SelectListItem> Country = new List<SelectListItem>();
                //SqlConnection cn = new SqlConnection(sqlcon);
                //SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                //SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //da.Fill(dt);
                //for (int dd = 0; dd < dt.Rows.Count; dd++)
                //{
                //    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                //}
                ////EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                //Country = Country.OrderBy(x => x.Text).ToList();
                //ViewData["TransDate"] = new SelectList(Country, "Value", "Text");



                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Country3= new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("SELECT COLUMN02,COLUMN06 COLUMN04 from  MATABLE010 where COLUMN30='True' and " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country3.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country3 = Country3.OrderBy(x => x.Text).ToList();
                ViewData["SalesRep"] = new SelectList(Country3, "Value", "Text");

                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                //List<SelectListItem> Brand = new List<SelectListItem>();
                //SqlConnection cnB = new SqlConnection(sqlcon);
                //SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                //SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                //DataTable dtB = new DataTable();
                //daB.Fill(dtB);
                //for (int dd = 0; dd < dtB.Rows.Count; dd++)
                //{
                //    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                //}
                //Brand = Brand.OrderBy(x => x.Text).ToList();
                //ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }




        //EMPHCS1556	PurchaseBySuppliersDetails & PurchaseBySuppliersSummary Reports BY RAJ.Jr
        [HttpGet]
        public ActionResult PurchasesBySuppliersSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1557 : PurchaseByProductDetails & PurchaseByProductSummary Reports By GNANESHWAR ON 9/2/2016
        [HttpGet]
        public ActionResult PurchaseByProductSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1557 : PurchaseByProductDetails & PurchaseByProductSummary Reports By GNANESHWAR ON 9/2/2016
        [HttpGet]
        public ActionResult PurchaseByProductDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Countryi = new List<SelectListItem>();
                SqlConnection cni = new SqlConnection(sqlcon);
                SqlCommand cmdi = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cni);
                SqlDataAdapter dai = new SqlDataAdapter(cmdi);
                DataTable dti = new DataTable();
                dai.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Countryi.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryi = Countryi.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Countryi, "Value", "Text");
                List<SelectListItem> Countryh = new List<SelectListItem>();
                SqlConnection cnh = new SqlConnection(sqlcon);
                SqlCommand cmdh = new SqlCommand("SELECT M32.COLUMN02 COLUMN02, M32.COLUMN04 COLUMN04 FROM MATABLE007 M7 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = M7.COLUMN75 AND ISNULL(M32.COLUMNA13, 0) = 0 WHERE M7.COLUMNA03 = '" + Session["AcOwner"] + "' AND ISNULL(M32.COLUMN04, '') != '' GROUP BY M32.COLUMN04,M32.COLUMN02 HAVING COUNT(M32.COLUMN04) > 1", cni);
                SqlDataAdapter dah = new SqlDataAdapter(cmdh);
                DataTable dth = new DataTable();
                dah.Fill(dth);
                for (int dd = 0; dd < dth.Rows.Count; dd++)
                {
                    Countryh.Add(new SelectListItem { Value = dth.Rows[dd]["COLUMN02"].ToString(), Text = dth.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryh = Countryh.OrderBy(x => x.Text).ToList();
                ViewData["HSN"] = new SelectList(Countryh, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1558 : SalesByCustomerSummary & SalesByCustomerDetails Reports By GNANESHWAR ON 9/2/2016
        [HttpGet]
        public ActionResult SalesByCustomerSummary()
        {
            try
            {
                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");



                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11160 )  and COLUMNA13='False'", cn2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country2, "Value", "Text");
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
        public ActionResult OpporutinesReport()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN07='False' and COLUMNA13='False'", cn2);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlConnection cn3 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11160 )  and COLUMNA13='False' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) ", cn3);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country2, "Value", "Text");
                List<SelectListItem> SalesPerson = new List<SelectListItem>();
                SqlConnection cn4 = new SqlConnection(sqlcon);
                SqlCommand cmd11 = new SqlCommand("select COLUMN02,COLUMN09 from MATABLE010 where COLUMN09!='' and isnull(COLUMNA13,0)=0 and isnull(COLUMN30,0)=1 and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' ", cn4);
                SqlDataAdapter da11 = new SqlDataAdapter(cmd11);
                DataTable dt11 = new DataTable();
                da11.Fill(dt11);
                for (int dd = 0; dd < dt11.Rows.Count; dd++)
                {
                    SalesPerson.Add(new SelectListItem { Value = dt11.Rows[dd]["COLUMN02"].ToString(), Text = dt11.Rows[dd]["COLUMN09"].ToString() });
                }
                SalesPerson = SalesPerson.OrderBy(x => x.Text).ToList();
                ViewData["SalesPerson"] = new SelectList(SalesPerson, "Value", "Text");
                List<SelectListItem> Priority = new List<SelectListItem>();
                SqlConnection cn5 = new SqlConnection(sqlcon);
                SqlCommand cmd111 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   COLUMN03 = 11135  and COLUMNA13='False' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) ", cn5);
                SqlDataAdapter da111 = new SqlDataAdapter(cmd111);
                DataTable dt111 = new DataTable();
                da111.Fill(dt111);
                for (int dd = 0; dd < dt111.Rows.Count; dd++)
                {
                    Priority.Add(new SelectListItem { Value = dt111.Rows[dd]["COLUMN02"].ToString(), Text = dt111.Rows[dd]["COLUMN04"].ToString() });
                }
                Priority = Priority.OrderBy(x => x.Text).ToList();
                ViewData["Priority"] = new SelectList(Priority, "Value", "Text");
                List<SelectListItem> Status = new List<SelectListItem>();
                SqlConnection cn6 = new SqlConnection(sqlcon);
                SqlCommand cmd1111 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where COLUMN03 = 11173 and COLUMNA13='False' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) ", cn6);
                SqlDataAdapter da1111 = new SqlDataAdapter(cmd1111);
                DataTable dt1111 = new DataTable();
                da1111.Fill(dt1111);
                for (int dd = 0; dd < dt1111.Rows.Count; dd++)
                {
                    Status.Add(new SelectListItem { Value = dt1111.Rows[dd]["COLUMN02"].ToString(), Text = dt1111.Rows[dd]["COLUMN04"].ToString() });
                }
                Status = Status.OrderBy(x => x.Text).ToList();
                ViewData["Status"] = new SelectList(Status, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult SalesByCustomerDetails()
        {
            try
            {
                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");


                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11160 )  and COLUMNA13='False'", cn2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }


        public ActionResult SalesRegister()
        {
            try
            {
                

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                List<SelectListItem> sales = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN06 from MATABLE010 WHERE COLUMN30=1 AND" + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN32='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    sales.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN06"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                sales = sales.OrderBy(x => x.Text).ToList();
                ViewData["SalesRep"] = new SelectList(sales, "Value", "Text");
                //EMPHCS1805 rajasekhar reddy patakota 27/08/2016 Customer Wise Brand filter in aging reports
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
               
                
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //CHNANGE BY GNANESHWAR ON 6/9/2016
        [HttpGet]
        public ActionResult SalesByCustomerDateWiseSummary()
        {
            try
            {
                //EMPHCS1825 : Adding Description Type Filter and Attended by to SalesByCustomerDateWise Details & Summary by GNANESHWAR
                List<SelectListItem> CountryD = new List<SelectListItem>();
                SqlConnection cnD = new SqlConnection(sqlcon);
                SqlCommand cmdD = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11186 )  and COLUMNA13='False'", cnD);
                SqlDataAdapter daD = new SqlDataAdapter(cmdD);
                DataTable dtD = new DataTable();
                daD.Fill(dtD);
                for (int dd = 0; dd < dtD.Rows.Count; dd++)
                {
                    CountryD.Add(new SelectListItem { Value = dtD.Rows[dd]["COLUMN02"].ToString(), Text = dtD.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                CountryD = CountryD.OrderBy(x => x.Text).ToList();
                ViewData["DisplayType"] = new SelectList(CountryD, "Value", "Text");

                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11160 )  and COLUMNA13='False'", cn2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country2, "Value", "Text");
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();

            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult SalesByCustomerDateWiseDetails()
        {
            try
            {
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");

                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11120)  and  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Countryc"] = new SelectList(Countryc, "Value", "Text");


                //EMPHCS1825 : Adding Description Type Filter and Attended by to SalesByCustomerDateWise Details & Summary by GNANESHWAR
                List<SelectListItem> CountryD = new List<SelectListItem>();
                SqlConnection cnD = new SqlConnection(sqlcon);
                SqlCommand cmdD = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11186 )  and COLUMNA13='False'", cnD);
                SqlDataAdapter daD = new SqlDataAdapter(cmdD);
                DataTable dtD = new DataTable();
                daD.Fill(dtD);
                for (int dd = 0; dd < dtD.Rows.Count; dd++)
                {
                    CountryD.Add(new SelectListItem { Value = dtD.Rows[dd]["COLUMN02"].ToString(), Text = dtD.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                CountryD = CountryD.OrderBy(x => x.Text).ToList();
                ViewData["DisplayType"] = new SelectList(CountryD, "Value", "Text");

                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  where   (COLUMN03 = 11160 )  and COLUMNA13='False'", cn2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cnc1 = new SqlConnection(sqlcon);
                SqlCommand cmdc1 = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cnc1);
                SqlDataAdapter dac1 = new SqlDataAdapter(cmdc1);
                DataTable dtc1 = new DataTable();
                dac1.Fill(dtc1);
                for (int dd = 0; dd < dtc1.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dtc1.Rows[dd]["COLUMN02"].ToString(), Text = dtc1.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");

                //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }


        //EMPHCS1562	New Reports For Returns BY RAJ.Jr
        [HttpGet]
        public ActionResult DebitMemoBySuppliersDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Vendor"] = new SelectList(Country2, "Value", "Text");
                List<SelectListItem> Countryc = new List<SelectListItem>();
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002 where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Countryc.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryc = Countryc.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Countryc, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult DebitMemoBySuppliersSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult CreditMemoByCustomerDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmdv = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002 where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                SqlDataAdapter dav = new SqlDataAdapter(cmdv);
                DataTable dtv = new DataTable();
                dav.Fill(dtv);
                for (int dd = 0; dd < dtv.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dtv.Rows[dd]["COLUMN02"].ToString(), Text = dtv.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country2, "Value", "Text");
                List<SelectListItem> CountryT = new List<SelectListItem>();
                SqlCommand cmdT = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11131 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter daT = new SqlDataAdapter(cmdT);
                DataTable dtT = new DataTable();
                daT.Fill(dtT);
                for (int dd = 0; dd < dtT.Rows.Count; dd++)
                {
                    CountryT.Add(new SelectListItem { Value = dtT.Rows[dd]["COLUMN02"].ToString(), Text = dtT.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            CountryT = CountryT.OrderBy(x => x.Text).ToList();
            ViewData["PartyType"] = new SelectList(CountryT, "Value", "Text");
            List<SelectListItem> Countryv = new List<SelectListItem>();
            SqlCommand cmdv1 = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            SqlDataAdapter dav1 = new SqlDataAdapter(cmdv1);
            DataTable dtv1 = new DataTable();
            dav1.Fill(dtv1);
            for (int dd = 0; dd < dtv1.Rows.Count; dd++)
            {
                Countryv.Add(new SelectListItem { Value = dtv1.Rows[dd]["COLUMN02"].ToString(), Text = dtv1.Rows[dd]["COLUMN04"].ToString() });
            }
            //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            Countryv = Countryv.OrderBy(x => x.Text).ToList();
            ViewData["Vendor"] = new SelectList(Countryv, "Value", "Text");
			            List<SelectListItem> Location = new List<SelectListItem>();
            SqlConnection cnz = new SqlConnection(sqlcon);
            SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
            SqlDataAdapter daz = new SqlDataAdapter(cmdz);
            DataTable dtz = new DataTable();
            daz.Fill(dtz);
            for (int dd = 0; dd < dtz.Rows.Count; dd++)
            {
                Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
            }
            Location = Location.OrderBy(x => x.Text).ToList();
            ViewData["Location"] = new SelectList(Location, "Value", "Text");
            List<SelectListItem> Category = new List<SelectListItem>();
            SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11120 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,0)=0", cn);
            SqlDataAdapter dac = new SqlDataAdapter(cmdc);
            DataTable dtc = new DataTable();
            dac.Fill(dtc);
            for (int dd = 0; dd < dtc.Rows.Count; dd++)
            {
                Category.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
            }
          
            Category = Category.OrderBy(x => x.Text).ToList();
            ViewData["Category"] = new SelectList(Category, "Value", "Text");
            //List<SelectListItem> Countryv = new List<SelectListItem>();
            //SqlCommand cmdv1 = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001 where (COLUMN22=22285 or COLUMN22 is NULL or COLUMN22='')  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN24='False' and COLUMNA13='False'", cn);
            //SqlDataAdapter dav1 = new SqlDataAdapter(cmdv1);
            //DataTable dtv1 = new DataTable();
            //dav1.Fill(dtv1);
            //for (int dd = 0; dd < dtv1.Rows.Count; dd++)
            //{
            //    Countryv.Add(new SelectListItem { Value = dtv1.Rows[dd]["COLUMN02"].ToString(), Text = dtv1.Rows[dd]["COLUMN04"].ToString() });
            //}
            
            //Countryv = Countryv.OrderBy(x => x.Text).ToList();
            //ViewData["Vendor"] = new SelectList(Countryv, "Value", "Text");
            SetDateFormat();
            return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult CreditMemoByCustomerSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                List<SelectListItem> Category = new List<SelectListItem>();
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11120 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Category.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }

                Category = Category.OrderBy(x => x.Text).ToList();
                ViewData["Category"] = new SelectList(Category, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult DebitMemoByProductDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult DebitMemoByProductSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult CreditMemoByProductDetails()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult CreditMemoByProductSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Location = new List<SelectListItem>();
                SqlConnection cnz = new SqlConnection(sqlcon);
                SqlCommand cmdz = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daz = new SqlDataAdapter(cmdz);
                DataTable dtz = new DataTable();
                daz.Fill(dtz);
                for (int dd = 0; dd < dtz.Rows.Count; dd++)
                {
                    Location.Add(new SelectListItem { Value = dtz.Rows[dd]["COLUMN02"].ToString(), Text = dtz.Rows[dd]["COLUMN04"].ToString() });
                }
                Location = Location.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Location, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult SetDateFormat()
        {
            try
            {
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlDataAdapter da = new SqlDataAdapter("select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0", cn);
                DataTable dt = new DataTable();
                da.Fill(dt); string DateFormat = null;
                if (dt.Rows.Count > 0)
                {
                    DateFormat = dt.Rows[0][0].ToString();
                    Session["FormatDate"] = dt.Rows[0][0].ToString();
                    Session["DateFormat"] = dt.Rows[0][0].ToString();
                }
                if (dt.Rows[0][1].ToString() != "")
                    Session["ReportDate"] = dt.Rows[0][1].ToString();
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                    Session["DateFormat"] = "dd/MM/yyyy";
                }
                return Json(new { Data2 = DateFormat }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

        // IP Config && Audit Information By Venkat
        [HttpGet]
        public ActionResult AuditInformation()
        {
            List<SelectListItem> User = new List<SelectListItem>();
            SqlConnection cnB = new SqlConnection(sqlcon);
            SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN09 as COLUMN04 from MATABLE010  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
            SqlDataAdapter daB = new SqlDataAdapter(cmdB);
            DataTable dtB = new DataTable();
            daB.Fill(dtB);
            for (int dd = 0; dd < dtB.Rows.Count; dd++)
            {
                User.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
            }
            User = User.OrderBy(x => x.Text).ToList();
            ViewData["User"] = new SelectList(User, "Value", "Text");
            List<SelectListItem> EmailID = new List<SelectListItem>();
            SqlConnection cnB1 = new SqlConnection(sqlcon);
            SqlCommand cmdB1 = new SqlCommand("select DISTINCT COLUMN03,COLUMN13 as COLUMN04 From MATABLE010 where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB1);
            SqlDataAdapter daB1 = new SqlDataAdapter(cmdB1);
            DataTable dtB1 = new DataTable();
            daB1.Fill(dtB1);
            for (int dd = 0; dd < dtB1.Rows.Count; dd++)
            {
                EmailID.Add(new SelectListItem { Value = dtB1.Rows[dd]["COLUMN03"].ToString(), Text = dtB1.Rows[dd]["COLUMN04"].ToString() });
            }
            EmailID = EmailID.OrderBy(x => x.Text).ToList();
            ViewData["EmailID"] = new SelectList(EmailID, "Value", "Text");
            
            List<SelectListItem> IPAddress = new List<SelectListItem>();
            SqlConnection cnB2 = new SqlConnection(sqlcon);
            SqlCommand cmdB2 = new SqlCommand("select DISTINCT COLUMN06 as COLUMN04,COLUMN03 From CONTABLE020 where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cnB2);
            
            SqlDataAdapter daB2 = new SqlDataAdapter(cmdB2);
            DataTable dtB2 = new DataTable();
            daB2.Fill(dtB2);
            for (int dd = 0; dd < dtB2.Rows.Count; dd++)
            {
                IPAddress.Add(new SelectListItem { Value = dtB2.Rows[dd]["COLUMN03"].ToString(), Text = dtB2.Rows[dd]["COLUMN04"].ToString() });
            }
            IPAddress = IPAddress.OrderBy(x => x.Text).ToList();
            ViewData["IPAddress"] = new SelectList(IPAddress, "Value", "Text");
            List<SelectListItem> browser = new List<SelectListItem>();
            browser.Add(new SelectListItem { Value = "1", Text = "Chrome" });
            browser.Add(new SelectListItem { Value = "2", Text = "Firefox" });
            browser.Add(new SelectListItem { Value = "3", Text = "Internet Explorer" });
            browser.Add(new SelectListItem { Value = "4", Text = "Safari" });
            browser.Add(new SelectListItem { Value = "5", Text = "Opera" });
            browser.Add(new SelectListItem { Value = "6", Text = "Lynx" });
            browser.Add(new SelectListItem { Value = "7", Text = "Konqueror" });
            browser.Add(new SelectListItem { Value = "8", Text = "UC Browser" });
            browser.Add(new SelectListItem { Value = "9", Text = "Tor" });
            browser.Add(new SelectListItem { Value = "10", Text = "Baidu" });
            browser.Add(new SelectListItem { Value = "11", Text = "Slim" });
            browser = browser.OrderBy(x => x.Text).ToList();
            ViewData["browser"] = new SelectList(browser, "Value", "Text");
            SetDateFormat();
            return View();
        }
        [HttpGet]
        public ActionResult JobbingLedger()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1587,1588) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1801 rajasekhar reddy patakota 25/08/2016 Grey Report Creation in jobbing in module
        [HttpGet]
        public ActionResult GreyReport()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN04 COLUMN02,COLUMN04 from PUTABLE022 where isnull((COLUMNA13),0)=0 and  COLUMN03=1605  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Joborderno"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult GreyReportSummary()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN04 COLUMN02,COLUMN04 from PUTABLE022 where isnull((COLUMNA13),0)=0 and  COLUMN03=1605  And  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Joborderno"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            for (int dd = 0; dd < dt1.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
            }
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            SetDateFormat();
            return View();
        }


        [HttpGet]
        public ActionResult ResourseConsumptions()
        {
            try
            {
                List<SelectListItem> cust = new List<SelectListItem>();
                SqlConnection cnc = new SqlConnection(sqlcon);
                SqlCommand cmdc = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cnc);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    cust.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                cust = cust.OrderBy(x => x.Text).ToList();
                ViewData["cust"] = new SelectList(cust, "Value", "Text");

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");

                List<SelectListItem> IFamily = new List<SelectListItem>();
                SqlConnection cnf = new SqlConnection(sqlcon);
                SqlCommand cmdf = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE003  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cnf);
                SqlDataAdapter daf = new SqlDataAdapter(cmdf);
                DataTable dtf = new DataTable();
                daf.Fill(dtf);
                for (int dd = 0; dd < dtf.Rows.Count; dd++)
                {
                    IFamily.Add(new SelectListItem { Value = dtf.Rows[dd]["COLUMN02"].ToString(), Text = dtf.Rows[dd]["COLUMN04"].ToString() });
                }
                IFamily = IFamily.OrderBy(x => x.Text).ToList();
                ViewData["IFamily"] = new SelectList(IFamily, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");

                List<SelectListItem> CountryT = new List<SelectListItem>();
                SqlCommand cmdT = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )  AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='False'", cn);
                SqlDataAdapter daT = new SqlDataAdapter(cmdT);
                DataTable dtT = new DataTable();
                daT.Fill(dtT);
                for (int dd = 0; dd < dtT.Rows.Count; dd++)
                {
                    CountryT.Add(new SelectListItem { Value = dtT.Rows[dd]["COLUMN02"].ToString(), Text = dtT.Rows[dd]["COLUMN04"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                CountryT = CountryT.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(CountryT, "Value", "Text");

                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1791	Resource Consumption prints and Reports By Raj.Jr
        [HttpGet]
        public ActionResult ResourceConsumptionSummary()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                List<SelectListItem> CountryT = new List<SelectListItem>();
                SqlCommand cmdT = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )  AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='False'", cn);
                SqlDataAdapter daT = new SqlDataAdapter(cmdT);
                DataTable dtT = new DataTable();
                daT.Fill(dtT);
                for (int dd = 0; dd < dtT.Rows.Count; dd++)
                {
                    CountryT.Add(new SelectListItem { Value = dtT.Rows[dd]["COLUMN02"].ToString(), Text = dtT.Rows[dd]["COLUMN04"].ToString() });
                }
                CountryT = CountryT.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(CountryT, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //EMPHCS1796 Production Entry Screen and Cost of Production Report Changes by gnaneshwar on 10/8/2016
        [HttpGet]
        public ActionResult ResourceConsumptionConsolidate()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                List<SelectListItem> CountryT = new List<SelectListItem>();
                SqlCommand cmdT = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )  AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and COLUMNA13='False'", cn);
                SqlDataAdapter daT = new SqlDataAdapter(cmdT);
                DataTable dtT = new DataTable();
                daT.Fill(dtT);
                for (int dd = 0; dd < dtT.Rows.Count; dd++)
                {
                    CountryT.Add(new SelectListItem { Value = dtT.Rows[dd]["COLUMN02"].ToString(), Text = dtT.Rows[dd]["COLUMN04"].ToString() });
                }
                CountryT = CountryT.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(CountryT, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }



        public ActionResult CustDetailes(string Category)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            List<SelectListItem> vendor = new List<SelectListItem>();
            var change = "";
            if (Category == "")
            {
                change = "";
            }
            else
            {
                change = "and COLUMN07=" + Category + "";
            }

            SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN03 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'" + change + " ", cn);
            // SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN03 from SATABLE002 ", cn);
            DataTable dtdata = new DataTable();
            cmddl.Fill(dtdata);
            ViewData["Customer"] = new SelectList(vendor, "Value", "Text");
            for (int dd = 0; dd < dtdata.Rows.Count; dd++)
            {
                vendor.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN03"].ToString() });
            }

            return Json(new { item = vendor }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InvoiceBillDetails()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
            }
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Item"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cnB = new SqlConnection(sqlcon);
            SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
            SqlDataAdapter daB = new SqlDataAdapter(cmdB);
            DataTable dtB = new DataTable();
            daB.Fill(dtB);
            for (int dd = 0; dd < dtB.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
            }
            Brand = Brand.OrderBy(x => x.Text).ToList();
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
            List<SelectListItem> Country1 = new List<SelectListItem>();
            SqlConnection cno = new SqlConnection(sqlcon);
            SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter dao = new SqlDataAdapter(cmdo);
            DataTable dto = new DataTable();
            dao.Fill(dto);
            for (int dd = 0; dd < dto.Rows.Count; dd++)
            {
                Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
            }
            Country1 = Country1.OrderBy(x => x.Text).ToList();
            ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
            List<SelectListItem> Country2 = new List<SelectListItem>();
            SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1273,1277) ", cn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            for (int dd = 0; dd < dt2.Rows.Count; dd++)
            {
                Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
            }
            Country2 = Country2.OrderBy(x => x.Text).ToList();
            ViewData["Type"] = new SelectList(Country2, "Value", "Text");

            //List<SelectListItem> Countryp = new List<SelectListItem>();
            //SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
            //SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            //DataTable dtp = new DataTable();
            //dap.Fill(dtp);
            //for (int dd = 0; dd < dtp.Rows.Count; dd++)
            //{
            //    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
            //}
            ////EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
            //Countryp = Countryp.OrderBy(x => x.Text).ToList();
            //ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
            //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
            //SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
            //SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
            //DataTable dtpo = new DataTable();
            //dapo.Fill(dtpo);
            //ViewBag.projectval = false;
            //if (dtpo.Rows.Count > 0)
            //{
            //    if (dtpo.Rows[0][0].ToString() == "Y")
            //        ViewBag.projectval = true;
            //}
            //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
            List<SelectListItem> Countryl = new List<SelectListItem>();
            SqlConnection cnl = new SqlConnection(sqlcon);
            SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
            SqlDataAdapter dal = new SqlDataAdapter(cmdl);
            DataTable dtl = new DataTable();
            dal.Fill(dtl);
            for (int dd = 0; dd < dtl.Rows.Count; dd++)
            {
                Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
            }
            Countryl = Countryl.OrderBy(x => x.Text).ToList();
            ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
            SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
            SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
            DataTable dtpl = new DataTable();
            dapl.Fill(dtpl);
            ViewBag.locationval = false;
            if (dtpl.Rows.Count > 0)
            {
                if (dtpl.Rows[0][0].ToString() == "Y")
                    ViewBag.locationval = true;
            }
            SetDateFormat();
            return View();
        }

        [HttpGet]
        public ActionResult ProfitAndLossByJobbing()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ProfitAndLossByProduct()
        {
            try
            {
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False' and COLUMN12='1296'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
            public ActionResult StockMoment()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
               
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
              
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult LotusReport()
            {
                try
                {
                    List<SelectListItem> Country1 = new List<SelectListItem>();
                    SqlConnection cn = new SqlConnection(sqlcon);
                    SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from SATABLE009 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or COLUMNA02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='0'", cn);
                    SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                    DataTable dt1 = new DataTable();
                    da1.Fill(dt1);
                    for (int dd = 0; dd < dt1.Rows.Count; dd++)
                    {
                        Country1.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                    }
                    Country1 = Country1.OrderBy(x => x.Text).ToList();
                    ViewData["IV"] = new SelectList(Country1, "Value", "Text");
                    SetDateFormat();
                    return View();
                }
                catch (Exception ex)
                {
                    var msg = "Page Not Found......";
                    Session["MessageFrom"] = msg + "Due to" + ex.Message;
                    Session["SuccessMessageFrom"] = "fail";
                    return RedirectToAction("Info", "EmployeeMaster");
                }
            }

        //BarCode Wise Stock By Venkat
        public ActionResult BarcodeWiseStock()
        {
            try
            {
               List<SelectListItem> Item = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
           
                  return View();
            }
            catch(Exception ex)
            {
             var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            
            
            }
           
        }

        [HttpGet]
        public ActionResult InventoryExpiry()
        {
            try
            {
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> Batch = new List<SelectListItem>();
                SqlCommand cmdb = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE043  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMNA13='False'", cn);
                SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                DataTable dtb = new DataTable();
                dab.Fill(dtb);
                for (int dd = 0; dd < dtb.Rows.Count; dd++)
                {
                    Batch.Add(new SelectListItem { Value = dtb.Rows[dd]["COLUMN02"].ToString(), Text = dtb.Rows[dd]["COLUMN04"].ToString() });
                }
                Batch = Batch.OrderBy(x => x.Text).ToList();
                ViewData["Batch"] = new SelectList(Batch, "Value", "Text");
                //List<SelectListItem> Brand = new List<SelectListItem>();
                //SqlConnection cnB = new SqlConnection(sqlcon);
                //SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                //SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                //DataTable dtB = new DataTable();
                //daB.Fill(dtB);
                //for (int dd = 0; dd < dtB.Rows.Count; dd++)
                //{
                //    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                //}
                //Brand = Brand.OrderBy(x => x.Text).ToList();
                //ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> TransDate = new List<SelectListItem>();
                SqlCommand cmdt = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002 where (COLUMN03=11160 )   and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                for (int dd = 0; dd < dtt.Rows.Count; dd++)
                {
                    TransDate.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN04"].ToString() });
                }
                TransDate = TransDate.OrderBy(x => x.Text).ToList();
                ViewData["TransDate"] = new SelectList(TransDate, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                //List<SelectListItem> Countryl = new List<SelectListItem>();
                //SqlConnection cnl = new SqlConnection(sqlcon);
                //SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or  isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                //SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                //DataTable dtl = new DataTable();
                //dal.Fill(dtl);
                //for (int dd = 0; dd < dtl.Rows.Count; dd++)
                //{
                //    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                //}
                //Countryl = Countryl.OrderBy(x => x.Text).ToList();
                //ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]
        public ActionResult ProjectPriceAnalysis()
        {
            try
            {
                List<SelectListItem> Project = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Project.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Project = Project.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Project, "Value", "Text");
                List<SelectListItem> SubProject = new List<SelectListItem>();
                SqlCommand cmdb = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE002  Where  COLUMN03=11171 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                DataTable dtb = new DataTable();
                dab.Fill(dtb);
                for (int dd = 0; dd < dtb.Rows.Count; dd++)
                {
                    SubProject.Add(new SelectListItem { Value = dtb.Rows[dd]["COLUMN02"].ToString(), Text = dtb.Rows[dd]["COLUMN04"].ToString() });
                }
                SubProject = SubProject.OrderBy(x => x.Text).ToList();
                ViewData["SubProject"] = new SelectList(SubProject, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult SalesInvoiceRK()
        {
            try
            {
                

                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from SATABLE009  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN04!='' and isnull(COLUMNA13,0)=0 and COLUMN03 =1277", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Country, "Value", "Text");
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult ActiveCustomers()
        {
            return View();
        }
		public ActionResult StockSummary()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult StockDetailsByItem()
        {
            try
            {
                List<SelectListItem> Country = new List<SelectListItem>();
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {
                    Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN04"].ToString() });
                }
                Country = Country.OrderBy(x => x.Text).ToList();
                ViewData["Item"] = new SelectList(Country, "Value", "Text");
                List<SelectListItem> Brand = new List<SelectListItem>();
                SqlConnection cnB = new SqlConnection(sqlcon);
                SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                SqlDataAdapter daB = new SqlDataAdapter(cmdB);
                DataTable dtB = new DataTable();
                daB.Fill(dtB);
                for (int dd = 0; dd < dtB.Rows.Count; dd++)
                {
                    Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
                }
                Brand = Brand.OrderBy(x => x.Text).ToList();
                ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
                List<SelectListItem> Country1 = new List<SelectListItem>();
                SqlConnection cno = new SqlConnection(sqlcon);
                SqlCommand cmdo = new SqlCommand("select COLUMN02,COLUMN03 from CONTABLE007 where COLUMN03!='' and ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))  AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False' and COLUMNA13='False'", cn);
                SqlDataAdapter dao = new SqlDataAdapter(cmdo);
                DataTable dto = new DataTable();
                dao.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Country1.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN03"].ToString() });
                }
                Country1 = Country1.OrderBy(x => x.Text).ToList();
                ViewData["OperatingUnit"] = new SelectList(Country1, "Value", "Text");
                List<SelectListItem> Country2 = new List<SelectListItem>();
                SqlCommand cmd2 = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE0010 where COLUMN02 in(1272,1273,1276,1277,1379,1357,1329,1354) ", cn);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                for (int dd = 0; dd < dt2.Rows.Count; dd++)
                {
                    Country2.Add(new SelectListItem { Value = dt2.Rows[dd]["COLUMN02"].ToString(), Text = dt2.Rows[dd]["COLUMN04"].ToString() });
                }
                Country2 = Country2.OrderBy(x => x.Text).ToList();
                ViewData["Type"] = new SelectList(Country2, "Value", "Text");

                List<SelectListItem> Countryp = new List<SelectListItem>();
                SqlCommand cmdp = new SqlCommand("select COLUMN02,COLUMN05 from PRTABLE001  WHERE " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMNA13='False'", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp);
                for (int dd = 0; dd < dtp.Rows.Count; dd++)
                {
                    Countryp.Add(new SelectListItem { Value = dtp.Rows[dd]["COLUMN02"].ToString(), Text = dtp.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                Countryp = Countryp.OrderBy(x => x.Text).ToList();
                ViewData["Project"] = new SelectList(Countryp, "Value", "Text");
                //EMPHCS1392 BY GNANESHWAR ON 27/11/2015 project checking condition in autofill config to display in reports
                SqlCommand cmdpo = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN36' and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
                DataTable dtpo = new DataTable();
                dapo.Fill(dtpo);
                ViewBag.projectval = false;
                if (dtpo.Rows.Count > 0)
                {
                    if (dtpo.Rows[0][0].ToString() == "Y")
                        ViewBag.projectval = true;
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                List<SelectListItem> Countryl = new List<SelectListItem>();
                SqlConnection cnl = new SqlConnection(sqlcon);
                SqlCommand cmdl = new SqlCommand("select COLUMN02,COLUMN04 from CONTABLE030 where COLUMN04!='' and ( " + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  AND COLUMNA03='" + Session["AcOwner"] + "'and isnull(COLUMN07,0)=0 and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtl = new DataTable();
                dal.Fill(dtl);
                for (int dd = 0; dd < dtl.Rows.Count; dd++)
                {
                    Countryl.Add(new SelectListItem { Value = dtl.Rows[dd]["COLUMN02"].ToString(), Text = dtl.Rows[dd]["COLUMN04"].ToString() });
                }
                Countryl = Countryl.OrderBy(x => x.Text).ToList();
                ViewData["Location"] = new SelectList(Countryl, "Value", "Text");
                SqlCommand cmdpl = new SqlCommand("Select s.COLUMN07  From  SETABLE012 s where s.COLUMN06 = 1251 and s.COLUMN05 = 'COLUMN47' and s.COLUMN04 =110011217 and s.COLUMNA03 = " + Session["AcOwner"] + " ", cn);
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                ViewBag.locationval = false;
                if (dtpl.Rows.Count > 0)
                {
                    if (dtpl.Rows[0][0].ToString() == "Y")
                        ViewBag.locationval = true;
                }
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        [HttpGet]
        public ActionResult MonthWiseSalesDetails()
        {
            List<SelectListItem> Country = new List<SelectListItem>();
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN25='False' and COLUMNA13='False'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int dd = 0; dd < dt.Rows.Count; dd++)
            {
                Country.Add(new SelectListItem { Value = dt.Rows[dd]["COLUMN02"].ToString(), Text = dt.Rows[dd]["COLUMN05"].ToString() });
            }
            
            Country = Country.OrderBy(x => x.Text).ToList();
            ViewData["Customer"] = new SelectList(Country, "Value", "Text");
            List<SelectListItem> Brand = new List<SelectListItem>();
            SqlConnection cnB = new SqlConnection(sqlcon);
            SqlCommand cmdB = new SqlCommand("select COLUMN02,COLUMN04 from MATABLE005  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cnB);
            SqlDataAdapter daB = new SqlDataAdapter(cmdB);
            DataTable dtB = new DataTable();
            daB.Fill(dtB);
            for (int dd = 0; dd < dtB.Rows.Count; dd++)
            {
                Brand.Add(new SelectListItem { Value = dtB.Rows[dd]["COLUMN02"].ToString(), Text = dtB.Rows[dd]["COLUMN04"].ToString() });
            }
            Brand = Brand.OrderBy(x => x.Text).ToList();
            ViewData["Brand"] = new SelectList(Brand, "Value", "Text");
            SetDateFormat();
            return View();
        }
         public ActionResult AuditTrackingReport()
            {
            try
            {
                SetDateFormat();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
    }
}
