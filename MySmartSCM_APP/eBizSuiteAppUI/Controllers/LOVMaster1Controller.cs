﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Xml;
using WebMatrix.Data;
using System.Net;
//using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using eBizSuiteDAL.classes;
using System.Configuration;

namespace MeBizSuiteAppUI.Controllers
{
    public class LOVMaster1Controller : Controller
    {
        private eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        //
        // GET: /LOVMaster1/
        string[] theader;
        string[] theadertext;
        public ActionResult Index()
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/LOVMaster1/Edit/" + item[tblcol] + " >Edit</a>|<a href=/LOVMaster1/Details/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   ''  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  '' />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;
                ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
				//EMPHCS805 rajasekhar reddy patakota 1/8/2015 Lov Details Null Operating Unit Checking
                var OPUnitWithNull = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                var query1 = "Select p.COLUMN02,p.COLUMN03,p.COLUMN04,d.COLUMN04 COLUMN06,o.COLUMN03 COLUMN07 from MATABLE01 p left outer join MATABLE002 d on d.COLUMN02=p.COLUMN06 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN07 where p.COLUMNA13='False' AND " + OPUnitWithNull + " AND p.COLUMNA03='" + Session["AcOwner"] + "'";
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                return View("Index", db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void Normal()
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                var col2 = "COLUMN02";
                cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN02",
                    Header = "Action",
                    Format = (item) => new HtmlString("<a href=/LOVMaster1/Edit/" + item[col2] + " >Edit</a>|<a href=/LOVMaster1/Detailes/" + item[col2] + " >Details</a>")
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN02",
                    Header = "LOV ID"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN03",
                    Header = "Name"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN04",
                    Header = "Description"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN05",
                    Header = "Subsidary"
                });
                ViewBag.cols = cols;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                RedirectToAction("Index", "LOVMaster1", new { FormName = Session["FormName"] });
            }
        }
        [HttpGet]
        public ActionResult Sort(string ShowInactives)
        {
            try
            {
                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;
                //Session["Data"] = null;
                //Session["sortselected"] = null;
                return View("Index", data);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "LOVMaster1", new { FormName = Session["FormName"] });
            }
        }


        //
        // GET: /LOVMaster1/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {
                MATABLE01 matable01 = db.MATABLE01.Find(id);
                var viewlist1 = db.CONTABLE001.Where(a => a.COLUMN03 != null).ToList();
                var viewlistou = db.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                var viewlistdept = db.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();
                ViewBag.views1 = viewlist1;
                ViewBag.view2 = viewlist1;
                ViewBag.ou = viewlistou;
                ViewBag.dept = viewlistdept;
                ViewBag.views1 = viewlist1;
                if (matable01 == null)
                {
                    return HttpNotFound();
                }
                return View(matable01);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // GET: /LOVMaster1/Create

        public ActionResult Create()
        {
            try
            {
                MATABLE01 all = new MATABLE01();
                var col2 = db.MATABLE01.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
                var col1 = col2;
                if (col2 == null || col2 == 0)
                { col1 = 11111; all.COLUMN02 = 56567; }
                else
                {
                    all.COLUMN02 = (Convert.ToInt32(col1) + 1);
                }
                int? ao = Convert.ToInt32(Session["AcOwner"]);
                var viewlist1 = db.CONTABLE001.Where(a => a.COLUMN03 != null).ToList();
                var viewlistou = db.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                var viewlistdept = db.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();
                ViewBag.views1 = viewlist1;
                ViewBag.view2 = viewlist1;
                ViewBag.ou = viewlistou;
                ViewBag.dept = viewlistdept;
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // POST: /LOVMaster1/Create

        [HttpPost]
        public ActionResult Create(MATABLE01 matable01, string SaveNew, string Save)
        {
            try
            {
            if (ModelState.IsValid)
            {
                //
                if (Session["LogedEmployeeID"] != null)
                    matable01.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                else
                    matable01.COLUMNA08 = null;

                var ac = Convert.ToInt32(Session["AcOwner"]);
                if (ac == 56567)
                {
                    matable01.COLUMNA02 = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                        matable01.COLUMNA02 = null;
                    else
                        matable01.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                }
                matable01.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                matable01.COLUMNA12 = true;
                matable01.COLUMNA13 = false;
                matable01.COLUMNA06 = DateTime.Now;
                matable01.COLUMNA07 = DateTime.Now;
                db.MATABLE01.Add(matable01);
                db.SaveChanges();
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1289 && q.COLUMN05 == 1).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Master Successfully Created... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                if (SaveNew != null)
                    return RedirectToAction("Create");
                else
                    return RedirectToAction("Index");

            }

            return View(matable01);
           }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1289 && q.COLUMN05 == 0).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Master Created Failed .........";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // GET: /LOVMaster1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                MATABLE01 matable01 = db.MATABLE01.Find(id);
                var viewlist1 = db.CONTABLE001.Where(a => a.COLUMN03 != null).ToList();
                var viewlistou = db.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                var viewlistdept = db.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();
                ViewBag.views1 = viewlist1;
                ViewBag.view2 = viewlist1;
                ViewBag.ou = viewlistou;
                ViewBag.dept = viewlistdept;
                ViewBag.views1 = viewlist1;

                if (matable01 == null)
                {
                    return HttpNotFound();
                }
                return View(matable01);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // POST: /LOVMaster1/Edit/5

        [HttpPost]
        public ActionResult Edit(MATABLE01 matable01)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var aa = Convert.ToString(matable01.COLUMN05);

                    // var viewlist2 = db.CONTABLE001.Where(a => a.COLUMN03 == aa).First();
                    // matable01.COLUMN05 = viewlist2.COLUMN02;
                    var ac = Convert.ToInt32(Session["AcOwner"]);
                    if (ac == 56567)
                    {
                        matable01.COLUMNA02 = null;
                    }
                    //else
                    //{
                    //    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    //        matable01.COLUMNA02 = null;
                    //    else
                    //        matable01.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                    //}
                    //matable01.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                    matable01.COLUMNA07 = DateTime.Now;

                    matable01.COLUMNA12 = true;

                    matable01.COLUMNA13 = false;
                    db.Entry(matable01).State = EntityState.Modified;
                    db.SaveChanges();
                    var msg = string.Empty;
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1289 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "List Of Value Master Successfully Updated... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    return RedirectToAction("Index");
                }
                return View(matable01);
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1289 && q.COLUMN05 == 3).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Master Update Failed .........";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // GET: /LOVMaster1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                MATABLE01 matable01 = db.MATABLE01.Find(id);

                var y = (from x in db.MATABLE01 where x.COLUMN02 == id select x).First();
                y.COLUMNA12 = false;
                y.COLUMNA13 = true;
                //db.MATABLE01.Remove(y);
                db.SaveChanges();
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1289 && q.COLUMN05 == 4).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Master Successfully Deleted.......... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1289 && q.COLUMN05 == 5).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Master Deletion Failed .........";
                }
      
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            
            }
        }

        //
        // POST: /LOVMaster1/Delete/5

        [HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    MATABLE01 matable01 = db.MATABLE01.Find(id);
        //    db.MATABLE01.Remove(matable01);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}



        public ActionResult UpdateInline(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list);
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                    var c = db.MATABLE01.Where(a => a.COLUMN02 == eid).FirstOrDefault();
                    c.COLUMN03 = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    c.COLUMN05 = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3].ToString());
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void ExportCSV()
        {
            try
            {
                StringWriter sw = new StringWriter();

                var y = db.MATABLE01.OrderBy(q => q.COLUMN02).ToList();

                sw.WriteLine("\"LOV ID\",\"Name\",\"Description\",\"Module ID\"");

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
                Response.ContentType = "text/csv";

                foreach (var line in y)
                {

                    sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\"",
                                               line.COLUMN02,
                                               line.COLUMN03,
                                               line.COLUMN04,
                                               line.COLUMN05));
                }

                Response.Write(sw.ToString());

                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void ExportPdf()
        {
            try
            {
                List<MATABLE01> all = new List<MATABLE01>();
                all = db.MATABLE01.ToList();
                var data = from e in db.MATABLE01.AsEnumerable() select new { LOVID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, ModuleID = e.COLUMN05 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gv.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public void Exportword()
        {
            try
            {
                List<MATABLE01> all = new List<MATABLE01>();
                all = db.MATABLE01.ToList();
                var data = from e in db.MATABLE01.AsEnumerable() select new { LOVID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, ModuleID = e.COLUMN05 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
                Response.ContentType = "application/vnd.ms-word ";
                Response.Charset = string.Empty;
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void ExportExcel()
        {
            try
            {
                List<MATABLE01> all = new List<MATABLE01>();
                all = db.MATABLE01.ToList();
                var data = from e in db.MATABLE01.AsEnumerable() select new { LOVID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, ModuleID = e.COLUMN05 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                gv.FooterRow.Visible = false;
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter ht = new HtmlTextWriter(sw);
                gv.RenderControl(ht);
                Response.Write(sw.ToString());
                Response.End();
                gv.FooterRow.Visible = true;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult Export(string items)
        {
            try
            {
                List<MATABLE01> all = new List<MATABLE01>();
                all = db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (items == "PDF")
                {
                    ExportPdf();
                }
                else if (items == "Excel")
                {
                    ExportExcel();
                }
                else if (items == "Word")
                {
                    Exportword();
                }
                else if (items == "CSV")
                {
                    ExportCSV();
                }
                return View("Index", db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
                var Name = Request["Name"];
                var LOVID = Request["LOVID"];


                if (Name != null && Name != "" && Name != string.Empty)
                {
                    Name = Name + "%";
                }
                else
                    Name = Request["Name"];


                if (LOVID != null && LOVID != "" && LOVID != string.Empty)
                {
                    LOVID = LOVID + "%";
                }
                else
                    LOVID = Request["LOVID"];

                var query = "SELECT * FROM MATABLE01 WHERE COLUMN03 like '" + Name + "' or COLUMN02 LIKE '" + LOVID + "'";

                var query1 = db.MATABLE01.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.cols = Session["cols"];
                ViewBag.Inlinecols = Session["Inlinecols"];
                return View("Index", gdata);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "LOVMaster1", new { FormName = Session["FormName"] });
            }
        }

        public JsonResult InActive(string ShowInactives)
        {
            List<MATABLE01> all = new List<MATABLE01>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            if (ShowInactives != null)
            {
                all = db.MATABLE01.ToList();
                var data = from e in db.MATABLE01.AsEnumerable() select new { LOVID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, ModuleID = e.COLUMN05 };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            else
            {
                all = db.MATABLE01.Where(a => a.COLUMNA12 == true).ToList();
                var data = from e in db.MATABLE01.AsEnumerable() where (e.COLUMNA12 == true) select new { LOVID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, ModuleID = e.COLUMN05 };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
      headerStyle: "webgrid-header",
      footerStyle: "webgrid-footer",
      alternatingRowStyle: "webgrid-alternating-row",
      rowStyle: "webgrid-row-style",
                          htmlAttributes: new { id = "DataTable" },
                          columns:
                              grid.Columns
                              (grid.Column("Action", "Action", format: (item) => new HtmlString("<a href='/LOVMaster1/Edit/" + item.LOVID + "'>Edit</a>|<a href='/LOVMaster1/Details/" + item.LOVID + "'>View</a>")),
                              grid.Column("LOVID", "LOV ID"),
                              grid.Column("Name", "Name"),
                              grid.Column("Description", "Description"),
                              grid.Column("ModuleID", "Module ID")));

            return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CustomView()
        {
            try
            {
                List<CONTABLE005> all = new List<CONTABLE005>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                //all = from e in dbContext.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
                var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
                all = db.CONTABLE005.SqlQuery(query).ToList();

                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "LOVMaster1", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
            int col2 = 0;
            var tbldata = db.CONTABLE004.Where(a => a.COLUMN05 == "LovMaster").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = db.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = db.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    db.CONTABLE013.Add(dd);
                    db.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        db.CONTABLE013.Add(dd);
                        db.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        }

        string tthdata, tthtext;
        string itthdata, itthtext;

        public ActionResult View1(string items, string sort, string inactive, string style)
        {
            try
            {
                List<MATABLE01> all = new List<MATABLE01>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                var ttid = tid.COLUMN02;
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (style == "Normal")
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var indata = db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from MATABLE01 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from MATABLE01 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();

                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/LOVMaster1/Edit/" + itm[Inline] + " >Edit</a>|<a href=/LOVMaster1/Details/" + itm[Inline] + " >View</a></td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                            else
                            {
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                    return View("Index", db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

                }
                else
                {
                    var grid = new WebGrid(null, canPage: false, canSort: false);
                    var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                    var tbtid = tbid.COLUMN02;
                    var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                    ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                    if (sort == "Recently Created")
                    {
                        all = db.MATABLE01.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        all = db.MATABLE01.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else
                    {
                        all = db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList();

                    }
                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/LOVMaster1/Edit/" + item[tblcol] + " >Edit</a>|<a href=/LOVMaster1/Details/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Index", db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult Style(string items, string inactive)
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                MATABLE01 lv = new MATABLE01();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                var data = from e in db.MATABLE01.AsEnumerable() select new { LOVID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, ModuleID = e.COLUMN05 };
                List<MATABLE01> tbldata = new List<MATABLE01>();
                tbldata = db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList();
                if (items == "Normal")
                {
                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == Attid).ToList();
                    var indata = db.MATABLE01.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from MATABLE01 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from MATABLE01 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/LOVMaster1/Edit/" + itm[Inline] + " >Edit</a>|<a href=/LOVMaster1/Details/" + itm[Inline] + " >Details</a></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   '' /></td>";
                            }
                            else
                            {
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    ''  /></td>";
                            }


                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                }
                return View("Index", db.MATABLE01.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "LOVMaster1", new { FormName = Session["FormName"] });
            }
        }

        //public ActionResult CustomView()
        //{
        //    List<CONTABLE005> all = new List<CONTABLE005>();
        //    var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
        //    var ttid = tid.COLUMN02;
        //    Session["tablid"] = ttid;

        //    var query = "SELECT TOP 5 * FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "";
        //    all = db.CONTABLE005.SqlQuery(query).ToList();

        //    return View(all);
        //}
        //string[] gdata = new string[3];
        //[HttpPost]
        //public JsonResult CustomView(string lst, string ViewName)
        //{
        //    if (ViewName == "")
        //    {
        //        return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
        //    }
        //    XmlDocument xDoc = new XmlDocument();
        //    xDoc.LoadXml(lst.ToString());
        //    DataSet ds = new DataSet();
        //    ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
        //    List<CONTABLE013> dd = new List<CONTABLE013>();
        //    int j = 0;
        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //        string act = ds.Tables[0].Rows[i]["Action"].ToString();
        //        string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
        //        if (act == "Y")
        //        {
        //            gdata[j] = Lab;

        //            //string tab = ds.Tables[0].Rows[i]["FieldName"].ToString();
        //            //string Lab= ds.Tables[0].Rows[i]["LabelName"].ToString();
        //            //string act = ds.Tables[0].Rows[i]["Action"].ToString();

        //            j++;
        //            if (j == 3)
        //            {
        //                CONTABLE013 add1 = new CONTABLE013();
        //               // var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
        //               // add1.COLUMN02 = (vid.COLUMN02) + 1;
        //                add1.COLUMN03 = ViewName;
        //                add1.COLUMN04 = Convert.ToInt32(Session["tablid"]);
        //                add1.COLUMN05 = "MATABLE01";
        //                add1.COLUMN06 = gdata[0];
        //                add1.COLUMN07 = gdata[1];
        //                add1.COLUMN08 = gdata[2];

        //                db.CONTABLE013.Add(add1);
        //                db.SaveChanges();
        //                return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        //                break;
        //            }
        //        }
        //    }
        //    List<CONTABLE005> all = new List<CONTABLE005>();

        //    return this.Json(all);
        //}


        //public ActionResult View1(string items)
        //{
        //    eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        //    if (items == "")
        //    {
        //        var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
        //        ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
        //        return View("Index", db.MATABLE01.Where(a => a.COLUMNA12 == true).ToList());
        //    }
        //    List<MATABLE01> all = new List<MATABLE01>();
        //    List<CONTABLE005> col = new List<CONTABLE005>();
        //    var tid = db.CONTABLE013.Where(a => a.COLUMN04 != null).First();
        //    var ttid = Convert.ToInt32(tid.COLUMN04);
        //    //Session["tablid"] = ttid;
        //    var collist = db.CONTABLE005.Where(a => a.COLUMN03 == ttid).ToList();
        //    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items).ToList();

        //    var col1 = viewlist.Select(a => a.COLUMN06).FirstOrDefault();
        //    var Acol1 = collist.Where(a => a.COLUMN05 == col1).FirstOrDefault();
        //    var actcol1 = Acol1.COLUMN04;

        //    var col2 = viewlist.Select(a => a.COLUMN07).FirstOrDefault();
        //    var Acol2 = collist.Where(a => a.COLUMN05 == col2).FirstOrDefault();
        //    var actcol2 = Acol2.COLUMN04;

        //    var col3 = viewlist.Select(a => a.COLUMN08).FirstOrDefault();
        //    var Acol3 = collist.Where(a => a.COLUMN05 == col3).FirstOrDefault();
        //    var actcol3 = Acol3.COLUMN04;

        //    //var col4 = viewlist.Select(a => a.COLUMN09).FirstOrDefault();
        //    //var Acol4 = collist.Where(a => a.COLUMN06 == col4).FirstOrDefault();
        //    //var actcol4 = Acol4.COLUMN05;

        //    //var col5 = viewlist.Select(a => a.COLUMN10).FirstOrDefault();
        //    //var Acol5 = collist.Where(a => a.COLUMN06 == col5).FirstOrDefault();
        //    //var actcol5 = Acol5.COLUMN05;

        //    //var col6 = viewlist.Select(a => a.COLUMN11).FirstOrDefault();
        //    //var Acol6 = collist.Where(a => a.COLUMN06 == col6).FirstOrDefault();
        //    //var actcol6 = Acol6.COLUMN05;
        //    //var col7 = viewlist.Select(a => a.COLUMN12).FirstOrDefault();
        //    //var Acol7 = collist.Where(a => a.COLUMN06 == col7).FirstOrDefault();
        //    //var actcol7 = Acol7.COLUMN05;
        //    //var col8 = viewlist.Select(a => a.COLUMN13).FirstOrDefault();
        //    //var Acol8 = collist.Where(a => a.COLUMN06 == col8).FirstOrDefault();
        //    //var actcol8 = Acol8.COLUMN05;
        //    //var col9 = viewlist.Select(a => a.COLUMN14).FirstOrDefault();
        //    //var Acol9 = collist.Where(a => a.COLUMN06 == col9).FirstOrDefault();
        //    //var actcol9 = Acol9.COLUMN05;
        //    //var col10 = viewlist.Select(a => a.COLUMN15).FirstOrDefault();
        //    //var Acol10 = collist.Where(a => a.COLUMN06 == col10).FirstOrDefault();
        //    //var actcol10 = Acol10.COLUMN05;

        //    all = db.MATABLE01.Where(a => a.COLUMNA12 == true).ToList();

        //    var data = from e in db.MATABLE01.AsEnumerable() where (e.COLUMNA12 == true) select new { actcol1 = col1, actcol2 = col2, actcol3 = col3 };

        //    var query = "SELECT  " + actcol1 + " as " + col1 + ", " + actcol2 + " as " + col2 + "," + actcol3 + " as " + col3 +  " FROM MATABLE01  where COLUMNA12 = 'true'";

        //    var dbC = WebMatrix.Data.Database.Open("sqlcon");
        //    var books = dbC.Query(query);
        //    var all1 = db.MATABLE01.SqlQuery(query);
        //    var grid = new WebGrid(books, canPage: false, canSort: false);
        //    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
        //              headerStyle: "webgrid-header",
        //              footerStyle: "webgrid-footer",
        //              alternatingRowStyle: "webgrid-alternating-row",
        //              rowStyle: "webgrid-row-style",
        //              htmlAttributes: new { id = "DataTable" },
        //              columns:
        //                  grid.Columns
        //                  (grid.Column("Action", "Action", format: (item) => new HtmlString("<a href='/LOVMaster1/Edit/" + item.LOV_ID + "'>Edit</a>|<a href='/LOVMaster1/Details/" + item.LOV_ID + "'>View1</a>")),
        //                 grid.Column(col1, col1),
        //                 grid.Column(col2, col2),
        //                 grid.Column(col3, col3)));


        //    return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);

        //}

        public ActionResult QuickSort(string item)
        {
            try
            {
                //var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE01").First();
                //var ttid = tid.COLUMN02;
                //var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                //ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                List<MATABLE01> all = new List<MATABLE01>();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                if (item == "Recently Created")
                {
                    all = db.MATABLE01.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA12 == true).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                else if (item == "Recently Modified")
                {
                    all = db.MATABLE01.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA12 == true).ToList();
                    all = all.Where(a => a.COLUMNA07 != null).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                else
                {
                    all = db.MATABLE01.Where(a => a.COLUMNA12 == true).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
              headerStyle: "webgrid-header",
              footerStyle: "webgrid-footer",
              alternatingRowStyle: "webgrid-alternating-row",
              rowStyle: "webgrid-row-style",
                                  htmlAttributes: new { id = "DataTable" },
                                  columns:
                                      grid.Columns
                                      (grid.Column("Action", "Action", format: (items) => new HtmlString("<a href='/LOVMaster1/Edit/" + items.COLUMN02 + "'>Edit</a>|<a href='/LOVMaster1/Detailes/" + items.COLUMN02 + "'>View</a>")),
                                      grid.Column("COLUMN02", "LOV ID"),
                                      grid.Column("COLUMN03", "Name"),
                                      grid.Column("COLUMN04", "Description"),
                                      grid.Column("COLUMN05", "Module ID")));



                return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "LOVMaster1", new { FormName = Session["FormName"] });
            }
        }




        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}