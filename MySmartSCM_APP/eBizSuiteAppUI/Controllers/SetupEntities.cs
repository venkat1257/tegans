﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeBizSuiteAppUI.Controllers
{
    public class SetupEntities
    {
        public string Trans { get; set; }
        public string Date { get; set; }
        public string FYear { get; set; }
        public string FYearID { get; set; }
        public string YSDate { get; set; }
        public string YEDate { get; set; }
        public string Lock { get; set; }
        public string Stock { get; set; }
        public string Account { get; set; }
        public string SubPeriod { get; set; }
        public string SubPeriodID { get; set; }
        public string NoofPeriods { get; set; }
        public string IsActive { get; set; }
        public string Memo { get; set; }
    }
}