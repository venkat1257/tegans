﻿using eBizSuiteAppModel.Table;
using eBizSuiteAppUI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;
using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using MeBizSuiteAppUI.Controllers;

namespace eBizSuiteAppUI.Controllers
{
    public class LOVDetailsController : Controller
    {
        //
        // GET: /LOVDetails/
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        CommonController cmn = new CommonController();
        eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        //create
        public ActionResult Create(int? MIS)
        {
            try
            {
                LOVDetailsModel obj = new LOVDetailsModel();
                MATABLE002 all = new MATABLE002();
                var viewlistou = dbContext.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                var viewlistdept = dbContext.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();
                var master = dbContext.MATABLE01.Where(n=>n.COLUMNA13==false).OrderBy(a => a.COLUMN03).ToList();
                obj.lstD = dbContext.MATABLE002.OrderBy(a => a.COLUMN01).ToList();
                ViewBag.ou = viewlistou;
                ViewBag.dept = viewlistdept;
                ViewBag.master = master;
                all.COLUMN03 = MIS;
                return View("Create", all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        //list
        string[] theader;
        string[] theadertext;
        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                var ttid = tid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/LOVDetails/Edit/" + item[tblcol] + " >Edit</a>|<a href=/LOVDetails/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;
                ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
				//EMPHCS805 rajasekhar reddy patakota 1/8/2015 Lov Details Null Operating Unit Checking
                var OPUnitWithNull = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                var query1 = "Select p.COLUMN02,p.COLUMN04,p.COLUMN05,d.COLUMN04 COLUMN06,o.COLUMN03 COLUMN07 from MATABLE002 p left outer join MATABLE002 d on d.COLUMN02=p.COLUMN06 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN07  where p.COLUMNA13='False' AND " + OPUnitWithNull + " AND p.COLUMNA03='" + Session["AcOwner"] + "' order by p.COLUMN02 DESC";
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;

                Session["Inlinecols"] = Inlinecols;
                return View("Info", act.MATABLE002.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        [HttpPost]
        public ActionResult Create(FormCollection collection, string Save, string SaveNew)
        {
            try
            {
                MATABLE002 lovDetails = new MATABLE002();
				//EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
                List<MATABLE002> objList = act.MATABLE002.OrderBy(s => s.COLUMN02).ToList();
                if (objList.Count > 0)
                {
                    lovDetails.COLUMN02 = objList[objList.Count - 1].COLUMN02 + 1;
                }
                else
                    lovDetails.COLUMN02 = 22222;
                lovDetails.COLUMN03 = Convert.ToInt32(collection["COLUMN03"]);
                lovDetails.COLUMN04 = collection["COLUMN04"];
                lovDetails.COLUMN05 = collection["COLUMN05"];
                //lovDetails.COLUMN06 = Convert.ToInt32(collection["COLUMN06"]);
                //lovDetails.COLUMN07 = Convert.ToInt32(collection["COLUMN07"]);
                lovDetails.COLUMNA01 = null;
                var ac = Convert.ToInt32(Session["AcOwner"]);
                if (ac == 56567)
                {
                    lovDetails.COLUMNA02 = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                        lovDetails.COLUMNA02 = null;
                    else
                        lovDetails.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                }
                lovDetails.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                if (Session["LogedEmployeeID"] != null)
                    lovDetails.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                else
                    lovDetails.COLUMNA04 = null;
                lovDetails.COLUMNA05 = null;
                lovDetails.COLUMNA06 = DateTime.Now;
                lovDetails.COLUMNA07 = null;
                lovDetails.COLUMNA09 = DateTime.Now;
                lovDetails.COLUMNA10 = null;
                lovDetails.COLUMNA11 = null;
                lovDetails.COLUMNA12 = true;
                lovDetails.COLUMNA13 = false;
                act.MATABLE002.Add(lovDetails);
                act.SaveChanges();
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1290 && q.COLUMN05 == 1).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Details Successfully Created... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1290", lovDetails.COLUMN03.ToString(), "Update"); }
                if (Save != null)
                    return RedirectToAction("Info");
                else if (SaveNew != null)
                    return RedirectToAction("Create", new { MIS = lovDetails.COLUMN03 });
                return RedirectToAction("Info");
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                if (Save != null)
                {
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1290 && q.COLUMN05 == 0).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "List Of Value Details Created Failed .........";
                    }
                    Session["MessageFrom"] = msg + " Due to " + ex.Message;
                    Session["SuccessMessageFrom"] = "fail";
                }
               // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }


        }

        //search
        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var ValueID = Request["ValueID"];
                var MasterID = Request["MasterID"];
                var ValueName = Request["ValueName"];


                if (ValueID != null && ValueID != "" && ValueID != string.Empty)
                {
                    ValueID = ValueID + "%";
                }
                else
                    ValueID = Request["ValueID"];


                if (MasterID != null && MasterID != "" && MasterID != string.Empty)
                {
                    MasterID = MasterID + "%";
                }
                else
                    MasterID = Request["MasterID"];

                if (ValueName != null && ValueName != "" && ValueName != string.Empty)
                {
                    ValueName = ValueName + "%";
                }
                else
                    ValueName = Request["ValueName"];

                var query = "SELECT * FROM MATABLE002  WHERE COLUMN02 like '" + ValueID + "' or COLUMN03 LIKE '" + MasterID + "' or COLUMN04 LIKE '" + ValueName + "'";

                var query1 = dbContext.MATABLE002.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.cols = Session["cols"];
                ViewBag.Inlinecols = Session["Inlinecols"];
                ViewBag.columns = Session["Inlinecols"];


                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                var ttid = tid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();

                return View("Info", gdata);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //Inline

        [HttpPost]
        public ActionResult UpdateInline(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list);
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                    var c = dc.MATABLE002.Where(a => a.COLUMN02 == eid).FirstOrDefault();
                    c.COLUMN03 = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[1].ToString());
                    c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                dc.SaveChanges();


                    dc.SaveChanges();
                }

                return RedirectToAction("Info");

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "LOVDetails", new { FormName = Session["FormName"] });
            }
        }
        //ShowInActive
        public JsonResult InActive(string ShowInactives)
        {

            List<MATABLE002> all = new List<MATABLE002>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            if (ShowInactives != null)
            {
                all = dbContext.MATABLE002.ToList();
                var data = from e in dbContext.MATABLE002.AsEnumerable() select new { ValueID = e.COLUMN02, MasterID = e.COLUMN03, ValueName = e.COLUMN04 };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            else
            {
                all = dbContext.MATABLE002.Where(a => a.COLUMNA12 == true).ToList();
                var data = from e in dbContext.MATABLE002.AsEnumerable() where (e.COLUMNA12 == true) select new { ValueID = e.COLUMN02, MasterID = e.COLUMN03, ValueName = e.COLUMN04 };
                grid = new WebGrid(data, canPage: false, canSort: false);
            }
            var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
      headerStyle: "webgrid-header",
      footerStyle: "webgrid-footer",
      alternatingRowStyle: "webgrid-alternating-row",
      rowStyle: "webgrid-row-style",
                          htmlAttributes: new { id = "DataTable" },
                          columns:
                              grid.Columns
                              (grid.Column("Action", "Action", format: (item) => new HtmlString("<a href='/LOVDetails/Edit/" + item.ValueID + "'>Edit</a>|<a href='/LOVDetails/Detailes/" + item.ValueID + "'>View</a>")),
                              grid.Column("ValueID", "ValueID"),
                              grid.Column("MasterID", "MasterID"),
                              grid.Column("ValueName", "ValueName")
                              ));
            return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /LOVDetails/Edit/5

        public ActionResult Edit(int id)
        {
            try
            {
                MATABLE002 edit = act.MATABLE002.Find(id);
                Session["editid"] = id;
                var col2 = edit.COLUMN02;
                var viewlistou = dbContext.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                var viewlistdept = act.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();
                Session["time"] = act.MATABLE002.Find(col2).COLUMNA06;
                var master = dbContext.MATABLE01.Where(n => n.COLUMNA13 == false).OrderBy(a => a.COLUMN03).ToList();
                ViewBag.ou = viewlistou;
                ViewBag.dept = viewlistdept;
                ViewBag.master = master;
                return View(edit);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // POST: /LOVDetails/Edit/5

        [HttpPost]
        public ActionResult Edit(MATABLE002 Info)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int editid = Convert.ToInt32(Session["editid"]);
                    var edit = act.MATABLE002.Where(a => a.COLUMN02 == editid).FirstOrDefault();
                    edit.COLUMNA07 = DateTime.Now;
                    //Info.COLUMNA06 = Convert.ToDateTime(Session["time"]);
                    edit.COLUMNA12 = true;
                    edit.COLUMNA13 = false;
                    Session["time"] = null;
                    edit.COLUMN02 = editid;
                    edit.COLUMN03 = Info.COLUMN03;
                    edit.COLUMN04 = Info.COLUMN04;
                    edit.COLUMN05 = Info.COLUMN05;
                    //edit.COLUMN06 = Info.COLUMN06;
                    //edit.COLUMN07 = Info.COLUMN07;
                    var ac = Convert.ToInt32(Session["AcOwner"]);
                    if (ac == 56567)
                    {
                        edit.COLUMNA02 = null;
                    }
                    //else
                    //{
                    //    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    //        edit.COLUMNA02 = null;
                    //    else
                    //        edit.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                    //}
                    //edit.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                    Info.COLUMN02 = editid;
                    act.Entry(edit).State = EntityState.Modified;
                    act.SaveChanges();
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1290 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "List Of Value Details Successfully Updated... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    if (Convert.ToString(Session["RediesCache"]) == "1")
                    { int d = cmn.UpdateRedisCacheStatus("1290", Info.COLUMN03.ToString(), "Update"); }
                    return RedirectToAction("Info");
                }
                return View(Info);
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1290 && q.COLUMN05 == 3).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Details Update Failed... ";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // GET: /LOVDetails/Delete/5
        public ActionResult Delete(int id = 0)
        {
            try
            {
                MATABLE002 del = dbContext.MATABLE002.Find(id);

                var y = (from x in dbContext.MATABLE002 where x.COLUMN02 == id select x).First();
                string s = "False";
                y.COLUMNA12 = Convert.ToBoolean(s);
                y.COLUMNA13 = true;
                //dbContext.MATABLE002.Remove(y);
                dbContext.SaveChanges();
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1290 && q.COLUMN05 == 4).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Details Successfully Deleted.......... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                if (Convert.ToString(Session["RediesCache"]) == "1")
                { int d = cmn.UpdateRedisCacheStatus("1290", y.COLUMN03.ToString(), "Update"); }
                return RedirectToAction("Info");
            }
            catch (Exception ex)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1290 && q.COLUMN05 == 5).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "List Of Value Details Deletion Failed .........";
                }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        //
        // POST: /LOVDetails/Delete/5





        public ActionResult Detailes(int id)
        {
            try
            {
                MATABLE002 Details = act.MATABLE002.Find(id);
                var viewlistou = dbContext.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
                var viewlistdept = act.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();
                var master = dbContext.MATABLE01.OrderBy(a => a.COLUMN03).ToList();
                ViewBag.ou = viewlistou;
                ViewBag.dept = viewlistdept;
                ViewBag.master = master;
                return View(Details);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }





        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult Style(string items, string inactive, string view, string sort)
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                List<MATABLE002> all = new List<MATABLE002>();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                var data = from e in dbContext.MATABLE002.AsEnumerable() select new { Value_ID = e.COLUMN02, LOV_ID = e.COLUMN03, Value_Name = e.COLUMN04 };
                List<MATABLE002> tbldata = new List<MATABLE002>();
                tbldata = dbContext.MATABLE002.Where(a => a.COLUMNA13 == false).ToList();
                if (items == "Normal")
                {
                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                    var indata = dbContext.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from MATABLE002 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from MATABLE002 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    } int r = 0; string rowColor = "";
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            r = r + 1;

                            if (r % 2 == 0)
                            {
                                rowColor = "alt";
                            }
                            else
                            {
                                rowColor = "alt0";
                            }

                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/LOVDetails/Edit/" + itm[Inline] + " >Edit</a>|<a href=/LOVDetails/Detailes/" + itm[Inline] + " >View</a></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                            }
                            else
                            {
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                            }


                        }
                        tthdata = "<tr class=" + @rowColor + " style='color:black'>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    grid = new WebGrid(null, canPage: false, canSort: false);

                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/LOVDetails/Edit/" + item[tblcol] + " >Edit</a>|<a href=/LOVDetails/Detailes/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Info", dbContext.MATABLE002.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "LOVDetails", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult CustomView()
        {
            try
            {
                List<CONTABLE005> all = new List<CONTABLE005>();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                //all = from e in dbContext.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
                var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
                all = dbContext.CONTABLE005.SqlQuery(query).ToList();

                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "LOVDetails", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
            int col2 = 0;
            var tbldata = dbContext.CONTABLE004.Where(a => a.COLUMN05 == "LOV_Details").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = dbContext.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    dbContext.CONTABLE013.Add(dd);
                    dbContext.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        dbContext.CONTABLE013.Add(dd);
                        dbContext.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult View(string items, string sort, string inactive, string style)
        {
            try
            {
                List<MATABLE002> all = new List<MATABLE002>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                var ttid = tid.COLUMN02;
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (style == "Normal")
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var indata = dbContext.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from MATABLE002 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from MATABLE002 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/LOVDetails/Edit/" + itm[Inline] + " >Edit</a>|<a href=/LOVDetails/Detailes/" + itm[Inline] + " >View</a></td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                            else
                            {
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                    return View("Info", dbContext.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

                }
                else
                {
                    var grid = new WebGrid(null, canPage: false, canSort: false);
                    var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                    var tbtid = tbid.COLUMN02;
                    var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                    ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                    if (sort == "Recently Created")
                    {
                        all = dbContext.MATABLE002.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        all = dbContext.MATABLE002.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else
                    {
                        all = dbContext.MATABLE002.Where(a => a.COLUMNA13 == false).ToList();

                    }
                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/LOVDetails/Edit/" + item[tblcol] + " >Edit</a>|<a href=/LOVDetails/Detailes/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Info", dbContext.MATABLE002.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }



        public void ExportPdf()
        {
            try
            {
                List<MATABLE002> all = new List<MATABLE002>();
                all = dbContext.MATABLE002.ToList();
                var data = from e in dbContext.MATABLE002.AsEnumerable() select new { Value_ID = e.COLUMN02, LOV_ID = e.COLUMN03, Value_Name = e.COLUMN04 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gv.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public void Exportword()
        {
            try
            {
                List<MATABLE002> all = new List<MATABLE002>();
                all = dbContext.MATABLE002.ToList();
                var data = from e in dbContext.MATABLE002.AsEnumerable() select new { Value_ID = e.COLUMN02, LOV_ID = e.COLUMN03, Value_Name = e.COLUMN04 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
                Response.ContentType = "application/vnd.ms-word ";
                Response.Charset = string.Empty;
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public void ExportCSV()
        {
            try
            {
                StringWriter sw = new StringWriter();

                var y = dbContext.MATABLE002.OrderBy(q => q.COLUMN02).ToList();

                sw.WriteLine("\"Value_ID\",\"LOV_ID\",\"Value_Name\"");

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
                Response.ContentType = "text/csv";

                foreach (var line in y)
                {

                    sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\"",
                                               line.COLUMN02,
                                               line.COLUMN03,
                                               line.COLUMN04));

                }

                Response.Write(sw.ToString());

                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }

        }
        public void ExportExcel()
        {
            try
            {
                List<MATABLE002> all = new List<MATABLE002>();
                all = dbContext.MATABLE002.ToList();
                var data = from e in dbContext.MATABLE002.AsEnumerable() select new { Value_ID = e.COLUMN02, LOV_ID = e.COLUMN03, Value_Name = e.COLUMN04 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                gv.FooterRow.Visible = false;
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter ht = new HtmlTextWriter(sw);
                gv.RenderControl(ht);
                Response.Write(sw.ToString());
                Response.End();
                gv.FooterRow.Visible = true;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult Export(string items)
        {
            try
            {
                List<MATABLE002> all = new List<MATABLE002>();
                all = dbContext.MATABLE002.Where(a => a.COLUMNA13 == false).ToList();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "MATABLE002").First();
                var ttid = tid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (items == "PDF")
                {
                    ExportPdf();
                }
                else if (items == "Excel")
                {
                    ExportExcel();
                }
                else if (items == "Word")
                {
                    Exportword();
                }
                else if (items == "CSV")
                {
                    ExportCSV();
                }
                return View("Info", dbContext.MATABLE002.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

    }
}
