﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class EmailTemplateController : Controller
    {
        //
        // GET: /EmailTemplate/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        string providerName = "System.Data.SqlClient";
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult NewTemplate()
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                int? OPUnit= Convert.ToInt32(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var Type = dc.MATABLE002.Where(a => a.COLUMN03 == 11179 && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                Type = Type.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                ViewBag.Type = Type;
                var Placeholder = dc.MATABLE002.Where(a => a.COLUMN03 == 11184 && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                Placeholder = Placeholder.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                ViewBag.Placeholder = Placeholder;
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1621).ToList().FirstOrDefault().COLUMN04.ToString();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult NewTemplate(FormCollection col,  HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1621).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit =Convert.ToString( Session["OPUnit"]);
                string opstatus=Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null||opstatus =="") OpUnit = null;
                string AOwner = Convert.ToString( Session["AcOwner"]);
                string insert = "Insert";
                cn.Open();
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE017 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_EmailTemplate", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1621");
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Type"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Name"]);
                string defaultflag=Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMN07", col["Message"]);
                Cmd.Parameters.AddWithValue("@COLUMN09", col["Body"]);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE017");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1621 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1621 && q.COLUMN05 == 0).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Creation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult TemplateInfo()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = "SELECT p.[COLUMN02 ] AS ID ,p.[COLUMN05 ] AS [Name],m.[COLUMN04] AS Type,p.[COLUMN06 ] AS [Default],p.[COLUMN07 ] AS [Message],isnull(p.COLUMNA03,0) NullAC" +
                " from SETABLE017 p left join  MATABLE002 m on m.COLUMN02=p.COLUMN04 where isnull(p.COLUMNA13,'False')='False'  and " + OPUnit + " and (p.COLUMNA03=" + acID + " or p.COLUMNA03 is null) AND  p.COLUMN03=1621  ORDER BY p.COLUMNA06 desc ";
                alCol.AddRange(new List<string> { "ID", "Name", "Type", "Default", "Message" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN05", "COLUMN04", "COLUMN06", "COLUMN07" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult TemplateEdit()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1621).ToList().FirstOrDefault().COLUMN04.ToString();
                List<int> permission = Session["Permission"] as List<int>;
                if (permission.Contains(22933))
                {
                    SqlDataAdapter cmdv = new SqlDataAdapter("SELECT COLUMN04 Type,COLUMN05 Name,COLUMN06 [Default],COLUMN07 Message,COLUMN09 Body,isnull(COLUMNA03,0) NullAC FROM SETABLE017 WHERE  COLUMN02=" + @Request["ide"] + "  and columna13=0 ", cn);
                    DataTable dtv = new DataTable();
                    cmdv.Fill(dtv);
                    System.Collections.IEnumerable rows = dtv.Rows;
                    ViewBag.pitemsdata = rows;
                    List<SelectListItem> Typeddl = new List<SelectListItem>();
                    List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                    int? OPUnit = Convert.ToInt32(Session["OPUnit"]);
                    var acOW = Convert.ToInt32(Session["AcOwner"]);
                    var Type = dc.MATABLE002.Where(a => a.COLUMN03 == 11179 && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                    Type = Type.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                    var Placeholder = dc.MATABLE002.Where(a => a.COLUMN03 == 11184 && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                    Placeholder = Placeholder.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                    ViewBag.Placeholder = Placeholder;                

                    List<POPrint> all1 = new List<POPrint>();
                    all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                    {
                         TempType ="",
                         TempName  ="",
                         TempDefault  ="",
                         TempMessage  ="",
                         TempBody = "",
                         NullAC = ""
                    });
                    all1[0].TempType = dtv.Rows[0]["Type"].ToString();
                    all1[0].TempName = dtv.Rows[0]["Name"].ToString();
                    all1[0].TempDefault = dtv.Rows[0]["Default"].ToString();
                    all1[0].TempMessage = dtv.Rows[0]["Message"].ToString();
                    all1[0].TempBody = dtv.Rows[0]["Body"].ToString();
                    all1[0].NullAC = dtv.Rows[0]["NullAC"].ToString();
                    Typeddl = Type.Select(m => new SelectListItem { Value = m.COLUMN02.ToString(), Text = m.COLUMN04 }).ToList();
                    ViewData["Type"] = new SelectList(Typeddl, "Value", "Text", selectedValue: all1[0].TempType);
                    return View(all1);
                }
                else
                {
                    Session["MessageFrom"] = "Edit Can't be Permitted ";
                    Session["SuccessMessageFrom"] = "fail";
                    return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult TemplateEdit(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1621).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update";
                cn.Open();
                var idi = "";var ide = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im>=0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE017 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_EmailTemplate", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1621");
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Type"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Name"]);
                string defaultflag = Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMN07", col["Message"]);
                Cmd.Parameters.AddWithValue("@COLUMN09", col["Body"]);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE017");
                int r = Cmd.ExecuteNonQuery();
                cn.Close(); 
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1621 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1621 && q.COLUMN05 == 3).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Updation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult TemplateDelete()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1621).ToList().FirstOrDefault().COLUMN04.ToString();
                var ide = ""; var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete";
                cn.Open();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_EmailTemplate", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE017");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1621 && q.COLUMN05 == 4).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1621 && q.COLUMN05 == 5).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Deletion Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult TemplateView()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1621).ToList().FirstOrDefault().COLUMN04.ToString();
                SqlDataAdapter cmdv = new SqlDataAdapter("SELECT COLUMN04 Type,COLUMN05 Name,COLUMN06 [Default],COLUMN07 Message,COLUMN09 Body,isnull(COLUMNA03,0) NullAC FROM SETABLE017 WHERE  COLUMN02=" + @Request["ide"] + "  and columna13=0 ", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                System.Collections.IEnumerable rows = dtv.Rows;
                ViewBag.pitemsdata = rows;
                List<SelectListItem> Typeddl = new List<SelectListItem>();
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                int? OPUnit = Convert.ToInt32(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var Type = dc.MATABLE002.Where(a => a.COLUMN03 == 11179 && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                Type = Type.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                var Placeholder = dc.MATABLE002.Where(a => a.COLUMN03 == 11184 && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                Placeholder = Placeholder.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                ViewBag.Placeholder = Placeholder;

                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    TempType = "",
                    TempName = "",
                    TempDefault = "",
                    TempMessage = "",
                    TempBody = "",
                    NullAC = ""
                });
                all1[0].TempType = dtv.Rows[0]["Type"].ToString();
                all1[0].TempName = dtv.Rows[0]["Name"].ToString();
                all1[0].TempDefault = dtv.Rows[0]["Default"].ToString();
                all1[0].TempMessage = dtv.Rows[0]["Message"].ToString();
                all1[0].TempBody = dtv.Rows[0]["Body"].ToString();
                all1[0].NullAC = dtv.Rows[0]["NullAC"].ToString();
                Typeddl = Type.Select(m => new SelectListItem { Value = m.COLUMN02.ToString(), Text = m.COLUMN04 }).ToList();
                ViewData["Type"] = new SelectList(Typeddl, "Value", "Text", selectedValue: all1[0].TempType);
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("TemplateInfo", "EmailTemplate", new { FormName = Session["FormName"] });
            }
        }
    }
}
