﻿using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class TransactionMasterController : Controller
    {
        //
        // GET: /TransactionMaster/
        string[] theader;
        string[] theadertext;

        //
        // GET: /UserPreferences/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Index()
        {
            return View();
        }
        //string col = "Transaction";
        //    eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
        //    var lst = act.SETABLE010.Where(q => q.COLUMN05 == col).ToList();
        //    var tbCode = lst.Select(a => a.COLUMN03).ToList();
        //    var tbName = lst.Select(a => a.COLUMN04).ToList();
        //    foreach (string cols in tbCode)
        //    {
        //        colNM.Add(cols);
        //    }
        [HttpGet]
        public ActionResult Create()
        {
            eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();

            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";


            var billsql = "  select COLUMN06 ,COLUMN05,COLUMN03,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13  from  MYTABLE002 where COLUMN04='Transaction'  and  COLUMNA03='" + Session["AcOwner"] + "' AND (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) and isnull(COLUMNA13,0)=0";
            var billsqlM = "  select COLUMN06 ,COLUMN05,COLUMN03,COLUMN07,COLUMN08,COLUMN09,COLUMN10,COLUMN12,COLUMN13 from  MYTABLE002 where COLUMN04='Master'  and COLUMNA03='" + Session["AcOwner"] + "' AND (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) and isnull(COLUMNA13,0)=0";


            var billdb = Database.OpenConnectionString(connectionString, providerName);
            var billGData = billdb.Query(billsql);
            var billGDataM = billdb.Query(billsqlM);

            var billid = billGData.FirstOrDefault();
            var billidM = billGDataM.FirstOrDefault();

            ViewBag.itemslist = billGData;
            ViewBag.itemslistM = billGDataM;

            string col = "Transaction";
            string col1 = "Master";

            for (int i = 0; i < billGData.Count(); i++)
            {
                var id = billGData.ElementAt(i)[2];
                var val = billGData.ElementAt(i)[1];
                if (id != null)
                {
                    var customForm = "SELECT CONTABLE0010.COLUMN04 FROM CONTABLE0010 INNER JOIN SETABLE010 ON CONTABLE0010.COLUMN06=SETABLE010.COLUMN06 WHERE CONTABLE0010.COLUMN05='Custom' and SETABLE010.COLUMN05='Transaction' and SETABLE010.COLUMN06=" + id + "   ORDER BY CONTABLE0010.COLUMN04;";
                    var customForms = billdb.Query(customForm);
                    ViewData[val] = customForms;
                }
            }

            for (int i = 0; i < billGDataM.Count(); i++)
            {
                var idm = billGDataM.ElementAt(i)[2];
                var valm = billGDataM.ElementAt(i)[1];
                if (idm != null)
                {
                    var customFormMast = "SELECT CONTABLE0010.COLUMN04 FROM CONTABLE0010 INNER JOIN SETABLE010 ON CONTABLE0010.COLUMN06=SETABLE010.COLUMN06 WHERE CONTABLE0010.COLUMN05='Custom' and SETABLE010.COLUMN05='Master' and SETABLE010.COLUMN06=" + idm + " ORDER BY CONTABLE0010.COLUMN04;";
                    var customFormsMast = billdb.Query(customFormMast);
                    ViewData[valm] = customFormsMast;
                }
            }

            var customForm1 = "SELECT CONTABLE0010.COLUMN04 FROM CONTABLE0010 INNER JOIN SETABLE010 ON CONTABLE0010.COLUMN06=SETABLE010.COLUMN06 WHERE CONTABLE0010.COLUMN05='Custom' and SETABLE010.COLUMN05='Transaction'  ORDER BY CONTABLE0010.COLUMN04;";
            var customForms1 = billdb.Query(customForm1);
            ViewBag.itemslistCF = customForms1.ToList();
            var customFormM = "SELECT CONTABLE0010.COLUMN04 FROM CONTABLE0010 INNER JOIN SETABLE010 ON CONTABLE0010.COLUMN06=SETABLE010.COLUMN06 WHERE CONTABLE0010.COLUMN05='Custom' and SETABLE010.COLUMN05='Master' ORDER BY CONTABLE0010.COLUMN04;";
            var customFormsM = billdb.Query(customFormM);
            ViewBag.itemslistCFM = customFormsM.ToList();
            var operatingunits = "SELECT COLUMN02,COLUMN03 FROM CONTABLE007  WHERE ( " + Session["OPUnitWithNull"] + " or COLUMN02 in(" + Session["OPUnit"] + "))     AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07='False'  AND COLUMN03!=''   and COLUMNA13=0 ORDER BY COLUMN02";
            var operatingunitlist = billdb.Query(operatingunits);
            ViewBag.operatingunit = operatingunitlist.ToList();
            //var operatingunit = operatingunitlist.ToList();
            //List<SelectListItem> Country = new List<SelectListItem>();
            //for (int dd = 0; dd < operatingunitlist.Count(); dd++)
            //{
            //    Country.Add(new SelectListItem { Value = operatingunit[dd].COLUMN02.ToString(), Text = operatingunit[dd].COLUMN03 });
            //}
            //ViewData["OperatingUnit"] = new SelectList(Country, "Value", "Text");

            eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
            var lst = act.SETABLE010.Where(q => q.COLUMN05 == col).ToList();
            var lst1 = act.SETABLE010.Where(q => q.COLUMN05 == col1).ToList();
            var tbName = lst.Select(a => a.COLUMN04).ToList();
            var tbNameM = lst1.Select(a => a.COLUMN04).ToList();
            ViewBag.TypesList = tbName;
            ViewBag.TypesListM = tbNameM;

            return View("create");
        }

        //[HttpPost]
        //public ActionResult Create()
        //{
        //    return View("");
        //}
        [HttpPost]
        public JsonResult SaveTab(string lst, string lstM, string FormName)
        {
            int? op; int aw;
            op = Convert.ToInt32(Session["OPUnit1"]);
            aw = Convert.ToInt32(Session["AcOwner"]);
            eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();


            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            List<MYTABLE002> dd = new List<MYTABLE002>();
            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string Type = ds.Tables[0].Rows[i]["Type"].ToString();
                    string Prefix = ds.Tables[0].Rows[i]["Prefix"].ToString();
                    string StartingNo = ds.Tables[0].Rows[i]["StartingNo"].ToString();
                    string EndingNo = ds.Tables[0].Rows[i]["EndingNo"].ToString();
                    string CurrentNo = ds.Tables[0].Rows[i]["CurrentNo"].ToString();
                    string Sufix = ds.Tables[0].Rows[i]["Sufix"].ToString();
                    string OperatingUnit = ds.Tables[0].Rows[i]["OperatingUnit"].ToString();
                    string CustomForm = ds.Tables[0].Rows[i]["CustomForm"].ToString();
                    if (CustomForm == "")
                    {
                        CustomForm = "default";
                    }
                    string Override = ds.Tables[0].Rows[i]["Override"].ToString();
                    var frm = dc.CONTABLE0010.Where(q => q.COLUMN04 == Type).Select(q => q.COLUMN02).FirstOrDefault();
                    MYTABLE002 add1 = new MYTABLE002();
                    //EMPHCS701  Rajasekhar Patakota 20/7/2015 Remove Mandatory Operating Unit
                    if (OperatingUnit != "null" && OperatingUnit != "" )
                        op = Convert.ToInt32(OperatingUnit);
                    else
                        op = null;
                    //EMPHCS701  Rajasekhar Patakota 20/7/2015 Remove Mandatory Operating Unit
                    var check = act.MYTABLE002.Where(a => a.COLUMN05 == Type && a.COLUMN11 == CustomForm && a.COLUMNA03 == aw  && a.COLUMNA02==op ).ToList();
                    if (op == null)
                        check = act.MYTABLE002.Where(a => a.COLUMN05 == Type && a.COLUMN11 == CustomForm && a.COLUMNA03 == aw && a.COLUMNA02.Equals(op)).ToList();

                    if (check.Count == 0)
                    {
                        var col2 = act.MYTABLE002.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        if (col2 == null)
                        {
                            add1.COLUMN02 = 1000;
                        }
                        else
                        {
                            add1.COLUMN02 = (col2.COLUMN02 + 1);
                        }
                        add1.COLUMN03 = frm;
                        add1.COLUMN04 = "Transaction";
                        add1.COLUMN05 = Type;
                        add1.COLUMN06 = Prefix;
                        add1.COLUMN07 = StartingNo;
                        add1.COLUMN08 = EndingNo;
                        add1.COLUMN09 = CurrentNo;
                        add1.COLUMN10 = Sufix;
                        add1.COLUMN13 = op;
                        if (CustomForm == "")
                        {
                            CustomForm = "default";
                        }
                        add1.COLUMN11 = CustomForm;
                        add1.COLUMN12 = Convert.ToBoolean(Override);
                        add1.COLUMNA03 = aw;
                        add1.COLUMNA02 = op;

                        act.MYTABLE002.Add(add1);
                        act.SaveChanges();
                        Session["MessageFrom"] = "Transaction Auto Generation Created Successfully ";
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    else
                    {
                        //op = Convert.ToInt32(OperatingUnit);
                        //EMPHCS701  Rajasekhar Patakota 20/7/2015 Remove Mandatory Operating Unit
                        var lst1 = act.MYTABLE002.Where(q => q.COLUMN05 == Type && q.COLUMN11 == CustomForm && q.COLUMNA03 == aw && q.COLUMNA02==op).ToList();
                        if(op==null)
                            lst1 = act.MYTABLE002.Where(q => q.COLUMN05 == Type && q.COLUMN11 == CustomForm && q.COLUMNA03 == aw && q.COLUMNA02.Equals(op)).ToList();
                        foreach (MYTABLE002 product in lst1)
                        {
                            product.COLUMN04 = "Transaction";
                            product.COLUMN05 = Type;
                            product.COLUMN06 = Prefix;
                            product.COLUMN07 = StartingNo;
                            product.COLUMN08 = EndingNo;
                            product.COLUMN09 = CurrentNo;
                            product.COLUMN10 = Sufix;
                            product.COLUMN13 = op;
                            if (CustomForm == "")
                            {
                                CustomForm = "default";
                            }
                            product.COLUMN11 = CustomForm;
                            product.COLUMN12 = Convert.ToBoolean(Override);
                            product.COLUMNA02 = op;
                            product.COLUMNA03 = aw;
                            //act.MYTABLE002.Add(product);
                            act.SaveChanges();
                        }
                    }
                }
                Session["MessageFrom"] = "Transaction Auto Generation Updated Successfully ";
                Session["SuccessMessageFrom"] = "Success";
            }
            XmlDocument xDocM = new XmlDocument();
            xDocM.LoadXml(lstM.ToString());
            DataSet dsM = new DataSet();
            dsM.ReadXml(new XmlTextReader(new StringReader(xDocM.DocumentElement.OuterXml)));
            List<MYTABLE002> ddM = new List<MYTABLE002>();
            if (dsM.Tables.Count > 0)
            {
                for (int i = 0; i < dsM.Tables[0].Rows.Count; i++)
                {
                    string Type = dsM.Tables[0].Rows[i]["Type"].ToString();
                    string Prefix = dsM.Tables[0].Rows[i]["Prefix"].ToString();
                    string StartingNo = dsM.Tables[0].Rows[i]["StartingNo"].ToString();
                    string EndingNo = dsM.Tables[0].Rows[i]["EndingNo"].ToString();
                    string CurrentNo = dsM.Tables[0].Rows[i]["CurrentNo"].ToString();
                    string Sufix = dsM.Tables[0].Rows[i]["Sufix"].ToString();
                    string OperatingUnit = dsM.Tables[0].Rows[i]["OperatingUnit"].ToString();
                    string CustomForm = dsM.Tables[0].Rows[i]["CustomForm"].ToString();
                    if (CustomForm == "")
                    {
                        CustomForm = "default";
                    }
                    string Override = dsM.Tables[0].Rows[i]["Override"].ToString();
                    MYTABLE002 addM = new MYTABLE002();
                    var frm = dc.CONTABLE0010.Where(q => q.COLUMN04 == Type).Select(q => q.COLUMN02).FirstOrDefault();
                    //EMPHCS701  Rajasekhar Patakota 20/7/2015 Remove Mandatory Operating Unit
                    if (OperatingUnit != "null" && OperatingUnit != "")
                        op = Convert.ToInt32(OperatingUnit);
                    else
                        op = null;
                    //EMPHCS701  Rajasekhar Patakota 20/7/2015 Remove Mandatory Operating Unit
                    var check = act.MYTABLE002.Where(a => a.COLUMN05 == Type && a.COLUMN11 == CustomForm && a.COLUMNA03 == aw  && a.COLUMNA02==op ).ToList();
                    if(op==null)
                        check = act.MYTABLE002.Where(a => a.COLUMN05 == Type && a.COLUMN11 == CustomForm && a.COLUMNA03 == aw && a.COLUMNA02.Equals(op)).ToList();
                    if (check.Count == 0)
                    {
                        var col2m = act.MYTABLE002.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        if (col2m == null)
                        {
                            addM.COLUMN02 = 1000;
                        }
                        else
                        {
                            addM.COLUMN02 = (col2m.COLUMN02 + 1);
                        }
                        addM.COLUMN03 = frm;
                        addM.COLUMN04 = "Master";
                        addM.COLUMN05 = Type;
                        addM.COLUMN06 = Prefix;
                        addM.COLUMN07 = StartingNo;
                        addM.COLUMN08 = EndingNo;
                        addM.COLUMN09 = CurrentNo;
                        addM.COLUMN10 = Sufix;
                        if (CustomForm == "")
                        {
                            CustomForm = "default";
                        }
                        addM.COLUMN11 = CustomForm;
                        addM.COLUMN12 = Convert.ToBoolean(Override);
                        if (OperatingUnit != "" && OperatingUnit != null)
                            addM.COLUMN13 = op;
                        else
                            addM.COLUMN13 = null;
                        addM.COLUMNA02 = Convert.ToInt32(OperatingUnit);
                        addM.COLUMNA03 = aw;
                        act.MYTABLE002.Add(addM);
                        act.SaveChanges();
                        Session["MessageFrom"] = "Transaction Auto Generation SuccessFully Created ";
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    else
                    {
                       // op = Convert.ToInt32(OperatingUnit);
                        //EMPHCS701  Rajasekhar Patakota 20/7/2015 Remove Mandatory Operating Unit
                        var lst1M = act.MYTABLE002.Where(q => q.COLUMN05 == Type && q.COLUMN11 == CustomForm && q.COLUMNA03 == aw && q.COLUMNA02 == op).ToList();
                        if(op==null)
                            lst1M = act.MYTABLE002.Where(q => q.COLUMN05 == Type && q.COLUMN11 == CustomForm && q.COLUMNA03 == aw && q.COLUMNA02.Equals(op)).ToList();
                        foreach (MYTABLE002 product in lst1M)
                        {
                            product.COLUMN04 = "Master";
                            product.COLUMN05 = Type;
                            product.COLUMN06 = Prefix;
                            product.COLUMN07 = StartingNo;
                            product.COLUMN08 = EndingNo;
                            product.COLUMN09 = CurrentNo;
                            product.COLUMN10 = Sufix;
                            if (CustomForm == "")
                            {
                                CustomForm = "default";
                            }
                            product.COLUMN11 = CustomForm;
                            if (OperatingUnit != "" && OperatingUnit != null)
                                product.COLUMN13 = op;
                            else
                                product.COLUMN13 = null;
                            product.COLUMN12 = Convert.ToBoolean(Override);
                            product.COLUMNA02 = op;
                            product.COLUMNA03 = aw;
                            //act.MYTABLE002.Add(product);
                            act.SaveChanges();
                        }
                        Session["MessageFrom"] = "Transaction Auto Generation Updated Successfully ";
                        Session["SuccessMessageFrom"] = "Success";

                    }
                }
            }

            return this.Json("Form Saved Successfully", JsonRequestBehavior.AllowGet);
        }



        public ActionResult customForms(int id)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";
            var customForm = "SELECT CONTABLE0010.COLUMN04 FROM CONTABLE0010 INNER JOIN SETABLE010 ON CONTABLE0010.COLUMN06=SETABLE010.COLUMN06 WHERE CONTABLE0010.COLUMN05='Custom' ORDER BY CONTABLE0010.COLUMN04;";
            var billdb = Database.OpenConnectionString(connectionString, providerName);
            var customForms = billdb.Query(customForm);

            var billid = customForms.FirstOrDefault();
            ViewBag.itemslistCF = customForms;

            return Json(new { item = customForms }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult getAccount()
        {
            string col = "Transaction";
            string col1 = "Master";
            eBizSuiteAppModel.Table.Entities act = new eBizSuiteAppModel.Table.Entities();
            var lst = act.SETABLE010.Where(q => q.COLUMN05 == col).ToList();
            var lst1 = act.SETABLE010.Where(q => q.COLUMN05 == col1).ToList();
            var tbName = lst.Select(a => a.COLUMN04).ToList();
            var tbNameM = lst1.Select(a => a.COLUMN04).ToList();
            ViewBag.TypesList = tbName;
            ViewBag.TypesListM = tbNameM;
            var result = tbName;
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";
            var customForm = "SELECT CONTABLE0010.COLUMN04 FROM CONTABLE0010 INNER JOIN SETABLE010 ON CONTABLE0010.COLUMN06=SETABLE010.COLUMN06 WHERE CONTABLE0010.COLUMN05='Custom' and SETABLE010.COLUMN05='Transaction'  ORDER BY CONTABLE0010.COLUMN04";

            var billdb = Database.OpenConnectionString(connectionString, providerName);
            var customForms = billdb.Query(customForm).ToList();
            //var customForms = from t in act.SETABLE010
            //                  join f in dc.CONTABLE0010
            //                  on t.COLUMN06 equals f.COLUMN06
            //                  where f.COLUMN05 == "Custom" && t.COLUMN05 == "Transaction"
            //                  select new
            //                  {
            //                      COLUMN06 = f.COLUMN04
            //                  };
            return Json(new { item = tbName, cf = customForms }, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public ActionResult Create()
        //{
        //    cn.Open();
        //    SqlCommand cmd1 = new SqlCommand("select COLUMN02,COLUMN04 from FITABLE001 Where COLUMNA02='" + Session["OPUnit"] + "' AND  COLUMNA03='" + Session["AcOwner"] + "'", cn);
        //    SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        //    DataTable dt = new DataTable();
        //    da1.Fill(dt);
        //    cn.Close();
        //    Session["FormName"] = Request["FormName"];
        //    var fname = Session["FormName"];
        //    var formid = dc.CONTABLE0010.Where(a => a.COLUMN04 == fname).FirstOrDefault();
        //    var data = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02).ToList();
        //    var itemdata = data.Where(a => a.COLUMN11 == "Item Level" && a.COLUMN07 != "N").ToList();
        //    var tblid = itemdata.Select(a => a.COLUMN04).Distinct().ToList();
        //    SqlCommand acmd = new SqlCommand(
        //                  "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN08 From CONTABLE006  " +
        //                "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
        //                "where CONTABLE005.COLUMN03 =" + tblid[0] + " and CONTABLE006.COLUMN04 =" + tblid[0] + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level' and CONTABLE006.COLUMN07 = 'Y'", cn);
        //    cn.Open();
        //    SqlDataAdapter ada = new SqlDataAdapter(acmd);
        //    DataTable adt = new DataTable();
        //    ada.Fill(adt);
        //    acmd.ExecuteNonQuery();
        //    cn.Close();
        //    var result = new List<dynamic>();
        //    if (itemdata.Count > 0)
        //    {
        //        var obj1 = (IDictionary<string, object>)new System.Dynamic.ExpandoObject();
        //        foreach (var row1 in itemdata)
        //        {
        //            obj1.Add(row1.COLUMN06, row1.COLUMN06);
        //        }
        //        result.Add(obj1);
        //    }
        //    string[] Inlinecolumns = new string[itemdata.Count];
        //    string htmlstring = ""; string line = ""; string dvalue = ""; int gfrow = 0;
        //    List<WebGridColumn> gcol = new List<WebGridColumn>();
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        var coo = 0; int itcount = 0;
        //        for (int g = 1; g <= itemdata.Count; g++)
        //        {
        //            if (i == 0) { gfrow = 0; } else { gfrow = 1; }
        //            for (int q = coo; q < itemdata.Count; q++)
        //            {
        //                if (itcount < dt.Rows.Count)
        //                {
        //                    if ("COLUMN08" == itemdata[q].COLUMN05)
        //                    {
        //                        itemdata[q].COLUMN09 = dt.Rows[i][itcount].ToString(); itcount += 1;
        //                    }
        //                }

        //                int ctblid = Convert.ToInt32(itemdata[q].COLUMN04);
        //                var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
        //                var ctname = ctbldata.COLUMN04;
        //                var cid = (ctname + itemdata[q].COLUMN05);
        //                string id = (from DataRow dr in adt.Rows
        //                             where (string)dr["COLUMN05"] == itemdata[q].COLUMN05 && (int)dr["COLUMN04"] == (itemdata[q].COLUMN04)
        //                             select (string)dr["DataType"]).FirstOrDefault(); string giid = "a" + cid;
        //                if (gfrow == 0)
        //                {
        //                    if (q == 0)
        //                    {
        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = "",
        //                            Header = "",
        //                            Format = (item) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'    value='' checked='true'   />")
        //                        });
        //                    }
        //                    if (itemdata[q].COLUMN10 == "TextBox")
        //                    {
        //                        if (id == "INT" || id == "int" || id == "BIT" || id == "bit" || id == "DECIMAL" || id == "decimal")
        //                        {
        //                            gcol.Add(new WebGridColumn()
        //                            {
        //                                ColumnName = itemdata[q].COLUMN06,
        //                                Header = itemdata[q].COLUMN06,
        //                                Format = (item) => new HtmlString("<input  type='text'  onchange='itemChange(this);' value='" + itemdata[q].COLUMN09 + "'  id='" + cid + "' class='txtintgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' name='" + cid + "'   /><input type='text'  id='" + giid + "' style=display:none  value='" + itemdata[q].COLUMN09 + "' />")

        //                            });
        //                        }
        //                        else
        //                        {
        //                            gcol.Add(new WebGridColumn()
        //                            {
        //                                ColumnName = itemdata[q].COLUMN06,
        //                                Header = itemdata[q].COLUMN06,
        //                                Format = (item) => new HtmlString("<input  type='text'  onchange='itemChange(this);' value='" + itemdata[q].COLUMN09 + "'  id='" + cid + "' class='txtgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' name='" + cid + "'   /><input type='text'  id='" + giid + "' style=display:none  value='" + itemdata[q].COLUMN09 + "' />")

        //                            });
        //                        }
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "DropDownList")
        //                    {
        //                        List<SelectListItem> Country = new List<SelectListItem>();
        //                        var firstname = itemdata[q].COLUMN06; ViewData[firstname] = "";
        //                        if (itemdata[q].COLUMN14 == "Control Value")
        //                        {
        //                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
        //                            var dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata).ToList();
        //                            for (int dd = 0; dd < dropdata.Count; dd++)
        //                            {
        //                                Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
        //                            }
        //                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: itemdata[q].COLUMN09);
        //                        }
        //                        else if (itemdata[q].COLUMN14 == "Master Value")
        //                        {
        //                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
        //                            var tblddl = dc.CONTABLE004.Where(r => r.COLUMN02 == ddata).OrderBy(r => r.COLUMN02);
        //                            var tblName = tblddl.Select(r => r.COLUMN04).First(); SqlDataAdapter cmddl = new SqlDataAdapter();
        //                            cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "", cn);
        //                            DataTable dtdata = new DataTable();
        //                            cmddl.Fill(dtdata);
        //                            for (int dd = 0; dd < dtdata.Rows.Count; dd++)
        //                            {
        //                                Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
        //                            }
        //                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: itemdata[q].COLUMN09);
        //                        }
        //                        string dddata = "";
        //                        if (Country.Count > 0)
        //                        {
        //                            dddata += "<option value=''>--Select--</option>";
        //                        }
        //                        for (int d = 0; d < Country.Count; d++)
        //                        {
        //                            if (Country[d].Value == itemdata[q].COLUMN09)
        //                                dddata += "<option value=" + Country[d].Value + "  selected>" + Country[d].Text + "</option>";
        //                            else
        //                                dddata += "<option value=" + Country[d].Value + " >" + Country[d].Text + "</option>";
        //                        }

        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = itemdata[q].COLUMN06,
        //                            Header = itemdata[q].COLUMN06,
        //                            Format = (item) => new HtmlString("<select  class =gridddl name='" + itemdata[q].COLUMN06 + "' itemid=" + id + "    id='" + cid + "'  >" + dddata + "</select>")

        //                        });
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "TextArea")
        //                    {
        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = itemdata[q].COLUMN06,
        //                            Header = itemdata[q].COLUMN06,
        //                            Format = (item) => new HtmlString("<textarea  id=" + cid + " class='txtgridclass'      pattern='" + itemdata[q].COLUMN08 + "'   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    >" + itemdata[q].COLUMN09 + "</textarea> ")
        //                        });
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "Image")
        //                    {
        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = itemdata[q].COLUMN06,
        //                            Header = itemdata[q].COLUMN06,
        //                            Format = (item) => new HtmlString("<input type='file' id=" + cid + "   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    value=" + itemdata[q].COLUMN09 + " />")
        //                        });
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "CheckBox")
        //                    {
        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = itemdata[q].COLUMN06,
        //                            Header = itemdata[q].COLUMN06,
        //                            Format = (item) => new HtmlString("<input  id=" + cid + " class='chkclass' itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "  type='checkbox'    value=" + itemdata[q].COLUMN09 + "  />")
        //                        });
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "RadioButton")
        //                    {
        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = itemdata[q].COLUMN06,
        //                            Header = itemdata[q].COLUMN06,
        //                            Format = (item) => new HtmlString("<input class='rdoclass' itemid=" + id + "  id=" + cid + " name=" + itemdata[q].COLUMN06 + " type='radio'    value=" + itemdata[q].COLUMN09 + " />")
        //                        });
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "DatePicker")
        //                    {
        //                        gcol.Add(new WebGridColumn()
        //                        {
        //                            ColumnName = itemdata[q].COLUMN06,
        //                            Header = itemdata[q].COLUMN06,
        //                            Format = (item) => new HtmlString("<input   name=" + itemdata[q].COLUMN06 + " type='text' id=" + cid + "  readonly='true' class=date    value=" + itemdata[q].COLUMN09 + " />")
        //                        });
        //                    }
        //                }
        //                else
        //                {
        //                    if (itemdata[q].COLUMN10 == "TextBox")
        //                    {
        //                        if (id == "INT" || id == "int" || id == "BIT" || id == "bit" || id == "DECIMAL" || id == "decimal")
        //                        {
        //                            Inlinecolumns[q] = "<td  ><input type='text' id='" + cid + "' class='txtintgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' onchange='itemChange(this);' name='" + itemdata[q].COLUMN06 + "'    value='" + itemdata[q].COLUMN09 + "'   /><input type='text'  id='" + giid + "' style=display:none  value='" + itemdata[q].COLUMN09 + "' /></td>";
        //                        }
        //                        else
        //                        {
        //                            Inlinecolumns[q] = "<td  ><input type='text' id='" + cid + "' class='txtgridclass'  pattern='" + itemdata[q].COLUMN08 + "'    itemid='" + id + "' onchange='itemChange(this);' name='" + itemdata[q].COLUMN06 + "'    value='" + itemdata[q].COLUMN09 + "'   /><input type='text'  id='" + giid + "' style=display:none  value='" + itemdata[q].COLUMN09 + "' /></td>";
        //                        }
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "TextArea")
        //                    {
        //                        Inlinecolumns[q] = "<td><textarea  id=" + cid + " class='txtgridclass'      pattern='" + itemdata[q].COLUMN08 + "'   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    >" + itemdata[q].COLUMN09 + "</textarea> </td>";

        //                    }
        //                    else if (itemdata[q].COLUMN10 == "Image")
        //                    {
        //                        Inlinecolumns[q] = "<td><input type='file' id=" + cid + "   itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "    value=" + itemdata[q].COLUMN09 + "></td>";

        //                    }
        //                    else if (itemdata[q].COLUMN10 == "CheckBox")
        //                    {
        //                        Inlinecolumns[q] = "<td><input  id=" + cid + " class='chkclass' itemid=" + id + "   name=" + itemdata[q].COLUMN06 + "  type='checkbox'    value=" + itemdata[q].COLUMN09 + "  ></td>";

        //                    }
        //                    else if (itemdata[q].COLUMN10 == "DropDownList")
        //                    {
        //                        List<SelectListItem> Country = new List<SelectListItem>();
        //                        var firstname = itemdata[q].COLUMN06; ViewData[firstname] = "";
        //                        if (itemdata[q].COLUMN14 == "Control Value")
        //                        {
        //                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
        //                            var dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata).ToList();
        //                            for (int dd = 0; dd < dropdata.Count; dd++)
        //                            {
        //                                Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
        //                            }
        //                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dvalue);
        //                        }
        //                        else if (itemdata[q].COLUMN14 == "Master Value")
        //                        {
        //                            int ddata = Convert.ToInt32(itemdata[q].COLUMN15);
        //                            var tblddl = dc.CONTABLE004.Where(r => r.COLUMN02 == ddata).OrderBy(r => r.COLUMN02);
        //                            var tblName = tblddl.Select(r => r.COLUMN04).First();
        //                            SqlDataAdapter cmddl = new SqlDataAdapter();
        //                               cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "", cn);
        //                            DataTable dtdata = new DataTable();
        //                            cmddl.Fill(dtdata);
        //                            for (int dd = 0; dd < dtdata.Rows.Count; dd++)
        //                            {
        //                                Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
        //                            }
        //                            ViewData[firstname] = new SelectList(Country, "Value", "Text", selectedValue: dvalue).ToString();
        //                        }
        //                        string dddata = "";
        //                        if (Country.Count > 0)
        //                        {
        //                            dddata += "<option value=''>--Select--</option>";
        //                        }
        //                        for (int d = 0; d < Country.Count; d++)
        //                        {
        //                            if (Country[d].Value == itemdata[q].COLUMN09)
        //                                dddata += "<option value=" + Country[d].Value + " selected>" + Country[d].Text + "</option>";
        //                            else
        //                                dddata += "<option value=" + Country[d].Value + ">" + Country[d].Text + "</option>";
        //                        }
        //                        Inlinecolumns[q] = "<td  >" + "<select  class =gridddl   name='" + itemdata[q].COLUMN06 + "' itemid=" + id + "    id='" + cid + "'  >" + dddata + "</select>" + "</td>";
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "RadioButton")
        //                    {
        //                        Inlinecolumns[q] = "<td><input class='rdoclass' itemid=" + id + "  id=" + cid + " name=" + itemdata[q].COLUMN06 + " type='radio'    value=" + itemdata[q].COLUMN09 + " ></td>";
        //                    }
        //                    else if (itemdata[q].COLUMN10 == "DatePicker")
        //                    {
        //                        Inlinecolumns[q] = "<td   ><input   name=" + itemdata[q].COLUMN06 + " id=" + cid + "  type='text' readonly='true' class=date    value=" + itemdata[q].COLUMN09 + " ></td>";
        //                    }
        //                    if (q == 0)
        //                    {
        //                        htmlstring += "<tr  ><td  ><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'    value='' checked='true'  /></td> " + Inlinecolumns[q];
        //                    }
        //                    else if (q == itemdata.Count - 1)
        //                    {
        //                        htmlstring += Inlinecolumns[q] + "</tr>";
        //                    }
        //                    else
        //                    {
        //                        htmlstring += Inlinecolumns[q];
        //                    }
        //                }
        //                coo = coo + 1;
        //                if (itcount <= dt.Columns.Count)
        //                {
        //                    if (dt.Columns[itcount - 1].ColumnName == itemdata[q].COLUMN05)
        //                        itemdata[q].COLUMN09 = dt.Rows[0][itcount - 1].ToString();
        //                }
        //                break;
        //            }
        //        }
        //    }
        //    ViewBag.cols = gcol;
        //    var grid1 = new WebGrid(null, canPage: false, canSort: false);
        //    grid1 = new WebGrid(result, canPage: false, canSort: false);
        //    var htmlstring1 = grid1.GetHtml(tableStyle: "webgrid-table",
        //    headerStyle: "webgrid-header",
        //    footerStyle: "webgrid-footer",
        //    alternatingRowStyle: "webgrid-alternating-row",
        //    rowStyle: "webgrid-row-style",
        //    htmlAttributes: new { id = "grdData", style = "width:500px" },
        //    columns: gcol);

        //    return Json(new { grid = htmlstring1.ToHtmlString(), remain = htmlstring }, JsonRequestBehavior.AllowGet);


        //}


        //Transaction Item Details

        public ActionResult TransactionItemDetails(string Item, string ItemID)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";
            var sql = "";
            List<string> colNM = new List<string>();
            //List<string> TDataC = new List<string>();
            List<WebGridColumn> TDataC = new List<WebGridColumn>();
            string tbl = "PUTABLE004"; string col = "COLUMN03";
            if (Item == "22393") { tbl = "PUTABLE004"; }
            else if (Item == "22394") { tbl = "SATABLE008"; col = "COLUMN04"; }
            else if (Item == "22395") { tbl = "FITABLE015"; }

            var tblddl = dc.CONTABLE004.Where(q => q.COLUMN04 == tbl).OrderBy(q => q.COLUMN02).FirstOrDefault();
            var tbleID = Convert.ToInt32(tblddl.COLUMN02);
            var formID = dc.CONTABLE006.Where(a => a.COLUMN04 == tbleID && a.COLUMN07 == "Y").OrderBy(q => q.COLUMN01).Take(4).ToList();
            var formid = formID.Where(a => a.COLUMN02 != null).FirstOrDefault().COLUMN03;
            var formname = dc.CONTABLE0010.Where(q => q.COLUMN02 == formid).FirstOrDefault().COLUMN04;
            var colLST = formID.Select(q => q.COLUMN06).ToList();
            foreach (string dtr in colLST)
            {
                colNM.Add(dtr);
            }
            var billsql = "  select COLUMN02 from " + tbl + " where " + col + " in (" + ItemID + ")";
            var billdb = Database.OpenConnectionString(connectionString, providerName);
            var billGData = billdb.Query(billsql);
            var billid = billGData.FirstOrDefault();
            string colids = null;
            for (int i = 0; i < billGData.Count(); i++)
            {
                colids += billGData.ElementAt(i)[0] + ",";
            }
            if (billid == null)
            {
                sql = "select top(1) 0 AA ,0 as [" + colNM[0] + "],0 as [" + colNM[1] + "],0 as [" + colNM[2] + "],0 as [" + colNM[3] + "] from " + tbl + " ";
            }
            else
            {
                colids = colids.TrimEnd(',');
                sql = "select COLUMN02 AA ,COLUMN03 as [" + colNM[0] + "],COLUMN04 as [" + colNM[1] + "],COLUMN05 as [" + colNM[2] + "],COLUMN06 as [" + colNM[3] + "] from " + tbl + " where COLUMN02 in(" + colids + ")";
            }
            var dbs = Database.OpenConnectionString(connectionString, providerName);
            var GData = dbs.Query(sql);
            var grid = new WebGrid(GData, canPage: false, canSort: false);
            TDataC.Add(new WebGridColumn()
            {
                ColumnName = "AA",
                Header = "ID",
                Format = (item) => new HtmlString("<a href='/FormBuilding/Edit?FormName=" + formname + "&ide=" + item.AA + "'>" + item.AA + "</a>")
            });
            foreach (string dtr in colNM)
            {
                TDataC.Add(new WebGridColumn()
                {
                    ColumnName = dtr,
                    Header = dtr
                });
            }
            var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
         headerStyle: "webgrid-header",
         footerStyle: "webgrid-footer",
         alternatingRowStyle: "webgrid-alternating-row",
         rowStyle: "webgrid-row-style",
                             htmlAttributes: new { id = "grdfData" },
                             columns: TDataC);
            return Json(new { grid = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        }

    }
}
