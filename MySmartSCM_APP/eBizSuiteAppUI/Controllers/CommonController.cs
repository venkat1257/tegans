﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using MvcMenuMaster.Models;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using System.IO;
using System.ComponentModel.DataAnnotations;
using eBizSuiteUI.Controllers;
using eBizSuiteAppModel.Table;
using WebMatrix.Data;
using System.Web.Helpers;
using MeBizSuiteAppUI.Controllers;
using System.Web.UI;
using iTextSharp.text;
using System.Text;
using eBizSuiteAppDAL.classes;
using eBizSuiteAppUI.Models;
using Microsoft.Reporting.WebForms;
using System.Diagnostics;
using GemBox.Document;
using GemBox.Document.Tables;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using eBizSuiteAppBAL.Fombuilding;
using System.Data.SqlTypes;
using System.Globalization;

namespace MeBizSuiteAppUI.Controllers
{
    public class CommonController : Controller
    {
        //

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        // GET: /Common/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult NewRow(string Edit, string micode, string Payee)
        {
            try
            {
                System.Web.HttpContext.Current.Session["FormName"] = Request["FormName"];
                var fname = System.Web.HttpContext.Current.Session["FormName"];
                var formid = dc.CONTABLE0010.Where(a => a.COLUMN04 == fname).FirstOrDefault();
                var data = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02).ToList();
                var itemdata = data.Where(a => a.COLUMN11 == "Item Level" && a.COLUMN07 != "N").ToList();
                var tblid = itemdata.Select(a => a.COLUMN04).Distinct().ToList();
                string tblids = "";
                for (int j = 0; j < tblid.Count; j++)
                {
                    if (j == 0)
                        tblids = tblid[j].ToString();
                    else
                        tblids += "," + tblid[j].ToString();
                }
                SqlCommand acmd = new SqlCommand();
				//EMPHCS1784	Job Order for IN Process 15/7/2016
                //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                if (formid.COLUMN02 == 1251 || formid.COLUMN02 == 1272 || formid.COLUMN02 == 1273 || formid.COLUMN02 == 1275 || formid.COLUMN02 == 1276 || formid.COLUMN02 == 1277 || formid.COLUMN02 == 1353 || formid.COLUMN02 == 1354 || formid.COLUMN02 == 1355 || formid.COLUMN02 == 1328 || formid.COLUMN02 == 1329 || formid.COLUMN02 == 1330 || formid.COLUMN02 == 1330 || formid.COLUMN02 == 1285 || formid.COLUMN02 == 1286 || formid.COLUMN02 == 1586|| formid.COLUMN02 == 1605)
                {
                    acmd = new SqlCommand(
                       "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09,CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15 From CONTABLE006  " +
                    "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                    "where CONTABLE005.COLUMN03 in (" + tblids + ") and CONTABLE006.COLUMN04 in (" + tblids + ") and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level' and CONTABLE006.COLUMN07 = 'Y' ORDER BY CAST(CONTABLE006.COLUMN13 AS INT)  ", cn);
                }
                else
                {
                    if (Edit == "1")
                    {
                        acmd = new SqlCommand("Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09,CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15 From CONTABLE006  " +
                            "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                            "where CONTABLE005.COLUMN03 in (" + tblids + ") and CONTABLE006.COLUMN04 in (" + tblids + ") and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level' and CONTABLE006.COLUMN07 = 'Y' ORDER BY CAST(CONTABLE006.COLUMN13 AS INT) ", cn);
                    }
                    else
                    {
                        acmd = new SqlCommand("Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, CONTABLE006.COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09,CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15 From CONTABLE006  " +
                            "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                            "where CONTABLE005.COLUMN03 in (" + tblids + ") and CONTABLE006.COLUMN04 in (" + tblids + ") and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level' and CONTABLE006.COLUMN07 = 'Y'", cn);
                    }

                } cn.Open();
                SqlDataAdapter ada = new SqlDataAdapter(acmd);
                DataTable adt = new DataTable();
                ada.Fill(adt);
                acmd.ExecuteNonQuery();
                cn.Close(); int ctblid = 0;
                string[] Inlinecolumns = new string[adt.Rows.Count];
                string htmlstring = "";
                var id = ""; var colid = ""; var mandatory = ""; var name = ""; var value = ""; var ctype = ""; int frmid = 0;
                var ddltype = ""; var ddlval = ""; string hiddenfield = null;
                for (int g = 0; g < adt.Rows.Count; g++)
                {
                    //var id = (from DataRow dr in adt.Rows
                    //          where (string)dr["COLUMN05"] == itemdata[g].COLUMN05 && (int)dr["COLUMN04"] == (itemdata[g].COLUMN04)
                    //          select (string)dr["DataType"]).FirstOrDefault();
                    //ctblid = Convert.ToInt32(adt.Rows[g][9]);
                    ctblid = Convert.ToInt32(adt.Rows[g]["COLUMN04"]);
                    var ctbldata = dc.CONTABLE004.Where(a => a.COLUMN02 == ctblid).FirstOrDefault();
                    var ctname = ctbldata.COLUMN04;
                    //itemdata[g].COLUMN05 = (ctname + adt.Rows[g][0].ToString());
                    id = adt.Rows[g]["DataType"].ToString();
                    colid = (ctname + adt.Rows[g]["COLUMN05"]);
                    mandatory = adt.Rows[g]["COLUMN08"].ToString();
                    name = adt.Rows[g]["COLUMN06"].ToString();
                    value = adt.Rows[g]["COLUMN09"].ToString();
                    ctype = adt.Rows[g]["COLUMN10"].ToString();
                    ddltype = adt.Rows[g]["COLUMN14"].ToString();
                    ddlval = adt.Rows[g]["COLUMN15"].ToString();
                    frmid = Convert.ToInt32(adt.Rows[g]["COLUMN03"].ToString());
                    if (ctype == "TextBox")
                    {
                        if (((int)System.Web.HttpContext.Current.Session["id"] == 1285 || (int)System.Web.HttpContext.Current.Session["id"] == 1378) && (colid == "MATABLE007COLUMN41" || colid == "PRTABLE002COLUMN03"))
                        {
                            int itemcode = Convert.ToInt32(micode.Substring(2));
                            int mitcode = itemcode + 1;
                            string itcode = null;
                            if (colid == "MATABLE007COLUMN41") { itcode = ("MI" + mitcode); } else { itcode = ("MS" + mitcode); }
                            Inlinecolumns[g] = "<td><input type='text' id='" + colid + "' class='txtgridclass'  pattern='" + mandatory + "'    itemid='" + id + "' name='" + name + "'    value='" + itcode + "' ></td>";
                        }
                        else
                        {
                            if (Edit != "1")
                            {
                                if (colid == "PUTABLE002COLUMN24" || colid == "PUTABLE006COLUMN12" || colid == "SATABLE006COLUMN25" || colid == "SATABLE010COLUMN14") { hiddenfield = "<input type='hidden'  id='a" + colid + "'    />"; }
                                //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                                if ((frmid == 1251 && (colid == "PUTABLE002COLUMN12" || colid == "PUTABLE002COLUMN13")) || (frmid == 1275 && (colid == "SATABLE006COLUMN12" || colid == "SATABLE006COLUMN13")) || (frmid == 1273 && (colid == "PUTABLE006COLUMN07")) || (frmid == 1277 && (colid == "SATABLE010COLUMN08")) || (frmid == 1275 && (colid == "SATABLE006COLUMN06")))
                                { }
                                else
                                {
                                    if (adt.Rows[g]["DataType"].ToString() == "INT" || adt.Rows[g]["DataType"].ToString() == "int" || adt.Rows[g]["DataType"].ToString() == "BIT" || adt.Rows[g]["DataType"].ToString() == "bit" || adt.Rows[g]["DataType"].ToString() == "DECIMAL" || adt.Rows[g]["DataType"].ToString() == "decimal")
                                    {
                                        Inlinecolumns[g] = "<td><input type='text' id='" + colid + "' class='txtintgridclass'  pattern='" + mandatory + "'    itemid='" + id + "' name='" + name + "'    value='" + value + "' >" + hiddenfield + "</td>";
                                    }
                                    else
                                    {
                                        Inlinecolumns[g] = "<td><input type='text' id='" + colid + "' class='txtgridclass'  pattern='" + mandatory + "'    itemid='" + id + "' name='" + name + "'    value='" + value + "' >" + hiddenfield + "</td>";
                                    }
                                }
                            }
                            else
                            {
                                if (colid == "PUTABLE002COLUMN24" || colid == "PUTABLE006COLUMN12" || colid == "SATABLE006COLUMN25" || colid == "SATABLE010COLUMN14") { hiddenfield = "<input type='hidden'  id='a" + colid + "'    />"; }
                                //EMPHCS684 done by srinivas 7/17/2015 
                                if ((frmid == 1251 && (colid == "PUTABLE002COLUMN12" || colid == "PUTABLE002COLUMN13")) || (frmid == 1275 && (colid == "SATABLE006COLUMN12" || colid == "SATABLE006COLUMN13")) || (frmid == 1273 && (colid == "PUTABLE006COLUMN07")) || (frmid == 1277 && (colid == "SATABLE010COLUMN08")))
                                { }
                                else
                                {
                                    if (adt.Rows[g]["DataType"].ToString() == "INT" || adt.Rows[g]["DataType"].ToString() == "int" || adt.Rows[g]["DataType"].ToString() == "BIT" || adt.Rows[g]["DataType"].ToString() == "bit" || adt.Rows[g]["DataType"].ToString() == "DECIMAL" || adt.Rows[g]["DataType"].ToString() == "decimal")
                                    {
                                        Inlinecolumns[g] = "<td><input type='text' id='" + colid + "' class='txtintgridclass'  pattern='" + mandatory + "'    itemid='" + id + "' name='" + name + "'    value='" + value + "' >" + hiddenfield + "</td>";
                                    }
                                    else
                                    {
                                        Inlinecolumns[g] = "<td><input type='text' id='" + colid + "' class='txtgridclass'  pattern='" + mandatory + "'    itemid='" + id + "' name='" + name + "'    value='" + value + "' >" + hiddenfield + "</td>";
                                    }
                                }
                            }

                        }
                    }
                    else if (ctype == "TextArea")
                    {
                        Inlinecolumns[g] = "<td><textarea  id=" + colid + " class='txtgridclass txtarealength'      pattern='" + mandatory + "'   itemid=" + id + "   name=" + name + "    >" + value + "</textarea></td>";

                    }
                    else if (ctype == "Image")
                    {
                        Inlinecolumns[g] = "<td><input type='file' id=" + colid + "   itemid=" + id + "   name=" + name + "    value=" + value + "></td>";

                    }
                    else if (ctype == "CheckBox")
                    {
                        Inlinecolumns[g] = "<td><input  id=" + colid + " class='chkclass' itemid=" + id + "   name=" + name + "  type='checkbox'    value=" + value + "  ></td>";

                    }
                    else if (ctype == "DropDownList")
                    {
                        List<SelectListItem> Country = new List<SelectListItem>();
                        if (ddltype == "Control Value")
                        {
                            int ddata = Convert.ToInt32(ddlval);
                            List<string> Oper = System.Web.HttpContext.Current.Session["OPUnit"].ToString().Split(',').ToList<string>();
                            var acOW = Convert.ToInt32(System.Web.HttpContext.Current.Session["AcOwner"]);
                            List<MATABLE002> dropdata = new List<MATABLE002>();
                            //if (ddata == 11130 || ddata == 11131 || ddata == 11114 || ddata == 11127 || ddata == 11124 || ddata == 11125 ||
                            //    ddata == 11126 || ddata == 11122 || ddata == 11123 || ddata == 11132 || ddata == 11128 || ddata == 11133 || ddata == 11134 || ddata == 11135)
                            //    dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata).ToList();
                            //else
                            //{
							//EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
                            dropdata = dc.MATABLE002.Where(a => a.COLUMN03 == ddata && (a.COLUMNA03 == acOW || a.COLUMNA03 == null) && a.COLUMNA13 == false).ToList();
                            if (string.IsNullOrEmpty(System.Web.HttpContext.Current.Session["OPUnitstatus"] as string))
                                dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                            else
                                dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString())).ToList();
                            //else
                            //    dropdata = dropdata.Where(a => Oper.Contains(a.COLUMNA02.ToString())).ToList();
                            //}
                            for (int dd = 0; dd < dropdata.Count; dd++)
                            {
                                Country.Add(new SelectListItem { Value = dropdata[dd].COLUMN02.ToString(), Text = dropdata[dd].COLUMN04 });
                            }
                            var firstname = name;
                            ViewData[firstname] = new SelectList(Country, "Value", "Text");
                        }
                        else if (ddltype == "Master Value")
                        {
                            int ddata = Convert.ToInt32(ddlval);
                            var tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                            var tblName = tblddl.Select(q => q.COLUMN04).First();
                            SqlDataAdapter cmddl = new SqlDataAdapter();
                            if (Edit != "1")
                            {
                                string[] edit = Edit.Split(','); string editvalues = "";
                                for (int e = 0; e < edit.Length; e++)
                                {
                                    if (e == 0)
                                        editvalues = edit[e].ToString();
                                    else
                                        editvalues += " and COLUMN02!=" + edit[e] + "";
                                }
                                if (editvalues == "0") editvalues = "''";
                                if (frmid == 1283 && adt.Rows[g]["COLUMN15"].ToString() == "110008817" && adt.Rows[g]["COLUMN05"].ToString() == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where COLUMN05!='ITTY008' and   (" + System.Web.HttpContext.Current.Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + System.Web.HttpContext.Current.Session["AcOwner"] + "'", cn);
                                else if (frmid == 1286 && adt.Rows[g]["COLUMN15"].ToString() == "110008817" && adt.Rows[g]["COLUMN05"].ToString() == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (frmid == 1286 && adt.Rows[g]["COLUMN15"].ToString() == "110008817" && adt.Rows[g]["COLUMN05"].ToString() == "COLUMN29")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where COLUMNA03='" + Session["AcOwner"] + "' AND ISNULL(COLUMNA13,0) = 0  AND ISNULL(COLUMN78,0) = 1 ", cn);
                                else if (frmid == 1363 && Payee == "22492" && colid == "FITABLE022COLUMN07")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 COLUMN04 FROM MATABLE010 where COLUMN30 !=1   AND    (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                                else if (frmid == 1363 && Payee == "22305" && colid == "FITABLE022COLUMN07")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 FROM SATABLE001 where (COLUMN22=22285 or COLUMN22=null or COLUMN22='')   AND   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                                else if (frmid == 1363 && Payee == "22334" && colid == "FITABLE022COLUMN07")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 FROM SATABLE001 where COLUMN22=22286    AND    (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                                else if (tblName == "MATABLE013")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)and COLUMN15='False' and COLUMNA13='False'", cn);
                                else if (tblName == "SATABLE001" && frmid == 1363)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where COLUMN22 in (22285,22286)    AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (tblName == "SATABLE002" && (frmid == 1386 || frmid == 1414))
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                else if (tblName == "FITABLE001" && (frmid == 1363 || frmid == 1386) && adt.Rows[g]["COLUMN05"].ToString() == "COLUMN03")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN07!=22266) AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL)", cn);
                                else if (tblName == "MATABLE008")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from " + tblName + "", cn);
                                else if (tblName == "PRTABLE001" && frmid == 1379)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                else if (tblName == "FITABLE001" && frmid == 1293)
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07=22344  AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL)", cn);
                                //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row

                                else if (tblName == "MATABLE007")
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                else
                                    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                DataTable dtdata = new DataTable();
                                cmddl.Fill(dtdata);
                                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                {
                                    Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                }
                                ViewData[name] = new SelectList(Country, "Value", "Text");
                            }
                            else
                            {
                                if (frmid == 1272 && adt.Rows[g]["COLUMN05"] == "COLUMN03")
                                {
                                    SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN03 from PUTABLE002   where     " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata2 = new DataTable();
                                    cmdd2.Fill(dtdata2); string dcol = "";
                                    for (int dd2 = 0; dd2 < dtdata2.Rows.Count; dd2++)
                                    {
                                        if (dd2 == 0)
                                            dcol = dtdata2.Rows[dd2][0].ToString();
                                        else
                                            dcol += " or COLUMN02=" + dtdata2.Rows[dd2][0].ToString();
                                    }
                                    if (dcol != "")
                                    {
                                        //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                                        if (tblName == "MATABLE007")
                                            cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                        else
                                            cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN02=" + dcol + "   AND COLUMN02=" + dcol + "   AND  " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                        DataTable dtdta = new DataTable();
                                        cmddl.Fill(dtdta);
                                        for (int dd = 0; dd < dtdta.Rows.Count; dd++)
                                        {
                                            Country.Add(new SelectListItem { Value = dtdta.Rows[dd]["COLUMN02"].ToString(), Text = dtdta.Rows[dd]["COLUMN04"].ToString() });
                                        }
                                    }
                                    ViewData[name] = new SelectList(Country, "Value", "Text");
                                }
                                else if (frmid == 1251 || frmid == 1379 || frmid == 1374 || frmid == 1272 || frmid == 1273 || frmid == 1274)
                                {
                                    ddata = Convert.ToInt32(adt.Rows[g]["COLUMN15"].ToString());
                                    tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                    tblName = tblddl.Select(q => q.COLUMN04).First(); cmddl = new SqlDataAdapter();
                                    //if ((frmid == 1251 || frmid == 1273) && adt.Rows[g]["COLUMN15"].ToString() == "110008817" && (adt.Rows[g]["COLUMN05"].ToString() == "COLUMN03" || adt.Rows[g]["COLUMN05"].ToString() == "COLUMN04"))
                                    //    cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where COLUMN05!='ITTY008' ", cn);
                                    if (tblName == "PRTABLE001" && frmid == 1379)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "  where    " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by COLUMN02 desc", cn);
                                    else if (tblName == "MATABLE013")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and COLUMN15='False' and COLUMNA13='False'", cn);
                                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[name] = new SelectList(Country, "Value", "Text");
                                }
                                else if (frmid == 1275 || frmid == 1276 || frmid == 1278 || frmid == 1277)
                                {
                                    ddata = Convert.ToInt32(adt.Rows[g]["COLUMN15"].ToString());
                                    tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                    tblName = tblddl.Select(q => q.COLUMN04).First(); cmddl = new SqlDataAdapter();

                                    if (tblName == "SATABLE009")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN19 in (Select COLUMN02 from MATABLE009 where COLUMN04='Sales Order' )  AND   " + Session["OPUnitWithNull"] + "   AND COLUMNA03='" + Session["AcOwner"] + "' order by Column02 desc", cn);
                                    else if (tblName == "MATABLE013")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and COLUMN15='False' and COLUMNA13='False'", cn);
                                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "   where    (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[name] = new SelectList(Country, "Value", "Text");
                                }
                                else if (frmid == 1283 || frmid == 1284 || frmid == 1286 || frmid == 1287 || frmid == 1288)
                                {
                                    ddata = Convert.ToInt32(adt.Rows[g]["COLUMN15"].ToString());
                                    tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                    tblName = tblddl.Select(q => q.COLUMN04).First(); cmddl = new SqlDataAdapter();

                                    if (frmid == 1286 && adt.Rows[g]["COLUMN15"].ToString() == "110008817" && adt.Rows[g]["COLUMN05"].ToString() == "COLUMN03")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (tblName == "MATABLE013")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and COLUMN15='False' and COLUMNA13='False'", cn);
                                    else if (frmid == 1283 && adt.Rows[g]["COLUMN15"].ToString() == "110008817" && adt.Rows[g]["COLUMN05"].ToString() == "COLUMN03")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04  from " + tblName + " where COLUMN05!='ITTY008'    AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "    WHERE  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[name] = new SelectList(Country, "Value", "Text");
                                }
                                else
                                {
                                    ddata = Convert.ToInt32(adt.Rows[g]["COLUMN15"].ToString());
                                    tblddl = dc.CONTABLE004.Where(q => q.COLUMN02 == ddata).OrderBy(q => q.COLUMN02);
                                    tblName = tblddl.Select(q => q.COLUMN04).First(); cmddl = new SqlDataAdapter();
                                    if (tblName == "SATABLE002" && frmid == 1260)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + " where COLUMN22='True'    AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    else if (tblName == "MATABLE013")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where (COLUMN13 ='true' or COLUMN13 ='1') AND (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and COLUMN15='False' and COLUMNA13='False'", cn);
                                    else if (frmid == 1263 && Payee == "22492")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN06 FROM MATABLE010 where COLUMN30 !=1   AND    (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                                    else if (frmid == 1263 && Payee == "22305")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 FROM SATABLE001 where (COLUMN22=22285 or COLUMN22=null or COLUMN22='')   AND   (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                                    else if (frmid == 1263 && Payee == "22334")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 FROM SATABLE001 where COLUMN22=22286    AND    (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'  and COLUMNA13='False'", cn);
                                    else if (tblName == "FITABLE001" && frmid == 1293)
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + " where COLUMN07=22344 AND ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL)", cn);
                                    else if (tblName == "SATABLE002" || tblName == "SATABLE001")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from " + tblName + "    WHERE  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                                    else if (tblName == "MATABLE007")
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN47='False' and COLUMNA13='False'", cn);
                                    else
                                        cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from " + tblName + "    WHERE  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'", cn);
                                    DataTable dtdata = new DataTable();
                                    cmddl.Fill(dtdata);
                                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                                    {
                                        Country.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                                    }
                                    ViewData[name] = new SelectList(Country, "Value", "Text");
                                }
                            }
                        }
                        string dddata = "";
                        dddata += "<option value=''>--Select--</option>";
                        for (int d = 0; d < Country.Count; d++)
                        {
                            if (frmid == 1363 && colid == "FITABLE022COLUMN03" && (Payee == "22305" || Payee == "22334") && Country[d].Value == "2000")
                                dddata += "<option value=" + Country[d].Value + " selected=2000>" + Country[d].Text + "</option>";
                            else if (frmid == 1363 && colid == "FITABLE022COLUMN03" && (Payee == "22492") && Country[d].Value == "4113")
                                dddata += "<option value=" + Country[d].Value + " selected=4113>" + Country[d].Text + "</option>";
                            else if (frmid == 1293 && colid == "FITABLE021COLUMN03" && (Payee == "4113") && Country[d].Value == "4113")
                                dddata += "<option value=" + Country[d].Value + " selected=4113>" + Country[d].Text + "</option>";
                            else
                                dddata += "<option value=" + Country[d].Value + ">" + Country[d].Text + "</option>";
                        }
                        //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Bill Edit and add new row feild
                        if (colid == "PUTABLE006COLUMN03" && Edit != "1")
                        {
                            //Inlinecolumns[g] = "<td>" + "<select  name='" + name + "' itemid='" + mandatory + "'  class =gridddl disabled=disabled     id='" + colid + "'  >" + dddata + "</select>" + "</td>";COMMMENTED BY GNANESH
                        }
                        else
                            Inlinecolumns[g] = "<td>" + "<select  name='" + name + "' itemid='" + mandatory + "'  class =gridddl     id='" + colid + "'  >" + dddata + "</select>" + "</td>";
                    }
                    else if (ctype == "RadioButton")
                    {
                        Inlinecolumns[g] = "<td><input class='rdoclass' itemid=" + id + "  id=" + colid + " name=" + name + " type='radio'    value=" + value + " ></td>";
                    }
                    else if (ctype == "DatePicker")
                    {
                        Inlinecolumns[g] = "<td   ><input  id=" + colid + "  name='" + name + "' type='text' readonly='true' class=date    value=" + DateTime.Now.ToShortDateString() + " ></td>";
                    }
                    if (g == 0)
                    {
                        if (Edit == "1")
                        {
                            if ((frmid == 1251 || frmid == 1379 || frmid == 1374 || frmid == 1375 || frmid == 1273 || frmid == 1275 || frmid == 1277 || frmid == 1283 || frmid == 1380 || frmid == 1381))
                                //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Bill Edit and add new row feild
                                htmlstring += "<tr><div  ><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></div></td><td><input type='text' id='UPCSearch' class=gridWH  pattern=''   itemid='' name=''    value=''' ></td>" + Inlinecolumns[g];
                            //EMPHCS684 done by srinivas 7/17/2015 
                            else
                                htmlstring += "<tr><div  ><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></div></td>" + Inlinecolumns[g];

                        }
                        else
                        {
                            if ((frmid == 1251 || frmid == 1379 || frmid == 1374 || frmid == 1375 || frmid == 1273 || frmid == 1275 || frmid == 1277 || frmid == 1283  || frmid == 1380 || frmid == 1381))
                                htmlstring += "<tr><div  ><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></div></td><td><input type='text' id='UPCSearch' class=gridWH  pattern=''   itemid='' name=''    value=''' ></td>" + Inlinecolumns[g];
                            else
                                htmlstring += "<tr><div  ><td><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'   value='' checked='true'/></div></td>" + Inlinecolumns[g];
                        }
                    }
                    else if (g == adt.Rows.Count - 1)
                    {
                        htmlstring += Inlinecolumns[g] + "</tr>";
                    }
                    else
                    {
                        htmlstring += Inlinecolumns[g];
                    }
                }

                return this.Json(new { Data = htmlstring }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        [HttpPost]
        public ActionResult SaveNewRow(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["GridData"] = ds;
                var saveform = Request.QueryString["FormName"];
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == saveform).ToList();
                var fname = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                Session["FormName"] = saveform;
                Session["id"] = fname;
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

        //Filter by Status 
        public ActionResult SortByStatus(string items)
        {
            try
            {
                if (items != "")
                {
                    Session["SortByStatus"] = items;
                    Session["SFormid"] = Request["FormName"];
                }
                else
                    Session["SortByStatus"] = null;
                Session["Reload"] = "Reload";
                return RedirectToAction("Info", new { FormName = Request["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult GetTaxAmount(string TaxType, string TaxCal, string FormName)
        {
            try
            {
                string fid = "1275";
                if (FormName != null && FormName != "")
                    fid = dc.CONTABLE0010.Where(q => q.COLUMN04 == FormName.TrimEnd()).OrderBy(a => a.COLUMN02).FirstOrDefault().COLUMN02.ToString();
                cn.Open();
                SqlCommand cmd = new SqlCommand("GetTaxAmount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TaxType", TaxType));
                cmd.Parameters.Add(new SqlParameter("@FormID", fid));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                cn.Close();
                var Tax = "";
                if (dt1.Rows.Count > 0)
                {
                    Tax = dt1.Rows[0]["COLUMN07"].ToString();
                }

                return Json(new { Tax = Tax, TaxCal = TaxCal }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

        public String TransactionNoGenerate(string FormName, int FormId, string opunit)
        {
            try
            {
                var acID = (int?)System.Web.HttpContext.Current.Session["AcOwner"];
                var OPUnit = System.Web.HttpContext.Current.Session["OPUnit1"];
                if (opunit != "") OPUnit = opunit;
                System.Web.HttpContext.Current.Session[FormId + "TranOPUnit"] = OPUnit;
                var eid = (int?)System.Web.HttpContext.Current.Session["eid"];
                SqlCommand cmd = new SqlCommand("usp_Proc_TransNoGenerate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Formid", FormId);
                //cmd.Parameters.AddWithValue("@TblID", tblID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", acID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string item = "", trancol = "", Override = "";
                if (dt.Rows.Count > 0 && dt.Columns.Contains("PO"))
                { item = dt.Rows[0]["PO"].ToString(); trancol = dt.Rows[0]["TransColumn"].ToString(); Override = dt.Rows[0]["Override"].ToString(); }
                return item;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                System.Web.HttpContext.Current.Session["MessageFrom"] = msg + " Due to " + ex.Message;
                System.Web.HttpContext.Current.Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }

        //EMPHCS1106 by gnaneshwar on 7/9/2015 dispaying default operating unit in all forms
        public ActionResult GetVendorAddres(int vendorId)
        {

            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_PUR_TP_PO_VendorAdress_DATA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@vendorId", vendorId));
                cmd.Parameters.Add(new SqlParameter("@accntId", System.Web.HttpContext.Current.Session["AcOwner"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                cn.Close();

                var Addressee = "";
                var Address1 = "";
                var Address2 = "";
                var Address3 = "";
                var City = "";
                var State = "";
                var Zip = "";
                var Country = "";
                var Operating_Unit = "";
                var Subsidary = "";
                var Classification = "";
                var Department = "";

                var HostId = "";
                var HostIntId = "";
                var HostExtId = "";

                var DefaultBilling = "";
                var DefaultShipping = "";

                var Payment_Terms = "";
                var vendorOU = "";
                var Payment_Mode = "";

                if (dt1.Rows.Count > 0)
                {
                    Addressee = dt1.Rows[0]["COLUMN06"].ToString();

                    Address1 = dt1.Rows[0]["COLUMN07"].ToString();
                    Address2 = dt1.Rows[0]["COLUMN08"].ToString();
                    Address3 = dt1.Rows[0]["COLUMN09"].ToString();
                    City = dt1.Rows[0]["COLUMN10"].ToString();

                    State = dt1.Rows[0]["COLUMN11"].ToString();

                    Zip = dt1.Rows[0]["COLUMN12"].ToString();
                    Country = dt1.Rows[0]["COLUMN16"].ToString();

                    Operating_Unit = dt1.Rows[0]["COLUMN21"].ToString();
                    Subsidary = dt1.Rows[0]["COLUMN22"].ToString();
                    Classification = dt1.Rows[0]["COLUMN23"].ToString();
                    Department = dt1.Rows[0]["COLUMN24"].ToString();

                    HostId = dt1.Rows[0]["HostId"].ToString();
                    HostIntId = dt1.Rows[0]["HostIntId"].ToString();
                    HostExtId = dt1.Rows[0]["HostExtId"].ToString();

                    DefaultBilling = dt1.Rows[0]["COLUMN17"].ToString();
                    DefaultShipping = dt1.Rows[0]["COLUMN18"].ToString();

                    vendorOU = dt1.Rows[0]["vOU"].ToString();
                    Payment_Terms = dt1.Rows[0]["COLUMN29"].ToString();
                    Payment_Mode = dt1.Rows[0]["COLUMN30"].ToString();

                }
                else
                {

                    Addressee = "";
                    Address1 = "";
                    Address2 = "";
                    Address3 = "";
                    City = "";
                    State = "";
                    Zip = "";
                    Country = "";
                    Operating_Unit = "";
                    Subsidary = "";
                    Classification = "";
                    Department = "";

                    HostId = "";
                    HostIntId = "";
                    HostExtId = "";

                    DefaultBilling = "";
                    DefaultShipping = "";
                    Payment_Terms = "";
                    Payment_Mode = "";
                }
                string dSval = "";

                List<SelectListItem> States = new List<SelectListItem>();
                if (Country != "")
                {
                    //var dropdata = dc.Where(a => a.COLUMN04 == countryId).ToList();
                    var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(connectionString, providerName);
                    var dropdata = "select COLUMN02,COLUMN03 FROM MATABLE017 where COLUMN04 =" + Country;
                    SqlCommand cmd1 = new SqlCommand(dropdata, cn);
                    SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                    DataTable dt12 = new DataTable();
                    da1.Fill(dt12);
                    for (int dd = 0; dd < dt12.Rows.Count; dd++)
                    {
                        States.Add(new SelectListItem { Value = dt12.Rows[dd]["COLUMN02"].ToString(), Text = dt12.Rows[dd]["COLUMN03"].ToString() });
                    }
                    ViewBag.sList1 = new SelectList(States, "Value", "Text", selectedValue: dSval);
                }
                return Json(new
                {
                    vId = vendorId,
                    Data1 = Addressee,
                    Data2 = Address1,
                    Data3 = Address2,
                    Data4 = Address3,
                    Data5 = City,
                    Data6 = State,
                    Data7 = Zip,
                    Data8 = Country,

                    Data9 = Operating_Unit,
                    Data10 = Subsidary,
                    Data11 = Classification,
                    Data12 = Department,
                    Data13 = HostId,
                    Data14 = HostIntId,
                    Data15 = HostExtId,
                    data17 = DefaultBilling,
                    data18 = DefaultShipping,
                    Data19 = Payment_Terms,
                    Data20 = Payment_Mode,
                    item = States,
                    Data21 = vendorOU
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }

        }


        public ActionResult GetItemDetails(string ItemID)
        {
            try
            {
                //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                string OPUnit = null; string OPUnitstatus = "0";
                string AcOwner = System.Web.HttpContext.Current.Session["AcOwner"].ToString();
                if (System.Web.HttpContext.Current.Session["OPUnitstatus"] != null)
                    OPUnitstatus = System.Web.HttpContext.Current.Session["OPUnitstatus"].ToString();
                if (OPUnitstatus == "1")
                    OPUnit = System.Web.HttpContext.Current.Session["OPUnit"].ToString();
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_PUR_TP_ItemDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                cmd.Parameters.Add(new SqlParameter("@OPUnit", OPUnit));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cn.Close();
                var item = "";
                var upc = "";
                var desc = "";
                var avlQTY = "";
                var price = "";
                var TaxType = "";
                var totprice = "";
                var uom = "";
                var itemdesc = "";
                var purchaseitemdesc = "";
                var purchasetax = "";
                var purchaseuom = "";
                var invoicetotprice = "";
                var invoiceitemdesc = "";
                var invouom = "";
                var invotax = "";
                if (dt.Rows.Count > 0)
                {
                    item = dt.Rows[0]["COLUMN02"].ToString();
                    upc = dt.Rows[0]["COLUMN06"].ToString();
                    desc = dt.Rows[0]["COLUMN09"].ToString();

                    avlQTY = dt.Rows[0]["COLUMN08"].ToString();
                    price = dt.Rows[0]["COLUMN17"].ToString();
                    TaxType = dt.Rows[0]["COLUMN54"].ToString();
                    //invotax = dt.Rows[0]["COLUMN54"].ToString();
                    totprice = dt.Rows[0]["COLUMN51"].ToString();
                    //invoicetotprice = dt.Rows[0]["COLUMN51"].ToString();
                    uom = dt.Rows[0]["COLUMN63"].ToString();
                    itemdesc = dt.Rows[0]["sdesc"].ToString();
                    purchaseitemdesc = dt.Rows[0]["pdesc"].ToString();
                    purchasetax = dt.Rows[0]["COLUMN61"].ToString();
                    purchaseuom = dt.Rows[0]["COLUMN63"].ToString();
                    //invoiceitemdesc = dt.Rows[0]["COLUMN50"].ToString();
                    //invouom = dt.Rows[0]["COLUMN63"].ToString();
                }
                else
                {
                    item = "";
                    upc = "";
                    desc = "";
                    avlQTY = "";
                    price = "";
                    TaxType = "";

                    totprice = "";
                    itemdesc = "";
                    uom = "";
                    purchaseitemdesc = "";
                    purchasetax = "";
                    purchaseuom = "";
                    //invoicetotprice = "";
                    //invoiceitemdesc = "";
                    //invouom = "";
                    //invotax = "";
                }
                return Json(new
                {
                    Data = upc,
                    Data1 = desc,
                    Data2 = avlQTY,
                    Data3 = item,
                    Data4 = price,
                    Data8 = itemdesc,
                    Data5 = TaxType,
                    Data6 = totprice,
                    Data7 = uom,
                    Data9 = purchaseitemdesc,
                    Data11 = purchasetax,
                    Data12 = purchaseuom,
                    //Data13 = invoicetotprice,
                    //Data14 = invoiceitemdesc,
                    //Data15 = invouom,
                    //Data16 = invotax,

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult getJODetails(string ItemID)
        {
            try
            {
                string OPUnit = null; string OPUnitstatus = "0";
                string AcOwner = Session["AcOwner"].ToString();
                OPUnit = Session["OPUnit"].ToString();
                cn.Open();
                SqlCommand cmd = new SqlCommand("usp_JO_TP_ItemCodeDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@itemcode", ItemID));
                cmd.Parameters.Add(new SqlParameter("@OPUnit", OPUnit));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                cn.Close();
                var color = ""; var style = ""; var size = ""; var brand = ""; var itemid = ""; var distP = ""; var itemcode = ""; var UPC = ""; var Itemdesc = ""; var price = ""; var Tax = ""; var UOM = ""; var billitemdesc = ""; var salesitemdesc = ""; var INVOICEUOM = ""; var INVOICETAX = ""; var INVOICEPRICE = "";
                var idesc = ""; var tax = ""; var iName = ""; var FabricItem = ""; var SMRP = "0"; var Colour = "";
                if (dt1.Rows.Count > 0)
                {
                    color = dt1.Rows[0]["color"].ToString();
                    style = dt1.Rows[0]["style"].ToString();
                    size = dt1.Rows[0]["size"].ToString();
                    brand = dt1.Rows[0]["brand"].ToString();
                    itemid = dt1.Rows[0]["itemid"].ToString();
                    UPC = dt1.Rows[0]["upc"].ToString();
                    price = dt1.Rows[0]["price"].ToString();
                    distP = dt1.Rows[0]["distP"].ToString();
                    UOM = dt1.Rows[0]["uom"].ToString();
                    idesc = dt1.Rows[0]["idesc"].ToString();
                    tax = dt1.Rows[0]["tax"].ToString();
                    iName = dt1.Rows[0]["iName"].ToString();
                    FabricItem = dt1.Rows[0]["FabricItem"].ToString();
                    SMRP = dt1.Rows[0]["SMRP"].ToString();
                    Colour = dt1.Rows[0]["Colour"].ToString();
                    //billitemdesc = dt1.Rows[0]["COLUMN57"].ToString();
                    //salesitemdesc = dt1.Rows[0]["COLUMN50"].ToString();
                    //INVOICEUOM = dt1.Rows[0]["COLUMN63"].ToString();
                    //INVOICETAX = dt1.Rows[0]["COLUMN54"].ToString();
                    //INVOICEPRICE = dt1.Rows[0]["COLUMN51"].ToString();
                }
                return Json(new
                {
                    color = color,
                    style = style,
                    size = size,
                    brand = brand,
                    item = itemid,
                    UPC = UPC,
                    price = price,
                    distP = distP,
                    UOM = UOM,
                    idesc = idesc,
                    tax = tax,
                    iName = iName,
                    FabricItem = FabricItem,
                    SMRP = SMRP,
                    Colour = Colour,
                    //billitemdesc = billitemdesc,
                    //salesitemdesc = salesitemdesc,
                    //INVOICEUOM = INVOICEUOM,
                    //INVOICETAX = INVOICETAX,
                    //INVOICEPRICE = INVOICEPRICE,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }

        }

        public int UpdateRedisCacheStatus(string formid, string tblID, string Type)
        {
            try
            {
                var acID = (int?)System.Web.HttpContext.Current.Session["AcOwner"];
                var OPUnit = System.Web.HttpContext.Current.Session["OPUnit1"];
                var eid = (int?)System.Web.HttpContext.Current.Session["eid"];
                SqlCommand cmd = new SqlCommand("usp_Proc_RedisCacheStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Formid", formid);
                cmd.Parameters.AddWithValue("@TblID", tblID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", acID);
                cmd.Parameters.AddWithValue("@Eid", eid);
                cmd.Parameters.AddWithValue("@Type", Type);
                cn.Open();
                int i = cmd.ExecuteNonQuery();
                cn.Close();
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public ActionResult UOMDataStorage(string ItemID, string xmldata, string opunit)
        {
            try
            {
                //EMPHCS1402 rajasekhar reddy patakota 5/12/2015 MultiUom Condition checking While opening in another tab
                string type = "Insert", FormName = "";
                type = Request.UrlReferrer.LocalPath.ToString().Replace("/", ""); type = type.Replace("FormBuilding", "");
                type = type.Replace("JobOrder", "");
                if (type == "FormBuild") { FormName = Request.UrlReferrer.Query.ToString(); FormName = FormName.Replace("?", ""); int l = FormName.IndexOf("&"); if (l > 0) { FormName = FormName.Substring(0, l).Replace("ide=", "").ToString(); } FormName = FormName.Replace("&", ""); FormName = FormName.Replace("%20", " "); FormName = FormName.Replace("FormName=", ""); }
                else if (type == "Edit") { FormName = Request.UrlReferrer.Query.ToString(); FormName = FormName.Replace("?", ""); FormName = FormName.Replace("%20", " "); FormName = FormName.Replace("FormName=", ""); int e = FormName.IndexOf("&"); if (e > 0) FormName = FormName.Remove(0, e); FormName = FormName.Replace("&ide=null", ""); FormName = FormName.Replace("&", ""); }

                else if (type == "NewForm") { FormName = Request.UrlReferrer.Query.ToString(); FormName = FormName.Replace("?", ""); int l = FormName.IndexOf("&"); if (l > 0) { FormName = FormName.Substring(0, l).Replace("ide=", "").ToString(); } FormName = FormName.Replace("&", ""); FormName = FormName.Replace("%20", " "); FormName = FormName.Replace("FormName=", ""); }

                string frmid = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).FirstOrDefault().COLUMN02.ToString();
                ItemID = string.Concat(ItemID, frmid);
                if (xmldata != "<ROOT></ROOT>")
                {
                    Session[ItemID] = xmldata;
                    Session["MultiUOMSelection"] = 1;
                }
                else
                    Session[ItemID] = null;
                return Json(new { Data = Session[ItemID] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("NewForm", "JobOrder", new { FormName = Session["FormName"] });
            }
        }

        public List<SelectListItem> TaxList(string ItemID, string opunit, string customer, string Type, string Price, string Mode, string Date)
        {
            try
            {
                if (Date != "" && Date != "undefined")
                    Date = DateTime.ParseExact(Date, Convert.ToString(System.Web.HttpContext.Current.Session["DateFormat"]), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                if (opunit == "") opunit = Convert.ToString(System.Web.HttpContext.Current.Session["OPUnit1"]);
                List<SelectListItem> Taxes = new List<SelectListItem>();
                SqlCommand cmd = new SqlCommand("usp_proc_GETGSTINBasedTaxes", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                //cmd.Parameters.AddWithValue("@Customer", customer);
                cmd.Parameters.AddWithValue("@OPUnit", opunit);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@Price", Price);
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@OperatingUnit", System.Web.HttpContext.Current.Session["OPUnit"]);
                cmd.Parameters.AddWithValue("@AcOwner", System.Web.HttpContext.Current.Session["AcOwner"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1); string TaxID = "";
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    if (Mode != "Tax" && TaxID == "" && dt1.Rows[dd]["ID"].ToString() == "1") { TaxID = dt1.Rows[dd]["COLUMN02"].ToString(); Taxes.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() }); }
                    if (Mode == "Tax")
                        Taxes.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                    else if (TaxID != "") break;
                }
                if (Taxes.Count == 0) { TaxID = "1000"; Taxes.Add(new SelectListItem { Value = "1000", Text = "Non Taxable" }); }
                    Taxes = Taxes.OrderBy(x => x.Text).ToList();
                return Taxes;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }
        public ActionResult TransactionNoCheck(int formid, string poval, string OPUNIT)
        {
            try
            {
                var flag = false; poval = poval.TrimEnd(); var FormName = Session["FormName"].ToString();
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == FormName).ToList();
                var customfrmID = fnamedata.Select(a => a.COLUMN02).ToString();
                var frmType = fnamedata.Select(a => a.COLUMN05).ToString();
                var MasterForm = fnamedata.Select(a => a.COLUMN06).ToString();
                if (frmType == "Custom")
                    formid = Convert.ToInt32(MasterForm);
                if (OPUNIT == "" || OPUNIT == null) OPUNIT = Convert.ToString(Session["OPUnit1"]);
                SqlCommand cmd = new SqlCommand("PROC_CHECK_TransactionNo", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TransNo", poval));
                cmd.Parameters.Add(new SqlParameter("@FormId", formid));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmd.Parameters.Add(new SqlParameter("@OPUNIT", OPUNIT));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                    flag = true;
                return Json(new { flag = flag, poval = poval, FormName = FormName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "JobOrder", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult JobTransactionNoGenerate(string FormName, int FormId, string opunit)
        {
            try
            {
                var acID = (int?)Session["AcOwner"];
                var OPUnit = Session["OPUnit1"];
                if (opunit != "") OPUnit = opunit;
                Session[FormId + "TranOPUnit"] = OPUnit;
                var eid = (int?)Session["eid"];
                SqlCommand cmd = new SqlCommand("usp_Proc_TransNoGenerate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Formid", FormId);
                //cmd.Parameters.AddWithValue("@TblID", tblID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", acID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string item = "", trancol = "", Override = "";
                if (dt.Rows.Count > 0 && dt.Columns.Contains("PO"))
                { item = dt.Rows[0]["PO"].ToString(); trancol = dt.Rows[0]["TransColumn"].ToString(); Override = dt.Rows[0]["Override"].ToString(); }
                return Json(new { item = item, trancol = trancol, Override = Override }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "JobOrder", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult GetItemLot(string ItemID, string UOM, int frmID, string type)
        {
            try
            {
                
                List<SelectListItem> vendor = new List<SelectListItem>();
                SqlCommand cmd = new SqlCommand("usp_SAL_TP_ItemWiseLotDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                cmd.Parameters.Add(new SqlParameter("@UOM", UOM));
                cmd.Parameters.Add(new SqlParameter("@Type", type));
                cmd.Parameters.Add(new SqlParameter("@FrmID", frmID));
                cmd.Parameters.Add(new SqlParameter("@OPUnit", Session["OPUnit"]));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                }
                
                vendor = vendor.OrderBy(x => x.Text).ToList();
                ViewBag.sList = new SelectList(vendor, "Value", "Text");

                return Json(new { Datalot = vendor }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult GetLotPrice(string ItemID, string UOM, string Lot, string Location, string Opunit, int frmID)
        {
            try
            {
                string PPrice = "0", SPrice = "0", AvailQty = "0"; if (Opunit == "") Opunit = Convert.ToString(Session["OPUnit1"]);
                SqlCommand cmd = new SqlCommand("usp_PROC_TP_LotWisePrice", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                cmd.Parameters.Add(new SqlParameter("@UOM", UOM));
                cmd.Parameters.Add(new SqlParameter("@Lot", Lot));
                cmd.Parameters.Add(new SqlParameter("@location", Location));
                cmd.Parameters.Add(new SqlParameter("@FrmID", frmID));
                cmd.Parameters.Add(new SqlParameter("@OPUnit", Opunit));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                if (dt1.Rows.Count > 0) { PPrice = dt1.Rows[0]["PPrice"].ToString(); SPrice = dt1.Rows[0]["SPrice"].ToString(); AvailQty = dt1.Rows[0]["AvailQty"].ToString(); }
                return Json(new { SPrice = SPrice, PPrice = PPrice, AvailQty = AvailQty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

    }
}
