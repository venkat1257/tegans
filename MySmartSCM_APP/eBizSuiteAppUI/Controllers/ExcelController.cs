﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using MvcMenuMaster.Models;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using System.IO;
using System.ComponentModel.DataAnnotations;
using eBizSuiteUI.Controllers;
using eBizSuiteAppModel.Table;
using WebMatrix.Data;
using System.Web.Helpers;
using MeBizSuiteAppUI.Controllers;
using System.Web.UI;
using iTextSharp.text;
using System.Text;
using eBizSuiteAppDAL.classes;
using eBizSuiteAppUI.Models;
using Microsoft.Reporting.WebForms;
using System.Diagnostics;
using GemBox.Document;
using GemBox.Document.Tables;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using eBizSuiteAppBAL.Fombuilding;
using System.Data.SqlTypes;
using System.Xml.Linq;
using System.Globalization;
using System.Data.OleDb;
using System.Dynamic;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text.RegularExpressions;

namespace MeBizSuiteAppUI.Controllers
{
    public class ExcelController : Controller
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Info()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();

                ViewBag.DateFormat = DateFormatsetup();
                var strQry = "SELECT M10.[COLUMN06 ] AS [User],(case when p.[TransactionType]=2 then 'Master' when p.[TransactionType]=3 then 'Transaction' else '' end) AS [TransactionType],p.[Transaction ] AS [Transaction],p.[Filename ] AS [Filename], (case when p.[Transaction ]= 'Item'then  M1.[COLUMNA06 ] when  p.[Transaction ]= 'customer'then  M2.[COLUMNA06 ] when p.[Transaction ]= 'Vendor'then M3.[COLUMNA06 ] else ''end) AS [Date],(case when P.[Transaction ]='Item' then M1.[ErrorMessage] when  p.[Transaction ]= 'customer'then M2.[ErrorMessage] when p.[Transaction ]='Vendor' then  M3.[ErrorMessage]else '' end) AS [ErrorMessage], (case when P.[Transaction ]='Item' then M1.[Status] when  p.[Transaction ]= 'customer'then M2.[Status] when p.[Transaction ]='Vendor' then  M3.[Status] else '' end) AS [Status] from ExcelImport p " +
                     "left join INTFMGR_ITEMMASTER M1 ON M1.COLUMNA03=p.COLUMNA03 AND M1.Excel_Id= p.Excel_Id " +
                     "left join INTFMGR_CUSTOMERMASTER M2 ON M2.COLUMNA03=p.COLUMNA03  AND M2.Excel_Id= p.Excel_Id " +
                     "left join INTFMGR_VENDORMASTER M3 ON M3.COLUMNA03=p.COLUMNA03  AND M3.Excel_Id= p.Excel_Id " +
                     "left join MATABLE010 M10 ON M10.COLUMN02=M1.COLUMNA08 OR M10.COLUMN02=M2.COLUMNA08 OR M10.COLUMN02=M3.COLUMNA08 where p.COLUMNA03= " + acID + " ";

                alCol.AddRange(new List<string> { "User", "TransactionType", "Transaction", "Filename", "Date", "ErrorMessage", "Status" });
                actCol.AddRange(new List<string> { "COLUMNA08", "TransactionType", "Transaction", "Filename", "COLUMNA06", "ErrorMessage", "Status" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;

                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public string DateFormatsetup()
        {
            string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
            SqlDataAdapter daf = new SqlDataAdapter(str, cn);
            DataTable dtf = new DataTable();
            daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
            if (dtf.Rows.Count > 0)
            {
                DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                Session["DateFormat"] = DateFormat;
                ViewBag.DateFormat = DateFormat;
            }
            if (JQDateFormat != "")
            {
                Session["DateFormat"] = DateFormat;
                Session["ReportDate"] = JQDateFormat;
            }
            else
            {
                Session["DateFormat"] = "dd/MM/yyyy";
                Session["ReportDate"] = "dd/mm/yy";
                ViewBag.DateFormat = "dd/MM/yyyy";
            }
            return ViewBag.DateFormat;
        }

        public ActionResult GETEXCELDATA(string Type, string Tran, string FromDT, string ToDT, string Employee)
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                string whrstring = "";
                if (Type != "") whrstring = "and p.[TransactionType]='" + Type + "'";
                if (Tran != "") whrstring = whrstring + "and p.[Transaction]='" + Tran + "'";
                if (FromDT != "" && ToDT != "") whrstring = whrstring + "and ((format(M1.[COLUMNA06],'" + Session["DateFormat"] + "') between '" + FromDT + "' and '" + ToDT + "') OR (format(M2.[COLUMNA06],'" + Session["DateFormat"] + "') between '" + FromDT + "' and '" + ToDT + "') OR (format(M3.[COLUMNA06], '" + Session["DateFormat"] + "') between '" + FromDT + "' and '" + ToDT + "'))";
                if (Employee != "") whrstring = whrstring + "and P.[COLUMNA06]='" + Employee + "'";
                var strQry = "SELECT M10.[COLUMN06 ] AS [User],(case when p.[TransactionType]=2 then 'Master' when p.[TransactionType]=3 then 'Transaction' else '' end) AS [TransactionType],p.[Transaction ] AS [Transaction],p.[Filename ] AS [Filename], (case when p.[Transaction ]= 'Item'then M1.[COLUMNA06 ] when  p.[Transaction ]= 'customer'then M2.[COLUMNA06 ] when p.[Transaction ]= 'Vendor'then M3.[COLUMNA06 ] else ''end) AS [Date],(case when P.[Transaction ]='Item' then M1.[ErrorMessage] when  p.[Transaction ]= 'customer'then M2.[ErrorMessage] when p.[Transaction ]='Vendor' then  M3.[ErrorMessage]else '' end) AS [ErrorMessage], (case when P.[Transaction ]='Item' then M1.[Status] when  p.[Transaction ]= 'customer'then M2.[Status] when p.[Transaction ]='Vendor' then  M3.[Status] else '' end) AS [Status] from ExcelImport p " +
                      "left join INTFMGR_ITEMMASTER M1 ON M1.COLUMNA03=p.COLUMNA03 AND M1.Excel_Id= p.Excel_Id " +
                      "left join INTFMGR_CUSTOMERMASTER M2 ON M2.COLUMNA03=p.COLUMNA03  AND M2.Excel_Id= p.Excel_Id " +
                      "left join INTFMGR_VENDORMASTER M3 ON M3.COLUMNA03=p.COLUMNA03  AND M3.Excel_Id= p.Excel_Id " +
                      "left join MATABLE010 M10 ON M10.COLUMN02=M1.COLUMNA08 OR M10.COLUMN02=M2.COLUMNA08 OR M10.COLUMN02=M3.COLUMNA08 where p.COLUMNA03= " + acID + " " + whrstring + " ";

                alCol.AddRange(new List<string> { "User", "TransactionType", "Transaction", "Filename", "Date", "ErrorMessage", "Status" });
                actCol.AddRange(new List<string> { "COLUMNA08", "TransactionType", "Transaction", "Filename", "COLUMNA06", "ErrorMessage", "Status" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
               headerStyle: "webgrid-header",
               footerStyle: "webgrid-footer",
               alternatingRowStyle: "webgrid-alternating-row",
               rowStyle: "webgrid-row-style",
               htmlAttributes: new { id = "grdfData" },
               columns: cols);
                return Json(new { grid = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult Create()
        {
            Session["Excelfiledata"] = null;
            var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1641).FirstOrDefault().COLUMN04.ToString();
            Session["FormName"] = FormName;
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection fc)
        {
            try
            {
                var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1641).FirstOrDefault().COLUMN04.ToString();
                Session["FormName"] = FormName;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int AcOwner = Convert.ToInt32(Session["AcOwner"]); 
                var tbldata = ""; string fileExtension = "";
                //var File = Request.Files[0] as HttpPostedFileBase;
                //var FileName = File.FileName;
                DataTable ds = new DataTable();
                ds = (DataTable)Session["Excelfiledata"];
                if (ds.Rows.Count > 0)
                {
                    //fileExtension = System.IO.Path.GetExtension(FileName);
                    //if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    //{
                    //    string fileLocation = Server.MapPath("~/Content/");
                    //    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AcOwner).FirstOrDefault().COLUMN04;
                    //    fileLocation = fileLocation + "Excel" + "\\" + AOwnerName;
                    //    if (!System.IO.Directory.Exists(fileLocation))
                    //        System.IO.Directory.CreateDirectory(fileLocation);
                    //    fileLocation = fileLocation + "\\" + FileName;
                    //    if (System.IO.File.Exists(fileLocation))
                    //        System.IO.File.Delete(fileLocation);
                    //    File.SaveAs(fileLocation);
                    //    string excelConnectionString = string.Empty;
                    //    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    //    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //    if (fileExtension == ".xls")
                    //    {
                    //        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                    //        fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    //    }
                    //    else if (fileExtension == ".xlsx")
                    //    {
                    //        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                    //        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //    }
                    //    DataTable dt = new DataTable();
                    //    using (OleDbConnection excelConnection = new OleDbConnection(excelConnectionString))
                    //    {
                    //        excelConnection.Open();
                    //        dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    //        if (dt == null)
                    //        {
                    //            return null;
                    //        }
                    //        excelConnection.Close();
                    //    }
                    //    String[] excelSheets = new String[dt.Rows.Count];
                    //    int t = 0;
                    //    foreach (DataRow row in dt.Rows)
                    //    {
                    //        excelSheets[t] = row["TABLE_NAME"].ToString();
                    //        t++;
                    //    }
                    //    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                    //    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    //    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    //    {
                    //        dataAdapter.Fill(ds);
                    //    }
                    //    File.InputStream.Dispose();
                    //}
                    //else if (fileExtension.ToString().ToLower().Equals(".xml"))
                    //{
                    //    string fileLocation = Server.MapPath("~/Content/");
                    //    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AcOwner).FirstOrDefault().COLUMN04;
                    //    fileLocation = fileLocation + "Excel" + "\\" + AOwnerName;
                    //    if (!System.IO.Directory.Exists(fileLocation))
                    //        System.IO.Directory.CreateDirectory(fileLocation);
                    //    fileLocation = fileLocation + "\\" + FileName;
                    //    if (System.IO.File.Exists(fileLocation))
                    //        System.IO.File.Delete(fileLocation);
                    //    File.SaveAs(fileLocation);
                    //    XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                    //    ds.ReadXml(xmlreader);
                    //    xmlreader.Close();
                    //    File.InputStream.Dispose();
                    //}
                    string Date = DateTime.Now.ToString();
                    string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                    if (opstatus == null || opstatus == "") OpUnit = null;
                    string insert = "Insert";
                    int OutParam = 0;
                    SqlCommand cmdd = new SqlCommand("select isnull(max(COLUMN02),0) from MATABLE034 ", cn);
                    SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                    DataTable dtt = new DataTable();
                    daa.Fill(dtt);
                    string RefCol2 = "";
                    if (dtt.Rows.Count > 0) RefCol2 = dtt.Rows[0][0].ToString();
                    else RefCol2 = "999";
                    RefCol2 = (Convert.ToInt32(RefCol2) + 1).ToString();
                    SqlCommand Cmd = new SqlCommand("usp_MAS_BL_ExcelImport", cn);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", RefCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", 1641);
                    Cmd.Parameters.AddWithValue("@COLUMN04", fc["Type"]);
                    Cmd.Parameters.AddWithValue("@COLUMN05", fc["master"]);
                    Cmd.Parameters.AddWithValue("@COLUMN06", fc["FileName"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit1"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AcOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "MATABLE034");
                    cn.Open();
                    int r = Cmd.ExecuteNonQuery();
                    cn.Close();
                    int l = 0;
                    if (fc["master"] == "1261")
                    {
                        for (int i = 0; i < ds.Rows.Count; i++)
                        {
                            SqlCommand cmd = new SqlCommand("select isnull(max(COLUMN02),0) from INTFMGR_ITEMMASTER ", cn);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            daa.Fill(dtt);
                            string HCol1 = "";
                            if (dtt.Rows.Count > 0) HCol1 = dtt.Rows[0][0].ToString();
                            else HCol1 = "999";
                            HCol1 = (Convert.ToInt32(HCol1) + 1).ToString();
                            string UPC = ds.Rows[i]["UPC"].ToString();
                            string ItemName = ds.Rows[i]["ItemName"].ToString();
                            string ItemDescription = ds.Rows[i]["ItemDescription"].ToString();
                            string Vendor = ds.Rows[i]["Vendor"].ToString().Replace(",", "");
                            string ItemGroup = ds.Rows[i]["ItemGroup"].ToString().Replace(",", "");
                            string Family = ds.Rows[i]["Family"].ToString().Replace(",", "");
                            string ItemBrand = ds.Rows[i]["ItemBrand"].ToString().Replace(",", "");
                            string PurchasePrice = ds.Rows[i]["PurchasePrice"].ToString().Replace(",", "");
                            string SalesPrice = ds.Rows[i]["SalesPrice"].ToString().Replace(",", "");
                            string PurchaseTax = ds.Rows[i]["PurchaseTax"].ToString().Replace(",", "");
                            string SalesTax = ds.Rows[i]["SalesTax"].ToString().Replace(",", "");
                            string MRP = ds.Rows[i]["MRP"].ToString().Replace(",", "");
                            string Units = ds.Rows[i]["Units"].ToString().Replace(",", "");
                            string HSNCode = ds.Rows[i]["HSNCode"].ToString().Replace(",", "");
                            string Type = ds.Rows[i]["Type"].ToString().Replace(",", "");
                            string FabricItem = ds.Rows[i]["FabricItem"].ToString().Replace(",", "");
                            string FinishedItemfromJobbing = ds.Rows[i]["Finished Item from Jobbing"].ToString().Replace(",", "");
                            string JobOrder = ds.Rows[i]["Job Order"].ToString().Replace(",", "");
                            string JobOrderIssue = ds.Rows[i]["Job Order Issue"].ToString().Replace(",", "");
                            string PrimaryFabricItem = ds.Rows[i]["Primary Fabric Item"].ToString().Replace(",", "");

                            if (UPC == "" || UPC == "undefined") UPC = "";
                            if (ItemName == "" || ItemName == " " || ItemName == null || ItemName == "undefined") ItemName = "0";
                            if (ItemDescription == "" || ItemDescription == " " || ItemDescription == null || ItemDescription == "undefined") ItemDescription = null;
                            if (Vendor == "" || Vendor == " " || Vendor == null || Vendor == "undefined") Vendor = "0";
                            if (ItemGroup == "" || ItemGroup == " " || ItemGroup == null || ItemGroup == "undefined") ItemGroup = "0";
                            if (Family == "" || Family == " " || Family == null || Family == "undefined") Family = "0";
                            if (ItemBrand == "" || ItemBrand == " " || ItemBrand == null || ItemBrand == "undefined") ItemBrand = "0";
                            if (PurchasePrice == "" || PurchasePrice == " " || PurchasePrice == null || PurchasePrice == "undefined") PurchasePrice = "0";
                            if (SalesPrice == "" || SalesPrice == " " || SalesPrice == null || SalesPrice == "undefined") SalesPrice = "0";
                            if (PurchaseTax == "" || PurchaseTax == " " || PurchaseTax == null || PurchaseTax == "undefined") PurchaseTax = "0";
                            if (SalesTax == "" || SalesTax == " " || SalesTax == null || SalesTax == "undefined") SalesTax = "0";
                            if (MRP == "" || MRP == " " || MRP == null || MRP == "undefined") MRP = "0";
                            if (Units == "" || Units == " " || Units == null || Units == "undefined") Units = "0";
                            if (HSNCode == "" || HSNCode == " " || HSNCode == null || HSNCode == "undefined") HSNCode = "0";
                            if (Type == "" || Type == " " || Type == null || Type == "undefined") Type = null;
                            if (FabricItem == "" || FabricItem == " " || FabricItem == null || FabricItem == "undefined") FabricItem = "0";
                            if (FinishedItemfromJobbing == "" || FinishedItemfromJobbing == " " || FinishedItemfromJobbing == null || FinishedItemfromJobbing == "undefined") FinishedItemfromJobbing = "0";
                            if (JobOrder == "" || JobOrder == " " || JobOrder == null || JobOrder == "undefined") JobOrder = null;
                            if (JobOrderIssue == "" || JobOrderIssue == " " || JobOrderIssue == null || JobOrderIssue == "undefined") JobOrderIssue = null;
                            if (PrimaryFabricItem == "" || PrimaryFabricItem == " " || PrimaryFabricItem == null || PrimaryFabricItem == "undefined") PrimaryFabricItem = null;



                            SqlCommand Cmdl = new SqlCommand("INTFMGR_WRAPPER", cn);
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", HCol1);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", UPC);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", ItemName);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", ItemDescription);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", Vendor);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", ItemGroup);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", Family);
                            Cmdl.Parameters.AddWithValue("@COLUMN09", ItemBrand);
                            Cmdl.Parameters.AddWithValue("@COLUMN10", PurchasePrice);
                            Cmdl.Parameters.AddWithValue("@COLUMN11", SalesPrice);
                            Cmdl.Parameters.AddWithValue("@COLUMN12", PurchaseTax);
                            Cmdl.Parameters.AddWithValue("@COLUMN13", SalesTax);
                            Cmdl.Parameters.AddWithValue("@COLUMN14", MRP);
                            Cmdl.Parameters.AddWithValue("@COLUMN15", Units);
                            Cmdl.Parameters.AddWithValue("@COLUMN16", HSNCode);
                            Cmdl.Parameters.AddWithValue("@COLUMN17", Type);
                            Cmdl.Parameters.AddWithValue("@COLUMN18", RefCol2);
                            Cmdl.Parameters.AddWithValue("@COLUMN19", FabricItem);
                            Cmdl.Parameters.AddWithValue("@COLUMN20", FinishedItemfromJobbing);
                            Cmdl.Parameters.AddWithValue("@COLUMN21", JobOrder);
                            Cmdl.Parameters.AddWithValue("@COLUMN22", JobOrderIssue);
                            Cmdl.Parameters.AddWithValue("@COLUMN23", PrimaryFabricItem);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit1"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AcOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            //Cmdl.Parameters.AddWithValue("@Direction", insert);
                            //Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE034");
                            cn.Open();
                            l = Cmdl.ExecuteNonQuery();
                            cn.Close();
                            if (r > 0)
                            {
                                var msg = string.Empty;
                                msg = "" + Session["FormName"] + " Successfully Created ";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "Success";
                            }
                            else
                            {
                                var msg = string.Empty;
                                msg = "" + Session["FormName"] + " Creation Failed";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "fail";
                            }
                        }
                    }
                    else if (fc["master"] == "1265")
                    {
                        for (int i = 0; i < ds.Rows.Count; i++)
                        {

                            SqlCommand cmd = new SqlCommand("select isnull(max(COLUMN02),0) from INTFMGR_CUSTOMERMASTER ", cn);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            daa.Fill(dtt);
                            string HCol1 = "";
                            if (dtt.Rows.Count > 0) HCol1 = dtt.Rows[0][0].ToString();
                            else HCol1 = "999";
                            HCol1 = (Convert.ToInt32(HCol1) + 1).ToString();
                            string CustomerName = ds.Rows[i]["CustomerName"].ToString();
                            string SalesRep = ds.Rows[i]["SalesRep"].ToString();
                            string Commission = ds.Rows[i]["Commission"].ToString();
                            string Email = ds.Rows[i]["Email"].ToString().Replace(",", "");
                            string Phone = ds.Rows[i]["Phone"].ToString().Replace(",", "");
                            string PaymentTerms = ds.Rows[i]["PaymentTerms"].ToString().Replace(",", "");
                            string TIN = ds.Rows[i]["TIN"].ToString().Replace(",", "");
                            string Address = ds.Rows[i]["Address"].ToString().Replace(",", "");
                            string Address1 = ds.Rows[i]["Address1"].ToString().Replace(",", "");
                            string Address2 = ds.Rows[i]["Address2"].ToString().Replace(",", "");
                            string City = ds.Rows[i]["City"].ToString().Replace(",", "");
                            string State = ds.Rows[i]["State"].ToString().Replace(",", "");
                            string Country = ds.Rows[i]["Country"].ToString().Replace(",", "");
                            string Pincode = ds.Rows[i]["Pincode"].ToString().Replace(",", "");
                            string GSTCategory = ds.Rows[i]["GSTCategory"].ToString().Replace(",", "");
                            string GSTIN = ds.Rows[i]["GSTIN"].ToString().Replace(",", "");
                            string StateCode = ds.Rows[i]["StateCode"].ToString().Replace(",", "");

                            if (CustomerName == "" || CustomerName == "undefined") CustomerName = null;
                            if (SalesRep == "" || SalesRep == " " || SalesRep == null || SalesRep == "undefined") SalesRep = null;
                            if (Commission == "" || Commission == " " || Commission == null || Commission == "undefined") Commission = null;
                            if (Email == "" || Email == " " || Email == null || Email == "undefined") Email = null;
                            if (Phone == "" || Phone == " " || Phone == null || Phone == "undefined") Phone = null;
                            if (PaymentTerms == "" || PaymentTerms == " " || PaymentTerms == null || PaymentTerms == "undefined") PaymentTerms = null;
                            if (TIN == "" || TIN == " " || TIN == null || TIN == "undefined") TIN = null;
                            if (Address == "" || Address == " " || Address == null || Address == "undefined") Address = null;
                            if (Address1 == "" || Address1 == " " || Address1 == null || Address1 == "undefined") Address1 = null;
                            if (Address2 == "" || Address2 == " " || Address2 == null || Address2 == "undefined") Address2 = null;
                            if (City == "" || City == " " || City == null || City == "undefined") City = null;
                            if (State == "" || State == " " || State == null || State == "undefined") State = null;
                            if (Country == "" || Country == " " || Country == null || Country == "undefined") Country = null;
                            if (Pincode == "" || Pincode == " " || Pincode == null || Pincode == "undefined") Pincode = null;
                            if (GSTCategory == "" || GSTCategory == " " || GSTCategory == null || GSTCategory == "undefined") GSTCategory = null;
                            if (GSTIN == "" || GSTIN == " " || GSTIN == null || GSTIN == "undefined") GSTIN = null;
                            if (StateCode == "" || StateCode == " " || StateCode == null || StateCode == "undefined") StateCode = null;

                            SqlCommand Cmdl = new SqlCommand("INTFMGR_CUSTOMERWRAPPER", cn);
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", HCol1);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", CustomerName);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", SalesRep);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", Commission);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", Email);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", Phone);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", PaymentTerms);
                            Cmdl.Parameters.AddWithValue("@COLUMN09", TIN);
                            Cmdl.Parameters.AddWithValue("@COLUMN10", Address);
                            Cmdl.Parameters.AddWithValue("@COLUMN11", Address1);
                            Cmdl.Parameters.AddWithValue("@COLUMN12", Address2);
                            Cmdl.Parameters.AddWithValue("@COLUMN13", City);
                            Cmdl.Parameters.AddWithValue("@COLUMN14", State);
                            Cmdl.Parameters.AddWithValue("@COLUMN15", Country);
                            Cmdl.Parameters.AddWithValue("@COLUMN16", Pincode);
                            Cmdl.Parameters.AddWithValue("@COLUMN17", GSTCategory);
                            Cmdl.Parameters.AddWithValue("@COLUMN18", GSTIN);
                            Cmdl.Parameters.AddWithValue("@COLUMN19", StateCode);
                            Cmdl.Parameters.AddWithValue("@COLUMN20", RefCol2);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit1"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AcOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            //Cmdl.Parameters.AddWithValue("@Direction", insert);
                            //Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE034");
                            cn.Open();
                            l = Cmdl.ExecuteNonQuery();
                            cn.Close();
                            if (r > 0)
                            {
                                var msg = string.Empty;
                                msg = "" + Session["FormName"] + " Successfully Created ";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "Success";
                            }
                            else
                            {
                                var msg = string.Empty;
                                msg = "" + Session["FormName"] + " Creation Failed";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "fail";
                            }
                        }
                    }
                    else if (fc["master"] == "1252")
                    {
                        for (int i = 0; i < ds.Rows.Count; i++)
                        {
                            SqlCommand cmd = new SqlCommand("select isnull(max(COLUMN02),0) from INTFMGR_VENDORMASTER ", cn);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            daa.Fill(dtt);
                            string HCol1 = "";
                            if (dtt.Rows.Count > 0) HCol1 = dtt.Rows[0][0].ToString();
                            else HCol1 = "999";
                            HCol1 = (Convert.ToInt32(HCol1) + 1).ToString();
                            string VendorName = ds.Rows[i]["VendorName"].ToString();
                            //string SalesRep = ds.Rows[i]["SalesRep"].ToString();
                            //string Commission = ds.Rows[i]["Commission"].ToString();
                            string Email = ds.Rows[i]["Email"].ToString().Replace(",", "");
                            string Phone = ds.Rows[i]["Phone"].ToString().Replace(",", "");
                            string PaymentTerms = ds.Rows[i]["PaymentTerms"].ToString().Replace(",", "");
                            string TIN = ds.Rows[i]["TIN"].ToString().Replace(",", "");
                            string Address = ds.Rows[i]["Address"].ToString().Replace(",", "");
                            string Address1 = ds.Rows[i]["Address1"].ToString().Replace(",", "");
                            string Address2 = ds.Rows[i]["Address2"].ToString().Replace(",", "");
                            string City = ds.Rows[i]["City"].ToString().Replace(",", "");
                            string State = ds.Rows[i]["State"].ToString().Replace(",", "");
                            string Country = ds.Rows[i]["Country"].ToString().Replace(",", "");
                            string Pincode = ds.Rows[i]["Pincode"].ToString().Replace(",", "");
                            string GSTCategory = ds.Rows[i]["GSTCategory"].ToString().Replace(",", "");
                            string GSTIN = ds.Rows[i]["GSTIN"].ToString().Replace(",", "");
                            string StateCode = ds.Rows[i]["StateCode"].ToString().Replace(",", "");
                            string Type = ds.Rows[i]["Type"].ToString().Replace(",", "");


                            if (VendorName == "" || VendorName == "undefined") VendorName = "";
                            //if (SalesRep == "" || SalesRep == " " || SalesRep == null || SalesRep == "undefined") SalesRep = null;
                            //if (Commission == "" || Commission == " " || Commission == null || Commission == "undefined") Commission = null;
                            if (Email == "" || Email == " " || Email == null || Email == "undefined") Email = null;
                            if (Phone == "" || Phone == " " || Phone == null || Phone == "undefined") Phone = null;
                            if (PaymentTerms == "" || PaymentTerms == " " || PaymentTerms == null || PaymentTerms == "undefined") PaymentTerms = null;
                            if (TIN == "" || TIN == " " || TIN == null || TIN == "undefined") TIN = null;
                            if (Address == "" || Address == " " || Address == null || Address == "undefined") Address = null;
                            if (Address1 == "" || Address1 == " " || Address1 == null || Address1 == "undefined") Address1 = null;
                            if (Address2 == "" || Address2 == " " || Address2 == null || Address2 == "undefined") Address2 = null;
                            if (City == "" || City == " " || City == null || City == "undefined") City = null;
                            if (State == "" || State == " " || State == null || State == "undefined") State = null;
                            if (Country == "" || Country == " " || Country == null || Country == "undefined") Country = null;
                            if (Pincode == "" || Pincode == " " || Pincode == null || Pincode == "undefined") Pincode = null;
                            if (GSTCategory == "" || GSTCategory == " " || GSTCategory == null || GSTCategory == "undefined") GSTCategory = null;
                            if (GSTIN == "" || GSTIN == " " || GSTIN == null || GSTIN == "undefined") GSTIN = null;
                            if (StateCode == "" || StateCode == " " || StateCode == null || StateCode == "undefined") StateCode = null;
                            if (Type == "" || Type == " " || Type == null || Type == "undefined") Type = null;

                            SqlCommand Cmdl = new SqlCommand("INTFMGR_VENDORWRAPPER", cn);
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", HCol1);
                            Cmdl.Parameters.AddWithValue("@COLUMN03", VendorName);
                            //Cmdl.Parameters.AddWithValue("@COLUMN04", SalesRep);
                            //Cmdl.Parameters.AddWithValue("@COLUMN05", Commission);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", Email);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", Phone);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", PaymentTerms);
                            Cmdl.Parameters.AddWithValue("@COLUMN09", TIN);
                            Cmdl.Parameters.AddWithValue("@COLUMN10", Address);
                            Cmdl.Parameters.AddWithValue("@COLUMN11", Address1);
                            Cmdl.Parameters.AddWithValue("@COLUMN12", Address2);
                            Cmdl.Parameters.AddWithValue("@COLUMN13", City);
                            Cmdl.Parameters.AddWithValue("@COLUMN14", State);
                            Cmdl.Parameters.AddWithValue("@COLUMN15", Country);
                            Cmdl.Parameters.AddWithValue("@COLUMN16", Pincode);
                            Cmdl.Parameters.AddWithValue("@COLUMN17", GSTCategory);
                            Cmdl.Parameters.AddWithValue("@COLUMN18", GSTIN);
                            Cmdl.Parameters.AddWithValue("@COLUMN19", StateCode);
                            Cmdl.Parameters.AddWithValue("@COLUMN20", Type);
                            Cmdl.Parameters.AddWithValue("@COLUMN21", RefCol2);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit1"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AcOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            //Cmdl.Parameters.AddWithValue("@Direction", insert);
                            //Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE034");
                            cn.Open();
                            l = Cmdl.ExecuteNonQuery();
                            cn.Close();
                            if (r > 0)
                            {
                                var msg = string.Empty;
                                msg = "" + Session["FormName"] + " Successfully Created ";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "Success";
                            }
                            else
                            {
                                var msg = string.Empty;
                                msg = "" + Session["FormName"] + " Creation Failed";
                                Session["MessageFrom"] = msg;
                                Session["SuccessMessageFrom"] = "fail";
                            }
                        }
                    }
                    SqlCommand Cmd2 = new SqlCommand("INTFMGR_WRAPPER1", cn);
                    Cmd2.CommandType = CommandType.StoredProcedure;
                    Cmd2.Parameters.AddWithValue("@COLUMN02", RefCol2);
                    Cmd2.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit1"]);
                    Cmd2.Parameters.AddWithValue("@COLUMNA03", AcOwner);
                    Cmd2.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                    Cmd2.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                    Cmd2.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd2.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd2.Parameters.AddWithValue("@COLUMNA13", 0);
                    //Cmdl.Parameters.AddWithValue("@Direction", insert);
                    //Cmdl.Parameters.AddWithValue("@TabelName", "MATABLE034");
                    Cmd2.Parameters.AddWithValue("@Status", "N");
                    Cmd2.Parameters.AddWithValue("@TYPE", fc["master"]);
                    Cmd2.Parameters.AddWithValue("@Excel_Id", RefCol2);
                    cn.Open();
                    l = Cmd2.ExecuteNonQuery();
                    cn.Close();
                } 
                Session["Excelfiledata"] = null;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                var linenumber = ex.StackTrace.Substring(ex.StackTrace.Length - 5, 5);
                Session["MessageFrom"] = msg + " Due to " + ex.Message.Replace("\r\n", " ") + " at line no : " + linenumber;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public void GetSampleStmt()
        {
            try
            {
                string fileLocation = Server.MapPath("~/Content/");
                int AOwner = Convert.ToInt32(Session["AcOwner"]);
                var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                fileLocation = fileLocation + "ExcelImp" + "\\New";
                if (!System.IO.Directory.Exists(fileLocation))
                    System.IO.Directory.CreateDirectory(fileLocation);
                var ide = @Request["ide"]; var FileName = "Sample11.xlsx";
                if (ide == "1261") FileName = "Item.xlsx";
                else if (ide == "1265") FileName = "Customer.xlsx";
                else if (ide == "1252") FileName = "Vendor.xlsx";
                fileLocation = fileLocation + "\\" + FileName;
                FileInfo file = new FileInfo(fileLocation);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", FileName));
                    Response.ContentType = "application/ms-excel";
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult FileNameCheck()
        {
            try
            {
                var File = Request.Files[0] as HttpPostedFileBase;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                var FileName = File.FileName; string fileExtension = "";
                var Transaction = Request["Transaction"].ToString();
                //Create a new DataTable.
                DataTable Idt = new DataTable();
                if (FileName.Length > 0)
                {
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    if (fileExtension != ".xlsx")
                        return Json(new { DataName = "", FileData = "", FileFormat = "1", FileName = FileName }, JsonRequestBehavior.AllowGet);
                    SqlCommand cmdf = new SqlCommand("select COLUMN06 from MATABLE034 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN06='" + FileName + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ", cn);
                    SqlDataAdapter daf = new SqlDataAdapter(cmdf);
                    DataTable dtf = new DataTable();
                    daf.Fill(dtf);
                    if (dtf.Rows.Count > 0)
                        return Json(new { DataName = dtf.Rows[0][0].ToString(), FileData = "", FileFormat = "", FileName = FileName }, JsonRequestBehavior.AllowGet);
                    string fileLocation = Server.MapPath("~/Content/");
                    int AOwner = Convert.ToInt32(Session["AcOwner"]);
                    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                    fileLocation = fileLocation + "Excel" + "\\" + AOwnerName;
                    if (!System.IO.Directory.Exists(fileLocation))
                        System.IO.Directory.CreateDirectory(fileLocation);
                    fileLocation = fileLocation + "\\" + FileName;
                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);
                    //Save the uploaded Excel file.
                    string filePath = fileLocation;
                    File.SaveAs(fileLocation);
                    DataTable dt = new DataTable();
                    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileLocation, true))
                    {
                        spreadSheetDocument.Close();
                    }

                    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileLocation, false))
                    {
                        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();
                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                        }
                        foreach (Row row in rows) //this will also include your header row...
                        {
                            DataRow tempRow = dt.NewRow();
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }
                            dt.Rows.Add(tempRow);
                        }
                    }
                    dt.Rows.RemoveAt(0);
                    Idt = dt;
                }
                DataColumnCollection columns = Idt.Columns;
                SqlCommand cmdc = new SqlCommand("select COLUMN04,COLUMN06 from SETABLE022 where COLUMN03=" + Transaction + " and COLUMN05=1641 and isnull(COLUMNA13,0)=0 ", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                if (dtc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtc.Rows.Count; i++)
                    {
                        string colname = dtc.Rows[i]["COLUMN04"].ToString();
                        colname = colname.TrimEnd();
                        if (columns.Contains(colname) && colname != "")
                        {
                        }
                        else
                            return Json(new { DataName = "", FileData = "", FileFormat = "1", FileName = FileName }, JsonRequestBehavior.AllowGet);
                    }
                    Session["Excelfiledata"] = Idt;
                    return Json(new { DataName = "", FileData = "1", FileFormat = "", FileName = FileName }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { DataName = "", FileData = "", FileFormat = "1", FileName = FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return "";
            }
            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null && cell.DataType == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }

        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        private string GetValue(SpreadsheetDocument doc, Cell cell)
        {
            if (cell.CellValue != null)
            {
                string value = cell.CellValue.InnerText;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
                return value;
            }
            return "";
        }

        public ActionResult getMaster(int TransactionId)
        {
            try
            {
                string dSval = "";
                List<SelectListItem> Master = new List<SelectListItem>();
                var dropdata = "SELECT  COLUMN06 COLUMN02,COLUMN04 FROM SETABLE010 where COLUMN06 IN(1265,1252,1261)";
                if (TransactionId == 3)
                    dropdata = "select COLUMN06 COLUMN02,COLUMN04 FROM SETABLE010 where COLUMN05 ='Transaction'";
                SqlCommand cmd = new SqlCommand(dropdata, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    Master.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN04"].ToString() });
                }
                Master = Master.OrderBy(x => x.Text).ToList();
                ViewBag.sList = new SelectList(Master, "Value", "Text", selectedValue: dSval);
                return Json(new { item = Master }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult ExcelImportStatus()
        {
            try
            {
                var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1642).FirstOrDefault().COLUMN04.ToString();
                Session["FormName"] = FormName; Session["id"] = 1642;
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                List<SelectListItem> Emp = new List<SelectListItem>();
                var dropdata = "select COLUMN02,COLUMN06 COLUMN04 from MATABLE010 where isnull(COLUMN32,0)=0 AND isnull(COLUMNA13,0)=0 AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'";
                var GData = dbs.Query(dropdata).OrderBy(a => a.COLUMN04).ToList();
                ViewData["Employee"] = new SelectList(GData, "COLUMN02", "COLUMN04");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
    }
}



