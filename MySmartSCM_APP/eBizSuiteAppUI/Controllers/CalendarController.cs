﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using MyCalendar.Models;
using MeBizSuiteAppUI.Models;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Data;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Month;
using DayPilot.Web.Mvc.Json;
using System.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class CalendarActionResponseModel
    {
        public String Status;
        public Int64 Source_id;
        public Int64 Target_id;

        public CalendarActionResponseModel(String status, Int64 source_id, Int64 target_id)
        {
            Status = status;
            Source_id = source_id;
            Target_id = target_id;
        }
    }

    public class CalendarController : Controller
    {
        //
        // GET: /Calendar/

        public ActionResult Index()
        {
            int eid=Convert.ToInt32(Session["eid"]);
            return View();
        }

        //public ActionResult Data()
        //{
        //    MyEventsDataContext data = new MyEventsDataContext();
        //    var emp = Convert.ToInt32(Session["eid"]);
        //    return View(data.Events.Where(q => q.empid == emp));
        //}

        //public ActionResult Save(Event changedEvent, FormCollection actionValues)
        //{
        //    String action_type = actionValues["!nativeeditor_status"];
        //    Int64 source_id = Int64.Parse(actionValues["id"]);
        //    Int64 target_id = source_id;


        //    MyEventsDataContext data = new MyEventsDataContext();
        //    try
        //    {
        //        switch (action_type)
        //        {
        //            case "inserted":
        //                changedEvent.empid = Convert.ToInt32(Session["eid"]);
        //                data.Events.InsertOnSubmit(changedEvent);
        //                break;
        //            case "updated":
        //                changedEvent.empid = Convert.ToInt32(Session["eid"]);
        //                //data.Events.InsertOnSubmit(changedEvent);
        //                break;
        //            case "deleted":
        //                changedEvent = data.Events.SingleOrDefault(ev => ev.id == source_id);
        //                data.Events.DeleteOnSubmit(changedEvent);
        //                break;
        //            default:  //"updated":
        //                changedEvent = data.Events.SingleOrDefault(ev => ev.id == source_id);
        //                UpdateModel(changedEvent);
        //                break;
        //        }
        //        //data.Events.empi
        //        data.SubmitChanges();
        //        target_id = changedEvent.id;
        //    }
        //    catch
        //    {
        //        action_type = "error";
        //    }

        //    return View(new CalendarActionResponseModel(action_type, source_id, target_id));
        //}

        public ActionResult Light()
        {
            return RedirectToAction("ThemeTransparent");

        }
        public ActionResult Red()
        {
            return RedirectToAction("Index");

        }
        public ActionResult Green()
        {
            return RedirectToAction("ThemeGreen");
        }

        public ActionResult ThemeGreen()
        {
            return View();
        }

        public ActionResult ThemeWhite()
        {
            return View();
        }

        public ActionResult ThemeTransparent()
        {
            return View();
        }

        public ActionResult ThemeBlue()
        {
            return View();
        }

        public ActionResult ThemeGoogleLike()
        {
            return View();
        }

        public ActionResult ThemeTraditional()
        {
            return View();
        }

        public ActionResult NextPrevious()
        {
            return View();
        }
        public ActionResult JQuery()
        {
            return View();
        }
        //public ActionResult EventCreating()
        //{
        //    return View();
        //}
        public ActionResult EventMoving()
        {
            return View();
        }
        public ActionResult EventResizing()
        {
            return View();
        }
        public ActionResult DropDown()
        {
            return View();
        }

        public ActionResult EventActiveAreas()
        {
            return View();
        }

        public ActionResult EventContextMenu()
        {
            return View();
        }

        public ActionResult EventSelecting()
        {
            return View();
        }

        public ActionResult EventBubble()
        {
            return View();
        }

        public ActionResult EventDoubleClick()
        {
            return View();
        }

        public ActionResult EventStartEndTime()
        {
            return View();
        }

        public ActionResult EventMoveToPosition()
        {
            return View();
        }

        public ActionResult RecurringEvents()
        {
            return View();
        }

        public ActionResult Weeks()
        {
            return View();
        }

        public ActionResult Today()
        {
            return View();
        }

        public ActionResult Weekends()
        {
            return View();
        }

        public ActionResult EventCssContinue()
        {
            return View();
        }

        public ActionResult AutoRefresh()
        {
            return View();
        }

        public ActionResult NotifyEventModel()
        {
            return View();
        }

        public ActionResult Message()
        {
            return View();
        }

        public ActionResult Height100Pct()
        {
            return View();
        }



        public ActionResult Backend()
        {
            return new Dpm().CallBack(this);
        }

        public class Dpm : DayPilotMonth
        {
            //protected override void OnTimeRangeSelected(TimeRangeSelectedArgs e)
            //{
            //    string name = (string)e.Data["name"];
            //    if (String.IsNullOrEmpty(name))
            //    {
            //        name = "(default)";
            //    }
            //    new EventManager(Controller).EventCreate(e.Start, e.End, name);
            //    Update();
            //}

            //protected override void OnEventMove(EventMoveArgs e)
            //{
            //    if (new EventManager(Controller).Get(e.Id) != null)
            //    {
            //        new EventManager(Controller).EventMove(e.Id, e.NewStart, e.NewEnd);
            //    }

            //    Update();
            //}

            //protected override void OnEventResize(EventResizeArgs e)
            //{
            //    new EventManager(Controller).EventMove(e.Id, e.NewStart, e.NewEnd);
            //    Update();
            //}

            protected override void OnCommand(CommandArgs e)
            {
                switch (e.Command)
                {
                    case "navigate":
                        StartDate = (DateTime)e.Data["start"];
                        Update(CallBackUpdateType.Full);
                        break;

                    case "previous":
                        StartDate = StartDate.AddMonths(-1);
                        Update(CallBackUpdateType.Full);
                        break;

                    case "next":
                        StartDate = StartDate.AddMonths(1);
                        Update(CallBackUpdateType.Full);
                        break;

                    case "today":
                        StartDate = DateTime.Today;
                        Update(CallBackUpdateType.Full);
                        break;

                    case "refresh":
                        Update();
                        break;
                }
            }

            protected override void OnInit(InitArgs initArgs)
            {
                Update(CallBackUpdateType.Full);
            }

            protected override void OnFinish()
            {
                // only load the data if an update was requested by an Update() call
                if (UpdateType == CallBackUpdateType.None)
                {
                    return;
                }

                // this select is a really bad example, no where clause
                Events = new EventManager(Controller).Data.AsEnumerable();

                DataStartField = "start";
                DataEndField = "end";
                DataTextField = "text";
                DataIdField = "id";
            }

        }

        public class EventManager
        {
            private Controller controller;
            private string key;

            public EventManager(Controller controller, string key)
            {
                this.controller = controller;
                this.key = key;

                if (this.controller.Session[key] == null)
                {
                    this.controller.Session[key] = generateData();
                }
            }

            public DataTable Data
            {
                get { return (DataTable)controller.Session[key]; }
            }

            public DataTable FilteredData(DateTime start, DateTime end, string keyword)
            {
                string where = String.Format("NOT (([end] <= '{0:s}') OR ([start] >= '{1:s}')) and [text] like '%{2}%'", start, end, keyword);
                DataRow[] rows = Data.Select(where);
                DataTable filtered = Data.Clone();

                foreach (DataRow r in rows)
                {
                    filtered.ImportRow(r);
                }

                return filtered;
            }

            public EventManager(Controller controller)
                : this(controller, "default")
            {
            }

            private DataTable generateData()
            {
                DataClasses1DataContext dp=new DataClasses1DataContext();
                DataTable dt = new DataTable();
                var eID=Convert.ToInt32(System.Web.HttpContext.Current.Session["eid"] );
                var data = dp.Events.Where(q => q.empid == eID).ToList();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("text", typeof(string));
                dt.Columns.Add("start", typeof(DateTime));
                dt.Columns.Add("end", typeof(DateTime));
                dt.Columns.Add("eid", typeof(int)); 
                foreach (var entity in data)
                {
                    var row = dt.NewRow();
                    row["id"] = entity.id;
                    row["text"] = entity.text;
                    row["start"] = entity.eventstart;
                    row["end"] = entity.eventend;
                    row["eid"] = entity.empid;
                    dt.Rows.Add(row);
                }
                //dt.Rows.Add(data);

                //dt.PrimaryKey = new DataColumn[] { dt.Columns["id"] };

                //DataRow dr;

                //dr = dt.NewRow();
                //dr["id"] = 1;
                //dr["start"] = Convert.ToDateTime("15:00");
                //dr["end"] = Convert.ToDateTime("15:00");
                //dr["text"] = "Event 1 (backslash test: TEST\\jname)";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 2;
                //dr["start"] = Convert.ToDateTime("16:00");
                //dr["end"] = Convert.ToDateTime("17:00");
                //dr["text"] = "Event 2";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 3;
                //dr["start"] = Convert.ToDateTime("14:15").AddDays(1);
                //dr["end"] = Convert.ToDateTime("18:45").AddDays(1);
                //dr["text"] = "Event 3";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 4;
                //dr["start"] = Convert.ToDateTime("16:30");
                //dr["end"] = Convert.ToDateTime("17:30");
                //dr["text"] = "Sales Dept. Meeting Once Again";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 5;
                //dr["start"] = Convert.ToDateTime("8:00");
                //dr["end"] = Convert.ToDateTime("9:00");
                //dr["text"] = "Event 4";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 6;
                //dr["start"] = Convert.ToDateTime("14:00");
                //dr["end"] = Convert.ToDateTime("20:00");
                //dr["text"] = "Event 6";
                //dt.Rows.Add(dr);


                //dr = dt.NewRow();
                //dr["id"] = 7;
                //dr["start"] = Convert.ToDateTime("11:00");
                //dr["end"] = Convert.ToDateTime("13:14");
                //dr["text"] = "Unicode test: 公曆 (requires Unicode fonts on the client side)";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 8;
                //dr["start"] = Convert.ToDateTime("13:14").AddDays(-1);
                //dr["end"] = Convert.ToDateTime("14:05").AddDays(-1);
                //dr["text"] = "Event 8";
                //dt.Rows.Add(dr);


                //dr = dt.NewRow();
                //dr["id"] = 9;
                //dr["start"] = Convert.ToDateTime("13:14").AddDays(7);
                //dr["end"] = Convert.ToDateTime("14:05").AddDays(7);
                //dr["text"] = "Event 9";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 10;
                //dr["start"] = Convert.ToDateTime("13:14").AddDays(-7);
                //dr["end"] = Convert.ToDateTime("14:05").AddDays(-7);
                //dr["text"] = "Event 10";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 14;
                //dr["start"] = Convert.ToDateTime("7:45:00");
                //dr["end"] = Convert.ToDateTime("8:30:00");
                //dr["text"] = "Event 14";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["id"] = 16;
                //dr["start"] = Convert.ToDateTime("8:30:00").AddDays(1);
                //dr["end"] = Convert.ToDateTime("9:00:00").AddDays(1);
                //dr["text"] = "Event 16";
                //dt.Rows.Add(dr);


                //dr = dt.NewRow();
                //dr["id"] = 17;
                //dr["start"] = Convert.ToDateTime("8:00:00").AddDays(1);
                //dr["end"] = Convert.ToDateTime("8:00:01").AddDays(1);
                //dr["text"] = "Event 17";
                //dt.Rows.Add(dr);

                return dt;
            }

            //public void EventEdit(string id, string name)
            //{
            //    DataRow dr = Data.Rows.Find(id);
            //    if (dr != null)
            //    {
            //        dr["text"] = name;
            //        Data.AcceptChanges();
            //    }
            //}

            //public void EventMove(string id, DateTime start, DateTime end)
            //{
            //    DataRow dr = Data.Rows.Find(id);
            //    if (dr != null)
            //    {
            //        dr["start"] = start;
            //        dr["end"] = end;
            //        Data.AcceptChanges();
            //    }
            //}

            //public Event Get(string id)
            //{
            //    DataRow dr = Data.Rows.Find(id);
            //    if (dr == null)
            //    {
            //        //return new Event();
            //        return null;
            //    }
            //    return new Event()
            //    {
            //        Id = (string)dr["id"],
            //        Text = (string)dr["text"]
            //    };
            //}
            //internal void EventCreate(DateTime start, DateTime end, string text, string id)
            //{
            //    DataRow dr = Data.NewRow();

            //    dr["id"] = id;
            //    dr["start"] = start;
            //    dr["end"] = end;
            //    dr["text"] = text;

            //    Data.Rows.Add(dr);
            //    Data.AcceptChanges();
            //}

            //internal void EventCreate(DateTime start, DateTime end, string text)
            //{
            //    EventCreate(start, end, text, Guid.NewGuid().ToString());
            //}

            public class Event
            {
                public string Id { get; set; }
                public string Text { get; set; }
                public DateTime Start { get; set; }
                public DateTime End { get; set; }
            }

            //public void EventDelete(string id)
            //{
            //    DataRow dr = Data.Rows.Find(id);
            //    if (dr != null)
            //    {
            //        Data.Rows.Remove(dr);
            //        Data.AcceptChanges();
            //    }
            //}
        }
    }
}
