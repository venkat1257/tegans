﻿using eBizSuiteDAL;
using eBizSuiteDAL.classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using WebMatrix.Data;
using eBizSuiteAppModel.Table;
using System.Configuration;

namespace eBizSuiteProduct.Controllers
{
    public class BranchMasterController : Controller
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();

        BranchManagerMaster userManager = new BranchManagerMaster();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";


        // GET: /RoleMaster/

        public ActionResult Index()
        {
            return View();
        }

        //get rolemaster
        [HttpGet]
        public ActionResult BranchMasterDetailes()
        {
        //    var connectionString = "data source=programmer5-pc;Initial Catalog=EMPHORADB;integrated security=true";

        //    var providerName = "System.Data.SqlClient";

        //    var db = Database.OpenConnectionString(connectionString, providerName);
        //    var query = "select * from tbl_BranchMaster";
        //    ViewBag.grid = db.Query(query);

            // dbContext.tbl_RoleMaster.ToList()
            return View("BranchMasterDetailes", dbContext.tbl_BranchMaster.ToList());
        }

//sudheer 1 svn test

        //create
        [HttpGet]
        public ActionResult NewBranchMaster()
        {
            return View();
        }
        //create
        [HttpPost]
        public ActionResult NewBranchMaster(tbl_BranchMaster user)
        {
            try
            {
                if (ModelState.IsValid)
                {



                    if (!userManager.IsUserLoginIDExist(user.BranchId))
                    {

                        userManager.Add(user);

                        FormsAuthentication.SetAuthCookie(user.BranchName, false);
                        return RedirectToAction("BranchMasterDetailes", "BranchMaster");

                    }
                    else
                    {
                        ModelState.AddModelError("", "BranchId already taken");
                    }
                }
            }
            catch
            {
                return View(user);
            }

            return View(user);
        }


//sudheer 2 svn test
        //edit
        [HttpGet]
        public ActionResult EditBranchMaster(int id)
        {
            var data = userManager.GetEmployeeDetail(id);
            tbl_BranchMaster emp = new tbl_BranchMaster();
            emp.BranchId = data.BranchId;
            emp.BranchName = data.BranchName;
            emp.BranchDesc = data.BranchDesc;


            return View("EditBranchMaster", emp);


        }

//sudheer3

        ////update
        [HttpPost]
        public ActionResult UpdateBranchMaster(tbl_BranchMaster register)
        {
            tbl_BranchMaster sysRegister = new tbl_BranchMaster();

            sysRegister.BranchId = register.BranchId;
            sysRegister.BranchName = register.BranchName;
            sysRegister.BranchDesc = register.BranchDesc;


            bool IsSuccess = userManager.UpdateBranchMaster(sysRegister);


            if (IsSuccess)
            {
                TempData["OperStatus"] = "Employee updated succeessfully";
                ModelState.Clear();
                return RedirectToAction("BranchMasterDetailes", "BranchMaster");
            }

            return View();

        }


        ////delete single row
        public ActionResult DeleteBranchMaster(int id)
        {
            bool check = userManager.DeleteBranchMaster(id);

            var data = userManager.GetBranchMaster();
            ViewBag.grid = data;
            return PartialView("BranchMasterDetailes", dbContext.tbl_BranchMaster.ToList());

        }

        //delete multiple selections
        [HttpPost]
        public ActionResult DeleteMultiple(string[] assignChkBx)
        {
            //Delete Selected 
            int[] id = null;
            if (assignChkBx != null)
            {
                id = new int[assignChkBx.Length];
                int j = 0;
                foreach (string i in assignChkBx)
                {
                    int.TryParse(i, out id[j++]);
                }
            }

            if (id != null && id.Length > 0)
            {
                List<tbl_BranchMaster> allSelected = new List<tbl_BranchMaster>();
                eBizSuiteTableEntities dc = new eBizSuiteTableEntities();

                allSelected = dc.tbl_BranchMaster.Where(a => id.Contains(a.BranchId)).ToList();
                foreach (var i in allSelected)
                {
                    dc.tbl_BranchMaster.Remove(i);
                }
                dc.SaveChanges();

            }

            return PartialView("BranchMasterDetailes");

            //return RedirectToAction("BranchMasterDetailes");
        }


        ////Update InLine
        [HttpPost]
        public ActionResult UpdateInline(string list)
        {

            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                var c = dc.tbl_BranchMaster.Where(a => a.BranchId == eid).FirstOrDefault();

                c.BranchName = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                c.BranchDesc = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                dc.SaveChanges();

            }
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_BranchMaster";
            ViewBag.grid = db.Query(query);

            return PartialView("BranchMasterDetailes", dbContext.tbl_BranchMaster.ToList());
        }


        //1 search RoleMaster
        [HttpGet]
        public ActionResult SearchBranchMaster()
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_BranchMaster";

            ViewBag.grid = db.Query(query);

            //dbContext.tbl_RoleMaster.ToList()
            return PartialView();
        }

        //2 search RoleMaster
        [HttpGet]
        [ActionName("SearchEmployePost")]
        public ActionResult SearchEmployePost(string id)
        {
            var db = Database.Open("sqlcon");

            var BranchId = id;
            var BranchName = "";

            if (BranchId != null)
            {
                BranchId = id + "%";
            }
            else
                BranchId = id;

            var query = "SELECT * FROM tbl_BranchMaster  WHERE BranchId like '" + BranchId + "' or BranchName LIKE '" + BranchName + "'  ";

            var grid = dbContext.tbl_BranchMaster.SqlQuery(query);

            ViewBag.grid = grid;
            Session["griddata"] = grid;
            Session["data"] = ViewBag.grid;

            return PartialView("SearchBranchMaster");
        }


        [HttpPost]
        [ActionName("SearchBranchMaster")]
        public ActionResult SearchBranchMaster(int assignChkBx)
        {

            var query = "SELECT * FROM tbl_BranchMaster  WHERE BranchId =" + assignChkBx + "";

            var query1 = dbContext.tbl_BranchMaster.SqlQuery(query);

            return View("BranchMasterDetailes", query1);
        }


        //Advance Search
        public ActionResult AdvanceSearch()
        {
            return View("AdvanceSearch");
        }
        [HttpPost]
        [ActionName("AdvanceSearch")]
        public ActionResult Searchpost()
        {

            var BranchId = "";
            var BranchName = "";
            var BranchDesc = "";

            if (Request["BranchId"] != string.Empty)
            {
                BranchId = Request["BranchId"] + "%";
            }
            else
                BranchId = Request["BranchId"];


            if (Request["BranchName"] != string.Empty)
            {
                BranchName = Request["BranchName"] + "%";
            }
            else
                BranchName = Request["BranchName"];


            if (Request["BranchDesc"] != string.Empty)
            {
                BranchDesc = Request["BranchDesc"] + "%";
            }
            else
                BranchDesc = Request["BranchDesc"];


            var query = "SELECT * FROM tbl_BranchMaster  WHERE BranchId like '" + BranchId + "' or BranchName LIKE '" + BranchName + "' or BranchDesc LIKE '" + BranchDesc + "'  ";

            var query1 = dbContext.tbl_BranchMaster.SqlQuery(query);

            return View("BranchMasterDetailes", query1);
        }

    }
}
