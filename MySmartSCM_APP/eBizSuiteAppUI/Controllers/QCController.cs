﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class QCController : Controller
    {
        //
        // GET: /PointOfSales/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Create()
        {
            try
            {
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' order by column05 ", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select max(S5.COLUMN02) COLUMN02,max(S5.COLUMN04) COLUMN04 from SATABLE005 S5 INNER JOIN SATABLE007 S17 ON S17.COLUMN06=S5.COLUMN02 AND S17.COLUMNA03=S5.COLUMNA03 AND S17.COLUMNA02=S5.COLUMNA02 Where S5.COLUMN04 !='' and ( S5.COLUMNA02 in(" + Session["OPUnit"] + ")   or S5.COLUMNA02 is null)   AND S5.COLUMNA03='" + Session["AcOwner"] + "'  and  isnull(S5.COLUMNA13,'False')='False'and  isnull(S17.COLUMNA13,'False')='False' and S5.column03=1275 and S17.column14 NOT IN('QC Completed','QC Failed') group by S5.column04 , S5.column02 order by S5.column02 desc ", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult DiscountTypes(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> DiscountType = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where COLUMN03=11158 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    DiscountType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["DiscountType"] = new SelectList(DiscountType, "Value", "Text");
                return PartialView("DiscountTypes");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult TaxInformation(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> TaxType = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013  where (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and isnull(COLUMN15,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    TaxType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                return PartialView("TaxInformation");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult GetTaxAmount(string TaxType, string TaxCal, string FormName)
        {
            try
            {
                string fid = "1275";
                SqlCommand cmd = new SqlCommand("GetTaxAmount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TaxType", TaxType));
                cmd.Parameters.Add(new SqlParameter("@FormID", fid));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1); var Tax = "";
                if (dt1.Rows.Count > 0)
                {
                    Tax = dt1.Rows[0]["COLUMN07"].ToString();
                }
                return Json(new { Tax = Tax }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult PaymentInformation(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> Account = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where COLUMN07=22266 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformation");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult getBalanceAmount(string Account)
        {
            try
            {
                string bal = "0";
                SqlCommand cmd = new SqlCommand("Select isnull(column10,0) from fitable001 where column02=" + Account + " AND    (" + Session["OPUnitWithNull"] + " or columna02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    bal = dt1.Rows[0][0].ToString();
                }
                return Json(new { Balance = bal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        [HttpPost]
        public ActionResult POSSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["POSGridData"] = ds;
                return Json("");
            }
            catch (Exception ex)
            {
                var saveform = Request.QueryString["FormName"];
                var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == saveform).ToList();
                var fname = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                lg.CreateFile(Server.MapPath("~/"), fname + "_" + Session["UserName"].ToString() + "", " \r\n" + "***********************TARGET1***********************" + " \r\n" + " \r\n" + " Line Data Receiving Failed .");

                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono="",TransNo="";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (frmIDchk == 1579)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE017  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE017  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "QC";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE017  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE017  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }
        [HttpPost]
        public ActionResult Saveform(string so, string Customer, string itemqty, string totalqty, string scqty)
        {
            try
            {
                SetDateFormat();
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string Date = DateTime.Now.ToString();
                string OpUnit =Convert.ToString( Session["OPUnit"]);
                string opstatus=Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null) OpUnit = null;
                string AOwner = Convert.ToString( Session["AcOwner"]);
                string insert = "Insert", TransNo="";
                SqlCommand cmdd = new SqlCommand("select  isnull(Max(COLUMN02),999) from SATABLE017", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                HCol2 = (Convert.ToInt32(HCol2)+1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_QCVERIFICATION", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1579");
                TransNo = GetTransactionNo(1579);
                Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                Cmd.Parameters.AddWithValue("@COLUMN05", Date);
                Cmd.Parameters.AddWithValue("@COLUMN06", so);
                Cmd.Parameters.AddWithValue("@COLUMN07", Customer);
                Cmd.Parameters.AddWithValue("@COLUMN10", totalqty);
                Cmd.Parameters.AddWithValue("@COLUMN11", itemqty);
                Cmd.Parameters.AddWithValue("@COLUMN12", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMN13", scqty);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE017");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  isnull(Max(COLUMN02),999) from SATABLE018 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string upc = itemdata.Tables[0].Rows[i]["upc"].ToString();
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    string desc = itemdata.Tables[0].Rows[i]["desc"].ToString();
			    	//EMPHCS1711 Adding QC Verification screen to sales module
                    string UOMId = itemdata.Tables[0].Rows[i]["UOMId"].ToString();
                    string brandId = itemdata.Tables[0].Rows[i]["brandId"].ToString();
                    string qtyScaned = itemdata.Tables[0].Rows[i]["qtyScaned"].ToString();
                    string IIIQTY = itemdata.Tables[0].Rows[i]["IIIQTY"].ToString();
                    SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_QCVERIFICATION", cn);
                    Cmdl.CommandType = CommandType.StoredProcedure;
                    Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                    Cmdl.Parameters.AddWithValue("@COLUMN03", upc);
                    Cmdl.Parameters.AddWithValue("@COLUMN04", ItemName);
                    Cmdl.Parameters.AddWithValue("@COLUMN05", desc);
                    Cmdl.Parameters.AddWithValue("@COLUMN06", brandId);
                    Cmdl.Parameters.AddWithValue("@COLUMN07", qtyScaned);
                    Cmdl.Parameters.AddWithValue("@COLUMN08", IIIQTY);
			    	//EMPHCS1711 Adding QC Verification screen to sales module
                    Cmdl.Parameters.AddWithValue("@COLUMN10", UOMId);
                    Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmdl.Parameters.AddWithValue("@Direction", insert);
                    Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE018");
                    Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                    cn.Open();
                    int l = Cmdl.ExecuteNonQuery();
                    cn.Close();
                }
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1539 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "QC Successfully Completed............ ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1539 && q.COLUMN05 == 2).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "QC Creation Failed............ ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return Json(new { Data = 1, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet); 
                    
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult QCInfo()
        {
            try
            {
                SetDateFormat();
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
            List<string> alCol = new List<string>();
				//EMPHCS1711 Adding QC Verification screen to sales module
            var strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [QC#],FORMAT(p.[COLUMN05 ],'"+Session["FormatDate"].ToString()+"')AS Date,s.[COLUMN05 ] AS Customer,s5.[COLUMN04 ] AS [Sales Order#],s7.COLUMN04 AS [Item Issue#],(isnull(p.COLUMN11,0)) AS [No Of Items],cast(sum(isnull(s8.COLUMN09,0)) as int) AS [Total Qty],(isnull(p.COLUMN13,0)) AS [Total Scanned Qty],(isnull(p.COLUMN14,0)) AS [Status],ma.COLUMN09 [Verified By] From SATABLE017 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN07   and s.columna03=p.columna03 left outer join  SATABLE005 s5 on s5.COLUMN02=p.COLUMN06 and s5.columna03=p.columna03 and s5.columna02=p.columna02 and isnull(s5.COLUMNA13,0)=0  left outer join  SATABLE007 s7 on s5.COLUMN02=s7.COLUMN06 and s5.columna03=s7.columna03 and s5.columna02=s7.columna02 and isnull(s7.COLUMNA13,0)=0 left outer join  SATABLE008 s8 on s7.COLUMN01=s8.COLUMN14 and s8.columna03=s7.columna03 and s8.columna02=s7.columna02 and isnull(s8.COLUMNA13,0)=0 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN12  and o.columna03=p.columna03 left join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)   where isnull(p.COLUMNA13,'False')='False' and " + OPUnit + " and p.COLUMNA03=" + acID + " group by s.COLUMN05,s5.COLUMN04,s7.COLUMN04,p.COLUMN02,p.COLUMN04,p.COLUMN14,ma.COLUMN09,p.COLUMN05,p.COLUMN11,p.COLUMN13 order by p.COLUMN02 desc ";
            alCol.AddRange(new List<string> { "ID", "QC#", "Date", "Customer", "Sales Order#", "Item Issue#", "No Of Items","Total Qty", "Total Scanned Qty", "Status", "Verified By" });
            actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN05", "COLUMN04", "COLUMN05", "COLUMN04", "COLUMN11","COLUMN09", "COLUMN13", "COLUMN14", "COLUMN09" });
                   var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                    var grid = new WebGrid(GData, canPage: false, canSort: false);
                    List<WebGridColumn> cols = new List<WebGridColumn>();
                int index=0;
                        foreach (var column in actCol)
                        {
                            cols.Add(grid.Column(alCol[index], alCol[index]));
                            index++;
                        }
                        ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult GetItemDetails(string ItemID)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("POSItemdetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                cmd.Parameters.AddWithValue("@OPUnit",OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                 SqlDataAdapter da = new SqlDataAdapter(cmd);
                 DataTable dt = new DataTable();
                 da.Fill(dt);
                 string htmlstring =null, htmlstring1 =null;
                 for (int g = 0; g < dt.Rows.Count; g++)
                 {
                     string dvalue =Convert.ToString(dt.Rows[g][dt.Columns[7].ColumnName]);
                     string dddata = "";
                     List<SelectListItem> UOM = new List<SelectListItem>();
                     SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                     DataTable dtdata = new DataTable();
                     cmddl.Fill(dtdata);
                     for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                     {
                         UOM.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["UOM"] = new SelectList(UOM, "Value", "Text", selectedValue: dvalue).ToString();
                     dddata += "<option value=''>--Select--</option>";
                     for (int d = 0; d < UOM.Count; d++)
                     {
                         if (UOM[d].Value == dvalue)
                             dddata += "<option value=" + UOM[d].Value + " selected>" + UOM[d].Text + "</option>";
                         else
                             dddata += "<option value=" + UOM[d].Value + ">" + UOM[d].Text + "</option>";
                     }
                     htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dItemId>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dItemName>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><select   class ='gridddl edit-mode'  name='" + dt.Columns[2].ColumnName + "'   id=dynamicuom  >" + dddata + "</select></td>";
                     //htmlstring += "<td><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dQuantity>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dPrice>" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dAmount>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                     htmlstring += "<td style='display:none;'><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "'  style='display:none;'><span id=dTaxamt style='display:none;'>0</span></td>";
                     htmlstring += "<td style='display:none;'><div class=initshow><span id=ddiscount style='display:none;'>0</span></div><input type='text' id='Discount' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
                     htmlstring += "<td style='display:none;'><input type='text' id='chkrow' class='txtgridclass edit-mode'  name='chkrow'    value=0  style='display:none;'><span id=saveUnitId style='display:none'>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span></td></tr>";
                 }
                return Json(new{Data = htmlstring }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult getHeaderQC(string SalesOrderID)
        {
            try
            {
                int SOID = 0; string form = "0"; string custformid = "";
                var custform = Request.UrlReferrer.Query.ToString(); int l = custform.IndexOf("&"); if (l > 0) custform = custform.Remove(0, l + 1); int m = custform.IndexOf("&"); if (m > 0) custform = custform.Remove(0, m + 1); custform = custform.Replace("Prevfi=", "");
                if (m > 0) custformid = " and column03 = " + custform + "";
                SqlCommand cmd = new SqlCommand("usp_SAL_TP_QC_HEADER_DATA", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                SqlCommand cmd1 = new SqlCommand("usp_SAL_TP_QC_LINE_DATA", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
                cmd1.Parameters.Add(new SqlParameter("@Form", form));
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da1.Fill(dt);
                var qty = "";
                var items = "";
                var Customer = "";
                var Operating_Unit = "";
                if (dt1.Rows.Count > 0)
                {
                    qty = dt1.Rows[0]["QTY"].ToString();
                    items = dt1.Rows[0]["ITEM"].ToString();
                    Customer = dt1.Rows[0]["CUST"].ToString();
                    Operating_Unit = dt1.Rows[0]["OPER"].ToString();
                }
                else
                {
                    qty = "";
                    items = "";
                    Customer = "";

                }
                Session["FormName"] = "QC Verification-OutWard";
                string htmlstring = null;
                for (int g = 0; g < dt.Rows.Count; g++)
                {
                    htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dItemId>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=upc style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dItemName>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' >" + "<span id=ItemId style='display:none'>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dQuantity>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
				    //EMPHCS1711 Adding QC Verification screen to sales module
                    htmlstring += "<td class=rowshow><div class=initshow><span id=duom>" + dt.Rows[g][dt.Columns[8].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[9].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[9].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[9].ColumnName] + "' ></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dPrice>" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' >" + "<span id=brandId style='display:none'>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dAmount><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass' readonly='true'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "' ><span id=IIQTY style='display:none'>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span></td></span></div>";
                }
                var db = WebMatrix.Data.Database.Open("sqlcon");
            var books = db.Query(@"SELECT m.COLUMN02 Item#, m.COLUMN04 ItemName, u.COLUMN04 Units, 1 Quantity, m.COLUMN17 Price,
m.COLUMN17 Amount FROM MATABLE007 m left JOIN MATABLE002 u  ON m.COLUMN63 = u.COLUMN02 where m.column02=0 ORDER BY m.COLUMN02 DESC");                                                   
     var grid = new WebGrid(books,canPage:false,canSort:false);
     var htmlstring1 = grid.GetHtml(tableStyle: "Retailwebgrid-table",
        headerStyle: "Retailwebgrid-header",
        footerStyle: "Retailwebgrid-footer",
            alternatingRowStyle: "Retailwebgrid-alternating-row",
        rowStyle: "Retailwebgrid-row-style", htmlAttributes: new { id = "grdData" },
columns: (
grid.Columns(grid.Column("UPC#", style: "col2"),
				//EMPHCS1711 Adding QC Verification screen to sales module
grid.Column("Item", style: "col3"), grid.Column("Item Description", style: "col4"), grid.Column("UOM", style: "col5"), grid.Column("Brand", style: "col5"),
grid.Column("Qty Scanned", style: "col6"))));
                return Json(new
                {
                    grid = htmlstring1.ToHtmlString(),
                    remain = htmlstring,
                    Data1 = qty,
                    Data2 = Customer,
                    Data3 = items

                },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult QCView()
        {
            try
            {
				//EMPHCS1711 Adding QC Verification screen to sales module
                SqlDataAdapter cmdv = new SqlDataAdapter(" SELECT S8.COLUMN03 UPC#,M7.COLUMN04 ITEM,S8.COLUMN04 ITEMID,S8.COLUMN05 [DESC],M5.COLUMN04 BRAND,S8.COLUMN06 BRANDID,cast(isnull(S8.COLUMN07,0) as int) [QtyScaned], " +
                                                         " S7.COLUMN06 SOID,S5.COLUMN04 SO,S7.COLUMN07 CUID,S2.COLUMN05 CUST,S7.COLUMN10 TQTY,S7.COLUMN11 ITQTY,S7.COLUMN13 TOTSCANNEDQTY,S7.COLUMN14 STATUS FROM SATABLE017 S7 " +
                                                         " INNER JOIN SATABLE018 S8 ON S8.COLUMN09=S7.COLUMN01 AND S7.COLUMNA02=S8.COLUMNA02 AND S7.COLUMNA03=S8.COLUMNA03 " +
                                                         " INNER JOIN SATABLE005 S5 ON S5.COLUMN02=S7.COLUMN06 AND S5.COLUMNA02=S7.COLUMNA02 AND S5.COLUMNA03=S7.COLUMNA03 " +
                                                         " INNER JOIN SATABLE002 S2 ON S2.COLUMN02=S7.COLUMN07 AND S5.COLUMNA03=S7.COLUMNA03 " +
                                                         " LEFT JOIN MATABLE007 M7 ON M7.COLUMN02=S8.COLUMN04 AND M7.COLUMNA03=S8.COLUMNA03 " +
                                                         " LEFT JOIN MATABLE005 M5 ON M5.COLUMN02=M7.COLUMN10 AND M5.COLUMNA03=S8.COLUMNA03 " +
                                                         " WHERE  S7.COLUMN02= " + @Request["ide"] + " AND S7.COLUMNA13=0 AND S8.COLUMNA13=0 ", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                List<SelectListItem> Customer = new List<SelectListItem>();
                Customer.Add(new SelectListItem { Value = dtv.Rows[0]["CUID"].ToString(), Text = dtv.Rows[0]["CUST"].ToString() });
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["CUID"].ToString());
                List<SelectListItem> Item = new List<SelectListItem>();
                Item.Add(new SelectListItem { Value = dtv.Rows[0]["SOID"].ToString(), Text = dtv.Rows[0]["SO"].ToString() });
                ViewData["Item"] = new SelectList(Item, "Value", "Text", selectedValue: dtv.Rows[0]["SOID"].ToString());
                ViewBag.SO = dtv.Rows[0]["SOID"].ToString();
                ViewBag.CUST = dtv.Rows[0]["CUID"].ToString();
                ViewBag.ITQTY = dtv.Rows[0]["ITQTY"].ToString();
                ViewBag.TQTY = dtv.Rows[0]["TQTY"].ToString();
                ViewBag.TOTSCANNEDQTY = dtv.Rows[0]["TOTSCANNEDQTY"].ToString();
                ViewBag.STATUS = dtv.Rows[0]["STATUS"].ToString();
               
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
				return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult QCEdit()
        {
            try
            {
				//EMPHCS1711 Adding QC Verification screen to sales module
                SqlDataAdapter cmdv = new SqlDataAdapter(" SELECT S8.COLUMN03 UPC#,M7.COLUMN04 ITEM,S8.COLUMN04 ITEMID,S8.COLUMN05 [DESC],M5.COLUMN04 BRAND,S8.COLUMN06 BRANDID,cast(isnull(S8.COLUMN07,0) as int) [QtyScaned], " +
                                                         " S7.COLUMN06 SOID,S5.COLUMN04 SO,S7.COLUMN07 CUID,S2.COLUMN05 CUST,S7.COLUMN10 TQTY,S7.COLUMN11 ITQTY,S7.COLUMN13 TOTSCANNEDQTY,S7.COLUMN14 STATUS FROM SATABLE017 S7 " +
                                                         " INNER JOIN SATABLE018 S8 ON S8.COLUMN09=S7.COLUMN01 AND S7.COLUMNA02=S8.COLUMNA02 AND S7.COLUMNA03=S8.COLUMNA03 " +
                                                         " INNER JOIN SATABLE005 S5 ON S5.COLUMN02=S7.COLUMN06 AND S5.COLUMNA02=S7.COLUMNA02 AND S5.COLUMNA03=S7.COLUMNA03 " +
                                                         " INNER JOIN SATABLE002 S2 ON S2.COLUMN02=S7.COLUMN07 AND S5.COLUMNA03=S7.COLUMNA03 " +
                                                         " LEFT JOIN MATABLE007 M7 ON M7.COLUMN02=S8.COLUMN04 AND M7.COLUMNA03=S8.COLUMNA03 " +
                                                         " LEFT JOIN MATABLE005 M5 ON M5.COLUMN02=M7.COLUMN10 AND M5.COLUMNA03=S8.COLUMNA03 " +
                                                         " WHERE  S7.COLUMN02= " + @Request["ide"] + " AND S7.COLUMNA13=0 AND S8.COLUMNA13=0 ", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                List<SelectListItem> Customer = new List<SelectListItem>();
                Customer.Add(new SelectListItem { Value = dtv.Rows[0]["CUID"].ToString(), Text = dtv.Rows[0]["CUST"].ToString() });
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["CUID"].ToString());
                List<SelectListItem> Item = new List<SelectListItem>();
                Item.Add(new SelectListItem { Value = dtv.Rows[0]["SOID"].ToString(), Text = dtv.Rows[0]["SO"].ToString() });
                ViewData["Item"] = new SelectList(Item, "Value", "Text", selectedValue: dtv.Rows[0]["SOID"].ToString());
                ViewBag.SO = dtv.Rows[0]["SOID"].ToString();
                ViewBag.CUST = dtv.Rows[0]["CUID"].ToString();
                ViewBag.ITQTY = dtv.Rows[0]["ITQTY"].ToString();
                ViewBag.TQTY = dtv.Rows[0]["TQTY"].ToString();
                ViewBag.TOTSCANNEDQTY = dtv.Rows[0]["TOTSCANNEDQTY"].ToString();
                ViewBag.STATUS = dtv.Rows[0]["STATUS"].ToString();
           
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
				return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult QCDelete()
        {
            try
            {
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from SATABLE009 where COLUMN02=" + HCol2 + "", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                string Date = DateTime.Now.ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OPunit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                SqlCommand cmd1 = new SqlCommand("select  Max(COLUMN02) from SATABLE011 where COLUMN07=" + HCol2 + "", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                int HColc = Convert.ToInt32(dt1.Rows[0][0].ToString());
                if (HColc > 0)
                {
                    SqlCommand Cmdc = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
                    Cmdc.CommandType = CommandType.StoredProcedure;
                    Cmdc.Parameters.AddWithValue("@COLUMN02", HColc);
                    Cmdc.Parameters.AddWithValue("@COLUMNA02", OPunit);
                    Cmdc.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdc.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdc.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdc.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdc.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdc.Parameters.AddWithValue("@COLUMNA13", 1);
                    Cmdc.Parameters.AddWithValue("@Direction", insert);
                    Cmdc.Parameters.AddWithValue("@TabelName", "SATABLE011");
                    Cmdc.Parameters.AddWithValue("@ReturnValue", "");
                    cn.Open();
                    int c = Cmdc.ExecuteNonQuery();
                    cn.Close();
                }
                cn.Close();
                return RedirectToAction("PointOfSalesInfo", "PointOfSales", new { idi = idi, FormName = custform });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
				return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        [HttpPost]
        public ActionResult EditSaveform(string so, string Customer, string itemqty, string totalqty, string scqty)
        {
            try
            {
                SetDateFormat();
                var idi = "";
                var ide = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.LastIndexOf("&"); if (n > 0) { ide = custform.Substring(n + 5, ((custform.Length - 1) - (n + 4))); }

                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null) OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update", TransNo = "";
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_QCVERIFICATION", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1579");
                Cmd.Parameters.AddWithValue("@COLUMN05", Date);
                Cmd.Parameters.AddWithValue("@COLUMN06", so);
                Cmd.Parameters.AddWithValue("@COLUMN07", Customer);
                Cmd.Parameters.AddWithValue("@COLUMN10", totalqty);
                Cmd.Parameters.AddWithValue("@COLUMN11", itemqty);
                Cmd.Parameters.AddWithValue("@COLUMN12", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMN13", scqty);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE017");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    string upc = itemdata.Tables[0].Rows[i]["upc"].ToString();
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    string desc = itemdata.Tables[0].Rows[i]["desc"].ToString();
                    string brandId = itemdata.Tables[0].Rows[i]["brandId"].ToString();
			 	    //EMPHCS1711 Adding QC Verification screen to sales module
                    string UOMId = itemdata.Tables[0].Rows[i]["UOMId"].ToString();
                    string qtyScaned = itemdata.Tables[0].Rows[i]["qtyScaned"].ToString();
                    SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_QCVERIFICATION", cn);
                    Cmdl.CommandType = CommandType.StoredProcedure;
                    Cmdl.Parameters.AddWithValue("@COLUMN02", upc);
                    Cmdl.Parameters.AddWithValue("@COLUMN03", upc);
                    Cmdl.Parameters.AddWithValue("@COLUMN04", ItemName);
                    Cmdl.Parameters.AddWithValue("@COLUMN05", desc);
                    Cmdl.Parameters.AddWithValue("@COLUMN06", brandId);
                    Cmdl.Parameters.AddWithValue("@COLUMN07", qtyScaned);
				    //EMPHCS1711 Adding QC Verification screen to sales module
                    Cmdl.Parameters.AddWithValue("@COLUMN10", UOMId);
                    Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmdl.Parameters.AddWithValue("@Direction", insert);
                    Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE018");
                    Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                    cn.Open();
                    int l = Cmdl.ExecuteNonQuery();
                    cn.Close();
                } if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1539 && q.COLUMN05 == 3).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "QC Changes Successfully Completed............ ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1539 && q.COLUMN05 == 4).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "QC Changes Failed............ ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return Json(new { Data = 1, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult SetDateFormat()
        {
            try
            {
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter da = new SqlDataAdapter(str, cn);
                DataTable dt = new DataTable();
                da.Fill(dt); string DateFormat = null;
                if (dt.Rows.Count > 0)
                {
                    DateFormat = dt.Rows[0][0].ToString();
                    Session["FormatDate"] = dt.Rows[0][0].ToString();
                }
                if (dt.Rows[0][1].ToString() != "")
                    Session["ReportDate"] = dt.Rows[0][1].ToString();
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                }
                return Json(new { Data2 = DateFormat }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
    }
}
