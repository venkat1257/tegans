﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MeBizSuiteAppUI.Controllers
{
    public class IPController : Controller
    {

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        public ActionResult IPCont()
        {
            return View();
        }
        public ActionResult getIpAddress()
        {
            try
            {

                //string VisitorsIPAddr = string.Empty;
                //string externalip = new System.Net.WebClient().DownloadString("http://bot.whatismyipaddress.com");
                string myHost = String.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? Request.ServerVariables["REMOTE_ADDR"] : Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(',')[0];
                //string myHost = System.Net.Dns.GetHostName();
                string myIP = "";
                //myIP = System.Net.Dns.GetHostEntry(myHost).AddressList[0].ToString();
                IPAddress[] ipaddress = Dns.GetHostAddresses(System.Net.Dns.GetHostName());
                foreach (IPAddress ip4 in ipaddress.Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork))
            {
                myIP+=ip4.ToString();
                } 
                IPHostEntry localIPs = Dns.Resolve(System.Environment.MachineName);
                IPAddress[] MyIp = localIPs.AddressList;
                string IpAddress = " 1 " + System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString() +
                    " 2 " + System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString() + " 3 " + Request.UserHostAddress + " 4 " + MyIp[0]
                    + " 5 " + Dns.GetHostAddresses(System.Net.Dns.GetHostName())[0].ToString() 
                    + " 6 " + Dns.GetHostAddresses(System.Net.Dns.GetHostName())[1].ToString() + " 7 " + myIP
                    + " 8 " + myHost;
                //string ComputerName = System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName;
                string ComputerName = System.Net.Dns.GetHostName();
                return Json(new { IpAddress = IpAddress, ComputerName = ComputerName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

    }
}
