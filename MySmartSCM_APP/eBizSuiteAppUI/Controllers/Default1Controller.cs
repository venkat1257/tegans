﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;
namespace MeBizSuiteAppUI.Controllers
{
    public class CustomerSearchController : Controller
    {
        //
        // GET: /Default1/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Create()
        {
            try
            {
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN05"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Phoneno = new List<SelectListItem>();
                SqlDataAdapter cmddl1 = new SqlDataAdapter("select COLUMN11 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata1 = new DataTable();
                cmddl1.Fill(dtdata1);
                for (int dd = 0; dd < dtdata1.Rows.Count; dd++)
                {
                    Phoneno.Add(new SelectListItem { Value = dtdata1.Rows[dd]["COLUMN11"].ToString(), Text = dtdata1.Rows[dd]["COLUMN11"].ToString() });
                }
                ViewData["Phoneno"] = new SelectList(Phoneno, "Value", "Text");
                //List<SelectListItem> Email = new List<SelectListItem>();
                //SqlDataAdapter cmddl1 = new SqlDataAdapter("select COLUMN02,COLUMN10 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                //DataTable dtdata1 = new DataTable();
                //cmddl1.Fill(dtdata1);
                //for (int dd = 0; dd < dtdata1.Rows.Count; dd++)
                //{
                //    Email.Add(new SelectListItem { Value = dtdata1.Rows[dd]["COLUMN02"].ToString(), Text = dtdata1.Rows[dd]["COLUMN10"].ToString() });
                //}
                //ViewData["Email"] = new SelectList(Email, "Value", "Text");
                //List<SelectListItem> Phone = new List<SelectListItem>();
                //SqlDataAdapter cmddl2 = new SqlDataAdapter("select COLUMN02,COLUMN11 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                //DataTable dtdata2 = new DataTable();
                //cmddl2.Fill(dtdata2);
                //for (int dd = 0; dd < dtdata2.Rows.Count; dd++)
                //{
                //    Phone.Add(new SelectListItem { Value = dtdata2.Rows[dd]["COLUMN02"].ToString(), Text = dtdata2.Rows[dd]["COLUMN11"].ToString() });
                //}
                //ViewData["Phone"] = new SelectList(Phone, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        //public ActionResult GetEmailDetails(string EmailID)
        //{

        //    var OPUnit = Session["OPUnit"];
        //    var AcOwner = Session["AcOwner"];
        //    SqlCommand cmd = new SqlCommand("select column10 from satable002  and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    if (dt.Rows.Count > 0) EmailID = dt.Rows[0][0].ToString();
        //    else { EmailID = "0"; }
        //    return Json(new { Data = EmailID }, JsonRequestBehavior.AllowGet);

        //}
        public ActionResult GetPhoneNo(string Phone)
        {

            var OPUnit = Session["OPUnit"];
            var AcOwner = Session["AcOwner"];
            SqlCommand cmd1 = new SqlCommand("select column11 from SATABLE002 and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            if (dt1.Rows.Count > 0) Phone = dt1.Rows[0][0].ToString();
            else { Phone = "0"; }
            return Json(new { Data = Phone }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetCustomerDetails(string Customer, string Phoneno,string Address)
        {
            try
            {
            var OPunit = Session["OPunit"];
            var AcOwner = Session["AcOwner"];
            SqlCommand cmd = new SqlCommand("CustomerSearch", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Customer", Customer);
            cmd.Parameters.AddWithValue("@Phoneno", Phoneno);
            cmd.Parameters.AddWithValue("@Address", Address);
            cmd.Parameters.AddWithValue("@AcOwner", AcOwner);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            string htmlstring = null, htmlstring1 = null;
            for (int g = 0; g < dt.Rows.Count; g++)
            {
                htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dCustomer Name>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dContact>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dPhone#>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dCity>" + dt.Rows[g][dt.Columns[3].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[3].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dAdvance>" + dt.Rows[g][dt.Columns[4].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[4].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dCredit Amount>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dDue Amount>" + dt.Rows[g][dt.Columns[6].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dOverdue Days >" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[7].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[7].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[7].ColumnName] + "' ></td>";
                htmlstring += "<td class=rowshow><div class=initshow><span id=dLast Business Activity Performed On >" + dt.Rows[g][dt.Columns[8].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[8].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[8].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[8].ColumnName] + "' ></td>";
            }
            return Json(new { Data = htmlstring, Data1 = ViewBag.Search }, JsonRequestBehavior.AllowGet);
          }
          catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

        }
}
    }
}

