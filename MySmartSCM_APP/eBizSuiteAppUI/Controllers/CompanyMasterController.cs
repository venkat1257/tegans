﻿
using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;

using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Data.SqlClient;


namespace eBizSuiteProduct.Controllers
{
    public class CompanyMasterController : Controller
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        string[] theader;
        string[] theadertext;
        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            try 
            {

                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                var ttid = tid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/CompanyMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    } theader[a] = tblcol; theadertext[a] = dcol;

                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols;
                ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                var ac = Convert.ToInt32(Session["AcOwner"]); string ounit = null;
                if (ac != 56567)
                    ounit = " (a.COLUMNA02 in(" + Session["OPUnit"] + ") or a.COLUMNA02 is null) ";
                else
                    ounit = "  a.COLUMNA02 is null ";
                var query1 = "Select a.COLUMN02,a.COLUMN03,a.COLUMN04,a.COLUMN05,a.COLUMN06,a.COLUMN07,a.COLUMN08,a.COLUMN09,a.COLUMN10,a.COLUMN11,a.COLUMN12,st.COLUMN03 COLUMN13,a.COLUMN14,ct.COLUMN03 COLUMN15,a.COLUMN24,a.COLUMN25,a.COLUMN26,a.COLUMN27 from CONTABLE008 a  left join MATABLE017 st on st.COLUMN02=a.COLUMN13 left join MATABLE016 ct on ct.COLUMN02=a.COLUMN15 where a.COLUMN05='False' and a.COLUMNA13='False' AND  " + ounit + " AND a.COLUMNA03='" + Session["AcOwner"] + "'";
                //"Select * from CONTABLE008 where COLUMN05='False' and COLUMNA13='False' AND " + ounit + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                ViewBag.itemsdata = books;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
   
        public ActionResult Create()
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var country = activity.MATABLE016.ToList();
                ViewBag.country = country;
                var state = activity.MATABLE017.ToList();
                ViewBag.state = state;

                //var StateCode = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11203).ToList();
                //ViewBag.Scode = StateCode;

                var sql = "select COLUMN02,(COLUMN06+'-'+COLUMN05) as COLUMN04 from MATABLE017 where COLUMN04 =1007 order by COLUMN04 asc";
                var StateCode = dbs.Query(sql);
                ViewBag.Scode = StateCode;
                CONTABLE008 all = new CONTABLE008();
                var col2 = dbContext.CONTABLE008.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                all.COLUMN02 = (col2.COLUMN02 + 1);
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            }
        }
     
        [HttpPost]
        public ActionResult Create(CONTABLE008 info, string Save, string SaveNew, HttpPostedFileBase hb)
        {
            try
            {
                var ac = Convert.ToInt32(Session["AcOwner"]);
                int? op = null;
                if (ac == 56567)
                {
                    info.COLUMNA02 = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(Convert.ToString(Session["OPUnitstatus"]) as string))
                    { info.COLUMNA02 = null; }
                    else
                    { info.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]); op = Convert.ToInt32(Session["OPUnit"]); }
                }
                var image = Request.Files[0] as HttpPostedFileBase;
                if (image != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        if (image.ContentLength > 0)
                        {
                            string fileName = Path.GetFileName(image.FileName);
                            string path = Path.Combine(Server.MapPath("~/Content/Upload/"), fileName);
                            image.SaveAs(path);
                            info.COLUMN28 = fileName;
                            var logo = dbContext.CONTABLE008.Where(a => a.COLUMNA02 == op && a.COLUMNA03 == ac && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                            if (logo != null)
                            {
                                if (ac != 56567)
                                    Session["logo"] = fileName;
                            }
                        }
                    }
                }
               // info.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                info.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                info.COLUMNA06 = DateTime.Now;
                info.COLUMNA07 = DateTime.Now;
                info.COLUMNA12 = true;
                info.COLUMNA13 = false;
                if (info.COLUMN06 == "--Select--") 
                    info.COLUMN06 = null;
                dbContext.CONTABLE008.Add(info);
                dbContext.SaveChanges();
                if (Convert.ToString(Request["savetype"]) == "New")
                {
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1242 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                         msg = "Company Successfully Created.......... ";
                    }
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    return RedirectToAction("Info");
                }
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1242 && q.COLUMN05 == 0).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Company Creation Failed .........";
                }
            
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
              // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var country = activity.MATABLE016.ToList();
                ViewBag.country = country;
                var state = activity.MATABLE017.ToList();
                ViewBag.state = state;
                //var StateCode = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11203).ToList();
                //ViewBag.Scode = StateCode;
                var sql = "select COLUMN02,(COLUMN06+'-'+COLUMN05) as COLUMN04 from MATABLE017 where COLUMN04 =1007 order by COLUMN04 asc";
                var StateCode = dbs.Query(sql);
                ViewBag.Scode = StateCode;
                CONTABLE008 edit = dbContext.CONTABLE008.Find(id);
                var col2 = edit.COLUMN02;
                Session["time"] = dbContext.CONTABLE008.Find(col2).COLUMNA06;
                return View(edit);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult Edit(CONTABLE008 info, HttpPostedFileBase hb)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var ac = Convert.ToInt32(Session["AcOwner"]);
                    int? op = null;
                    if (ac == 56567)
                    {
                        info.COLUMNA02 = null;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(Session["OPUnitstatus"]) as string))
                            info.COLUMNA02 = null;
                        else
                        { info.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]); op = Convert.ToInt32(Session["OPUnit"]); }
                    }
                    var image = Request.Files[0] as HttpPostedFileBase;
                    if (image != null)
                    {
                        if (Request.Files.Count > 0)
                        {
                            if (image.ContentLength > 0)
                            {
                                string fileName = Path.GetFileName(image.FileName);
                                string path = Path.Combine(Server.MapPath("~/Content/Upload/"), fileName);
                                image.SaveAs(path);
                                info.COLUMN28 = fileName;
                                var logo = dbContext.CONTABLE008.Where(a => a.COLUMNA02 == op && a.COLUMNA03 == ac && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                                if (logo != null)
                                {
                                    if (ac != 56567)
                                        Session["logo"] = fileName;
                                }
                            }
                            else
                            {
                                var logoname=dbContext.CONTABLE008.Where(a => a.COLUMN02 == info.COLUMN02 &&a.COLUMNA02 == op && a.COLUMNA03 == ac && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                                if (logoname != null)
                                    info.COLUMN28 = logoname; 
                            }
                        }
                    }
                    dbContext.Entry(info).State = EntityState.Modified;
                    info.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                    info.COLUMNA12 = true;
                    info.COLUMNA13 = false;
                    info.COLUMNA07 = DateTime.Now;
                    if (info.COLUMN06 == "--Select--")
                        info.COLUMN06 = null;
                    //info.COLUMNA06 = Convert.ToDateTime(Session["time"]); Session["time"] = null;
                    dbContext.SaveChanges();
                }
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1242 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Company Successfully Modified.......... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    return RedirectToAction("Info");
            }
            catch(Exception ex)
            {
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1242 && q.COLUMN05 == 3).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Company Modification Failed .........";
                    }
               
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
         
            }
        }
       
        public ActionResult Detailes(int id)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var country = activity.MATABLE016.ToList();
                ViewBag.country = country;
                var state = activity.MATABLE017.ToList();
                ViewBag.state = state;
                //var StateCode = activity.MATABLE002.Where(a => a.COLUMNA13 == false && a.COLUMN03 == 11203).ToList();
                //ViewBag.Scode = StateCode;
                var sql = "select COLUMN02,(COLUMN06+'-'+COLUMN05) as COLUMN04 from MATABLE017 where COLUMN04 =1007 order by COLUMN04 asc";
                var StateCode = dbs.Query(sql);
                ViewBag.Scode = StateCode;
                
                CONTABLE008 Details = dbContext.CONTABLE008.Find(id);

                return View(Details);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                CONTABLE008 del = dbContext.CONTABLE008.Find(id);

                var y = (from x in dbContext.CONTABLE008 where x.COLUMN02 == id select x).First();

                y.COLUMNA13 = true;
                y.COLUMNA12 = false;

                dbContext.SaveChanges();
                var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1242 && q.COLUMN05 == 4).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Comapany Successfully Deleted.......... ";
                    }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
            return RedirectToAction("Info");
            }
            catch(Exception ex)
            {
                    var msg = string.Empty;
                    var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1242 && q.COLUMN05 == 5).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Comapany Deletion Failed .........";
                   
                    }
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            
            }
        }
        //c
        [HttpPost]
        public ActionResult UpdateInline(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list);
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                    var c = dc.CONTABLE008.Where(a => a.COLUMN02 == eid).FirstOrDefault();
                    c.COLUMN03 = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    c.COLUMN06 = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    c.COLUMN07 = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    c.COLUMN08 = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    c.COLUMN21 = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    c.COLUMN22 = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[7].ToString());
                    c.COLUMN24 = ds.Tables[0].Rows[i].ItemArray[8].ToString();
                    c.COLUMN25 = ds.Tables[0].Rows[i].ItemArray[9].ToString();

                    c.COLUMNA07 = DateTime.Now;

                    dc.SaveChanges();
                }

                return RedirectToAction("Info");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            }
        }
       
        public ActionResult Search()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var CompanyID = Request["CompanyID"];
                var Name = Request["Name"];
                var BusinessType = Request["BusinessType"];
                var Email = Request["Email"];

                if (CompanyID != null && CompanyID != "" && CompanyID != string.Empty)
                {
                    CompanyID = CompanyID + "%";
                }
                else
                    CompanyID = Request["CompanyID"];


                if (Name != null && Name != "" && Name != string.Empty)
                {
                    Name = Name + "%";
                }
                else
                    Name = Request["Name"];


                if (BusinessType != null && BusinessType != "" && BusinessType != string.Empty)
                {
                    BusinessType = BusinessType + "%";
                }
                else
                    BusinessType = Request["BusinessType"];

                if (Email != null && Email != "" && Email != string.Empty)
                {
                    Email = Email + "%";
                }
                else
                    Email = Request["Email"];

                var query = "SELECT * FROM CONTABLE008  WHERE COLUMN02 like '" + CompanyID + "' or COLUMN03 LIKE '" + Name + "' or COLUMN06 LIKE '" + BusinessType + "'  or COLUMN07 LIKE '" + Email + "'";

                var query1 = dbContext.CONTABLE008.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.gdata = gdata;

                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();

                ViewBag.itemsdata = Session["books"];
                ViewBag.theadertext = Session["th"];
                ViewBag.cols = Session["cols"];
                ViewBag.itemscol = Session["theader"];
                return View("Info", gdata);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            }
        }

        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult Style(string items, string inactive, string view, string sort)
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                List<CONTABLE008> all = new List<CONTABLE008>();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                var data = from e in dbContext.CONTABLE008.AsEnumerable() select new { Company_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, InActive = e.COLUMN05, Business_Type = e.COLUMN06, Email = e.COLUMN07, Attention = e.COLUMN08, Addressee = e.COLUMN09, Address1 = e.COLUMN10, Address2 = e.COLUMN11, City = e.COLUMN12, State = e.COLUMN13, Zip = e.COLUMN14, Country = e.COLUMN15, RAttention = e.COLUMN16, RAddressee = e.COLUMN17, RAddress1 = e.COLUMN18, RAddress2 = e.COLUMN19, RCity = e.COLUMN20, RState = e.COLUMN21, RZip = e.COLUMN22, RCountry = e.COLUMN23, Info1 = e.COLUMN24, Info2 = e.COLUMN25, Info3 = e.COLUMN26 };
                List<CONTABLE008> tbldata = new List<CONTABLE008>();
                tbldata = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList();
                if (items == "Normal")
                {
                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                    var indata = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from CONTABLE008 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from CONTABLE008 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    } int r = 0; string rowColor = "";
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            r = r + 1;

                            if (r % 2 == 0)
                            {
                                rowColor = "alt";
                            }
                            else
                            {
                                rowColor = "alt0";
                            }

                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/CompanyMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + itm[Inline] + " >View</a></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                            }
                            else
                            {
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                            }


                        }
                        tthdata = "<tr class=" + @rowColor + " style='color:black'>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    grid = new WebGrid(null, canPage: false, canSort: false);

                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/CompanyMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            }
        }

        //string tthdata, tthtext;
        //string itthdata, itthtext;
        //public ActionResult Style(string items, string inactive, string view, string sort)
        //{
        //    List<WebGridColumn> cols = new List<WebGridColumn>();
        //    List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
        //    List<CONTABLE008> all = new List<CONTABLE008>();
        //    var grid = new WebGrid(null, canPage: false, canSort: false);
        //    var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //    var tbtid = tbid.COLUMN02;
        //    var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
        //    ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
        //    var data = from e in dbContext.CONTABLE008.AsEnumerable() select new { Company_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, InActive = e.COLUMN05, Business_Type = e.COLUMN06, Email = e.COLUMN07, Attention = e.COLUMN08, Addressee = e.COLUMN09, Address1 = e.COLUMN10, Address2 = e.COLUMN11, City = e.COLUMN12, State = e.COLUMN13, Zip = e.COLUMN14, Country = e.COLUMN15, RAttention = e.COLUMN16, RAddressee = e.COLUMN17, RAddress1 = e.COLUMN18, RAddress2 = e.COLUMN19, RCity = e.COLUMN20, RState = e.COLUMN21, RZip = e.COLUMN22, RCountry = e.COLUMN23, Info1 = e.COLUMN24, Info2 = e.COLUMN25, Info3 = e.COLUMN26 };
        //    List<CONTABLE008> tbldata = new List<CONTABLE008>();
        //    tbldata = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList();
        //    if (items == "Normal")
        //    {
        //        var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //        var Attid = Atid.COLUMN02;
        //        var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
        //        var indata = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
        //        var query1 = "";
        //        if (inactive != null)
        //        {
        //            query1 = "Select * from CONTABLE008 where COLUMNA13='True' and COLUMNA12 = 'False'   ";
        //        }
        //        else
        //        {
        //            query1 = "Select * from CONTABLE008 where COLUMNA13='False' ";
        //        }
        //        var db2 = Database.Open("sqlcon");
        //        var books = db2.Query(query1);
        //        if (sort == "Recently Created")
        //        {
        //            books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        else if (sort == "Recently Modified")
        //        {
        //            books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
        //        var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
        //        theader = new string[Acolslist.Count];
        //        theadertext = new string[Acolslist.Count];
        //        for (int a = 0; a < Acolslist.Count; a++)
        //        {
        //            var acol = Acolslist[a].ToString();
        //            var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
        //            var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

        //            var tblcol = columndata.COLUMN04;
        //            var dcol = Ccolslist[a].ToString();
        //            theader[a] = tblcol;
        //            theadertext[a] = dcol;
        //            if (a == 0)
        //            {
        //                tthtext += "<th>Action</th>";
        //                tthtext += "<th>" + dcol + "</th>";
        //                itthtext += "<th>" + dcol + "</th>";
        //            }
        //            else
        //            {
        //                tthtext += "<th>" + dcol + "</th>";
        //                itthtext += "<th>" + dcol + "</th>";
        //            }
        //        } int r = 0; string rowColor = "";
        //        foreach (var itm in books)
        //        {
        //            foreach (var Inline in theader)
        //            {
        //                r = r + 1;

        //                if (r % 2 == 0)
        //                {
        //                    rowColor = "alt";
        //                }
        //                else
        //                {
        //                    rowColor = "alt0";
        //                }

        //                if (Inline == "COLUMN02")
        //                {
        //                    tthdata += "<td><a href=/CompanyMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + itm[Inline] + " >View</a></td>";
        //                    tthdata += "<td>" + itm[Inline] + "</td>";
        //                    itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
        //                }
        //                else
        //                {
        //                    tthdata += "<td>" + itm[Inline] + "</td>";
        //                    itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
        //                }


        //            }
        //            tthdata = "<tr class=" + @rowColor + " style='color:black'>" + tthdata + "</tr>";
        //            itthdata = "<tr>" + itthdata + "</tr>";
        //        }
        //        ViewBag.cols = cols;
        //        ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
        //        ViewBag.itemscol = theader;
        //        ViewBag.theadertext = theadertext;
        //        Session["cols"] = cols;
        //        Session["Inlinecols"] = Inlinecols;
        //        ViewBag.itemsdata = null;
        //        ViewBag.itemsdata = books;
        //        var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
        //        var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
        //        return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        grid = new WebGrid(null, canPage: false, canSort: false);

        //        if (inactive != null)
        //        {
        //            all = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == true).ToList();
        //        }
        //        else
        //        {
        //            all = all.Where(a => a.COLUMN02 != null).ToList();
        //        }

        //        var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //        var Attid = Atid.COLUMN02;
        //        var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
        //        var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
        //        var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
        //        for (int a = 0; a < Acolslist.Count; a++)
        //        {
        //            var acol = Acolslist[a].ToString();
        //            var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
        //            var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

        //            var tblcol = columndata.COLUMN04;
        //            var dcol = Ccolslist[a].ToString();
        //            if (a == 0)
        //            {
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = "Action",
        //                    Format = (item) => new HtmlString("<a href=/CompanyMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + item[tblcol] + " >View</a>")

        //                });
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol
        //                });
        //                Inlinecols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol,
        //                    Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
        //                });
        //            }
        //            else
        //            {
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol
        //                });
        //                Inlinecols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol,
        //                    Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
        //                });
        //            }
        //        }
        //        ViewBag.cols = cols;
        //        ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
        //        ViewBag.columnscount = Inlinecols.Count;
        //        Session["cols"] = cols;
        //        Session["Inlinecols"] = Inlinecols;
        //        grid = new WebGrid(all, canPage: false, canSort: false);
        //        var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
        //     headerStyle: "webgrid-header",
        //     footerStyle: "webgrid-footer",
        //     alternatingRowStyle: "webgrid-alternating-row",
        //     rowStyle: "webgrid-row-style",
        //                         htmlAttributes: new { id = "DataTable" },
        //                         columns: cols);
        //        var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
        //     headerStyle: "webgrid-header",
        //     footerStyle: "webgrid-footer",
        //     alternatingRowStyle: "webgrid-alternating-row",
        //     rowStyle: "webgrid-row-style",
        //                         htmlAttributes: new { id = "grdData" },
        //                         columns: Inlinecols);
        //        return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        //    }
        //    return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList());
        //}

        public ActionResult CustomView()
        {
            try
            {
                List<CONTABLE005> all = new List<CONTABLE005>();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                //all = from e in dbContext.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
                var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
                all = dbContext.CONTABLE005.SqlQuery(query).ToList();

                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            }
        }
        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
            int col2 = 0;
            var tbldata = dbContext.CONTABLE004.Where(a => a.COLUMN05 == "Company").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = dbContext.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    dbContext.CONTABLE013.Add(dd);
                    dbContext.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        dbContext.CONTABLE013.Add(dd);
                        dbContext.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult View(string items, string sort, string inactive, string style)
        {
            try
            {
                List<CONTABLE008> all = new List<CONTABLE008>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                var ttid = tid.COLUMN02;
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (style == "Normal")
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var indata = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from CONTABLE008 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from CONTABLE008 where COLUMNA13='False' ";
                    }
                    var db2 = Database.Open("sqlcon");
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/CompanyMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + itm[Inline] + " >View</a></td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                            else
                            {
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                    return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

                }
                else
                {
                    var grid = new WebGrid(null, canPage: false, canSort: false);
                    var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                    var tbtid = tbid.COLUMN02;
                    var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                    ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                    if (sort == "Recently Created")
                    {
                        all = dbContext.CONTABLE008.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        all = dbContext.CONTABLE008.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else
                    {
                        all = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList();

                    }
                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/CompanyMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        //public ActionResult View(string items, string sort, string inactive, string style)
        //{
        //    List<CONTABLE008> all = new List<CONTABLE008>();
        //    List<WebGridColumn> cols = new List<WebGridColumn>();
        //    List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
        //    var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //    var ttid = tid.COLUMN02;
        //    var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
        //    ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
        //    if (style == "Normal")
        //    {
        //        all = all.Where(a => a.COLUMNA12 == true).ToList();
        //        var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //        var Attid = Atid.COLUMN02;
        //        var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
        //        var indata = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
        //        var query1 = "";
        //        if (inactive != null)
        //        {
        //            query1 = "Select * from CONTABLE008 where COLUMNA13='True' and COLUMNA12 = 'False'  ";
        //        }
        //        else
        //        {
        //            query1 = "Select * from CONTABLE008 where COLUMNA13='False' and COLUMNA12 = 'False' ";
        //        }
        //        var db2 = Database.Open("sqlcon");
        //        var books = db2.Query(query1);
        //        if (sort == "Recently Created")
        //        {
        //            books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        else if (sort == "Recently Modified")
        //        {
        //            books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
        //        var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
        //        theader = new string[Acolslist.Count];
        //        theadertext = new string[Acolslist.Count];
        //        for (int a = 0; a < Acolslist.Count; a++)
        //        {
        //            var acol = Acolslist[a].ToString();
        //            var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
        //            var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

        //            var tblcol = columndata.COLUMN04;
        //            var dcol = Ccolslist[a].ToString();
        //            theader[a] = tblcol;
        //            theadertext[a] = dcol;
        //            if (a == 0)
        //            {
        //                tthtext += "<th>Action</th>";
        //                tthtext += "<th>" + dcol + "</th>";
        //                itthtext += "<th>" + dcol + "</th>";
        //            }
        //            else
        //            {
        //                tthtext += "<th>" + dcol + "</th>";
        //                itthtext += "<th>" + dcol + "</th>";
        //            }
        //        }
        //        foreach (var itm in books)
        //        {
        //            foreach (var Inline in theader)
        //            {
        //                if (Inline == "COLUMN02")
        //                {
        //                    tthdata += "<td><a href=/CompanyMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + itm[Inline] + " >View</a></td>";
        //                    itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
        //                    tthdata += "<td>" + itm[Inline] + "</td>";
        //                }
        //                else
        //                {
        //                    itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
        //                    tthdata += "<td>" + itm[Inline] + "</td>";
        //                }
        //            }
        //            tthdata = "<tr>" + tthdata + "</tr>";
        //            itthdata = "<tr>" + itthdata + "</tr>";
        //        }
        //        ViewBag.cols = cols;
        //        ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
        //        ViewBag.itemscol = theader;
        //        ViewBag.theadertext = theadertext;
        //        Session["cols"] = cols;
        //        Session["Inlinecols"] = Inlinecols;
        //        ViewBag.itemsdata = null;
        //        ViewBag.itemsdata = books;
        //        var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
        //        var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
        //        return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
        //        return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

        //    }
        //    else
        //    {
        //        var grid = new WebGrid(null, canPage: false, canSort: false);
        //        var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //        var tbtid = tbid.COLUMN02;
        //        var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
        //        ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


        //        if (sort == "Recently Created")
        //        {
        //            all = dbContext.CONTABLE008.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        else if (sort == "Recently Modified")
        //        {
        //            all = dbContext.CONTABLE008.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        else
        //        {
        //            all = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList();

        //        }
        //        if (inactive != null)
        //        {
        //            all = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == true).ToList();
        //        }
        //        else
        //        {
        //            all = all.Where(a => a.COLUMN02 != null).ToList();
        //        }

        //        var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
        //        var Attid = Atid.COLUMN02;
        //        var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
        //        var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
        //        var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
        //        for (int a = 0; a < Acolslist.Count; a++)
        //        {
        //            var acol = Acolslist[a].ToString();
        //            var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
        //            var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

        //            var tblcol = columndata.COLUMN04;
        //            var dcol = Ccolslist[a].ToString();
        //            if (a == 0)
        //            {
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = "Action",
        //                    Format = (item) => new HtmlString("<a href=/CompanyMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/CompanyMaster/Detailes/" + item[tblcol] + " >View</a>")

        //                });
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol
        //                });
        //                Inlinecols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol,
        //                    Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
        //                });
        //            }
        //            else
        //            {
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol
        //                });
        //                Inlinecols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol,
        //                    Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
        //                });
        //            }
        //        }
        //        ViewBag.cols = cols;
        //        ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
        //        ViewBag.columnscount = Inlinecols.Count;
        //        Session["cols"] = cols;
        //        Session["Inlinecols"] = Inlinecols;
        //        grid = new WebGrid(all, canPage: false, canSort: false);
        //        var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
        //     headerStyle: "webgrid-header",
        //     footerStyle: "webgrid-footer",
        //     alternatingRowStyle: "webgrid-alternating-row",
        //     rowStyle: "webgrid-row-style",
        //                         htmlAttributes: new { id = "DataTable" },
        //                         columns: cols);
        //        var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
        //     headerStyle: "webgrid-header",
        //     footerStyle: "webgrid-footer",
        //     alternatingRowStyle: "webgrid-alternating-row",
        //     rowStyle: "webgrid-row-style",
        //                         htmlAttributes: new { id = "grdData" },
        //                         columns: Inlinecols);
        //        return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        //    }
        //    return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList());
        //}

        public void ExportPdf()
        {
            try
            {
                List<CONTABLE008> all = new List<CONTABLE008>();
                all = dbContext.CONTABLE008.ToList();
                var data = from e in dbContext.CONTABLE008.AsEnumerable() select new { Company_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, InActive = e.COLUMN05, Business_Type = e.COLUMN06, Email = e.COLUMN07, Attention = e.COLUMN08, Addressee = e.COLUMN09, Address1 = e.COLUMN10, Address2 = e.COLUMN11, City = e.COLUMN12, State = e.COLUMN13, Zip = e.COLUMN14, Country = e.COLUMN15, RAttention = e.COLUMN16, RAddressee = e.COLUMN17, RAddress1 = e.COLUMN18, RAddress2 = e.COLUMN19, RCity = e.COLUMN20, RState = e.COLUMN21, RZip = e.COLUMN22, RCountry = e.COLUMN23, Info1 = e.COLUMN24, Info2 = e.COLUMN25, Info3 = e.COLUMN26 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gv.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
               // Exception Handling By Venkat
			    RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void Exportword()
        {
            try
            {
                List<CONTABLE008> all = new List<CONTABLE008>();
                all = dbContext.CONTABLE008.ToList();
                var data = from e in dbContext.CONTABLE008.AsEnumerable() select new { Company_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, InActive = e.COLUMN05, Business_Type = e.COLUMN06, Email = e.COLUMN07, Attention = e.COLUMN08, Addressee = e.COLUMN09, Address1 = e.COLUMN10, Address2 = e.COLUMN11, City = e.COLUMN12, State = e.COLUMN13, Zip = e.COLUMN14, Country = e.COLUMN15, RAttention = e.COLUMN16, RAddressee = e.COLUMN17, RAddress1 = e.COLUMN18, RAddress2 = e.COLUMN19, RCity = e.COLUMN20, RState = e.COLUMN21, RZip = e.COLUMN22, RCountry = e.COLUMN23, Info1 = e.COLUMN24, Info2 = e.COLUMN25, Info3 = e.COLUMN26 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
                Response.ContentType = "application/vnd.ms-word ";
                Response.Charset = string.Empty;
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void ExportCSV()
        {
            try
            {
                StringWriter sw = new StringWriter();

                var y = dbContext.CONTABLE008.OrderBy(q => q.COLUMN02).ToList();

                sw.WriteLine("\"Company_ID\",\"Name\",\"Description\",\"Inactive\",\"Business_Type\",\"Email\",\"Attention\",\"Addressee\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Country\",\"Attention\",\"Addressee\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Country\",\"Info1\",\"Info2\",\"Info3\"");

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
                Response.ContentType = "text/csv";

                foreach (var line in y)
                {

                    sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\",\"{19}\",\"{20}\",\"{21}\",\"{22}\",\"{23}\",\"{24}\"",
                                               line.COLUMN02,
                                               line.COLUMN03,
                                               line.COLUMN04,
                                               line.COLUMN05,
                                               line.COLUMN06,
                                               line.COLUMN07,
                                               line.COLUMN08,
                                               line.COLUMN09,
                                               line.COLUMN10,
                                               line.COLUMN11,
                                               line.COLUMN12,
                                               line.COLUMN13,
                                               line.COLUMN14,
                                               line.COLUMN15,
                                               line.COLUMN16,
                                               line.COLUMN17,
                                               line.COLUMN18,
                                               line.COLUMN19,
                                               line.COLUMN20,
                                               line.COLUMN21,
                                               line.COLUMN22,
                                               line.COLUMN23,
                                               line.COLUMN24,
                                               line.COLUMN25,
                                               line.COLUMN26));

                }

                Response.Write(sw.ToString());

                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }

        }

        public void ExportExcel()
        {
            try
            {
                List<CONTABLE008> all = new List<CONTABLE008>();
                all = dbContext.CONTABLE008.ToList();
                var data = from e in dbContext.CONTABLE008.AsEnumerable() select new { Company_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, InActive = e.COLUMN05, Business_Type = e.COLUMN06, Email = e.COLUMN07, Attention = e.COLUMN08, Addressee = e.COLUMN09, Address1 = e.COLUMN10, Address2 = e.COLUMN11, City = e.COLUMN12, State = e.COLUMN13, Zip = e.COLUMN14, Country = e.COLUMN15, RAttention = e.COLUMN16, RAddressee = e.COLUMN17, RAddress1 = e.COLUMN18, RAddress2 = e.COLUMN19, RCity = e.COLUMN20, RState = e.COLUMN21, RZip = e.COLUMN22, RCountry = e.COLUMN23, Info1 = e.COLUMN24, Info2 = e.COLUMN25, Info3 = e.COLUMN26 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                gv.FooterRow.Visible = false;
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter ht = new HtmlTextWriter(sw);
                gv.RenderControl(ht);
                Response.Write(sw.ToString());
                Response.End();
                gv.FooterRow.Visible = true;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult Export(string items)
        {
            try
            {
                List<CONTABLE008> all = new List<CONTABLE008>();
                all = dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList();
                var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE008").First();
                var ttid = tid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (items == "PDF")
                {
                    ExportPdf();
                }
                else if (items == "Excel")
                {
                    ExportExcel();
                }
                else if (items == "Word")
                {
                    Exportword();
                }
                else if (items == "CSV")
                {
                    ExportCSV();
                }
                return View("Info", dbContext.CONTABLE008.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                // Exception Handling By Venkat
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult getStateList(int countryId)
        {
            try
            {
                string dSval = "";

                List<SelectListItem> States = new List<SelectListItem>();
                //var dropdata = dc.Where(a => a.COLUMN04 == countryId).ToList();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var dropdata = "select COLUMN02,COLUMN03 FROM MATABLE017 where COLUMN04 =" + countryId;
                SqlConnection cn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(dropdata, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    States.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN03"].ToString() });
                }
                ViewBag.sList = new SelectList(States, "Value", "Text", selectedValue: dSval);
                return Json(new { item = States }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "CompanyMaster", new { FormName = Session["FormName"] });
            }
        }
    }
}