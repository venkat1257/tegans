﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBizSuiteAppModel.Table;
using System.Data.Entity;
using MvcMenuMaster.Models;
using System.ComponentModel.DataAnnotations;

namespace MvcMenuMaster.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/
        public eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        public ActionResult Index()
        {
            return View();
        }
        static List<eBizSuiteTableEntities> Menu = new List<eBizSuiteTableEntities>();
        
        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(MenuItems menu)
        {
            string Parent1 = Request["Parent"];
                MenuItems items = new MenuItems();
                Menu_Master menu1 = new Menu_Master();
                int centerid = Convert.ToInt32(Request["CenterId"]);
                string MenuName = Request["MenuName"];
                int ModuleId = Convert.ToInt32(Request["ModuleId"]);
                string Parent = Request["Parent"];
                //int? Parent = Convert.ToInt32(Request["Parent"]);
                int PriorityLevel = Convert.ToInt32(Request["PriorityLevel"]);
                string IsEnable = Request["IsEnable"];
                string IsFormApplicable = Request["IsFormApplicable"];
                eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
                //var db1 = dc.MenuMasters.Where(a => a.MenuName == Parent).ToList();
                var db1 = dc.Menu_Master.Where(a => a.MenuName == Parent).ToList();
                menu1.CenterId = centerid;
                menu1.MenuName = MenuName;
                menu1.ModuleId = ModuleId;
                if (string.IsNullOrEmpty(Parent)||Parent=="null")
                {
                    menu1.Parent = null;
                }
                else
                {
                int mid=db1.First().MenuId;
                    int cent = (mid);
                    menu1.Parent = cent;
                }
                menu1.PriorityLevel = PriorityLevel;
                menu1.IsEnable = IsEnable;
                menu1.IsFormApplicable = IsFormApplicable;
                db.Menu_Master.Add(menu1);
                db.SaveChanges();
                ViewBag.Message = "Data has been saved succeessfully";
                return RedirectToAction("Index");
            //return View();
            //return View("Create", menu);
        }
        public ActionResult DropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Select", Value = "0", Selected = true });
            items.Add(new SelectListItem { Text="101",  Value="1" });
            items.Add(new SelectListItem { Text = "102", Value = "2" });
            items.Add(new SelectListItem { Text = "103", Value = "3" });
            ViewBag.Data = items;
            return View();
        }
    }
}
