﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using eBizSuiteAppModel.Table;
using System.Web.Helpers;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class BarcodeTemplateController : Controller
    {
        //
        // GET: /BarcodeTemplate/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        string providerName = "System.Data.SqlClient";
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Template()
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult ItemDetailsFill()
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var customfrmID = 1261; var tblid = 110008817;
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                SqlCommand acmd = new SqlCommand(
                                   "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15,CONTABLE006.COLUMN13 From CONTABLE006  " +
                                 " Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                                 " left join SETABLE011 s on s.COLUMN04 = " + tblid + " and CONTABLE006.COLUMN05 =s.COLUMN05 and s.COLUMN06 = " + customfrmID + "  " +
                                 " left join SETABLE012 s1 on s.COLUMN04 = s1.COLUMN04  and s1.COLUMN05 =s.COLUMN05 and s.COLUMN06 = s1.COLUMN06 and s1.COLUMNA03 = " + Session["AcOwner"] + " " +
                                 " where CONTABLE005.COLUMN03 =" + tblid + " and CONTABLE006.COLUMN04 =" + tblid + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04   and CONTABLE006.COLUMN06!='Refered Form'   and CONTABLE006.COLUMN03=" + customfrmID + " and  iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)='Y'  ORDER BY CAST(CONTABLE006.COLUMN13 AS INT) ", cn);
                SqlDataAdapter ada = new SqlDataAdapter(acmd);
                DataTable adt = new DataTable();
                ada.Fill(adt);
                List<SelectListItem> Country = new List<SelectListItem>();
                for (int dd = 0; dd < adt.Rows.Count; dd++)
                {
                    DataRow dtr = adt.Rows[dd]; string ColVal = " p." + adt.Rows[dd]["COLUMN05"].ToString();
                    if (adt.Rows[dd]["COLUMN10"].ToString() == "DropDownList" || adt.Rows[dd]["COLUMN10"].ToString() == "DatePicker")
                        ColVal = tblreturn(dtr);
                    Country.Add(new SelectListItem { Value = ColVal, Text = adt.Rows[dd]["COLUMN06"].ToString() });
                }
                SqlCommand cmd = new SqlCommand("Select COLUMN03,COLUMN04,COLUMN05,COLUMN06 from CONTABLE005 where COLUMN03= 110014054 and (COLUMN04='COLUMN04' or COLUMN04='COLUMN05')  ", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int dd = 0; dd < dt.Rows.Count; dd++)
                {var type="Sales";
                var ColVal = " (select  top(1) " + dt.Rows[dd]["COLUMN04"].ToString() + " from MATABLE024 where column06=@Sales@ and column07=p.COLUMN02 and isnull(COLUMN04,0)>0 and (isnull(COLUMN03,0)=isnull(p.COLUMNA02,0) or isnull(COLUMN03,0)=0) and COLUMNA03=p.COLUMNA03 and isnull(COLUMNA13,0)=0)";
                Country.Add(new SelectListItem { Value = ColVal, Text = dt.Rows[dd]["COLUMN05"].ToString() });
                }
                ViewData["ItemConfig"] = new SelectList(Country, "Value", "Text");
                ViewBag.ItemConfig = ViewData["ItemConfig"];
                var items="";
                foreach (var Item in ViewBag.ItemConfig)
                {
                    items = items + "<li class='ui-state-default' itemid='" + Item.Value + "' title=" + Item.Text + ">" + Item.Text + "</li>";
                }
                return Json(new { items = items }, JsonRequestBehavior.AllowGet);
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult CompanyDetailsFill()
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var customfrmID = 1261; var tblid = 110008817;
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                SqlCommand cmdc = new SqlCommand("Select COLUMN03,COLUMN04,COLUMN05,COLUMN06 from CONTABLE005 where COLUMN03= 110000011 and len(COLUMN04)<=8 and COLUMN04!='COLUMN01' and COLUMN04!='COLUMN02' ", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                List<SelectListItem> Company = new List<SelectListItem>();
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    string ColVal = " (select  top(1) " + dtc.Rows[dd]["COLUMN04"].ToString() + " from CONTABLE008 where COLUMNA03=p.COLUMNA03 and isnull(COLUMN05,0)=0 and isnull(COLUMNA13,0)=0)";
                    Company.Add(new SelectListItem { Value = ColVal, Text = dtc.Rows[dd]["COLUMN05"].ToString() });
                }
                ViewData["Company"] = new SelectList(Company, "Value", "Text");
                ViewBag.Company = ViewData["Company"];
                var items = "";
                foreach (var Item in ViewBag.Company)
                {
                    items = items + "<li class='ui-state-default' itemid='" + Item.Value + "' title=" + Item.Text + ">" + Item.Text + "</li>";
                }
                return Json(new { items = items }, JsonRequestBehavior.AllowGet);
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        public string tblreturn(DataRow dr)
        {
            string strQry = ""; var col = "COLUMN04";
            var actcol = dr["COLUMN05"].ToString(); var aliascol = dr["COLUMN06"].ToString(); var ctblid = Convert.ToInt32(dr["COLUMN04"].ToString()); var dtblid = dr["COLUMN15"].ToString(); var ddata = dr["COLUMN15"].ToString(); var frmID = Convert.ToInt32(dr["COLUMN03"].ToString());
            var tbl = ""; var ltbl = ""; var alcol = "";
            var formlist = dc.CONTABLE006.Where(q => q.COLUMN04 == ctblid && q.COLUMN03 == frmID && q.COLUMN05 == actcol).FirstOrDefault();
            alcol = "p." + actcol;
            if (formlist.COLUMN10 == "DropDownList")
            {
                if (formlist.COLUMN14 == "Control Value") tbl = "MATABLE002";
                else
                    tbl = dc.CONTABLE004.Where(a => a.COLUMN02 == formlist.COLUMN15).FirstOrDefault().COLUMN04;
               
                if (tbl == "SATABLE001" || tbl == "SATABLE002") col = "COLUMN05";
                else if (tbl == "MATABLE010") col = "COLUMN09";
                else if (tbl == "CONTABLE008" || tbl == "CONTABLE007" || tbl == "CONTABLE009") col = "COLUMN03";
                strQry += "  (select " + col + " from " + tbl + " where column02=" + alcol + ")   ";

            }
            else if (formlist.COLUMN10 == "DatePicker")
            {
                col = "convert(varchar(11)," + alcol + ", 103)";
                strQry += " " + col + " ";
            }
            return strQry;
        }

        [HttpPost]
        public ActionResult Template(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert";
                cn.Open();
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE018 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodePrinterSetup", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1622");
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Name"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Order"]);
                string defaultflag = Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                string Message = Convert.ToString(col["Message"]);
                Message = Message.Replace("@Sales@", "'Sales'");
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMN07", Message);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["Barcode Labels"]);
                string Query = Convert.ToString(col["Barcode Query"]);
                Query = Query.Replace("@Sales@", "'Sales'");
                Cmd.Parameters.AddWithValue("@COLUMN09", Query);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE018");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1622 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1622 && q.COLUMN05 == 0).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Creation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult BarcodeTemplateInfo()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                var strQry = "SELECT p.[COLUMN02 ] AS ID ,p.[COLUMN04 ] AS [Name],p.[COLUMN05] AS [Order],p.[COLUMN06 ] AS [Default],m.COLUMN09 [Created By]" +
                " from SETABLE018 p  left join MATABLE010 m on m.COLUMN02=p.columna08 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 and (m.COLUMNA02=p.COLUMNA02 or m.COLUMNA02 is null) where isnull(p.COLUMNA13,'False')='False'  and " + OPUnit + " and (p.COLUMNA03=" + acID + " or p.COLUMNA03 is null) AND  p.COLUMN03=1622  ORDER BY p.COLUMNA06 desc ";
                alCol.AddRange(new List<string> { "ID", "Name", "Order", "Default","Created By" });
                actCol.AddRange(new List<string> { "COLUMN02","COLUMN04", "COLUMN05",  "COLUMN06",  "COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult BarcodeTemplateEdit()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                List<int> permission = Session["Permission"] as List<int>;
                if (permission.Contains(22933))
                {
                    SqlDataAdapter cmdv = new SqlDataAdapter("SELECT COLUMN04 Name,COLUMN05 [Order],COLUMN06 [Default],COLUMN07 Message,COLUMN08 Labels,COLUMN09 Query FROM SETABLE018 WHERE  COLUMN02=" + @Request["ide"] + "  and isnull(columna13,0)=0   and " + OPUnit + " and (COLUMNA03=" + acID + " or COLUMNA03 is null)", cn);
                    DataTable dtv = new DataTable();
                    cmdv.Fill(dtv);
                    List<POPrint> all1 = new List<POPrint>();
                    all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                    {
                        TempName = "",
                        TempType = "",
                        TempDefault = "",
                        TempMessage = "",
                        TempBody = "",
                        Memo = ""
                    });
                    all1[0].TempName = dtv.Rows[0]["Name"].ToString();
                    all1[0].TempType = dtv.Rows[0]["Order"].ToString();
                    all1[0].TempDefault = dtv.Rows[0]["Default"].ToString();
                    all1[0].TempMessage = dtv.Rows[0]["Message"].ToString();
                    all1[0].TempBody = dtv.Rows[0]["Labels"].ToString();
                    all1[0].Memo = dtv.Rows[0]["Query"].ToString();
                    return View(all1);
                }
                else
                {
                    Session["MessageFrom"] = "Edit Can't be Permitted ";
                    Session["SuccessMessageFrom"] = "fail";
                    return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult BarcodeTemplateEdit(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update";
                cn.Open();
                var idi = ""; var ide = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE018 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodePrinterSetup", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1622");
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Name"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Order"]);
                string defaultflag = Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                string Message = Convert.ToString(col["Message"]);
                Message = Message.Replace("@Sales@", "'Sales'");
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMN07", Message);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["Barcode Labels"]);
                string Query = Convert.ToString(col["Barcode Query"]);
                Query = Query.Replace("@Sales@", "'Sales'");
                Cmd.Parameters.AddWithValue("@COLUMN09", Query);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE018");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1622 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1622 && q.COLUMN05 == 3).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Updation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult BarcodeTemplateDelete()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                var ide = @Request["ide"]; var idi = "";
                //var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete";
                cn.Open();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodePrinterSetup", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE018");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1622 && q.COLUMN05 == 4).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1622 && q.COLUMN05 == 5).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Deletion Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult BarcodeTemplateView()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1622).ToList().FirstOrDefault().COLUMN04.ToString();
                SqlDataAdapter cmdv = new SqlDataAdapter("SELECT COLUMN04 Name,COLUMN05 [Order],COLUMN06 [Default],COLUMN07 Message,COLUMN08 Labels,COLUMN09 Query FROM SETABLE018 WHERE  COLUMN02=" + @Request["ide"] + "  and isnull(columna13,0)=0   and " + OPUnit + " and (COLUMNA03=" + acID + " or COLUMNA03 is null)", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    TempName = "",
                    TempType = "",
                    TempDefault = "",
                    TempMessage = "",
                    TempBody = "",
                    Memo = ""
                });
                all1[0].TempName = dtv.Rows[0]["Name"].ToString();
                all1[0].TempType = dtv.Rows[0]["Order"].ToString();
                all1[0].TempDefault = dtv.Rows[0]["Default"].ToString();
                all1[0].TempMessage = dtv.Rows[0]["Message"].ToString();
                all1[0].TempBody = dtv.Rows[0]["Labels"].ToString();
                all1[0].Memo = dtv.Rows[0]["Query"].ToString();
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeTemplateInfo", "BarcodeTemplate", new { FormName = Session["FormName"] });
            }
        }

    }
}
