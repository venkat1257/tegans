﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using eBizSuiteAppModel.Table;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;
using eBizSuiteUI.Controllers;

namespace MeBizSuiteAppUI.Controllers
{
    public class BankReconsilationController : Controller
    {
        //
        // GET: /BankReconsilation/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        CommonController cmnctlr = new CommonController();
        public ActionResult DocUpload()
        {
            try
            {
                //Dateformat();
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1630).ToList().FirstOrDefault().COLUMN04.ToString();
                int? acID = (int?)Session["AcOwner"];
                string OPUnit1 = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                List<SelectListItem> Bank = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001 where COLUMN07=22266 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Bank.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                Bank = Bank.OrderBy(x => x.Text).ToList();
                ViewData["Bank"] = new SelectList(Bank, "Value", "Text");
                List<SelectListItem> Status = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11193 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMNA13,0)=0", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    Status.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                Status = Status.OrderBy(x => x.Text).ToList();
                ViewData["Status"] = new SelectList(Status, "Value", "Text", selectedValue: 23134);
                var strQry = "SELECT p.[COLUMN02 ] AS ID ,p.[COLUMN04 ] AS [Import#],format(p.COLUMN05,'" + Session["FormatDate"] + "') Date,p.[COLUMN09 ] AS [Name],f.[COLUMN04 ] AS [Bank] from SETABLE019 p " +
                " left join FITABLE001 f on f.COLUMN02=p.column08 and (f.COLUMNA03=p.COLUMNA03 or f.COLUMNA03 is null) and isnull(f.COLUMNA13,0)=0 and (f.COLUMNA02=p.COLUMNA02 or f.COLUMNA02 is null) " +
                " where isnull(p.COLUMNA13,'False')='False' and " + OPUnit1 + " and (p.COLUMNA03=" + acID + " or p.COLUMNA03 is null)  ORDER BY p.COLUMNA06 desc ";
                alCol.AddRange(new List<string> { "ID", "Import#", "Date", "Name", "Bank" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN05", "COLUMN09", "COLUMN04" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;

                //List<SelectListItem> TargetBank = new List<SelectListItem>();
                //SqlDataAdapter cmdt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11194 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMNA13,0)=0", cn);
                //DataTable dtt = new DataTable();
                //cmdt.Fill(dtt);
                //for (int dd = 0; dd < dtt.Rows.Count; dd++)
                //{
                //    TargetBank.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN04"].ToString() });
                //}
                //TargetBank = TargetBank.OrderBy(x => x.Text).ToList();
                //ViewData["TargetBank"] = new SelectList(TargetBank, "Value", "Text");
                var fname = dc.CONTABLE0010.Where(q => q.COLUMN02 == 1629).Select(q => q.COLUMN04).FirstOrDefault();
                ViewBag.TransNo = cmnctlr.TransactionNoGenerate(fname, 1629, Convert.ToString(Session["OPUnit1"]));
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                var toDT = new DateTime(now.Year, now.Month, 1).ToString(Session["FormatDate"].ToString());
                ViewBag.frmDT = frmDT;
                ViewBag.toDT = toDT;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }

        public ActionResult GetBankStmt(string id, string Reconcilid)
        {
            try
            {
                //Dateformat();
                string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                var result = new List<dynamic>();
                var depositlst = new List<dynamic>();
                var withdrawlst = new List<dynamic>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> depcols = new List<WebGridColumn>();
                List<WebGridColumn> withcols = new List<WebGridColumn>();
                var rowcnt = 0;
                var deprowcnt = 0;
                var withrowcnt = 0;
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                object[] sp = new object[6];
                sp[0] = (id);
                sp[1] = (Reconcilid);
                sp[2] = ("");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var result1 = dbs.Query("Exec usp_FIN_REPORT_BANKRECONCILATION @Bank=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);
                rowcnt = result1.Count();
                sp[0] = (id);
                sp[1] = (Reconcilid);
                sp[2] = ("Receivable");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var RecGData = dbs.Query("Exec usp_FIN_REPORT_BANKRECONCILATION @Bank=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);
                deprowcnt = RecGData.Count();
                sp[0] = (id);
                sp[1] = (Reconcilid);
                sp[2] = ("Payable");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var PayGData = dbs.Query("Exec usp_FIN_REPORT_BANKRECONCILATION @Bank=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);
                withrowcnt = PayGData.Count();
                var grid = new WebGrid(result1, canPage: false, canSort: false);
                //cols.Add(grid.Column("Checkrow", "", format: (items) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox' />")));
                cols.Add(grid.Column("Date", "Date"));
                cols.Add(grid.Column("Bank", "Bank"));
                cols.Add(grid.Column("Trans", "Trans#"));
                cols.Add(grid.Column("Type", "TransType"));
                cols.Add(grid.Column("Name", "Party"));
                cols.Add(grid.Column("Memo", "Memo"));
                cols.Add(grid.Column("PaymentAmount", "Payment Amt"));
                cols.Add(grid.Column("DepositAmount", "Deposit Amt"));
                cols.Add(grid.Column("BankType", "Type"));
                var htmlstring = grid.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdbData" },
                                 columns: cols);
                var griddep = new WebGrid(RecGData, canPage: false, canSort: false);
                depcols.Add(grid.Column("Date", "Date"));
                depcols.Add(grid.Column("Bank", "Bank"));
                depcols.Add(grid.Column("Trans", "Trans#"));
                depcols.Add(grid.Column("Type", "TransType"));
                depcols.Add(grid.Column("Name", "Party"));
                depcols.Add(grid.Column("Memo", "Memo"));
                //depcols.Add(grid.Column("PaymentAmount", "Payment Amt"));
                depcols.Add(grid.Column("DepositAmount", "Amount"));
                //depcols.Add(grid.Column("BankType", "Type"));
                var htmlstringdep = griddep.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grddepData" },
                                 columns: depcols);
                var gridwith = new WebGrid(PayGData, canPage: false, canSort: false);
                withcols.Add(grid.Column("Date", "Date"));
                withcols.Add(grid.Column("Bank", "Bank"));
                withcols.Add(grid.Column("Trans", "Trans#"));
                withcols.Add(grid.Column("Type", "TransType"));
                withcols.Add(grid.Column("Name", "Party"));
                withcols.Add(grid.Column("Memo", "Memo"));
                withcols.Add(grid.Column("PaymentAmount", "Amount"));
                //withcols.Add(grid.Column("DepositAmount", "Deposit Amt"));
                //withcols.Add(grid.Column("BankType", "Type"));
                var htmlstringwith = gridwith.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdwithData" },
                                 columns: withcols);
                var Balance = "0";
                if (result1.Count() > 0)
                    Balance = result1.FirstOrDefault().BalanceAmount.ToString();
                return Json(new { Data = htmlstring.ToHtmlString(), Data1 = htmlstringdep.ToHtmlString(), Data2 = htmlstringwith.ToHtmlString(), Balance = Balance, rowcnt = rowcnt, deprowcnt = deprowcnt, withrowcnt = withrowcnt }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult DocUpload(FormCollection fc)
        {
            try
            {
                //var frmDT = Request["frmDT"].ToString();
                DataSet ds = new DataSet();
                var File = Request.Files[0] as HttpPostedFileBase;
                var FileName = File.FileName; var tbldata = "";
                if (FileName.Length > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(FileName);
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Server.MapPath("~/Content/");
                        int AOwner = Convert.ToInt32(Session["AcOwner"]);
                        var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                        fileLocation = fileLocation + "BankReconcil" + "\\" + AOwnerName;
                        if (!System.IO.Directory.Exists(fileLocation))
                            System.IO.Directory.CreateDirectory(fileLocation);
                        fileLocation = fileLocation + "\\" + FileName;
                        if (System.IO.File.Exists(fileLocation))
                            System.IO.File.Delete(fileLocation);
                        File.SaveAs(fileLocation);
                        string excelConnectionString = string.Empty;
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        //connection String for xls file format.
                        if (fileExtension == ".xls")
                        {
                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                            fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                            fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                        //Create Connection to Excel work book and add oledb namespace
                        DataTable dt = new DataTable();
                        using (OleDbConnection excelConnection = new OleDbConnection(excelConnectionString))
                        {
                            excelConnection.Open();
                            dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dt == null)
                            {
                                return null;
                            }
                            excelConnection.Close();
                        }
                        String[] excelSheets = new String[dt.Rows.Count];
                        int t = 0;
                        //excel data saves in temp file here.
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }
                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(ds);
                        }
                        File.InputStream.Dispose();
                    }
                    else if (fileExtension.ToString().ToLower().Equals(".xml"))
                    {
                        string fileLocation = Server.MapPath("~/Content/");
                        int AOwner = Convert.ToInt32(Session["AcOwner"]);
                        var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                        fileLocation = fileLocation + "BankReconcil" + "\\" + AOwnerName;
                        if (!System.IO.Directory.Exists(fileLocation))
                            System.IO.Directory.CreateDirectory(fileLocation);
                        fileLocation = fileLocation + "\\" + FileName;
                        if (System.IO.File.Exists(fileLocation))
                            System.IO.File.Delete(fileLocation);
                        File.SaveAs(fileLocation);
                        XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                        // DataSet ds = new DataSet();
                        ds.ReadXml(xmlreader);
                        xmlreader.Close();
                        File.InputStream.Dispose();
                    }
                    tbldata = "<table id='griddata'>";
                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        if (ds.Tables[0].Columns[i].ColumnName != "")
                        {
                            if (i == 0) tbldata = tbldata + "<tr><th></th>";
                            tbldata = tbldata + "<th>" + ds.Tables[0].Columns[i].ColumnName + "</th>";
                            if (i == ds.Tables[0].Columns.Count - 1) tbldata = tbldata + "</tr>";
                        }
                    }
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        tbldata = tbldata + "<tr  ><td  ><input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'  /></td>";
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            if (ds.Tables[0].Columns[j].ColumnName != "")
                            {
                                tbldata = tbldata + "<td><label  id='LineID'>" + ds.Tables[0].Rows[i][j].ToString() + " </label></td>";
                                if (j == ds.Tables[0].Columns.Count - 1) tbldata = tbldata + "</tr>";
                            }
                        }
                    }
                    tbldata = tbldata + "</table>";
                }

                string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                DateTime now = DateTime.Now;
                var frmDT = Request["frmDT"].ToString();
                var toDT = Request["toDT"].ToString();
                if (frmDT == "" || frmDT == null)
                    frmDT = now.ToShortDateString();
                if (toDT == "" || toDT == null)
                    toDT = new DateTime(now.Year, now.Month, 1).ToShortDateString();
                SqlCommand cmd = new SqlCommand("[usp_FIN_REPORT_BANKRECONCILATION]", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(frmDT.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(toDT.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                cmd.Parameters.AddWithValue("@Bank", Request["Bank"]);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dts = new DataTable();
                da.Fill(dts);
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var result = new List<dynamic>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                if (dts.Rows.Count > 0)
                {
                    List<WebGridColumn> gcol = new List<WebGridColumn>();
                    foreach (DataRow row in dts.Rows)
                    {
                        var obj = (IDictionary<string, object>)new ExpandoObject();
                        obj.Add("", "");
                        foreach (DataColumn col in dts.Columns)
                        {
                            obj.Add(col.ColumnName, row[col.ColumnName]);
                        }
                        result.Add(obj);
                    }
                    ViewBag.grddata = result;
                    grid = new WebGrid(result, canPage: false, canSort: false);
                    cols.Add(grid.Column("Checkrow", "", format: (items) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox' />")));
                    cols.Add(grid.Column("Date", "Date"));
                    cols.Add(grid.Column("Bank", "Bank"));
                    cols.Add(grid.Column("Trans", "Trans#"));
                    cols.Add(grid.Column("Type", "Type"));
                    cols.Add(grid.Column("Name", "Party"));
                    cols.Add(grid.Column("Memo", "Memo"));
                    cols.Add(grid.Column("PaymentAmount", "Payment Amt"));
                    cols.Add(grid.Column("DepositAmount", "Deposit Amt"));
                    var htmlstring = grid.GetHtml(mode: WebGridPagerModes.All,
                                     htmlAttributes: new { id = "grdfData" },
                                     columns: cols);
                    return Json(new { Data = tbldata, Data1 = htmlstring.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Data = tbldata, Data1 = "" }, JsonRequestBehavior.AllowGet);
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }

        public ActionResult GetUploadBankStmt(string id, string Reconcilationid)
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                string Reconcilid = Reconcilationid;
                if (Reconcilationid == "" || Reconcilationid == "23132") Reconcilationid = "23133,23134";
                SqlCommand cmdb = new SqlCommand("SELECT COLUMN08 FROM SETABLE019 WHERE COLUMN02=" + id + " and COLUMNA03=" + acID + " and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                DataTable dtb = new DataTable();
                dab.Fill(dtb);
                var BankId = "";
                if (dtb.Rows.Count > 0)
                    BankId = dtb.Rows[0][0].ToString();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                object[] sp = new object[6];
                sp[0] = (id);
                sp[1] = (Reconcilid);
                sp[2] = ("");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var sysresult = dbs.Query("Exec usp_FIN_REPORT_SYSRECONCILATIONDATA @Id=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);

                sp[0] = (id);
                sp[1] = (Reconcilid);
                sp[2] = ("Receivable");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var sysDepresult = dbs.Query("Exec usp_FIN_REPORT_SYSRECONCILATIONDATA @Id=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);

                sp[0] = (id);
                sp[1] = (Reconcilid);
                sp[2] = ("Payable");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var syswithresult = dbs.Query("Exec usp_FIN_REPORT_SYSRECONCILATIONDATA @Id=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);

                var result = new List<dynamic>();
                var depositlst = new List<dynamic>();
                var withdrawlst = new List<dynamic>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> depcols = new List<WebGridColumn>();
                List<WebGridColumn> withcols = new List<WebGridColumn>();
                //foreach (DataRow row in dts.Rows)
                //{
                //    var Debitamt = "0";
                //    var Creditamt = "0";
                //    var obj = (IDictionary<string, object>)new ExpandoObject();
                //    var deposit = (IDictionary<string, object>)new ExpandoObject();
                //    var withdraw = (IDictionary<string, object>)new ExpandoObject();
                //    foreach (DataColumn col in dts.Columns)
                //    {
                //        if (row["Debit"].ToString() == "" || row["Debit"].ToString() == "0.00" || row["Debit"].ToString() == null) Debitamt = "0";
                //        else Debitamt = row["Debit"].ToString();
                //        if (row["Credit"].ToString() == "" || row["Credit"].ToString() == "0.00" || row["Credit"].ToString() == null) Creditamt = "0";
                //        else Creditamt = row["Credit"].ToString();

                //        obj.Add(col.ColumnName, row[col.ColumnName]);
                //        if (Debitamt != "0") deposit.Add(col.ColumnName, row[col.ColumnName]);
                //        else if (Creditamt!="0") withdraw.Add(col.ColumnName, row[col.ColumnName]);
                //    }
                //    result.Add(obj);
                //    if (Debitamt != "0") depositlst.Add(deposit);
                //    else if (Creditamt != "0") withdrawlst.Add(withdraw);
                //}
                var grid = new WebGrid(sysresult, canPage: false, canSort: false);
                //result.Add(grid.Column("chkrow", "", format: (items) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox' />")));
                cols.Add(grid.Column("Date", "Date"));
                cols.Add(grid.Column("Memo", "Memo"));
                cols.Add(grid.Column("Ref", "Reference"));
                cols.Add(grid.Column("Status", "Status"));
                cols.Add(grid.Column("Debit", "Debit"));
                cols.Add(grid.Column("Credit", "Credit"));
                var htmlstringall = grid.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdupallData" },
                                 columns: cols);
                var griddep = new WebGrid(sysDepresult, canPage: false, canSort: false);
                depcols.Add(grid.Column("chkrow", "", format: (item) =>
                {
                    if (Convert.ToString(item.BankReconcilid) == "23133")
                    {
                        return new HtmlString(string.Format("<input  id='checkbankdepRow' name='chkbd' TYPE='RADIO' disabled='disabled'/><input id='bankdepid' type='text' val='" + item.chkrow + "' style='display:none'/><input id='bankdepamt' type='text' val='0' style='display:none'/><input id='bankdepsysbankid' type='text' val='" + item.BankId + "' style='display:none'/><input id='bankdepsysmemo' type='text' val='" + item.Memo + "' style='display:none'/>"));
                    }
                    else
                    {
                        return new HtmlString(string.Format(" <input  id='checkbankdepRow' name='chkbd' TYPE='RADIO' /><input id='bankdepid' type='text' val='" + item.chkrow + "' style='display:none'/><input id='bankdepamt' type='text' val='" + item.Debit + "' style='display:none'/><input id='bankdepsysbankid' type='text' val='" + item.BankId + "' style='display:none'/><input id='bankdepsysmemo' type='text' val='" + item.Memo + "' style='display:none'/>"));
                    }
                }));
                depcols.Add(grid.Column("Date", "Date"));
                depcols.Add(grid.Column("Memo", "Memo"));
                depcols.Add(grid.Column("Ref", "Reference"));
                depcols.Add(grid.Column("Debit", "Debit"));
                var htmlstringdep = griddep.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdupdepData" },
                                 columns: depcols);
                var gridwith = new WebGrid(syswithresult, canPage: false, canSort: false);
                withcols.Add(grid.Column("chkrow", "", format: (item) =>
                {
                    if (Convert.ToString(item.BankReconcilid) == "23133")
                    {
                        return new HtmlString(string.Format("<input  id='checkbankwithRow' name='chkbw' TYPE='RADIO' disabled='disabled'/><input id='bankcrid' type='text' val='" + item.chkrow + "' style='display:none'/><input id='bankcramt' type='text' val='0' style='display:none'/><input id='bankcrsysbankid' type='text' val='" + item.BankId + "' style='display:none'/><input id='bankcrsysmemo' type='text' val='" + item.Memo + "' style='display:none'/>"));
                    }
                    else
                    { return new HtmlString(string.Format("<input  id='checkbankwithRow' name='chkbw' TYPE='RADIO' /><input id='bankcrid' type='text' val='" + item.chkrow + "' style='display:none'/><input id='bankcramt' type='text' val='" + item.Credit + "' style='display:none'/><input id='bankcrsysbankid' type='text' val='" + item.BankId + "' style='display:none'/><input id='bankcrsysmemo' type='text' val='" + item.Memo + "' style='display:none'/>")); }
                }));
                withcols.Add(grid.Column("Date", "Date"));
                withcols.Add(grid.Column("Memo", "Memo"));
                withcols.Add(grid.Column("Ref", "Reference"));
                withcols.Add(grid.Column("Credit", "Credit"));
                var htmlstringwith = gridwith.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdupwithData" },
                                 columns: withcols);

                sp[0] = (BankId);
                sp[1] = (Reconcilid);
                sp[2] = ("");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var result1 = dbs.Query("Exec usp_FIN_REPORT_BANKRECONCILATION @Bank=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);
                sp[0] = (BankId);
                sp[1] = (Reconcilid);
                sp[2] = ("Receivable");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var DepGrid = dbs.Query("Exec usp_FIN_REPORT_BANKRECONCILATION @Bank=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);
                sp[0] = (BankId);
                sp[1] = (Reconcilid);
                sp[2] = ("Payable");
                sp[3] = (OPUnit);
                sp[4] = (Session["AcOwner"]);
                sp[5] = (Session["FormatDate"]);
                var PayGrid = dbs.Query("Exec usp_FIN_REPORT_BANKRECONCILATION @Bank=@0,@Reconcilid=@1,@Type=@2,@OPUnit=@3,@AcOwner=@4,@DateF=@5", sp);
                //SqlCommand cmds = new SqlCommand("[usp_FIN_REPORT_BANKRECONCILATION]", cn);
                //cmds.CommandType = CommandType.StoredProcedure;
                //cmds.Parameters.AddWithValue("@Reconcilid", Reconcilid);
                //cmds.Parameters.AddWithValue("@Bank", BankId);
                //cmds.Parameters.AddWithValue("@OPUnit", OPUnit);
                //cmds.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                //cmds.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                //SqlDataAdapter das = new SqlDataAdapter(cmds);
                //DataTable dt = new DataTable();
                //das.Fill(dt);
                List<WebGridColumn> syscols = new List<WebGridColumn>();
                List<WebGridColumn> sysdepcols = new List<WebGridColumn>();
                List<WebGridColumn> syswithcols = new List<WebGridColumn>();
                //foreach (DataRow row in dt.Rows)
                //{
                //    var obj = (IDictionary<string, object>)new ExpandoObject();
                //    var deposit = (IDictionary<string, object>)new ExpandoObject();
                //    var withdraw = (IDictionary<string, object>)new ExpandoObject();
                //    foreach (DataColumn col in dt.Columns)
                //    {
                //        obj.Add(col.ColumnName, row[col.ColumnName]);
                //        if (row["BankType"].ToString() == "Receivable") deposit.Add(col.ColumnName, row[col.ColumnName]);
                //        else if (row["BankType"].ToString() == "Payable") withdraw.Add(col.ColumnName, row[col.ColumnName]);
                //    }
                //    sysresult.Add(obj);
                //    if (row["BankType"].ToString() == "Receivable") sysdepositlst.Add(deposit);
                //    else if (row["BankType"].ToString() == "Payable") syswithdrawlst.Add(withdraw);
                //}
                var gridsys = new WebGrid(result1, canPage: false, canSort: false);
                syscols.Add(gridsys.Column("Checkrow", "", format: (items) => new HtmlString("<input  id='checksysRow'  name='chku' TYPE='RADIO' />")));
                syscols.Add(gridsys.Column("Date", "Date"));
                //syscols.Add(gridsys.Column("Bank", "Bank"));
                syscols.Add(gridsys.Column("Trans", "Trans#"));
                syscols.Add(gridsys.Column("Type", "TransType"));
                //syscols.Add(gridsys.Column("Name", "Party"));
                syscols.Add(gridsys.Column("Memo", "Memo"));
                syscols.Add(gridsys.Column("PaymentAmount", "Payment Amt"));
                syscols.Add(gridsys.Column("DepositAmount", "Deposit Amt"));
                //syscols.Add(grid.Column("BankType", "Type"));
                var htmlstring = gridsys.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdsysData" },
                                 columns: syscols);
                var gridsysdep = new WebGrid(DepGrid, canPage: false, canSort: false);
                sysdepcols.Add(gridsys.Column("Checkrow", "", format: (item) =>
                {
                    if (item.Reconcilid == 23133)
                    {
                        return new HtmlString(string.Format("<input  id='checksysdepRow' name='chkd' TYPE='checkbox' disabled='disabled'/><input id='bankdepid' type='text' val='" + item.TransID + "' style='display:none'/><input id='bankdeptno'  type='text' val='" + item.Trans + "' style='display:none'/><input id='sysdepamt' type='text' val='0' style='display:none'/><input id='sysdepbankid' type='text' val='" + item.BankID + "' style='display:none'/>"));
                    }
                    else
                    { return new HtmlString(string.Format("<input  id='checksysdepRow' name='chkd' TYPE='checkbox' /><input id='bankdepid' type='text' val='" + item.TransID + "' style='display:none'/><input id='bankdeptno' type='text' val='" + item.Trans + "' style='display:none'/><input id='sysdepamt' type='text' val='" + item.DepositAmount + "' style='display:none'/><input id='sysdepbankid' type='text' val='" + item.BankID + "' style='display:none'/>")); }
                }));
                sysdepcols.Add(gridsys.Column("Date", "Date"));
                //sysdepcols.Add(gridsys.Column("Bank", "Bank"));
                sysdepcols.Add(gridsys.Column("Trans", "Trans#"));
                sysdepcols.Add(gridsys.Column("Type", "TransType"));
                //sysdepcols.Add(gridsys.Column("Name", "Party"));
                sysdepcols.Add(gridsys.Column("Memo", "Memo"));
                //depcols.Add(gridsys.Column("PaymentAmount", "Payment Amt"));
                sysdepcols.Add(gridsys.Column("DepositAmount", "Amount"));
                //sysdepcols.Add(gridsys.Column("BankType", "Type"));
                var htmlstringsysdep = gridsysdep.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdsysdepData" },
                                 columns: sysdepcols);
                var gridsyswith = new WebGrid(PayGrid, canPage: false, canSort: false);
                syswithcols.Add(gridsys.Column("Checkrow", "", format: (item) =>
                {
                    if (item.Reconcilid == 23133)
                    {
                        return new HtmlString(string.Format("<input  id='checksyswithRow'   name='chkw' TYPE='checkbox' disabled='disabled'/><input id='syswithid' type='text' val='" + item.TransID + "' style='display:none'/><input id='syswithtno' type='text' val='" + item.Trans + "' style='display:none'/><input id='syswithamt' type='text' val='0' style='display:none'/><input id='syswithbankid' type='text' val='" + item.BankID + "' style='display:none'/>"));
                    }
                    else
                    { return new HtmlString(string.Format("<input  id='checksyswithRow'   name='chkw' TYPE='checkbox'  /><input id='syswithid' type='text' val='" + item.TransID + "' style='display:none'/><input id='syswithtno' type='text' val='" + item.Trans + "' style='display:none'/><input id='syswithamt' type='text' val='" + item.PaymentAmount + "' style='display:none'/><input id='syswithbankid' type='text' val='" + item.BankID + "' style='display:none'/>")); }
                }));
                syswithcols.Add(gridsys.Column("Date", "Date"));
                //syswithcols.Add(gridsys.Column("Bank", "Bank"));
                syswithcols.Add(gridsys.Column("Trans", "Trans#"));
                syswithcols.Add(gridsys.Column("Type", "TransType"));
                //syswithcols.Add(gridsys.Column("Name", "Party"));
                syswithcols.Add(gridsys.Column("Memo", "Memo"));
                syswithcols.Add(gridsys.Column("PaymentAmount", "Amount"));
                //withcols.Add(gridsys.Column("DepositAmount", "Deposit Amt"));
                //syswithcols.Add(gridsys.Column("BankType", "Type"));
                var htmlstringsyswith = gridsyswith.GetHtml(mode: WebGridPagerModes.All,
                                 htmlAttributes: new { id = "grdsyswithData" },
                                 columns: syswithcols);
                return Json(new { Data1 = htmlstringdep.ToHtmlString(), Data2 = htmlstringwith.ToHtmlString(), Data3 = htmlstringsysdep.ToHtmlString(), Data4 = htmlstringsyswith.ToHtmlString(), Data5 = htmlstringall.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }
        [HttpPost]
        public ActionResult SaveGrdData(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["RecGridData"] = ds;
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }


        [HttpPost]
        public ActionResult Reconcillation(FormCollection fc)
        {
            try
            {
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                var itemdata = (DataSet)Session["RecGridData"];
                //Dateformat();
                string insert = "Insert";
                var msg = string.Empty;
                DateTime Date = DateTime.Now;
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmdl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE021 ", cn);
                    SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                    DataTable dtl = new DataTable();
                    dal.Fill(dtl);
                    string HCol = dtl.Rows[0][0].ToString();
                    if (HCol == "") HCol = "999";
                    HCol = (Convert.ToInt32(HCol) + 1).ToString();
                    var Transid = itemdata.Tables[0].Rows[i]["Transid"].ToString();
                    var Transno = itemdata.Tables[0].Rows[i]["Transno"].ToString();
                    var TransAcc = itemdata.Tables[0].Rows[i]["TransAcc"].ToString();
                    var TransAmt = itemdata.Tables[0].Rows[i]["TransAmt"].ToString();
                    if (TransAmt == "" || TransAmt == " " || TransAmt == null || TransAmt == "undefined") TransAmt = "0";
                    var Transdt = itemdata.Tables[0].Rows[i]["Transdt"].ToString();
                    if (Transdt == "" || Transdt == " " || Transdt == null || Transdt == "undefined") Transdt = Date.ToString();
                    var Transtype = itemdata.Tables[0].Rows[i]["Transtype"].ToString();
                    var Memo = itemdata.Tables[0].Rows[i]["Memo"].ToString();
                    var bankdeprecid = itemdata.Tables[0].Rows[i]["bankdeprecid"].ToString();
                    var Rectype = itemdata.Tables[0].Rows[i]["Rectype"].ToString();
                    SqlCommand cmd = new SqlCommand("[usp_SET_BL_BankReconcilation]", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@COLUMN02", HCol);
                    cmd.Parameters.AddWithValue("@COLUMN03", bankdeprecid);
                    cmd.Parameters.AddWithValue("@COLUMN04", Rectype);
                    cmd.Parameters.AddWithValue("@COLUMN05", Date);
                    cmd.Parameters.AddWithValue("@COLUMN06", Transno);
                    cmd.Parameters.AddWithValue("@COLUMN07", DateTime.ParseExact(Transdt.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@COLUMN08", Transtype);
                    cmd.Parameters.AddWithValue("@COLUMN09", Transid);
                    cmd.Parameters.AddWithValue("@COLUMN10", Memo);
                    cmd.Parameters.AddWithValue("@COLUMN11", TransAmt);
                    cmd.Parameters.AddWithValue("@COLUMN12", TransAcc);
                    cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    cmd.Parameters.AddWithValue("@COLUMNA03", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    cmd.Parameters.AddWithValue("@Direction", insert);
                    cmd.Parameters.AddWithValue("@TabelName", "SETABLE021");
                    cn.Open();
                    int r = cmd.ExecuteNonQuery();
                    cn.Close();
                    if (r > 0)
                    {
                        msg = string.Empty;
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1630 && q.COLUMN05 == 1).FirstOrDefault();
                        if (msgMaster != null)
                            msg = msgMaster.COLUMN03;
                        else
                            msg = "" + Session["FormName"] + " Successfully Created";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    else
                    {
                        msg = string.Empty;
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1630 && q.COLUMN05 == 0).FirstOrDefault();
                        if (msgMaster != null)
                            msg = msgMaster.COLUMN03;
                        else
                            msg = "" + Session["FormName"] + " Creation Failed";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "fail";
                    }
                }
                Session["RecGridData"] = null;
                return Json(new { item = 1, MessageFrom = msg, SuccessMessageFrom = Session["SuccessMessageFrom"] }, JsonRequestBehavior.AllowGet);
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }
        
        [HttpPost]
        public ActionResult AutoReconcillation(string id, string Reconcilationid)
        {
            try
            {
                string OpUnit = Convert.ToString(Session["OPUnit1"]);
                string AcOwner = Convert.ToString(Session["AcOwner"]);
                var msg = "";
                SqlCommand cmd = new SqlCommand("usp_Proc_TP_BANKAUTORECONCILATION", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportId", id);
                cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"]);
                cmd.Parameters.AddWithValue("@Eid", Session["eid"]);
                cmd.Parameters.AddWithValue("@OPUnit", OpUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                cn.Open();
                int r = cmd.ExecuteNonQuery();
                if (cmd.Parameters["@ReturnValue"].Value != "")
                    r = Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value);
                else r = 0;
                cn.Close();
                if (r > 0)
                {
                    msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1630 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Created";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    msg = "No Matching Data Found";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return Json(new { item = 1, MessageFrom = msg, SuccessMessageFrom = Session["SuccessMessageFrom"], rowseffected = r }, JsonRequestBehavior.AllowGet);
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult FileNameCheck()
        {
            try
            {
                var File = Request.Files[0] as HttpPostedFileBase;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                var FileName = File.FileName; string fileExtension = "";
                //var TargetBank = Request["TargetBank"].ToString();
                //Create a new DataTable.
                DataTable Idt = new DataTable();
                if (FileName.Length > 0)
                {
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    if (fileExtension != ".xlsx")
                        return Json(new { DataName = "", FileData = "", FileFormat = "1", FileName = FileName }, JsonRequestBehavior.AllowGet);
                    SqlCommand cmdf = new SqlCommand("select COLUMN09 from SETABLE019 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN09='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ", cn);
                    SqlDataAdapter daf = new SqlDataAdapter(cmdf);
                    DataTable dtf = new DataTable();
                    daf.Fill(dtf);
                    if (dtf.Rows.Count > 0)
                        return Json(new { DataName = dtf.Rows[0][0].ToString(), FileData = "", FileFormat = "", FileName = FileName }, JsonRequestBehavior.AllowGet);
                    string fileLocation = Server.MapPath("~/Content/");
                    int AOwner = Convert.ToInt32(Session["AcOwner"]);
                    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                    fileLocation = fileLocation + "BankReconcil" + "\\" + AOwnerName;
                    if (!System.IO.Directory.Exists(fileLocation))
                        System.IO.Directory.CreateDirectory(fileLocation);
                    fileLocation = fileLocation + "\\" + FileName;
                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);
                    //Save the uploaded Excel file.
                    string filePath = fileLocation;
                    File.SaveAs(fileLocation);
                    DataTable dt = new DataTable();

                    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileLocation, false))
                    {
                        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();
                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                        }
                        foreach (Row row in rows) //this will also include your header row...
                        {
                            DataRow tempRow = dt.NewRow();
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (cellColumnIndex <= dt.Columns.Count)
                                {
                                    if (columnIndex < cellColumnIndex)
                                    {
                                        do
                                        {
                                            tempRow[columnIndex] = ""; //Insert blank data here;
                                            columnIndex++;
                                        }
                                        while (columnIndex < cellColumnIndex);
                                    }
                                    tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);
                                }
                                columnIndex++;
                            }
                            dt.Rows.Add(tempRow);
                        }
                    }
                    dt.Rows.RemoveAt(0);
                    Idt = dt;
                }
                DataColumnCollection columns = Idt.Columns;
                SqlCommand cmdc = new SqlCommand("select COLUMN04,COLUMN06 from SETABLE022 where COLUMN03=23139 and COLUMN05=1629 and isnull(COLUMNA13,0)=0 ", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                if (dtc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtc.Rows.Count; i++)
                    {
                        string colname = dtc.Rows[i]["COLUMN04"].ToString();
                        string actcolname = dtc.Rows[i]["COLUMN06"].ToString();
                        colname = colname.TrimEnd();
                        if (columns.Contains(colname) && actcolname != "")
                        {
                            Idt.Columns[colname].ColumnName = actcolname.TrimEnd();
                            if (actcolname.TrimEnd().ToString() == "COLUMN03")
                            {
                                DateTime date;
                                for (int l = 0; l < Idt.Rows.Count; l++)
                                {
                                    if (Idt.Rows[l]["COLUMN03"].ToString() != "" && Idt.Rows[l]["COLUMN03"].ToString() != null)
                                    {
                                        var formatStrings = new string[] { "d/M/yyyy", "d/MM/yyyy", "dd/M/yyyy", "dd/MM/yyyy", "d-M-yyyy", "d-MM-yyyy", "dd-M-yyyy", "dd-MM-yyyy", "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy", "yyyy-MM-dd", "dd-MM-yy", "d-MM-yy", "dd-M-yy", "d-M-yy", "dd/MM/yy", "d/MM/yy", "dd/M/yy", "d/M/yy" };
                                        var normalformat = new string[] { "d/M/yyyy", "d/MM/yyyy", "dd/M/yyyy", "dd/MM/yyyy", "dd/MM/yy", "d/MM/yy", "dd/M/yy", "d/M/yy" };
                                        if (DateTime.TryParseExact(Idt.Rows[l]["COLUMN03"].ToString(), formatStrings, null, DateTimeStyles.None, out date) == true)
                                        {
                                            //Idt.Rows[l][Idt.Columns[colname].ColumnName]=DateTime.TryParseExact(Idt.Rows[l][Idt.Columns[colname].ColumnName].ToString(), new[] { "yyyy-MM-dd", "MM/dd/yy", "dd-MM-yyyy", "dd/MM/yyyy", "dd-MM-yy" }, null, DateTimeStyles.None, out date);
                                        }
                                        else
                                        {
                                            Idt.Rows[l]["COLUMN03"] = DateTime.FromOADate(Convert.ToDouble(Idt.Rows[l]["COLUMN03"].ToString())).ToShortDateString();
                                            if (DateTime.TryParseExact(Idt.Rows[l]["COLUMN03"].ToString(), formatStrings, null, DateTimeStyles.None, out date) == true)
                                            {
                                                DateTime datestring;
                                                if (DateTime.TryParseExact(Idt.Rows[l]["COLUMN03"].ToString(), formatStrings, CultureInfo.InvariantCulture, DateTimeStyles.None, out datestring))
                                                    Idt.Rows[l]["COLUMN03"] = datestring.ToString("dd/MM/yyyy");
                                            }
                                            else if (DateTime.TryParseExact(Idt.Rows[l]["COLUMN03"].ToString(), normalformat, null, DateTimeStyles.None, out date) == true)
                                            {
                                                DateTime datestring;
                                                if (DateTime.TryParseExact(Idt.Rows[l]["COLUMN03"].ToString(), normalformat, CultureInfo.InvariantCulture, DateTimeStyles.None, out datestring))
                                                    Idt.Rows[l]["COLUMN03"] = datestring.ToString("dd/MM/yyyy");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                            return Json(new { DataName = "", FileData = "", FileFormat = "1", FileName = FileName }, JsonRequestBehavior.AllowGet);
                    }
                    Session["filedata"] = Idt;
                    //return Json(new{ DataName = "", FileData = Idt, FileFormat = "" }, "application/json",JsonRequestBehavior.AllowGet);
                    return Json(new { DataName = "", FileData = "1", FileFormat = "", FileName = FileName }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { DataName = "", FileData = "", FileFormat = "1", FileName = FileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return "";
            }
            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null && cell.DataType == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }

        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        private string GetValue(SpreadsheetDocument doc, Cell cell)
        {
            if (cell.CellValue != null)
            {
                string value = cell.CellValue.InnerText;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
                return value;
            }
            return "";
        }

        [HttpPost]
        public ActionResult StmtUpload(string fromdt, string todt, string Bank, string Import, string FileName)
        {
            try
            {
                var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1630).ToList().FirstOrDefault().COLUMN04.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                //var File = Request.Files[0] as HttpPostedFileBase;
                //var FileName1 = File.FileName; 
                var tbldata = ""; string fileExtension = "";
                DataTable Idt = new DataTable();
                Idt = (DataTable)Session["filedata"];
                if (Idt.Rows.Count > 0)
                {
                    string Date = DateTime.Now.ToString();
                    string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                    if (opstatus == null || opstatus == "") OpUnit = null;
                    string insert = "Insert";
                    int OutParam = 0;
                    SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE019 ", cn);
                    SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                    DataTable dtt = new DataTable();
                    daa.Fill(dtt);
                    string HCol2 = dtt.Rows[0][0].ToString();
                    if (HCol2 == "") HCol2 = "999";
                    HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                    SqlCommand Cmd = new SqlCommand("usp_SET_BL_BankStmtUpload", cn);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1629");
                    Cmd.Parameters.AddWithValue("@COLUMN04", Import);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Date);
                    if (fromdt == "" || fromdt == "undefined") Cmd.Parameters.AddWithValue("@COLUMN06", DateTime.Now);
                    else Cmd.Parameters.AddWithValue("@COLUMN06", DateTime.ParseExact(fromdt, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    if (todt == "" || todt == "undefined") Cmd.Parameters.AddWithValue("@COLUMN07", DateTime.Now);
                    else Cmd.Parameters.AddWithValue("@COLUMN07", DateTime.ParseExact(todt, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    Cmd.Parameters.AddWithValue("@COLUMN08", Bank);
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    Cmd.Parameters.AddWithValue("@COLUMN09", FileName.Replace(fileExtension, ""));
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", ACwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cn.Open();
                    int r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                    cn.Close();
                    if (r > 0)
                    {
                        var msg = string.Empty;
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 1).FirstOrDefault();
                        if (msgMaster != null)
                            msg = msgMaster.COLUMN03;
                        else
                            msg = "" + FormName + " Successfully Created ";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    else
                    {
                        var msg = string.Empty;
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 0).FirstOrDefault();
                        if (msgMaster != null)
                            msg = msgMaster.COLUMN03;
                        else
                            msg = "" + FormName + " Creation Failed";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "fail";
                    }
                    if (OutParam > 0 && Idt.Rows.Count > 0)
                    {
                        int l = 0;
                        for (int i = 0; i < Idt.Rows.Count; i++)
                        {
                            SqlCommand cmd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE020 ", cn);
                            SqlDataAdapter da = new SqlDataAdapter(cmdd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            string HCol = dt.Rows[0][0].ToString();
                            if (HCol == "") HCol = "999";
                            HCol = (Convert.ToInt32(HCol) + 1).ToString();
                            string lDate = Idt.Rows[i]["COLUMN03"].ToString();
                            string Memo = Idt.Rows[i]["COLUMN06"].ToString();
                            string Reference = Idt.Rows[i]["COLUMN07"].ToString();
                            string Debit = Idt.Rows[i]["COLUMN08"].ToString().Replace(",", "");
                            string Credit = Idt.Rows[i]["COLUMN09"].ToString().Replace(",", "");
                            string Balance = Idt.Rows[i]["COLUMN10"].ToString().Replace(",", "");
                            if (Memo == "" || Memo == "undefined") Memo = "";
                            if (Reference == "" || Reference == "undefined") Reference = "";
                            if (Debit == "" || Debit == " " || Debit == null || Debit == "undefined") Debit = "0";
                            if (Credit == "" || Credit == " " || Credit == null || Credit == "undefined") Credit = "0";
                            if (Balance == "" || Balance == " " || Balance == null || Balance == "undefined") Balance = "0";
                            if (Debit != "0" || Credit != "0")
                            {
                                SqlCommand Cmdl = new SqlCommand("usp_SET_BL_BankStmtUpload", cn);
                                Cmdl.CommandType = CommandType.StoredProcedure;
                                Cmdl.Parameters.AddWithValue("@COLUMN02", HCol);
                                //if (lDate == "" || lDate == "undefined") lDate = DateTime.Now.ToString();
                                //else { lDate =Convert.ToDateTime(lDate).ToString(Session["FormatDate"].ToString()); Cmdl.Parameters.AddWithValue("@COLUMN03", DateTime.ParseExact(lDate, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture)); }
                                Cmdl.Parameters.AddWithValue("@COLUMN03", lDate);
                                Cmdl.Parameters.AddWithValue("@COLUMN04", Bank);
                                Cmdl.Parameters.AddWithValue("@COLUMN05", null);
                                Cmdl.Parameters.AddWithValue("@COLUMN06", Memo);
                                Cmdl.Parameters.AddWithValue("@COLUMN07", Reference);
                                Cmdl.Parameters.AddWithValue("@COLUMN08", Debit);
                                Cmdl.Parameters.AddWithValue("@COLUMN09", Credit);
                                Cmdl.Parameters.AddWithValue("@COLUMN10", Balance);
                                Cmdl.Parameters.AddWithValue("@COLUMN11", OutParam);
                                Cmdl.Parameters.AddWithValue("@COLUMN12", 23134);
                                Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                                Cmdl.Parameters.AddWithValue("@COLUMNA03", ACwner);
                                Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                                Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                                Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                                Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                                Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                                Cmdl.Parameters.AddWithValue("@Direction", insert);
                                Cmdl.Parameters.AddWithValue("@TabelName", "SETABLE020");
                                cn.Open();
                                l = Cmdl.ExecuteNonQuery();
                                cn.Close();
                            }
                        }
                        if (l > 0)
                        {
                            var msg = string.Empty;
                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 1).FirstOrDefault();
                            if (msgMaster != null)
                                msg = msgMaster.COLUMN03;
                            else
                                msg = "" + FormName + " Successfully Created ";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "Success";
                        }
                        else
                        {
                            var msg = string.Empty;
                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 0).FirstOrDefault();
                            if (msgMaster != null)
                                msg = msgMaster.COLUMN03;
                            else
                                msg = "" + FormName + " Creation Failed";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "fail";
                        }
                    }
                    Session["filedata"] = null;
                }
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("DocUpload", "BankReconsilation", new { idi = "3235", FormName = Session["FormName"] });
            }
        }


        public void GetSampleStmt()
        {
            try
            {
                string fileLocation = Server.MapPath("~/Content/");
                int AOwner = Convert.ToInt32(Session["AcOwner"]);
                var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                fileLocation = fileLocation + "BankReconcil" + "\\SampleBankStmts";
                if (!System.IO.Directory.Exists(fileLocation))
                    System.IO.Directory.CreateDirectory(fileLocation);
                var ide = @Request["ide"]; var FileName = "BankTemplate.xlsx";
                //if (ide == "23139") FileName = "Sample.xlsx";
                //else if (ide == "23140") FileName = "SampleICICI.xlsx";
                fileLocation = fileLocation + "\\" + FileName;
                FileInfo file = new FileInfo(fileLocation);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", FileName));
                    Response.ContentType = "application/ms-excel";
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //return RedirectToAction("DocUpload", "BankReconsilation", new { FormName = Session["FormName"] });
            }
        }

        public class StatementUploadEditdetails
        {
            public string Import { get; set; }
            public string Bank { get; set; }
            public string Date { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string FileName { get; set; }
        }

    }
}
