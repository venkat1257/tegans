﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text.RegularExpressions;

namespace MeBizSuiteAppUI.Controllers
{
    public class StatementUploadController : Controller
    {
        //
        // GET: /StatementUpload/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        string providerName = "System.Data.SqlClient";
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Info()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                Dateformat();
                var strQry = "SELECT p.[COLUMN02 ] AS ID ,p.[COLUMN04 ] AS [Import#],format(p.COLUMN05,'" + Session["FormatDate"] + "') Date,p.[COLUMN09 ] AS [Name],f.[COLUMN04 ] AS [Bank],m.COLUMN09 [Created By] from SETABLE019 p " +
                " left join FITABLE001 f on f.COLUMN02=p.column08 and (f.COLUMNA03=p.COLUMNA03 or f.COLUMNA03 is null) and isnull(f.COLUMNA13,0)=0 and (f.COLUMNA02=p.COLUMNA02 or f.COLUMNA02 is null) " +
                " left join MATABLE010 m on m.COLUMN02=p.columna08 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 and (m.COLUMNA02=p.COLUMNA02 or m.COLUMNA02 is null) " +
                " where isnull(p.COLUMNA13,'False')='False' and " + OPUnit + " and (p.COLUMNA03=" + acID + " or p.COLUMNA03 is null) AND  p.COLUMN03=1629  ORDER BY p.COLUMNA06 desc ";
                alCol.AddRange(new List<string> { "ID", "Import#", "Date", "Name", "Bank", "Created By" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN05", "COLUMN09", "COLUMN04", "COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        public void Dateformat()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).FirstOrDefault().COLUMN04;
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter dad = new SqlDataAdapter(str, cn);
                DataTable dtd = new DataTable();
                dad.Fill(dtd); string DateFormat = null;
                if (dtd.Rows.Count > 0)
                {
                    DateFormat = dtd.Rows[0][0].ToString();
                    Session["FormatDate"] = dtd.Rows[0][0].ToString();
                }
                if (dtd.Rows[0][1].ToString() != "")
                    Session["ReportDate"] = dtd.Rows[0][1].ToString();
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult NewForm()
        {
            try
            {
                List<int> permission = Session["Permission"] as List<int>;
                if (permission.Contains(22935))
                {
                    List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                    string OPUnit = Convert.ToString(Session["OPUnit"]);
                    var acOW = Convert.ToInt32(Session["AcOwner"]);
                    Dateformat();
                    List<SelectListItem> Bank = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001 where COLUMN07=22266 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Bank.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    Bank = Bank.OrderBy(x => x.Text).ToList();
                    ViewData["Bank"] = new SelectList(Bank, "Value", "Text");
                    List<SelectListItem> TargetBank = new List<SelectListItem>();
                    SqlDataAdapter cmdt = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where COLUMN03=11194 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMNA13,0)=0", cn);
                    DataTable dtt = new DataTable();
                    cmdt.Fill(dtt);
                    for (int dd = 0; dd < dtt.Rows.Count; dd++)
                    {
                        TargetBank.Add(new SelectListItem { Value = dtt.Rows[dd]["COLUMN02"].ToString(), Text = dtt.Rows[dd]["COLUMN04"].ToString() });
                    }
                    TargetBank = TargetBank.OrderBy(x => x.Text).ToList();
                    ViewData["TargetBank"] = new SelectList(TargetBank, "Value", "Text");
                    ViewBag.TransNo = GetTransactionNo(1629);
                    DateTime now = DateTime.Now;
                    var frmDT = now.ToString(Session["FormatDate"].ToString());
                    var toDT = new DateTime(now.Year, now.Month, 1).ToString(Session["FormatDate"].ToString());
                    ViewBag.frmDT = frmDT;
                    ViewBag.toDT = toDT;
                    return View();
                }
                else
                {
                    Session["MessageFrom"] = "New Can't be Permitted ";
                    Session["SuccessMessageFrom"] = "fail";
                    return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }
        [HttpPost]
        public ActionResult FileNameCheck()
        {
            try
            {
                var File = Request.Files[0] as HttpPostedFileBase;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                var FileName = File.FileName; string fileExtension = "";
                var TargetBank = Request["TargetBank"].ToString();
                //Create a new DataTable.
                DataTable Idt = new DataTable();
                if (FileName.Length > 0)
                {
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    if (fileExtension != ".xlsx")
                        return Json(new { DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
                    SqlCommand cmdf = new SqlCommand("select COLUMN09 from SETABLE019 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN09='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ", cn);
                    SqlDataAdapter daf = new SqlDataAdapter(cmdf);
                    DataTable dtf = new DataTable();
                    daf.Fill(dtf);
                    if (dtf.Rows.Count > 0)
                        return Json(new { DataName = dtf.Rows[0][0].ToString(), FileData="", FileFormat="" }, JsonRequestBehavior.AllowGet);
                    string fileLocation = Server.MapPath("~/Content/");
                    int AOwner = Convert.ToInt32(Session["AcOwner"]);
                    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                    fileLocation = fileLocation + "BankReconcil" + "\\" + AOwnerName;
                    if (!System.IO.Directory.Exists(fileLocation))
                        System.IO.Directory.CreateDirectory(fileLocation);
                    fileLocation = fileLocation + "\\" + FileName;
                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);
                    //Save the uploaded Excel file.
                    string filePath = fileLocation;
                    File.SaveAs(fileLocation);
                    DataTable dt = new DataTable();

                    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileLocation, false))
                    {
                        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();
                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                        }
                        foreach (Row row in rows) //this will also include your header row...
                        {
                            DataRow tempRow = dt.NewRow();
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }
                            dt.Rows.Add(tempRow);
                        }
                    }
                    dt.Rows.RemoveAt(0);
                    Idt = dt;
                }
                DataColumnCollection columns = Idt.Columns;
                SqlCommand cmdc = new SqlCommand("select COLUMN04,COLUMN06 from SETABLE022 where COLUMN03=" + TargetBank + " and COLUMN05=1629 and isnull(COLUMNA13,0)=0 ", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc= new DataTable();
                dac.Fill(dtc);
                if (dtc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtc.Rows.Count; i++)
                    {
                        string colname = dtc.Rows[i]["COLUMN04"].ToString();
                        string actcolname = dtc.Rows[i]["COLUMN06"].ToString();
                        colname = colname.TrimEnd();
                        if (columns.Contains(colname) && actcolname != "")
                            Idt.Columns[colname].ColumnName = actcolname.TrimEnd();
                            else
                            return Json(new { DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
                    }
                    Session["filedata"] = Idt;
                    //return Json(new{ DataName = "", FileData = Idt, FileFormat = "" }, "application/json",JsonRequestBehavior.AllowGet);
                    return Json(new { DataName = "", FileData = "1", FileFormat = "" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return "";
            }
            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null && cell.DataType == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }

        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        private string GetValue(SpreadsheetDocument doc, Cell cell)
        {
            if (cell.CellValue != null)
            {
                string value = cell.CellValue.InnerText;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
                return value;
            }
            return "";
        }

        [HttpPost]
        public ActionResult NewForm(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).ToList().FirstOrDefault().COLUMN04.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                var File = Request.Files[0] as HttpPostedFileBase;
                var FileName = File.FileName; var tbldata = ""; string fileExtension = "";

                DataTable Idt = new DataTable();
                Idt = (DataTable)Session["filedata"];
                if (Idt.Rows.Count > 0)
                {
                    string Date = DateTime.Now.ToString();
                    string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                    if (opstatus == null || opstatus == "") OpUnit = null;
                    string insert = "Insert";
                    int OutParam = 0;
                    SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE019 ", cn);
                    SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                    DataTable dtt = new DataTable();
                    daa.Fill(dtt);
                    string HCol2 = dtt.Rows[0][0].ToString();
                    if (HCol2 == "") HCol2 = "999";
                    HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                    SqlCommand Cmd = new SqlCommand("usp_SET_BL_BankStmtUpload", cn);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1629");
                    Cmd.Parameters.AddWithValue("@COLUMN04", col["Import"]);
                    Cmd.Parameters.AddWithValue("@COLUMN05", Date);
                    var fromdt = col["From Date"].ToString();
                    var todt = col["To Date"].ToString();
                    if (fromdt == "" || fromdt == "undefined") Cmd.Parameters.AddWithValue("@COLUMN06", DateTime.Now);
                    else Cmd.Parameters.AddWithValue("@COLUMN06", DateTime.ParseExact(fromdt, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    if (todt == "" || todt == "undefined") Cmd.Parameters.AddWithValue("@COLUMN07", DateTime.Now);
                    else Cmd.Parameters.AddWithValue("@COLUMN07", DateTime.ParseExact(todt, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    Cmd.Parameters.AddWithValue("@COLUMN08", col["Bank"]);
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    Cmd.Parameters.AddWithValue("@COLUMN09", FileName.Replace(fileExtension, ""));
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", ACwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cn.Open();
                    int r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                    cn.Close();
                    if (r > 0)
                    {
                        var msg = string.Empty;
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 1).FirstOrDefault();
                        if (msgMaster != null)
                            msg = msgMaster.COLUMN03;
                        else
                            msg = "" + Session["FormName"] + " Successfully Created ";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    else
                    {
                        var msg = string.Empty;
                        var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 0).FirstOrDefault();
                        if (msgMaster != null)
                            msg = msgMaster.COLUMN03;
                        else
                            msg = "" + Session["FormName"] + " Creation Failed";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "fail";
                    }
                    if (OutParam > 0 && Idt.Rows.Count > 0)
                    {
                        int l = 0;
                        for (int i = 0; i < Idt.Rows.Count; i++)
                        {
                            SqlCommand cmd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE020 ", cn);
                            SqlDataAdapter da = new SqlDataAdapter(cmdd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            string HCol = dt.Rows[0][0].ToString();
                            if (HCol == "") HCol = "999";
                            HCol = (Convert.ToInt32(HCol) + 1).ToString();
                            string lDate = Idt.Rows[i]["COLUMN03"].ToString();
                            string Memo = Idt.Rows[i]["COLUMN06"].ToString();
                            string Reference = Idt.Rows[i]["COLUMN07"].ToString();
                            string Debit = Idt.Rows[i]["COLUMN08"].ToString().Replace(",", "");
                            string Credit = Idt.Rows[i]["COLUMN09"].ToString().Replace(",", "");
                            string Balance = Idt.Rows[i]["COLUMN10"].ToString().Replace(",", "");
                            if (Memo == "" || Memo == "undefined") Memo = "";
                            if (Reference == "" || Reference == "undefined") Reference = "";
                            if (Debit == "" || Debit == " " || Debit == null || Debit == "undefined") Debit = "0";
                            if (Credit == "" || Credit == " " || Credit == null || Credit == "undefined") Credit = "0";
                            if (Balance == "" || Balance == " " || Balance == null || Balance == "undefined") Balance = "0";
                            SqlCommand Cmdl = new SqlCommand("usp_SET_BL_BankStmtUpload", cn);
                            Cmdl.CommandType = CommandType.StoredProcedure;
                            Cmdl.Parameters.AddWithValue("@COLUMN02", HCol);
                            //if (lDate == "" || lDate == "undefined") lDate = DateTime.Now.ToString();
                            //else { lDate =Convert.ToDateTime(lDate).ToString(Session["FormatDate"].ToString()); Cmdl.Parameters.AddWithValue("@COLUMN03", DateTime.ParseExact(lDate, Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture)); }
                            Cmdl.Parameters.AddWithValue("@COLUMN03", lDate);
                            Cmdl.Parameters.AddWithValue("@COLUMN04", col["Bank"]);
                            Cmdl.Parameters.AddWithValue("@COLUMN05", null);
                            Cmdl.Parameters.AddWithValue("@COLUMN06", Memo);
                            Cmdl.Parameters.AddWithValue("@COLUMN07", Reference);
                            Cmdl.Parameters.AddWithValue("@COLUMN08", Debit);
                            Cmdl.Parameters.AddWithValue("@COLUMN09", Credit);
                            Cmdl.Parameters.AddWithValue("@COLUMN10", Balance);
                            Cmdl.Parameters.AddWithValue("@COLUMN11", OutParam);
                            Cmdl.Parameters.AddWithValue("@COLUMN12", 23134);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", ACwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "SETABLE020");
                            cn.Open();
                            l = Cmdl.ExecuteNonQuery();
                            cn.Close();
                        }
                        if (l > 0)
                        {
                            var msg = string.Empty;
                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 1).FirstOrDefault();
                            if (msgMaster != null)
                                msg = msgMaster.COLUMN03;
                            else
                                msg = "" + Session["FormName"] + " Successfully Created ";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "Success";
                        }
                        else
                        {
                            var msg = string.Empty;
                            var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 0).FirstOrDefault();
                            if (msgMaster != null)
                                msg = msgMaster.COLUMN03;
                            else
                                msg = "" + Session["FormName"] + " Creation Failed";
                            Session["MessageFrom"] = msg;
                            Session["SuccessMessageFrom"] = "fail";
                        }
                    }
                    Session["filedata"] = null;
                }
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono = "", TransNo = "";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (frmIDchk == 1629)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SETABLE019  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SETABLE019  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "IM";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SETABLE019  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SETABLE019  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                //pacmd.ExecuteNonQuery(); 
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }

        public void GetSampleStmt()
        {
            try
            {
                string fileLocation = Server.MapPath("~/Content/");
                int AOwner = Convert.ToInt32(Session["AcOwner"]);
                var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                fileLocation = fileLocation + "BankReconcil" + "\\SampleBankStmts";
                if (!System.IO.Directory.Exists(fileLocation))
                    System.IO.Directory.CreateDirectory(fileLocation);
                var ide = @Request["ide"]; var FileName = "Sample.xlsx";
                if (ide == "23139") FileName = "Sample.xlsx";
                else if (ide == "23140") FileName = "SampleICICI.xlsx";
                fileLocation = fileLocation + "\\" + FileName;
                FileInfo file = new FileInfo(fileLocation);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", FileName));
                    Response.ContentType = "application/ms-excel";
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        public class StatementUploadEditdetails
        {
            public string Import { get; set; }
            public string Bank { get; set; }
            public string Date { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string FileName { get; set; }
        }

        public ActionResult StatementUploadEdit()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).ToList().FirstOrDefault().COLUMN04.ToString();
                List<int> permission = Session["Permission"] as List<int>;
                if (permission.Contains(22933))
                {
                    SqlDataAdapter cmdv = new SqlDataAdapter("SELECT [COLUMN02 ] AS ID ,[COLUMN04 ] AS [Import#],format(COLUMN05,'" + Session["FormatDate"] + "') [Date],format(COLUMN06,'" + Session["FormatDate"] + "') FromDate,format(COLUMN07,'" + Session["FormatDate"] + "') ToDate,[COLUMN08 ] AS [Bank],[COLUMN09 ] AS [Name] from SETABLE019 WHERE  COLUMN02=" + @Request["ide"] + "  and isnull(columna13,0)=0   and " + OPUnit + " and (COLUMNA03=" + acID + " or COLUMNA03 is null)", cn);
                    DataTable dtv = new DataTable();
                    cmdv.Fill(dtv);
                    List<StatementUploadEditdetails> all1 = new List<StatementUploadEditdetails>();
                    all1.Add(new MeBizSuiteAppUI.Controllers.StatementUploadController.StatementUploadEditdetails
                    {
                        Import = "",
                        Bank = "",
                        FromDate = "",
                        Date = "",
                        ToDate = "",
                        FileName = ""
                    });
                    all1[0].Import = dtv.Rows[0]["Import#"].ToString();
                    all1[0].FromDate = dtv.Rows[0]["FromDate"].ToString();
                    all1[0].Date = dtv.Rows[0]["Date"].ToString();
                    all1[0].ToDate = dtv.Rows[0]["ToDate"].ToString();
                    all1[0].Bank = dtv.Rows[0]["Bank"].ToString();
                    all1[0].FileName = dtv.Rows[0]["Name"].ToString();
                    List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                    OPUnit = Convert.ToString(Session["OPUnit"]);
                    var acOW = Convert.ToInt32(Session["AcOwner"]);
                    List<SelectListItem> Bank = new List<SelectListItem>();
                    SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001 where COLUMN07=22266 AND  ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)   AND (COLUMNA03='" + Session["AcOwner"] + "' OR COLUMNA03 IS NULL) and isnull(COLUMN12,0)=0 and isnull(COLUMNA13,0)=0", cn);
                    DataTable dtdata = new DataTable();
                    cmddl.Fill(dtdata);
                    for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                    {
                        Bank.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                    }
                    Bank = Bank.OrderBy(x => x.Text).ToList();
                    ViewData["Bank"] = new SelectList(Bank, "Value", "Text", selectedValue: all1[0].Bank);
                    return View(all1);
                }
                else
                {
                    Session["MessageFrom"] = "Edit Can't be Permitted ";
                    Session["SuccessMessageFrom"] = "fail";
                    return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult StatementUploadEdit(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update";
                cn.Open();
                var idi = ""; var ide = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE019 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodePrinterSetup", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1629");
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Name"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["Order"]);
                string defaultflag = Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                string Message = Convert.ToString(col["Message"]);
                Message = Message.Replace("@Sales@", "'Sales'");
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMN07", Message);
                Cmd.Parameters.AddWithValue("@COLUMN08", col["Barcode Labels"]);
                string Query = Convert.ToString(col["Barcode Query"]);
                Query = Query.Replace("@Sales@", "'Sales'");
                Cmd.Parameters.AddWithValue("@COLUMN09", Query);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 3).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Updation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult StatementUploadDelete()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).ToList().FirstOrDefault().COLUMN04.ToString();
                var ide = @Request["ide"]; var idi = "";
                //var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete";
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BankStmtUpload", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 4).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1629 && q.COLUMN05 == 5).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Deletion Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult StatementUploadView()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1629).ToList().FirstOrDefault().COLUMN04.ToString();

                SqlDataAdapter cmdv = new SqlDataAdapter("SELECT p.[COLUMN02 ] AS ID ,p.[COLUMN04 ] AS [Import#],format(p.COLUMN05,'" + Session["FormatDate"] + "') [Date],format(p.COLUMN06,'" + Session["FormatDate"] + "') FromDate,format(p.COLUMN07,'" + Session["FormatDate"] + "') ToDate,f.[COLUMN04 ] AS [Bank],p.[COLUMN09 ] AS [Name] from SETABLE019 p left join FITABLE001 f on f.COLUMN02=p.COLUMN08 and  f.COLUMNA03=p.COLUMNA03 and  isnull(f.COLUMNA13,0)=0 WHERE  p.COLUMN02=" + @Request["ide"] + "  and isnull(p.columna13,0)=0   and " + OPUnit + " and (p.COLUMNA03=" + acID + " or p.COLUMNA03 is null)", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                List<StatementUploadEditdetails> all1 = new List<StatementUploadEditdetails>();
                all1.Add(new MeBizSuiteAppUI.Controllers.StatementUploadController.StatementUploadEditdetails
                {
                    Import = "",
                    Bank = "",
                    FromDate = "",
                    Date = "",
                    ToDate = "",
                    FileName = ""
                });
                all1[0].Import = dtv.Rows[0]["Import#"].ToString();
                all1[0].FromDate = dtv.Rows[0]["FromDate"].ToString();
                all1[0].Date = dtv.Rows[0]["Date"].ToString();
                all1[0].ToDate = dtv.Rows[0]["ToDate"].ToString();
                all1[0].Bank = dtv.Rows[0]["Bank"].ToString();
                all1[0].FileName = dtv.Rows[0]["Name"].ToString();
                //int? acID = (int?)Session["AcOwner"];
                //string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                SqlCommand cmd = new SqlCommand("SELECT p.COLUMN03 [Date],p.[COLUMN06] AS [Memo],p.[COLUMN07] Ref,p.[COLUMN08] Debit,p.[COLUMN09] Credit,p.[COLUMN10] Balance,f.[COLUMN04] AS [Bank],m.[COLUMN04] AS [Status],h.COLUMN08 BankId,p.COLUMN02 chkrow,p.[COLUMN12] AS [BankReconcilid] from SETABLE020 p inner join SETABLE019 h on h.COLUMN01=p.COLUMN11 and h.COLUMNA03=p.COLUMNA03 and h.COLUMNA02=p.COLUMNA02 and isnull(h.COLUMNA13,0)=0 left join FITABLE001 f on f.COLUMN02=h.COLUMN08 and f.COLUMNA03=p.COLUMNA03 left join MATABLE002 m on m.COLUMN02=p.COLUMN12 and isnull(m.COLUMNA13,0)=0 where h.COLUMN02=" + @Request["ide"] + " and p.COLUMNA03=" + acID + " and isnull(p.COLUMNA13,0)=0", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dts = new DataTable();
                da.Fill(dts);
                var result = new List<dynamic>();
                var depositlst = new List<dynamic>();
                var withdrawlst = new List<dynamic>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                foreach (DataRow row in dts.Rows)
                {
                    var obj = (IDictionary<string, object>)new ExpandoObject();
                    foreach (DataColumn col in dts.Columns)
                    {
                        obj.Add(col.ColumnName, row[col.ColumnName]);
                    }
                    result.Add(obj);
                }
                ViewBag.cols = result;
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "StatementUpload", new { FormName = Session["FormName"] });
            }
        }

        //Excel Import by Venkat
        public ActionResult FileNameCheckForIA()
        {
            try
            {
                var File = Request.Files[0] as HttpPostedFileBase;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                var FileName = File.FileName; string fileExtension = "";
                //var TargetBank = Request["TargetBank"].ToString();
                DataTable Idt = new DataTable();
                if (FileName.Length > 0)
                {
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    if (fileExtension != ".xlsx")
                        return Json(new { DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
                    //SqlCommand cmdf = new SqlCommand("select COLUMN04 from MATABLE007 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN04='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ", cn);
                    //SqlDataAdapter daf = new SqlDataAdapter(cmdf);
                    //DataTable dtf = new DataTable();
                    //daf.Fill(dtf);
                    //if (dtf.Rows.Count > 0)
                    //    return Json(new { DataName = dtf.Rows[0][0].ToString(), FileData = "", FileFormat = "" }, JsonRequestBehavior.AllowGet);
                    //string str = "select COLUMN02,COLUMN04 from MATABLE002 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN03=11119 and COLUMN04='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ";
                    //SqlDataAdapter daf1 = new SqlDataAdapter(str, cn);
                    //DataTable dtf1 = new DataTable();
                    //daf1.Fill(dtf1);
                    //string str1 = "select COLUMN04 from FITABLE043 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN04='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ";
                    //SqlDataAdapter daf2 = new SqlDataAdapter(str, cn);
                    //DataTable dtf2 = new DataTable();
                    //daf2.Fill(dtf2);
                    string fileLocation = Server.MapPath("~/Content/");
                    int AOwner = Convert.ToInt32(Session["AcOwner"]);
                    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                    fileLocation = fileLocation + "InventoryAdjustments" + "\\" + AOwnerName;
                    if (!System.IO.Directory.Exists(fileLocation))
                        System.IO.Directory.CreateDirectory(fileLocation);
                    fileLocation = fileLocation + "\\" + FileName;
                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);

                    string filePath = fileLocation;
                    File.SaveAs(fileLocation);
                    DataTable dt = new DataTable();

                    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileLocation, false))
                    {
                        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();
                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                        }
                        foreach (Row row in rows)
                        {
                            DataRow tempRow = dt.NewRow();
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {

                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }
                            dt.Rows.Add(tempRow);
                        }
                    }
                    dt.Rows.RemoveAt(0);
                    Idt = dt;
                }
                DataColumnCollection columns = Idt.Columns;
                string htmlstring = "";
                //var fname = Session["FormName"];
                //var formid = dc.CONTABLE0010.Where(a => a.COLUMN04 == fname).FirstOrDefault();
                //var data = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02).ToList();
                //var itemdata = data.Where(a => a.COLUMN11 == "Item Level" && a.COLUMN07 != "N").ToList();
                //var tblid = itemdata.Select(a => a.COLUMN04).Distinct().ToList();
                SqlConnection cn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                //SqlCommand acmd = new SqlCommand(
                //              "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06, iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN08,CONTABLE006.COLUMN13,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15 From CONTABLE006  " +
                //            "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                //            " left join SETABLE011 s on s.COLUMN04 = " + tblid[0] + " and CONTABLE006.COLUMN05 =s.COLUMN05 and s.COLUMN06 = " + formid.COLUMN02 + "  " +
                //            " left join SETABLE012 s1 on s.COLUMN04 = s1.COLUMN04  and s1.COLUMN05 =s.COLUMN05 and s.COLUMN06 = s1.COLUMN06 and s1.COLUMNA03 = " + Session["AcOwner"] + " " +
                //            "where CONTABLE005.COLUMN03 =" + tblid[0] + " and CONTABLE006.COLUMN04 =" + tblid[0] + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level'  and  iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)='Y'", cn);
                string dddataI = "";
                string dddataU = "";
                string dddataL = "";
                dddataI += "<option value='0'>--Select--</option>";
                dddataU += "<option value='0'>--Select--</option>";
                dddataL += "<option value='0'>--Select--</option>";
                string lot = "N";
                SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1357 and column05='COLUMN23'  and column04=110010845 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                DataTable dtlt = new DataTable();
                dalt.Fill(dtlt);
                if (dtlt.Rows.Count > 0) lot = dtlt.Rows[0][0].ToString();
                for (int i = 0; i < Idt.Rows.Count; i++)
                {
                    string Items = Idt.Rows[i]["Items"].ToString();
                    string Units = Idt.Rows[i]["Units"].ToString();
                    string Lot = Idt.Rows[i]["Lot"].ToString();
                    SqlCommand cmd = new SqlCommand("USP_FIN_INVIMPORTECXEL", cn1);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Items", Items);
                    cmd.Parameters.AddWithValue("@Units", Units);
                    cmd.Parameters.AddWithValue("@Lot", Lot);
                    cmd.Parameters.AddWithValue("@OPUnit", Convert.ToString(Session["OPUnit"]));
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    SqlDataAdapter daS = new SqlDataAdapter(cmd);
                    DataTable dtS = new DataTable();
                    daS.Fill(dtS);
                    if (dtS.Rows.Count > 0)
                    {
                        dddataI = "<option value=" + dtS.Rows[0]["ID"] + " >" + Idt.Rows[i]["Items"] + "</option>";
                        dddataU = "<option value=" + dtS.Rows[0]["UOMID"] + " >" + Idt.Rows[i]["Units"] + "</option>";
                        htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''  name=''  type='checkbox'   value='' checked='true'/></td>";
                        htmlstring += "<td><input type='text' id='FITABLE015COLUMN04' class='txtgridclass' pattern=''  itemid='FITABLE015COLUMN04' name='UPC#' style='min-width: 150px'></td>";
                        htmlstring += "<td><select class='gridddl' id='FITABLE015COLUMN03' name='Items' style='min-width: 150px'>" + dddataI + "</select></td>";
                        htmlstring += "<td><textarea id='FITABLE015COLUMN05' class='txtgridclass' pattern=''  itemid='FITABLE015COLUMN05' name='Item Desc' style='min-width: 150px'></textarea></td>";
                        htmlstring += "<td><select class='gridddl' id='FITABLE015COLUMN22' name='Units' style='min-width: 150px'>" + dddataU + "</select></td>";
                        if (lot == "Y")
                        {
                            dddataL = "<option value=" + dtS.Rows[0]["LotID"] + " >" + Idt.Rows[i]["Lot"] + "</option>";
                            htmlstring += "<td><select class='gridddl' id='FITABLE015COLUMN23' name='Lot#' checked='true' style='min-width: 150px'>" + dddataL + "</select></td>";
                        }
                        htmlstring += "<td><input id='FITABLE015COLUMN06' class='txtgridclass' pattern='' itemid='FITABLE015COLUMN06' name='QTY On Hand' readonly='' value='" + dtS.Rows[0]["QtyAvailable"] + "'></td>";
                        htmlstring += "<td><input id='FITABLE015COLUMN07' class='txtgridclass' pattern='' itemid='FITABLE015COLUMN07' name='Total Amount' readonly=''></td>";
                        htmlstring += "<td><input id='FITABLE015COLUMN08' class='txtgridclass' pattern='' itemid='FITABLE015COLUMN08' name='Increased By' value='" + Idt.Rows[i]["Increased By"] + "'></td>";
                        htmlstring += "<td><input type='text' id='FITABLE015COLUMN16' class='txtgridclass'  pattern=''    itemid='FITABLE015COLUMN16' name='Decreased By' value='" + Idt.Rows[i]["Decreased By"] + "'></td>";
                        htmlstring += "<td><input type='text' id='FITABLE015COLUMN09' class='txtgridclass'  pattern=''    itemid='FITABLE015COLUMN09' name='Remaining Qty'></td>";
                        htmlstring += "<td><input type='text' id='FITABLE015COLUMN17' class='txtgridclass'  pattern=''    itemid='FITABLE015COLUMN09' name='Current Qty'></td>";
                        htmlstring += "<td><input id='FITABLE015COLUMN10' class='txtgridclass' pattern='' itemid='FITABLE015COLUMN10' name='Estimated Price' value='" + Idt.Rows[i]["Estimated Price"].ToString() + "'></td></tr>";
                    }
                    else if (htmlstring == "")
                    {
                        htmlstring = "1";
                    }
                }
                return Json(new { Data = htmlstring, Data1 = Idt.Rows.Count }, JsonRequestBehavior.AllowGet);
                //return Json(new { Data = htmlstring, DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult FileNameCheckForOBC()
        {
            try
            {
                var File = Request.Files[0] as HttpPostedFileBase;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                int ACwner = Convert.ToInt32(Session["AcOwner"]);
                var FileName = File.FileName; string fileExtension = "";
                //var TargetBank = Request["TargetBank"].ToString();
                DataTable Idt = new DataTable();
                if (FileName.Length > 0)
                {
                    fileExtension = System.IO.Path.GetExtension(FileName);
                    if (fileExtension != ".xlsx")
                        return Json(new { DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
                    SqlCommand cmdf = new SqlCommand("select COLUMN04 from MATABLE007 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN04='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ", cn);
                    SqlDataAdapter daf = new SqlDataAdapter(cmdf);
                    DataTable dtf = new DataTable();
                    daf.Fill(dtf);
                    if (dtf.Rows.Count > 0)
                        return Json(new { DataName = dtf.Rows[0][0].ToString(), FileData = "", FileFormat = "" }, JsonRequestBehavior.AllowGet);
                    string str = "select COLUMN02,COLUMN04 from MATABLE002 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN03=11119 and COLUMN04='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ";
                    SqlDataAdapter daf1 = new SqlDataAdapter(str, cn);
                    DataTable dtf1 = new DataTable();
                    daf1.Fill(dtf1);
                    string str1 = "select COLUMN04 from FITABLE043 where ( " + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) and COLUMN04='" + FileName.Replace(fileExtension, "") + "' and COLUMNA03=" + ACwner + " and isnull(COLUMNA13,0)=0 ";
                    SqlDataAdapter daf2 = new SqlDataAdapter(str, cn);
                    DataTable dtf2 = new DataTable();
                    daf2.Fill(dtf2);
                    string fileLocation = Server.MapPath("~/Content/");
                    int AOwner = Convert.ToInt32(Session["AcOwner"]);
                    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                    fileLocation = fileLocation + "InventoryAdjustments" + "\\" + AOwnerName;
                    if (!System.IO.Directory.Exists(fileLocation))
                        System.IO.Directory.CreateDirectory(fileLocation);
                    fileLocation = fileLocation + "\\" + FileName;
                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);

                    string filePath = fileLocation;
                    File.SaveAs(fileLocation);
                    DataTable dt = new DataTable();

                    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileLocation, false))
                    {
                        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();
                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                        }
                        foreach (Row row in rows)
                        {
                            DataRow tempRow = dt.NewRow();
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {

                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }
                            dt.Rows.Add(tempRow);
                        }
                    }
                    dt.Rows.RemoveAt(0);
                    Idt = dt;
                }
                DataColumnCollection columns = Idt.Columns;
                string htmlstring = "";
                string htmlstring1 = "";
                SqlConnection cn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
               
                string dddataI = "";
                string dddataA = "";
                string dddataP = "";
                dddataI += "<option value='0'>--Select--</option>";
                dddataA += "<option value='0'>--Select--</option>";
                dddataP += "<option value='0'>--Select--</option>";
                for (int i = 0; i < Idt.Rows.Count; i++)
                {
                   int saveformid = Convert.ToInt32(Session["id"]);
                   if (saveformid == 1615)
                   {
                       string Customer = Idt.Rows[i]["Customer"].ToString();
                       SqlCommand cmd = new SqlCommand("OBCIMPORTECXEL", cn1);
                       cmd.CommandType = CommandType.StoredProcedure;
                       cmd.Parameters.AddWithValue("@Customer", Customer);
                       cmd.Parameters.AddWithValue("@OPUnit", Convert.ToString(Session["OPUnit"]));
                       cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                       SqlDataAdapter daS = new SqlDataAdapter(cmd);
                       DataTable dtS = new DataTable();
                       daS.Fill(dtS);
                       if (dtS.Rows.Count > 0)
                       {
                           dddataI = "<option value=" + dtS.Rows[0]["ID"] + " >" + Idt.Rows[i]["Customer"] + "</option>";
                           dddataA = "<option value=" + dtS.Rows[0]["AID"] + " >" + dtS.Rows[0]["AR"] + "</option>";
                           htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''  name=''  type='checkbox'   value='' checked='true'/></td>";
                           htmlstring += "<td><select class='gridddl' id='FITABLE018COLUMN08' name='Account' style='min-width: 150px'>" + dddataA + "</select></td>";
                           htmlstring += "<td><input id='FITABLE018COLUMN09' class='txtgridclass' pattern='' itemid='FITABLE018COLUMN09' name='Increased By' value='" + Idt.Rows[i]["Increased By"] + "'></td>";
                           htmlstring += "<td><input id='FITABLE018COLUMN10' class='txtgridclass' pattern='' itemid='FITABLE018COLUMN10' name='Decreased By' value='" + Idt.Rows[i]["Decreased By"] + "'></td>";
                           htmlstring += "<td><select class='gridddl' id='FITABLE018COLUMN07' name='Customer' style='min-width: 150px'>" + dddataI + "</select></td>";
                           htmlstring += "<td><textarea id='FITABLE018COLUMN12' class='txtgridclass' pattern='' itemid='FITABLE018COLUMN12' name='Memo'>" + Idt.Rows[i]["Memo"] + "</textarea></td></tr>";
                       }
                       else if (htmlstring == "")
                       {
                           htmlstring = "1";
                       }

                   }
                   else if (saveformid == 1616)
                   {
                       string Vendor = Idt.Rows[i]["Vendor"].ToString();
                       SqlCommand cmd = new SqlCommand("OBVIMPORTECXEL", cn1);
                       cmd.CommandType = CommandType.StoredProcedure;
                       cmd.Parameters.AddWithValue("@Vendor", Vendor);
                       cmd.Parameters.AddWithValue("@OPUnit", Convert.ToString(Session["OPUnit"]));
                       cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                       SqlDataAdapter daS = new SqlDataAdapter(cmd);
                       DataTable dtS = new DataTable();
                       daS.Fill(dtS);
                       if (dtS.Rows.Count > 0)
                       {
                           dddataI = "<option value=" + dtS.Rows[0]["ID"] + " >" + Idt.Rows[i]["Vendor"] + "</option>";
                           dddataA = "<option value=" + dtS.Rows[0]["AID"] + " >" + dtS.Rows[0]["AR"] + "</option>";
                    htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''  name=''  type='checkbox'   value='' checked='true'/></td>";
                    htmlstring += "<td><select class='gridddl' id='FITABLE018COLUMN08' name='Account' style='min-width: 150px'>" + dddataA + "</select></td>";
                    htmlstring += "<td><input id='FITABLE018COLUMN09' class='txtgridclass' pattern='' itemid='FITABLE018COLUMN09' name='Increased By' value='" + Idt.Rows[i]["Increased By"] + "'></td>";
                    htmlstring += "<td><input id='FITABLE018COLUMN10' class='txtgridclass' pattern='' itemid='FITABLE018COLUMN10' name='Decreased By' value='" + Idt.Rows[i]["Decreased By"] + "'></td>";
                    htmlstring += "<td><select class='gridddl' id='FITABLE018COLUMN07' name='Customer' style='min-width: 150px'>" + dddataI + "</select></td>";
                    htmlstring += "<td><textarea id='FITABLE018COLUMN12' class='txtgridclass' pattern='' itemid='FITABLE018COLUMN12' name='Memo'>" + Idt.Rows[i]["Memo"] + "</textarea></td></tr>";
                       }
                       else if (htmlstring == "")
                       {
                           htmlstring = "1";
                       }
                   }
                   else if (saveformid == 1525)
                   {
                       string Account = Idt.Rows[i]["Account"].ToString();
                       SqlCommand cmd = new SqlCommand("PVIMPORTECXEL", cn1);
                       cmd.CommandType = CommandType.StoredProcedure;
                       cmd.Parameters.AddWithValue("@Account", Account);
                       cmd.Parameters.AddWithValue("@OPUnit", Convert.ToString(Session["OPUnit"]));
                       cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                       SqlDataAdapter daS = new SqlDataAdapter(cmd);
                       DataTable dtS = new DataTable();
                       daS.Fill(dtS);
                       if (dtS.Rows.Count > 0)
                       {
                           dddataI = "<option value=" + dtS.Rows[0]["AID"] + " >" + Idt.Rows[i]["Account"] + "</option>";
                           dddataA = "<option value=" + dtS.Rows[0]["TID"] + " >" + dtS.Rows[0]["TNAME"] + "</option>";
                           dddataP = "<option value=" + dtS.Rows[0]["Project"] + " >" + dtS.Rows[0]["Project"] + "</option>";
                           htmlstring += "<tr><td><input  id='checkRow' class='chkclass' itemid=''  name=''  type='checkbox'   value='' checked='true'/></td>";
                           htmlstring += "<td><select class='gridddl' id='FITABLE022COLUMN03' name='Account' style='min-width: 150px'>" + dddataI + "</select></td>";
                           htmlstring += "<td><textarea id='FITABLE022COLUMN04' class='txtgridclass' pattern='' itemid='FITABLE022COLUMN04' name='Memo'>" + Idt.Rows[i]["Memo"] + "</textarea></td>";
                           htmlstring += "<td><select class='gridddl' id='FITABLE022COLUMN08' name='Project' style='min-width: 150px'>" + dddataP + "</select></td>";
                           htmlstring += "<td><input id='FITABLE022COLUMN05' class='txtgridclass' pattern='' itemid='FITABLE022COLUMN05' name='Amount' value='" + Idt.Rows[i]["Amount"] + "'></td>";
                           htmlstring += "<td><select class='gridddl' id='FITABLE022COLUMN14' name='Tax' style='min-width: 150px'>" + dddataA + "</select></td>";
                           htmlstring += "<td><input id='FITABLE022COLUMN06' class='txtgridclass' pattern='' itemid='FITABLE022COLUMN06' name='Tax Amount'></td>";
                           htmlstring += "<td><input id='FITABLE022COLUMN15' class='txtgridclass' pattern='' itemid='FITABLE022COLUMN15' name='Total' value='" + Idt.Rows[i]["Total"] + "'></td>";
                           htmlstring += "<td><input id='FITABLE022COLUMN16' class='txtgridclass' pattern='' itemid='FITABLE022COLUMN16' name='Advance' value='" + Idt.Rows[i]["Advance"] + "'></td>";
                           htmlstring += "<td><input id='FITABLE022COLUMN17' class='txtgridclass' pattern='' itemid='FITABLE022COLUMN17' name='Payment' value='" + Idt.Rows[i]["Payment"] + "'></td></tr>";
                       }
                       else if(htmlstring =="")
                       {
                         htmlstring = "1";
                       }
                   }
                      }
                return Json(new { Data = htmlstring, Data1 = Idt.Rows.Count, }, JsonRequestBehavior.AllowGet);
                //return Json(new { DataName = "", FileData = "", FileFormat = "1" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public void EcxelImportStmt()
        {
            try
            {
                string fileLocation = Server.MapPath("~/Content/");
                int AOwner = Convert.ToInt32(Session["AcOwner"]);
                var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                var FileName = "";
                 int saveformid = Convert.ToInt32(Session["id"]);
                 if (saveformid == 1357)
                 {
                     fileLocation = fileLocation + "ExcelImport" + "\\InvAdj";
                     if (!System.IO.Directory.Exists(fileLocation))
                     System.IO.Directory.CreateDirectory(fileLocation);
                     FileName = "InvAdj.xlsx";
                 }
                 if (saveformid == 1615)
                 {
                     fileLocation = fileLocation + "ExcelImport" + "\\OBCV";
                     if (!System.IO.Directory.Exists(fileLocation))
                     System.IO.Directory.CreateDirectory(fileLocation);
                     FileName = "OpeningBalanceCustomer.xlsx";
                 }
                 if (saveformid == 1616)
                 {
                     fileLocation = fileLocation + "ExcelImport" + "\\OBCV";
                     if (!System.IO.Directory.Exists(fileLocation))
                         System.IO.Directory.CreateDirectory(fileLocation);
                     FileName = "OpeningBalanceVendor.xlsx";
                 }
                 if (saveformid == 1525)
                 {
                     fileLocation = fileLocation + "ExcelImport" + "\\OBCV";
                     if (!System.IO.Directory.Exists(fileLocation))
                         System.IO.Directory.CreateDirectory(fileLocation);
                     FileName = "Payment Vocher.xlsx";
                 }
                fileLocation = fileLocation + "\\" + FileName;
                FileInfo file = new FileInfo(fileLocation);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", FileName));
                    Response.ContentType = "application/ms-excel";
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                
            }
        }

        
    }
}
