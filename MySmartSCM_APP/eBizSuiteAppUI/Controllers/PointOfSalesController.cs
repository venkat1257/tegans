﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class PointOfSalesController : Controller
    {
        //
        // GET: /PointOfSales/
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        CommonController taxlst = new CommonController();
        public ActionResult Create()
        {
            try
            {
                //List<SelectListItem> Customer = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                //DataTable dtdata = new DataTable();
                //cmddl.Fill(dtdata);
                //for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                //{
                //    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                //}
                //ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Item = new List<SelectListItem>();
				//EMPHCS1849 rajasekhar reddy patakota 02/01/2017 Sales Receipt Invertory checking for Track QOH=0 items 
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' AND COLUMN05='ITTY001' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN02 desc", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
				//EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                List<SelectListItem> Attendedby = new List<SelectListItem>();
                SqlDataAdapter cmda = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                DataTable dta = new DataTable();
                cmda.Fill(dta);
                var salesrep="";
                for (int dd = 0; dd < dta.Rows.Count; dd++)
                {
                    if (Convert.ToString(Session["eid"]) == dta.Rows[dd]["COLUMN02"].ToString())
                        salesrep = dta.Rows[dd]["COLUMN02"].ToString();
                    Attendedby.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: salesrep);
                List<SelectListItem> Customer1 = new List<SelectListItem>();
                SqlDataAdapter cmddld = new SqlDataAdapter("select COLUMN02,COLUMN05,COLUMN04 from SATABLE002  where  (COLUMN40='True' or COLUMN40=1) and COLUMN05 !='' and (" + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' order by COLUMN05 ", cn);
                DataTable dtdatad = new DataTable();
                cmddld.Fill(dtdatad);
                SqlDataAdapter cmddl1 = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  COLUMN05 !='' and (" + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False' order by COLUMN05 ", cn);
                DataTable dtdata1 = new DataTable();
                cmddl1.Fill(dtdata1);
                var DefaultFlag = "";var  DefaultFlagval = "";
                if (dtdatad.Rows.Count > 0) DefaultFlagval = dtdatad.Rows[0]["COLUMN02"].ToString();
                for (int dd = 0; dd < dtdata1.Rows.Count; dd++)
                {
                    if (DefaultFlagval == dtdata1.Rows[dd]["COLUMN02"].ToString())
                        DefaultFlag = dtdata1.Rows[dd]["COLUMN02"].ToString();
                    Customer1.Add(new SelectListItem { Value = dtdata1.Rows[dd]["COLUMN02"].ToString(), Text = dtdata1.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer1, "Value", "Text", selectedValue: DefaultFlag);
                List<SelectListItem> Amtin = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Amtin.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: Session["TaxInclusive"]);
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
               // KK Issues By Venkat
                string TransNo = "";
                {
                    TransNo = GetTransactionNo(1532);
                    ViewBag.TransNo = TransNo;
                }
                SqlDataAdapter cmdS = new SqlDataAdapter("SELECT IIF(EXISTS(SELECT COLUMN04 FROM CONTABLE031 WHERE  COLUMNA03='" + Session["ACOWNER"] + "'  AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND COLUMN03='ALLOW PAYMENT'),(SELECT COLUMN04 FROM CONTABLE031 WHERE  COLUMNA03='" + Session["ACOWNER"] + "'  AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND COLUMN03='ALLOW PAYMENT'),0) COLUMN01, " +
                "IIF(EXISTS(SELECT MAX(COLUMN02) FROM FITABLE001 WHERE  COLUMNA03='" + Session["ACOWNER"] + "'  AND (" + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND ISNULL(COLUMN14,0)=1  AND (COLUMN07 in(22266,22409)) AND ISNULL(COLUMN12,'FALSE')='FALSE'),(SELECT MAX(COLUMN02) FROM FITABLE001 WHERE  COLUMNA03='" + Session["ACOWNER"] + "' AND (" + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)  AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND ISNULL(COLUMN14,0)=1  AND (COLUMN07 in(22266,22409)) AND ISNULL(COLUMN12,'FALSE')='FALSE'),0) COLUMN02", cn);
                DataTable dtaS = new DataTable();
                cmdS.Fill(dtaS);
                if (dtaS.Rows.Count > 0)
                {
                    Session["PayRule"] = dtaS.Rows[0][0];
                    Session["PayRuleAC"] = dtaS.Rows[0][1];
                }
                else
                {
                    Session["PayRule"] = 0;
                    Session["PayRuleAC"] = 0;
                }
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1532).ToList().FirstOrDefault().COLUMN04;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
				//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult EditSave(string totamt, string payamt, string totamta, string payacc, string Qty, string SubTotal, string Discount, string TaxAmt,   string Customer, string Type, string CardNo)
        {
            try
            {
                int OutParam = 0;
               int id=  Convert.ToInt32(Session["IDE"]);

              string Date = DateTime.Now.ToString();
              string OpUnit = Convert.ToString(Session["OPUnit"]);
              string opstatus = Convert.ToString(Session["OPUnitstatus"]);
              if (opstatus == null) OpUnit = null;
              string AOwner = Convert.ToString(Session["AcOwner"]);
              string insert = "Insert", TransNo = "";
              cn.Open();
              //Payment
              SqlCommand cmdp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE011 ", cn);
              SqlDataAdapter dap = new SqlDataAdapter(cmdp);
              DataTable dtp = new DataTable();
              dap.Fill(dtp);
              string HColp = dtp.Rows[0][0].ToString();
              HColp = (Convert.ToInt32(HColp) + 1).ToString();

              SqlCommand CmdP = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
              CmdP.CommandType = CommandType.StoredProcedure;
              CmdP.Parameters.AddWithValue("@COLUMN02", HColp);
              CmdP.Parameters.AddWithValue("@COLUMN03", "1278");
              TransNo = GetTransactionNo(1278);
              CmdP.Parameters.AddWithValue("@COLUMN04", TransNo);
              CmdP.Parameters.AddWithValue("@COLUMN05", Customer);
              //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
              CmdP.Parameters.AddWithValue("@COLUMN06", "");
              CmdP.Parameters.AddWithValue("@COLUMN18", 1002);
              CmdP.Parameters.AddWithValue("@COLUMN19", Date);
              CmdP.Parameters.AddWithValue("@COLUMN07", id);
              CmdP.Parameters.AddWithValue("@COLUMN08", payacc);
              CmdP.Parameters.AddWithValue("@COLUMN14", OpUnit);
              CmdP.Parameters.AddWithValue("@COLUMN13", payamt);
              CmdP.Parameters.AddWithValue("@COLUMN10", Type);
              CmdP.Parameters.AddWithValue("@COLUMN09", CardNo);
              CmdP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
              CmdP.Parameters.AddWithValue("@COLUMNA03", AOwner);
              CmdP.Parameters.AddWithValue("@COLUMNA06", Date);
              CmdP.Parameters.AddWithValue("@COLUMNA07", Date);
              CmdP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
              CmdP.Parameters.AddWithValue("@COLUMNA12", 1);
              CmdP.Parameters.AddWithValue("@COLUMNA13", 0);
              CmdP.Parameters.AddWithValue("@Direction", insert);
              CmdP.Parameters.AddWithValue("@TabelName", "SATABLE011");
              CmdP.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
              int pm = CmdP.ExecuteNonQuery();
              OutParam = Convert.ToInt32(CmdP.Parameters["@ReturnValue"].Value);

              eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
              int? Oper = Convert.ToInt32(Session["AUTO"]);
              if (Oper == null || Oper == 0)
                  Oper = Convert.ToInt32(Session["OPUnit1"]);
              if (Oper == 0)
                  Oper = null;
              int? acOW = Convert.ToInt32(Session["AcOwner"]);
              var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1277).FirstOrDefault().COLUMN04.ToString();
              var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
               

              if (pm > 0)
              {
                  custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1278).FirstOrDefault().COLUMN04.ToString();
                  listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                  if (Oper == null)
                      listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                  if (listTM == null)
                  {
                      listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                      if (Oper == null)
                          listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                      if (listTM == null)
                      {
                          listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                      }
                  }
                  if (listTM != null)
                  {
                      MYTABLE002 product = listTM;
                      if (listTM.COLUMN09 == "")
                          product.COLUMN09 = (1).ToString();
                      else
                          product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                      act1.SaveChanges();
                  }
              }
              int lP = 0;
              SqlCommand cmddlp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE012 ", cn);
              SqlDataAdapter daalp = new SqlDataAdapter(cmddlp);
              DataTable dttlp = new DataTable();
              daalp.Fill(dttlp);
              string LColp = dttlp.Rows[0][0].ToString();
              LColp = (Convert.ToInt32(LColp) + 1).ToString();
              SqlCommand CmdlP = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
              CmdlP.CommandType = CommandType.StoredProcedure;
              CmdlP.Parameters.AddWithValue("@COLUMN08", OutParam);
              CmdlP.Parameters.AddWithValue("@COLUMN02", LColp);
              CmdlP.Parameters.AddWithValue("@COLUMN03", id);
              CmdlP.Parameters.AddWithValue("@COLUMN04", totamt);
              CmdlP.Parameters.AddWithValue("@COLUMN05", totamta);
              CmdlP.Parameters.AddWithValue("@COLUMN06", payamt);
              CmdlP.Parameters.AddWithValue("@COLUMN14", "0");
              CmdlP.Parameters.AddWithValue("@COLUMN13", "0");
              CmdlP.Parameters.AddWithValue("@COLUMN09", "0");
              CmdlP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
              CmdlP.Parameters.AddWithValue("@COLUMNA03", AOwner);
              CmdlP.Parameters.AddWithValue("@COLUMNA06", Date);
              CmdlP.Parameters.AddWithValue("@COLUMNA07", Date);
              CmdlP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
              CmdlP.Parameters.AddWithValue("@COLUMNA12", 1);
              CmdlP.Parameters.AddWithValue("@COLUMNA13", 0);
              CmdlP.Parameters.AddWithValue("@Direction", insert);
              CmdlP.Parameters.AddWithValue("@TabelName", "SATABLE012");
              CmdlP.Parameters.AddWithValue("@ReturnValue", "");
              lP = CmdlP.ExecuteNonQuery();

              cn.Close();
              //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
              var idi = "";
              var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
              if (pm > 0)
              {
                  //    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 1).FirstOrDefault();
                  var msg = string.Empty;
                  //    if (msgMaster != null)
                  //    {
                  //        msg = msgMaster.COLUMN03;
                  //    }
                  //    else
                  msg = "Sales Receipt Successfully Created ";
                  Session["MessageFrom"] = msg;
                  Session["SuccessMessageFrom"] = "Success";
              }
              //SqlCommand cmd = new SqlCommand("update SATABLE012  set column05 =" + totamt + " ,COLUMN06= " + totamta + " FROM SATABLE009 h inner join SATABLE010 l  on l.COLUMN15=h.COLUMN01    left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 where  h.columnA03 = "+Convert.ToInt32(Session["AcOwner"])+" and  h.column02=" + id + "", cn);
              //cn.Open();
              //cmd.ExecuteNonQuery();
              //cn.Close();

              return Json(new { Data = 1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult PointOfSalesView()
        {
            try
            {
                Session["ide"] = Request["ide"];
                //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                // KK Issues By Venkat
                SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(l.COLUMN02) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN10) [Qty], h.COLUMN22 [SubTotal], h.COLUMN25 Discount, h.COLUMN24 Tax, (isnull(h.COLUMN20,0)) Total, (isnull(p.COLUMN06,0)) Payment, (isnull(h.COLUMN20,0)- (isnull(p.COLUMN06,0))) Dueamt ,s11.COLUMN08 PayAcc,s11.COLUMN09 cardNo ,s11.COLUMN10 PayType,h.COLUMN49 Attendedby,h.COLUMN08 CreatedDate,h.COLUMN12 Memo,h.COLUMN04 TransNo,h.COLUMN46 Amtin, h.COLUMN56 HeaderDiscount,isnull(h.COLUMN68,0) roundoffadjamt FROM SATABLE009 h inner join SATABLE010 l on l.COLUMN15=h.COLUMN01  and isnull(l.COLUMNA13,0)=0  " +
                " left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 and isnull(p.COLUMNA13,0)=0 left join SATABLE011 s11 on s11.column01= p.COLUMN08 and s11.column07= " + @Request["ide"] + " where h.column02=" + @Request["ide"] + " group by h.COLUMN05,h.COLUMN22,h.COLUMN25,h.COLUMN24,h.COLUMN20,c.COLUMN05,p.COLUMN06,s11.COLUMN09,s11.COLUMN08,s11.COLUMN10,h.COLUMN49,h.COLUMN08 ,h.COLUMN12,h.COLUMN04,h.COLUMN46,h.COLUMN56,h.COLUMN68", cn);
                DataTable dtv = new DataTable();
                cmdv.Fill(dtv);
                System.Collections.IEnumerable rows = dtv.Rows;
                ViewBag.pitemsdata = rows;

                SqlDataAdapter cmdCnt = new SqlDataAdapter("select count(*) [No of Items] from SATABLE011 where column07= " + @Request["ide"] + " and isnull(COLUMNA13,0)=0 ", cn);
                DataTable dtCnt = new DataTable();
                cmdCnt.Fill(dtCnt);
                int i = Convert.ToInt32(dtCnt.Rows[0]["No of Items"].ToString());
                

                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());
                List<SelectListItem> Item = new List<SelectListItem>();
                //EMPHCS1849 rajasekhar reddy patakota 02/01/2017 Sales Receipt Invertory checking for Track QOH=0 items 
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' AND COLUMN05='ITTY001' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
				//EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                List<SelectListItem> Attendedby = new List<SelectListItem>();
                SqlDataAdapter cmda = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                DataTable dta = new DataTable();
                cmda.Fill(dta);
                for (int dd = 0; dd < dta.Rows.Count; dd++)
                {
                    Attendedby.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: dtv.Rows[0]["Attendedby"].ToString());
                List<POPrint> all1 = new List<POPrint>();
            all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
            {
                rNoofitems = "",
                rTotalItems = "",
                rCustomerno = "",
                rCustomer = "",
                rSubTotal = "",
                rDiscount = "",
                rTax = "",
                rTotal = "",
                rPayment = "",
                rDueamt = "",
                rcardNo = "",
                rPayAcc = "",
                rPayType = "",
				//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                //KK Issues By Venkat
				Date = "",
                Memo = "",
                TransNo = "",
                amttype = "",
                DiscountAmount = ""
            });
            all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                all1[0].rTotalItems = dtv.Rows[0]["Qty"].ToString();
                all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                all1[0].rSubTotal = dtv.Rows[0]["SubTotal"].ToString();
                all1[0].rDiscount = dtv.Rows[0]["Discount"].ToString();
                all1[0].rTax = dtv.Rows[0]["Tax"].ToString();
                all1[0].rTotal = dtv.Rows[0]["Total"].ToString();
                all1[0].rPayment = dtv.Rows[0]["Payment"].ToString();
                all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                all1[0].rcardNo = dtv.Rows[0]["cardNo"].ToString();
                all1[0].rPayAcc = dtv.Rows[0]["PayAcc"].ToString();
                all1[0].rPayType = dtv.Rows[0]["PayType"].ToString();
				//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                all1[0].Date = dtv.Rows[0]["CreatedDate"].ToString();
				//KK Issues By Venkat
                all1[0].Memo = dtv.Rows[0]["Memo"].ToString();
                all1[0].TransNo = dtv.Rows[0]["TransNo"].ToString();
                all1[0].amttype = dtv.Rows[0]["Amtin"].ToString();
                all1[0].DiscountAmount = dtv.Rows[0]["HeaderDiscount"].ToString();
                all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                if (i >= 1)
                {
                    SqlDataAdapter cmdc = new SqlDataAdapter(" select isnull(max(column04),0), isnull(sum(column06),0) SumPaid,( isnull(max(column04),0) - isnull(sum(column06),0) ) Dueamt from SATABLE012 where column03= " + @Request["ide"] + "  and isnull(COLUMNA13,0)=0 ", cn);
                    DataTable dtc = new DataTable();
                    cmdc.Fill(dtc);

                    all1[0].rDueamt = (Convert.ToDecimal(dtv.Rows[0]["Total"].ToString()) - Convert.ToDecimal(dtc.Rows[0]["SumPaid"].ToString())).ToString(); 
                }
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                string lot = "N";
                SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN31'  and column04=110010828 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                DataTable dtlt = new DataTable();
                dalt.Fill(dtlt);
                if (dtlt.Rows.Count > 0) lot = dtlt.Rows[0][0].ToString();
                ViewBag.Islot = lot;
                List<SelectListItem> Amtin = new List<SelectListItem>();
                SqlDataAdapter cmdai = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtai = new DataTable();
                cmdai.Fill(dtai);
                for (int dd = 0; dd < dtai.Rows.Count; dd++)
                {
                    Amtin.Add(new SelectListItem { Value = dtai.Rows[dd]["COLUMN02"].ToString(), Text = dtai.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: all1[0].amttype);
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        
        
        public ActionResult PointOfSalesEdit()
        {
            Session["FormName"] = "Service Receipt";
            Session["ide"] = Request["ide"];
            try
            {
                 List<int> permission = Session["Permission"] as List<int>;
                 if (permission.Contains(22933))
                 {
				 	//EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                     //KK Issues By Venkat
                     SqlDataAdapter cmdv = new SqlDataAdapter("SELECT count(l.COLUMN02) [No of Items], h.COLUMN05 [Customerno], c.COLUMN05 [Customer],sum(l.COLUMN10) [Qty], h.COLUMN22 [SubTotal], h.COLUMN25 Discount, h.COLUMN24 Tax, h.COLUMN38 Tds,  h.COLUMN45 Cess,(isnull(h.COLUMN20,0)) Total, (isnull(p.COLUMN06,0)) Payment, (isnull(h.COLUMN20,0)- (isnull(p.COLUMN06,0))) Dueamt,ph.COLUMN08 Account,ph.COLUMN09 cardNo ,ph.COLUMN10 PayType,h.COLUMN49 Attendedby,h.COLUMN08 CreatedDate,h.COLUMN12 Memo,h.COLUMN04 TransNo,h.COLUMN46 Amtin, h.COLUMN56 HeaderDiscount,isnull(h.COLUMN68,0) roundoffadjamt  FROM SATABLE009 h inner join SATABLE010 l on l.COLUMN15=h.COLUMN01   " +
                      " left JOIN SATABLE002 c  ON c.COLUMN02 = h.COLUMN05 left JOIN SATABLE012 p  ON p.COLUMN03 =h.COLUMN02 and isnull(p.COLUMNA13,0)=0 left JOIN SATABLE011 ph  ON ph.COLUMN01 =p.COLUMN08 and isnull(p.COLUMNA13,0)=0 where h.column02=" + @Request["ide"] + " and isnull(l.COLUMNA13,0)=0 group by h.COLUMN05,h.COLUMN22,h.COLUMN25,h.COLUMN24,h.COLUMN20,h.COLUMN38,h.COLUMN45,c.COLUMN05,p.COLUMN06,ph.COLUMN08 ,ph.COLUMN09,ph.COLUMN10,h.COLUMN49,h.COLUMN08,h.COLUMN12,h.COLUMN04,h.COLUMN46,h.COLUMN56, h.COLUMN68 ", cn);
                     DataTable dtv = new DataTable();
                     cmdv.Fill(dtv);
                     System.Collections.IEnumerable rows = dtv.Rows;
                     ViewBag.pitemsdata = rows;

                     SqlDataAdapter cmdCnt = new SqlDataAdapter("select count(*) [No of Items] from SATABLE011 where column07= " + @Request["ide"] + " and isnull(COLUMNA13,0)=0 ", cn);
                     DataTable dtCnt = new DataTable();
                     cmdCnt.Fill(dtCnt);
                     int i = Convert.ToInt32(dtCnt.Rows[0]["No of Items"].ToString());

                     List<SelectListItem> Customer = new List<SelectListItem>();
                     SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                     DataTable dtdata = new DataTable();
                     cmddl.Fill(dtdata);
                     for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                     {
                         Customer.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: dtv.Rows[0]["Customerno"].ToString());
                     List<SelectListItem> Item = new List<SelectListItem>();
                     //EMPHCS1849 rajasekhar reddy patakota 02/01/2017 Sales Receipt Invertory checking for Track QOH=0 items 
                     SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04 !='' AND COLUMN05='ITTY001' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False' and  isnull(COLUMNA13,'False')='False'", cn);
                     DataTable dti = new DataTable();
                     cmdi.Fill(dti);
                     for (int dd = 0; dd < dti.Rows.Count; dd++)
                     {
                         Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["Item"] = new SelectList(Item, "Value", "Text");

                     //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                     List<SelectListItem> Attendedby = new List<SelectListItem>();
                     SqlDataAdapter cmda = new SqlDataAdapter("SELECT COLUMN02,RTRIM(LTRIM(COLUMN09)) COLUMN04 from MATABLE010 where (COLUMN30='True' or COLUMN30=1) and COLUMN09 !='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and  isnull(COLUMNA13,'False')='False'  order by COLUMN09", cn);
                     DataTable dta = new DataTable();
                     cmda.Fill(dta);
                     var salesrep = "";
                     for (int dd = 0; dd < dta.Rows.Count; dd++)
                     {
                         if (Convert.ToString(Session["eid"]) == dta.Rows[dd]["COLUMN02"].ToString())
                             salesrep = dta.Rows[dd]["COLUMN02"].ToString();
                         Attendedby.Add(new SelectListItem { Value = dta.Rows[dd]["COLUMN02"].ToString(), Text = dta.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["Attendedby"] = new SelectList(Attendedby, "Value", "Text", selectedValue: dtv.Rows[0]["Attendedby"].ToString());

                     List<SelectListItem> UOM = new List<SelectListItem>();
                     SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                     DataTable dtdataU = new DataTable();
                     cmddlU.Fill(dtdataU);
                     for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                     {
                         UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["UOM"] = new SelectList(UOM, "Value", "Text");

                     List<POPrint> all1 = new List<POPrint>();
                     all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                     {
                         rNoofitems = "",
                         rTotalItems = "",
                         rCustomerno = "",
                         rCustomer = "",
                         rSubTotal = "",
                         rDiscount = "",
                         rTax = "",
                         rTotal = "",
                         rPayment = "",
                         rDueamt = "",
                         rcardNo = "",
                         rPayAcc = "",
                         rPayType = "",
                         Attendedby = "",
                         rPamt = "",
						 //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                         //KK Issues By Venkat
						 Date = "",
                         Memo="",
                         TransNo="",
                         amttype = "",
                         DiscountAmount = ""
                     });
                     all1[0].rNoofitems = dtv.Rows[0]["No of Items"].ToString();
                     all1[0].rTotalItems = dtv.Rows[0]["Qty"].ToString();
                     all1[0].rCustomerno = dtv.Rows[0]["Customerno"].ToString();
                     all1[0].rCustomer = dtv.Rows[0]["Customer"].ToString();
                     all1[0].rSubTotal = dtv.Rows[0]["SubTotal"].ToString();
                     all1[0].rDiscount = dtv.Rows[0]["Discount"].ToString();
                     all1[0].rTax = dtv.Rows[0]["Tax"].ToString();
                     all1[0].rTotal = dtv.Rows[0]["Total"].ToString();

                     string payamt = dtv.Rows[0]["Payment"].ToString();
                     if (payamt == "" || payamt == "null") payamt = "0";
                     string dueamt = dtv.Rows[0]["Dueamt"].ToString();
                     if (dueamt == "" || dueamt == "null") dueamt = "0";
                     all1[0].rPamt = dtv.Rows[0]["Payment"].ToString();
                     all1[0].rDueamt = dtv.Rows[0]["Dueamt"].ToString();
                     all1[0].rPayment = (Convert.ToDecimal(payamt) + Convert.ToDecimal(dueamt)).ToString();
                     all1[0].rAccount = dtv.Rows[0]["Account"].ToString();
                     all1[0].rcardNo = dtv.Rows[0]["cardNo"].ToString();
                     all1[0].Attendedby = dtv.Rows[0]["Attendedby"].ToString();
					 //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                     all1[0].Date = dtv.Rows[0]["CreatedDate"].ToString();
                    //KK Issues By Venkat
					all1[0].Memo = dtv.Rows[0]["Memo"].ToString();
                     all1[0].TransNo = dtv.Rows[0]["TransNo"].ToString();
                     all1[0].amttype = dtv.Rows[0]["Amtin"].ToString();
                     all1[0].DiscountAmount = dtv.Rows[0]["HeaderDiscount"].ToString();
                     all1[0].RoundOff = dtv.Rows[0]["roundoffadjamt"].ToString();
                     if (i >= 1)
                     {
                         SqlDataAdapter cmdc = new SqlDataAdapter(" select isnull(max(column04),0), isnull(sum(column06),0) SumPaid,( isnull(max(column04),0) - isnull(sum(column06),0) ) Dueamt from SATABLE012 where column03= " + @Request["ide"] + "  and isnull(COLUMNA13,0)=0 ", cn);
                         DataTable dtc = new DataTable();
                         cmdc.Fill(dtc);

                         all1[0].rPamt = dtc.Rows[0]["SumPaid"].ToString();
                         all1[0].rDueamt = (Convert.ToDecimal(dtv.Rows[0]["Total"].ToString()) - Convert.ToDecimal(dtc.Rows[0]["SumPaid"].ToString())).ToString(); 
                     }
                     string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                     SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                     DataTable dtf = new DataTable();
                     daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                     if (dtf.Rows.Count > 0)
                     {
                         DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                         JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                         Session["DateFormat"] = DateFormat;
                         ViewBag.DateFormat = DateFormat;
                     }
                     if (JQDateFormat != "")
                     {
                         Session["DateFormat"] = DateFormat;
                         Session["ReportDate"] = JQDateFormat;
                     }
                     else
                     {
                         Session["DateFormat"] = "dd/MM/yyyy";
                         Session["ReportDate"] = "dd/mm/yy";
                         ViewBag.DateFormat = "dd/MM/yyyy";
                     }
                     SqlDataAdapter cmdS = new SqlDataAdapter("SELECT IIF(EXISTS(SELECT COLUMN04 FROM CONTABLE031 WHERE  COLUMNA03='" + Session["ACOWNER"] + "'  AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND COLUMN03='ALLOW PAYMENT'),(SELECT COLUMN04 FROM CONTABLE031 WHERE  COLUMNA03='" + Session["ACOWNER"] + "'  AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND COLUMN03='ALLOW PAYMENT'),0) COLUMN01, " +
                     "IIF(EXISTS(SELECT MAX(COLUMN02) FROM FITABLE001 WHERE  COLUMNA03='" + Session["ACOWNER"] + "'  AND (" + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL) AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND ISNULL(COLUMN14,0)=1  AND (COLUMN07 in(22266,22409)) AND ISNULL(COLUMN12,'FALSE')='FALSE'),(SELECT MAX(COLUMN02) FROM FITABLE001 WHERE  COLUMNA03='" + Session["ACOWNER"] + "' AND (" + Session["OPUnitWithNull"] + " OR COLUMNA02 IS NULL)  AND  ISNULL(COLUMNA13,'FALSE')='FALSE' AND ISNULL(COLUMN14,0)=1  AND (COLUMN07 in(22266,22409)) AND ISNULL(COLUMN12,'FALSE')='FALSE'),0) COLUMN02", cn);
                     DataTable dtaS = new DataTable();
                     cmdS.Fill(dtaS);
                     if (dtaS.Rows.Count > 0)
                     {
                         Session["PayRule"] = dtaS.Rows[0][0];
                         Session["PayRuleAC"] = dtaS.Rows[0][1];
                     }
                     else
                     {
                         Session["PayRule"] = 0;
                         Session["PayRuleAC"] = 0;
                     }

                     string lot = "N";
                     SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN31'  and column04=110010828 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                     SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                     DataTable dtlt = new DataTable();
                     dalt.Fill(dtlt);
                     if (dtlt.Rows.Count > 0) lot = dtlt.Rows[0][0].ToString();
                     ViewBag.Islot = lot;
					 //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                     string location = "N";
                     SqlCommand cmdloc = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN38'  and column04=110010828 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                     SqlDataAdapter daloc = new SqlDataAdapter(cmdloc);
                     DataTable dtloc = new DataTable();
                     daloc.Fill(dtloc);
                     if (dtloc.Rows.Count > 0) location = dtloc.Rows[0][0].ToString();
                     ViewBag.Islocation = location;
                     List<SelectListItem> Amtin = new List<SelectListItem>();
                     SqlDataAdapter cmdai = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11159 AND (COLUMNA03='" + Session["AcOwner"] + "' or isnull(COLUMNA03,0)=0) and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                     DataTable dtai = new DataTable();
                     cmdai.Fill(dtai);
                     for (int dd = 0; dd < dtai.Rows.Count; dd++)
                     {
                         Amtin.Add(new SelectListItem { Value = dtai.Rows[dd]["COLUMN02"].ToString(), Text = dtai.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["Amtin"] = new SelectList(Amtin, "Value", "Text", selectedValue: all1[0].amttype);
                     return View(all1);
                 }
                 else
                 {
                     Session["MessageFrom"] = "Edit Can't be Permitted ";
                     Session["SuccessMessageFrom"] = "fail";
                     return RedirectToAction("Info", "PointOfSalesInfo", new { FormName = Session["FormName"] });
                 }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult PointOfSalesDelete()
        {
            try
            {
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete", TransNo = ""; int l = 0;
                var idi = ""; string HCol2 = "";
                var custform = Request.UrlReferrer.Query.ToString();
                if (Request["ide"] != "" && Request["ide"] != null) { HCol2 = Request["ide"]; }
                else
                {
                    custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                }
                SqlCommand cmdd = new SqlCommand("select  COLUMNA02,COLUMN01 from SATABLE009 where COLUMN02=" + HCol2 + "", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string OPunit = dtt.Rows[0][0].ToString();
                string HCol1 = dtt.Rows[0][1].ToString();
                string Date = DateTime.Now.ToString();
                SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OPunit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                Cmd.Parameters.AddWithValue("@ReturnValue", "");
                cn.Open();
                int r = Cmd.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("select COLUMN02 from SATABLE011 where COLUMN07=" + HCol2 + "", cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                int c = 0;
                for (int p = 0; p < dt1.Rows.Count; p++)
                {
                    int HColc = Convert.ToInt32(dt1.Rows[p][0].ToString());
                    if (HColc > 0)
                    {
                        SqlCommand Cmdc = new SqlCommand("usp_SAL_BL_PAYMENT", cn);
                        Cmdc.CommandType = CommandType.StoredProcedure;
                        Cmdc.Parameters.AddWithValue("@COLUMN02", HColc);
                        Cmdc.Parameters.AddWithValue("@COLUMNA02", OPunit);
                        Cmdc.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdc.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdc.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdc.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdc.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdc.Parameters.AddWithValue("@COLUMNA13", 1);
                        Cmdc.Parameters.AddWithValue("@Direction", insert);
                        Cmdc.Parameters.AddWithValue("@TabelName", "SATABLE011");
                        Cmdc.Parameters.AddWithValue("@ReturnValue", "");
                        c = Cmdc.ExecuteNonQuery();
                    }
                }
                if (c > 0)
                {
                    //    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    //    if (msgMaster != null)
                    //    {
                    //        msg = msgMaster.COLUMN03;
                    //    }
                    //    else
                    msg = "Sales Receipt Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                cn.Close();
                return RedirectToAction("PointOfSalesInfo", "PointOfSales", new { idi = 3100, FormName = Session["FormName"] });
                return Json(new { Data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) {  custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult DiscountTypes(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> DiscountType = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where COLUMN03=11158 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    DiscountType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["DiscountType"] = new SelectList(DiscountType, "Value", "Text");
                return PartialView("DiscountTypes");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult PriceLevelTypes(string ItemID,string OPUnit)
        {
            try
            {
                PriceLevelInfo(ItemID, OPUnit);
                return PartialView("PriceLevelTypes");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public string PriceLevelInfo(string ItemID,string OPUnit)
        {
            try
            {
                var Defaultpl = "";
                List<SelectListItem> PriceLevel = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04,isnull(COLUMN13,0)COLUMN13 from MATABLE023  where (" + Session["OPUnitWithNull"] + " or isnull(COLUMNA02,0)=0)  and (COLUMN07='Item' or isnull(COLUMN07,'')='')  AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False'", cn);
                SqlCommand cmddl = new SqlCommand("ItemWisePriceLevelDetails", cn);
                cmddl.CommandType = CommandType.StoredProcedure;
                cmddl.Parameters.AddWithValue("@Item", ItemID);
                cmddl.Parameters.AddWithValue("@OPUNIT", OPUnit);
                cmddl.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                SqlDataAdapter da = new SqlDataAdapter(cmddl);
                DataTable dtdata = new DataTable();
                da.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    if (dtdata.Rows[dd]["COLUMN13"].ToString() == "1" || dtdata.Rows[dd]["COLUMN13"].ToString() == "True") Defaultpl = dtdata.Rows[dd]["COLUMN02"].ToString();
                    PriceLevel.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["PriceLevel"] = new SelectList(PriceLevel, "Value", "Text", selectedValue: Defaultpl);
                return Defaultpl;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return "";
            }
        }

        public ActionResult SalesReceiptPriceLevelDetails(string Pricelevel, string Price, string OU, string Qty, string Date)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("InvoiceHeaderPriceLevelDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Type", "Line");
                cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@Pricelevel", Pricelevel);
                cmd.Parameters.AddWithValue("@Qty", Qty);
                cmd.Parameters.AddWithValue("@Price", Price);
                if (Date == "") Date = null;
                else { Date = DateTime.ParseExact(Date, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }                
                cmd.Parameters.AddWithValue("@Date", Date);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string Amount = "", pricetype = "";
                if (dt.Rows.Count > 0)
                {
                    Amount = dt.Rows[0][0].ToString();
                    pricetype = dt.Rows[0][1].ToString();
                }
                else
                    Amount = Price;
                return Json(new { item = Amount, item1 = pricetype }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public class BatchItemDetails
        {
            public string Value { get; set; }
            public string Text { get; set; }
            public string ExDate { get; set; }
        }
        public ActionResult ItemWiseLotInfo(string FormName, string ItemId, string itemBatchId, string UnitId)
        {
            try
            {
                List<BatchItemDetails> Lot = new List<BatchItemDetails>();
                //if (ItemId != "" && ItemId != null) ItemId = " COLUMN09=" + ItemId + " and ";
                //ItemId += " isnull(COLUMN15,0)=" + UnitId + " and ";
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04,COLUMN05  from FITABLE043 Where " + ItemId + " (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False' order by convert(datetime, COLUMN05, 101)", cn);
                SqlCommand cmdl = new SqlCommand("usp_PROC_TP_POSLotDetails", cn);
                cmdl.CommandType = CommandType.StoredProcedure;
                cmdl.Parameters.AddWithValue("@ItemID", ItemId);
                cmdl.Parameters.AddWithValue("@UOM", UnitId);
                cmdl.Parameters.AddWithValue("@Lot", itemBatchId);
                cmdl.Parameters.AddWithValue("@DateF", Session["DateFormat"]);
                cmdl.Parameters.AddWithValue("@OPUnit", Session["OPUnit1"]);
                cmdl.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtdata = new DataTable();
                dal.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Lot.Add(new BatchItemDetails { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString(), ExDate = dtdata.Rows[dd]["COLUMN05"].ToString() });
                }
                ViewData["Lot"] = new SelectList(Lot, "Value", "Text", itemBatchId);
                ViewBag.Dateformat = Convert.ToString(Session["DateFormat"]);
                return PartialView("ItemWiseLotInfo");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetExpDate(string Lotid, string ItemId, string UnitId, string LocationId)
        {
            try
            {
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04,format(COLUMN05,'" + Session["DateFormat"] + "')COLUMN05  from FITABLE043 Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "'  or COLUMNA03 is null)  and isnull(COLUMNA13,'False')='False' and COLUMN02=" + Lotid + " order by cast(COLUMN05 as date)", cn);
                SqlCommand cmdl = new SqlCommand("usp_PROC_TP_POSLotDetails", cn);
                cmdl.CommandType = CommandType.StoredProcedure;
                cmdl.Parameters.AddWithValue("@ItemId", ItemId);
                cmdl.Parameters.AddWithValue("@UOM", UnitId);
                cmdl.Parameters.AddWithValue("@Lot", Lotid);
                cmdl.Parameters.AddWithValue("@DateF", Session["DateFormat"]);
                cmdl.Parameters.AddWithValue("@Type", "LOT");
                cmdl.Parameters.AddWithValue("@OPUnit", Session["OPUnit1"]);
                cmdl.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                DataTable dtdata = new DataTable();
                dal.Fill(dtdata);
                var ExpDate = DateTime.Now.ToShortDateString();
                if (dtdata.Rows.Count > 0)
                {
                    ExpDate = dtdata.Rows[0]["COLUMN05"].ToString();
                    Lotid = dtdata.Rows[0]["COLUMN02"].ToString();
                } 
                string PPrice = "0", SPrice = "0", AvailQty = "0";
                if (ItemId != "" && ItemId != null && ItemId != "undefined")
                {
                    SqlCommand cmd = new SqlCommand("usp_PROC_TP_LotWisePrice", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ItemID", ItemId));
                    cmd.Parameters.Add(new SqlParameter("@UOM", UnitId));
                    cmd.Parameters.Add(new SqlParameter("@Lot", Lotid));
                    cmd.Parameters.Add(new SqlParameter("@location", LocationId));
                    cmd.Parameters.Add(new SqlParameter("@FrmID", 1277));
                    cmd.Parameters.AddWithValue("@DateF", Session["DateFormat"]);
                    cmd.Parameters.Add(new SqlParameter("@OPUnit", Session["OPUnit1"]));
                    cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    if (dt1.Rows.Count > 0) { PPrice = dt1.Rows[0]["PPrice"].ToString(); SPrice = dt1.Rows[0]["SPrice"].ToString(); AvailQty = dt1.Rows[0]["AvailQty"].ToString(); }
                }
                return Json(new { ExpDate = ExpDate, LotId = Lotid, PPrice = PPrice, SPrice = SPrice, AvailQty = AvailQty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetLotDetails(string ItemId, string Lotid, string UnitId, string LocationId)
        {
            try
            {
                var ExpDate = DateTime.Now.ToShortDateString();
                string PPrice = "0", SPrice = "0", AvailQty = "0";
                if (ItemId != "" && ItemId != null && ItemId != "undefined")
                {
                    SqlCommand cmd = new SqlCommand("usp_PROC_TP_LotWisePrice", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ItemID", ItemId));
                    cmd.Parameters.Add(new SqlParameter("@UOM", UnitId));
                    cmd.Parameters.Add(new SqlParameter("@Lot", Lotid));
                    cmd.Parameters.Add(new SqlParameter("@location", LocationId));
                    cmd.Parameters.Add(new SqlParameter("@FrmID", 1277));
                    cmd.Parameters.AddWithValue("@DateF", Session["DateFormat"]);
                    cmd.Parameters.Add(new SqlParameter("@OPUnit", Session["OPUnit1"]));
                    cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    if (dt1.Rows.Count > 0) { PPrice = dt1.Rows[0]["PPrice"].ToString(); SPrice = dt1.Rows[0]["SPrice"].ToString(); AvailQty = dt1.Rows[0]["AvailQty"].ToString(); ExpDate = dt1.Rows[0]["ExpDate"].ToString(); }
                }
                return Json(new { ExpDate = ExpDate, LotId = Lotid, PPrice = PPrice, SPrice = SPrice, AvailQty = AvailQty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
		//EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
        public ActionResult GetLotWiseItemDetails(string ItemID, string LotId, string Locationid, string UomId, string Type)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("POSItemdetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                if (Type == null || Convert.ToString(Type) == "")
                    cmd.Parameters.AddWithValue("@Type", "POS");
                cmd.Parameters.AddWithValue("@uom", UomId);
                cmd.Parameters.AddWithValue("@LotId", LotId);
                cmd.Parameters.AddWithValue("@Locationid", Locationid);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                var avl = 0;
                var Price = 0;
                if (dt.Rows.Count > 0)
                {
                    avl = Convert.ToInt32(dt.Rows[0]["AVL"]);
                    Price = Convert.ToInt32(dt.Rows[0]["Price"]);
                }
                return Json(new { Avail = avl, Price = Price }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
		//EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
        public ActionResult LocationDetails(string FormName, string ItemId, string itemBatchId, string LocationId)
        {
            try
            {
                List<BatchItemDetails> Location = new List<BatchItemDetails>();
                if (ItemId != "" && ItemId != null) ItemId = " and  f.COLUMN03=" + ItemId + " ";
                else ItemId = "";
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlDataAdapter cmddl = new SqlDataAdapter("select l.COLUMN02,l.COLUMN04 from CONTABLE030 l left join fiTABLE010 f on f.COLUMN21=l.COLUMN02 and f.COLUMNA03=l.COLUMNA03 " + ItemId + " Where  (l.COLUMNA02 in(" + OPUnit + ") or isnull(l.COLUMNA02,0)=0)   AND (l.COLUMNA03='" + Session["AcOwner"] + "'  or l.COLUMNA03 is null)  and isnull(l.COLUMNA13,'False')='False'  and isnull(l.COLUMN07,'False')='False' group by l.COLUMN02,l.COLUMN04 order by sum(isnull(f.COLUMN04,0)) desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Location.Add(new BatchItemDetails { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Location"] = new SelectList(Location, "Value", "Text", LocationId);
                return PartialView("LocationDetails");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult GETGSTINBasedTaxes(string ItemID, string opunit, string customer, string Type, string Price, string Mode, string Date)
        {
            try
            {
                List<SelectListItem> Taxes = new List<SelectListItem>();
                Taxes = taxlst.TaxList(ItemID, opunit, customer, Type, Price, Mode, Date); var TaxID = "";
                if(Mode!= "Tax" && Taxes.Count>0)TaxID=Taxes.FirstOrDefault().Value;
                return Json(new { item = Taxes, TaxID = TaxID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult TaxInformation(string ItemId, string Customer, string FormName, string Qty, string Price, string Date)
        {
            try
            {
                List<SelectListItem> TaxType = new List<SelectListItem>();
                //SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE013	where (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMN15,0)=0 and isnull(COLUMNA13,0)=0	union all	select '-'+COLUMN02,'['+COLUMN04+']' from matable014 	where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null) AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0", cn);
                //DataTable dtdata = new DataTable();
                //cmddl.Fill(dtdata);
                //for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                //{
                //    TaxType.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                //}
                TaxType = taxlst.TaxList(ItemId, "", Customer, "Sales", Price, "Tax", Date);
                ViewData["TaxType"] = new SelectList(TaxType, "Value", "Text");
                return PartialView("TaxInformation");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetTaxAmount(string TaxType, string TaxCal, string FormName, int grp = -1)
        {
            try
            {
                if (Convert.ToInt32(TaxType) < 0) { grp = 0; TaxType = TaxType.Replace("-", ""); }
                string fid = "1275";
                SqlCommand cmd = new SqlCommand("GetTaxAmount", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TaxType", TaxType));
                cmd.Parameters.Add(new SqlParameter("@FormID", fid));
                cmd.Parameters.Add(new SqlParameter("@Group", grp));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1); var Agency = "";
                var Tax = "0"; var TTax = 0.00;
                List<SelectListItem> grptaxes = new List<SelectListItem>();
                if (dt1.Rows.Count > 0)
                {
                    if (grp == 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            grptaxes.Add(new SelectListItem { Value = dt1.Rows[i]["COLUMN07"].ToString() });
                            Tax = dt1.Rows[0]["COLUMN07"].ToString();
                            if (Tax == "" || Tax == null) Tax = "0";
                            TTax = TTax + Convert.ToDouble(Tax);
                        } Tax = Convert.ToString(TTax);
                    }
                    else
                        Tax = dt1.Rows[0]["COLUMN07"].ToString();
                    if (Tax == "" || Tax == null) Tax = "0";
                    Agency = dt1.Rows[0]["COLUMN16"].ToString();
                }
                return Json(new { Tax = Tax, Agency = Agency, grptaxes = grptaxes }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult PaymentInformation(string FormName, string Qty, string Price)
        {
            try
            {
                List<SelectListItem> Account = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformation");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult PaymentInformationNewCash(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {
                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
				//EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and isnull(COLUMN14,0)=1 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationNewCash");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public ActionResult PaymentInformationEdit(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {
                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
				//EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and isnull(COLUMN14,0)=1 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationEdit");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
       
        public ActionResult PaymentInformationEditSave(string FormName, string Qty, string Price, string TypeChk)
        {
            try
            {
                Session["TypeChk"] = TypeChk;
                List<SelectListItem> Account = new List<SelectListItem>();
                //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from FITABLE001  where (COLUMN07 in(22266,22409)) and isnull(COLUMN14,0)=1 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN12,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Account.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                ViewData["Account"] = new SelectList(Account, "Value", "Text");
                return PartialView("PaymentInformationEditSave");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
       
        public ActionResult getBalanceAmount(string Account)
        {
            try
            {
                string bal = "0";
                SqlCommand cmd = new SqlCommand("Select isnull(column10,0) from fitable001 where column02=" + Account + " AND    (" + Session["OPUnitWithNull"] + " or columna02 is null)   AND  COLUMNA03='" + Session["AcOwner"] + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    bal = dt1.Rows[0][0].ToString();
                }
                return Json(new { Balance = bal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        [HttpPost]
        public ActionResult POSSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["POSGridData"] = ds;
                //var saveform = Request.QueryString["FormName"];
                //var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == saveform).ToList();
                //var fname = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var frmID = fnamedata.Select(q => q.COLUMN02).FirstOrDefault();
                //var customfrmID = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                //var frmType = fnamedata.Select(q => q.COLUMN05).FirstOrDefault().ToString();
                //var MasterForm = fnamedata.Select(q => q.COLUMN06).FirstOrDefault();
                //if (frmType == "Custom")
                //{
                //    frmID = Convert.ToInt32(MasterForm);
                //    saveform = dc.CONTABLE0010.Where(q => q.COLUMN02 == MasterForm).FirstOrDefault().COLUMN04;
                //}
                //Session["FormName"] = saveform;
                //Session["id"] = fname;
                //eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
                //lg.CreateFile(Server.MapPath("~/"), saveform + "_" + Session["UserName"].ToString() + "", " \r\n" + "***********************TARGET1***********************" + " \r\n" + " \r\n" + " Line Data Received .");

                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        public string TransactionNoGenerate(string FormName, int FormId, string opunit)
        {
            try
            {
                var acID = (int?)Session["AcOwner"];
                var OPUnit = Session["OPUnit1"];
                if (opunit != "") OPUnit = opunit;
                Session[FormId + "TranOPUnit"] = OPUnit;
                var eid = (int?)Session["eid"];
                SqlCommand cmd = new SqlCommand("usp_Proc_TransNoGenerate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Formid", FormId);
                //cmd.Parameters.AddWithValue("@TblID", tblID);
                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", acID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string item = "", trancol = "", Override = "";
                if (dt.Rows.Count > 0 && dt.Columns.Contains("PO"))
                { item = dt.Rows[0]["PO"].ToString(); trancol = dt.Rows[0]["TransColumn"].ToString(); Override = dt.Rows[0]["Override"].ToString(); }
                return item;
                //return Json(new { item = item, trancol = trancol, Override = Override }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return "";
            }
        }

        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono="",TransNo="";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (frmIDchk == 1532)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "SR";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE009  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                else if (frmIDchk == 1278)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "CP";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE011  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                //pacmd.ExecuteNonQuery(); 
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }
        [HttpPost]
        //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
        //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
        //KK Issues By Venkat
        public ActionResult Saveform(string Qty, string SubTotal, string Discount, string TaxAmt, string TotAmt, string PayAmt, string PayAcc, string Customer, string Type, string CardNo, string Attendedby, string CreatedDate, string Memo, string Amtin, string HeaderDiscount, string roundoffadjamt, string TransNo)
        {
            try
            {
                int OutParam = 0;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string OrderType = "1002";
                string Date = DateTime.Now.ToString();
                //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                if (CreatedDate == "") CreatedDate = Date;
                else { CreatedDate = DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                if (CreatedDate == "") CreatedDate = null;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert";
                //cn.Open();

                //SqlCommand cmdi = new SqlCommand("select  COLUMN02 from SATABLE009 WHERE COLUMN04='" + TransNo + "' and COLUMN04!='' and (COLUMNA02=" + OpUnit + " or isnull(COLUMNA02,0)=0) and COLUMNA03=" + AOwner + " and isnull(COLUMNA13,0)=0 ", cn);
                SqlCommand cmdi = new SqlCommand("PROC_CHECK_TransactionNo", cn);
                cmdi.CommandType = CommandType.StoredProcedure;
                cmdi.Parameters.Add(new SqlParameter("@TransNo", TransNo));
                cmdi.Parameters.Add(new SqlParameter("@FormId", "1532"));
                //cmdi.Parameters.Add(new SqlParameter("@Id", "0"));
                cmdi.Parameters.Add(new SqlParameter("@AcOwner", AOwner));
                cmdi.Parameters.Add(new SqlParameter("@OPUNIT", OpUnit));
                SqlDataAdapter dai = new SqlDataAdapter(cmdi);
                DataTable dti = new DataTable();
                dai.Fill(dti);
                if (dti.Rows.Count > 0)
                    return Json(new { Data = 0, InvoiceNo = 0, idi = 0, FormName = 0, Duplicate = 1 }, JsonRequestBehavior.AllowGet);
                else
                {
                    SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE009 ", cn);
                    SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                    DataTable dtt = new DataTable();
                    daa.Fill(dtt);
                    string HCol2 = dtt.Rows[0][0].ToString();
                    HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                    int r = 0;
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                        Cmd.Parameters.AddWithValue("@COLUMN03", "1532");
                        //TransNo = GetTransactionNo(1532);
                        Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);
                        Cmd.Parameters.AddWithValue("@COLUMN05", Customer);
                        //KK Issues By Venkat
                        Cmd.Parameters.AddWithValue("@COLUMN12", Memo);
                        //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                        Cmd.Parameters.AddWithValue("@COLUMN06", "");
                        Cmd.Parameters.AddWithValue("@COLUMN19", OrderType);
                        //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                        Cmd.Parameters.AddWithValue("@COLUMN08", CreatedDate);
                        Cmd.Parameters.AddWithValue("@COLUMN10", CreatedDate);
                        Cmd.Parameters.AddWithValue("@COLUMN14", OpUnit);
                        Cmd.Parameters.AddWithValue("@COLUMN23", "1000");
                        Cmd.Parameters.AddWithValue("@COLUMN24", TaxAmt);
                        Cmd.Parameters.AddWithValue("@COLUMN22", SubTotal);
                        Cmd.Parameters.AddWithValue("@COLUMN20", TotAmt);
                        Cmd.Parameters.AddWithValue("@COLUMN25", Discount);
                        //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                        Cmd.Parameters.AddWithValue("@COLUMN49", Attendedby);
                        Cmd.Parameters.AddWithValue("@COLUMN56", HeaderDiscount);
                        Cmd.Parameters.AddWithValue("@COLUMN46", Amtin);
                        Cmd.Parameters.AddWithValue("@COLUMN68", roundoffadjamt);
                        Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmd.Parameters.AddWithValue("@Direction", insert);
                        Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                        Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cnc.Open();
                        Cmd.CommandTimeout = 0;
                        r = Cmd.ExecuteNonQuery();
                        OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                    }
                    eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                    int? Oper = Convert.ToInt32(Session["AUTO"]);
                    if (Oper == null || Oper == 0)
                        Oper = Convert.ToInt32(Session["OPUnit1"]);
                    if (Oper == 0)
                        Oper = null;
                    int? acOW = Convert.ToInt32(Session["AcOwner"]);
                    var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1532).FirstOrDefault().COLUMN04.ToString();
                    var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                    if (r > 0)
                    {
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                            if (Oper == null)
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                            if (listTM == null)
                            {
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                            }
                        }
                        if (listTM != null)
                        {
                            MYTABLE002 product = listTM;
                            if (listTM.COLUMN09 == "")
                                product.COLUMN09 = (1).ToString();
                            else
                                product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                            act1.SaveChanges();
                        }
                    }
                    //Line
                    for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                    {
                        SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE010 ", cn);
                        SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                        DataTable dttl = new DataTable();
                        daal.Fill(dttl);
                        string LCol2 = dttl.Rows[0][0].ToString();
                        LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                        string Item = itemdata.Tables[0].Rows[i]["ItemID"].ToString();

                        //Anusha UPC
                        string UPC = itemdata.Tables[0].Rows[i]["UPC"].ToString();
                        //End UPC
                        string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                        //EMPHCS1820 rajasekhar reddy patakota 22/09/2016 Adding Item Description in Sales Receipt screen
                        string SalesDesc = itemdata.Tables[0].Rows[i]["SalesDesc"].ToString();
                        string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                        string lQty = itemdata.Tables[0].Rows[i]["Qty"].ToString();
                        string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                        string Amount = itemdata.Tables[0].Rows[i]["Amount"].ToString();
                        string lDiscount = itemdata.Tables[0].Rows[i]["Discount"].ToString();
                        string TaxType = itemdata.Tables[0].Rows[i]["TaxType"].ToString();
                        string lTaxAmt = itemdata.Tables[0].Rows[i]["TaxAmt"].ToString();
                        if (Units == "0" || Units == "" || Units == null) Units = "10000";
                        string BatchId = itemdata.Tables[0].Rows[i]["BatchId"].ToString();
                        if (BatchId == "0" || BatchId == "" || BatchId == null) BatchId = "0";
                        //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                        string LocationId = itemdata.Tables[0].Rows[i]["LocationId"].ToString();
                        if (LocationId == "0" || LocationId == "" || LocationId == null) LocationId = "0";
                        string PriceLevelId = itemdata.Tables[0].Rows[i]["PriceLevelId"].ToString();
                        if (PriceLevelId == "0" || PriceLevelId == "" || PriceLevelId == null) PriceLevelId = "1000";
                        string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                        if (Rate == "0" || Rate == "" || Rate == null) Rate = Price;
                        string InclusiveTotal = itemdata.Tables[0].Rows[i]["InclusiveTotal"].ToString();
                        //if (InclusiveTotal == "0" || InclusiveTotal == "" || InclusiveTotal == null) InclusiveTotal = Amount;
                        //else Amount = InclusiveTotal;
                        string Avlqty = "0";
                        if (OpUnit != null)
                        {
                            SqlCommand cmddi = new SqlCommand("select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=" + Item + " and COLUMN19=" + Units + " AND iif(COLUMN22='',0,isnull(COLUMN22,0))=" + BatchId + " AND iif(COLUMN21='',0,isnull(COLUMN21,0))=" + LocationId + " and COLUMN13 in(" + OpUnit + ") and COLUMNA03=" + AOwner + " ", cn);
                            SqlDataAdapter daai = new SqlDataAdapter(cmddi);
                            DataTable dtti = new DataTable();
                            daai.Fill(dtti);
                            Avlqty = dtti.Rows[0][0].ToString();
                            if (Avlqty == "" || Avlqty == null) Avlqty = "0";
                        }
                        using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                        {
                            SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                            Cmdl.CommandType = CommandType.StoredProcedure;

                            Cmdl.Parameters.AddWithValue("@COLUMN15", OutParam);
                            Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                            //Anusha UPC COLUMN 
                            Cmdl.Parameters.AddWithValue("@COLUMN03", UPC);
                            //End UPC
                            Cmdl.Parameters.AddWithValue("@COLUMN05", Item);
                            //EMPHCS1820 rajasekhar reddy patakota 22/09/2016 Adding Item Description in Sales Receipt screen
                            Cmdl.Parameters.AddWithValue("@COLUMN06", SalesDesc);
                            Cmdl.Parameters.AddWithValue("@COLUMN22", Units);
                            Cmdl.Parameters.AddWithValue("@COLUMN31", BatchId);
                            Cmdl.Parameters.AddWithValue("@COLUMN10", lQty);
                            Cmdl.Parameters.AddWithValue("@COLUMN21", TaxType);
                            Cmdl.Parameters.AddWithValue("@COLUMN13", Price);
                            Cmdl.Parameters.AddWithValue("@COLUMN37", PriceLevelId);
                            Cmdl.Parameters.AddWithValue("@COLUMN35", Rate);
                            Cmdl.Parameters.AddWithValue("@COLUMN14", Amount);
                            Cmdl.Parameters.AddWithValue("@COLUMN23", lTaxAmt);
                            Cmdl.Parameters.AddWithValue("@COLUMN19", lDiscount);
                            Cmdl.Parameters.AddWithValue("@COLUMN11", Avlqty);
                            Cmdl.Parameters.AddWithValue("@COLUMN30", "22557");
                            //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                            Cmdl.Parameters.AddWithValue("@COLUMN38", LocationId);
                            Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                            Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                            Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                            Cmdl.Parameters.AddWithValue("@COLUMNB01", UPC);
                            Cmdl.Parameters.AddWithValue("@Direction", insert);
                            Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE010");
                            Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                            int l = 0;
                            cnc.Open();
                            Cmdl.CommandTimeout = 0;
                            l = Cmdl.ExecuteNonQuery();
                        }
                    }
                    //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                    Session["POSGridData"] = null;
                    int lP = 0;
                    SqlCommand cmddc = new SqlCommand("select  COLUMN02 from SATABLE009 where column01=" + OutParam + " ", cn);
                    SqlDataAdapter daac = new SqlDataAdapter(cmddc);
                    DataTable dttc = new DataTable();
                    daac.Fill(dttc);
                    //if (PayAcc != "0" && PayAmt != "0" && PayAcc != "" && PayAmt != "" && Convert.ToDecimal(PayAmt) > 0)
                    //{
                    if (dttc.Rows.Count > 0)
                    {
                        //Payment
                        SqlCommand cmdp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE011 ", cn);
                        SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                        DataTable dtp = new DataTable();
                        dap.Fill(dtp);
                        string HColp = dtp.Rows[0][0].ToString();
                        HColp = (Convert.ToInt32(HColp) + 1).ToString();
                        int pm = 0;
                        using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                        {
                            SqlCommand CmdP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                            CmdP.CommandType = CommandType.StoredProcedure;
                            CmdP.Parameters.AddWithValue("@COLUMN02", HColp);
                            CmdP.Parameters.AddWithValue("@COLUMN03", "1278");
                            TransNo = GetTransactionNo(1278);
                            CmdP.Parameters.AddWithValue("@COLUMN04", TransNo);
                            CmdP.Parameters.AddWithValue("@COLUMN05", Customer);
                            //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                            CmdP.Parameters.AddWithValue("@COLUMN06", "");
                            CmdP.Parameters.AddWithValue("@COLUMN18", OrderType);
                            //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                            CmdP.Parameters.AddWithValue("@COLUMN19", CreatedDate);
                            CmdP.Parameters.AddWithValue("@COLUMN07", HCol2);
                            CmdP.Parameters.AddWithValue("@COLUMN08", PayAcc);
                            CmdP.Parameters.AddWithValue("@COLUMN14", OpUnit);
                            CmdP.Parameters.AddWithValue("@COLUMN13", PayAmt);
                            CmdP.Parameters.AddWithValue("@COLUMN10", Type);
                            CmdP.Parameters.AddWithValue("@COLUMN09", CardNo);
                            CmdP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            CmdP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            CmdP.Parameters.AddWithValue("@COLUMNA06", Date);
                            CmdP.Parameters.AddWithValue("@COLUMNA07", Date);
                            CmdP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            CmdP.Parameters.AddWithValue("@COLUMNA12", 1);
                            CmdP.Parameters.AddWithValue("@COLUMNA13", 0);
                            CmdP.Parameters.AddWithValue("@Direction", insert);
                            CmdP.Parameters.AddWithValue("@TabelName", "SATABLE011");
                            CmdP.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                            cnc.Open();
                            CmdP.CommandTimeout = 0;
                            pm = CmdP.ExecuteNonQuery();
                            OutParam = Convert.ToInt32(CmdP.Parameters["@ReturnValue"].Value);
                        }
                        if (pm > 0)
                        {
                            custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1278).FirstOrDefault().COLUMN04.ToString();
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                            if (Oper == null)
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                            if (listTM == null)
                            {
                                listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                                if (Oper == null)
                                    listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                                if (listTM == null)
                                {
                                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                                }
                            }
                            if (listTM != null)
                            {
                                MYTABLE002 product = listTM;
                                if (listTM.COLUMN09 == "")
                                    product.COLUMN09 = (1).ToString();
                                else
                                    product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                                act1.SaveChanges();
                            }
                        }
                        //Line                
                        SqlCommand cmddlp = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE012 ", cn);
                        SqlDataAdapter daalp = new SqlDataAdapter(cmddlp);
                        DataTable dttlp = new DataTable();
                        daalp.Fill(dttlp);
                        string LColp = dttlp.Rows[0][0].ToString();
                        LColp = (Convert.ToInt32(LColp) + 1).ToString();
                        using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                        {
                            SqlCommand CmdlP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                            CmdlP.CommandType = CommandType.StoredProcedure;
                            CmdlP.Parameters.AddWithValue("@COLUMN08", OutParam);
                            CmdlP.Parameters.AddWithValue("@COLUMN02", LColp);
                            CmdlP.Parameters.AddWithValue("@COLUMN03", HCol2);
                            CmdlP.Parameters.AddWithValue("@COLUMN04", TotAmt);
                            CmdlP.Parameters.AddWithValue("@COLUMN05", TotAmt);
                            CmdlP.Parameters.AddWithValue("@COLUMN06", PayAmt);
                            CmdlP.Parameters.AddWithValue("@COLUMN14", "0");
                            CmdlP.Parameters.AddWithValue("@COLUMN13", "0");
                            CmdlP.Parameters.AddWithValue("@COLUMN09", "0");
                            CmdlP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                            CmdlP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                            CmdlP.Parameters.AddWithValue("@COLUMNA06", Date);
                            CmdlP.Parameters.AddWithValue("@COLUMNA07", Date);
                            CmdlP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                            CmdlP.Parameters.AddWithValue("@COLUMNA12", 1);
                            CmdlP.Parameters.AddWithValue("@COLUMNA13", 0);
                            CmdlP.Parameters.AddWithValue("@Direction", insert);
                            CmdlP.Parameters.AddWithValue("@TabelName", "SATABLE012");
                            CmdlP.Parameters.AddWithValue("@ReturnValue", "");
                            cnc.Open();
                            CmdlP.CommandTimeout = 0;
                            lP = CmdlP.ExecuteNonQuery();
                        }
                    }
                    //}
                    // cn.Close();
                    //EMPHCS1503 rajasekhar reddy patakota 10/01/2015 Point Of Sales Creation In Retail application
                    var idi = "";
                    var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); }
                    custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                    if (r > 0)
                    {
                        //    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1261 && q.COLUMN05 == 1).FirstOrDefault();
                        var msg = string.Empty;
                        //    if (msgMaster != null)
                        //    {
                        //        msg = msgMaster.COLUMN03;
                        //    }
                        //    else
                        msg = "Sales Receipt Successfully Created ";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    return Json(new { Data = lP, InvoiceNo = HCol2, idi = idi, FormName = custform, Duplicate = 0 }, JsonRequestBehavior.AllowGet);
                }
                //return RedirectToAction("PointOfSalesInfo", "PointOfSales");
                //return View("PointOfSalesInfo");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); }
                custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
		//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
        [HttpPost]
        //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
        //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
        //KK Issues By Venkat
        public ActionResult EditSaveform(string Qty, string SubTotal, string Discount, string TaxAmt, string TotAmt, string totamta, string PayAmt, string PayAcc, string Customer, string ide, string Type, string CardNo, string Attendedby, string CreatedDate, string Memo, string Amtin, string HeaderDiscount, string roundoffadjamt)
        {
            try
            {
                int OutParam = 0;
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["POSGridData"];
                string OrderType = "1002";
                string Date = DateTime.Now.ToString();
                //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                if (CreatedDate == "") CreatedDate = Date;
                else { CreatedDate = DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                if (CreatedDate == "") CreatedDate = null;
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update", TransNo = "", IHeaderIID = "", PHeaderIID = ""; int l = 0;
                SqlCommand cmdd = new SqlCommand("select  COLUMN01,COLUMN04,COLUMN08 from SATABLE009 where column02=" + ide + " and COLUMNA03=" + AOwner + " and isnull(COLUMNA13,0)=0", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                IHeaderIID = dtt.Rows[0][0].ToString();
                TransNo = dtt.Rows[0]["COLUMN04"].ToString();
                string TDate = Convert.ToDateTime(dtt.Rows[0]["COLUMN08"].ToString()).ToShortDateString();
                string HCol2 = ide;
                int r = 0;
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    //HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                    SqlCommand Cmd = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1532");
                    Cmd.Parameters.AddWithValue("@COLUMN04", TransNo);//chk
                    Cmd.Parameters.AddWithValue("@COLUMN05", Customer);
                    //KK Issues By Venkat
                    Cmd.Parameters.AddWithValue("@COLUMN12", Memo);
                    Cmd.Parameters.AddWithValue("@COLUMN06", "");
                    Cmd.Parameters.AddWithValue("@COLUMN19", OrderType);
                    //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                    Cmd.Parameters.AddWithValue("@COLUMN08", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN10", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN14", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN23", "1000");
                    Cmd.Parameters.AddWithValue("@COLUMN24", TaxAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN22", SubTotal);
                    Cmd.Parameters.AddWithValue("@COLUMN20", TotAmt);
                    Cmd.Parameters.AddWithValue("@COLUMN25", Discount);
                    //EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                    Cmd.Parameters.AddWithValue("@COLUMN49", Attendedby);
                    Cmd.Parameters.AddWithValue("@COLUMN46", Amtin);
                    Cmd.Parameters.AddWithValue("@COLUMN56", HeaderDiscount);
                    Cmd.Parameters.AddWithValue("@COLUMN68", roundoffadjamt);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE009");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cnc.Open();
                    Cmd.CommandTimeout = 0;
                    r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                }
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                int? Oper = Convert.ToInt32(Session["AUTO"]);
                if (Oper == null || Oper == 0)
                    Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                int? acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1532).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE010 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string rowid = itemdata.Tables[0].Rows[i]["rowid"].ToString();
                    if (rowid == "0" || rowid == "") rowid = LCol2;
                    string Item = itemdata.Tables[0].Rows[i]["ItemID"].ToString();
                    //Anusha UPC
                    string UPC = itemdata.Tables[0].Rows[i]["UPC"].ToString();
                    //End UPC
                    string ItemName = itemdata.Tables[0].Rows[i]["ItemName"].ToString();
                    //EMPHCS1820 rajasekhar reddy patakota 22/09/2016 Adding Item Description in Sales Receipt screen
                    string SalesDesc = itemdata.Tables[0].Rows[i]["SalesDesc"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string lQty = itemdata.Tables[0].Rows[i]["Qty"].ToString();
                    string Price = itemdata.Tables[0].Rows[i]["Price"].ToString();
                    string Amount = itemdata.Tables[0].Rows[i]["Amount"].ToString();
                    string lDiscount = itemdata.Tables[0].Rows[i]["Discount"].ToString();
                    string TaxType = itemdata.Tables[0].Rows[i]["TaxType"].ToString();
                    string lTaxAmt = itemdata.Tables[0].Rows[i]["TaxAmt"].ToString();
                    if (Units == "0" || Units == "" || Units == null) Units = "10000";
                    string BatchId = itemdata.Tables[0].Rows[i]["BatchId"].ToString();
                    if (BatchId == "0" || BatchId == "" || BatchId == null) BatchId = "0";
                    //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                    string LocationId = itemdata.Tables[0].Rows[i]["LocationId"].ToString();
                    if (LocationId == "0" || LocationId == "" || LocationId == null) LocationId = "0";
                    string PriceLevelId = itemdata.Tables[0].Rows[i]["PriceLevelId"].ToString();
                    if (PriceLevelId == "0" || PriceLevelId == "" || PriceLevelId == null) PriceLevelId = "1000";
                    string Rate = itemdata.Tables[0].Rows[i]["Rate"].ToString();
                    if (Rate == "0" || Rate == "" || Rate == null) Rate = Price;
                    string InclusiveTotal = itemdata.Tables[0].Rows[i]["InclusiveTotal"].ToString();
                    //if (InclusiveTotal == "0" || InclusiveTotal == "" || InclusiveTotal == null) InclusiveTotal = Amount;
                    //else Amount = InclusiveTotal;
                    SqlCommand cmddi = new SqlCommand("select sum(isnull(COLUMN04,0)) from FITABLE010 where COLUMN03=" + Item + " and COLUMN19=" + Units + " AND iif(COLUMN22='',0,isnull(COLUMN22,0))=" + BatchId + " AND iif(COLUMN21='',0,isnull(COLUMN21,0))=" + LocationId + " and COLUMN13 in(" + OpUnit + ") and COLUMNA03=" + AOwner + " ", cn);
                    SqlDataAdapter daai = new SqlDataAdapter(cmddi);
                    DataTable dtti = new DataTable();
                    daai.Fill(dtti);
                    string Avlqty = dtti.Rows[0][0].ToString();
                    if (Avlqty == "" || Avlqty == null) Avlqty = "0";
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_SAL_BL_INVOICE", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN02", rowid);

                        //Anusha UPC
                        Cmdl.Parameters.AddWithValue("@COLUMN03", UPC);
                        //End UPC 

                        Cmdl.Parameters.AddWithValue("@COLUMN05", Item);
                        //EMPHCS1820 rajasekhar reddy patakota 22/09/2016 Adding Item Description in Sales Receipt screen
                        Cmdl.Parameters.AddWithValue("@COLUMN06", SalesDesc);
                        Cmdl.Parameters.AddWithValue("@COLUMN22", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN31", BatchId);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", lQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN21", TaxType);
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Price);
                        Cmdl.Parameters.AddWithValue("@COLUMN37", PriceLevelId);
                        Cmdl.Parameters.AddWithValue("@COLUMN35", Rate);
                        Cmdl.Parameters.AddWithValue("@COLUMN14", Amount);
                        Cmdl.Parameters.AddWithValue("@COLUMN23", lTaxAmt);
                        Cmdl.Parameters.AddWithValue("@COLUMN19", lDiscount);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", Avlqty);
                        Cmdl.Parameters.AddWithValue("@COLUMN15", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN30", "22557");
                        //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                        Cmdl.Parameters.AddWithValue("@COLUMN38", LocationId);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@COLUMNB01", UPC);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE010");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        Cmdl.CommandTimeout = 0;
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                int lP = 0;
                SqlCommand cmddc = new SqlCommand("select  l.COLUMN02 LID,l.COLUMN08,h.COLUMN04,h.COLUMN02 HID from SATABLE012 l inner join SATABLE011 h on h.COLUMN01=l.COLUMN08 and h.COLUMNA02=l.COLUMNA02 and h.COLUMNA03=l.COLUMNA03 and isnull(h.COLUMNA13,0)=0 where l.column03=" + ide + "  and l.COLUMNA03=" + AOwner + " and isnull(l.COLUMNA13,0)=0", cn);
                SqlDataAdapter daac = new SqlDataAdapter(cmddc);
                DataTable dttc = new DataTable();
                daac.Fill(dttc);
                if (dttc.Rows.Count > 0)
                {
                    OutParam =Convert.ToInt32(dttc.Rows[0]["COLUMN08"].ToString());
                    string HColp = dttc.Rows[0]["HID"].ToString();
                    TransNo = dttc.Rows[0]["COLUMN04"].ToString();
                    int pm = 0;
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand CmdP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                        CmdP.CommandType = CommandType.StoredProcedure;
                        CmdP.Parameters.AddWithValue("@COLUMN02", HColp);
                        CmdP.Parameters.AddWithValue("@COLUMN03", "1278");
                        CmdP.Parameters.AddWithValue("@COLUMN04", TransNo);
                        CmdP.Parameters.AddWithValue("@COLUMN05", Customer);
                        CmdP.Parameters.AddWithValue("@COLUMN06", "");
                        CmdP.Parameters.AddWithValue("@COLUMN18", OrderType);
                        //EMPHCS1821 rajasekhar reddy patakota 23/09/2016 Date Feild Adding in Sales Receipt & Customer Onfly Cahanges
                        CmdP.Parameters.AddWithValue("@COLUMN19", CreatedDate);
                        CmdP.Parameters.AddWithValue("@COLUMN07", HCol2);
                        CmdP.Parameters.AddWithValue("@COLUMN10", Type);
                        CmdP.Parameters.AddWithValue("@COLUMN08", PayAcc);
                        CmdP.Parameters.AddWithValue("@COLUMN09", CardNo);
                        CmdP.Parameters.AddWithValue("@COLUMN14", OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMN13", PayAmt);
                        CmdP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        CmdP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        CmdP.Parameters.AddWithValue("@COLUMNA06", Date);
                        CmdP.Parameters.AddWithValue("@COLUMNA07", Date);
                        CmdP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        CmdP.Parameters.AddWithValue("@COLUMNA12", 1);
                        CmdP.Parameters.AddWithValue("@COLUMNA13", 0);
                        CmdP.Parameters.AddWithValue("@Direction", insert);
                        CmdP.Parameters.AddWithValue("@TabelName", "SATABLE011");
                        cnc.Open();
                        CmdP.CommandTimeout = 0;
                        pm = CmdP.ExecuteNonQuery();
                    }
                    //Line                
                    string LColp = dttc.Rows[0]["LID"].ToString();
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand CmdlP = new SqlCommand("usp_SAL_BL_PAYMENT", cnc);
                        CmdlP.CommandType = CommandType.StoredProcedure;
                        CmdlP.Parameters.AddWithValue("@COLUMN08", OutParam);
                        CmdlP.Parameters.AddWithValue("@COLUMN02", LColp);
                        CmdlP.Parameters.AddWithValue("@COLUMN03", HCol2);
                        CmdlP.Parameters.AddWithValue("@COLUMN04", TotAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN05", totamta);
                        CmdlP.Parameters.AddWithValue("@COLUMN06", PayAmt);
                        CmdlP.Parameters.AddWithValue("@COLUMN14", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMN13", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMN09", "0");
                        CmdlP.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        CmdlP.Parameters.AddWithValue("@COLUMNA03", AOwner);
                        CmdlP.Parameters.AddWithValue("@COLUMNA06", Date);
                        CmdlP.Parameters.AddWithValue("@COLUMNA07", Date);
                        CmdlP.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        CmdlP.Parameters.AddWithValue("@COLUMNA12", 1);
                        CmdlP.Parameters.AddWithValue("@COLUMNA13", 0);
                        CmdlP.Parameters.AddWithValue("@Direction", insert);
                        CmdlP.Parameters.AddWithValue("@TabelName", "SATABLE012");
                        CmdlP.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        CmdlP.CommandTimeout = 0;
                        lP = CmdlP.ExecuteNonQuery();
                    }
                }
                if (r > 0)
                {
                    var msg = string.Empty;
                    msg = "Sales Receipt Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                var idi = "3100";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return Json(new { Data = lP, InvoiceNo = HCol2, idi = idi, FormName = custform }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("PointOfSalesInfo", "PointOfSales");
                //return View("PointOfSalesInfo");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        
        public ActionResult PointOfSalesInfo()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                //var idi = "";
                //var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                var frmid = 1532;
                Session["id"] = frmid;
                var FormName = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmid).ToList().FirstOrDefault().COLUMN04;
                Session["FormName"] = FormName;
                Session["idi"] = dc.CONTABLE003.Where(a => a.COLUMN05 == FormName&&a.COLUMNA03 ==acID ).ToList().FirstOrDefault().COLUMN02;                     
                object[] sp = new object[4];
                sp[0] = (frmid);
                sp[1] = (Session["OPUnit"]);
                sp[2] = (Session["AcOwner"]);
                sp[3] = ("Info");
                var GData = dbs.Query("Exec USP_PROC_FormBuildInfo @FormId=@0,@OPUnit=@1,@AcOwner=@2,@Type=@3", sp);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                ViewBag.Columns = GData.FirstOrDefault().Columns;
                ViewBag.Grid = GData;
                string str = "select isnull(COLUMN04,'dd/MM/yyyy')COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    Session["FormatDate"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["FormatDate"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetCustomerDetails(string CustomerId)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
				//EMPHCS1816 rajasekhar reddy patakota 15/09/2016 Retail Screen changes and operating unit set up to null
                SqlCommand cmd = new SqlCommand("select column02 from SATABLE002 where (cast(COLUMN02 as nvarchar)="+CustomerId+" or COLUMN11 like '" + CustomerId + "%') and  isnull(COLUMNA13,'False')='False' and (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) and COLUMNA03=" + AcOwner + " ", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0) CustomerId = dt.Rows[0][0].ToString();
                else { CustomerId = "0"; }
                return Json(new { Data = CustomerId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }
        public ActionResult GetItemDetails(string ItemID,string CustomerID)
        {
            try
            {
                var OPUnit = Session["OPUnit"];
                var AcOwner = Session["AcOwner"];
                SqlCommand cmd = new SqlCommand("POSItemdetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItemID", ItemID);
                cmd.Parameters.AddWithValue("@Customer", CustomerID);
                cmd.Parameters.AddWithValue("@Type", "POS");
                cmd.Parameters.AddWithValue("@OPUnit",OPUnit);
                cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
                 SqlDataAdapter da = new SqlDataAdapter(cmd);
                 DataTable dt = new DataTable();
                 da.Fill(dt);
                 var DefaultPriceLevel = "";
				 //EMPHCS1849 rajasekhar reddy patakota 02/01/2017 Sales Receipt Invertory checking for Track QOH=0 items 
                 string htmlstring =null, htmlstring1 =null,TrackQty = "0";
                 var avl = "0";
                 string lot = "N", LotId = "", PPrice = "0", SPrice = "0";var AvailQty = "0";
                 var ExpDate = DateTime.Now.ToShortDateString();
                 for (int g = 0; g < dt.Rows.Count; g++)
                 {
                     ItemID = dt.Rows[0]["ItemId"].ToString();
                     LotId = dt.Rows[0]["LOT"].ToString();
                    if (LotId == "" || LotId == "undefined" || LotId == null) LotId = "0";
                     DefaultPriceLevel =PriceLevelInfo(ItemID, Convert.ToString(OPUnit));
                     if (DefaultPriceLevel == "" || DefaultPriceLevel == "1000")
                         DefaultPriceLevel = dt.Rows[0]["pricelevel"].ToString();
                     avl = dt.Rows[0][9].ToString();
                     SPrice = dt.Rows[g][dt.Columns[4].ColumnName].ToString();
                     TrackQty = dt.Rows[0]["TrackQty"].ToString();
                     if (TrackQty == "1" || TrackQty == "True") TrackQty = "1";
                     string dvalue =Convert.ToString(dt.Rows[g][dt.Columns[7].ColumnName]);
                     string dddata = "";
                     List<SelectListItem> UOM = new List<SelectListItem>();
                     SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,'False')='False'", cn);
                     DataTable dtdata = new DataTable();
                     cmddl.Fill(dtdata);
                     for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                     {
                         UOM.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                     }
                     ViewData["UOM"] = new SelectList(UOM, "Value", "Text", selectedValue: dvalue).ToString();
                     dddata += "<option value=''>--Select--</option>";
                     for (int d = 0; d < UOM.Count; d++)
                     {
                         if (UOM[d].Value == dvalue)
                             dddata += "<option value=" + UOM[d].Value + " selected>" + UOM[d].Text + "</option>";
                         else
                             dddata += "<option value=" + UOM[d].Value + ">" + UOM[d].Text + "</option>";
                     }
                     if (dt.Rows.Count > 0)
                     {
                         SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN31'  and column04=110010828 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                         SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                         DataTable dtlt = new DataTable();
                         dalt.Fill(dtlt);
                         if (dtlt.Rows.Count > 0)
                         {
                             lot = dtlt.Rows[0][0].ToString();
                             if (ItemID != "" && lot != "N" && lot != "0" && lot != "")
                             {
                                 SqlCommand cmdl = new SqlCommand("usp_PROC_TP_POSLotDetails", cn);
                                 cmdl.CommandType = CommandType.StoredProcedure;
                                 cmdl.Parameters.AddWithValue("@ItemID", ItemID);
                                 cmdl.Parameters.AddWithValue("@UOM", dt.Rows[0]["unitid"]);
                                 cmdl.Parameters.AddWithValue("@DateF", Session["DateFormat"]);
                                 cmdl.Parameters.AddWithValue("@OPUnit", Session["OPUnit1"]);
                                 cmdl.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                                 SqlDataAdapter dal = new SqlDataAdapter(cmdl);
                                 DataTable dtl = new DataTable();
                                 dal.Fill(dtl);
                                 if (dtl.Rows.Count > 0)
                                 {
                                     ExpDate = dtl.Rows[0]["COLUMN05"].ToString();
                                    if (LotId == "" || LotId == "0" || LotId == null)
                                        LotId = dtl.Rows[0]["COLUMN02"].ToString();
                                 }
                                 if (ItemID != "" && ItemID != null && ItemID != "undefined")
                                 {
                                     SqlCommand cmdp = new SqlCommand("usp_PROC_TP_LotWisePrice", cn);
                                     cmdp.CommandType = CommandType.StoredProcedure;
                                     cmdp.Parameters.Add(new SqlParameter("@ItemID", ItemID));
                                     cmdp.Parameters.Add(new SqlParameter("@UOM", dt.Rows[0]["unitid"]));
                                     cmdp.Parameters.Add(new SqlParameter("@Lot", LotId));
                                     //cmdp.Parameters.Add(new SqlParameter("@location", LocationId));
                                     cmdp.Parameters.Add(new SqlParameter("@FrmID", 1277));
                                     cmdp.Parameters.Add(new SqlParameter("@OPUnit", Session["OPUnit1"]));
                                     cmdp.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                                     SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                                     DataTable dt1 = new DataTable();
                                     dap.Fill(dt1);
                                     if (dt1.Rows.Count > 0) { PPrice = dt1.Rows[0]["PPrice"].ToString(); SPrice = dt1.Rows[0]["SPrice"].ToString(); avl = dt1.Rows[0]["AvailQty"].ToString(); }
                                 }
                             }
                         }
                     }
                     if (avl == "" || avl == null) avl = "0"; var qty = "0"; if (avl != "0" && avl != "0.00" && avl != "0.000") qty = "1";
                     htmlstring += "<tr><td class=rowshow><div class=initshow><span id=dItemId>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[0].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[0].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[0].ColumnName] + "' ><span id=saveItemId style='display:none'>" + dt.Rows[g][dt.Columns[0].ColumnName] + "</span></td>";
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dUPC>" + dt.Rows[g][dt.Columns[13].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[13].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[13].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[13].ColumnName] + "' ></td>";
                    //Anusha UPC 
                    htmlstring += "<td class=rowshow><div class=initshow><span id=dItemName>" + dt.Rows[g][dt.Columns[1].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[1].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[1].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[1].ColumnName] + "' ></td>";
                    //End UPC

                    var idi = "";
                     var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                     var fnamedata = dc.CONTABLE0010.Where(a => a.COLUMN04 == custform).ToList();
                     var fid = fnamedata.Select(a => a.COLUMN02).FirstOrDefault();
                     if (fid != 1603 && fid != 1604)
                     {
					 //EMPHCS1820 rajasekhar reddy patakota 22/09/2016 Adding Item Description in Sales Receipt screen
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dSalesDesc>" + dt.Rows[g][dt.Columns[8].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[8].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[8].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[8].ColumnName] + "' ></td>";}
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><select   class ='gridddl edit-mode'  name='" + dt.Columns[2].ColumnName + "'   id=dynamicuom  >" + dddata + "</select></td>";
                     //htmlstring += "<td><div class=initshow><span id=dUnits>" + dt.Rows[g][dt.Columns[2].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[2].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[2].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[2].ColumnName] + "' ></td>";
					 //EMPHCS1849 rajasekhar reddy patakota 02/01/2017 Sales Receipt Invertory checking for Track QOH=0 items 
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dQuantity>" + qty + "</span></div><input type='text' id='" + dt.Columns[3].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[3].ColumnName + "'    value='" + qty + "' ><input type='text' id='avl' style='display: none'   name='avl'    value='" + avl + "' ><input type='TrackQty' id='TrackQty' style='display: none'   name='TrackQty'    value='" + dt.Rows[g]["TrackQty"] + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dPrice>" + SPrice + "</span></div><input type='text' id='" + dt.Columns[4].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[4].ColumnName + "'    value='" + SPrice + "' ></td>";
                     htmlstring += "<td class=rowshow><div class=initshow><span id=dAmount>" + dt.Rows[g][dt.Columns[5].ColumnName] + "</span></div><input type='text' id='" + dt.Columns[5].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[5].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[5].ColumnName] + "' ></td>";
                     htmlstring += "<td style='display:none;'><input type='text' id='" + dt.Columns[6].ColumnName + "' class='txtgridclass edit-mode'  name='" + dt.Columns[6].ColumnName + "'    value='" + dt.Rows[g][dt.Columns[6].ColumnName] + "'  style='display:none;'><span id=dTaxamt style='display:none;'>0</span></td>";
                     htmlstring += "<td style='display:none;'><div class=initshow><span id=ddiscount style='display:none;'>0</span></div><input type='text' id='Discount' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
                     htmlstring += "<td style='display:none;'><div class=initshow><span id=ddisper style='display:none;'>0</span></div><input type='text' id='disper1' class='txtgridclass edit-mode'  name='Discount'    value=0  style='display:none;'></td>";
					 //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                     htmlstring += "<td style='display:none;'><input type='text' id='chkrow' class='txtgridclass edit-mode'  name='chkrow'    value=0  style='display:none;'><span id=saveUnitId style='display:none'>" + dt.Rows[g][dt.Columns[7].ColumnName] + "</span><span id=itemBatchId style='display:none'>0</span><span id=lotExpDate style='display:none'></span><span id=LocationId style='display:none'>0</span></td></tr>";
                 }
				 //EMPHCS1861 rajasekhar reddy patakota 20/02/2017 Location setup in invoice line level
                 string location = "N", LocationId = "0";
                 if (ItemID != "" && ItemID != "0" && dt.Rows.Count > 0)
                 {
                     SqlCommand cmdlt = new SqlCommand("select COLUMN07 from SETABLE012 where column06=1277 and column05='COLUMN39'  and column04=110010827 AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                     SqlDataAdapter dalt = new SqlDataAdapter(cmdlt);
                     DataTable dtlt = new DataTable();
                     dalt.Fill(dtlt);
                     if (dtlt.Rows.Count > 0)
                     {
                         location = dtlt.Rows[0][0].ToString();
                         if (ItemID != "" && ItemID != "0" && location != "N" && location != "0" && location != "")
                         {
                             SqlDataAdapter cmddl = new SqlDataAdapter("select l.COLUMN02,l.COLUMN04 from CONTABLE030 l left join fiTABLE010 f on f.COLUMN21=l.COLUMN02 and f.COLUMNA03=l.COLUMNA03 and f.COLUMN03=" + ItemID + " Where  (l.COLUMNA02 in(" + OPUnit + ") or isnull(l.COLUMNA02,0)=0)   AND (l.COLUMNA03='" + Session["AcOwner"] + "'  or l.COLUMNA03 is null)  and isnull(l.COLUMNA13,'False')='False'  and isnull(l.COLUMN07,'False')='False' group by l.COLUMN02,l.COLUMN04 order by sum(isnull(f.COLUMN04,0)) desc", cn);
                             DataTable dtdata = new DataTable();
                             cmddl.Fill(dtdata);
                             if (dtdata.Rows.Count > 0)
                                 LocationId = dtdata.Rows[0]["COLUMN02"].ToString();
                         }
                     }
                 }
				 //EMPHCS1849 rajasekhar reddy patakota 02/01/2017 Sales Receipt Invertory checking for Track QOH=0 items 
                 return Json(new { Data = htmlstring, Avail = avl, TrackQty = TrackQty, lot = lot, LotId = LotId, ExpDate = ExpDate, location = location, LocationId = LocationId, PriceLevelId = DefaultPriceLevel }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
                var idi = "";
                if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return RedirectToAction("Info", "EmployeeMaster", new { idi = idi, FormName = custform });
            }
        }

        //EMPHCS1807 rajasekhar reddy patakota 29/08/2016 Edit,View Permission setup at account owner level
        public void PermissionSetup()
        {
            try
            {
                SqlCommand cmdpl = new SqlCommand("usp_MAS_TP_Permissionchk", cn);
                cmdpl.CommandType = CommandType.StoredProcedure;
                cmdpl.Parameters.Add(new SqlParameter("@Emp", Session["eid"]));
                cmdpl.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmdpl.Parameters.Add(new SqlParameter("@Opunit", Session["OPUnit1"]));
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                Session["Permission"] = "";
                List<int> myList = new List<int>();
                if (dtpl.Rows.Count > 0)
                {
                    var ids = dtpl.Rows[0]["COLUMN03"].ToString();
                    if (ids == "" || ids == null)
                        myList = dc.MATABLE002.Where(a => a.COLUMN03 == 11172).Select(t => t.COLUMN02).ToList();
                    else if (ids != "")
                        myList = ids.Split(',').Select(t => int.Parse(t)).ToList();
                }
                else
                    myList = dc.MATABLE002.Where(a => a.COLUMN03 == 11172).Select(t => t.COLUMN02).ToList();
                Session["Permission"] = myList;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
            }
        }

        public ActionResult ItemFullGridFill(string Query, string FormName, int RowNo, string Type)
        {
            try
            {
                Session["NoGridRows"] = "";
                string type = "Insert", formname = "";
                var acID = (int?)Session["AcOwner"];
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                SqlCommand cmd = new SqlCommand(Query, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int endrow = RowNo + 99;
                if (Type == "Next") endrow = RowNo + 99;
                else if (Type == "Prev") endrow = RowNo - 99;
                string ORDER = " ORDER BY p.COLUMNA06 desc ";
                var last = Query.Trim().Length;
                int m = Query.IndexOf("ORDER BY"); if (m > 0) { ORDER = Query.Substring(m).ToString(); Query = Query.Replace(ORDER, "").ToString(); }
                Query = Query.Replace("SELECT", "SELECT ROW_NUMBER() OVER (" + ORDER + ") AS 'RowNumber',");
                if (Type == "Next") Query = "select * from ( " + Query + " ) q where q.RowNumber BETWEEN " + RowNo + " and " + endrow + "";
                else if (Type == "Prev") Query = "select * from ( " + Query + " ) q where q.RowNumber BETWEEN " + endrow + " and " + RowNo + "";
                var GData = dbs.Query(Query);
                List<WebGridColumn> gcol = new List<WebGridColumn>();
                List<string> colNM = new List<string>();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    colNM.Add(dt.Columns[i].ColumnName);
                }
                List<int> permission = Session["Permission"] as List<int>; var infospcace = " | "; if (Convert.ToString(Session["PForms"]) == "") infospcace = "";
                if (formname != null && permission.Contains(22935))
                {
                    gcol.Add(new WebGridColumn()
                    {
                        Header = "",
                        Format = (item) => new HtmlString("<div class='ch'><a href='/PointOfSales/PointOfSalesEdit?ide=" + item.ID + "&FormName=" + FormName + " &idi=3100 ' style=''>Edit</a> | <a href='/PointOfSales/PointOfSalesView?ide=" + item.ID + "&FormName=" + FormName + " &idi=3100 '' style=''>View</a> | <a  href='/ReturnReceipt/ReturnReceiptNew?idi=3139&FormName=" + FormName + "&ide=" + item.ID + "  '>Return</a> </div>")
                    });
                }
                else
                {
                    gcol.Add(new WebGridColumn()
                    {
                        ColumnName = "",
                        Header = "",
                        Format = (item) => new HtmlString("<div class='ch'><a href='/PointOfSales/PointOfSalesView?ide=" + item.ID + "&FormName=" + FormName + " &idi=3100 '' style=''>View</a></div>")
                    });
                }
                foreach (string dtr in colNM)
                {
                    if (dtr != "ID")
                    {
                        gcol.Add(new WebGridColumn()
                        {
                            ColumnName = dtr,
                            Header = dtr
                        });
                    }
                }
                var grid1 = new WebGrid(null, canPage: false, canSort: false);
                grid1 = new WebGrid(GData, canPage: false, canSort: false);
                var htmlstring1 = grid1.GetHtml(
                htmlAttributes: new { id = "grdfData", style = "500px" },
                columns: gcol);
                return Json(new { grid = htmlstring1.ToHtmlString(), rowscnt = dt.Rows.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
        }
    }
}
