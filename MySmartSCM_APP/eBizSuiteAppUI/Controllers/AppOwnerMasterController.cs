﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using eBizSuiteAppModel.Table;
using eBizSuiteDAL.classes;
using WebMatrix.Data;
using System.Configuration;

namespace eBizSuiteProduct.Controllers
{
    public class AppOwnerMasterController : Controller
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        AppOwnerManage userManager = new AppOwnerManage();

        // GET: /RoleMaster/

        public ActionResult Index()
        {
            return View();
        }

        //get rolemaster
        [HttpGet]
        public ActionResult AppOwnerMasterDetailes()
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_AppOwnerMaster";
            ViewBag.grid = db.Query(query);

            // dbContext.tbl_RoleMaster.ToList()
            return View("AppOwnerMasterDetailes", dbContext.tbl_AppOwnerMaster.ToList());
        }

        //create
        [HttpGet]
        public ActionResult NewAppOwnerMaster()
        {
            return View();
        }
        //create
        [HttpPost]
        public ActionResult NewAppOwnerMaster(tbl_AppOwnerMaster user)
        {
            try
            {
                if (ModelState.IsValid)
                {



                    if (!userManager.IsUserLoginIDExist(user.ApplicationOwnerId))
                    {

                        userManager.Add(user);

                        FormsAuthentication.SetAuthCookie(user.AppOwnerUserName, false);
                        return RedirectToAction("AppOwnerMasterDetailes", "AppOwnerMaster");

                    }
                    else
                    {
                        ModelState.AddModelError("", "AppOwnerId already taken");
                    }
                }
            }
            catch
            {
                return View(user);
            }

            return View(user);
        }



        //edit
        [HttpGet]
        public ActionResult EditAppOwnerMaster(int id)
        {
            var data = userManager.GetEmployeeDetail(id);
            tbl_AppOwnerMaster emp = new tbl_AppOwnerMaster();
            emp.ApplicationOwnerId = data.ApplicationOwnerId;
            emp.AppOwnerUserName = data.AppOwnerUserName;
            emp.CompanyName = data.CompanyName;


            return View("EditAppOwnerMaster", emp);


        }

        ////update
        [HttpPost]
        public ActionResult UpdateAppOwnerMaster(tbl_AppOwnerMaster register)
        {
            tbl_AppOwnerMaster sysRegister = new tbl_AppOwnerMaster();

            sysRegister.ApplicationOwnerId = register.ApplicationOwnerId;
            sysRegister.AppOwnerUserName = register.AppOwnerUserName;
            sysRegister.CompanyName = register.CompanyName;


            bool IsSuccess = userManager.UpdateAppOwnerMaster(sysRegister);


            if (IsSuccess)
            {
                TempData["OperStatus"] = "Employee updated succeessfully";
                ModelState.Clear();
                return RedirectToAction("AppOwnerMasterDetailes", "AppOwnerMaster");
            }

            return View();

        }


        ////delete single row
        public ActionResult DeleteAppOwnerMaster(int id)
        {
            bool check = userManager.DeleteAppOwnerMaster(id);

            var data = userManager.GetAppOwnerMaster();
            ViewBag.grid = data;
            return PartialView("AppOwnerMasterDetailes", dbContext.tbl_AppOwnerMaster.ToList());

        }

        //delete multiple selections
        [HttpPost]
        public ActionResult DeleteMultiple(string[] assignChkBx)
        {
            //Delete Selected 
            int[] id = null;
            if (assignChkBx != null)
            {
                id = new int[assignChkBx.Length];
                int j = 0;
                foreach (string i in assignChkBx)
                {
                    int.TryParse(i, out id[j++]);
                }
            }

            if (id != null && id.Length > 0)
            {
                List<tbl_AppOwnerMaster> allSelected = new List<tbl_AppOwnerMaster>();
                eBizSuiteTableEntities dc = new eBizSuiteTableEntities();

                allSelected = dc.tbl_AppOwnerMaster.Where(a => id.Contains(a.ApplicationOwnerId)).ToList();
                foreach (var i in allSelected)
                {
                    dc.tbl_AppOwnerMaster.Remove(i);
                }
                dc.SaveChanges();

            }

            return PartialView("AppOwnerMasterDetailes");

            //return RedirectToAction("AppOwnerMasterDetailes");
        }


        ////Update InLine
        [HttpPost]
        public ActionResult UpdateInline(string list)
        {

            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                var c = dc.tbl_AppOwnerMaster.Where(a => a.ApplicationOwnerId == eid).FirstOrDefault();

                c.AppOwnerUserName = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                c.CompanyName = ds.Tables[0].Rows[i].ItemArray[2].ToString();

                dc.SaveChanges();

            }
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_AppOwnerMaster";
            ViewBag.grid = db.Query(query);

            return PartialView("AppOwnerMasterDetailes", dbContext.tbl_AppOwnerMaster.ToList());
        }

        //1 search RoleMaster
        [HttpGet]
        public ActionResult SearchAppOwnerMaster()
        {
            var db = Database.Open("sqlcon");
            var query = "select * from tbl_AppOwnerMaster";

            ViewBag.grid = db.Query(query);

            //dbContext.tbl_RoleMaster.ToList()
            return PartialView();
        }

        //2 search RoleMaster
        [HttpGet]
        [ActionName("SearchEmployePost")]
        public ActionResult SearchEmployePost(string id)
        {
            var db = Database.Open("sqlcon");

            var ApplicationOwnerId = id;
            var AppOwnerUserName = "";

            if (ApplicationOwnerId != null)
            {
                ApplicationOwnerId = id + "%";
            }
            else
                ApplicationOwnerId = id;

            var query = "SELECT * FROM tbl_AppOwnerMaster  WHERE ApplicationOwnerId like '" + ApplicationOwnerId + "' or AppOwnerUserName LIKE '" + AppOwnerUserName + "'  ";

            var grid = dbContext.tbl_AppOwnerMaster.SqlQuery(query);

            ViewBag.grid = grid;
            Session["griddata"] = grid;
            Session["data"] = ViewBag.grid;

            return PartialView("SearchAppOwnerMaster");
        }


        [HttpPost]
        [ActionName("SearchAppOwnerMaster")]
        public ActionResult SearchAppOwnerMaster(int assignChkBx)
        {

            var query = "SELECT * FROM tbl_AppOwnerMaster  WHERE ApplicationOwnerId =" + assignChkBx + "";

            var query1 = dbContext.tbl_AppOwnerMaster.SqlQuery(query);

            return View("AppOwnerMasterDetailes", query1);
        }


        //Advance Search
        public ActionResult AdvanceSearch()
        {
            return View("AdvanceSearch");
        }
        [HttpPost]
        [ActionName("AdvanceSearch")]
        public ActionResult Searchpost()
        {

            var ApplicationOwnerId = "";
            var AppOwnerUserName = "";
            var CompanyName = "";

            if (Request["ApplicationOwnerId"] != string.Empty)
            {
                ApplicationOwnerId = Request["ApplicationOwnerId"] + "%";
            }
            else
                ApplicationOwnerId = Request["ApplicationOwnerId"];


            if (Request["AppOwnerUserName"] != string.Empty)
            {
                AppOwnerUserName = Request["AppOwnerUserName"] + "%";
            }
            else
                AppOwnerUserName = Request["AppOwnerUserName"];


            if (Request["CompanyName"] != string.Empty)
            {
                CompanyName = Request["CompanyName"] + "%";
            }
            else
                CompanyName = Request["CompanyName"];


            var query = "SELECT * FROM tbl_AppOwnerMaster  WHERE ApplicationOwnerId like '" + ApplicationOwnerId + "' or AppOwnerUserName LIKE '" + AppOwnerUserName + "' or CompanyName LIKE '" + CompanyName + "'  ";

            var query1 = dbContext.tbl_AppOwnerMaster.SqlQuery(query);

            return View("AppOwnerMasterDetailes", query1);
        }
    }
}
