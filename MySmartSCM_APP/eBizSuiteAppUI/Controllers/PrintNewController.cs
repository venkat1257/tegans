﻿using eBizSuiteAppDAL.classes;
using eBizSuiteAppModel.Table;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using MeBizSuiteAppUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Dynamic;
using System.Runtime.InteropServices;
namespace MeBizSuiteAppUI.Controllers
{
    public class PrintNewController : Controller
    {
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string providerName = "System.Data.SqlClient";
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public List<POPrint> GETCUSTOMERDETAILS(int CustomerId, int a1, int CompanyID, List<POPrint> all1)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();

                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();
                    if (dtA.Rows[0]["custPhNo"].ToString() == "" || dtA.Rows[0]["custPhNo"].ToString() == null)
                        all1[0].custPhNo = "";
                    else
                        all1[0].custPhNo = dtA.Rows[0]["custPhNo"].ToString();
                    if (dtA.Rows[0]["Custmail"].ToString() == "" || dtA.Rows[0]["Custmail"].ToString() == null)
                        all1[0].Custmail = "";
                    else
                        all1[0].Custmail = dtA.Rows[0]["Custmail"].ToString();
                    if (dtA.Rows[0]["CustGSTIN"].ToString() == "" || dtA.Rows[0]["CustGSTIN"].ToString() == null)
                        all1[0].CustGSTIN = "";
                    else
                        all1[0].CustGSTIN = dtA.Rows[0]["CustGSTIN"].ToString();
                }
                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);

                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();
                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].OPUPhNo = "";
                    else
                        all1[0].OPUPhNo = odt.Rows[0]["COLUMN34"].ToString();
                    if (odt.Rows[0]["COLUMND02"].ToString() == "" || odt.Rows[0]["COLUMND02"].ToString() == null)
                        all1[0].OPGSTIN = "";
                    else
                        all1[0].OPGSTIN = odt.Rows[0]["COLUMND02"].ToString();

                }
                SqlCommand ocmcc = new SqlCommand("CompanyAddress", conn);
                ocmcc.CommandType = CommandType.StoredProcedure;
                ocmcc.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                SqlDataAdapter odc = new SqlDataAdapter(ocmcc);
                DataTable odct = new DataTable();
                odc.Fill(odct);
                if (odct.Rows.Count > 0)
                {
                    if (odct.Rows[0]["COLUMN03"].ToString() == "" || odct.Rows[0]["COLUMN03"].ToString() == null)
                        all1[0].compADD = "";
                    else
                        all1[0].compADD = odct.Rows[0]["COLUMN03"].ToString();

                    if (odct.Rows[0]["COLUMN04"].ToString() == "" || odct.Rows[0]["COLUMN04"].ToString() == null)
                        all1[0].companydesc = "";
                    else
                        all1[0].companydesc = odct.Rows[0]["COLUMN04"].ToString();

                    if (odct.Rows[0]["COLUMN07"].ToString() == "" || odct.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].companymail = "";
                    else
                        all1[0].companymail = odct.Rows[0]["COLUMN07"].ToString();

                    if (odct.Rows[0]["COLUMN09"].ToString() == "" || odct.Rows[0]["COLUMN09"].ToString() == null)
                        all1[0].compA1 = "";
                    else
                        all1[0].compA1 = odct.Rows[0]["COLUMN09"].ToString();

                    if (odct.Rows[0]["COLUMN10"].ToString() == "" || odct.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].compA2 = "";
                    else
                        all1[0].compA2 = odct.Rows[0]["COLUMN10"].ToString();

                    if (odct.Rows[0]["COLUMN11"].ToString() == "" || odct.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].compA3 = "";
                    else
                        all1[0].compA3 = odct.Rows[0]["COLUMN11"].ToString();

                    if (odct.Rows[0]["compCtry"].ToString() == "" || odct.Rows[0]["compCtry"].ToString() == null)
                        all1[0].compCtry = "";
                    else
                        all1[0].compCtry = odct.Rows[0]["compCtry"].ToString();

                    if (odct.Rows[0]["compState"].ToString() == "" || odct.Rows[0]["compState"].ToString() == null)
                        all1[0].compState = "";
                    else
                        all1[0].compState = odct.Rows[0]["compState"].ToString();

                    if (odct.Rows[0]["compCity"].ToString() == "" || odct.Rows[0]["compCity"].ToString() == null)
                        all1[0].compCity = "";
                    else
                        all1[0].compCity = odct.Rows[0]["compCity"].ToString();

                    if (odct.Rows[0]["COLUMND02"].ToString() == "" || odct.Rows[0]["COLUMND02"].ToString() == null)
                        all1[0].CGSTIN = "";
                    else
                        all1[0].CGSTIN = odct.Rows[0]["COLUMND02"].ToString();
                }


                return all1;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }
        public List<POPrint> GETCUSTOMERDETAILSO(int CustomerId, List<POPrint> all1)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();

                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();
                }
                return all1;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }
        public string RupeesToWordsNew(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }
            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }
        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWordsNew(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWordsNew(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWordsNew(res) + " Thousand";
            }
            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                result = result + ' ' + RupeesToWordsNew(res) + " Hundred";
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWordsNew(res);
            }
            result = result;
            return result;
        }
        public ActionResult pdf(string data)
        {
            Session["data"] = data;
            return RedirectToAction("GetPdf", new { ID = Session["PRINTID"] });
        }
        public class HeaderF1 : PdfPageEventHelper
        {
            public override void OnStartPage(PdfWriter writer, Document document)
            {
                string filePath = "";
                //string logo = "partner.png";
                string logo = System.Web.HttpContext.Current.Session["logoName"].ToString();
                if (logo == "")
                {
                    filePath = HostingEnvironment.MapPath("~/Content/Upload/NoLogo.jpg");
                }
                else
                {
                    filePath = HostingEnvironment.MapPath("~/Content/Upload/" + logo);
                }
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(filePath);
                jpg.ScaleToFit(45f, 45f);
                jpg.SetAbsolutePosition(document.PageSize.Width - 36f - 72f, document.PageSize.Height - 36f - 53f);
                jpg.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_RIGHT;
                jpg.IndentationLeft = 9f;
                jpg.SpacingAfter = 9f;
                jpg.BorderWidthTop = 46f;
                if (logo == "")
                {
                    jpg.BorderColor = iTextSharp.text.BaseColor.WHITE;
                    jpg.BackgroundColor = iTextSharp.text.BaseColor.WHITE;
                }
                document.Add(jpg);
                base.OnStartPage(writer, document);
            }
        }
        public FileStreamResult GetPdf(string ID)
        {
            try
            {
                string filePath = "";
                string data = Session["data"].ToString();
                string pdfData = String.Format("<html><head>{0}</head><body>{1}</body></html>", "<style>table{border-spacing:10px;border-collapse:collapse;}</style>", data);
                var pdfbytes = System.Text.Encoding.UTF8.GetBytes(pdfData);
                var input = new MemoryStream(pdfbytes);
                var output = new MemoryStream();
                var document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                var writer = PdfWriter.GetInstance(document, Response.OutputStream);
                writer.PageEvent = new HeaderF1();
                writer.CloseStream = false;
                document.Open();
                var xmlworker = iTextSharp.tool.xml.XMLWorkerHelper.GetInstance();
                xmlworker.ParseXHtml(writer, document, input, System.Text.Encoding.UTF8);
                document.Close();
                output.Position = 0;
                return new FileStreamResult(output, "application/pdf");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }
        public void printpdf(string fname, string html)
        {
            try
            {
                var idName = Session["idName"];

                Session["file"] = null;
                Session["subject"] = null;

                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                string filePath = HostingEnvironment.MapPath("~/Content/Pdf/");

                string path = idName.ToString();
                string pathpdf1 = path.Replace(" ", "");
                string pathpdf2 = pathpdf1.Replace("/", "");
                string pathpdf = pathpdf2.Replace(":", "");
                int AOwner = Convert.ToInt32(Session["AcOwner"]);
                var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                filePath = filePath + AOwnerName;
                Session["file"] = Convert.ToString(Request["FormName"]) + " " + pathpdf + ".pdf";
                ViewBag.file = pathpdf + ".pdf";
                if (pdfDoc.IsOpen())
                    pdfDoc.Close();
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                if (System.IO.File.Exists(filePath + "\\" + pathpdf + ".pdf"))
                {
                    System.IO.File.Delete(filePath + "\\" + pathpdf + ".pdf");
                }
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(filePath + "\\" + pathpdf + ".pdf", FileMode.Create));
                pdfDoc.Open();
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                StringReader sr = new StringReader(html.ToString().Replace("<br />", "").Replace("<br>", "").Replace("alt=\"\">", "></img>"));
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                Session["PATH"] = null;
                Session["PATH"] = filePath + "\\" + pathpdf + ".pdf";

                Session["subject"] = Convert.ToString(Request["FormName"]) + " # " + pathpdf;
                ViewBag.subject = pathpdf + " transaction";

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");

                pdfDoc.Close();

            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
            }
        }
        public ActionResult SalesQuotationPrintfly(string sono, string checkval)
        {
            try
            {
                ViewBag.checkval = checkval;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    SONO = "",
                    Reference = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    Signature = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select S15.COLUMN04,s.COLUMN05 COLUMN05,S15.COLUMN07,S15.COLUMN01,S15.COLUMN02 COLUMN02,S15.COLUMN15,S15.COLUMN05 cust,S15.COLUMN13 oper ,S15.COLUMN20 as Discount,S15.COLUMN05 as cId,(select COLUMN07 from MATABLE013 WHERE	 COLUMN02= S15.COLUMN16) as HTamnt,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,c.COLUMN04 as company,c.COLUMN28 as logoName, S15.COLUMN24 as oppunit,  s13.COLUMN04 as ref,m.COLUMN04 as PaymentTerms,cast(S15.COLUMN08 as date) duedate,c.COLUMN27 vatno,c.COLUMN29 cstno,  c.COLUMN31 panno,S15.COLUMN22 totalamt,CONVERT(varchar,CAST(S15.COLUMN22 AS money), 1) totalamt1,pm.COLUMN04 Paymentmode  ,s.COLUMN26 cpanno ,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,opu.COLUMN03 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO, opu.COLUMN34 as OPUPhNo,c.COLUMN35 as tin, c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip,s.COLUMN11 custPhNo,c.COLUMN34 as footerText,s.COLUMN29 ctaxno,m10.COLUMN33 signature,M16.COLUMN03 compCtry,M17.COLUMN03 compState,c.COLUMND02 CGSTIN,s.COLUMN42 CustGSTIN,c.COLUMN02 CompanyID  from SATABLE015 S15 left join matable002 pm on pm.column02=S15.COLUMN12  left join MATABLE022 m on m.column02=S15.column11    left join CONTABLE007 opu on opu.COLUMN02=S15.COLUMN24  left join SATABLE002 s on s.column02=S15.COLUMN05	 left join CONTABLE008 c on c.COLUMNA03=S15.COLUMNA03 and c.COLUMNA13=0 left outer join SATABLE013 s13 on s13.COLUMN02=S15.COLUMN18 left outer join MATABLE010 m10 on m10.COLUMN02=S15.COLUMNA08 LEFT JOIN MATABLE016 M16 ON M16.COLUMN02 = c.COLUMN15 LEFT JOIN MATABLE017 M17 ON M17.COLUMN02 = c.COLUMN13  LEFT JOIN CONTABLE007 C7 ON C7.COLUMN02 = S15.COLUMN13 where S15.column04='" + sono + "' OR S15.column02='" + sono + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                SqlCommand cmdSQ = new SqlCommand("select max(COLUMN02) from SATABLE005 where COLUMN34=" + sono + "", cn);
                SqlDataAdapter dasq = new SqlDataAdapter(cmdSQ);
                DataTable dtsq = new DataTable();
                string SalesOrderID = "";
                if (dtsq.Rows.Count > 0)
                    SalesOrderID = Convert.ToString(dtsq.Rows[0][0].ToString());
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0]["oper"].ToString());
                Session["OPUnitM"] = opunit;
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0]["logoName"]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0]["logoName"].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0]["logoName"]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0]["logoName"].ToString());
                Session["logoName"] = (dt.Rows[0]["logoName"].ToString());
                if (Convert.ToString(dt.Rows[0]["signature"]) != "")
                    all1[0].Signature = "/Content/Upload/Images/Employee/Employee/" + (dt.Rows[0]["signature"].ToString());
                else
                    all1[0].Signature = "";
                if (formName != null && Convert.ToString(dt.Rows[0]["signature"]) != "")
                    all1[0].Signature = Server.MapPath("/Content/Upload/Images/Employee/Employee/" + dt.Rows[0]["signature"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0]["PaymentTerms"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].PaymentMode = (dt.Rows[0]["Paymentmode"].ToString());
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());

                all1[0].Reference = (dt.Rows[0]["ref"].ToString());
                all1[0].VATNO = (dt.Rows[0]["vatno"].ToString());
                all1[0].CSTNO = (dt.Rows[0]["cstno"].ToString());
                all1[0].PANNO = (dt.Rows[0]["panno"].ToString());
                all1[0].Reference = (dt.Rows[0]["ref"].ToString());
                var duedate = Convert.ToDateTime(dt.Rows[0]["duedate"].ToString()).ToString(DateF);
                all1[0].DueDate = duedate;
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].CGSTIN = dt.Rows[0]["CGSTIN"].ToString();
                all1[0].CustGSTIN = dt.Rows[0]["CustGSTIN"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                int CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"].ToString());
                string[] param1 = dt.Rows[0]["totalamt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();

                var dbs = Database.Open("sqlcon");
                var sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN03) item,(select column06 from matable007 where COLUMN02=a.COLUMN03) upc,a.COLUMN06 quantity,a.COLUMN05 'desc',CONVERT(varchar, CAST(a.COLUMN07 AS money), 1) rate,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN12) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= ( select COLUMN16 from  SATABLE015 b   where b.COLUMN01='" + internalid + "')) as HTamnt,cast((a.COLUMN08  * ((select COLUMN07 from MATABLE013 WHERE  COLUMN02= ( select COLUMN16 from  SATABLE015 b  where b.COLUMN01='" + internalid + "')))/100) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE    COLUMN02= ( select COLUMN16 from  SATABLE015 b  where b.COLUMN01='" + internalid + "'))) names, a.COLUMN08  amt from  SATABLE016 a  where  a.column11='" + internalid + "' and a.columna13=0";
                var GData = dbs.Query(sql);
                ViewBag.pitemsdata = GData;
                SqlCommand cmdg = new SqlCommand("select sum(isnull(COLUMN08,0)) amt,sum(isnull(COLUMN06,0)) qty,CONVERT(varchar,CAST(sum(isnull(COLUMN08,0)) AS money), 1) subtotamt from  SATABLE016 where column11='" + internalid + "' and columna13=0", cn);
                SqlDataAdapter dag = new SqlDataAdapter(cmdg);
                DataTable dtg = new DataTable();
                dag.Fill(dtg);
                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*(CAST(isnull(lt.column07,0)as decimal(18,2) )/100)) as decimal(18,2))) linetotaltax from   SATABLE016 a  left join SATABLE015 b on  b.COLUMN01=a.COLUMN11 left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN13,'-',''))) s)) or lt.COLUMN02=a.COLUMN13) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0     where  a.column11='" + internalid + "'  and a.columna13=0   group by lt.column04,lt.column07 union all select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, sum(cast(((isnull(a.COLUMN06 ,0)*isnull(a.COLUMN07 ,0))*(CAST(isnull(lt.column07,0)as decimal(18,2) )/100)) as decimal(18,2)))	 linetotaltax 	from   SATABLE016 a  left join SATABLE015 b on  b.COLUMN01=a.COLUMN11 left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN13,'-',''))) s)) or lt.COLUMN02=a.COLUMN13) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0  left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN16,'-',''))) s)) or ht.COLUMN02=b.COLUMN16) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0   where  a.column11='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	  and b.COLUMN16 not in(select isnull(COLUMN13,0) from SATABLE016   where  column11 in('" + internalid + "')) and a.columna13=0 group by lt.column04,lt.column07,ht.column04,ht.column07";
                var GDatat = dbs.Query(sqlt);
                ViewBag.taxitemsdata = GDatat;
                all1 = GETCUSTOMERDETAILS(CustomerId, opunit, CompanyID, all1);
                decimal total = 0; decimal qty = 0;
                if (dtg.Rows.Count > 0)
                {
                    total = Convert.ToDecimal(dtg.Rows[0][0]);
                    qty = Convert.ToDecimal(dtg.Rows[0][1]);
                }
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;

                AddressMaster obj = new AddressMaster();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                if (SalesOrderID != "")
                {
                    SqlCommand socmd = new SqlCommand("usp_SAL_TP_SO_Address", conn);
                    socmd.CommandType = CommandType.StoredProcedure;
                    socmd.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
                    SqlDataAdapter soda = new SqlDataAdapter(socmd);
                    DataTable sodt = new DataTable();
                    soda.Fill(sodt);

                    if (sodt.Rows.Count > 0)
                    {


                        if (sodt.Rows[0]["COLUMN06"].ToString() == "" || sodt.Rows[0]["COLUMN06"].ToString() == null)
                            all1[0].soAdresss = "";
                        else
                            all1[0].soAdresss = sodt.Rows[0]["COLUMN06"].ToString();

                        if (sodt.Rows[0]["COLUMN07"].ToString() == "" || sodt.Rows[0]["COLUMN07"].ToString() == null)
                            all1[0].soAdresss1 = "";
                        else
                            all1[0].soAdresss1 = sodt.Rows[0]["COLUMN07"].ToString();

                        if (sodt.Rows[0]["COLUMN08"].ToString() == "" || sodt.Rows[0]["COLUMN08"].ToString() == null)
                            all1[0].soAdresss2 = "";
                        else
                            all1[0].soAdresss2 = sodt.Rows[0]["COLUMN08"].ToString();

                        if (sodt.Rows[0]["COLUMN10"].ToString() == "" || sodt.Rows[0]["COLUMN10"].ToString() == null)
                            all1[0].socity = "";
                        else
                            all1[0].socity = sodt.Rows[0]["COLUMN10"].ToString();

                        if (sodt.Rows[0]["COLUMN11"].ToString() == "" || sodt.Rows[0]["COLUMN11"].ToString() == null)
                            all1[0].sostate = "";
                        else
                            all1[0].sostate = sodt.Rows[0]["COLUMN11"].ToString();

                        if (sodt.Rows[0]["COLUMN16"].ToString() == "" || sodt.Rows[0]["COLUMN16"].ToString() == null)
                            all1[0].socountry = "";
                        else
                            all1[0].socountry = sodt.Rows[0]["COLUMN16"].ToString();

                        if (sodt.Rows[0]["COLUMN12"].ToString() == "" || sodt.Rows[0]["COLUMN12"].ToString() == null)
                            all1[0].sozip = "";
                        else
                            all1[0].sozip = sodt.Rows[0]["COLUMN12"].ToString();

                    }
                }
                else if (SalesOrderID == "")
                {

                    SqlCommand socmd1 = new SqlCommand("usp_SAL_TP_C_Address", conn);
                    socmd1.CommandType = CommandType.StoredProcedure;
                    socmd1.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                    SqlDataAdapter soda1 = new SqlDataAdapter(socmd1);
                    DataTable sodt1 = new DataTable();
                    soda1.Fill(sodt1);

                    if (sodt1.Rows.Count > 0)
                    {


                        if (sodt1.Rows[0]["COLUMN06"].ToString() == "" || sodt1.Rows[0]["COLUMN06"].ToString() == null)
                            all1[0].soAdresss = "";
                        else
                            all1[0].soAdresss = sodt1.Rows[0]["COLUMN06"].ToString();

                        if (sodt1.Rows[0]["COLUMN07"].ToString() == "" || sodt1.Rows[0]["COLUMN07"].ToString() == null)
                            all1[0].soAdresss1 = "";
                        else
                            all1[0].soAdresss1 = sodt1.Rows[0]["COLUMN07"].ToString();

                        if (sodt1.Rows[0]["COLUMN08"].ToString() == "" || sodt1.Rows[0]["COLUMN08"].ToString() == null)
                            all1[0].soAdresss2 = "";
                        else
                            all1[0].soAdresss2 = sodt1.Rows[0]["COLUMN08"].ToString();

                        if (sodt1.Rows[0]["COLUMN10"].ToString() == "" || sodt1.Rows[0]["COLUMN10"].ToString() == null)
                            all1[0].socity = "";
                        else
                            all1[0].socity = sodt1.Rows[0]["COLUMN10"].ToString();

                        if (sodt1.Rows[0]["COLUMN11"].ToString() == "" || sodt1.Rows[0]["COLUMN11"].ToString() == null)
                            all1[0].sostate = "";
                        else
                            all1[0].sostate = sodt1.Rows[0]["COLUMN11"].ToString();

                        if (sodt1.Rows[0]["COLUMN16"].ToString() == "" || sodt1.Rows[0]["COLUMN16"].ToString() == null)
                            all1[0].socountry = "";
                        else
                            all1[0].socountry = sodt1.Rows[0]["COLUMN16"].ToString();

                        if (sodt1.Rows[0]["COLUMN12"].ToString() == "" || sodt1.Rows[0]["COLUMN12"].ToString() == null)
                            all1[0].sozip = "";
                        else
                            all1[0].sozip = sodt1.Rows[0]["COLUMN12"].ToString();

                    }
                }

                all1[0].Total = dt.Rows[0]["totalamt1"].ToString();
                all1[0].SubTotal = dtg.Rows[0]["subtotamt"].ToString();
                all1[0].TotalQuantity = qty.ToString();
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public ActionResult InvoicePrintGSTfly(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                int CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                all1[0].Signature = (dt.Rows[0]["signature"].ToString());
                if (Convert.ToString(dt.Rows[0]["signature"]) != "")
                    all1[0].Signature = "/Content/Upload/Images/Employee/Employee/" + (dt.Rows[0]["signature"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN75  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN75 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN75 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);
               
                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                //}

                ViewBag.pitemsdata = GData;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();
                all1 = GETCUSTOMERDETAILS(CustomerId, opunit, CompanyID, all1);
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
               
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();

                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }

                //all1 = GETCUSTOMERDETAILSO(CustomerId, all1);
               
                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);
               
                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else
                       
                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }
                
                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public ActionResult InvoicePrintMillePest(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                string[] param2 = dt.Rows[0]["COLUMN24"].ToString().Replace(".00", "").Split('.');
                string amountText1 = null;
                for (int i = 0; i < param2.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param2[i].ToString());
                    if (i == 0)
                        amountText1 = Rupees(NumVal);
                    else
                        amountText1 += " and" + Rupees(NumVal);
                }
                if (param2.Length == 1)
                    amountText1 = amountText1 + ' ' + "Rupees Only";
                else
                    amountText1 = amountText1 + ' ' + "Paise Only";
                all1[0].amtInWord1 = amountText1.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,,M3.COLUMN04 itemfamily  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN75 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13,,M3.COLUMN04";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN75 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN75 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(iif(lt.COLUMN16 IN (23582,23583,23584),lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0     where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN23,'-',''))) s)) or ht.COLUMN02=b.COLUMN23) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0    where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,lt.COLUMN16,ht.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                //"select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                var sqlt2 = "select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(f.lineamt) lineamt,f.SerItem as SerItem from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt,isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,it.COLUMN48 SerItem from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02='" + sono + "'  and isnull(a.COLUMNA13,0)=0 group by M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST";
                var GDataTP = dbs.Query(sqlt2);
                ViewBag.pitemsdata = GData;
                ViewBag.pitemsdatatax = GDataTP;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);

                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();


                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }


                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);

                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else

                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }

                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public ActionResult InvoicePrintRKM(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                var OPGSTIN ="";
                if (dt.Rows[0]["Date1"].ToString() != "")
                    OPGSTIN = Convert.ToDateTime(dt.Rows[0]["Date1"].ToString()).ToString(DateF);
                all1[0].OPGSTIN = OPGSTIN;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT ="";
                if (dt.Rows[0][8].ToString() != "")
                    dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                all1[0].dcno = (dt.Rows[0]["dcno"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["dispatchno"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                string[] param2 = dt.Rows[0]["COLUMN24"].ToString().Replace(".00", "").Split('.');
                string amountText1 = null;
                for (int i = 0; i < param2.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param2[i].ToString());
                    if (i == 0)
                        amountText1 = Rupees(NumVal);
                    else
                        amountText1 += " and" + Rupees(NumVal);
                }
                if (param2.Length == 1)
                    amountText1 = amountText1 + ' ' + "Rupees Only";
                else
                    amountText1 = amountText1 + ' ' + "Paise Only";
                all1[0].amtInWord1 = amountText1.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,,M3.COLUMN04 itemfamily  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN75 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13,,M3.COLUMN04";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN75 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN75 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(iif(lt.COLUMN16 IN (23582,23583,23584),lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0     where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN23,'-',''))) s)) or ht.COLUMN02=b.COLUMN23) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0    where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,lt.COLUMN16,ht.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                //"select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                //var sqlt2 = "select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(f.lineamt) lineamt,f.SerItem as SerItem from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt,isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,it.COLUMN48 SerItem from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02='" + sono + "'  and isnull(a.COLUMNA13,0)=0 group by M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST";
                //var GDataTP = dbs.Query(sqlt2);
                object[] sp1 = new object[2];
                sp1[0] = (sono);
                sp1[1] = ("HSNCODE");
                var db1 = Database.Open("sqlcon");
                var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);
                ViewBag.pitemsdata = GData;
                ViewBag.pitemsdatatax = GDataTP;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);

                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();


                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }


                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);

                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else

                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }

                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public ActionResult InvoicePrintGSTTC(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                //SqlCommand cmd = new SqlCommand("select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,S9.COLUMN10, S9.COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,ht.COLUMN07 as HTamnt,c.COLUMN04 as company  , c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN34 as TrackingNo,(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0)) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,f.COLUMN04 as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+S9.COLUMN24) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,s5.COLUMN04 as reference,S9.COLUMN41 TruckNo,S9.COLUMN46,s.COLUMN42 CustGSTIN,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,S9.COLUMN07 BillingAddress,S9.COLUMN67 RevCharge  from SATABLE009 S9 left join SATABLE002 s on s.column02=S9.COLUMN05 left join matable002 mot on mot.column02=S9.column33 left join matable002 ft on ft.column02=S9.column30 left join matable002 st on st.column02=s.column43 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND ISNULL(c.COLUMNA13,'FALSE')='FALSE' AND ISNULL(c.COLUMN05,'FALSE')='FALSE' left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 where (S9.column04='" + sono + "' OR S9.column02='" + sono + "') AND  ISNULL(S9.COLUMNA13,'FALSE')='FALSE'", cn);
                // SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //da.Fill(dt);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["dispatchno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["SalesQuotation"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);
                //var GData = dbs.Query(sql);
                //SqlDataAdapter dag = new SqlDataAdapter(taxQ1);
                //DataTable dtg = new DataTable();
                //dag.Fill(dtg);
                //decimal total = 0; decimal qty = 0;
                //if (dtg.Rows.Count > 0)
                //{
                //    total = Convert.ToDecimal(dtg.Rows[0][0]);
                //    qty = Convert.ToDecimal(dtg.Rows[0][1]);
                //}

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                //}

                ViewBag.pitemsdata = GData;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                //conn.Open();
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                //conn.Close();
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();

                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }

                all1 = GETCUSTOMERDETAILSO(CustomerId, all1);
                //conn.Open();
                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);
                //conn.Close();
                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else
                        //EMPHCS1286 INVOICE PRINT ADDRESS CHANGE By Srinivas 08/10/2015
                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }
                //all1[0].SubTotal = dtg.Rows[0]["subtotamt"].ToString();
                //all1[0].TotalQuantity = qty.ToString();
                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public dynamic GetTaxesInfo(string sqlt, string internalid, string AcOwner, [Optional] string Type)
        {
            try
            {
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmdt = new SqlCommand(sqlt, cn);
                DataTable dtt = new DataTable();
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                dat.Fill(dtt);

                SqlCommand cmds = new SqlCommand("GetServiceTaxes", cn);
                cmds.CommandType = CommandType.StoredProcedure;
                DataTable dts = new DataTable();
                cmds.Parameters.AddWithValue("@TransID", internalid);
                cmds.Parameters.AddWithValue("@AcOwner", AcOwner);
                cmds.Parameters.AddWithValue("@Type", Type);
                SqlDataAdapter das = new SqlDataAdapter(cmds);
                das.Fill(dts);
                if (dts.Rows.Count > 0)
                    dtt.Merge(dts, true);
                SqlCommand cmde = new SqlCommand("GetExciseTaxes", cn);
                cmde.CommandType = CommandType.StoredProcedure;
                DataTable dte = new DataTable();
                cmde.Parameters.AddWithValue("@TransID", internalid);
                cmde.Parameters.AddWithValue("@AcOwner", AcOwner);
                cmde.Parameters.AddWithValue("@Type", Type);
                SqlDataAdapter dae = new SqlDataAdapter(cmde);
                dae.Fill(dte);
                if (dte.Rows.Count > 0)
                    dtt.Merge(dte, true);
                var result = new List<dynamic>();
                foreach (DataRow row in dtt.Rows)
                {
                    var obj1 = (IDictionary<string, object>)new ExpandoObject();
                    foreach (DataColumn col in dte.Columns)
                    {
                        obj1.Add(col.ColumnName, row[col.ColumnName]);
                    }
                    result.Add(obj1);
                }
                return result;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult InvoiceRetailolive(string sono)
        {
            Session["PRINTID"] = sono;
            List<POPrint> all1 = new List<POPrint>();
            all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
            {
                Date = "",
                SONO = "",
                CVATNO = "",
                CCSTNO = "",
                CPANNO = "",
                CServiceTaxNO = "",
                CustomerAddress = "",
                Customer = "",
                Item = "",
                Quantity = "",
                Units = "",
                SerialNumbers = "",
                Description = "",
                Options = "",
                Rate = "",
                Amount = "",
                Total = "",
                TaxAmnt = "",
                TotalAmount = "",
                UOM = "",
                company = "",
                cAdresss = "",
                cAdresss1 = "",
                ccity = "",
                cstate = "",
                ccountry = "",
                czip = "",
                oAdresss = "",
                oAdresss1 = "",
                ocity = "",
                ostate = "",
                ocountry = "",
                ozip = "",
                soAdresss = "",
                soAdresss1 = "",
                socity = "",
                sostate = "",
                socountry = "",
                sozip = "",
                ouName = "",
                TDSAmount = ""
            });
            SqlConnection cn = new SqlConnection(sqlcon);
            SqlCommand cmd = new SqlCommand("select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,S9.COLUMN10, S9.COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,ht.COLUMN07 as HTamnt,c.COLUMN04 as company  , c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN34 as TrackingNo,(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0)) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,f.COLUMN04 as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN03 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+(iif(S9.COLUMN46=22713,0,S9.COLUMN24))+ISNULL(S9.COLUMN47,0)-ISNULL(S9.COLUMN38,0)-ISNULL(S9.COLUMN25,0)-ISNULL(S9.COLUMN56,0)+ISNULL(S9.COLUMN45,0)) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,isnull(s9.COLUMN45,0) as CESSAmount,s5.COLUMN04 as reference,S9.COLUMN46,c.COLUMND02 CGSTIN from SATABLE009 S9 left join SATABLE002 s on s.column02=S9.COLUMN05 left join matable002 mot on mot.column02=S9.column33 left join matable002 ft on ft.column02=S9.column30 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND ISNULL(c.COLUMNA13,'FALSE')='FALSE' AND ISNULL(c.COLUMN05,'FALSE')='FALSE' left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 where (S9.column04='" + sono + "' OR S9.column02='" + sono + "')  AND ISNULL(S9.COLUMNA13,'FALSE')='FALSE'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            string DateF = "dd/MM/yyyy"; 
            Session["idName"] = null;
            Session["idName"] = (dt.Rows[0][0].ToString());
            all1[0].SONO = (dt.Rows[0][0].ToString());
            all1[0].Customer = (dt.Rows[0][1].ToString());
            Session["customerIdName"] = (dt.Rows[0][1].ToString());
            var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
            all1[0].Date = DT;
            string internalid = (dt.Rows[0][3].ToString());
            Session["internalid"] = (dt.Rows[0][3].ToString());
            string SalesOrderID = dt.Rows[0][4].ToString();
            int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
            all1[0].Total = (dt.Rows[0][5].ToString());
            var a1 = 0;
            if (dt.Rows[0]["oppunit"].ToString() != "" && dt.Rows[0]["oppunit"].ToString() != null)
                a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
            Session["OPUnitM"] = a1;
            all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
            all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
            all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
            all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
            all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
            all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
            all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
            all1[0].footerText = dt.Rows[0]["footerText"].ToString();
            all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
            all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
            all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
            all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
            all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
            all1[0].Reference = (dt.Rows[0]["reference"].ToString());
            all1[0].form = (dt.Rows[0]["form"].ToString());
            all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
            all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
            all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
            all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
            all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
            all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
            all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
            all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
            all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
            all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
            all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
            all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
            all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
            all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
            all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
            all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
            all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
            var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
            Session["customerId"] = cId;
            all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
            all1[0].logoName = (dt.Rows[0][15].ToString());
            Session["logoName"] = (dt.Rows[0][15].ToString());
            all1[0].SOrderID = (dt.Rows[0][4].ToString());
            all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
            all1[0].PaymentMode = (dt.Rows[0][7].ToString());
            var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
            all1[0].DueDate = dDT;
            all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
            all1[0].TotalAmount = (dt.Rows[0][10].ToString());
            var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
            int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
            all1[0].company = (dt.Rows[0]["company"].ToString());
            all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
            all1[0].discount = (dt.Rows[0]["discount"].ToString());
            all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
            all1[0].CESSAmount = (dt.Rows[0]["CESSAmount"].ToString());
            all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
            all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
            all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
            all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
            string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
            string amountText = null;
            for (int i = 0; i < param1.Length; i++)
            {
                Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                if (i == 0)
                    amountText = Rupees(NumVal);
                else
                    amountText += " and" + Rupees(NumVal);
            }
            if (param1.Length == 1)
                amountText = amountText + ' ' + "Rupees Only";
            else
                amountText = amountText + ' ' + "Paise Only";
            all1[0].amtInWord = amountText.ToString();
            var dbs = Database.Open("sqlcon");
            var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
            SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
            DataTable dg = new DataTable();
            dag1.Fill(dg);
            string AcOwner = Session["AcOwner"].ToString(); var sql = "";
            var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
            SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
            DataTable dg2 = new DataTable();
            dag2.Fill(dg2);
            if (dg.Rows.Count > 0)
            {
                string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                if (columnA03 == AcOwner)
                {
                    sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,m32.COLUMN04 HSN from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21  inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = a.COLUMNA03 AND ISNULL(M7.COLUMN13,0)=0left join matable032 m32 on m32.COLUMN02 = M7.COLUMN75  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13,m32.COLUMN04";
                }
                Session["Lot"] = "";
            }
            else if (dg2.Rows.Count > 0)
            {
                string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                if (columnA03 == AcOwner && LotNo == "Y")
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02=a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE  COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,m.COLUMN51 MRP,isnull(a.COLUMN13,0) PTR,m32.COLUMN04 HSN from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0  LEFT JOIN MATABLE007 M7 ON M7.COLUMN02 = a.COLUMN05 AND M7.COLUMNA03 = a.COLUMNA03 AND ISNULL(M7.COLUMN13,0)=0 left join matable032 m32 on m32.COLUMN02 = M7.COLUMN75  where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                }
            }
            else
            {
                sql = "select it.column04 item,it.column06 upc,isnull(CAST(a.COLUMN10 AS DECIMAL(18,0)),0) quantity,a.COLUMN22 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,CONVERT(varchar, CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) AS money), 1) lineamt,isnull(a.COLUMN19,0) Discount,sum(isnull(CAST(t.COLUMN07 AS DECIMAL(18,2)),0)) val,CONVERT(varchar,CAST(cast((isnull(a.COLUMN10 ,0)*(iif((pl.COLUMN05=22831 or b.COLUMN46=22713),isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))))-(isnull(CAST(IIF(ISNULL(a.COLUMN19,'')='','0',a.COLUMN19) AS DECIMAL(18,2)),0))as decimal(18,2)) AS money), 1)  amt,u.COLUMN04 UOM ,a.COLUMN06 Description,sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,CAST(t.COLUMN07 AS DECIMAL(18,2)),0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * ((sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100))as decimal(18,2)) as Tamnt  ,(isnull(ht.COLUMN07,0)) as HTamnt,(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))* (ht.COLUMN07)/100) as decimal(18,2))) as lTamnt , (select column04 from MATABLE002 where column02=ht.COLUMN16) names,((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (sum(CAST(isnull(t.COLUMN07,0) as decimal(18,2))))/100)as decimal(18,2))+cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0)) * (cast(isnull(ht.COLUMN07,0)as decimal(18,2))/100)) as decimal(18,2) )) as LHtaxAmnt,M33.COLUMN04 SACCode,M32.COLUMN04 HSNCode,it.COLUMN48 SerItem,isnull(a.COLUMN13,0) Price,a.COLUMN02,cast(sum(isnull(iif(t.COLUMN16 in (23582,23583,23584),t.COLUMN07,0),0)) as int) GSTRate,CAST(isnull(pl.COLUMN06,0) AS DECIMAL(18,0)) PLDiscount from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON M33.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  a.column15=1000045971  and isnull(a.COLUMNA13,0)=0 group by it.column04,it.column06,a.COLUMN22,a.COLUMN19,a.COLUMN13,a.COLUMN04,a.COLUMN05,u.COLUMN04,M33.COLUMN04,M32.COLUMN04,it.COLUMN48,ht.COLUMN16,a.COLUMN10,a.COLUMN35,ht.COLUMN07,a.COLUMN02,a.COLUMN06,pl.COLUMN05,b.COLUMN46,a.COLUMNA03,pl.COLUMN06";
                Session["Lot"] = "";
            }
            SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where a.column15='" + internalid + "'  and isnull(a.COLUMNA13,0)=0)p", cn);
            //var GData = dbs.Query(sql);
            SqlDataAdapter dag = new SqlDataAdapter(taxQ1);
            DataTable dtg = new DataTable();
            dag.Fill(dtg);
            decimal total = 0; decimal qty = 0;
            if (dtg.Rows.Count > 0)
            {
                total = Convert.ToDecimal(dtg.Rows[0][0]);
                qty = Convert.ToDecimal(dtg.Rows[0][1]);
            }
            var sqlt = "select isnull(p.linetaxes,'')linetaxes,isnull(p.linetaxamt,0)linetaxamt,sum(isnull(p.linetotaltax,0))linetotaltax,p.headertaxamt,p.headertaxes from(select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,null headertaxes,null headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	 and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23)p group by p.linetaxes,p.linetaxamt,p.headertaxamt,p.headertaxes ";
            var GDataX = dbs.Query(sqlt);
            GDataX = GetTaxesInfo(sqlt, internalid, AcOwner);
            //ViewBag.pitemsdata = GData;
            ViewBag.taxitemsdata = GDataX;
            object[] sp = new object[2];
            sp[0] = (sono);
            sp[1] = ("LINE");
            var db = Database.Open("sqlcon");
            var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);
            SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
            cmdt.CommandType = CommandType.StoredProcedure;
            cmdt.Parameters.AddWithValue("@SONO", sono);
            cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
            cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
            SqlDataAdapter dat = new SqlDataAdapter(cmdt);
            DataTable dtt = new DataTable();
            dat.Fill(dtt);
            if (dtt.Rows.Count > 0)
            {
                all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
            }
            object[] sp1 = new object[2];
            sp1[0] = (sono);
            sp1[1] = ("HSNCODE");
            var db1 = Database.Open("sqlcon");
            var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);
            ViewBag.pitemsdata = GData;
            ViewBag.pitemsdatatax = GDataTP;
            object[] sp2 = new object[2];
            sp2[0] = (sono);
            sp2[1] = ("LineTax");
            var db2 = Database.Open("sqlcon");
            var GDatat = db2.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp2);
            ViewBag.taxitemsdata = GDatat;
            //ViewBag.taxitemsdata = GDataX;
            //ViewBag.taxitemsTT = GDataTT;
            var cAdresss = "";
            var cAdresss1 = "";
            var ccity = "";
            var cstate = "";
            var ccountry = "";
            int czip;
            var oAdresss = "";
            var oAdresss1 = "";
            var ocity = "";
            var ostate = "";
            var ocountry = "";
            int ozip;
            var soAdresss = "";
            var soAdresss1 = "";
            var socity = "";
            var sostate = "";
            var socountry = "";
            int sozip;
            AddressMaster obj = new AddressMaster();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
            Ccmd.CommandType = CommandType.StoredProcedure;
            Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
            SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
            DataTable dtA = new DataTable();
            cda.Fill(dtA);
            //conn.Close();
            if (dtA.Rows.Count > 0)
            {
                if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                    all1[0].cAdresss = "";
                else
                    all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                    all1[0].cAdresss1 = "";
                else
                    all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                    all1[0].ccity = "";
                else
                    all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                    all1[0].cstate = "";
                else
                    all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                    all1[0].ccountry = "";
                else
                    all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                    all1[0].czip = "";
                else
                    all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

            }
            if (SalesOrderID != "")
            {
                SqlCommand socmd = new SqlCommand("usp_SAL_TP_SO_Address", conn);
                socmd.CommandType = CommandType.StoredProcedure;
                socmd.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
                SqlDataAdapter soda = new SqlDataAdapter(socmd);
                DataTable sodt = new DataTable();
                soda.Fill(sodt);
                //conn.Close();
                if (sodt.Rows.Count > 0)
                {


                    if (sodt.Rows[0]["COLUMN06"].ToString() == "" || sodt.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].soAdresss = "";
                    else
                        all1[0].soAdresss = sodt.Rows[0]["COLUMN06"].ToString();

                    if (sodt.Rows[0]["COLUMN07"].ToString() == "" || sodt.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].soAdresss1 = "";
                    else
                        all1[0].soAdresss1 = sodt.Rows[0]["COLUMN07"].ToString();

                    if (sodt.Rows[0]["COLUMN08"].ToString() == "" || sodt.Rows[0]["COLUMN08"].ToString() == null)
                        all1[0].soAdresss2 = "";
                    else
                        all1[0].soAdresss2 = sodt.Rows[0]["COLUMN08"].ToString();

                    if (sodt.Rows[0]["COLUMN10"].ToString() == "" || sodt.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].socity = "";
                    else
                        all1[0].socity = sodt.Rows[0]["COLUMN10"].ToString();

                    if (sodt.Rows[0]["COLUMN11"].ToString() == "" || sodt.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].sostate = "";
                    else
                        all1[0].sostate = sodt.Rows[0]["COLUMN11"].ToString();

                    if (sodt.Rows[0]["COLUMN16"].ToString() == "" || sodt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].socountry = "";
                    else
                        all1[0].socountry = sodt.Rows[0]["COLUMN16"].ToString();

                    if (sodt.Rows[0]["COLUMN12"].ToString() == "" || sodt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].sozip = "";
                    else
                        all1[0].sozip = sodt.Rows[0]["COLUMN12"].ToString();

                }
            }
            else if (SalesOrderID == "")
            {
                //conn.Open();
                SqlCommand socmd1 = new SqlCommand("usp_SAL_TP_C_Address", conn);
                socmd1.CommandType = CommandType.StoredProcedure;
                socmd1.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter soda1 = new SqlDataAdapter(socmd1);
                DataTable sodt1 = new DataTable();
                soda1.Fill(sodt1);
                //conn.Close();
                if (sodt1.Rows.Count > 0)
                {


                    if (sodt1.Rows[0]["COLUMN06"].ToString() == "" || sodt1.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].soAdresss = "";
                    else
                        all1[0].soAdresss = sodt1.Rows[0]["COLUMN06"].ToString();

                    if (sodt1.Rows[0]["COLUMN07"].ToString() == "" || sodt1.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].soAdresss1 = "";
                    else
                        all1[0].soAdresss1 = sodt1.Rows[0]["COLUMN07"].ToString();

                    if (sodt1.Rows[0]["COLUMN08"].ToString() == "" || sodt1.Rows[0]["COLUMN08"].ToString() == null)
                        all1[0].soAdresss2 = "";
                    else
                        all1[0].soAdresss2 = sodt1.Rows[0]["COLUMN08"].ToString();

                    if (sodt1.Rows[0]["COLUMN10"].ToString() == "" || sodt1.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].socity = "";
                    else
                        all1[0].socity = sodt1.Rows[0]["COLUMN10"].ToString();

                    if (sodt1.Rows[0]["COLUMN11"].ToString() == "" || sodt1.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].sostate = "";
                    else
                        all1[0].sostate = sodt1.Rows[0]["COLUMN11"].ToString();

                    if (sodt1.Rows[0]["COLUMN16"].ToString() == "" || sodt1.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].socountry = "";
                    else
                        all1[0].socountry = sodt1.Rows[0]["COLUMN16"].ToString();

                    if (sodt1.Rows[0]["COLUMN12"].ToString() == "" || sodt1.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].sozip = "";
                    else
                        all1[0].sozip = sodt1.Rows[0]["COLUMN12"].ToString();

                }
            }

            //conn.Open();
            SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
            ocmd.CommandType = CommandType.StoredProcedure;
            ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
            SqlDataAdapter oda = new SqlDataAdapter(ocmd);
            DataTable odt = new DataTable();
            oda.Fill(odt);
            //conn.Close();
            if (odt.Rows.Count > 0)
            {
                if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                    all1[0].oAdresss = "";
                else
                    all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                    all1[0].oAdresss1 = "";
                else
                    all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                    all1[0].oAdresss2 = "";
                else
                    all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                    all1[0].ocity = "";
                else
                    all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                    all1[0].ostate = "";
                else
                    all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                    all1[0].ocountry = "";
                else
                    all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                    all1[0].ozip = "";
                else
                    all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                    all1[0].douPhno = "";
                else
                    all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();
            }
            all1[0].SubTotal = dtg.Rows[0]["subtotamt"].ToString();
            all1[0].TotalQuantity = qty.ToString();
            all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
            GETRoundOffVal(sono, 1277, all1);

            return View(all1);
        }
        public ActionResult InvoicePrintGSTmainac(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                //SqlCommand cmd = new SqlCommand("select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,S9.COLUMN10, S9.COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,ht.COLUMN07 as HTamnt,c.COLUMN04 as company  , c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN34 as TrackingNo,(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0)) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,f.COLUMN04 as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+S9.COLUMN24) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,s5.COLUMN04 as reference,S9.COLUMN41 TruckNo,S9.COLUMN46,s.COLUMN42 CustGSTIN,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,S9.COLUMN07 BillingAddress,S9.COLUMN67 RevCharge  from SATABLE009 S9 left join SATABLE002 s on s.column02=S9.COLUMN05 left join matable002 mot on mot.column02=S9.column33 left join matable002 ft on ft.column02=S9.column30 left join matable002 st on st.column02=s.column43 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND ISNULL(c.COLUMNA13,'FALSE')='FALSE' AND ISNULL(c.COLUMN05,'FALSE')='FALSE' left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 where (S9.column04='" + sono + "' OR S9.column02='" + sono + "') AND  ISNULL(S9.COLUMNA13,'FALSE')='FALSE'", cn);
                // SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //da.Fill(dt);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["companyaddress"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["dispatchno"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);
                //var GData = dbs.Query(sql);
                //SqlDataAdapter dag = new SqlDataAdapter(taxQ1);
                //DataTable dtg = new DataTable();
                //dag.Fill(dtg);
                //decimal total = 0; decimal qty = 0;
                //if (dtg.Rows.Count > 0)
                //{
                //    total = Convert.ToDecimal(dtg.Rows[0][0]);
                //    qty = Convert.ToDecimal(dtg.Rows[0][1]);
                //}

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                ViewBag.pitemsdata = GData;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                object[] sp1 = new object[2];
                sp1[0] = (sono);
                sp1[1] = ("LineTax");
                var dbs1 = Database.Open("sqlcon");
                var GDataXY = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);
                ViewBag.taxitemsdatas = GDataXY;
                object[] sp2 = new object[2];
                sp2[0] = (sono);
                sp2[1] = ("HSNCODE");
                var db1 = Database.Open("sqlcon");
                var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp2);
                ViewBag.pitemsdatatax = GDataTP;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();

                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }

                all1 = GETCUSTOMERDETAILSO(CustomerId, all1);
                
                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);
                
                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else
                       
                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }
                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public ActionResult InvoicePrintGSTRaghu(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                //SqlCommand cmd = new SqlCommand("select S9.COLUMN04,s.COLUMN05 COLUMN05,S9.COLUMN08,S9.COLUMN01,S9.COLUMN06,S9.COLUMN20,m.COLUMN04,pm.COLUMN04  mode ,S9.COLUMN10, S9.COLUMN24,CONVERT(varchar,CAST(s9.COLUMN22 AS money), 1),S9.COLUMN14,S9.COLUMN05 cust,ht.COLUMN07 as HTamnt,c.COLUMN04 as company  , c.COLUMN28 as logoName,S9.COLUMN14 as oppunit ,S9.COLUMN34 as TrackingNo,(isnull(S9.COLUMN25,0)+isnull(S9.COLUMN56,0)) as Discount,S9.COLUMN35 as PackingCharges,S9.COLUMN36 as ShippingCharges,f.COLUMN04 as form, S9.COLUMN05 as cId,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip, CONVERT(varchar,CAST(S9.COLUMN20 AS money), 1) totalamt1,s.COLUMN26 cpanno ,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,s.COLUMN29 ctaxno,s.COLUMN11 custPhNo,s9.COLUMN20 amt ,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,opu.COLUMN36 as footerText,opu.COLUMN04 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,c.COLUMN35 as tin,opu.COLUMN34 as OPUPhNo,CONVERT(varchar,CAST(S9.COLUMN25 AS money), 1) discount,S9.COLUMN38 TDSAmount,(S9.COLUMN20-S9.COLUMN38) BalanceDue , S9.COLUMNA03 ACOUNTOWNER ,(S9.COLUMN22+S9.COLUMN24) TotalnonTDS,isnull(s12.COLUMN06,0) as paid,s5.COLUMN04 as reference,S9.COLUMN41 TruckNo,S9.COLUMN46,s.COLUMN42 CustGSTIN,(M17.COLUMN06+'-'+M17.COLUMN05) StateCode,S9.COLUMN07 BillingAddress,S9.COLUMN67 RevCharge  from SATABLE009 S9 left join SATABLE002 s on s.column02=S9.COLUMN05 left join matable002 mot on mot.column02=S9.column33 left join matable002 ft on ft.column02=S9.column30 left join matable002 st on st.column02=s.column43 left join CONTABLE008 c on c.COLUMNA03=s9.COLUMNA03 AND ISNULL(c.COLUMNA13,'FALSE')='FALSE' AND ISNULL(c.COLUMN05,'FALSE')='FALSE' left join CONTABLE007 opu on opu.COLUMN02=s9.COLUMN14 left join matable002 f on f.column02=S9.column31    left join MATABLE022 m on m.column02=S9.COLUMN11  left join matable002 pm on pm.column02=S9.COLUMN21    left join MATABLE013 ht on ht.column02=S9.COLUMN23 left outer join SATABLE012 s12 on s12.COLUMN03=s9.COLUMN02 left outer join SATABLE005 s5 on s5.COLUMN02=S9.COLUMN06 left JOIN MATABLE017 M17  ON M17.COLUMN02 = s.COLUMN43 where (S9.column04='" + sono + "' OR S9.column02='" + sono + "') AND  ISNULL(S9.COLUMNA13,'FALSE')='FALSE'", cn);
                // SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //da.Fill(dt);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["SalesRep"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                all1[0].DLNO = (dt.Rows[0]["DLNO"].ToString());
                all1[0].STNO = (dt.Rows[0]["STNO"].ToString());
                all1[0].companymail = (dt.Rows[0]["companymail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);
                //var GData = dbs.Query(sql);
                //SqlDataAdapter dag = new SqlDataAdapter(taxQ1);
                //DataTable dtg = new DataTable();
                //dag.Fill(dtg);
                //decimal total = 0; decimal qty = 0;
                //if (dtg.Rows.Count > 0)
                //{
                //    total = Convert.ToDecimal(dtg.Rows[0][0]);
                //    qty = Convert.ToDecimal(dtg.Rows[0][1]);
                //}

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                object[] sp1 = new object[2];
                sp1[0] = (sono);
                sp1[1] = ("HSNCODE");
                var db1 = Database.Open("sqlcon");
                var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);

                object[] sp2 = new object[2];
                sp2[0] = (sono);
                sp2[1] = ("GROUPTAX");
                var db2 = Database.Open("sqlcon");
                var GDataTG = db2.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp2);


                ViewBag.gpitemsdatatax = GDataTG;

                ViewBag.pitemsdatatax = GDataTP;
                ViewBag.pitemsdata = GData;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                //conn.Open();
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                //conn.Close();
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();

                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }

                all1 = GETCUSTOMERDETAILSO(CustomerId, all1);
                //conn.Open();
                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);
                //conn.Close();
                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else
                        //EMPHCS1286 INVOICE PRINT ADDRESS CHANGE By Srinivas 08/10/2015
                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }
                //all1[0].SubTotal = dtg.Rows[0]["subtotamt"].ToString();
                //all1[0].TotalQuantity = qty.ToString();
                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);
                if (Convert.ToString(Session["AcOwner"]) == "56832" || Convert.ToString(Session["AcOwner"]) == "56578")
                    return View("InvoicePrintGSTLios", all1);
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
		
		public ActionResult InvoicePrintGSTNSE(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["Discount1"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                string[] param2 = dt.Rows[0]["COLUMN24"].ToString().Replace(".00", "").Split('.');
                string amountText1 = null;
                for (int i = 0; i < param2.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param2[i].ToString());
                    if (i == 0)
                        amountText1 = Rupees(NumVal);
                    else
                        amountText1 += " and" + Rupees(NumVal);
                }
                if (param2.Length == 1)
                    amountText1 = amountText1 + ' ' + "Rupees Only";
                else
                    amountText1 = amountText1 + ' ' + "Paise Only";
                all1[0].amtInWord1 = amountText1.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,,M3.COLUMN04 itemfamily  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13,,M3.COLUMN04";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                    all1[0].SUBTOT1 = dtt.Rows[0]["subtotamt1"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(iif(lt.COLUMN16 IN (23582,23583,23584),lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0     where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN23,'-',''))) s)) or ht.COLUMN02=b.COLUMN23) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0    where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,lt.COLUMN16,ht.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                //"select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                //var sqlt2 = "select sum(f.CGST) CGST,sum(f.SGST) SGST,sum(f.IGST) IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(f.lineamt) lineamt,f.SerItem as SerItem from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt,isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,it.COLUMN48 SerItem from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0where  b.column02='" + sono + "'  and isnull(a.COLUMNA13,0)=0 group by M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.SACCode,f.HSNCode,f.SerItem";
                //var GDataTP = dbs.Query(sqlt2);
                object[] sp1 = new object[2];
                sp1[0] = (sono);
                sp1[1] = ("HSNCODE");
                var db1 = Database.Open("sqlcon");
                var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);
                ViewBag.pitemsdata = GData;
                ViewBag.pitemsdatatax = GDataTP;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);

                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();


                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }


                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);

                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else

                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }

                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);
                if (Convert.ToString(Session["AcOwner"]) == "56860")
                    return View("InvoicePrintGSTSPCA", all1);
                else if (Convert.ToString(Session["AcOwner"]) == "56731" || Convert.ToString(Session["AcOwner"]) == "56732" || Convert.ToString(Session["AcOwner"]) == "56788" || Convert.ToString(Session["AcOwner"]) == "56807" || Convert.ToString(Session["AcOwner"]) == "56816")
                    return View("InvoicePrintGSTMVIK", all1);

                else if (Convert.ToString(Session["AcOwner"]) == "56925")
                {
                    return View("InvoicePrintGSTGPI", all1);
                }
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
		
        public ActionResult BillofSupplyGST(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["Tax"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());
                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                all1[0].dcno = (dt.Rows[0]["dcno"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["dispatchno"].ToString());
                int CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"].ToString());
                all1[0].compGST = (dt.Rows[0]["compGST"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                string[] param1 = dt.Rows[0]["Tax"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                string[] param2 = dt.Rows[0]["COLUMN24"].ToString().Replace(".00", "").Split('.');
                string amountText1 = null;
                for (int i = 0; i < param2.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param2[i].ToString());
                    if (i == 0)
                        amountText1 = Rupees(NumVal);
                    else
                        amountText1 += " and" + Rupees(NumVal);
                }
                if (param2.Length == 1)
                    amountText1 = amountText1 + ' ' + "Rupees Only";
                else
                    amountText1 = amountText1 + ' ' + "Paise Only";
                all1[0].amtInWord1 = amountText1.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,,M3.COLUMN04 itemfamily  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13,,M3.COLUMN04";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(iif(lt.COLUMN16 IN (23582,23583,23584),lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0     where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN23,'-',''))) s)) or ht.COLUMN02=b.COLUMN23) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0    where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,lt.COLUMN16,ht.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                //"select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                //var sqlt2 = "select CAST(f.CGST as decimal(18,0)) CGST,CAST(f.SGST as decimal(18,0)) SGST,CAST(f.IGST as decimal(18,0)) IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,sum(f.lineamt) lineamt from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0 left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02='" + sono + "'  and isnull(a.COLUMNA13,0)=0 group by a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.CGST ,f.SGST,f.IGST";
                //select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(f.lineamt) lineamt,f.SerItem as SerItem from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt,isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,it.COLUMN48 SerItem from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02='" + sono + "'  and isnull(a.COLUMNA13,0)=0 group by M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST";

                //var GDataTP = dbs.Query(sqlt2);
                object[] sp1 = new object[2];
                sp1[0] = (sono);
                sp1[1] = ("HSNCODE");
                var db1 = Database.Open("sqlcon");
                var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);
                ViewBag.pitemsdata = GData;
                ViewBag.pitemsdatatax = GDataTP;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                all1 = GETCUSTOMERDETAILS(CustomerId, opunit, CompanyID, all1);
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);

                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();


                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }


                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);

                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else

                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }

                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public List<POPrint> GETRoundOffVal(string TransId, int FormId, List<POPrint> all1)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                SqlCommand cmd = new SqlCommand("USP_GET_TransRoundOffVal", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TransId", TransId));
                cmd.Parameters.Add(new SqlParameter("@FormId", FormId));
                cmd.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmd.Parameters.Add(new SqlParameter("@OPUNIT", Session["OPUnit1"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtA = new DataTable();
                da.Fill(dtA);
                all1[0].RoundOff = "0";
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["RoundOff"].ToString() != "" && dtA.Rows[0]["RoundOff"].ToString() != null)
                        all1[0].RoundOff = dtA.Rows[0]["RoundOff"].ToString();
                }
                return all1;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }
        public ActionResult CreditMemoGSTRKVJPrint(string sono)
        {
            try
            {
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    SONO = "",
                    Reference = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",

                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    RevCharge = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("select S5.COLUMN04,s.COLUMN05 COLUMN05,S5.COLUMN06,S5.COLUMN01,S5.COLUMN02,S5.COLUMN15,S5.COLUMN05 cust,S5.COLUMN24 oper ,(isnull(S5.COLUMN14,0)+isnull(S5.COLUMN60,0)) as Discount,S5.COLUMN05 as cId,(select COLUMN07 from MATABLE013 WHERE	 COLUMN02= S5.COLUMN31) as HTamnt,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,c.COLUMN04 as company,	 c.COLUMN28 as logoName, S5.COLUMN24 as oppunit, S5.COLUMN41 as PackingCharges,S5.COLUMN40 as TrackingNo, S5.COLUMN42 as ShippingCharges, S5.COLUMN11 as ref,f.COLUMN04 as form, m.COLUMN04 as PaymentTerms,mot.COLUMN04 as ModeOfTrans,ft.COLUMN04 as FreightTerm,cast(S5.COLUMN08 as date) duedate,c.COLUMN27 vatno,c.COLUMN29 cstno,  c.COLUMN31 panno,S5.COLUMN15 totalamt,CONVERT(varchar,CAST(S5.COLUMN15 AS money), 1) totalamt1,pm.COLUMN04 Paymentmode  ,s.COLUMN26 cpanno ,s.COLUMN21 custTin,s.COLUMN27 cvatno,s.COLUMN28 ccstno,s.COLUMN29 ctaxno,S5.COLUMN50,S5.COLUMN67 CustGSTIN,(M18.COLUMN06+'-'+M18.COLUMN05) StateCode,S5.COLUMN70 RevCharge,(M17.COLUMN06+'-'+M17.COLUMN05) PlaceofSupply,c.COLUMN02 CompanyID,S5.COLUMN09 memo,ISNULL(S5.COLUMN12,0) SubTotalvalue,c.COLUMN14 compZip,S5.COLUMN71 RoundOff from SATABLE005 S5 left join matable002 pm on pm.column02=s5.COLUMN30  left join MATABLE022 m on m.column02=s5.column10 left join matable002 f on f.column02=s5.column37   left join matable002 mot on mot.column02=s5.column39 left join matable002 ft on ft.column02=s5.column36   left join SATABLE002 s on s.column02=S5.COLUMN05	left join matable002 st on st.column02=s.column43 left join CONTABLE008 c on c.COLUMNA03=s5.COLUMNA03 and isnull(c.COLUMNA13,0)=0 LEFT JOIN MATABLE017  M17  ON M17.COLUMN02 = S5.COLUMN69 left JOIN MATABLE017 M18 ON M18.COLUMN02 = s.COLUMN43  where S5.column04='" + sono + "' OR S5.column02='" + sono + "'", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                string SalesOrderID = Convert.ToString(dt.Rows[0][4].ToString());
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0]["oper"].ToString());

                Session["OPUnitM"] = opunit;
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;

                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0]["logoName"]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0]["logoName"].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0]["logoName"]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0]["logoName"].ToString());
                Session["logoName"] = (dt.Rows[0]["logoName"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0]["RoundOff"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());


                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["SubTotalvalue"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());

                all1[0].amttype = (dt.Rows[0]["COLUMN50"].ToString());

                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].PaymentMode = (dt.Rows[0]["Paymentmode"].ToString());
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].Reference = (dt.Rows[0]["ref"].ToString());
                all1[0].VATNO = (dt.Rows[0]["vatno"].ToString());
                all1[0].CSTNO = (dt.Rows[0]["cstno"].ToString());
                all1[0].PANNO = (dt.Rows[0]["panno"].ToString());
                all1[0].Reference = (dt.Rows[0]["ref"].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].PlaceofSupply = (dt.Rows[0]["PlaceofSupply"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                int CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                string[] param1 = dt.Rows[0]["totalamt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = 0;
                    if (param1[i].ToString() != "")
                        NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();

                var dbs = Database.Open("sqlcon");
                var sql2 = "select COLUMN02,COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010824 and COLUMN05='COLUMN17' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);

                var sql = "";
                if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == Session["AcOwner"].ToString() && LotNo == "Y")

                        sql = "select f.item,SUM(f.quantity)quantity,CONVERT(varchar, CAST((f.rate)AS money), 1)rate,CONVERT(varchar, CAST(SUM(lineamt) AS money), 1) lineamt,SUM(f.Discount) Discount,SUM(f.Val)Val,CONVERT(varchar, CAST(SUM(f.amt) AS money), 1)  amt ,f.UOM ,(f.CGST)CGST,(f.SGST)SGST,(f.IGST)IGST,CAST(SUM(f.lineamt) * ((f.CGST) / 100) AS DECIMAL(18, 2))CGSTAMT,CAST(SUM(f.lineamt) * ((f.SGST) / 100) AS DECIMAL(18, 2))SGSTAMT,CAST(SUM(f.lineamt) * ((f.IGST) / 100) AS DECIMAL(18, 2))IGSTAMT,CAST(SUM(f.lineamt) * (SUM(f.VAL) / 100) AS DECIMAL(18, 2))Tamnt,SUM(f.HTamnt)HTamnt,SUM(f.lTamnt)lTamnt,f.names,SUM(f.LHtaxAmnt) LHtaxAmnt,f.SACCode,f.HSNCode,f.SerItem FROM (SELECT m4.column04 item,(isnull(a.COLUMN07,0)) quantity,CONVERT(varchar, CAST(iif(pl.COLUMN05 = 22831, isnull(a.COLUMN32, isnull(a.COLUMN09, 0)), isnull(a.COLUMN09, 0)) AS money), 1) rate,SUM(CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2))) val,(select column04 from MATABLE002 where column02 = th.COLUMN16) names,cast((a.COLUMN25 * (iif(h.COLUMN50 = 22713, SUM((CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / (SUM((CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) + 100), SUM((CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / 100))) as decimal(18, 2)) as Tamnt,u.COLUMN04 UOM,(cast(isnull(th.COLUMN07, 0) as decimal(18, 2))) as HTamnt,cast((a.COLUMN25 * (iif(h.COLUMN46 = 22713, cast(th.COLUMN07 as decimal(18, 2)) / ((cast(th.COLUMN07 as decimal(18, 2))) + 100), (cast(th.COLUMN07 as decimal(18, 2))) / 100))) as decimal(18, 2)) as lTamnt,(a.COLUMN25 + cast((a.COLUMN25 * (iif(h.COLUMN50 = 22713, (SUM(CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) /((SUM(CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) + 100), (SUM(CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / 100)) + cast((a.COLUMN25 * (iif(h.COLUMN46 = 22713, cast(th.COLUMN07 as decimal(18, 2)) / ((cast(th.COLUMN07 as decimal(18, 2))) + 100), (cast(th.COLUMN07 as decimal(18, 2))) / 100))) as decimal(18, 2))) as decimal(18, 2))) as LHtaxAmnt,(CAST(iif(pl.COLUMN05 = 22831, a.COLUMN25, cast(isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0) - ISNULL(a.COLUMN10, 0) as decimal(18, 2))) AS money)) amt,CAST((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) as decimal(18, 2)) lineamt,M32.COLUMN04 HSNCode, M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,SUM(iif(t1.COLUMN16 = 23582, CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2)), 0)) CGST, SUM(iif(t1.COLUMN16 = 23583, CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2)), 0)) SGST, SUM(iif(t1.COLUMN16 = 23584, CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2)), 0)) IGST, cast(((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) * (SUM(CAST((isnull(iif(t1.COLUMN16 = 23582, t1.COLUMN07, 0), 0)) as decimal(18, 2))) / 100)) as decimal(18, 2)) CGSTAMT,cast(((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) * (SUM(CAST((isnull(iif(t1.COLUMN16 = 23582, t1.COLUMN07, 0), 0)) as decimal(18, 2))) / 100)) as decimal(18, 2)) SGSTAMT,cast(((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) * (SUM(CAST((isnull(iif(t1.COLUMN16 = 23582, t1.COLUMN07, 0), 0)) as decimal(18, 2))) / 100)) as decimal(18, 2)) IGSTAMT,a.COLUMN10 Discount from SATABLE006 a LEFT join SATABLE005 h on h.column01 = a.COLUMN19 AND h.COLUMNA03 = a.COLUMNA03  and h.COLUMNA13 = 0 left join MATABLE013 t on t.column02 = a.COLUMN26 AND t.COLUMNA03 = a.COLUMNA03 and t.COLUMNA13 = 0 left join matable007 m on m.column02 = a.COLUMN05 and m.COLUMNA03 = a.COLUMNA03 and isnull(m.columna13,0)= 0 left join matable007 m7 on m7.column02 = a.COLUMN03 AND a.COLUMNA03 = m7.COLUMNA03 and isnull(m7.columna13,0)= 0 left join MATABLE013 th on th.column02 = h.COLUMN31 AND h.COLUMNA03 = th.COLUMNA03 and isnull(th.COLUMNA13,0)= 0 left join MATABLE023 pl on pl.COLUMN02 = a.COLUMN37 and pl.COLUMNA03 = a.COLUMNA03 and isnull(pl.COLUMNA13,0)= 0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON M33.COLUMN02 = m7.COLUMN76 left join MATABLE002 u on u.column02 = a.COLUMN27 and(u.COLUMNA03 = a.COLUMNA03 or isnull(u.COLUMNA03, 0) = 0) and isnull(u.COLUMNA13,0)= 0 left JOIN MATABLE013 t1 on(t1.COLUMN02 in ((SELECT ListValue FROM dbo.FN_ListToTable(',', (select COLUMN05  from MATABLE014 where COLUMNA03 = a.COLUMNA03 and COLUMN02 = replace(a.COLUMN26, '-', ''))) s)) or t1.COLUMN02 = a.COLUMN26) and t1.COLUMNA03 = a.COLUMNA03 and isnull(t1.COLUMNA13,0)= 0 LEFT join MATABLE003 m4 on m4.COLUMN02 = m7.COLUMN12 and m4.COLUMNA03 = m7.COLUMNA03 and isnull(m4.COLUMNA13,0)= 0 where a.column19 = '" + internalid + "' and ISNULL(a.columna13,0) = 0 GROUP BY a.COLUMN02,a.COLUMN03,a.COLUMN07,a.COLUMN16,a.COLUMN15,a.COLUMN05,pl.COLUMN05,a.COLUMN09,a.COLUMN32,th.COLUMN07,th.COLUMN16,a.column25,h.column50,a.column27,a.column26,h.column31,h.column46,a.column11,M32.COLUMN04,M33.COLUMN04,m7.COLUMN48,a.COLUMN10,m4.column04,u.COLUMN04 ) f group by f.item,f.rate,f.UOM,f.SGST,f.IGST,f.CGST,f.SACCode,f.HSNCode,f.SerItem,f.names order by f.item";
                            //"select f.item,SUM(f.quantity)quantity,CONVERT(varchar, CAST((f.rate) AS money), 1)rate,CONVERT(varchar, CAST(SUM(lineamt) AS money), 1) lineamt,SUM(f.Discount) Discount,SUM(f.Val)Val,CONVERT(varchar, CAST(SUM(f.amt) AS money) , 1)  amt ,f.UOM ,(f.CGST) CGST,(f.SGST) SGST,(f.IGST) IGST,CAST(SUM(f.lineamt)*((f.CGST)/100) AS DECIMAL(18,2))CGSTAMT,CAST(SUM(f.lineamt)*((f.SGST)/100) AS DECIMAL(18,2))SGSTAMT,CAST(SUM(f.lineamt)*((f.IGST)/100) AS DECIMAL(18,2))IGSTAMT,CAST(SUM(f.lineamt)*(SUM(f.VAL)/100) AS DECIMAL(18,2))Tamnt,SUM(f.HTamnt)HTamnt,SUM(f.lTamnt)lTamnt,f.names,SUM(f.LHtaxAmnt) LHtaxAmnt,f.SACCode,f.HSNCode,f.SerItem from (select m4.column04 item,a.COLUMN07 quantity,CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN32,isnull(a.COLUMN09,0)),isnull(a.COLUMN09,0)) AS money), 1) rate,SUM(CAST((isnull(t.COLUMN07,0))as decimal(18,2))) val,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((a.COLUMN25 * (iif(h.COLUMN50=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN27) UOM ,(select COLUMN07 from MATABLE013  WHERE COLUMN02= a.COLUMN26 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN26 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN31) as HTamnt,cast((a.COLUMN25  * (iif(h.COLUMN46=22713,cast(th.COLUMN07 as decimal(18,2))/((cast(th.COLUMN07 as decimal(18,2)))+100),(cast(th.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt,(select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE    COLUMN02=h.COLUMN31)) names, (a.COLUMN25+cast((a.COLUMN25 * (iif(h.COLUMN50=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))+cast((a.COLUMN25 * (iif(h.COLUMN46=22713,cast(th.COLUMN07 as decimal(18,2))/((cast(th.COLUMN07 as decimal(18,2)))+100),(cast(th.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2))) as decimal(18,2))) as LHtaxAmnt,(CAST(iif(pl.COLUMN05=22831,a.COLUMN25,cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)-ISNULL(a.COLUMN10,0) as decimal(18,2))) AS money)) amt,CAST( (isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0)) as decimal(18,2)) lineamt,M32.COLUMN04 HSNCode,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,sum(isnull(iif(t1.COLUMN16=23582,t1.COLUMN07,0),0)) CGST,sum(isnull(iif(t1.COLUMN16=23583,t1.COLUMN07,0),0)) SGST,sum(isnull(iif(t1.COLUMN16=23584,t1.COLUMN07,0),0)) IGST, cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN09 ,0))*((CAST(sum(isnull(iif(t1.COLUMN16=23582,t1.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT,cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN09 ,0))*((CAST(sum(isnull(iif(t1.COLUMN16=23583,t1.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT,cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN09 ,0))*((CAST(sum(isnull(iif(t1.COLUMN16=23584,t1.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT,a.COLUMN10 Discount from  SATABLE006 a left join SATABLE005 h on h.column01=a.COLUMN19 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN26 and t.COLUMNA13=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join matable007 m7 on m7.column02=a.COLUMN03 and isnull(m7.columna13,0)=0 left join MATABLE013 th on th.column02=h.COLUMN31 and th.COLUMNA13=0  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0  LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76 LEFT JOIN MATABLE013 t1 on (t1.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and COLUMN02=replace(a.COLUMN26,'-',''))) s)) or t1.COLUMN02=a.COLUMN26) and t1.COLUMNA03=a.COLUMNA03 and isnull(t1.COLUMNA13,0)=0 left join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 and m4.COLUMNA03=m7.COLUMNA03 and isnull(m4.COLUMNA13,0)=0 where  a.column19='" + internalid + "' and a.columna13=0  GROUP BY a.COLUMN03,a.COLUMN07,a.COLUMN16,a.COLUMN15,a.COLUMN05,pl.COLUMN05,a.COLUMN09,a.COLUMN32,th.COLUMN07,t.COLUMN07,t.COLUMN16,a.column25,h.column50,a.column27,a.column26,h.column31,h.column46,a.column11,M32.COLUMN04,M33.COLUMN04,m7.COLUMN48,a.COLUMN10,m4.column04) f group by f.item,f.rate,f.UOM,f.SGST,f.IGST,f.CGST,f.SACCode,f.HSNCode,f.SerItem,f.names order by f.item";
                }
                else
                {

                    sql = "select f.item,SUM(f.quantity)quantity,CONVERT(varchar, CAST((f.rate)AS money), 1)rate,CONVERT(varchar, CAST(SUM(lineamt) AS money), 1) lineamt,SUM(f.Discount) Discount,SUM(f.Val)Val,CONVERT(varchar, CAST(SUM(f.amt) AS money), 1)  amt ,f.UOM ,(f.CGST)CGST,(f.SGST)SGST,(f.IGST)IGST,CAST(SUM(f.lineamt) * ((f.CGST) / 100) AS DECIMAL(18, 2))CGSTAMT,CAST(SUM(f.lineamt) * ((f.SGST) / 100) AS DECIMAL(18, 2))SGSTAMT,CAST(SUM(f.lineamt) * ((f.IGST) / 100) AS DECIMAL(18, 2))IGSTAMT,CAST(SUM(f.lineamt) * (SUM(f.VAL) / 100) AS DECIMAL(18, 2))Tamnt,SUM(f.HTamnt)HTamnt,SUM(f.lTamnt)lTamnt,f.names,SUM(f.LHtaxAmnt) LHtaxAmnt,f.SACCode,f.HSNCode,f.SerItem FROM(SELECT m4.column04 item, (isnull(a.COLUMN07,0)) quantity,CONVERT(varchar, CAST(iif(pl.COLUMN05 = 22831, isnull(a.COLUMN32, isnull(a.COLUMN09, 0)), isnull(a.COLUMN09, 0)) AS money), 1) rate,SUM(CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2))) val,(select column04 from MATABLE002 where column02 = th.COLUMN16) names,cast((a.COLUMN25 * (iif(h.COLUMN50 = 22713, SUM((CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / (SUM((CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) + 100), SUM((CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / 100))) as decimal(18, 2)) as Tamnt,u.COLUMN04 UOM,(cast(isnull(th.COLUMN07, 0) as decimal(18, 2))) as HTamnt,cast((a.COLUMN25 * (iif(h.COLUMN46 = 22713, cast(th.COLUMN07 as decimal(18, 2)) / ((cast(th.COLUMN07 as decimal(18, 2))) + 100), (cast(th.COLUMN07 as decimal(18, 2))) / 100))) as decimal(18, 2)) as lTamnt,(a.COLUMN25 + cast((a.COLUMN25 * (iif(h.COLUMN50 = 22713, (SUM(CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / ((SUM(CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) + 100), (SUM(CAST(isnull(t1.COLUMN07, 0) as decimal(18, 2)))) / 100)) + cast((a.COLUMN25 * (iif(h.COLUMN46 = 22713, cast(th.COLUMN07 as decimal(18, 2)) / ((cast(th.COLUMN07 as decimal(18, 2))) + 100), (cast(th.COLUMN07 as decimal(18, 2))) / 100))) as decimal(18, 2))) as decimal(18, 2))) as LHtaxAmnt,(CAST(iif(pl.COLUMN05 = 22831, a.COLUMN25, cast(isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0) - ISNULL(a.COLUMN10, 0) as decimal(18, 2))) AS money)) amt,CAST((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) as decimal(18, 2)) lineamt,M32.COLUMN04 HSNCode, M33.COLUMN04 SACCode, m7.COLUMN48 SerItem, SUM(iif(t1.COLUMN16 = 23582, CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2)), 0)) CGST, SUM(iif(t1.COLUMN16 = 23583, CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2)), 0)) SGST, SUM(iif(t1.COLUMN16 = 23584, CAST((isnull(t1.COLUMN07, 0)) as decimal(18, 2)), 0)) IGST, cast(((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) * (SUM(CAST((isnull(iif(t1.COLUMN16 = 23582, t1.COLUMN07, 0), 0)) as decimal(18, 2))) / 100)) as decimal(18, 2)) CGSTAMT,cast(((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) * (SUM(CAST((isnull(iif(t1.COLUMN16 = 23582, t1.COLUMN07, 0), 0)) as decimal(18, 2))) / 100)) as decimal(18, 2)) SGSTAMT,cast(((isnull(a.COLUMN07, 0) * isnull(a.COLUMN09, 0)) * (SUM(CAST((isnull(iif(t1.COLUMN16 = 23582, t1.COLUMN07, 0), 0)) as decimal(18, 2))) / 100)) as decimal(18, 2)) IGSTAMT,a.COLUMN10 Discount from SATABLE006 a LEFT join SATABLE005 h on h.column01 = a.COLUMN19 AND h.COLUMNA03 = a.COLUMNA03  and h.COLUMNA13 = 0 left join MATABLE013 t on t.column02 = a.COLUMN26 AND t.COLUMNA03 = a.COLUMNA03 and t.COLUMNA13 = 0 left join matable007 m on m.column02 = a.COLUMN05 and m.COLUMNA03 = a.COLUMNA03 and isnull(m.columna13,0)= 0 left join matable007 m7 on m7.column02 = a.COLUMN03 AND a.COLUMNA03 = m7.COLUMNA03 and isnull(m7.columna13,0)= 0 left join MATABLE013 th on th.column02 = h.COLUMN31 AND h.COLUMNA03 = th.COLUMNA03 and isnull(th.COLUMNA13,0)= 0 left join MATABLE023 pl on pl.COLUMN02 = a.COLUMN37 and pl.COLUMNA03 = a.COLUMNA03 and isnull(pl.COLUMNA13,0)= 0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 = m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON M33.COLUMN02 = m7.COLUMN76 left join MATABLE002 u on u.column02 = a.COLUMN27 and(u.COLUMNA03 = a.COLUMNA03 or isnull(u.COLUMNA03, 0) = 0) and isnull(u.COLUMNA13,0)= 0 left JOIN MATABLE013 t1 on(t1.COLUMN02 in ((SELECT ListValue FROM dbo.FN_ListToTable(',', (select COLUMN05  from MATABLE014 where COLUMNA03 = a.COLUMNA03 and COLUMN02 = replace(a.COLUMN26, '-', ''))) s)) or t1.COLUMN02 = a.COLUMN26) and t1.COLUMNA03 = a.COLUMNA03 and isnull(t1.COLUMNA13,0)= 0 LEFT join MATABLE003 m4 on m4.COLUMN02 = m7.COLUMN12 and m4.COLUMNA03 = m7.COLUMNA03 and isnull(m4.COLUMNA13,0)= 0 where a.column19 = '" + internalid + "' and ISNULL(a.columna13,0) = 0 GROUP BY a.COLUMN02,a.COLUMN03,a.COLUMN07,a.COLUMN16,a.COLUMN15,a.COLUMN05,pl.COLUMN05,a.COLUMN09,a.COLUMN32,th.COLUMN07,th.COLUMN16,a.column25,h.column50,a.column27,a.column26,h.column31,h.column46,a.column11,M32.COLUMN04,M33.COLUMN04,m7.COLUMN48,a.COLUMN10,m4.column04,u.COLUMN04 ) f group by f.item,f.rate,f.UOM,f.SGST,f.IGST,f.CGST,f.SACCode,f.HSNCode,f.SerItem,f.names order by f.item";
                    Session["Lot"] = "";
                }
                var GData = dbs.Query(sql);
                ViewBag.pitemsdata = GData;

                SqlCommand cmdg = new SqlCommand("select sum(isnull(p.amt,0)) amt,sum(isnull(p.qty,0)) qty,CONVERT(varchar,CAST(sum(isnull(p.subtotamt,0)) AS money), 1) subtotamt from(select (isnull(a.COLUMN25,0)) amt,(isnull(a.COLUMN07,0)) qty,iif(pl.COLUMN05=22831,a.COLUMN25,cast(isnull(a.COLUMN07,0)*isnull(a.COLUMN09,0) as decimal(18,2))) subtotamt,a.COLUMN10 Discount from  SATABLE006 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where a.column19='" + internalid + "' and isnull(a.columna13,0)=0) p", cn);
                SqlDataAdapter dag = new SqlDataAdapter(cmdg);
                DataTable dtg = new DataTable();
                dag.Fill(dtg);
                SqlCommand cmdGST = new SqlCommand("select sum(f.cgtttax)cgtttax,sum(f.sgtttax)sgtttax,sum(f.igtttax)igtttax,sum(f.subtotamt)subtotamt,sum(subtotamt1) subtotamt1,sum(f.totaltax)totaltax,sum(f.amt)amt from(select cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(iif(t1.COLUMN16=23582,t1.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) cgtttax,cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(iif(t1.COLUMN16=23583,t1.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) sgtttax, cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST(sum(isnull(iif(t1.COLUMN16=23584,t1.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) igtttax,(CAST(isnull(a.COLUMN07,0)*(iif((h.COLUMN50=22713),isnull(a.COLUMN32,0),isnull(a.COLUMN09,0))) as decimal(18,2))) subtotamt,cast((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN09 ,0)) as decimal(18,2)) subtotamt1,cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*(sum(CAST((isnull(t.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2)) totaltax,cast(((isnull(a.COLUMN07 ,0)*(iif((pl.COLUMN05=22831 or h.COLUMN50=22713),isnull(a.COLUMN32,isnull(a.COLUMN09,0)),isnull(a.COLUMN09,0))))-((CAST(IIF(ISNULL(a.COLUMN10,0)=0,'0',a.COLUMN10) AS DECIMAL(18,2))))) as decimal(18,2)) amt from  SATABLE006 a left join SATABLE005 h on h.column01=a.COLUMN19 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN26 and t.COLUMNA13=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join matable007 m7 on m7.column02=a.COLUMN03 and isnull(m7.columna13,0)=0 left join MATABLE013 th on th.column02=h.COLUMN31 and th.COLUMNA13=0  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0  LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76 LEFT JOIN MATABLE013 t1 on (t1.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and COLUMN02=replace(a.COLUMN26,'-',''))) s)) or t1.COLUMN02=a.COLUMN26) and t1.COLUMNA03=a.COLUMNA03 and isnull(t1.COLUMNA13,0)=0 left join MATABLE002 M2A ON M2A.column02=t1.COLUMN16 where  a.column19='" + internalid + "' and a.columna13=0 GROUP BY a.COLUMN02,a.COLUMN07,pl.COLUMN05,a.COLUMN09,a.COLUMN32,h.column50,a.column27,a.COLUMN10,m7.COLUMN04) f", cn);
                SqlDataAdapter daGST = new SqlDataAdapter(cmdGST);
                DataTable dtGST = new DataTable();
                daGST.Fill(dtGST);
                if (dtGST.Rows.Count > 0)
                {

                    all1[0].SUBTOT = dtGST.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtGST.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtGST.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtGST.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtGST.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtGST.Rows[0]["amt"].ToString();

                }

                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN07 ,0)*isnull(a.COLUMN32 ,0))*((CAST((isnull(lt.COLUMN07,0)) as decimal(18,2)))/100)) as decimal(18,2))) linetotaltax from   SATABLE006 a  left join SATABLE005 b on  b.COLUMN01=a.COLUMN19 left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN26,'-',''))) s)) or lt.COLUMN02=a.COLUMN26) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0    where  a.column19='" + internalid + "'  and a.columna13=0   group by lt.column04,lt.column07,a.COLUMN26,b.COLUMN31,b.column50    union all      select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, 	cast((cast((max(b.COLUMN12)-max(b.COLUMN41)-max(b.COLUMN42) )as decimal(18,2))*(iif(b.COLUMN50=22713,cast(ht.COLUMN07 as decimal(18,2))/((cast(ht.COLUMN07 as decimal(18,2)))+100),(cast(ht.COLUMN07 as decimal(18,2)))/100)))as decimal(18,2)) linetotaltax from   SATABLE006 a  left join SATABLE005 b on  b.COLUMN01=a.COLUMN19 left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMNA03=a.COLUMNA03 and isnull(COLUMNA13,0)=0 and COLUMN02=replace(a.COLUMN26,'-',''))) s)) or lt.COLUMN02=a.COLUMN26) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0  left join MATABLE013 ht on ht.COLUMN02=b.COLUMN31  where  a.column19='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	  and b.COLUMN31 not in(select isnull(COLUMN26,0) from SATABLE006   where  column19 in('" + internalid + "')) and a.columna13=0 group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN26, b.COLUMN31,b.column50 ";
                var GDatat = dbs.Query(sqlt);
                ViewBag.taxitemsdata = GDatat;
                decimal total = 0; decimal qty = 0;
                if (dtg.Rows.Count > 0)
                {
                    total = Convert.ToDecimal(dtg.Rows[0][0]);
                    qty = Convert.ToDecimal(dtg.Rows[0][1]);
                }
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;

                AddressMaster obj = new AddressMaster();
                all1 = GETCUSTOMERDETAILS(CustomerId, opunit, CompanyID, all1);
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                //conn.Open();
                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);
                //conn.Close();
                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();
                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();
                }

                if (SalesOrderID != "")
                {
                    SqlCommand socmd = new SqlCommand("usp_SAL_TP_SO_Address", conn);
                    socmd.CommandType = CommandType.StoredProcedure;
                    socmd.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
                    SqlDataAdapter soda = new SqlDataAdapter(socmd);
                    DataTable sodt = new DataTable();
                    soda.Fill(sodt);
                    //conn.Close();
                    if (sodt.Rows.Count > 0)
                    {


                        if (sodt.Rows[0]["COLUMN06"].ToString() == "" || sodt.Rows[0]["COLUMN06"].ToString() == null)
                            all1[0].soAdresss = "";
                        else
                            all1[0].soAdresss = sodt.Rows[0]["COLUMN06"].ToString();

                        if (sodt.Rows[0]["COLUMN07"].ToString() == "" || sodt.Rows[0]["COLUMN07"].ToString() == null)
                            all1[0].soAdresss1 = "";
                        else
                            all1[0].soAdresss1 = sodt.Rows[0]["COLUMN07"].ToString();

                        if (sodt.Rows[0]["COLUMN10"].ToString() == "" || sodt.Rows[0]["COLUMN10"].ToString() == null)
                            all1[0].socity = "";
                        else
                            all1[0].socity = sodt.Rows[0]["COLUMN10"].ToString();

                        if (sodt.Rows[0]["COLUMN11"].ToString() == "" || sodt.Rows[0]["COLUMN11"].ToString() == null)
                            all1[0].sostate = "";
                        else
                            all1[0].sostate = sodt.Rows[0]["COLUMN11"].ToString();

                        if (sodt.Rows[0]["COLUMN16"].ToString() == "" || sodt.Rows[0]["COLUMN16"].ToString() == null)
                            all1[0].socountry = "";
                        else
                            all1[0].socountry = sodt.Rows[0]["COLUMN16"].ToString();

                        if (sodt.Rows[0]["COLUMN12"].ToString() == "" || sodt.Rows[0]["COLUMN12"].ToString() == null)
                            all1[0].sozip = "";
                        else
                            all1[0].sozip = sodt.Rows[0]["COLUMN12"].ToString();

                    }
                }
                //conn.Open();
                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", opunit));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);
                //conn.Close();
                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();
                }

                all1[0].Total = dt.Rows[0]["totalamt1"].ToString();
                all1[0].SubTotal = dtg.Rows[0]["subtotamt"].ToString();
                all1[0].TotalQuantity = qty.ToString();
                if (Convert.ToString(Session["AcOwner"]) == "56837" || Convert.ToString(Session["AcOwner"]) == "56836" || Convert.ToString(Session["AcOwner"]) == "56571")
                    return View("CreditMemoGSTSDE", all1);
                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
        public ActionResult InvoicePrintGstSahasra(string sono)
        {
            try
            {
                Session["PRINTID"] = sono;
                string AcOwner001 = Session["AcOwner"].ToString(); 
                List<POPrint> all1 = new List<POPrint>();
                all1.Add(new MeBizSuiteAppUI.Controllers.POPrint
                {
                    Date = "",
                    SONO = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    Customer = "",
                    Item = "",
                    Quantity = "",
                    Units = "",
                    SerialNumbers = "",
                    Description = "",
                    Options = "",
                    Rate = "",
                    Amount = "",
                    Total = "",
                    TaxAmnt = "",
                    TotalAmount = "",
                    UOM = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    soAdresss = "",
                    soAdresss1 = "",
                    socity = "",
                    sostate = "",
                    socountry = "",
                    sozip = "",
                    ouName = "",
                    TDSAmount = "",
                    cDrugLIC = "",
                    opDrugLIC = "",
                    MRP = "",
                    PTR = "",
                    VehicleNo = "",
                    CustGSTIN = "",
                    GSTTYPE= "",

                    HSNCode = "",
                    ItemTotal = "",
                    ItemDiscount = "",
                    StateCode = "",
                    SACCode = "",
                    SHAdresss = "",
                    SHAdresss1 = "",
                    SHcity = "",
                    SHstate = "",
                    SHcountry = "",
                    SHzip = ""
                });
                SqlConnection cn = new SqlConnection(sqlcon);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SONO", sono);
                cmd.Parameters.AddWithValue("@TYPE", "HEADER");
                cmd.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string DateF = Session["DateFormat"].ToString();
                Session["idName"] = null;
                Session["idName"] = (dt.Rows[0][0].ToString());
                all1[0].SONO = (dt.Rows[0][0].ToString());
                all1[0].Customer = (dt.Rows[0][1].ToString());
                Session["customerIdName"] = (dt.Rows[0][1].ToString());
                var DT = Convert.ToDateTime(dt.Rows[0][2].ToString()).ToString(DateF);
                all1[0].Date = DT;
                string internalid = (dt.Rows[0][3].ToString());
                Session["internalid"] = (dt.Rows[0][3].ToString());
                string SalesOrderID = dt.Rows[0][4].ToString();
                int CustomerId = Convert.ToInt32(dt.Rows[0]["cust"].ToString());
                all1[0].Total = (dt.Rows[0][5].ToString());
                var a1 = Convert.ToInt32(dt.Rows[0]["oppunit"].ToString());
                Session["OPUnitM"] = a1;
                all1[0].VATNO = dt.Rows[0]["VATNO"].ToString();
                all1[0].CSTNO = dt.Rows[0]["CSTNO"].ToString();
                all1[0].PANNO = dt.Rows[0]["PANNO"].ToString();
                all1[0].OPUVATNO = dt.Rows[0]["OPUVATNO"].ToString();
                all1[0].OPUCSTNO = dt.Rows[0]["OPUCSTNO"].ToString();
                all1[0].OPUPANNO = dt.Rows[0]["OPUPANNO"].ToString();
                all1[0].OPUPhNo = dt.Rows[0]["OPUPhNo"].ToString();
                all1[0].footerText = dt.Rows[0]["footerText"].ToString();
                all1[0].CPANNO = (dt.Rows[0]["cpanno"].ToString());
                all1[0].CVATNO = (dt.Rows[0]["cvatno"].ToString());
                all1[0].CCSTNO = (dt.Rows[0]["ccstno"].ToString());
                all1[0].CServiceTaxNO = (dt.Rows[0]["ctaxno"].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].Reference = (dt.Rows[0]["reference"].ToString());
                all1[0].form = (dt.Rows[0]["form"].ToString());
                all1[0].PackingCharges = (dt.Rows[0]["PackingCharges"].ToString());
                all1[0].ShippingCharges = (dt.Rows[0]["ShippingCharges"].ToString());
                all1[0].Discount = (dt.Rows[0]["Discount"].ToString());
                all1[0].TrackingNo = (dt.Rows[0]["TrackingNo"].ToString());
                all1[0].StateLincense = (dt.Rows[0]["StateLincense"].ToString());
                all1[0].CntralLincese = (dt.Rows[0]["CntralLincese"].ToString());
                all1[0].custTin = (dt.Rows[0]["custTin"].ToString());

              

                all1[0].amttype = (dt.Rows[0]["COLUMN46"].ToString());
                all1[0].ModeOfTrans = (dt.Rows[0]["ModeOfTrans"].ToString());
                all1[0].FreightTerm = (dt.Rows[0]["FreightTerm"].ToString());
                all1[0].ouName = (dt.Rows[0]["ouName"].ToString());
                all1[0].compA1 = (dt.Rows[0]["compA1"].ToString());
                all1[0].compA2 = (dt.Rows[0]["compA2"].ToString());
                all1[0].compCity = (dt.Rows[0]["compCity"].ToString());
                all1[0].compZip = (dt.Rows[0]["compZip"].ToString());
                all1[0].cTIn = (dt.Rows[0]["tin"].ToString());  
                all1[0].custPhNo = (dt.Rows[0]["custPhNo"].ToString());
                all1[0].CustGSTIN = (dt.Rows[0]["CustGSTIN"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["SalesRep"].ToString());
                all1[0].GSTTYPE = (dt.Rows[0]["GSTTYPE"].ToString());

                all1[0].StateCode = (dt.Rows[0]["StateCode"].ToString());
                all1[0].BillingAddress = (dt.Rows[0]["BillingAddress"].ToString());
                all1[0].RevCharge = (dt.Rows[0]["RevCharge"].ToString());
                all1[0].CGSTIN = (dt.Rows[0]["CGSTIN"].ToString());            
                all1[0].compCtry = (dt.Rows[0]["compCtry"].ToString());
                all1[0].compState = (dt.Rows[0]["compState"].ToString());
                all1[0].Custmail = (dt.Rows[0]["Custmail"].ToString());
                var cId = Convert.ToInt32(dt.Rows[0]["cId"].ToString());
                Session["customerId"] = cId;
                all1[0].HTamnt = (dt.Rows[0]["HTamnt"].ToString());
                if (Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = "/Content/Upload/" + (dt.Rows[0][15].ToString());
                else
                    all1[0].logoName = "";
                var formName = Request["FormName"];
                if (formName != null && Convert.ToString(dt.Rows[0][15]) != "")
                    all1[0].logoName = Server.MapPath("/Content/Upload/" + dt.Rows[0][15].ToString());
                Session["logoName"] = (dt.Rows[0][15].ToString());
                all1[0].VehicleNo = (dt.Rows[0]["TruckNo"].ToString());
                all1[0].SOrderID = (dt.Rows[0][4].ToString());
                all1[0].PaymentTerms = (dt.Rows[0][6].ToString());
                all1[0].PaymentMode = (dt.Rows[0][7].ToString());
                var dDT = Convert.ToDateTime(dt.Rows[0][8].ToString()).ToString(DateF);
                all1[0].DueDate = dDT;
                all1[0].TaxAmnt = (dt.Rows[0][9].ToString());
                all1[0].TotalAmount = (dt.Rows[0][10].ToString());
                var tamnt = Convert.ToDecimal(dt.Rows[0][11].ToString());
                int opunit = Convert.ToInt32(dt.Rows[0][12].ToString());
                all1[0].company = (dt.Rows[0]["company"].ToString());
                all1[0].discount = (dt.Rows[0]["discount"].ToString());
                all1[0].TDSAmount = (dt.Rows[0]["TDSAmount"].ToString());
                all1[0].BalanceDue = (dt.Rows[0]["BalanceDue"].ToString());
                all1[0].TotalnonTDS = (dt.Rows[0]["TotalnonTDS"].ToString());
                all1[0].DueAmount = (Convert.ToDouble(dt.Rows[0]["TotalnonTDS"]) - Convert.ToDouble(dt.Rows[0]["paid"])).ToString();
                all1[0].AmountPaid = (dt.Rows[0]["paid"].ToString());
                all1[0].memo = (dt.Rows[0]["memo"].ToString());
                all1[0].SalesRep = (dt.Rows[0]["dispatchno"].ToString());
                string[] param1 = dt.Rows[0]["amt"].ToString().Replace(".00", "").Split('.');
                string amountText = null;
                for (int i = 0; i < param1.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param1[i].ToString());
                    if (i == 0)
                        amountText = Rupees(NumVal);
                    else
                        amountText += " and" + Rupees(NumVal);
                }
                if (param1.Length == 1)
                    amountText = amountText + ' ' + "Rupees Only";
                else
                    amountText = amountText + ' ' + "Paise Only";
                all1[0].amtInWord = amountText.ToString();
                string[] param2 = dt.Rows[0]["COLUMN24"].ToString().Replace(".00", "").Split('.');
                string amountText1 = null;
                for (int i = 0; i < param2.Length; i++)
                {
                    Int64 NumVal = Convert.ToInt64(param2[i].ToString());
                    if (i == 0)
                        amountText1 = Rupees(NumVal);
                    else
                        amountText1 += " and" + Rupees(NumVal);
                }
                if (param2.Length == 1)
                    amountText1 = amountText1 + ' ' + "Rupees Only";
                else
                    amountText1 = amountText1 + ' ' + "Paise Only";
                all1[0].amtInWord1 = amountText1.ToString();
                var dbs = Database.Open("sqlcon");
                var sql1 = "select COLUMN02,COLUMN03,COLUMNA03 from CONTABLE029 where COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag1 = new SqlDataAdapter(sql1, cn);
                DataTable dg = new DataTable();
                dag1.Fill(dg);
                string AcOwner = Session["AcOwner"].ToString(); var sql = "";
                var sql2 = "select COLUMN05,COLUMN07,COLUMNA03 from SETABLE012 where COLUMN04=110010828 and COLUMN05='COLUMN31' and COLUMN07='Y' and  COLUMNA03='" + Session["AcOwner"] + "'";
                SqlDataAdapter dag2 = new SqlDataAdapter(sql2, cn);
                DataTable dg2 = new DataTable();
                dag2.Fill(dg2);
                if (dg.Rows.Count > 0)
                {
                    string columnA03 = dg.Rows[0]["COLUMNA03"].ToString();
                    if (columnA03 == AcOwner)
                    {

                        sql = "select m4.column04 item,sum(a.COLUMN10) quantity,'NOs' units,'' serial,'' 'desc', m4.column04 upc,CONVERT(varchar,CAST((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0))) AS money), 1) rate,m13.column07 val,'' name ,cast((sum(a.COLUMN14) * ( max(m13.COLUMN07))/100) as decimal(18,2)) as Tamnt,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*((iif(isnull(a.COLUMN35,0)>0,isnull(a.COLUMN35,0),isnull(a.COLUMN13,0)))) AS money), 1)  amt,'NOs' UOM ,m131.COLUMN07 cVAT,m132.COLUMN07 cCST ,m133.COLUMN07 as HTamnt ,cast((sum(a.COLUMN14)  * ((m133.COLUMN07))/100) as decimal(18,0)) as lTamnt ,'' names,(sum(a.COLUMN14)+sum(a.COLUMN23)+cast((sum(a.COLUMN14) * ((m133.COLUMN07))/100) as decimal(18,0) )) as LHtaxAmnt,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m7.COLUMN48 SerItem,,M3.COLUMN04 itemfamily  from SATABLE010 a inner join MATABLE007 m7 on m7.COLUMN02=a.COLUMN05 inner join MATABLE003 m4 on m4.COLUMN02=m7.COLUMN12 left join MATABLE013 m13 on m13.COLUMN02=a.COLUMN21 left join MATABLE013 m131 on m131.COLUMN02=a.COLUMN21  left join MATABLE013 m132 on m132.COLUMN02=a.COLUMN21 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and  b.COLUMN01='" + internalid + "' left join MATABLE013 m133 on m133.COLUMN02=b.COLUMN23 and  b.COLUMN01='" + internalid + "' LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m7.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m7.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by m4.column04,m13.column07,m131.COLUMN07,m132.COLUMN07,m133.COLUMN07,a.COLUMN13,,M3.COLUMN04";
                    }
                    Session["Lot"] = "";
                }

                else if (dg2.Rows.Count > 0)
                {
                    string columnA03 = dg2.Rows[0]["COLUMNA03"].ToString();
                    string LotNo = dg2.Rows[0]["COLUMN07"].ToString();
                    Session["Lot"] = dg2.Rows[0]["COLUMN07"].ToString();
                    if (columnA03 == AcOwner && LotNo == "Y")
                    {
                        sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16) name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt  ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02= h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,(select COLUMN04 from FITABLE043 where COLUMN02=a.COLUMN31) Lot,(select FORMAT(column05,'" + DateF + "') from FITABLE043 where COLUMN02=a.COLUMN31) Expiry,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,isnull(a.COLUMN13,0) PTR,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0  left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    }
                }
                else
                {
                    sql = "select (select column04 from matable007 where COLUMN02=a.COLUMN05) item,(select column06 from matable007 where COLUMN02=a.COLUMN05) upc,a.COLUMN10 quantity,a.COLUMN07 units,a.COLUMN04 serial,a.COLUMN06 'desc',CONVERT(varchar,CAST(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0)) AS money), 1) rate,t.COLUMN07 val,t.COLUMN04 tax,a.COLUMN23 taxamt,(select column04 from MATABLE002 where column02=t.COLUMN16)  name,cast((cast(a.COLUMN14 as decimal(18,2)) * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as Tamnt ,CONVERT(varchar,CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) AS money), 1)  amt,(select COLUMN04 from MATABLE002 WHERE COLUMN02= a.COLUMN22) UOM ,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22399) cVAT, (select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=22400) cCST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23582) CGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23583) SGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02= a.COLUMN21 AND COLUMN16=23584) IGST,(select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as HTamnt ,cast((a.COLUMN14  * (iif(h.COLUMN46=22713,cast(t.COLUMN07 as decimal(18,2))/((cast(t.COLUMN07 as decimal(18,2)))+100),(cast(t.COLUMN07 as decimal(18,2)))/100))) as decimal(18,2)) as lTamnt , (select column04 from MATABLE002 where column02=(select COLUMN16 from MATABLE013 WHERE COLUMN02=h.COLUMN23)) names,(a.COLUMN14+a.COLUMN23+cast((a.COLUMN14 * (iif(h.COLUMN46=22713,(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23)as decimal(18,2))/(cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))+100)),cast((select COLUMN07 from MATABLE013 WHERE COLUMN02=h.COLUMN23) as decimal(18,2))/100))) as decimal(18,2) )) as LHtaxAmnt,iif(isnull(s.COLUMN04,0)>0,s.COLUMN04,m.COLUMN51) MRP,M32.COLUMN04 HSNCode,a.COLUMN14 ItemTotal,a.COLUMN19 ItemDiscount,M33.COLUMN04 SACCode,m.COLUMN48 SerItem,,M3.COLUMN04 itemfamily from  SATABLE010 a  left join MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 left join matable007 m on m.column02=a.COLUMN05 and isnull(m.columna13,0)=0 left join MATABLE013 M139 ON M139.COLUMN02=a.COLUMN21 AND M139.COLUMN16 = 23582 left join MATABLE024 s on s.column07=a.COLUMN05 and  s.column06='Sales' and s.COLUMNA03=a.COLUMNA03 and isnull(s.COLUMNA13,0)=0  left join SATABLE009 h on h.column01=a.COLUMN15 and h.COLUMNA13=0 left join MATABLE013 t on t.column02=a.COLUMN21 and t.COLUMNA13=0 LEFT JOIN MATABLE032 M32 ON M32.COLUMN02 =m.COLUMN75 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = m.COLUMN76 LEFT JOIN MATABLE003 M3 ON M3.COLUMN02 = it.COLUMN12 AND M3.COLUMNA03 = it.COLUMNA03 and isnull(M3.COLUMNA13,0)=0 where  a.column15='" + internalid + "'  and a.COLUMNA13=0";
                    Session["Lot"] = "";
                }

                SqlCommand taxQ1 = new SqlCommand("select sum(isnull(p.amt,0))amt,sum(isnull(p.qty,0))qty,CONVERT(varchar,cast(sum(cast(isnull(p.subtotamt,0) as decimal(18,2)))  AS money), 1)subtotamt from(select (isnull(a.COLUMN14,0)) amt,(isnull(a.COLUMN10,0)) qty,(CAST((isnull(a.COLUMN10,0))*(iif(pl.COLUMN05=22831,isnull(a.COLUMN35,isnull(a.COLUMN13,0)),isnull(a.COLUMN13,0))) as decimal(18,2))) subtotamt from  SATABLE010 a left join MATABLE023 pl on pl.column02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and pl.COLUMNA13=0 where a.column15='" + internalid + "'  and a.COLUMNA13=0)p", cn);

                object[] sp = new object[2];
                sp[0] = (sono);
                sp[1] = ("LINE");
                var db = Database.Open("sqlcon");
                var GData = db.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp);


                SqlCommand cmdt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdt.CommandType = CommandType.StoredProcedure;
                cmdt.Parameters.AddWithValue("@SONO", sono);
                cmdt.Parameters.AddWithValue("@TYPE", "TOTAL");
                cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                if (dtt.Rows.Count > 0)
                {
                    all1[0].TOTQTY = dtt.Rows[0]["qty"].ToString();
                    all1[0].SUBTOT = dtt.Rows[0]["subtotamt"].ToString();
                    all1[0].TOTTAX = dtt.Rows[0]["totaltax"].ToString();
                    all1[0].TOTCGT = dtt.Rows[0]["cgtttax"].ToString();
                    all1[0].TOTSGT = dtt.Rows[0]["sgtttax"].ToString();
                    all1[0].TOTIGT = dtt.Rows[0]["igtttax"].ToString();
                    all1[0].TOTAMT = dtt.Rows[0]["amt"].ToString();
                    //all1[0].SUBTOT1 = dtt.Rows[0]["subtotamt1"].ToString();
                }
                SqlCommand cmdtt = new SqlCommand("USP_PROC_GSTINVOICE", cn);
                cmdtt.CommandType = CommandType.StoredProcedure;
                cmdtt.Parameters.AddWithValue("@SONO", sono);
                cmdtt.Parameters.AddWithValue("@TYPE", "ITEMFAMILY");
                //cmdt.Parameters.AddWithValue("@frDT", Session["FormatDate"]);
                SqlDataAdapter datt = new SqlDataAdapter(cmdtt);
                DataTable dttt = new DataTable();
                datt.Fill(dttt);
                if (dttt.Rows.Count > 0)
                {
                    all1[0].Checkmtr = dttt.Rows[0]["itemfamily"].ToString();

                }
                var sqlt = "select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,sum(cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST((isnull(iif(lt.COLUMN16 IN (23582,23583,23584),lt.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)))  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0     where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,lt.COLUMN16 union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on (lt.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or lt.COLUMN02=a.COLUMN21) and lt.COLUMNA03=a.COLUMNA03 and isnull(lt.COLUMNA13,0)=0 left join MATABLE013 ht on (ht.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(b.COLUMN23,'-',''))) s)) or ht.COLUMN02=b.COLUMN23) and ht.COLUMNA03=a.COLUMNA03 and isnull(ht.COLUMNA13,0)=0    where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,lt.COLUMN16,ht.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                //"select distinct lt.column04 linetaxes,lt.column07 linetaxamt,null headertaxes,null headertaxamt,isnull(sum(isnull(a.column23,0)),0)  linetotaltax,lt.COLUMN16 TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21    where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.column04,lt.column07,a.COLUMN21,b.COLUMN23,b.COLUMN46,lt.COLUMN16   union all   select distinct  ht.column04 linetaxes,ht.column07 linetaxamt,ht.column04 headertaxes,ht.column07 headertaxamt, cast((cast((max(b.COLUMN22)-max(b.COLUMN35)-max(b.COLUMN36)-max(b.COLUMN25) ) as decimal(18,2))*(cast(ht.column07 as decimal(18,2))*0.01))as decimal(18,2)) linetotaltax,lt.COLUMN16 TaxType   from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23   where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.column04,lt.column07,ht.column04,ht.column07,a.COLUMN21,b.COLUMN23,lt.COLUMN16 union all select ' SBC @0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column45 as decimal(18,2))>0 and COLUMNA13=0 union all select ' Surcharge @10%' linetaxes,'10' linetaxamt,NULL headertaxes,NULL headertaxamt,column47 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column47 as decimal(18,2))>0 and COLUMNA13=0 union all select ' KKC@0.50%' linetaxes,'0.5' linetaxamt,NULL headertaxes,NULL headertaxamt,column45 linetotaltax,NULL TaxType   from   SATABLE009   where  column01='" + internalid + "' and cast(column48 as decimal(18,2))>0 and COLUMNA13=0 ";
                var GDataX = dbs.Query(sqlt);
                var sqlt1 = "select distinct isnull(lt.COLUMN16,0) TaxType from SATABLE010 a left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 and  lt.COLUMN16=23582  where  a.column15='" + internalid + "'  and a.COLUMNA13=0 group by lt.COLUMN16 union all   select distinct isnull(lt.COLUMN16,0) TaxType from   SATABLE010 a    left join SATABLE009 b on  b.COLUMN01=a.COLUMN15   left join MATABLE013 lt on lt.COLUMN02=a.COLUMN21 left join MATABLE013 ht on ht.COLUMN02=b.COLUMN23 and  ht.COLUMN16=23582  where  a.column15='" + internalid + "' and cast(ht.column07 as decimal(18,2))>0	and b.COLUMN23 not in(select isnull(COLUMN21,0) from SATABLE010   where  column15 in('" + internalid + "'))  and a.COLUMNA13=0  group by lt.COLUMN16 ";
                var GDataTT = dbs.Query(sqlt1);
                //var sqlt2 = "select f.CGST CGST,f.SGST SGST,f.IGST IGST,sum(f.CGSTAMT) CGSTAMT,sum(f.SGSTAMT) SGSTAMT,sum(f.IGSTAMT) IGSTAMT,f.SACCode as SACCode,f.HSNCode as HSNCode,sum(f.lineamt) lineamt,f.SerItem as SerItem from (select sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) CGST,SUM(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) SGST,SUM(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) IGST,cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23582,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) CGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23583,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) SGSTAMT, cast(((isnull(a.COLUMN10 ,0)*isnull(a.COLUMN35 ,0))*((CAST(sum(isnull(iif(t.COLUMN16=23584,t.COLUMN07,0),0)) as decimal(18,2)))/100)) as decimal(18,2)) IGSTAMT, (CAST(isnull(a.COLUMN10,0)*isnull(a.COLUMN13,0) as decimal(18,2))) lineamt,isnull(M33.COLUMN04,0) SACCode,isnull(M32.COLUMN04,0) HSNCode,it.COLUMN48 SerItem from  SATABLE010 a inner join SATABLE009 b on b.COLUMN01=a.COLUMN15 and b.COLUMNA02=a.COLUMNA02 and b.COLUMNA03=a.COLUMNA03 and isnull(b.COLUMNA13,0)=0 inner join matable007 it on it.column02=a.COLUMN05 and it.COLUMNA03=a.COLUMNA03 and isnull(it.COLUMNA13,0)=0  left join MATABLE013 ht on ht.column02=b.COLUMN23 and ht.COLUMNA03=b.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 left join MATABLE002 u on u.column02=a.COLUMN22 and (u.COLUMNA03=a.COLUMNA03 or isnull(u.COLUMNA03,0)=0) and isnull(u.COLUMNA13,0)=0 left join MATABLE013 t on (t.COLUMN02 in((SELECT ListValue FROM dbo.FN_ListToTable(',',(select COLUMN05  from MATABLE014 where COLUMN02=replace(a.COLUMN21,'-',''))) s)) or t.COLUMN02=a.COLUMN21) and t.COLUMNA03=a.COLUMNA03 and isnull(t.COLUMNA13,0)=0 LEFT JOIN MATABLE032 M33 ON  M33.COLUMN02 = it.COLUMN76 LEFT JOIN MATABLE032 M32 ON  M32.COLUMN02 = it.COLUMN75 LEFT JOIN MATABLE002 tn on tn.column02=t.COLUMN16 and isnull(a.COLUMNA13,0)=0 LEFT JOIN MATABLE023 pl on pl.COLUMN02=a.COLUMN37 and pl.COLUMNA03=a.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 where  b.column02='" + sono + "'  and isnull(a.COLUMNA13,0)=0 group by M33.COLUMN04,M32.COLUMN04,a.COLUMN10,a.COLUMN35,a.COLUMN13,it.COLUMN48) f group by f.SACCode,f.HSNCode,f.SerItem,f.CGST ,f.SGST,f.IGST";
                //var GDataTP = dbs.Query(sqlt2);
                object[] sp1 = new object[2];
                sp1[0] = (sono);
                sp1[1] = ("HSNCODE");
                var db1 = Database.Open("sqlcon");
                var GDataTP = db1.Query("Exec USP_PROC_GSTINVOICE @SONO=@0,@TYPE=@1", sp1);
                ViewBag.pitemsdata = GData;
                ViewBag.pitemsdatatax = GDataTP;
                ViewBag.taxitemsdata = GDataX;
                ViewBag.taxitemsTT = GDataTT;
                var cAdresss = "";
                var cAdresss1 = "";
                var ccity = "";
                var cstate = "";
                var ccountry = "";
                int czip;
                var oAdresss = "";
                var oAdresss1 = "";
                var ocity = "";
                var ostate = "";
                var ocountry = "";
                int ozip;
                var soAdresss = "";
                var soAdresss1 = "";
                var socity = "";
                var sostate = "";
                var socountry = "";
                int sozip;
                AddressMaster obj = new AddressMaster();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

                SqlCommand Ccmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
                Ccmd.CommandType = CommandType.StoredProcedure;
                Ccmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
                SqlDataAdapter cda = new SqlDataAdapter(Ccmd);
                DataTable dtA = new DataTable();
                cda.Fill(dtA);

                if (dtA.Rows.Count > 0)
                {
                    if (dtA.Rows[0]["COLUMN06"].ToString() == "" || dtA.Rows[0]["COLUMN06"].ToString() == null)
                        all1[0].cAdresss = "";
                    else
                        all1[0].cAdresss = dtA.Rows[0]["COLUMN06"].ToString();

                    if (dtA.Rows[0]["COLUMN07"].ToString() == "" || dtA.Rows[0]["COLUMN07"].ToString() == null)
                        all1[0].cAdresss1 = "";
                    else
                        all1[0].cAdresss1 = dtA.Rows[0]["COLUMN07"].ToString();

                    if (dtA.Rows[0]["COLUMN10"].ToString() == "" || dtA.Rows[0]["COLUMN10"].ToString() == null)
                        all1[0].ccity = "";
                    else
                        all1[0].ccity = dtA.Rows[0]["COLUMN10"].ToString();

                    if (dtA.Rows[0]["COLUMN11"].ToString() == "" || dtA.Rows[0]["COLUMN11"].ToString() == null)
                        all1[0].cstate = "";
                    else
                        all1[0].cstate = dtA.Rows[0]["COLUMN11"].ToString();

                    if (dtA.Rows[0]["COLUMN16"].ToString() == "" || dtA.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ccountry = "";
                    else
                        all1[0].ccountry = dtA.Rows[0]["COLUMN16"].ToString();

                    if (dtA.Rows[0]["COLUMN12"].ToString() == "" || dtA.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].czip = "";
                    else
                        all1[0].czip = dtA.Rows[0]["COLUMN12"].ToString();

                    if (dtA.Rows[0]["COLUMN34"].ToString() == "" || dtA.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].cDrugLIC = "";
                    else
                        all1[0].cDrugLIC = dtA.Rows[0]["COLUMN34"].ToString();


                    if (dtA.Rows[0]["COLUMN25"].ToString() == "" || dtA.Rows[0]["COLUMN25"].ToString() == null)
                        all1[0].SHAdresss = "";
                    else
                        all1[0].SHAdresss = dtA.Rows[0]["COLUMN25"].ToString();
                    if (dtA.Rows[0]["COLUMN26"].ToString() == "" || dtA.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].SHAdresss1 = "";
                    else
                        all1[0].SHAdresss1 = dtA.Rows[0]["COLUMN26"].ToString();
                    if (dtA.Rows[0]["COLUMN27"].ToString() == "" || dtA.Rows[0]["COLUMN27"].ToString() == null)
                        all1[0].SHAdresss2 = "";
                    else
                        all1[0].SHAdresss2 = dtA.Rows[0]["COLUMN27"].ToString();
                    if (dtA.Rows[0]["COLUMN30"].ToString() == "" || dtA.Rows[0]["COLUMN30"].ToString() == null)
                        all1[0].SHcity = "";
                    else
                        all1[0].SHcity = dtA.Rows[0]["COLUMN30"].ToString();

                    if (dtA.Rows[0]["COLUMN29"].ToString() == "" || dtA.Rows[0]["COLUMN29"].ToString() == null)
                        all1[0].SHstate = "";
                    else
                        all1[0].SHstate = dtA.Rows[0]["COLUMN29"].ToString();

                    if (dtA.Rows[0]["SHcity"].ToString() == "" || dtA.Rows[0]["SHcity"].ToString() == null)
                        all1[0].SHcountry = "";
                    else
                        all1[0].SHcountry = dtA.Rows[0]["SHcity"].ToString();

                    if (dtA.Rows[0]["COLUMN31"].ToString() == "" || dtA.Rows[0]["COLUMN31"].ToString() == null)
                        all1[0].SHzip = "";
                    else
                        all1[0].SHzip = dtA.Rows[0]["COLUMN31"].ToString();



                }

                SqlConnection cn2 = new SqlConnection(sqlcon);
                SqlCommand cmd2 = new SqlCommand("select COLUMN24,COLUMN25,COLUMN26 from contable008 where COLUMNA03='" + AcOwner001 + "' and column24 is not null  and column25 is not null  and column26 is not null  ", cn2);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);
                if (dt2.Rows.Count > 0)
                {
                    if (dt2.Rows[0][0].ToString() == "" || dt2.Rows[0][0].ToString() == null)      
                         ViewBag.plno = "";
                    else
                   ViewBag.plno = (dt2.Rows[0][0].ToString());

                    if (dt2.Rows[0][1].ToString() == "" || dt2.Rows[0][1].ToString() == null)
                        ViewBag.sdlno = "";
                    else
                        ViewBag.sdlno = (dt2.Rows[0][1].ToString());

                    if (dt2.Rows[0][2].ToString() == "" || dt2.Rows[0][2].ToString() == null)
                        ViewBag.flno = "";
                    else
                        ViewBag.flno = (dt2.Rows[0][2].ToString());

                }




                SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.Add(new SqlParameter("@oppUnit", a1));
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                DataTable odt = new DataTable();
                oda.Fill(odt);

                if (odt.Rows.Count > 0)
                {
                    if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                        all1[0].oAdresss = "";
                    else
                        all1[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                    if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                        all1[0].oAdresss1 = "";
                    else
                        all1[0].oAdresss1 = odt.Rows[0]["COLUMN13"].ToString();

                    if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                        all1[0].oAdresss2 = "";
                    else

                        all1[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                    if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                        all1[0].ocity = "";
                    else
                        all1[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                    if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                        all1[0].ostate = "";
                    else
                        all1[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                    if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                        all1[0].ocountry = "";
                    else
                        all1[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                    if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                        all1[0].ozip = "";
                    else
                        all1[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                    if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();

                    if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                        all1[0].douPhno = "";
                    else
                        all1[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                }

                all1[0].TotalAmount = dt.Rows[0]["totalamt1"].ToString();
                GETRoundOffVal(sono, 1277, all1);

                return View(all1);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");

            }
        }
    }
}