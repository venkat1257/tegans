﻿using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebMatrix.Data;
using System.Net;
using eBizSuiteUI.Controllers;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
namespace eBizSuiteProduct.Controllers
{
    public class RoleMasterController : Controller
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        //
        // GET: /RoleMaster/

        public ActionResult Index()
        {
            return View();
        }

        string[] theader;
        string[] theadertext;
        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
            var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
            theader = new string[Acolslist.Count];
            theadertext = new string[Acolslist.Count];
            for (int a = 0; a < Acolslist.Count; a++)
            {
                var acol = Acolslist[a].ToString();
                var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                var tblcol = columndata.COLUMN04;
                var dcol = Ccolslist[a].ToString();
                if (a == 0)
                {
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = "Action",
                        Format = (item) => new HtmlString("<a href=/RoleMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/RoleMaster/Detailes/" + item[tblcol] + " >View</a>")

                    });
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol
                    });
                    Inlinecols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol,
                        Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                    });
                }
                else
                {
                    cols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol
                    });
                    Inlinecols.Add(new WebGridColumn()
                    {
                        ColumnName = tblcol,
                        Header = dcol,
                        Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                    });
                } theader[a] = tblcol; theadertext[a] = dcol;

            }
            ViewBag.cols = cols;
            ViewBag.Inlinecols = Inlinecols;
            ViewBag.columns = Inlinecols;
            ViewBag.itemscol = theader;
            ViewBag.theadertext = theadertext;
            var ac = Convert.ToInt32(Session["AcOwner"]);string ounit =null;
            if (ac != 56567)
                ounit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
            else
                ounit = "  p.COLUMNA02 is null ";
            var query1 = "Select p.COLUMN02,p.COLUMN03,p.COLUMN04,p.COLUMN05,d.COLUMN04 COLUMN07,o.COLUMN03 COLUMN06,p.COLUMN08 from CONTABLE012 p left outer join MATABLE002 d on d.COLUMN02=p.COLUMN07 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN06 where p.COLUMNA13='False'  AND " + ounit + " AND p.COLUMNA03='" + Session["AcOwner"] + "'";
            var db2 = Database.Open("sqlcon");
            var books = db2.Query(query1);
            ViewBag.itemsdata = books;
            Session["cols"] = cols;
            Session["Inlinecols"] = Inlinecols;
            return View("Info", dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList());
        }



        public ActionResult Create()
        {
            CONTABLE012 all = new CONTABLE012();
            var ac = Convert.ToInt32(Session["AcOwner"]);
            var eid = (Session["OPUnit"]).ToString(); int? ou = null;
            //if (ac != 56567)
            //    ou = Convert.ToInt32(Session["OPUnit"].ToString());
            if (ac == 56567)
            {
                ou = null;
            }
            else
            {
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    ou = null;
                else
                    ou = Convert.ToInt32(Session["OPUnit"]);
            }
            var cen = dbContext.CONTABLE012.Where(q => eid.Contains(q.COLUMN06)).Select(q => q.COLUMN05).ToList();
            int acID = 0;
            var col2 = dbContext.CONTABLE012.OrderByDescending(a => a.COLUMN02).Select(a => a.COLUMN02).FirstOrDefault();
            var col1 = col2;
            if (col2 == null || col2 == 0)
            { col1 = 11111; all.COLUMN02 = 5656501; }
            else
            {
                all.COLUMN02 = (Convert.ToInt32(col1) + 1);
            }
            ViewBag.a = dbContext.CONTABLE011.Where(a => a.COLUMN03 != null && a.COLUMNA02 == ou && a.COLUMNA03 == ac ).ToList();
            ViewBag.o = dbContext.CONTABLE007.Where(a => a.COLUMN03 != null && a.COLUMNA02 == ou &&a.COLUMNA03 == ac ).ToList();
            ViewBag.d = dbContext.MATABLE002.Where(a => a.COLUMN03 == 11117 ).ToList();
            //}
            return View(all);
        }

        [HttpPost]
        public ActionResult Create(CONTABLE012 info, string Save, string SaveNew)
        {
            int idval = 0;
            var ac = Convert.ToInt32(Session["AcOwner"]);
            if (ac == 56567)
            {
                info.COLUMNA02 = null;
            }
            else
            {
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    info.COLUMNA02 = null;
                else
                    info.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
            }
            info.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
            info.COLUMNA06 = DateTime.Now;
            info.COLUMNA07 = DateTime.Now;
            info.COLUMNA12 = true;
            info.COLUMNA13 = false;
            dbContext.CONTABLE012.Add(info);
            dbContext.SaveChanges();

            CONTABLE003 MenuM = new CONTABLE003();

            var str = dbContext.CONTABLE003.Where(a => a.COLUMN04 == info.COLUMN05).Select(q => q.COLUMN05).ToArray();
            var strID1 = dbContext.CONTABLE003.Where(a => str.Contains(a.COLUMN05) && a.COLUMN09 == "Y" && a.COLUMN04 == info.COLUMN05).Select(q => q.COLUMN05).ToArray();

            var strID2 = dbContext.CONTABLE0010.Where(a => strID1.Contains(a.COLUMN04)).Select(q => q.COLUMN02).ToArray();

            int len = 0;

            if (strID1.Length < strID2.Length)
                len = strID1.Length;
            else
                len = strID2.Length;
            //Form V's Role

            for (int l = 0; l < len; l++)
            {
                CONTABLE016 FvsR = new CONTABLE016();
                var id = dbContext.CONTABLE016.OrderByDescending(a => a.COLUMN02).FirstOrDefault();

                if (id == null)
                {
                    FvsR.COLUMN02 = 1000;
                    FvsR.COLUMN03 = info.COLUMN02;
                    FvsR.COLUMN04 = info.COLUMN05;
                    FvsR.COLUMN05 = strID2[l];
                    FvsR.COLUMN06 = strID1[l].ToString();
                    dbContext.CONTABLE016.Add(FvsR);
                    dbContext.SaveChanges();
                }
                else
                {
                    int idval1 = id.COLUMN02;
                    FvsR.COLUMN02 = idval1 + 1;
                    FvsR.COLUMN03 = info.COLUMN02;
                    FvsR.COLUMN04 = info.COLUMN05;
                    FvsR.COLUMN05 = strID2[l];
                    FvsR.COLUMN06 = strID1[l].ToString();
                    dbContext.CONTABLE016.Add(FvsR);
                    dbContext.SaveChanges();
                }
            }

            dbContext.SaveChanges();
            if (Save != null)
            {
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1244 && q.COLUMN05 == 1).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Role Successfully Created";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                return RedirectToAction("Info");
            }
            return RedirectToAction("Create");
        }



        [HttpGet]
        public ActionResult Edit(int id)
        {
            var ac = Convert.ToInt32(Session["AcOwner"]);
            var eid = (Session["OPUnit"]).ToString(); int? ou = null;
            if (ac != 56567)
                ou = Convert.ToInt32(Session["OPUnit"].ToString());
            CONTABLE012 edit = dbContext.CONTABLE012.Find(id);
            var col2 = edit.COLUMN02;
            Session["time"] = dbContext.CONTABLE012.Find(col2).COLUMNA06;
            ViewBag.a = dbContext.CONTABLE011.Where(a => a.COLUMN03 != null && a.COLUMNA02 == ou && a.COLUMNA03 == ac).ToList();
            ViewBag.o = dbContext.CONTABLE007.Where(a => a.COLUMN03 != null && a.COLUMNA02 == ou && a.COLUMNA03 == ac).ToList();
            ViewBag.d = dbContext.MATABLE002.Where(a => a.COLUMN03 == 11117 ).ToList();
            return View(edit);
        }

        [HttpPost]
        public ActionResult Edit(CONTABLE012 info)
        {
            if (ModelState.IsValid)
            {
                dbContext.Entry(info).State = EntityState.Modified;
                var ac = Convert.ToInt32(Session["AcOwner"]);
                if (ac == 56567)
                {
                    info.COLUMNA02 = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                        info.COLUMNA02 = null;
                    else
                        info.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                }
                info.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                info.COLUMNA07 = DateTime.Now;
                info.COLUMNA06 = Convert.ToDateTime(Session["time"]);
                Session["time"] = null;
                info.COLUMNA12 = true;
                info.COLUMNA13 = false;
                dbContext.SaveChanges();
                var msg = string.Empty;
                var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1244 && q.COLUMN05 == 2).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Role Successfully Modified";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                return RedirectToAction("info");
            }
            return View(info);
        }



        public ActionResult Detailes(int id)
        {
            CONTABLE012 Detailes = dbContext.CONTABLE012.Find(id);
            ViewBag.a = dbContext.CONTABLE011.ToList();
            ViewBag.o = dbContext.CONTABLE007.Where(a => a.COLUMN03 != null).ToList();
            ViewBag.d = dbContext.MATABLE002.Where(a => a.COLUMN03 == 11117).ToList();

            return View(Detailes);
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            CONTABLE012 del = dbContext.CONTABLE012.Find(id);

            var y = (from x in dbContext.CONTABLE012 where x.COLUMN02 == id select x).First();
            y.COLUMNA12 = false;
            y.COLUMNA13 = true;

            dbContext.SaveChanges();
            var msg = string.Empty;
            var msgMaster = dbContext.CONTABLE023.Where(q => q.COLUMN04 == 1244 && q.COLUMN05 == 4).FirstOrDefault();
            if (msgMaster != null)
            {
                msg = msgMaster.COLUMN03;
            }
            else
            {
                msg = "Comapany Successfully Deleted.......... ";
            }
            Session["MessageFrom"] = msg;
            Session["SuccessMessageFrom"] = "Success";
            return RedirectToAction("Info");

        }

        [HttpPost]
        public ActionResult UpdateInline(string list)
        {

            XmlDocument obj = new XmlDocument();
            obj.LoadXml(list);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(obj));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                var c = dbContext.CONTABLE012.Where(a => a.COLUMN02 == eid).FirstOrDefault();
                c.COLUMN03 = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                c.COLUMN05 = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[3].ToString());
                c.COLUMN06 = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                c.COLUMN07 = ds.Tables[0].Rows[i].ItemArray[5].ToString();

                c.COLUMNA07 = DateTime.Now;

                dbContext.SaveChanges();
            }

            return RedirectToAction("Info");
        }

        [HttpGet]
        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            var RoleID = Request["RoleID"];
            var Name = Request["Name"];
            var Description = Request["Description"];
            //var Description = Request["Description"];
            //  var Operating_Unit = Request["Operating_Unit"];

            if (RoleID != null && RoleID != "" && RoleID != string.Empty)
            {
                RoleID = RoleID + "%";
            }
            else
                RoleID = Request["Role_ID"];


            if (Name != null && Name != "" && Name != string.Empty)
            {
                Name = Name + "%";
            }
            else
                Name = Request["Name"];


            if (Description != null && Description != "" && Description != string.Empty)
            {
                Description = Description + "%";
            }
            else
                Description = Request["Description"];

            //if (Operating_Unit != null && Operating_Unit != "" && Operating_Unit != string.Empty)
            //{
            //    Operating_Unit = Operating_Unit + "%";
            //}
            //else
            //    Operating_Unit = Request["Operating_Unit"];

            var query = "SELECT * FROM CONTABLE012  WHERE COLUMN02 like '" + RoleID + "' or COLUMN03 LIKE '" + Name + "' or COLUMN04 LIKE '" + Description + "'";


            var query1 = dbContext.CONTABLE012.SqlQuery(query);
            var gdata = from e in query1 select e;
            ViewBag.cols = Session["cols"];
            ViewBag.Inlinecols = Session["Inlinecols"];
            ViewBag.columns = Session["Inlinecols"];

            //var query1 = dbContext.CONTABLE012.SqlQuery(query);
            //var gdata = from e in query1 select e;
            //ViewBag.gdata = gdata;
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();


            return View("Info", gdata);
        }



        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult Style(string items, string inactive, string view, string sort)
        {
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            List<CONTABLE012> all = new List<CONTABLE012>();
            var grid = new WebGrid(null, canPage: false, canSort: false);
            var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
            var tbtid = tbid.COLUMN02;
            var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
            ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
            var data = from e in dbContext.CONTABLE012.AsEnumerable() select new { Role_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Center_ID = e.COLUMN05, Operating_Unit = e.COLUMN06, Subsidary = e.COLUMN07 };
            List<CONTABLE012> tbldata = new List<CONTABLE012>();
            tbldata = dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList();
            if (items == "Normal")
            {
                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                var indata = dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE012 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE012 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th>Action</th>";
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                } int r = 0; string rowColor = "";
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        r = r + 1;

                        if (r % 2 == 0)
                        {
                            rowColor = "alt";
                        }
                        else
                        {
                            rowColor = "alt0";
                        }

                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td><a href=/RoleMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/RoleMaster/Detailes/" + itm[Inline] + " >View</a></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                        }
                        else
                        {
                            tthdata += "<td>" + itm[Inline] + "</td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                        }


                    }
                    tthdata = "<tr class=" + @rowColor + " style='color:black'>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                grid = new WebGrid(null, canPage: false, canSort: false);

                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == view && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/RoleMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/RoleMaster/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Info", dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList());
        }




        public ActionResult CustomView()
        {
            List<CONTABLE005> all = new List<CONTABLE005>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
            var ttid = tid.COLUMN02;
            Session["tablid"] = ttid;
            //all = from e in dbContext.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
            var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
            all = dbContext.CONTABLE005.SqlQuery(query).ToList();

            return View(all);
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
            int col2 = 0;
            var tbldata = dbContext.CONTABLE004.Where(a => a.COLUMN05 == "Role_Master").FirstOrDefault();
            var tblid = tbldata.COLUMN02;
            var vdata = dbContext.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
            var formid = vdata.COLUMN05;
            var nfd1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
            eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
            if (ViewName == "")
            {
                return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
            }
            if (nfd1.Count > 0)
            {
                return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
            }

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(lst.ToString());
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                CONTABLE013 dd = new CONTABLE013();
                string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                string act = ds.Tables[0].Rows[i]["Action"].ToString();
                string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                if (i == 0)
                {
                    var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (vid == null)
                    {
                        col2 = 1001;
                    }
                    else
                    {
                        col2 = (vid.COLUMN02) + 1;
                    }
                    dd.COLUMN02 = col2;
                    dd.COLUMN03 = ViewName;
                    dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                    dd.COLUMN05 = Convert.ToInt32(formid);
                    dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                    dbContext.CONTABLE013.Add(dd);
                    dbContext.SaveChanges();
                }
                else
                {
                    if (act == "Y")
                    {
                        var vid = dbContext.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        dd.COLUMN02 = (vid.COLUMN02) + 1;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                        dbContext.CONTABLE013.Add(dd);
                        dbContext.SaveChanges();
                    }

                }
            }
            return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult View(string items, string sort, string inactive, string style)
        {
            List<CONTABLE012> all = new List<CONTABLE012>();
            List<WebGridColumn> cols = new List<WebGridColumn>();
            List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
            var ttid = tid.COLUMN02;
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (style == "Normal")
            {
                all = all.Where(a => a.COLUMNA12 == true).ToList();
                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var indata = dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                var query1 = "";
                if (inactive != null)
                {
                    query1 = "Select * from CONTABLE012 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                }
                else
                {
                    query1 = "Select * from CONTABLE012 where COLUMNA13='False' ";
                }
                var db2 = Database.Open("sqlcon");
                var books = db2.Query(query1);
                if (sort == "Recently Created")
                {
                    books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                theader = new string[Acolslist.Count];
                theadertext = new string[Acolslist.Count];
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    theader[a] = tblcol;
                    theadertext[a] = dcol;
                    if (a == 0)
                    {
                        tthtext += "<th>Action</th>";
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                    else
                    {
                        tthtext += "<th>" + dcol + "</th>";
                        itthtext += "<th>" + dcol + "</th>";
                    }
                }
                foreach (var itm in books)
                {
                    foreach (var Inline in theader)
                    {
                        if (Inline == "COLUMN02")
                        {
                            tthdata += "<td><a href=/RoleMaster/Edit/" + itm[Inline] + " >Edit</a>|<a href=/RoleMaster/Detailes/" + itm[Inline] + " >View</a></td>";
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                        }
                        else
                        {
                            itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                            tthdata += "<td>" + itm[Inline] + "</td>";
                        }
                    }
                    tthdata = "<tr>" + tthdata + "</tr>";
                    itthdata = "<tr>" + itthdata + "</tr>";
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.itemscol = theader;
                ViewBag.theadertext = theadertext;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                ViewBag.itemsdata = null;
                ViewBag.itemsdata = books;
                var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                return View("Info", dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

            }
            else
            {
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                if (sort == "Recently Created")
                {
                    all = dbContext.CONTABLE012.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                }
                else if (sort == "Recently Modified")
                {
                    all = dbContext.CONTABLE012.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                }
                else
                {
                    all = dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList();

                }
                if (inactive != null)
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                }
                else
                {
                    all = all.Where(a => a.COLUMN02 != null).ToList();
                }

                var Atid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
                var Attid = Atid.COLUMN02;
                var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                for (int a = 0; a < Acolslist.Count; a++)
                {
                    var acol = Acolslist[a].ToString();
                    var columnslist = dbContext.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                    var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                    var tblcol = columndata.COLUMN04;
                    var dcol = Ccolslist[a].ToString();
                    if (a == 0)
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = "Action",
                            Format = (item) => new HtmlString("<a href=/RoleMaster/Edit/" + item[tblcol] + " >Edit</a>|<a href=/RoleMaster/Detailes/" + item[tblcol] + " >View</a>")

                        });
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                        });
                    }
                    else
                    {
                        cols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol
                        });
                        Inlinecols.Add(new WebGridColumn()
                        {
                            ColumnName = tblcol,
                            Header = dcol,
                            Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                        });
                    }
                }
                ViewBag.cols = cols;
                ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                ViewBag.columnscount = Inlinecols.Count;
                Session["cols"] = cols;
                Session["Inlinecols"] = Inlinecols;
                grid = new WebGrid(all, canPage: false, canSort: false);
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "DataTable" },
                                 columns: cols);
                var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
             headerStyle: "webgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
                                 htmlAttributes: new { id = "grdData" },
                                 columns: Inlinecols);
                return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            return View("Info", dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList());
        }

        public void ExportPdf()
        {
            List<CONTABLE012> all = new List<CONTABLE012>();
            all = dbContext.CONTABLE012.ToList();
            var data = from e in dbContext.CONTABLE012.AsEnumerable() select new { Role_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Center_ID = e.COLUMN05, Operating_Unit = e.COLUMN06, Subsidary = e.COLUMN07 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gv.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        public void Exportword()
        {
            List<CONTABLE012> all = new List<CONTABLE012>();
            all = dbContext.CONTABLE012.ToList();
            var data = from e in dbContext.CONTABLE012.AsEnumerable() select new { Role_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Center_ID = e.COLUMN05, Operating_Unit = e.COLUMN06, Subsidary = e.COLUMN07 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
            Response.ContentType = "application/vnd.ms-word ";
            Response.Charset = string.Empty;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        public void ExportCSV()
        {
            StringWriter sw = new StringWriter();

            var y = dbContext.CONTABLE012.OrderBy(q => q.COLUMN02).ToList();

            sw.WriteLine("\"Role_ID\",\"Name\",\"Description\",\"Center_ID\",\"Operating_Unit\",\"Subsidary\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
            Response.ContentType = "text/csv";

            foreach (var line in y)
            {

                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"",
                                           line.COLUMN02,
                                           line.COLUMN03,
                                           line.COLUMN04,
                                           line.COLUMN05,
                                           line.COLUMN06,
                                           line.COLUMN07));

            }

            Response.Write(sw.ToString());

            Response.End();

        }
        public void ExportExcel()
        {
            List<CONTABLE012> all = new List<CONTABLE012>();
            all = dbContext.CONTABLE012.ToList();
            var data = from e in dbContext.CONTABLE012.AsEnumerable() select new { Role_ID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Center_ID = e.COLUMN05, Operating_Unit = e.COLUMN06, Subsidary = e.COLUMN07 };
            GridView gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            gv.FooterRow.Visible = false;
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter ht = new HtmlTextWriter(sw);
            gv.RenderControl(ht);
            Response.Write(sw.ToString());
            Response.End();
            gv.FooterRow.Visible = true;

        }
        public ActionResult Export(string items)
        {
            List<CONTABLE012> all = new List<CONTABLE012>();
            all = dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList();
            var tid = dbContext.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE012").First();
            var ttid = tid.COLUMN02;
            var viewlist = dbContext.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
            var viewsdata = dbContext.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
            ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
            if (items == "PDF")
            {
                ExportPdf();
            }
            else if (items == "Excel")
            {
                ExportExcel();
            }
            else if (items == "Word")
            {
                Exportword();
            }
            else if (items == "CSV")
            {
                ExportCSV();
            }
            return View("Info", dbContext.CONTABLE012.Where(a => a.COLUMNA13 == false).ToList());
        }
    }
}

