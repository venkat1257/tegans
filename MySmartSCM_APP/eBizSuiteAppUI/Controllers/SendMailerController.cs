﻿using MeBizSuiteAppUI.Content;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
//using Twilio; 
using System.Net;
using System.IO;
using System.Text;
using eBizSuiteAppModel.Table;
using WebMatrix.Data;
using System.Data.SqlClient;
using System.Data;
using eBizSuiteAppModel;
using TweetSharp;
//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
using System.Web.UI;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using System.Web.Hosting;
using eBizSuiteAppDAL.classes;
using System.Globalization;
using System.Web.Helpers;
//using Twitterizer;
using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;
namespace MeBizSuiteAppUI.Controllers
{
    public class SendMailerController : Controller
    {
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
           public int Prty;
        //
        // GET: /SendMailer/ 
        public ActionResult Index()
        {
            try
            {
            //EMPHCS1539 Mail Changes by srinivas 
			Session["SendFrom"] = Session["Sub"] = Session["Pwd"] = null;
            var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            var providerName = "System.Data.SqlClient";
            var billdb = Database.OpenConnectionString(connectionString, providerName);

            SqlConnection cn = new SqlConnection(sqlcon);
            //EMPHCS1539 Mail Changes by srinivas 
			SqlCommand cmd = new SqlCommand("PROC_CHECK_EMAILCONFIG", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@EMPID", Session["eid"]));
            cmd.Parameters.Add(new SqlParameter("@ACOWNER", Session["AcOwner"]));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Session["SendFrom"] = dt.Rows[0][0];
                Session["Pwd"] = dt.Rows[0][1];
                Session["Sub"] = "  From " + dt.Rows[0][2] + "";
                Session["Subject"] = Convert.ToString(Session["Subject"])  + Convert.ToString(Session["Sub"]);

            }
            MailModel mail = new MailModel();
            mail.Body = "";
            eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
            //var Oper = Session["OPUnit"];
            var acOW = Convert.ToInt32(Session["AcOwner"]);
            var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
            ViewBag.smssend = lov;
            int formID = Convert.ToInt32(Session["id"]);
            var customForm1 = "";
			//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
            if (formID == 1251 || formID == 1273 || formID == 1274)
            {
                Session["customerId"] = "0";
                customForm1 = "select COLUMN11 as COLUMN02 , COLUMN02 COLUMN05 from SATABLE001 where COLUMN05='" + Session["vendorIdName"] + "' AND COLUMN02= '" + Session["vendorId"] + "' and COLUMNA03=" + acOW + " and isnull(COLUMNA13,0)=0";
            }
            else
                customForm1 = "select COLUMN10 as COLUMN02 , COLUMN02 COLUMN05 from SATABLE002 where COLUMN02= '" + Session["customerId"] + "' and COLUMNA03=" + acOW + " and isnull(COLUMNA13,0)=0";
            if (Convert.ToString(Session["customerId"]) == "" || Session["customerId"] == null) customForm1 = "select COLUMN10 as COLUMN02 , COLUMN02 COLUMN05 from SATABLE002 where COLUMN05!='' and COLUMNA03=" + acOW + " and isnull(COLUMNA13,0)=0";
            var customForms1 = billdb.Query(customForm1);
			//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
            ViewBag.Custmail = customForms1.FirstOrDefault().COLUMN02;
            ViewBag.cust = customForms1.ToList();
			//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
            var idi = "";
            if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
            var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("ide=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
            SqlCommand cmdb = new SqlCommand("PROC_CHECK_EMAILTemplate", cn);
            cmdb.CommandType = CommandType.StoredProcedure;
            cmdb.Parameters.Add(new SqlParameter("@Ide", idi));
            cmdb.Parameters.Add(new SqlParameter("@FormName", custform));
			//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
            cmdb.Parameters.Add(new SqlParameter("@DateFormat", Session["DateFormat"]));
            cmdb.Parameters.Add(new SqlParameter("@ACOWNER", Session["AcOwner"]));
            SqlDataAdapter dab = new SqlDataAdapter(cmdb);
            DataTable dtb = new DataTable();
            dab.Fill(dtb);
            string Body = "";
            if (dtb.Rows.Count > 0)
            { Body = dtb.Rows[0][0].ToString(); }
            mail.Body = Body;
            return View(mail);
        }
         catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

		//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
        public ActionResult EmailSend(string fname, string html, string Transno)
        {
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            try
            {
                var idName = Session["idName"];
                //EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
                SqlConnection cn = new SqlConnection(sqlcon);
                int formID = Convert.ToInt32(Session["id"]);
                if (formID == 1276||formID == 1278)
                {
                    string tname = "SATABLE007"; if (formID == 1278) tname = "SATABLE011";
                    SqlCommand cmdc = new SqlCommand("select COLUMN05,COLUMN04 from "+tname+" where COLUMN02=" + Transno + "", cn);
                    SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                    DataTable dtc = new DataTable();
                    dac.Fill(dtc);
                    if (dtc.Rows.Count > 0)
                    { Session["customerId"] = dtc.Rows[0][0].ToString(); idName = dtc.Rows[0][1].ToString(); }
                }

                Session["file"] = null;
                Session["subject"] = null;
                //String htmlText = "<script src=/Scripts/jquery-1.7.1.js></script><script src=/Content/formbuildjquery.js></script>";
				//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
                if (html != "" && html != null)
                {
                    StringReader sr = new StringReader(html.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 10f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    string filePath = HostingEnvironment.MapPath("~/Content/Pdf/");
                    //string path = fname + (Request["no"]);
                    string path = idName.ToString();
                    string pathpdf1 = path.Replace(" ", "");
                    string pathpdf2 = pathpdf1.Replace("/", "");
                    string pathpdf = pathpdf2.Replace(":", "");
                    int AOwner = Convert.ToInt32(Session["AcOwner"]);
                    var AOwnerName = dc.CONTABLE002.Where(a => a.COLUMN02 == AOwner).FirstOrDefault().COLUMN04;
                    filePath = filePath + AOwnerName;
                    Session["file"] = Convert.ToString(Request["FormName"]) + " " + pathpdf + ".pdf";
                    ViewBag.file = pathpdf + ".pdf";
                    if (!Directory.Exists(filePath))
                        Directory.CreateDirectory(filePath);
                    if (System.IO.File.Exists(filePath + "\\" + pathpdf + ".pdf"))
                    {
                        System.IO.File.Delete(filePath + "\\" + pathpdf + ".pdf");
                    }
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(filePath + "\\" + pathpdf + ".pdf", FileMode.Create));
                    //PdfWriter.GetInstance(pdfDoc, new FileStream(filePath + "\\" + pathpdf + ".pdf", FileMode.Create));
                    Session["PATH"] = null;
                    Session["PATH"] = filePath + "\\" + pathpdf + ".pdf";
                    Session["subject"] = Convert.ToString(Request["FormName"]) + " # " + pathpdf;
                    ViewBag.subject = pathpdf + " transaction";
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
                    pdfDoc.Open();
                    htmlparser.Parse(sr);
                    pdfDoc.Close();
                }
                else
                    Session["subject"] = Convert.ToString(Request["FormName"]) + " # " + idName.ToString();
                Session["SendFrom"] = Session["Sub"] = Session["Pwd"] = null;
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var billdb = Database.OpenConnectionString(connectionString, providerName);

                //EMPHCS1539 Mail Changes by srinivas 
                SqlCommand cmd = new SqlCommand("PROC_CHECK_EMAILCONFIG", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EMPID", Session["eid"]));
                cmd.Parameters.Add(new SqlParameter("@ACOWNER", Session["AcOwner"]));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Session["SendFrom"] = dt.Rows[0][0];
                    Session["Pwd"] = dt.Rows[0][1];
                    Session["Sub"] = "  From " + dt.Rows[0][2] + "";
                    Session["Subject"] = Convert.ToString(Session["Subject"]) + Convert.ToString(Session["Sub"]);

                }
                //MailModel mail = new MailModel();
                //mail.Body = "";
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                //var Oper = Session["OPUnit"];
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                ViewBag.smssend = lov;
                var customForm1 = "";
                if (formID == 1251 || formID == 1273 || formID == 1274)
                    customForm1 = "select COLUMN11 as COLUMN02 , COLUMN02 COLUMN05 from SATABLE001 where COLUMN05='" + Session["vendorIdName"] + "' AND COLUMN02= '" + Session["vendorId"] + "' and COLUMNA03=" + acOW + "";
                else
                    customForm1 = "select COLUMN10 as COLUMN02 , COLUMN02 COLUMN05 from SATABLE002 where COLUMN02= '" + Session["customerId"] + "' and COLUMNA03=" + acOW + " and isnull(COLUMNA13,0)=0";
                SqlCommand cmd1 = new SqlCommand(customForm1, cn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                var customForms1 = billdb.Query(customForm1);
                ViewBag.cust = customForms1.ToList();
                if (dt1.Rows.Count > 0)
                {
                    //_objModelMail.From = "MailSendingTesting@gmail.com";
                    MailMessage mail = new MailMessage();
                    mail.To.Add(dt1.Rows[0][0].ToString());
                    mail.From = new MailAddress(dt.Rows[0][0].ToString());
                    mail.Subject = Convert.ToString(Session["Subject"]);
                    //if (Convert.ToString(mail.CC) != "")
                    //    mail.CC.Add(_objModelMail.Cc);
                    //string Body = _objModelMail.Body;
                    mail.Body = "";
                    mail.IsBodyHtml = true;
					//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
                    if (Session["PATH"] != null)
                    {
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(Session["PATH"].ToString());
                        mail.Attachments.Add(attachment);
                    }
                    SmtpClient smtp = new SmtpClient();
                    SqlCommand cmde = new SqlCommand("Select COLUMN02,COLUMN04,COLUMN05 From SETABLE016 where isnull(COLUMN09,0)=1 and isnull(COLUMNA13,0)=0 and COLUMNA03=" + Session["AcOwner"] + "", cn);
                    SqlDataAdapter dae = new SqlDataAdapter(cmde);
                    DataTable dte = new DataTable();
                    dae.Fill(dte);
                    if (dte.Rows.Count > 0)
                    {
                        if (dte.Rows[0]["COLUMN04"].ToString() != "")
                            smtp.Port = Convert.ToInt32(dte.Rows[0]["COLUMN04"].ToString());
                        smtp.Host = dte.Rows[0]["COLUMN05"].ToString();
                    }
                    else
                    {
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                    }
					//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
                    var idi = "";
                    if (Request.UrlReferrer == null) return RedirectToAction("Logon", "Account");
					//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
                    var custform = Request.UrlReferrer.Query.ToString();
                    string type = "Insert";
                    type = Request.UrlReferrer.LocalPath.ToString().Replace("/", ""); type = type.Replace("FormBuilding", "");
                    if (type == "FormBuild") { custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int l = custform.IndexOf("&"); if (l > 0) { custform = custform.Substring(0, l).Replace("ide=", "").ToString(); } custform = custform.Replace("&", ""); custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); }
                    else if (type == "Detailes") { custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int e = custform.IndexOf("&"); if (e > 0) custform = custform.Remove(0, e); custform = custform.Replace("&ide=null", ""); custform = custform.Replace("&", ""); }
                    else { custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { custform = custform.Substring(0, m).Replace("FormName=", "").ToString(); } custform = custform.Replace("%20", " "); }
                    SqlCommand cmdb = new SqlCommand("PROC_CHECK_EMAILTemplate", cn);
                    cmdb.CommandType = CommandType.StoredProcedure;
                    cmdb.Parameters.Add(new SqlParameter("@Ide", Transno));
                    cmdb.Parameters.Add(new SqlParameter("@FormName", custform));
                    cmdb.Parameters.Add(new SqlParameter("@DateFormat", Session["DateFormat"]));
                    cmdb.Parameters.Add(new SqlParameter("@ACOWNER", Session["AcOwner"]));
                    SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                    DataTable dtb = new DataTable();
                    dab.Fill(dtb);
                    string Body = "";
                    if (dtb.Rows.Count > 0)
                    { Body = dtb.Rows[0][0].ToString(); }
                    mail.Body = Body;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString());// Enter seders User name and password
                    smtp.EnableSsl = true; 
                    smtp.Send(mail);
                    //smtp.SendAsync(mail, "");
                    //eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                    //var Oper = Session["OPUnit"];
                    //var acOW = Convert.ToInt32(Session["AcOwner"]);
                    //var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                    var cust = activity.SATABLE002.Where(a => a.COLUMNA03 == acOW).ToList();
                    cust = cust.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                    ViewBag.smssend = lov;
                    ViewBag.cust = cust;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1370 && q.COLUMN05 == 0).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    //if (System.IO.File.Exists(Session["PATH"].ToString()))
                    //{
                    //    var pdf = Session["PATH"].ToString();
                    //    Session["PATH"] = null;
                    //    System.IO.File.Delete(pdf);
                    //}
                    //EMPHCS1539 Mail Changes by srinivas 
                    //if (Session["PATH"] == null)
                    //    return View("Index", _objModelMail);
                    //else
                    //{
                    Session["PATH"] = null;
                    return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"], ide = "null" });
                    //}
                }
                else
                {
					//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    MailModel mail = new MailModel();
                    mail.Body = "";
                    //eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                    //eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                    //var Oper = Session["OPUnit"];
                    //var acOW = Convert.ToInt32(Session["AcOwner"]);
                    //var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                    var cust = activity.SATABLE002.Where(a => a.COLUMNA03 == acOW).ToList();
                    cust = cust.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                    ViewBag.smssend = lov;
                    ViewBag.cust = cust; var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1370 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                    //EMPHCS1539 Mail Changes by srinivas 
                    return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"], ide = "null" });
                }
            }
            catch (Exception ex)
            {
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1370 && q.COLUMN05 == 1).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Successfully Created..... ";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                //EMPHCS1539 Mail Changes by srinivas 
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"], ide = "null" });
            }
        }

        [HttpPost]
        public ActionResult Index(FormCollection col, MailModel _objModelMail, HttpPostedFileBase hb)
        {
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            try
            {
                if (!string.IsNullOrEmpty(_objModelMail.To))
                {
                    eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                    var owner = Convert.ToInt32(Session["AcOwner"]);
                    var name = dc.CONTABLE002.Where(q => q.COLUMN02 == owner).FirstOrDefault();
                    //_objModelMail.From = "MailSendingTesting@gmail.com";
                    MailMessage mail = new MailMessage();
                    mail.To.Add(_objModelMail.To);
                    mail.From = new MailAddress(_objModelMail.From,name.COLUMN03);
                    mail.ReplyTo = mail.From;
                    mail.Subject = _objModelMail.Subject;
					//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    if (Convert.ToString(_objModelMail.Cc) != "" && Convert.ToString(_objModelMail.Cc) != null)
                        mail.CC.Add(_objModelMail.Cc);
                    string Body = _objModelMail.Body;
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    if (Session["PATH"] == null)
                    {
                        var image = Request.Files[0] as HttpPostedFileBase;
                        if (image != null)
                        {
                            if (Request.Files.Count > 0)
                            {
                                if (image.ContentLength > 0)
                                {
                                    string fileName = Path.GetFileName(image.FileName);
                                    mail.Attachments.Add(new Attachment(image.InputStream, fileName));
                                }
                            }
                        }
                    }
                    else
                    {
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(Session["PATH"].ToString());
                        mail.Attachments.Add(attachment);
                    }
                    ////EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    //SqlConnection cn = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("Select COLUMN02,COLUMN04,COLUMN05 From SETABLE016 where isnull(COLUMN09,0)=1 and isnull(COLUMNA13,0)=0 and COLUMNA03=" + Session["AcOwner"] + "", cn);
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //DataTable dt = new DataTable();
                    //da.Fill(dt);
                    SmtpClient smtp = new SmtpClient();
                    //if (dt.Rows.Count > 0) {
                    //    if (dt.Rows[0]["COLUMN04"].ToString() != "")
                    //        smtp.Port = Convert.ToInt32(dt.Rows[0]["COLUMN04"].ToString());
                    //    smtp.Host = dt.Rows[0]["COLUMN05"].ToString();
                    //}
                    //else
                    //{
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                    //}
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential
					//EMPHCS1539 Mail Changes by srinivas 
                    ("emphorasoft@gmail.com", "Welcome12");// Enter seders User name and password
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                    mail.Attachments.Dispose();
                    //var directory = new DirectoryInfo(Session["PATH"].ToString());
                    //foreach (FileInfo file in directory.GetFiles())
                    //{
                    //        file.Delete();
                    //}
                    if (System.IO.File.Exists(Session["PATH"].ToString()))
                    {
                        System.IO.File.Delete(Session["PATH"].ToString());
                    }
                    //smtp.SendAsync(mail, "");
                    eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                    //var Oper = Session["OPUnit"];
                    List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                    var acOW = Convert.ToInt32(Session["AcOwner"]);
                    var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                    var cust = activity.SATABLE002.Where(a => a.COLUMNA03 == acOW).ToList();
                    cust = cust.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                    ViewBag.smssend = lov;
                    ViewBag.cust = cust;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1370 && q.COLUMN05 == 0).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                    //if (System.IO.File.Exists(Session["PATH"].ToString()))
                    //{
                    //    var pdf = Session["PATH"].ToString();
                    //    Session["PATH"] = null;
                    //    System.IO.File.Delete(pdf);
                    //}
					//EMPHCS1539 Mail Changes by srinivas 
                    //if (Session["PATH"] == null)
                    //    return View("Index", _objModelMail);
                    //else
                    //{
                        Session["PATH"] = null;
                        return Json(new { FormName = Session["FormName"] }, JsonRequestBehavior.AllowGet);
                    //}
                }
                else
                {

                    MailModel mail = new MailModel();
                    mail.Body = "";
                    eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                    eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                    //var Oper = Session["OPUnit"];
                    List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                    var acOW = Convert.ToInt32(Session["AcOwner"]);
                    var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                    var cust = activity.SATABLE002.Where(a =>  a.COLUMNA03 == acOW).ToList();
                    cust = cust.Where(a => Oper.Contains(a.COLUMNA02.ToString()) || a.COLUMNA02 == null).ToList();
                    ViewBag.smssend = lov;
                    ViewBag.cust = cust; var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1370 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Successfully Created..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
					//EMPHCS1539 Mail Changes by srinivas 
                    return Json(new { FormName = Session["FormName"] }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1370 && q.COLUMN05 == 1).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Successfully Created..... ";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
				//EMPHCS1539 Mail Changes by srinivas 
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"], ide = "null" });
            }
        }


        [HttpGet]
        public ActionResult SmsSettings()
        {
            try
            {
                //Old Coding For Sms Settings

                //eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                //eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                //var Oper = Session["OPUnit"];
                //var acOW = Convert.ToInt32(Session["AcOwner"]);
                //var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                //var cust = activity.SATABLE002.Where(a => a.COLUMNA02 == Oper && a.COLUMNA03 == acOW).ToList();
                //ViewBag.smssend = lov;           
                //ViewBag.cust = cust;

                //Ending old Code For Sms Settings.

                string direction = "GetSpList";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                //SqlConnection con = new SqlConnection(Connection.Con);
                //con.Open();
                //SqlCommand cmd = new SqlCommand("ServiceProCRUD", con);
                //cmd.Parameters.AddWithValue("@COLUMNA03",acOW);
                //cmd.Parameters.AddWithValue("@Direction", direction);
                //cmd.CommandType = CommandType.StoredProcedure;
                //SqlDataAdapter da = new SqlDataAdapter(cmd);          
                //DataTable dt = new DataTable();
                //da.Fill(dt);
                //var GData = dt;
                List<string> actCol = new List<string>();
                actCol.Add("");
                actCol.Add("ServiceProvider");                              
                actCol.Add("UserName");             
                actCol.Add("SenderId");
                actCol.Add("SendSMSUrl");                           
                actCol.Add("MobileNo");              
                actCol.Add("AuthenticationKey");
                actCol.Add("APIKey");
                actCol.Add("PortNo");
                actCol.Add("Priority"); //COLUMN05 as 'Priority'
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);

                ViewBag.sortselected = Session["SortByStatus"];
                ViewBag.viewselected = Session["ViewStyle"];

                var GData = dbs.Query("ServiceProCRUD @Direction=" + direction + ", @COLUMNA03=" + acOW+ "");
                
                var grid = new WebGrid(GData, canPage: false, canSort: false);

                ViewBag.Columns = actCol;
                if (Session["SDT"] != null)
                {
                    ViewBag.Grid = Session["SDT"];
                    Session["SDT"] = null;
                }
                else
                    ViewBag.Grid = GData;
                return View("~/Views/SendMailer/SmsSettings.cshtml", new { FormName = Session["FormName"] });
            }

            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }





        [HttpPost]
        public ActionResult SmsSettings(FormCollection sms)
        {
            try
            {
               
                return View();
            }
            catch(Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
            
        }

        [HttpGet]
        public ActionResult InsertServiceProvider()
        {
            try
            {
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat =Convert.ToString(dtf.Rows[0]["COLUMN04"]);
                    JQDateFormat =Convert.ToString(dtf.Rows[0]["COLUMN10"]);
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                return View();
            }
            catch (Exception ex)
            {

                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpPost]
        public ActionResult InsertServiceProvider(FormCollection sms)
        {
            try
            {
                string direction = "Insert";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                String ServiceProvider = sms["ServiceProvider"];
                String Description = sms["Description"];
                String Priority = sms["Priority"];
                String UserName = sms["UserName"];
                String PassWord = sms["PassWord"];
                String SenderId = sms["SenderId"];
                String SendSMSUrl = sms["SendSMSUrl"];
                String SentReportUrl = sms["SentReportUrl"];
                String ChechBalanceUrl = sms["ChechBalanceUrl"];
                String MobileNo = sms["MobileNo"];
                string FromDate = sms["FromDate"];
                string ToDate = sms["ToDate"];
                //DateTime FromDate = Convert.ToDateTime(sms["FromDate"], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                // DateTime FromDate = DateTime.ParseExact(sms["FromDate"].ToString(), @"dd/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);    
                //DateTime ToDate = DateTime.ParseExact(sms["ToDate"], Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                // DateTime FromDate = Convert.ToDateTime(sms["FromDate"]); DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                //SendSmsClass1 sm1 = new SendSmsClass1();
                //sm1.InsertServiceProviders1(ServiceProvider, Description, Prty, UserName, PassWord, SenderId, SendSMSUrl, SentReportUrl, ChechBalanceUrl, MobileNo, FromDate, ToDate, Message, MessageType);

                //DateTime ToDate = Convert.ToDateTime(sms["ToDate"], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                String Message = sms["Message"];
                int MessageType = Convert.ToInt32(sms["MessageType"]);               
                String AuthKey = sms["AuthenticationKey"];
                String APiKey = sms["APIKey"];
                String PortNo = sms["PortNo"];
                using (SqlConnection con = new SqlConnection(Connection.Con))
                {
                    //SqlCommand cmd = new SqlCommand("smsconfigurarion", con);              
                    SqlCommand cmd = new SqlCommand("InsServicePro", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@COLUMNA03", acOW);
                    cmd.Parameters.AddWithValue("@COLUMN03", ServiceProvider);
                    cmd.Parameters.AddWithValue("@COLUMN04", Description);
                    cmd.Parameters.AddWithValue("@COLUMN05", Priority);
                    cmd.Parameters.AddWithValue("@COLUMN06", UserName);
                    cmd.Parameters.AddWithValue("@COLUMN07", PassWord);
                    cmd.Parameters.AddWithValue("@COLUMN08", SenderId);
                    cmd.Parameters.AddWithValue("@COLUMN09", SendSMSUrl);
                    cmd.Parameters.AddWithValue("@COLUMN10", SentReportUrl);
                    cmd.Parameters.AddWithValue("@COLUMN11", ChechBalanceUrl);
                    cmd.Parameters.AddWithValue("@COLUMN12", MobileNo);
                    cmd.Parameters.AddWithValue("@COLUMN13", DateTime.ParseExact(FromDate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@COLUMN14", DateTime.ParseExact(ToDate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@COLUMN15", Message);
                    cmd.Parameters.AddWithValue("@COLUMN16", MessageType);
                    cmd.Parameters.AddWithValue("@COLUMN17", AuthKey);
                    cmd.Parameters.AddWithValue("@COLUMN18", APiKey);
                    cmd.Parameters.AddWithValue("@COLUMN19", PortNo);
                    cmd.Parameters.AddWithValue("@Direction", direction);
                    con.Open();
                    int r = 0;
                    r = cmd.ExecuteNonQuery();
                
                if (r > 0)
                {
                    var msg = string.Empty;
                    msg = "ServiceProvider Successfully Created";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
              }
                return RedirectToAction("SmsSettings");
            }
            catch (Exception ex)
            {
                var msg = "ServiceProvider Failed";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        [HttpGet]

        public ActionResult PriorityAlert(string Id)
        {
            String priority = Id;
            try
            {
            var acOW = Convert.ToInt32(Session["AcOwner"]);
            var cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            using (SqlConnection con = new SqlConnection(cn))
            {
                SqlCommand cmd = new SqlCommand("select Count (*) Column05 from SETABLE023 where columnA03= " + acOW + " and column05=" + priority + " ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1); string val = null;
                if (dt1.Rows.Count > 0)
                    val = dt1.Rows[0]["COLUMN05"].ToString();
                 return Json(new { Data1 = val}, JsonRequestBehavior.AllowGet);
              }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InsertServiceProvider", "SendMailer", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult UpdateServiceProvider(string inm)
        {
            try
            {
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter daf = new SqlDataAdapter(str, cn);
                DataTable dtf = new DataTable();
                daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
                if (dtf.Rows.Count > 0)
                {
                    DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                    JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                    Session["DateFormat"] = DateFormat;
                    ViewBag.DateFormat = DateFormat;
                }
                if (JQDateFormat != "")
                {
                    Session["DateFormat"] = DateFormat;
                    Session["ReportDate"] = JQDateFormat;
                }
                else
                {
                    Session["DateFormat"] = "dd/MM/yyyy";
                    Session["ReportDate"] = "dd/mm/yy";
                    ViewBag.DateFormat = "dd/MM/yyyy";
                }
                return View();
            }
            catch(Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SmsSettings", "SendMailer");
            
            }
        }

        public ActionResult ViewServiceProvider(string inm)
        {
            try
            {
                return View();
            }
            catch(Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }

        public ActionResult Edit(string Id)
        {
            try
            {
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                string direction = "UpdateGet";
                int Id1 = Convert.ToInt32(Id);
                string dateF =Convert.ToString(Session["FormatDate"]);
                using (SqlConnection con = new SqlConnection(Connection.Con))
                {
                    SqlCommand cmd = new SqlCommand("ServiceProCRUD", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@column02", Id1);
                    cmd.Parameters.AddWithValue("@Direction", direction);
                    cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                    cmd.Parameters.AddWithValue("@DateF", dateF);
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable ta = new DataTable();
                    da.Fill(ta);

                    var SpName = ta.Rows[0]["COLUMN03"].ToString();
                    var Desc = ta.Rows[0]["COLUMN04"].ToString();
                    var Prty = ta.Rows[0]["COLUMN05"].ToString();
                    var UName = ta.Rows[0]["COLUMN06"].ToString();
                    var PWd = ta.Rows[0]["COLUMN07"].ToString();
                    var SId = ta.Rows[0]["COLUMN08"].ToString();
                    var SendSMSU = ta.Rows[0]["COLUMN09"].ToString();
                    var SentRepo = ta.Rows[0]["COLUMN10"].ToString();
                    var ChechBal = ta.Rows[0]["COLUMN11"].ToString();
                    var MobileNo = ta.Rows[0]["COLUMN12"].ToString();
                    var FromDate = ta.Rows[0]["COLUMN13"].ToString();
                    var ToDate = ta.Rows[0]["COLUMN14"].ToString();
                    var Msg = ta.Rows[0]["COLUMN15"].ToString();
                    var MsgType = ta.Rows[0]["COLUMN16"].ToString();
                    var AuthKey = ta.Rows[0]["COLUMN17"].ToString();
                    var APiKey = ta.Rows[0]["COLUMN18"].ToString();
                    var PortNo = ta.Rows[0]["COLUMN19"].ToString();


                    return Json(new { a1 = SpName, b1 = Desc, c1 = Prty, d1 = UName, e1 = PWd, f1 = SId, g1 = SendSMSU, h1 = SentRepo, i1 = ChechBal, j1 = MobileNo, k1 = FromDate, l1 = ToDate, m1 = Msg, n1 = MsgType, p1 = AuthKey, q1 = APiKey, r1 = PortNo },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex) 
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SmsSettings", "SendMailer");
            }
        }

        [HttpPost]
        public ActionResult UpdateServiceProvider(FormCollection sms)
        {
            try
            {
                string direction = "UpdatePost";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Int32 Id1 = Convert.ToInt32(sms["IDN"]);
                //Int32 Id1 = Request.QueryString["Id"];
                String ServiceProvider = sms["ServiceProvider"];
                String Description = sms["Description"];
                String Priority = sms["Priority"];
                String UserName = sms["UserName"];
                String PassWord = sms["PassWord"];
                String SenderId = sms["SenderId"];
                String SendSMSUrl = sms["SendSMSUrl"];
                String SentReportUrl = sms["SentReportUrl"];
                String ChechBalanceUrl = sms["ChechBalanceUrl"];
                String MobileNo = sms["MobileNo"];
                var FromDate = sms["FromDate"];
                var ToDate = sms["ToDate"];
               // DateTime FromDate = DateTime.ParseExact(sms["FromDate"].Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                // DateTime FromDate = DateTime.ParseExact(sms["FromDate"].ToString(), @"dd/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);    
                //DateTime ToDate = DateTime.ParseExact(sms["ToDate"], Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture);
                // DateTime FromDate = Convert.ToDateTime(sms["FromDate"]); DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                //DateTime ToDate = Convert.ToDateTime(sms["ToDate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                String Message = sms["Message"];
                int MessageType = Convert.ToInt32(sms["MessageType"]);
                String AuthKey = sms["AuthenticationKey"];
                String APiKey = sms["APIKey"];
                String PortNo = sms["PortNo"];
                //SendSmsClass1 sm1 = new SendSmsClass1();
                //sm1.InsertServiceProviders1(ServiceProvider, Description, Prty, UserName, PassWord, SenderId, SendSMSUrl, SentReportUrl, ChechBalanceUrl, MobileNo, FromDate, ToDate, Message, MessageType);

                using (SqlConnection con = new SqlConnection(Connection.Con))
                {
                    SqlCommand cmd = new SqlCommand("InsServicePro", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Direction", direction);
                    cmd.Parameters.Add("@COLUMN02", Id1);
                    cmd.Parameters.Add("@COLUMNA03", acOW);
                    cmd.Parameters.AddWithValue("@COLUMN03", ServiceProvider);
                    cmd.Parameters.AddWithValue("@COLUMN04", Description);
                    cmd.Parameters.AddWithValue("@COLUMN05", Priority);
                    cmd.Parameters.AddWithValue("@COLUMN06", UserName);
                    cmd.Parameters.AddWithValue("@COLUMN07", PassWord);
                    cmd.Parameters.AddWithValue("@COLUMN08", SenderId);
                    cmd.Parameters.AddWithValue("@COLUMN09", SendSMSUrl);
                    cmd.Parameters.AddWithValue("@COLUMN10", SentReportUrl);
                    cmd.Parameters.AddWithValue("@COLUMN11", ChechBalanceUrl);
                    cmd.Parameters.AddWithValue("@COLUMN12", MobileNo);
                    cmd.Parameters.AddWithValue("@COLUMN13", DateTime.ParseExact(FromDate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@COLUMN14", DateTime.ParseExact(ToDate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@COLUMN15", Message);
                    cmd.Parameters.AddWithValue("@COLUMN16", MessageType);
                    cmd.Parameters.AddWithValue("@COLUMN17", AuthKey);
                    cmd.Parameters.AddWithValue("@COLUMN18", APiKey);
                    cmd.Parameters.AddWithValue("@COLUMN19", PortNo);
                    con.Open();
                    int r = 0;
                    r = cmd.ExecuteNonQuery();
                    if (r > 0)
                    {
                        var msg = string.Empty;
                        msg = "ServiceProvider Successfully Updated";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    return RedirectToAction("SmsSettings");
                }
            }
            catch (Exception ex)
            {
                var msg = "ServiceProvider Updation Failed";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SmsSettings");
            }
        }
        public ActionResult DeleteServiceProvider(string Id)
        {
            try
            {
                string direction = "Delete";
                int IdDel = Convert.ToInt32(Id);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
               using( SqlConnection con = new SqlConnection(Connection.Con))
                {
                SqlCommand cmd = new SqlCommand("ServiceProCRUD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@COLUMN02", IdDel);
                cmd.Parameters.Add("@Direction", direction);
                cmd.Parameters.Add("@COLUMNA03", acOW);
                con.Open();
                int res = cmd.ExecuteNonQuery();
                if (res > 0)
                    {
                        var msg = string.Empty;
                        msg = "ServiceProvider Successfully Deleted";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    return RedirectToAction("SmsSettings");
                }
                //return RedirectToAction("SmsSettings");
                //}
            }
            catch(Exception ex)
            {
                var msg = "ServiceProvider Deletion Failed";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SmsSettings", "SendMailer");
            
            }
        }


        private static string GetPageContent(string FullUri)
        {

            try
            {
                HttpWebRequest Request;
                StreamReader ResponseReader;
                Request = ((HttpWebRequest)(WebRequest.Create(FullUri)));
                ResponseReader = new StreamReader(Request.GetResponse().GetResponseStream());
                return ResponseReader.ReadToEnd();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";

                return null;
            }
        }

        public ActionResult GetPersons(string ID)
        {
            try
            {
                SelectList vendor = getpayees(ID);
                return Json(new { item = vendor }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InstantSms", "SendMailer");
            }
        }

        public SelectList getpayees(string ID)
        {
            try
            {
                List<SelectListItem> vendor = new List<SelectListItem>();
                var cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                using (SqlConnection scn = new SqlConnection(cn))
                {
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(cn, providerName);
                    SqlCommand cmd = new SqlCommand("usp_PROC_TP_GETPayee", scn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@Type", ID);
                    cmd.Parameters.AddWithValue("@OPUnit", Session["OPUnit"]);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    for (int dd = 0; dd < dt1.Rows.Count; dd++)
                    {
                        vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN05"].ToString() });
                    }
                    ViewBag.cust = new SelectList(vendor, "Value", "Text", selectedValue: "");
                    return ViewBag.cust;
                }
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return null;
            }
        }

        public ActionResult GetMails(string ID)
        {

            try
            {
                List<SelectListItem> vendor = new List<SelectListItem>();
                string PayeeType = "";
                if (ID == "22371")
                {
                    PayeeType = "Vendor";
                    string dSval = "";
                    //var dropdata = dc.Where(a => a.COLUMN04 == countryId).ToList();
                    var cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    SqlConnection scn = new SqlConnection(cn);
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(cn, providerName);
                    var dropdata = "select COLUMN13 COLUMN02,COLUMN06 COLUMN05 FROM MATABLE010 where COLUMN30 =1   AND ( COLUMNA02='" + Session["OPUnit"] + "'  or COLUMNA02 is null) AND  COLUMNA03='" + Session["AcOwner"] + "' AND COLUMN13!=''";
                    SqlCommand cmd = new SqlCommand(dropdata, scn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    for (int dd = 0; dd < dt1.Rows.Count; dd++)
                    {
                        vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN05"].ToString() });
                    }
                    ViewBag.cust = new SelectList(vendor, "Value", "Text", selectedValue: dSval);
                }
                else if (ID == "22368")
                {
                    PayeeType = "Customer";
                    string dSval = "";
                    //var dropdata = dc.Where(a => a.COLUMN04 == countryId).ToList();
                    var cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    SqlConnection scn = new SqlConnection(cn);
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(cn, providerName);
                    var dropdata = "select COLUMN10 COLUMN02,COLUMN05 FROM SATABLE002   Where ( COLUMNA02='" + Session["OPUnit"] + "' or COLUMNA02 is null) AND COLUMN02= '" + Session["customerId"] + "'  AND  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN10!=''";
                    SqlCommand cmd = new SqlCommand(dropdata, scn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    for (int dd = 0; dd < dt1.Rows.Count; dd++)
                    {
                        vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN05"].ToString() });
                    }
                    ViewBag.cust = new SelectList(vendor, "Value", "Text", selectedValue: dSval);
                }
                else
                {
                    PayeeType = "Vendor";
                    string dSval = "";
                    //var dropdata = dc.Where(a => a.COLUMN04 == countryId).ToList();
                    var cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                    SqlConnection scn = new SqlConnection(cn);
                    var providerName = "System.Data.SqlClient";
                    var dbs = Database.OpenConnectionString(cn, providerName);
                    var dropdata = "select COLUMN11 COLUMN02,COLUMN05 FROM SATABLE001 where   (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) AND  COLUMNA03='" + Session["AcOwner"] + "'AND COLUMN02= '" + Session["vendorId"] + "'  AND COLUMN11!=''";
                    SqlCommand cmd = new SqlCommand(dropdata, scn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt1 = new DataTable();
                    da.Fill(dt1);
                    for (int dd = 0; dd < dt1.Rows.Count; dd++)
                    {
                        vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN05"].ToString() });
                    }
                    ViewBag.cust = new SelectList(vendor, "Value", "Text", selectedValue: dSval);
                }
                return Json(new { item = vendor }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InstantSms", "SendMailer");
            }
        }

        public ActionResult FacebookLogin()
        {
            Session["accessToken"] = "CAAWSylDKzAEBAPD7qp5OjUlHGQuwHtyPFMFf36ZC6hs6ggBiSZAG5Rd4hf1oXzLiLKZCJ7GSylZBrS4DQNS7McrlSSwRPDe9757CYZC1iLTBPI4Lnif4mN4q5lYNHcMTUp9eVrLKDIsVu95nCLUGWEZA1fkgHoiBzDkCbm8v0DN1sPfwksgZCURikdQiaOeHKdUNXSG3Noyc8vK1X0MH07l";
            Session["uid"] = "101362076866271";
            return View();
        }

        [HttpPost]
        public ActionResult FacebookLogin(WallMessageModel model)
        {
            //try
            //{
            //    var postValues = new Dictionary<string, string>();

            //    // list of available parameters available @ http://developers.facebook.com/docs/reference/api/post
            //    postValues.Add("access_token", Session["accessToken"].ToString());
            //    postValues.Add("message", model.message);

            //    string facebookWallMsgId = string.Empty;
            //    string response;
            //    MethodResult header = Helper.SubmitPost(string.Format("https://graph.facebook.com/{0}/feed", Session["uid"].ToString()),
            //                                                Helper.BuildPostString(postValues),
            //                                                out response);

            //    if (header.returnCode == MethodResult.ReturnCode.Success)
            //    {
            //        var deserialised =
            //            Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(response);
            //        facebookWallMsgId = deserialised["id"];

            //        return RedirectToAction("CreatedSuccessfully");
            //    }
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            string postUrl = "https://graph.facebook.com/101362076866271/feed";
            string postParameters;
            System.Net.WebRequest req = System.Net.WebRequest.Create(postUrl);

            postParameters = string.Format("access_token={0}&message={1}", Session["accessToken"], "" + model.message + "");

            try
            {

                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = "POST";
                req.UseDefaultCredentials = true;
                req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postParameters);
                req.ContentLength = bytes.Length;
                using (System.IO.Stream os = req.GetRequestStream())
                {
                    os.Write(bytes, 0, bytes.Length); //Push it out there
                    os.Close();
                    using (WebResponse resp = req.GetResponse())
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        ViewBag.PostResult = sr.ReadToEnd().Trim();
                        sr.Close();
                    }
                    os.Close();
                }
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1383 && q.COLUMN05 == 0).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Success..... ";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                return RedirectToAction("SocialSites");
            }
            catch (Exception ex)
            {
                //string pageContent = new StreamReader(resp.Response.GetResponseStream()).ReadToEnd().ToString();
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1383 && q.COLUMN05 == 1).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Failed..... ";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SocialSites");
                //throw new Exception("An error ocurred while posting data to the user's wall: " + postUrl + "?" + postParameters, ex);
            }

            ////return RedirectToAction(XXXXXXXXXXXxx....); //Then i redirect to another page.

            //    }
            //    catch
            //    {

            //    }

            //return RedirectToAction("WallMessageError");
        }

        [HttpPost]
        public ActionResult Twitter(WallMessageModel model)
        {
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            try
            {
                var service = new TwitterService("lcGrvHVX0kbJoFyMQR8EJ9aVK", "JSOENKcbADy8SqlZUmZz8xZpj5s1DbfXhqjF3FRWEkcA7OuyGi");

                // Using the values Twitter sent back, get an access token from Twitter
                var accessToken = service.GetAccessToken(new OAuthRequestToken { Token = "3189602196-97HiGUuQOxkajEcAf79FBQ9fqMq9SEFHMP27p8b" }, "fsOCpwDB2cC7UUdxvN5RSued2TNKmGuQNje3kwbvkk6Eh");

                // Use that access token and send a tweet on the user's behalf
                service.AuthenticateWith("3189602196-97HiGUuQOxkajEcAf79FBQ9fqMq9SEFHMP27p8b", "fsOCpwDB2cC7UUdxvN5RSued2TNKmGuQNje3kwbvkk6Eh");
                var result = service.SendTweet(new SendTweetOptions { Status = model.message });

                // Maybe check the "result" for success or failure?
                //var oauth = new OAuth.Manager();
                //oauth["consumer_key"] = "lcGrvHVX0kbJoFyMQR8EJ9aVK";
                //oauth["consumer_secret"] = "JSOENKcbADy8SqlZUmZz8xZpj5s1DbfXhqjF3FRWEkcA7OuyGi";
                //oauth["token"] = "3189602196-97HiGUuQOxkajEcAf79FBQ9fqMq9SEFHMP27p8b";
                //oauth["token_secret"] = "fsOCpwDB2cC7UUdxvN5RSued2TNKmGuQNje3kwbvkk6Eh";
                //var url = "https://api.twitter.com/1/statuses/update.xml?status=" + model.message + "";
                //var authzHeader = oauth.GenerateAuthzHeader(url, "POST");

                //var request = (HttpWebRequest)WebRequest.Create(url);
                //request.Method = "POST";
                //request.Headers.Add("Authorization", authzHeader);
                //using (var response = (HttpWebResponse)request.GetResponse())
                //{
                //    //http://api.twitter.com/1/statuses/update.json
                //    //if (response.StatusCode != HttpStatusCode.OK)
                //    //    MessageBox.Show("There's been a problem trying to tweet:" +
                //    //                    Environment.NewLine +
                //    //                    response.StatusDescription +
                //    //                    Environment.NewLine +
                //    //                    Environment.NewLine +
                //    //                    "You will have to tweet manually." +
                //    //                    Environment.NewLine);
                //}
                //OAuthTokens tokens = new OAuthTokens();

                //tokens.ConsumerKey = "lcGrvHVX0kbJoFyMQR8EJ9aVK";
                //tokens.ConsumerSecret = "JSOENKcbADy8SqlZUmZz8xZpj5s1DbfXhqjF3FRWEkcA7OuyGi";
                //tokens.AccessToken = "3189602196-97HiGUuQOxkajEcAf79FBQ9fqMq9SEFHMP27p8b";
                //tokens.AccessTokenSecret = "fsOCpwDB2cC7UUdxvN5RSued2TNKmGuQNje3kwbvkk6Eh";
                //var result = TwitterStatus.Update(tokens, "This is a sample tweet!");
                if (result != null)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1383 && q.COLUMN05 == 0).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Success..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1383 && q.COLUMN05 == 1).FirstOrDefault();
                    var msg = string.Empty;
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Failed..... ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                    return RedirectToAction("SocialSites");
                }
                return RedirectToAction("SocialSites");
            }
            catch (Exception ex)
            {
                var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1382 && q.COLUMN05 == 1).FirstOrDefault();
                var msg = string.Empty;
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                    msg = "Failed..... ";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SocialSites");
                //throw new Exception("An error ocurred while posting data to the user's wall: " + postUrl + "?" + postParameters, ex);
            }
        }

        public ActionResult Twitter()
        {
            return View();
        }

        public ActionResult SocialSites()
        {
            Session["accessToken"] = "CAAWSylDKzAEBAPD7qp5OjUlHGQuwHtyPFMFf36ZC6hs6ggBiSZAG5Rd4hf1oXzLiLKZCJ7GSylZBrS4DQNS7McrlSSwRPDe9757CYZC1iLTBPI4Lnif4mN4q5lYNHcMTUp9eVrLKDIsVu95nCLUGWEZA1fkgHoiBzDkCbm8v0DN1sPfwksgZCURikdQiaOeHKdUNXSG3Noyc8vK1X0MH07l";
            Session["uid"] = "101362076866271"; 
            return View();
        }

        public string MailPassword(MailModel _objModelMail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(_objModelMail.To);
                mail.From = new MailAddress(_objModelMail.From, "Accnu ERP");
                mail.Subject = _objModelMail.Subject;
                string Body = _objModelMail.Body;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                ("emphorasoft@gmail.com", "Welcome12");
                smtp.EnableSsl = true;
                smtp.Send(mail);
                return "1";
            }
            catch
            {
                return "0";
            }
        }

        //Send Sms Adding By Venkat.K
        [HttpGet]
        public ActionResult InstantSms()
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                var Oper = Session["OPUnit"];
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                List<SelectListItem> lov = new List<SelectListItem>();
                SqlDataAdapter cmdj1 = new SqlDataAdapter("select COLUMN02, COLUMN04 from MATABLE002  where   COLUMN03 = 11137  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtj1 = new DataTable();
                cmdj1.Fill(dtj1);
                for (int dd = 0; dd < dtj1.Rows.Count; dd++)
                {
                    lov.Add(new SelectListItem { Value = dtj1.Rows[dd]["COLUMN02"].ToString(), Text = dtj1.Rows[dd]["COLUMN04"].ToString() });
                }
                lov = lov.OrderBy(x => x.Text).ToList();
                ViewData["lov"] = new SelectList(lov, "Value", "Text");
                SelectList payee = getpayees("22368");
                ViewBag.cust = payee;
                //ViewData["cust"] = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InstantSms", "SendMailer");
            }
        }


        public ActionResult GetSalesRep(string Idforsel)
        {
            try
            {
                //SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());

                List<SelectListItem> vendor = new List<SelectListItem>();
                string PayeeType = "";
                string dSval = "";
                string IDFORSEL = Idforsel;
                var Oper = Session["OPUnit"];
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                SqlConnection con = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("SmsClientsGetting", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COLUMNA02", Oper);
                cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                cmd.Parameters.AddWithValue("@IdForSelection", IDFORSEL);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                for (int dd = 0; dd < dt1.Rows.Count; dd++)
                {
                    vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN05"].ToString() });
                }
                //EMPHCS1724	Sorted dropdown list by venkat 2/5/2016
                vendor = vendor.OrderBy(x => x.Text).ToList();
                ViewBag.cust = new SelectList(vendor, "Value", "Text", selectedValue: dSval);
                //ViewBag.aList = new SelectList(account, "Value", "Text");
                return Json(new { item = vendor }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return RedirectToAction("InstantSms", "SendMailer", new { FormName = Session["FormName"] });
            }
       }


        [HttpPost]
        public ActionResult InstantSms(FormCollection sms)
        {

            try
            {
                var Idd=""; 
                DateTime dateTime=DateTime.Now;             
                var Oper = Session["OPUnit"];
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                string CustType = sms["SendTo"];
                string custidS =Convert.ToString(sms["SendPerson"]);
                //String custidS = custid.Remove(0, 1);

                SqlConnection con = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("GettingClients", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CustType", CustType);
                cmd.Parameters.AddWithValue("@COLUMN02", custidS);
                cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                //cmd.Parameters.AddWithValue("@COLUMN08", custidS);            
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ta = new DataTable();
                da.Fill(ta);
                string MobileNos = "";
                string custName = "";
                for (int dd = 0; dd < ta.Rows.Count; dd++)
                {
                    //vendor.Add(new SelectListItem { Value = dt1.Rows[dd]["COLUMN02"].ToString(), Text = dt1.Rows[dd]["COLUMN05"].ToString() });
                    var x = "";
                    if (dd < ta.Rows.Count-1 )
                    {
                        x = ",";
                    }
                    else
                    {
                        x = "";
                    }
                    MobileNos += ta.Rows[dd]["COLUMN009"].ToString() + x; custName += ta.Rows[dd]["COLUMN008"].ToString() + x;

                }
              



                //write Code for getting Service Provider Whose Priority Is One
               SqlConnection con2 = new SqlConnection(Connection.Con);
               SqlCommand cmd2 = new SqlCommand("select Column06 username,Column07 password,COLUMN09 Url from setable023 where column05=1 and columna03=" + acOW + "", con2);
               SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
               DataTable ta2 = new DataTable();
               da2.Fill(ta2);
                //string username = "EMPHORA";
                //string password = "emphora@123";
                //string tos = sms["SendPerson"];
                //String to = MobileNos.Remove(0, 1);                
                //string from = "EMPHRA";
               string username = ta2.Rows[0]["username"].ToString();
               string password =Convert.ToString(ta2.Rows[0]["password"]);
               string from = "EMPHRA";
               string message = sms["msg"];
               string smsurl =Convert.ToString(ta2.Rows[0]["Url"]);
               //string url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=" + username + "&password=" + password + "&to=" + MobileNos + "&from=" + from + "&message=" + message + "";
               string url = ""+smsurl +"?username=" + username + "&password=" + password + "&to=" + MobileNos + "&from=" + from + "&message=" + message + "";
                String result = GetPageContent(url);




                string clientType = CustType;
                string direction = "Add";
                SqlConnection con1 = new SqlConnection(Connection.Con);
                SqlCommand cmd1 = new SqlCommand("SendSMSList", con1);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@COLUMNA02", Oper);
                cmd1.Parameters.AddWithValue("@COLUMNA03", acOW);
                cmd1.Parameters.AddWithValue("@Direction", direction);
                cmd1.Parameters.AddWithValue("@COLUMN05", MobileNos);
                cmd1.Parameters.AddWithValue("@COLUMN06", message);
                //cmd1.Parameters.AddWithValue("@CustType", CustType);
                cmd1.Parameters.AddWithValue("@COLUMN03", custName);
                DateTime now = DateTime.Now;
                var frmDT = now.ToString(Session["FormatDate"].ToString());
                cmd1.Parameters.AddWithValue("@COLUMN04", dateTime);
                cmd1.Parameters.AddWithValue("@COLUMN07", clientType);
                cmd1.Parameters.AddWithValue("@COLUMN08", custidS); 
                con1.Open();
                int r = 0;
                r = cmd1.ExecuteNonQuery();
                //con1.Close();
                //return View();
                //string url = "http://reseller.siegsms.com/sendingsms.aspx";
                //string url = "http://onlinebulksmslogin.com/spanelv2/home.php";
                //WebClient wc = new WebClient();
                //wc.BaseAddress = url;
                //// appending parameters
                //wc.QueryString["username"] = "emphora";
                //wc.QueryString["password"] = "emphora@123";
                //wc.QueryString["to"] = sms["Numbers"];
                //wc.QueryString["message"] = sms["Message"];
                //wc.QueryString["from"] = "9000482706";
                //Stream sr = wc.OpenRead(url);
                //StreamReader st = new StreamReader(sr);
                //// Reading Response Code
                //string response = st.ReadToEnd();
                //st.Close();
                //sr.Close();
                // Create a request using a URL that can receive a post.
                //WebRequest request =
                //WebRequest.Create("http://reseller.siegsms.com/Testing_BulkSMS.aspx");
                //// Set the Method property of the request to POST.
                //request.Method = "POST";
                //// Create POST data and convert it to a byte array.
                //string Msg = HttpUtility.UrlEncode(sms["Message"]);
                //string postData = "userid=reseller&pass=welcome123&phone=" + sms["Numbers"] + "&msg=" + Msg;
                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                //// Set the ContentType property of the WebRequest.
                //request.ContentType = "application/x-www-form-urlencoded";
                //// Set the ContentLength property of the WebRequest.
                //request.ContentLength = byteArray.Length;
                //// Get the request stream.
                //Stream dataStream = request.GetRequestStream();
                //// Write the data to the request stream.
                //dataStream.Write(byteArray, 0, byteArray.Length);
                //// Close the Stream object.
                //dataStream.Close();
                //// Get the response.
                //WebResponse response = request.GetResponse();
                //// Get the stream containing content returned by the server.
                //dataStream = response.GetResponseStream();
                //// Open the stream using a StreamReader for easy access.
                //StreamReader reader = new StreamReader(dataStream);
                //// Read the content.
                //string responseFromServer = reader.ReadToEnd();
                //// Clean up the streams.
                //reader.Close();
                //dataStream.Close();
                //response.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    msg = "Instant SMS Successfully Sent";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //return Json("");
                //ViewData["SuccessMsg"] = "Message sent successfully";
                return RedirectToAction("InstantSMSInfo", "SendMailer");  
            }
            catch
            {
                var msg = "Instant SMS Failed";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";

                return RedirectToAction("Info","EmployeeMaster");
                //ViewData["SuccessMsg"] = "Message sending failed";
                //return RedirectToAction("InstantSms");
            }
            //eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
            //eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
            //var Oper = Session["OPUnit"];
            //var acOW = Convert.ToInt32(Session["AcOwner"]);
            //var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
            //var cust = activity.SATABLE002.Where(a => a.COLUMNA02 == Oper && a.COLUMNA03 == acOW).ToList();
            //ViewBag.smssend = lov;
            //ViewBag.cust = cust;
            
        }


        [HttpGet]
        public ActionResult SentSms()
        {
            try
            {
                string direction = "GetSmsList";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var Oper = Session["OPUnit"];
                List<string> MessageList = new List<string>();
                MessageList.Add("ID");
                MessageList.Add("CustomerName");
                MessageList.Add("DateTime");
                MessageList.Add("PhoneNumbers");
                MessageList.Add("Message");
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);

                //ViewBag.sortselected = Session["SortByStatus"];
               // ViewBag.viewselected = Session["ViewStyle"];

              //var GData = dbs.Query("SendSMSList @Direction=" + direction + ", @COLUMNA02=" + Oper + " , @COLUMNA03=" + acOW + "");
                //string dateF = System.Web.HttpContext.Current.Session["Format"].ToString();
                string dateF = Convert.ToString(Session["FormatDate"]);
                var GData = dbs.Query(" Select COLUMN02 as ID,COLUMN03 as 'CustomerName',FORMAT(COLUMN04,'" + dateF + "') as 'DateTime',COLUMN05 as 'PhoneNumbers',COLUMN06 as 'Message'  from SETABLE024 where COLUMNA02 =" + Oper + " and COLUMNA03 =" + acOW + " order by column02 desc");
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                ViewBag.Columns = MessageList;
                if (Session["SDT"] != null)
                {
                    ViewBag.Grid = Session["SDT"];
                    Session["SDT"] = null;
                }
                else
                    ViewBag.Grid = GData;
                return View("~/Views/SendMailer/SentSms.cshtml", new { FormName = Session["FormName"] });
            }

            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("SentSms", "SendMailer");
            }

            
        }
        [HttpGet]
        public ActionResult InstantSMSInfo()
        {
            try
            {
                string direction = "GetSmsList";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var Oper = Session["OPUnit"];
                List<string> MessageList = new List<string>();
                MessageList.Add("");
                MessageList.Add("CustomerName");
                MessageList.Add("DateTime");
                MessageList.Add("PhoneNumbers");
                MessageList.Add("Message");
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);

                //ViewBag.sortselected = Session["SortByStatus"];
                // ViewBag.viewselected = Session["ViewStyle"];

                //var GData = dbs.Query("SendSMSList @Direction=" + direction + ", @COLUMNA02=" + Oper + " , @COLUMNA03=" + acOW + "");
                //string dateF = System.Web.HttpContext.Current.Session["Format"].ToString();
                string dateF = Session["FormatDate"].ToString();
                var GData = dbs.Query("Select COLUMN02,COLUMN03 as 'CustomerName',FORMAT(COLUMN04,'" + dateF + "') as 'DateTime',COLUMN05 as 'PhoneNumbers',COLUMN06 as 'Message'  from SETABLE024 where COLUMNA02 in(SELECT ListValue FROM dbo.FN_ListToTable(',','" + Oper + "') s)  and COLUMNA03 =" + acOW + " and isnull(COLUMNA13,0) =0 order by column02 desc");
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                ViewBag.Columns = MessageList;
                if (Session["SDT"] != null)
                {
                    ViewBag.Grid = Session["SDT"];
                    Session["SDT"] = null;
                }
                else
                    ViewBag.Grid = GData;
                return View("~/Views/SendMailer/InstantSMSInfo.cshtml", new { FormName = Session["FormName"] });
            }

            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }


        }
        public ActionResult UpdateSentSms(String ide)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var billdb = Database.OpenConnectionString(connectionString, providerName);
                var acOW = Convert.ToInt32(Session["AcOwner"]);

                SqlConnection con = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("SendSMSList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@column02", ide);
                cmd.Parameters.AddWithValue("@Direction", "UpdateSentMsg");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ta = new DataTable();
                da.Fill(ta);
                var SendTo = "";
                var SendPerson = "";
                var Party = "";
                var Message = "";
                if (ta.Rows.Count > 0)
                {
                    SendTo = ta.Rows[0]["COLUMN07"].ToString();
                    SendPerson = ta.Rows[0]["COLUMN08"].ToString();
                    Message = ta.Rows[0]["COLUMN06"].ToString();
                    Party = GetPartyinfo(SendTo, SendPerson);
                }
                var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                ViewBag.requestid = ide;
                ViewBag.SendToval = SendTo;
                ViewBag.SendPerson = SendPerson;
                ViewBag.txtmsg = Message;

                ViewBag.smssend = lov;
                //SelectList payee = getpayees(SendTo);
                //payee = new SelectList(payee, "Value", "Text", selectedValue: SendPerson);
                //ViewBag.cust = payee;
                ViewBag.cust = Party;
                return View();
            }
            catch (Exception ex)
            {
                var msg = " Failed......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InstantSMSInfo", "SendMailer");
            }
        }


        public ActionResult EditSentSms(String ide)
        {
            try
            {
                string direction = "UpdateSentMsg";
                int Id1 = Convert.ToInt32(ide);
                SqlConnection con = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("SendSMSList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@column02", Id1);
                cmd.Parameters.AddWithValue("@Direction", direction);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ta = new DataTable();
                da.Fill(ta);
                var ID = Id1;
                var SendTo = "";
                var SendPerson = "";
                var Message = "";
                if (ta.Rows.Count > 0)
                {
                     SendTo = Convert.ToString(ta.Rows[0]["COLUMN07"]);
                     SendPerson = Convert.ToString(ta.Rows[0]["COLUMN08"]);
                     Message = Convert.ToString(ta.Rows[0]["COLUMN06"]);
                }
                return Json(new { a1 = SendTo, b1 = SendPerson, c1 = Message, e1 = ID },
                JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = " Failed......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InstantSMSInfo", "SendMailer");
            }
        }

        [HttpPost]
        public ActionResult UpdateSentSms(FormCollection fc)
        {
            try
            {
            //string username = "EMPHORA";
            //string password = "emphora@123";
            //string tos = sms["SendPerson"];
            //String to = MobileNos.Remove(0, 1);                
            //string from = "EMPHRA";
            //string message = fc["msg"];
            var acOW = Convert.ToInt32(Session["AcOwner"]);

            SqlConnection con = new SqlConnection(Connection.Con);
            SqlCommand cmd = new SqlCommand("GettingClients", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CustType", fc["SendType"]);
            cmd.Parameters.AddWithValue("@COLUMN02", fc["chkSendPerson"]);
            cmd.Parameters.AddWithValue("@COLUMNA03", acOW);          
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ta = new DataTable();
            da.Fill(ta);
            string MobileNos = "";
            string custName = "";
            for (int dd = 0; dd < ta.Rows.Count; dd++)
            {
                var x = "";
                if (dd < ta.Rows.Count - 1)
                {
                    x = ",";
                }
                else
                {
                    x = "";
                }
                MobileNos += ta.Rows[dd]["COLUMN009"].ToString() + x; custName += ta.Rows[dd]["COLUMN008"].ToString() + x;
            }
            SqlConnection con2 = new SqlConnection(Connection.Con);
            SqlCommand cmd2 = new SqlCommand("select Column06 username,Column07 password,COLUMN09 Url from setable023 where column05=1 and columna03=" + acOW + "", con2);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable ta2 = new DataTable();
            da2.Fill(ta2);
            //string username = "EMPHORA";
            //string password = "emphora@123";
            //string tos = sms["SendPerson"];
            //String to = MobileNos.Remove(0, 1);                
            //string from = "EMPHRA";
            string username =Convert.ToString(ta2.Rows[0]["username"]);
            string password =Convert.ToString(ta2.Rows[0]["password"]);
            string from = "EMPHRA";
            string message = fc["msg"];
            string smsurl =Convert.ToString( ta2.Rows[0]["Url"]);

            //string url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=" + username + "&password=" + password + "&to=" + MobileNos + "&from=" + from + "&message=" + message + "";
            string url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=" + username + "&password=" + password + "&to=" + MobileNos + "&from=" + from + "&message=" + message + "";
                String result = GetPageContent(url);



            string direction = "ResendMessage";
            
            SqlCommand cmdu = new SqlCommand("SendSMSList", con);
            cmdu.CommandType = CommandType.StoredProcedure;
            cmdu.Parameters.AddWithValue("@column02", fc["requestid"]);
            cmdu.Parameters.AddWithValue("@column06", fc["msg"]);
            cmdu.Parameters.AddWithValue("@COLUMNA03", acOW);
            cmdu.Parameters.AddWithValue("@Direction", direction);
            con.Open();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataTable ta = new DataTable();
            int status = 0;
            status= cmdu.ExecuteNonQuery();
           con.Close();

           if (status > 0)
           {
               var msg = string.Empty;
               msg = " Instant SMS Successfully Update";
               Session["MessageFrom"] = msg;
               Session["SuccessMessageFrom"] = "Success";
           }
           //return Json("");
           //ViewData["SuccessMsg"] = "Message sent successfully";
           return RedirectToAction("InstantSMSInfo","SendMailer");
            }
            catch
            {
                var msg = " Instant SMS Updation Failed";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";

                return RedirectToAction("InstantSMSInfo", "SendMailer");
                //ViewData["SuccessMsg"] = "Message sending failed";
                //return RedirectToAction("InstantSms");
            }
        }

        public ActionResult ViewMessage(String ID)
        {
            try
            {
                return View();
            }
            catch(Exception ex)
            {
            return View();
            }
        }
        [HttpPost]
        public ActionResult SentSms(String abc)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult InstantSMSInfo(String abc)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster");
            }
        }
        public ActionResult DeleteMsg(String Id)
        {
            try
            {
                string direction = "Delete";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var Oper = Session["OPUnit"];
                var IdL = Id;
                var IdList = IdL.Replace(" ,", "");
                using (SqlConnection con = new SqlConnection(Connection.Con))
                {
                    SqlCommand cmd = new SqlCommand("SendSMSList", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@COLUMNA02", Oper);
                    cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                    cmd.Parameters.AddWithValue("@Direction", direction);
                    cmd.Parameters.AddWithValue("@COLUMN10", IdList);
                    con.Open();
                    int r = 0;
                    r = cmd.ExecuteNonQuery();
                     if (r > 0)
                    {
                        var msg = string.Empty;
                        msg = " Instant SMS Successfully Deleted ";
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                    }
                    //return Json("");
                    //ViewData["SuccessMsg"] = "Message sent successfully";
                    return RedirectToAction("InstantSMSInfo");
                }
            }
            catch
            {
                var msg = "Instant SMS Deleted Failed";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";
               return RedirectToAction("InstantSMSInfo");
                //ViewData["SuccessMsg"] = "Message sending failed";
                //return RedirectToAction("InstantSms");
            }
        }
        public string GetPartyinfo(string ID,string Party)
        {
            var text = new StringBuilder();
            var cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
            using (SqlConnection scn = new SqlConnection(cn))
            {
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(cn, providerName);
                SqlCommand cmd = new SqlCommand("usp_PROC_TP_GETPayee", scn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@Type", ID);
                cmd.Parameters.AddWithValue("@Party", Party);
                cmd.Parameters.AddWithValue("@OPUnit", Session["OPUnit"]);
                cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        text.Append(dt1.Rows[i]["COLUMN05"].ToString()); text.Append("\n");
                    }
                }
                return text.ToString();
            }
            return text.ToString();
        }
               
    
        public ActionResult SentSmsView(String ide)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                eBizSuiteTableEntities ed = new eBizSuiteTableEntities();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var billdb = Database.OpenConnectionString(connectionString, providerName);
                var acOW = Convert.ToInt32(Session["AcOwner"]);

                SqlConnection con = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("SendSMSList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@column02", ide);
                cmd.Parameters.AddWithValue("@Direction", "UpdateSentMsg");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ta = new DataTable();
                da.Fill(ta);
                var SendTo = "";
                var SendPerson = "";
                var Party = "";
                var Message = "";
                if (ta.Rows.Count > 0)
                {
                    SendTo =Convert.ToString( ta.Rows[0]["COLUMN07"]);
                    SendPerson =Convert.ToString(ta.Rows[0]["COLUMN08"]);
                    Party=GetPartyinfo(SendTo, SendPerson);
                    Message =Convert.ToString(ta.Rows[0]["COLUMN06"]);
                }
                var lov = ed.MATABLE002.Where(q => q.COLUMN03 == 11137).ToList();
                ViewBag.requestid = ide;
                ViewBag.SendToval = SendTo;
                ViewBag.SendPerson = SendPerson;
                ViewBag.txtmsg = Message;

                ViewBag.smssend = lov;
                //SelectList payee = getpayees(SendTo);
                //payee = new SelectList(payee, "Value", "Text", selectedValue: SendPerson);
                ViewBag.cust = Party;
                return View();
            }
            catch(Exception ex)
            {
                var msg = " Failed......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("InstantSMSInfo", "SendMailer");
            }
        }
        [HttpPost]
        public ActionResult SentSmsView(FormCollection fc)
        {
            try
            {
                string username = "EMPHORA";
                string password = "emphora@123";
                //string tos = sms["SendPerson"];
                //String to = MobileNos.Remove(0, 1);                
                string from = "EMPHRA";
                string message = fc["msg"];
                var acOW = Convert.ToInt32(Session["AcOwner"]);

                SqlConnection con = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("GettingClients", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CustType", fc["SendType"]);
                cmd.Parameters.AddWithValue("@COLUMN02", fc["chkSendPerson"]);
                cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ta = new DataTable();
                da.Fill(ta);
                string MobileNos = "";
                string custName = "";
                for (int dd = 0; dd < ta.Rows.Count; dd++)
                {
                    var x = "";
                    if (dd < ta.Rows.Count - 1)
                    {
                        x = ",";
                    }
                    else
                    {
                        x = "";
                    }
                    MobileNos += ta.Rows[dd]["COLUMN009"].ToString() + x; custName += ta.Rows[dd]["COLUMN008"].ToString() + x;
                }

                string url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=" + username + "&password=" + password + "&to=" + MobileNos + "&from=" + from + "&message=" + message + "";
                String result = GetPageContent(url);



                string direction = "ResendMessage";

                SqlCommand cmdu = new SqlCommand("SendSMSList", con);
                cmdu.CommandType = CommandType.StoredProcedure;
                cmdu.Parameters.AddWithValue("@column02", fc["requestid"]);
                cmdu.Parameters.AddWithValue("@column06", fc["msg"]);
                cmdu.Parameters.AddWithValue("@COLUMNA03", acOW);
                cmdu.Parameters.AddWithValue("@Direction", direction);
                con.Open();
                //SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataTable ta = new DataTable();
                int status = 0;
                status = cmdu.ExecuteNonQuery();
                con.Close();

                if (status > 0)
                {
                    var msg = string.Empty;
                    msg = " Message Successfully Sended ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                //return Json("");
                //ViewData["SuccessMsg"] = "Message sent successfully";
                return RedirectToAction("UpdateSentSms");
            }
            catch
            {
                var msg = "Message sending Failed";
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "fail";

                return RedirectToAction("UpdateSentSms");
                //ViewData["SuccessMsg"] = "Message sending failed";
                //return RedirectToAction("InstantSms");
            }
        }
    }

}
