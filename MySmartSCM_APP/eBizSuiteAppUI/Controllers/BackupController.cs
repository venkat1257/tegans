﻿using eBizSuiteAppDAL.classes;
using eBizSuiteAppModel.Table;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using MeBizSuiteAppUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Dynamic;
using System.Runtime.InteropServices;


namespace MeBizSuiteAppUI.Controllers
{
    public class BackupController : Controller
    {
        //
        // GET: /Backup/

        [HttpGet]
        public ActionResult Create()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Create1(string FilePath)
        {
            try { 
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                cn.Open();
                SqlCommand cmd = new SqlCommand("BACKUP",cn);
                cmd.Parameters.AddWithValue("@FileName", FilePath);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string val = null;
            //if (dt.Rows.Count > 0)
            //    val = dt.Rows[0][0].ToString();

            val = dt.Rows[0]["Result"].ToString();
            return Json(new { Data1=val}, JsonRequestBehavior.AllowGet);
                cn.Close();
            }
            catch(Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
            //return View();
            //return Json(JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetFilePath()
        {
            try
            {
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                cn.Open();
                SqlCommand cmd = new SqlCommand("Backupfilepath", cn);
                cmd.Parameters.AddWithValue("@FilePath", "");
                cmd.Parameters.AddWithValue("@Direction", "Getpath");
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string val = null;
                //if (dt.Rows.Count > 0)
                //    val = dt.Rows[0][0].ToString();

                val = dt.Rows[0]["COLUMN02"].ToString();
                return Json(new { Data1 = val }, JsonRequestBehavior.AllowGet);
                cn.Close();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "FormBuilding", new { FormName = Session["FormName"] });
            }
            //return View();
            //return Json(JsonRequestBehavior.AllowGet);
        }
    }
}
