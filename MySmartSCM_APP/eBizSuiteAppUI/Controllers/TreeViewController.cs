﻿using eBizSuiteAppModel.Table;
using MvcMenuMaster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace MvcMenuMaster.Controllers
{
    public class TreeViewController : Controller
    {
        //
        // GET: /TreeView/
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Employees(int? Priority)
        {
            List<CONTABLE003> all = new List<CONTABLE003>();
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();

            var db = dc.CONTABLE003.Where(a => a.COLUMN05 != null && a.COLUMNA03 == null && a.COLUMN04==101).ToList();
            int co = db.Count();
            ViewBag.count = co;
            all = dc.CONTABLE003.Where(a => a.COLUMNA03 == null && a.COLUMN04 == 101).ToList();
            return View(all);
        }

        public ActionResult Menu()
        {
            var email = Session["LogedeMailId"];
            List<CONTABLE003> all = new List<CONTABLE003>();
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            var db = dc.CONTABLE003.Where(a => a.COLUMN05 != null).ToList();
            var acOwnr = dc.CONTABLE002.Where(a => a.COLUMN08 == email).ToList();
            var acID = acOwnr.Select(a => a.COLUMN02).FirstOrDefault();
            int co = db.Count();
            ViewBag.count = co;
            ViewBag.Data = dc;
            all = dc.CONTABLE003.Where(a => a.COLUMNA03 == acID).ToList();
            Session["LogedeMailId"] = email;
            return View(all);
        }
        [HttpPost]
        public ActionResult Delete(MenuItems menu)
        {
            MenuItems items = new MenuItems();
            List<CONTABLE003> menu1 = new List<CONTABLE003>();
            string mod = NodeText;
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            var db1 = dc.CONTABLE003.Where(a => a.COLUMN05 == mod).ToList();
            menu1 = dc.CONTABLE003.Where(a => a.COLUMN05 == mod).ToList();
            var parent = (db1.Select(a => a.COLUMN06).ToList());
            int? parentNode = Convert.ToInt32(parent[0]);
            var MId = db1.Select(a => a.COLUMN02).ToList();
            int modid = Convert.ToInt32(MId[0]);
            if (parentNode == null || parentNode == 0)
            {
                var db2 = db.CONTABLE003.Where(a => a.COLUMN02 == modid && a.COLUMNA13 == null).ToList();
                for (int c = 0; c < db2.Count; c++)
                {
                    menu1[0].COLUMNA13 = true;
                    var child = db2.FirstOrDefault().COLUMN02;
                    var col2 = dc.CONTABLE003.Where(a => a.COLUMN06 == child && a.COLUMNA13 == null).ToList();
                    for (int i = 0; i < col2.Count; i++)
                    {
                        col2[i].COLUMNA13 = true;
                        dc.SaveChanges();
                        var scol2 = col2[i].COLUMN02;
                        var sub = dc.CONTABLE003.Where(a => a.COLUMN06 == scol2 && a.COLUMNA13 == null).ToList();
                        for (int j = 0; j < sub.Count; j++)
                        {
                            sub[j].COLUMNA13 = true;
                            dc.SaveChanges();
                            var scol3 = sub[j].COLUMN02;
                            var subsub = dc.CONTABLE003.Where(a => a.COLUMN06 == scol3 && a.COLUMNA13 == null).ToList();
                            for (int k = 0; k < subsub.Count; k++)
                            {
                                subsub[k].COLUMNA13 = true;
                                dc.SaveChanges();
                            }
                        }
                    }
                    dc.SaveChanges();
                } dc.SaveChanges();
            }
            else
            {
                menu1[0].COLUMNA13 = true;
                var child = db1.FirstOrDefault().COLUMN02;
                var col2 = dc.CONTABLE003.Where(a => a.COLUMN06 == child && a.COLUMNA13 == null).ToList();
                for (int i = 0; i < col2.Count; i++)
                {
                    col2[i].COLUMNA13 = true;
                    dc.SaveChanges();
                    var scol2 = col2[i].COLUMN02;
                    var sub = dc.CONTABLE003.Where(a => a.COLUMN06 == scol2 && a.COLUMNA13 == null).ToList();
                    for (int j = 0; j < sub.Count; j++)
                    {
                        sub[j].COLUMNA13 = true;
                        dc.SaveChanges(); var scol3 = sub[j].COLUMN02;
                        var subsub = dc.CONTABLE003.Where(a => a.COLUMN06 == scol3 && a.COLUMNA13 == null).ToList();
                        for (int k = 0; k < subsub.Count; k++)
                        {
                            subsub[k].COLUMNA13 = true;
                            dc.SaveChanges();
                        }
                    }
                }
                dc.SaveChanges();
            }
            return RedirectToAction("Employees");
        }
        static string NodeText;
        //[HttpPost]
        public ActionResult Message(string result)
        {
            result = "Node Does Not Created At This Level";
            ViewBag.jscirpt = "<script language='javascript' type='text/javascript'> alert('Node Does Not Created At This Level');</script>";
            return View(result);
        }
        [HttpPost]
        public JsonResult emp(string result)
        {
            NodeText = result;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Employees(string NodeType, string NodePlace, string NodeName, string IsFormApplicable, string ScreenName)
        {
           
            MenuItems items = new MenuItems();
            CONTABLE003 menu1 = new CONTABLE003();
            string mod = NodeText;
            string NewNode = NodeName;
            string Type = NodeType;
            string Place =NodePlace;
            string FormApplicable = IsFormApplicable;
            string SName =ScreenName;
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();

            var db1 = dc.CONTABLE003.Where(a => a.COLUMN05 == mod).ToList();
           
           //To Find CenterId
            var ce = dc.CONTABLE003.OrderByDescending((a => a.COLUMN04)).ToList();
            var cent = ce.Max(a => a.COLUMN04);
            int center = Convert.ToInt32(cent); 
            //To Find MenuId
            var me = dc.CONTABLE003.OrderByDescending((a => a.COLUMN02)).ToList();
            var men = me.Max(a => a.COLUMN02);
            int menuid = Convert.ToInt32(men);
            //To Find ModuleId
            var mo = dc.CONTABLE003.OrderByDescending((a => a.COLUMN03)).ToList();
            var modu = mo.Max(a => a.COLUMN03);
            int module = Convert.ToInt32(modu);
            //To Find PriorityLevel
            var pl = dc.CONTABLE003.OrderByDescending((a => a.COLUMN07)).ToList();
            int prl = Convert.ToInt32(pl.Max(a => a.COLUMN07));

            int Plevel = Convert.ToInt32(db1.FirstOrDefault().COLUMN07);
            int moduleid = Convert.ToInt32(db1.FirstOrDefault().COLUMN03);

            menu1.COLUMN02 = (menuid + 1);
            menu1.COLUMN05 = NewNode;
            menu1.COLUMN08 = "Y";
            menu1.COLUMN04 = 101;
            if (IsFormApplicable == "true")
            {
                menu1.COLUMN09 = "Y";
            }
            else
            {
                menu1.COLUMN09 = "N";
            }
            menu1.COLUMN10 = ScreenName;

            if (Type == "ParentNode")
            {
                if (Plevel == 0 )
                {
                    var result = "Node Does Not Created At This Level";
                    return RedirectToAction("Employees");
                }
                else
                {
                    int mid = Convert.ToInt32(db1[0].COLUMN03);
                    menu1.COLUMN03 = mid;
                    var r = db1[0].COLUMN06;
                    menu1.COLUMN06 = (r);
                    var pll = db1[0].COLUMN07;
                    menu1.COLUMN07 = pll;


                    db1[0].COLUMN06 =( menuid + 1);
                     db1[0].COLUMN07 = pll + 1;
                    //dc.SaveChanges();       
                }

            }
            else if (Type == "ChildNode")
            {
                int mid = Convert.ToInt32(db1[0].COLUMN03);
                menu1.COLUMN03 = mid;
                int r =Convert.ToInt32( db1[0].COLUMN02);
                menu1.COLUMN06 = (r);
                var pll = db1[0].COLUMN07;
                menu1.COLUMN07 = Convert.ToInt32(pll+1);
                int or = Convert.ToInt32(db1[0].COLUMN11);
                int co = dc.CONTABLE003.Where(a => a.COLUMN06 == r).Count();
                menu1.COLUMN11 = co + 1;
                menu1.COLUMN10 = ScreenName; dc.CONTABLE003.Add(menu1);
                var FormAppl = dc.CONTABLE003.Where(a => a.COLUMN02 == r).ToList();
                if (ScreenName != null)
                {
                CONTABLE0010 formmaster = new CONTABLE0010();
                var formdata = dc.CONTABLE0010.OrderBy(a => a.COLUMN02 != null).ToList();
                if (formdata == null)
                {
                    formmaster.COLUMN02 = 1001;
                }
                else
                {
                    var fid = formdata[formdata.Count-1].COLUMN02;
                    formmaster.COLUMN02 = (fid + 1);
                    formmaster.COLUMN06 = (fid + 1);
                }
                formmaster.COLUMN03 = mid;
                formmaster.COLUMN04 = ScreenName;
                formmaster.COLUMN05 = "Standard";
                formmaster.COLUMN07 = "0";
                    
                    dc.CONTABLE0010.Add(formmaster);
                }
                if (FormAppl[0].COLUMN09 == "Y")
                {
                    dc.SaveChanges();
                    return RedirectToAction("Employees");
                }
                else
                {
                   // dc.SaveChanges();
                }
            }
            else if (Type == "Node")
            {
                if (Plevel == 0)
                {
                    var result = "Node Does Not Created At This Level";
                    return RedirectToAction("Employees");
                }
                else
                {
                    if (Place == "After")
                    {
                        int mid = Convert.ToInt32(db1[0].COLUMN03);
                        menu1.COLUMN03 = mid;
                        var r = db1[0].COLUMN06;
                        menu1.COLUMN06 =(r);
                        int? pll = db1[0].COLUMN07;
                        menu1.COLUMN07 = Convert.ToInt32(pll);
                        int ord = Convert.ToInt32(db1[0].COLUMN11);
                        menu1.COLUMN11 = ord + 1;
                        int co = dc.CONTABLE003.Where(a => a.COLUMN06 == r).Count();
                        for (int p = ord; p < co; p++)
                        {
                            var rr = dc.CONTABLE003.Where(a => a.COLUMN06 == r).ToList();
                            int? s = rr.ElementAt(p).COLUMN11;
                            rr.ElementAt(p).COLUMN11 = s + 1;
                        }
                        //dc.SaveChanges();
                    }
                    else if (Place == "Before")
                    {
                        int mid = Convert.ToInt32(db1[0].COLUMN03);
                        menu1.COLUMN03 = mid;
                        var r = db1[0].COLUMN06;
                        menu1.COLUMN06 = (r);
                        int? pll = db1[0].COLUMN07;
                        menu1.COLUMN07 = Convert.ToInt32(pll);
                        int ord = Convert.ToInt32(db1[0].COLUMN11);
                        menu1.COLUMN11 = ord;
                        int co = dc.CONTABLE003.Where(a => a.COLUMN06 == r).Count();
                        for (int p = ord - 1; p < co; p++)
                        {
                            var rr = dc.CONTABLE003.Where(a => a.COLUMN06 == r).ToList();
                            int? s = rr.ElementAt(p).COLUMN11;
                            rr.ElementAt(p).COLUMN11 = s + 1;
                        }
                       // dc.SaveChanges();
                    }
                }
            }
            dc.CONTABLE003.Add(menu1);
            dc.SaveChanges();
            ViewBag.Message = "Data has been saved succeessfully";
            return RedirectToAction("Employees");
        }
            TreeView Tview ;
        public ActionResult OnDemand()
        {
           List<Menu_Master> all = new List<Menu_Master>();
           eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            var db = dc.Menu_Master.Where(a => a.MenuName != null).ToList();
            int co = db.Count();
            ViewBag.count = co;
            all = dc.Menu_Master.Where(a => a.MenuName != null).ToList();
            return View(all);
        }
        [HttpPost]
        public ActionResult OnDemand(CustomMenuMaster menu, string command)
        {
            if (ModelState.IsValid)
            {
                db.CustomMenuMasters.Add(menu);
                db.SaveChanges();
                ViewBag.Message = "Data has been saved succeessfully";
                return RedirectToAction("OnDemand");
            }
            //return View();
            return View("OnDemand", menu);
        }
   
        public ActionResult Create()        
        {
            var d = db.Menu_Master.Where(a => a.Parent == null);
            var t = db.Menu_Master.Where(a => a.Parent != null);
            var t1 = d.Select(a => a.MenuName);
            var t3 = d.Select(a => a.CenterId);
            var t4 = t.Select(a => a.Parent);
            var t2 = t.Select(a => a.MenuName);
            //List<EbizsuiteDbEntities> db1 = new List<EbizsuiteDbEntities>();
            ViewBag.Parentlist = t1;
            ViewBag.ParentCode = t3;
            ViewBag.ChildCode = t4;
            ViewBag.Childlist = t2;
            return View();
        }
        [HttpPost]
        public ActionResult Create(CustomMenuMaster  menu, string command)
        {
            if (ModelState.IsValid)
            {
                db.CustomMenuMasters.Add(menu);
                db.SaveChanges();
                ViewBag.Message = "Data has been saved succeessfully";
                return RedirectToAction("Create");
            }
            //return View();
            return View("Create", menu);
        }
        public ActionResult ParentNode()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ParentNode(CustomMenuMaster menu, string command)
        {
            if (ModelState.IsValid)
            {
                db.CustomMenuMasters.Add(menu);
                db.SaveChanges();
                ViewBag.Message = "Data has been saved succeessfully";
                return RedirectToAction("OnDemand");
            }
            //return View();
            return View("ParentNode",menu );
        }
        public ActionResult ChildrenNode()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChildrenNode(CustomMenuMaster menu, string command)
        {
            if (ModelState.IsValid)
            {
                db.CustomMenuMasters.Add(menu);
                db.SaveChanges();
                ViewBag.Message = "Data has been saved succeessfully";
                return RedirectToAction("OnDemand");
            }
            //return View();
            return View("ChildrenNode", menu);
        }
        public ActionResult Custom()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Custom(CustomMenuMaster menu, string command)
        {
            if (ModelState.IsValid)
            {
                db.CustomMenuMasters.Add(menu);
                db.SaveChanges();
                ViewBag.Message = "Data has been saved succeessfully";
                return RedirectToAction("OnDemand");
            }
            //return View();
            return View("Custom", menu);
        }
   
        public ActionResult SampleTreeView()
        {
            List<Menu_Master> all = new List<Menu_Master>();
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            var db = dc.Menu_Master.Where(a => a.MenuName != null).ToList();
            int co = db.Count();
            ViewBag.count = co;
            var db1 = db.Select(a => a.MenuName);
            ViewBag.Tree = db;
            all = dc.Menu_Master.Where(a => a.MenuName != null).ToList();
            return View(all);
        }
    }
}
