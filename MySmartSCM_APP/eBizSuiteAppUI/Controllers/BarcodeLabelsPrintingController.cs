﻿using eBizSuiteAppModel.Table;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class BarcodeLabelsPrintingController : Controller
    {
        //
        // GET: /BarcodeLabelsPrinting/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        string providerName = "System.Data.SqlClient";
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult BarcodeLabelsPrintingInfo()
        {
            try
            {
                //int? acID = (int?)Session["AcOwner"];
                //string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                //List<string> actCol = new List<string>();
                //List<string> alCol = new List<string>();
                //var strQry = "(Select p.COLUMN02 ID,0 AliasID,p.COLUMN04 Item,p.COLUMN50 [Desc],p.COLUMN06 [UPC/BarCode],m.COLUMN04 Units,s.COLUMN04 Price,p.COLUMNA06 From MATABLE007 p left join MATABLE002 m on m.COLUMN02=p.COLUMN63 and m.COLUMNA03=p.COLUMNA03" +
                //" left join MATABLE024 s on s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 " +
                //" and (isnull(s.COLUMN03,0)=p.COLUMNA02  or isnull(s.COLUMN03,0)=0) "+
                //" and s.COLUMN07=p.COLUMN02 and s.COLUMN06='sales' where p.COLUMNA03=" + acID + " and p.COLUMN06!='' and " + OPUnit + " and isnull(p.COLUMNA13,0)=0) "+
                //" union all (Select p.COLUMN02 ID,ia.COLUMN02 AliasID,p.COLUMN04 Item,p.COLUMN50 [Desc],ia.COLUMN04 [UPC/BarCode],im.COLUMN04 Units,ia.COLUMN09 Price,ia.COLUMNA06 From MATABLE007 p " +
                //" inner join MATABLE021 ia on ia.COLUMN05=p.COLUMN02 and  ia.COLUMNA03=p.COLUMNA03 and isnull(ia.COLUMNA13,0)=0 " +
                //" left join MATABLE002 im on im.COLUMN02=ia.COLUMN06 and im.COLUMNA03=ia.COLUMNA03 " +
                //" where p.COLUMNA03=" + acID + " and ia.COLUMN04!='' and " + OPUnit + " and isnull(p.COLUMNA13,0)=0) ORDER BY COLUMNA06 desc ";
                //alCol.AddRange(new List<string> { "ID", "Item", "Desc", "UPC/BarCode","Units", "Price" });
                //actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN50", "p.COLUMN06", "m.COLUMN04", "s.COLUMN04" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                object[] sp = new object[4];
                sp[0] = (1623);
                sp[1] = (Session["OPUnit"]);
                sp[2] = (Session["AcOwner"]);
                sp[3] = ("Info");
                var GData = dbs.Query("Exec USP_PROC_FormBuildInfo @FormId=@0,@OPUnit=@1,@AcOwner=@2,@Type=@3", sp);
                //var GData = dbs.Query(strQry);
                //var grid = new WebGrid(GData, canPage: false, canSort: false);
                //List<WebGridColumn> cols = new List<WebGridColumn>();
                //int index = 0;
                //foreach (var column in actCol)
                //{
                //    cols.Add(grid.Column(alCol[index], alCol[index]));
                //    index++;
                //}
                ViewBag.columns = GData.FirstOrDefault().Columns;
                ViewBag.Grid = GData;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult BarCodePrint(string ide,string AliasID)
        {
            try
            {
                //ide = @Request["ide"];
                string barCode = "",Units = "";
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                string barcodeqry="select * from MATABLE007 p where p.COLUMN02=" + ide + " and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                if (AliasID != "0" && AliasID != "") barcodeqry = "select p.COLUMN02,p.COLUMN04 COLUMN06,p.COLUMN06 COLUMN63 from MATABLE021 p where p.COLUMN05=" + ide + " and p.COLUMN02=" + AliasID + " and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                SqlCommand cmdd = new SqlCommand(barcodeqry, cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);
                if (dtd.Rows.Count > 0) { barCode = dtd.Rows[0]["COLUMN06"].ToString(); if (dtd.Columns.Contains("COLUMN63")) Units = dtd.Rows[0]["COLUMN63"].ToString(); }
                System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                byte[] byteImage;
                var len = barCode.Length;
                if (len <= 6) len = 6;
                using (Bitmap bitMap = new Bitmap(len * 35, 80))
                {
                    using (Graphics graphics = Graphics.FromImage(bitMap))
                    {
                        Font oFont = new Font("IDAutomationHC39M", 16, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
                        PointF point = new PointF(2f, 2f);
                        SolidBrush blackBrush = new SolidBrush(Color.Black);
                        SolidBrush whiteBrush = new SolidBrush(Color.White);
                        graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                        graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byteImage = ms.ToArray();

                        Convert.ToBase64String(byteImage);
                        //barCode = byteImage != null ? "data:image/jpg;base64," + Convert.ToBase64String((byte[])byteImage) : "";
                        imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    }
                    if (barCode.Length > 0)
                    {
                        Barcode128 code128 = new Barcode128();
                        code128.CodeType = Barcode.CODE128;
                        code128.ChecksumText = true;
                        code128.GenerateChecksum = true;
                        code128.Code = barCode;
                        if (acID == 56577 && len > 10) len = 10;
                        else if (acID == 56874 && len > 10) len = 9;
                        else if (len > 10) len = 13;
                        System.Drawing.Bitmap bmpimg = new Bitmap(len * 20, 28);
                        Graphics bmpgraphics = Graphics.FromImage(bmpimg);
                        bmpgraphics.Clear(Color.White);
                        bmpgraphics.DrawImage(code128.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White), new Point(0, 0));

                        //bmpgraphics.DrawString(barCode, new System.Drawing.Font("Thoma", 7,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point), SystemBrushes.WindowText, new Point(15, 24));
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bmpimg.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                            byteImage = ms.ToArray();
                        }
                    }
                }
                var image = Convert.ToBase64String(byteImage);
                string query = "Select top(1)s.COLUMN05 MRP From MATABLE007 p left join MATABLE002 m on m.COLUMN02=p.COLUMN63 and m.COLUMNA03=p.COLUMNA03" +
                " left join MATABLE024 s on s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 " +
                " and (isnull(s.COLUMN03,0)=p.COLUMNA02  or isnull(s.COLUMN03,0)=0) " +
                " and s.COLUMN07=p.COLUMN02 and s.COLUMN06='sales' where p.COLUMN02='" + ide + "' and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                SqlCommand cmdt = new SqlCommand("select *  from SETABLE018 p where isnull(p.COLUMN06,0)=1 and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0", cn);
                SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                DataTable dtt = new DataTable();
                dat.Fill(dtt);
                int order = 2;
                if (dtt.Rows.Count > 0) { order = Convert.ToInt32(dtt.Rows[0]["COLUMN05"].ToString()); query = dtt.Rows[0]["COLUMN07"].ToString(); query = query + " where p.COLUMN02='" + ide + "' and isnull(p.COLUMN47,0)=0 and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 "; }
                if (Units != "") query = query.Replace("p.COLUMN63", Units);
                if (AliasID != "0" && AliasID != "") query = query.Replace("p.COLUMN06", "'" + barCode + "'");
                SqlCommand cmd = new SqlCommand(query, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string tbldata = "", colscnt = dt.Columns.Count.ToString();
                List<SelectListItem> tbldatacols = new List<SelectListItem>();
                if (dt.Rows.Count > 0)
                {
                    tbldata = "<tr>";
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (i % order == 0 && i != 0) tbldata = tbldata + "</tr><tr>";
                        tbldata = tbldata + "<td style='padding-right:3px;max-width: 70px;'>" + dt.Columns[i].ColumnName + "</td><td style='padding-right:3px;max-width: 70px;'>" + dt.Rows[0][dt.Columns[i].ColumnName].ToString() + "</td>";
                        tbldatacols.Add(new SelectListItem { Text = dt.Columns[i].ColumnName.ToString(), Value = dt.Rows[0][dt.Columns[i].ColumnName].ToString() });
                    }
                    tbldata = tbldata + "</tr>";
                }
                string Company = "", compAdd = "";
                SqlCommand cmdc = new SqlCommand("select  top(1) c.COLUMN03 Company, c.COLUMN09 as compAdd,c.COLUMN10 as compA1, c.COLUMN11 as compA2,s.COLUMN03 as compstate, c.COLUMN12 as compCity,c.COLUMN14 as compZip from CONTABLE008 c left join MATABLE017 s on s.COLUMN02=c.COLUMN13 and isnull(s.COLUMNA13,0)=0 where c.COLUMNA03=" + acID + " and isnull(c.COLUMN05,0)=0 and isnull(c.COLUMNA13,0)=0", cn);
                SqlDataAdapter dac = new SqlDataAdapter(cmdc);
                DataTable dtc = new DataTable();
                dac.Fill(dtc);
                if (dtc.Rows.Count > 0)
                {
                    Company = dtc.Rows[0]["Company"].ToString();
                    if (dtc.Rows[0]["compAdd"].ToString() != "" && dtc.Rows[0]["compAdd"].ToString() != null) compAdd = compAdd + dtc.Rows[0]["compAdd"].ToString() + ",";
                    if (dtc.Rows[0]["compA1"].ToString() != "" && dtc.Rows[0]["compA1"].ToString() != null) compAdd = compAdd + dtc.Rows[0]["compA1"].ToString() + ",";
                    if (dtc.Rows[0]["compA2"].ToString() != "" && dtc.Rows[0]["compA2"].ToString() != null) compAdd = compAdd + dtc.Rows[0]["compA2"].ToString() + ",";
                    if (dtc.Rows[0]["compstate"].ToString() != "" && dtc.Rows[0]["compstate"].ToString() != null) compAdd = compAdd + dtc.Rows[0]["compstate"].ToString() + ",";
                    if (dtc.Rows[0]["compCity"].ToString() != "" && dtc.Rows[0]["compCity"].ToString() != null) compAdd = compAdd + dtc.Rows[0]["compCity"].ToString() + ",";
                    if (dtc.Rows[0]["compZip"].ToString() != "" && dtc.Rows[0]["compZip"].ToString() != null) compAdd = compAdd + dtc.Rows[0]["compZip"].ToString();
                }
                var OPUnitval = Session["OPUnit1"]; string Adresss = "";
                if (OPUnitval != "" && OPUnitval != null)
                {
                    SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", cn);
                    ocmd.CommandType = CommandType.StoredProcedure;
                    ocmd.Parameters.Add(new SqlParameter("@oppUnit", OPUnitval));
                    SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                    DataTable odt = new DataTable();
                    oda.Fill(odt);
                    if (dt.Rows.Count > 0)
                        Adresss = odt.Rows[0]["COLUMN12"].ToString();
                }
                var salesprice = "";
                SqlCommand cmdp = new SqlCommand("Select top(1)s.COLUMN04 SalesPrice,s.COLUMN05 MRP From MATABLE007 p left join MATABLE002 m on m.COLUMN02=p.COLUMN63 and m.COLUMNA03=p.COLUMNA03" +
                " left join MATABLE024 s on s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 " +
                " and (isnull(s.COLUMN03,0)=p.COLUMNA02  or isnull(s.COLUMN03,0)=0) " +
                " and s.COLUMN07=p.COLUMN02 and s.COLUMN06='sales' where p.COLUMN02='" + ide + "' and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0", cn);
                SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                DataTable dtp = new DataTable();
                dap.Fill(dtp); if (dtp.Rows.Count > 0) salesprice = dtp.Rows[0][0].ToString();
                return Json(new { image = image, tbldata = tbldata, Company = Company, compAdd = compAdd, colscnt = colscnt, order = order, tbldatacols = tbldatacols, Adresss = Adresss, barCode = barCode, imgwidth = len * 20, salesprice = salesprice }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult BillBarCodePrint(string sono)
        {
            try
            {
                var ide = 0;
                //ide = @Request["ide"];
                string barCode = "", Units = "";
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                SqlCommand cmdb = new SqlCommand("usp_Proc_Barcodegen", cn);
                cmdb.CommandType = CommandType.StoredProcedure;
                cmdb.Parameters.Add(new SqlParameter("@BillNO", sono));
                cmdb.Parameters.Add(new SqlParameter("@OPUnit", Session["OPUnit1"]));
                cmdb.Parameters.Add(new SqlParameter("@AcOwner", acID));
                SqlDataAdapter dab = new SqlDataAdapter(cmdb);
                DataTable dtbi = new DataTable();
                dab.Fill(dtbi);
                DataSet ds = new DataSet();
                dab.Fill(ds, "UOM");
                dtbi = ds.Tables["UOM"];
                //SqlCommand cmdbi = new SqlCommand("select isnull(l.COLUMN04,0)COLUMN04,isnull(l.COLUMN09,0)COLUMN09 from PUTABLE005 p inner join PUTABLE006 l on l.COLUMN13=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and l.COLUMNA02=p.COLUMNA02 and isnull(l.COLUMNA13,0)=0  where p.COLUMN02=" + sono + " and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ", cn);
                //SqlDataAdapter dabi = new SqlDataAdapter(cmdbi);
                //DataTable dtbi = new DataTable();
                //dabi.Fill(dtbi);
                //var bytes = Encoding.UTF8.GetBytes("");
                //var base64 = Convert.ToBase64String(bytes);
                //var image = base64; 
                var imag = "";var imgpadding = "";var qty = 0;var tqty = 0;var tbldsgn = "<table>";var tblrowcnt = 0;var tblimg = "";
                for (int j = 0; j < dtbi.Rows.Count; j++)
                {
                    ide = Convert.ToInt32(dtbi.Rows[j][0]);
                    qty = Convert.ToInt32(dtbi.Rows[j][1]); tqty = tqty + qty;
                    if (dtbi.Columns.Contains("BarCode")) { barCode = dtbi.Rows[j]["BarCode"].ToString(); Units = dtbi.Rows[j]["UOM"].ToString(); }
                    else
                    {
                        barCode = ""; Units = dtbi.Rows[j]["UOM"].ToString();
                        string barcodeqry = "select * from MATABLE007 p where p.COLUMN02=" + ide + " and isnull(p.COLUMN06,'')!='' and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                        //if (AliasID != "0" && AliasID != "") barcodeqry = "select p.COLUMN02,p.COLUMN04 COLUMN06,p.COLUMN06 COLUMN63 from MATABLE021 p where p.COLUMN05=" + ide + " and p.COLUMN02=" + AliasID + " and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                        SqlCommand cmdd = new SqlCommand(barcodeqry, cn);
                        SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                        DataTable dtd = new DataTable();
                        dad.Fill(dtd);
                        if (dtd.Rows.Count > 0) { barCode = dtd.Rows[0]["COLUMN06"].ToString(); if (dtd.Columns.Contains("COLUMN63")) Units = dtd.Rows[0]["COLUMN63"].ToString(); }
                        else
                        {
                            barcodeqry = "select p.COLUMN02,p.COLUMN04 COLUMN06,p.COLUMN06 COLUMN63 from MATABLE021 p where p.COLUMN05=" + ide + " and p.COLUMN06=" + Units + " and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                            SqlCommand cmda = new SqlCommand(barcodeqry, cn);
                            SqlDataAdapter daa = new SqlDataAdapter(cmda);
                            DataTable dta = new DataTable();
                            daa.Fill(dta);
                            if (dta.Rows.Count > 0) { barCode = dta.Rows[0]["COLUMN06"].ToString(); if (dta.Columns.Contains("COLUMN63")) Units = dta.Rows[0]["COLUMN63"].ToString(); }
                        }
                    }
                    if (barCode != "")
                    {
                        System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                        byte[] byteImage = Encoding.UTF8.GetBytes("");
                        var len = barCode.Length;
                        if (len <= 6) len = 6;
                        using (Bitmap bitMap = new Bitmap(len * 35, 80))
                        {
                            if (barCode.Length > 0)
                            {
                                Barcode128 code128 = new Barcode128();
                                code128.CodeType = Barcode.CODE128;
                                code128.ChecksumText = true;
                                code128.GenerateChecksum = true;
                                code128.Code = barCode;
                                if (len > 10) len = 10;
                                System.Drawing.Bitmap bmpimg = new Bitmap(len * 20, 28);
                                Graphics bmpgraphics = Graphics.FromImage(bmpimg);
                                bmpgraphics.Clear(Color.White);
                                bmpgraphics.DrawImage(code128.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White), new Point(0, 0));
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    bmpimg.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                                    byteImage = ms.ToArray();
                                }
                            }
                        }
                        var image = Convert.ToBase64String(byteImage);
                        string query = "Select top(1)s.COLUMN05 MRP From MATABLE007 p left join MATABLE002 m on m.COLUMN02=p.COLUMN63 and m.COLUMNA03=p.COLUMNA03" +
                        " left join MATABLE024 s on s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 " +
                        " and (isnull(s.COLUMN03,0)=p.COLUMNA02  or isnull(s.COLUMN03,0)=0) " +
                        " and s.COLUMN07=p.COLUMN02 and s.COLUMN06='sales' where p.COLUMN02='" + ide + "' and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 ";
                        SqlCommand cmdt = new SqlCommand("select *  from SETABLE018 p where isnull(p.COLUMN06,0)=1 and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0", cn);
                        SqlDataAdapter dat = new SqlDataAdapter(cmdt);
                        DataTable dtt = new DataTable();
                        dat.Fill(dtt);
                        int order = 2;
                        if (dtt.Rows.Count > 0) { order = Convert.ToInt32(dtt.Rows[0]["COLUMN05"].ToString()); query = dtt.Rows[0]["COLUMN07"].ToString(); query = query + " where p.COLUMN02='" + ide + "' and isnull(p.COLUMN47,0)=0 and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0 "; }
                        if (Units != "") query = query.Replace("p.COLUMN63", Units);
                        //if (AliasID != "0" && AliasID != "") query = query.Replace("p.COLUMN06", "'" + barCode + "'");
                        SqlCommand cmd = new SqlCommand(query, cn);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        string tbldata = "", colscnt = dt.Columns.Count.ToString();
                        List<SelectListItem> tbldatacols = new List<SelectListItem>();
                        if (dt.Rows.Count > 0)
                        {
                            tbldata = "<tr>";
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                if (i % order == 0 && i != 0) tbldata = tbldata + "</tr><tr>";
                                tbldata = tbldata + "<td style='padding-right:3px;max-width: 70px;'>" + dt.Columns[i].ColumnName + "</td><td style='padding-right:3px;max-width: 70px;'>" + dt.Rows[0][dt.Columns[i].ColumnName].ToString() + "</td>";
                                tbldatacols.Add(new SelectListItem { Text = dt.Columns[i].ColumnName.ToString(), Value = dt.Rows[0][dt.Columns[i].ColumnName].ToString() });
                            }
                            tbldata = tbldata + "</tr>";
                        }
                        var OPUnitval = Session["OPUnit1"]; string Adresss = "";
                        if (OPUnitval != "" && OPUnitval != null)
                        {
                            SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", cn);
                            ocmd.CommandType = CommandType.StoredProcedure;
                            ocmd.Parameters.Add(new SqlParameter("@oppUnit", OPUnitval));
                            SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                            DataTable odt = new DataTable();
                            oda.Fill(odt);
                            if (dt.Rows.Count > 0)
                                Adresss = odt.Rows[0]["COLUMN12"].ToString();
                        }
                        var salesprice = "";
                        SqlCommand cmdp = new SqlCommand("Select top(1)s.COLUMN04 SalesPrice,s.COLUMN05 MRP From MATABLE007 p left join MATABLE002 m on m.COLUMN02=p.COLUMN63 and m.COLUMNA03=p.COLUMNA03" +
                        " left join MATABLE024 s on s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 " +
                        " and (isnull(s.COLUMN03,0)=p.COLUMNA02  or isnull(s.COLUMN03,0)=0) " +
                        " and s.COLUMN07=p.COLUMN02 and s.COLUMN06='sales' where p.COLUMN02='" + ide + "' and p.COLUMNA03=" + acID + " and " + OPUnit + " and isnull(p.COLUMNA13,0)=0", cn);
                        SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                        DataTable dtp = new DataTable();
                        dap.Fill(dtp); if (dtp.Rows.Count > 0) salesprice = dtp.Rows[0][0].ToString();

                        var tbldatacols1 = "<tr>"; var tbldatacolsflyee = ""; var brand = "";var codesep = ","; var tdpadding = "padding-top:10px;"; var firtd = "width:50%;padding-left:15px;"; var sectd = "width:50%;padding-left:0px;"; var diff = ""; var tblcolwidth = "max-width:70px;word-wrap:break-word;"; var tblmrpcolwidth = ""; var barcodeimgwidth = "width: 180px;";
                        if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681") tbldatacols1 = "<tr><td>";
                        for (var i = 0; i < tbldatacols.Count(); i++)
                        {
                            if (i % order == 0 && i != 0)
                            {
                                if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681") tbldatacols1 = tbldatacols1 + "</td></tr><tr><td>";
                                else
                                    tbldatacols1 = tbldatacols1 + "</tr><tr>";
                            }
                            if (Convert.ToString(Session["AcOwner"]) == "56577")
                            {
                                diff = ":"; tblcolwidth = "max-width:75px;word-wrap:break-word;";
                                tbldatacols[i].Text = tbldatacols[i].Text;
                                if (tbldatacols[i].Text == "Units") { tblmrpcolwidth = "padding-left:5px;font-size:16px;font-weight:bold;"; tbldatacolsflyee = tbldatacolsflyee + "<tr><td style='font-size:12px;font-weight:bold;'>Size" + diff + "</td><td>" + tbldatacols[i].Value + "</td>"; tbldatacols[i].Text = ""; tbldatacols[i].Value = ""; }
                                else if (tbldatacols[i].Text == "MRP") { tblmrpcolwidth = "padding-left:5px;font-size:16px;font-weight:bold;"; tbldatacolsflyee = tbldatacolsflyee + "<td style='padding-left:20px;font-size:12px;font-weight:bold;'>MRP" + diff + "</td><td>Rs.&nbsp;&nbsp;" + tbldatacols[i].Value + "</td></tr>"; tbldatacols[i].Text = ""; tbldatacols[i].Value = ""; }
                                else { tbldatacols[i].Text = ""; }
                            }
                            else if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681")
                            {
                                tblcolwidth = "max-width:70px;word-wrap:break-word;";
                                if (tbldatacols[i].Text == "Brand/Company") { brand = tbldatacols[i].Value; tbldatacols[i].Value = ""; tblcolwidth = ""; }
                                else if (tbldatacols[i].Text == "Name") { tblcolwidth = "max-width:100px;word-wrap:break-word;"; }
                                tbldatacols[i].Text = "";
                            }
                            else if (Convert.ToString(Session["OPUnit"]) == "56918" || Convert.ToString(Session["OPUnit"]) == "56921" || Convert.ToString(Session["OPUnit"]) == "56586" || Convert.ToString(Session["OPUnit"]) == "56866" || Convert.ToString(Session["OPUnit"]) == "56768")
                            {
                                diff = " - "; tblmrpcolwidth = "width: 220px;font-size:24px;font-weight:bold;padding-top:0px;";
                                if (tbldatacols[i].Text == "MRP") { tbldatacolsflyee = tbldatacolsflyee + "<tr><td style='font-size:12px;max-width: 13px;padding-left: 5px;font-weight: bold;'></td><td style='max-width: 33px;font-weight: bold;'>" + tbldatacols[i].Value + "</td>"; tbldatacols[i].Text = ""; tbldatacols[i].Value = ""; }
                                else if (tbldatacols[i].Text == "Price") { tbldatacolsflyee = tbldatacolsflyee + "<td style='font-size:12px;max-width: 1px;'></td><td style='max-width: 34px;font-weight: bold;'>" + tbldatacols[i].Value + "</td></tr>"; tbldatacols[i].Text = ""; tbldatacols[i].Value = ""; }
                            }
                            if ((Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681") && tbldatacols[i].Value != "")
                            { tbldatacols1 = tbldatacols1 + tbldatacols[i].Value + codesep; codesep = " "; }
                            else
                                tbldatacols1 = tbldatacols1 + "<td style='padding-right:3px;" + tblcolwidth + "'>" + tbldatacols[i].Text + "</td><td style='padding-right:3px;" + tblcolwidth + "font-weight:bold;'>" + tbldatacols[i].Value + "</td>";
                        }
                        if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681")
                            tbldatacols1 = tbldatacols1 + "</td></tr>";
                        else
                            tbldatacols1 = tbldatacols1 + "</tr>";
                        var tblstyle = "font-size:8px;font-weight:bold;padding-left:5px;padding-bottom:0px;";
                        if (colscnt == "1") tblstyle = "font-size:24px;font-weight:bold;padding-left:5px;width:100%;";
                        if (Convert.ToString(Session["AcOwner"]) == "56577") tblstyle = "padding-bottom:0px;font-size:12px;padding-left:5px;width:100%;";
                        else if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681") tblstyle = "min-width:300px;font-size:18px;font-weight:bold;padding-left:5px;padding-bottom:0px;";
                        else if (Convert.ToString(Session["OPUnit"]) == "56921" || Convert.ToString(Session["OPUnit"]) == "56635" || Convert.ToString(Session["OPUnit"]) == "56866") tblstyle = "display:none";
                        var Company = "<div style='padding-left:5px;width: 220px;font-size:7px;'>" + Adresss + "</div> ";
                        if (colscnt == "1" || colscnt == "2") Company = "<div style='padding-left:5px;width: 220px;font-size:11px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>" + Adresss + "</div> ";
                        if (Convert.ToString(Session["AcOwner"]) == "56637") Company = "<div style='padding-left:5px;text-align:center;width: 220px;font-size:18px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>" + Adresss + "</div> ";
                        else if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681") Company = "<div style='padding-left:15px;text-align:left;width: 220px;font-size:20px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;min-height:20px;'>" + brand + "</div> ";
                        var barcodelblstyle = "font-size:16px;font-weight:bold;padding-left:55px"; var salesprice1 = ""; var barcodeimght = "width: 220px;height:42px;";
                        if (Convert.ToString(Session["AcOwner"]) == "56578" || Convert.ToString(Session["AcOwner"]) == "56681")
                        {
                            barcodelblstyle = "float:left;font-size:16px;font-weight:bold;padding-left:55px;";
                            //salesprice1 = "<div style='font-size:13px;font-weight:bold;padding-left:10px;float:left;padding-top:1px;'>Offer Price  Rs.&nbsp;&nbsp;</div><div style='font-size:15px;font-weight:bold;float:left;'>" + salesprice + "</div>";
                            barcodeimght = "width: 250px;height:55px;"; barcodeimgwidth = "min-width: 300px;";
                        }
                        if (j > 0 && (Convert.ToString(Session["AcOwner"]) != "56578" && Convert.ToString(Session["AcOwner"]) != "56681")) imgpadding = "padding-top:5px;page-break-before: always;";
                        var imagdata = "<div style='" + imgpadding + "" + barcodeimgwidth + "'>" + Company + "<table style=" + tblstyle + ">" + tbldatacols1 + "</table><table style='" + tblmrpcolwidth + "'>" + tbldatacolsflyee + "</table><div style='padding-left:10px;" + barcodeimght + "'><img  "
                         + "src='" + "data:image/jpg;base64,"
                         + image + "' width='100%' height='100%'/><div style=" + barcodelblstyle + ">" + barCode + "</div>" + salesprice1 + "</div></div>";
                        if (acID != 56578 && acID != 56681)
                            imag = imag + imagdata;
                        else
                            imag = imagdata;
                        if (qty > 0 && (Convert.ToString(Session["AcOwner"]) != "56578" && Convert.ToString(Session["AcOwner"]) != "56681")) imgpadding = "padding-top:5px;page-break-before: always;";
                        imagdata = "<div style='" + imgpadding + "" + barcodeimgwidth + "'>" + Company + "<table style=" + tblstyle + ">" + tbldatacols1 + "</table><table style='" + tblmrpcolwidth + "'>" + tbldatacolsflyee + "</table><div style='padding-left:10px;" + barcodeimght + "'><img  "
                         + "src='" + "data:image/jpg;base64,"
                         + image + "' width='100%' height='100%'/><div style=" + barcodelblstyle + ">" + barCode + "</div>" + salesprice1 + "</div></div>";
                        if (Convert.ToString(Session["AcOwner"]) != "56578" && Convert.ToString(Session["AcOwner"]) != "56681") { for (int k = 1; k < qty; k++) { imag = imag + imagdata; } }
                        else
                        {
                            for (int k = 0; k < qty; k++)
                            {
                                if (tblrowcnt == 1) tdpadding = "";
                                else tdpadding = "padding-top:10px;";
                                if (tblrowcnt == 0)
                                    tblimg = "<table style='width:100%;'><tr style='width:100%;'><td style='width:50%;'>" + imag;
                                else if (k == qty - 1 && tblrowcnt % 2 == 0 && j == dtbi.Rows.Count - 1)
                                    tblimg += "</td></tr><tr style='width:100%;'><td style='"+tdpadding+""+firtd+"'>" + imag + "</td></tr></table>";
                                else if (k == qty - 1 && j == dtbi.Rows.Count - 1)
                                    tblimg += "</td><td style='" + tdpadding + "" + sectd + "'>" + imag + "</td></tr></table>";
                                else if (k == 0 && tblrowcnt % 2 == 0)
                                    tblimg += "</td></tr><tr style='width:100%;'><td style='" + tdpadding + "" + firtd + "'>" + imag;
                                else if (k == 0)
                                    tblimg += "</td><td style='" + tdpadding + "" + sectd + "'>" + imag;
                                else if (tblrowcnt % 2 == 0)
                                    tblimg += "</td></tr><tr style='width:100%;'><td style='" + tdpadding + "" + firtd + "'>" + imag;
                                else
                                    tblimg += "</td><td style='" + tdpadding + "" + sectd + "'>" + imag;
                                tblrowcnt = tblrowcnt + 1;
                            }
                        }
                    }
                }
                //tbldsgn = tbldsgn + "</table>";
                if (acID != 56578 && acID != 56681)
                    ViewBag.Barcodedata = imag;
                else if ((acID == 56578 || acID == 56681) && tqty == 1)
                    ViewBag.Barcodedata = "<table style='width:100%;'><tr style='width:100%;'><td style='width:50%;'>" + imag + "</td><td style='width:49%;'>" + imag + "</td></tr></table>";
                else
                    ViewBag.Barcodedata = tblimg;
                return View();
                //return Json(new { image = imag }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }
        public ActionResult BarcodeLabelsPrinting()
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1623).ToList().FirstOrDefault().COLUMN04.ToString();
                List<SelectListItem> Printers = new List<SelectListItem>();
                foreach (string printname in PrinterSettings.InstalledPrinters)
                {
                    Printers.Add(new SelectListItem { Value = printname, Text = printname });
                }
                ViewData["Printer Type"] = new SelectList(Printers, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult BarcodeLabelsPrinting(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1623).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Insert";
                cn.Open();
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE019 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodeLabelsPrinting", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1623");
                Cmd.Parameters.AddWithValue("@COLUMN07", col["Name"]);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Printer Type"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["IP Address Port"]);
                string defaultflag = Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1623 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1623 && q.COLUMN05 == 0).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Creation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }



        [HttpPost]
        public ActionResult BarcodeLabelsPrintingEdit(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1623).ToList().FirstOrDefault().COLUMN04.ToString();
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Update";
                cn.Open();
                var idi = ""; var ide = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SETABLE019 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodeLabelsPrinting", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMN03", "1623");
                Cmd.Parameters.AddWithValue("@COLUMN07", col["Name"]);
                Cmd.Parameters.AddWithValue("@COLUMN04", col["Printer Type"]);
                Cmd.Parameters.AddWithValue("@COLUMN05", col["IP Address Port"]);
                string defaultflag = Convert.ToString(col["Default"]);
                if (defaultflag == null) defaultflag = "0";
                else defaultflag = "1";
                Cmd.Parameters.AddWithValue("@COLUMN06", defaultflag);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1623 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1623 && q.COLUMN05 == 3).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Updation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult BarcodeLabelsPrintingDelete()
        {
            try
            {
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1623).ToList().FirstOrDefault().COLUMN04.ToString();
                var ide = ""; var idi = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int im = custform.IndexOf("idi"); int m = custform.IndexOf("&"); if (m > 0 && im >= 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { ide = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                string Date = DateTime.Now.ToString();
                string OpUnit = Convert.ToString(Session["OPUnit"]);
                string opstatus = Convert.ToString(Session["OPUnitstatus"]);
                if (opstatus == null || opstatus == "") OpUnit = null;
                string AOwner = Convert.ToString(Session["AcOwner"]);
                string insert = "Delete";
                cn.Open();
                SqlCommand Cmd = new SqlCommand("usp_SET_BL_BarcodeLabelsPrinting", cn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddWithValue("@COLUMN02", ide);
                Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                Cmd.Parameters.AddWithValue("@COLUMNA03", AOwner);
                Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                Cmd.Parameters.AddWithValue("@Direction", insert);
                Cmd.Parameters.AddWithValue("@TabelName", "SETABLE019");
                int r = Cmd.ExecuteNonQuery();
                cn.Close();
                if (r > 0)
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1623 && q.COLUMN05 == 4).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msg = string.Empty;
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1623 && q.COLUMN05 == 5).FirstOrDefault();
                    if (msgMaster != null)
                        msg = msgMaster.COLUMN03;
                    else
                        msg = "" + Session["FormName"] + " Deletion Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("BarcodeLabelsPrintingInfo", "BarcodeLabelsPrinting", new { FormName = Session["FormName"] });
            }
        }


        //queuing/storing barcodes for print
        public ActionResult BarcodestobePrint(string Id, string Barcode, string Qty)
        {
            try
            {
                string cn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
                var Date = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
                SqlConnection con = new SqlConnection(cn);
                SqlCommand cmd = new SqlCommand("barcodestracking", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FormId", 1623);
                cmd.Parameters.AddWithValue("@Trans_Id", Id);
                cmd.Parameters.AddWithValue("@Item", Id);
                cmd.Parameters.AddWithValue("@Barcode", Barcode);
                cmd.Parameters.AddWithValue("@Qty", Qty);
                cmd.Parameters.AddWithValue("@Isactive", 1);
                
                cmd.Parameters.AddWithValue("@COLUMNA02", Session["OPUnit"]);
                cmd.Parameters.AddWithValue("@COLUMNA03", Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                cmd.Parameters.AddWithValue("@DIRECTION", "INSERT");
                cmd.Parameters.AddWithValue("@ReturnValue", "");
                con.Open();
                int l = cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                if (l > 0)
                {
                    Session["MessageFrom"] = "Print generated successfully!";
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    Session["MessageFrom"] = "Something went wrong";
                    Session["SuccessMessageFrom"] = "fail";
                }
                return Json(new { barcodeqty = l }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //GetExceptionMsg(ex);
                return Json(new { barcodeqty = "0" }, JsonRequestBehavior.AllowGet);
            }
        }


        public class trsndetails
        {
            public string Id { get; set; }
            public string Qty { get; set; }
            public string Barcode { get; set; }
            public string FormId { get; set; }
            public string OPUnit { get; set; }
            public string AcOwner { get; set; }
            public string CreatedBy { get; set; }
        }

        public class barcodeinfo
        {
            public string Id { get; set; }
            public string Item { get; set; }
            public string Barcode { get; set; }
            public string PACKDATE { get; set; }
            public string EXPIRY { get; set; }
            public string MRP { get; set; }
            public string UPC { get; set; }
            public string QTY { get; set; }
        }
    }
}
