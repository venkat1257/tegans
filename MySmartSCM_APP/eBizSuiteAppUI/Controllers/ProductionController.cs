﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Controllers
{
    public class ProductionController : Controller
    {
        //
        // GET: /Production/

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        string providerName = "System.Data.SqlClient";
        eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public ActionResult Info()
        {
            try
            {
                int? acID = (int?)Session["AcOwner"];
                string OPUnit = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                List<string> actCol = new List<string>();
                List<string> alCol = new List<string>();
                ViewBag.DateFormat = DateFormatsetup();
                var strQry = "SELECT p.[COLUMN02 ] AS ID ,p.[COLUMN04 ] AS [Production#],format(p.COLUMN11,'" + ViewBag.DateFormat + "') Date,jo.[COLUMN04] AS [Job Order#],ji.[COLUMN04] AS [Issue#],j.[COLUMN05] AS [Jobber],cs.[COLUMN04] AS [Current Status],st.[COLUMN04] AS [Submitted To],m.COLUMN09 [Created By] from SATABLE019 p " +
                " left join SATABLE005 jo on jo.COLUMN02=p.column06 and jo.COLUMNA03=p.COLUMNA03 and isnull(jo.COLUMNA13,0)=0 and (jo.COLUMNA02=p.COLUMNA02 or jo.COLUMNA02 is null) " +
                " left join SATABLE007 ji on ji.COLUMN02=p.column05 and ji.COLUMNA03=p.COLUMNA03 and isnull(ji.COLUMNA13,0)=0 and (ji.COLUMNA02=p.COLUMNA02 or ji.COLUMNA02 is null) " +
                " left join SATABLE001 j on j.COLUMN02=p.COLUMN07 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and (j.COLUMNA02=p.COLUMNA02 or j.COLUMNA02 is null) " +
                " left join SATABLE021 ps on ps.COLUMN04=p.COLUMN04 and ps.COLUMN07=p.COLUMN01 and ps.COLUMN05=p.COLUMN12 and ps.COLUMNA03=p.COLUMNA03 and isnull(ps.COLUMNA13,0)=0  " +
                " left join MATABLE030 cs on cs.COLUMN02=ps.COLUMN05 and cs.COLUMNA03=ps.COLUMNA03 and isnull(cs.COLUMNA13,0)=0 and (cs.COLUMNA02=ps.COLUMNA02 or cs.COLUMNA02 is null) " +
                " left join MATABLE030 st on st.COLUMN02=ps.COLUMN06 and st.COLUMNA03=ps.COLUMNA03 and isnull(st.COLUMNA13,0)=0 and (st.COLUMNA02=ps.COLUMNA02 or st.COLUMNA02 is null) " +
                " left join MATABLE010 m on m.COLUMN02=p.columna08 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0 and (m.COLUMNA02=p.COLUMNA02 or m.COLUMNA02 is null) " +
                " where isnull(p.COLUMNA13,'False')='False'  and isnull(cs.COLUMN04,'0')!='0' and " + OPUnit + " and (p.COLUMNA03=" + acID + " or p.COLUMNA03 is null) AND  p.COLUMN03=1626  ORDER BY p.COLUMNA06 desc ";
                alCol.AddRange(new List<string> { "ID", "Production#","Date", "Job Order#", "Issue#","Jobber","Current Status","Submitted To", "Created By" });
                actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04","COLUMN11", "COLUMN06", "COLUMN05", "COLUMN07","COLUMN12","COLUMN13", "COLUMN09" });
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(strQry);
                var grid = new WebGrid(GData, canPage: false, canSort: false);
                List<WebGridColumn> cols = new List<WebGridColumn>();
                int index = 0;
                foreach (var column in actCol)
                {
                    cols.Add(grid.Column(alCol[index], alCol[index]));
                    index++;
                }
                ViewBag.columns = cols;
                ViewBag.Grid = GData;
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;
                ViewBag.Receipt = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1286).ToList().FirstOrDefault().COLUMN04.ToString();
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult NewForm()
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;
                List<SelectListItem> Issue = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE007 where COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 ) and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Issue.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                //Routing = Routing.OrderBy(x => x.Text).ToList();
                ViewData["Issue"] = new SelectList(Issue, "Value", "Text");
                List<SelectListItem> JobOrder = new List<SelectListItem>();
                SqlDataAdapter cmdj = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtj = new DataTable();
                cmdj.Fill(dtj);
                for (int dd = 0; dd < dtj.Rows.Count; dd++)
                {
                    JobOrder.Add(new SelectListItem { Value = dtj.Rows[dd]["COLUMN02"].ToString(), Text = dtj.Rows[dd]["COLUMN04"].ToString() });
                }
                //JobOrder = JobOrder.OrderBy(x => x.Text).ToList();
                ViewData["JobOrder"] = new SelectList(JobOrder, "Value", "Text");
                List<SelectListItem> SalesOrder = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1002 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    SalesOrder.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                //SalesOrder = SalesOrder.OrderBy(x => x.Text).ToList();
                ViewData["SalesOrder"] = new SelectList(SalesOrder, "Value", "Text");
                List<SelectListItem> Jobber = new List<SelectListItem>();
                SqlDataAdapter cmdj1 = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN22=22286 and isnull(COLUMN24,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtj1 = new DataTable();
                cmdj1.Fill(dtj1);
                for (int dd = 0; dd < dtj1.Rows.Count; dd++)
                {
                    Jobber.Add(new SelectListItem { Value = dtj1.Rows[dd]["COLUMN02"].ToString(), Text = dtj1.Rows[dd]["COLUMN04"].ToString() });
                }
                Jobber = Jobber.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Jobber, "Value", "Text");
                List<SelectListItem> Opportunity = new List<SelectListItem>();
                SqlDataAdapter cmdo = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dto = new DataTable();
                cmdo.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Opportunity.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN04"].ToString() });
                }
                //Opportunity = Opportunity.OrderBy(x => x.Text).ToList();
                ViewData["Opportunity"] = new SelectList(Opportunity, "Value", "Text");
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmdc = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtc = new DataTable();
                cmdc.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Customer = Customer.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text");
                List<SelectListItem> Status = new List<SelectListItem>();
                var sql1 = "select COLUMN02,COLUMN04 from MATABLE030 where COLUMN08!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN10,'False')='False' and isnull(COLUMNA13,'False')='False' order by case IsNumeric(COLUMN06) when 1 then Replicate(0, 100 - Len(COLUMN06)) + COLUMN06 else COLUMN06 end";
                //SqlDataAdapter cmdrs = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE030 where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter cmdrs = new SqlDataAdapter(sql1, cn);
                DataTable dtrs = new DataTable();
                cmdrs.Fill(dtrs);
                for (int dd = 0; dd < dtrs.Rows.Count; dd++)
                {
                    Status.Add(new SelectListItem { Value = dtrs.Rows[dd]["COLUMN02"].ToString(), Text = dtrs.Rows[dd]["COLUMN04"].ToString() });
                }
                //Status = Status.OrderBy(x => x.Text).ToList();
                ViewData["Status"] = new SelectList(Status, "Value", "Text");
                ViewData["SubmittedTo"] = new SelectList(Status, "Value", "Text");
                List<SelectListItem> Executed = new List<SelectListItem>();
                SqlDataAdapter cmde = new SqlDataAdapter("select COLUMN02,COLUMN09 COLUMN04 from MATABLE010  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dte = new DataTable();
                cmde.Fill(dte);
                for (int dd = 0; dd < dte.Rows.Count; dd++)
                {
                    Executed.Add(new SelectListItem { Value = dte.Rows[dd]["COLUMN02"].ToString(), Text = dte.Rows[dd]["COLUMN04"].ToString() });
                }
                Executed = Executed.OrderBy(x => x.Text).ToList();
                ViewData["ExecutedBy"] = new SelectList(Executed, "Value", "Text",selectedValue:Session["eid"]);
                ViewData["Handedover"] = new SelectList(Executed, "Value", "Text");
                List<SelectListItem> OPunit = new List<SelectListItem>();
                SqlDataAdapter cmdop = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from CONTABLE007 where COLUMN03!='' and  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtop = new DataTable();
                cmdop.Fill(dtop);
                for (int dd = 0; dd < dtop.Rows.Count; dd++)
                {
                    OPunit.Add(new SelectListItem { Value = dtop.Rows[dd]["COLUMN02"].ToString(), Text = dtop.Rows[dd]["COLUMN04"].ToString() });
                }
                OPunit = OPunit.OrderBy(x => x.Text).ToList();
                ViewData["OPunit"] = new SelectList(OPunit, "Value", "Text", Session["OPUnit1"]);
                List<SelectListItem> MachineType = new List<SelectListItem>();
                SqlDataAdapter cmdmt = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  where    ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtmt = new DataTable();
                cmdmt.Fill(dtmt);
                for (int dd = 0; dd < dtmt.Rows.Count; dd++)
                {
                    MachineType.Add(new SelectListItem { Value = dtmt.Rows[dd]["COLUMN02"].ToString(), Text = dtmt.Rows[dd]["COLUMN04"].ToString() });
                }
                MachineType = MachineType.OrderBy(x => x.Text).ToList();
                ViewData["MachineType"] = new SelectList(MachineType, "Value", "Text");
                List<SelectListItem> Machine = new List<SelectListItem>();
                SqlDataAdapter cmdm = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN03=11171 and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtm = new DataTable();
                cmdm.Fill(dtm);
                for (int dd = 0; dd < dtm.Rows.Count; dd++)
                {
                    Machine.Add(new SelectListItem { Value = dtm.Rows[dd]["COLUMN02"].ToString(), Text = dtm.Rows[dd]["COLUMN04"].ToString() });
                }
                Machine = Machine.OrderBy(x => x.Text).ToList();
                ViewData["Machine"] = new SelectList(Machine, "Value", "Text");
                ViewBag.DateFormat = DateFormatsetup();

                var TData = GetGriddetails();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var sql = "select top(1) null UPC,COLUMN05 [Item],null [Item Desc],null [Units],null [No. Of Prints],null [Qty Expected],null [Qty Actual] ,null [Remarks] from MATABLE007  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMN47,'False')='False'  and isnull(COLUMNA13,'False')='False'";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                ViewBag.GData = dbs.Query(sql);
                ViewBag.Cols = TData;
                ViewBag.Processes = dbs.Query(sql1);
                ViewBag.TransNo = GetTransactionNo(1626);
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult GetIssuetoProdDetails(int IssueID)
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                string OPUnitnull = " (p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null) ";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626; string htmlstring = "";
                SqlCommand cmd = new SqlCommand("select p.COLUMN05 Jobber,j.COLUMN53 Customer,p.COLUMN06 JobberNo,p.COLUMN04 IssueNo,p.COLUMN15 Opunit,p.COLUMN01 Hid,j.COLUMN54 Opportunity from SATABLE007 p left join SATABLE005 j on j.COLUMN02=p.COLUMN06 and j.COLUMNA03=p.COLUMNA03  and isnull(j.COLUMNA13,0)=0 Where  p.COLUMN02='" + IssueID + "'  AND " + OPUnitnull + "  AND p.COLUMNA03='" + Session["AcOwner"] + "' and isnull(p.COLUMNA13,'False')='False'", cn);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@IssueID", IssueID));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt1 = new DataTable();
                da.Fill(dt1);
                var JobOrder = "";
                var Opportunity = "";
                var Jobber = "";
                var Issue = "";
                var Hid = "";
                var Customer = "";
                var Operating_Unit = "";
                if (dt1.Rows.Count > 0)
                {
                    JobOrder = dt1.Rows[0]["JobberNo"].ToString();
                    Opportunity = dt1.Rows[0]["Opportunity"].ToString();
                    Jobber = dt1.Rows[0]["Jobber"].ToString();
                    Issue = dt1.Rows[0]["IssueNo"].ToString();
                    Operating_Unit = dt1.Rows[0]["Opunit"].ToString();
                    Hid = dt1.Rows[0]["Hid"].ToString();
                    Customer = dt1.Rows[0]["Customer"].ToString();
                }
                else
                {
                    JobOrder = "";
                    Opportunity = "";
                    Jobber = "";
                    Issue = "";
                    Operating_Unit = "";
                    Hid = "";
                    Customer = "";
                }
                //SqlCommand cmd1 = new SqlCommand("usp_JO_TP_PRD_Line_DATA", cn);
                //cmd1.CommandType = CommandType.StoredProcedure;
                //cmd1.Parameters.Add(new SqlParameter("@IssueID", IssueID));
                //SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                //DataTable dt = new DataTable();
                //da1.Fill(dt);
                //var TData = GetGriddetails();
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var sql = "select p.COLUMN02 RefCols,null UPC,p.COLUMN04 [Item],p.COLUMN05 [Item Desc],p.COLUMN19 UOM,iif((isnull(p.COLUMN09,0))-sum(isnull(pl.COLUMN07,0))>=0,(isnull(p.COLUMN09,0))-sum(isnull(pl.COLUMN07,0)),(p.COLUMN09))[Qty Expected],(isnull(p.COLUMN09,0))-sum(isnull(pl.COLUMN07,0)) [Qty Actual] ,null [Remarks] from SATABLE008 p " +
               "inner join SATABLE007 i on i.COLUMN01=p.COLUMN14 and i.COLUMNA02=p.COLUMNA02 and i.COLUMNA03=p.COLUMNA03 and isnull(i.COLUMNA13,0)=0 " +
               " left join SATABLE020 pl on pl.COLUMN11=(select top(1) os.COLUMN02 FROM SATABLE020 pll inner join SATABLE019 plh on plh.COLUMN01=pll.COLUMN09 and plh.COLUMNA03=pll.COLUMNA03 and plh.COLUMN05=" + IssueID + " left join MATABLE030 os on os.COLUMN02=pll.COLUMN11 and os.COLUMNA03=pll.COLUMNA03 and  isnull(pll.COLUMNA13,0)=0 where os.COLUMN06!='' order by case IsNumeric(os.COLUMN06) when 1 then Replicate(0, 100 - Len(os.COLUMN06)) + os.COLUMN06 else os.COLUMN06 end)  and p.COLUMN02=pl.COLUMN10 and p.COLUMNA02=pl.COLUMNA02 and p.COLUMNA03=pl.COLUMNA03 and isnull(pl.COLUMNA13,0)=0 " +
               "Where  " + OPUnitnull + "   AND p.COLUMN14 in(" + Hid + ")  AND p.COLUMNA03='" + Session["AcOwner"] + "' and isnull(p.COLUMNA13,'False')='False' group by p.COLUMN04,p.COLUMN05,p.COLUMN02,p.COLUMN09,p.COLUMN19 having iif((isnull(p.COLUMN09,0))-sum(isnull(pl.COLUMN07,0))>=0,(isnull(p.COLUMN09,0))-sum(isnull(pl.COLUMN07,0)),(p.COLUMN09))>0";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var GData = dbs.Query(sql);
                SqlCommand cmdd = new SqlCommand(sql, cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);


                List<string> colNM = new List<string>();
                List<WebGridColumn> TData = new List<WebGridColumn>();
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                Item.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> UOM = new List<SelectListItem>();
                SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdataU = new DataTable();
                cmddlU.Fill(dtdataU);
                for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                {
                    UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                }
                UOM = UOM.OrderBy(x => x.Text).ToList();
                UOM.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["UOM"] = new SelectList(UOM, "Value", "Text");
                string dddata = "",dddata1 = "";
                for (int d = 0; d < Item.Count; d++)
                {
                    var OpSelected = "";
                    if (dtd.Rows.Count > 0) OpSelected = dtd.Rows[0]["Item"].ToString();
                    if (Item[d].Value == OpSelected && Item[d].Value != "")
                        dddata += "<option value=" + Item[d].Value + "  selected>" + Item[d].Text + "</option>";
                    else
                        dddata += "<option value=" + Item[d].Value + " >" + Item[d].Text + "</option>";
                }
                for (int d = 0; d < UOM.Count; d++)
                {
                    var UOMSelected = "";
                    if (dtd.Rows.Count > 0) UOMSelected = dtd.Rows[0]["UOM"].ToString();
                    if (UOM[d].Value == UOMSelected && UOM[d].Value != "")
                        dddata1 += "<option value=" + UOM[d].Value + "  selected>" + UOM[d].Text + "</option>";
                    else
                        dddata1 += "<option value=" + UOM[d].Value + " >" + UOM[d].Text + "</option>";
                }
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "",
                    Header = "",
                    Format = (item) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'  value='" + dtd.Rows[0]["RefCols"] + "'   checked='checked' />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "UPC",
                    Header = "UPC",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN03'   class='txtintgridclass' itemid='N' name='UPC'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Item",
                    Header = "Item",
                    Format = (item) => new HtmlString("<select  class =gridddl name='Item' itemid=N    id='SATABLE020COLUMN04'  >" + dddata + "</select>")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Item Desc",
                    Header = "Item Desc",
                    Format = (item) => new HtmlString("<textarea id='SATABLE020COLUMN05' itemid=N  name='Item Desc' class='txtgridclass gridtxtarealength' type='text'>" + dtd.Rows[0]["Item Desc"] + " </textarea>")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Units",
                    Header = "Units",
                    Format = (item) => new HtmlString("<select  class =gridddl name='Units' itemid=N    id='SATABLE020COLUMN13'  >" + dddata1 + "</select>")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "No. Of Prints",
                    Header = "No. Of Prints",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN14'   class='txtintgridclass' itemid='N' name='No. Of Prints'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Qty Expected",
                    Header = "Qty Expected",
                    Format = (item) => new HtmlString("<input  type='text'  id='SATABLE020COLUMN06'  class='txtintgridclass'  itemid='N' name='Qty Expected' value='" + dtd.Rows[0]["Qty Expected"] + "'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Qty Actual",
                    Header = "Qty Actual",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN07' class='txtintgridclass'  itemid='Y' name='Qty Actual'  value='" + dtd.Rows[0]["Qty Actual"] + "' />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Remarks",
                    Header = "Remarks",
                    Format = (item) => new HtmlString("<textarea id='SATABLE020COLUMN08' itemid=N  name='Remarks' class='txtgridclass gridtxtarealength' type='text'></textarea>")

                });
                string dddata2 = "",dddata3 = "";
                for (int i = 1; i < dtd.Rows.Count; i++)
                {
                    dddata2 = ""; dddata3 = "";
                    for (int d = 0; d < Item.Count; d++)
                    {
                        var OpSelected = "";
                        if (dtd.Rows.Count > 0) OpSelected = dtd.Rows[i]["Item"].ToString();
                        if (Item[d].Value == OpSelected && Item[d].Value != "")
                            dddata2 += "<option value=" + Item[d].Value + "  selected>" + Item[d].Text + "</option>";
                        else
                            dddata2 += "<option value=" + Item[d].Value + " >" + Item[d].Text + "</option>";
                    }
                    for (int d = 0; d < UOM.Count; d++)
                    {
                        var UOMSelected = "";
                        if (dtd.Rows.Count > 0) UOMSelected = dtd.Rows[i]["UOM"].ToString();
                        if (UOM[d].Value == UOMSelected && UOM[d].Value != "")
                            dddata3 += "<option value=" + UOM[d].Value + "  selected>" + UOM[d].Text + "</option>";
                        else
                            dddata3 += "<option value=" + UOM[d].Value + " >" + UOM[d].Text + "</option>";
                    }
                    htmlstring = htmlstring + "<tr><td  ><input  type='checkbox' id='checkRow' class=chkclass itemid='Y' name='' checked=checked   value='" + dtd.Rows[i]["RefCols"] + "' /></td>";
                    htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN03' class=txtintgridclass   value=''  name=UPC /></td>";
                    htmlstring = htmlstring + "<td  ><select  class =gridddl name='Item' itemid=Y    id='SATABLE020COLUMN04'  >" + dddata2 + "</select></td>";
                    htmlstring = htmlstring + "<td  ><textarea id='SATABLE020COLUMN05'  class='txtgridclass gridtxtarealength' name=Item Desc>" + dtd.Rows[i]["Item Desc"] + "</textarea></td>";
                    htmlstring = htmlstring + "<td  ><select  class =gridddl name='Units' itemid=N    id='SATABLE020COLUMN13'  >" + dddata3 + "</select></td>";
                    htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN14' class=txtintgridclass   value=''  name='No. Of Prints' /></td>";
                    htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN06' class=txtintgridclass value='" + dtd.Rows[i]["Qty Expected"] + "'  itemid='Y' name=Qty Expected  /></td>";
                    htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN07'  class=txtintgridclass   value='" + dtd.Rows[i]["Qty Actual"] + "'  name=Qty Actual /></td>";
                    htmlstring = htmlstring + "<td  ><textarea id='SATABLE020COLUMN08' class='txtgridclass gridtxtarealength'  name='Remarks'>" + dtd.Rows[i]["Remarks"] + "</textarea></td></tr>";
                }
                var grid1 = new WebGrid(GData, canPage: false, canSort: false);
                var htmlstring1 = grid1.GetHtml(mode: WebGridPagerModes.All, tableStyle: "webgrid-table",
                headerStyle: "webgrid-header",
                footerStyle: "webgrid-footer",
                alternatingRowStyle: "webgrid-alternating-row",
                rowStyle: "webgrid-row-style",
                htmlAttributes: new { id = "grdData" },
                columns: TData);
                return Json(new
                {
                    grid = htmlstring1.ToHtmlString(),
                    grid1 = htmlstring,
                    pid = IssueID,
                    Data1 = JobOrder,
                    Data2 = Jobber,
                    Data3 = Issue,
                    Data4 = Operating_Unit,
                    Data5 = Customer,
                    Data6 = Opportunity
                },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public string DateFormatsetup()
        {
            string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and isnull(COLUMNA13,0)=0";
            SqlDataAdapter daf = new SqlDataAdapter(str, cn);
            DataTable dtf = new DataTable();
            daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
            if (dtf.Rows.Count > 0)
            {
                DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
                Session["DateFormat"] = DateFormat;
                ViewBag.DateFormat = DateFormat;
            }
            if (JQDateFormat != "")
            {
                Session["DateFormat"] = DateFormat;
                Session["ReportDate"] = JQDateFormat;
            }
            else
            {
                Session["DateFormat"] = "dd/MM/yyyy";
                Session["ReportDate"] = "dd/mm/yy";
                ViewBag.DateFormat = "dd/MM/yyyy";
            }
            return ViewBag.DateFormat;
        }

        public List<WebGridColumn> GetGriddetails()
        {
            try
            {
                List<string> colNM = new List<string>();
                List<WebGridColumn> TData = new List<WebGridColumn>();
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                Item.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                string dddata = "";
                for (int d = 0; d < Item.Count; d++)
                    dddata += "<option value=" + Item[d].Value + " >" + Item[d].Text + "</option>";

                List<SelectListItem> UOM = new List<SelectListItem>();
                SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdataU = new DataTable();
                cmddlU.Fill(dtdataU);
                for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                {
                    UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                }
                UOM = UOM.OrderBy(x => x.Text).ToList();
                UOM.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["UOM"] = new SelectList(UOM, "Value", "Text");
                string dddata1 = "";
                for (int d = 0; d < UOM.Count; d++)
                    dddata1 += "<option value=" + UOM[d].Value + " >" + UOM[d].Text + "</option>";
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "",
                    Header = "",
                    Format = (item) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'    checked='checked' />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "UPC",
                    Header = "UPC",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN03'   class='txtintgridclass' itemid='N' name='UPC'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Item",
                    Header = "Item",
                    Format = (item) => new HtmlString("<select  class =gridddl style=width:180px name='Item' itemid=N    id='SATABLE020COLUMN04'  >" + dddata + "</select>")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Item Desc",
                    Header = "Item Desc",
                    Format = (item) => new HtmlString("<textarea id='SATABLE020COLUMN05' itemid=N  name='Item Desc' class='txtgridclass gridtxtarealength' type='text'></textarea>")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Units",
                    Header = "Units",
                    Format = (item) => new HtmlString("<select  class =gridddl style=width:180px name='Units' itemid=N    id='SATABLE020COLUMN13'  >" + dddata1 + "</select>")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "No. Of Prints",
                    Header = "No. Of Prints",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN14'   class='txtintgridclass' itemid='N' name='No. Of Prints'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Qty Expected",
                    Header = "Qty Expected",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN06'  class='txtintgridclass'  itemid='N' name='Qty Expected'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Qty Actual",
                    Header = "Qty Actual",
                    Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN07' class='txtintgridclass'  itemid='Y' name='Qty Actual'  />")

                });
                TData.Add(new WebGridColumn()
                {
                    ColumnName = "Remarks",
                    Header = "Remarks",
                    Format = (item) => new HtmlString("<textarea id='SATABLE020COLUMN08' itemid=N  name='Remarks' class='txtgridclass gridtxtarealength' type='text'></textarea>")

                });
                return TData;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }

        public string GetTransactionNo(int frmID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                var idc = dc.CONTABLE0010.Where(q => q.COLUMN02 == frmID).ToList();
                var frmIDc = idc.Select(a => a.COLUMN06).FirstOrDefault();
                int frmIDchk = Convert.ToInt32(frmIDc);
                int? Oper = Convert.ToInt32(Session["OPUnit1"]);
                if (Oper == 0)
                    Oper = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == frmID).FirstOrDefault().COLUMN04.ToString();
                var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                var Tranlist = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW).ToList();
                if (Oper == null)
                {
                    listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                }
                if (listTM == null)
                {
                    listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA03 == acOW && p.COLUMNA02 == Oper).FirstOrDefault();
                    if (Oper == null)
                    {
                        Tranlist = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW).ToList();
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                    }
                    if (listTM == null)
                    {
                        listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                    }
                }
                var Prefix = "";
                var Endno = "";
                var Sufix = " ";
                var CurrentNo = "";
                var sNum = "";
                int c = 0;
                string fino = "", pono = "", TransNo = "";
                SqlCommand pacmd = new SqlCommand();
                if (listTM != null)
                {
                    Prefix = listTM.COLUMN06.ToString();
                    Sufix = listTM.COLUMN10.ToString();
                    sNum = listTM.COLUMN07.ToString();
                    CurrentNo = listTM.COLUMN09.ToString();
                    ViewBag.Override = listTM.COLUMN12.ToString();
                    //ViewBag.ColumnName = all1[alco + row].Field_Name;
                    c = Prefix.Length + 1;
                    if (string.IsNullOrEmpty(Sufix))
                        Sufix = " ";
                }
                else
                {
                    Prefix = null;
                }
                frmIDchk = Convert.ToInt32(frmID);

                if (frmIDchk == 1626)
                {
                    if (Prefix != null)
                    {
                        fino = Prefix;
                        pacmd = new SqlCommand("Select  substring(COLUMN04," + c + ",len(COLUMN04)) PO FROM SATABLE019  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE019  Where COLUMN03='" + frmID + "' and " + Session["OPUnitWithNull"] + "  AND COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                    else
                    {
                        fino = "PR";
                        pacmd = new SqlCommand("Select  substring(COLUMN04,3,len(COLUMN04)) PO FROM SATABLE019  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "'  AND COLUMN02=(Select max(COLUMN02) PO FROM SATABLE019  Where COLUMN03='" + frmID + "' and  COLUMNA03='" + Session["AcOwner"] + "' AND isnull(COLUMNA13,0)=0 AND COLUMN04 LIKE '" + fino + "%') AND COLUMN04 LIKE '" + fino + "%'  ORDER BY COLUMN02 DESC", cn);
                    }
                }
                SqlDataAdapter pada = new SqlDataAdapter(pacmd);
                DataTable padt = new DataTable();
                pada.Fill(padt);
                //pacmd.ExecuteNonQuery(); 
                var paddingno = "00000"; //var paddingnolength = "";
                int tranrowcount = padt.Rows.Count;
                if (tranrowcount > 0)
                {
                    if (padt.Rows[0][0].ToString() == "" || padt.Rows[0][0].ToString() == null)
                    {
                        if (sNum == "") sNum = "00001";
                        else sNum = sNum;
                        pono = fino + sNum + Sufix;
                        Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        TransNo = (pono).TrimEnd();
                    }
                    else
                    {
                        if ((sNum != "" && sNum != null) && (sNum == CurrentNo))
                        {
                            pono = (fino + ((Convert.ToInt32(sNum) + 1).ToString(paddingno)) + Sufix).TrimEnd();
                            Session["CurrentNo"] = (Convert.ToInt32(sNum) + 1);
                        }
                        else if (CurrentNo != "" && CurrentNo != null)
                        {
                            pono = fino + ((Convert.ToInt32(CurrentNo) + 1).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(CurrentNo) + 1);
                        }
                        else if ((sNum != "" && sNum != null && CurrentNo != "" && CurrentNo != null))
                        {
                            sNum = (Convert.ToInt32(sNum) + 0).ToString();
                            pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(sNum));
                        }
                        else
                        {
                            sNum = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")).ToString());
                            pono = fino + (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1).ToString(paddingno) + Sufix;
                            Session["CurrentNo"] = (Convert.ToInt32(padt.Rows[0][0].ToString().Replace(Sufix, "")) + 1);
                        }
                        TransNo = pono.TrimEnd();
                    }
                }
                else
                {
                    if (sNum == "") sNum = "00001";
                    else sNum = sNum;
                    if (CurrentNo == "") CurrentNo = "00001";
                    sNum = (Convert.ToInt32(sNum) + 0).ToString();
                    if (listTM != null)
                    {
                        if (padt.Rows.Count == 0) { CurrentNo = (Convert.ToInt32(CurrentNo)).ToString(); sNum = (Convert.ToInt32(sNum)).ToString(); }
                        else
                        {
                            CurrentNo = (Convert.ToInt32(CurrentNo) + 1).ToString(); sNum = (Convert.ToInt32(sNum) + 1).ToString();
                        }
                    }
                    if (CurrentNo != "" && CurrentNo != null)
                    { pono = fino + ((Convert.ToInt32(CurrentNo)).ToString(paddingno)) + Sufix; sNum = CurrentNo; }
                    else
                        pono = fino + ((Convert.ToInt32(sNum)).ToString(paddingno)) + Sufix;
                    Session["CurrentNo"] = (Convert.ToInt32(sNum));
                    TransNo = (pono.TrimEnd());
                }
                return TransNo;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                var idi = "";
                if (Request.UrlReferrer == null) return null;
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", ""); int m = custform.IndexOf("&"); if (m > 0) { idi = custform.Substring(0, m).Replace("idi=", "").ToString(); custform = custform.Remove(0, m + 1); } custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { custform = custform.Substring(0, n); }
                return null;
            }
        }

        [HttpPost]
        public ActionResult PRDSaveGrid(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                Session["PRDGridData"] = ds;
                return Json("");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "Production", new { FormName = Session["FormName"] });
            }
        }

        
        SqlCommand Cmd;
        [HttpPost]
        public ActionResult NewForm(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                int OutParam = 0; string CreatedDate = "", DueDate = "", OpUnit = "",SubmittedTo="";
                string Date = DateTime.Now.ToString(); CreatedDate = col["Date"]; DueDate = col["Due Date"];
                if (CreatedDate == "") CreatedDate = Date;
                else { CreatedDate = DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                if (CreatedDate == "") CreatedDate = null;
                if (DueDate == "") DueDate = Date;
                else { DueDate = DateTime.ParseExact(DueDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                if (DueDate == "") DueDate = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["PRDGridData"]; 
                OpUnit=col["OPunit"];
                if(OpUnit==""||OpUnit=="0")OpUnit=null;
                if (col["OutParam"] != "" && col["OutParam"] != null) OutParam =Convert.ToInt32(col["OutParam"]);
                SubmittedTo = col["SubmittedTo"];
                if (SubmittedTo == "" || SubmittedTo == "0") SubmittedTo = null;
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;

                string insert = "Insert", TransNo = ""; int r = 0, l = 0;
                SqlCommand cmdd = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE019 ", cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                string HCol2 = dtt.Rows[0][0].ToString();
                if (HCol2 == "") HCol2 = "999";
                HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_JO_BL_Production", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1626");
                    Cmd.Parameters.AddWithValue("@COLUMN04", col["Production"]);
                    Cmd.Parameters.AddWithValue("@COLUMN05", col["Issue"]);
                    Cmd.Parameters.AddWithValue("@COLUMN06", col["JobOrder"]);
                    Cmd.Parameters.AddWithValue("@COLUMN07", col["Jobber"]);
                    Cmd.Parameters.AddWithValue("@COLUMN08", col["SalesOrder"]);
                    Cmd.Parameters.AddWithValue("@COLUMN09", col["Opportunity"]);
                    Cmd.Parameters.AddWithValue("@COLUMN10", col["Customer"]);
                    Cmd.Parameters.AddWithValue("@COLUMN11", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN12", col["Status"]);
                    Cmd.Parameters.AddWithValue("@COLUMN13", SubmittedTo);
                    Cmd.Parameters.AddWithValue("@COLUMN14", DueDate);
                    Cmd.Parameters.AddWithValue("@COLUMN15", col["ExecutedBy"]);
                    Cmd.Parameters.AddWithValue("@COLUMN16", col["Handedover"]);
                    //Cmd.Parameters.AddWithValue("@COLUMN17", col["SubmittedTo"]);
                    Cmd.Parameters.AddWithValue("@COLUMN18", col["Remarks "]);
                    Cmd.Parameters.AddWithValue("@COLUMN19", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN20", col["MachineType"]);
                    Cmd.Parameters.AddWithValue("@COLUMN21", col["Machine"]);
                    Cmd.Parameters.AddWithValue("@COLUMN22", col["Processing In"]);
                    Cmd.Parameters.AddWithValue("@COLUMN23", col["Processing Out"]);
                    Cmd.Parameters.AddWithValue("@COLUMN24", col["Processing Duration"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE019");
                    Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cnc.Open();
                    r = Cmd.ExecuteNonQuery();
                    OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                }
                if (col["OutParam"] == "" || col["OutParam"] == null)
                {
                    eBizSuiteAppModel.Table.Entities act1 = new eBizSuiteAppModel.Table.Entities();
                    int? Oper = Convert.ToInt32(Session["AUTO"]);
                    if (Oper == null || Oper == 0)
                        Oper = Convert.ToInt32(Session["OPUnit1"]);
                    if (Oper == 0)
                        Oper = null;
                    var custfname = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).FirstOrDefault().COLUMN04.ToString();
                    var listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02 == Oper).FirstOrDefault();
                    if (r > 0)
                    {
                        if (Oper == null)
                            listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();
                        if (listTM == null)
                        {
                            listTM = act1.MYTABLE002.Where(p => p.COLUMN11 == custfname && p.COLUMNA02 == Oper && p.COLUMNA03 == acOW).FirstOrDefault();
                            if (Oper == null)
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN11 == custfname && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(Oper)).FirstOrDefault();

                            if (listTM == null)
                            {
                                listTM = act1.MYTABLE002.Where(a => a.COLUMN05 == custfname && a.COLUMN11 == "default" && a.COLUMNA03 == acOW && a.COLUMNA02.Equals(null)).FirstOrDefault();
                            }
                        }
                        if (listTM != null)
                        {
                            MYTABLE002 product = listTM;
                            if (listTM.COLUMN07 == "") listTM.COLUMN07 = "1";
                            if (listTM.COLUMN09 == "")
                                product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN07)).ToString();
                            else
                                product.COLUMN09 = (Convert.ToInt32(listTM.COLUMN09) + 1).ToString();
                            act1.SaveChanges();
                        }
                    }
                }
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE020 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    if (LCol2 == "") LCol2 = "999";
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string UPC = itemdata.Tables[0].Rows[i]["UPC"].ToString();
                    string RefCols = itemdata.Tables[0].Rows[i]["RefCols"].ToString();
                    string Item = itemdata.Tables[0].Rows[i]["Item"].ToString();
                    string Desc = itemdata.Tables[0].Rows[i]["Desc"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string Prints = itemdata.Tables[0].Rows[i]["Prints"].ToString();
                    string EQty = itemdata.Tables[0].Rows[i]["EQty"].ToString();
                    string AQty = itemdata.Tables[0].Rows[i]["AQty"].ToString();
                    string Remarks = itemdata.Tables[0].Rows[i]["Remarks"].ToString();
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_JO_BL_Production", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN09", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", UPC);
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Item);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Desc);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", EQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN07", AQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN08", Remarks);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", RefCols);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", col["Status"]);
                        Cmdl.Parameters.AddWithValue("@COLUMN12", SubmittedTo);
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN14", Prints);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", acOW);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE020");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                Session["PRDGridData"] = null;
                var msg = string.Empty;
                if (l > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1626 && q.COLUMN05 == 1).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Process Successfully Created ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1626 && q.COLUMN05 == 0).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Process Creation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                string lineid = "", Submittedcol="", Submittedcol1="";
                if (SubmittedTo != "" && SubmittedTo !=null) { Submittedcol = " and l.COLUMN11=" + SubmittedTo + ""; Submittedcol1 = " and pl.COLUMN11=" + SubmittedTo + ""; }
                SqlCommand cmdi = new SqlCommand("select top(1) l.COLUMN09 FROM SATABLE019 h inner join SATABLE020 l on l.COLUMN09=h.COLUMN01 and h.COLUMNA03=l.COLUMNA03 and isnull(l.COLUMNA13,0)=0  where h.COLUMN05=" + col["Issue"] + " " + Submittedcol + " AND l.COLUMNA03=" + acOW + " order by case IsNumeric(l.COLUMN09) when 1 then Replicate(0, 100 - Len(l.COLUMN09)) + l.COLUMN09 else l.COLUMN09 end desc", cn);
                SqlDataAdapter dadi = new SqlDataAdapter(cmdi);
                DataTable dtdi = new DataTable();
                dadi.Fill(dtdi);
                if (dtdi.Rows.Count > 0) { lineid = dtdi.Rows[0][0].ToString(); lineid = "pl.COLUMN09=" + lineid + " and "; }
                //List<SelectListItem> ActqtyList = new List<SelectListItem>();
                List<ProcedureOrderDetails> ActqtyList = new List<ProcedureOrderDetails>();
                var sql = "select pl.COLUMN04,sum(isnull(pl.COLUMN06,0))-sum(isnull(pl.COLUMN07,0)) [QtyActual],pl.COLUMN10 from SATABLE019 plh  inner join   SATABLE020 pl on " + lineid + " plh.COLUMN01=pl.COLUMN09 and plh.COLUMNA03=pl.COLUMNA03 " + Submittedcol1 + "  and isnull(pl.COLUMNA13,0)=0 Where  plh.COLUMN05=" + col["Issue"] + " and  (plh.COLUMNA02 in(" + OpUnit + ") or plh.COLUMNA02 is null)   AND plh.COLUMNA03=" + acOW + " and isnull(plh.COLUMNA13,'False')='False' group by pl.COLUMN04,pl.COLUMN10";
                SqlCommand cmd = new SqlCommand(sql, cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);
                for (int dd = 0; dd < dtd.Rows.Count; dd++)
                {
                    ActqtyList.Add(new ProcedureOrderDetails { Item = dtd.Rows[dd]["COLUMN04"].ToString(), Qty = dtd.Rows[dd]["QtyActual"].ToString(), IssueNo = dtd.Rows[dd]["COLUMN10"].ToString() });
                }
                return Json(new { status = col["SubmittedTo"], OutParam = OutParam, ActqtyList = ActqtyList, MessageFrom = msg, SuccessMessageFrom = Session["SuccessMessageFrom"] }, JsonRequestBehavior.AllowGet);
                return RedirectToAction("Info", "Production", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public class ProductionEditModel
        {
            public string Productionid { get; set; }
            public string Production { get; set; }
            public string Jobber { get; set; }
            public string Customer { get; set; }
            public string JobOrder { get; set; }
            public string SalesOrder { get; set; }
            public string Opportunity { get; set; }
            public string Date { get; set; }
            public string CurrentStatus { get; set; }
            public string SubmittedTo { get; set; }
            public string DueDate { get; set; }
            public string ExecutedBy { get; set; }
            public string HandedoverTo { get; set; }
            public string Remarks { get; set; }
            public string Issue { get; set; }
            public string OperatingUnit { get; set; }
            public string Machineype { get; set; }
            public string Machine { get; set; }
            public string ProcessingIn { get; set; }
            public string ProcessingOut { get; set; }
            public string ProcessingDuration { get; set; }
            public string company { get; set; }
            public string cAdresss { get; set; }
            public string cAdresss1 { get; set; }
            public string ccity { get; set; }
            public string cstate { get; set; }
            public string ccountry { get; set; }
            public string czip { get; set; }
            public string logoName { get; set; } 
            public string StateLincense { get; set; }
            public string CntralLincese { get; set; }
            public string tin { get; set; } 
            public string oAdresss { get; set; }
            public string oAdresss1 { get; set; }
            public string oAdresss2 { get; set; }
            public string ocity { get; set; }
            public string ostate { get; set; }
            public string ocountry { get; set; }
            public string ozip { get; set; }
            public string opDrugLIC { get; set; }
            public string douPhno { get; set; }
            public string CVATNO { get; set; }
            public string CCSTNO { get; set; }
            public string CPANNO { get; set; }
            public string CServiceTaxNO { get; set; }
            public string CustomerAddress { get; set; }
            public string ouName { get; set; }
            public string OPUVATNO { get; set; }
            public string OPUCSTNO { get; set; }
            public string OPUPANNO { get; set; }
            public string OPUPhNo { get; set; }
        }
        public class ProcedureOrderDetails
        {
            public string IssueNo { get; set; }
            public string Item { get; set; }
            public string Qty { get; set; }
        }
        public ActionResult ProductionEdit(string ide)
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;
                var sqle = "select * from SATABLE019 where COLUMN02=" + ide + " and COLUMNA03=" + acOW + " and isnull(COLUMNA13,0)=0 and  (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null)  ";
                SqlCommand cmd = new SqlCommand(sqle, cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);

                List<ProductionEditModel> all = new List<ProductionEditModel>();
                all.Add(new MeBizSuiteAppUI.Controllers.ProductionController.ProductionEditModel
                {
                    Productionid = "",
                    Production  = "",
                    Jobber  = "",
                    Customer  = "",
                    JobOrder  = "",
                    SalesOrder  = "",
                    Opportunity  = "",
                    Date  = "",
                    CurrentStatus  = "",
                    SubmittedTo  = "",
                    DueDate  = "",
                    ExecutedBy  = "",
                    HandedoverTo  = "",
                    Remarks  = "",
                    Issue  = "",
                    OperatingUnit  = "",
                    Machineype  = "",
                    Machine  = "",
                    ProcessingIn  = "",
                    ProcessingOut  = "",
                    ProcessingDuration=""
                });
                all[0].Productionid = dtd.Rows[0]["COLUMN02"].ToString();
                all[0].Production  = dtd.Rows[0]["COLUMN04"].ToString();
                all[0].Jobber  = dtd.Rows[0]["COLUMN07"].ToString();
                all[0].Customer  = dtd.Rows[0]["COLUMN10"].ToString();
                all[0].JobOrder  = dtd.Rows[0]["COLUMN06"].ToString();
                all[0].SalesOrder  = dtd.Rows[0]["COLUMN08"].ToString();
                all[0].Opportunity  = dtd.Rows[0]["COLUMN09"].ToString();
                all[0].Date  = dtd.Rows[0]["COLUMN11"].ToString();
                all[0].CurrentStatus  = dtd.Rows[0]["COLUMN12"].ToString();
                all[0].SubmittedTo  = dtd.Rows[0]["COLUMN13"].ToString();
                all[0].DueDate  = dtd.Rows[0]["COLUMN14"].ToString();
                all[0].ExecutedBy  = dtd.Rows[0]["COLUMN15"].ToString();
                all[0].HandedoverTo  = dtd.Rows[0]["COLUMN16"].ToString();
                all[0].Remarks  = dtd.Rows[0]["COLUMN18"].ToString();
                all[0].Issue  = dtd.Rows[0]["COLUMN05"].ToString();
                all[0].OperatingUnit  = dtd.Rows[0]["COLUMN19"].ToString();
                all[0].Machineype  = dtd.Rows[0]["COLUMN20"].ToString();
                all[0].Machine  = dtd.Rows[0]["COLUMN21"].ToString();
                all[0].ProcessingIn  = dtd.Rows[0]["COLUMN22"].ToString();
                all[0].ProcessingOut  = dtd.Rows[0]["COLUMN23"].ToString();
                all[0].ProcessingDuration = dtd.Rows[0]["COLUMN24"].ToString();
                List<SelectListItem> Issue = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE007 where COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 ) and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Issue.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                //Routing = Routing.OrderBy(x => x.Text).ToList();
                ViewData["Issue"] = new SelectList(Issue, "Value", "Text", selectedValue: all[0].Issue);
                List<SelectListItem> JobOrder = new List<SelectListItem>();
                SqlDataAdapter cmdj = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtj = new DataTable();
                cmdj.Fill(dtj);
                for (int dd = 0; dd < dtj.Rows.Count; dd++)
                {
                    JobOrder.Add(new SelectListItem { Value = dtj.Rows[dd]["COLUMN02"].ToString(), Text = dtj.Rows[dd]["COLUMN04"].ToString() });
                }
                //JobOrder = JobOrder.OrderBy(x => x.Text).ToList();
                ViewData["JobOrder"] = new SelectList(JobOrder, "Value", "Text", selectedValue: all[0].JobOrder);
                List<SelectListItem> SalesOrder = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1002 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    SalesOrder.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                //SalesOrder = SalesOrder.OrderBy(x => x.Text).ToList();
                ViewData["SalesOrder"] = new SelectList(SalesOrder, "Value", "Text", selectedValue: all[0].SalesOrder);
                List<SelectListItem> Jobber = new List<SelectListItem>();
                SqlDataAdapter cmdj1 = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN22=22286 and isnull(COLUMN24,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtj1 = new DataTable();
                cmdj1.Fill(dtj1);
                for (int dd = 0; dd < dtj1.Rows.Count; dd++)
                {
                    Jobber.Add(new SelectListItem { Value = dtj1.Rows[dd]["COLUMN02"].ToString(), Text = dtj1.Rows[dd]["COLUMN04"].ToString() });
                }
                Jobber = Jobber.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Jobber, "Value", "Text", selectedValue: all[0].Jobber);
                List<SelectListItem> Opportunity = new List<SelectListItem>();
                SqlDataAdapter cmdo = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dto = new DataTable();
                cmdo.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Opportunity.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN04"].ToString() });
                }
                //Opportunity = Opportunity.OrderBy(x => x.Text).ToList();
                ViewData["Opportunity"] = new SelectList(Opportunity, "Value", "Text", selectedValue: all[0].Opportunity);
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmdc = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtc = new DataTable();
                cmdc.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Customer = Customer.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: all[0].Customer);
                List<SelectListItem> Status = new List<SelectListItem>();
                var sql1 = "select COLUMN02,COLUMN04 from MATABLE030 where COLUMN08!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN10,'False')='False' and isnull(COLUMNA13,'False')='False' order by case IsNumeric(COLUMN06) when 1 then Replicate(0, 100 - Len(COLUMN06)) + COLUMN06 else COLUMN06 end";
                //SqlDataAdapter cmdrs = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE030 where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter cmdrs = new SqlDataAdapter(sql1, cn);
                DataTable dtrs = new DataTable();
                cmdrs.Fill(dtrs);
                for (int dd = 0; dd < dtrs.Rows.Count; dd++)
                {
                    Status.Add(new SelectListItem { Value = dtrs.Rows[dd]["COLUMN02"].ToString(), Text = dtrs.Rows[dd]["COLUMN04"].ToString() });
                }
                //Status = Status.OrderBy(x => x.Text).ToList();
                ViewData["Status"] = new SelectList(Status, "Value", "Text", selectedValue: all[0].CurrentStatus);
                ViewData["SubmittedTo"] = new SelectList(Status, "Value", "Text", selectedValue: all[0].SubmittedTo);
                List<SelectListItem> Executed = new List<SelectListItem>();
                SqlDataAdapter cmde = new SqlDataAdapter("select COLUMN02,COLUMN09 COLUMN04 from MATABLE010  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dte = new DataTable();
                cmde.Fill(dte);
                for (int dd = 0; dd < dte.Rows.Count; dd++)
                {
                    Executed.Add(new SelectListItem { Value = dte.Rows[dd]["COLUMN02"].ToString(), Text = dte.Rows[dd]["COLUMN04"].ToString() });
                }
                Executed = Executed.OrderBy(x => x.Text).ToList();
                ViewData["ExecutedBy"] = new SelectList(Executed, "Value", "Text", selectedValue: all[0].ExecutedBy);
                ViewData["Handedover"] = new SelectList(Executed, "Value", "Text", selectedValue: all[0].HandedoverTo);
                List<SelectListItem> OPunit = new List<SelectListItem>();
                SqlDataAdapter cmdop = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from CONTABLE007 where COLUMN03!='' and  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtop = new DataTable();
                cmdop.Fill(dtop);
                for (int dd = 0; dd < dtop.Rows.Count; dd++)
                {
                    OPunit.Add(new SelectListItem { Value = dtop.Rows[dd]["COLUMN02"].ToString(), Text = dtop.Rows[dd]["COLUMN04"].ToString() });
                }
                OPunit = OPunit.OrderBy(x => x.Text).ToList();
                ViewData["OPunit"] = new SelectList(OPunit, "Value", "Text", selectedValue: all[0].OperatingUnit);
                List<SelectListItem> MachineType = new List<SelectListItem>();
                SqlDataAdapter cmdmt = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  where    ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtmt = new DataTable();
                cmdmt.Fill(dtmt);
                for (int dd = 0; dd < dtmt.Rows.Count; dd++)
                {
                    MachineType.Add(new SelectListItem { Value = dtmt.Rows[dd]["COLUMN02"].ToString(), Text = dtmt.Rows[dd]["COLUMN04"].ToString() });
                }
                MachineType = MachineType.OrderBy(x => x.Text).ToList();
                ViewData["MachineType"] = new SelectList(MachineType, "Value", "Text", selectedValue: all[0].Machineype);
                List<SelectListItem> Machine = new List<SelectListItem>();
                SqlDataAdapter cmdm = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN03=11171 and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtm = new DataTable();
                cmdm.Fill(dtm);
                for (int dd = 0; dd < dtm.Rows.Count; dd++)
                {
                    Machine.Add(new SelectListItem { Value = dtm.Rows[dd]["COLUMN02"].ToString(), Text = dtm.Rows[dd]["COLUMN04"].ToString() });
                }
                Machine = Machine.OrderBy(x => x.Text).ToList();
                ViewData["Machine"] = new SelectList(Machine, "Value", "Text", selectedValue: all[0].Machine);
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                Item.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> UOM = new List<SelectListItem>();
                SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdataU = new DataTable();
                cmddlU.Fill(dtdataU);
                for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                {
                    UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                }
                UOM = UOM.OrderBy(x => x.Text).ToList();
                UOM.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["UOM"] = new SelectList(UOM, "Value", "Text");
                ViewBag.DateFormat = DateFormatsetup();

                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                ViewBag.Processes = dbs.Query(sql1);
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult ProductionEdit(FormCollection col, HttpPostedFileBase hb)
        {
            try
            {
                int OutParam = 0, Productionid = 0; string CreatedDate = "", DueDate = "", OpUnit = "", SubmittedTo = "";
                string Date = DateTime.Now.ToString(); CreatedDate = col["Date"]; DueDate = col["Due Date"];
                if (CreatedDate == "") CreatedDate = Date;
                else { CreatedDate = DateTime.ParseExact(CreatedDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                if (CreatedDate == "") CreatedDate = null;
                if (DueDate == "") DueDate = Date;
                else { DueDate = DateTime.ParseExact(DueDate, Session["DateFormat"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture); }
                if (DueDate == "") DueDate = null;
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                DataSet itemdata = new DataSet();
                itemdata = (DataSet)Session["PRDGridData"];
                OpUnit = col["OPunit"];
                if (OpUnit == "" || OpUnit == "0") OpUnit = null;
                if (col["Productionid"] != "" && col["Productionid"] != null) Productionid = Convert.ToInt32(col["Productionid"]);
                SubmittedTo = col["SubmittedTo"];
                if (SubmittedTo == "" || SubmittedTo == "0") SubmittedTo = null;
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;

                string insert = "Update",sqlpi = ""; int r = 0, l = 0; string HCol2 = "";
                if (Productionid != 0)
                    sqlpi = "select  COLUMN01,COLUMN02 from SATABLE019 where COLUMN02=" + Productionid + "";
                else
                    sqlpi = "select  Max(isnull(COLUMN02,999)) from SATABLE019 ";
                SqlCommand cmdd = new SqlCommand(sqlpi, cn);
                SqlDataAdapter daa = new SqlDataAdapter(cmdd);
                DataTable dtt = new DataTable();
                daa.Fill(dtt);
                if (Productionid != 0 && dtt.Rows.Count > 0)
                {
                    HCol2 = dtt.Rows[0][1].ToString();
                    OutParam = Convert.ToInt32(dtt.Rows[0][0].ToString());
                }
                else
                {
                    HCol2 = dtt.Rows[0][0].ToString();
                    if (HCol2 == "") HCol2 = "999";
                    HCol2 = (Convert.ToInt32(HCol2) + 1).ToString();
                    insert = "Insert";
                }
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_JO_BL_Production", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMN03", "1626");
                    Cmd.Parameters.AddWithValue("@COLUMN04", col["Production"]);
                    Cmd.Parameters.AddWithValue("@COLUMN05", col["Issue"]);
                    Cmd.Parameters.AddWithValue("@COLUMN06", col["JobOrder"]);
                    Cmd.Parameters.AddWithValue("@COLUMN07", col["Jobber"]);
                    Cmd.Parameters.AddWithValue("@COLUMN08", col["SalesOrder"]);
                    Cmd.Parameters.AddWithValue("@COLUMN09", col["Opportunity"]);
                    Cmd.Parameters.AddWithValue("@COLUMN10", col["Customer"]);
                    Cmd.Parameters.AddWithValue("@COLUMN11", CreatedDate);
                    Cmd.Parameters.AddWithValue("@COLUMN12", col["Status"]);
                    Cmd.Parameters.AddWithValue("@COLUMN13", SubmittedTo);
                    Cmd.Parameters.AddWithValue("@COLUMN14", DueDate);
                    Cmd.Parameters.AddWithValue("@COLUMN15", col["ExecutedBy"]);
                    Cmd.Parameters.AddWithValue("@COLUMN16", col["Handedover"]);
                    //Cmd.Parameters.AddWithValue("@COLUMN17", col["SubmittedTo"]);
                    Cmd.Parameters.AddWithValue("@COLUMN18", col["Remarks "]);
                    Cmd.Parameters.AddWithValue("@COLUMN19", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMN20", col["MachineType"]);
                    Cmd.Parameters.AddWithValue("@COLUMN21", col["Machine"]);
                    Cmd.Parameters.AddWithValue("@COLUMN22", col["Processing In"]);
                    Cmd.Parameters.AddWithValue("@COLUMN23", col["Processing Out"]);
                    Cmd.Parameters.AddWithValue("@COLUMN24", col["Processing Duration"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 0);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE019");
                    if (Productionid == 0)
                        Cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cnc.Open();
                    r = Cmd.ExecuteNonQuery();
                    if (Productionid == 0)
                        OutParam = Convert.ToInt32(Cmd.Parameters["@ReturnValue"].Value);
                }
                //Line
                for (int i = 0; i < itemdata.Tables[0].Rows.Count; i++)
                {
                    SqlCommand cmddl = new SqlCommand("select  Max(isnull(COLUMN02,999)) from SATABLE020 ", cn);
                    SqlDataAdapter daal = new SqlDataAdapter(cmddl);
                    DataTable dttl = new DataTable();
                    daal.Fill(dttl);
                    string LCol2 = dttl.Rows[0][0].ToString();
                    if (LCol2 == "") LCol2 = "999";
                    LCol2 = (Convert.ToInt32(LCol2) + 1).ToString();
                    string UPC = itemdata.Tables[0].Rows[i]["UPC"].ToString();
                    string RefCols = itemdata.Tables[0].Rows[i]["RefCols"].ToString();
                    if (RefCols != "0" && RefCols != null) LCol2 = RefCols;
                    string IIRefCols = itemdata.Tables[0].Rows[i]["IIRefCols"].ToString();
                    string Item = itemdata.Tables[0].Rows[i]["Item"].ToString();
                    string Desc = itemdata.Tables[0].Rows[i]["Desc"].ToString();
                    string Units = itemdata.Tables[0].Rows[i]["Units"].ToString();
                    string Prints = itemdata.Tables[0].Rows[i]["Prints"].ToString();
                    string EQty = itemdata.Tables[0].Rows[i]["EQty"].ToString();
                    string AQty = itemdata.Tables[0].Rows[i]["AQty"].ToString();
                    string Remarks = itemdata.Tables[0].Rows[i]["Remarks"].ToString();
                    using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                    {
                        SqlCommand Cmdl = new SqlCommand("usp_JO_BL_Production", cnc);
                        Cmdl.CommandType = CommandType.StoredProcedure;
                        Cmdl.Parameters.AddWithValue("@COLUMN09", OutParam);
                        Cmdl.Parameters.AddWithValue("@COLUMN02", LCol2);
                        Cmdl.Parameters.AddWithValue("@COLUMN03", UPC);
                        Cmdl.Parameters.AddWithValue("@COLUMN04", Item);
                        Cmdl.Parameters.AddWithValue("@COLUMN05", Desc);
                        Cmdl.Parameters.AddWithValue("@COLUMN06", EQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN07", AQty);
                        Cmdl.Parameters.AddWithValue("@COLUMN08", Remarks);
                        Cmdl.Parameters.AddWithValue("@COLUMN10", IIRefCols);
                        Cmdl.Parameters.AddWithValue("@COLUMN11", col["Status"]);
                        Cmdl.Parameters.AddWithValue("@COLUMN12", SubmittedTo);
                        Cmdl.Parameters.AddWithValue("@COLUMN13", Units);
                        Cmdl.Parameters.AddWithValue("@COLUMN14", Prints);
                        Cmdl.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                        Cmdl.Parameters.AddWithValue("@COLUMNA03", acOW);
                        Cmdl.Parameters.AddWithValue("@COLUMNA06", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA07", Date);
                        Cmdl.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                        Cmdl.Parameters.AddWithValue("@COLUMNA12", 1);
                        Cmdl.Parameters.AddWithValue("@COLUMNA13", 0);
                        Cmdl.Parameters.AddWithValue("@Direction", insert);
                        Cmdl.Parameters.AddWithValue("@TabelName", "SATABLE020");
                        Cmdl.Parameters.AddWithValue("@ReturnValue", "");
                        cnc.Open();
                        l = Cmdl.ExecuteNonQuery();
                    }
                }
                Session["PRDGridData"] = null;
                var msg = string.Empty;
                if (l > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1626 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Process Successfully Updated ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1626 && q.COLUMN05 == 3).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Process Updation Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                string lineid = "", Submittedcol = "", Submittedcol1 = "", headerout = "";
                if (SubmittedTo != "" && SubmittedTo != null) { headerout = " and COLUMN12=" + SubmittedTo + ""; Submittedcol = " and l.COLUMN11=" + SubmittedTo + ""; Submittedcol1 = " and pl.COLUMN11=" + SubmittedTo + ""; }
                SqlCommand cmdi = new SqlCommand("select top(1) l.COLUMN09 FROM SATABLE019 h inner join SATABLE020 l on l.COLUMN09=h.COLUMN01 and h.COLUMNA03=l.COLUMNA03 and isnull(l.COLUMNA13,0)=0  where h.COLUMN05=" + col["Issue"] + " " + Submittedcol + " AND l.COLUMNA03=" + acOW + " order by case IsNumeric(l.COLUMN09) when 1 then Replicate(0, 100 - Len(l.COLUMN09)) + l.COLUMN09 else l.COLUMN09 end desc", cn);
                SqlDataAdapter dadi = new SqlDataAdapter(cmdi);
                DataTable dtdi = new DataTable();
                dadi.Fill(dtdi);
                if (dtdi.Rows.Count > 0) { lineid = dtdi.Rows[0][0].ToString(); lineid = "pl.COLUMN09=" + lineid + " and "; }
                //List<SelectListItem> ActqtyList = new List<SelectListItem>();
                List<ProcedureOrderDetails> ActqtyList = new List<ProcedureOrderDetails>();
                if (Submittedcol1 != "")
                {
                    var sql = "select pl.COLUMN04,sum(isnull(pl.COLUMN06,0))-sum(isnull(pl.COLUMN07,0)) [QtyActual],pl.COLUMN10 from SATABLE019 plh  inner join   SATABLE020 pl on plh.COLUMN01=pl.COLUMN09 and plh.COLUMNA03=pl.COLUMNA03 " + Submittedcol1 + "  and isnull(pl.COLUMNA13,0)=0 Where  plh.COLUMN04!='" + col["Production"] + "' and plh.COLUMN05=" + col["Issue"] + " and  (plh.COLUMNA02 in(" + OpUnit + ") or plh.COLUMNA02 is null)   AND plh.COLUMNA03=" + acOW + " and isnull(plh.COLUMNA13,'False')='False' group by pl.COLUMN04,pl.COLUMN10";
                    SqlCommand cmd = new SqlCommand(sql, cn);
                    SqlDataAdapter dad = new SqlDataAdapter(cmd);
                    DataTable dtd = new DataTable();
                    dad.Fill(dtd);
                    for (int dd = 0; dd < dtd.Rows.Count; dd++)
                    {
                        ActqtyList.Add(new ProcedureOrderDetails { Item = dtd.Rows[dd]["COLUMN04"].ToString(), Qty = dtd.Rows[dd]["QtyActual"].ToString(), IssueNo = dtd.Rows[dd]["COLUMN10"].ToString() });
                    }
                }
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var sqlp = "SELECT * FROM SATABLE019 where column04='" + col["Production"] + "' " + headerout + "" +
                " and (COLUMNA02 in(" + OpUnit + ") or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'";
                var Processdata = dbs.Query(sqlp);
                if (headerout != "" && headerout != null && Processdata.Count()>0)
                {
                    OutParam = Processdata.FirstOrDefault().COLUMN01;
                    Productionid = Processdata.FirstOrDefault().COLUMN02;
                }
                if (Processdata.Count() == 0) Productionid = 0;
                return Json(new { status = col["SubmittedTo"], OutParam = OutParam, Productionid = Productionid, ActqtyList = ActqtyList, MessageFrom = msg, SuccessMessageFrom = Session["SuccessMessageFrom"] }, JsonRequestBehavior.AllowGet);
                return RedirectToAction("Info", "Production", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }


        public ActionResult ProductionView(string ide)
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;
                var sqle = "select * from SATABLE019 where COLUMN02=" + ide + " and COLUMNA03=" + acOW + " and isnull(COLUMNA13,0)=0 and  (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null)  ";
                SqlCommand cmd = new SqlCommand(sqle, cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);

                List<ProductionEditModel> all = new List<ProductionEditModel>();
                all.Add(new MeBizSuiteAppUI.Controllers.ProductionController.ProductionEditModel
                {
                    Productionid = "",
                    Production = "",
                    Jobber = "",
                    Customer = "",
                    JobOrder = "",
                    SalesOrder = "",
                    Opportunity = "",
                    Date = "",
                    CurrentStatus = "",
                    SubmittedTo = "",
                    DueDate = "",
                    ExecutedBy = "",
                    HandedoverTo = "",
                    Remarks = "",
                    Issue = "",
                    OperatingUnit = "",
                    Machineype = "",
                    Machine = "",
                    ProcessingIn = "",
                    ProcessingOut = "",
                    ProcessingDuration = ""
                });
                all[0].Productionid = dtd.Rows[0]["COLUMN02"].ToString();
                all[0].Production = dtd.Rows[0]["COLUMN04"].ToString();
                all[0].Jobber = dtd.Rows[0]["COLUMN07"].ToString();
                all[0].Customer = dtd.Rows[0]["COLUMN10"].ToString();
                all[0].JobOrder = dtd.Rows[0]["COLUMN06"].ToString();
                all[0].SalesOrder = dtd.Rows[0]["COLUMN08"].ToString();
                all[0].Opportunity = dtd.Rows[0]["COLUMN09"].ToString();
                all[0].Date = dtd.Rows[0]["COLUMN11"].ToString();
                all[0].CurrentStatus = dtd.Rows[0]["COLUMN12"].ToString();
                all[0].SubmittedTo = dtd.Rows[0]["COLUMN13"].ToString();
                all[0].DueDate = dtd.Rows[0]["COLUMN14"].ToString();
                all[0].ExecutedBy = dtd.Rows[0]["COLUMN15"].ToString();
                all[0].HandedoverTo = dtd.Rows[0]["COLUMN16"].ToString();
                all[0].Remarks = dtd.Rows[0]["COLUMN18"].ToString();
                all[0].Issue = dtd.Rows[0]["COLUMN05"].ToString();
                all[0].OperatingUnit = dtd.Rows[0]["COLUMN19"].ToString();
                all[0].Machineype = dtd.Rows[0]["COLUMN20"].ToString();
                all[0].Machine = dtd.Rows[0]["COLUMN21"].ToString();
                all[0].ProcessingIn = dtd.Rows[0]["COLUMN22"].ToString();
                all[0].ProcessingOut = dtd.Rows[0]["COLUMN23"].ToString();
                all[0].ProcessingDuration = dtd.Rows[0]["COLUMN24"].ToString();
                List<SelectListItem> Issue = new List<SelectListItem>();
                SqlDataAdapter cmddl = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE007 where COLUMN20 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 ) and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtdata = new DataTable();
                cmddl.Fill(dtdata);
                for (int dd = 0; dd < dtdata.Rows.Count; dd++)
                {
                    Issue.Add(new SelectListItem { Value = dtdata.Rows[dd]["COLUMN02"].ToString(), Text = dtdata.Rows[dd]["COLUMN04"].ToString() });
                }
                //Routing = Routing.OrderBy(x => x.Text).ToList();
                ViewData["Issue"] = new SelectList(Issue, "Value", "Text", selectedValue: all[0].Issue);
                List<SelectListItem> JobOrder = new List<SelectListItem>();
                SqlDataAdapter cmdj = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1000 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dtj = new DataTable();
                cmdj.Fill(dtj);
                for (int dd = 0; dd < dtj.Rows.Count; dd++)
                {
                    JobOrder.Add(new SelectListItem { Value = dtj.Rows[dd]["COLUMN02"].ToString(), Text = dtj.Rows[dd]["COLUMN04"].ToString() });
                }
                //JobOrder = JobOrder.OrderBy(x => x.Text).ToList();
                ViewData["JobOrder"] = new SelectList(JobOrder, "Value", "Text", selectedValue: all[0].JobOrder);
                List<SelectListItem> SalesOrder = new List<SelectListItem>();
                SqlDataAdapter cmds = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE005 where COLUMN29 in (Select COLUMN02 from MATABLE009 where COLUMN02=1002 )  AND  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dts = new DataTable();
                cmds.Fill(dts);
                for (int dd = 0; dd < dts.Rows.Count; dd++)
                {
                    SalesOrder.Add(new SelectListItem { Value = dts.Rows[dd]["COLUMN02"].ToString(), Text = dts.Rows[dd]["COLUMN04"].ToString() });
                }
                //SalesOrder = SalesOrder.OrderBy(x => x.Text).ToList();
                ViewData["SalesOrder"] = new SelectList(SalesOrder, "Value", "Text", selectedValue: all[0].SalesOrder);
                List<SelectListItem> Jobber = new List<SelectListItem>();
                SqlDataAdapter cmdj1 = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE001  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN22=22286 and isnull(COLUMN24,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtj1 = new DataTable();
                cmdj1.Fill(dtj1);
                for (int dd = 0; dd < dtj1.Rows.Count; dd++)
                {
                    Jobber.Add(new SelectListItem { Value = dtj1.Rows[dd]["COLUMN02"].ToString(), Text = dtj1.Rows[dd]["COLUMN04"].ToString() });
                }
                Jobber = Jobber.OrderBy(x => x.Text).ToList();
                ViewData["Jobber"] = new SelectList(Jobber, "Value", "Text", selectedValue: all[0].Jobber);
                List<SelectListItem> Opportunity = new List<SelectListItem>();
                SqlDataAdapter cmdo = new SqlDataAdapter("select COLUMN02,COLUMN04 from SATABLE013  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False' order by COLUMN02 desc", cn);
                DataTable dto = new DataTable();
                cmdo.Fill(dto);
                for (int dd = 0; dd < dto.Rows.Count; dd++)
                {
                    Opportunity.Add(new SelectListItem { Value = dto.Rows[dd]["COLUMN02"].ToString(), Text = dto.Rows[dd]["COLUMN04"].ToString() });
                }
                //Opportunity = Opportunity.OrderBy(x => x.Text).ToList();
                ViewData["Opportunity"] = new SelectList(Opportunity, "Value", "Text", selectedValue: all[0].Opportunity);
                List<SelectListItem> Customer = new List<SelectListItem>();
                SqlDataAdapter cmdc = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from SATABLE002  where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN25,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtc = new DataTable();
                cmdc.Fill(dtc);
                for (int dd = 0; dd < dtc.Rows.Count; dd++)
                {
                    Customer.Add(new SelectListItem { Value = dtc.Rows[dd]["COLUMN02"].ToString(), Text = dtc.Rows[dd]["COLUMN04"].ToString() });
                }
                Customer = Customer.OrderBy(x => x.Text).ToList();
                ViewData["Customer"] = new SelectList(Customer, "Value", "Text", selectedValue: all[0].Customer);
                List<SelectListItem> Status = new List<SelectListItem>();
                var sql1 = "select COLUMN02,COLUMN04 from MATABLE030 where COLUMN08!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN10,'False')='False' and isnull(COLUMNA13,'False')='False' order by case IsNumeric(COLUMN06) when 1 then Replicate(0, 100 - Len(COLUMN06)) + COLUMN06 else COLUMN06 end";
                //SqlDataAdapter cmdrs = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE030 where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                SqlDataAdapter cmdrs = new SqlDataAdapter(sql1, cn);
                DataTable dtrs = new DataTable();
                cmdrs.Fill(dtrs);
                for (int dd = 0; dd < dtrs.Rows.Count; dd++)
                {
                    Status.Add(new SelectListItem { Value = dtrs.Rows[dd]["COLUMN02"].ToString(), Text = dtrs.Rows[dd]["COLUMN04"].ToString() });
                }
                //Status = Status.OrderBy(x => x.Text).ToList();
                ViewData["Status"] = new SelectList(Status, "Value", "Text", selectedValue: all[0].CurrentStatus);
                ViewData["SubmittedTo"] = new SelectList(Status, "Value", "Text", selectedValue: all[0].SubmittedTo);
                List<SelectListItem> Executed = new List<SelectListItem>();
                SqlDataAdapter cmde = new SqlDataAdapter("select COLUMN02,COLUMN09 COLUMN04 from MATABLE010  Where  (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN32,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dte = new DataTable();
                cmde.Fill(dte);
                for (int dd = 0; dd < dte.Rows.Count; dd++)
                {
                    Executed.Add(new SelectListItem { Value = dte.Rows[dd]["COLUMN02"].ToString(), Text = dte.Rows[dd]["COLUMN04"].ToString() });
                }
                Executed = Executed.OrderBy(x => x.Text).ToList();
                ViewData["ExecutedBy"] = new SelectList(Executed, "Value", "Text", selectedValue: all[0].ExecutedBy);
                ViewData["Handedover"] = new SelectList(Executed, "Value", "Text", selectedValue: all[0].HandedoverTo);
                List<SelectListItem> OPunit = new List<SelectListItem>();
                SqlDataAdapter cmdop = new SqlDataAdapter("select COLUMN02,COLUMN03 as COLUMN04 from CONTABLE007 where COLUMN03!='' and  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)     AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN07,'False')='False' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtop = new DataTable();
                cmdop.Fill(dtop);
                for (int dd = 0; dd < dtop.Rows.Count; dd++)
                {
                    OPunit.Add(new SelectListItem { Value = dtop.Rows[dd]["COLUMN02"].ToString(), Text = dtop.Rows[dd]["COLUMN04"].ToString() });
                }
                OPunit = OPunit.OrderBy(x => x.Text).ToList();
                ViewData["OPunit"] = new SelectList(OPunit, "Value", "Text", selectedValue: all[0].OperatingUnit);
                List<SelectListItem> MachineType = new List<SelectListItem>();
                SqlDataAdapter cmdmt = new SqlDataAdapter("select COLUMN02,COLUMN05 COLUMN04 from PRTABLE001  where    ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtmt = new DataTable();
                cmdmt.Fill(dtmt);
                for (int dd = 0; dd < dtmt.Rows.Count; dd++)
                {
                    MachineType.Add(new SelectListItem { Value = dtmt.Rows[dd]["COLUMN02"].ToString(), Text = dtmt.Rows[dd]["COLUMN04"].ToString() });
                }
                MachineType = MachineType.OrderBy(x => x.Text).ToList();
                ViewData["MachineType"] = new SelectList(MachineType, "Value", "Text", selectedValue: all[0].Machineype);
                List<SelectListItem> Machine = new List<SelectListItem>();
                SqlDataAdapter cmdm = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002  where  ( " + Session["OPUnitWithNull"] + " or COLUMNA02 is null)  AND COLUMNA03='" + Session["AcOwner"] + "' and COLUMN03=11171 and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtm = new DataTable();
                cmdm.Fill(dtm);
                for (int dd = 0; dd < dtm.Rows.Count; dd++)
                {
                    Machine.Add(new SelectListItem { Value = dtm.Rows[dd]["COLUMN02"].ToString(), Text = dtm.Rows[dd]["COLUMN04"].ToString() });
                }
                Machine = Machine.OrderBy(x => x.Text).ToList();
                ViewData["Machine"] = new SelectList(Machine, "Value", "Text", selectedValue: all[0].Machine);
                List<SelectListItem> Item = new List<SelectListItem>();
                SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dti = new DataTable();
                cmdi.Fill(dti);
                for (int dd = 0; dd < dti.Rows.Count; dd++)
                {
                    Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                }
                Item = Item.OrderBy(x => x.Text).ToList();
                Item.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["Item"] = new SelectList(Item, "Value", "Text");
                List<SelectListItem> UOM = new List<SelectListItem>();
                SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                DataTable dtdataU = new DataTable();
                cmddlU.Fill(dtdataU);
                for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                {
                    UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                }
                UOM = UOM.OrderBy(x => x.Text).ToList();
                UOM.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                ViewData["UOM"] = new SelectList(UOM, "Value", "Text");
                ViewBag.DateFormat = DateFormatsetup();

                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                ViewBag.Processes = dbs.Query(sql1);
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult GetProdEditDetails(int Prodid, int Processid, string Transno)
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                string OPUnitnull = " (h.COLUMNA02 in(" + Session["OPUnit"] + ") or h.COLUMNA02 is null) ";
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626; string htmlstring = "";
                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var sqlp = "SELECT * FROM SATABLE019 h  where h.column04='" + Transno + "' and h.COLUMN12=" + Processid + "" +
                " and " + OPUnitnull + "  AND h.COLUMNA03='" + Session["AcOwner"] + "' and isnull(h.COLUMNA13,'False')='False'";
                var Processdata = dbs.Query(sqlp);
                if (Processdata.Count() > 0)
                    Prodid = Processdata.FirstOrDefault().COLUMN02;
                SqlCommand cmd = new SqlCommand(sqlp, cn);
                SqlDataAdapter dadp = new SqlDataAdapter(cmd);
                DataTable dtdp = new DataTable();
                dadp.Fill(dtdp);
                string Productionid = "",Date="", DueDate ="",
                Production = "",
                Jobber = "",
                Customer = "",
                JobOrder = "",
                SalesOrder = "",
                Opportunity = "",
                CurrentStatus = "",
                SubmittedTo = "",
                ExecutedBy = "",
                HandedoverTo = "",
                Remarks = "",
                Issue = "",
                OperatingUnit = "",
                Machineype = "",
                Machine = "",
                ProcessingIn = "",
                ProcessingOut = "",
                ProcessingDuration = "";
                if (dtdp.Rows.Count > 0)
                {
                    Productionid = dtdp.Rows[0]["COLUMN02"].ToString();
                    Production = dtdp.Rows[0]["COLUMN04"].ToString();
                    Jobber = dtdp.Rows[0]["COLUMN07"].ToString();
                    Customer = dtdp.Rows[0]["COLUMN10"].ToString();
                    JobOrder = dtdp.Rows[0]["COLUMN06"].ToString();
                    SalesOrder = dtdp.Rows[0]["COLUMN08"].ToString();
                    Opportunity = dtdp.Rows[0]["COLUMN09"].ToString();
                    Date = Convert.ToDateTime(dtdp.Rows[0]["COLUMN11"].ToString()).ToString(Session["DateFormat"].ToString());
                    CurrentStatus = dtdp.Rows[0]["COLUMN12"].ToString();
                    SubmittedTo = dtdp.Rows[0]["COLUMN13"].ToString();
                    DueDate = Convert.ToDateTime(dtdp.Rows[0]["COLUMN14"].ToString()).ToString(Session["DateFormat"].ToString());
                    ExecutedBy = dtdp.Rows[0]["COLUMN15"].ToString();
                    HandedoverTo = dtdp.Rows[0]["COLUMN16"].ToString();
                    Remarks = dtdp.Rows[0]["COLUMN18"].ToString();
                    Issue = dtdp.Rows[0]["COLUMN05"].ToString();
                    OperatingUnit = dtdp.Rows[0]["COLUMN19"].ToString();
                    Machineype = dtdp.Rows[0]["COLUMN20"].ToString();
                    Machine = dtdp.Rows[0]["COLUMN21"].ToString();
                    ProcessingIn = dtdp.Rows[0]["COLUMN22"].ToString();
                    ProcessingOut = dtdp.Rows[0]["COLUMN23"].ToString();
                    ProcessingDuration = dtdp.Rows[0]["COLUMN24"].ToString();
                }
                var grid1 =new WebGrid(null);
                var htmlstring2 = "";
                if (Processdata.Count() > 0)
                {
                    var sql = "SELECT l.COLUMN10 RefCols,l.COLUMN02 Rowid, l.COLUMN03 UPC, l.COLUMN04 Item, l.COLUMN05 [Item Desc], l.COLUMN13 UOM, l.COLUMN14 Prints, l.COLUMN06 [Qty Expected], l.COLUMN07 [Qty Actual], l.COLUMN08 Remarks FROM " +
                        " SATABLE019 h inner join SATABLE020 l on l.COLUMN09=h.COLUMN01 and h.COLUMNA03=l.COLUMNA03 and isnull(l.COLUMNA13,0)=0  where h.column02=" + Prodid + " and l.column11=" + Processid + "" +
                    " and " + OPUnitnull + "  AND h.COLUMNA03='" + Session["AcOwner"] + "' and isnull(h.COLUMNA13,'False')='False'";
                    var GData = dbs.Query(sql);
                    SqlCommand cmdd = new SqlCommand(sql, cn);
                    SqlDataAdapter dad = new SqlDataAdapter(cmdd);
                    DataTable dtd = new DataTable();
                    dad.Fill(dtd);

                    List<string> colNM = new List<string>();
                    List<WebGridColumn> TData = new List<WebGridColumn>();
                    List<SelectListItem> Item = new List<SelectListItem>();
                    SqlDataAdapter cmdi = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE007  Where COLUMN04!='' and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMN47,'False')='False'  and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dti = new DataTable();
                    cmdi.Fill(dti);
                    for (int dd = 0; dd < dti.Rows.Count; dd++)
                    {
                        Item.Add(new SelectListItem { Value = dti.Rows[dd]["COLUMN02"].ToString(), Text = dti.Rows[dd]["COLUMN04"].ToString() });
                    }
                    Item = Item.OrderBy(x => x.Text).ToList();
                    Item.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                    ViewData["Item"] = new SelectList(Item, "Value", "Text");
                    string dddata = "";
                    for (int d = 0; d < Item.Count; d++)
                    {
                        var OpSelected = "";
                        if (dtd.Rows.Count > 0) OpSelected = dtd.Rows[0]["Item"].ToString();
                        if (Item[d].Value == OpSelected && Item[d].Value != "")
                            dddata += "<option value=" + Item[d].Value + "  selected>" + Item[d].Text + "</option>";
                        else
                            dddata += "<option value=" + Item[d].Value + " >" + Item[d].Text + "</option>";
                    }
                    List<SelectListItem> UOM = new List<SelectListItem>();
                    SqlDataAdapter cmddlU = new SqlDataAdapter("select COLUMN02,COLUMN04 from MATABLE002 where column03=11119 and (" + Session["OPUnitWithNull"] + " or COLUMNA02 is null)   AND COLUMNA03='" + Session["AcOwner"] + "' and isnull(COLUMNA13,'False')='False'", cn);
                    DataTable dtdataU = new DataTable();
                    cmddlU.Fill(dtdataU);
                    for (int dd = 0; dd < dtdataU.Rows.Count; dd++)
                    {
                        UOM.Add(new SelectListItem { Value = dtdataU.Rows[dd]["COLUMN02"].ToString(), Text = dtdataU.Rows[dd]["COLUMN04"].ToString() });
                    }
                    UOM = UOM.OrderBy(x => x.Text).ToList();
                    UOM.Insert(0, (new SelectListItem { Value = "", Text = "--Select--" }));
                    ViewData["UOM"] = new SelectList(UOM, "Value", "Text");
                    string dddata1 = "";
                    for (int d = 0; d < UOM.Count; d++)
                    {
                        var UOMSelected = "";
                        if (dtd.Rows.Count > 0) UOMSelected = dtd.Rows[0]["UOM"].ToString();
                        if (UOM[d].Value == UOMSelected && UOM[d].Value != "")
                            dddata1 += "<option value=" + UOM[d].Value + "  selected>" + UOM[d].Text + "</option>";
                        else
                            dddata1 += "<option value=" + UOM[d].Value + " >" + UOM[d].Text + "</option>";
                    }

                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "",
                        Header = "",
                        Format = (item) => new HtmlString("<input  id='checkRow' class='chkclass' itemid=''   name=''  type='checkbox'  value='" + dtd.Rows[0]["Rowid"] + "'   checked='checked' /><input id ='RefCols' type='text' style='display:none' value='" + dtd.Rows[0]["RefCols"] + "'/>")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "UPC",
                        Header = "UPC",
                        Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN03'   class='txtintgridclass' itemid='N' name='UPC' value='" + dtd.Rows[0]["UPC"] + "' />")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "Item",
                        Header = "Item",
                        Format = (item) => new HtmlString("<select  class =gridddl style=width:180px name='Item' itemid=N    id='SATABLE020COLUMN04'  >" + dddata + "</select>")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "Item Desc",
                        Header = "Item Desc",
                        Format = (item) => new HtmlString("<textarea id='SATABLE020COLUMN05' itemid=N  name='Item Desc' class='txtgridclass gridtxtarealength' type='text'>" + dtd.Rows[0]["Item Desc"] + " </textarea>")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "Units",
                        Header = "Units",
                        Format = (item) => new HtmlString("<select  class =gridddl name='Units' itemid=N    id='SATABLE020COLUMN13'  >" + dddata1 + "</select>")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "No. Of Prints",
                        Header = "No. Of Prints",
                        Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN14'   class='txtintgridclass' itemid='N' value='" + dtd.Rows[0]["Prints"] + "' name='No. Of Prints'  />")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "Qty Expected",
                        Header = "Qty Expected",
                        Format = (item) => new HtmlString("<input  type='text'  id='SATABLE020COLUMN06'  class='txtintgridclass'  itemid='N' name='Qty Expected' value='" + dtd.Rows[0]["Qty Expected"] + "'  />")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "Qty Actual",
                        Header = "Qty Actual",
                        Format = (item) => new HtmlString("<input  type='text'     id='SATABLE020COLUMN07' class='txtintgridclass'  itemid='Y' name='Qty Actual'  value='" + dtd.Rows[0]["Qty Actual"] + "' />")

                    });
                    TData.Add(new WebGridColumn()
                    {
                        ColumnName = "Remarks",
                        Header = "Remarks",
                        Format = (item) => new HtmlString("<textarea id='SATABLE020COLUMN08' itemid=N  name='Remarks' class='txtgridclass gridtxtarealength' type='text'>" + dtd.Rows[0]["Remarks"] + "</textarea>")

                    });
                    string dddata2 = "", dddata3 = "";
                    for (int i = 1; i < dtd.Rows.Count; i++)
                    {
                        dddata2 = ""; dddata3 = "";
                        for (int d = 0; d < Item.Count; d++)
                        {
                            var OpSelected = "";
                            if (dtd.Rows.Count > 0) OpSelected = dtd.Rows[i]["Item"].ToString();
                            if (Item[d].Value == OpSelected && Item[d].Value != "")
                                dddata2 += "<option value=" + Item[d].Value + "  selected>" + Item[d].Text + "</option>";
                            else
                                dddata2 += "<option value=" + Item[d].Value + " >" + Item[d].Text + "</option>";
                        }
                        for (int d = 0; d < UOM.Count; d++)
                        {
                            var UOMSelected = "";
                            if (dtd.Rows.Count > 0) UOMSelected = dtd.Rows[i]["UOM"].ToString();
                            if (UOM[d].Value == UOMSelected && UOM[d].Value != "")
                                dddata3 += "<option value=" + UOM[d].Value + "  selected>" + UOM[d].Text + "</option>";
                            else
                                dddata3 += "<option value=" + UOM[d].Value + " >" + UOM[d].Text + "</option>";
                        }
                        htmlstring = htmlstring + "<tr><td  ><input  type='checkbox' id='checkRow' class=chkclass itemid='Y' name='' checked=checked   value='" + dtd.Rows[i]["Rowid"] + "' /><input id ='RefCols' type='text' style='display:none' value='" + dtd.Rows[i]["RefCols"] + "'/></td>";
                        htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN03' class=txtintgridclass   value=''  name=UPC value='" + dtd.Rows[i]["UPC"] + "' /></td>";
                        htmlstring = htmlstring + "<td  ><select  class =gridddl name='Item' itemid=Y    id='SATABLE020COLUMN04'  >" + dddata2 + "</select></td>";
                        htmlstring = htmlstring + "<td  ><textarea id='SATABLE020COLUMN05'  class='txtgridclass gridtxtarealength' name=Item Desc>" + dtd.Rows[i]["Item Desc"] + "</textarea></td>";
                        htmlstring = htmlstring + "<td  ><select  class =gridddl name='Units' itemid=N    id='SATABLE020COLUMN13'  >" + dddata3 + "</select></td>";
                        htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN14' class=txtintgridclass value='" + dtd.Rows[i]["Prints"] + "'  name='No. Of Prints' /></td>";
                        htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN06' class=txtintgridclass value='" + dtd.Rows[i]["Qty Expected"] + "'  itemid='Y' name=Qty Expected  /></td>";
                        htmlstring = htmlstring + "<td  ><input  type='text' id='SATABLE020COLUMN07'  class=txtintgridclass   value='" + dtd.Rows[i]["Qty Actual"] + "'  name=Qty Actual /></td>";
                        htmlstring = htmlstring + "<td  ><textarea id='SATABLE020COLUMN08' class='txtgridclass gridtxtarealength'  name='Remarks'>" + dtd.Rows[i]["Remarks"] + "</textarea></td></tr>";
                    }
                    grid1 = new WebGrid(GData, canPage: false, canSort: false);
                    var htmlstring1 = grid1.GetHtml(mode: WebGridPagerModes.All, tableStyle: "webgrid-table",
                    headerStyle: "webgrid-header",
                    footerStyle: "webgrid-footer",
                    alternatingRowStyle: "webgrid-alternating-row",
                    rowStyle: "webgrid-row-style",
                    htmlAttributes: new { id = "grdData" },
                    columns: TData);
                    htmlstring2 = htmlstring1.ToHtmlString();
                }
                return Json(new
                {
                    grid = htmlstring2,
                    grid1 = htmlstring,
                    Prodid = Prodid,
                    Production = Production ,
                    Jobber =Jobber,
                    Customer =Customer,
                    JobOrder =JobOrder,
                    SalesOrder =SalesOrder,
                    Opportunity =Opportunity,
                    Date = Date,
                    CurrentStatus =CurrentStatus,
                    SubmittedTo =SubmittedTo,
                    DueDate =DueDate,
                    ExecutedBy =ExecutedBy,
                    HandedoverTo =HandedoverTo,
                    Remarks =Remarks,
                    Issue =Issue,
                    OperatingUnit =OperatingUnit,
                    Machineype =Machineype,
                    Machine =Machine,
                    ProcessingIn =ProcessingIn,
                    ProcessingOut =ProcessingOut,
                    ProcessingDuration =ProcessingDuration
                },
                 JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult ProductionDelete()
        {
            try
            {
                string acOW = Convert.ToString(Session["AcOwner"]);
                string OpUnit = Convert.ToString(Session["OPUnit1"]);
                string Date = DateTime.Now.ToString();
                string insert = "Delete"; int r = 0;
                var idi = ""; string HCol2 = "";
                var custform = Request.UrlReferrer.Query.ToString(); custform = custform.Replace("?", "");  custform = custform.Replace("%20", " "); custform = custform.Replace("FormName=", ""); int n = custform.IndexOf("&"); if (n > 0) { HCol2 = custform.Remove(0, n + 1).Replace("ide=", "").ToString(); custform = custform.Substring(0, n); }
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    SqlCommand Cmd = new SqlCommand("usp_JO_BL_Production", cnc);
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.AddWithValue("@COLUMN02", HCol2);
                    Cmd.Parameters.AddWithValue("@COLUMNA02", OpUnit);
                    Cmd.Parameters.AddWithValue("@COLUMNA03", acOW);
                    Cmd.Parameters.AddWithValue("@COLUMNA06", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA07", Date);
                    Cmd.Parameters.AddWithValue("@COLUMNA08", Session["eid"]);
                    Cmd.Parameters.AddWithValue("@COLUMNA12", 1);
                    Cmd.Parameters.AddWithValue("@COLUMNA13", 1);
                    Cmd.Parameters.AddWithValue("@Direction", insert);
                    Cmd.Parameters.AddWithValue("@TabelName", "SATABLE019");
                    cnc.Open();
                    r = Cmd.ExecuteNonQuery();
                }
                var msg = "";
                if (r > 0)
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1626 && q.COLUMN05 == 4).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Process Successfully Deleted ";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";
                }
                else
                {
                    var msgMaster = dc.CONTABLE023.Where(q => q.COLUMN04 == 1626 && q.COLUMN05 == 5).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                        msg = "Process Deletion Failed";
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "fail";
                }
                return RedirectToAction("Info", "Production", new { FormName = Session["FormName"] });
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult ProcessPrint(string ide)
        {
            try
            {
                List<string> Oper = Session["OPUnit"].ToString().Split(',').ToList<string>();
                string OPUnit = Convert.ToString(Session["OPUnit"]);
                var acOW = Convert.ToInt32(Session["AcOwner"]);
                Session["FormName"] = dc.CONTABLE0010.Where(a => a.COLUMN02 == 1626).ToList().FirstOrDefault().COLUMN04.ToString();
                Session["id"] = 1626;
                var DateFormat = DateFormatsetup();
                var sqle = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Production#],j.[COLUMN05] AS [Jobber],cu.[COLUMN05] AS [Customer],format(p.COLUMN11,'"+ DateFormat + "') Date,"+
	            " jo.[COLUMN04] AS [Job Order#],o.[COLUMN04] AS [Opportunity#],eb.COLUMN09 Executed,ht.COLUMN09 Handedover,p.COLUMN24 Duration,"+
                " cs.[COLUMN04] AS [Current Status],	st.[COLUMN04] AS [Submitted To],p.COLUMN18 Remarks,p.COLUMN19 OPUnitID,ji.[COLUMN04] AS [Issue#],"+
                " opu.COLUMN04 as ouName,opu.COLUMN30 as OPUVATNO,opu.COLUMN31 as OPUCSTNO,opu.COLUMN33 as OPUPANNO,opu.COLUMN34 as OPUPhNo,"+
                " c.COLUMN04 as company, c.COLUMN28 as logoName,c.COLUMN27 as VATNO,c.COLUMN29 as CSTNO,c.COLUMN31  as PANNO, c.COLUMN10 as compA1, c.COLUMN11 as compA2, c.COLUMN12 as compCity,c.COLUMN14 as compZip,c.COLUMN32 as StateLincense,c.COLUMN33 as CntralLincese,c.COLUMN35 as tin from SATABLE019 p " +
	            " inner join SATABLE020 l on l.COLUMN09=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0 and (l.COLUMNA02=p.COLUMNA02 or l.COLUMNA02 is null) "+
	            " left join SATABLE005 jo on jo.COLUMN02=p.column06 and jo.COLUMNA03=p.COLUMNA03 and isnull(jo.COLUMNA13,0)=0 and (jo.COLUMNA02=p.COLUMNA02 or jo.COLUMNA02 is null) "+
	            " left join SATABLE007 ji on ji.COLUMN02=p.column05 and ji.COLUMNA03=p.COLUMNA03 and isnull(ji.COLUMNA13,0)=0 and (ji.COLUMNA02=p.COLUMNA02 or ji.COLUMNA02 is null) "+
	            " left join SATABLE013 o on o.COLUMN02=p.COLUMN09 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 and (o.COLUMNA02=p.COLUMNA02 or o.COLUMNA02 is null) "+
	            " left join SATABLE001 j on j.COLUMN02=p.COLUMN07 and j.COLUMNA03=p.COLUMNA03 and isnull(j.COLUMNA13,0)=0 and (j.COLUMNA02=p.COLUMNA02 or j.COLUMNA02 is null) "+
	            " left join SATABLE002 cu on cu.COLUMN02=p.COLUMN10 and cu.COLUMNA03=p.COLUMNA03 and isnull(cu.COLUMNA13,0)=0 and (cu.COLUMNA02=p.COLUMNA02 or cu.COLUMNA02 is null) "+
	            " left join MATABLE030 cs on cs.COLUMN02=p.COLUMN12 and cs.COLUMNA03=p.COLUMNA03 and isnull(cs.COLUMNA13,0)=0 and (cs.COLUMNA02=p.COLUMNA02 or cs.COLUMNA02 is null) "+
	            " left join MATABLE030 st on st.COLUMN02=p.COLUMN13 and st.COLUMNA03=p.COLUMNA03 and isnull(st.COLUMNA13,0)=0 and (st.COLUMNA02=p.COLUMNA02 or st.COLUMNA02 is null) "+
	            " left join MATABLE010 eb on eb.COLUMN02=p.COLUMN15 and eb.COLUMNA03=p.COLUMNA03 and isnull(eb.COLUMNA13,0)=0 and (eb.COLUMNA02=p.COLUMNA02 or eb.COLUMNA02 is null) "+
                " left join MATABLE010 ht on ht.COLUMN02=p.COLUMN16 and ht.COLUMNA03=p.COLUMNA03 and isnull(ht.COLUMNA13,0)=0 and (ht.COLUMNA02=p.COLUMNA02 or ht.COLUMNA02 is null) " +
                " left join CONTABLE007 opu on opu.COLUMN02=p.COLUMN19 and opu.COLUMNA03=p.COLUMNA03 and isnull(opu.COLUMNA13,0)=0 " +
                " left join CONTABLE008 c on c.COLUMNA03=p.COLUMNA03 AND ISNULL(c.COLUMNA13,'FALSE')='FALSE' AND ISNULL(c.COLUMN05,'FALSE')='FALSE' "+
                " where isnull(p.COLUMNA13,0)=0  AND  p.COLUMN03=1626 and p.COLUMN02=" + ide + " and p.COLUMNA03=" + acOW + " and isnull(p.COLUMNA13,0)=0 and  (p.COLUMNA02 in(" + OPUnit + ") or p.COLUMNA02 is null)  ";
                SqlCommand cmd = new SqlCommand(sqle, cn);
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);

                List<ProductionEditModel> all = new List<ProductionEditModel>();
                all.Add(new MeBizSuiteAppUI.Controllers.ProductionController.ProductionEditModel
                {
                    Productionid = "",
                    Production = "",
                    Jobber = "",
                    Customer = "",
                    JobOrder = "",
                    SalesOrder = "",
                    Opportunity = "",
                    Date = "",
                    CurrentStatus = "",
                    SubmittedTo = "",
                    DueDate = "",
                    ExecutedBy = "",
                    HandedoverTo = "",
                    Remarks = "",
                    Issue = "",
                    OperatingUnit = "",
                    Machineype = "",
                    Machine = "",
                    ProcessingIn = "",
                    ProcessingOut = "",
                    ProcessingDuration = "",
                    company = "",
                    cAdresss = "",
                    cAdresss1 = "",
                    ccity = "",
                    cstate = "",
                    ccountry = "",
                    czip = "",
                    oAdresss = "",
                    oAdresss1 = "",
                    ocity = "",
                    ostate = "",
                    ocountry = "",
                    ozip = "",
                    CVATNO = "",
                    CCSTNO = "",
                    CPANNO = "",
                    CServiceTaxNO = "",
                    CustomerAddress = "",
                    ouName="",
                    OPUVATNO="",
                    OPUCSTNO = "",
                    OPUPANNO = "",
                    OPUPhNo = ""
                });
                if (dtd.Rows.Count > 0)
                {
                    all[0].Productionid = dtd.Rows[0]["ID"].ToString();
                    all[0].Production = dtd.Rows[0]["Production#"].ToString();
                    all[0].Jobber = dtd.Rows[0]["Jobber"].ToString();
                    all[0].Customer = dtd.Rows[0]["Customer"].ToString();
                    all[0].JobOrder = dtd.Rows[0]["Job Order#"].ToString();
                    all[0].Opportunity = dtd.Rows[0]["Opportunity#"].ToString();
                    all[0].Date = dtd.Rows[0]["Date"].ToString();
                    all[0].CurrentStatus = dtd.Rows[0]["Current Status"].ToString();
                    all[0].SubmittedTo = dtd.Rows[0]["Submitted To"].ToString();
                    all[0].ExecutedBy = dtd.Rows[0]["Executed"].ToString();
                    all[0].HandedoverTo = dtd.Rows[0]["Handedover"].ToString();
                    all[0].Remarks = dtd.Rows[0]["Remarks"].ToString();
                    all[0].Issue = dtd.Rows[0]["Issue#"].ToString();
                    all[0].ProcessingDuration = dtd.Rows[0]["Duration"].ToString();
                    all[0].ouName = dtd.Rows[0]["ouName"].ToString();
                    all[0].OPUVATNO = dtd.Rows[0]["OPUVATNO"].ToString();
                    all[0].OPUCSTNO = dtd.Rows[0]["OPUCSTNO"].ToString();
                    all[0].OPUPANNO = dtd.Rows[0]["OPUPANNO"].ToString();
                    all[0].OPUPhNo = dtd.Rows[0]["OPUPhNo"].ToString();
                    all[0].company = dtd.Rows[0]["company"].ToString();
                    if (Convert.ToString(dtd.Rows[0]["logoName"]) != "")
                        all[0].logoName = "/Content/Upload/" + (dtd.Rows[0]["logoName"].ToString());
                    else
                        all[0].logoName = "";
                    all[0].cAdresss = dtd.Rows[0]["compA1"].ToString();
                    all[0].cAdresss1 = dtd.Rows[0]["compA2"].ToString();
                    all[0].ccity = dtd.Rows[0]["compCity"].ToString();
                    all[0].czip = dtd.Rows[0]["compZip"].ToString();
                    all[0].StateLincense = dtd.Rows[0]["StateLincense"].ToString();
                    all[0].CntralLincese = dtd.Rows[0]["CntralLincese"].ToString();
                    all[0].tin = dtd.Rows[0]["tin"].ToString();
                    //all[0].ProcessingIn = dtd.Rows[0]["COLUMN22"].ToString();
                    //all[0].ProcessingOut = dtd.Rows[0]["COLUMN23"].ToString();
                    var opunit = dtd.Rows[0]["OPUnitID"].ToString();
                    SqlCommand ocmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", cn);
                    ocmd.CommandType = CommandType.StoredProcedure;
                    ocmd.Parameters.Add(new SqlParameter("@oppUnit", opunit));
                    SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                    DataTable odt = new DataTable();
                    oda.Fill(odt);
                    if (odt.Rows.Count > 0)
                    {
                        if (odt.Rows[0]["COLUMN12"].ToString() == "" || odt.Rows[0]["COLUMN12"].ToString() == null)
                            all[0].oAdresss = "";
                        else
                            all[0].oAdresss = odt.Rows[0]["COLUMN12"].ToString();

                        if (odt.Rows[0]["COLUMN13"].ToString() == "" || odt.Rows[0]["COLUMN13"].ToString() == null)
                            all[0].oAdresss1 = "";
                        else
                            all[0].oAdresss = odt.Rows[0]["COLUMN13"].ToString();

                        if (odt.Rows[0]["add2"].ToString() == "" || odt.Rows[0]["add2"].ToString() == null)
                            all[0].oAdresss2 = "";
                        else
                            all[0].oAdresss2 = odt.Rows[0]["add2"].ToString();
                        if (odt.Rows[0]["COLUMN14"].ToString() == "" || odt.Rows[0]["COLUMN14"].ToString() == null)
                            all[0].ocity = "";
                        else
                            all[0].ocity = odt.Rows[0]["COLUMN14"].ToString();

                        if (odt.Rows[0]["COLUMN15"].ToString() == "" || odt.Rows[0]["COLUMN15"].ToString() == null)
                            all[0].ostate = "";
                        else
                            all[0].ostate = odt.Rows[0]["COLUMN15"].ToString();

                        if (odt.Rows[0]["COLUMN26"].ToString() == "" || odt.Rows[0]["COLUMN26"].ToString() == null)
                            all[0].ocountry = "";
                        else
                            all[0].ocountry = odt.Rows[0]["COLUMN26"].ToString();

                        if (odt.Rows[0]["COLUMN16"].ToString() == "" || odt.Rows[0]["COLUMN16"].ToString() == null)
                            all[0].ozip = "";
                        else
                            all[0].ozip = odt.Rows[0]["COLUMN16"].ToString();

                        if (odt.Rows[0]["COLUMN34"].ToString() == "" || odt.Rows[0]["COLUMN34"].ToString() == null)
                            all[0].douPhno = "";
                        else
                            all[0].douPhno = odt.Rows[0]["COLUMN34"].ToString();
                        if (odt.Rows[0]["COLUMN37"].ToString() == "" || odt.Rows[0]["COLUMN37"].ToString() == null)
                            all[0].douPhno = "";
                        else
                            all[0].opDrugLIC = odt.Rows[0]["COLUMN37"].ToString();
                    }
                }

                var connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                var providerName = "System.Data.SqlClient";
                var dbs = Database.OpenConnectionString(connectionString, providerName);
                var sql1 = "SELECT m.COLUMN04 [Item],l.COLUMN05 [Desc],u.COLUMN04 [Units],l.COLUMN14 [NoofPrints],(isnull(l.COLUMN06,0)) [ExpQty],(isnull(l.COLUMN07,0)) [ActQty],l.COLUMN08 [Remarks] from SATABLE019 p " +
                " inner join SATABLE020 l on l.COLUMN09=p.COLUMN01 and l.COLUMNA03=p.COLUMNA03 and isnull(l.COLUMNA13,0)=0 and (l.COLUMNA02=p.COLUMNA02 or l.COLUMNA02 is null) " +
                " left join MATABLE007 m on m.COLUMN02=l.column04 and m.COLUMNA03=l.COLUMNA03 and isnull(m.COLUMNA13,0)=0 and (m.COLUMNA02=l.COLUMNA02 or m.COLUMNA02 is null) " +
                " left join MATABLE002 u on u.COLUMN02=l.COLUMN13 and u.COLUMNA03=l.COLUMNA03 and isnull(u.COLUMNA13,0)=0 and (u.COLUMNA02=l.COLUMNA02 or u.COLUMNA02 is null) " +
                " where isnull(p.COLUMNA13,0)=0  AND  p.COLUMN03=1626 and p.COLUMN02=" + ide + " and p.COLUMNA03=" + acOW + " and isnull(p.COLUMNA13,0)=0 and  (p.COLUMNA02 in(" + OPUnit + ") or p.COLUMNA02 is null)  ";
                ViewBag.Processes = dbs.Query(sql1);
                ViewBag.ProcessTitle = "Production (" + all[0].CurrentStatus + ")";
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

    }
}
