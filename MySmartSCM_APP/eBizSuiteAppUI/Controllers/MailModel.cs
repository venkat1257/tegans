﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeBizSuiteAppUI.Controllers
{
    public class MailModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Subject { get; set; }
        //[AllowHtml]
        public string Body { get; set; }
        public HttpPostedFile Attachment { get; set; }

    }
}