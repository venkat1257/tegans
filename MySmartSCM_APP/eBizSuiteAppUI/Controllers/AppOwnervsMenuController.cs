﻿using eBizSuiteAppModel.Table;
using eBizSuiteUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eBizSuiteAppUI.Controllers
{
    public class AppOwnervsMenuController : Controller
    {
        //
        // GET: /AppOwnervsMenu/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            AppOwnervsMenuModel obj = new AppOwnervsMenuModel();
            obj.AppOwner = db.CONTABLE002.OrderBy(q => q.COLUMN02).Where(q => q.COLUMN06 == true).ToList();
            obj.Menu = db.CONTABLE003.OrderBy(q => q.COLUMN03).ToList();
            return View("Create",obj);
        }


        private List<DynaNode> GetTreeView()
        {
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            //List<tbl_TreeView> objTreeList = objContext.tbl_TreeViews.OrderBy(s => s.ParentId).ToList();
            List<CONTABLE003> objTreeList = db.CONTABLE003.OrderBy(s => s.COLUMN06).Where(q=>q.COLUMN04==101).ToList();
            List<DynaNode> objList = new List<DynaNode>();
            foreach (var item in objTreeList)
            {
                DynaNode objTree = new DynaNode();
                objTree.key = item.COLUMN02.ToString();
                objTree.title = item.COLUMN05;
                objTree.isLazy = true;
                var objChild = objTreeList.FindAll(s => s.COLUMN06 == item.COLUMN06);
                if (objChild.Count > 0)
                {
                    objTree.isFolder = true;
                }
                else
                    objTree.isFolder = false;
                objTree.parentId = item.COLUMN06.ToString();
                objList.Add(objTree);
            }

            return objList;

        }


        public JsonResult GetAllNode()
        {
            List<DynaNode> objList = GetTreeView();
            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        public class DynaNode
        {
            public string title { get; set; }
            public bool isFolder { get; set; }
            public bool isLazy { get; set; }
            public string key { get; set; }
            public string parentId { get; set; }
        }

        [HttpPost]
        public JsonResult Save(string list, int id,string kid )
        {
            try
            {
                eBizSuiteTableEntities db = new eBizSuiteTableEntities();
                CONTABLE003 mm = new CONTABLE003();
                List<string> listValues = list.Split(',').ToList();
                List<string> listKeys = kid.Split(',').ToList();
                List<CONTABLE003> objList = db.CONTABLE003.OrderBy(s => s.COLUMN02).ToList();
                List<CONTABLE003> objCheck = db.CONTABLE003.OrderBy(s => s.COLUMN02).Where(q=>q.COLUMNA03==id).ToList();
                if (objCheck.Count > 0)
                {
                    return Json("This Account Owner already existed......", JsonRequestBehavior.AllowGet);
                }
                for (int i = 0; i < listValues.Count; i++)
                {
                    var k=Convert.ToInt32(listKeys[i].ToString());
                    var mid = db.CONTABLE003.Where(q => q.COLUMN02 == k).First();
                    mm.COLUMN02 = objList[objList.Count - 1].COLUMN02 + i+1;
                    mm.COLUMN03 =mid.COLUMN03;
                    mm.COLUMN04 = 101;
                    mm.COLUMN06 = mid.COLUMN06;
                    mm.COLUMN07 = mid.COLUMN07;
                    mm.COLUMN10 = mid.COLUMN10;
                    mm.COLUMN11 = mid.COLUMN11;
                    mm.COLUMNA03 = id;
                    mm.COLUMN05 = listValues[i].ToString();
                    mm.COLUMN08 = "Y";
                    mm.COLUMN09 = "Y";
                    mm.COLUMNA01 = null;
                    mm.COLUMNA02 = null;
                    if (Session["LogedEmployeeID"] != null)
                    {
                        mm.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"].ToString());
                    }
                    else
                        mm.COLUMNA08 = null;
                    mm.COLUMNA04 = null;
                    mm.COLUMNA05 = null;
                    mm.COLUMNA06 = DateTime.Now;
                    mm.COLUMNA07 = null;
                    mm.COLUMNA09 = DateTime.Now;
                    mm.COLUMNA10 = null;
                    mm.COLUMNA11 = null;
                    mm.COLUMNA12 = true;
                    mm.COLUMNA13 = false;
                    db.CONTABLE003.Add(mm);
                    db.SaveChanges();
                }
                return Json("Data Saved SUccessfully", JsonRequestBehavior.AllowGet);
            }
            catch { return Json("Data Creation Failed", JsonRequestBehavior.AllowGet); }
        }

    }
}
