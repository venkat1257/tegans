﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeBizSuiteAppUI.Controllers
{
    public class POPrint
    {
        public string Date { get; set; }

        //EMPHCS743 - GNANESHWAR 22/7/2015 Adding Shipping Tab Fileds in Sales order and Invoice
                   
        public string ModeOfTrans { get; set; }
        public string FreightTerm { get; set; }
        public string form { get; set; }
        public string PackingCharges { get; set; }
        public string ShippingCharges { get; set; }
        //EMPHCS1231	3 reports to Nandyal By Raj.Jr 15/10/2015
        public string DriverName { get; set; }
        public string TruckNo { get; set; }
        public string DriverContactNo { get; set; }
        public string Memo { get; set; }
        public string Advance { get; set; }
        public string Balance { get; set; }
        public string Discount { get; set; }
        public string TrackingNo { get; set; }
        public string StateLincense { get; set; }
        public string CntralLincese { get; set; }
        public string custTin { get; set; }

        public string soAdresss { get; set; }
        public string soAdresss1 { get; set; }
        public string soAdresss2 { get; set; }
        public string socity { get; set; }
        public string sostate { get; set; }
        public string socountry { get; set; }
        public string sozip { get; set; }
        public string mailid { get; set; }


        public string discount { get; set; }
        public string company { get; set; }
        public string HTamnt { get; set; }
        public string logoName { get; set; }
        public string itemlogo { get; set; }
        public string PONO { get; set; }
        public string SONO { get; set; }
        public string JONO { get; set; }
        public string CustomerAddress { get; set; }
        public string VendorAddress { get; set; }
        public string JoberAddress { get; set; }
        public string Vendor { get; set; }
        public string Customer { get; set; }
        public string Reference { get; set; }
        public string Note { get; set; }
        public string RoundOffAmount { get; set; }
        public string VATNO { get; set; }
        public string CSTNO { get; set; }
        public string PANNO { get; set; }
        public string SentThrough { get; set; }
        public string SMobileNo { get; set; }
        public string ModeOfTransportation { get; set; }
        public string STrackingNo { get; set; }
        public string CVATNO { get; set; }
        public string CCSTNO { get; set; }
        public string CPANNO { get; set; }
        public string ACCOUNTNO { get; set; }
        public string TINNO { get; set; }
        public string CServiceTaxNO { get; set; }

        public string Item { get; set; }
        public string Quantity { get; set; }
        public string Weight { get; set; }
        public string Units { get; set; }
        public string SerialNumbers { get; set; }
        public string Description { get; set; }
        public string Options { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public string Total { get; set; }
        public string TaxAmnt { get; set; }

        public string SubTotal { get; set; }
        public string TotalAmount { get; set; }
        public string TotalQuantity { get; set; }
        public string PaymentTerms { get; set; }
        public string PaymentMode { get; set; }
        public string DueDate { get; set; }
        public string SOrderID { get; set; }
        public string JOrderID { get; set; }

        public string PhoneNo { get; set; }
        public string Name { get; set; }
        public string UOM { get; set; }

        public string vatAmnt { get; set; }
        public string quantity { get; set; }
        public string Jober { get; set; }
        public string amtInWord { get; set; }

        public string operatingUnitAdds { get; set; }

        public string S { get; set; }
        public string M { get; set; }
        public string L { get; set; }
        public string XL { get; set; }
        public string XXL { get; set; }
        public string GrandTotal { get; set; }
        public string VehicleNo { get; set; }
        public string CustGSTIN { get; set; }
        public string TransportName { get; set; }
        public string HSNCode  { get; set; }
        public string ItemTotal { get; set; }
        public string ItemDiscount { get; set; }
        public string StateCode { get; set; }
        public string BillingAddress { get; set; }
        public string SACCode { get; set; }
        public string RevCharge { get; set; }
        public string PlaceofSupply { get; set; }
        public string TotQty { get; set; }

        public string purchaseitemDesc { get; set; }
        public string fCpm { get; set; }
        public string dDoctNo { get; set; }
        public string avgCons { get; set; }
        public string delvPoint { get; set; }
        public string despDate { get; set; }

        public string Adresss { get; set; }
        public string Adresss1 { get; set; }
        public string Adresss2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zip { get; set; }

        public string cAdresss { get; set; }
        public string cAdresss1 { get; set; }
        public string cAdresss2 { get; set; }
        public string ccity { get; set; }
        public string cstate { get; set; }
        public string ccountry { get; set; }
        public string czip { get; set; }

        public string ogstin { get; set; }
        public string opuname { get; set; }
        public string oAdresss { get; set; }
        public string oAdresss1 { get; set; }
        public string oAdresss2 { get; set; }
        public string ocity { get; set; }
        public string ostate { get; set; }
        public string ocountry { get; set; }
        public string ozip { get; set; }

        public string SHAdresss { get; set; }
        public string SHAdresss1 { get; set; }
        public string SHAdresss2 { get; set; }
        public string SHcity { get; set; }
        public string SHstate { get; set; }
        public string SHcountry { get; set; }
        public string SHzip { get; set; }
//EMPHCS1080 - GNANESHWAR 3/9/2015 Adding VAT NO,CST NO,Service Tax NO,PAN NO columns in operating unit
//--EMPHCS1081 - GNANESHWAR 3/9/2015 Adding Amount to be deposited in favour of COLUMN on company master
		public string memo { get; set; }
        public string ouName { get; set; }

        public string compADD { get; set; }
        public string compA1 { get; set; }
        public string compA2 { get; set; }
        public string compST { get; set; }
        public string compCT { get; set; }
        public string compCity { get; set; }
        public string compZip { get; set; }
        public string compCtry { get; set; }
        public string compState { get; set; }
        public string OPUDVATNO { get; set; }
        public string OPUDCSTNO { get; set; }
        public string OPUVATNO { get; set; }
        public string OPUCSTNO { get; set; }
        public string OPUPANNO { get; set; }
        public string OPUPhNo { get; set; }
        public string footerText { get; set; }
        public string souPhno { get; set; }
        public string douPhno { get; set; }
        public string cTIn { get; set; }
        public string OPUDTINNO { get; set; }
        public string custPhNo { get; set; }
        //EMPHCS1231  3 reports to Nandyal By Raj.Jr 15/10/2015
        public string opunit { get; set; }
        public string tamnt { get; set; }
        public string dcno { get; set; }
        public string TDSAmount { get; set; }
        public string BalanceDue { get; set; }
		public string ACOUNTOWNER { get; set; }
//EMPHCS1196 by gnaneshwar on 22/9/2015 INVOICE AMOUNT / bILL AMOUNT - SYSTEM IS NOT CONSIDRING THE OVERWRITTEN AMOUNT
        public string TotalnonTDS { get; set; }
        public string wayBill { get; set; }
        public string totalWeight { get; set; }
        public string totalBags { get; set; }
        public string Wastage { get; set; }
        public string NetWeight { get; set; }
        public string qty { get; set; }
        public string OtherCharges { get; set; }
		//EMPHCS1500	Invoice custom Report BY RAJ.Jr
        public string AmountPaid { get; set; }
        public string DueAmount { get; set; }
        public string Account { get; set; }
        //EMPHCS1501	Invoice Print changes adding Drug LIC,MRP,PTR BY RAJ.Jr
        public string cDrugLIC { get; set; }
        public string opDrugLIC { get; set; }
        public string MRP { get; set; }
        public string PTR { get; set; }
        public string purchaseorder { get; set; }
       
		//EMPHCS1507 12/01/2015 Service Receipt Screen Adding in retail
        public string CESSAmount { get; set; }
        public string KCESSAmount { get; set; }
        //EMPHCS1535 - Invoice print changes according to UKS Global pest Mang sol.  By GNANESHWAR on 24/1/2016 
        public string UksDate { get; set; }
        public string UksRef { get; set; }
		//EMPHCS1547 rajasekhar patakota 02/02/2016 Return and edit changes in retail application
		public string rCustomerno { get; set; }
        public string rCustomer { get; set; }
        public string rNoofitems { get; set; }
        public string rTotalItems { get; set; }
        public string rSubTotal { get; set; }
        public string rDiscount { get; set; }
        public string rTax { get; set; }
        public string rTotal { get; set; }
        public string rPayment { get; set; }
        public string rPamt { get; set; }
        public string rDueamt { get; set; }
        public string rAccount { get; set; }
        public string ide { get; set; }
        public string rcardNo { get; set; }
        public string rPayAcc { get; set; }
        public string rPayType { get; set; }

        public string HCustomer{ get; set; }
        public string CreditMemo{ get; set; }
        public string OrderStatus{ get; set; }
        public string Approval{ get; set; }
        public string ReceiptType{ get; set; }
        public string HReceipt{ get; set; }
        public string Terms{ get; set; }
        public string HPaymentMode{ get; set; }
        public string Operating_Unit{ get; set; }
        public string HDepartment{ get; set; }
        public string HTaxType{ get; set; }
        public string Notes{ get; set; }
        public string HProject{ get; set; }
        public string Subtotal{ get; set; }
        public string DiscountAmount{ get; set; }
        public string TaxAmount{ get; set; }
        public string Addressee{ get; set; }
        public string Address1{ get; set; }
        public string Address2{ get; set; }
        public string HCountry{ get; set; }
        public string HState{ get; set; }
        public string City{ get; set; }
        public string Zip{ get; set; }
        public string Billing{ get; set; }
        public string Shipping{ get; set; }
        public string location{ get; set; }
        //EMPHCS1548  Creating Accounts Payable & Receivable PDF Prints by gnaneshwar on 2/4/2016
		public string pFDate{ get; set; }
        public string pTDate{ get; set; }

        public string OPCIN { get; set; }
        public string cCIN { get; set; }
        public string UPC { get; set; }

        public string Signature { get; set; }
		//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
        public string Markup { get; set; }
        public string amttype { get; set; }
		//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
        public string FreeQty { get; set; }
//EMPHCS1791	Resource Consumption prints and Reports By Raj.Jr
        public string Quality { get; set; }
        public string BatchNo { get; set; }
        public string Shade { get; set; }
        public string Shift { get; set; }
        public string Pcs { get; set; }
        public string Kgs { get; set; }
        public string Mts { get; set; }
        public string ReceiptNo { get; set; }
        public string ReceiptDate { get; set; }
        public string LotNo { get; set; }
        public string Greymtr { get; set; }
        public string Checkmtr { get; set; }
        public string Greywidth { get; set; }
        public string Fold { get; set; }
        public string Avgwt { get; set; }
        public string Sno { get; set; }
        public string OrderQty { get; set; }
        public string ReceivedQty { get; set; }
        public string IssuedQty { get; set; }
        //EMPHCS1810 : Issues Resolving VamanTex Reports,Prints by GNANESHWAR ON 3/9/2016
		public string Shrinkage { get; set; }
        public string cMail { get; set; }
        public string Attendedby { get; set; }
		//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
        public string SalesRep { get; set; }
        public string LeadSource { get; set; }
        public string Followup { get; set; }
        public string CloseDate { get; set; }
		//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
        public string TempType { get; set; }
        public string TempName { get; set; }
        public string TempDefault { get; set; }
        public string TempMessage { get; set; }
        public string TempBody { get; set; }
        public string NullAC { get; set; }

        public string TransNo { get; set; }
        public string TOTQTY { get; set; }
        public string SUBTOT { get; set; }
        public string TOTTAX { get; set; }
        public string TOTCGT { get; set; }
        public string TOTSGT { get; set; }
        public string TOTCESS { get; set; }
        public string TOTIGT { get; set; }
        public string TOTAMT { get; set; }
        public string CGSTIN { get; set; }
        public string Custmail {get; set;}
        public string OPGSTIN {get; set;}
        public string companydesc{ get; set; }
        public string companymail { get; set; }
        public string compA3 { get; set; }
        public string CompanyID { get; set; }
        public string subtotamt { get; set; }
        public string totaltax { get; set; }
        public string cgtttax { get; set; }
        public string sgtttax { get; set; }
        public string igtttax { get; set; }




        public string amtInWord1 { get; set; }
        public string SUBTOT1 { get; set; }
        public string roundoffadjamt { get; set; }
        public string RoundOff { get; set; }
        public string compGST { get; set; }
        public string DLNO { get; set; }
        public string STNO { get; set; }
        public string GSTSUBTOTAL { get; set; }
        public string GSTTYPE { get; set; }
        public string StateCodep { get; set; }
        public string cusmemo { get; set; }
        public string Durgno { get; set; }

    }




}