﻿
using eBizSuiteAppModel.Table;
using eBizSuiteDAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.Data;
using iTextSharp;
using System.Text;
//EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
using System.Data.SqlClient;
using System.Data;
using System.Net.Sockets;
using System.IO;
using MeBizSuiteAppUI.Controllers;
using System.Net.Mail;

namespace eBizSuiteAppUI.Controllers
{
    public class AccountController : Controller
    {
        string cn = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();
		//EMPHCS1800 rajasekhar reddy patakota 22/08/2016 Partner Login Setup in Customer Level
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        eBizSuiteAppDAL.classes.LogWriter lg = new eBizSuiteAppDAL.classes.LogWriter();
        StringBuilder logfile = new StringBuilder();
        #region Logon
        public ActionResult Logon(string id)
        {
            try
            {
List<string> ImageExtensions = new List<string> { ".JPG",".JIF", ".JPE", ".JP2", ".JPX", ".J2K", ".JPEG", ".BMP", ".GIF", ".PNG", ".TIF", ".TIFF" };
                if (Session["logoP"] != null)
                {
                    foreach (string s in ImageExtensions)
                    {
                        id = Convert.ToString(Session["logoP"]).Replace(s,"");
                    }
                }
                //else
                //    Session["logoP"] = id;
            if (id != null)
            { foreach (string s in ImageExtensions)

                {
                    id = id + s;
                    DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/Content/Upload/"));
                    FileInfo[] TXTFiles = di.GetFiles(id);
                    if (TXTFiles.Length != 0)
                    {
                        Session["logoP"] = id;
                        id = id.Replace(s, "");

                    }
                    else
                        id = id.Replace(s, "");
                }
            }
                // EMPHC IP Config Venkat 05/05/2016
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                DateTime signout = DateTime.Now;
                if (Session["LogedEmployeeID"] != null && Session["IPAddress"] != null && Session["Browser"] != null)
                {
                    string st = " COLUMNA02 is NULL ";
                    if (Convert.ToString(Session["OperatingUnit"]) != "NULL")
                        st = " COLUMNA02 in ( " + Convert.ToString(Session["OperatingUnit"]) + ")";
                    SqlDataAdapter das = new SqlDataAdapter("SELECT MAX (COLUMN02) FROM CONTABLE020 where COLUMNA03='" + Session["AcOwner"] + "' and COLUMN06='" + Session["IPAddress"] + "'and COLUMN07='" + Session["Browser"] + "' and " + st + "", con);
                    DataTable dts = new DataTable();
                    das.Fill(dts);
                    int num1 = 0;
                    if (dts.Rows.Count > 0 && dts.Rows[0][0].ToString() != "") num1 = Convert.ToInt32(dts.Rows[0][0]);

                    SqlCommand cmd = new SqlCommand(" UPDATE CONTABLE020  SET COLUMN05 ='" + signout + "'   Where COLUMN03=" + Session["LogedEmployeeID"] + " AND  COLUMNA03='" + Session["AcOwner"] + "' AND  COLUMN06= '" + Session["IPAddress"] + "' AND  COLUMN07= '" + Session["Browser"] + "' and COLUMN02=" + num1 + "", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                Session.Abandon();
                return View();
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return View();
            }
        }
        #endregion

        public void GetExceptionMsg(Exception ex)
        {
            var msg = "Failed........";
            msg = msg + " Due to " + ex.Message.ToString().Replace("\r\n", " ");
            var linenumber = ex.StackTrace.Substring(ex.StackTrace.Length - 5, 5);
            logfile.Append(msg + " at line no : " + linenumber + "At" + DateTime.Now + " \r\n");
            lg.CreateFile(Server.MapPath("~/"), Session["UserName"].ToString() + "", Convert.ToString(logfile));
        }
        #region PasswordSetup
        public ActionResult PasswordSetup(string Type, string Id)
        {
            try
            {
                var Msg = TempData["ResetMsg"]; var userid = TempData["userid"];
                if (Msg != "" && Msg != null) ViewBag.SuccessMsg = Msg;
                if (userid != "" && userid != null) ViewBag.Id = Msg;
                else ViewBag.Id = Request["Id"];
                ViewBag.Type = Request["Type"];
                return View();
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return View();
            }
        }
        #endregion

        #region PasswordSetupPOST
        [HttpPost]
        public ActionResult PasswordSetup(FormCollection fc, HttpPostedFileBase hb)
        {
            try
            {
                var Type = fc["Type"];
                ViewBag.Type = Type; var Msg = ""; TempData["ResetMsg"] = ""; TempData["userid"] = "";
                if (Type == "Reset")
                {
                    string email = fc["email"];
                    string currentpassword = fc["currentpassword"];
                    string password = fc["password"];
                    string cpassword = fc["cpassword"];
                    SqlCommand cmd = new SqlCommand(" UPDATE MATABLE010  SET COLUMN21 ='" + password + "'   Where COLUMN13='" + email + "' and COLUMN21 ='" + currentpassword + "' ", con);
                    con.Open();
                    int i = cmd.ExecuteNonQuery();
                    con.Close();
                    if (i > 0)
                        Msg = "Password Updated Successfully";
                    else
                        Msg = "Password Updation Failed";
                }
                else if (Type == "ForgotSave")
                {
                    string userid = fc["userid"];
                    string password = fc["password"];
                    string cpassword = fc["cpassword"];
                    SqlCommand cmd = new SqlCommand(" UPDATE MATABLE010  SET COLUMN21 ='" + password + "'   Where COLUMN02='" + userid + "'", con);
                    con.Open();
                    int i = cmd.ExecuteNonQuery();
                    con.Close();
                    if (i > 0)
                        Msg = "Password Updated Successfully";
                    else
                        Msg = "Password Updation Failed";
                    TempData["userid"] = userid;
                }
                else if (Type == "Forgot")
                {
                    string email = fc["email"];
                    SqlCommand cmd = new SqlCommand(" SELECT COLUMN02 FROM MATABLE010  Where COLUMN13='" + email + "' and isnull(COLUMNA13,0)=0 ", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        MailMessage mm = new MailMessage();
                        mm.To.Add(email.Trim());
                        mm.From = new MailAddress("emphorasoft@gmail.com");
                        mm.Subject = "Password Recovery";
                        mm.Body = string.Format("Hi {0},<br /><br />This email was sent automatically by Emphorasoft in response to your request to forgot password. This is done for your protection; only you, the recipient of this email can take the next step in the password recovery process." +
                        "<br /><br />To access your password and access your account, either click on the button or copy and paste the following link (expires in 24 hours) into the address bar of your browser:" +
                        "http://www.mysmartscm.com/Account/PasswordSetup?Type=ForgotSave&Id=" + dt.Rows[0][0] + "", email, dt.Rows[0][0]);
                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        if (email.Contains("yahoo"))
                        {
                            smtp.Host = "smtp.mail.yahoo.com";
                            smtp.Port = 587;
                        }
                        else
                        {
                            smtp.Host = "smtp.gmail.com";
                            smtp.Port = 587;
                        }
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential();
                        NetworkCred.UserName = "emphorasoft@gmail.com";
                        NetworkCred.Password = "Welcome12";
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Send(mm);
                        Msg = "Please check your inbox, we've sent you reset password url.";
                    }
                    else
                        Msg = "You given wrong input,please contact admin";
                }
                TempData["ResetMsg"] = Msg;
                return RedirectToAction("PasswordSetup", new { Type = Type });
            }
            catch (Exception ex)
            {
                TempData["ResetMsg"] = ex.Message;
                GetExceptionMsg(ex);
                return RedirectToAction("PasswordSetup", new { Type = fc["Type"] });
            }
        }
        #endregion

		//EMPHCS1800 rajasekhar reddy patakota 22/08/2016 Partner Login Setup in Customer Level
        public void CustomerLoginsetup()
        {
            try
            {
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
                string str = "select iif(isnull(COLUMN04,'')='','dd/MM/yyyy',COLUMN04)COLUMN04,iif(isnull(COLUMN10,'')='','dd/mm/yy',COLUMN10)COLUMN10  from MYTABLE001 where COLUMN09=" + Session["eid"] + " and (COLUMNA03='" + Session["AcOwner"] + "' or COLUMNA03 is null) and isnull(COLUMNA13,0)=0";
                SqlDataAdapter dad = new SqlDataAdapter(str, cn);
                DataTable dtd = new DataTable();
                dad.Fill(dtd);
                if (dtd.Rows.Count > 0)
                {
                    Session["DateFormat"] = dtd.Rows[0][0].ToString();
                    Session["FormatDate"] = dtd.Rows[0][0].ToString();
                    Session["ReportDate"] = dtd.Rows[0][1].ToString();
                }
                else
                {
                    Session["ReportDate"] = "dd/mm/yy";
                    Session["FormatDate"] = "dd/MM/yyyy";
                    Session["DateFormat"] = "dd/MM/yyyy";
                }
                //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                SqlCommand cmd = new SqlCommand("SELECT max(COLUMN02) FROM CONTABLE030 where (COLUMN37=1 or COLUMN37='True') and COLUMNA03='" + Session["AcOwner"] + "' and " + Session["OPUnitWithNull"] + " and isnull(COLUMNA13,0)=0 and isnull(COLUMN07,0)=0", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0) Session["Location"] = dt.Rows[0][0].ToString();
                else Session["Location"] = "";
                //EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
                SqlCommand cmdr = new SqlCommand("usp_Proc_TP_UserConfiguarations", cn);
                cmdr.CommandType = CommandType.StoredProcedure;
                cmdr.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmdr.Parameters.Add(new SqlParameter("@Opunit", Session["OPUnit1"]));
                SqlDataAdapter dar = new SqlDataAdapter(cmdr);
                DataTable dtr = new DataTable();
                dar.Fill(dtr);
                Session["TaxInclusive"] = ""; Session["BackOrder"] = "0";
                Session["CustomerPricing"] = "";
                Session["RediesCache"] = "1";
                Session["RoundOff"] = "0"; Session["RoundType"] = "";
                if (dtr.Rows.Count > 0)
                {
                    if (dtr.Columns.Contains("Inclusive"))
                    {
                        var value = dtr.Rows[0]["Inclusive"].ToString();
                        if (value == "1" || value == "True")
                            Session["TaxInclusive"] = "22713";
                    }
                    if (dtr.Columns.Contains("BackOrder"))
                    {
                        var valueb = dtr.Rows[0]["BackOrder"].ToString();
                        if (valueb == "1" || valueb == "True") valueb = "1";
                        else valueb = "0";
                        Session["BackOrder"] = valueb;
                    }
                    if (dtr.Columns.Contains("Pricing"))
                    {
                        var valuep = dtr.Rows[0]["Pricing"].ToString();
                        if (valuep == "1" || valuep == "True")
                            Session["CustomerPricing"] = valuep;
                    }
                    if (dtr.Columns.Contains("Cache"))
                    {
                        var valuec = dtr.Rows[0]["Cache"].ToString();
                        if (valuec == "True") valuec = "1";
                        else if (valuec == "False") valuec = "0";
                        Session["RediesCache"] = valuec;
                    }
                    if (dtr.Columns.Contains("RoundOff"))
                    {
                        var valuer = dtr.Rows[0]["RoundOff"].ToString();
                        if (valuer == "True") valuer = "1";
                        Session["RoundOff"] = valuer;
                        Session["RoundType"] = dtr.Rows[0]["RoundOffType"].ToString();
                    }
                }
                //EMPHCS1807 rajasekhar reddy patakota 29/08/2016 Edit,View Permission setup at account owner level
                SqlCommand cmdpl = new SqlCommand("usp_MAS_TP_Permissionchk", con);
                cmdpl.CommandType = CommandType.StoredProcedure;
                cmdpl.Parameters.Add(new SqlParameter("@Emp", Session["eid"]));
                cmdpl.Parameters.Add(new SqlParameter("@AcOwner", Session["AcOwner"]));
                cmdpl.Parameters.Add(new SqlParameter("@Opunit", Session["OPUnit1"]));
                SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                DataTable dtpl = new DataTable();
                dapl.Fill(dtpl);
                Session["Permission"] = "";
                List<int> myList = new List<int>();
                if (dtpl.Rows.Count > 0)
                {
                    var ids = dtpl.Rows[0]["COLUMN03"].ToString();
                    if (ids == "" || ids == null)
                        myList = dbContext.MATABLE002.Where(a => a.COLUMN03 == 11172).Select(t => t.COLUMN02).ToList();
                    else if (ids != "")
                        myList = ids.Split(',').Select(t => int.Parse(t)).ToList();
                }
                else
                    myList = dbContext.MATABLE002.Where(a => a.COLUMN03 == 11172).Select(t => t.COLUMN02).ToList();
                Session["Permission"] = myList;
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
            }
        }

        #region LogonPost
        [HttpPost]
        public ActionResult Logon(MATABLE010 regUser)
        {
            Entities dbContextIP = new Entities();
            string IpAddress = "";
            IpAddress = String.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? Request.ServerVariables["REMOTE_ADDR"] : Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(',')[0];// Convert.ToString(Dns.GetHostAddresses(Dns.GetHostName())[0]); //System.Net.Dns.GetHostEntry(myHost).AddressList[1].ToString();
            Session["FormIDACCRDN"] = null;
            System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
            string browser = browse.Browser;
            Session["browser"] = browser;
            Session["IPAddress"] = IpAddress;
            if (ModelState.IsValid)
            {
                try
                {
                    var v = dbContext.MATABLE010.Where(a => a.COLUMN13.Equals(regUser.COLUMN13) && a.COLUMN21.Equals(regUser.COLUMN21) && a.COLUMN20 == true).FirstOrDefault();
                    //EMPHC venkat IP Config 04/05/2016
                    //EMPHCS1800 rajasekhar reddy patakota 22/08/2016 Partner Login Setup in Customer Level
                    if (v != null && v.ToString() != "")
                    {
                    Session["LogedEmployeeID"] = v.COLUMN02.ToString();
                    Session["UserName"] = v.COLUMN06;
                    Session["AcOwner"] = v.COLUMNA03.ToString();
                    if (v.COLUMNA02 != null)
                        Session["OperatingUnit"] = v.COLUMNA02.ToString();
                    else
                        Session["OperatingUnit"] = "NULL";
                    var IP = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false ).Select(a => a.COLUMN07).FirstOrDefault();
                    var type = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN06).FirstOrDefault();
                    if (type == "22900")
                    {
                        var v1 = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false  && a.COLUMNA13 == false).FirstOrDefault();
                        ViewBag.v1 = v1.COLUMN07;
                    }
                    else
                    {
                        var FIP = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN07).FirstOrDefault();
                        var TIP = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN08).FirstOrDefault();
                        if (type == "22901")
                        {
                            var v2 = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).FirstOrDefault();
                            ViewBag.v2 = v2.COLUMN07;
                            var v3 = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).FirstOrDefault();
                            ViewBag.v3 = v3.COLUMN08;
                        }
                        if (FIP != null && TIP != null)
                        {
                            //EMPHC venkat IP Config 04/05/2016
                            string[] FromIPAddress = FIP.Split('.');
                            string FrIP = "";
                            foreach (string s in FromIPAddress)
                            {
                                FrIP += s;
                            }
                            string[] ToIPAddress = TIP.Split('.');
                            string ToIP = "";
                            foreach (string s in ToIPAddress)
                            {
                                ToIP += s;
                            }
                            string[] SIP = IpAddress.ToString().Split('.');
                            string SyIP = "";
                            foreach (string s in SIP)
                            {
                                SyIP += s;
                            }
                            if (SyIP == "") SyIP = "0";
                            if (FrIP == "") FrIP = "0";
                            if (ToIP == "") ToIP = "0";
                            if (Convert.ToInt32(SyIP) >= Convert.ToInt32(FrIP) && Convert.ToInt32(SyIP) <= Convert.ToInt32(ToIP))
                            {
                                var v1 = IpAddress;
                                ViewBag.v1 = v1;
                                var v2 = IpAddress;
                                ViewBag.v2 = v2;
                                var v3 = IpAddress;
                                ViewBag.v3 = v3;
                            }
                        }
                    }
                    //EMPHC venkat IP Config 04/05/2016
                    //DateTime Login = DateTime.Now;
                    int num=0;
                    SqlDataAdapter daA = new SqlDataAdapter("SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM CONTABLE020", con);
                    DataTable dtA = new DataTable();
                    daA.Fill(dtA);
                    if (dtA.Rows.Count > 0)
                    {
                        if (dtA.Rows[0][0].ToString() == "" || dtA.Rows[0][0].ToString() == null)
                            num = 1000;
                        else
                            num = Convert.ToInt32(dtA.Rows[0][0]) + 1;
                    }
                    SqlCommand cmdA = new SqlCommand("insert into CONTABLE020 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMNA03,COLUMNA02)values(" + num + "," + Session["LogedEmployeeID"] + ",getdate(),NULL,'" + Session["IPAddress"] + "','" + Session["browser"] + "',"+Session["AcOwner"]+"," + Session["OperatingUnit"]+")", con);
                    con.Open();
                    cmdA.ExecuteNonQuery();
                    con.Close();
                    //EMPHC venkat IP Config 04/05/2016
                    if (type == "22900" && ViewBag.v2==null && ViewBag.v3==null)
                    {
                        ViewBag.v2 = ViewBag.v1;
                        ViewBag.v3 = ViewBag.v1;
                        }
                    }
                    //EMPHCS1800 rajasekhar reddy patakota 22/08/2016 Partner Login Setup in Customer Level
                    var username = regUser.COLUMN13; var password = regUser.COLUMN21;
                    SqlCommand cmdpl = new SqlCommand("usp_MAS_TP_PartnerLoginchk", con);
                    cmdpl.CommandType = CommandType.StoredProcedure;
                    cmdpl.Parameters.Add(new SqlParameter("@Username", username));
                    cmdpl.Parameters.Add(new SqlParameter("@Password", password));
                    SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                    DataTable dtpl = new DataTable();
                    dapl.Fill(dtpl);
                    if ((v != null && Convert.ToString(ViewBag.v1) == Convert.ToString(Session["IPAddress"]) && Convert.ToString(ViewBag.v2) == Convert.ToString(Session["IPAddress"]) && Convert.ToString(ViewBag.v3) == Convert.ToString(Session["IPAddress"])) || (v != null && ViewBag.v1 == null && ViewBag.v2 == null && ViewBag.v3 == null))
                    {                                   
                        ViewBag.UserID = Session["LogedEmployeeID"] = v.COLUMN02.ToString();
                        Session["LogedEmailId"] = v.COLUMN13.ToString();

                        Session["AcOwner"] = v.COLUMNA03;
                        if (v.COLUMN26 != null && v.COLUMN26 != "")
                        {
                            Session["OPUnitstatus"] = 1;
                            Session["OPUnit1"] = v.COLUMN26;
                            Session["OPUnit"] = v.COLUMN26;
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") ) ";
                            //EMPHCS1625 ADDING SURCHARGE FIELD TO INVOICE BY SRINI 24/3/1016
                            var sOP = Convert.ToInt32(Session["OPUnit1"]);
                            var sCR = dbContext.CONTABLE007.Where(q => q.COLUMN02 == sOP).ToList();
                            var srEX = sCR.Select(q => q.COLUMN39).FirstOrDefault();
                            var qcEX = sCR.Select(q => q.COLUMN40).FirstOrDefault();
                            Session["SURC"] = srEX;
                            Session["QCRC"] = qcEX;
                        }
                        else
                        {
                            //EMPHCS1625 ADDING SURCHARGE FIELD TO INVOICE BY SRINI 24/3/1016
                            Session["SURC"] = true;
                            Session["QCRC"] = false; 
                            Session["OPUnit1"] = null;
                            Session["OPUnitstatus"] = null;
                            int acid = Convert.ToInt32(Session["AcOwner"]);
                            var accountowner = dbContext.CONTABLE007.Where(a => a.COLUMNA03 == acid).ToList();

                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < accountowner.Count; i++)
                            {
                                sb.Append(accountowner[i].COLUMN02);
                                sb.Append(",");
                            }
                            string accountownerids = sb.ToString();
                            Session["OPUnit"] = accountownerids.TrimEnd(',');
                            if (Session["OPUnit"] == "")
                                Session["OPUnit"] = "''";
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                        }

                        Session["eid"] = v.COLUMN02;
                        Session["UserName"] = v.COLUMN06;
                        //EMPHCS1800 rajasekhar reddy patakota 22/08/2016 Partner Login Setup in Customer Level
                        CustomerLoginsetup();
                        int eID = Convert.ToInt32(v.COLUMN02);
                        var centerID = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID).Select(q => q.COLUMN04).FirstOrDefault();
                        var centerIDR = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID).Select(q => q.COLUMN04).ToList();
                        var center = dbContext.CONTABLE012.Where(a => a.COLUMN02 == centerID).Select(q => q.COLUMN05).FirstOrDefault();
                        var logo = dbContext.CONTABLE008.Where(a =>  a.COLUMNA03 == v.COLUMNA03 && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                        var logop = dbContext.CONTABLE002.Where(a => a.COLUMN02 == v.COLUMNA03 && a.COLUMN27 == "1").Select(q => q.COLUMN28).FirstOrDefault();
                        Session["cenid"] = center;
                        Session["logo"] = logo;
                        Session["logoP"] = logop;
                        var rls = dbContext.CONTABLE012.Where(a => centerIDR.Contains(a.COLUMN02)).ToList().OrderByDescending(q => q.COLUMN02);
                        SelectList Country = new SelectList(rls, "COLUMN05", "COLUMN03");

                        Session["roles"] = Country;
                        ViewData["roleM"] = Session["roles"];

                        return RedirectToAction("Info", "EmployeeMaster");
                        //EMPHCS1800 rajasekhar reddy patakota 22/08/2016 Partner Login Setup in Customer Level
                    }
                    else if (dtpl.Rows.Count > 0)
                    {
                        int AcOwner = Convert.ToInt32(dtpl.Rows[0]["COLUMNA03"]);
                        var OPUnit = dtpl.Rows[0]["COLUMNA02"].ToString();
                        var EmployeeID = dtpl.Rows[0]["COLUMN02"].ToString();
                        ViewBag.UserID = Session["LogedEmployeeID"] = EmployeeID.ToString();
                        Session["LogedEmailId"] = username.ToString();
                        Session["UserName"] = username;
                        if (OPUnit == "" || OPUnit == null)
                        {
                            var accountowner = dbContext.CONTABLE007.Where(a => a.COLUMNA03 == AcOwner).ToList();
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < accountowner.Count; i++)
                            {
                                sb.Append(accountowner[i].COLUMN02);
                                sb.Append(",");
                            }
                            OPUnit = sb.ToString();
                            OPUnit = OPUnit.TrimEnd(',');
                            Session["OPUnit"] = OPUnit.TrimEnd(',');
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                            Session["OPUnit1"] = null;
                            Session["OPUnitstatus"] = null;
                        }
                        else
                        {
                            Session["OPUnit"] = OPUnit;
                            Session["OPUnit1"] = OPUnit;
                            Session["OPUnitstatus"] = 1;
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ")) ";
                        }

                        Session["OperatingUnit"] = OPUnit.ToString();
                        Session["AcOwner"] = AcOwner;
                        Session["OPUnit"] = OPUnit;
                        Session["eid"] = EmployeeID;
                        int eID = Convert.ToInt32(EmployeeID);
                        var centerID = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID && a.COLUMN08 == "Partner").Select(q => q.COLUMN04).FirstOrDefault();
                        var centerIDR = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID && a.COLUMN08 == "Partner").Select(q => q.COLUMN04).ToList();
                        var center = dbContext.CONTABLE012.Where(a => a.COLUMN02 == centerID).Select(q => q.COLUMN05).FirstOrDefault();
                        var logo = dbContext.CONTABLE008.Where(a => a.COLUMNA03 == AcOwner && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                        var logop = dbContext.CONTABLE002.Where(a => a.COLUMN02 == AcOwner && a.COLUMN27 == "1").Select(q => q.COLUMN28).FirstOrDefault();
                        Session["cenid"] = center;
                        Session["logo"] = logo;
                        Session["logoP"] = logop;
                        var rls = dbContext.CONTABLE012.Where(a => centerIDR.Contains(a.COLUMN02)).ToList().OrderByDescending(q => q.COLUMN02);
                        SelectList Country = new SelectList(rls, "COLUMN05", "COLUMN03");

                        Session["roles"] = Country;
                        ViewData["roleM"] = Session["roles"];
                        CustomerLoginsetup();
                        return RedirectToAction("Info", "EmployeeMaster");
                    }
                    else if (v != null || ViewBag.v1 == null || ViewBag.v2 == null || ViewBag.v3 == null)
                    {
                        //EMPHC venkat IP Config 04/05/2016
                        Session["ErrorMessage"] = "You Are Not Authorised Please Contact Your Administraton";
                        return View(regUser);

                    }
                }
                catch(Exception ex)
                {
                    GetExceptionMsg(ex);
                    return RedirectToAction("Logon", "Account");
                }
            }
            ViewData["ErrorMessage"] = "Invalid Username or Password";
            return View(regUser);
        }
        #endregion

        #region LogIn
        public ActionResult LogIn()
        {
            return View("LogIn");
        }
        #endregion

        #region LogInPost
        [HttpPost]
        public ActionResult LogIn(CONTABLE021 regUser)
        {
            try
            {
                Session["FormIDACCRDN"] = null;
                System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
                string browser = browse.Browser;
                Session["browser"] = browser;

                string strHost = "";
                strHost = System.Net.Dns.GetHostName();
                IPHostEntry ipAdd = System.Net.Dns.GetHostEntry(strHost);
                IPAddress[] addr = ipAdd.AddressList;
                string IPAddress = addr[addr.Length - 2].ToString();


                if (ModelState.IsValid)
                {

                    var C = dbContext.CONTABLE021.Where(a => a.COLUMN10.Equals(regUser.COLUMN10) && a.COLUMN14.Equals(regUser.COLUMN14));
                    {
                        var v = dbContext.CONTABLE021.Where(a => a.COLUMN10.Equals(regUser.COLUMN10) && a.COLUMN14.Equals(regUser.COLUMN14)).FirstOrDefault();
                        if (v != null)
                        {
                            ViewBag.UserID = Session["LogedEmployeeID"] = v.COLUMN02.ToString();
                            Session["LogedEmailId"] = v.COLUMN10.ToString();

                            int empID = Convert.ToInt32(Session["LogedEmployeeID"]);





                            var db = Database.OpenConnectionString(cn, providerName);
                            var query = "select COLUMN02 from CONTABLE021 where COLUMN10='" + v.COLUMN10.ToString() + "'";
                            Session["id"] = db.Query(query).ToString();

                            var chk = dbContext.CONTABLE019.Where(q => q.COLUMN03 == v.COLUMN02).FirstOrDefault();



                            if (chk == null)
                            {


                                return RedirectToAction("SecurityQuestion", "Account");
                            }
                            else
                            {
                                var chk1 = dbContext.CONTABLE020.Where(a => a.COLUMN06 == IPAddress && a.COLUMN07 == browser && a.COLUMN03 == v.COLUMN02).FirstOrDefault();


                                if (chk1 != null)
                                {
                                    return RedirectToAction("Info", "EmployeeMaster");
                                }
                                else
                                {
                                    ViewBag.a = dbContext.CONTABLE018.Select(a => a.COLUMN03);


                                    Session["error01"] = "Please Answer Security Question !";
                                    // return View("SecurityAnswerCheck");

                                    return RedirectToAction("SecurityAnswerCheck", "Account");

                                }

                            }



                        }

                        else
                        {
                            ViewBag.error = "The Email or Password you entered is incorrect.";
                        }
                    }

                }
                return View(regUser);
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return View();
            }

        }
        #endregion

        #region SecurityQuestion
        public ActionResult SecurityQuestion()
        {
            try
            {
                ViewBag.a = dbContext.CONTABLE018.Select(a => a.COLUMN03);
                ViewBag.UserID = Convert.ToInt32(Session["LogedEmployeeID"]);
                return View("SecurityQuestion");
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return View();
            }
        }
        #endregion

        #region SecurityQuestionPost
        [HttpPost]
        public ActionResult SecurityQuestion(CONTABLE019 user)
        {

            try
            {

                if (ModelState.IsValid)
                {
                    //user info
                    int UserID = ViewBag.UserID = Convert.ToInt32(Session["LogedEmployeeID"]);

                    CONTABLE020 sysLogInfo = new CONTABLE020();
                    var col2 = dbContext.CONTABLE020.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (col2 == null)
                    {
                        sysLogInfo.COLUMN02 = 100;
                    }
                    else
                    {
                        sysLogInfo.COLUMN02 = (col2.COLUMN02 + 1);
                    }
                    sysLogInfo.COLUMN03 = Convert.ToInt32(UserID);

                    sysLogInfo.COLUMN04 = DateTime.Now.Date;

                    sysLogInfo.COLUMN05 = DateTime.Now;

                    string strHost = "";
                    strHost = System.Net.Dns.GetHostName();
                    IPHostEntry ipAdd = System.Net.Dns.GetHostEntry(strHost);
                    IPAddress[] addr = ipAdd.AddressList;
                    sysLogInfo.COLUMN06 = addr[addr.Length - 2].ToString();

                    System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
                    string browser = browse.Browser;

                    sysLogInfo.COLUMN07 = browser;

                    sysLogInfo.COLUMN08 = Session["LogedeMailId"].ToString();
                    sysLogInfo.COLUMN09 = "India";
                    dbContext.CONTABLE020.Add(sysLogInfo);
                    dbContext.SaveChanges();


                    AdminSesurityAnswerManage userManager = new AdminSesurityAnswerManage();
                    if (!userManager.IsUserLoginIDExist(UserID))
                    {
                        userManager.Add(user, UserID);
                        FormsAuthentication.SetAuthCookie(user.COLUMN04, false);
                        //chage path to code
                        return RedirectToAction("Info", "EmployeeMaster");

                    }
                    else
                    {
                        ModelState.AddModelError("", "LogID already taken");
                    }
                }
                else
                {

                    ViewBag.a = dbContext.CONTABLE018.Select(a => a.COLUMN03);
                    ViewBag.UserID = Convert.ToInt32(Session["LogedEmployeeID"]);
                    return View("SecurityQuestion");
                }
            }
            catch(Exception ex)
            {
                GetExceptionMsg(ex);
                return View(user);
            }

            return View(user);
        }
        #endregion

        #region SecurityAnswerCheck
        public ActionResult SecurityAnswerCheck()
        {
            try
            {
                ViewBag.a = dbContext.CONTABLE018.Select(a => a.COLUMN03);
                ViewBag.errorLogin = "Please Answer Security Question !";
                return View("SecurityAnswerCheck");
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return View();
            }
        }
        #endregion

        #region SecurityAnswerCheckPost
        [HttpPost]
        public ActionResult SecurityAnswerCheck(FormCollection user)
        {
            try
            {
                int id = Convert.ToInt32(Session["LogedEmployeeID"]);
                string sq = Request["SQ"].ToString();
                string sa = Request["SA"].ToString();

                var v = dbContext.CONTABLE019.Where((a => a.COLUMN04.Equals(sq) && a.COLUMN05.Equals(sa) && a.COLUMN03.Equals(id) || a.COLUMN06.Equals(sq) && a.COLUMN07.Equals(sa) && a.COLUMN03.Equals(id) || a.COLUMN08.Equals(sq) && a.COLUMN09.Equals(sa) && a.COLUMN03.Equals(id))).FirstOrDefault();
                if (v != null)
                {
                    //code modifiy

                    CONTABLE020 sysLogInfo = new CONTABLE020();
                    var col2 = dbContext.CONTABLE020.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                    if (col2 == null)
                    {
                        sysLogInfo.COLUMN02 = 100;
                    }
                    else
                    {
                        sysLogInfo.COLUMN02 = (col2.COLUMN02 + 1);
                    }
                    sysLogInfo.COLUMN03 = Convert.ToInt32(id);

                    sysLogInfo.COLUMN04 = DateTime.Now.Date;

                    sysLogInfo.COLUMN05 = DateTime.Now;

                    string strHost = "";
                    strHost = System.Net.Dns.GetHostName();
                    IPHostEntry ipAdd = System.Net.Dns.GetHostEntry(strHost);
                    IPAddress[] addr = ipAdd.AddressList;
                    sysLogInfo.COLUMN06 = addr[addr.Length - 2].ToString();

                    System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
                    string browser = browse.Browser;

                    sysLogInfo.COLUMN07 = browser;

                    sysLogInfo.COLUMN08 = Session["LogedeMailId"].ToString();
                    sysLogInfo.COLUMN09 = "India";
                    dbContext.CONTABLE020.Add(sysLogInfo);
                    dbContext.SaveChanges();

                    return RedirectToAction("Info", "EmployeeMaster");
                }
                else
                {
                    ViewBag.a = dbContext.CONTABLE018.Select(a => a.COLUMN03);
                    ViewBag.UserID = Convert.ToInt32(Session["LogedUserID"]);
                    ViewBag.errorCheck = "You are Not Authorized! please contact Admin";
                    return View("SecurityAnswerCheck");
                }
                return View(user);
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return View();
            }

        }
        #endregion

        #region SignOut
        public ActionResult SignOut()
        {
            Session["browser"] = null;
            Session["LogedEmployeeID"] = null;
            Session["LogedEmailId"] = null;
            Session["id"] = null;
            Session.Abandon();
            return View();
        }

        public ActionResult GetRole(int roleID)
        {
            try
            {
                var center = dbContext.CONTABLE012.Where(a => a.COLUMN05 == roleID).Select(q => q.COLUMN05).FirstOrDefault();
                Session["cenid"] = center;
                return Json("");
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return Json("");
            }
        }
        #endregion

        public JsonResult GetActvity()
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                var eID = Convert.ToInt32(Session["eid"]);
                var dt = DateTime.Now.ToString();
                var actTD = activity.SETABLE001.Where(q => q.COLUMN09 != "22347" && q.COLUMN09 != "22348" && q.COLUMNA08 == eID && q.COLUMN07 < DateTime.Now && q.COLUMN08 > DateTime.Now).ToList();
                //var taskTD = activity.SETABLE002.ToList();
                //var bulTD = activity.SETABLE003.ToList();
                var act = "";
                var actID = 0;
                act = actTD.Select(q => q.COLUMN05).FirstOrDefault();
                actID = actTD.Select(q => q.COLUMN02).FirstOrDefault(); if (act == null) act = "";
                //var task = taskTD.Select(q => q.COLUMN05).FirstOrDefault();
                //var bult = bulTD.Select(q => q.COLUMN05).FirstOrDefault();
                return Json(new { ACT = act, ID = actID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return Json(new { ACT = "", ID = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateActvity(string status, int ID)
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                var actTD = activity.SETABLE001.Where(q => q.COLUMN02 == ID).FirstOrDefault();
                actTD.COLUMN09 = status;
                actTD.COLUMNA11 = Convert.ToInt32(Session["eid"]);
                activity.SaveChanges();
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCompanyList()
        {
            try
            {
                var email = Request["Email"];
                var pwd = Request["Password"];
            if (Request["Check"] == "0")
            {
                Session["Email"]=email = Request["Email"];
                Session["Password"]=pwd = Request["Password"];
            }
            else if (Request["Check"] == null)
            {
               email = Convert.ToString(Session["Email"]);
                pwd =  Convert.ToString(Session["Password"]);
            }
            var C = dbContext.MATABLE010.Where(a => a.COLUMN13.Equals(email) && a.COLUMN21.Equals(pwd) && a.COLUMN20 == true);
            Session["UserName"] = email;
            var RES = C.Select(W => W.COLUMNA03).ToList();
            List<Tuple<string, string, string>> country = new List<Tuple<string, string, string>>();
            if (RES.Count > 0)
            {
                for (int j = 0; j < RES.Count; j++)
                {
                    var id = RES[j];
                    var company = dbContext.CONTABLE002.Where(a => a.COLUMN02 == id).ToList();
                    var compan = company.Select(a => a.COLUMN02).ToList();
                    //var comp = dbContext.CONTABLE002.Where(a => a.COLUMN02 == RES[0]).ToList();
                    //for (int i = 0; i < compan.Count; i++)
                    //{
                        var num = compan[0];
                        //var companyy = dbContext.CONTABLE002.Where(a => a.COLUMN02 == num).ToList();
                        var comp = dbContext.CONTABLE002.Where(a => a.COLUMN02 == num).ToList();
                        var tuple = Tuple.Create(Convert.ToString(comp[0].COLUMN02), Convert.ToString(comp[0].COLUMN03), Convert.ToString(comp[0].COLUMND01));
                        country.Add(tuple);
                        //country.Add(new string { Value = company[0].COLUMN02.ToString(), Text = company[0].COLUMN03.ToString() });
                        //}
                    }
                    //country = country.OrderBy(x => x.COLUMN02).ToList();
                    //ViewData["Company"] = new SelectList(country, "Value", "Text");
                    ViewBag.CompanyList = country;
                }
                return View();
            }
            catch (Exception ex)
            {

                GetExceptionMsg(ex);
                return RedirectToAction("Logon", "Account");
            }
        }
        [HttpGet]
        public ActionResult Company()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("MASTER_COUNTRY_STATE_PROC", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<SelectListItem> Country = new List<SelectListItem>();
                List<SelectListItem> State = new List<SelectListItem>();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToString(dr["C1"]) != "" && Convert.ToString(dr["C2"]) != "")
                            Country.Add(new SelectListItem { Value = Convert.ToString(dr["C1"]), Text = Convert.ToString(dr["C2"]) });
                        if (Convert.ToString(dr["S1"]) != "" && Convert.ToString(dr["S2"]) != "")
                            State.Add(new SelectListItem { Value = Convert.ToString(dr["S1"]), Text = Convert.ToString(dr["S2"]) });
                    }
                    Country = Country.OrderBy(x => x.Text).ToList();
                    ViewData["Country"] = new SelectList(Country, "Value", "Text");
                    State = State.OrderBy(x => x.Text).ToList();
                    ViewData["State"] = new SelectList(State, "Value", "Text");
                }
                return View("Company");
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return RedirectToAction("Logon", "Account");
            }

        }

        [HttpPost]
        public ActionResult Company(FormCollection fcl)
        {
            try
            {
                var logo = "";
                var image = Request.Files[0] as HttpPostedFileBase;
                if (image != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        if (image.ContentLength > 0)
                        {

                        string fileName = Path.GetFileName(image.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                        image.SaveAs(path);
                        logo = fileName;
                    }

                }
            }
            using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
            {
                SqlCommand cmd = new SqlCommand("CRETAE_ACCOUNT", cnc);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EMPNAME", Convert.ToString(Session["Email"]));
                cmd.Parameters.AddWithValue("@EMPEMAIL", Convert.ToString(Session["Email"]));
                cmd.Parameters.AddWithValue("@EMPPWD", Convert.ToString(Session["Password"]));
                cmd.Parameters.AddWithValue("@COMPANY", fcl["Company"]);
                cmd.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                cmd.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                cmd.Parameters.AddWithValue("@ADDS", fcl["Address"]);
                cmd.Parameters.AddWithValue("@ADDS1", fcl["Address1"]);
                cmd.Parameters.AddWithValue("@COUNTRY", fcl["Country"]);
                cmd.Parameters.AddWithValue("@STATE", fcl["State"]);
                cmd.Parameters.AddWithValue("@CITY", fcl["City"]);
                cmd.Parameters.AddWithValue("@ZIP", fcl["Zip"]);
                cmd.Parameters.AddWithValue("@LOGO", logo);
                cnc.Open();
                cmd.ExecuteNonQuery();
            }

                return RedirectToAction("GetCompanyList");
            }
            catch (Exception ex)
            {

                GetExceptionMsg(ex);
                return RedirectToAction("Logon", "Account");
            }
        }

        public JsonResult CheckAccount()
        {
            try
            {
                var chkA = 0;
                var email = Request["Email"];
                var pwd = Request["Password"];
                var C = dbContext.MATABLE010.Where(a => a.COLUMN13.Equals(email) && a.COLUMN21.Equals(pwd) && a.COLUMN20 == true);
                var RES = C.Select(W => W.COLUMNA03).ToList();
                if (RES.Count != 0)
                {
                    var id = RES[0];
                    var company = dbContext.CONTABLE002.Where(a => a.COLUMN02 == id).ToList();
                    var compan = company.Select(a => a.COLUMNA01).ToList();
                    if (compan.Count == 1 && compan[0] != null)
                    {
                        chkA = 1;
                    }
                }
                string s = CreatePassword(10);
                return Json(new { ACT = chkA }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return Json("Logon", "Account");
            }
        }

        public ActionResult LogonSelect()
        {

            var email = Request["Email"];
            var pwd = Request["Password"];
            var acw = Convert.ToInt32(Request["Account"]);
            Entities dbContextIP = new Entities();
            string IpAddress = "";
            IpAddress = String.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? Request.ServerVariables["REMOTE_ADDR"] : Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(',')[0];// Convert.ToString(Dns.GetHostAddresses(Dns.GetHostName())[0]); //System.Net.Dns.GetHostEntry(myHost).AddressList[1].ToString();
            Session["FormIDACCRDN"] = null;
            System.Web.HttpBrowserCapabilitiesBase browse = Request.Browser;
            string browser = browse.Browser;
            Session["browser"] = browser;
            Session["IPAddress"] = IpAddress;
            if (ModelState.IsValid)
            {
                try
                {
                    var v = dbContext.MATABLE010.Where(a => a.COLUMN13.Equals(email) && a.COLUMN21.Equals(pwd) && a.COLUMN20 == true && a.COLUMNA03 == acw).FirstOrDefault();
                     if (v != null && v.ToString() != "")
                    {
                        Session["LogedEmployeeID"] = v.COLUMN02.ToString();
                        Session["UserName"] = v.COLUMN06;
                        Session["AcOwner"] = v.COLUMNA03.ToString();
                        if (v.COLUMNA02 != null)
                            Session["OperatingUnit"] = v.COLUMNA02.ToString();
                        else
                            Session["OperatingUnit"] = "NULL";
                        var IP = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN07).FirstOrDefault();
                        var type = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN06).FirstOrDefault();
                        if (type == "22900")
                        {
                            var v1 = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).FirstOrDefault();
                            ViewBag.v1 = v1.COLUMN07;
                        }
                        else
                        {
                            var FIP = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN07).FirstOrDefault();
                            var TIP = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).Select(a => a.COLUMN08).FirstOrDefault();
                            if (type == "22901")
                            {
                                var v2 = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).FirstOrDefault();
                                ViewBag.v2 = v2.COLUMN07;
                                var v3 = dbContextIP.SETABLE014.Where(a => (a.COLUMN04 == v.COLUMN02 || a.COLUMNA02 == v.COLUMNA02 || a.COLUMNA02 == null) && (a.COLUMNA03 == v.COLUMNA03 || a.COLUMNA03 == null) && a.COLUMN09 == false && a.COLUMNA13 == false).FirstOrDefault();
                                ViewBag.v3 = v3.COLUMN08;
                            }
                            if (FIP != null && TIP != null)
                            {
                                string[] FromIPAddress = FIP.Split('.');
                                string FrIP = "";
                                foreach (string s in FromIPAddress)
                                {
                                    FrIP += s;
                                }
                                string[] ToIPAddress = TIP.Split('.');
                                string ToIP = "";
                                foreach (string s in ToIPAddress)
                                {
                                    ToIP += s;
                                }
                                string[] SIP = IpAddress.ToString().Split('.');
                                string SyIP = "";
                                foreach (string s in SIP)
                                {
                                    SyIP += s;
                                }
                                if (SyIP == "") SyIP = "0";
                                if (FrIP == "") FrIP = "0";
                                if (ToIP == "") ToIP = "0";
                                if (Convert.ToInt32(SyIP) >= Convert.ToInt32(FrIP) && Convert.ToInt32(SyIP) <= Convert.ToInt32(ToIP))
                                {
                                    var v1 = IpAddress;
                                    ViewBag.v1 = v1;
                                    var v2 = IpAddress;
                                    ViewBag.v2 = v2;
                                    var v3 = IpAddress;
                                    ViewBag.v3 = v3;
                                }
                            }
                        }
                        //DateTime Login = DateTime.Now;
                        int num = 0;
                        SqlDataAdapter daA = new SqlDataAdapter("SELECT MAX(ISNULL(COLUMN02 ,10001)) FROM CONTABLE020", con);
                        DataTable dtA = new DataTable();
                        daA.Fill(dtA);
                        if (dtA.Rows.Count > 0)
                        {
                            if (dtA.Rows[0][0].ToString() == "" || dtA.Rows[0][0].ToString() == null)
                                num = 1000;
                            else
                                num = Convert.ToInt32(dtA.Rows[0][0]) + 1;
                        }
                        SqlCommand cmdA = new SqlCommand("insert into CONTABLE020 (COLUMN02,COLUMN03,COLUMN04,COLUMN05,COLUMN06,COLUMN07,COLUMNA03,COLUMNA02)values(" + num + "," + Session["LogedEmployeeID"] + ",getdate(),NULL,'" + Session["IPAddress"] + "','" + Session["browser"] + "'," + Session["AcOwner"] + "," + Session["OperatingUnit"] + ")", con);
                        con.Open();
                        cmdA.ExecuteNonQuery();
                        con.Close();
                        if (type == "22900" && ViewBag.v2 == null && ViewBag.v3 == null)
                        {
                            ViewBag.v2 = ViewBag.v1;
                            ViewBag.v3 = ViewBag.v1;
                        }
                    }
                    var username = email; var password = pwd;
                    SqlCommand cmdpl = new SqlCommand("usp_MAS_TP_PartnerLoginchk", con);
                    cmdpl.CommandType = CommandType.StoredProcedure;
                    cmdpl.Parameters.Add(new SqlParameter("@Username", username));
                    cmdpl.Parameters.Add(new SqlParameter("@Password", password));
                    SqlDataAdapter dapl = new SqlDataAdapter(cmdpl);
                    DataTable dtpl = new DataTable();
                    dapl.Fill(dtpl);
                    if ((v != null && Convert.ToString(ViewBag.v1) == Convert.ToString(Session["IPAddress"]) && Convert.ToString(ViewBag.v2) == Convert.ToString(Session["IPAddress"]) && Convert.ToString(ViewBag.v3) == Convert.ToString(Session["IPAddress"])) || (v != null && ViewBag.v1 == null && ViewBag.v2 == null && ViewBag.v3 == null))
                    {
                        ViewBag.UserID = Session["LogedEmployeeID"] = v.COLUMN02.ToString();
                        Session["LogedEmailId"] = v.COLUMN13.ToString();

                        Session["AcOwner"] = v.COLUMNA03;
                        if (v.COLUMN26 != null && v.COLUMN26 != "")
                        {
                            Session["OPUnitstatus"] = 1;
                            Session["OPUnit1"] = v.COLUMN26;
                            Session["OPUnit"] = v.COLUMN26;
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") ) ";
                            var sOP = Convert.ToInt32(Session["OPUnit1"]);
                            var sCR = dbContext.CONTABLE007.Where(q => q.COLUMN02 == sOP).ToList();
                            var srEX = sCR.Select(q => q.COLUMN39).FirstOrDefault();
                            var qcEX = sCR.Select(q => q.COLUMN40).FirstOrDefault();
                            Session["SURC"] = srEX;
                            Session["QCRC"] = qcEX;
                        }
                        else
                        {
                            Session["SURC"] = true;
                            Session["QCRC"] = false;
                            Session["OPUnit1"] = null;
                            Session["OPUnitstatus"] = null;
                            int acid = Convert.ToInt32(Session["AcOwner"]);
                            var accountowner = dbContext.CONTABLE007.Where(a => a.COLUMNA03 == acid).ToList();

                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < accountowner.Count; i++)
                            {
                                sb.Append(accountowner[i].COLUMN02);
                                sb.Append(",");
                            }
                            string accountownerids = sb.ToString();
                            Session["OPUnit"] = accountownerids.TrimEnd(',');
                            if (Session["OPUnit"] == "")
                                Session["OPUnit"] = "''";
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                        }

                        Session["eid"] = v.COLUMN02;
                        Session["UserName"] = v.COLUMN06;
                        CustomerLoginsetup();
                        int eID = Convert.ToInt32(v.COLUMN02);
                        var centerID = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID).Select(q => q.COLUMN04).FirstOrDefault();
                        var centerIDR = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID).Select(q => q.COLUMN04).ToList();
                        var center = dbContext.CONTABLE012.Where(a => a.COLUMN02 == centerID).Select(q => q.COLUMN05).FirstOrDefault();
                        var logo = dbContext.CONTABLE008.Where(a => a.COLUMNA03 == v.COLUMNA03 && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                        var logop = dbContext.CONTABLE002.Where(a => a.COLUMN02 == v.COLUMNA03 && a.COLUMN27 == "1").Select(q => q.COLUMN28).FirstOrDefault();
                        Session["cenid"] = center;
                        Session["logo"] = logo;
                        Session["logoP"] = logop;
                        var rls = dbContext.CONTABLE012.Where(a => centerIDR.Contains(a.COLUMN02)).ToList().OrderByDescending(q => q.COLUMN02);
                        SelectList Country = new SelectList(rls, "COLUMN05", "COLUMN03");

                        Session["roles"] = Country;
                        ViewData["roleM"] = Session["roles"];

                        return RedirectToAction("Info", "EmployeeMaster");
                    }
                    else if (dtpl.Rows.Count > 0)
                    {
                        int AcOwner = Convert.ToInt32(dtpl.Rows[0]["COLUMNA03"]);
                        var OPUnit = dtpl.Rows[0]["COLUMNA02"].ToString();
                        var EmployeeID = dtpl.Rows[0]["COLUMN02"].ToString();
                        ViewBag.UserID = Session["LogedEmployeeID"] = EmployeeID.ToString();
                        Session["LogedEmailId"] = username.ToString();
                        Session["UserName"] = username;
                        if (OPUnit == "" || OPUnit == null)
                        {
                            var accountowner = dbContext.CONTABLE007.Where(a => a.COLUMNA03 == AcOwner).ToList();
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < accountowner.Count; i++)
                            {
                                sb.Append(accountowner[i].COLUMN02);
                                sb.Append(",");
                            }
                            OPUnit = sb.ToString();
                            OPUnit = OPUnit.TrimEnd(',');
                            Session["OPUnit"] = OPUnit.TrimEnd(',');
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                            Session["OPUnit1"] = null;
                            Session["OPUnitstatus"] = null;
                        }
                        else
                        {
                            Session["OPUnit"] = OPUnit;
                            Session["OPUnit1"] = OPUnit;
                            Session["OPUnitstatus"] = 1;
                            Session["OPUnitWithNull"] = " (COLUMNA02 in(" + Session["OPUnit"] + ")) ";
                        }

                        Session["OperatingUnit"] = OPUnit.ToString();
                        Session["AcOwner"] = AcOwner;
                        Session["OPUnit"] = OPUnit;
                        Session["eid"] = EmployeeID;
                        int eID = Convert.ToInt32(EmployeeID);
                        var centerID = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID && a.COLUMN08 == "Partner").Select(q => q.COLUMN04).FirstOrDefault();
                        var centerIDR = dbContext.CONTABLE027.Where(a => a.COLUMN03 == eID && a.COLUMN08 == "Partner").Select(q => q.COLUMN04).ToList();
                        var center = dbContext.CONTABLE012.Where(a => a.COLUMN02 == centerID).Select(q => q.COLUMN05).FirstOrDefault();
                        var logo = dbContext.CONTABLE008.Where(a => a.COLUMNA03 == AcOwner && a.COLUMN28 != null).Select(q => q.COLUMN28).FirstOrDefault();
                        var logop = dbContext.CONTABLE002.Where(a => a.COLUMN02 == AcOwner && a.COLUMN27 == "1").Select(q => q.COLUMN28).FirstOrDefault();
                        Session["cenid"] = center;
                        Session["logo"] = logo;
                        Session["logoP"] = logop;
                        var rls = dbContext.CONTABLE012.Where(a => centerIDR.Contains(a.COLUMN02)).ToList().OrderByDescending(q => q.COLUMN02);
                        SelectList Country = new SelectList(rls, "COLUMN05", "COLUMN03");

                        Session["roles"] = Country;
                        ViewData["roleM"] = Session["roles"];
                        CustomerLoginsetup();
                        return RedirectToAction("Info", "EmployeeMaster");
                    }
                    else if (v != null || ViewBag.v1 == null || ViewBag.v2 == null || ViewBag.v3 == null)
                    {
                        //EMPHC venkat IP Config 04/05/2016
                        Session["ErrorMessage"] = "You Are Not Authorised Please Contact Your Administraton";
                        return View();

                    }
                    //}
                }
                catch (Exception ex)
                {
                    GetExceptionMsg(ex);
                    return RedirectToAction("Logon", "Account");
                }
            }
            ViewData["ErrorMessage"] = "Invalid Username or Password";
            return View();
        }

        public string CreatePassword(int length)
        {
            try
            {
                const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(valid[rnd.Next(valid.Length)]);
                }
                return res.ToString();
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return "Logon";
            }
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("MASTER_COUNTRY_STATE_PROC", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                List<SelectListItem> Country = new List<SelectListItem>();
                List<SelectListItem> State = new List<SelectListItem>();
                List<SelectListItem> Partner = new List<SelectListItem>();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Convert.ToString(dr["C1"]) != "" && Convert.ToString(dr["C2"]) != "")
                            Country.Add(new SelectListItem { Value = Convert.ToString(dr["C1"]), Text = Convert.ToString(dr["C2"]) });
                        if (Convert.ToString(dr["S1"]) != "" && Convert.ToString(dr["S2"]) != "")
                            State.Add(new SelectListItem { Value = Convert.ToString(dr["S1"]), Text = Convert.ToString(dr["S2"]) });
                        if (Convert.ToString(dr["P1"]) != "" && Convert.ToString(dr["P2"]) != "")
                            Partner.Add(new SelectListItem { Value = Convert.ToString(dr["P1"]), Text = Convert.ToString(dr["P2"]) });
                    }
                    Country = Country.OrderBy(x => x.Text).ToList();
                    ViewData["Country"] = new SelectList(Country, "Value", "Text");
                    State = State.OrderBy(x => x.Text).ToList();
                    ViewData["State"] = new SelectList(State, "Value", "Text");
                    Partner = Partner.OrderBy(x => x.Text).ToList();
                    ViewData["Partner"] = new SelectList(Partner, "Value", "Text");
                }
                return View();
            }
            catch (Exception ex)
            {
                GetExceptionMsg(ex);
                return RedirectToAction("Logon", "Account");
            }
        }

        [HttpPost]
        public ActionResult SignUp(FormCollection fcl)
        {
            try
            {
                var logo = "";
                var acType = "10000";
                var lgType = "";
                var multi = "0";
                var pDisplay = "0";
                if (fcl["Multi"] == "on") multi = "1";
                if (fcl["Logo"] == "on") pDisplay = "1";
                if (fcl["acType"] != null && fcl["acType"] != "0" && fcl["acType"] != "") acType = fcl["acType"];
                if (fcl["lgType"] != null && fcl["lgType"] != "0" && fcl["lgType"] != "") lgType = fcl["lgType"];
                var image = Request.Files[0] as HttpPostedFileBase;
                if (image != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        if (image.ContentLength > 0)
                        {

                            string fileName = Path.GetFileName(image.FileName);
                            var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                            image.SaveAs(path);
                            logo = fileName;
                        }

                    }
                }
                using (SqlConnection cnc = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString()))
                {
                    var _pwd = CreatePassword(10);
                    SqlCommand cmd = new SqlCommand("CRETAE_ACCOUNT", cnc);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EMPNAME", fcl["Name"]);
                    cmd.Parameters.AddWithValue("@EMPEMAIL", fcl["Email"]);
                    cmd.Parameters.AddWithValue("@EMPPWD", _pwd);
                    cmd.Parameters.AddWithValue("@COMPANY", fcl["Company"]);
                    cmd.Parameters.AddWithValue("@COLUMNA06", DateTime.Now);
                    cmd.Parameters.AddWithValue("@COLUMNA07", DateTime.Now);
                    cmd.Parameters.AddWithValue("@CITY", fcl["City"]);
                    cmd.Parameters.AddWithValue("@ADDS", fcl["ADDS"]);
                    cmd.Parameters.AddWithValue("@ADDS1", fcl["ADDS1"]);
                    cmd.Parameters.AddWithValue("@EMPMOBILE", fcl["Mobile"]);
                    cmd.Parameters.AddWithValue("@COUNTRY", fcl["Country"]);
                    cmd.Parameters.AddWithValue("@STATE", fcl["State"]);
                    cmd.Parameters.AddWithValue("@ZIP", fcl["PIN"]);
                    cmd.Parameters.AddWithValue("@MULTI", multi);
                    cmd.Parameters.AddWithValue("@LOGO", logo);
                    cmd.Parameters.AddWithValue("@ACTYPE", acType);
                    cmd.Parameters.AddWithValue("@LGTYPE", lgType);
                    cmd.Parameters.AddWithValue("@PERMISSIONS", fcl["Permission"]);
                    cmd.Parameters.AddWithValue("@BUZTYPE", fcl["agType"]);
                    cmd.Parameters.AddWithValue("@GSTIN", fcl["GSTIN"]);
                    cmd.Parameters.AddWithValue("@PARTNER", fcl["Partner"]);
                    cmd.Parameters.AddWithValue("@PDISPLAY", pDisplay);
                    cnc.Open();
                   int res = cmd.ExecuteNonQuery();
                   if (res > 0)
                   {
                       MailModel _mail = new MailModel();
                       _mail.To = fcl["Email"];
                       _mail.From = "emphorasoft@gmail.com";
                       _mail.Subject = "Welcome to Accnu ERP";
                       _mail.Body = "<b><h2>Welcome to Accnu ERP,  " + fcl["Email"] + "</h2></b>, <br/><br/> We are thanking you for choosing Accnu ERP <br/><br/>Accnu ERP is a cloud based online ERP software, simple to manage your business.<br/> <div style='border-style:solid;' border='1'><br/> Login details <br/><br/><br/>Username: " + fcl["Email"] + " <br/><br/> Password:" + _pwd + "<br/><br/>Login url:https://www.accnu.com <br/><br/></div>";
                       SendMailerController _send = new SendMailerController();
                       var result = _send.MailPassword(_mail);
                   }
                }

                return RedirectToAction("Logon");
            }
            catch(Exception ex)
            {
                GetExceptionMsg(ex);
                return RedirectToAction("Logon");
            }
        }

        [HttpGet]
        public ActionResult AccountSelection()
        {
            return View();
        }

    }
}





