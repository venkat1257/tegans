﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBizSuiteAppModel.Table;
using System.IO;
using System.Xml;
using System.Web.Helpers;
using WebMatrix.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using System.Configuration;

namespace MvcApplication2.Controllers
{
    public class SubsidaryController : Controller
    {
        eBizSuiteTableEntities db = new eBizSuiteTableEntities();
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";

        //
        // GET: /Subsidary/

         string[] theader;
         string[] theadertext;

         public ActionResult Index(string ShowInactives)
         {
             try
             {

                 List<WebGridColumn> cols = new List<WebGridColumn>();
                 List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                 var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                 var ttid = tid.COLUMN02;
                 var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                 var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                 ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                 var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                 var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                 theader = new string[Acolslist.Count];
                 theadertext = new string[Acolslist.Count];
                 for (int a = 0; a < Acolslist.Count; a++)
                 {
                     var acol = Acolslist[a].ToString();
                     var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == ttid).ToList();
                     var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                     var tblcol = columndata.COLUMN04;
                     var dcol = Ccolslist[a].ToString();
                     if (a == 0)
                     {
                         cols.Add(new WebGridColumn()
                         {
                             ColumnName = tblcol,
                             Header = "Action",
                             Format = (item) => new HtmlString("<a href=/Subsidary/Edit/" + item[tblcol] + " >Edit</a>|<a href=/Subsidary/Details/" + item[tblcol] + " >View</a>")

                         });
                         cols.Add(new WebGridColumn()
                         {
                             ColumnName = tblcol,
                             Header = dcol
                         });
                         Inlinecols.Add(new WebGridColumn()
                         {
                             ColumnName = tblcol,
                             Header = dcol,
                             Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   ''  />")
                         });
                     }
                     else
                     {
                         cols.Add(new WebGridColumn()
                         {
                             ColumnName = tblcol,
                             Header = dcol
                         });
                         Inlinecols.Add(new WebGridColumn()
                         {
                             ColumnName = tblcol,
                             Header = dcol,
                             Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  '' />")
                         });
                     } theader[a] = tblcol; theadertext[a] = dcol;

                 }
                 ViewBag.cols = cols;
                 ViewBag.Inlinecols = Inlinecols;
                 ViewBag.columns = Inlinecols;
                 ViewBag.itemscol = theader;
                 ViewBag.theadertext = theadertext;
                 var ac = Convert.ToInt32(Session["AcOwner"]); string ounit = null;
                 if (ac != 56567)
                     ounit = " (COLUMNA02 in(" + Session["OPUnit"] + ") or COLUMNA02 is null) ";
                 else
                     ounit = "   COLUMNA02 is null ";
                 var query1 = "Select * from CONTABLE009 where  COLUMNA13='False' and " + ounit + " AND COLUMNA03='" + Session["AcOwner"] + "'";
                 var db2 =WebMatrix.Data.Database.Open("sqlcon");
                 var books = db2.Query(query1);
                 ViewBag.itemsdata = books;
                 Session["cols"] = cols;
                 Session["Inlinecols"] = Inlinecols;
                 return View("Index", db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList());
             }
             catch (Exception ex)
             {
                 var msg = "Failed........";
                 Session["MessageFrom"] = msg + " Due to " + ex.Message;
                 Session["SuccessMessageFrom"] = "fail";
                 return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
             }
         }


        public ActionResult CustomForm()
        {
            try
            {

                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                List<CONTABLE006> all = new List<CONTABLE006>();
                var alll = db.CONTABLE006.Where(a => a.COLUMN04 == ttid && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                all = alll.Where(a => a.COLUMN06 != null).ToList();
                var Tab = all.Where(a => a.COLUMN11 == "Tab" && a.COLUMN06 != null && a.COLUMN06 != "").ToList();
                var Ta = Tab.Select(a => a.COLUMN12).Distinct().ToList();
                var type = all.Select(b => b.COLUMN11).Distinct();
                var sn = all.Select(b => b.COLUMN12).Distinct();
                var Section = all.Where(b => b.COLUMN11 != "Tab" && b.COLUMN11 != "Item Level" && b.COLUMN06 != null && b.COLUMN06 != "").ToList();
                var sec = Section.Select(b => b.COLUMN12).Distinct();
                //var roles = db.CONTABLE012.Where(a => a.COLUMN04 != null).ToList();
                //ViewBag.Roles = roles;
                ViewBag.Type = type;
                ViewBag.Tabs = sn;
                ViewBag.TabMaster = Ta;
                ViewBag.Section = sec;
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }


        public void Normal()
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                var col2 = "COLUMN02";
                cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN02",
                    Header = "Action",
                    Format = (item) => new HtmlString("<a href=/Subsidary/Edit/" + item[col2] + " >Edit</a>|<a href=/Subsidary/Details/" + item[col2] + " >View</a>")
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN02",
                    Header = "Subsidary ID"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN03",
                    Header = "Name"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN04",
                    Header = "Description"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN05",
                    Header = "Logo"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN08",
                    Header = "Parent Of"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN10",
                    Header = "Attention"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN23",
                    Header = "RState"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN24",
                    Header = "RZip"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN26",
                    Header = "Info1"
                }); cols.Add(new WebGridColumn()
                {
                    ColumnName = "COLUMN27",
                    Header = "Info2"
                });
                ViewBag.cols = cols;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                 RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }




          [HttpGet]
        public ActionResult Sort( string ShowInactives)
        {
            try
            {

                var data = Session["Data"];
                var item = Session["sortselected"];
                ViewBag.sortselected = item;

                return View("Index", data);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }

      
         //
        // GET: /Subsidary/Details/5

        public ActionResult Details(int id = 0)
        {
            try
            {

                CONTABLE009 contable009 = db.CONTABLE009.Find(id);
                if (contable009 == null)
                {
                    return HttpNotFound();
                }
                return View(contable009);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
			    return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // GET: /Subsidary/Create

        public ActionResult Create()
        {
            try
            {
                eBizSuiteAppModel.Table.Entities activity = new eBizSuiteAppModel.Table.Entities();
                var country = activity.MATABLE016.ToList();
                ViewBag.country = country;
                var state = activity.MATABLE017.ToList();
                ViewBag.state = state;
                Session["DDDynamicItems"] = "";
                CONTABLE009 all = new CONTABLE009();
                var col2 = db.CONTABLE009.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                all.COLUMN02 = (col2.COLUMN02 + 1);
                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // POST: /Subsidary/Create

        [HttpPost]
        public ActionResult Create(CONTABLE009 contable009, string Create, string Save)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var image = Request.Files[0] as HttpPostedFileBase;
                    if (image != null)
                    {
                        if (Request.Files.Count > 0)
                        {
                            //var image = Request.Files[0] as HttpPostedFileBase;
                            if (image.ContentLength > 0)
                            {

                                string fileName = Path.GetFileName(image.FileName);
                                var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                                image.SaveAs(path);
                                contable009.COLUMN05 = fileName;

                                //db.CONTABLE009.Add(contable009);
                                //db.SaveChanges();

                            }

                        }
                    }
                    contable009.COLUMNA13 = false;
                    contable009.COLUMNA12 = true;
                    var ac = Convert.ToInt32(Session["AcOwner"]);
                    if (ac == 56567)
                    {
                        contable009.COLUMNA02 = null;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            contable009.COLUMNA02 = null;
                        else
                            contable009.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                    }
                   // contable009.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                    contable009.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                    contable009.COLUMNA08 = Convert.ToInt32(Session["LogedEmployeeID"]);
                    contable009.COLUMNA06 = DateTime.Now;
                    contable009.COLUMNA07 = DateTime.Now;
                    db.CONTABLE009.Add(contable009);
                    db.SaveChanges();
                    if (Session["DDDynamicItems"] == "DDDynamicItems")
                    {
                        Session["DDDynamicItems"] = null;
                        return RedirectToAction("FormBuild","FormBuilding", new { FormName = Session["FormName"] });
                    }
                    if (Create != null)
                    {
                        var msg = string.Empty;
                        var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1245 && q.COLUMN05 == 1).FirstOrDefault();
                        if (msgMaster != null)
                        {
                            msg = msgMaster.COLUMN03;
                        }
                        else
                        {
                            msg = "Subsidary Successfully Created.......... ";
                        }
                        Session["MessageFrom"] = msg;
                        Session["SuccessMessageFrom"] = "Success";
                        return RedirectToAction("Index");
                    }
                    else
                        return RedirectToAction("Create");

                }



                return View(contable009);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

       
        //
        // GET: /Subsidary/Edit/5

        public ActionResult Edit(int id = 0)
        {
            try
            {
                CONTABLE009 contable009 = db.CONTABLE009.Find(id);

                var tmp = db.CONTABLE009.Where(a => a.COLUMN05 != null && a.COLUMN02 == id).ToList();
                var tmp1 = tmp.Select(a => a.COLUMN05).SingleOrDefault();

                TempData["abc"] = tmp1;

                if (contable009 == null)
                {
                    return HttpNotFound();
                }
                return View(contable009);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // POST: /Subsidary/Edit/5



        [HttpPost]
        public ActionResult Edit(CONTABLE009 contable009)
        {
            try
            {

                if (ModelState.IsValid)
                {


                    //if (contable009.COLUMN05 == null)
                    //{

                    var image = Request.Files[0] as HttpPostedFileBase;
                    if (image != null)
                    {
                        if (Request.Files.Count > 0)
                        {
                            //var image = Request.Files[0] as HttpPostedFileBase;
                            if (image.ContentLength > 0)
                            {

                                string fileName = Path.GetFileName(image.FileName);
                                var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                                image.SaveAs(path);
                                contable009.COLUMN05 = fileName;

                                //db.CONTABLE009.Add(contable009);
                                //db.SaveChanges();

                            }

                        }
                    }
                    //}
                    else
                    {
                        contable009.COLUMN05 = Convert.ToString(TempData["abc"]);
                        db.CONTABLE009.Add(contable009);
                        //db.Entry(contable009).State = EntityState.Modified;
                        //db.SaveChanges();
                    }

                    var ac = Convert.ToInt32(Session["AcOwner"]);
                    if (ac == 56567)
                    {
                        contable009.COLUMNA02 = null;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            contable009.COLUMNA02 = null;
                        else
                            contable009.COLUMNA02 = Convert.ToInt32(Session["OPUnit"]);
                    }
                    contable009.COLUMNA03 = Convert.ToInt32(Session["AcOwner"]);
                    contable009.COLUMNA07 = DateTime.Now;

                    contable009.COLUMNA12 = true;

                    contable009.COLUMNA13 = false;


                    db.Entry(contable009).State = EntityState.Modified;
                    db.SaveChanges();

                    var msg = string.Empty;
                    var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1245 && q.COLUMN05 == 2).FirstOrDefault();
                    if (msgMaster != null)
                    {
                        msg = msgMaster.COLUMN03;
                    }
                    else
                    {
                        msg = "Subsidary Successfully Modified.......... ";
                    }
                    Session["MessageFrom"] = msg;
                    Session["SuccessMessageFrom"] = "Success";

                    return RedirectToAction("Index");
                }
                return View(contable009);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        //
        // GET: /Subsidary/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                CONTABLE009 contable007 = db.CONTABLE009.Find(id);

                var y = (from x in db.CONTABLE009 where x.COLUMN02 == id select x).First();
                y.COLUMNA12 = false;
                y.COLUMNA13 = true;

                //db.CONTABLE009.Remove(y);
                db.SaveChanges();
                var msg = string.Empty;
                var msgMaster = db.CONTABLE023.Where(q => q.COLUMN04 == 1245 && q.COLUMN05 == 4).FirstOrDefault();
                if (msgMaster != null)
                {
                    msg = msgMaster.COLUMN03;
                }
                else
                {
                    msg = "Subsidary Successfully Deleted.......... ";
                }
                Session["MessageFrom"] = msg;
                Session["SuccessMessageFrom"] = "Success";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }

        }

        public void ExportCSV()
        {
            try
            {
                StringWriter sw = new StringWriter();

                var y = db.CONTABLE009.OrderBy(q => q.COLUMN02).ToList();

                sw.WriteLine("\"Subsidary ID\",\"Name\",\"Description\",\"Logo\",\"Inactive\",\"Make Inventary Available\",\"Parent Of\",\"Attention\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Return Attention\",\"Return Address1\",\"Return Address2\",\"Return City\",\"Return State\",\"Return Zip\",\"Info1\",\"Info2\",\"Info3\"");

                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment;filename=Exported_Users.csv");
                Response.ContentType = "text/csv";

                foreach (var line in y)
                {

                    sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\",\"{19}\",\"{20}\",\"{21}\"",
                                               line.COLUMN02,
                                               line.COLUMN03,
                                               line.COLUMN04,
                                               line.COLUMN05,
                                               line.COLUMN07,
                                               line.COLUMN08,
                                               line.COLUMN09,
                                               line.COLUMN10,
                                               line.COLUMN11,
                                               line.COLUMN12,
                                               line.COLUMN13,
                                               line.COLUMN14,
                                               line.COLUMN15,
                                               line.COLUMN16,
                                               line.COLUMN17,
                                               line.COLUMN18,
                                               line.COLUMN19,
                                               line.COLUMN20,
                                               line.COLUMN21,
                                               line.COLUMN22,
                                               line.COLUMN23,
                                               line.COLUMN24));

                }

                Response.Write(sw.ToString());

                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public void ExportPdf()
        {
            try
            {
                List<CONTABLE009> all = new List<CONTABLE009>();
                all = db.CONTABLE009.ToList();
                var data = from e in db.CONTABLE009.AsEnumerable() select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Logo1 = e.COLUMN05, Logo2 = e.COLUMN06, InActive = e.COLUMN07, MakeInventaryAvailable = e.COLUMN08, ParentOf = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Details.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                gv.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        public void Exportword()
        {
            try
            {
                List<CONTABLE009> all = new List<CONTABLE009>();
                all = db.CONTABLE009.ToList();
                var data = from e in db.CONTABLE009.AsEnumerable() select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Logo1 = e.COLUMN05, Logo2 = e.COLUMN06, InActive = e.COLUMN07, MakeInventaryAvailable = e.COLUMN08, ParentOf = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=EmployeeDetails.doc");
                Response.ContentType = "application/vnd.ms-word ";
                Response.Charset = string.Empty;
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
       
        public void ExportExcel()
        {
            try
            {
                List<CONTABLE009> all = new List<CONTABLE009>();
                all = db.CONTABLE009.ToList();
                var data = from e in db.CONTABLE009.AsEnumerable() select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Logo1 = e.COLUMN05, Logo2 = e.COLUMN06, InActive = e.COLUMN07, MakeInventaryAvailable = e.COLUMN08, ParentOf = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
                GridView gv = new GridView();
                gv.DataSource = data;
                gv.DataBind();
                gv.FooterRow.Visible = false;
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Details.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter ht = new HtmlTextWriter(sw);
                gv.RenderControl(ht);
                Response.Write(sw.ToString());
                Response.End();
                gv.FooterRow.Visible = true;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }

        }
        public ActionResult Export(string items)
        {
            try
            {
                List<CONTABLE009> all = new List<CONTABLE009>();
                all = db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == ttid).ToList();
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (items == "PDF")
                {
                    ExportPdf();
                }
                else if (items == "Excel")
                {
                    ExportExcel();
                }
                else if (items == "Word")
                {
                    Exportword();
                }
                else if (items == "CSV")
                {
                    ExportCSV();
                }
                return View("Index", db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();

                var SubsidaryID = Request["SubsidaryID"];
                var Name = Request["Name"];
                var Attention = Request["Attention"];



                if (Name != null && Name != "" && Name != string.Empty)
                {
                    Name = Name + "%";
                }
                else
                    Name = Request["Name"];


                if (Attention != null && Attention != "" && Attention != string.Empty)
                {
                    Attention = Attention + "%";
                }
                else
                    Attention = Request["Attention"];



                if (SubsidaryID != null && SubsidaryID != "" && SubsidaryID != string.Empty)
                {
                    SubsidaryID = SubsidaryID + "%";
                }
                else
                    SubsidaryID = Request["SubsidaryID"];

                var query = "SELECT * FROM CONTABLE009  WHERE COLUMN03 like '" + Name + "' or COLUMN10 LIKE '" + Attention + "'  or COLUMN02 LIKE '" + SubsidaryID + "'";

                var query1 = db.CONTABLE009.SqlQuery(query);
                var gdata = from e in query1 select e;
                ViewBag.cols = Session["cols"];
                ViewBag.Inlinecols = Session["Inlinecols"];
                ViewBag.gdata = gdata;

                return View("Index", gdata);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }

        public ActionResult UpdateInline(string list)
        {
            try
            {
                XmlDocument obj = new XmlDocument();
                obj.LoadXml(list);
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlNodeReader(obj));
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int eid = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                    var c = db.CONTABLE009.Where(a => a.COLUMN02 == eid).FirstOrDefault();
                    c.COLUMN03 = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    c.COLUMN07 = Convert.ToBoolean(ds.Tables[0].Rows[i].ItemArray[3].ToString());
                    c.COLUMN08 = Convert.ToBoolean(ds.Tables[0].Rows[i].ItemArray[4].ToString());
                    c.COLUMN09 = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    c.COLUMN10 = ds.Tables[0].Rows[i].ItemArray[6].ToString();



                    //c.COLUMN03 = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    //c.COLUMN04 = ds.Tables[0].Rows[i].ItemArray[2].ToString();
                    //c.COLUMN07 = Convert.ToBoolean(ds.Tables[0].Rows[i].ItemArray[3].ToString());
                    //c.COLUMN08 = Convert.ToBoolean(ds.Tables[0].Rows[i].ItemArray[4].ToString());
                    //c.COLUMN09 = ds.Tables[0].Rows[i].ItemArray[5].ToString();
                    //c.COLUMN10 = ds.Tables[0].Rows[i].ItemArray[6].ToString();
                    //c.COLUMN11 = ds.Tables[0].Rows[i].ItemArray[7].ToString();
                    //c.COLUMN12 = ds.Tables[0].Rows[i].ItemArray[8].ToString();
                    //c.COLUMN13 = ds.Tables[0].Rows[i].ItemArray[9].ToString();
                    //c.COLUMN14 = ds.Tables[0].Rows[i].ItemArray[10].ToString();
                    //c.COLUMN15 = ds.Tables[0].Rows[i].ItemArray[11].ToString();
                    //c.COLUMN16 = ds.Tables[0].Rows[i].ItemArray[12].ToString();
                    //c.COLUMN17 = ds.Tables[0].Rows[i].ItemArray[13].ToString();
                    //c.COLUMN18 = ds.Tables[0].Rows[i].ItemArray[14].ToString();
                    //c.COLUMN19 = ds.Tables[0].Rows[i].ItemArray[15].ToString();
                    //c.COLUMN20 = ds.Tables[0].Rows[i].ItemArray[16].ToString();
                    //c.COLUMN21 = ds.Tables[0].Rows[i].ItemArray[17].ToString();
                    //c.COLUMN22 = ds.Tables[0].Rows[i].ItemArray[18].ToString();
                    //c.COLUMN23 = ds.Tables[0].Rows[i].ItemArray[19].ToString();
                    //c.COLUMN24 = ds.Tables[0].Rows[i].ItemArray[20].ToString();

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }

        [HttpGet]
        public ActionResult Info(string ShowInactives)
        {
            try
            {
                var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 != null).ToList();
                ViewBag.views = viewlist.Select(a => a.COLUMN03).ToList();
                if (ShowInactives != null)
                {
                    var all = db.CONTABLE009.ToList();
                    //return View(all);
                    return View("Index", db.CONTABLE009.ToList());
                }
                if (Session["Data"] != null)
                {
                    var data = Session["Data"];
                    var item = Session["sortselected"];
                    ViewBag.sortselected = item;
                    return View("Index", data);

                }
                return View("Index", db.CONTABLE009.Where(a => a.COLUMNA12 == true).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
				return RedirectToAction("Info", "EmployeeMaster", new { FormName = Session["FormName"] });
            }
        }
        string tthdata, tthtext;
        string itthdata, itthtext;
        public ActionResult View1(string items, string sort, string inactive, string style)
        {
            try
            {
                List<CONTABLE009> all = new List<CONTABLE009>();
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                var viewsdata = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewsdata.Select(a => a.COLUMN03).Distinct().ToList();
                if (style == "Normal")
                {
                    all = all.Where(a => a.COLUMNA12 == true).ToList();
                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var indata = db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from CONTABLE009 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from CONTABLE009 where COLUMNA13='False' ";
                    }
                    var db2 = WebMatrix.Data.Database.Open(sqlcon);
                    var books = db2.Query(query1);
                    if (sort == "Recently Created")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();

                    }
                    else if (sort == "Recently Modified")
                    {
                        books = books.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/Subsidary/Edit/" + itm[Inline] + " >Edit</a>|<a href=/Subsidary/Details/" + itm[Inline] + " >View</a></td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   style=width:80px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                            else
                            {
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    style=width:100px  /></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                            }
                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                    return View("Index", db.CONTABLE002.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList());

                }
                else
                {
                    var grid = new WebGrid(null, canPage: false, canSort: false);
                    var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                    var tbtid = tbid.COLUMN02;
                    var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                    ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();


                    if (sort == "Recently Created")
                    {
                        all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else if (sort == "Recently Modified")
                    {
                        all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
                    }
                    else
                    {
                        all = db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList();

                    }
                    if (inactive != null)
                    {
                        all = all.Where(a => a.COLUMNA12 == true).ToList();
                    }
                    else
                    {
                        all = all.Where(a => a.COLUMN02 != null).ToList();
                    }

                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        if (a == 0)
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = "Action",
                                Format = (item) => new HtmlString("<a href=/Subsidary/Edit/" + item[tblcol] + " >Edit</a>|<a href=/Subsidary/Details/" + item[tblcol] + " >View</a>")

                            });
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
                            });
                        }
                        else
                        {
                            cols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol
                            });
                            Inlinecols.Add(new WebGridColumn()
                            {
                                ColumnName = tblcol,
                                Header = dcol,
                                Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
                            });
                        }
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.columnscount = Inlinecols.Count;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    grid = new WebGrid(all, canPage: false, canSort: false);
                    var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "DataTable" },
                                     columns: cols);
                    var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
                 headerStyle: "webgrid-header",
                 footerStyle: "webgrid-footer",
                 alternatingRowStyle: "webgrid-alternating-row",
                 rowStyle: "webgrid-row-style",
                                     htmlAttributes: new { id = "grdData" },
                                     columns: Inlinecols);
                    return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
                }
                return View("Index", db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }


        //public ActionResult View1(string items, string sort, string inactive)
        //{
        //    try
        //    {
        //        List<WebGridColumn> cols = new List<WebGridColumn>();
        //        List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
        //        var grid = new WebGrid(null, canPage: false, canSort: false);
        //        var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
        //        var tbtid = tbid.COLUMN02;
        //        var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
        //        ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
        //        List<CONTABLE009> all = new List<CONTABLE009>();

        //        if (sort == "Recently Created")
        //        {
        //            all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        else if (sort == "Recently Modified")
        //        {
        //            all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA13 == false).ToList();
        //        }
        //        else
        //        {
        //            all = db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList();

        //        }
        //        if (inactive != null)
        //        {
        //            all = all.Where(a => a.COLUMNA12 == true).ToList();
        //        }
        //        else
        //        {
        //            all = all.Where(a => a.COLUMN02 != null).ToList();
        //        }

        //        var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
        //        var Attid = Atid.COLUMN02;

        //        var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == items && a.COLUMN04 == Attid).ToList();
        //        var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
        //        var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
        //        for (int a = 0; a < Acolslist.Count; a++)
        //        {
        //            var acol = Acolslist[a].ToString();
        //            var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
        //            var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

        //            var tblcol = columndata.COLUMN04;
        //            var dcol = Ccolslist[a].ToString();
        //            if (a == 0)
        //            {
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = "Action",
        //                    Format = (item) => new HtmlString("<a href=/Subsidary/Edit/" + item[tblcol] + " >Edit</a>|<a href=/Subsidary/Details/" + item[tblcol] + " >View</a>")

        //                });
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol
        //                });
        //                Inlinecols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol,
        //                    Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + " readonly=true   style=width:80px  />")
        //                });
        //            }
        //            else
        //            {
        //                cols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol
        //                });
        //                Inlinecols.Add(new WebGridColumn()
        //                {
        //                    ColumnName = tblcol,
        //                    Header = dcol,
        //                    Format = (item) => new HtmlString("<input type='text' id=" + tblcol + " name=" + tblcol + "    Value=" + item[tblcol] + "  style=width:100px />")
        //                });
        //            }
        //        }
        //        ViewBag.cols = cols;
        //        ViewBag.Inlinecols = Inlinecols;
        //        Session["cols"] = cols;
        //        Session["Inlinecols"] = Inlinecols;
        //        grid = new WebGrid(all, canPage: false, canSort: false);
        //        var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
        //     headerStyle: "webgrid-header",
        //     footerStyle: "webgrid-footer",
        //     alternatingRowStyle: "webgrid-alternating-row",
        //     rowStyle: "webgrid-row-style",
        //                         htmlAttributes: new { id = "DataTable" },
        //                         columns: cols);
        //        var htmlstring2 = grid.GetHtml(tableStyle: "webgrid-table",
        //     headerStyle: "webgrid-header",
        //     footerStyle: "webgrid-footer",
        //     alternatingRowStyle: "webgrid-alternating-row",
        //     rowStyle: "webgrid-row-style",
        //                         htmlAttributes: new { id = "grdData" },
        //                         columns: Inlinecols);
        //        return this.Json(new { Data = htmlstring1.ToHtmlString(), Data1 = htmlstring2.ToHtmlString() }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Content("Something went wrong <a onclick='history.go(-1); return false;' href=''> Back</a>");
        //    }
        //}




        public ActionResult ListView()
        {
            return View(db.CONTABLE009.ToList());
        }

        public ActionResult New()
        {
            return View();
        }



        public ActionResult QuickSort(string item)
        {
            try
            {
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == ttid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                List<CONTABLE009> all = new List<CONTABLE009>();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                if (item == "Recently Created")
                {
                    all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA12 == true).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                else if (item == "Recently Modified")
                {
                    all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA12 == true).ToList();
                    all = all.Where(a => a.COLUMNA07 != null).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                else
                {
                    all = db.CONTABLE009.Where(a => a.COLUMNA12 == true).ToList();
                    grid = new WebGrid(all, canPage: false, canSort: false);
                }
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
              headerStyle: "webgrid-header",
              footerStyle: "webgrid-footer",
              alternatingRowStyle: "webgrid-alternating-row",
              rowStyle: "webgrid-row-style",
                                  htmlAttributes: new { id = "DataTable" },
                                  columns:
                                      grid.Columns
                                      (grid.Column("Action", "Action", format: (items) => new HtmlString("<a href='/Subsidary/Edit/" + items.COLUMN02 + "'>Edit</a>|<a href='/Subsidary/Details/" + items.COLUMN02 + "'>View</a>")),
                                      grid.Column("COLUMN02", "Subsidary ID"),
                                      grid.Column("COLUMN03", "Name"),
                                      grid.Column("COLUMN04", "Description"),
                                      grid.Column("COLUMN09", "Parent Of"),

                                      grid.Column("COLUMN10", "Attention"),
                                      grid.Column("COLUMN23", "RState"),
                                      grid.Column("COLUMN24", "RZip"),
                                      grid.Column("COLUMN26", "Info1"),
                                      grid.Column("COLUMN27", "Info2")
                                      ));
                return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }



       

        //public ActionResult QuickSort(string item)
        //{
        //    List<CONTABLE009> all = new List<CONTABLE009>();
        //    if (item == "Recently Created")
        //    {
        //        all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA06).Where(a => a.COLUMNA12 == true).ToList();
        //        Session["sortselected"] = item;
        //        Session["Data"] = all;
        //        return View("Index", all);
        //    }
        //    else if (item == "Recently Modified")
        //    {
        //        all = db.CONTABLE009.OrderByDescending(a => a.COLUMNA07).Where(a => a.COLUMNA12 == true).ToList();
        //        all = all.Where(a => a.COLUMNA07 != null).ToList();
        //        Session["sortselected"] = item;
        //        Session["Data"] = all;
        //        return View("Index", all);
        //    }
        //    else
        //    {
        //        all = db.CONTABLE009.Where(a => a.COLUMNA12 == true).ToList();
        //        Session["Data"] = all; Session["sortselected"] = item;
        //        return View("Index", all);
        //    }
            
        //}
      
        public JsonResult InActive(string ShowInactives)
        {
            
                List<CONTABLE009> all = new List<CONTABLE009>();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                if (ShowInactives != null)
                {
                    all = db.CONTABLE009.ToList();
                    var data = from e in db.CONTABLE009.AsEnumerable() select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Inactive = e.COLUMN07, ParentOf = e.COLUMN09, Attention = e.COLUMN10, RState = e.COLUMN20, RZip = e.COLUMN21, Info1 = e.COLUMN22, Info2 = e.COLUMN23 };
                    grid = new WebGrid(data, canPage: false, canSort: false);
                }
                else
                {
                    all = db.CONTABLE009.Where(a => a.COLUMNA12 == true).ToList();
                    var data = from e in db.CONTABLE009.AsEnumerable() where (e.COLUMNA12 == true) select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Inactive = e.COLUMN07, ParentOf = e.COLUMN09, Attention = e.COLUMN10, RState = e.COLUMN20, RZip = e.COLUMN21, Info1 = e.COLUMN22, Info2 = e.COLUMN23 };
                    grid = new WebGrid(data, canPage: false, canSort: false);
                }
                var htmlstring1 = grid.GetHtml(tableStyle: "webgrid-table",
          headerStyle: "webgrid-header",
          footerStyle: "webgrid-footer",
          alternatingRowStyle: "webgrid-alternating-row",
          rowStyle: "webgrid-row-style",
                              htmlAttributes: new { id = "DataTable" },
                              columns:
                                  grid.Columns
                                  (grid.Column("Action", "Action", format: (item) => new HtmlString("<a href='/Subsidary/Edit/" + item.SubsidaryID + "'>Edit</a>|<a href='/Subsidary/Details/" + item.SubsidaryID + "'>View</a>")),
                                  grid.Column("SubsidaryID", "SubsidaryID"),
                                  grid.Column("Name", "Name"),
                                  grid.Column("Description", "Description"),
                                  grid.Column("Inactive", "Inactive"),

                                  grid.Column("Attention", "Attention"),
                                  grid.Column("RState", "RState"),
                                  grid.Column("RZip", "RZip"),
                                  grid.Column("Info1", "Info1"),
                                  grid.Column("Info2", "Info2")
                                  ));
                return this.Json(new { Data = htmlstring1.ToHtmlString() }, JsonRequestBehavior.AllowGet);
            
        }


        public ActionResult Style(string items, string inactive)
        {
            try
            {
                List<WebGridColumn> cols = new List<WebGridColumn>();
                List<WebGridColumn> Inlinecols = new List<WebGridColumn>();
                CONTABLE009 lv = new CONTABLE009();
                var grid = new WebGrid(null, canPage: false, canSort: false);
                var tbid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var tbtid = tbid.COLUMN02;
                var viewlist1 = db.CONTABLE013.Where(a => a.COLUMN03 != null && a.COLUMN04 == tbtid).ToList();
                ViewBag.views = viewlist1.Select(a => a.COLUMN03).ToList();
                var data = from e in db.CONTABLE009.AsEnumerable() select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Logo1 = e.COLUMN05, Logo2 = e.COLUMN06, InActive = e.COLUMN07, MakeInventaryAvailable = e.COLUMN08, ParentOf = e.COLUMN09, Attention = e.COLUMN10, Addressee = e.COLUMN11, Address1 = e.COLUMN12, Address2 = e.COLUMN13, City = e.COLUMN14, State = e.COLUMN15, Zip = e.COLUMN16, Country = e.COLUMN17, RAttention = e.COLUMN18, RAddressee = e.COLUMN19, RAddress1 = e.COLUMN20, RAddress2 = e.COLUMN21, RCity = e.COLUMN22, RState = e.COLUMN23, RZip = e.COLUMN24, RCountry = e.COLUMN25, Info1 = e.COLUMN26, Info2 = e.COLUMN27, Info3 = e.COLUMN28 };
                List<CONTABLE009> tbldata = new List<CONTABLE009>();
                tbldata = db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList();
                if (items == "Normal")
                {
                    var Atid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                    var Attid = Atid.COLUMN02;
                    var viewlist = db.CONTABLE013.Where(a => a.COLUMN03 == "Default" && a.COLUMN04 == Attid).ToList();
                    var indata = db.CONTABLE009.Where(a => a.COLUMNA13 == false && a.COLUMNA12 == true).ToList();
                    var query1 = "";
                    if (inactive != null)
                    {
                        query1 = "Select * from CONTABLE009 where COLUMNA13='False' and COLUMNA12 = 'True'  ";
                    }
                    else
                    {
                        query1 = "Select * from CONTABLE009 where COLUMNA13='False' ";
                    }
                    var db2 = WebMatrix.Data.Database.Open(sqlcon);
                    var books = db2.Query(query1);
                    var Acolslist = viewlist.Select(a => a.COLUMN06).ToList();
                    var Ccolslist = viewlist.Select(a => a.COLUMN07).ToList();
                    theader = new string[Acolslist.Count];
                    theadertext = new string[Acolslist.Count];
                    for (int a = 0; a < Acolslist.Count; a++)
                    {
                        var acol = Acolslist[a].ToString();
                        var columnslist = db.CONTABLE005.Where(b => b.COLUMN03 == Attid).ToList();
                        var columndata = columnslist.Where(b => b.COLUMN05 == acol).FirstOrDefault();

                        var tblcol = columndata.COLUMN04;
                        var dcol = Ccolslist[a].ToString();
                        theader[a] = tblcol;
                        theadertext[a] = dcol;
                        if (a == 0)
                        {
                            tthtext += "<th>Action</th>";
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                        else
                        {
                            tthtext += "<th>" + dcol + "</th>";
                            itthtext += "<th>" + dcol + "</th>";
                        }
                    }
                    foreach (var itm in books)
                    {
                        foreach (var Inline in theader)
                        {
                            if (Inline == "COLUMN02")
                            {
                                tthdata += "<td><a href=/Subsidary/Edit/" + itm[Inline] + " >Edit</a>|<a href=/Subsidary/Details/" + itm[Inline] + " >Details</a></td>";
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + " readonly=true   '' /></td>";
                            }
                            else
                            {
                                tthdata += "<td>" + itm[Inline] + "</td>";
                                itthdata += "<td><input type='text' id=" + itm[Inline] + " name=" + itm[Inline] + "    Value=" + itm[Inline] + "    ''  /></td>";
                            }


                        }
                        tthdata = "<tr>" + tthdata + "</tr>";
                        itthdata = "<tr>" + itthdata + "</tr>";
                    }
                    ViewBag.cols = cols;
                    ViewBag.Inlinecols = Inlinecols; ViewBag.columns = Inlinecols;
                    ViewBag.itemscol = theader;
                    ViewBag.theadertext = theadertext;
                    Session["cols"] = cols;
                    Session["Inlinecols"] = Inlinecols;
                    ViewBag.itemsdata = null;
                    ViewBag.itemsdata = books;
                    var htmlString = "<table id=tbldata><tr>" + tthtext + "</tr><tr>" + tthdata + "</tr></table>";
                    var htmlString1 = "<table id=itbldata><tr>" + itthtext + "</tr><tr>" + itthdata + "</tr></table>";
                    return this.Json(new { Data = htmlString, Data1 = htmlString1 }, JsonRequestBehavior.AllowGet);
                }
                return View("Index", db.CONTABLE009.Where(a => a.COLUMNA13 == false).ToList());
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }
   


        //public ActionResult Style(string items)
        //{
            
        //    List<CONTABLE009> all = new List<CONTABLE009>();
        //    var data = from e in db.CONTABLE009.AsEnumerable() where (e.COLUMNA12 == true) select new { SubsidaryID = e.COLUMN02, Name = e.COLUMN03, Description = e.COLUMN04, Inactive = e.COLUMN07, ParentOf = e.COLUMN09, Attention = e.COLUMN10, RState = e.COLUMN20, RZip = e.COLUMN21, Info1 = e.COLUMN22, Info2 = e.COLUMN23 };

        //    return View("Index", data);
        //}


        public ActionResult CustomView()
        {
            try
            {
                List<CONTABLE005> all = new List<CONTABLE005>();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                //all = from e in db.CONTABLE005 where (e.COLUMN03 ==ttid &&  e.COLUMN04!=e.COLUMN05   &&  e.COLUMN04!="COLUMN01") select e;
                var query = "SELECT *  FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04!=COLUMN05   and  COLUMN04!='COLUMN01' and LEN(COLUMN04)=8  ";
                all = db.CONTABLE005.SqlQuery(query).ToList();

                return View(all);
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public JsonResult CustomView(string lst, string ViewName)
        {
           

                int col2 = 0;
                var tbldata = db.CONTABLE004.Where(a => a.COLUMN05 == "tbl_subsidary").FirstOrDefault();
                var tblid = tbldata.COLUMN02;
                var vdata = db.CONTABLE013.Where(a => a.COLUMN04 == tblid).FirstOrDefault();
                var formid = vdata.COLUMN05;
                var nfd1 = db.CONTABLE013.Where(a => a.COLUMN03 == ViewName).ToList();
                eBizSuiteTableEntities db1 = new eBizSuiteTableEntities();
                if (ViewName == "")
                {
                    return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
                }
                if (nfd1.Count > 0)
                {
                    return this.Json("ViewName Already Exists", JsonRequestBehavior.AllowGet);
                }

                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(lst.ToString());
                DataSet ds = new DataSet();
                ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    CONTABLE013 dd = new CONTABLE013();
                    string fname = ds.Tables[0].Rows[i]["FieldName"].ToString();
                    string act = ds.Tables[0].Rows[i]["Action"].ToString();
                    string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
                    if (i == 0)
                    {
                        var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                        if (vid == null)
                        {
                            col2 = 1001;
                        }
                        else
                        {
                            col2 = (vid.COLUMN02) + 1;
                        }
                        dd.COLUMN02 = col2;
                        dd.COLUMN03 = ViewName;
                        dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                        dd.COLUMN05 = Convert.ToInt32(formid);
                        dd.COLUMN06 = ds.Tables[0].Rows[0]["FieldName"].ToString();
                        dd.COLUMN07 = ds.Tables[0].Rows[0]["LabelName"].ToString();
                        db.CONTABLE013.Add(dd);
                        db.SaveChanges();
                    }
                    else
                    {
                        if (act == "Y")
                        {
                            var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
                            dd.COLUMN02 = (vid.COLUMN02) + 1;
                            dd.COLUMN03 = ViewName;
                            dd.COLUMN04 = Convert.ToInt32(Session["tablid"]);
                            dd.COLUMN05 = Convert.ToInt32(formid);
                            dd.COLUMN06 = ds.Tables[0].Rows[i]["FieldName"].ToString();
                            dd.COLUMN07 = ds.Tables[0].Rows[i]["LabelName"].ToString();
                            db.CONTABLE013.Add(dd);
                            db.SaveChanges();
                        }

                    }
                }
                return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
            
        }



        //public ActionResult CustomView()
        //{
        //    List<CONTABLE006> all = new List<CONTABLE006>();
        //    var tid = db.Table_Mapping.Where(a => a.Table_Name == "CONTABLE009").First();
        //    var ttid = tid.Table_ID;
        //    Session["tablid"] = ttid;
           
        //    var query = "SELECT *  FROM CONTABLE006  WHERE COLUMN04 = " + ttid + "   and  COLUMN06!=COLUMN05";
        //    all = db.CONTABLE006.SqlQuery(query).ToList();

        //    return View(all);
        //}
        //string[] gdata = new string[10];
        //[HttpPost]
        //public JsonResult CustomView(string lst, string ViewName)
        //{
        //    if (ViewName == "")
        //    {
        //        return this.Json("Please Enter ViewName", JsonRequestBehavior.AllowGet);
        //    }
        //    XmlDocument xDoc = new XmlDocument();
        //    xDoc.LoadXml(lst.ToString());
        //    DataSet ds = new DataSet();
        //    ds.ReadXml(new XmlTextReader(new StringReader(xDoc.DocumentElement.OuterXml)));
        //    List<CONTABLE013> dd = new List<CONTABLE013>();
        //    int j = 0;
        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //        string act = ds.Tables[0].Rows[i]["Action"].ToString();
        //        string Lab = ds.Tables[0].Rows[i]["LabelName"].ToString();
        //        if (act == "Y")
        //        {
        //            gdata[j] = Lab;

        //            //string tab = ds.Tables[0].Rows[i]["FieldName"].ToString();
        //            //string Lab= ds.Tables[0].Rows[i]["LabelName"].ToString();
        //            //string act = ds.Tables[0].Rows[i]["Action"].ToString();

        //            j++;
        //            if (j == 10)
        //            {
        //                CONTABLE013 add1 = new CONTABLE013();
        //                var vid = db.CONTABLE013.OrderByDescending(a => a.COLUMN02).FirstOrDefault();
        //                add1.COLUMN02 = (vid.COLUMN02) + 1;
        //                add1.COLUMN03 = ViewName;
        //                add1.COLUMN04 = Convert.ToInt32(Session["tablid"]);
        //                add1.COLUMN05 = "CONTABLE006";
        //                add1.COLUMN06 = gdata[0];
        //                add1.COLUMN07 = gdata[1];
        //                add1.COLUMN08 = gdata[2];
        //                add1.COLUMN09 = gdata[3];
        //                add1.COLUMN10 = gdata[4];
        //                add1.COLUMN11 = gdata[5];
        //                add1.COLUMN12 = gdata[6];
        //                add1.COLUMN13 = gdata[7];
        //                add1.COLUMN14 = gdata[8];
        //                add1.COLUMN15 = gdata[9];
        //                db.CONTABLE013.Add(add1);
        //                db.SaveChanges();
        //                return this.Json("Data Saveed Successfully", JsonRequestBehavior.AllowGet);
        //                break;
        //            }
        //        }
        //    }
        //    List<CONTABLE006> all = new List<CONTABLE006>();

        //    return this.Json(all);
        //}


        public ActionResult NewField()
        {
            try
            {
                //var table2= Convert.ToString();
                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = Convert.ToInt32(tid.COLUMN02);
                var SecNames = db.CONTABLE006.Where(a => a.COLUMN12 != null && a.COLUMN04 == ttid).ToList();
                var dsecnames = SecNames.Select(a => a.COLUMN12).Distinct();
                ViewBag.SecNames = dsecnames;
                return View();
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }

        [HttpPost]
        public ActionResult NewField(FormCollection fc)
        {
            try
            {
                CONTABLE006 fm = new CONTABLE006();

                var tid = db.CONTABLE004.Where(a => a.COLUMN04 == "CONTABLE009").First();
                var ttid = tid.COLUMN02;
                Session["tablid"] = ttid;
                var query = "SELECT  top( 1 ) * FROM CONTABLE005  WHERE COLUMN03 = " + ttid + "   and  COLUMN04=COLUMN05  ";
                var data = db.CONTABLE005.SqlQuery(query).ToList();
                var tblid = data.Select(a => a.COLUMN03).FirstOrDefault();
                var colname = data.Select(a => a.COLUMN04).FirstOrDefault();
                var alicolname = data.Select(a => a.COLUMN05).FirstOrDefault();
                var con6data = db.CONTABLE006.Where(a => a.COLUMN03 == ttid).FirstOrDefault();
                fm.COLUMN03 = con6data.COLUMN03;
                fm.COLUMN04 = Convert.ToInt32(tblid);
                fm.COLUMN05 = Convert.ToString(colname);
                fm.COLUMN06 = Request["Label_Name"];
                fm.COLUMN07 = "Y";
                fm.COLUMN08 = Request["Mandatory"];
                fm.COLUMN10 = Request["Control_Type"];
                fm.COLUMN11 = Request["Section_Type"];
                fm.COLUMN12 = Request["Section_Name"];
                var pr = con6data.COLUMN13;
                fm.COLUMN13 = (pr + 1).ToString();
                db.CONTABLE006.Add(fm);
                db.SaveChanges();
                return View("Index");
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                Session["MessageFrom"] = msg + " Due to " + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                return RedirectToAction("Index", "Subsidary", new { FormName = Session["FormName"] });
            }
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}