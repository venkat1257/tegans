﻿using eBizSuiteAppModel.Table;
using eBizSuiteUI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eBizSuiteAppUI.Models
{
    public class LOVDetailsModel
    {
        
            public List<MATABLE002> lstD { get; set; }
            public List<MATABLE01> lstM { get; set; }
            [Required(ErrorMessage = "Please select at least one option")]
            [MustBeSelected(ErrorMessage = "Please Select Module")]
            public string COLUMN03 { get; set; }
            [Required(ErrorMessage = "required")]
            public string COLUMN04 { get; set; }
        
    }
}