﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eBizSuiteAppModel.Table;

namespace eBizSuiteUI.Models
{
    public class eBizSuiteEDMModel
    {
        [Required(ErrorMessage = "Please select at least one option")]
        [MustBeSelected(ErrorMessage = "Please Select Module")]
        public string Module_ID { get; set; }
        [Required(ErrorMessage="required")]
        public string Table_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Alias_Table_Name { get; set; }
        public string Table_ID { get; set; }
        public IList<Column_Mapping> lst { get; set; }
        public IList<Column_Mapping1> lstBD { get; set; }
        public List<CONTABLE003> lstMD { get; set; }
    }

    public class Column_Mapping
    {
        [Required(ErrorMessage = "required")]
        public string Column_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Alias_Column_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Data_Type { get; set; }
        [Required(ErrorMessage = "required")]
        public string IsNullable { get; set; }
        [Required(ErrorMessage = "required")]
        public string Size { get; set; }

    }
    public class Column_Mapping1
    {
        [Required(ErrorMessage = "required")]
        public string Column_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Alias_Column_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Data_Type { get; set; }
        [Required(ErrorMessage = "required")]
        public string IsNullable { get; set; }
        [Required(ErrorMessage = "required")]
        public string Size { get; set; }

    }


    public class MustBeSelected : ValidationAttribute, IClientValidatable // IClientValidatable for client side Validation
    {
        public override bool IsValid(object value)
        {
            if (value == null || (int)value == 0)
                return false;
            else
                return true;
        }
        // Implement IClientValidatable for client side Validation
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule { ValidationType = "dropdown", ErrorMessage = this.ErrorMessage } };
        }
    }
}
