﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcMenuMaster.Models
{
    public class MenuItems
    {
       [Required][Display(Name="MenuId")] public int MenuId { get; set; }
       [Required]
       [Display(Name = "MenuName")]
       public string MenuName { get; set; }
       //public string ScreenName { get; set; }
       [Required]
       [Display(Name = "CenterId")] 
       public int CenterId { get; set; }
       [Required]
       [Display(Name = "ModuleId")] 
       public int ModuleId { get; set; }
        public int? Parent { get; set; }
        [Required]
        [Display(Name = "PriorityLevel")] 
        public int PriorityLevel { get; set; }
        [Required]
        [Display(Name = "IsEnable")] 
        public bool IsEnable { get; set; }
        [Required]
        [Display(Name = "IsFormApplicable")]
        public string IsFormApplicable { get; set; }
        public string ParentNode { get; set; }
        public IList<MenuItems> ParentNodeList { get; set; }
        public IList<MenuItems> ParentNodeCode { get; set; }
        public IList<MenuItems> ChildNodeCode { get; set; }
        public IList<MenuItems> ChildNodeList { get; set; }
        public string ChildNode { get; set; }
        public string AParentNode { get; set; }
        public string AChildNode { get; set; }
        public bool Delete { get; set; }
        public bool Enable { get; set; }
        public string NodeType { get; set; }
        public string NodePlace { get; set; }
    }
}