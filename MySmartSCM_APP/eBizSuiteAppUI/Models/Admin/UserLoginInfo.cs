﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBizSuite.Models.Admin
{
    public class UserLoginInfo
    {
        public int UserID { get; set; }
        public DateTimeOffset Date { get; set; }
        public TimeZone Time { get; set; }
        public string IPAdress { get; set; }
        public string Browser { get; set; }
        public string EmailID { get; set; }
        public string Location { get; set; }
    }
}