﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eBizSuite.Models.Admin
{
    public class SecurityAnswer
    {
        public int UserID { get; set; }
        [Required(ErrorMessage = "Please Select Security Question", AllowEmptyStrings = false)]
        [Display(Name = "Security Question 1")]
        public string SeqQue1 { get; set; }
        [Required(ErrorMessage = "Please provide Security Answer", AllowEmptyStrings = false)]
        [Display(Name = "Security Answer 1")]
        public string SecAns1 { get; set; }
        [Display(Name = "Security Question 2")]
         [Required(ErrorMessage = "Please provide Security Question", AllowEmptyStrings = false)]
        public string SeqQue2 { get; set; }
        [Display(Name = "Security Answer 2")]
        [Required(ErrorMessage = "Please provide Security Answer", AllowEmptyStrings = false)]
        public string SecAns2 { get; set; }
        [Display(Name = "Security Question 3")]
         [Required(ErrorMessage = "Please provide Security Question", AllowEmptyStrings = false)]
        public string SeqQue3 { get; set; }
        [Display(Name = "Security Answer 3")]
        [Required(ErrorMessage = "Please provide Security Answer", AllowEmptyStrings = false)]
        public string SecAns3 { get; set; }
    }
}