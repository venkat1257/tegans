﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eBizSuiteUI.Models
{
    public class QueryGeneratorModel
    {
        [Required(ErrorMessage = "Please select at least one option")]
        [MustBeSelected(ErrorMessage = "Please Select Module")]
        public string Module_ID { get; set; }
        [Required(ErrorMessage = "required")]
        public string Table_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Alias_Table_Name { get; set; }
        public string Table_ID { get; set; }
        public IList<Column_Mapping> list { get; set; }
        public IList<Column_Mapping1> lstBD { get; set; }
        public List<CONTABLE003> lstMD { get; set; }
        public List<CONTABLE004> tree { get; set; }

        public List<eBizSuiteAppModel.Table.CONTABLE005> tbl { get; set; }

        public List<CONTABLE0010> lst { get; set; }

        public List<CONTABLE003> lstM { get; set; }
    }
}
