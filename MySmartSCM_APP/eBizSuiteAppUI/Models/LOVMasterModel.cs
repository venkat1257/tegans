﻿using eBizSuiteAppModel.Table;
using eBizSuiteUI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeBizSuiteAppUI.Models
{
    public class LOVMasterModel
    {
        public List<CONTABLE003> lstM { get; set; }
        public List<MATABLE01> lstT { get; set; }
        [Required(ErrorMessage = "Please select at least one option")]
        [MustBeSelected(ErrorMessage = "Please Select Module")]
        public string COLUMN05 { get; set; }
        [Required(ErrorMessage = "required")]
        public string COLUMN03 { get; set; }
        [Required(ErrorMessage = "required")]
        public string COLUMN04 { get; set; }
    }
}