﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBizSuite.Models
{
    public class ModelServices1
    {
        eBizSuiteTableEntities entities = new eBizSuiteTableEntities();
        public IEnumerable<tbl_EmployeeMaster> GetEmployee()
        {
            return entities.tbl_EmployeeMaster.ToList();
        }

        public void Dispose()
        {
            entities.Dispose();
        } 
        public tbl_EmployeeMaster GetEmployeeDetail(int empID)
        {
            
            return entities.tbl_EmployeeMaster.Where(m => m.EmployeeID == empID).FirstOrDefault();
        }

        public bool UpdateEmployee(tbl_EmployeeMaster register)
        {
            try
            {
                tbl_EmployeeMaster sysRegister = entities.tbl_EmployeeMaster.Where(m => m.EmployeeID == register.EmployeeID).FirstOrDefault();

                sysRegister.EmployeeID = register.EmployeeID;
                sysRegister.EmployeeName = register.EmployeeName;
                sysRegister.CompanyName = register.CompanyName;
                sysRegister.TypeOfBusines = register.TypeOfBusines;
                sysRegister.IsActive = Convert.ToString(register.IsActive);
                sysRegister.Email = register.Email;
                sysRegister.Password = register.Password;
                //if (register.Password == true)
                //{
                //    sysRegister.Password = "eBizSuite";
                //}
                //else { sysRegister.Password = "NULL"; }

                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public bool DeleteEmployee(int mCustID)
        {
            try
            {
                tbl_EmployeeMaster data = entities.tbl_EmployeeMaster.Where(m => m.EmployeeID == mCustID).FirstOrDefault();
                entities.tbl_EmployeeMaster.Remove(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public void xmlconver()
        {
        }

        


    }
}