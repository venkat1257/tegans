﻿using eBizSuite.Models.User;
using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBizSuite.Models.UserManage
{
    public class EmployeMasterManage
    {
        public eBizSuiteTableEntities dbRegister = new eBizSuiteTableEntities();
        public void Add(EmployeMaster register)
        {
            tbl_EmployeeMaster sysRegister = new tbl_EmployeeMaster();
            //sysRegister.UserID = register.UserID;
            sysRegister.EmployeeID = register.EmployeeID;
            sysRegister.EmployeeName = register.EmployeeName;
            sysRegister.CompanyName = register.CompanyName;
            sysRegister.TypeOfBusines = register.TypeOfBusines;
            sysRegister.IsActive = Convert.ToString(register.IsActive);
            sysRegister.Email = register.Email;
            if (register.Password == true)
            {
                sysRegister.Password = "eBizSuite";
            }
            else { sysRegister.Password = "NULL"; }
            
                
              
            dbRegister.tbl_EmployeeMaster.Add(sysRegister);
            dbRegister.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in dbRegister.tbl_EmployeeMaster where o.EmployeeID == userId select o).Any();
        }
    }
}