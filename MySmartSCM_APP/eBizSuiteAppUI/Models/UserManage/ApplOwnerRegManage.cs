﻿using eBizSuite.Models.User;
using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace eBizSuite.Models.UserManage
{
    public class ApplOwnerRegManage
    {
        public eBizSuiteTableEntities dbRegister = new eBizSuiteTableEntities();
        public void Add(ApplicationOwnerReg register)
        {
            tbl_AppOwnerMaster sysRegister = new tbl_AppOwnerMaster();
            sysRegister.ApplicationOwnerId = register.ApplicationOwnerId;
            sysRegister.AppOwnerUserName = register.AppOwnerUserName;
            sysRegister.CompanyName = register.CompanyName;
            sysRegister.TypeOfBusines = register.TypeOfBusines;
            sysRegister.Address1 = register.Address1;
            sysRegister.Address2 = register.Address2;
            sysRegister.City = register.City;
            sysRegister.State = register.State;
            sysRegister.ZipCode = register.ZipCode;
            sysRegister.Country = register.Country;
            sysRegister.DetailsNeedFor = register.DetailsNeedFor;
            sysRegister.EmailId = register.Email;
            sysRegister.IsActive = register.IsActive;
            sysRegister.Password = register.Password;

            dbRegister.tbl_AppOwnerMaster.Add(sysRegister);
            dbRegister.SaveChanges();

            
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in dbRegister.tbl_AppOwnerMaster where o.ApplicationOwnerId == userId select o).Any();
        }
    }
}