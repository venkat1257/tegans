﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eBizSuiteUI.Models
{
    //[MetadataType(typeof(tbl_AppOwnerMaster))]
    public class AppOwnervsMenuModel
    {
        public List<CONTABLE002> AppOwner { get; set; }

        public List<CONTABLE003> Menu { get; set; }

        public int AppOwnerID { get; set; }

        public int MenuId { get; set; }

        public Nullable<int> CenterId { get; set; }

        public string MenuName { get; set; }

        public Nullable<int> ModuleId { get; set; }

        public Nullable<int> Parent { get; set; }

        public Nullable<int> PriorityLevel { get; set; }

        public string IsEnable { get; set; }

        public string IsFormApplicable { get; set; }

        public string ScreenName { get; set; }

        public Nullable<int> Order { get; set; }

        public int ApplicationOwnerId { get; set; }

        public string AppOwnerUserName { get; set; }

        public string CompanyName { get; set; }

        public string TypeOfBusines { get; set; }

        public string DetailsNeedFor { get; set; }

        public string EmailId { get; set; }

        public string Password { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public Nullable<int> ZipCode { get; set; }

        public string Country { get; set; }

        public Nullable<bool> IsActive { get; set; }
    }
}