﻿using eBizSuite.Models.Admin;
using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBizSuite.Models.AdminManage
{
    public class SesurityAnswerManage
    {
        public eBizSuiteTableEntities se = new eBizSuiteTableEntities();

        public void Add(SecurityAnswer sa)
        {
            //eBizSuiteDBEntitiesAdminRegister id = new eBizSuiteDBEntitiesAdminRegister();
            //int obj = id.tblAdminRegisters.Single(b => b.UserID);

            tbl_AdminSecurityAnswer dbSecAnd = new tbl_AdminSecurityAnswer();
            dbSecAnd.EmployeeID = sa.UserID;
            dbSecAnd.SeqQue1 = sa.SeqQue1;
            dbSecAnd.SecAns1 = sa.SecAns1;
            dbSecAnd.SecAns2 = sa.SecAns2;
            dbSecAnd.SeqQue2 = sa.SeqQue2;
            dbSecAnd.SeqQue3 = sa.SeqQue3;
            dbSecAnd.SecAns3 = sa.SecAns3;
            se.tbl_AdminSecurityAnswer.Add(dbSecAnd);
            se.SaveChanges();
        }
        public bool IsUserLoginIDExist(int userId1)
        {
            return (from o in se.tbl_AdminSecurityAnswer where o.EmployeeID == userId1 select o).Any();
        }
    }
}