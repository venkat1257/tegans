﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eBizSuite.Models.Admin;
using eBizSuiteAppModel.Table;
using System.Data;
using System.Data.SqlClient;

namespace eBizSuite.Models.AdminManage
{
    public class RegisterManage
    {
        public eBizSuiteTableEntities dbUser = new eBizSuiteTableEntities();
        public eBizSuiteTableEntities dbRegister = new eBizSuiteTableEntities();
        public eBizSuiteTableEntities dbSecAns = new eBizSuiteTableEntities();
        public void Add(Register register)
        {
            
            tbl_AdminRegistration sysRegister = new tbl_AdminRegistration();
            sysRegister.UserID = register.UserID;
            sysRegister.FullName = register.FullName;
            sysRegister.UserName = register.UserName;
            sysRegister.Password = register.Password;
            sysRegister.EmailID = register.EmailID;


            tbl_AdminLogOn sysUser = new tbl_AdminLogOn();
            sysUser.UserID = register.UserID;
            sysUser.UserName = register.UserName;
            sysUser.Password = register.Password;

            dbRegister.tbl_AdminRegistration.Add(sysRegister);
            dbRegister.SaveChanges();
           
            dbUser.tbl_AdminLogOn.Add(sysUser);
            dbUser.SaveChanges();


        }
        //public void Add(Register userReg)
        //{
        //    tblAdminUser sysUser = new tblAdminUser();
        //    sysUser.UserName = userReg.UserName;
        //    sysUser.Password = userReg.Password;

        //}

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in dbRegister.tbl_AdminLogOn where o.UserID == userId select o).Any();
        }
    }
}