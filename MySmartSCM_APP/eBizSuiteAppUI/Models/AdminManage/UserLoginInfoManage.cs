﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace eBizSuite.Models.AdminManage
{
    public class UserLoginInfoManage : Controller
    {
        public eBizSuiteTableEntities dbe = new eBizSuiteTableEntities();
        public void Add(int userId)
        {
            tbl_AdminLoginInformation sysLogInfo = new tbl_AdminLoginInformation();

            sysLogInfo.UserID =userId;

            sysLogInfo.Date = DateTime.Now.Date;

            sysLogInfo.Time = DateTime.Now.TimeOfDay;

            string strHost = "";
            strHost = System.Net.Dns.GetHostName();
            IPHostEntry ipAdd = System.Net.Dns.GetHostEntry(strHost);
            IPAddress[] addr = ipAdd.AddressList;
            sysLogInfo.IPAddress = addr[addr.Length - 2].ToString();

            //string strHostName = System.Net.Dns.GetHostName();
            //IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            //string ipAddress = Convert.ToString(ipEntry.AddressList[2]);


            //HttpBrowserCapabilitiesBase obj=Request.Browser ;

            //System.Net.Http.HttpRequestMessage currentRequest = Request.Browser;
            //System.Net.Http.Headers.HttpHeaderValueCollection<System.Net.Http.Headers.ProductInfoHeaderValue> userAgentHeader = currentRequest.Headers.UserAgent;


            sysLogInfo.Browser = "";

            sysLogInfo.EmailID = "email";
            sysLogInfo.Location = "India";
            dbe.tbl_AdminLoginInformation.Add(sysLogInfo);
            dbe.SaveChanges();
        }
    }
}