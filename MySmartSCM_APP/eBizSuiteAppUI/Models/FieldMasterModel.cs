﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TreeViewSample.Models;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace eBizSuiteUI.Models
{
    //[MetadataType(typeof(CONTABLE004))]
    public class FieldMasterModel
    {

        public List<CONTABLE004> tree { get; set; }

        public List<MATABLE01> Source { get; set; }

        public List<CONTABLE004> Source_Master { get; set; }

        public List<eBizSuiteAppModel.Table.CONTABLE005> tbl { get; set; }

        public List<DataTable> tblDT { get; set; }

        public List<CONTABLE0010> lst { get; set; }

        public List<CONTABLE003> lstM { get; set; }

        public int COLUMN01 { get; set; }
        [Required(ErrorMessage = "required")]
        public Nullable<int> COLUMN03 { get; set; }
        [Required(ErrorMessage = "required")]
        public string COLUMN04 { get; set; }
        [Required(ErrorMessage = "required")]
        public string COLUMN05 { get; set; }

        [Required(ErrorMessage = "required")]
        public string Column_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Alias_Column_Name { get; set; }
        [Required(ErrorMessage = "required")]
        public string Data_Type { get; set; }
        public Nullable<bool> IsNullable { get; set; }
        public string Size { get; set; }

        [Required(ErrorMessage = "required")]
        public string Control_Type { get; set; }
        [Required(ErrorMessage = "required")]
        public string Section_Type { get; set; }
        [Required(ErrorMessage = "required")]
        public string Section_Name { get; set; }

        public Nullable<int> COLUMNA01 { get; set; }
        public Nullable<int> COLUMNA02 { get; set; }
        public Nullable<int> COLUMNA03 { get; set; }
        public Nullable<int> COLUMNA04 { get; set; }
        public Nullable<int> COLUMNA05 { get; set; }
        public Nullable<System.DateTime> COLUMNA06 { get; set; }
        public Nullable<System.DateTime> COLUMNA07 { get; set; }
        public Nullable<int> COLUMNA08 { get; set; }
        public Nullable<System.DateTime> COLUMNA09 { get; set; }
        public Nullable<System.DateTime> COLUMNA10 { get; set; }
        public Nullable<int> COLUMNA11 { get; set; }
        public Nullable<bool> COLUMNA12 { get; set; }
        public Nullable<bool> COLUMNA13 { get; set; }

        public virtual ICollection<Column_Mapping> Column_Mapping { get; set; }
        public virtual ICollection<Field_Master> Field_Master { get; set; }
    }
}