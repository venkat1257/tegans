﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eBizSuite.Models.User
{
    public class ModelServices
    {
        public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();
        public IEnumerable GetRoleMaster()
        {
            return entities.tbl_RoleMaster.ToList();
        }
        
    }
}