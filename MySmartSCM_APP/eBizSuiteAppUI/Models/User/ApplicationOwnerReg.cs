﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebMatrix.Data;

namespace eBizSuite.Models.User
{
    public class ApplicationOwnerReg
    {
        public int ApplicationOwnerId { get; set; }


        [Required(ErrorMessage = "Please provide AppOwnerUserName", AllowEmptyStrings = false)]
        public string AppOwnerUserName { get; set; }

        [Required(ErrorMessage = "Please provide CompanyName", AllowEmptyStrings = false)]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Please provide TypeOfBusines", AllowEmptyStrings = false)]
        public string TypeOfBusines { get; set; }

        [Required(ErrorMessage = "Please provide Address1", AllowEmptyStrings = false)]
        public string Address1 { get; set; }
        [Required(ErrorMessage = "Please provide Address2", AllowEmptyStrings = false)]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "Please provide City", AllowEmptyStrings = false)]
        public string City { get; set; }
        [Required(ErrorMessage = "Please provide State", AllowEmptyStrings = false)]
        public string State { get; set; }

        [DataType(System.ComponentModel.DataAnnotations.DataType.PhoneNumber)]
        [Required(ErrorMessage = "Please provide ZipCode")]
        public int ZipCode { get; set; }

        [Required(ErrorMessage = "Please provide Country", AllowEmptyStrings = false)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Please provide DetailsNeedFor", AllowEmptyStrings = false)]
        public string DetailsNeedFor { get; set; }

        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,3})$",
        ErrorMessage = "Please provide valid email id")]
        public string Email { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Please provide Password", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Password must be 8 char long.")]
        public string Password { get; set; }
    }
}