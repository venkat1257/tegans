﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace eBizSuite.Models.User
{
    public class EmployeMaster
    {
        public int EmployeeID { get; set; }

        [Required(ErrorMessage = "Please provide EmployeeName", AllowEmptyStrings = false)]
        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "Please provide CompanyName", AllowEmptyStrings = false)]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Please provide TypeOfBusines", AllowEmptyStrings = false)]
        public string TypeOfBusines { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Please provide Email ID", AllowEmptyStrings = false)]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,3})$",
        ErrorMessage = "Please provide valid email id")]
        public string Email { get; set; }

        //[Required(ErrorMessage = "Please provide Password", AllowEmptyStrings = false)]
        //[DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        //[StringLength(50, MinimumLength = 8, ErrorMessage = "Password must be 8 char long.")]
        [Display(Name="Generate password ?")]
        public bool Password { get; set; }

        
    }


    public class DisplayData
    {
        /// <summary>
        /// Search records from database.
        /// </summary>
        /// <param name="keywords">the list of keywords</param>
        /// <returns>all found records</returns>
        public List<tbl_EmployeeMaster> Search(List<string> keywords)
        {
            // Generate a complex Sql command.
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.Append("select * from [tblEmployeMaster] where ");
            foreach (string item in keywords)
            {
                sqlBuilder.AppendFormat("([EmployeeID] like '%{0}%' or [EmployeeName] like '%{0}%' or [CompanyName] like '%{0}%') and ", item);
            }

            // Remove unnecessary string " and " at the end of the command.
            string sql = sqlBuilder.ToString(0, sqlBuilder.Length - 4);

            return QueryList(sql);
        }

        /// <summary>
        /// Excute a Sql command.
        /// </summary>
        /// <param name="cmdText">Command text</param>
        /// <returns></returns>
        protected List<tbl_EmployeeMaster> QueryList(string cmdText)
        {
            List<tbl_EmployeeMaster> ctn = new List<tbl_EmployeeMaster>();

            SqlCommand cmd = GenerateSqlCommand(cmdText);
            using (cmd.Connection)
            {
                SqlDataReader reader = cmd.ExecuteReader();

                // Transform records to a list.
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ctn.Add(ReadValue(reader));
                    }
                }
            }
            return ctn;
        }

        /// <summary>
        /// Create a connected SqlCommand object.
        /// </summary>
        /// <param name="cmdText">Command text</param>
        /// <returns>SqlCommand object</returns>
        protected SqlCommand GenerateSqlCommand(string cmdText)
        {
            // Read Connection String from web.config file.
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            SqlCommand cmd = new SqlCommand(cmdText, con);
            cmd.Connection.Open();
            return cmd;
        }

        /// <summary>
        /// Create an Content object from a SqlDataReader object.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected tbl_EmployeeMaster ReadValue(SqlDataReader reader)
        {
            tbl_EmployeeMaster obj = new tbl_EmployeeMaster();

            obj.EmployeeID = (int)reader["EmployeeID"];
            obj.EmployeeName = (string)reader["EmployeeName"];
            obj.CompanyName = (string)reader["CompanyName"];

            return obj;
        }
    }
}