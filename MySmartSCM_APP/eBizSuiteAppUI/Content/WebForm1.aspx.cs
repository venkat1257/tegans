﻿using eBizSuiteAppModel.Table;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Content
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities(); string color = "Blue";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Write("<script type=text/javascript> window.parent.location.href ='/EmployeeMaster/Info';</script>");
                //Response.Redirect("http://localhost:5028/EmployeeMaster/Info");
            }
            try
            {
                if (!IsPostBack)
                {
                    int? eID = Convert.ToInt32(Session["eid"]);
                    var colortype = dbContext.MYTABLE001.Where(a => a.COLUMN09 == eID).FirstOrDefault();
                    if (colortype != null)
                    { color = colortype.COLUMN06; }
                    string OPUnit = Session["OPUnit"].ToString();
                    int OPUnitStatus = 1;
                    if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                    {
                        OPUnit = OPUnit.Replace(",null", ",");
                        OPUnit = OPUnit.TrimEnd(',');
                        OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                    }
                    if (Request.QueryString["ReportName"] == "TransactionReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/TransactionReport.rdlc";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "TransactionReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ARAgeing")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ARAgeing.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ARAgeingDetailes")
                    {
                        //string FDate = Request.QueryString["FDate"];
                        //string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Type = Request.QueryString["sales"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ARAgeingDetailes.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        //cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@SalesRep", Type);

                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                    else if (Request.QueryString["ReportName"] == "ARAgeingNew")
                    {
                        //string FDate = Request.QueryString["FDate"];
                        //string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1706 InventoyByUOM and AR Aging Repot Chages ading Location And Sales rep by GNANESHWAR ON 22/4/2016
                        string Type = Request.QueryString["sales"];
                        //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                        if (OperatingUnit != "" && OperatingUnit != null) OPUnit = OperatingUnit;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ARAgeingNew.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                        SqlCommand cmd = new SqlCommand("AgingREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        //cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@SalesRep", Type);
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1615 rajasekhar reddy patakota 16/03/2016 ARAging Details Report Changes for rk
                    else if (Request.QueryString["ReportName"] == "ARAgeingDetailesNew")
                    {
                        //string FDate = Request.QueryString["FDate"];
                        //string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Type = Request.QueryString["sales"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ARAgeingDetailesNew.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ARAgingDetailsREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        //cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@SalesRep", Type);

                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "APAgeing")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/APAgeing.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "GeneralLedger")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1332	Adding type,Account filter in General Ledger Report BY RAj.Jr 9/11/2015
                        string Type = Request.QueryString["Type"];
                        string Account = Request.QueryString["Account"];
                        string Project = Request.QueryString["Project"];
                        string Vendor = Request.QueryString["Vendor"];
                        string Customer = Request.QueryString["Customer"];
                        //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET,General ledger COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/GeneralLedger.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GeneralLedger", con);
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        //EMPHCS1332	Adding type,Account filter in General Ledger Report BY RAj.Jr 9/11/2015
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Account", Account);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "TrialBalance")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/TrialBalance.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@ProjectName", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ProfitAndLoss")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProfitAndLoss.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@ProjectName", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1285 gnaneshwar on 7/10/2015  Creating Profitandloss By Project Report
                    else if (Request.QueryString["ReportName"] == "ProfitAndLossByProject")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string ProjectName = Request.QueryString["ProjectName"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProfitAndLossByProject.rdl";
                        //EMPHCS1828 : Profit And Loss By Project  Adding Links and Project Filter to related records by  gnaneshwar on 1/11/2016
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@ProjectName", ProjectName);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        //EMPHCS1828 : Profit And Loss By Project  Adding Links and Project Filter to related records by  gnaneshwar on 1/11/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "VatInfo")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/VatInfo.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@ProjectName", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "BalanceSheet")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.ProcessingMode = ProcessingMode.Local;
                        ReportViewer1.LocalReport.ReportPath = "Content/BalanceSheet.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@ProjectName", Project);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CommissionPaymentandDues")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Customer"];
                        string Type = Request.QueryString["sales"];
                        //EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                        string Category = Request.QueryString["Category"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CommissionPaymentDues.rdl";

                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_SAL_COMMISSION_PAYMENT_DUES", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CUSTOMER", Vendor);
                        cmd.Parameters.AddWithValue("@FRDATE", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@TODATE", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@SALESREP", Type);
                        cmd.Parameters.AddWithValue("@OPERATING", OperatingUnit);
                        //EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                        cmd.Parameters.AddWithValue("@Category", Category);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CommissionDues")
                    {
                        string FDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CommissionDues.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_SAL_COMMISSION_DUES", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ASONDATE", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CommissionPaymentDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Customer"];
                        string Type = Request.QueryString["sales"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CommissionPayment.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_SAL_COMMISSION_PAYMENT_DETAILS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CUSTOMER", Vendor);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@SALESREP", Type);
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CustomerVsSalesRep")
                    {
                        string Vendor = Request.QueryString["Customer"];
                        string Type = Request.QueryString["sales"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CustomerVsSalesRepCommission.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_SAL_CUSTOMER_SALESREP_COMMISSION", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CUSTOMER", Vendor);
                        cmd.Parameters.AddWithValue("@SALESREP", Type);
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1338	Rename Stock Details as Stock Ledger BY RAJ.Jr
                    else if (Request.QueryString["ReportName"] == "StockLedger")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //string Vendor = Request.QueryString["Vendor"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        //string Project = Request.QueryString["Project"];
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockLedger.rdl";
                        //EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGER", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", "");
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        cmd.Parameters.AddWithValue("@Location", Location);
                        //cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "StockDetailes")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //string Vendor = Request.QueryString["Vendor"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        //string Project = Request.QueryString["Project"];
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockDetailes.rdl";
                        //EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGER", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", "");
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        cmd.Parameters.AddWithValue("@Location", Location);
                        //cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ResourseConsumptions")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string IFamily = Request.QueryString["IFamily"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string cust = Request.QueryString["cust"];
                        string Type = Request.QueryString["Type"];
                        string Project = Request.QueryString["Project"];
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ResourseConsumptions.rdl";
                        //EMPHCS1660 : Stock Ledger and Inventory By Item reports in inventory  COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 7/4/2016
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_ResourseConsumption", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ResourseConsumptions");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        //cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@IFamily", IFamily);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Customer", cust);
                        //cmd.Parameters.AddWithValue("@Brand", Brand);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        // cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1791	Resource Consumption prints and Reports By Raj.Jr
                    else if (Request.QueryString["ReportName"] == "ResourceConsumptionSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ResourceConsumptionSummary.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_ResourseConsumptionSummary", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ResourceConsumptionSummary");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1796 Production Entry Screen and Cost of Production Report Changes by gnaneshwar on 10/8/2016
                    else if (Request.QueryString["ReportName"] == "ResourceConsumptionConsolidate")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ResourceConsumptionConsolidate.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_ResourceConsumptionConsolidate", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ResourceConsumptionConsolidate");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1302 BY GNANESHWAR ON 14/10/2015 Craeting Report InvertoryByItem
                    else if (Request.QueryString["ReportName"] == "InventoryByItem")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //string Vendor = Request.QueryString["Vendor"];
                        //string Type = Request.QueryString["Type"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InventoryByItem.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "InventoryByItem");
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "projectPayableReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Vendor"];
                        //string Type = Request.QueryString["Type"];
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/Accountspayable-Location.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ProjectPayableReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "projectPayableDueDates")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Vendor"];
                        //string Type = Request.QueryString["Type"];
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/AccountspayableDueDates.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "projectPayableDueDates");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "projectReceivableReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        //string Type = Request.QueryString["Type"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProjectAccountReceivable.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "projectReceivableReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ProjectReceivableDueDates")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        //string Type = Request.QueryString["Type"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProjectAccountReceivableDueDates.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ProjectReceivableDueDates");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "projectBankRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //string Customer = Request.QueryString["Customer"];
                        //string Type = Request.QueryString["Type"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProjectBankReport.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "projectBankRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@Customer", Customer);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "projectExpenses")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //string Customer = Request.QueryString["Customer"];
                        //string Type = Request.QueryString["Type"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProjectExpenses.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "projectExpenses");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@Customer", Customer);
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PayableReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Vendor"];
                        string Type = Request.QueryString["Type"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/AccountPayable.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReportsAPAR", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PayableReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@Project", Project);
                        //EMPHCS1476 EMPHCS1477 EMPHCS1478 Add Advances to Account Payable By Srinivas
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"]);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ReceivableReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        string Type = Request.QueryString["Type"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ReceivableAccountReport.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReportsAPAR", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ReceivableReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@Project", Project);

                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PaymentReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PaymentReport.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PaymentReport");
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@Project", Project);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("PaymentDataSet", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "DayReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/DayReport.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("DayReport", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "EmployeeReport")
                    {
                        //string Commission = Request.QueryString["Commission"];
                        string SalesRef = Request.QueryString["SalesRef"];
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/EmployeeReport.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetCommissionReport", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@Commission", Commission);
                        cmd.Parameters.AddWithValue("@SalesRef", SalesRef);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PurchaseOrderSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Vendor = Request.QueryString["Vendor"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/POSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        //ReportViewer1.Reset();  
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Sales Order Summary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Customer = Request.QueryString["Customer"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SOSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SOSummary");
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "JobberIN")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Customer = Request.QueryString["Customer"];
                        string Item = Request.QueryString["Item"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/JobberIN.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_JobberIN", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "JobberIN");
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "VendorPayment")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Vendor = Request.QueryString["Vendor"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/VendorPayment.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "VendorPayment");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CustomerPayment")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Customer = Request.QueryString["Customer"];
                        string SalesRep = Request.QueryString["SalesRep"];
                        if (OperatingUnit == "undefined")
                            OperatingUnit = "";
                        if (Customer == "undefined")
                            Customer = "";
                        if (SalesRep == "undefined")
                            SalesRep = "";
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CustomerPayment.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "CustomerPayment");
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@SalesRep", SalesRep);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "POByItem")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/POByItem.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "POByItem");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SOByItem")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SOByItem.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SOByItem");
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "POHistory")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Vendor = Request.QueryString["Vendor"];
                        string Status = Request.QueryString["Status"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/POHistory.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "POHistory");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SOHistory")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Customer = Request.QueryString["Customer"];
                        string Status = Request.QueryString["Status"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SOHistory.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SOHistory");
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ItemOrderDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Item = Request.QueryString["Item"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ItemOrderDetails.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ItemOrderDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ItemFulfilment")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Item = Request.QueryString["Item"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ItemFulfilment.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "ItemFulfilment");
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "VendorPaymentDues")
                    {
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Vendor = Request.QueryString["Vendor"];
                        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];

                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/VendorPaymentDues.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                        SqlCommand cmd = new SqlCommand("CVPaymentDues", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "VendorPaymentDues");
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);

                        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CustomerPaymentDues")
                    {
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Customer = Request.QueryString["Customer"];
                        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CustomerPaymentDues.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                        SqlCommand cmd = new SqlCommand("CVPaymentDues", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "CustomerPaymentDues");
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "JobberPayment")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Jobber = Request.QueryString["Jobber"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/JobberPayment.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "JobberPayment");
                        cmd.Parameters.AddWithValue("@Jobber", Jobber);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "StockOfJobber")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Jobber = Request.QueryString["Jobber"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockOfJobber.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockOfJobber");
                        cmd.Parameters.AddWithValue("@Jobber", Jobber);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        //cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "InventoryByLoc")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Item = Request.QueryString["Item"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InventoryByLoc.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "InventoryByLoc");
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        //cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        cmd.CommandTimeout = 0;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "JobberPaymentDues")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Jobber = Request.QueryString["Jobber"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/JobberPaymentDues.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "JobberPaymentDues");
                        cmd.Parameters.AddWithValue("@Jobber", Jobber);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        //cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "Top10Vendors")
                    {
                        string OPUnitWithNull = " s1.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " s1.COLUMNA02 in(" + Session["OPUnit"] + ") or s1.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/TopVendors.rdl";
                        string strqrt = "SELECT s1.COLUMN04, s1.COLUMN05, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE001 AS s1 INNER JOIN PUTABLE001 AS s2 ON s1.COLUMN02 = s2.COLUMN05  where " + OPUnitWithNull + " AND s1.COLUMNA03='" + Session["AcOwner"] + "' GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "Top10Vendors");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "VendorRegister")
                    {
                        string OPUnitWithNull = " p.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null";
                        string Vendor = Request.QueryString["Vendor"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/VendorRegister.rdlc";
                        string strqrt = "SELECT p.COLUMN04 as COLUMN04 , p.COLUMN05,p. COLUMN06, d.column04 as COLUMN07, f.COLUMN04 as COLUMN08, COLUMN11, COLUMN12 FROM SATABLE001 p left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN07 left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN08  where " + OPUnitWithNull + " AND p.COLUMNA03='" + Session["AcOwner"] + "'";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "VendorRegister");
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PendingBills")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Vendor"];
                        string BillNO = Request.QueryString["BillNO"];
                        string Status = Request.QueryString["Status"];

                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PendingBills.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PendingBills");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@BillNO", BillNO);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PendingPurchaseOrders")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Vendor"];
                        string PONO = Request.QueryString["PONO"];
                        string Status = Request.QueryString["Status"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PendingPO.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        //string strqrt = "SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM PUTABLE001 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN16 <> 'CLOSE')";
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PendingPO");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@PONO", PONO);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Top10Customers")
                    {
                        string OPUnitWithNull = " s1.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " s1.COLUMNA02 in(" + Session["OPUnit"] + ") or s1.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/TopCustomers.rdl";
                        string strqrt = "SELECT s1.COLUMN04, s1.COLUMN05, SUM(s2.COLUMN15) AS Expr1 FROM SATABLE002 AS s1 INNER JOIN SATABLE005 AS s2 ON s1.COLUMN02 = s2.COLUMN05  where " + OPUnitWithNull + " AND s1.COLUMNA03='" + Session["AcOwner"] + "' GROUP BY s1.COLUMN04, s1.COLUMN05 ORDER BY Expr1 DESC";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "Top10Customers");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CustomerRegister")
                    {
                        string OPUnitWithNull = " p.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " p.COLUMNA02 in(" + Session["OPUnit"] + ") or p.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CustomerRegister.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT p.COLUMN04, p.COLUMN05, d.COLUMN04 AS COLUMN07, f.COLUMN04 AS COLUMN08, p.COLUMN10, p.COLUMN11 FROM SATABLE002 AS p LEFT OUTER JOIN MATABLE002 AS d ON d.COLUMN02 = p.COLUMN07 LEFT OUTER JOIN MATABLE002 AS f ON f.COLUMN02 = p.COLUMN08  where " + OPUnitWithNull + " AND p.COLUMNA03='" + Session["AcOwner"] + "'";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PendingInvoices")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        string BillNO = Request.QueryString["BillNO"];
                        string Status = Request.QueryString["Status"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PendingInvoices.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        //string strqrt = "SELECT p.COLUMN04, f.COLUMN05, p.COLUMN06, p.COLUMN09, p.COLUMN10, p.COLUMN12, p.COLUMN13 FROM SATABLE009 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE(p.COLUMN13 <> 'CLOSE') and p.column04 like'IV%' AND (p.COLUMN13 <> 'AMOUNT FULLY RECEIVED')";
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PendingInvoices");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@BillNO", BillNO);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PendingSalesOrders")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        string SONO = Request.QueryString["SONO"];
                        string Status = Request.QueryString["Status"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PendingSO.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        // string strqrt = "SELECT p.COLUMN04, p.COLUMN06, f.COLUMN05, p.COLUMN08, p.COLUMN09, p.COLUMN11, p.COLUMN15, p.COLUMN16 FROM SATABLE005 p left outer join  SATABLE001 f on f.COLUMN02=p.COLUMN05 WHERE (p.COLUMN16 <> 'CLOSE') and p.column04 like'S%'";
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PendingSalesOrders");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@SONO", SONO);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                    else if (Request.QueryString["ReportName"] == "GetJobOrderProcessingDetails")
                    {
                        string Joborder = Request.QueryString["Joborder"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/JobOrderProcessing.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetJobOrderProcessingDetails", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Joborder", Joborder);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1339	Reame Stock Ledger as Current Inventory BY RAj.Jr
                    else if (Request.QueryString["ReportName"] == "CurrentInventory")
                    {
                        string Item = Request.QueryString["Item"];
                        string UPC = Request.QueryString["UPC"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        string Location = Request.QueryString["Location"];
                        string reorder = Request.QueryString["reorder"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CurrentInventory.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        //string strqrt = "select COLUMN03, (select column04 from MATABLE007 where COLUMN02=FITABLE010.COLUMN03) Item,(select COLUMN06 from MATABLE007 where COLUMN02=FITABLE010.COLUMN03) COLUMN10,COLUMN04 ,COLUMN05 ,COLUMN06 ,COLUMN07 ,COLUMN08  ,COLUMN09  ,COLUMN12  from FITABLE010 order by COLUMN03 desc";
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_CURRENTINVENTORY", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        cmd.Parameters.AddWithValue("@Location", Location);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        cmd.Parameters.AddWithValue("@reorder", reorder);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1848 : Creating Inventory report and stock register with out price by gnaneshwar on 30/12/2016
                    else if (Request.QueryString["ReportName"] == "InventoryReport")
                    {
                        string Item = Request.QueryString["Item"];
                        string UPC = Request.QueryString["UPC"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        string Location = Request.QueryString["Location"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InventoryReport.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        //string strqrt = "select COLUMN03, (select column04 from MATABLE007 where COLUMN02=FITABLE010.COLUMN03) Item,(select COLUMN06 from MATABLE007 where COLUMN02=FITABLE010.COLUMN03) COLUMN10,COLUMN04 ,COLUMN05 ,COLUMN06 ,COLUMN07 ,COLUMN08  ,COLUMN09  ,COLUMN12  from FITABLE010 order by COLUMN03 desc";
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_CURRENTINVENTORY", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1712 rajasekhar reddy patakota 26/04/2016 Add Default Locaion in all Screens
                        cmd.Parameters.AddWithValue("@Location", Location);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "AccountsLedger")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/AccountsLedger.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT a.COLUMN04, a.COLUMN05,(SELECT COLUMN04 from FITABLE001 where column02=a.COLUMN06)COLUMN06,(SELECT COLUMN04 from matable002 where column02=a.COLUMN07) COLUMN07, a.COLUMN08, a.COLUMN09, (select sum( column10) from putable019 where column03 = a.column02) payable, (select sum( column11) from putable019 where column03 = a.column02) receible FROM FITABLE001 a  where column09 between  @FromDate  and @ToDate  and " + Session["OPUnitWithNull"] + " AND COLUMNA03='" + Session["AcOwner"] + "'and COLUMN07=@Type";
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "AccountsLedger");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "JobberReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Jobber"];
                        string Type = Request.QueryString["JNO"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/JobberReport.rdlc";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("[usp_SAL_REPORT_PENDINGJO]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PayableReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Jobber", Vendor);
                        cmd.Parameters.AddWithValue("@JNO", Type);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "BankRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Bank"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        //EMPHCS1214	Bank register balance must be cumulative and also operating unit must be added . RECEIPT must be added BY RAJ.Jr 30/9/2015
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/BankRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("[usp_FIN_REPORT_BANKREGISTER]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "BankRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Bank", Vendor);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@Project", Project);
                        //EMPHCS1214	Bank register balance must be cumulative and also operating unit must be added . RECEIPT must be added BY RAJ.Jr 30/9/2015
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "Purchase Order Report")
                    {
                        string OPUnitWithNull = " PUTABLE001.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " PUTABLE001.COLUMNA02 in(" + Session["OPUnit"] + ") or PUTABLE001.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string DateF = Session["FormatDate"].ToString();
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PO.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT PUTABLE001.COLUMN04 AS Pono, FORMAT(PUTABLE001.COLUMN06,'" + DateF + "') AS date, s.COLUMN05 AS vendor, PUTABLE001.COLUMN08 AS shipdate,PUTABLE001.COLUMN09 AS note, PUTABLE001.COLUMN11 AS reference, PUTABLE001.COLUMN15 AS totalamount, PUTABLE001.COLUMN16 AS status,i.COLUMN04 AS item, PUTABLE002.COLUMN04, PUTABLE002.COLUMN07, PUTABLE002.COLUMN09, PUTABLE002.COLUMN10, PUTABLE002.COLUMN11,PUTABLE002.COLUMN07 AS Expr1, PUTABLE002.COLUMN12, PUTABLE002.COLUMN13, PUTABLE002.COLUMN18 FROM PUTABLE001 INNER JOIN PUTABLE002 ON PUTABLE001.COLUMN01 = PUTABLE002.COLUMN19 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = PUTABLE001.COLUMN05 LEFT OUTER JOIN MATABLE007 AS i ON i.COLUMN02 = PUTABLE002.COLUMN03   where " + OPUnitWithNull + " AND PUTABLE001.COLUMNA03='" + Session["AcOwner"] + "' order by  PUTABLE001.COLUMN06	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Item Receipt Report")
                    {
                        string OPUnitWithNull = " PUTABLE003.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " PUTABLE003.COLUMNA02 in(" + Session["OPUnit"] + ") or PUTABLE003.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string DateF = Session["FormatDate"].ToString();
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/IR.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT        PUTABLE003.COLUMN04, FORMAT(PUTABLE003.COLUMN09,'" + DateF + "'), s.COLUMN05 AS vendor, PUTABLE003.COLUMN06, PUTABLE003.COLUMN10, PUTABLE003.COLUMN12, i.COLUMN04 AS item, PUTABLE004.COLUMN06 AS Expr1, PUTABLE004.COLUMN07, PUTABLE004.COLUMN09 AS Expr2, PUTABLE004.COLUMN10 AS Expr3,PUTABLE004.COLUMN11 FROM PUTABLE003 INNER JOIN PUTABLE004 ON PUTABLE003.COLUMN01 = PUTABLE004.COLUMN12 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = PUTABLE003.COLUMN05 LEFT OUTER JOIN MATABLE007 AS i ON i.COLUMN02 = PUTABLE004.COLUMN03   where " + OPUnitWithNull + " AND PUTABLE003.COLUMNA03='" + Session["AcOwner"] + "' order by  PUTABLE003.COLUMN09	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Bill Report")
                    {
                        string OPUnitWithNull = " PUTABLE005.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " PUTABLE005.COLUMNA02 in(" + Session["OPUnit"] + ") or PUTABLE005.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string DateF = Session["FormatDate"].ToString();
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/Bill.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT PUTABLE005.COLUMN04, FORMAT(PUTABLE005.COLUMN08,'" + DateF + "'), s.COLUMN05, PUTABLE005.COLUMN06, PUTABLE005.COLUMN09, PUTABLE005.COLUMN12,PUTABLE005.COLUMN14, PUTABLE005.COLUMN13, i.COLUMN04 AS Expr1, PUTABLE006.COLUMN07, PUTABLE006.COLUMN08 AS Expr2, PUTABLE006.COLUMN10, PUTABLE006.COLUMN11, PUTABLE006.COLUMN12 AS Expr3 FROM PUTABLE005 INNER JOIN PUTABLE006 ON PUTABLE005.COLUMN01 = PUTABLE006.COLUMN13 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = PUTABLE005.COLUMN05 LEFT OUTER JOIN MATABLE007 AS i ON i.COLUMN02 = PUTABLE006.COLUMN04   where " + OPUnitWithNull + " AND PUTABLE005.COLUMNA03='" + Session["AcOwner"] + "' order by  PUTABLE005.COLUMN08	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "ExpenseTracking")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Project = Request.QueryString["Project"];
                        string Employee = Request.QueryString["Employee"];
                        string Account = Request.QueryString["Account"];
                        ReportViewer1.Reset();
                        //EMPHCS1197	 EXPENSE TRACKING Report not displaying spent amount BY RAJ.Jr 23/9/2015
                        //EMPHCS1563	Expense Tracking Report Changes-Booking and Payment By RAJ.Jr
                        ReportViewer1.LocalReport.ReportPath = "Content/ExpenseTracking.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);

                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandTimeout = 0;
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Proc_ExpenseTrachReport";
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@Employee", Employee);
                        cmd.Parameters.AddWithValue("@AType", Account);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Pay Bill Report")
                    {
                        string OPUnitWithNull = " PUTABLE014.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " PUTABLE014.COLUMNA02 in(" + Session["OPUnit"] + ") or PUTABLE014.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string DateF = Session["FormatDate"].ToString();
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PB.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT  PUTABLE014.COLUMN04, FORMAT(PUTABLE014.COLUMN18,'" + DateF + "'), s.COLUMN05, PUTABLE014.COLUMN06, PUTABLE014.COLUMN07, PUTABLE014.COLUMN08, PUTABLE015.COLUMN04 AS Expr2, PUTABLE015.COLUMN05 AS Expr3, PUTABLE015.COLUMN06 AS Expr, PUTABLE014.COLUMN11, PUTABLE015.COLUMN03, PUTABLE015.COLUMN04 AS Expr1, PUTABLE015.COLUMN05 AS Expr4, PUTABLE015.COLUMN06 AS Expr5, PUTABLE015.COLUMN07 AS Expr6 FROM PUTABLE014 INNER JOIN PUTABLE015 ON PUTABLE014.COLUMN01 = PUTABLE015.COLUMN08 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = PUTABLE014.COLUMN05  where " + OPUnitWithNull + " AND PUTABLE014.COLUMNA03='" + Session["AcOwner"] + "'  order by  PUTABLE014.COLUMN18	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Sales Order Report")
                    {
                        string OPUnitWithNull = " SATABLE005.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " SATABLE005.COLUMNA02 in(" + Session["OPUnit"] + ") or SATABLE005.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SO.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT        SATABLE005.COLUMN04, SATABLE005.COLUMN06, s.COLUMN05, SATABLE005.COLUMN08, SATABLE005.COLUMN09, SATABLE005.COLUMN11, SATABLE005.COLUMN15, SATABLE005.COLUMN16, i.COLUMN04 AS Expr4, SATABLE006.COLUMN04 AS Expr1, SATABLE006.COLUMN07, SATABLE006.COLUMN09 AS Expr2, SATABLE006.COLUMN10, SATABLE006.COLUMN11 AS Expr3 FROM  SATABLE005 INNER JOIN SATABLE006 ON SATABLE005.COLUMN01 = SATABLE006.COLUMN19 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = SATABLE005.COLUMN05 LEFT OUTER JOIN MATABLE007 AS i ON i.COLUMN02 = SATABLE006.COLUMN03 where SATABLE005.COLUMN04 like 'SO%'  AND " + OPUnitWithNull + " AND SATABLE005.COLUMNA03='" + Session["AcOwner"] + "' order by  SATABLE005.COLUMN06	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Item Issue Report")
                    {
                        string OPUnitWithNull = " SATABLE007.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " SATABLE007.COLUMNA02 in(" + Session["OPUnit"] + ") or SATABLE007.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/II.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT        SATABLE007.COLUMN04, SATABLE007.COLUMN08, s.COLUMN05, SATABLE007.COLUMN06, SATABLE007.COLUMN09, SATABLE007.COLUMN13,SATABLE007.COLUMN14, i.COLUMN04 AS Expr1, SATABLE008.COLUMN07, SATABLE008.COLUMN08 AS Expr2, SATABLE008.COLUMN10, SATABLE008.COLUMN12, SATABLE008.COLUMN13 AS Expr3 FROM SATABLE007 INNER JOIN SATABLE008 ON SATABLE007.COLUMN01 = SATABLE008.COLUMN14 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = SATABLE007.COLUMN05 LEFT OUTER JOIN MATABLE007 AS i ON i.COLUMN02 = SATABLE008.COLUMN04 where SATABLE007.COLUMN04 like 'II%'  AND " + OPUnitWithNull + " AND SATABLE007.COLUMNA03='" + Session["AcOwner"] + "' order by  SATABLE007.COLUMN08	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Invoice Report")
                    {
                        string OPUnitWithNull = " SATABLE009.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " SATABLE009.COLUMNA02 in(" + Session["OPUnit"] + ") or SATABLE009.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/Invoice.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT        SATABLE009.COLUMN04, s.COLUMN05, SATABLE009.COLUMN06, SATABLE009.COLUMN09, SATABLE009.COLUMN10, SATABLE009.COLUMN12,  SATABLE009.COLUMN13, i.COLUMN04 AS Expr1, SATABLE010.COLUMN08, SATABLE010.COLUMN09 AS Expr2, SATABLE010.COLUMN12 AS Expr3, SATABLE010.COLUMN13 AS Expr4, SATABLE010.COLUMN14 FROM  SATABLE009 INNER JOIN SATABLE010 ON SATABLE009.COLUMN01 = SATABLE010.COLUMN15 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = SATABLE009.COLUMN05 LEFT OUTER JOIN MATABLE007 AS i ON i.COLUMN02 = SATABLE010.COLUMN05 where SATABLE009.COLUMN04 like 'IV%'  AND " + OPUnitWithNull + " AND SATABLE009.COLUMNA03='" + Session["AcOwner"] + "'  order by  SATABLE009.COLUMN10	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Payment")
                    {
                        string OPUnitWithNull = " SATABLE011.COLUMNA02 in(" + Session["OPUnit"] + ")";
                        if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                            OPUnitWithNull = " SATABLE011.COLUMNA02 in(" + Session["OPUnit"] + ") or SATABLE011.COLUMNA02 is null";
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/Payment.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        string strqrt = "SELECT        SATABLE011.COLUMN04, s.COLUMN05, SATABLE011.COLUMN06, SATABLE011.COLUMN07, SATABLE011.COLUMN08, SATABLE011.COLUMN09,SATABLE011.COLUMN10, SATABLE011.COLUMN11, SATABLE011.COLUMN13, SATABLE012.COLUMN03, SATABLE012.COLUMN04 AS Expr1, SATABLE012.COLUMN05 AS Expr2, SATABLE012.COLUMN06 AS Expr3, SATABLE012.COLUMN07 AS Expr4 FROM   SATABLE011 INNER JOIN SATABLE012 ON SATABLE011.COLUMN01 = SATABLE012.COLUMN08 LEFT OUTER JOIN SATABLE001 AS s ON s.COLUMN02 = SATABLE011.COLUMN05 where SATABLE011.COLUMN04 like 'PB%' AND " + OPUnitStatus + " AND SATABLE011.COLUMNA03='" + Session["AcOwner"] + "' order by  SATABLE011.COLUMN19	desc";
                        SqlCommand cmd = new SqlCommand(strqrt, con);
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "OpeningBalance")
                    {
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/OpeningBalance.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "OpeningBalance");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "InventoryAsset")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InventoryAsset.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_INVENTORYASSET", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "InventoryAsset");
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                        cmd.Parameters.AddWithValue("@Project", Project);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SOByCustomer")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SOByCustomer.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SOByCustomer");
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "Customer Discount")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/Discount.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);

                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Discount_Report";
                        // cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "POSummary");
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1160 VAT REport BY RAJ.Jr 01/10/2015
                    else if (Request.QueryString["ReportName"] == "VATComputation")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/VATComputation.rdlc";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "VATComputation");
                        cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        ReportViewer1.LocalReport.Refresh();
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        this.ReportViewer1.LocalReport.Refresh();
                    }
                    //EMPHCS1210	Inventory by UOM Report BY RAJ.Jr 25/9/2015
                    else if (Request.QueryString["ReportName"] == "InventoryByUOM")
                    {
                        //string FDate = Request.QueryString["FDate"];
                        //string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Item = Request.QueryString["Item"];
                        //string UOM = Request.QueryString["UOM"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InventoryByUOM.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "InventoryByUOM");
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        //cmd.Parameters.AddWithValue("@UPC", UOM);
                        //cmd.Parameters.AddWithValue("@FromDate", FDate);
                        //cmd.Parameters.AddWithValue("@ToDate", TDate);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        cmd.CommandTimeout = 0;
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1551	Sales and Purchase summary and detail Reports By SRINI
                    else if (Request.QueryString["ReportName"] == "SalesByProductSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByProductSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByProductSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SalesByProductDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByProductDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByProductDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1556	PurchaseBySuppliersDetails & PurchaseBySuppliersSummary Reports BY RAJ.Jr
                    else if (Request.QueryString["ReportName"] == "PurchasesBySuppliersDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string Vendor = Request.QueryString["Vendor"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchasesBySuppliersDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("PurchaseSuppliersGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchasesBySuppliersDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1556	PurchaseBySuppliersDetails & PurchaseBySuppliersSummary Reports BY RAJ.Jr
                    else if (Request.QueryString["ReportName"] == "PurchasesBySuppliersSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchasesBySuppliersSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("PurchaseSuppliersGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchasesBySuppliersSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1557 : PurchaseByProductDetails & PurchaseByProductSummary Reports By GNANESHWAR ON 9/2/2016
                    else if (Request.QueryString["ReportName"] == "PurchaseByProductSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchaseByProductSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("PurchaseGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchaseByProductSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1557 : PurchaseByProductDetails & PurchaseByProductSummary Reports By GNANESHWAR ON 9/2/2016
                    else if (Request.QueryString["ReportName"] == "PurchaseByProductDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchaseByProductDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("PurchaseGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchaseByProductDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "SalesByCustomerSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        // SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                        SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomer");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@TransDate", TransDate);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    else if (Request.QueryString["ReportName"] == "OpporutinesReport")
                    {

                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Priority = Request.QueryString["Priority"];
                        string SalesPerson = Request.QueryString["SalesPerson"];
                        string Status = Request.QueryString["Status"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/OpporutinesReport.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("OpporutinesReport", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "OpporutinesReport");
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Priority", Priority);
                        cmd.Parameters.AddWithValue("@SalesPerson", SalesPerson);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        //cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1558 : SalesBycustomerDetails & SalesByCustomerSummary Reports By GNANESHWAR ON 9/2/2016
                    else if (Request.QueryString["ReportName"] == "SalesByCustomerDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomer");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        // cmd.Parameters.AddWithValue("@TransDate", TransDate);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }




                //CHNANGE BY GNANESHWAR ON 6/9/2016
                    else if (Request.QueryString["ReportName"] == "SalesByCustomerDateWiseSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerDateWiseDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomer");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@TransDate", TransDate);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SalesByCustomerDateWiseDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        string Brand = Request.QueryString["Brand"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerDateWiseDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomer");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        // cmd.Parameters.AddWithValue("@TransDate", TransDate);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }





     //EMPHCS1562	New Reports For Returns BY RAJ.Jr
                    else if (Request.QueryString["ReportName"] == "DebitMemoBySuppliersDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Vendor = Request.QueryString["Vendor"];
                        string Customer = Request.QueryString["Customer"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoBySuppliersDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "DebitMemoBySuppliersDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }

                    else if (Request.QueryString["ReportName"] == "DebitMemoBySuppliersSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoBySuppliersSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "DebitMemoBySuppliersSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CreditMemoByCustomerDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        string Vendor = Request.QueryString["Vendor"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Category = Request.QueryString["Category"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByCustomerDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByCustomerDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@Vendor", Vendor);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        cmd.Parameters.AddWithValue("@Category", Category);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1562	New Reports For Returns BY RAJ.Jr
                    else if (Request.QueryString["ReportName"] == "CreditMemoByCustomerSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string Category = Request.QueryString["Category"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByCustomerSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByCustomerSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("Brand", Brand);
                        cmd.Parameters.AddWithValue("Location", Location);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        cmd.Parameters.AddWithValue("@Category", Category);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "DebitMemoByProductDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoByProductDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "DebitMemoByProductDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "DebitMemoByProductSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoByProductSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "DebitMemoByProductSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CreditMemoByProductDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByProductDetails.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByProductDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "CreditMemoByProductSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string TransDate = Request.QueryString["TransDate"];
                        string Brand = Request.QueryString["Brand"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByProductSummary.rdl";
                        //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByProductSummary");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1580 rajasekhar reddy patakota 27/02/2016 Adding New Auditor Tax Reports
                    //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
                    else if (Request.QueryString["ReportName"] == "SalesTaxRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesTaxRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        using (SqlConnection con = new SqlConnection(sqlcon))
                        {
                            SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ReportName", "SalesTaxRegister");
                            cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                            cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                            cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                            cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                            if (Type != "")
                                cmd.Parameters.AddWithValue("@Type", Type);
                            if (Taxes != "")
                                cmd.Parameters.AddWithValue("@Taxes", Taxes);
                            cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                            cmd.Parameters.AddWithValue("@Brand", Brand);
                            cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dtCustomers = new DataTable();
                            cmd.CommandTimeout = 0;
                            da.Fill(dtCustomers);
                            cmd.Dispose();
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                            ReportViewer1.LocalReport.DataSources.Add(rds);
                            ReportViewer1.DataBind();
                            ReportViewer1.LocalReport.Refresh();
                            ColorCodes();
                        }
                    }
                    else if (Request.QueryString["ReportName"] == "SalesTaxRegisterDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesTaxRegisterDetails.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        using (SqlConnection con = new SqlConnection(sqlcon))
                        {
                            SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ReportName", "SalesTaxRegisterDetails");
                            cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                            cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                            cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                            cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                            if (Type != "")
                                cmd.Parameters.AddWithValue("@Type", Type);
                            if (Taxes != "")
                                cmd.Parameters.AddWithValue("@Taxes", Taxes);
                            cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                            cmd.Parameters.AddWithValue("@Brand", Brand);
                            cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            cmd.CommandTimeout = 0;
                            DataTable dtCustomers = new DataTable();
                            da.Fill(dtCustomers);
                            cmd.Dispose();
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                            ReportViewer1.LocalReport.DataSources.Add(rds);
                            ReportViewer1.DataBind();
                            ReportViewer1.LocalReport.Refresh();
                            ColorCodes();
                        }
                    }
                    //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
                    else if (Request.QueryString["ReportName"] == "SalesSevicesTaxRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        //EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesSevicesTaxRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesServicesTaxRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016

                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        if (Taxes != "")
                            cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PurchaseTaxRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchaseTaxRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchaseTaxRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        if (Taxes != "")
                            cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PurchaseTaxRegisterDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];

                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchaseTaxRegisterDetails.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchaseTaxRegisterDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        if (Taxes != "")
                            cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SalesReturnTaxRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesReturnTaxRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesReturnTaxRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        if (Taxes != "")
                            cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PurchaseReturnTaxRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchaseReturnTaxRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchaseReturnTaxRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        if (Taxes != "")
                            cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "PurchaseSevicesTaxRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        string Brand = Request.QueryString["Brand"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/PurchaseSevicesTaxRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "PurchaseServicesTaxRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //EMPHCS1734 : Placing Brand Filter in ll Taxation Reports by GNANESHWAR on 14/5/2016
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        if (Taxes != "")
                            cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "TDSRegister")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Type = Request.QueryString["Type"];
                        string Taxes = Request.QueryString["Taxes"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        //EMPHCS1637  Finance Reports TAXATION Reports COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 1/4/2016
                        Session["FDate"] = FDate;
                        Session["TDate"] = TDate;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/TDSRegister.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditorTaxReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "TDSRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        if (Type != "")
                            cmd.Parameters.AddWithValue("@Type", Type);
                        //cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }// IP Config && Audit Information By Venkat
                    else if (Request.QueryString["ReportName"] == "AuditInformation")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string User = Request.QueryString["User"];
                        string EmailID = Request.QueryString["EmailID"];
                        string LoginDateTime = Request.QueryString["LoginDateTime"];
                        string LogoutDateTime = Request.QueryString["LogoutDateTime"];
                        string IPAddress = Request.QueryString["IPAddress"];
                        string Browser = Request.QueryString["Browser"];
                        Session["LoginDateTime"] = LoginDateTime;
                        Session["LogoutDateTime"] = LogoutDateTime;
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/AuditInformation.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_AUDITINFORMATION", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@ReportName", "TDSRegister");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@User", User);
                        cmd.Parameters.AddWithValue("@EmailID", EmailID);
                        cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                        cmd.Parameters.AddWithValue("@Browser", Browser);
                        //if (Type != "")
                        //cmd.Parameters.AddWithValue("@Type", Type);
                        //cmd.Parameters.AddWithValue("@Taxes", Taxes);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    //EMPHCS1801 rajasekhar reddy patakota 25/08/2016 Grey Report Creation in jobbing in module
                    else if (Request.QueryString["ReportName"] == "GreyReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Joborderno = Request.QueryString["Joborderno"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/GreyReport.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_GreyReport", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "GreyReport");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Joborderno", Joborderno);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "GreyReportSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string Joborderno = Request.QueryString["Joborderno"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/GreyReportSummary.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_GreyReportSummary", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "GreyReportSummary");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Joborderno", Joborderno);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "JobbingLedger")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        string Project = Request.QueryString["Project"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/JobbingLedger.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_JOBBINGLEDGER", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "JobbingLedger");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "SalesByProductDetailReport")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        string Item = Request.QueryString["Item"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/SalesByProductDetailReport.rdl";

                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "SalesByProductDetailReport");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "InvoiceBillDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];

                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        //string Project = Request.QueryString["Project"];
                        string DCNO = Request.QueryString["DCNO"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InvoiceBillDetails.rdl";

                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_InvoiceBillDetails", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "InvoiceBillDetails");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@DCNO", DCNO);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));

                        cmd.Parameters.AddWithValue("@Location", Location);

                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);

                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "StockMoment")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockMoment.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKMOMENT", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockMoment");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", "");

                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));

                        cmd.Parameters.AddWithValue("@Location", Location);

                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                            else if (Request.QueryString["ReportName"] == "BarcodeWiseStock")
                            {
                                string Item = Request.QueryString["Item"];
                                string UPC = Request.QueryString["UPC"];
                                //string OperatingUnit = Request.QueryString["OperatingUnit"];
                                ReportViewer1.Reset();
                                ReportViewer1.LocalReport.ReportPath = "Content/BarcodeWiseStock.rdl";
                                SqlConnection con = new SqlConnection(sqlcon);
                                SqlCommand cmd = new SqlCommand("BarCodeWiseStock", con);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Item", Item);
                                cmd.Parameters.AddWithValue("@UPC", UPC);
                                cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                                cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                                cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                    else if (Request.QueryString["ReportName"] == "InventoryExpiry")
                    {
                        string FromDate = Request.QueryString["FromDate"];
                        string ToDate = Request.QueryString["ToDate"];
                        string Item = Request.QueryString["Item"];
                        string Batch = Request.QueryString["Batch"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/InventoryExpiryCheck.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_INVENTORYEXPIRY", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FromDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(ToDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Batch", Batch);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ProjectPriceAnalysis")
                    {
                        string FromDate = Request.QueryString["FDate"];
                        string ToDate = Request.QueryString["TDate"];
                        string Project = Request.QueryString["Project"];
                        string SubProject = Request.QueryString["SubProject"];
                        //string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ProjectPriceAnalysis.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_ProjectPriceAnalysis", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@SubProject", SubProject);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FromDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(ToDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", Session["OPUnit1"]);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "ActiveCustomers")
                    {
                        //string FromDate = Request.QueryString["FDate"];
                        //string ToDate = Request.QueryString["TDate"];
                        //string Project = Request.QueryString["Project"];
                        //string SubProject = Request.QueryString["SubProject"];
                        //string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/ActiveCustomers.rdl";
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("Last_Used", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@OPUnit", Session["OPUnit1"]);
                        //cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        //cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "AuditTrackingReport") 
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/AuditTrackingReport.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("AuditTrackingReport", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet3", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                Response.Write("<script type=text/javascript> window.parent.location.href ='/EmployeeMaster/Info';</script>");

            }
        }
        public void SetSubDataSource(object sender, SubreportProcessingEventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlcon);
            var id = e.Parameters["taxid"].Values[0];
            var opunit = e.Parameters["opunit"].Values[0];
            var acowner = e.Parameters["acowner"].Values[0];
            var type = e.Parameters["type"].Values[0];
            var frmdate = Session["FDate"];
            var todate = Session["TDate"];
            SqlCommand cmds = new SqlCommand("VatInfoDetails", con);
            cmds.CommandType = CommandType.StoredProcedure;
            cmds.Parameters.AddWithValue("@id", id);
            cmds.Parameters.AddWithValue("@opunit", opunit);
            cmds.Parameters.AddWithValue("@acowner", acowner);
            cmds.Parameters.AddWithValue("@type", type);
            cmds.Parameters.AddWithValue("@frmdate", DateTime.ParseExact(frmdate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
            cmds.Parameters.AddWithValue("@todate", DateTime.ParseExact(todate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
            cmds.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
            SqlDataAdapter das = new SqlDataAdapter(cmds);
            DataSet subreport = new DataSet();
            das.Fill(subreport);
            e.DataSources.Add(new ReportDataSource("DataSet1", subreport.Tables[0]));
        }

        public void ColorCodes()
        {
            string FDate = Request.QueryString["FDate"];
            string TDate = Request.QueryString["TDate"];

            if ((FDate == null && TDate == null) || (FDate == "undefined" && TDate == "undefined"))
            {
                var dt = DateTime.Now.ToString(Session["FormatDate"].ToString());

                FDate = dt;
                TDate = dt;

            }

            SqlConnection con = new SqlConnection(sqlcon);
            SqlCommand cmdp = new SqlCommand("SELECT m.COLUMN03 AS [COLOR TYPE], m.COLUMN04 AS TABLEHEADERBACKCOLOR, m.COLUMN05 AS TABLEHEADERFORECOLOR,  m.COLUMN06 AS TABLEROWBACKCOLOR1, m.COLUMN07 AS TABLEROWBACKCOLOR2, m.COLUMN08 AS TABLEROWFORECOLOR, m.COLUMN09 AS [HOVER COLOR], m.COLUMN10 AS [TITTLE BAR COLOR],isnull(c8.COLUMN03,' ') Cname FROM   MYTABLE003 m left outer join CONTABLE008 c8 on c8.columna03='" + Session["AcOwner"] + "' and (c8.columna02='" + Session["OPUnit1"] + "' or c8.columna02 is null) and isnull(c8.column05,0)=0 and isnull(c8.columna13,0)=0 where m.column03='" + color + "'", con);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            ReportDataSource rdsp = new ReportDataSource("DataSet2", dtp);
            ReportViewer1.LocalReport.DataSources.Add(rdsp);
            ReportViewer1.DataBind();
            ReportParameter[] myParams = new ReportParameter[]{
                    new ReportParameter("ReportParameter1", dtp.Rows[0]["TABLEHEADERBACKCOLOR"].ToString()),
                    new ReportParameter("ReportParameter2", dtp.Rows[0]["TABLEROWBACKCOLOR1"].ToString()),
                    new ReportParameter("ReportParameter3", dtp.Rows[0]["TABLEROWBACKCOLOR2"].ToString()),
                    new ReportParameter("ReportParameter4", dtp.Rows[0]["TABLEROWFORECOLOR"].ToString()),
                    new ReportParameter("ReportParameter5", dtp.Rows[0]["Cname"].ToString()),
                    new ReportParameter("ReportParameter6",FDate),
                    new ReportParameter("ReportParameter7", TDate)};
            ReportViewer1.LocalReport.SetParameters(myParams);
            ReportViewer1.LocalReport.Refresh();
        }
    }
}
