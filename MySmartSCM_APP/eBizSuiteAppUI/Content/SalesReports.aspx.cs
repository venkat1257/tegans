﻿using eBizSuiteAppModel.Table;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.Data;

namespace MeBizSuiteAppUI.Content
{
    public partial class SalesReports : System.Web.UI.Page
    {
        string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
        string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
        string providerName = "System.Data.SqlClient";
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities(); string color = "Blue";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["username"] == null)
            {
                Response.Write("<script type=text/javascript> window.parent.location.href ='/EmployeeMaster/Info';</script>");
            }
                try
                {
            if (!IsPostBack)
            {
                int? eID = Convert.ToInt32(Session["eid"]);
                var colortype = dbContext.MYTABLE001.Where(a => a.COLUMN09 == eID).FirstOrDefault();
                if (colortype != null)
                { color = colortype.COLUMN06; }
                string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                if (Request.QueryString["ReportName"] == "SalesByProductSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesByProductSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);

                    //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesByProductSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "SalesByProductDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesByProductDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesByProductDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "SalesByCustomerSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Brand = Request.QueryString["Brand"];
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    string Customer = Request.QueryString["Customer"];
                    string Category = Request.QueryString["Category"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                   //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomer");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    ////cmd.Parameters.AddWithValue("@TransDate", TransDate);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    cmd.Parameters.AddWithValue("@Category", Category);
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }

                else if (Request.QueryString["ReportName"] == "SalesByCustomerDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    string Category = Request.QueryString["Category"];
                    string Customer = Request.QueryString["Customer"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomer");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    cmd.Parameters.AddWithValue("@Category", Category); 
                    cmd.Parameters.AddWithValue("@Customer", Customer);                    
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                     //cmd.Parameters.AddWithValue("@TransDate", TransDate);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }


                else if (Request.QueryString["ReportName"] == "SalesRegister")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Customer = Request.QueryString["Customer"];
                    string SalesRep = Request.QueryString["SalesRep"];
                    string Brand = Request.QueryString["Brand"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesRegister.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("SalesRegister", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesRegister");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@SalesRep", SalesRep);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    cmd.CommandTimeout = 0;
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }








                else if (Request.QueryString["ReportName"] == "SalesByCustomerDateWiseSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Brand = Request.QueryString["Brand"];
                    string DisplayType = Request.QueryString["DisplayType"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerDateWiseSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomerDateWise");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //cmd.Parameters.AddWithValue("@TransDate", TransDate);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@DisplayType", DisplayType);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }

                else if (Request.QueryString["ReportName"] == "SalesByCustomerDateWiseDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    string Customer = Request.QueryString["Customer"];
                    string Category = Request.QueryString["Category"];
                    string TransDate = Request.QueryString["TransDate"];
                    string DisplayType = Request.QueryString["DisplayType"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesByCustomerDateWiseDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("SalesGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesByCustomerDateWise");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@DisplayType", DisplayType);
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    // cmd.Parameters.AddWithValue("@TransDate", TransDate);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    cmd.Parameters.AddWithValue("@Category", Category);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }

                else if (Request.QueryString["ReportName"] == "PurchasesBySuppliersDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
                    string Vendor = Request.QueryString["Vendor"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/PurchasesBySuppliersDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    // SqlCommand cmd = new SqlCommand("PurchaseSuppliersGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "PurchasesBySuppliersDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Vendor", Vendor);
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }

                else if (Request.QueryString["ReportName"] == "PurchaseRegister")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    //string Brand = Request.QueryString["Brand"];
                    string Vendor = Request.QueryString["Vendor"];
                    //string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/PurchaseRegister.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    // SqlCommand cmd = new SqlCommand("PurchaseSuppliersGetReports", con);
                    SqlCommand cmd = new SqlCommand("PurchaseRegister", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "PurchaseRegister");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Vendor", Vendor);
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    //cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }


                else if (Request.QueryString["ReportName"] == "PurchasesBySuppliersSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Brand = Request.QueryString["Brand"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/PurchasesBySuppliersSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "PurchasesBySuppliersSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "PurchaseByProductSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/PurchaseByProductSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("PurchaseGetReports", con);
                    //SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "PurchaseByProductSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "PurchaseByProductDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    string Brand = Request.QueryString["Brand"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Item = Request.QueryString["Item"];
                    string HSN = Request.QueryString["HSN"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/PurchaseByProductDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                   // SqlCommand cmd = new SqlCommand("PurchaseGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "PurchaseByProductDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //EMPHCS1676 : Brand Filter in Purchase And Sales Product ,CUstomer detailes and sumary by gnaneshwar on 9/4/2016
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@Item", Item);
                    cmd.Parameters.AddWithValue("@HSN", HSN);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }

                else if (Request.QueryString["ReportName"] == "DebitMemoBySuppliersDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Vendor = Request.QueryString["Vendor"];
                    string Customer = Request.QueryString["Customer"];
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoBySuppliersDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "DebitMemoBySuppliersDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Vendor", Vendor);
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "DebitMemoBySuppliersSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoBySuppliersSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "DebitMemoBySuppliersSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "DebitMemoByProductDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoByProductDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "DebitMemoByProductDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "CreditMemoByProductDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByProductDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByProductDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "DebitMemoByProductSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/DebitMemoByProductSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "DebitMemoByProductSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "CreditMemoByCustomerDetails")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Customer = Request.QueryString["Customer"];
                    string Vendor = Request.QueryString["Vendor"];
                    string Brand = Request.QueryString["Brand"];
                    string Category = Request.QueryString["Category"];
                    string Location = Request.QueryString["Location"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByCustomerDetails.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByCustomerDetails");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@Vendor", Vendor);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    cmd.Parameters.AddWithValue("@Category", Category);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "CreditMemoByCustomerSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    string Category = Request.QueryString["Category"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByCustomerSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                     //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByCustomerSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("Brand", Brand);
                    cmd.Parameters.AddWithValue("Location", Location);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    cmd.Parameters.AddWithValue("@Category", Category);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "CreditMemoByProductSummary")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string TransDate = Request.QueryString["TransDate"];
                    string Brand = Request.QueryString["Brand"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/CreditMemoByProductSummary.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //SqlCommand cmd = new SqlCommand("ReturnsGetReports", con);
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "CreditMemoByProductSummary");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "VendorPaymentDues")
                {
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Vendor = Request.QueryString["Vendor"];
                    //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Brand = Request.QueryString["Brand"];
                    string TransDate = Request.QueryString["TransDate"];

                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/VendorPaymentDues.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                    //SqlCommand cmd = new SqlCommand("CVPaymentDues", con); 
                    SqlCommand cmd = new SqlCommand("GetPurchaseReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "VendorPaymentDues");
                    cmd.Parameters.AddWithValue("@Vendor", Vendor);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);

                    //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }


                else if (Request.QueryString["ReportName"] == "CustomerPaymentDues")
                {
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Customer = Request.QueryString["Customer"];
                    //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Brand = Request.QueryString["Brand"];
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    string Category = Request.QueryString["Category"];
                    string TransDate = Request.QueryString["TransDate"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/CustomerPaymentDues.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                  //SqlCommand cmd = new SqlCommand("CVPaymentDues", con);
                   SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "CustomerPaymentDues");
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    cmd.Parameters.AddWithValue("@Category", Category);
                    //EMPHCS1755 : Adding Brand Filter to Vendor and customer Payment Dues and Apply Logo Print in Invoice By Gnaneshwar on 14/6/2016
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.CommandTimeout = 0;
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "SalesInvoiceRK")
                {
                    //string FDate = Request.QueryString["FDate"];
                    //string TDate = Request.QueryString["TDate"];
                    
                    //string Brand = Request.QueryString["Brand"];
                   
                    //string Category = Request.QueryString["Category"];
                    string Customer = Request.QueryString["Customer"];
                    //string TransDate = Request.QueryString["TransDate"];
                    //string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/SalesinvoiceRK.rdl";
                    
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    
                    SqlCommand cmd = new SqlCommand("GetSalesReports", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "SalesInvoiceRK");
                    //cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    
                    //cmd.Parameters.AddWithValue("@Brand", Brand);
                    
                    //cmd.Parameters.AddWithValue("@Category", Category);
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    
                    //cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    //cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.CommandTimeout = 0;
                        DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                    else if (Request.QueryString["ReportName"] == "MonthWiseSalesDetails")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Customer = Request.QueryString["Customer"];
                        string Brand = Request.QueryString["Brand"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/RETAILORSYEARWISEDATA.rdl";
                        
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        
                        SqlCommand cmd = new SqlCommand("RETAILYEARWISEDATA", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "MonthWiseSalesDetails");
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        
                        cmd.Parameters.AddWithValue("@Customer", Customer);
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        cmd.CommandTimeout = 0;
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                }

        }

                catch (Exception ex)
                {
                    var msg = "Page Not Found......";
                    Session["MessageFrom"] = msg + "Due to" + ex.Message;
                    Session["SuccessMessageFrom"] = "fail";
                    Response.Write("<script type=text/javascript> window.parent.location.href ='/EmployeeMaster/Info';</script>");

                }
            }
        public void SetSubDataSource(object sender, SubreportProcessingEventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlcon);
            var id = e.Parameters["taxid"].Values[0];
            var opunit = e.Parameters["opunit"].Values[0];
            var acowner = e.Parameters["acowner"].Values[0];
            var type = e.Parameters["type"].Values[0];
            var frmdate = Session["FDate"];
            var todate = Session["TDate"];
            SqlCommand cmds = new SqlCommand("VatInfoDetails", con);
            cmds.CommandType = CommandType.StoredProcedure;
            cmds.Parameters.AddWithValue("@id", id);
            cmds.Parameters.AddWithValue("@opunit", opunit);
            cmds.Parameters.AddWithValue("@acowner", acowner);
            cmds.Parameters.AddWithValue("@type", type);
            cmds.Parameters.AddWithValue("@frmdate", DateTime.ParseExact(frmdate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
            cmds.Parameters.AddWithValue("@todate", DateTime.ParseExact(todate.ToString(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
            cmds.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
            SqlDataAdapter das = new SqlDataAdapter(cmds);
            DataSet subreport = new DataSet();
            das.Fill(subreport);
            e.DataSources.Add(new ReportDataSource("DataSet1", subreport.Tables[0]));
        }

        public void ColorCodes()
        {
            string FDate = Request.QueryString["FDate"];
            string TDate = Request.QueryString["TDate"];
            if ((FDate == null && TDate == null) || (FDate == "undefined" && TDate == "undefined"))
           
            {
                var dt = DateTime.Now.ToString(Session["FormatDate"].ToString());

                FDate = dt;
                TDate = dt;

            }
            SqlConnection con = new SqlConnection(sqlcon);
            SqlCommand cmdp = new SqlCommand("SELECT m.COLUMN03 AS [COLOR TYPE], m.COLUMN04 AS TABLEHEADERBACKCOLOR, m.COLUMN05 AS TABLEHEADERFORECOLOR,  m.COLUMN06 AS TABLEROWBACKCOLOR1, m.COLUMN07 AS TABLEROWBACKCOLOR2, m.COLUMN08 AS TABLEROWFORECOLOR, m.COLUMN09 AS [HOVER COLOR], m.COLUMN10 AS [TITTLE BAR COLOR],isnull(c8.COLUMN03,' ') Cname FROM MYTABLE003 m left outer join CONTABLE008 c8 on c8.columna03='" + Session["AcOwner"] + "' and (c8.columna02='" + Session["OPUnit1"] + "' or c8.columna02 is null) and isnull(c8.column05,0)=0 and isnull(c8.columna13,0)=0 where m.column03='" + color + "'", con);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            ReportDataSource rdsp = new ReportDataSource("DataSet2", dtp);
            ReportViewer1.LocalReport.DataSources.Add(rdsp);
            ReportViewer1.DataBind();
            ReportParameter[] myParams = new ReportParameter[]{
                    new ReportParameter("ReportParameter1", dtp.Rows[0]["TABLEHEADERBACKCOLOR"].ToString()),
                    new ReportParameter("ReportParameter2", dtp.Rows[0]["TABLEROWBACKCOLOR1"].ToString()),
                    new ReportParameter("ReportParameter3", dtp.Rows[0]["TABLEROWBACKCOLOR2"].ToString()),
                    new ReportParameter("ReportParameter4", dtp.Rows[0]["TABLEROWFORECOLOR"].ToString()),
                    new ReportParameter("ReportParameter5", dtp.Rows[0]["Cname"].ToString()),
                   new ReportParameter("ReportParameter6",FDate),
                   new ReportParameter("ReportParameter7",TDate)};
            ReportViewer1.LocalReport.SetParameters(myParams);
            ReportViewer1.LocalReport.Refresh();
        }
    }
}